

   MEMBER('WebServer_Phase3.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER_PHASE3008.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER_PHASE3002.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE3003.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE3005.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE3009.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE3010.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE3011.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE3012.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE3013.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE3014.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE3015.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE3016.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER_PHASE3019.INC'),ONCE        !Req'd for module callout resolution
                     END


clearTagFile         PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
FilesOpened     BYTE(0)
  CODE
    do openFiles

    Access:TAGFILE.Clearkey(tag:keyTagged)
    tag:sessionID    = p_web.sessionID
    set(tag:keyTagged,tag:keyTagged)
    loop
        if (Access:TAGFILE.Next())
            Break
        end ! if (Access:TAGFILE.Next())
        if (tag:sessionID    <> p_web.sessionID)
            Break
        end ! if (tag:sessionID    <> p_web.sessionID)
        DELETE(TAGFILE)
    end ! loop

    do closeFiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:TagFile.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TagFile.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:TagFile.Close
     FilesOpened = False
  END
FormJobsInBatch      PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
FilesOpened     Long
MULDESP::State  USHORT
MULDESPJ::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormJobsInBatch')
  loc:formname = 'FormJobsInBatch_frm'
  WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('FormJobsInBatch','')
    p_web._DivHeader('FormJobsInBatch',clip('fdiv') & ' ' & clip('FormContent'))
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormJobsInBatch',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of InsertRecord + NET:WEB:StagePost
    do RestoreMem
    do PostInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormJobsInBatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:StagePost
    do RestoreMem
    do PostCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormJobsInBatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormJobsInBatch',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    If loc:act = InsertRecord
      do PostInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do PostUpdate
    End

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormJobsInBatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of DeleteRecord + NET:WEB:StagePost
    do RestoreMem
    do PostDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormJobsInBatch',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(MULDESP)
  p_web._OpenFile(MULDESPJ)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(MULDESP)
  p_Web._CloseFile(MULDESPJ)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FormJobsInBatch_form:inited_',1)
  p_web.SetValue('UpdateFile','MULDESPJ')
  p_web.SetValue('UpdateKey','mulj:RecordNumberKey')
  p_web.SetValue('IDField','mulj:RecordNumber')
  do RestoreMem

CancelForm  Routine
  IF p_web.GetSessionValue('FormJobsInBatch:Primed') = 1
    p_web._deleteFile(MULDESPJ)
    p_web.SetSessionValue('FormJobsInBatch:Primed',0)
  End

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  p_web.SetValue('UpdateFile','MULDESPJ')
  p_web.SetValue('UpdateKey','mulj:RecordNumberKey')
AfterLookup Routine
  loc:TabNumber = -1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

RestoreMem       Routine
  !FormSource=File

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FormJobsInBatch_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferFormJobsInBatch')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormJobsInBatch_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormJobsInBatch_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormJobsInBatch_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  packet = clip(packet) & '<input type="hidden" name="MULDESPJ__FileAction" value="'&p_web.getSessionValue('MULDESPJ:FileAction')&'" ></input><13,10>'
  packet = clip(packet) & '<input type="hidden" name="file" value="MULDESPJ" ></input><13,10>'
  packet = clip(packet) & '<input type="hidden" name="UpdateFile" value="MULDESPJ" ></input><13,10>'
  packet = clip(packet) & '<input type="hidden" name="UpdateKey" value="mulj:RecordNumberKey" ></input><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FormJobsInBatch" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FormJobsInBatch" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FormJobsInBatch" ></input><13,10>'
  end

  do SendPacket
  packet = clip(packet) & p_web.CreateInput('hidden','mulj:RecordNumber',p_web._jsok(p_web.getSessionValue('mulj:RecordNumber'))) & '<13,10>'
  If p_web.Translate('Update MULDESPJ') <> ''
    packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Update MULDESPJ',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FormJobsInBatch">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FormJobsInBatch" class="'&clip('MyTab')&'">'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FormJobsInBatch')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
        Loc:Tabnumber = p_web.getSessionValue('showtab_FormJobsInBatch')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FormJobsInBatch'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
          Loc:Tabnumber = p_web.getSessionValue('showtab_FormJobsInBatch')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket


CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FormJobsInBatch_form:ready_',1)
  p_web.SetSessionValue('FormJobsInBatch_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FormJobsInBatch',0)

PreCopy  Routine
  p_web.SetValue('FormJobsInBatch_form:ready_',1)
  p_web.SetSessionValue('FormJobsInBatch_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormJobsInBatch',0)
  p_web._PreCopyRecord(MULDESPJ,mulj:RecordNumberKey)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('FormJobsInBatch_form:ready_',1)
  p_web.SetSessionValue('FormJobsInBatch_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FormJobsInBatch:Primed',0)

PreDelete       Routine
  p_web.SetValue('FormJobsInBatch_form:ready_',1)
  p_web.SetSessionValue('FormJobsInBatch_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FormJobsInBatch:Primed',0)
  p_web.setsessionvalue('showtab_FormJobsInBatch',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormJobsInBatch_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormJobsInBatch_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostInsert      Routine

PostCopy        Routine
  p_web.SetSessionValue('FormJobsInBatch:Primed',0)

PostUpdate      Routine
  p_web.SetSessionValue('FormJobsInBatch:Primed',0)

PostDelete      Routine
  CountBatch# = 0
  Access:MULDESPJ.ClearKey(mulj:JobNumberKey)
  mulj:RefNumber = p_web.GSV('muld:RecordNumber')
  Set(mulj:JobNumberKey,mulj:JobNumberKey)
  Loop
      If Access:MULDESPJ.NEXT()
          Break
      End !If
      If mulj:RefNumber <> p_web.GSV('muld:RecordNumber')      |
          Then Break.  ! End If
      CountBatch# += 1
  End !Loop
  
  Access:MULDESP.ClearKey(muld:RecordNumberKey)
  muld:RecordNumber = p_web.GSV('muld:RecordNumber')
  IF (Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign)
      
      muld:BatchTotal = CountBatch#
      Access:MULDESP.TryUpdate()
  END
  
BrowseJobsInBatch    PROCEDURE  (NetWebServerWorker p_web)
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(MULDESPJ)
                      Project(mulj:RecordNumber)
                      Project(mulj:JobNumber)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
MULDESP::State  USHORT
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return
  End
  GlobalErrors.SetProcedureName('BrowseJobsInBatch')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseJobsInBatch:NoForm')
      loc:NoForm = p_web.GetValue('BrowseJobsInBatch:NoForm')
      loc:FormName = p_web.GetValue('BrowseJobsInBatch:FormName')
    else
      loc:FormName = 'BrowseJobsInBatch_frm'
    End
    p_web.SSV('BrowseJobsInBatch:NoForm',loc:NoForm)
    p_web.SSV('BrowseJobsInBatch:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseJobsInBatch:NoForm')
    loc:FormName = p_web.GSV('BrowseJobsInBatch:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseJobsInBatch') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseJobsInBatch')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('fdiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(MULDESPJ,mulj:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'MULJ:JOBNUMBER') then p_web.SetValue('BrowseJobsInBatch_sort','1')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseJobsInBatch:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseJobsInBatch:LookupFrom','LookupFrom')
    p_web.StoreValue('BrowseJobsInBatch:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseJobsInBatch:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseJobsInBatch:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:FileLoad
  loc:FillBack         = 0
  loc:Sorting          = Net:ServerSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:Below
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  IF (p_web.IfExistsValue('locPassedBatchNumber'))
      p_web.StoreValue('locPassedBatchNumber')
  END
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 10
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseJobsInBatch_sort',net:DontEvaluate)
  p_web.SetSessionValue('BrowseJobsInBatch_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'mulj:JobNumber','-mulj:JobNumber')
    Loc:LocateField = 'mulj:JobNumber'
  of 2
    Loc:LocateField = ''
  end
  if loc:vorder = ''
    loc:vorder = '+mulj:RefNumber,+mulj:JobNumber'
  end
  If False ! add range fields to sort order
  Else
    If Instring('MULJ:REFNUMBER',upper(loc:vOrder),1,1) = 0
      loc:vOrder = 'mulj:RefNumber,' & loc:vorder
    End
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('mulj:JobNumber')
    loc:SortHeader = p_web.Translate('Job Number')
    p_web.SetSessionValue('BrowseJobsInBatch_LocatorPic','@s8')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GetSessionValue('BrowseJobsInBatch:LookupFrom')
  End!Else
  loc:CloseAction = 'MultipleBatchDespatch'
    loc:formaction = 'FormJobsInBatch'
    loc:formactiontarget = '_self'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseJobsInBatch:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseJobsInBatch:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseJobsInBatch:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="MULDESPJ"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="mulj:RecordNumberKey"></input><13,10>'
  end
  If p_web.Translate('Job In Batch Number: ' & p_web.GSV('muld:RecordNumber')) <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Job In Batch Number: ' & p_web.GSV('muld:RecordNumber'),0)&'</span>'&CRLF
  End
  If clip('Job In Batch Number: ' & p_web.GSV('muld:RecordNumber')) <> ''
    packet = clip(packet) & p_web.br
  End
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseJobsInBatch',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2BrowseJobsInBatch',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseJobsInBatch.locate(''Locator2BrowseJobsInBatch'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseJobsInBatch.cl(''BrowseJobsInBatch'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('BrowseLookup')&'"><table class="sortable" id="BrowseJobsInBatch_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('BrowseLookup')&'"><table class="'&clip('BrowseTable')&'" id="BrowseJobsInBatch_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'1','BrowseJobsInBatch','Job Number','Click here to sort by Job Number',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Job Number')&'">'&p_web.Translate('Job Number')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
    If loc:Selecting = 0
        packet = clip(packet) & '<th>'&NBSP&'</th>'&CRLF ! no heading for this column  Delete
        do AddPacket
        loc:columns += 1
    End ! Selecting
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,10,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('mulj:recordnumber',lower(Thisview{prop:order}),1,1) = 0 !and MULDESPJ{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'mulj:RecordNumber'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('mulj:RecordNumber'),p_web.GetValue('mulj:RecordNumber'),p_web.GetSessionValue('mulj:RecordNumber'))
    muld:RecordNumber = p_web.RestoreValue('muld:RecordNumber')
    loc:FilterWas = 'mulj:RefNumber = ' & muld:RecordNumber
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseJobsInBatch',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseJobsInBatch_Filter')
    p_web.SetSessionValue('BrowseJobsInBatch_FirstValue','')
    p_web.SetSessionValue('BrowseJobsInBatch_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,MULDESPJ,mulj:RecordNumberKey,loc:PageRows,'BrowseJobsInBatch',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If MULDESPJ{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(MULDESPJ,loc:firstvalue)
              Reset(ThisView,MULDESPJ)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If MULDESPJ{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(MULDESPJ,loc:lastvalue)
            Reset(ThisView,MULDESPJ)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(mulj:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseJobsInBatch.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseJobsInBatch.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseJobsInBatch.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseJobsInBatch.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
    If loc:found
          TableQueue.Kind = Net:BeforeTable
          do AddPacket
    End
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseJobsInBatch',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseJobsInBatch_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseJobsInBatch_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1BrowseJobsInBatch',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseJobsInBatch.locate(''Locator1BrowseJobsInBatch'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseJobsInBatch.cl(''BrowseJobsInBatch'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('BrowseJobsInBatch_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseJobsInBatch_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseJobsInBatch.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseJobsInBatch.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseJobsInBatch.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseJobsInBatch.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  If loc:found
        do SendPacket
  End
  End
  If loc:selecting = 0 and loc:parent = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:Formname,loc:CloseAction)
      do SendPacket
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
    loc:field = mulj:RecordNumber
    p_web._thisrow = p_web._nocolon('mulj:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseJobsInBatch:LookupField')) = mulj:RecordNumber and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((mulj:RecordNumber = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="BrowseJobsInBatch.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If MULDESPJ{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(MULDESPJ)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If MULDESPJ{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(MULDESPJ)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','mulj:RecordNumber',clip(loc:field),,loc:checked,,,'onclick="BrowseJobsInBatch.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','mulj:RecordNumber',clip(loc:field),,'checked',,,'onclick="BrowseJobsInBatch.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::mulj:JobNumber
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        If Loc:Selecting = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::Delete
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="BrowseJobsInBatch.omv(this);" onMouseOut="BrowseJobsInBatch.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var BrowseJobsInBatch=new browseTable(''BrowseJobsInBatch'','''&clip(loc:formname)&''','''&p_web._jsok('mulj:RecordNumber',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('mulj:RecordNumber')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''',''FormJobsInBatch'');<13,10>'&|
      'BrowseJobsInBatch.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'BrowseJobsInBatch.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseJobsInBatch')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseJobsInBatch')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseJobsInBatch')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseJobsInBatch')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(MULDESPJ)
  p_web._CloseFile(MULDESP)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(MULDESPJ)
  Bind(mulj:Record)
  Clear(mulj:Record)
  NetWebSetSessionPics(p_web,MULDESPJ)
  p_web._OpenFile(MULDESP)
  Bind(muld:Record)
  NetWebSetSessionPics(p_web,MULDESP)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('mulj:RecordNumber',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
! ----------------------------------------------------------------------------------------
value::mulj:JobNumber   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('mulj:JobNumber_'&mulj:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(mulj:JobNumber,'@s8')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::Delete   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('Delete_'&mulj:RecordNumber,,net:crc)
          If loc:viewonly = 0
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallDeleteButton,'BrowseJobsInBatch',loc:field) & '<13,10>'
          End
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  p_web._OpenFile(MULDESP)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(MULDESP)
     FilesOpened = False
  END
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
PushDefaultSelection  Routine
  loc:default = mulj:RecordNumber

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('mulj:RecordNumber',mulj:RecordNumber)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('mulj:RecordNumber',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('mulj:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('mulj:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
AccessoryCheck       PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
FilesOpened     BYTE(0)
  CODE
    !Validate Accessories
    ! Variables Passed:
    ! AccessoryCheck:Type
    ! AccessoryCheck:RefNumber
    
    ! Returned Value
! AccessoryCheck:Return

    do OpenFiles
    Case p_web.GSV('AccessoryCheck:Type')
    of 'LOA'
        error# = 0
        Access:LOANACC.Clearkey(lac:Ref_Number_Key)
        lac:Ref_Number    = p_web.GSV('AccessoryCheck:RefNumber')
        set(lac:Ref_Number_Key,lac:Ref_Number_Key)
        loop
            if (Access:LOANACC.Next())
                Break
            end ! if (Access:LOANACC.Next())
            if (lac:Ref_Number    <> p_web.GSV('AccessoryCheck:RefNumber'))
                Break
            end ! if (lac:Ref_Number    <> p_web.GSV('tmp:LoanUnitNumber'))
    
    
            Access:TAGFILE.Clearkey(tag:keyTagged)
            tag:sessionID    = p_web.sessionID
            tag:taggedValue  = lac:accessory
            if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
                ! Found
                if (tag:tagged <> 1)
                    error# = 1
                    break
                end ! if (tag:tagged <> 1)
            else ! if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
                ! Error
                error# = 1
                break
            end ! if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
        end ! loop
    
        if (error# = 0)
            Access:TAGFILE.Clearkey(tag:keyTagged)
            tag:sessionID    = p_web.sessionID
            set(tag:keyTagged,tag:keyTagged)
            loop
                if (Access:TAGFILE.Next())
                    Break
                end ! if (Access:TAGFILE.Next())
                if (tag:sessionID    <> p_web.sessionID)
                    Break
                end ! if (tag:sessionID    <> p_web.sessionID)
                if (tag:Tagged = 0)
                    cycle
                end ! if (tag:Tagged = 0)
                Access:LOANACC.Clearkey(lac:ref_number_Key)
                lac:ref_number    = p_web.GSV('AccessoryCheck:RefNumber')
                lac:accessory    = tag:taggedValue
                if (Access:LOANACC.TryFetch(lac:ref_number_Key) = Level:Benign)
                    ! Found
                else ! if (Access:LOANACC.TryFetch(lac:ref_number_Key) = Level:Benign)
                    ! Error
                    error# = 2
                    break
                end ! if (Access:LOANACC.TryFetch(lac:ref_number_Key) = Level:Benign)
            end ! loop
        end ! if (error# = 0)
    Else
        error# = 0
        Access:JOBACC.Clearkey(jac:Ref_Number_Key)
        jac:Ref_Number    = p_web.GSV('AccessoryCheck:RefNumber')
        set(jac:Ref_Number_Key,jac:Ref_Number_Key)
        loop
            if (Access:JOBACC.Next())
                Break
            end ! if (Access:LOANACC.Next())
            if (jac:Ref_Number    <> p_web.GSV('AccessoryCheck:RefNumber'))
                Break
            end ! if (lac:Ref_Number    <> p_web.GSV('tmp:LoanUnitNumber'))
    
            if (p_web.GSV('BookingSite') <> 'RRC')
                if (NOT jac:Attached)
                    Cycle
                End
            End
    
            Access:TAGFILE.Clearkey(tag:keyTagged)
            tag:sessionID    = p_web.sessionID
            tag:taggedValue  = jac:accessory
            if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
                ! Found
                if (tag:tagged <> 1)
                    error# = 1
                    break
                else
                    if (p_web.GSV('BookingSite') <> 'RRC' AND NOT jac:Attached)
                        error# = 1
                        break
                    End
                end ! if (tag:tagged <> 1)
            else ! if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
                ! Error
                error# = 1
                break
            end ! if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
        end ! loop
    
        if (error# = 0)
            Access:TAGFILE.Clearkey(tag:keyTagged)
            tag:sessionID    = p_web.sessionID
            set(tag:keyTagged,tag:keyTagged)
            loop
                if (Access:TAGFILE.Next())
                    Break
                end ! if (Access:TAGFILE.Next())
                if (tag:sessionID    <> p_web.sessionID)
                    Break
                end ! if (tag:sessionID    <> p_web.sessionID)
                if (tag:Tagged = 0)
                    cycle
                end ! if (tag:Tagged = 0)
                Access:JOBACC.Clearkey(jac:ref_number_Key)
                jac:ref_number    = p_web.GSV('AccessoryCheck:RefNumber')
                jac:accessory    = tag:taggedValue
                if (Access:JOBACC.TryFetch(jac:ref_number_Key) = Level:Benign)
                    ! Found
                else ! if (Access:LOANACC.TryFetch(lac:ref_number_Key) = Level:Benign)
                    ! Error
                    error# = 2
                    break
                end ! if (Access:LOANACC.TryFetch(lac:ref_number_Key) = Level:Benign)
            end ! loop
        end ! if (error# = 0)
    End
    
    do CloseFiles

    p_web.SSV('AccessoryCheck:Return',error#)
    ! 1 = Missing
    ! 2 = Mismatch
    ! Else = All Fine
!--------------------------------------
OpenFiles  ROUTINE
  Access:TagFile.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TagFile.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBACC.Open                                       ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBACC.UseFile                                    ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:LOANACC.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:LOANACC.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:TagFile.Close
     Access:JOBACC.Close
     Access:LOANACC.Close
     FilesOpened = False
  END
TagValidateLoanAccessories PROCEDURE  (NetWebServerWorker p_web)
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(ACCESSOR)
                      Project(acr:Accessory)
                      Project(acr:Accessory)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
TagFile::State  USHORT
  CODE
  GlobalErrors.SetProcedureName('TagValidateLoanAccessories')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('TagValidateLoanAccessories:NoForm')
      loc:NoForm = p_web.GetValue('TagValidateLoanAccessories:NoForm')
      loc:FormName = p_web.GetValue('TagValidateLoanAccessories:FormName')
    else
      loc:FormName = 'TagValidateLoanAccessories_frm'
    End
    p_web.SSV('TagValidateLoanAccessories:NoForm',loc:NoForm)
    p_web.SSV('TagValidateLoanAccessories:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('TagValidateLoanAccessories:NoForm')
    loc:FormName = p_web.GSV('TagValidateLoanAccessories:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('TagValidateLoanAccessories') & '_' & lower(loc:parent)
  else
    loc:divname = lower('TagValidateLoanAccessories')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('fdiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(ACCESSOR,acr:AccessOnlyKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'TAG:TAGGED') then p_web.SetValue('TagValidateLoanAccessories_sort','2')
    ElsIf (loc:vorder = 'ACR:ACCESSORY') then p_web.SetValue('TagValidateLoanAccessories_sort','1')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('TagValidateLoanAccessories:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('TagValidateLoanAccessories:LookupFrom','LookupFrom')
    p_web.StoreValue('TagValidateLoanAccessories:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('TagValidateLoanAccessories:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('TagValidateLoanAccessories:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:FileLoad
  loc:FillBack         = 0
  loc:Sorting          = Net:ServerSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:Below
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 10
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('TagValidateLoanAccessories_sort',net:DontEvaluate)
  p_web.SetSessionValue('TagValidateLoanAccessories_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'tag:tagged','-tag:tagged')
    Loc:LocateField = 'tag:tagged'
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(acr:Accessory)','-UPPER(acr:Accessory)')
    Loc:LocateField = 'acr:Accessory'
  end
  if loc:vorder = ''
    loc:vorder = '+UPPER(acr:Model_Number),+UPPER(acr:Accessory)'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('tag:tagged')
    loc:SortHeader = p_web.Translate('Tag')
    p_web.SetSessionValue('TagValidateLoanAccessories_LocatorPic','@n3')
  Of upper('acr:Accessory')
    loc:SortHeader = p_web.Translate('Accessory')
    p_web.SetSessionValue('TagValidateLoanAccessories_LocatorPic','@s30')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GetSessionValue('TagValidateLoanAccessories:LookupFrom')
  End!Else
  loc:formaction = 'TagValidateLoanAccessories'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="TagValidateLoanAccessories:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="TagValidateLoanAccessories:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('TagValidateLoanAccessories:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="ACCESSOR"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="acr:AccessOnlyKey"></input><13,10>'
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'TagValidateLoanAccessories',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2TagValidateLoanAccessories',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="TagValidateLoanAccessories.locate(''Locator2TagValidateLoanAccessories'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'TagValidateLoanAccessories.cl(''TagValidateLoanAccessories'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('BrowseLookup')&'"><table class="sortable" id="TagValidateLoanAccessories_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('BrowseLookup')&'"><table class="'&clip('BrowseTable')&'" id="TagValidateLoanAccessories_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'2','TagValidateLoanAccessories','Tag','Click here to sort by tagged',,,50,1)
        Else
          packet = clip(packet) & '<th width="'&clip(50)&'" Title="'&p_web.Translate('Click here to sort by tagged')&'">'&p_web.Translate('Tag')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'1','TagValidateLoanAccessories','Accessory','Click here to sort by Accessory',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Accessory')&'">'&p_web.Translate('Accessory')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,10,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('acr:accessory',lower(Thisview{prop:order}),1,1) = 0 !and ACCESSOR{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'acr:Accessory'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('acr:Accessory'),p_web.GetValue('acr:Accessory'),p_web.GetSessionValue('acr:Accessory'))
      loc:FilterWas = 'Upper(acr:Model_Number) = Upper(''' & p_web.GSV('tmp:LoanModelNumber') & ''')'
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'TagValidateLoanAccessories',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('TagValidateLoanAccessories_Filter')
    p_web.SetSessionValue('TagValidateLoanAccessories_FirstValue','')
    p_web.SetSessionValue('TagValidateLoanAccessories_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,ACCESSOR,acr:AccessOnlyKey,loc:PageRows,'TagValidateLoanAccessories',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If ACCESSOR{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(ACCESSOR,loc:firstvalue)
              Reset(ThisView,ACCESSOR)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If ACCESSOR{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(ACCESSOR,loc:lastvalue)
            Reset(ThisView,ACCESSOR)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(acr:Accessory)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'TagValidateLoanAccessories.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'TagValidateLoanAccessories.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'TagValidateLoanAccessories.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'TagValidateLoanAccessories.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'TagValidateLoanAccessories',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('TagValidateLoanAccessories_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('TagValidateLoanAccessories_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1TagValidateLoanAccessories',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="TagValidateLoanAccessories.locate(''Locator1TagValidateLoanAccessories'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'TagValidateLoanAccessories.cl(''TagValidateLoanAccessories'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('TagValidateLoanAccessories_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('TagValidateLoanAccessories_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'TagValidateLoanAccessories.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'TagValidateLoanAccessories.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'TagValidateLoanAccessories.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'TagValidateLoanAccessories.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
    Access:TAGFILE.Clearkey(tag:keyTagged)
    tag:sessionID    = p_web.SessionID
    tag:taggedValue    = acr:Accessory
    if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
        ! Found
    else ! if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
        ! Error
        tag:Tagged = 0
    end ! if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
    loc:field = acr:Accessory
    p_web._thisrow = p_web._nocolon('acr:Accessory')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('TagValidateLoanAccessories:LookupField')) = acr:Accessory and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((acr:Accessory = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="TagValidateLoanAccessories.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If ACCESSOR{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(ACCESSOR)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If ACCESSOR{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(ACCESSOR)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','acr:Accessory',clip(loc:field),,loc:checked,,,'onclick="TagValidateLoanAccessories.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','acr:Accessory',clip(loc:field),,'checked',,,'onclick="TagValidateLoanAccessories.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td width="'&clip(50)&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::tag:tagged
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::acr:Accessory
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="TagValidateLoanAccessories.omv(this);" onMouseOut="TagValidateLoanAccessories.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var TagValidateLoanAccessories=new browseTable(''TagValidateLoanAccessories'','''&clip(loc:formname)&''','''&p_web._jsok('acr:Accessory',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('acr:Accessory')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''','''');<13,10>'&|
      'TagValidateLoanAccessories.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'TagValidateLoanAccessories.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2TagValidateLoanAccessories')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1TagValidateLoanAccessories')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1TagValidateLoanAccessories')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2TagValidateLoanAccessories')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(ACCESSOR)
  p_web._CloseFile(TagFile)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(ACCESSOR)
  Bind(acr:Record)
  Clear(acr:Record)
  NetWebSetSessionPics(p_web,ACCESSOR)
  p_web._OpenFile(TagFile)
  Bind(tag:Record)
  NetWebSetSessionPics(p_web,TagFile)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('acr:Accessory',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(ACCESSOR)
  Case upper(p_web.GetValue('_EIPClm'))
  of upper('tag:tagged')
    do Validate::tag:tagged
  End
  p_web._CloseFile(ACCESSOR)
! ----------------------------------------------------------------------------------------
Validate::tag:tagged  Routine
  data
loc:was                    Any
loc:result                 Long
  code
  do OpenFilesB
  loc:ViewState = p_web.GetValue('ViewState')
  acr:Accessory = p_web.Base64Decode(p_web._Unescape(loc:ViewState,Net:NoPlus))
  loc:result = p_web._GetFile(ACCESSOR,acr:AccessOnlyKey)
  p_web.FileToSessionQueue(ACCESSOR)
  loc:was = tag:tagged
  tag:tagged = Choose(p_web.GetValue('value') = 1,1,0)
  Access:TAGFILE.Clearkey(tag:keyTagged)
  tag:sessionID    = p_web.SessionID
  tag:taggedValue    = acr:Accessory
  if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
      ! Found
      tag:tagged = Choose(p_web.getValue('value') = 1,true,false)
      access:TAGFILE.tryUpdate()
  else ! if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
      ! Error
      if (Access:TAGFILE.PrimeRecord() = Level:Benign)
          tag:sessionID    = p_web.sessionID
          tag:taggedValue    = acr:Accessory
          tag:tagged = Choose(p_web.getValue('value') = 1,true,false)
          if (Access:TAGFILE.TryInsert() = Level:Benign)
              ! Inserted
          else ! if (Access:TAGFILE.TryInsert() = Level:Benign)
              ! Error
          end ! if (Access:TAGFILE.TryInsert() = Level:Benign)
      end ! if (Access:TAGFILE.PrimeRecord() = Level:Benign)
  end ! if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
  ! Validation goes here, if fails set loc:invalid & loc:alert
  do CheckForDuplicate
  If loc:invalid = '' ! save record
      p_web._updatefile(TagFile)
      p_web.FileToSessionQueue(TagFile)
  End
  do SendAlert
  if loc:alert <> ''
    tag:tagged = loc:was
    do Value::tag:tagged
  End
  ! updating other browse cells goes here.
  do ClosefilesB
! ----------------------------------------------------------------------------------------
value::tag:tagged   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('tag:tagged_'&acr:Accessory,,net:crc)
      If p_web.GSV('Hide:ValidateAccessoriesButton') <> 1
          loc:extra = Choose(tag:tagged = 1,'checked','')
          packet = clip(packet) & p_web.CreateInput('checkbox','inp'&p_web.crc32('tag:tagged'&acr:Accessory),clip(1),,loc:extra & ' ' & loc:disabled,,,'onclick="TagValidateLoanAccessories.eip(this,'''&p_web._jsok('tag:tagged')&''','''&p_web._jsok(loc:viewstate)&''');"',,) & '<13,10>'
      Else
        loc:disabled = 'disabled'
          loc:extra = Choose(tag:tagged = 1,'checked','')
          packet = clip(packet) & p_web.CreateInput('checkbox','inp'&p_web.crc32('tag:tagged'&acr:Accessory),clip(1),,loc:extra & ' ' & loc:disabled,,,'onclick="TagValidateLoanAccessories.eip(this,'''&p_web._jsok('tag:tagged')&''','''&p_web._jsok(loc:viewstate)&''');"',,) & '<13,10>'
      End
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::acr:Accessory   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('acr:Accessory_'&acr:Accessory,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(acr:Accessory,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  p_web._OpenFile(TagFile)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(TagFile)
     FilesOpened = False
  END
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
  If loc:invalid <> '' then exit. ! no need to check, record is already invalid
  If Duplicate(acr:Accesory_Key)
    loc:Invalid = 'acr:Model_Number'
    loc:Alert = clip(p_web.site.DuplicateText) & ' Accesory_Key --> acr:Model_Number, '&clip('Accessory')&''
  End
  If Duplicate(acr:Model_Number_Key)
    loc:Invalid = 'acr:Accessory'
    loc:Alert = clip(p_web.site.DuplicateText) & ' Model_Number_Key --> '&clip('Accessory')&', acr:Model_Number'
  End
PushDefaultSelection  Routine
  loc:default = acr:Accessory

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('acr:Accessory',acr:Accessory)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('acr:Accessory',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('acr:Accessory'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('acr:Accessory'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
FormBatchesInProgress PROCEDURE !Procedure not yet defined
  CODE
  GlobalErrors.ThrowMessage(Msg:ProcedureToDo,'FormBatchesInProgress') ! This procedure acts as a place holder for a procedure yet to be defined
  SETKEYCODE(0)
  GlobalResponse = RequestCancelled                        ! Request cancelled is the implied action
BrowseBatchesInProgress PROCEDURE  (NetWebServerWorker p_web)
locCompanyName       STRING(30)                            !
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(MULDESP)
                      Project(muld:RecordNumber)
                      Project(muld:AccountNumber)
                      Project(muld:Courier)
                      Project(muld:BatchNumber)
                      Project(muld:BatchTotal)
                      Project(muld:BatchType)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
SUBTRACC::State  USHORT
TRADEACC::State  USHORT
  CODE
  GlobalErrors.SetProcedureName('BrowseBatchesInProgress')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseBatchesInProgress:NoForm')
      loc:NoForm = p_web.GetValue('BrowseBatchesInProgress:NoForm')
      loc:FormName = p_web.GetValue('BrowseBatchesInProgress:FormName')
    else
      loc:FormName = 'BrowseBatchesInProgress_frm'
    End
    p_web.SSV('BrowseBatchesInProgress:NoForm',loc:NoForm)
    p_web.SSV('BrowseBatchesInProgress:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseBatchesInProgress:NoForm')
    loc:FormName = p_web.GSV('BrowseBatchesInProgress:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseBatchesInProgress') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseBatchesInProgress')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('fdiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(MULDESP,muld:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'MULD:ACCOUNTNUMBER') then p_web.SetValue('BrowseBatchesInProgress_sort','1')
    ElsIf (loc:vorder = 'LOCCOMPANYNAME') then p_web.SetValue('BrowseBatchesInProgress_sort','6')
    ElsIf (loc:vorder = 'MULD:COURIER') then p_web.SetValue('BrowseBatchesInProgress_sort','2')
    ElsIf (loc:vorder = 'MULD:BATCHNUMBER') then p_web.SetValue('BrowseBatchesInProgress_sort','3')
    ElsIf (loc:vorder = 'MULD:BATCHTOTAL') then p_web.SetValue('BrowseBatchesInProgress_sort','4')
    ElsIf (loc:vorder = 'MULD:BATCHTYPE') then p_web.SetValue('BrowseBatchesInProgress_sort','5')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseBatchesInProgress:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseBatchesInProgress:LookupFrom','LookupFrom')
    p_web.StoreValue('BrowseBatchesInProgress:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseBatchesInProgress:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseBatchesInProgress:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:FileLoad
  loc:FillBack         = 0
  loc:Sorting          = Net:ServerSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:Below
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  p_web.site.SmallDeleteButton.TextValue = p_web.Translate('Delete Batch')
  p_web.site.SmallDeleteButton.Class = 'button-inbrowse DarkRedBold'
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 10
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseBatchesInProgress_sort',net:DontEvaluate)
  p_web.SetSessionValue('BrowseBatchesInProgress_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(muld:AccountNumber)','-UPPER(muld:AccountNumber)')
    Loc:LocateField = 'muld:AccountNumber'
  of 6
    loc:vorder = Choose(Loc:SortDirection=1,'locCompanyName','-locCompanyName')
    Loc:LocateField = 'locCompanyName'
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(muld:Courier)','-UPPER(muld:Courier)')
    Loc:LocateField = 'muld:Courier'
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(muld:BatchNumber)','-UPPER(muld:BatchNumber)')
    Loc:LocateField = 'muld:BatchNumber'
  of 4
    loc:vorder = Choose(Loc:SortDirection=1,'muld:BatchTotal','-muld:BatchTotal')
    Loc:LocateField = 'muld:BatchTotal'
  of 5
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(muld:BatchType)','-UPPER(muld:BatchType)')
    Loc:LocateField = 'muld:BatchType'
  of 8
    Loc:LocateField = ''
  of 9
    Loc:LocateField = ''
  of 10
    Loc:LocateField = ''
  end
  if loc:vorder = ''
    loc:vorder = '+UPPER(muld:HeadAccountNumber),+UPPER(muld:AccountNumber)'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('muld:AccountNumber')
    loc:SortHeader = p_web.Translate('Account Number')
    p_web.SetSessionValue('BrowseBatchesInProgress_LocatorPic','@s30')
  Of upper('locCompanyName')
    loc:SortHeader = p_web.Translate('Company Name')
    p_web.SetSessionValue('BrowseBatchesInProgress_LocatorPic','@s30')
  Of upper('muld:Courier')
    loc:SortHeader = p_web.Translate('Courier')
    p_web.SetSessionValue('BrowseBatchesInProgress_LocatorPic','@s30')
  Of upper('muld:BatchNumber')
    loc:SortHeader = p_web.Translate('Batch Number')
    p_web.SetSessionValue('BrowseBatchesInProgress_LocatorPic','@s30')
  Of upper('muld:BatchTotal')
    loc:SortHeader = p_web.Translate('Total In Batch')
    p_web.SetSessionValue('BrowseBatchesInProgress_LocatorPic','@s8')
  Of upper('muld:BatchType')
    loc:SortHeader = p_web.Translate('Batch Type')
    p_web.SetSessionValue('BrowseBatchesInProgress_LocatorPic','@s3')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GetSessionValue('BrowseBatchesInProgress:LookupFrom')
  End!Else
    loc:formaction = 'FormBatchesInProgress'
    loc:formactiontarget = '_self'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseBatchesInProgress:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseBatchesInProgress:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseBatchesInProgress:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="MULDESP"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="muld:RecordNumberKey"></input><13,10>'
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseBatchesInProgress',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2BrowseBatchesInProgress',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseBatchesInProgress.locate(''Locator2BrowseBatchesInProgress'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseBatchesInProgress.cl(''BrowseBatchesInProgress'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('BrowseLookup')&'"><table class="sortable" id="BrowseBatchesInProgress_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('BrowseLookup')&'"><table class="'&clip('BrowseTable')&'" id="BrowseBatchesInProgress_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'1','BrowseBatchesInProgress','Account Number','Click here to sort by Account Number',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Account Number')&'">'&p_web.Translate('Account Number')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'6','BrowseBatchesInProgress','Company Name',,,,,1)
        Else
          packet = clip(packet) & '<th>'&p_web.Translate('Company Name')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'2','BrowseBatchesInProgress','Courier','Click here to sort by Courier',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Courier')&'">'&p_web.Translate('Courier')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'3','BrowseBatchesInProgress','Batch Number','Click here to sort by Batch Number',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Batch Number')&'">'&p_web.Translate('Batch Number')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'4','BrowseBatchesInProgress','Total In Batch','Click here to sort by Total In Batch',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Total In Batch')&'">'&p_web.Translate('Total In Batch')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
  If (false) AND  true
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'5','BrowseBatchesInProgress','Batch Type','Click here to sort by Batch Type',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Batch Type')&'">'&p_web.Translate('Batch Type')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
  End ! Field condition
        packet = clip(packet) & '<th>'&NBSP&'</th>'&CRLF ! no heading for this column  ViewJobsInBatch
        do AddPacket
        loc:columns += 1
    If loc:Selecting = 0
        packet = clip(packet) & '<th class="CenterJustify">'&NBSP&'</th>'&CRLF ! no heading for this column  Delete
        do AddPacket
        loc:columns += 1
    End ! Selecting
        packet = clip(packet) & '<th class="CenterJustify">'&NBSP&'</th>'&CRLF ! no heading for this column  FinishBatch
        do AddPacket
        loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,10,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('muld:recordnumber',lower(Thisview{prop:order}),1,1) = 0 !and MULDESP{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'muld:RecordNumber'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('muld:RecordNumber'),p_web.GetValue('muld:RecordNumber'),p_web.GetSessionValue('muld:RecordNumber'))
      loc:FilterWas = 'Upper(muld:HeadAccountNumber) = Upper(<39>' & p_web.GSV('BookingAccount') & '<39>)'
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseBatchesInProgress',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseBatchesInProgress_Filter')
    p_web.SetSessionValue('BrowseBatchesInProgress_FirstValue','')
    p_web.SetSessionValue('BrowseBatchesInProgress_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,MULDESP,muld:RecordNumberKey,loc:PageRows,'BrowseBatchesInProgress',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If MULDESP{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(MULDESP,loc:firstvalue)
              Reset(ThisView,MULDESP)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If MULDESP{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(MULDESP,loc:lastvalue)
            Reset(ThisView,MULDESP)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      locCompanyName = ''
      IF (muld:BatchType = 'TRA')
          Access:TRADEACC.ClearKey(tra:Account_Number_Key)
          tra:Account_Number = muld:AccountNumber
          IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
              locCompanyName = tra:Company_Name
          END
      ELSE
          Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
          sub:Account_Number = muld:AccountNumber
          IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
              locCompanyName = sub:Company_Name
          END
      END
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(muld:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Batches To Despatch')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseBatchesInProgress.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseBatchesInProgress.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseBatchesInProgress.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseBatchesInProgress.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
    If loc:found
          TableQueue.Kind = Net:BeforeTable
          do AddPacket
    End
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseBatchesInProgress',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseBatchesInProgress_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseBatchesInProgress_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1BrowseBatchesInProgress',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseBatchesInProgress.locate(''Locator1BrowseBatchesInProgress'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseBatchesInProgress.cl(''BrowseBatchesInProgress'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('BrowseBatchesInProgress_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseBatchesInProgress_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseBatchesInProgress.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseBatchesInProgress.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseBatchesInProgress.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseBatchesInProgress.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  If loc:found
        do SendPacket
  End
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
    loc:field = muld:RecordNumber
    p_web._thisrow = p_web._nocolon('muld:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseBatchesInProgress:LookupField')) = muld:RecordNumber and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((muld:RecordNumber = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="BrowseBatchesInProgress.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If MULDESP{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(MULDESP)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If MULDESP{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(MULDESP)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','muld:RecordNumber',clip(loc:field),,loc:checked,,,'onclick="BrowseBatchesInProgress.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','muld:RecordNumber',clip(loc:field),,'checked',,,'onclick="BrowseBatchesInProgress.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::muld:AccountNumber
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::locCompanyName
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::muld:Courier
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::muld:BatchNumber
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::muld:BatchTotal
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      If (false) AND  true
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::muld:BatchType
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      End ! Field Condition
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::ViewJobsInBatch
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        If Loc:Selecting = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="CenterJustify">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::Delete
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="CenterJustify">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::FinishBatch
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="BrowseBatchesInProgress.omv(this);" onMouseOut="BrowseBatchesInProgress.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var BrowseBatchesInProgress=new browseTable(''BrowseBatchesInProgress'','''&clip(loc:formname)&''','''&p_web._jsok('muld:RecordNumber',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('muld:RecordNumber')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''',''FormBatchesInProgress'');<13,10>'&|
      'BrowseBatchesInProgress.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'BrowseBatchesInProgress.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseBatchesInProgress')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseBatchesInProgress')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseBatchesInProgress')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseBatchesInProgress')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(MULDESP)
  p_web._CloseFile(SUBTRACC)
  p_web._CloseFile(TRADEACC)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(MULDESP)
  Bind(muld:Record)
  Clear(muld:Record)
  NetWebSetSessionPics(p_web,MULDESP)
  p_web._OpenFile(SUBTRACC)
  Bind(sub:Record)
  NetWebSetSessionPics(p_web,SUBTRACC)
  p_web._OpenFile(TRADEACC)
  Bind(tra:Record)
  NetWebSetSessionPics(p_web,TRADEACC)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('muld:RecordNumber',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(MULDESP)
  Case upper(p_web.GetValue('_EIPClm'))
  of upper('ViewJobsInBatch')
    do Validate::ViewJobsInBatch
  of upper('FinishBatch')
    do Validate::FinishBatch
  End
  p_web._CloseFile(MULDESP)
! ----------------------------------------------------------------------------------------
value::muld:AccountNumber   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('muld:AccountNumber_'&muld:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(muld:AccountNumber,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::locCompanyName   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('locCompanyName_'&muld:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(locCompanyName,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::muld:Courier   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('muld:Courier_'&muld:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(muld:Courier,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::muld:BatchNumber   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('muld:BatchNumber_'&muld:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(muld:BatchNumber,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::muld:BatchTotal   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('muld:BatchTotal_'&muld:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(muld:BatchTotal,'@s8')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::muld:BatchType   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
  If (false)
    if false
    else
      packet = clip(packet) & p_web._DivHeader('muld:BatchType_'&muld:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(muld:BatchType,'@s3')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
  End
! ----------------------------------------------------------------------------------------
Validate::ViewJobsInBatch  Routine
  data
loc:was                    Any
loc:result                 Long
  code
  do OpenFilesB
  loc:ViewState = p_web.GetValue('ViewState')
  muld:RecordNumber = p_web.Base64Decode(p_web._Unescape(loc:ViewState,Net:NoPlus))
  loc:result = p_web._GetFile(MULDESP,muld:RecordNumberKey)
  p_web.FileToSessionQueue(MULDESP)
  do CheckForDuplicate
  do ClosefilesB
! ----------------------------------------------------------------------------------------
value::ViewJobsInBatch   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('ViewJobsInBatch_'&muld:RecordNumber,,net:crc)
      loc:disabled = ''
      packet = clip(packet) & p_web.CreateButton('button','ViewJobsInBatch','View Jobs In Batch','button-inbrowse',loc:formname,,,'window.open('''& p_web._MakeURL(clip('BrowseJobsInBatch&locPassedBatchNumber = ' & muld:RecordNumber)  & '&' & p_web._noColon('muld:RecordNumber')&'='& p_web.escape(muld:RecordNumber) & '&PressedButton=' ) & ''','''&clip('_self')&''')',,loc:disabled,,,,,) & '<13,10>' !2
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::Delete   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('Delete_'&muld:RecordNumber,'CenterJustify',net:crc)
          If loc:viewonly = 0
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallDeleteButton,'BrowseBatchesInProgress',loc:field) & '<13,10>'
          End
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
Validate::FinishBatch  Routine
  data
loc:was                    Any
loc:result                 Long
  code
  do OpenFilesB
  loc:ViewState = p_web.GetValue('ViewState')
  muld:RecordNumber = p_web.Base64Decode(p_web._Unescape(loc:ViewState,Net:NoPlus))
  loc:result = p_web._GetFile(MULDESP,muld:RecordNumberKey)
  p_web.FileToSessionQueue(MULDESP)
  do CheckForDuplicate
  do ClosefilesB
! ----------------------------------------------------------------------------------------
value::FinishBatch   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('FinishBatch_'&muld:RecordNumber,'CenterJustify',net:crc)
      loc:disabled = ''
      packet = clip(packet) & p_web.CreateButton('button','FinishBatch','Finish Batch','button-inbrowse GreenBold',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FinishBatch')  & '&' & p_web._noColon('muld:RecordNumber')&'='& p_web.escape(muld:RecordNumber) & '&PressedButton=1' ) & ''','''&clip('_self')&''')',,loc:disabled,,,,,) & '<13,10>' !2
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  p_web._OpenFile(SUBTRACC)
  p_web._OpenFile(TRADEACC)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(TRADEACC)
     FilesOpened = False
  END
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
PushDefaultSelection  Routine
  loc:default = muld:RecordNumber

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('muld:RecordNumber',muld:RecordNumber)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('muld:RecordNumber',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('muld:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('muld:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
WOBEDIStatus         PROCEDURE  (fStatus)                  ! Declare Procedure
  CODE
    Case fStatus
    Of 'NO'
        Return 'PENDING'
    Of 'YES'
        Return 'SUBMITTED'
    Of 'APP'
        Return 'APPROVED'
    Of 'PAY'
        Return 'PAID'
    Of 'EXC'
        Return 'REJECTED'
    Of 'EXM'
        Return 'REJECTED'
    Of 'AAJ'
        Return 'ACCEPTED REJECTION'
    Of 'REJ'
        Return 'FINAL REJECTION'
    End !func:Status
    Return ''
ViewJob              PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locEngineeringOption STRING(30)                            !
locCurrentEngineer   STRING(60)                            !
locChargeableParts   BYTE                                  !
locWarrantyParts     BYTE                                  !
locCompleteRepair    STRING(10000)                         !
locCChargeTypeReason STRING(255)                           !
locWChargeTypeReason STRING(255)                           !
locCRepairTypeReason STRING(255)                           !
locWRepairTypeReason STRING(255)                           !
tmp:FoundAccessory   STRING(1000)                          !
locHOClaimStatus     STRING(30)                            !
locRRCClaimStatus    STRING(30)                            !
locJobType           STRING(30)                            !
locMobileLifetimeValue STRING(30)                          !
locMobileAverageSpend STRING(20)                           !
locMobileLoyaltyStatus STRING(30)                          !
locMobileUpgradeDate DATE                                  !
locMobileIDNumber    STRING(30)                            !
! ================================================================
! Xml Export FILE structure
fileXmlExport   FILE,DRIVER('ASCII'),CREATE,BINDABLE,THREAD
Record              RECORD
recbuff                 STRING(256)
                    END
                END
! Xml Export Class Instance
objXmlExport XmlExport
! ================================================================
FilesOpened     Long
ESTPARTS::State  USHORT
PARTS::State  USHORT
WARPARTS::State  USHORT
UNITTYPE::State  USHORT
WEBJOB::State  USHORT
JOBS::State  USHORT
TRADEACC::State  USHORT
JOBSE2::State  USHORT
JOBSE::State  USHORT
ACCSTAT::State  USHORT
USERS::State  USHORT
JOBNOTES::State  USHORT
NETWORKS::State  USHORT
SUBTRACC::State  USHORT
JOBSWARR::State  USHORT
JOBACC::State  USHORT
WAYBAWT::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
                    MAP
showHideCreateInvoice   PROCEDURE(),BYTE
ShowAlert     Procedure(String fAlert)
                    END

  CODE
! ================================================================
! Initialise Xml Export Object
  objXmlExport.FInit(fileXmlExport)
! ================================================================
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('ViewJob')
  loc:formname = 'ViewJob_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('ViewJob','Change','wob:RecordNumber',clip(wob:RecordNumber))
    p_web._DivHeader('ViewJob',clip('fdiv') & ' ' & clip('FormContent'))
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferViewJob',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of InsertRecord + NET:WEB:StagePost
    do RestoreMem
    do PostInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferViewJob',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:StagePost
    do RestoreMem
    do PostCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferViewJob',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_ViewJob',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    If loc:act = InsertRecord
      do PostInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do PostUpdate
    End

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferViewJob',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of DeleteRecord + NET:WEB:StagePost
    do RestoreMem
    do PostDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_ViewJob',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
  return Ans
checkParts    routine
    p_web.SSV('CheckParts:Type','C')
    checkParts(p_web)
    p_web.SSV('GetStatus:StatusNumber',0)
    case p_web.GSV('CheckParts:ReturnValue')
        of 1 ! Pending Order
            p_web.SSV('GetStatus:StatusNumber',330)
        of 2 ! On Order
            p_web.SSV('GetStatus:StatusNumber',335)
        of 3 ! Back Order Status
            p_web.SSV('GetStatus:StatusNumber',340)
        of 4 ! Spares Received
            p_web.SSV('GetStatus:StatusNumber',345)
        else
            p_web.SSV('CheckParts:Type','W')
            checkParts(p_web)
            case p_web.GSV('CheckParts:ReturnValue')
                of 1 ! Pending Order
                    p_web.SSV('GetStatus:StatusNumber',330)
                of 2 ! On Order
                    p_web.SSV('GetStatus:StatusNumber',335)
                of 3 ! Back Order Status
                    p_web.SSV('GetStatus:StatusNumber',340)
                of 4 ! Spares Received
                    p_web.SSV('GetStatus:StatusNumber',345)
                else
                    if (sub(p_web.GSV('job:Current_Status'),1,3) = '330' Or |
                        sub(p_web.GSV('job:Current_Status'),1,3) = '340')

                        Access:AUDSTATS.Clearkey(aus:statusTimeKey)
                        aus:refNumber    = p_web.GSV('job:Ref_Number')
                        aus:type    = 'JOB'
                        aus:dateChanged    = today() + 1
                        set(aus:statusTimeKey,aus:statusTimeKey)
                        loop
                            if (Access:AUDSTATS.Previous())
                                Break
                            end ! if (Access:AUDSTATS.Next())
                            if (aus:refNumber    <> p_web.GSV('job:Ref_Number'))
                                Break
                            end ! if (aus:ref_Number    <> p_web.GSV('job:Ref_Number'))
                            if (aus:type    <> 'JOB')
                                Break
                            end ! if (aus:type    <> 'JOB')
                            if (aus:dateChanged    > today() + 1)
                                Break
                            end ! if (aus:dateChanged    <> today() + 1)

                            if (sub(aus:NewStatus,1,3) = '330')
                                p_web.SSV('GetStatus:StatusNumber',sub(aus:oldStatus,1,3))
                                break
                            end ! if (sub(aus:NewStatus,1,3) = '330')
                            if (sub(aus:NewStatus,1,3) = '340')
                                p_web.SSV('GetStatus:StatusNumber',sub(aus:oldStatus,1,3))
                                break
                            end ! if (sub(aus:NewStatus,1,3) = '330')
                        end ! loop
                    end ! if (sub(p_web.GSV('job:Current_Status',1,3) = '330' Or |
            end ! case p_web.GSV('CheckParts:ReturnValue')
    end ! case p_web.GSV('CheckParts:ReturnValue')
    if (p_web.GSV('GetStatus:StatusNumber') > 0)
        p_web.SSV('GetStatus:Type','JOB')
        getStatus(p_web)
    end ! if (p_web.GSV('GetStatus:StatusNumber') > 0)
completeRepair      routine
    data
locErrorMessage     String(10000)
    code

    p_web.SSV('JobCompleteProcess',0)
    p_web.SSV('locCompleteRepair','')
    p_web.SSV('textCompleteRepair','')
    calculateBilling(p_web)
    if (p_web.GSV('job:Repair_Type') = '' and p_web.GSV('job:Repair_Type_Warranty') = '')
        locErrorMessage = 'The fault codes allocated have not returned a repair type.<13,10>'
    end ! if (p_web.GSV('job:Repair_Type') = '' and p_web.GSV('job:Repair_Type_Warranty') = '')

    if (p_web.GSV('job:Chargeble_Job') = 'YES')
        Access:PARTS.Clearkey(par:part_number_key)
        par:ref_Number    = p_web.GSV('job:Ref_Number')
        set(par:part_number_key,par:part_number_key)
        loop
            if (Access:PARTS.Next())
                Break
            end ! if (Access:PARTS.Next())
            if (par:ref_Number    <> p_web.GSV('job:Ref_Number'))
                Break
            end ! if (par:part_number_Key    <> job:Ref_Number)
            if (par:part_Number = 'EXCH')
                if (par:Status = 'REQ')
                    locErrorMessage = clip(locErrorMessage) & 'An exchange unit has been requested for this job. ' & |
                                    ' No exchange unit has been allocated.<13,10>'
                end ! if (par:Status = 'REQ')
            else ! if (par:part_Number = 'EXCH')
                Access:STOCK.Clearkey(sto:ref_Number_Key)
                sto:ref_Number    = par:part_Ref_Number
                if (Access:STOCK.TryFetch(sto:ref_Number_Key) = Level:Benign)
                    ! Found
                    if (~par:PartAllocated and useStockAllocation(sto:Location))
                        locErrorMessage = clip(locErrorMessage) & |
                                'Part ' & Clip(par:Part_Number) & ', ' & |
                                 Clip(par:Description) & ' has not been allocated to this repair.<13,10>'
                    end ! if (par:PartAllocated)
                else ! if (Access:STOCK.TryFetch(sto:ref_Number_Key) = Level:Benign)
                    ! Error
                end ! if (Access:STOCK.TryFetch(sto:ref_Number_Key) = Level:Benign)
            end ! if (par:part_Number = 'EXCH')
        end ! loop
    end ! if (p_web.GSV('job:Chargeble_Job') = 'YES')

    if (p_web.GSV('job:Warranty_Job') = 'YES')
        Access:WARPARTS.Clearkey(wpr:part_number_key)
        wpr:Ref_Number    = p_web.GSV('job:Ref_Number')
        set(wpr:part_number_key,wpr:part_number_key)
        loop
            if (Access:WARPARTS.Next())
                Break
            end ! if (Access:PARTS.Next())
            if (wpr:ref_Number    <> p_web.GSV('job:Ref_Number'))
                Break
            end ! if (par:part_number_Key    <> job:Ref_Number)
            if (wpr:part_Number = 'EXCH')
                if (wpr:Status = 'REQ')
                    locErrorMessage = clip(locErrorMessage) & 'An exchange unit has been requested for this job. ' & |
                                    ' No exchange unit has been allocated.<13,10>'
                end ! if (par:Status = 'REQ')
            else ! if (par:part_Number = 'EXCH')
                Access:STOCK.Clearkey(sto:ref_Number_Key)
                sto:ref_Number    = wpr:part_Ref_Number
                if (Access:STOCK.TryFetch(sto:ref_Number_Key) = Level:Benign)
                    ! Found
                    if (~wpr:PartAllocated and useStockAllocation(sto:Location))
                        locErrorMessage = clip(locErrorMessage) & |
                                'Part ' & Clip(wpr:Part_Number) & ', ' & |
                                        Clip(wpr:Description) & ' has not been allocated to this repair.<13,10>'
                    end ! if (par:PartAllocated)
                else ! if (Access:STOCK.TryFetch(sto:ref_Number_Key) = Level:Benign)
                    ! Error
                end ! if (Access:STOCK.TryFetch(sto:ref_Number_Key) = Level:Benign)
            end ! if (par:part_Number = 'EXCH')
        end ! loop
    end ! if (p_web.GSV('job:Chargeble_Job') = 'YES')

    p_web.SSV('locCompleteRepair',locErrorMessage)

        p_web.SSV('CompulsoryFieldCheck:Type','C')
        
        compulsoryFieldCheck(p_web)
        
    if (p_web.GSV('locCompleteRepair') <> '')
        p_web.SSV('locCompleteRepair','The repair cannot be completed because of the following reasons:<13,10,13,10>' |
                & p_web.GSV('locCompleteRepair'))
        ShowALert('This repair cannot be completed.')
        exit
    end ! if (p_web.GSV('locCompleteRepair') <> '')

    p_web.SSV('JobPricingRoutine:Force',0)
    JobPricingRoutine(p_web)

    p_web.SSV('textCompleteRepair','The repair will be completed when you click "Save". ' & |
                'You can then QA the unit. You will not be able to un-complete the repair.')
    ShowAlert('This repair will be completed when you click Save.')

    p_web.SSV('job:On_Test','YES')
    p_web.SSV('job:Date_On_Test',Today())
    p_web.SSV('job:Time_On_Test',Clock())

    p_web.SSV('GetStatus:StatusNumber',605)
    p_web.SSV('GetStatus:Type','JOB')
    getStatus(p_web)



deleteVariables     routine
    ! Remove Locks
    IF (p_web.GSV('Job:ViewOnly') <> 1)
        lockRecord(p_web.gsv('job:Ref_Number'),p_web.sessionid,1) ! Remove Lock
    END
    
    p_web.deleteSessionValue('locEngineeringOption')
    p_web.deleteSessionValue('locCurrentEngieer')
    p_web.deleteSessionValue('locChargeableParts')
    p_web.deleteSessionValue('locWarrantyParts')
    p_web.deleteSessionValue('locCompleRepair')
    p_web.deleteSessionValue('locCChargeTypeReason')
    p_web.deleteSessionValue('locWChargeTypeReason')
    p_web.deleteSessionValue('locCRepairTypeReason')
    p_web.deleteSessionValue('locWRepairTypeReason')
    p_web.deleteSessionValue('locEstimateReadyOption')
    p_web.deleteSessionValue('SecondTime')
    p_web.deleteSessionValue('Hide:ButtonViewCosts')
    p_web.deleteSessionValue('Hide:ButtonFaultCodes')
    p_web.deleteSessionValue('Hide:ButtonAccessories')
    p_web.deleteSessionValue('Hide:ButtonAllocateEngineer')   
    p_web.DeleteSessionValue('Hide:ButtonCreateInvoice')
    
didAnythingChange   routine
    data
locNotes        String(255)
    code

    !Have Change Reasons Been Entered.

        If (p_web.GSV('job:Charge_Type') <> p_web.GSV('save:CChargeType') and p_web.GSV('save:CChargeType') <> '')
            if (p_web.GSV('locCChargeTypeReason') <> '' and p_web.GSV('Hide:CChargeTypeReason') = 0)
                p_web.SSV('AddToAudit:Type','JOB')
                p_web.SSV('AddToAudit:Action','DETAILS CHANGED: CHARGEABLE CHARGE TYPE')
                p_web.SSV('AddToAudit:Notes','PREVIOUS: ' & p_web.GSV('save:CChargeType') & '<13,10>REASON: ' & p_web.GSV('locCChargeTypeReason'))
                addToAudit(p_web)
            end !if (p_web.SSV('locCChargeTypeReason',''))
        end ! If (p_web.GSV('job:Charge_Type') <> p_web.GSV('save:CChargeType') and p_web.GSV('save:CChargeType') <> '')
        If (p_web.GSV('job:Warranty_Charge_Type') <> p_web.GSV('save:WChargeType') and p_web.GSV('save:WChargeType') <> '')
            if (p_web.GSV('locWChargeTypeReason') <> '' and p_web.GSV('Hide:WChargeTypeReason') = 0)
                p_web.SSV('AddToAudit:Type','JOB')
                p_web.SSV('AddToAudit:Action','DETAILS CHANGED: WARRANTY CHARGE TYPE')
                p_web.SSV('AddToAudit:Notes','PREVIOUS: ' & p_web.GSV('save:WChargeType') & '<13,10>REASON: ' & p_web.GSV('locWChargeTypeReason'))
                addToAudit(p_web)
            end ! if (p_web.SSV('locWChargeTypeReason',''))
        end ! If (p_web.GSV('job:Charge_Type') <> p_web.GSV('save:CChargeType') and p_web.GSV('save:CChargeType') <> '')
        If (p_web.GSV('job:Repair_Type') <> p_web.GSV('save:CRepairType') and p_web.GSV('save:CRepairType') <> '')
            if (p_web.GSV('locCRepairTypeReason') <> '' and p_web.GSV('Hide:CRepairTypeReason') = 0)
                p_web.SSV('AddToAudit:Type','JOB')
                p_web.SSV('AddToAudit:Action','DETAILS CHANGED: CHARGEABLE REPAIR TYPE')
                p_web.SSV('AddToAudit:Notes','PREVIOUS: ' & p_web.GSV('save:CRepairType') & '<13,10>REASON: ' & p_web.GSV('locCRepairTypeReason'))
                addToAudit(p_web)
            end ! if (p_web.SSV('locWRepairTypeReason',''))
        end ! If (p_web.GSV('job:Charge_Type') <> p_web.GSV('save:CChargeType') and p_web.GSV('save:CChargeType') <> '')
        If (p_web.GSV('job:Repair_Type_Warranty') <> p_web.GSV('save:WRepairType') and p_web.GSV('save:WRepairType') <> '')
            if (p_web.GSV('locWRepairTypeReason') <> '' and p_web.GSV('Hide:WRepairTypeReason') = 0)
                p_web.SSV('AddToAudit:Type','JOB')
                p_web.SSV('AddToAudit:Action','DETAILS CHANGED: WARRANTY REPAIR TYPE')
                p_web.SSV('AddToAudit:Notes','PREVIOUS: ' & p_web.GSV('save:WRepairType') & '<13,10>REASON: ' & p_web.GSV('locWChargeTypeReason'))
                addToAudit(p_web)
            end ! if (p_web.SSV('locWRepairTypeReason',''))
        end ! If (p_web.GSV('job:Charge_Type') <> p_web.GSV('save:CChargeType') and p_web.GSV('save:CChargeType') <> '')

        p_web.SSV('AddToAudit:Type','JOB')

    !----
        locNotes = ''
        if (p_web.GSV('save:FaultDescription') <> p_web.GSV('jbn:Fault_Description'))
            locNotes = clip(locNotes) & |
                '<13,10,13,10>' & CLip(jbn:Fault_Description)
            p_web.SSV('AddToAudit:Notes','JOB DETAILS CHANGED:' & clip(locNotes))
            p_web.SSV('AddToAudit:Action','JOB UPDATED: FAULT DESCRIPION')
            p_web.SSV('AddToAudit:Type','JOB')
            addToAudit(p_web)
        end ! if (p_web.GSV('save:FaultDescription') <> p_web.GSV('jbn:Fault_Description'))

        p_web.SSV('AddToAudit:Action',p_web.GSV('JOB UPDATED'))

        locNotes = ''
        if (p_web.GSV('save:ChargeableJob') <> p_web.GSV('job:Chargeable_Job'))
            locNotes = clip(locNotes) & |
                '<13,10,13,10>CHARGEABLE JOB: ' & p_web.GSV('job:Chargeable_Job')
        end ! if (p_web.GSV('save:ChargeableJob') <> p_web.GSV('job:Chargeable_Job'))

        if (p_web.GSV('save:WarrantyJob') <> p_web.GSV('job:Warranty_Job'))
            locNotes = clip(locNotes) & |
                '<13,10,13,10>WARRANTY JOB: ' & p_web.GSV('job:Warranty_Job')
        end ! if (p_web.GSV('save:WarrantyJob') <> p_web.GSV('job:Warranty_Job'))

        if (p_web.GSV('save:HubRepair') <> p_web.GSV('jobe:HubRepair'))
            if (p_web.GSV('jobe:HubRepair') = 1)
                locNotes = clip(locNotes) & |
                    '<13,10,13,10>HUB REPAIR SELECTED'
            else ! if (p_web.GSV('jobe:HubRepair') = 1)
                locNotes = clip(locNotes) & |
                    '<13,10,13,10>HUB REPAIR SELECTED'
            end ! if (p_web.GSV('jobe:HubRepair') = 1)
        end ! if (p_web.GSV('save:HubRepair') <> p_web.GSV('jobe:HubRepair'))

        if (clip(locNotes) <> '')
            p_web.SSV('AddToAudit:Notes','JOB DETAILS CHANGED:' & clip(locNotes))
            p_web.SSV('AddToAudit:Action','JOB UPDATED')
            p_web.SSV('AddToAudit:Type','JOB')
            addToAudit(p_web)

        end ! if (clip(locNotes) <> '')

    !--

        locNotes = ''
        if (p_web.GSV('save:TransitType') <> p_web.GSV('job:Transit_Type'))
            locNotes = clip(locNotes) & |
                '<13,10,13,10>TRANS TYPE: ' & p_web.GSV('job:Transit_Type')
        end ! if (p_web.GSV('save:TransitType') <> p_web.GSV('job:Transit_Type'))

        if (p_web.GSV('save:MSN') <> p_web.GSV('job:MSN'))
            locNotes = clip(locNotes) & |
                '<13,10,13,10>MSN: ' & p_web.GSV('job:MSN')
        end ! if (p_web.GSV('save:MSN') <> p_web.GSV('job:MSN'))

        if (p_web.GSV('save:ModelNumber') <> p_web.GSV('job:Model_Number'))
            locNotes = clip(locNotes) & |
                '<13,10,13,10>MODEL NO: ' & p_web.GSV('job:Model_Number')
        end ! if (p_web.GSV('save:ModelNumber') <> p_web.GSV('job:Model_Number'))

        if (p_web.GSV('save:DOP') <> p_web.GSV('job:DOP'))
            locNotes = clip(locNotes) & |
                '<13,10,13,10>DOP: ' & CLip(Format(p_web.GSV('job:DOP'),@d6))
        end ! if (p_web.GSV('save:DOP') <> p_web.GSV('job:DOP'))

        if (p_web.GSV('save:OrderNumber') <> p_web.GSV('job:Order_Number'))
            locNotes = clip(locNotes) & |
                '<13,10,13,10>ORDER NO: ' & p_web.GSV('job:Order_Number')
        end ! if (p_web.GSV('save:OrderNumber') <> p_web.GSV('job:Order_Number'))

        if (clip(locNotes) <> '')
            p_web.SSV('AddToAudit:Notes','JOB DETAILS CHANGED:' & clip(locNotes))
            p_web.SSV('AddToAudit:Action','JOB UPDATED: FAULT DESCRIPTION')
            p_web.SSV('AddToAudit:Type','JOB')
            addToAudit(p_web)

        end ! if (clip(locNotes) <> '')

    !----
        locNotes = ''
        loop x# = 1 to 20
            if (x# < 13)
                if (p_web.GSV('save:FaultCode' & x#) <> p_web.GSV('job:Fault_Code' & x#))
                    locNotes = clip(locNotes) & |
                        '<13,10,13,10>FAULT CODE' & x# & ': ' & p_web.GSV('job:Fault_Code' & x#)
                end !if (p_web.GSV('save:FaultCode' & x#) <> p_web.GSV('job:Fault_Code' & x#))
            else  ! if (x# < 13)
                if (p_web.GSV('save:FaultCode' & x#) <> p_web.GSV('wob:FaultCode' & x#))
                    locNotes = clip(locNotes) & |
                        '<13,10,13,10>FAULT CODE' & x# & ': ' & p_web.GSV('wob:FaultCode' & x#)
                end !if (p_web.GSV('save:FaultCode' & x#) <> p_web.GSV('job:Fault_Code' & x#))
            end ! if (x# < 13)
        end ! loop x# = 1 to 20
        if (clip(locNotes) <> '')
            p_web.SSV('AddToAudit:Notes','JOB DETAILS CHANGED:' & clip(locNotes))
            p_web.SSV('AddToAudit:Action','JOB UPDATED')
            p_web.SSV('AddToAudit:Type','JOB')
            addToAudit(p_web)

        end ! if (clip(locNotes) <> '')

    !----
        if (p_web.GSV('save:AccountNumber') <> p_web.GSV('job:account_number'))
            p_web.SSV('AddToAudit:Type','JOB')
            p_web.SSV('AddToAudit:Action','ACCOUNT NUMBER CHANGED')
            p_web.SSV('AddToAudit:Notes','PREVIOUS ACCOUNT: ' & p_web.GSV('save:AccountNumber') & |
                '<13,10>NEW ACCOUNT: ' & p_web.GSV('job:Account_Number'))
            addToAudit(p_web)
        end !if (p_web.GSV('save:AccountNumber') <> p_web.GSV('job:account_number'))

        if (p_web.GSV('save:CompanyName') <> p_web.GSV('job:Company_Name') Or |
            p_web.GSV('save:AddressLine1') <> p_web.GSV('job:Address_Line1') Or |
            p_web.GSV('save:AddressLine2') <> p_web.GSV('job:Address_Line2') Or |
            p_web.GSV('save:AddressLine3') <> p_web.GSV('job:Address_Line3') Or |
            p_web.GSV('save:Postcode') <> p_web.GSV('job:Postcode') Or |
            p_web.GSV('save:TelephoneNumber') <> p_web.GSV('job:Telephone_Number') Or |
            p_web.GSV('save:FaxNumber') <> p_web.GSV('job:Fax_Number'))

            p_web.SSV('AddToAudit:Notes','PREVIOUS: <13,10>' & p_web.GSV('save:CompanyName') & |
                '<13,10>' & p_web.GSV('save:AddressLine1') & |
                '<13,10>' & p_web.GSV('save:AddressLine2') & |
                '<13,10>' & p_web.GSV('save:AddressLine3') & |
                '<13,10>' & p_web.GSV('save:Postcode') & |
                '<13,10>TEL: ' & p_web.GSV('save:TelephoneNumber') & |
                '<13,10>FAX: ' & p_web.GSV('save:FaxNumber'))
            p_web.SSV('AddToAudit:Action','ADDRESS DETAILS CHANGED - CUSTOMER')
            p_web.SSV('AddToAudit:Type','JOB')
            addToAudit(p_web)
        end ! if (p_web.GSV('save:CompanyName') <> p_web.GSV('job:Company_Name') Or |

        if (p_web.GSV('save:CompanyNameDelivery') <> p_web.GSV('job:Company_Name_Delivery') Or |
            p_web.GSV('save:AddressLine1Delivery') <> p_web.GSV('job:Address_Line1_Delivery') Or |
            p_web.GSV('save:AddressLine2Delivery') <> p_web.GSV('job:Address_Line2_Delivery') Or |
            p_web.GSV('save:AddressLine3Delivery') <> p_web.GSV('job:Address_Line3_Delivery') Or |
            p_web.GSV('save:PostcodeDelivery') <> p_web.GSV('job:Postcode_Delivery') Or |
            p_web.GSV('save:TelephoneDelivery') <> p_web.GSV('job:Telephone_Delivery'))

            p_web.SSV('AddToAudit:Notes','PREVIOUS: <13,10>' & p_web.GSV('save:CompanyNameDelivery') & |
                '<13,10>' & p_web.GSV('save:AddressLine1Delivery') & |
                '<13,10>' & p_web.GSV('save:AddressLine2Delivery') & |
                '<13,10>' & p_web.GSV('save:AddressLine3Delivery') & |
                '<13,10>' & p_web.GSV('save:PostcodeDelivery') & |
                '<13,10>TEL: ' & p_web.GSV('save:TelephoneDelivery'))
            p_web.SSV('AddToAudit:Action','ADDRESS DETAILS CHANGED - DESPATCH')
            p_web.SSV('AddToAudit:Type','JOB')
            addToAudit(p_web)
        end ! if (p_web.GSV('save:CompanyName') <> p_web.GSV('job:Company_Name') Or |


haveAccessoriesChanged      routine
    data
locAccessoryQueue      Queue(),Pre(locque)
sessionID                  Long()
Accessory                  String(30)
                        End
locAuditNotes         String(255)
    code
        loop x# = 1 to records(locAccessoryQueue)
            get(locAccessoryQUeue,x#)
            if (locque:sessionID = p_web.sessionID)
                delete(locAccessoryQueue)
            end !if (locque:sessionID) = p_web.sessionID)
        end ! loop x# = 1 to records(locAccessoryQueue)

        if Clip(p_web.GSV('tmp:TheJobAccessory')) <> ''
            Loop x# = 1 To 1000
                If Sub(p_web.GSV('tmp:TheJobAccessory'),x#,2) = '|;'
                    Start# = x# + 2
                    Cycle
                End ! If Sub(p_web.GSV('tmp:TheJobAccessory'),x#,2) = '|;'

                If Start# > 0
                    If Sub(p_web.GSV('tmp:TheJobAccessory'),x#,2) = ';|'
                        tmp:FoundAccessory = Sub(p_web.GSV('tmp:TheJobAccessory'),Start#,x# - Start#)

                        If tmp:FoundAccessory <> ''
                            locque:sessionID = p_web.SessionID
                            locque:Accessory = clip(tmp:foundACcessory)
                            add(locAccessoryQUeue)
                        End ! If tmp:FoundAccessory <> ''
                        Start# = 0
                    End ! If Sub(p_web.GSV('tmp:TheJobAccessory'),x#,2) = ';|'
                End ! If Start# > 0
            End ! Loop x# = 1 To 1000

            If Start# > 0
                tmp:FoundAccessory = Clip(Sub(p_web.GSV('tmp:TheJobAccessory'),Start#,30))
                If tmp:FoundAccessory <> ''
                    If tmp:FoundAccessory <> ''
                        locque:sessionID = p_web.SessionID
                        locque:Accessory = clip(tmp:foundAccessory)
                        add(locAccessoryQUeue)
                    End ! If tmp:FoundAccessory <> ''
                End ! If tmp:FoundAccessory <> ''
            End ! If Start# > 0
        end ! If Clip(p_web.GSV('tmp:TheJobAccessory') <> ''

        locAuditNotes = ''
        loop x# = 1 to records(locAccessoryQueue)
            get(locAccessoryQueue,x#)
            if (locque:sessionID <> p_web.sessionID)
                cycle
            end ! if (locque:sessionID <> p_web.sessionID)
            Access:JOBACC.Clearkey(jac:ref_Number_Key)
            jac:ref_Number    = p_web.GSV('job:Ref_Number')
            jac:accessory    = locque:accessory
            if (Access:JOBACC.TryFetch(jac:ref_Number_Key) = Level:Benign)
                ! Found
            else ! if (Access:JOBACC.TryFetch(jac:ref_Number_Key) = Level:Benign)
                ! Error
                ! Accessory has been added
                locAuditNotes = clip(locAuditNotes) & '<13,10>' & clip(locque:accessory) & ' -ADDED'
                If Access:JOBACC.PrimeRecord() = Level:Benign
                    jac:Ref_Number = p_web.GSV('job:Ref_Number')
                    jac:Accessory = locque:accessory
                    jac:Damaged = 0

                    If p_web.GSV('BookingSite') = 'RRC'
                        jac:Attached = False
                    Else !If jobe:WebJob
                        jac:Attached = True
                    End !If jobe:WebJob
                    If Access:JOBACC.TryInsert() = Level:Benign
                    Else !If Access:JOBACC.TryInsert() = Level:Benign
                        Access:JOBACC.CancelAutoInc()
                    End !If Access:JOBACC.TryInsert() = Level:Benign

                End ! If Access:JOBACC.PrimeRcord() = Level:Benign

            end ! if (Access:JOBACC.TryFetch(jac:ref_Number_Key) = Level:Benign)
        end ! loop x# = 1 to records(locAccessoryQueue)


        Access:JOBACC.Clearkey(jac:ref_Number_Key)
        jac:ref_Number    = p_web.GSV('job:Ref_Number')
        set(jac:ref_Number_Key,jac:ref_Number_Key)
        loop
            if (Access:JOBACC.Next())
                Break
            end ! if (Access:JOBACC.Next())
            if (jac:ref_Number    <> p_web.GSV('job:Ref_Number'))
                Break
            end ! if (jac:ref_Number    <> p_web.GSV('job:Ref_Number'))
            clear(locAccessoryQueue)
            locque:sessionID = p_web.SessionID
            locque:accessory = jac:accessory
            get(locAccessoryQueue,locque:sessionID, locque:accessory)
            if (error())
                ! Accessory has been removed
                locAuditNotes = clip(locAuditNotes) & '<13,10>' & clip(locque:accessory) & ' -DELETED'
                access:JOBACC.deleterecord(0)
            end ! if (error())
        end ! loop

        if (locAuditNotes <> '')
            p_web.SSV('AddToAudit:Type','JOB')
            p_web.SSV('AddToAudit:Action','JOB ACCESSORIES AMENDED')
            p_web.SSV('AddToAudit:Notes',sub(clip(locAuditNotes),3,255))
            addToAudit(p_web)
        end ! if (locAuditNotes <> '')
ReceiptFromPUP      Routine
Data
local:Site              String(3)
local:CurrentLocation   String(30)
local:CurrentStatus     String(3)
local:DefaultStatus     String(3)
local:AuditNotes        String(255)
Code

!    If glo:WebJob
!        ! Inserting (DBH 13/11/2006) # 8485 - Check that the logged in user matches the RRC of the job
!        Access:USERS.ClearKey(use:Password_Key)
!        use:Password = glo:Password
!        If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
!            !Found
!            If use:Location <> tmp:BookingSiteLocation
!                Case Missive('You cannot receive a job from a different RRC.','ServiceBase 3g',|
!                               'mstop.jpg','/OK') 
!                    Of 1 ! OK Button
!                End ! Case Missive
!                Exit
!            End ! If use:Location <> tmp:BookingSiteLocation
!        Else ! If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
!            !Error
!        End ! If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
!        ! End (DBH 13/11/2006) #8485
!        local:Site = 'RRC'
!        local:CurrentLocation = GETINI('RRC','RRCLocation',,Clip(Path()) & '\SB2KDEF.INI')
!    Else ! If glo:WebJob
!        local:Site = 'ARC'
!        local:CurrentLocation = GETINI('RRC','ARCLocation',,Clip(Path()) & '\SB2KDEF.INI')
!    End ! If glo:WebJob
!    local:DefaultStatus = GETINI('RRC','StatusReceivedFromPUP',,Clip(Path()) & '\SB2KDEF.INI')
!
!    If job:Location <> GETINI('RRC','InTransitFromPUPLocation',,Clip(Path()) & '\SB2KDEF.INI')
!! Changing (DBH 07/08/2006) # 8021 - Not at right location. Does the user have access to press this button?
!!        Case Missive('Warning! The current location of this job is NOT "In Transit From PUP".'&|
!!          'Are you sure you still want to receive this job from the PUP?','ServiceBase 3g',|
!!                       'mexclam.jpg','\Cancel|/Receive') 
!!            Of 2 ! Receive Button
!!            Of 1 ! Cancel Button
!!                Exit
!!        End ! Case Missive
!! to (DBH 07/08/2006) # 8021
!        If SecurityCheck('FORCE RECEIPT FROM PUP')
!            Case Missive('Cannot receive this job!'&|
!              '|The current location is NOT "In Transit From PUP".','ServiceBase 3g',|
!                           'mstop.jpg','/OK') 
!                Of 1 ! OK Button
!            End ! Case Missive
!            Exit
!        Else ! If SecurityCheck('FORCE RECEIPT FROM PUP')
!            Case Missive('Warning! The current location of this job is NOT "In Transit From PUP".'&|
!              'Are you sure you still want to receive this job from the PUP?','ServiceBase 3g',|
!                           'mexclam.jpg','\Cancel|/Receive') 
!                Of 2 ! Receive Button
!                Of 1 ! Cancel Button
!                    Exit
!            End ! Case Missive
!        End ! If SecurityCheck('FORCE RECEIPT FROM PUP')
!! End (DBH 07/08/2006) #8021
!    Else ! If job:Location <> GETINI('RRC','InTransitFromPUPLocation',,Clip(Path()) & '\SB2KDEF.INI')
!        Case Missive('Are you sure you want to receive the PUP job into the ' & Clip(local:Site) & '?','ServiceBase 3g',|
!                       'mquest.jpg','\No|/Yes') 
!            Of 2 ! Yes Button
!            Of 1 ! No Button
!                Exit
!        End ! Case Missive
!    End ! If job:Location <> GETINI('RRC','InTransitFromPUPLocation',,Clip(Path()) & '\SB2KDEF.INI')
!
!    do PUPValidation
!
!    local:AuditNotes = ''
!
!    Rtn# = AccessoryCheck('JOB')
!
!    If Rtn# = 1 Or Rtn# = 2
!        local:AuditNotes = 'ACCESSORY MISMATCH. BOOKED AT PUP:-'
!        
!        Save_jac_ID = Access:JOBACC.SaveFile()
!        Access:JOBACC.Clearkey(jac:Ref_Number_Key)
!        jac:Ref_Number = job:Ref_Number
!        Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
!        Loop ! Begin JOBACC Loop
!            If Access:JOBACC.Next()
!                Break
!            End ! If !Access
!            If jac:Ref_Number <> job:Ref_Number
!                Break
!            End ! If
!            local:AuditNotes = Clip(local:AuditNotes) & '<13,10>' & Clip(jac:Accessory)
!        End ! End JOBACC Loop
!        Access:JOBACC.RestoreFile(Save_jac_ID)
!        local:AuditNotes = Clip(local:AuditNotes) & '<13,10,13,10>ACCESSORIES RECEIVED:-'
!        Loop x# = 1 To Records(glo:Queue)
!            Get(glo:Queue,x#)
!            local:AuditNotes = Clip(local:AuditNotes) & '<13,10>' & Clip(glo:Pointer)
!        End ! Loop x# = 1 To Records(glo:Queue)
!    End ! If Rtn# = 1 Or Rtn# = 2
!
!    If AddToAudit(job:Ref_Number,'JOB','UNIT RECEIVED AT ' & CLip(local:Site) & ' FROM PUP',Clip(local:AuditNotes))
!        LocationChange(GETINI('RRC',local:Site & 'Location',,Clip(Path()) & '\SB2KDEF.INI'))
!        GetStatus(local:DefaultStatus,0,'JOB')
!
!        Case Missive('This job will now be saved.','ServiceBase 3g',|
!                       'midea.jpg','/OK') 
!            Of 1 ! OK Button
!        End ! Case Missive
!        ! Changing (DBH 03/07/2006) # 7149 - Do not do the OK validation
!        ! Post(Event:Accepted,?OK)
!        ! ! to (DBH 03/07/2006) # 7149
!        job:Workshop = 'YES'
!        Access:JOBS.Update()
!        do SetJobse
!        Access:JOBSE.Update()
!        ! Inserting (DBH 16/09/2008) # 10253 - Update Date/Time Stamp
!        UpdateDateTimeStamp(job:Ref_Number)
!        ! End (DBH 16/09/2008) #10253
!        Post(Event:Accepted,?Close)
!        ! End (DBH 03/07/2006) #7149
!    End ! If AddToAuditEntry(job:Ref_Number,'JOB','UNIT RECEIVED AT ' & CLip(local:Site) & ' FROM PUP','')
!
!
!
ResendXML       Routine
Data
local:Seq        Long()
local:ToSendFolder  CString(255)
local:SentFolder    CString(255)
local:ReceivedFolder CString(255)
local:ProcessedFolder CString(255)
local:XMLFileName   CString(255)
Code
    Access:TRDPARTY.ClearKey(trd:Company_Name_Key)
    trd:Company_Name = p_web.GSV('job:Third_Party_Site')
    If Access:TRDPARTY.TryFetch(trd:Company_Name_Key) = Level:Benign
        !Found
        If trd:LHubAccount = 0
            Exit
        End ! If trd:LHubAccount = 0
    Else ! If Access:TRDPARTY.TryFetch(trd:Company_Name_Key) = Level:Benign
        !Error
    End ! If Access:TRDPARTY.TryFetch(trd:Company_Name_Key) = Level:Benign


    local:ToSendFolder = GETINI('NMSFTP','XMLFolder',,Clip(Path()) & '\SB2KDEF.INI')
    If Sub(Clip(local:ToSendFolder),-1,1) <> '\'
        local:ToSendFolder = Clip(local:ToSendFolder) & '\'
    End ! If Sub(Clip(local:ToSendFolder),-1,1) <> '\'

    local:SentFolder = Clip(local:ToSendFolder) & 'Sent'

    local:ReceivedFolder = Clip(local:ToSendFolder) & 'Received'

    local:ProcessedFolder = Clip(local:ToSendFolder) & 'Processed'

    local:ProcessedFolder = Clip(local:ToSendFolder) & 'Bad'

    local:ToSendFolder = Clip(local:ToSendFolder) & 'ToSend'

    local:XMLFileName =  'RR_' & trb:Batch_Number & Format(Today(),@d12) & Clock() & '.xml'


    If objXmlExport.FOpen(local:ToSendFolder & '\' & local:XMLFileName,1) = Level:Benign
        objXmlExport.OpenTag('ShipmentOrderRequest','Version="4.0"')
        objXmlExport.OpenTag('Authentication')
            objXmlExport.WriteTag('SiteId',GETINI('NMSFTP','SiteID',,Clip(Path()) & '\SB2KDEF.INI'))
            objXmlExport.WriteTag('UserId',GETINI('NMSFTP','UserID',,Clip(Path()) & '\SB2KDEF.INI'))
            objXmlExport.WriteTag('ToolType','CT')
            objXmlExport.WriteTag('AccessKey',GETINI('NMSFTP','AccessKey',,Clip(Path()) & '\SB2KDEF.INI'))
        objXmlExport.CloseTag('Authentication')
        objXmlExport.WriteTag('ClientReferenceNumber',trb:Batch_Number & '-' & Today() & Clock())
        objXmlExport.WriteTag('ShipmentType','001')
        objXmlExport.WriteTag('SourceSiteId',trd:ASCID)
        objXmlExport.OpenTag('Booking')
            objXmlExport.WriteTag('SequenceNo',1)
            objXmlExport.WriteTag('BookingType','001')
            objXmlExport.WriteTag('ItemType','PHONE')
            objXmlExport.WriteTag('ClientBookingNumber',p_web.GSV('job:Ref_Number'))
            objXmlExport.WriteTag('FaultSymptomCode',Clip(p_web.GSV('job:Fault_Code1')))
            objXmlExport.WriteTag('ExistenceOfSymptom',Clip(p_web.GSV('wob:FaultCode15')))
            objXmlExport.WriteTag('OrderType','REPAIR')
            objXmlExport.WriteTag('ProductCode',Clip(p_web.GSV('job:ProductCode')))
            objXmlExport.WriteTag('OriginalSerialNumber',Clip(p_web.GSV('job:ESN')))
            objXmlExport.WriteTag('ClientBookingTimestamp',Format(Today(),@d05b) & ' ' & Format(Clock(),@t04b))
            If p_web.GSV('job:Warranty_Job') = 'YES'
                objXmlExport.WriteTag('WarrantyOption','001')
            Else ! If p_web.GSV('job:Warranty_Job = 'YES'
                objXmlExport.WriteTag('WarrantyOption','002')
            End ! If p_web.GSV('job:Warranty_Job = 'YES'
            objXmlExport.WriteTag('WarrantyDatabaseDate',Format(p_web.GSV('job:DOP'),@d05b) & ' ' & Format(Clock(),@t04b))
            objXmlExport.WriteTag('OwnershipCode','001')
            Found# = 0
            If p_web.GSV('job:Warranty_Job') = 'YES'
                If Instring('SOFTWARE',Upper(p_web.GSV('job:Repair_Type_Warranty')),1,1)
                    objXmlExport.WriteTag('TransactionType','002')
                    Found# = 1
                End ! If Instring('SOFTWARE',Upper(p_web.GSV('job:Warranty_Repair_Type),1,1)
            Else ! If p_web.GSV('job:Warranty_Job = 'YES'
                If Instring('SOFTWARE',Upper(p_web.GSV('job:Repair_Type')),1,1)
                    objXmlExport.WriteTag('TransactionType','002')
                    Found# = 1
                End ! If Instring('SOFTWARE',Upper(p_web.GSV('job:Repair_Type),1,1)
            End ! If p_web.GSV('job:Warranty_Job = 'YES'
            If Found# = 0
                objXmlExport.WriteTag('TransactionType','001')
            End ! If Found# = 0
        objXmlExport.CloseTag('Booking')
        objXmlExport.CloseTag('ShipmentOrderRequest')
        objXMLExport.FClose()
    End ! If objXmlExport.FOpen(Filename.xml,1) = Level:Benign

saveFields      routine
    p_web.SSV('save:CChargeType',p_web.GSV('job:Charge_Type'))
    p_web.SSV('save:CRepairType',p_web.GSV('job:Repair_Type'))
    p_web.SSV('save:WChargeType',p_web.GSV('job:Warranty_Charge_Type'))
    p_web.SSV('save:WRepairType',p_web.GSV('job:Repair_Type_Warranty'))
    p_web.SSV('save:ChargeableJob',p_web.GSV('job:Chargeable_Job'))
    p_web.SSV('save:WarrantyJob',p_web.GSV('job:Warranty_Job'))
    p_web.SSV('save:HubRepair',p_web.GSV('jobe:HubRepair'))
    p_web.SSV('save:TransitType',p_web.GSV('job:Transit_Type'))

    p_web.SSV('save:MSN',p_web.GSV('job:MSN'))
    p_web.SSV('save:ModelNumber',p_web.GSV('job:Model_Number'))
    p_web.SSV('save:DOP',p_web.GSV('job:DOP'))
    p_web.SSV('save:OrderNumber',p_web.GSV('job:Order_Number'))
    loop x#= 1 to 20
        if (x# < 13)
            p_web.SSV('save:FaultCode' & x#,p_web.GSV('job:Fault_Code' & x#))
        else
            p_web.SSV('save:FaultCode' & x#,p_web.GSV('wob:FaultCode' & x#))
        end ! if (x# < 13)
    end ! loop x#= 1 to 20

    p_web.SSV('save:FaultDescription',p_web.GSV('jbn:Fault_Description'))
    
    p_web.SSV('save:AccountNumber',p_web.GSV('job:Account_Number'))
    p_web.SSV('save:CompanyName',p_web.GSV('job:Company_Name'))
    p_web.SSV('save:AddressLine1',p_web.GSV('job:Address_Line1'))
    p_web.SSV('save:AddressLine2',p_web.GSV('job:Address_Line2'))
    p_web.SSV('save:AddressLine3',p_web.GSV('job:Address_Line3'))
    p_web.SSV('save:Postcode',p_web.GSV('job:Postcode'))
    p_web.SSV('save:TelephoneNumber',p_web.GSV('job:Telephone_Number'))
    p_web.SSV('save:FaxNumber',p_web.GSV('job:Fax_Number'))
    p_web.SSV('save:AddressLine1Delivery',p_web.GSV('job:Address_Line1_Delivery'))
    p_web.SSV('save:CompanyNameDelivery',p_web.GSV('job:Company_Name_Delivery'))
    p_web.SSV('save:AddressLine2Delivery',p_web.GSV('job:Address_Line2_Delivery'))
    p_web.SSV('save:AddressLine3Delivery',p_web.GSV('job:Address_Line3_Delivery'))
    p_web.SSV('save:PostcodeDelivery',p_web.GSV('job:Postcode_Delivery'))
    p_web.SSV('save:TelephoneDelivery',p_web.GSV('job:Telephone_Delivery'))


SetStatusAccessLevelFilter          routine
    p_web.SSV('filter:AccessLevel','***')
    if (p_web.GSV('job:Engineer') <> '')
        Access:USERS.Clearkey(use:User_Code_Key)
        use:User_Code    = p_web.GSV('job:Engineer')
        if (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
            ! Found
            p_web.SSV('filter:AccessLevel',use:User_Level)
        else ! if (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
            ! Error
        end ! if (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
    else ! if (p_web.GSV('job:Engineer') <> '')
        Access:USERS.Clearkey(use:Password_Key)
        use:Password    = p_web.GSV('BookingUserPassword')
        if (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
            ! Found
            p_web.SSV('filter:AccessLevel',use:User_Level)
        else ! if (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
            ! Error
        end ! if (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
    end ! if (p_web.GSV('job:Engineer') <> '')
waybillCheck        routine
    if (p_web.GSV('jobe:HubRepair') = 1)
        if (p_web.GSV('job:Incoming_Consignment_Number') <> '')
            Access:WAYBAWT.Clearkey(wya:accountJobNumberKey)
            wya:accountNumber    = p_web.GSV('wob:HeadAccountNumber')
            wya:jobNumber    = p_web.GSV('job:ref_Number')
            if (Access:WAYBAWT.TryFetch(wya:accountJobNumberKey) = Level:Benign)
                ! Found
                relate:WAYBAWT.delete(0)
            end ! if (Access:WAYBAWT.TryFetch(wya:accountJobNumberKey) = Level:Benign)

            exit
        end ! if (p_web.GSV('job:Incoming_Consignment_Number') <> '')

        if (p_web.GSV('job:Location') <> p_web.GSV('Default:RRCLocation'))
            Access:WAYBAWT.Clearkey(wya:accountJobNumberKey)
            wya:accountNumber    = p_web.GSV('wob:HeadAccountNumber')
            wya:jobNumber    = p_web.GSV('job:ref_Number')
            if (Access:WAYBAWT.TryFetch(wya:accountJobNumberKey) = Level:Benign)
                ! Found
                relate:WAYBAWT.delete(0)
            end ! if (Access:WAYBAWT.TryFetch(wya:accountJobNumberKey) = Level:Benign)

            exit
        end ! if (p_web.GSV('job:Location') <> p_web.GSV('Default:RRCLocation'))

        Access:WAYBAWT.Clearkey(wya:accountJobNumberKey)
        wya:accountNumber    = p_web.GSV('wob:HeadAccountNumber')
        wya:jobNumber    = p_web.GSV('job:ref_Number')
        if (Access:WAYBAWT.TryFetch(wya:accountJobNumberKey) = Level:Benign)
            ! Found
        else ! if (Access:WAYBAWT.TryFetch(wya:accountJobNumberKey) = Level:Benign)
            ! Error
            if (Access:WAYBAWT.PrimeRecord() = Level:Benign)
                wya:jobNumber        = p_web.GSV('job:Ref_Number')
                wya:accountNumber    = p_web.GSV('wob:HeadAccountNumber')
                wya:manufacturer    = p_web.GSV('job:manufacturer')
                wya:modelNumber        = p_web.GSV('job:model_Number')
                wya:IMEINumber        = p_web.GSV('job:ESN')
                if (Access:WAYBAWT.TryInsert() = Level:Benign)
                    ! Inserted
                else ! if (Access:WAYBAWT.TryInsert() = Level:Benign)
                    ! Error
                    Access:WAYBAWT.CancelAutoInc()
                end ! if (Access:WAYBAWT.TryInsert() = Level:Benign)
            end ! if (Access:WAYBAWT.PrimeRecord() = Level:Benign)
        end ! if (Access:WAYBAWT.TryFetch(wya:accountJobNumberKey) = Level:Benign)
    else !if (p_web.GSV('jobe:HubRepair') = 1)
        Access:WAYBAWT.Clearkey(wya:accountJobNumberKey)
        wya:accountNumber    = p_web.GSV('wob:HeadAccountNumber')
        wya:jobNumber    = p_web.GSV('job:ref_Number')
        if (Access:WAYBAWT.TryFetch(wya:accountJobNumberKey) = Level:Benign)
            ! Found
            relate:WAYBAWT.delete(0)
        end ! if (Access:WAYBAWT.TryFetch(wya:accountJobNumberKey) = Level:Benign)
    end ! if (p_web.GSV('jobe:HubRepair') = 1)
    
OpenFiles  ROUTINE
  p_web._OpenFile(ESTPARTS)
  p_web._OpenFile(PARTS)
  p_web._OpenFile(WARPARTS)
  p_web._OpenFile(UNITTYPE)
  p_web._OpenFile(WEBJOB)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(JOBSE2)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(ACCSTAT)
  p_web._OpenFile(USERS)
  p_web._OpenFile(JOBNOTES)
  p_web._OpenFile(NETWORKS)
  p_web._OpenFile(SUBTRACC)
  p_web._OpenFile(JOBSWARR)
  p_web._OpenFile(JOBACC)
  p_web._OpenFile(WAYBAWT)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(ESTPARTS)
  p_Web._CloseFile(PARTS)
  p_Web._CloseFile(WARPARTS)
  p_Web._CloseFile(UNITTYPE)
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(JOBSE2)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(ACCSTAT)
  p_Web._CloseFile(USERS)
  p_Web._CloseFile(JOBNOTES)
  p_Web._CloseFile(NETWORKS)
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(JOBSWARR)
  p_Web._CloseFile(JOBACC)
  p_Web._CloseFile(WAYBAWT)
     FilesOpened = False
  END
      !Clear variables for update jobs
  
      ClearUpdateJobVariables(p_web)
      

InitForm       Routine
  DATA
LF  &FILE
  CODE
  IF (p_web.IfExistsValue('IgnoreMessage'))
      p_web.StoreValue('IgnoreMessage')
  END
  
  p_web.SetValue('ViewJob_form:inited_',1)
  p_web.SetValue('UpdateFile','WEBJOB')
  p_web.SetValue('UpdateKey','wob:RecordNumberKey')
  p_web.SetValue('IDField','wob:RecordNumber')
  do RestoreMem

CancelForm  Routine
  do waybillCheck
  IF p_web.GetSessionValue('ViewJob:Primed') = 1
    p_web._deleteFile(WEBJOB)
    p_web.SetSessionValue('ViewJob:Primed',0)
  End
  do deleteVariables

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  p_web.SetValue('UpdateFile','WEBJOB')
  p_web.SetValue('UpdateKey','wob:RecordNumberKey')
AfterLookup Routine
  !!After Lookup
  !if (p_web.getvalue('lookupfield') = 'job:Current_Status')
  !    if (loc:LookupDone)
  !        p_web.FileToSessionQueue(ACCSTAT)
  !        p_web.SSV('job:Current_Status',acs:Status)
  !    end ! if loc:LookupDone
  !end ! if (p_web.getvalue('lookupfield') = 'job:Current_Status')
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  If p_web.GSV('textBouncer') <> ''
    loc:TabNumber += 1
  End
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'job:Current_Status'
    p_web.setsessionvalue('showtab_ViewJob',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(ACCSTAT)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.job:Location')
  End
  If p_web.GSV('job:Warranty_Job') = 'YES' and p_web.GSV('job:Date_Completed') > 0
    loc:TabNumber += 1
  End
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'jobe:Network'
    p_web.setsessionvalue('showtab_ViewJob',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(NETWORKS)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.job:Authority_Number')
  Of 'job:Unit_Type'
    p_web.setsessionvalue('showtab_ViewJob',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(UNITTYPE)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.')
  End
  loc:TabNumber += 1
  If p_web.GSV('Hide:CustomerClassification') <> 1
    loc:TabNumber += 1
  End
  loc:TabNumber += 1
  loc:TabNumber += 1
  If p_web.GSV('job:Date_Completed') = 0
    loc:TabNumber += 1
  End
  If p_web.GSV('Hide:CChargeType') <> 1 Or p_web.GSV('Hide:WChargeType') <> 1 Or p_web.GSV('Hide:CRepairType') <> 1 Or p_web.GSV('Hide:WRepairType') <> 1
    loc:TabNumber += 1
  End
  If p_web.GSV('Hide:EstimateReady') = 0
    loc:TabNumber += 1
  End
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locEngineeringOption',locEngineeringOption)
  p_web.SetSessionValue('job:Order_Number',job:Order_Number)
  p_web.SetSessionValue('job:ESN',job:ESN)
  p_web.SetSessionValue('job:MSN',job:MSN)
  p_web.SetSessionValue('locJobType',locJobType)
  p_web.SetSessionValue('job:Charge_Type',job:Charge_Type)
  p_web.SetSessionValue('job:Repair_Type',job:Repair_Type)
  p_web.SetSessionValue('job:Warranty_Charge_Type',job:Warranty_Charge_Type)
  p_web.SetSessionValue('job:Repair_Type_Warranty',job:Repair_Type_Warranty)
  p_web.SetSessionValue('job:Current_Status',job:Current_Status)
  p_web.SetSessionValue('job:Location',job:Location)
  p_web.SetSessionValue('job:Exchange_Status',job:Exchange_Status)
  p_web.SetSessionValue('job:Loan_Status',job:Loan_Status)
  p_web.SetSessionValue('jbn:Fault_Description',jbn:Fault_Description)
  p_web.SetSessionValue('jbn:Engineers_Notes',jbn:Engineers_Notes)
  p_web.SetSessionValue('locHOClaimStatus',locHOClaimStatus)
  p_web.SetSessionValue('locRRCClaimStatus',locRRCClaimStatus)
  p_web.SetSessionValue('locCurrentEngineer',locCurrentEngineer)
  p_web.SetSessionValue('jobe:Network',jobe:Network)
  p_web.SetSessionValue('job:Authority_Number',job:Authority_Number)
  p_web.SetSessionValue('job:Unit_Type',job:Unit_Type)
  p_web.SetSessionValue('locMobileLifetimeValue',locMobileLifetimeValue)
  p_web.SetSessionValue('locMobileAverageSpend',locMobileAverageSpend)
  p_web.SetSessionValue('locMobileLoyaltyStatus',locMobileLoyaltyStatus)
  p_web.SetSessionValue('locMobileUpgradeDate',locMobileUpgradeDate)
  p_web.SetSessionValue('locMobileIDNumber',locMobileIDNumber)
  p_web.SetSessionValue('locCompleteRepair',locCompleteRepair)
  p_web.SetSessionValue('locCChargeTypeReason',locCChargeTypeReason)
  p_web.SetSessionValue('locCRepairTypeReason',locCRepairTypeReason)
  p_web.SetSessionValue('locWChargeTypeReason',locWChargeTypeReason)
  p_web.SetSessionValue('locWRepairTypeReason',locWRepairTypeReason)

RestoreMem       Routine
  !FormSource=File
  if p_web.IfExistsValue('locEngineeringOption')
    locEngineeringOption = p_web.GetValue('locEngineeringOption')
    p_web.SetSessionValue('locEngineeringOption',locEngineeringOption)
  End
  if p_web.IfExistsValue('job:Order_Number')
    job:Order_Number = p_web.GetValue('job:Order_Number')
    p_web.SetSessionValue('job:Order_Number',job:Order_Number)
  End
  if p_web.IfExistsValue('job:ESN')
    job:ESN = p_web.GetValue('job:ESN')
    p_web.SetSessionValue('job:ESN',job:ESN)
  End
  if p_web.IfExistsValue('job:MSN')
    job:MSN = p_web.GetValue('job:MSN')
    p_web.SetSessionValue('job:MSN',job:MSN)
  End
  if p_web.IfExistsValue('locJobType')
    locJobType = p_web.GetValue('locJobType')
    p_web.SetSessionValue('locJobType',locJobType)
  End
  if p_web.IfExistsValue('job:Charge_Type')
    job:Charge_Type = p_web.GetValue('job:Charge_Type')
    p_web.SetSessionValue('job:Charge_Type',job:Charge_Type)
  End
  if p_web.IfExistsValue('job:Repair_Type')
    job:Repair_Type = p_web.GetValue('job:Repair_Type')
    p_web.SetSessionValue('job:Repair_Type',job:Repair_Type)
  End
  if p_web.IfExistsValue('job:Warranty_Charge_Type')
    job:Warranty_Charge_Type = p_web.GetValue('job:Warranty_Charge_Type')
    p_web.SetSessionValue('job:Warranty_Charge_Type',job:Warranty_Charge_Type)
  End
  if p_web.IfExistsValue('job:Repair_Type_Warranty')
    job:Repair_Type_Warranty = p_web.GetValue('job:Repair_Type_Warranty')
    p_web.SetSessionValue('job:Repair_Type_Warranty',job:Repair_Type_Warranty)
  End
  if p_web.IfExistsValue('job:Current_Status')
    job:Current_Status = p_web.GetValue('job:Current_Status')
    p_web.SetSessionValue('job:Current_Status',job:Current_Status)
  End
  if p_web.IfExistsValue('job:Location')
    job:Location = p_web.GetValue('job:Location')
    p_web.SetSessionValue('job:Location',job:Location)
  End
  if p_web.IfExistsValue('job:Exchange_Status')
    job:Exchange_Status = p_web.GetValue('job:Exchange_Status')
    p_web.SetSessionValue('job:Exchange_Status',job:Exchange_Status)
  End
  if p_web.IfExistsValue('job:Loan_Status')
    job:Loan_Status = p_web.GetValue('job:Loan_Status')
    p_web.SetSessionValue('job:Loan_Status',job:Loan_Status)
  End
  if p_web.IfExistsValue('jbn:Fault_Description')
    jbn:Fault_Description = p_web.GetValue('jbn:Fault_Description')
    p_web.SetSessionValue('jbn:Fault_Description',jbn:Fault_Description)
  End
  if p_web.IfExistsValue('jbn:Engineers_Notes')
    jbn:Engineers_Notes = p_web.GetValue('jbn:Engineers_Notes')
    p_web.SetSessionValue('jbn:Engineers_Notes',jbn:Engineers_Notes)
  End
  if p_web.IfExistsValue('locHOClaimStatus')
    locHOClaimStatus = p_web.GetValue('locHOClaimStatus')
    p_web.SetSessionValue('locHOClaimStatus',locHOClaimStatus)
  End
  if p_web.IfExistsValue('locRRCClaimStatus')
    locRRCClaimStatus = p_web.GetValue('locRRCClaimStatus')
    p_web.SetSessionValue('locRRCClaimStatus',locRRCClaimStatus)
  End
  if p_web.IfExistsValue('locCurrentEngineer')
    locCurrentEngineer = p_web.GetValue('locCurrentEngineer')
    p_web.SetSessionValue('locCurrentEngineer',locCurrentEngineer)
  End
  if p_web.IfExistsValue('jobe:Network')
    jobe:Network = p_web.GetValue('jobe:Network')
    p_web.SetSessionValue('jobe:Network',jobe:Network)
  End
  if p_web.IfExistsValue('job:Authority_Number')
    job:Authority_Number = p_web.GetValue('job:Authority_Number')
    p_web.SetSessionValue('job:Authority_Number',job:Authority_Number)
  End
  if p_web.IfExistsValue('job:Unit_Type')
    job:Unit_Type = p_web.GetValue('job:Unit_Type')
    p_web.SetSessionValue('job:Unit_Type',job:Unit_Type)
  End
  if p_web.IfExistsValue('locMobileLifetimeValue')
    locMobileLifetimeValue = p_web.GetValue('locMobileLifetimeValue')
    p_web.SetSessionValue('locMobileLifetimeValue',locMobileLifetimeValue)
  End
  if p_web.IfExistsValue('locMobileAverageSpend')
    locMobileAverageSpend = p_web.GetValue('locMobileAverageSpend')
    p_web.SetSessionValue('locMobileAverageSpend',locMobileAverageSpend)
  End
  if p_web.IfExistsValue('locMobileLoyaltyStatus')
    locMobileLoyaltyStatus = p_web.GetValue('locMobileLoyaltyStatus')
    p_web.SetSessionValue('locMobileLoyaltyStatus',locMobileLoyaltyStatus)
  End
  if p_web.IfExistsValue('locMobileUpgradeDate')
    locMobileUpgradeDate = p_web.GetValue('locMobileUpgradeDate')
    p_web.SetSessionValue('locMobileUpgradeDate',locMobileUpgradeDate)
  End
  if p_web.IfExistsValue('locMobileIDNumber')
    locMobileIDNumber = p_web.GetValue('locMobileIDNumber')
    p_web.SetSessionValue('locMobileIDNumber',locMobileIDNumber)
  End
  if p_web.IfExistsValue('locCompleteRepair')
    locCompleteRepair = p_web.GetValue('locCompleteRepair')
    p_web.SetSessionValue('locCompleteRepair',locCompleteRepair)
  End
  if p_web.IfExistsValue('locCChargeTypeReason')
    locCChargeTypeReason = p_web.GetValue('locCChargeTypeReason')
    p_web.SetSessionValue('locCChargeTypeReason',locCChargeTypeReason)
  End
  if p_web.IfExistsValue('locCRepairTypeReason')
    locCRepairTypeReason = p_web.GetValue('locCRepairTypeReason')
    p_web.SetSessionValue('locCRepairTypeReason',locCRepairTypeReason)
  End
  if p_web.IfExistsValue('locWChargeTypeReason')
    locWChargeTypeReason = p_web.GetValue('locWChargeTypeReason')
    p_web.SetSessionValue('locWChargeTypeReason',locWChargeTypeReason)
  End
  if p_web.IfExistsValue('locWRepairTypeReason')
    locWRepairTypeReason = p_web.GetValue('locWRepairTypeReason')
    p_web.SetSessionValue('locWRepairTypeReason',locWRepairTypeReason)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('ViewJob_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  IF (p_web.IfExistsValue('ViewOnly'))
      p_web.StoreValue('ViewOnly')
  END
  
      
      if (p_web.GSV('SecondTime') = 0)
          Access:JOBS.Clearkey(job:Ref_Number_Key)
          job:Ref_Number    = p_web.GSV('wob:RefNumber')
          if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
              ! Found
              if (jobInUse(p_web.sessionid) = 1)
                  ShowALert('This job is use by another station. Click ''OK'' to view the job')
                  p_web.SSV('Job:ViewOnly',1)
                  
              end !if (jobInUse() = 1)            
              p_web.FileToSessionQueue(JOBS)
          else ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
              ! Error
          end ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
          
          ! If passed view only flag, make job view only
          IF (p_web.GSV('ViewOnly') = 1)
              p_web.SSV('Job:ViewOnly',1)
          END
          
  
          Access:JOBSE.Clearkey(jobe:RefNumberKey)
          jobe:RefNumber    = p_web.GSV('wob:RefNumber')
          if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
              ! Found
              p_web.FileToSessionQueue(JOBSE)
          else ! if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
              ! Error
          end ! if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
  
  
          Access:JOBSE2.Clearkey(jobe2:RefNumberKey)
          jobe2:RefNumber    = p_web.GSV('wob:RefNumber')
          if (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
              ! Found
              p_web.FileToSessionQueue(JOBSE2)
          else ! if (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
              ! Error
              if (Access:JOBSE2.PrimeRecord() = Level:Benign)
                  jobe2:refNumber    = p_web.GSV('wob:RefNumber')
                  if (Access:JOBSE2.TryInsert() = Level:Benign)
                      ! Inserted
                      p_web.FileToSessionQueue(JOBSE2)
                  else ! if (Access:JOBSE2.TryInsert() = Level:Benign)
                      ! Error
                      Access:JOBSE2.CancelAutoInc()
                  end ! if (Access:JOBSE2.TryInsert() = Level:Benign)
              end ! if (Access:JOBSE2.PrimeRecord() = Level:Benign)
          end ! if (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
  
  
          Access:JOBNOTES.Clearkey(jbn:RefNumberKey)
          jbn:RefNumber    = p_web.GSV('wob:RefNumber')
          if (Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign)
              ! Found
              p_web.FileToSessionQueue(JOBNOTES)
              
          else ! if (Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign)
              ! Error
              if (Access:JOBNOTES.PrimeRecord() = Level:Benign)
                  jbn:refNumber    = p_web.GSV('wob:RefNumber')
                  if (Access:JOBNOTES.TryInsert() = Level:Benign)
                      ! Inserted
                      p_web.FileToSessionQueue(JOBNOTES)
                  else ! if (Access:JOBNOTES.TryInsert() = Level:Benign)
                      ! Error
                  end ! if (Access:JOBNOTES.TryInsert() = Level:Benign)
              end ! if (Access:JOBNOTES.PrimeRecord() = Level:Benign)
          end ! if (Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign)
  
          p_web.SSV('tmp:TheJobAccessory','')
          Access:JOBACC.Clearkey(jac:ref_Number_Key)
          jac:Ref_Number    = p_web.GSV('job:Ref_Number')
          set(jac:ref_Number_Key,jac:ref_Number_Key)
          loop
              if (Access:JOBACC.Next())
                  Break
              end ! if (Access:JOBACC.Next())
              if (jac:Ref_Number    <> p_web.GSV('job:Ref_Number'))
                  Break
              end ! if (jac:Ref_Number    <> p_web.GSV('job:Ref_Number'))
              p_web.SSV('tmp:TheJobAccessory',p_web.GSV('tmp:TheJobAccessory') & ';|;' & jac:Accessory)
          end ! loop
          !p_web.SSV('tmp:TheJobAccessory',clip(sub(p_web.GSV('tmp:TheJobAccessory'),3,1000)) & ':|;')
          p_web.SSV('tmp:TheJobAccessory',clip(sub(p_web.GSV('tmp:TheJobAccessory'),2,1000)) & ';|;')
  
  
          case p_web.GSV('jobe:Booking48HourOption')
          of 1
              p_web.SSV('locBookingOption','48 Hour Exchange')
          of 2
              p_web.SSV('locBookingOption','ARC Repair')
          of 3
              p_web.SSV('locBookingOption','7 Day TAT')
          OF 4
              p_web.SSV('locBookingOption','Liquid Damage')
          Else
              p_web.SSV('locBookingOption','Not Set')
          end ! case (jobe:Booking48HourOption)
  
          case p_web.GSV('jobe:Engineer48HourOption')
          of 1
              p_web.SSV('locEngineeringOption','48 Hour Exchange' )
          of 2
              p_web.SSV('locEngineeringOption','ARC Repair')
          of 3
              p_web.SSV('locEngineeringOption','7 Day TAT')
          of 4
              p_web.SSV('locEngineeringOption','Standard Repair')
          Else
              p_web.SSV('locEngineeringOption','Not Set')
          end ! case (jobe:Booking48HourOption)
  
          p_web.SSV('SecondTime',1)
  
          if (p_web.GSV('Job:ViewOnly') <> 1)
              if (p_web.GSV('BookingSite') = 'RRC' And |
                  ((p_web.GSV('job:Location') <> p_web.GSV('Default:RRCLocation') And |
                  p_web.GSV('job:Location') <> p_web.GSV('Default:DespatchToCustomer') And |
                  p_web.GSV('job:Location') <> p_web.GSV('Default:InTransitPUP') And |
                  p_web.GSV('job:Location') <> p_web.GSV('Default:PUPLocation')) Or |
                  (p_web.GSV('jobe:HubRepair') = 1) Or |
                  (p_web.GSV('wob:HeadAccountNumber') <> p_web.GSV('BookingAccount'))))
  
                  ShowAlert('This job is not in this RRC''s control and cannot be amended.')
  
                  p_web.SSV('Job:ViewOnly',1)
              end ! if
          end ! if (p_web.GSV('Job:ViewOnly') <> 1)
          
          SentToHub(p_web)
          
          IF (p_web.GSV('job:warranty_job') = 'YES' AND p_web.GSV('wob:EDI') <> 'XXX')
              IF (p_web.GSV('job:date_Completed') > 0 AND p_web.GSV('SentToHub') = 0)
                  IF (p_web.GSV('wob:EDI') = 'NO' OR p_web.GSV('wob:EDI') = 'YES' OR p_web.GSV('wob:EDI') = 'PAY' OR p_web.GSV('wob:EDI') = 'APP')
                      p_web.SSV('Job:ViewOnly',1)
                  ELSE
                       ! Rejected warranty is handeled outsite this screen
                  END
              END
          END
          IF (p_web.GSV('Job:ViewOnly') <> 1)
              IF (p_web.GSV('BookingSite') = 'RRC' AND p_web.GSV('SentToHub') = 1)
                  if (securitycheckfailed(p_web.GSV('BookingUserPassword'),'JOBS - RRC AMEND ARC JOB'))
                      p_web.SSV('Hide:ButtonAllocateEngineer',1)
                      p_web.SSV('Hide:ButtonCreateInvoice',1)
                      p_web.SSV('Hide:ValidatePOP',1)
                      p_web.SSV('Job:ViewOnly',1)
                  end
              END
          END
          
  
      Access:MANUFACT.Clearkey(man:Manufacturer_Key)
      man:Manufacturer    = p_web.GSV('job:Manufacturer')
      if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
              ! Found
              ! Show/Hide Adjustment Buttons (DBH: 10/06/2009)
          if (man:ForceParts)
              p_web.SSV('Hide:ChargeableAdjustment',0)
              p_web.SSV('Hide:WarrantyAdjustment',0)
          else ! if (man:ForceParts)
              p_web.SSV('Hide:ChargeableAdjustment',1)
              p_web.SSV('Hide:WarrantyAdjustment',1)
          end ! if (man:ForceParts)
      else ! if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
              ! Error
      end ! if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
  
          ! If job invoiced, then don't allow adjustment
      if (jobInvoiced(p_web.GSV('wob:RefNumber'),p_web.GSV('BookingSite')))
          p_web.SSV('hide:ChargeableAdjustment',1)
      end ! if (jobInvoice(p_web.GSV('wob:RefNumber'),p_web.GSV('BookingSite'))
  
      if (p_web.GSV('job:Invoice_Number_Warranty') > 0)
          p_web.SSV('hide:WarrantyAdjustment',1)
      end ! if (p_web.GSV('job:Invoice_Number_Warranty') > 0)
  
      do saveFields
  
  
      p_web.SSV('Hide:CChargeType',1)
      p_web.SSV('Hide:WChargeType',1)
      p_web.SSV('Hide:CRepairType',1)
      p_web.SSV('Hide:WRepairType',1)
      p_web.SSV('locCChargeTypeReason','')
      p_web.SSV('locWChargeTypeReason','')
      p_web.SSV('locCRepairTypeReason','')
      p_web.SSV('locWChargeTypeReason','')
  
      p_web.SSV('Hide:EstimateQuery',1)
      p_web.SSV('Hide:EstimateReady',1)
  
      end ! if (p_web.GSV('FirstTime') = 1)
  
      p_web.SSV('textBouncer','')
  
      countHistory(p_web)
  
      if (p_web.GSV('CountHistory') > 0)
          p_web.SSV('textBouncer','There are ' & p_web.GSV('CountHistory') & ' Previous Jobs')
      end ! if (x# > 0)
  
      ! Set URL
      IF (p_web.GSV('Job:ViewOnly') = 1)
          p_web.SSV('URL:ViewCosts','ViewCosts')
      ELSE
          p_web.ssv('URL:ViewCosts','BillingConfirmation')
      END
  
  
      IF (p_web.GSV('Job:ViewOnly') <> 1)
          lockRecord(job:Ref_Number,p_web.sessionid,0) ! Job Not Read Only. Add A Lock
      END
      ! Return to the browse, unless called from somewhere else
      p_web.SSV('locViewJobURL','BrowseWebmasterJobs')
      if (p_web.ifexistsvalue('BackURL'))
          p_web.SSV('locViewJobURL',p_web.getvalue('BackURL'))
      end ! if (p_web.ifexistsvalue('locViewJobNextURL'))
  
      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
      tra:Account_Number    = p_web.GSV('wob:HeadAccountNumber')
      if (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
          ! Found
          p_web.SSV('locJobNumber',p_web.GSV('wob:RefNumber') & '-' & clip(tra:BranchIdentification) & p_web.GSV('wob:JobNumber'))
          
          p_web.SSV('JobBookingSiteLocation',tra:SiteLocation)
      else ! if (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
          ! Error
      end ! if (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
  
      Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
      sub:Account_Number    = p_web.GSV('job:Account_Number')
      if (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
          ! Found
          p_web.SSV('locTradeAccount',clip(sub:Account_Number) & ' (' & clip(sub:Company_Name) & ')')
      else ! if (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
          ! Error
      end ! if (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
  
      if (p_web.GSV('job:Exchange_Unit_Number') > 0)
          if (p_web.GSV('jobe:ExchangedAtRRC') = 1)
              p_web.SSV('locExchangeText','RRC Exchange Unit Attached')
          else ! if (p_web.GSV('jobe:ExchangedAtRRC') = 1)
              p_web.SSV('locExchangeText','ARC Exchange Unit Attached')
          end ! if (p_web.GSV('jobe:ExchangedAtRRC') = 1)
      else ! if (job:Exchange_Unit_Number > 0)
          p_web.SSV('locExchangeText','Not Issued')
      end ! if (job:Exchange_Unit_Number > 0)
  
      if (p_web.GSV('job:Loan_Unit_Number') > 0)
          p_web.SSV('locLoanText','Loan Unit Attached')
      else ! if (p_web.GSV('job:Loan_Unit_Number') > 0)
          p_web.SSV('locLoanText','Not Issued')
      end ! if (p_web.GSV('job:Loan_Unit_Number') > 0)
  
  
      Access:USERS.Clearkey(use:User_Code_Key)
      use:User_Code    = p_web.GSV('job:Engineer')
      if (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
          ! Found
          p_web.SSV('locCurrentEngineer',clip(use:Forename) & ' ' & clip(use:Surname))
  
  
          Access:JOBSENG.Clearkey(joe:UserCodeKey)
          joe:JobNumber    = p_web.GSV('wob:RefNumber')
          joe:UserCode    = use:User_Code
          joe:DateAllocated    = Today()
          set(joe:UserCodeKey,joe:UserCodeKey)
          loop
              if (Access:JOBSENG.Previous())
                  Break
              end ! if (Access:JOBSENG.Next())
              if (joe:JobNumber    <> p_web.GSV('wob:RefNumber'))
                  Break
              end ! if (joe:JobNumber    <> p_web.GSV('wob:RefNumber'))
              if (joe:UserCode    <> use:User_Code)
                  Break
              end ! if (joe:UserCode    <> use:User_Code)
              p_web.SSV('locEngineerAllocated',Format(joe:DateAllocated,@d06b) & '  (Eng Level: ' & joe:EngSkillLevel & ')')
              Break
          end ! loop
  
      else ! if (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
          ! Error
          p_web.SSV('locCurrentEngineer','Not Allocated')
      end ! if (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
  
  
      p_web.SSV('FormEngineeringOption:FirstTime',1)
  
      p_web.SSV('hide:EngOptionButton',0)
      ! Do not allow an ARC engineer to change an RRC's options (DBH: 24-03-2005)
      If p_web.GSV('jobe:WebJob') = 1 And p_web.GSV('BookingSite') <> 'RRC'
          p_web.SSV('hide:EngOptionButton',1)
      End ! jobe:WebJob And ~job:WebJob
  
      ! If completed, do not allow to change engineer option (DBH: 24-03-2005)
      If p_web.GSV('job:Date_Completed') > 0
          p_web.SSV('hide:EngOptionButton',1)
      End ! job:Date_Completed <> ''
  
      IF (p_web.GSV('job:Warranty_Job') = 'YES')
          IF (p_web.GSV('job:Chargeable_Job') = 'YES')
              p_web.SSV('locJobType','Warranty / Chargeable')
          ELSE
              p_web.SSV('locJobType','Warranty')
          END
      ELSE
          p_web.SSV('locJobType','Chargeable')
      END
  
  
      do SetStatusAccessLevelFilter
  !Security Checks
      p_web.SSV('Hide:ExchangeButton',0)
      ! No access to add exchange. Hide button if none attached
      if (p_web.GSV('job:Exchange_Unit_Number') = 0)
          if (securityCheckFailed(p_web.GSV('BookingUserPassword'),'JOBS - ADD EXCHANGE UNIT'))
              p_web.SSV('Hide:ExchangeButton',1)
          end ! if (securityCheckFailed(p_web.GSV('BookingUserPassword'),' '))
      end ! if (p_web.GSV('job:Exchange_Unit_Number') > 0)
  
      p_web.SSV('Hide:LoanButton',0)
      if (p_web.GSV('job:Loan_Unit_Number') = 0)
          if (securityCheckFailed(p_web.GSV('BookingUserPassword'),'JOBS - ADD LOAN UNIT'))
              p_web.SSV('Hide:LoanButton',1)
          end ! if (securityCheckFailed(p_web.GSV('BookingUserPassword'),'JOBS - ADD LOAN UNIT'))
      end ! if (p_web.GSV('job:Loan_Unit_Number') = 0)
  
      p_web.SSV('Hide:ValidatePOP',0)
      if (securityCheckFailed(p_web.GSV('BookingUserPassword'),'JOBS - AMEND POP DETAILS'))
          p_web.SSV('Hide:ValidatePOP',1)
      end ! if (securityCheckFailed('Hide:ValidatePOP'),'JOBS - AMEND POP DETAILS')
  
      p_web.SSV('Hide:ResendXML',1)
      if (p_web.GSV('job:Workshop') <> 'YES' and p_web.GSV('job:Third_Party_Site') <> '')
          Access:TRDPARTY.Clearkey(trd:company_Name_Key)
          trd:company_Name    = job:Third_Party_Site
          if (Access:TRDPARTY.TryFetch(trd:company_Name_Key) = Level:Benign)
              ! Found
              if (trd:LHubAccount)
                  
  
                  Access:TRDBATCH.ClearKey(trb:JobStatusKey)
                  trb:Status = 'OUT'
                  trb:Ref_Number = p_web.GSV('job:Ref_Number ')
                  If Access:TRDBATCH.TryFetch(trb:JobStatusKey) = Level:Benign
                      !Found
                      p_web.SSV('Hide:ResendXML',0)
                  Else ! If Access:TRDBATCH.TryFetch(trb:JobStatusKey) = Level:Benign
                      !Error
                  End ! If Access:TRDBATCH.TryFetch(trb:JobStatusKey) = Level:Benign
              end ! if (trd:LHubAccount)
          else ! if (Access:TRDPARTY.TryFetch(trd:company_Name_Key) = Level:Benign)
              ! Error
          end ! if (Access:TRDPARTY.TryFetch(trd:company_Name_Key) = Level:Benign)
  
      end ! if (p_web.GSV('job:Workshop') <> 'YES' and p_web.GSV('job:Third_Party_Site') <> '')
  
      p_web.SSV('Hide:ReceiptFromPUP',1)
  
      if (securitycheckfailed(p_web.GSV('BookingUserPassword'),'JOBS - SHOW COSTS'))
          p_web.SSV('Hide:ButtonViewCosts',1)
      end
      if (securitycheckfailed(p_web.GSV('BookingUserPassword'),'RAPID ENG - AMEND FAULT CODES'))
          p_web.SSV('Hide:ButtonFaultCodes',1)
      end
      if (securitycheckfailed(p_web.GSV('BookingUserPassword'),'RAPID ENG - ACCESSORIES'))
          p_web.SSV('Hide:ButtonAccessories',1)
      end
      if (securitycheckfailed(p_web.GSV('BookingUserPassword'),'RAPID ENG - ALLOCATE JOB'))
          p_web.SSV('Hide:ButtonAllocateEngineer',1)
      end
      IF (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'RAPID ENG - CREATE INVOICE'))
          p_web.SSV('Hide:ButtonCreateInvoice',1)
      ELSE
          IF showHideCreateInvoice()
              p_web.SSV('Hide:ButtonCreateInvoice',1)
          ELSE
              p_web.SSV('Hide:ButtonCreateInvoice',0)
          END
          
      END
  
  
      ! Claim Status
      lochoclaimstatus = p_web.GSV('jobe:warrantyclaimstatus')
      locRRCclaimStatus = WOBEDIStatus(p_web.GSV('wob:EDI'))
  
      if (p_web.GSV('wob:EDI') = 'PAY')
          access:audit.clearkey(aud:action_key)
          aud:ref_number = p_web.GSV('job:ref_number')
          aud:action = 'WARRANTY CLAIM MARKED PAID (RRC)'
          set(aud:action_key,aud:action_key)
          loop
              if (access:audit.next())
                  break
              end
              if (aud:ref_number <> p_web.GSV('job:ref_Number'))
                  break
              end
              if (aud:action <> 'WARRANTY CLAIM MARKED PAID (RRC)')
                  break
              end
              lochoclaimstatus = clip(lochoclaimstatus) & ' (' & Format(aud:Date,@d6) & ')'
              break
          end
  
      end
      p_web.SSV('locHOClaimStatus',locHOClaimStatus)
      p_web.SSV('locRRCClaimStatus',locRRCClaimStatus)
                 
  
  !Completion Check
  if (p_web.GSV('JobCompleteProcess') = 1)
      do completeRepair
  else ! if (p_web.GSV('Job:CompleteProcess') = 1)
      p_web.SSV('textCompleteRepair','')
  end ! if (p_web.GSV('Job:CompleteProcess') = 1)
  ! Customer Collection
  IF (CustCollectionValidated(p_web.GSV('job:Ref_Number')))
      p_web.SSV('Hide:CustomerClassification',0)
      CustClassificationFields(p_web.GSV('job:Ref_Number'),locMobileLifetimeValue, |
          locMobileAverageSpend, | 
          locMobileLoyaltyStatus, |
          locMobileUpgradeDate, |
          locMobileIDNumber)
      p_web.SSV('locMobileLifetimeValue',locMobileLifetimeValue)
      p_web.SSV('locMobileAverageSpend',locMobileAverageSpend)
      p_web.SSV('locMobileLoyaltyStatus',locMobileLoyaltyStatus)
      p_web.SSV('locMobileUpgradeDate',FORMAT(locMobileUpgradeDate,@d06b))
      p_web.SSV('locMobileIDNumber',locMobileIDNumber)
      
  ELSE
      p_web.SSV('Hide:CustomerClassification',1)
  END
  ! Liquid Damage #10544
      IF (IsUnitLiquidDamaged(p_web.GSV('job:ESN'),p_web.GSV('job:Ref_Number')) = TRUE)
          p_web.SSV('Hide:LiquidDamage',0)
      ELSE
          p_web.SSV('Hide:LiquidDamage',1)
      END
  p_web.SSV('RepairTypesNoParts',vod.RepairTypesNoParts(p_web.GSV('job:Chargeable_Job'), |
      p_web.GSV('job:Warranty_job'), |
      p_web.GSV('job:Manufacturer'), |
      p_web.GSV('job:Repair_Type'), |
      p_web.GSV('job:Repair_Type_Warranty')))
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locEngineeringOption = p_web.RestoreValue('locEngineeringOption')
 locJobType = p_web.RestoreValue('locJobType')
 locHOClaimStatus = p_web.RestoreValue('locHOClaimStatus')
 locRRCClaimStatus = p_web.RestoreValue('locRRCClaimStatus')
 locCurrentEngineer = p_web.RestoreValue('locCurrentEngineer')
 locMobileLifetimeValue = p_web.RestoreValue('locMobileLifetimeValue')
 locMobileAverageSpend = p_web.RestoreValue('locMobileAverageSpend')
 locMobileLoyaltyStatus = p_web.RestoreValue('locMobileLoyaltyStatus')
 locMobileUpgradeDate = p_web.RestoreValue('locMobileUpgradeDate')
 locMobileIDNumber = p_web.RestoreValue('locMobileIDNumber')
 locCompleteRepair = p_web.RestoreValue('locCompleteRepair')
 locCChargeTypeReason = p_web.RestoreValue('locCChargeTypeReason')
 locCRepairTypeReason = p_web.RestoreValue('locCRepairTypeReason')
 locWChargeTypeReason = p_web.RestoreValue('locWChargeTypeReason')
 locWRepairTypeReason = p_web.RestoreValue('locWRepairTypeReason')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'JobSearch'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('ViewJob_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('ViewJob_ChainTo')
    loc:formaction = p_web.GetSessionValue('ViewJob_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'JobSearch'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
    loc:viewonly = Choose(p_web.GSV('Job:ViewOnly') = 1,1,0)
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  packet = clip(packet) & '<input type="hidden" name="WEBJOB__FileAction" value="'&p_web.getSessionValue('WEBJOB:FileAction')&'" ></input><13,10>'
  packet = clip(packet) & '<input type="hidden" name="file" value="WEBJOB" ></input><13,10>'
  packet = clip(packet) & '<input type="hidden" name="UpdateFile" value="WEBJOB" ></input><13,10>'
  packet = clip(packet) & '<input type="hidden" name="UpdateKey" value="wob:RecordNumberKey" ></input><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="ViewJob" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="ViewJob" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="ViewJob" ></input><13,10>'
  end

  do SendPacket
  packet = clip(packet) & p_web.CreateInput('hidden','wob:RecordNumber',p_web._jsok(p_web.getSessionValue('wob:RecordNumber'))) & '<13,10>'
  If p_web.Translate('Amend Job') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Amend Job',0)&'</span>'&CRLF
  End
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_ViewJob">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_ViewJob" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  do GenerateTab3
  do GenerateTab4
  do GenerateTab5
  do GenerateTab6
  do GenerateTab7
  do GenerateTab8
  do GenerateTab9
  do GenerateTab10
  do GenerateTab11
  do GenerateTab12
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_ViewJob')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Browse') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Job Details') & ''''
        If p_web.GSV('textBouncer') <> ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Bouncers') & ''''
        End
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Job Status') & ''''
        If p_web.GSV('job:Warranty_Job') = 'YES' and p_web.GSV('job:Date_Completed') > 0
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Claim Status') & ''''
        End
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Repair Details') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Attached Parts') & ''''
        If p_web.GSV('Hide:CustomerClassification') <> 1
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Customer Classification') & ''''
        End
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        If p_web.GSV('job:Date_Completed') = 0
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Complete Repair') & ''''
        End
        If p_web.GSV('Hide:CChargeType') <> 1 Or p_web.GSV('Hide:WChargeType') <> 1 Or p_web.GSV('Hide:CRepairType') <> 1 Or p_web.GSV('Hide:WRepairType') <> 1
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Enter Reason For The Following Field(s) Changing') & ''''
        End
        If p_web.GSV('Hide:EstimateReady') = 0
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Estimate Ready') & ''''
        End
        Loc:Tabnumber = p_web.getSessionValue('showtab_ViewJob')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_ViewJob'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('ViewJob_BrowseEstimateParts_embedded_div')&''');'
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('ViewJob_BrowseChargeableParts_embedded_div')&''');'
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('ViewJob_BrowseWarrantyParts_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('ViewJob_BrowseEstimateParts_embedded_div')&''');'
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('ViewJob_BrowseChargeableParts_embedded_div')&''');'
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('ViewJob_BrowseWarrantyParts_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('ViewJob_BrowseEstimateParts_embedded_div')&''');'
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('ViewJob_BrowseChargeableParts_embedded_div')&''');'
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('ViewJob_BrowseWarrantyParts_embedded_div')&''');'
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
    If upper(p_web.getvalue('LookupFile'))='ACCSTAT'
            p_web.SetValue('SelectField',clip(loc:formname) & '.jbn:Engineers_Notes')
    End
    If upper(p_web.getvalue('LookupFile'))='NETWORKS'
            p_web.SetValue('SelectField',clip(loc:formname) & '.job:Authority_Number')
    End
    If upper(p_web.getvalue('LookupFile'))='UNITTYPE'
      If p_web.GSV('Hide:CChargeType') <> 1 Or p_web.GSV('Hide:WChargeType') <> 1 Or p_web.GSV('Hide:CRepairType') <> 1 Or p_web.GSV('Hide:WRepairType') <> 1
        If p_web.GSV('Hide:CChargeType') <> 1
          If Not (p_web.GSV('Hide:CChargeType') = 1)
            p_web.SetValue('SelectField',clip(loc:formname) & '.locCChargeTypeReason')
          End
        End
      End
    End
  Else
    If False
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          If p_web.GSV('textBouncer') <> ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab7'''
          End
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          If p_web.GSV('job:Warranty_Job') = 'YES' and p_web.GSV('job:Date_Completed') > 0
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab14'''
          End
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab5'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab6'''
          If p_web.GSV('Hide:CustomerClassification') <> 1
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab15'''
          End
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab11'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab12'''
          If p_web.GSV('job:Date_Completed') = 0
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab8'''
          End
          If p_web.GSV('Hide:CChargeType') <> 1 Or p_web.GSV('Hide:WChargeType') <> 1 Or p_web.GSV('Hide:CRepairType') <> 1 Or p_web.GSV('Hide:WRepairType') <> 1
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab9'''
          End
          If p_web.GSV('Hide:EstimateReady') = 0
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab10'''
          End
          Loc:Tabnumber = p_web.getSessionValue('showtab_ViewJob')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
    if p_web.GSV('textBouncer') <> ''
      packet = clip(packet) & 'roundCorners(''tab7'');'&CRLF
    end
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
    if p_web.GSV('job:Warranty_Job') = 'YES' and p_web.GSV('job:Date_Completed') > 0
      packet = clip(packet) & 'roundCorners(''tab14'');'&CRLF
    end
      packet = clip(packet) & 'roundCorners(''tab5'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab6'');'&CRLF
    if p_web.GSV('Hide:CustomerClassification') <> 1
      packet = clip(packet) & 'roundCorners(''tab15'');'&CRLF
    end
      packet = clip(packet) & 'roundCorners(''tab11'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab12'');'&CRLF
    if p_web.GSV('job:Date_Completed') = 0
      packet = clip(packet) & 'roundCorners(''tab8'');'&CRLF
    end
    if p_web.GSV('Hide:CChargeType') <> 1 Or p_web.GSV('Hide:WChargeType') <> 1 Or p_web.GSV('Hide:CRepairType') <> 1 Or p_web.GSV('Hide:WRepairType') <> 1
      packet = clip(packet) & 'roundCorners(''tab9'');'&CRLF
    end
    if p_web.GSV('Hide:EstimateReady') = 0
      packet = clip(packet) & 'roundCorners(''tab10'');'&CRLF
    end
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Browse') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_ViewJob_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Browse')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Browse')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Browse')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Browse')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::link:AmendAddress
      do Comment::link:AmendAddress
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::link:AuditTrail
      do Comment::link:AuditTrail
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::link:EngineerHistory
      do Comment::link:EngineerHistory
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::link:StatusChanges
      do Comment::link:StatusChanges
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::link:BrowseLocationHistory
      do Comment::link:BrowseLocationHistory
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Job Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_ViewJob_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Job Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Job Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Job Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Job Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::text:jobNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::text:jobNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::text:jobNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::text:DateBooked
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::text:DateBooked
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::text:DateBooked
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locBookingOption
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locBookingOption
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locBookingOption
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locEngineeringOption
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locEngineeringOption
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locEngineeringOption
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::gap:JobDetails
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::gap:JobDetails
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::button:ChangeEngineeringOption
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::button:ChangeEngineeringOption
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::text:accountNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::text:accountNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::text:accountNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::text:Completed
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::text:Completed
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::text:Completed
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::text:modelDetails
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::text:modelDetails
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::text:modelDetails
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::text:HubRepair
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::text:HubRepair
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::text:HubRepair
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Order_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Order_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Order_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::text:OBFValidated
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::text:OBFValidated
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::text:OBFValidated
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:ESN
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:ESN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:ESN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:MSN
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:MSN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:MSN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locJobType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locJobType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locJobType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::button:AmendJobType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::button:AmendJobType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    If p_web.GSV('job:Chargeable_Job') = 'YES'
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Charge_Type
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Charge_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Charge_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    end
    do SendPacket
    If p_web.GSV('job:Chargeable_Job') = 'YES'
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Repair_Type
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Repair_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Repair_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('job:Warranty_Job') = 'YES'
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Warranty_Charge_Type
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Warranty_Charge_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Warranty_Charge_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    end
    do SendPacket
    If p_web.GSV('job:Warranty_Job') = 'YES'
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Repair_Type_Warranty
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Repair_Type_Warranty
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Repair_Type_Warranty
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab2  Routine
  If p_web.GSV('textBouncer') <> ''
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel7">'&CRLF &|
                                    '  <div id="panel7Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Bouncers') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel7Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_ViewJob_7">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Bouncers')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab7" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab7">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Bouncers')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab7">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Bouncers')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab7">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Bouncers')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::textBouncer
      do Comment::textBouncer
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonPreviousUnitHistory
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonPreviousUnitHistory
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab3  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Job Status') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_ViewJob_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Job Status')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Job Status')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Job Status')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Job Status')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Current_Status
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Current_Status
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Current_Status
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Location
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Location
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Location
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::text:ExchangeUnit
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::text:ExchangeUnit
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::text:ExchangeUnit
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::text:LoanUnit
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::text:LoanUnit
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::text:LoanUnit
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Exchange_Status
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Exchange_Status
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Exchange_Status
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Loan_Status
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Loan_Status
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Loan_Status
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    If p_web.GSV('jobe:SecondExchangeNumber') > 0
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::text:SecondExchangeUnit
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::text:SecondExchangeUnit
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::text:SecondExchangeUnit
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jbn:Fault_Description
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jbn:Fault_Description
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jbn:Fault_Description
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jbn:Engineers_Notes
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jbn:Engineers_Notes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jbn:Engineers_Notes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::gap
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::gap
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::button:EngineersNotes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::button:EngineersNotes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab4  Routine
  If p_web.GSV('job:Warranty_Job') = 'YES' and p_web.GSV('job:Date_Completed') > 0
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel14">'&CRLF &|
                                    '  <div id="panel14Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Claim Status') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel14Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_ViewJob_14">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Claim Status')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab14" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab14">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Claim Status')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab14">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Claim Status')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab14">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Claim Status')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locHOClaimStatus
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locHOClaimStatus
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locHOClaimStatus
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locRRCClaimStatus
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locRRCClaimStatus
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locRRCClaimStatus
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab5  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel5">'&CRLF &|
                                    '  <div id="panel5Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Repair Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel5Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_ViewJob_5">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Repair Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab5" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab5">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Repair Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab5">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Repair Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab5">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Repair Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
    If p_web.GSV('Hide:LiquidDamage') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::textLiquidDamage
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::textLiquidDamage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::textLiquidDamage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locCurrentEngineer
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locCurrentEngineer
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locCurrentEngineer
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locEngineerAllocated
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locEngineerAllocated
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locEngineerAllocated
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe:Network
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe:Network
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe:Network
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Authority_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Authority_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Authority_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Unit_Type
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Unit_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Unit_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab6  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel6">'&CRLF &|
                                    '  <div id="panel6Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Attached Parts') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel6Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_ViewJob_6">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Attached Parts')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab6" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab6" class="'&clip('FormCentreFixed')&'">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Attached Parts')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab6" class="'&clip('FormCentreFixed')&'">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Attached Parts')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab6">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Attached Parts')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
    If p_web.GSV('RepairTypesNoParts') = 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::locRepairTypesNoParts
      do Comment::locRepairTypesNoParts
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If (p_web.GSV('job:Estimate') = 'YES' and p_web.GSV('job:Estimate_Accepted') <> 'YES' and p_web.GSV('job:Estimate_Rejected') <> 'YES') AND p_web.GSV('RepairTypesNoParts') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::ViewEstimateParts
      do Comment::ViewEstimateParts
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    end
    do SendPacket
    If (p_web.GSV('job:Estimate') <> 'YES' or (p_web.GSV('job:Estimate') = 'YES' and (p_web.GSV('job:Estimate_Accepted') = 'YES' or p_web.GSV('job:Estimate_Rejected') = 'YES'))) AND p_web.GSV('RepairTypesNoParts') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::browse:ChargeableParts
      do Comment::browse:ChargeableParts
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    end
    do SendPacket
    If p_web.GSV('RepairTypesNoParts') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::browseWarrantyParts
      do Comment::browseWarrantyParts
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab7  Routine
  If p_web.GSV('Hide:CustomerClassification') <> 1
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel15">'&CRLF &|
                                    '  <div id="panel15Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Customer Classification') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel15Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_ViewJob_15">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Customer Classification')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab15" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab15">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Customer Classification')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab15">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Customer Classification')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab15">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Customer Classification')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locMobileLifetimeValue
      do Value::locMobileLifetimeValue
      do Comment::locMobileLifetimeValue
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locMobileAverageSpend
      do Value::locMobileAverageSpend
      do Comment::locMobileAverageSpend
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locMobileLoyaltyStatus
      do Value::locMobileLoyaltyStatus
      do Comment::locMobileLoyaltyStatus
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locMobileUpgradeDate
      do Value::locMobileUpgradeDate
      do Comment::locMobileUpgradeDate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locMobileIDNumber
      do Value::locMobileIDNumber
      do Comment::locMobileIDNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab8  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel11">'&CRLF &|
                                    '  <div id="panel11Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel11Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_ViewJob_11">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab11" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab11">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab11">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab11">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
    If p_web.GSV('Job:ViewOnly') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::buttonAllocateEngineer
      do Comment::buttonAllocateEngineer
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    end
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::buttonAccessories
      do Comment::buttonAccessories
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::buttonFaultCodes
      do Comment::buttonFaultCodes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
    If p_web.GSV('Job:ViewOnly') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::buttonEstimate
      do Comment::buttonEstimate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    end
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::buttonContactHistory
      do Comment::buttonContactHistory
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    If p_web.GSV('Job:ViewOnly') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::buttonAllocateExchange
      do Comment::buttonAllocateExchange
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    end
    do SendPacket
    If p_web.GSV('Job:ViewOnly') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::buttonAllocateLoan
      do Comment::buttonAllocateLoan
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    end
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::buttonCreateInvoice
      do Comment::buttonCreateInvoice
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::buttonViewCosts
      do Comment::buttonViewCosts
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab9  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel12">'&CRLF &|
                                    '  <div id="panel12Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel12Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_ViewJob_12">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab12" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab12">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab12">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab12">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
    If p_web.GSV('Job:ViewOnly') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::buttonValidatePOP
      do Comment::buttonValidatePOP
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    end
    do SendPacket
    If p_web.GSV('Job:ViewOnly') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::buttonResendXML
      do Comment::buttonResendXML
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    end
    do SendPacket
    If p_web.GSV('job:Who_Booked') = 'WEB'
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::buttonReceiptFromPUP
      do Comment::buttonReceiptFromPUP
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab10  Routine
  If p_web.GSV('job:Date_Completed') = 0
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel8">'&CRLF &|
                                    '  <div id="panel8Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Complete Repair') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel8Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_ViewJob_8">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Complete Repair')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab8" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab8">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Complete Repair')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab8">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Complete Repair')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab8">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Complete Repair')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::buttonCompleteRepair
      do Comment::buttonCompleteRepair
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locCompleteRepair
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locCompleteRepair
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::textCompleteRepair
      do Comment::textCompleteRepair
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab11  Routine
  If p_web.GSV('Hide:CChargeType') <> 1 Or p_web.GSV('Hide:WChargeType') <> 1 Or p_web.GSV('Hide:CRepairType') <> 1 Or p_web.GSV('Hide:WRepairType') <> 1
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel9">'&CRLF &|
                                    '  <div id="panel9Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Enter Reason For The Following Field(s) Changing') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel9Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_ViewJob_9">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Enter Reason For The Following Field(s) Changing')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab9" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab9">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Enter Reason For The Following Field(s) Changing')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab9">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Enter Reason For The Following Field(s) Changing')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab9">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Enter Reason For The Following Field(s) Changing')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
    If p_web.GSV('Hide:CChargeType') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locCChargeTypeReason
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locCChargeTypeReason
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locCChargeTypeReason
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    end
    do SendPacket
    If p_web.GSV('Hide:CRepairType') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locCRepairTypeReason
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locCRepairTypeReason
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locCRepairTypeReason
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('Hide:WChargeType') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locWChargeTypeReason
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locWChargeTypeReason
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locWChargeTypeReason
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    end
    do SendPacket
    If p_web.GSV('Hide:WRepairType') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locWRepairTypeReason
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locWRepairTypeReason
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locWRepairTypeReason
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab12  Routine
  If p_web.GSV('Hide:EstimateReady') = 0
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel10">'&CRLF &|
                                    '  <div id="panel10Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Estimate Ready') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel10Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_ViewJob_10">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Estimate Ready')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab10" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab10">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Estimate Ready')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab10">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Estimate Ready')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab10">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Estimate Ready')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::textEstimateReady
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::textEstimateReady
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonPrintEstimate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonPrintEstimate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end


Validate::link:AmendAddress  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('link:AmendAddress',p_web.GetValue('NewValue'))
    do Value::link:AmendAddress
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::link:AmendAddress  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('link:AmendAddress') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web.CreateHyperLink(p_web.Translate('Amend Addresses'),'AmendAddress?FromURL=ViewJob',,,loc:javascript,,0) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::link:AmendAddress  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('link:AmendAddress') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::link:AuditTrail  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('link:AuditTrail',p_web.GetValue('NewValue'))
    do Value::link:AuditTrail
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::link:AuditTrail  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('link:AuditTrail') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web.CreateHyperLink(p_web.Translate('Audit Trail'),'BrowseAuditFilter','_self',,loc:javascript,,0) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::link:AuditTrail  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('link:AuditTrail') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::link:EngineerHistory  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('link:EngineerHistory',p_web.GetValue('NewValue'))
    do Value::link:EngineerHistory
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::link:EngineerHistory  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('link:EngineerHistory') & '_value',Choose(p_web.GSV('Hide:EngineerHistory') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:EngineerHistory') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web.CreateHyperLink(p_web.Translate('Engineer History'),'FormBrowseEngineerHistory','_self',,loc:javascript,,0) & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::link:EngineerHistory  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('link:EngineerHistory') & '_comment',Choose(p_web.GSV('Hide:EngineerHistory') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:EngineerHistory') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::link:StatusChanges  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('link:StatusChanges',p_web.GetValue('NewValue'))
    do Value::link:StatusChanges
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::link:StatusChanges  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('link:StatusChanges') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web.CreateHyperLink(p_web.Translate('Status Changes'),'BrowseStatusFilter','_self',,loc:javascript,,0) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::link:StatusChanges  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('link:StatusChanges') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::link:BrowseLocationHistory  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('link:BrowseLocationHistory',p_web.GetValue('NewValue'))
    do Value::link:BrowseLocationHistory
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::link:BrowseLocationHistory  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('link:BrowseLocationHistory') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web.CreateHyperLink(p_web.Translate('Location History'),'FormBrowseLocationHistory','_self',,loc:javascript,,0) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::link:BrowseLocationHistory  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('link:BrowseLocationHistory') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::text:jobNumber  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('text:jobNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Job Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::text:jobNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('text:jobNumber',p_web.GetValue('NewValue'))
    do Value::text:jobNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::text:jobNumber  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('text:jobNumber') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate(p_web.GSV('locJobNumber'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::text:jobNumber  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('text:jobNumber') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::text:DateBooked  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('text:DateBooked') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Booked')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::text:DateBooked  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('text:DateBooked',p_web.GetValue('NewValue'))
    do Value::text:DateBooked
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::text:DateBooked  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('text:DateBooked') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate(Format(p_web.GSV('job:Date_Booked'),@d06b) & ' ' & Format(p_web.GSV('job:Time_Booked'),@T01) & ' (' & p_web.GSV('job:Who_Booked') & ')',) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::text:DateBooked  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('text:DateBooked') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locBookingOption  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('locBookingOption') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Booking Option')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locBookingOption  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locBookingOption',p_web.GetValue('NewValue'))
    do Value::locBookingOption
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::locBookingOption  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('locBookingOption') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate(p_web.GSV('locBookingOption'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::locBookingOption  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('locBookingOption') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locEngineeringOption  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('locEngineeringOption') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Engineering Option')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locEngineeringOption  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locEngineeringOption',p_web.GetValue('NewValue'))
    locEngineeringOption = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locEngineeringOption
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locEngineeringOption',p_web.GetValue('Value'))
    locEngineeringOption = p_web.GetValue('Value')
  End

Value::locEngineeringOption  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('locEngineeringOption') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locEngineeringOption
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locEngineeringOption'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::locEngineeringOption  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('locEngineeringOption') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::gap:JobDetails  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('gap:JobDetails',p_web.GetValue('NewValue'))
    do Value::gap:JobDetails
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::gap:JobDetails  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('gap:JobDetails') & '_value',Choose(1,'hdiv','adiv'))
  loc:extra = ''
  If Not (1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::gap:JobDetails  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('gap:JobDetails') & '_comment',Choose(1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::button:ChangeEngineeringOption  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('button:ChangeEngineeringOption',p_web.GetValue('NewValue'))
    do Value::button:ChangeEngineeringOption
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::button:ChangeEngineeringOption  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('button:ChangeEngineeringOption') & '_value',Choose(p_web.GSV('hide:EngOptionButton') = 1 or p_web.GSV('job:Engineer') = '' or p_web.GSV('job:Date_Completed') > 0 OR p_web.GSV('Job:ViewOnly') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('hide:EngOptionButton') = 1 or p_web.GSV('job:Engineer') = '' or p_web.GSV('job:Date_Completed') > 0 OR p_web.GSV('Job:ViewOnly') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ChangeEngOption','Change Eng. Option','MenuButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormEngineeringOption')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()

Comment::button:ChangeEngineeringOption  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('button:ChangeEngineeringOption') & '_comment',Choose(p_web.GSV('hide:EngOptionButton') = 1 or p_web.GSV('job:Engineer') = '' or p_web.GSV('job:Date_Completed') > 0 OR p_web.GSV('Job:ViewOnly') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('hide:EngOptionButton') = 1 or p_web.GSV('job:Engineer') = '' or p_web.GSV('job:Date_Completed') > 0 OR p_web.GSV('Job:ViewOnly') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::text:accountNumber  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('text:accountNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Trade Account')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::text:accountNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('text:accountNumber',p_web.GetValue('NewValue'))
    do Value::text:accountNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::text:accountNumber  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('text:accountNumber') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate(p_web.GSV('locTradeAccount'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::text:accountNumber  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('text:accountNumber') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::text:Completed  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('text:Completed') & '_prompt',Choose(p_web.GSV('job:Date_Completed') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Completed')
  If p_web.GSV('job:Date_Completed') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::text:Completed  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('text:Completed',p_web.GetValue('NewValue'))
    do Value::text:Completed
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::text:Completed  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('text:Completed') & '_value',Choose(p_web.GSV('job:Date_Completed') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('job:Date_Completed') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('RedBold')&'">' & p_web.Translate(Format(p_web.GSV('job:Date_Completed'),@d06b) & ' ' & Format(p_web.GSV('job:Time_Completed'),@T01b) & ' - JOB COMPLETED',0) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::text:Completed  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('text:Completed') & '_comment',Choose(p_web.GSV('job:Date_Completed') = 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('job:Date_Completed') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::text:modelDetails  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('text:modelDetails') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Model Details')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::text:modelDetails  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('text:modelDetails',p_web.GetValue('NewValue'))
    do Value::text:modelDetails
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::text:modelDetails  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('text:modelDetails') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate(p_web.GSV('job:Manufacturer') & ' ' & p_web.GSV('job:Model_Number') & ' - ' & p_web.GSV('job:Unit_Type'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::text:modelDetails  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('text:modelDetails') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::text:HubRepair  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('text:HubRepair') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Hub Repair')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::text:HubRepair  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('text:HubRepair',p_web.GetValue('NewValue'))
    do Value::text:HubRepair
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::text:HubRepair  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('text:HubRepair') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate(Format(p_web.GSV('jobe:HubRepairDate'),@d06b) & ' ' & Format(p_web.GSV('jobe:HubRepairTime'),@T01b),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::text:HubRepair  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('text:HubRepair') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Order_Number  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('job:Order_Number') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Order Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Order_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Order_Number',p_web.GetValue('NewValue'))
    job:Order_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Order_Number
    do Value::job:Order_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Order_Number',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Order_Number = p_web.GetValue('Value')
  End

Value::job:Order_Number  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('job:Order_Number') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- job:Order_Number
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Order_Number'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::job:Order_Number  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('job:Order_Number') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::text:OBFValidated  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('text:OBFValidated') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('OBF Validated')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::text:OBFValidated  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('text:OBFValidated',p_web.GetValue('NewValue'))
    do Value::text:OBFValidated
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::text:OBFValidated  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('text:OBFValidated') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate(Format(p_web.GSV('jobe:OBFValidateDate'),@d06b) & ' ' & Format(p_web.GSV('jobe:OBFValidateTime'),@T01b),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::text:OBFValidated  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('text:OBFValidated') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:ESN  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('job:ESN') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('IMEI Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:ESN  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:ESN',p_web.GetValue('NewValue'))
    job:ESN = p_web.GetValue('NewValue') !FieldType= STRING Field = job:ESN
    do Value::job:ESN
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:ESN',p_web.dFormat(p_web.GetValue('Value'),'@s20'))
    job:ESN = p_web.GetValue('Value')
  End

Value::job:ESN  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('job:ESN') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- job:ESN
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:ESN'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::job:ESN  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('job:ESN') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:MSN  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('job:MSN') & '_prompt',Choose(p_web.GSV('Hide:MSN') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('MSN')
  If p_web.GSV('Hide:MSN') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:MSN  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:MSN',p_web.GetValue('NewValue'))
    job:MSN = p_web.GetValue('NewValue') !FieldType= STRING Field = job:MSN
    do Value::job:MSN
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:MSN',p_web.dFormat(p_web.GetValue('Value'),'@s20'))
    job:MSN = p_web.GetValue('Value')
  End

Value::job:MSN  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('job:MSN') & '_value',Choose(p_web.GSV('Hide:MSN') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:MSN') = 1)
  ! --- DISPLAY --- job:MSN
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:MSN'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::job:MSN  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('job:MSN') & '_comment',Choose(p_web.GSV('Hide:MSN') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:MSN') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locJobType  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('locJobType') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Job Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locJobType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locJobType',p_web.GetValue('NewValue'))
    locJobType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locJobType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locJobType',p_web.GetValue('Value'))
    locJobType = p_web.GetValue('Value')
  End

Value::locJobType  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('locJobType') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locJobType
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locJobType'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::locJobType  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('locJobType') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::button:AmendJobType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('button:AmendJobType',p_web.GetValue('NewValue'))
    do Value::button:AmendJobType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::button:AmendJobType  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('button:AmendJobType') & '_value',Choose(p_web.GSV('Job:ViewOnly') = 1 or p_web.GSV('locCurrentEngineer') = '' or p_web.gsv('locEngineeringOption') = 'Not Set' or p_web.GSV('Hide:ButtonViewCosts') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Job:ViewOnly') = 1 or p_web.GSV('locCurrentEngineer') = '' or p_web.gsv('locEngineeringOption') = 'Not Set' or p_web.GSV('Hide:ButtonViewCosts') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ChangeJobType','Change Job Type(s)','MenuButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('SetJobType')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()

Comment::button:AmendJobType  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('button:AmendJobType') & '_comment',Choose(p_web.GSV('Job:ViewOnly') = 1 or p_web.GSV('locCurrentEngineer') = '' or p_web.gsv('locEngineeringOption') = 'Not Set' or p_web.GSV('Hide:ButtonViewCosts') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Job:ViewOnly') = 1 or p_web.GSV('locCurrentEngineer') = '' or p_web.gsv('locEngineeringOption') = 'Not Set' or p_web.GSV('Hide:ButtonViewCosts') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Charge_Type  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('job:Charge_Type') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Cha. Charge Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Charge_Type  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Charge_Type',p_web.GetValue('NewValue'))
    job:Charge_Type = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Charge_Type
    do Value::job:Charge_Type
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Charge_Type',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Charge_Type = p_web.GetValue('Value')
  End

Value::job:Charge_Type  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('job:Charge_Type') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- job:Charge_Type
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Charge_Type'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::job:Charge_Type  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('job:Charge_Type') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Repair_Type  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('job:Repair_Type') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Cha. Repair Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Repair_Type  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Repair_Type',p_web.GetValue('NewValue'))
    job:Repair_Type = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Repair_Type
    do Value::job:Repair_Type
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Repair_Type',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Repair_Type = p_web.GetValue('Value')
  End

Value::job:Repair_Type  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('job:Repair_Type') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- job:Repair_Type
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Repair_Type'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::job:Repair_Type  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('job:Repair_Type') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Warranty_Charge_Type  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('job:Warranty_Charge_Type') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Warr. Charge Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Warranty_Charge_Type  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Warranty_Charge_Type',p_web.GetValue('NewValue'))
    job:Warranty_Charge_Type = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Warranty_Charge_Type
    do Value::job:Warranty_Charge_Type
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Warranty_Charge_Type',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Warranty_Charge_Type = p_web.GetValue('Value')
  End

Value::job:Warranty_Charge_Type  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('job:Warranty_Charge_Type') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- job:Warranty_Charge_Type
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Warranty_Charge_Type'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::job:Warranty_Charge_Type  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('job:Warranty_Charge_Type') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Repair_Type_Warranty  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('job:Repair_Type_Warranty') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Warr. Repair Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Repair_Type_Warranty  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Repair_Type_Warranty',p_web.GetValue('NewValue'))
    job:Repair_Type_Warranty = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Repair_Type_Warranty
    do Value::job:Repair_Type_Warranty
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Repair_Type_Warranty',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Repair_Type_Warranty = p_web.GetValue('Value')
  End

Value::job:Repair_Type_Warranty  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('job:Repair_Type_Warranty') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- job:Repair_Type_Warranty
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Repair_Type_Warranty'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::job:Repair_Type_Warranty  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('job:Repair_Type_Warranty') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::textBouncer  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('textBouncer',p_web.GetValue('NewValue'))
    do Value::textBouncer
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::textBouncer  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('textBouncer') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('RedBold')&'">' & p_web.Translate(p_web.GSV('textBouncer'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::textBouncer  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('textBouncer') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonPreviousUnitHistory  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonPreviousUnitHistory',p_web.GetValue('NewValue'))
    do Value::buttonPreviousUnitHistory
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::buttonPreviousUnitHistory  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('buttonPreviousUnitHistory') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','PreviousUnitHistory','Previous Unit History','SmallButtonFixed',loc:formname,,,'window.open('''& p_web._MakeURL(clip('BrowseIMEIHistory?fromURL=ViewJob&currentJob=' & p_web.GSV('job:Ref_number'))) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()

Comment::buttonPreviousUnitHistory  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('buttonPreviousUnitHistory') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Current_Status  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('job:Current_Status') & '_prompt',Choose(p_web.GSV('locCurrentEngineer') = 'Not Allocated','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Current Status')
  If p_web.GSV('locCurrentEngineer') = 'Not Allocated'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Current_Status  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Current_Status',p_web.GetValue('NewValue'))
    job:Current_Status = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Current_Status
    do Value::job:Current_Status
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Current_Status',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Current_Status = p_web.GetValue('Value')
  End
  p_Web.SetValue('lookupfield','job:Current_Status')
  do AfterLookup
  do Comment::job:Current_Status

Value::job:Current_Status  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('job:Current_Status') & '_value',Choose(p_web.GSV('locCurrentEngineer') = 'Not Allocated','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locCurrentEngineer') = 'Not Allocated')
  ! --- STRING --- job:Current_Status
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:CurrentStatus') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('ReadOnly:CurrentStatus') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('job:Current_Status')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Current_Status'',''viewjob_job:current_status_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Current_Status')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = 'readonly'
    packet = clip(packet) & p_web.CreateInput('text','job:Current_Status',p_web.GetSessionValue('job:Current_Status'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s30',loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('SelectAccessJobStatus')&'?LookupField=job:Current_Status&Tab=12&ForeignField=acs:Status&_sort=acs:Status&Refresh=sort&LookupFrom=ViewJob&'),) !lookupextra
    End
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewJob_' & p_web._nocolon('job:Current_Status') & '_value')

Comment::job:Current_Status  Routine
      loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('job:Current_Status') & '_comment',Choose(p_web.GSV('locCurrentEngineer') = 'Not Allocated','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locCurrentEngineer') = 'Not Allocated'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewJob_' & p_web._nocolon('job:Current_Status') & '_comment')

Prompt::job:Location  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('job:Location') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Location')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Location  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Location',p_web.GetValue('NewValue'))
    job:Location = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Location
    do Value::job:Location
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Location',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Location = p_web.GetValue('Value')
  End

Value::job:Location  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('job:Location') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- job:Location
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Location'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::job:Location  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('job:Location') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::text:ExchangeUnit  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('text:ExchangeUnit') & '_prompt',Choose(p_web.GSV('job:Exchange_Unit_Number') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Exchange Unit')
  If p_web.GSV('job:Exchange_Unit_Number') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::text:ExchangeUnit  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('text:ExchangeUnit',p_web.GetValue('NewValue'))
    do Value::text:ExchangeUnit
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::text:ExchangeUnit  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('text:ExchangeUnit') & '_value',Choose(p_web.GSV('job:Exchange_Unit_Number') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('job:Exchange_Unit_Number') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate(p_web.GSV('locExchangeText'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::text:ExchangeUnit  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('text:ExchangeUnit') & '_comment',Choose(p_web.GSV('job:Exchange_Unit_Number') = 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('job:Exchange_Unit_Number') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::text:LoanUnit  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('text:LoanUnit') & '_prompt',Choose(p_web.GSV('job:Loan_Unit_Number') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Loan Unit')
  If p_web.GSV('job:Loan_Unit_Number') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::text:LoanUnit  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('text:LoanUnit',p_web.GetValue('NewValue'))
    do Value::text:LoanUnit
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::text:LoanUnit  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('text:LoanUnit') & '_value',Choose(p_web.GSV('job:Loan_Unit_Number') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('job:Loan_Unit_Number') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate(p_web.GSV('locLoanText'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::text:LoanUnit  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('text:LoanUnit') & '_comment',Choose(p_web.GSV('job:Loan_Unit_Number') = 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('job:Loan_Unit_Number') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Exchange_Status  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('job:Exchange_Status') & '_prompt',Choose(p_web.GSV('job:Exchange_Unit_Number') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Exchange Status')
  If p_web.GSV('job:Exchange_Unit_Number') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Exchange_Status  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Exchange_Status',p_web.GetValue('NewValue'))
    job:Exchange_Status = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Exchange_Status
    do Value::job:Exchange_Status
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Exchange_Status',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Exchange_Status = p_web.GetValue('Value')
  End

Value::job:Exchange_Status  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('job:Exchange_Status') & '_value',Choose(p_web.GSV('job:Exchange_Unit_Number') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('job:Exchange_Unit_Number') = 0)
  ! --- DISPLAY --- job:Exchange_Status
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Exchange_Status'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::job:Exchange_Status  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('job:Exchange_Status') & '_comment',Choose(p_web.GSV('job:Exchange_Unit_Number') = 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('job:Exchange_Unit_Number') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Loan_Status  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('job:Loan_Status') & '_prompt',Choose(p_web.GSV('job:Loan_Unit_Number') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Loan Status')
  If p_web.GSV('job:Loan_Unit_Number') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Loan_Status  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Loan_Status',p_web.GetValue('NewValue'))
    job:Loan_Status = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Loan_Status
    do Value::job:Loan_Status
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Loan_Status',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Loan_Status = p_web.GetValue('Value')
  End

Value::job:Loan_Status  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('job:Loan_Status') & '_value',Choose(p_web.GSV('job:Loan_Unit_Number') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('job:Loan_Unit_Number') = 0)
  ! --- DISPLAY --- job:Loan_Status
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Loan_Status'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::job:Loan_Status  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('job:Loan_Status') & '_comment',Choose(p_web.GSV('job:Loan_Unit_Number') = 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('job:Loan_Unit_Number') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::text:SecondExchangeUnit  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('text:SecondExchangeUnit') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Second Exchange Unit')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::text:SecondExchangeUnit  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('text:SecondExchangeUnit',p_web.GetValue('NewValue'))
    do Value::text:SecondExchangeUnit
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::text:SecondExchangeUnit  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('text:SecondExchangeUnit') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate('Second Exchange Unit Attached',) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::text:SecondExchangeUnit  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('text:SecondExchangeUnit') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jbn:Fault_Description  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('jbn:Fault_Description') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Fault Description')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jbn:Fault_Description  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jbn:Fault_Description',p_web.GetValue('NewValue'))
    jbn:Fault_Description = p_web.GetValue('NewValue') !FieldType= STRING Field = jbn:Fault_Description
    do Value::jbn:Fault_Description
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jbn:Fault_Description',p_web.dFormat(p_web.GetValue('Value'),'@s255'))
    jbn:Fault_Description = p_web.GetValue('Value')
  End
  do Value::jbn:Fault_Description
  do SendAlert

Value::jbn:Fault_Description  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('jbn:Fault_Description') & '_value','adiv')
  loc:extra = ''
  ! --- TEXT --- jbn:Fault_Description
  loc:fieldclass = Choose(sub('TextEntry',1,1) = ' ',clip('FormEntry') & 'TextEntry','TextEntry')
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('jbn:Fault_Description')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jbn:Fault_Description'',''viewjob_jbn:fault_description_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'readonly'
  packet = clip(packet) & p_web.CreateTextArea('jbn:Fault_Description',p_web.GetSessionValue('jbn:Fault_Description'),4,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,255,,Net:Web:Control) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewJob_' & p_web._nocolon('jbn:Fault_Description') & '_value')

Comment::jbn:Fault_Description  Routine
      loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('jbn:Fault_Description') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jbn:Engineers_Notes  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('jbn:Engineers_Notes') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Engineers Notes')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jbn:Engineers_Notes  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jbn:Engineers_Notes',p_web.GetValue('NewValue'))
    jbn:Engineers_Notes = p_web.GetValue('NewValue') !FieldType= STRING Field = jbn:Engineers_Notes
    do Value::jbn:Engineers_Notes
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jbn:Engineers_Notes',p_web.dFormat(p_web.GetValue('Value'),'@s255'))
    jbn:Engineers_Notes = p_web.GetValue('Value')
  End
    jbn:Engineers_Notes = Upper(jbn:Engineers_Notes)
    p_web.SetSessionValue('jbn:Engineers_Notes',jbn:Engineers_Notes)
  !Workaround for blank
  if (sub(p_web.GSV('jbn:Engineers_Notes'),1,1) = ' ')
      p_web.SSV('jbn:Engineers_Notes',sub(p_web.GSV('jbn:Engineers_Notes'),2,255))
  end ! if (sub(p_web.GSV('jbn:Engineers_Notes',1,1) = '  ')
  do Value::jbn:Engineers_Notes
  do SendAlert

Value::jbn:Engineers_Notes  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('jbn:Engineers_Notes') & '_value','adiv')
  loc:extra = ''
  ! --- TEXT --- jbn:Engineers_Notes
  loc:fieldclass = Choose(sub('TextEntry',1,1) = ' ',clip('FormEntry') & 'TextEntry','TextEntry')
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('Job:ViewOnly') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('jbn:Engineers_Notes')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jbn:Engineers_Notes'',''viewjob_jbn:engineers_notes_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('jbn:Engineers_Notes')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('Job:ViewOnly') = 1,'readonly','')
  packet = clip(packet) & p_web.CreateTextArea('jbn:Engineers_Notes',p_web.GetSessionValue('jbn:Engineers_Notes'),5,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,255,,Net:Web:Control) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewJob_' & p_web._nocolon('jbn:Engineers_Notes') & '_value')

Comment::jbn:Engineers_Notes  Routine
      loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('jbn:Engineers_Notes') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::gap  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('gap',p_web.GetValue('NewValue'))
    do Value::gap
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::gap  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('gap') & '_value',Choose(1,'hdiv','adiv'))
  loc:extra = ''
  If Not (1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::gap  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('gap') & '_comment',Choose(1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::button:EngineersNotes  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('button:EngineersNotes',p_web.GetValue('NewValue'))
    do Value::button:EngineersNotes
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::button:EngineersNotes  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('button:EngineersNotes') & '_value',Choose(p_web.GSV('Job:ViewOnly') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Job:ViewOnly') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','EngineersNotes','Engineers Notes','MenuButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('PickEngineersNotes')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()

Comment::button:EngineersNotes  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('button:EngineersNotes') & '_comment',Choose(p_web.GSV('Job:ViewOnly') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Job:ViewOnly') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locHOClaimStatus  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('locHOClaimStatus') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('H/O Status')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locHOClaimStatus  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locHOClaimStatus',p_web.GetValue('NewValue'))
    locHOClaimStatus = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locHOClaimStatus
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locHOClaimStatus',p_web.GetValue('Value'))
    locHOClaimStatus = p_web.GetValue('Value')
  End

Value::locHOClaimStatus  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('locHOClaimStatus') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locHOClaimStatus
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locHOClaimStatus'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::locHOClaimStatus  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('locHOClaimStatus') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locRRCClaimStatus  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('locRRCClaimStatus') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('RRC Status')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locRRCClaimStatus  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locRRCClaimStatus',p_web.GetValue('NewValue'))
    locRRCClaimStatus = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locRRCClaimStatus
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locRRCClaimStatus',p_web.GetValue('Value'))
    locRRCClaimStatus = p_web.GetValue('Value')
  End

Value::locRRCClaimStatus  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('locRRCClaimStatus') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locRRCClaimStatus
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locRRCClaimStatus'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::locRRCClaimStatus  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('locRRCClaimStatus') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::textLiquidDamage  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('textLiquidDamage') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::textLiquidDamage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('textLiquidDamage',p_web.GetValue('NewValue'))
    do Value::textLiquidDamage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::textLiquidDamage  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('textLiquidDamage') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('RedBold')&'">' & p_web.Translate('LIQUID DAMAGE',) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::textLiquidDamage  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('textLiquidDamage') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locCurrentEngineer  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('locCurrentEngineer') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Current Engineer')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locCurrentEngineer  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locCurrentEngineer',p_web.GetValue('NewValue'))
    locCurrentEngineer = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locCurrentEngineer
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locCurrentEngineer',p_web.GetValue('Value'))
    locCurrentEngineer = p_web.GetValue('Value')
  End

Value::locCurrentEngineer  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('locCurrentEngineer') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locCurrentEngineer
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locCurrentEngineer'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::locCurrentEngineer  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('locCurrentEngineer') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locEngineerAllocated  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('locEngineerAllocated') & '_prompt',Choose(p_web.GSV('job:Engineer') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Allocated')
  If p_web.GSV('job:Engineer') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locEngineerAllocated  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locEngineerAllocated',p_web.GetValue('NewValue'))
    do Value::locEngineerAllocated
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::locEngineerAllocated  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('locEngineerAllocated') & '_value',Choose(p_web.GSV('job:Engineer') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('job:Engineer') = '')
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate(p_web.GSV('locEngineerAllocated'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::locEngineerAllocated  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('locEngineerAllocated') & '_comment',Choose(p_web.GSV('job:Engineer') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('job:Engineer') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jobe:Network  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('jobe:Network') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Network')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jobe:Network  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe:Network',p_web.GetValue('NewValue'))
    jobe:Network = p_web.GetValue('NewValue') !FieldType= STRING Field = jobe:Network
    do Value::jobe:Network
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jobe:Network',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    jobe:Network = p_web.GetValue('Value')
  End
    jobe:Network = Upper(jobe:Network)
    p_web.SetSessionValue('jobe:Network',jobe:Network)
      Access:NETWORKS.Clearkey(net:NetworkKey)
      net:Network    = p_web.GSV('jobe:Network')
      if (Access:NETWORKS.TryFetch(net:NetworkKey) = Level:Benign)
          ! Found
      else ! if (Access:NETWORKS.TryFetch(net:NetworkKey) = Level:Benign)
          ! Error
          p_web.SSV('jobe:Network','')
      end ! if (Access:NETWORKS.TryFetch(net:NetworkKey) = Level:Benign)
  p_Web.SetValue('lookupfield','jobe:Network')
  do AfterLookup
  do Value::jobe:Network
  do SendAlert
  do Comment::jobe:Network

Value::jobe:Network  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('jobe:Network') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- jobe:Network
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('jobe:Network')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe:Network'',''viewjob_jobe:network_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('jobe:Network')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','jobe:Network',p_web.GetSessionValue('jobe:Network'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s30',loc:javascript,p_web.PicLength('@s30'),'Network') & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('SelectNetworks')&'?LookupField=jobe:Network&Tab=12&ForeignField=net:Network&_sort=net:Network&Refresh=sort&LookupFrom=ViewJob&'),) !lookupextra
    End
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewJob_' & p_web._nocolon('jobe:Network') & '_value')

Comment::jobe:Network  Routine
      loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('jobe:Network') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewJob_' & p_web._nocolon('jobe:Network') & '_comment')

Prompt::job:Authority_Number  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('job:Authority_Number') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Authority Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Authority_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Authority_Number',p_web.GetValue('NewValue'))
    job:Authority_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Authority_Number
    do Value::job:Authority_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Authority_Number',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Authority_Number = p_web.GetValue('Value')
  End
    job:Authority_Number = Upper(job:Authority_Number)
    p_web.SetSessionValue('job:Authority_Number',job:Authority_Number)
  do Value::job:Authority_Number
  do SendAlert

Value::job:Authority_Number  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('job:Authority_Number') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- job:Authority_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('Job:ViewOnly') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('Job:ViewOnly') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('job:Authority_Number')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Authority_Number'',''viewjob_job:authority_number_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Authority_Number',p_web.GetSessionValueFormat('job:Authority_Number'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewJob_' & p_web._nocolon('job:Authority_Number') & '_value')

Comment::job:Authority_Number  Routine
      loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('job:Authority_Number') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Unit_Type  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('job:Unit_Type') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Unit Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Unit_Type  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Unit_Type',p_web.GetValue('NewValue'))
    job:Unit_Type = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Unit_Type
    do Value::job:Unit_Type
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Unit_Type',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Unit_Type = p_web.GetValue('Value')
  End
  p_Web.SetValue('lookupfield','job:Unit_Type')
  do AfterLookup
  do Value::job:Unit_Type
  do SendAlert
  do Comment::job:Unit_Type

Value::job:Unit_Type  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('job:Unit_Type') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- job:Unit_Type
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('Job:ViewOnly') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('Job:ViewOnly') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('job:Unit_Type')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Unit_Type'',''viewjob_job:unit_type_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Unit_Type')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','job:Unit_Type',p_web.GetSessionValue('job:Unit_Type'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s30',loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('SelectUnitTypes')&'?LookupField=job:Unit_Type&Tab=12&ForeignField=uni:Unit_Type&_sort=uni:Unit_Type&Refresh=sort&LookupFrom=ViewJob&'),) !lookupextra
    End
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewJob_' & p_web._nocolon('job:Unit_Type') & '_value')

Comment::job:Unit_Type  Routine
      loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('job:Unit_Type') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewJob_' & p_web._nocolon('job:Unit_Type') & '_comment')

Validate::locRepairTypesNoParts  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locRepairTypesNoParts',p_web.GetValue('NewValue'))
    do Value::locRepairTypesNoParts
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::locRepairTypesNoParts  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('locRepairTypesNoParts') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('RedBold')&'">' & p_web.Translate('Repair Type does not allow parts',) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::locRepairTypesNoParts  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('locRepairTypesNoParts') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::ViewEstimateParts  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('ViewEstimateParts',p_web.GetValue('NewValue'))
    do Value::ViewEstimateParts
  Else
    p_web.StoreValue('epr:Record_Number')
  End

Value::ViewEstimateParts  Routine
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(p_web.GSV('job:Chargeable_Job') <> 'YES' or p_web.GSV('job:Estimate') <> 'YES' or (p_web.GSV('job:Estimate') = 'YES' and (p_web.GSV('job:Estimate_Accepted') = 'YES' or p_web.GSV('job:Estimate_Rejected') = 'YES')),1,0))
  ! --- BROWSE ---  BrowseEstimateParts --
  p_web.SetValue('BrowseEstimateParts:NoForm',1)
  p_web.SetValue('BrowseEstimateParts:FormName',loc:formname)
  p_web.SetValue('BrowseEstimateParts:parentIs','Form')
  p_web.SetValue('_parentProc','ViewJob')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('ViewJob_BrowseEstimateParts_embedded_div')&'"><!-- Net:BrowseEstimateParts --></div><13,10>'
    p_web._DivHeader('ViewJob_' & lower('BrowseEstimateParts') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('ViewJob_' & lower('BrowseEstimateParts') & '_value')
  else
    packet = clip(packet) & '<!-- Net:BrowseEstimateParts --><13,10>'
  end
  do SendPacket

Comment::ViewEstimateParts  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('ViewEstimateParts') & '_comment',Choose(p_web.GSV('job:Chargeable_Job') <> 'YES' or p_web.GSV('job:Estimate') <> 'YES' or (p_web.GSV('job:Estimate') = 'YES' and (p_web.GSV('job:Estimate_Accepted') = 'YES' or p_web.GSV('job:Estimate_Rejected') = 'YES')),'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('job:Chargeable_Job') <> 'YES' or p_web.GSV('job:Estimate') <> 'YES' or (p_web.GSV('job:Estimate') = 'YES' and (p_web.GSV('job:Estimate_Accepted') = 'YES' or p_web.GSV('job:Estimate_Rejected') = 'YES'))
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::browse:ChargeableParts  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('browse:ChargeableParts',p_web.GetValue('NewValue'))
    do Value::browse:ChargeableParts
  Else
    p_web.StoreValue('par:Record_Number')
  End

Value::browse:ChargeableParts  Routine
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(p_web.GSV('job:Chargeable_Job') <> 'YES' or (p_web.GSV('job:Estimate') = 'YES' and p_web.GSV('job:Estimate_Accepted') <> 'YES' and p_web.GSV('job:Estimate_Rejected') <> 'YES'),1,0))
  ! --- BROWSE ---  BrowseChargeableParts --
  p_web.SetValue('BrowseChargeableParts:NoForm',1)
  p_web.SetValue('BrowseChargeableParts:FormName',loc:formname)
  p_web.SetValue('BrowseChargeableParts:parentIs','Form')
  p_web.SetValue('_parentProc','ViewJob')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('ViewJob_BrowseChargeableParts_embedded_div')&'"><!-- Net:BrowseChargeableParts --></div><13,10>'
    p_web._DivHeader('ViewJob_' & lower('BrowseChargeableParts') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('ViewJob_' & lower('BrowseChargeableParts') & '_value')
  else
    packet = clip(packet) & '<!-- Net:BrowseChargeableParts --><13,10>'
  end
  do SendPacket

Comment::browse:ChargeableParts  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('browse:ChargeableParts') & '_comment',Choose(p_web.GSV('job:Chargeable_Job') <> 'YES' or (p_web.GSV('job:Estimate') = 'YES' and p_web.GSV('job:Estimate_Accepted') <> 'YES' and p_web.GSV('job:Estimate_Rejected') <> 'YES'),'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('job:Chargeable_Job') <> 'YES' or (p_web.GSV('job:Estimate') = 'YES' and p_web.GSV('job:Estimate_Accepted') <> 'YES' and p_web.GSV('job:Estimate_Rejected') <> 'YES')
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::browseWarrantyParts  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('browseWarrantyParts',p_web.GetValue('NewValue'))
    do Value::browseWarrantyParts
  Else
    p_web.StoreValue('wpr:Record_Number')
  End

Value::browseWarrantyParts  Routine
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(p_web.GSV('job:Warranty_Job') <> 'YES',1,0))
  ! --- BROWSE ---  BrowseWarrantyParts --
  p_web.SetValue('BrowseWarrantyParts:NoForm',1)
  p_web.SetValue('BrowseWarrantyParts:FormName',loc:formname)
  p_web.SetValue('BrowseWarrantyParts:parentIs','Form')
  p_web.SetValue('_parentProc','ViewJob')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('ViewJob_BrowseWarrantyParts_embedded_div')&'"><!-- Net:BrowseWarrantyParts --></div><13,10>'
    p_web._DivHeader('ViewJob_' & lower('BrowseWarrantyParts') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('ViewJob_' & lower('BrowseWarrantyParts') & '_value')
  else
    packet = clip(packet) & '<!-- Net:BrowseWarrantyParts --><13,10>'
  end
  do SendPacket

Comment::browseWarrantyParts  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('browseWarrantyParts') & '_comment',Choose(p_web.GSV('job:Warranty_Job') <> 'YES','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('job:Warranty_Job') <> 'YES'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locMobileLifetimeValue  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('locMobileLifetimeValue') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Lifetime Value')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locMobileLifetimeValue  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locMobileLifetimeValue',p_web.GetValue('NewValue'))
    locMobileLifetimeValue = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locMobileLifetimeValue
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locMobileLifetimeValue',p_web.GetValue('Value'))
    locMobileLifetimeValue = p_web.GetValue('Value')
  End

Value::locMobileLifetimeValue  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('locMobileLifetimeValue') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locMobileLifetimeValue
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locMobileLifetimeValue'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::locMobileLifetimeValue  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('locMobileLifetimeValue') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locMobileAverageSpend  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('locMobileAverageSpend') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Average Spend')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locMobileAverageSpend  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locMobileAverageSpend',p_web.GetValue('NewValue'))
    locMobileAverageSpend = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locMobileAverageSpend
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locMobileAverageSpend',p_web.GetValue('Value'))
    locMobileAverageSpend = p_web.GetValue('Value')
  End

Value::locMobileAverageSpend  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('locMobileAverageSpend') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locMobileAverageSpend
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locMobileAverageSpend'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::locMobileAverageSpend  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('locMobileAverageSpend') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locMobileLoyaltyStatus  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('locMobileLoyaltyStatus') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Loyalty Status')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locMobileLoyaltyStatus  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locMobileLoyaltyStatus',p_web.GetValue('NewValue'))
    locMobileLoyaltyStatus = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locMobileLoyaltyStatus
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locMobileLoyaltyStatus',p_web.GetValue('Value'))
    locMobileLoyaltyStatus = p_web.GetValue('Value')
  End

Value::locMobileLoyaltyStatus  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('locMobileLoyaltyStatus') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locMobileLoyaltyStatus
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locMobileLoyaltyStatus'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::locMobileLoyaltyStatus  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('locMobileLoyaltyStatus') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locMobileUpgradeDate  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('locMobileUpgradeDate') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Upgrade Date')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locMobileUpgradeDate  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locMobileUpgradeDate',p_web.GetValue('NewValue'))
    locMobileUpgradeDate = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locMobileUpgradeDate
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locMobileUpgradeDate',p_web.GetValue('Value'))
    locMobileUpgradeDate = p_web.GetValue('Value')
  End

Value::locMobileUpgradeDate  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('locMobileUpgradeDate') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locMobileUpgradeDate
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locMobileUpgradeDate'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::locMobileUpgradeDate  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('locMobileUpgradeDate') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locMobileIDNumber  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('locMobileIDNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('ID Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locMobileIDNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locMobileIDNumber',p_web.GetValue('NewValue'))
    locMobileIDNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locMobileIDNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locMobileIDNumber',p_web.GetValue('Value'))
    locMobileIDNumber = p_web.GetValue('Value')
  End

Value::locMobileIDNumber  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('locMobileIDNumber') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locMobileIDNumber
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locMobileIDNumber'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::locMobileIDNumber  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('locMobileIDNumber') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonAllocateEngineer  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonAllocateEngineer',p_web.GetValue('NewValue'))
    do Value::buttonAllocateEngineer
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::buttonAllocateEngineer  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('buttonAllocateEngineer') & '_value',Choose(p_web.GSV('Hide:ButtonAllocateEngineer') = 1 or p_web.GSV('job:Workshop') <> 'YES','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:ButtonAllocateEngineer') = 1 or p_web.GSV('job:Workshop') <> 'YES')
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','AllocateEngineer','Allocate Engineer','DoubleButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('AllocateEngineer')) & ''','''&clip('_self')&''')',loc:javascript,0,'/images/alleng.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()

Comment::buttonAllocateEngineer  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('buttonAllocateEngineer') & '_comment',Choose(p_web.GSV('Hide:ButtonAllocateEngineer') = 1 or p_web.GSV('job:Workshop') <> 'YES','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:ButtonAllocateEngineer') = 1 or p_web.GSV('job:Workshop') <> 'YES'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonAccessories  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonAccessories',p_web.GetValue('NewValue'))
    do Value::buttonAccessories
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::buttonAccessories  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('buttonAccessories') & '_value',Choose(p_web.GSV('Hide:ButtonAccessories') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:ButtonAccessories') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','Accessories','Accessories','DoubleButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('JobAccessories')) & ''','''&clip('_self')&''')',loc:javascript,0,'/images/accessories.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()

Comment::buttonAccessories  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('buttonAccessories') & '_comment',Choose(p_web.GSV('Hide:ButtonAccessories') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:ButtonAccessories') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonFaultCodes  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonFaultCodes',p_web.GetValue('NewValue'))
    do Value::buttonFaultCodes
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::buttonFaultCodes  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('buttonFaultCodes') & '_value',Choose(p_web.GSV('locCurrentEngineer') = '' or p_web.gsv('locEngineeringOption') = 'Not Set' or p_web.GSV('Hide:ButtonFaultCodes') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locCurrentEngineer') = '' or p_web.gsv('locEngineeringOption') = 'Not Set' or p_web.GSV('Hide:ButtonFaultCodes') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','FaultCodes','Fault Codes','DoubleButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('JobFaultCodes')) & ''','''&clip('_self')&''')',loc:javascript,0,'/images/fault.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()

Comment::buttonFaultCodes  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('buttonFaultCodes') & '_comment',Choose(p_web.GSV('locCurrentEngineer') = '' or p_web.gsv('locEngineeringOption') = 'Not Set' or p_web.GSV('Hide:ButtonFaultCodes') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locCurrentEngineer') = '' or p_web.gsv('locEngineeringOption') = 'Not Set' or p_web.GSV('Hide:ButtonFaultCodes') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonEstimate  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonEstimate',p_web.GetValue('NewValue'))
    do Value::buttonEstimate
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::buttonEstimate  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('buttonEstimate') & '_value',Choose(p_web.GSV('job:Chargeable_Job') <> 'YES' OR p_web.gsv('locEngineeringOption') = 'Not Set','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('job:Chargeable_Job') <> 'YES' OR p_web.gsv('locEngineeringOption') = 'Not Set')
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','Estimate','Estimate','DoubleButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('JobEstimate')) & ''','''&clip('_self')&''')',loc:javascript,0,'/images/estimate.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()

Comment::buttonEstimate  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('buttonEstimate') & '_comment',Choose(p_web.GSV('job:Chargeable_Job') <> 'YES' OR p_web.gsv('locEngineeringOption') = 'Not Set','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('job:Chargeable_Job') <> 'YES' OR p_web.gsv('locEngineeringOption') = 'Not Set'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonContactHistory  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonContactHistory',p_web.GetValue('NewValue'))
    do Value::buttonContactHistory
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::buttonContactHistory  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('buttonContactHistory') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ContactHistory','Contact History','DoubleButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormBrowseContactHistory')) & ''','''&clip('_self')&''')',loc:javascript,0,'images\contacthist.png',,,,)

  do SendPacket
  p_web._DivFooter()

Comment::buttonContactHistory  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('buttonContactHistory') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonAllocateExchange  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonAllocateExchange',p_web.GetValue('NewValue'))
    do Value::buttonAllocateExchange
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::buttonAllocateExchange  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('buttonAllocateExchange') & '_value',Choose(p_web.GSV('Hide:ExchangeButton') = 1 or p_web.gsv('locEngineeringOption') = 'Not Set','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:ExchangeButton') = 1 or p_web.gsv('locEngineeringOption') = 'Not Set')
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','PickExchangeUnit','Exchange Unit','DoubleButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('PickExchangeUnit')) & ''','''&clip('_self')&''')',loc:javascript,0,'/images/allexchange.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()

Comment::buttonAllocateExchange  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('buttonAllocateExchange') & '_comment',Choose(p_web.GSV('Hide:ExchangeButton') = 1 or p_web.gsv('locEngineeringOption') = 'Not Set','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:ExchangeButton') = 1 or p_web.gsv('locEngineeringOption') = 'Not Set'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonAllocateLoan  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonAllocateLoan',p_web.GetValue('NewValue'))
    do Value::buttonAllocateLoan
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::buttonAllocateLoan  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('buttonAllocateLoan') & '_value',Choose(p_web.GSV('Hide:LoanButton') = 1 or p_web.gsv('locEngineeringOption') = 'Not Set','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:LoanButton') = 1 or p_web.gsv('locEngineeringOption') = 'Not Set')
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','PickLoanUnit','Loan Unit','DoubleButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('PickLoanUnit')) & ''','''&clip('_self')&''')',loc:javascript,0,'/images/allexchange.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()

Comment::buttonAllocateLoan  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('buttonAllocateLoan') & '_comment',Choose(p_web.GSV('Hide:LoanButton') = 1 or p_web.gsv('locEngineeringOption') = 'Not Set','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:LoanButton') = 1 or p_web.gsv('locEngineeringOption') = 'Not Set'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonCreateInvoice  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonCreateInvoice',p_web.GetValue('NewValue'))
    do Value::buttonCreateInvoice
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::buttonCreateInvoice  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('buttonCreateInvoice') & '_value',Choose(p_web.GSV('locCurrentEngineer') = '' or p_web.gsv('locEngineeringOption') = 'Not Set' or p_web.GSV('Job:ViewOnly') = 1 or p_web.GSV('Hide:ButtonCreateInvoice') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locCurrentEngineer') = '' or p_web.gsv('locEngineeringOption') = 'Not Set' or p_web.GSV('Job:ViewOnly') = 1 or p_web.GSV('Hide:ButtonCreateInvoice') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','CreateInvoice',p_web.GSV('URL:CreateInvoiceText'),'DoubleButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip(p_web.GSV('URL:CreateInvoice'))) & ''','''&clip(p_web.GSV('URL:CreateInvoiceTarget'))&''')',loc:javascript,0,'\images\money.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()

Comment::buttonCreateInvoice  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('buttonCreateInvoice') & '_comment',Choose(p_web.GSV('locCurrentEngineer') = '' or p_web.gsv('locEngineeringOption') = 'Not Set' or p_web.GSV('Job:ViewOnly') = 1 or p_web.GSV('Hide:ButtonCreateInvoice') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locCurrentEngineer') = '' or p_web.gsv('locEngineeringOption') = 'Not Set' or p_web.GSV('Job:ViewOnly') = 1 or p_web.GSV('Hide:ButtonCreateInvoice') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonViewCosts  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonViewCosts',p_web.GetValue('NewValue'))
    do Value::buttonViewCosts
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::buttonViewCosts  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('buttonViewCosts') & '_value',Choose(p_web.GSV('locCurrentEngineer') = '' or p_web.gsv('locEngineeringOption') = 'Not Set' or p_web.GSV('Hide:ButtonViewCosts') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locCurrentEngineer') = '' or p_web.gsv('locEngineeringOption') = 'Not Set' or p_web.GSV('Hide:ButtonViewCosts') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ViewCosts','View Costs','DoubleButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip(p_web.GSV('URL:ViewCosts'))) & ''','''&clip('_self')&''')',loc:javascript,0,'/images/money.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()

Comment::buttonViewCosts  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('buttonViewCosts') & '_comment',Choose(p_web.GSV('locCurrentEngineer') = '' or p_web.gsv('locEngineeringOption') = 'Not Set' or p_web.GSV('Hide:ButtonViewCosts') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locCurrentEngineer') = '' or p_web.gsv('locEngineeringOption') = 'Not Set' or p_web.GSV('Hide:ButtonViewCosts') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonValidatePOP  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonValidatePOP',p_web.GetValue('NewValue'))
    do Value::buttonValidatePOP
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::buttonValidatePOP  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('buttonValidatePOP') & '_value',Choose(p_web.GSV('Hide:ValidatePOP') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:ValidatePOP') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ValidatePOP','Validate POP','DoubleButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('ProofOfPurchase')) & ''','''&clip('_self')&''')',loc:javascript,0,'/images/validate.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()

Comment::buttonValidatePOP  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('buttonValidatePOP') & '_comment',Choose(p_web.GSV('Hide:ValidatePOP') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:ValidatePOP') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonResendXML  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonResendXML',p_web.GetValue('NewValue'))
    do Value::buttonResendXML
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
      do ResendXML
  do Value::buttonResendXML
  do SendAlert

Value::buttonResendXML  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('buttonResendXML') & '_value',Choose(p_web.GSV('Hide:ResendXML') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:ResendXML') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&clip('alert(''Job Resent.'');')&';'
  loc:javascript = clip(loc:javascript) & ' ' &p_web._nocolon('sv(''buttonResendXML'',''viewjob_buttonresendxml_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ResendXML','Resend XML To NMS','DoubleButton',loc:formname,,,,loc:javascript,0,'/images/resend.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewJob_' & p_web._nocolon('buttonResendXML') & '_value')

Comment::buttonResendXML  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('buttonResendXML') & '_comment',Choose(p_web.GSV('Hide:ResendXML') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:ResendXML') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonReceiptFromPUP  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonReceiptFromPUP',p_web.GetValue('NewValue'))
    do Value::buttonReceiptFromPUP
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::buttonReceiptFromPUP  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('buttonReceiptFromPUP') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ReceiptFromPUP','Receipt From PUP','DoubleButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('ReceiptFromPUP')) & ''','''&clip('_self')&''')',loc:javascript,0,'/images/receipt.png',,,,)

  do SendPacket
  p_web._DivFooter()

Comment::buttonReceiptFromPUP  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('buttonReceiptFromPUP') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonCompleteRepair  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonCompleteRepair',p_web.GetValue('NewValue'))
    do Value::buttonCompleteRepair
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::buttonCompleteRepair  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('buttonCompleteRepair') & '_value',Choose(p_web.GSV('Hide:CompleteRepair') = 1 Or p_web.GSV('locCurrentEngineer') = '' or p_web.gsv('locEngineeringOption') = 'Not Set' Or p_web.GSV('Job:ViewOnly') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:CompleteRepair') = 1 Or p_web.GSV('locCurrentEngineer') = '' or p_web.gsv('locEngineeringOption') = 'Not Set' Or p_web.GSV('Job:ViewOnly') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','CompleteRepair','Complete Repair','DoubleButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('BillingConfirmation?JobCompleteProcess=1')) & ''','''&clip('_self')&''')',loc:javascript,0,'images/complete.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()

Comment::buttonCompleteRepair  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('buttonCompleteRepair') & '_comment',Choose(p_web.GSV('Hide:CompleteRepair') = 1 Or p_web.GSV('locCurrentEngineer') = '' or p_web.gsv('locEngineeringOption') = 'Not Set' Or p_web.GSV('Job:ViewOnly') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:CompleteRepair') = 1 Or p_web.GSV('locCurrentEngineer') = '' or p_web.gsv('locEngineeringOption') = 'Not Set' Or p_web.GSV('Job:ViewOnly') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::locCompleteRepair  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locCompleteRepair',p_web.GetValue('NewValue'))
    locCompleteRepair = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locCompleteRepair
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locCompleteRepair',p_web.GetValue('Value'))
    locCompleteRepair = p_web.GetValue('Value')
  End

Value::locCompleteRepair  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('locCompleteRepair') & '_value',Choose(p_web.GSV('locCompleteRepair') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locCompleteRepair') = '')
  ! --- TEXT --- locCompleteRepair
  loc:fieldclass = Choose(sub('RedBoldSmall',1,1) = ' ',clip('FormEntry') & 'RedBoldSmall','RedBoldSmall')
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('locCompleteRepair')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'readonly'
  packet = clip(packet) & p_web.CreateTextArea('locCompleteRepair',p_web.GetSessionValue('locCompleteRepair'),10,60,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(locCompleteRepair),,Net:Web:Control) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::locCompleteRepair  Routine
      loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('locCompleteRepair') & '_comment',Choose(p_web.GSV('locCompleteRepair') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locCompleteRepair') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::textCompleteRepair  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('textCompleteRepair',p_web.GetValue('NewValue'))
    do Value::textCompleteRepair
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::textCompleteRepair  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('textCompleteRepair') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate(p_web.GSV('textCompleteRepair'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::textCompleteRepair  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('textCompleteRepair') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locCChargeTypeReason  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('locCChargeTypeReason') & '_prompt',Choose(p_web.GSV('Hide:CChargeType') = 1,'hdiv','' & clip('FormPrompt2') & ''))
  loc:prompt = p_web.Translate('Reason For Chargeable Charge Type Change')
  If p_web.GSV('Hide:CChargeType') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locCChargeTypeReason  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locCChargeTypeReason',p_web.GetValue('NewValue'))
    locCChargeTypeReason = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locCChargeTypeReason
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locCChargeTypeReason',p_web.GetValue('Value'))
    locCChargeTypeReason = p_web.GetValue('Value')
  End
  If locCChargeTypeReason = ''
    loc:Invalid = 'locCChargeTypeReason'
    loc:alert = p_web.translate('Reason For Chargeable Charge Type Change') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locCChargeTypeReason = Upper(locCChargeTypeReason)
    p_web.SetSessionValue('locCChargeTypeReason',locCChargeTypeReason)
  do Value::locCChargeTypeReason
  do SendAlert

Value::locCChargeTypeReason  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('locCChargeTypeReason') & '_value',Choose(p_web.GSV('Hide:CChargeType') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:CChargeType') = 1)
  ! --- TEXT --- locCChargeTypeReason
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locCChargeTypeReason')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locCChargeTypeReason = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locCChargeTypeReason'',''viewjob_loccchargetypereason_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'readonly','')
  packet = clip(packet) & p_web.CreateTextArea('locCChargeTypeReason',p_web.GetSessionValue('locCChargeTypeReason'),5,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(locCChargeTypeReason),,Net:Web:Control) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewJob_' & p_web._nocolon('locCChargeTypeReason') & '_value')

Comment::locCChargeTypeReason  Routine
    loc:comment = p_web.Translate('')
  p_web._DivHeader('ViewJob_' & p_web._nocolon('locCChargeTypeReason') & '_comment',Choose(p_web.GSV('Hide:CChargeType') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:CChargeType') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locCRepairTypeReason  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('locCRepairTypeReason') & '_prompt',Choose(p_web.GSV('Hide:CRepairType') = 1,'hdiv','' & clip('FormPrompt2') & ''))
  loc:prompt = p_web.Translate('Reason For Chargeable Repair Type Change')
  If p_web.GSV('Hide:CRepairType') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locCRepairTypeReason  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locCRepairTypeReason',p_web.GetValue('NewValue'))
    locCRepairTypeReason = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locCRepairTypeReason
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locCRepairTypeReason',p_web.GetValue('Value'))
    locCRepairTypeReason = p_web.GetValue('Value')
  End
  If locCRepairTypeReason = ''
    loc:Invalid = 'locCRepairTypeReason'
    loc:alert = p_web.translate('Reason For Chargeable Repair Type Change') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locCRepairTypeReason = Upper(locCRepairTypeReason)
    p_web.SetSessionValue('locCRepairTypeReason',locCRepairTypeReason)
  do Value::locCRepairTypeReason
  do SendAlert

Value::locCRepairTypeReason  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('locCRepairTypeReason') & '_value',Choose(p_web.GSV('Hide:CRepairType') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:CRepairType') = 1)
  ! --- TEXT --- locCRepairTypeReason
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locCRepairTypeReason')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locCRepairTypeReason = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locCRepairTypeReason'',''viewjob_loccrepairtypereason_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'readonly','')
  packet = clip(packet) & p_web.CreateTextArea('locCRepairTypeReason',p_web.GetSessionValue('locCRepairTypeReason'),5,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(locCRepairTypeReason),,Net:Web:Control) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewJob_' & p_web._nocolon('locCRepairTypeReason') & '_value')

Comment::locCRepairTypeReason  Routine
    loc:comment = p_web.Translate('')
  p_web._DivHeader('ViewJob_' & p_web._nocolon('locCRepairTypeReason') & '_comment',Choose(p_web.GSV('Hide:CRepairType') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:CRepairType') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locWChargeTypeReason  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('locWChargeTypeReason') & '_prompt',Choose(p_web.GSV('Hide:WChargeType') = 1,'hdiv','' & clip('FormPrompt2') & ''))
  loc:prompt = p_web.Translate('Reason For Warranty Charge Type Change')
  If p_web.GSV('Hide:WChargeType') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locWChargeTypeReason  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locWChargeTypeReason',p_web.GetValue('NewValue'))
    locWChargeTypeReason = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locWChargeTypeReason
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locWChargeTypeReason',p_web.GetValue('Value'))
    locWChargeTypeReason = p_web.GetValue('Value')
  End
  If locWChargeTypeReason = ''
    loc:Invalid = 'locWChargeTypeReason'
    loc:alert = p_web.translate('Reason For Warranty Charge Type Change') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locWChargeTypeReason = Upper(locWChargeTypeReason)
    p_web.SetSessionValue('locWChargeTypeReason',locWChargeTypeReason)
  do Value::locWChargeTypeReason
  do SendAlert

Value::locWChargeTypeReason  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('locWChargeTypeReason') & '_value',Choose(p_web.GSV('Hide:WChargeType') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:WChargeType') = 1)
  ! --- TEXT --- locWChargeTypeReason
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locWChargeTypeReason')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locWChargeTypeReason = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locWChargeTypeReason'',''viewjob_locwchargetypereason_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'readonly','')
  packet = clip(packet) & p_web.CreateTextArea('locWChargeTypeReason',p_web.GetSessionValue('locWChargeTypeReason'),5,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(locWChargeTypeReason),,Net:Web:Control) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewJob_' & p_web._nocolon('locWChargeTypeReason') & '_value')

Comment::locWChargeTypeReason  Routine
    loc:comment = p_web.Translate('')
  p_web._DivHeader('ViewJob_' & p_web._nocolon('locWChargeTypeReason') & '_comment',Choose(p_web.GSV('Hide:WChargeType') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:WChargeType') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locWRepairTypeReason  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('locWRepairTypeReason') & '_prompt',Choose(p_web.GSV('Hide:WRepairType') = 1,'hdiv','' & clip('FormPrompt2') & ''))
  loc:prompt = p_web.Translate('Reason For Warranty Repair Type Change')
  If p_web.GSV('Hide:WRepairType') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locWRepairTypeReason  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locWRepairTypeReason',p_web.GetValue('NewValue'))
    locWRepairTypeReason = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locWRepairTypeReason
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locWRepairTypeReason',p_web.GetValue('Value'))
    locWRepairTypeReason = p_web.GetValue('Value')
  End
  If locWRepairTypeReason = ''
    loc:Invalid = 'locWRepairTypeReason'
    loc:alert = p_web.translate('Reason For Warranty Repair Type Change') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locWRepairTypeReason = Upper(locWRepairTypeReason)
    p_web.SetSessionValue('locWRepairTypeReason',locWRepairTypeReason)
  do Value::locWRepairTypeReason
  do SendAlert

Value::locWRepairTypeReason  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('locWRepairTypeReason') & '_value',Choose(p_web.GSV('Hide:WRepairType') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:WRepairType') = 1)
  ! --- TEXT --- locWRepairTypeReason
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locWRepairTypeReason')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locWRepairTypeReason = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locWRepairTypeReason'',''viewjob_locwrepairtypereason_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'readonly','')
  packet = clip(packet) & p_web.CreateTextArea('locWRepairTypeReason',p_web.GetSessionValue('locWRepairTypeReason'),5,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(locWRepairTypeReason),,Net:Web:Control) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewJob_' & p_web._nocolon('locWRepairTypeReason') & '_value')

Comment::locWRepairTypeReason  Routine
    loc:comment = p_web.Translate('')
  p_web._DivHeader('ViewJob_' & p_web._nocolon('locWRepairTypeReason') & '_comment',Choose(p_web.GSV('Hide:WRepairType') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:WRepairType') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::textEstimateReady  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('textEstimateReady',p_web.GetValue('NewValue'))
    do Value::textEstimateReady
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::textEstimateReady  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('textEstimateReady') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate('Your estimate is now ready',) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::textEstimateReady  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('textEstimateReady') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonPrintEstimate  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonPrintEstimate',p_web.GetValue('NewValue'))
    do Value::buttonPrintEstimate
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
          p_web.SSV('GetStatus:Type','JOB')
          p_web.SSV('GetStatus:StatusNumber',520)
          getStatus(p_web)
  
          Access:JOBS.Clearkey(job:ref_Number_Key)
          job:ref_Number    = p_web.GSV('job:Ref_Number')
          if (Access:JOBS.TryFetch(job:ref_Number_Key) = Level:Benign)
              ! Found
              p_web.SessionQueueToFile(JOBS)
              access:JOBS.tryUpdate()
          else ! if (Access:JOBS.TryFetch(job:ref_Number_Key) = Level:Benign)
              ! Error
          end ! if (Access:JOBS.TryFetch(job:ref_Number_Key) = Level:Benign)
  
  
          Access:WEBJOB.Clearkey(wob:refNumberKey)
          wob:refNumber    = p_web.GSV('job:Ref_Number')
          if (Access:WEBJOB.TryFetch(wob:refNumberKey) = Level:Benign)
              ! Found
              p_web.SessionQueueToFile(WEBJOB)
              access:WEBJOB.tryUpdate()
          else ! if (Access:WEBJOB.TryFetch(wob:refNumberKey) = Level:Benign)
              ! Error
          end ! if (Access:WEBJOB.TryFetch(wob:refNumberKey) = Level:Benign)
  
  
          p_web.SSV('TotalPrice:Type','E')
          totalPrice(p_web)
  
          p_web.SSV('AddToAudit:Type','JOB')
          p_web.SSV('AddToAudit:Notes','ESTIMATE VALUE: ' & Format(p_web.GSV('TotalPrice:Total'),@n14.2))
          p_web.SSV('AddToAudit:Action','ESTIMATE SENT')
          addToAudit(p_web)
  do Value::buttonPrintEstimate
  do SendAlert
  do Value::job:Current_Status  !1

Value::buttonPrintEstimate  Routine
  p_web._DivHeader('ViewJob_' & p_web._nocolon('buttonPrintEstimate') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonPrintEstimate'',''viewjob_buttonprintestimate_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','PrintEstimate','Print Estimate','MainButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('Estimate?var=' & RANDOM(1,9999999))) & ''','''&clip('_blank')&''')',loc:javascript,0,'images/printer.png',,,,)

  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewJob_' & p_web._nocolon('buttonPrintEstimate') & '_value')

Comment::buttonPrintEstimate  Routine
    loc:comment = ''
  p_web._DivHeader('ViewJob_' & p_web._nocolon('buttonPrintEstimate') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('ViewJob_job:Current_Status_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Current_Status
      else
        do Value::job:Current_Status
      end
  of lower('ViewJob_jbn:Fault_Description_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jbn:Fault_Description
      else
        do Value::jbn:Fault_Description
      end
  of lower('ViewJob_jbn:Engineers_Notes_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jbn:Engineers_Notes
      else
        do Value::jbn:Engineers_Notes
      end
  of lower('ViewJob_jobe:Network_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe:Network
      else
        do Value::jobe:Network
      end
  of lower('ViewJob_job:Authority_Number_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Authority_Number
      else
        do Value::job:Authority_Number
      end
  of lower('ViewJob_job:Unit_Type_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Unit_Type
      else
        do Value::job:Unit_Type
      end
  of lower('ViewJob_buttonResendXML_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonResendXML
      else
        do Value::buttonResendXML
      end
  of lower('ViewJob_locCChargeTypeReason_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locCChargeTypeReason
      else
        do Value::locCChargeTypeReason
      end
  of lower('ViewJob_locCRepairTypeReason_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locCRepairTypeReason
      else
        do Value::locCRepairTypeReason
      end
  of lower('ViewJob_locWChargeTypeReason_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locWChargeTypeReason
      else
        do Value::locWChargeTypeReason
      end
  of lower('ViewJob_locWRepairTypeReason_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locWRepairTypeReason
      else
        do Value::locWRepairTypeReason
      end
  of lower('ViewJob_buttonPrintEstimate_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonPrintEstimate
      else
        do Value::buttonPrintEstimate
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('ViewJob_form:ready_',1)
  p_web.SetSessionValue('ViewJob_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_ViewJob',0)
  Access:WEBJOB.PrimeRecord()
  Ans = ChangeRecord
  p_web.SetSessionValue('ViewJob:Primed',1)

PreCopy  Routine
  p_web.SetValue('ViewJob_form:ready_',1)
  p_web.SetSessionValue('ViewJob_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_ViewJob',0)
  Access:WEBJOB.PrimeRecord()
  Ans = ChangeRecord
  p_web.SetSessionValue('ViewJob:Primed',1)
  p_web._PreCopyRecord(WEBJOB,wob:RecordNumberKey,Net:Web:Autonumbered)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('ViewJob_form:ready_',1)
  p_web.SetSessionValue('ViewJob_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('ViewJob:Primed',0)

PreDelete       Routine
  p_web.SetValue('ViewJob_form:ready_',1)
  p_web.SetSessionValue('ViewJob_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('ViewJob:Primed',0)
  p_web.setsessionvalue('showtab_ViewJob',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine
  If p_web.GSV('textBouncer') <> ''
  End
  If p_web.GSV('job:Warranty_Job') = 'YES' and p_web.GSV('job:Date_Completed') > 0
  End
  If p_web.GSV('Hide:CustomerClassification') <> 1
  End
  If p_web.GSV('job:Date_Completed') = 0
  End
  If p_web.GSV('Hide:CChargeType') <> 1 Or p_web.GSV('Hide:WChargeType') <> 1 Or p_web.GSV('Hide:CRepairType') <> 1 Or p_web.GSV('Hide:WRepairType') <> 1
  End
  If p_web.GSV('Hide:EstimateReady') = 0
  End

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  ! Validation
  if (p_web.GSV('job:Date_Completed') <> 0)
      p_web.SSV('CompulsoryFieldCheck:Type','C')
  else ! if (p_web.GSV('job:Date_Completed') <> 0)
      p_web.SSV('CompulsoryFieldCheck:Type','B')
  end !if (p_web.GSV('job:Date_Completed') <> 0)
        
  p_web.SSV('locCompleteRepair','')
  
        
    compulsoryFieldCheck(p_web)
        
  
  
  if (p_web.GSV('locCompleteRepair') <> '')
      p_web.SSV('locCompleteRepair','You cannot complete editing due to the following errors:<13,10>' & |
          p_web.GSV('locCompleteRepair'))
      loc:alert = 'You cannot complete editing. There are errors.'
      loc:invalid = 'locCompleteRepair'
      exit
  end ! if (p_web.GSV('locCompleteRepair') <> '')
  
  if (p_web.GSV('Hide:EstimateQuery') = 0 and p_web.GSV('locEstimateReadyOption') = 0)
      loc:alert = 'You selected Complete Repair and this job is an estimate. Choose an option'
      loc:invalid = 'locEstimateReadyOption'
      exit
  end ! if (p_web.GSV('Hide:EstimateQuery') = 0 and p_web.GSV('locEstimateReadyOption') = 0)
  
  p_web.SSV('Hide:CChargeType',1)
  p_web.SSV('Hide:WChargeType',1)
  p_web.SSV('Hide:CRepairType',1)
  p_web.SSV('Hide:WRepairType',1)
  
  
  If (p_web.GSV('job:Charge_Type') <> p_web.GSV('save:CChargeType') and p_web.GSV('save:CChargeType') <> '')
      if (p_web.GSV('locCChargeTypeReason') = '')
          loc:alert = 'Charge/Repair Type Has Changed. You must enter a reason for this change.'
          loc:invalid = 'locCChargeTypeReason'
          p_web.SSV('Hide:CChargeType',0)
      end !if (p_web.SSV('locCChargeTypeReason',''))
  end ! If (p_web.GSV('job:Charge_Type') <> p_web.GSV('save:CChargeType') and p_web.GSV('save:CChargeType') <> '')
  If (p_web.GSV('job:Warranty_Charge_Type') <> p_web.GSV('save:WChargeType') and p_web.GSV('save:WChargeType') <> '')
      if (p_web.GSV('locWChargeTypeReason') = '')
          loc:alert = 'Charge/Repair Type Has Changed. You must enter a reason for this change.'
          loc:invalid = 'locWChargeTypeReason'
          p_web.SSV('Hide:WChargeType',0)
      end ! if (p_web.SSV('locWChargeTypeReason',''))
  end ! If (p_web.GSV('job:Charge_Type') <> p_web.GSV('save:CChargeType') and p_web.GSV('save:CChargeType') <> '')
  If (p_web.GSV('job:Repair_Type') <> p_web.GSV('save:CRepairType') and p_web.GSV('save:CRepairType') <> '')
      if (p_web.GSV('locCRepairTypeReason') = '')
          loc:alert = 'Charge/Repair Type Has Changed. You must enter a reason for this change.'
          loc:invalid = 'locCChargeTypeReason'
          p_web.SSV('Hide:CRepairType',0)
      end ! if (p_web.SSV('locWRepairTypeReason',''))
  end ! If (p_web.GSV('job:Charge_Type') <> p_web.GSV('save:CChargeType') and p_web.GSV('save:CChargeType') <> '')
  If (p_web.GSV('job:Repair_Type_Warranty') <> p_web.GSV('save:WRepairType') and p_web.GSV('save:WRepairType') <> '')
      if (p_web.GSV('locWRepairTypeReason') = '')
          loc:alert = 'Charge/Repair Type Has Changed. You must enter a reason for this change.'
          loc:invalid = 'locWChargeTypeReason'
          p_web.SSV('Hide:WRepairType',0)
      end ! if (p_web.SSV('locWRepairTypeReason',''))
  end ! If (p_web.GSV('job:Charge_Type') <> p_web.GSV('save:CChargeType') and p_web.GSV('save:CChargeType') <> '')
  
  if (loc:invalid) <> ''
      exit
  end ! if (loc:invalid) <> ''
  
  do CompleteCheckBoxes
  do ValidateRecord
  ! Write Child Files
      do CheckParts
      do wayBillCheck
  
      saveJob(p_web)
  
      do didAnythingChange
      do haveAccessoriesChanged
  
  
  ! Inserting (DBH 16/09/2008) # 10253 - Update Date/Time Stamp
      UpdateDateTimeStamp(wob:RefNumber)
  ! End (DBH 16/09/2008) #10253
      do deleteVariables

ValidateDelete  Routine
  p_web.DeleteSessionValue('ViewJob_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('ViewJob_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 3
    loc:InvalidTab += 1
  ! tab = 1
    loc:InvalidTab += 1
  ! tab = 7
  If p_web.GSV('textBouncer') <> ''
    loc:InvalidTab += 1
  End
  ! tab = 2
    loc:InvalidTab += 1
      If not (p_web.GSV('locCurrentEngineer') = 'Not Allocated')
          job:Current_Status = Upper(job:Current_Status)
          p_web.SetSessionValue('job:Current_Status',job:Current_Status)
        If loc:Invalid <> '' then exit.
      End
          jbn:Engineers_Notes = Upper(jbn:Engineers_Notes)
          p_web.SetSessionValue('jbn:Engineers_Notes',jbn:Engineers_Notes)
        If loc:Invalid <> '' then exit.
  ! tab = 14
  If p_web.GSV('job:Warranty_Job') = 'YES' and p_web.GSV('job:Date_Completed') > 0
    loc:InvalidTab += 1
  End
  ! tab = 5
    loc:InvalidTab += 1
          jobe:Network = Upper(jobe:Network)
          p_web.SetSessionValue('jobe:Network',jobe:Network)
        If loc:Invalid <> '' then exit.
          job:Authority_Number = Upper(job:Authority_Number)
          p_web.SetSessionValue('job:Authority_Number',job:Authority_Number)
        If loc:Invalid <> '' then exit.
  ! tab = 6
    loc:InvalidTab += 1
  ! tab = 15
  If p_web.GSV('Hide:CustomerClassification') <> 1
    loc:InvalidTab += 1
  End
  ! tab = 11
    loc:InvalidTab += 1
  ! tab = 12
    loc:InvalidTab += 1
  ! tab = 8
  If p_web.GSV('job:Date_Completed') = 0
    loc:InvalidTab += 1
  End
  ! tab = 9
  If p_web.GSV('Hide:CChargeType') <> 1 Or p_web.GSV('Hide:WChargeType') <> 1 Or p_web.GSV('Hide:CRepairType') <> 1 Or p_web.GSV('Hide:WRepairType') <> 1
    loc:InvalidTab += 1
    If p_web.GSV('Hide:CChargeType') <> 1
      If not (p_web.GSV('Hide:CChargeType') = 1)
        If locCChargeTypeReason = ''
          loc:Invalid = 'locCChargeTypeReason'
          loc:alert = p_web.translate('Reason For Chargeable Charge Type Change') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locCChargeTypeReason = Upper(locCChargeTypeReason)
          p_web.SetSessionValue('locCChargeTypeReason',locCChargeTypeReason)
        If loc:Invalid <> '' then exit.
      End
    End
    If p_web.GSV('Hide:CRepairType') <> 1
      If not (p_web.GSV('Hide:CRepairType') = 1)
        If locCRepairTypeReason = ''
          loc:Invalid = 'locCRepairTypeReason'
          loc:alert = p_web.translate('Reason For Chargeable Repair Type Change') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locCRepairTypeReason = Upper(locCRepairTypeReason)
          p_web.SetSessionValue('locCRepairTypeReason',locCRepairTypeReason)
        If loc:Invalid <> '' then exit.
      End
    End
    If p_web.GSV('Hide:WChargeType') <> 1
      If not (p_web.GSV('Hide:WChargeType') = 1)
        If locWChargeTypeReason = ''
          loc:Invalid = 'locWChargeTypeReason'
          loc:alert = p_web.translate('Reason For Warranty Charge Type Change') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locWChargeTypeReason = Upper(locWChargeTypeReason)
          p_web.SetSessionValue('locWChargeTypeReason',locWChargeTypeReason)
        If loc:Invalid <> '' then exit.
      End
    End
    If p_web.GSV('Hide:WRepairType') <> 1
      If not (p_web.GSV('Hide:WRepairType') = 1)
        If locWRepairTypeReason = ''
          loc:Invalid = 'locWRepairTypeReason'
          loc:alert = p_web.translate('Reason For Warranty Repair Type Change') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locWRepairTypeReason = Upper(locWRepairTypeReason)
          p_web.SetSessionValue('locWRepairTypeReason',locWRepairTypeReason)
        If loc:Invalid <> '' then exit.
      End
    End
  End
  ! tab = 10
  If p_web.GSV('Hide:EstimateReady') = 0
    loc:InvalidTab += 1
  End
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostInsert      Routine

PostCopy        Routine
  p_web.SetSessionValue('ViewJob:Primed',0)

PostUpdate      Routine
  p_web.SetSessionValue('ViewJob:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locEngineeringOption')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('job:Order_Number')
  p_web.StoreValue('')
  p_web.StoreValue('job:ESN')
  p_web.StoreValue('job:MSN')
  p_web.StoreValue('locJobType')
  p_web.StoreValue('')
  p_web.StoreValue('job:Charge_Type')
  p_web.StoreValue('job:Repair_Type')
  p_web.StoreValue('job:Warranty_Charge_Type')
  p_web.StoreValue('job:Repair_Type_Warranty')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('job:Current_Status')
  p_web.StoreValue('job:Location')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('job:Exchange_Status')
  p_web.StoreValue('job:Loan_Status')
  p_web.StoreValue('')
  p_web.StoreValue('jbn:Fault_Description')
  p_web.StoreValue('jbn:Engineers_Notes')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locHOClaimStatus')
  p_web.StoreValue('locRRCClaimStatus')
  p_web.StoreValue('')
  p_web.StoreValue('locCurrentEngineer')
  p_web.StoreValue('')
  p_web.StoreValue('jobe:Network')
  p_web.StoreValue('job:Authority_Number')
  p_web.StoreValue('job:Unit_Type')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locMobileLifetimeValue')
  p_web.StoreValue('locMobileAverageSpend')
  p_web.StoreValue('locMobileLoyaltyStatus')
  p_web.StoreValue('locMobileUpgradeDate')
  p_web.StoreValue('locMobileIDNumber')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locCompleteRepair')
  p_web.StoreValue('')
  p_web.StoreValue('locCChargeTypeReason')
  p_web.StoreValue('locCRepairTypeReason')
  p_web.StoreValue('locWChargeTypeReason')
  p_web.StoreValue('locWRepairTypeReason')
  p_web.StoreValue('')
  p_web.StoreValue('')

PostDelete      Routine
ShowAlert     Procedure(String fAlert)
Code
    packet = clip(packet) & '<script language="JavaScript" type="text/javascript">alert("' & clip(fAlert) & '")</script>'
    do sendPacket
showHideCreateInvoice       PROCEDURE()
    CODE
        IF (p_web.gsv('job:Chargeable_Job') <> 'YES')
            RETURN TRUE
        END
        
        SentToHub(p_web)
        IF (p_web.GSV('BookingSite') = 'ARC' AND p_web.GSV('SentToHub') <> 1)
            RETURN TRUE
        END
        
        IF (p_web.GSV('job:Bouncer') = 'X')
            RETURN TRUE
        END
        
        ! Job not completed
        IF (p_web.GSV('job:Date_Completed') = 0 AND p_web.GSV('job:Exchange_Unit_Number') = 0)
            RETURN TRUE
        END
        
        ! Job has not been priced
        IF (p_web.GSV('job:ignore_Chargeable_Charges') = 'YES')
            IF (p_web.GSV('BookingSite') = 'RRC')
                IF (p_web.GSV('jobe:RRCSubTotal') = 0)
                    RETURN TRUE
                END
            ELSE
                IF (p_web.GSV('job:Sub_Total') = 0)
                    RETURN TRUE
                END
                
            END
        END
        p_web.SSV('URL:CreateInvoice','CreateInvoice?returnURL=ViewJob')
        p_web.SSV('URL:CreateInvoiceTarget','_self')
        p_web.SSV('URL:CreateInvoiceText','Create Invoice')
 
        IF (p_web.GSV('job:Invoice_Number') > 0)
            Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
            inv:Invoice_Number = p_web.GSV('job:Invoice_Number')
            IF (Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign)
                IF (inv:Invoice_Type <> 'SIN')
                    RETURN TRUE
                END
                ! If job has been invoiced, just print the invoice, rather than asking to create one
                IF (p_web.GSV('BookingSite') = 'RRC')
                    IF (inv:RRCInvoiceDate > 0)
                        p_web.SSV('URL:CreateInvoice','InvoiceNote?var=' & RANDOM(1,9999999))
                        p_web.SSV('URL:CreateInvoiceTarget','_blank')
                        p_web.SSV('URL:CreateInvoiceText','Print Invoice')
                    END
                ELSE
                    If (inv:ARCInvoiceDate > 0)
                        p_web.SSV('URL:CreateInvoice','InvoiceNote?var=' & RANDOM(1,9999999))
                        p_web.SSV('URL:CreateInvoiceTarget','_blank')
                        p_web.SSV('URL:CreateInvoiceText','Print Invoice')
                    END
                END
            END
            
        END
        
        
useStockAllocation   PROCEDURE  (fLocation)                ! Declare Procedure
FilesOpened     BYTE(0)
  CODE
    do openFiles

    retValue# = 0

    Access:LOCATION.Clearkey(loc:location_Key)
    loc:location    = fLocation
    if (Access:LOCATION.TryFetch(loc:location_Key) = Level:Benign)
        ! Found
        if (loc:useRapidStock)
            retValue# = 1
        end !if (loc:useRapidStock)
    else ! if (Access:LOCATION.TryFetch(loc:location_Key) = Level:Benign)
        ! Error
    end ! if (Access:LOCATION.TryFetch(loc:location_Key) = Level:Benign)

    do closeFiles

    return retValue#
!--------------------------------------
OpenFiles  ROUTINE
  Access:LOCATION.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:LOCATION.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:LOCATION.Close
     FilesOpened = False
  END
