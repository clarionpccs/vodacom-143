

   MEMBER('archive.clw')                              ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('ARCHI002.INC'),ONCE        !Local module procedure declarations
                     END


XFiles PROCEDURE                                      !Generated from procedure template - Window

QuickWindow          WINDOW('Window'),AT(,,260,160),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,HLP('XFiles'),SYSTEM,GRAY,RESIZE
                       BUTTON('&OK'),AT(140,140,56,16),USE(?Ok),MSG('Accept operation'),TIP('Accept Operation')
                       BUTTON('&Cancel'),AT(200,140,56,16),USE(?Cancel),MSG('Cancel Operation'),TIP('Cancel Operation')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  0{prop:Text} = 'ServiceBase 3g'
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('XFiles')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Ok
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Ok,RequestCancelled)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:ARCAUDIT.Open
  Relate:ARCAUDIT2.Open
  Relate:ARCAUDSTATS.Open
  Relate:ARCCONTHIST.Open
  Relate:ARCDEFAULTV.Open
  Relate:ARCESTPARTS.Open
  Relate:ARCJOBACC.Open
  Relate:ARCJOBACCNO.Open
  Relate:ARCJOBEXHIS.Open
  Relate:ARCJOBLOHIS.Open
  Relate:ARCJOBOUTFL.Open
  Relate:ARCJOBS.Open
  Relate:ARCJOBSCONS.Open
  Relate:ARCJOBSE.Open
  Relate:ARCJOBSE2.Open
  Relate:ARCJOBSENG.Open
  Relate:ARCJOBSOBF.Open
  Relate:ARCJOBSTAGE.Open
  Relate:ARCJOBSTAMP.Open
  Relate:ARCJOBSWARR.Open
  Relate:ARCJOBTHIRD.Open
  Relate:ARCLOCATLOG.Open
  Relate:ARCPARTS.Open
  Relate:ARCSMSMAIL.Open
  Relate:ARCWARPARTS.Open
  Relate:ARCWEBJOB.Open
  Relate:AUDIT.Open
  Relate:AUDIT2.Open
  Relate:AUDSTATS.Open
  Relate:CONTHIST.Open
  Relate:DEFAULTV.Open
  Relate:ESTPARTS.Open
  Relate:JOBACC.Open
  Relate:JOBACCNO.Open
  Relate:JOBEXHIS.Open
  Relate:JOBLOHIS.Open
  Relate:JOBOUTFL.Open
  Relate:JOBS.Open
  Relate:JOBSCONS.Open
  Relate:JOBSE.Open
  Relate:JOBSE2.Open
  Relate:JOBSENG.Open
  Relate:JOBSOBF.Open
  Relate:JOBSTAGE.Open
  Relate:JOBSTAMP.Open
  Relate:JOBSWARR.Open
  Relate:JOBTHIRD.Open
  Relate:LOCATLOG.Open
  Relate:PARTS.Open
  Relate:SMSMAIL.Open
  Relate:WARPARTS.Open
  Relate:WEBJOB.Open
  SELF.FilesOpened = True
  OPEN(QuickWindow)
  SELF.Opened=True
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ARCAUDIT.Close
    Relate:ARCAUDIT2.Close
    Relate:ARCAUDSTATS.Close
    Relate:ARCCONTHIST.Close
    Relate:ARCDEFAULTV.Close
    Relate:ARCESTPARTS.Close
    Relate:ARCJOBACC.Close
    Relate:ARCJOBACCNO.Close
    Relate:ARCJOBEXHIS.Close
    Relate:ARCJOBLOHIS.Close
    Relate:ARCJOBOUTFL.Close
    Relate:ARCJOBS.Close
    Relate:ARCJOBSCONS.Close
    Relate:ARCJOBSE.Close
    Relate:ARCJOBSE2.Close
    Relate:ARCJOBSENG.Close
    Relate:ARCJOBSOBF.Close
    Relate:ARCJOBSTAGE.Close
    Relate:ARCJOBSTAMP.Close
    Relate:ARCJOBSWARR.Close
    Relate:ARCJOBTHIRD.Close
    Relate:ARCLOCATLOG.Close
    Relate:ARCPARTS.Close
    Relate:ARCSMSMAIL.Close
    Relate:ARCWARPARTS.Close
    Relate:ARCWEBJOB.Close
    Relate:AUDIT.Close
    Relate:AUDIT2.Close
    Relate:AUDSTATS.Close
    Relate:CONTHIST.Close
    Relate:DEFAULTV.Close
    Relate:ESTPARTS.Close
    Relate:JOBACC.Close
    Relate:JOBACCNO.Close
    Relate:JOBEXHIS.Close
    Relate:JOBLOHIS.Close
    Relate:JOBOUTFL.Close
    Relate:JOBS.Close
    Relate:JOBSCONS.Close
    Relate:JOBSE.Close
    Relate:JOBSE2.Close
    Relate:JOBSENG.Close
    Relate:JOBSOBF.Close
    Relate:JOBSTAGE.Close
    Relate:JOBSTAMP.Close
    Relate:JOBSWARR.Close
    Relate:JOBTHIRD.Close
    Relate:LOCATLOG.Close
    Relate:PARTS.Close
    Relate:SMSMAIL.Close
    Relate:WARPARTS.Close
    Relate:WEBJOB.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

