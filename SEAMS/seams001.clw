

   MEMBER('seams.clw')                                ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('NetSimp.inc'),ONCE

                     MAP
                       INCLUDE('SEAMS001.INC'),ONCE        !Local module procedure declarations
                     END


Main PROCEDURE                                        !Generated from procedure template - Window

window               WINDOW('SEAMS CSI'),AT(,,240,140),FONT('Tahoma',8,,FONT:regular),CENTER,WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(5,5,230,130),USE(?Panel1),FILL(09A6A7CH)
                       BUTTON('Run for Yesterday'),AT(80,19,68,16),USE(?ButtonRUN)
                       BUTTON('Run Another Day'),AT(80,48,68,16),USE(?ButtonRunAnotherDay)
                       BUTTON('SET UP'),AT(80,77,68,16),USE(?ButtonSetup)
                       BUTTON('EXIT'),AT(80,106,68,16),USE(?ButtonEXIT)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

ReadIni     Routine              !all read into globals ready for use

    if not exists(clip(path())&'\seams.ini') then do PrimeINI.

    Branch[1] = getini('Branches','Branch1','',clip(path())&'\seams.ini') !'FA32' Vodacom Repairs Sandton
    Branch[2] = getini('Branches','Branch2','',clip(path())&'\seams.ini') !'FA51' Vodacom Repairs Menlyn
    Branch[3] = getini('Branches','Branch3','',clip(path())&'\seams.ini') !'FA50' Vodacom Hicell Mall@Reds


    FTPAddress    =  getini('Connect','Address','',clip(path())&'\seams.ini')
    Username      =  getini('Connect','UserName','',clip(path())&'\seams.ini')
    UserPassword  =  getini('Connect','Password','',clip(path())&'\seams.ini')
    LocationPath  =  getini('Connect','LocationPath','',clip(path())&'\seams.ini')

    EXIT


PrimeIni    Routine


    putini('Branches','Branch1','FA32',clip(path())&'\seams.ini')  
    putini('Branches','Branch2','FA51',clip(path())&'\seams.ini')
    putini('Branches','Branch3','FA50',clip(path())&'\seams.ini')


    putini('Connect','Address','10.132.45.150',clip(path())&'\seams.ini') 
    putini('Connect','UserName','PSQLSA',clip(path())&'\seams.ini')       
    putini('Connect','Password','Vodacom@101',clip(path())&'\seams.ini')  
    putini('Connect','LocationPath','\SeamsDataDump\ServiceBase\New',clip(path())&'\seams.ini')

    !WriteSeamsLog('INI file primed with default settings on entry')
    WriteSeamsLog('INI file primed with default settings on entry')

    EXIT


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Panel1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:JOBS.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  OpenSeamsLog(clip(path())&'\ServiceBase_Seams.log')
  WriteSeamsLog('===============================================================================')
  WriteSeamsLog(format(today(),@D06)&' at '& format(clock(),@t04) & ' System entry')
  If command('/AUTO') then
      if not exists(clip(path())&'\seams.ini') then
          message('You must set up the system before running in auto mode')
          WriteSeamsLog('Auto mode requested, but no setup has been made, Aborting')
      ELSE
          AutoMode = true
          do readini
          RunForDay(Today()-1)
          WriteSeamsLog('Export completed - closing down')
      END
      !if exists(ExportFileName) then remove(ExportFileName).
      CloseSeamsLog()
      post(event:closeWindow)
  ELSE
      AutoMode = false
      Do Readini
  END !If command /auto
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?ButtonEXIT
      WriteSeamsLog('Manual exit from system - closing down')
      CloseSeamsLog()
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonRUN
      ThisWindow.Update
      RunForDay(Today()-1)
    OF ?ButtonRunAnotherDay
      ThisWindow.Update
      SelectDay
      ThisWindow.Reset
    OF ?ButtonSetup
      ThisWindow.Update
      IniSetup
      ThisWindow.Reset
    OF ?ButtonEXIT
      ThisWindow.Update
       POST(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

IniSetup PROCEDURE                                    !Generated from procedure template - Window

window               WINDOW('Setup'),AT(,,240,140),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(5,5,230,130),USE(?Panel1),FILL(09A6A7CH)
                       PROMPT('Branch1:'),AT(12,10),USE(?Branch:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       ENTRY(@s4),AT(72,10,58,10),USE(Branch[1]),FONT(,,,FONT:bold,CHARSET:ANSI)
                       PROMPT('Branch2:'),AT(12,24),USE(?Branch:Prompt:2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       ENTRY(@s4),AT(72,24,58,10),USE(Branch[2]),FONT(,,,FONT:bold,CHARSET:ANSI)
                       PROMPT('Branch3:'),AT(12,38),USE(?Branch:Prompt:3),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       ENTRY(@s4),AT(72,38,58,10),USE(Branch[3]),FONT(,,,FONT:bold,CHARSET:ANSI)
                       PROMPT('FTP Address:'),AT(12,52),USE(?FTPAddress:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       ENTRY(@s100),AT(72,52,126,10),USE(FTPAddress),FONT(,,,FONT:bold,CHARSET:ANSI)
                       PROMPT('Username:'),AT(12,66),USE(?Username:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       ENTRY(@s20),AT(72,66,126,10),USE(Username),FONT(,,,FONT:bold,CHARSET:ANSI)
                       PROMPT('User Password:'),AT(12,80),USE(?UserPassword:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       ENTRY(@s20),AT(72,80,126,10),USE(UserPassword),FONT(,,,FONT:bold,CHARSET:ANSI)
                       PROMPT('Location Path:'),AT(12,94),USE(?LocationPath:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       ENTRY(@s255),AT(72,94,126,10),USE(LocationPath),FONT(,,,FONT:bold,CHARSET:ANSI)
                       BUTTON('...'),AT(206,94,12,12),USE(?LookupFile)
                       BUTTON('Cancel'),AT(72,114,56,16),USE(?ButtonCancel)
                       BUTTON('OK'),AT(144,114,56,16),USE(?ButtonOK)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FileLookup3          SelectFileClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

WriteINI       routine

    putini('Branches','Branch1',Branch[1],clip(path())&'\seams.ini') !'FA32' Vodacom Repairs Sandton
    putini('Branches','Branch2',Branch[2],clip(path())&'\seams.ini') !'FA51' Vodacom Repairs Menlyn
    putini('Branches','Branch3',Branch[3],clip(path())&'\seams.ini') !'FA50' Vodacom Hicell Mall@Reds

    putini('Connect','Address',FTPAddress,clip(path())&'\seams.ini')
    putini('Connect','UserName',Username,clip(path())&'\seams.ini')
    putini('Connect','Password',UserPassword,clip(path())&'\seams.ini')
    putini('Connect','LocationPath',LocationPath,clip(path())&'\seams.ini')

    WriteSeamsLog('INI file updated with settings ')
    WriteSeamsLog('Branches: '&Branch[1]&', '&branch[2]&', '&Branch[3])
    WriteSeamsLog('Address: '&clip(FTPAddress))
    WriteSeamsLog('UserName: '&clip(Username))
    WriteSeamsLog('Password: '&clip(userpassword))
    WriteSeamsLog('Path: '&clip(LocationPath))

    EXIT

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('IniSetup')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Panel1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(window)
  SELF.Opened=True
  FileLookup3.Init
  FileLookup3.Flags=BOR(FileLookup3.Flags,FILE:LongName)
  FileLookup3.Flags=BOR(FileLookup3.Flags,FILE:Directory)
  FileLookup3.SetMask('All Files','*.*')
  FileLookup3.WindowTitle='Save Folder'
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?ButtonCancel
      WriteSeamsLog('Settings left unchanged on exit')
    OF ?ButtonOK
      Do WriteINI
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?LookupFile
      ThisWindow.Update
      LocationPath = FileLookup3.Ask(1)
      DISPLAY
    OF ?ButtonCancel
      ThisWindow.Update
       POST(Event:CloseWindow)
    OF ?ButtonOK
      ThisWindow.Update
       POST(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

RunForDay            PROCEDURE  (SentDay)             ! Declare Procedure
BranchMatch          STRING(20)
TempDateString       STRING(10)
Count                LONG
TransactionType      STRING(30)
Counsultant          STRING(60)
RememberRefNumber    LONG
i                    LONG
ExportFile    File,Driver('ASCII'),Pre(Expx),Name(ExportFileName),Create,Bindable,Thread
Record              Record
ExpLine               String(1000)
                    End
                End

  CODE
   Relate:JOBS.Open
   Relate:JOBSE.Open
   Relate:WEBJOB.Open
   Relate:TRADEACC.Open
   Relate:USERS.Open
   Relate:AUDIT.Open
!(SentDay)

    Do ReadIni

    !check for local folder
    if not exists(LocationPath) then

        if ~AutoMode then
            message('The Local path for saving the export '&clip(LocationPath)&' does not exist. Please set this up before you try again.')
        END
        WriteSeamsLog('Local path set at: '&clip(LocationPath)&' does not exist - Aborting Export')

    ELSE

        Count = 0

        !Despatched on the day sent - want job despatched (Version 1)
        Access:jobs.clearkey(job:DateDespatchKey)
        job:Courier = 'COLLECTED'
        job:Date_Despatched = SentDay
        set(job:DateDespatchKey,job:DateDespatchKey)
        Loop

            if access:jobs.next() then break.
            if job:Courier <> 'COLLECTED' then break.
            if job:Date_Despatched <> SentDay then break.

            !look up account details and filter unwanted
            Access:Webjob.clearkey(wob:RefNumberKey)
            wob:RefNumber = job:Ref_number
            if access:Webjob.fetch(wob:RefNumberKey) then cycle.

            !matches one of the three stores
            if instring(clip(wob:HeadAccountNumber),BranchMatch,1,1) = 0 then cycle.

            Do TheRestOfIt

        END !loop through jobs by datecompleted

        !Despatched on the day sent - want exchange dispatched    (version 2)
        Access:jobs.clearkey(job:DateDespExcKey)
        job:Exchange_Courier = 'COLLECTED'
        job:Exchange_Despatched = SentDay
        set(job:DateDespExcKey,job:DateDespExcKey)
        Loop

            if access:jobs.next() then break.
            if job:Exchange_Courier <> 'COLLECTED' then break.
            if job:Exchange_Despatched <> SentDay then break.

            !look up account details and filter unwanted
            Access:Webjob.clearkey(wob:RefNumberKey)
            wob:RefNumber = job:Ref_number
            if access:Webjob.fetch(wob:RefNumberKey) then cycle.

            !matches one of the three stores
            if instring(clip(wob:HeadAccountNumber),BranchMatch,1,1) = 0 then cycle.

            Do TheRestOfIt

        END !loop through jobs by job:DateDespExcKey


        !now we want the webjobs for RRC despatched jobs (Exchanges will have been caught in version 2 above
        !DespatchCourierKey (HeadAccountNumber, readyToDespatch, DespatchCourier,Refnumber)
        !first get an esitmate of a Ref number from a month ago
        Access:Jobs.clearkey(job:DateCompletedKey)
        job:Date_Completed = SentDay - 30
        Set(job:DateCompletedKey,job:DateCompletedKey)
        if Access:jobs.next() then
            !eh - nothing in the last thirty days?
        END
        RememberRefNumber = Job:Ref_number

        Loop i = 1 to 3
            if clip(Branch[i]) <> '' then
                !now to look for jobs despatch with courier 'COLLECTED' by the branches after this record
                Access:Webjob.clearkey(wob:DespatchCourierKey)
                wob:HeadAccountNumber = Branch[i]
                wob:readyToDespatch   = 0
                wob:DespatchCourier   = 'COLLECTED'
                wob:Refnumber         = RememberRefNumber
                Set(wob:DespatchCourierKey,wob:DespatchCourierKey)
                Loop

                    if access:Webjob.next() then break.
                    if wob:HeadAccountNumber <> Branch[i] then break.
                    if wob:ReadyToDespatch <> 0 then break.

                    !But was it despatched on the right day?
                    if wob:DateJobDespatched <> SentDay then cycle.

                    Access:jobs.clearkey(job:Ref_Number_Key)
                    job:Ref_Number = wob:RefNumber
                    if access:jobs.fetch(job:Ref_Number_Key) then cycle.

                    Do TheRestOfIt

                END !loop through webjobs on Despatch Courier key
            END !if branch[i] is not blank
        END !loop i = 1 to 3

        close(ExportFile)

        WriteSeamsLog('Export completed at '& format(clock(),@t4))
        WriteSeamsLog(clip(Count)&' Records exported.')
        
        SendFile

        if not(AutoMode) then message('Export completed').

    END !if LocationPath does not exist



TheRestOfIt     Routine

    !common bit for job despatched and exchange dispatched

    !get trade account details from WOB
    access:tradeacc.clearkey(tra:Account_Number_Key)
    tra:Account_Number = wob:HeadAccountNumber
    if access:tradeacc.fetch(tra:Account_Number_Key) then
        tra:Account_Number = 'NOT FOUND'
        tra:Company_Name   = 'NOT FOUND'
    END

    
    !look up user details from JOB
    if Job:Who_Booked = 'WEB' then
        !fake up the details
        Use:Forename = 'VCP'
        Use:Surname  = ''
        Use:User_Level = 'N/A'
        Use:user_Code = 'WEB'
    ELSE
        if clip(Job:who_booked) = ''
            !try to find this in the audit trail
            !Keys have the date and time in descending order - so I need the last one for this job
            Access:Audit.clearkey(aud:Ref_Number_Key)
            aud:Ref_Number = Job:Ref_number
            aud:Date = Today() + 1
            aud:Time = 0
            Set(aud:Ref_Number_Key,aud:Ref_Number_Key)
            Loop
                if access:Audit.next() then break.
                if aud:Ref_Number <> Job:Ref_number then break.
                Job:Who_Booked = aud:User  !this will remember the last one before the ref_number changed
            END !loop through audit trail
        END
        Access:users.clearkey(use:User_Code_Key)
        use:User_Code = job:who_Booked
        if access:users.fetch(use:User_Code_Key) then
            use:Forename   = 'NOT'
            use:Surname    = 'FOUND'
            use:User_Level = 'UNKNOWN'
        END
    END

    Counsultant = clip(use:Forename)&' '&clip(use:Surname)

    !make up TransactionType Warranty_Charge_type or if that is empty use Charge_type
    if clip(job:Warranty_Charge_type) = '' then
        TransactionType = job:Charge_type
    else
        TransactionType = job:Warranty_Charge_type
    end

    !send it out
    Expx:ExpLine =  clip(Job:mobile_number)     &','&|  !MSISDN
                    clip(tra:Account_Number)    &','&|  !Branch ID
                    clip(tra:Company_Name)      &','&|  !Store
                    clip(use:User_Code)         &','&|  !User ID
                    clip(Counsultant)           &','&|  !Consultant
                    clip(use:User_Level)        &','&|  !Access Level
                    clip(TransactionType)       &','&|  !Transaction
                    'COLLECTED'                         !Outgoinig Courier

    Add(ExportFile)
   
    Count += 1

    EXIT


ReadIni     Routine

    if AutoMode then
        WriteSeamsLog('Export running in Automatic mode, by scheduled task.')
    ELSE
        WriteSeamsLog('Export being run manually by a user.')
    END
    WriteSeamsLog('Running for date: '&format(sentDay,@D06))

    !look up the details required
    Branch[1] = getini('Branches','Branch1','FA32',clip(path())&'\seams.ini') !'FA32' Vodacom Repairs Sandton
    Branch[2] = getini('Branches','Branch2','FA51',clip(path())&'\seams.ini') !'FA51' Vodacom Repairs Menlyn
    Branch[3] = getini('Branches','Branch3','FA50',clip(path())&'\seams.ini') !'FA50' Vodacom Hicell Mall@Reds

    BranchMatch = '  ' & clip(Branch[1])&'  '&clip(Branch[2])&'  '&clip(Branch[3]&' ')

    FTPAddress    =  getini('Connect','Address','10.132.45.150',clip(path())&'\seams.ini')  
    Username      =  getini('Connect','UserName','PSQLSA',clip(path())&'\seams.ini')        
    UserPassword  =  getini('Connect','Password','Vodacom@101',clip(path())&'\seams.ini')   
    LocationPath  =  getini('Connect','LocationPath','E:\SeamsDataDump\ServiceBase\New',clip(path())&'\seams.ini')

    !Split the ftp address to site and folder
    Count = instring('/',FTPAddress,1,1)
    if count = 0 then
        FTPSite = FTPAddress
        FTPFolder = ''
    ELSE
        FTPSite = FTPAddress[1: Count - 1]
        FTPFolder = FTPAddress[ Count : len(clip(FTPAddress)) ]
    END

    WriteSeamsLog('Local saving path set to '&clip(LocationPath))
    WriteSeamsLog('Upload set to site='&clip(FTPSite)&' Folder='&clip(FTPFolder))

    TempDateString = format(Today(),@d06)   !dd/mm/yyyy with zeros if needed
    ShortFileName  = 'ServiceBase' & TempDateString[1:2] & TempDateString[4:5] & TempDateString[7:10] & format(clock(),@t5) &'.csv'
    !they asked for DDMMYYYY - this will be used for the transfer name

    ExportFileName = clip(LocationPath)&'\'& clip(ShortFilename)

    Create(ExportFile)
    Open(ExportFile)

    Expx:ExpLine =  'MSISDN,Branch ID,Store,User ID,Consultant,Access Level,Transaction,Outgoing Courier'
    Add(ExportFile)
    
    EXIT
   Relate:JOBS.Close
   Relate:JOBSE.Close
   Relate:WEBJOB.Close
   Relate:TRADEACC.Close
   Relate:USERS.Close
   Relate:AUDIT.Close
SelectDay PROCEDURE                                   !Generated from procedure template - Window

ChoosingDate         DATE
window               WINDOW('Select Day for Export'),AT(,,240,140),FONT('Tahoma',8,,FONT:regular),CENTER,WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(5,5,230,130),USE(?Panel1),FILL(09A6A7CH)
                       BUTTON('...'),AT(204,30,16,14),USE(?PopCalendar)
                       PROMPT('Choose Date:'),AT(16,32),USE(?ChoosingDate:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       ENTRY(@d17),AT(76,32,116,10),USE(ChoosingDate),FONT(,,,FONT:bold,CHARSET:ANSI)
                       BUTTON('OK'),AT(108,114,56,16),USE(?ButtonOK),DEFAULT
                       BUTTON('Cancel'),AT(168,114,56,16),USE(?ButtonCancel)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('SelectDay')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Panel1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(window)
  SELF.Opened=True
  WriteSeamsLog('Entry into date selection routine.')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?ButtonOK
      if ChoosingDate = '' then cycle.
    OF ?ButtonCancel
      WriteSeamsLog('Date not selected for export')
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          ChoosingDate = TINCALENDARStyle1()
          Display(?ChoosingDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?ButtonOK
      ThisWindow.Update
      RunForDay(ChoosingDate)
      ThisWindow.Reset
       POST(Event:CloseWindow)
    OF ?ButtonCancel
      ThisWindow.Update
       POST(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

SendFile PROCEDURE                                    !Generated from procedure template - Window

window               WINDOW('Sending File'),AT(,,65,48),FONT('Tahoma',8,,FONT:regular),CENTER,WALLPAPER('sbback.jpg'),TILED,GRAY,ICONIZE,DOUBLE
                       BUTTON('Abort'),AT(4,4,56,16),USE(?ButtonAbort)
                       BUTTON('Send'),AT(4,26,56,16),USE(?ButtonSend)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Local Data Classes
ThisFTPControl       CLASS(NetFTPClientControl)       !Generated by NetTalk Extension (Class Definition)
Done                   PROCEDURE(),DERIVED
ErrorTrap              PROCEDURE(string errorStr,string functionName),DERIVED

                     END

!Local Data Classes
MyFTPData            CLASS(NetFTPClientData)          !Generated by NetTalk Extension (Class Definition)

                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('SendFile')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonAbort
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(window)
  SELF.Opened=True
  !WriteSeamsLog('Enry to ftp routine')
  !do PutFile
  Post(event:Accepted,?ButtonSend)
                                               ! Generated by NetTalk Extension (Start)
  ThisFTPControl.SuppressErrorMsg = 1         ! No Object Generated Error Messages ! Generated by NetTalk Extension
  ThisFTPControl.DataConnection &= MyFTPData ! FTP needs _2_ objects. Check the settings on the "Settings" Tab
  ThisFTPControl.init(NET:SimpleClient)
  if ThisFTPControl.error <> 0
    ! Put code in here to handle if the object does not initialise properly
  end
                                               ! Generated by NetTalk Extension (Start)
  MyFTPData.ControlConnection &= ThisFTPControl ! FTP needs _2_ objects. Check the settings on the "Settings" Tab
  MyFTPData.init(NET:SimpleServer)
  if MyFTPData.error <> 0
    ! Put code in here to handle if the object does not initialise properly
  end
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ThisFTPControl.Kill()                              ! Generated by NetTalk Extension
  MyFTPData.Kill()                              ! Generated by NetTalk Extension
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonAbort
      ThisWindow.Update
       POST(Event:CloseWindow)
    OF ?ButtonSend
      ThisWindow.Update
      ! This is the code to send a remote file from the FTP server.
      ! The first 5 lines you just need to do once, to set up the
      ! properties so the object knows who to connect to.
      ! This FTP object will automatically log in to the FTP
      ! server, the first time you call one of the action methods
      ! like the ChangeDir() or GetRemoteFile() method.
          ThisFTPControl.ServerHost = FTPSite
          ThisFTPControl.User = Username
          ThisFTPControl.Password = UserPassword
          ThisFTPControl.OpenNewControlConnection = 1
          ThisFTPControl.BinaryTransfer = 1
      
          if clip(FTPFolder) <> '' then
              ThisFTPControl.ChangeDir(FTPFolder) ! Change to this Directory (will also send the file)
          ELSE
              ThisFTPControl.PutFile(ExportFileName,ShortFileName)    !just send the file
          END
          
      
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    ThisFTPControl.TakeEvent()                 ! Generated by NetTalk Extension
    MyFTPData.TakeEvent()                 ! Generated by NetTalk Extension
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisFTPControl.Done PROCEDURE


  CODE
  PARENT.Done
  ! Put code after the parent call if you want the log file information
  ! to be more accurate and useful.
  
  case lower(clip(self._Command))
  of 'aborting'
  of 'appendtofile'
  of 'changedir'
    ! This is the code to get a remote file. 
    ! We do this after the change directory has worked
    WriteSeamsLog('FTP ChangeDir command implimented')
    !self.BinaryTransfer = 0 ! Do not use this line for EXE or ZIP files !!
    self.PutFile(ExportFileName,ShortFileName)
    ! You could change this method call to call one of the other transactions
    ! like PutFile() etc.
  of 'changedirup'
  of 'deletefile'
  of 'getcurrentdir'
  of 'getdirlisting'
  of 'getremotefile'
  of 'makedir'
  of 'noop'
  of 'putfile'
      WriteSeamsLog('FTP file transfer completed')
      self.quit()
      Post(Event:CloseWindow)
  of 'removedir'
  of 'rename'
  of 'quit'
      WriteSeamsLog('FTP connection successfully closed')
  end


ThisFTPControl.ErrorTrap PROCEDURE(string errorStr,string functionName)


  CODE
      WriteSeamsLog('FTP Error: ' & Clip(self._Command) & ' - ' & (ErrorStr))
      Post(Event:CloseWindow)
  PARENT.ErrorTrap(errorStr,functionName)

