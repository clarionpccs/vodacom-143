

   MEMBER('vodr0081.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('VODR0002.INC'),ONCE        !Local module procedure declarations
                     END


SparesRequestedStatusAnalysisReport PROCEDURE         !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::5:TAGFLAG          BYTE(0)
DASBRW::5:TAGMOUSE         BYTE(0)
DASBRW::5:TAGDISPSTATUS    BYTE(0)
DASBRW::5:QUEUE           QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
tmp:Clipboard        ANY
tmp:VersionNumber    STRING(30)
save_res_id          USHORT
Local                CLASS
DrawBox              Procedure(String func:TL,String func:TR,String func:BL,String func:BR,Long func:Colour)
WriteLine            Procedure(String func:Type)
WriteLine2           Procedure(String func:PartType)
WriteLine3           Procedure(String func:PartType)
WriteLine4           Procedure()
UpdateProgressWindow Procedure(String  func:Text)
DidStatusChangeInTime Procedure(String func:Status),Byte
CheckARCJobs         Procedure(String func:Status,String func:Type)
CheckRRCJobs         Procedure(String func:Status,String func:Type)
                     END
StatusQueue          QUEUE,PRE(staque)
StatusMessage        STRING(60)
                     END
tmp:Desktop          CSTRING(255)
TempFilePath         CSTRING(255)
tmp:StartDate        DATE
tmp:EndDate          DATE
tmp:UserName         STRING(70)
tmp:HeadAccountNumber STRING(30)
tmp:MainStoreLocation STRING(30)
tmp:ARCLocation      STRING(30)
Progress:Thermometer BYTE(0)
tmp:ARCAccount       BYTE(0)
tmp:ARCCompanyName   STRING(30)
tmp:ARCUser          BYTE(0)
TempFileQueue        QUEUE,PRE(tmpque)
AccountNumber        STRING(30)
CompanyName          STRING(30)
Count                LONG
FileName             STRING(255)
EXCHCount            LONG
EXCHFilename         STRING(255)
ExcelFileName        STRING(255)
                     END
tmp:NumberOfDays     LONG
tmp:Tag              STRING(1)
tmp:PartsExportedCount LONG
tmp:ExchangeExportedCount LONG
BRW4::View:Browse    VIEW(TRADEACC)
                       PROJECT(tra:Account_Number)
                       PROJECT(tra:Company_Name)
                       PROJECT(tra:RecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tmp:Tag                LIKE(tmp:Tag)                  !List box control field - type derived from local data
tmp:Tag_Icon           LONG                           !Entry's icon ID
tra:Account_Number     LIKE(tra:Account_Number)       !List box control field - type derived from field
tra:Company_Name       LIKE(tra:Company_Name)         !List box control field - type derived from field
tra:RecordNumber       LIKE(tra:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Excel                SIGNED !OLE Automation holder
excel:ProgramName    CString(255)
excel:ActiveWorkBook CString(20)
excel:Selected       CString(20)
excel:FileName       CString(255)
loc:Version          Cstring(30)
window               WINDOW('** Report Title **'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),ICON('Cellular3g.ico'),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE,IMM
                       PANEL,AT(60,38,560,360),USE(?PanelMain),FILL(0D6EAEFH)
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(64,56,552,44),USE(?PanelTip),FILL(0D6EAEFH)
                       GROUP('Top Tip'),AT(68,60,544,36),USE(?GroupTip),BOXED,TRN
                         STRING(@S255),AT(73,76,499,10),USE(SRN:TipText)
                       END
                       BUTTON,AT(576,64,36,32),USE(?ButtonHelp),TRN,FLAT,ICON('F1Help.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Spares Requested Status Analysis Report'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(64,104,552,258),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('Criteria'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Number Of Days'),AT(268,126),USE(?tmp:NumberOfDays:Prompt),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(348,126,64,10),USE(tmp:NumberOfDays),RIGHT(1),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Number Of Day'),TIP('Number Of Day'),CAP
                           PROMPT(''),AT(68,154),USE(?StatusText)
                           LIST,AT(230,150,220,174),USE(?List),IMM,VSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11L(2)I@s1@67L(2)|M~Account Number~@s15@120L(2)|M~Company Name~@s30@'),FROM(Queue:Browse)
                           BUTTON('&Rev tags'),AT(320,202,1,1),USE(?DASREVTAG),HIDE
                           BUTTON('sho&W tags'),AT(320,238,1,1),USE(?DASSHOWTAG),HIDE
                           BUTTON,AT(232,330),USE(?DASTAG),TRN,FLAT,ICON('tagitemp.jpg')
                           BUTTON,AT(307,330),USE(?DASTAGAll),TRN,FLAT,ICON('tagallp.jpg')
                           BUTTON,AT(384,330),USE(?DASUNTAGALL),TRN,FLAT,ICON('untagalp.jpg')
                         END
                       END
                       BUTTON,AT(472,366),USE(?Print),TRN,FLAT,ICON('printp.jpg'),DEFAULT
                       BUTTON,AT(544,366),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                       PROMPT('Report Version:'),AT(72,376),USE(?ReportVersion),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                     END

!Progress Window (DBH: 22-03-2004)
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progresswindow WINDOW('Progress...'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH), |
         CENTER,IMM,ICON('cellular3g.ico'),WALLPAPER('sbback.jpg'),TILED,TIMER(1),GRAY,DOUBLE
       PANEL,AT(160,64,360,300),USE(?Panel5),FILL(0D6E7EFH)
       PANEL,AT(164,68,352,12),USE(?Panel1),FILL(09A6A7CH)
       LIST,AT(208,88,268,206),USE(?List1),VSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH), |
           FORMAT('20L(2)|M'),FROM(StatusQueue),GRID(COLOR:White)
       PANEL,AT(164,84,352,246),USE(?Panel4),FILL(09A6A7CH)
       PROGRESS,USE(progress:thermometer),AT(206,314,268,12),RANGE(0,100)
       STRING(''),AT(259,300,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,080FFFFH,FONT:bold), |
           COLOR(09A6A7CH)
       STRING(''),AT(232,136,161,10),USE(?progress:pcttext),TRN,HIDE,CENTER,FONT('Arial',8,,)
       PANEL,AT(164,332,352,28),USE(?Panel2),FILL(09A6A7CH)
       PROMPT('Report Progress'),AT(168,70),USE(?WindowTitle2),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
       BUTTON,AT(444,332),USE(?ProgressCancel),TRN,FLAT,LEFT,ICON('Cancelp.jpg')
       BUTTON,AT(376,332),USE(?Button:OpenReportFolder),TRN,FLAT,HIDE,ICON('openrepp.jpg')
       BUTTON,AT(444,332),USE(?Finish),TRN,FLAT,HIDE,ICON('Finishp.jpg')
     END
!Export Files (DBH: 22-03-2004)
ExportFile    File,Driver('BASIC'),Pre(exp),Name(glo:ExportFile),Create,Bindable,Thread
Record                  Record
JobNumber               String(30)
HeadAccountName         String(30)
CurrentStatus           String(30)
NumberOfDaysInStatus    String(30)
Manufacturer            String(30)
ModelNumber             String(30)
PartNumber              String(30)
PartDescription         String(30)
MainStoreBinNumber      String(30)
DateOrdered             String(255)
MainStoreStockOnHand    String(30)
RRCStockOnHand          String(30)
                        End
                    End
EXCHExportFile    File,Driver('BASIC'),Pre(excexp),Name(glo:EXCHExportFile),Create,Bindable,Thread
Record                  Record
JobNumber               String(30)
HeadAccountName         String(30)
CurrentStatus           String(30)
NumberOfDaysInStatus    String(30)
Manufacturer            String(30)
ModelNumber             String(30)
MainStoreBinNumber      String(30)
DateOrdered             String(255)
MainStoreStockOnHand    String(30)
RRCStockOnHand          String(30)
                        End
                    End
 
Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?Prog:CNPrompt),FONT(,14,,FONT:bold)
     END
***

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
E1                   Class(oiExcel)
Init                   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0),byte,proc,virtual,name('INIT@F17OIEXCEL')
Kill                   PROCEDURE (byte pUnloadCOM=1),byte,proc,virtual,name('KILL@F17OIEXCEL')
TakeEvent              PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0),virtual,name('TAKEEVENT@F17OIEXCEL')
                     End

BRW4                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW4::Sort0:Locator  StepLocatorClass                 !Default Locator
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
    Map
ExcelSetup          Procedure(Byte      func:Visible)

ExcelMakeWorkBook   Procedure(String    func:Title,String   func:Author,String  func:AppName)

ExcelMakeSheet      Procedure()

ExcelSheetType      Procedure(String    func:Type)

ExcelHorizontal     Procedure(String    func:Direction)

ExcelVertical        Procedure(String    func:Direction)

ExcelCell   Procedure(String    func:Text,Byte  func:Bold)

ExcelFormatCell     Procedure(String    func:Format)

ExcelFormatRange    Procedure(String    func:Range,String   func:Format)

ExcelNewLine    Procedure(Long  func:Number)

ExcelMoveDown   Procedure()

ExcelColumnWidth        Procedure(String    func:Range,Long   func:Width)

ExcelCellWidth          Procedure(Long  func:Width)

ExcelAutoFit            Procedure(String    func:Range)

ExcelGrayBox            Procedure(String    func:Range)

ExcelGrid   Procedure(String    func:Range,Byte  func:Left,Byte  func:Top,Byte   func:Right,Byte func:Bottom,Byte func:Colour)

ExcelSelectRange        Procedure(String    func:Range)

ExcelFontSize           Procedure(Byte  func:Size)

ExcelSheetName          Procedure(String    func:Name)

ExcelSelectSheet    Procedure(String    func:SheetName)

ExcelAutoFilter         Procedure(String    func:Range)

ExcelDropAllSheets      Procedure()

ExcelDeleteSheet        Procedure(String    func:SheetName)

ExcelClose              Procedure()

ExcelSaveWorkBook       Procedure(String    func:Name)

ExcelFontColour         Procedure(String    func:Range,Long func:Colour)

ExcelWrapText           Procedure(String    func:Range,Byte func:True)

ExcelGetFilename        Procedure(Byte      func:DontAsk),Byte

ExcelGetDirectory       Procedure(),Byte

ExcelCurrentColumn      Procedure(),String

ExcelCurrentRow         Procedure(),String

ExcelPasteSpecial       Procedure(String    func:Range)

ExcelConvertFormula     Procedure(String    func:Formula),String

ExcelColumnLetter Procedure(Long  func:ColumnNumber),String

ExcelOpenDoc            Procedure(String    func:FileName)

ExcelFreeze             Procedure(String    func:Cell)
    End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::5:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW4.UpdateBuffer
   glo:Queue.Pointer = tra:Account_Number
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = tra:Account_Number
     ADD(glo:Queue,glo:Queue.Pointer)
    tmp:Tag = '*'
  ELSE
    DELETE(glo:Queue)
    tmp:Tag = ''
  END
    Queue:Browse.tmp:Tag = tmp:Tag
  IF (tmp:tag = '*')
    Queue:Browse.tmp:Tag_Icon = 2
  ELSE
    Queue:Browse.tmp:Tag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::5:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW4.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW4::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = tra:Account_Number
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW4.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::5:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW4.Reset
  SETCURSOR
  BRW4.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::5:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::5:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::5:QUEUE = glo:Queue
    ADD(DASBRW::5:QUEUE)
  END
  FREE(glo:Queue)
  BRW4.Reset
  LOOP
    NEXT(BRW4::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::5:QUEUE.Pointer = tra:Account_Number
     GET(DASBRW::5:QUEUE,DASBRW::5:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = tra:Account_Number
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW4.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::5:DASSHOWTAG Routine
   CASE DASBRW::5:TAGDISPSTATUS
   OF 0
      DASBRW::5:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::5:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::5:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW4.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
Reporting       Routine  !Do the Report (DBH: 22-03-2004)
Data
local:LoopCount         Long()
local:LocalPath              String(255)
local:CurrentAccount    String(30)
local:ReportStartDate   Date()
local:ReportStartTime   Time()
local:CountJobsExported Long()
local:DateStatusChanged     Date()
local:Desktop               Cstring(255)
Code
    !Set the temp folder for the csv files (DBH: 10-03-2004)
    If GetTempPathA(255,TempFilePath)
        If Sub(TempFilePath,-1,1) = '\'
            local:LocalPath = Clip(TempFilePath) & ''
        Else !If Sub(TempFilePath,-1,1) = '\'
            local:LocalPath = Clip(TempFilePath) & '\'
        End !If Sub(TempFilePath,-1,1) = '\'
    End

    excel:ProgramName = 'Spares Requested Status Analysis'
    SHGetSpecialFolderPath( GetDesktopWindow(), local:Desktop, 5, FALSE )
    local:Desktop = Clip(local:Desktop) & '\ServiceBase Export'

    !Does the Export Folder already Exists?
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    local:Desktop = Clip(local:Desktop) & '\' & Clip(excel:ProgramName)
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    excel:FileName = Clip(local:Desktop) & '\' & Clip(excel:ProgramName) & ' ' & Format(Today(),@d12)

    If Exists(excel:FileName & '.xls')
        Remove(excel:FileName & '.xls')
        If Error()
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('Cannot get access to the selected document:'&|
                '|' & Clip(excel:FileName) & ''&|
                '|'&|
                '|Ensure the file is not in use and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If Error()
    End !If Exists(excel:FileName)

    If COMMAND('/DEBUG')
        Remove('c:\debug.ini')
    End !If COMMAND('/DEBUG')

    !Open Program Window (DBH: 10-03-2004)
    recordspercycle         = 25
    recordsprocessed        = 0
    percentprogress         = 0
    progress:thermometer    = 0
    recordstoprocess        = 50
    Open(ProgressWindow)

    ?progress:userstring{prop:text} = 'Running...'
    ?progress:pcttext{prop:text} = '0% Completed'


    local:ReportStartDate = Today()
    local:ReportStartTime = Clock()

    If Command('/DEBUG')
        local.UpdateProgressWindow('========')
        local.UpdateProgressWindow('*** DEBUG MODE ***')
        local.UpdateProgressWindow('========')
        local.UpdateProgressWindow('')
    End !If Command('/DEBUG')

    local.UpdateProgressWindow('Report Started: ' & Format(local:ReportStartDate,@d6b) & ' ' & Format(local:ReportStartTime,@t1b))
    local.UpdateProgressWindow('')
    local.UpdateProgressWindow('Creating Export Files...')

    !_____________________________________________________________________

    !CREATE CSV FILES
    !_____________________________________________________________________


    glo:ExportFile = Clip(local:LocalPath) & 'SPARESTEMP.CSV'
    Remove(ExportFile)
    Create(ExportFile)
    Open(ExportFile)
    glo:EXCHExportFile = Clip(local:LocalPath) & 'EXCHANGESTEMP.CSV'
    Remove(EXCHExportFile)
    Create(EXCHExportFile)
    Open(EXCHExportFile)

    tmp:PartsExportedCount = 0
    tmp:ExchangeExportedCount = 0

    Loop local:LoopCount = 1 To Records(glo:Queue)
        Get(glo:Queue,local:LoopCount)

        If glo:Pointer = tmp:HeadAccountNumber
            !This is the ARC account - TrkBs:  (DBH: 03-03-2005)
            

            Access:TRADEACC.Clearkey(tra:Account_Number_Key)
            tra:Account_Number  = glo:Pointer
            If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                !Found

            Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                !Error
                Cycle
            End ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign

            local.UpdateProgressWindow('Reading Account: ' & Clip(tra:Account_Number) & ' - ' & Clip(tra:Company_Name))
            !message('This is the headoffice account number')
    !_____________________________________________________________________
    !Check Job Status
            local.CheckARCJobs('330 SPARES REQUESTED','PART')

            local.CheckARCJobs('335 SPARES ORDERED','PART')

            local.CheckARCJobs('340 BACK ORDER SPARES','PART')

            local.CheckARCJobs('350 EXCHANGE ORDER REQUIRED','EXCH')

            local.CheckARCJobs('355 EXCHANGE REQUESTED','EXCH')

            local.CheckARCJobs('360 EXCHANGE ORDERED','EXCH')
    !_____________________________________________________________________


!            If tmp:PartsExportedCount = 0
!!                Remove(ExportFile)
!            Else ! If tmp:PartsExportedCount = 0
!                !Add the filename to a queue - TrkBs:  (DBH: 03-03-2005)
!                local.UpdateProgressWindow('Parts Found: ' & tmp:partsExportedCount)
!                tmpque:AccountNumber = tmp:HeadAccountNumber
!                Get(TempFileQueue,tmpque:AccountNumber)
!                If Error()
!                    tmpque:AccountNumber = tmp:HeadAccountNumber
!                    tmpque:CompanyName = tmp:ARCCompanyName
!                    tmpque:FileName = glo:ExportFile
!                    tmpque:Count    = tmp:PartsExportedCount
!                    Add(TempFileQueue)
!                Else !If Error()
!                    tmpque:FileName = glo:ExportFile
!                    tmpque:Count    = tmp:PartsExportedCount
!                    Put(TempFileQueue)
!                End !If Error()
!            End ! If tmp:PartsExportedCount = 0
!
!            If tmp:ExchangeExportedCount = 0
!!                Remove(EXCHExportFile)
!            Else ! If tmp:ExchangeExportedCount = 0
!                !Add the filename to a queue - TrkBs:  (DBH: 03-03-2005)
!                local.UpdateProgressWindow('Exchanges Found: ' & tmp:ExchangeExportedCount)
!                tmpque:AccountNumber = tmp:HeadAccountNumber
!                Get(TempFileQueue,tmpque:AccountNumber)
!                If Error()
!                    tmpque:AccountNumber = tmp:HeadAccountNumber
!                    tmpque:CompanyName = tmp:ARCCompanyName
!                    tmpque:EXCHFileName = glo:EXCHExportFile
!                    tmpque:ExchCount    = tmp:ExchangeExportedCount
!                    Add(TempFileQueue)
!                Else !If Error()
!                    tmpque:EXCHFileName = glo:EXCHExportFile
!                    tmpque:EXCHCount    = tmp:ExchangeExportedCount
!                    Put(TempFileQueue)
!                End !If Error()
!
!            End ! If tmp:ExchangeExportedCount = 0


        Else ! If glo:Pointer = tmp:HeadAccountNumber
            !This is the RRC account - TrkBs:  (DBH: 03-03-2005)


            Access:TRADEACC.Clearkey(tra:Account_Number_Key)
            tra:Account_Number  = glo:Pointer
            If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign

            Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                Cycle
            End ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign

            local.UpdateProgressWindow('Reading Account: ' & Clip(tra:Account_Number) & ' - ' & Clip(tra:Company_Name))

    !_____________________________________________________________________
    !Check Job Status
            local.CheckRRCJobs('330 SPARES REQUESTED','PART')

            local.CheckRRCJobs('335 SPARES ORDERED','PART')

            local.CheckRRCJobs('340 BACK ORDER SPARES','PART')

            local.CheckRRCJobs('350 EXCHANGE ORDER REQUIRED','EXCH')

            local.CheckRRCJobs('355 EXCHANGE REQUESTED','EXCH')

            local.CheckRRCJobs('360 EXCHANGE ORDERED','EXCH')
    !_____________________________________________________________________


!            If tmp:PartsExportedCount = 0
!                Remove(ExportFile)
!            Else ! If tmp:PartsExportedCount = 0
!                !Add the filename to a queue - TrkBs:  (DBH: 03-03-2005)
!                local.UpdateProgressWindow('Parts Found: ' & tmp:partsExportedCount)
!                tmpque:AccountNumber = tra:Account_Number
!                Get(TempFileQueue,tmpque:AccountNumber)
!                If Error()
!                    tmpque:AccountNumber = tra:Account_Number
!                    tmpque:CompanyName = tra:Company_Name
!                    tmpque:FileName = glo:ExportFile
!                    tmpque:Count    = tmp:PartsExportedCount
!                    Add(TempFileQueue)
!                Else !If Error()
!                    tmpque:FileName = glo:ExportFile
!                    tmpque:Count    = tmp:PartsExportedCount
!                    Put(TempFileQueue)
!                End !If Error()
!            End ! If tmp:PartsExportedCount = 0
!
!            If tmp:ExchangeExportedCount = 0
!                Remove(EXCHExportFile)
!            Else ! If tmp:ExchangeExportedCount = 0
!                !Add the filename to a queue - TrkBs:  (DBH: 03-03-2005)
!                local.UpdateProgressWindow('Exchanges Found: ' & tmp:ExchangeExportedCount)
!                tmpque:AccountNumber = tra:Account_Number
!                Get(TempFileQueue,tmpque:AccountNumber)
!                If Error()
!                    tmpque:AccountNumber = tra:Account_Number
!                    tmpque:CompanyName = tra:Company_Name
!                    tmpque:EXCHFileName = glo:EXCHExportFile
!                    tmpque:ExchCount    = tmp:ExchangeExportedCount
!                    Add(TempFileQueue)
!                Else !If Error()
!                    tmpque:EXCHFileName = glo:EXCHExportFile
!                    tmpque:EXCHCount    = tmp:ExchangeExportedCount
!                    Put(TempFileQueue)
!                End !If Error()
!
!            End ! If tmp:ExchangeExportedCount = 0
        End ! If glo:Pointer = tmp:HeadAccountNumber
    End ! Loop local:LoopCount = 1 To Records(glo:Queue)

    Close(ExportFile)
    Close(EXCHExportFile)

    !_____________________________________________________________________

    If tmp:Cancel = 0 Or tmp:Cancel = 2

        ?ProgressCancel{prop:Hide} = 1

        local.UpdateProgressWindow('================')
        local.UpdateProgressWindow('Building Excel Document..')
    !_____________________________________________________________________

    !CREATE EXPORT DOCUMENT, CREATE SHEETS, COLUMNS and AUTOFILTER
    !_____________________________________________________________________

        ExcelSetup(0)
!        tmpque:ExcelFileName = excel:FileName
!        Put(TempFileQueue)
        ExcelMakeWorkbook('Spares Requested Status Analysis','','Spares Requested Status Analysis')
        ExcelSheetName('Spares Requested')
        ExcelSelectRange('A12')
        ExcelCell('Job Number',1)
        ExcelCell('Head Account Name',1)
        ExcelCell('Current Status',1)
        ExcelCell('No. Of Days In Status',1)
        ExcelCell('Manufacturer',1)
        ExcelCell('Model Number',1)
        ExcelCell('Part Number',1)
        ExcelCell('Description',1)
        ExcelCell('Main Store Bin No',1)
        ExcelCell('Date ARC/RRC Ordered',1)
        ExcelCell('Main Store Current Stock On Hand',1)
        ExcelCell('ARC/RRC Current Stock On Hand',1)
        ExcelAutoFilter('A12:L12')
        ExcelFreeze('B13')
        ExcelWrapText('D:D',1)
        ExcelWrapText('I:L',1)
        ExcelMakeSheet()
        ExcelSheetName('Exchanges Requested')
        ExcelSelectRange('A12')
        ExcelCell('Job Number',1)
        ExcelCell('Head Account Name',1)
        ExcelCell('Current Status',1)
        ExcelCell('No. Of Days In Status',1)
        ExcelCell('Manufacturer',1)
        ExcelCell('Model Number',1)
        ExcelCell('Main Store Bin No',1)
        ExcelCell('Date ARC/RRC Ordered',1)
        ExcelCell('Main Store Current Stock On Hand',1)
        ExcelCell('ARC/RRC Current Stock On Hand',1)
        ExcelAutoFilter('A12:J12')
        ExcelFreeze('B13')
        ExcelWrapText('D:D',1)
        ExcelWrapText('G:J',1)
        ExcelDeleteSheet('Sheet3')
        ExcelSaveWorkBook(excel:FileName)
        ExcelClose()

        local.UpdateProgressWindow('Creating Excel Sheets...')
        local.UpdateProgressWindow('')

        If E1.Init(0,0) = 0
            Case Missive('An error has occurred finding your Excel document.'&|
              '<13,10>'&|
              '<13,10>Please quit and try again.','ServiceBase 3g',|
                           'mstop.jpg','/OK') 
                Of 1 ! OK Button
            End ! Case Missive
            Exit
        End !If E1.Init(0,0,1) = 0


!        Loop local:LoopCount = 1 To Records(TempFileQueue)
!            Get(TempFileQueue,local:LoopCount)
!            Do GetNextRecord2
!            Yield()
!            If tmpque:Count = 0
!                Cycle
!            End !If tmpque:Count = 0
        If tmp:PartsExportedCount <> 0
            local.UpdateProgressWindow('Building Parts Report...')

            !Open account csv file. Copy the data (DBH: 22-03-2004)
            E1.OpenWorkBook(Clip(glo:ExportFile))
            E1.Copy('A1','L' & tmp:PartsExportedCount)

            tmp:Clipboard = ClipBoard()
            SetClipBoard('')
            E1.CloseWorkBook(2)
            !Remove temp file - TrkBs:  (DBH: 08-03-2005)
            Remove(glo:ExportFile)

            !Open "final" document and paste the data (DBH: 22-03-2004)
            E1.OpenWorkBook(excel:FileName)
            E1.SelectWorkSheet('Spares Requested')
            E1.SelectCells('A13')
            SetClipBoard(tmp:ClipBoard)
            E1.SetCellNumberFormat(oix:NumberFormatText,,,,'F13','I' & tmp:PartsExportedCount + 13)
            E1.Paste()
            E1.SelectCells('B13')
            !MUST Clear the clipboard (DBH: 22-03-2004)
            SetClipBoard('')
            E1.CloseWorkBook(3)
        End ! If local:RecordCount <> 0
        If tmp:ExchangeExportedCount <> 0
            local.UpdateProgressWindow('Building Exchanges Report...')
            !Open exchange csv file - TrkBs:  (DBH: 07-03-2005)
            E1.OpenWorkBook(Clip(glo:EXCHExportFile))
            E1.Copy('A1','J' & tmp:ExchangeExportedCount)

            tmp:Clipboard = Clipboard()
            SetClipboard('')
            E1.CloseWorkBook(2)
            !Remove temp file - TrkBs:  (DBH: 08-03-2005)
            Remove(glo:EXCHExportFile)

            !Re-open final document and paste in exchange sheet - TrkBs:  (DBH: 07-03-2005)
            E1.OpenWorkBook(excel:FileName)
            E1.SelectWorkSheet('Exchanges Requested')
            E1.SetCellNumberFormat(oix:NumberFormatText,,,,'F13','F' & tmp:ExchangeExportedCount + 13)
            E1.SelectCells('A13')
            SetClipboard(tmp:Clipboard)
            E1.Paste()
            E1.SelectCells('B13')
            !Clear the clipboard - TrkBs:  (DBH: 07-03-2005)
            SetClipboard('')
            E1.SelectWorkSheet('Spares Requested')
            

            E1.SelectCells('B13')
            E1.CloseWorkBook(3)

            local.UpdateProgressWindow('Done.')
            local.UpdateProgressWindow('')
        End ! If local:RecordCount2 <> 0
!        End !Loop x# = 1 To Records(TempFileQueue)
        SetClipBoard('')

!        E1.OpenWorkBook(excel:FileName)
    !_____________________________________________________________________

    !SUMMARY
    !_____________________________________________________________________

        !Fill In Summary Sheet  (DBH: 03-03-2004)
        local.UpdateProgressWindow('================')
        local.UpdatePRogressWindow('Finishing Off Formatting..')
        local.UpdateProgressWindow('')

!        E1.SelectWorkSheet('Summary')
!
!        Do DrawSummaryTitle
    !_____________________________________________________________________

    !FORMATTING
    !_____________________________________________________________________

!        Loop local:LoopCount = 1 To Records(TempFileQueue)
!            Get(TempFileQueue,local:LoopCount)
            Do GetNextRecord2
            E1.OpenWorkBook(excel:FileName)
            E1.SelectWorkSheet('Exchanges Requested')
            Do DrawExchangeTitle
            E1.SelectCells('B13')
            E1.SelectWorkSheet('Spares Requested')
            Do DrawPartsTitle
            E1.SelectCells('B13')
            E1.CloseWorkBook(3)
!        End ! Loop x# = 1 To Records(TempFileQueue)

        E1.Kill()


        local.UpdateProgressWindow('================')
        local.UpdateProgressWindow('Report Finished: ' & Format(Today(),@d6b) & ' ' & Format(Clock(),@t1b))

    Else!If tmp:Cancel = False
        staque:StatusMessage = '=========='
        Add(StatusQueue)
        Select(?List1,Records(StatusQueue))

        staque:StatusMessage = 'Report CANCELLED: ' & Format(Today(),@d6b) & ' ' & Format(Clock(),@t1b)
        Add(StatusQueue)
        Select(?List1,Records(StatusQueue))

    End !If tmp:Cancel = False

    BHReturnDaysHoursMins(BHTimeDifference24Hr(local:ReportStartDate,Today(),local:ReportStartTime,Clock()),Days#,Hours#,Mins#)


    local.UpdateProgressWindow('Time To Finish: ' & Days# & ' Dys, ' & Format(Hours#,@n02) & ':' & Format(Mins#,@n02))
    Add(StatusQueue)
    Select(?List1,Records(StatusQueue))

    Do EndPrintRun
    ?progress:userstring{prop:text} = 'Finished...'

    Display()
    ?ProgressCancel{prop:Hide} = 1
    ?Finish{prop:Hide} = 0
    ?Button:OpenReportFolder{Prop:Hide} = 0
    Accept
        Case Field()
            Of ?Finish
                Case Event()
                    Of Event:Accepted
                        Break

                End !Case Event()
            Of ?Button:OpenReportFolder
                Case Event()
                Of Event:Accepted
                    RUN('EXPLORER.EXE ' & Clip(local:Desktop))
                End ! Case Event()
        End !Case Field()
    End !Accept
    Close(ProgressWindow)
    ?StatusText{prop:Text} = ''
    Post(Event:CloseWindow)
DrawPartsTitle        Routine   !Set Summary Screen (DBH: 22-03-2004)
Data
local:LastRow          Long()
Code
    local:LastRow = tmp:PartsExportedCount + 13

    E1.SetColumnWidth('A','','15')

    E1.WriteToCell('Spares Requested Status Anaylsis Report','A1')
    E1.SetCellFontStyle('Bold','A1','E3')
    E1.SetCellFontSize(12,'A1')

    E1.WriteToCell(tmp:VersionNumber,'C3')

    E1.WriteToCell('Criteria','A3')
    E1.WriteToCell('End Date','A4')
    E1.WriteToCell(Format(Today(),@d18),'B4')
    E1.WriteToCell('Period','A5')
    E1.WriteToCell(tmp:NumberOfDays,'B5')
    If tmp:UserName <> ''
        E1.WriteToCell('Created By','A6')
        E1.WriteToCell(tmp:UserName,'B6')
    End ! If tmp:UserName <> ''
    E1.WriteToCell('Created Date','A7')
    E1.WriteToCell(Format(TOday(),@d18),'B7')
    E1.WriteToCell('Created Time','A8')
    E1.WriteToCell(FOrmat(Clock(),@t1),'B8')

    E1.WriteToCell('Section Name','A10')

    E1.WriteToCell('AA10 MAIN STORE','B10')
    E1.SetCellFontStyle('Bold','B4','B10')

    local.DrawBox('A1','L1','A1','L1',color:Silver)
    local.DrawBox('A3','L3','A3','L3',color:Silver)
    local.DrawBox('A4','L4','A8','L8',color:Silver)
    local.DrawBox('A10','L10','A10','L10',color:Silver)
    local.DrawBox('A12','L12','A12','L12',color:Silver)

    E1.WriteToCell('Total Records','D10')
    E1.WriteToCell(tmp:PartsExportedCount,'E10')
    E1.SetCellFontStyle('Bold','E10')

    E1.WriteToCell('Showing','F10')
    E1.WriteToCell('=SUBTOTAL(3,A13:A' & local:LastRow & ')','G10')
    E1.SetCellFontStyle('Bold','G10')

    E1.SetCellFontName('Tahoma','A1', 'L' & local:LastRow)
    E1.SetCellFontSize(8,'A2','L' & local:LastRow)
    E1.SetColumnWidth('B','L','')


DrawExchangeTitle        Routine   !Set Summary Screen (DBH: 22-03-2004)
Data
local:LastRow           Long()
Code
    local:LastRow = tmp:ExchangeExportedCount + 13

    E1.SetColumnWidth('A','','15')
    
    E1.WriteToCell('Spares Requested Status Anaylsis Report','A1')
    E1.SetCellFontStyle('Bold','A1','E3')
    E1.SetCellFontSize(12,'A1')

    E1.WriteToCell(tmp:VersionNumber,'C3')

    E1.WriteToCell('Criteria','A3')
    E1.WriteToCell('End Date','A4')
    E1.WriteToCell(Format(Today(),@d18),'B4')
    E1.WriteToCell('Period','A5')
    E1.WriteToCell(tmp:NumberOfDays,'B5')
    If tmp:UserName <> ''
        E1.WriteToCell('Created By','A6')
        E1.WriteToCell(tmp:UserName,'B6')
    End ! If tmp:UserName <> ''
    E1.WriteToCell('Created Date','A7')
    E1.WriteToCell(Format(TOday(),@d18),'B7')
    E1.WriteToCell('Created Time','A8')
    E1.WriteToCell(FOrmat(Clock(),@t1),'B8')

    E1.WriteToCell('Section Name','A10')

    E1.WriteToCell('AA10 MAIN STORE','B10')
    E1.SetCellFontStyle('Bold','B4','B10')

    local.DrawBox('A1','J1','A1','J1',color:Silver)
    local.DrawBox('A3','J3','A3','J3',color:Silver)
    local.DrawBox('A4','J4','A8','J8',color:Silver)
    local.DrawBox('A10','J10','A10','J10',color:Silver)
    local.DrawBox('A12','J12','A12','J12',color:Silver)

    E1.WriteToCell('Total Records','D10')
    E1.WriteToCell(tmp:ExchangeExportedCount,'E10')
    E1.SetCellFontStyle('Bold','E10')

    E1.WriteToCell('Showing','F10')
    E1.WriteToCell('=SUBTOTAL(3,A13:A' & local:LastRow & ')','G10')
    E1.SetCellFontStyle('Bold','G10')

    E1.SetColumnWidth('B','J')
    E1.SetCellFontName('Tahoma','A1', 'J' & local:LastRow)
    E1.SetCellFontSize(8,'A2','J' & local:LastRow)
getnextrecord2      routine !Progress Window Routines (DBH: 22-03-2004)
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress >= 100
        recordsprocessed        = 0
        percentprogress         = 0
        progress:thermometer    = 0
        recordsthiscycle        = 0
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    !0{prop:Text} = ?progress:pcttext{prop:text}
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                Yield()
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case Missive('Do you want to finish off building the excel document with the data you have compiled so far, or just quit now?','ServiceBase 3g',|
                       'mquest.jpg','\Cancel|Quit|/Finish') 
            Of 3 ! Finish Button
                tmp:Cancel = 2
            Of 2 ! Quit Button
                tmp:Cancel = 1
            Of 1 ! Cancel Button
        End ! Case Missive
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    display()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020626'&'0'
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('SparesRequestedStatusAnalysisReport')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelMain
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:AUDIT.Open
  Relate:EXCHANGE.Open
  Relate:EXCHOR48.Open
  Relate:ORDHEAD.Open
  Relate:ORDITEMS.Open
  Relate:RETSALES.Open
  Relate:TRADEAC2.Open
  Relate:TRADEACC_ALIAS.Open
  Relate:WAYITEMS.Open
  Relate:WEBJOB.Open
  Access:JOBSE.UseFile
  Access:JOBS.UseFile
  Access:PARTS.UseFile
  Access:WARPARTS.UseFile
  Access:LOCATION.UseFile
  Access:STOCK.UseFile
  Access:RETSTOCK.UseFile
  SELF.FilesOpened = True
  !Initialize Dates and save Head Account Information (DBH: 22-03-2004)
  
  tmp:StartDate = Deformat('1/' & Month(Today()) & '/' & Year(Today()),@d6)
  tmp:EndDate = Today()
  
  tmp:HeadAccountNumber   = GETINI('BOOKING','HeadAccount',,CLIP(Path()) & '\SB2KDEF.INI')
  
  Access:TRADEACC.Clearkey(tra:Account_Number_Key)
  tra:Account_Number  = tmp:HeadAccountNumber
  If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      !Found
      tmp:ARCLocation = tra:SiteLocation
      tmp:ARCCompanyName = tra:Company_Name
  Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      !Error
  End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  
  !Get the name of the "Main Store" - TrkBs:  (DBH: 03-03-2005)
  Access:LOCATION.ClearKey(loc:Main_Store_Key)
  loc:Main_Store = 'YES'
  Set(loc:Main_Store_Key,loc:Main_Store_Key)
  Loop
      If Access:LOCATION.NEXT()
         Break
      End !If
      If loc:Main_Store <> 'YES'      |
          Then Break.  ! End If
      tmp:MainStoreLocation = loc:Location
      Break
  End !Loop
  
  !do we need to swap the headaccout trade location to the mainstore?
  !TB12031 - J - 23/05/12
  glo:RelocateStore = false   !jsut to be sure
  Access:Tradeac2.clearkey(TRA2:KeyAccountNumber)
  TRA2:Account_Number = TRA:Account_Number
  if access:Tradeac2.fetch(TRA2:KeyAccountNumber) = level:Benign
      !found it
      if TRA2:UseSparesRequest = 'YES' then
          !message('Relocating store')
          glo:RelocateStore = true
      END !if TRA2:UseSparesRequest
  END !if tradeac2.fetched
  
  
  !Put in user name IF run from ServiceBase (DBH: 25-03-2004)
  pos# = Instring('%',COMMAND(),1,1)
  If pos#
      Access:USERS.Clearkey(use:Password_Key)
      use:Password    = Clip(Sub(COMMAND(),pos#+1,30))
      If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
          !Found
          tmp:UserName = Clip(use:Forename) & ' ' & Clip(use:Surname)
      Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
          !Error
      End !If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
  
  End !pos#
  
  !Start - Create the desktop folders - TrkBs:  (DBH: 04-03-2005)
  SHGetSpecialFolderPath( GetDesktopWindow(), tmp:Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
  
  tmp:Desktop = Clip(tmp:Desktop) & '\ServiceBase Export'
  !Does the Export Folder already Exists?
  If ~Exists(Clip(tmp:Desktop))
      If ~MkDir(tmp:Desktop)
          
      End !If MkDir(func:Desktop)
  End !If Exists(Clip(tmp:Desktop))
  
  tmp:Desktop = Clip(tmp:Desktop) & '\Spares Requested Status Analysis'
  !Does the Export Folder already Exists?    
  If ~Exists(Clip(tmp:Desktop))
      If ~MkDir(tmp:Desktop)
          
      End !If MkDir(func:Desktop)
  End !If Exists(Clip(tmp:Desktop))
  
  !End   - Create the desktop folders - TrkBs:  (DBH: 04-03-2005)
  BRW4.Init(?List,Queue:Browse.ViewPosition,BRW4::View:Browse,Queue:Browse,Relate:TRADEACC,SELF)
  OPEN(window)
  SELF.Opened=True
  ! ========= Set Report Version =============
  Include('..\ReportVersion.inc')
  tmp:VersionNumber = 'Version Number: ' & Clip(tmp:VersionNumber) & '5001'
  ?ReportVersion{prop:Text} = tmp:VersionNumber
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW4.Q &= Queue:Browse
  BRW4.AddSortOrder(,tra:Account_Number_Key)
  BRW4.AddLocator(BRW4::Sort0:Locator)
  BRW4::Sort0:Locator.Init(,tra:Account_Number,1,BRW4)
  BRW4.SetFilter('(Upper(tra:BranchIdentification) <<> '''')')
  BIND('tmp:Tag',tmp:Tag)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW4.AddField(tmp:Tag,BRW4.Q.tmp:Tag)
  BRW4.AddField(tra:Account_Number,BRW4.Q.tra:Account_Number)
  BRW4.AddField(tra:Company_Name,BRW4.Q.tra:Company_Name)
  BRW4.AddField(tra:RecordNumber,BRW4.Q.tra:RecordNumber)
  BRW4.AddToolbarTarget(Toolbar)
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:AUDIT.Close
    Relate:EXCHANGE.Close
    Relate:EXCHOR48.Close
    Relate:ORDHEAD.Close
    Relate:ORDITEMS.Close
    Relate:RETSALES.Close
    Relate:TRADEAC2.Close
    Relate:TRADEACC_ALIAS.Close
    Relate:WAYITEMS.Close
    Relate:WEBJOB.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020626'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020626'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020626'&'0')
      ***
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?Print
      ThisWindow.Update
      Do Reporting
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeEvent()
    E1.TakeEvent ('', '')
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::5:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

local.WriteLine         Procedure(String func:Type) !Write CSV Files (DBH: 22-03-2004)
Code
    !For 48 Hour jobs, check the 48 hr exchange orders - TrkBs:  (DBH: 07-03-2005)
    !message('Write line called')
    If jobe:Engineer48HourOption = 1

        Local.WriteLine4()

    Else !! jobe:Engineer48HourOption = 1
        !Otherwise, just check of ordered parts on the job - TrkBs:  (DBH: 07-03-2005)
        !Loop through parts attached to job - TrkBs:  (DBH: 03-03-2005)
        Access:PARTS.ClearKey(par:Part_Number_Key)
        par:Ref_Number  = job:Ref_Number
        Set(par:Part_Number_Key,par:Part_Number_Key)
        Loop
            If Access:PARTS.NEXT()
                !message('Break on parts.next()')
               Break
            End !If
            If par:Ref_Number  <> job:Ref_Number                      |
                Then Break.  ! End If
            If par:Part_Number = 'EXCH'
                Cycle
            Else ! If par:Part_Number = 'EXCH'
                !Make sure we're looking for normal parts - TrkBs:  (DBH: 04-03-2005)
                If func:Type <> 'PART'
                    Cycle
                End !If func:Type <> 'PART'

                !Only count 'on order' parts - TrkBs:  (DBH: 03-03-2005)
                If par:WebOrder <> 1
                    !message('par:weborder cycle')
                    if tra:SiteLocation = tmp:ArcLocation and glo:RelocateStore = true then
                        !let it through
                    ELSE
                        Cycle
                    END
                End ! If par:WebOrder <> 1

                Access:STOCK.ClearKey(sto:Ref_Number_Key)
                sto:Ref_Number = par:Part_Ref_Number
                If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                    !Found
                    !message('Part found')
                    !Did this part come from the same location as is being reported on? - TrkBs:  (DBH: 04-03-2005)
                    If sto:Location <> tra:SiteLocation
                        !MESSAGE('Charge - location do not match sto='&clip(sto:Location)&' tra='&clip(tra:SiteLocation)&' tmp='&clip(tmp:ArcLocation)&' relocate = '&clip(glo:RelocateStore))
                        if tra:SiteLocation = tmp:ArcLocation and glo:RelocateStore = true then
                            if  sto:Location = tmp:MainStoreLocation then
                                !this can go through
                            ELSE
                                cycle
                            END
                        ELSE
                            Cycle
                        END !if relocating
                    End !If sto:Location <> glo:Pointer
                Else !If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                    !Error
                    !message('Part not found cycle')
                    Cycle
                End !If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                !message('Should write the line')
                local.WriteLine2('CHA')
            End ! If par:Part_Number = 'EXCH'                     
        End !Loop                
                   
        Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
        wpr:Ref_Number  = job:Ref_Number
        Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
        Loop
            If Access:WARPARTS.NEXT()
               Break
            End !If
            If wpr:Ref_Number  <> job:Ref_Number      |
                Then Break.  ! End If
            If wpr:Part_Number = 'EXCH'
                Cycle
            Else ! If wpr:Part_Number = 'EXCH'
                !Make sure we're looking for normal parts - TrkBs:  (DBH: 04-03-2005)
                If func:Type <> 'PART'
                    Cycle
                End !If func:Type <> 'PART'

                !Only count 'on order' parts - TrkBs:  (DBH: 03-03-2005)
                If wpr:WebOrder <> 1
                    if tra:SiteLocation = tmp:ArcLocation and glo:RelocateStore = true then
                        !let it through
                    ELSE
                        Cycle
                    END
                End !If wpr:WebOrder <> 1

                Access:STOCK.ClearKey(sto:Ref_Number_Key)
                sto:Ref_Number = wpr:Part_Ref_Number
                If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                    !Found
                    !Did this part come from the same location as is being reported on? - TrkBs:  (DBH: 04-03-2005)
                    If sto:Location <> tra:SiteLocation
                        !MESSAGE('Warr - location do not match sto='&clip(sto:Location)&' tra='&clip(tra:SiteLocation)&' tmp='&clip(tmp:ArcLocation)&' relocate = '&clip(glo:RelocateStore))
                        if tra:SiteLocation = tmp:ArcLocation and glo:RelocateStore = true then
                            if  sto:Location = tmp:MainStoreLocation then
                                !this can go through
                            ELSE
                                cycle
                            END
                        ELSE
                            Cycle
                        END !if relocating
                        
                    End !If sto:Location <> glo:Pointer
                Else !If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                    !Error
                    Cycle
                End !If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign

                local.WriteLine2('WAR')
            End ! If wpr:Part_Number = 'EXCH'               
        End !Loop                    

    End ! jobe:Engineer48HourOption = 1
local.WriteLine2        Procedure(String func:PartType)         !Normal Part Export
local:DaysInStatus      Long()
local:OrderDate         String(255)
Code
    Clear(exp:Record)
    !Job Number
    exp:JobNumber               = job:Ref_NUmber
    !Head Account Name
    Access:TRADEACC_ALIAS.ClearKey(tra_ali:Account_Number_Key)
    tra_ali:Account_Number = wob:HeadACcountNumber
    If Access:TRADEACC_ALIAS.TryFetch(tra_ali:Account_Number_Key) = Level:Benign
        !Found
        exp:HeadAccountName     = tra_ali:Company_Name
    Else !If Access:TRADEACC_ALIAS.TryFetch(tra_ali:Account_Number_Key) = Level:Benign
        !Error
        exp:HeadAccountName     = 'UNKNOWN'
    End !If Access:TRADEACC_ALIAS.TryFetch(tra_ali:Account_Number_Key) = Level:Benign
    !Current Status
    exp:CurrentStatus           = job:Current_Status
    !Number Of Days In Status
    local:DaysInStatus = 0
    Access:AUDSTATS.ClearKey(aus:StatusTypeRecordKey)
    aus:RefNumber    = job:Ref_Number
    aus:Type         = 'JOB'
    Set(aus:StatusTypeRecordKey,aus:StatusTypeRecordKey)
    Loop
        If Access:AUDSTATS.NEXT()
           Break
        End !If
        If aus:RefNumber    <> job:Ref_Number       |
        Or aus:Type         <> 'JOB'      |
            Then Break.  ! End If
        If Sub(aus:NewStatus,1,3) = Sub(job:Current_Status,1,3)
            local:DaysInStatus = Today() - aus:DateChanged
        End ! If Sub(aus:NewStatus,1,3) = Sub(job:Current_Status,1,3)
    End !Loop
    
    exp:NumberOfDaysInStatus    = local:DaysInStatus
    !Manufacturer
    exp:Manufacturer            = job:Manufacturer
    !Model Number
    exp:ModelNumber             = '''' & job:Model_Number

    Case func:PartType
        Of 'CHA' !Chargeable Part
            !Part Number
            exp:PartNumber      = '''' & par:Part_Number
            !Part Description
            exp:PartDescription = par:Description
            !Main Stock Bin Number
            Access:STOCK.ClearKey(sto:Location_Key)
            sto:Location    = tmp:MainStoreLocation
            sto:Part_Number = par:Part_Number
            If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                !Found
                exp:MainStoreBinNumber  = sto:Shelf_Location
                !Date Ordered
                !Lookup unprocess orders for the current site - TrkBs:  (DBH: 04-03-2005)
                Access:ORDHEAD.ClearKey(orh:AccountDateKey)
                orh:account_no = tra:StoresAccount
                Set(orh:AccountDateKey,orh:AccountDateKey)
                Loop
                    If Access:ORDHEAD.NEXT()
                       Break
                    End !If
                    If orh:account_no <> tra:StoresAccount      |
                        Then Break.  ! End If

                    If orh:Procesed <> 0
                        Cycle
                    End !If ori:Processed <> 0

                    Access:ORDITEMS.ClearKey(ori:Keyordhno)
                    ori:ordhno = orh:Order_no
                    Set(ori:Keyordhno,ori:Keyordhno)
                    Loop
                        If Access:ORDITEMS.NEXT()
                           Break
                        End !If
                        If ori:ordhno <> orh:Order_no      |
                            Then Break.  ! End If

                        If ori:PartNo = par:Part_Number
                            local:OrderDate = Clip(local:OrderDate) & ' - ' & Format(orh:thedate,@d17)
                            Break
                        End ! If ori:PartNo = par:Part_Number
                    End !Loop
                End !Loop

                ! Check for retail sales with back orders (DBH: 16-05-2005)
                Save_res_ID = Access:RETSTOCK.SaveFile()
                Access:RETSTOCK.ClearKey(res:DespatchedKey)
                res:Despatched           = 'PEN'
                Set(res:DespatchedKey,res:DespatchedKey)
                Loop
                    If Access:RETSTOCK.NEXT()
                       Break
                    End !If
                    If res:Despatched           <> 'PEN'      |
                        Then Break.  ! End If
                    Access:RETSALES.Clearkey(ret:Ref_Number_Key)
                    ret:Ref_Number = res:Ref_Number
                    If Access:RETSALES.Tryfetch(ret:Ref_Number_key) = Level:Benign
                        !Found
                        IF ret:Account_Number = tra:StoresAccount
                            If res:Part_Number = par:Part_Number
                                local:OrderDate = Clip(local:OrderDate) & ' - ' & Format(ret:Date_Booked,@d17)
                            End ! If ret:Part_Number = par:Part_Number
                        End ! IF ret:AccountNumber = tra:StoresAccount
                    Else ! Access:RETSALES.Tryfetch(ret:Ref_Number_key)
                        !Error
                    End ! Access:RETSALES.Tryfetch(ret:Ref_Number_key)
                End !Loop
                Access:RETSTOCK.RestoreFile(Save_res_ID)

                exp:DateOrdered         = Sub(local:OrderDate,4,255)

                !Main Store Stock On Hand
                exp:MainStoreStockOnHand = sto:Quantity_Stock

            Else !If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                !Error
                exp:MainStoreBinNumber  = 'UNKNOWN'
                !Date Ordered
                exp:DateOrdered         = 'UNKNOWN'
                !Main Store Stock On Hand
                exp:MainStoreStockOnHand = 'UNKNOWN'

            End !If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
            !RRC Stock On Hand
            Access:STOCK.ClearKey(sto:Ref_Number_Key)
            sto:Ref_Number = par:Part_Ref_Number
            If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                !Found
                exp:RRCStockOnHand  = sto:Quantity_Stock
            Else !If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                !Error
                exp:RRCStockOnHand  = 'UNKNOWN'
            End !If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign

        Of 'WAR'
            !Part Number
            exp:PartNumber      = '''' & wpr:Part_Number
            !Part Description
            exp:PartDescription = wpr:Description
            !Main Stock Bin Number
            Access:STOCK.ClearKey(sto:Location_Key)
            sto:Location    = tmp:MainStoreLocation
            sto:Part_Number = wpr:Part_Number
            If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                !Found
                exp:MainStoreBinNumber  = sto:Shelf_Location
                !Date Ordered
                !Lookup unprocess orders for the current site - TrkBs:  (DBH: 04-03-2005)
                Access:ORDHEAD.ClearKey(orh:AccountDateKey)
                orh:account_no = glo:Pointer
                Set(orh:AccountDateKey,orh:AccountDateKey)
                Loop
                    If Access:ORDHEAD.NEXT()
                       Break
                    End !If
                    If orh:account_no <> tra:StoresAccount      |
                        Then Break.  ! End If

                    If orh:Procesed <> 0
                        Cycle
                    End !If ori:Processed <> 0

                    Access:ORDITEMS.ClearKey(ori:Keyordhno)
                    ori:ordhno = orh:Order_no
                    Set(ori:Keyordhno,ori:Keyordhno)
                    Loop
                        If Access:ORDITEMS.NEXT()
                           Break
                        End !If
                        If ori:ordhno <> tra:StoresAccount      |
                            Then Break.  ! End If
                        If ori:PartNo = wpr:Part_Number
                            local:OrderDate = Clip(local:OrderDate) & ' - ' & Format(orh:thedate,@d17)
                            Break
                        End ! If ori:PartNo = par:Part_Number
                    End !Loop
                End !Loop

                ! Check for retail sales with back orders (DBH: 16-05-2005)
                Save_res_ID = Access:RETSTOCK.SaveFile()
                Access:RETSTOCK.ClearKey(res:DespatchedKey)
                res:Despatched           = 'PEN'
                Set(res:DespatchedKey,res:DespatchedKey)
                Loop
                    If Access:RETSTOCK.NEXT()
                       Break
                    End !If
                    If res:Despatched           <> 'PEN'      |
                        Then Break.  ! End If
                    Access:RETSALES.Clearkey(ret:Ref_Number_Key)
                    ret:Ref_Number = res:Ref_Number
                    If Access:RETSALES.Tryfetch(ret:Ref_Number_key) = Level:Benign
                        !Found
                        IF ret:Account_Number = tra:StoresAccount
                            If res:Part_Number = wpr:Part_Number
                                local:OrderDate = Clip(local:OrderDate) & ' - ' & Format(ret:Date_Booked,@d17)
                            End ! If ret:Part_Number = par:Part_Number
                        End ! IF ret:AccountNumber = tra:StoresAccount
                    Else ! Access:RETSALES.Tryfetch(ret:Ref_Number_key)
                        !Error
                    End ! Access:RETSALES.Tryfetch(ret:Ref_Number_key)
                End !Loop
                Access:RETSTOCK.RestoreFile(Save_res_ID)

                exp:DateOrdered         = Sub(local:OrderDate,4,255)

                !Main Store Stock On Hand
                exp:MainStoreStockOnHand = sto:Quantity_Stock

            Else !If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                !Error
                exp:MainStoreBinNumber  = 'UNKNOWN'
                !Date Ordered
                exp:DateOrdered         = 'UNKNOWN'
                !Main Store Stock On Hand
                exp:MainStoreStockOnHand = 'UNKNOWN'

            End !If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
            !RRC Stock On Hand
            Access:STOCK.ClearKey(sto:Ref_Number_Key)
            sto:Ref_Number = wpr:Part_Ref_Number
            If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                !Found
                exp:RRCStockOnHand  = sto:Quantity_Stock
            Else !If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                !Error
                exp:RRCStockOnHand  = 'UNKNOWN'
            End !If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign

    End ! Case func:PartType
    Add(ExportFile)

    tmp:PartsExportedCount += 1
local.WriteLine3        Procedure(String func:PartType)  !Exchange Export (Normal Part)
local:DaysInStatus      Long()
local:OrderDate         String(255)
local:CountExchanges    Long()
Code
    Clear(excexp:Record)
    !Job Number
    excexp:JobNumber               = job:Ref_NUmber
    !Head Account Name
    Access:TRADEACC_ALIAS.ClearKey(tra_ali:Account_Number_Key)
    tra_ali:Account_Number = wob:HeadACcountNumber
    If Access:TRADEACC_ALIAS.TryFetch(tra_ali:Account_Number_Key) = Level:Benign
        !Found
        excexp:HeadAccountName     = tra_ali:Company_Name
    Else !If Access:TRADEACC_ALIAS.TryFetch(tra_ali:Account_Number_Key) = Level:Benign
        !Error
        excexp:HeadAccountName     = 'UNKNOWN'
    End !If Access:TRADEACC_ALIAS.TryFetch(tra_ali:Account_Number_Key) = Level:Benign
    !Current Status
    excexp:CurrentStatus           = job:Current_Status
    !Number Of Days In Status
    local:DaysInStatus = 0
    Access:AUDSTATS.ClearKey(aus:StatusTypeRecordKey)
    aus:RefNumber    = job:Ref_NUmber
    aus:Type         = 'JOB'
    Set(aus:StatusTypeRecordKey,aus:StatusTypeRecordKey)
    Loop
        If Access:AUDSTATS.NEXT()
           Break
        End !If
        If aus:RefNumber    <> job:Ref_Number       |
        Or aus:Type         <> 'JOB'      |
            Then Break.  ! End If
        If Sub(aus:NewStatus,1,3) = Sub(job:Current_Status,1,3)
            local:DaysInStatus = Today() - aus:DateChanged
        End ! If Sub(aus:NewStatus,1,3) = Sub(job:Current_Status,1,3)
    End !Loop
    excexp:NumberOfDaysInStatus    = local:DaysInStatus
    !Manufacturer
    excexp:Manufacturer            = job:Manufacturer
    !Model Number
    excexp:ModelNumber             = '''' & job:Model_Number

    Case func:PartType
        Of 'CHA' !Chargeable Part
            !Main Stock Bin Number
            Access:STOCK.ClearKey(sto:Location_Key)
            sto:Location    = tmp:MainStoreLocation
            sto:Part_Number = par:Part_Number
            If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                !Found
                excexp:MainStoreBinNumber  = sto:Shelf_Location
                !Date Ordered
                !Lookup unprocess orders for the current site - TrkBs:  (DBH: 04-03-2005)
                Access:ORDHEAD.ClearKey(orh:AccountDateKey)
                orh:account_no = tra:StoresAccount
                Set(orh:AccountDateKey,orh:AccountDateKey)
                Loop
                    If Access:ORDHEAD.NEXT()
                       Break
                    End !If
                    If orh:account_no <> tra:StoresAccount      |
                        Then Break.  ! End If

                    If orh:Procesed <> 0
                        Cycle
                    End !If ori:Processed <> 0

                    Access:ORDITEMS.ClearKey(ori:Keyordhno)
                    ori:ordhno = orh:Order_no
                    Set(ori:Keyordhno,ori:Keyordhno)
                    Loop
                        If Access:ORDITEMS.NEXT()
                           Break
                        End !If
                        If ori:ordhno <> orh:Order_no      |
                            Then Break.  ! End If

                        If ori:PartNo = par:Part_Number
                            local:OrderDate = Clip(local:OrderDate) & ' - ' & Format(orh:thedate,@d17)
                            Break
                        End ! If ori:PartNo = par:Part_Number
                    End !Loop
                End !Loop
                ! Check for retail sales with back orders (DBH: 16-05-2005)
                Save_res_ID = Access:RETSTOCK.SaveFile()
                Access:RETSTOCK.ClearKey(res:DespatchedKey)
                res:Despatched           = 'PEN'
                Set(res:DespatchedKey,res:DespatchedKey)
                Loop
                    If Access:RETSTOCK.NEXT()
                       Break
                    End !If
                    If res:Despatched           <> 'PEN'      |
                        Then Break.  ! End If
                    Access:RETSALES.Clearkey(ret:Ref_Number_Key)
                    ret:Ref_Number = res:Ref_Number
                    If Access:RETSALES.Tryfetch(ret:Ref_Number_key) = Level:Benign
                        !Found
                        IF ret:Account_Number = tra:StoresAccount
                            If res:Part_Number = par:Part_Number
                                local:OrderDate = Clip(local:OrderDate) & ' - ' & Format(ret:Date_Booked,@d17)
                            End ! If ret:Part_Number = par:Part_Number
                        End ! IF ret:AccountNumber = tra:StoresAccount
                    Else ! Access:RETSALES.Tryfetch(ret:Ref_Number_key)
                        !Error
                    End ! Access:RETSALES.Tryfetch(ret:Ref_Number_key)
                End !Loop
                Access:RETSTOCK.RestoreFile(Save_res_ID)

                excexp:DateOrdered         = Sub(local:OrderDate,4,255)

                !Main Store Current STock In Head
                local:CountExchanges = 0
                Access:EXCHANGE.ClearKey(xch:AvailLocModel)
                xch:Available    = 'AVL'
                xch:Location     = tmp:MainStoreLocation
                xch:Model_Number = job:Model_Number
                Set(xch:AvailLocModel,xch:AvailLocModel)
                Loop
                    If Access:EXCHANGE.NEXT()
                       Break
                    End !If
                    If xch:Available    <> 'AVL'      |
                    Or xch:Location     <> tmp:MainStoreLocation      |
                    Or xch:Model_Number <> job:Model_Number      |
                        Then Break.  ! End If
                    local:CountExchanges += 1                
                End !Loop 
                excexp:MainStoreStockOnHand  = local:CountExchanges
            
                !RRC Stock In Hand
                local:CountExchanges = 0
                Access:EXCHANGE.ClearKey(xch:AvailLocModel)
                xch:Available    = 'AVL'
                xch:Location     = tra:SiteLocation
                xch:Model_Number = job:Model_Number
                Set(xch:AvailLocModel,xch:AvailLocModel)
                Loop
                    If Access:EXCHANGE.NEXT()
                       Break
                    End !If
                    If xch:Available    <> 'AVL'      |
                    Or xch:Location     <> tra:SiteLocation      |
                    Or xch:Model_Number <> job:Model_Number      |
                        Then Break.  ! End If
                    local:CountExchanges += 1                
                End !Loop 
                excexp:RRCStockOnHand  = local:CountExchanges

            Else !If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                !Error
                excexp:MainStoreBinNumber  = 'UNKNOWN'
                !Date Ordered
                excexp:DateOrdered         = 'UNKNOWN'
                !Main Store Stock On Hand
                excexp:MainStoreStockOnHand = 'UNKNOWN'

            End !If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign

        Of 'WAR'
            !Main Stock Bin Number
            Access:STOCK.ClearKey(sto:Location_Key)
            sto:Location    = tmp:MainStoreLocation
            sto:Part_Number = wpr:Part_Number
            If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                !Found
                excexp:MainStoreBinNumber  = sto:Shelf_Location
                !Date Ordered
                !Lookup unprocess orders for the current site - TrkBs:  (DBH: 04-03-2005)
                Access:ORDHEAD.ClearKey(orh:AccountDateKey)
                orh:account_no = glo:Pointer
                Set(orh:AccountDateKey,orh:AccountDateKey)
                Loop
                    If Access:ORDHEAD.NEXT()
                       Break
                    End !If
                    If orh:account_no <> tra:StoresAccount      |
                        Then Break.  ! End If

                    If orh:Procesed <> 0
                        Cycle
                    End !If ori:Processed <> 0

                    Access:ORDITEMS.ClearKey(ori:Keyordhno)
                    ori:ordhno = orh:Order_no
                    Set(ori:Keyordhno,ori:Keyordhno)
                    Loop
                        If Access:ORDITEMS.NEXT()
                           Break
                        End !If
                        If ori:ordhno <> tra:StoresAccount      |
                            Then Break.  ! End If
                        If ori:PartNo = wpr:Part_Number
                            local:OrderDate = Clip(local:OrderDate) & ' - ' & Format(orh:thedate,@d17)
                            Break
                        End ! If ori:PartNo = par:Part_Number
                    End !Loop
                End !Loop
                ! Check for retail sales with back orders (DBH: 16-05-2005)
                Save_res_ID = Access:RETSTOCK.SaveFile()
                Access:RETSTOCK.ClearKey(res:DespatchedKey)
                res:Despatched           = 'PEN'
                Set(res:DespatchedKey,res:DespatchedKey)
                Loop
                    If Access:RETSTOCK.NEXT()
                       Break
                    End !If
                    If res:Despatched           <> 'PEN'      |
                        Then Break.  ! End If
                    Access:RETSALES.Clearkey(ret:Ref_Number_Key)
                    ret:Ref_Number = res:Ref_Number
                    If Access:RETSALES.Tryfetch(ret:Ref_Number_key) = Level:Benign
                        !Found
                        IF ret:Account_Number = tra:StoresAccount
                            If res:Part_Number = wpr:Part_Number
                                local:OrderDate = Clip(local:OrderDate) & ' - ' & Format(ret:Date_Booked,@d17)
                            End ! If ret:Part_Number = par:Part_Number
                        End ! IF ret:AccountNumber = tra:StoresAccount
                    Else ! Access:RETSALES.Tryfetch(ret:Ref_Number_key)
                        !Error
                    End ! Access:RETSALES.Tryfetch(ret:Ref_Number_key)
                End !Loop
                Access:RETSTOCK.RestoreFile(Save_res_ID)

                excexp:DateOrdered         = Sub(local:OrderDate,4,255)

                !Main Store Current STock In Head
                local:CountExchanges = 0
                Access:EXCHANGE.ClearKey(xch:AvailLocModel)
                xch:Available    = 'AVL'
                xch:Location     = tmp:MainStoreLocation
                xch:Model_Number = job:Model_Number
                Set(xch:AvailLocModel,xch:AvailLocModel)
                Loop
                    If Access:EXCHANGE.NEXT()
                       Break
                    End !If
                    If xch:Available    <> 'AVL'      |
                    Or xch:Location     <> tmp:MainStoreLocation      |
                    Or xch:Model_Number <> job:Model_Number      |
                        Then Break.  ! End If
                    local:CountExchanges += 1                
                End !Loop 
                excexp:MainStoreStockOnHand  = local:CountExchanges
            
                !RRC Stock In Hand
                local:CountExchanges = 0
                Access:EXCHANGE.ClearKey(xch:AvailLocModel)
                xch:Available    = 'AVL'
                xch:Location     = tra:SiteLocation
                xch:Model_Number = job:Model_Number
                Set(xch:AvailLocModel,xch:AvailLocModel)
                Loop
                    If Access:EXCHANGE.NEXT()
                       Break
                    End !If
                    If xch:Available    <> 'AVL'      |
                    Or xch:Location     <> tra:SiteLocation      |
                    Or xch:Model_Number <> job:Model_Number      |
                        Then Break.  ! End If
                    local:CountExchanges += 1                
                End !Loop 
                excexp:RRCStockOnHand  = local:CountExchanges
            Else !If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                !Error
                excexp:MainStoreBinNumber  = 'UNKNOWN'
                !Date Ordered
                excexp:DateOrdered         = 'UNKNOWN'
                !Main Store Stock On Hand
                excexp:MainStoreStockOnHand = 'UNKNOWN'

            End !If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
    End ! Case func:PartType
    Add(EXCHExportFile)

    tmp:ExchangeExportedCount += 1
local.WriteLine4            Procedure()         !48 Hour Exchange Export
local:DaysInStatus      Long()
local:CountExchanges    Long()
Code
    Clear(exp:Record)
    !Job Number
    excexp:JobNumber               = job:Ref_NUmber
    !Head Account Name
    Access:TRADEACC_ALIAS.ClearKey(tra_ali:Account_Number_Key)
    tra_ali:Account_Number = wob:HeadAccountNumber
    If Access:TRADEACC_ALIAS.TryFetch(tra_ali:Account_Number_Key) = Level:Benign
        !Found
        excexp:HeadAccountName     = tra_ali:Company_Name
    Else !If Access:TRADEACC_ALIAS.TryFetch(tra_ali:Account_Number_Key) = Level:Benign
        !Error
        excexp:HeadAccountName     = 'UNKNOWN'
    End !If Access:TRADEACC_ALIAS.TryFetch(tra_ali:Account_Number_Key) = Level:Benign
    !Current Status
    excexp:CurrentStatus           = job:Current_Status
    !Number Of Days In Status
    local:DaysInStatus = 0
    Access:AUDSTATS.ClearKey(aus:StatusTypeRecordKey)
    aus:RefNumber    = job:Ref_NUmber
    aus:Type         = 'JOB'
    Set(aus:StatusTypeRecordKey,aus:StatusTypeRecordKey)
    Loop
        If Access:AUDSTATS.NEXT()
           Break
        End !If
        If aus:RefNumber    <> job:Ref_Number       |
        Or aus:Type         <> 'JOB'      |
            Then Break.  ! End If
        If Sub(aus:NewStatus,1,3) = Sub(job:Current_Status,1,3)
            local:DaysInStatus = Today() - aus:DateChanged
        End ! If Sub(aus:NewStatus,1,3) = Sub(job:Current_Status,1,3)
    End !Loop
    excexp:NumberOfDaysInStatus    = local:DaysInStatus
    !Manufacturer
    excexp:Manufacturer            = job:Manufacturer
    !Model Number
    excexp:ModelNumber             = '''' & job:Model_Number
    !Check 48 Hour orders for pending unit - TrkBs:  (DBH: 07-03-2005)
    excexp:MainStoreBinNumber  = ''
    excexp:DateOrdered         = ''
    Access:EXCHOR48.ClearKey(ex4:JobNumberKey)
    ex4:Location  = tra_ali:SiteLocation
    ex4:JobNumber = job:Ref_Number
    Set(ex4:JobNumberKey,ex4:JobNumberKey)
    Loop
        If Access:EXCHOR48.NEXT()
           Break
        End !If
        If ex4:Location  <> tra_ali:SiteLocation      |
        Or ex4:JobNumber <> job:Ref_Number      |
            Then Break.  ! End If
        !Make sure unit was ordered from the criteria location - TrkBs:  (DBH: 07-03-2005)
        If ex4:Location <> tra:SiteLocation
            Cycle
        End ! If ex4:Location <> tra_SiteLocation

        !Has a waybill been produced for this unit, for this job? - TrkBs:  (DBH: 09-03-2005)
        FoundWaybill# = False
        Access:WAYITEMS.ClearKey(wai:WaybillLookupKey)
        wai:IsExchange = True
        wai:Ref_Number = xch:Ref_Number
        Set(wai:WaybillLookupKey,wai:WaybillLookupKey)
        Loop
            If Access:WAYITEMS.NEXT()
               Break
            End !If
            If wai:IsExchange <> True      |
            Or wai:Ref_Number <> xch:Ref_Number      |
                Then Break.  ! End If
            If wai:JobNumber48Hour = job:Ref_Number         
                FoundWaybill# = True
                Break
            End ! If wai:JobNumber48Hour = job:Ref_Number         
        End !Loop
        If FoundWaybill# = True
            Cycle
        End !If FoundWaybill# = True

        Found# = True
        If ex4:AttachedToJob = False
            If ex4:OrderUnitNumber <> 0
                Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
                xch:Ref_Number = ex4:OrderUnitNumber
                If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign
                    !Found
                    excexp:MainStoreBinNumber   = xch:Shelf_Location
                    excexp:DateOrdered          = Format(ex4:DateOrdered,@d17)
                    Break
                Else !If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign
                    !Error
                End !If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign
            End ! If ex4:OrderUnitNumber <> 0
        End ! If ex4:AttachedToJob = False
    End !Loop

    If Found# <> True
        Return
    End ! If Found# <> True


    !Main Store Current STock In Head
    local:CountExchanges = 0
    Access:EXCHANGE.ClearKey(xch:AvailLocModel)
    xch:Available    = 'AVL'
    xch:Location     = tmp:MainStoreLocation
    xch:Model_Number = job:Model_Number
    Set(xch:AvailLocModel,xch:AvailLocModel)
    Loop
        If Access:EXCHANGE.NEXT()
           Break
        End !If
        If xch:Available    <> 'AVL'      |
        Or xch:Location     <> tmp:MainStoreLocation      |
        Or xch:Model_Number <> job:Model_Number      |
            Then Break.  ! End If
        local:CountExchanges += 1                
    End !Loop 
    excexp:MainStoreStockOnHand  = local:CountExchanges

    !RRC Stock In Hand
    local:CountExchanges = 0
    Access:EXCHANGE.ClearKey(xch:AvailLocModel)
    xch:Available    = 'AVL'
    xch:Location     = tra:SiteLocation
    xch:Model_Number = job:Model_Number
    Set(xch:AvailLocModel,xch:AvailLocModel)
    Loop
        If Access:EXCHANGE.NEXT()
           Break
        End !If
        If xch:Available    <> 'AVL'      |
        Or xch:Location     <> tra:SiteLocation      |
        Or xch:Model_Number <> job:Model_Number      |
            Then Break.  ! End If
        local:CountExchanges += 1                
    End !Loop 
    excexp:RRCStockOnHand  = local:CountExchanges

    Add(EXCHExportFile)

    tmp:ExchangeExportedCount += 1
Local.DrawBox       Procedure(String func:TL,String func:TR,String func:BL,String func:BR,Long func:Colour)
Code
    If func:BR = ''
        func:BR = func:TR
    End !If func:BR = ''

    If func:BL = ''
        func:BL = func:TL
    End !If func:BL = ''

    If func:Colour = 0
        func:Colour = oix:ColorWhite
    End !If func:Colour = ''
    E1.SetCellBackgroundColor(func:Colour,func:TL,func:BR)
    E1.SetCellBorders(func:BL,func:BR,oix:BorderEdgeBottom,oix:LineStyleContinuous)
    E1.SetCellBorders(func:TL,func:TR,oix:BorderEdgeTop,oix:LineStyleContinuous)
    E1.SetCellBorders(func:TL,func:BL,oix:BorderEdgeLeft,oix:LineStyleContinuous)
    E1.SetCellBorders(func:TR,func:BR,oix:BorderEdgeRight,oix:LineStyleContinuous)
Local.UpdateProgressWindow      Procedure(String    func:Text)
Code
    staque:StatusMessage = Clip(func:Text)
    Add(StatusQueue)
    Select(?List1,Records(StatusQueue))
    Display()
local.DidStatusChangeInTime         Procedure(String func:Status)
local:DateStatusChanged         Date()
Code
    !When did the status change? - TrkBs:  (DBH: 04-03-2005)
    local:DateStatusChanged = Today()
    Access:AUDSTATS.ClearKey(aus:RefRecordNumberKey)
    aus:RefNumber    = job:Ref_Number
    Set(aus:RefRecordNumberKey,aus:RefRecordNumberKey)
    Loop
        If Access:AUDSTATS.NEXT()
           Break
        End !If
        If aus:RefNumber    <> job:Ref_Number      |
            Then Break.  ! End If
        If Sub(aus:NewStatus,1,3) = Sub(func:Status,1,3)
            local:DateStatusChanged = aus:DateChanged
        End ! If Sub(aus:NewStatus,1,3) = Sub(job:Current_Status,1,3)
    End !Loop

    If local:DateStatusChanged < (Today() - tmp:NumberOfDays)
        Return False
    Else ! If local:DateStatusChanged < (Today() - tmp:NumberOfDays)
        Return True
    End ! If local:DateStatusChanged < (Today() - tmp:NumberOfDays)
local.CheckARCJobs      Procedure(String func:Status,String func:Type)
Code

    !message('In check arc job by status '&clip(func:Status))
    Access:JOBS.ClearKey(job:By_Status)
    job:Current_Status = func:Status
    Set(job:By_Status,job:By_Status)
    Loop
        If Access:JOBS.NEXT()
           Break
        End !If
        !message('Found job with status '&clip(job:Current_status))
        If job:Current_Status <> func:Status      |
            Then Break.  ! End If

        Do GetNextRecord2
        Do CancelCheck
        If tmp:Cancel
            Break
        End !If tmp:Cancel
        !message('Past breaks')
        If local.DidStatusChangeInTime(job:Current_Status) = False
            !message('Status change in time cycle')
            Cycle
        End !If local.DidStatusChangeInTime(job:Current_Status) = False
        Access:WEBJOB.Clearkey(wob:RefNumberKey)
        wob:RefNumber   = job:Ref_Number
        If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
            !Found
        Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
            !Error
            !message('Webjob error cycle')
            Cycle
        End ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign

        Access:JOBSE.Clearkey(jobe:RefNumberKey)
        jobe:RefNumber  = job:Ref_Number
        If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
            !Found

        Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
            !Error
            !message('jobse fetch cycle')
            Cycle

        End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        local.WriteLine(func:Type)
    End !Loop
local.CheckRRCJobs      Procedure(String func:Status,String func:Type)
Code
    Access:WEBJOB.ClearKey(wob:HeadCurrentStatusKey)
    wob:HeadAccountNumber = tra:Account_Number
    wob:Current_Status    = func:Status
    Set(wob:HeadCurrentStatusKey,wob:HeadCurrentStatusKey)
    Loop
        If Access:WEBJOB.NEXT()
           Break
        End !If
        If wob:HeadAccountNumber <> tra:Account_Number      |
        Or wob:Current_Status    <> func:Status      |
            Then Break.  ! End If

        Do GetNextRecord2
        Do CancelCheck
        If tmp:Cancel
            Break
        End !If tmp:Cancel

        Access:JOBS.Clearkey(job:Ref_Number_Key)
        job:Ref_Number  = wob:RefNumber
        If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
            !Found
        Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
            !Error
            Cycle
        End ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign

        Access:JOBSE.Clearkey(jobe:RefNumberKey)
        jobe:RefNumber  = job:Ref_Number
        If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
            !Found

        Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
            !Error
            Cycle
        End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

        If local.DidStatusChangeInTime(job:Current_Status) = False
            Cycle
        End !local.DidStatusChangeInTime(job:CurrentStatus) = False
        local.WriteLine(func:Type)

    End !Loop 
    
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
!---------------------------------------------------------------------------------
E1.Init   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0)
ReturnValue   byte
  CODE
  ReturnValue = PARENT.Init (pStartVisible,pEnableEvents)
  self.TakeSnapShotOfWindowPos()
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
E1.Kill   PROCEDURE (byte pUnloadCOM=1)
ReturnValue   byte
  CODE
  self.RestoreSnapShotOfWindowPos()
  ReturnValue = PARENT.Kill (pUnloadCOM)
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
E1.TakeEvent   PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0)
  CODE
  PARENT.TakeEvent (pEventString1,pEventString2,pEventNumber,pEventType,pEventStatus)
  if pEventType = 0  ! Generated by CapeSoft Office Inside
    case event()
      of event:accepted
        case field()
      end
    end
  end
!Initialise
ExcelSetup          Procedure(Byte      func:Visible)
    Code
    Excel   = Create(0,Create:OLE)
    Excel{prop:Create} = 'Excel.Application'
    Excel{'ASYNC'}  = False
    Excel{'Application.DisplayAlerts'} = False

    Excel{'Application.ScreenUpdating'} = func:Visible
    Excel{'Application.Visible'} = func:Visible
    Excel{'Application.Calculation'} = 0FFFFEFD9h
    excel:ActiveWorkBook    = Excel{'ActiveWorkBook'}
    Yield()

!Make a WorkBook
ExcelMakeWorkBook   Procedure(String    func:Title,String   func:Author,String  func:AppName)
    Code
    Excel{prop:Release} = excel:ActiveWorkBook
    excel:ActiveWorkBook = Excel{'Application.Workbooks.Add()'}

    Excel{excel:ActiveWorkBook & '.BuiltinDocumentProperties("Title")'} = func:Title
    Excel{excel:ActiveWorkBook & '.BuiltinDocumentProperties("Author")'} = func:Author
    Excel{excel:ActiveWorkBook & '.BuiltinDocumentProperties("Application Name")'} = func:AppName

    excel:Selected = Excel{'Sheets("Sheet2").Select'}
    Excel{prop:Release} = excel:Selected

    Excel{'ActiveWindow.SelectedSheets.Delete'}

    excel:Selected = Excel{'Sheets("Sheet1").Select'}
    Excel{prop:Release} = excel:Selected
    Yield()

ExcelMakeSheet      Procedure()
ActiveWorkBook  CString(20)
    Code
    ActiveWorkBook = Excel{'ActiveWorkBook'}

    Excel{ActiveWorkBook & '.Sheets("Sheet3").Select'}
    Excel{prop:Release} = ActiveWorkBook

    Excel{excel:ActiveWorkBook & '.Sheets.Add'}
    Yield()
!Select A Sheet
ExcelSelectSheet    Procedure(String    func:SheetName)
    Code
    Excel{'Sheets("' & Clip(func:SheetName) & '").Select'}
    Yield()
!Setup Sheet Type (P = Portrait, L = Lanscape)
ExcelSheetType      Procedure(String    func:Type)
    Code
    Case func:Type
        Of 'L'
            Excel{'ActiveSheet.PageSetup.Orientation'}  = 2
        Of 'P'
            Excel{'ActiveSheet.PageSetup.Orientation'}  = 1
    End !Case func:Type
    Excel{'ActiveSheet.PageSetup.FitToPagesWide'}  = 1
    Excel{'ActiveSheet.PageSetup.FitToPagesTall'}  = 9999
    Excel{'ActiveSheet.PageSetup.Order'}  = 2

    Yield()
ExcelHorizontal     Procedure(String    func:Direction)
Selection   Cstring(20)
    Code
    Selection = Excel{'Selection'}
    Case func:Direction
        Of 'Centre'
            Excel{Selection & '.HorizontalAlignment'} = 0FFFFEFF4h
        Of 'Left'
            Excel{Selection & '.HorizontalAlignment'} = 0FFFFEFDDh
        Of 'Right'
            Excel{Selection & '.HorizontalAlignment'} = 0FFFFEFC8h
    End !Case tmp:Direction
    Excel{prop:Release} = Selection
    Yield()
ExcelVertical   Procedure(String func:Direction)
Selection   CString(20)
    Code
    Selection = Excel{'Selection'}
    Case func:Direction
        Of 'Top'
            Excel{Selection & '.VerticalAlignment'} = 0FFFFEFC0h
        Of 'Centre'
            Excel{Selection & '.VerticalAlignment'} = 0FFFFEFF4h
        Of 'Bottom'
            Excel{Selection & '.VerticalAlignment'} = 0FFFFEFF5h
    End ! Case func:Direction
    Excel{prop:Release} = Selection
    Yield()

ExcelCell   Procedure(String    func:Text,Byte    func:Bold)
Selection   Cstring(20)
    Code
    Selection = Excel{'Selection'}
    If func:Bold
        Excel{Selection & '.Font.Bold'} = True
    Else
        Excel{Selection & '.Font.Bold'} = False
    End !If func:Bold
    Excel{prop:Release} = Selection

    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.Formula'}  = func:Text
    Excel{excel:Selected & '.Offset(0, 1).Select'}
    Excel{prop:Release} = excel:Selected
    Yield()
ExcelFormatCell     Procedure(String    func:Format)
    Code
    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.NumberFormat'} = func:Format
    Excel{prop:Release} = excel:Selected
    Yield()
ExcelFormatRange    Procedure(String    func:Range,String   func:Format)
Selection       Cstring(20)
    Code

    ExcelSelectRange(func:Range)
    Selection   = Excel{'Selection'}
    Excel{Selection & '.NumberFormat'} = func:Format
    Excel{prop:Release} = Selection
    Yield()

ExcelNewLine    Procedure(Long func:Number)
    Code
    Loop excelloop# = 1 to func:Number
        ExcelSelectRange('A' & (ExcelCurrentRow() + 1))
    End !Loop excelloop# = 1 to func:Number
    !excel:Selected = Excel{'ActiveCell'}
    !Excel{excel:Selected & '.Offset(0, -' & Excel{excel:Selected & '.Column'} - 1 & ').Select'}
    !Excel{excel:Selected & '.Offset(1, 0).Select'}
    !Excel{prop:Release} = excel:Selected
    Yield()

ExcelMoveDown   Procedure()
    Code
    ExcelSelectRange(ExcelCurrentColumn() & (ExcelCurrentRow() + 1))
    Yield()
!Set Column Width

ExcelColumnWidth        Procedure(String    func:Range,Long   func:Width)
    Code
    Excel{'ActiveSheet.Columns("' & Clip(func:Range) & '").ColumnWidth'} = func:Width
    Yield()
ExcelCellWidth          Procedure(Long  func:Width)
    Code
    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.ColumnWidth'} = func:Width
    Excel{prop:Release} = excel:Selected
    Yield()
ExcelAutoFit            Procedure(String func:Range)
    Code
    Excel{'ActiveSheet.Columns("' & Clip(func:Range) & '").Columns.AutoFit'}
    Yield()
!Set Gray Box

ExcelGrayBox            Procedure(String    func:Range)
Selection   CString(20)
    Code
    Selection = Excel{'Selection'}
    Excel{'Range("' & Clip(func:Range) & '").Select'}
    Excel{Selection & '.Interior.ColorIndex'} = 15
    Excel{Selection & '.Interior.Pattern'} = 1
    Excel{Selection & '.Interior.PatternColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(7).LineStyle'} = 1
    Excel{Selection & '.Borders(7).Weight'} = 2
    Excel{Selection & '.Borders(7).ColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(10).LineStyle'} = 1
    Excel{Selection & '.Borders(10).Weight'} = 2
    Excel{Selection & '.Borders(10).ColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(8).LineStyle'} = 1
    Excel{Selection & '.Borders(8).Weight'} = 2
    Excel{Selection & '.Borders(8).ColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(9).LineStyle'} = 1
    Excel{Selection & '.Borders(9).Weight'} = 2
    Excel{Selection & '.Borders(9).ColorIndex'} = 0FFFFEFF7h
    Excel{prop:Release} = Selection
    Yield()
ExcelGrid   Procedure(String    func:Range,Byte  func:Left,Byte  func:Top,Byte   func:Right,Byte func:Bottom,Byte func:Colour)
Selection   Cstring(20)
    Code
    Excel{'Range("' & Clip(func:Range) & '").Select'}
    Selection = Excel{'Selection'}
    If func:Colour
        Excel{Selection & '.Interior.ColorIndex'} = func:Colour
        Excel{Selection & '.Interior.Pattern'} = 1
        Excel{Selection & '.Interior.PatternColorIndex'} = 0FFFFEFF7h
    End !If func:Colour
    If func:Left
        Excel{Selection & '.Borders(7).LineStyle'} = 1
        Excel{Selection & '.Borders(7).Weight'} = 2
        Excel{Selection & '.Borders(7).ColorIndex'} = 0FFFFEFF7h
    End !If func:Left

    If func:Right
        Excel{Selection & '.Borders(10).LineStyle'} = 1
        Excel{Selection & '.Borders(10).Weight'} = 2
        Excel{Selection & '.Borders(10).ColorIndex'} = 0FFFFEFF7h
    End !If func:Top

    If func:Top
        Excel{Selection & '.Borders(8).LineStyle'} = 1
        Excel{Selection & '.Borders(8).Weight'} = 2
        Excel{Selection & '.Borders(8).ColorIndex'} = 0FFFFEFF7h
    End !If func:Right

    If func:Bottom
        Excel{Selection & '.Borders(9).LineStyle'} = 1
        Excel{Selection & '.Borders(9).Weight'} = 2
        Excel{Selection & '.Borders(9).ColorIndex'} = 0FFFFEFF7h
    End !If func:Bottom
    Excel{prop:Release} = Selection
    Yield()
!Select a range of cells
ExcelSelectRange        Procedure(String    func:Range)
    Code
    Excel{'Range("' & Clip(func:Range) & '").Select'}
    Yield()
!Change font size
ExcelFontSize           Procedure(Byte  func:Size)
    Code
    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.Font.Size'}   = func:Size
    Excel{prop:Release} = excel:Selected
    Yield()
!Sheet Name
ExcelSheetName          Procedure(String    func:Name)
    Code
    Excel{'ActiveSheet.Name'} = func:Name
    Yield()
ExcelAutoFilter         Procedure(String    func:Range)
Selection   Cstring(20)
    Code
    ExcelSelectRange(func:Range)
    Selection = Excel{'Selection'}
    Excel{Selection & '.AutoFilter'}
    Excel{prop:Release} = Selection
    Yield()
ExcelDropAllSheets      Procedure()
    Code
    Excel{prop:Release} = excel:ActiveWorkBook
    Loop While Excel{'WorkBooks.Count'} > 0
        excel:ActiveWorkBook = Excel{'ActiveWorkBook'}
        Excel{'ActiveWorkBook.Close(1)'}
        Excel{prop:Release} = excel:ActiveWorkBook
    End !Loop While Excel{'WorkBooks.Count'} > 0
    Yield()
ExcelClose              Procedure()
!xlCalculationAutomatic
    Code
    Excel{'Application.Calculation'}= 0FFFFEFF7h
    Excel{'Application.Quit'}
    Excel{prop:Deactivate}
    Destroy(Excel)
    Yield()
ExcelDeleteSheet        Procedure(String    func:SheetName)
    Code
    ExcelSelectSheet(func:SheetName)
    Excel{'ActiveWindow.SelectedSheets.Delete'}
    Yield()
ExcelSaveWorkBook       Procedure(String    func:Name)
    Code
    Excel{'Application.ActiveWorkBook.SaveAs("' & LEFT(CLIP(func:Name)) & '")'}
    Excel{'Application.ActiveWorkBook.Close()'}
   Excel{'Application.Calculation'} = 0FFFFEFF7h
    Excel{'Application.Quit'}

    Excel{PROP:DEACTIVATE}
    YIELD()
ExcelFontColour         Procedure(String    func:Range,Long func:Colour)
    Code
    !16 = Gray
    ExcelSelectRange(func:Range)
    Excel{'Selection.Font.ColorIndex'} = func:Colour
    Yield()
ExcelWrapText           Procedure(String    func:Range,Byte func:True)
Selection   Cstring(20)
    Code
    ExcelSelectRange(func:Range)
    Selection = Excel{'Selection'}
    Excel{Selection & '.WrapText'} = func:True
    Excel{prop:Release} = Selection
    Yield()
ExcelCurrentColumn      Procedure()
CurrentColumn   String(20)
    Code
    excel:Selected = Excel{'ActiveCell'}
    CurrentColumn = Excel{excel:Selected & '.Column'}
    Excel{prop:Release} = excel:Selected
    Yield()
    Return CurrentColumn

ExcelCurrentRow         Procedure()
CurrentRow      String(20)
    Code
    excel:Selected = Excel{'ActiveCell'}
    CurrentRow = Excel{excel:Selected & '.Row'}
    Excel{prop:Release} = excel:Selected
    Yield()
    Return CurrentRow

ExcelPasteSpecial       Procedure(String    func:Range)
Selection       CString(20)
    Code
    ExcelSelectRange(func:Range)
    Selection   = Excel{'ActiveCell'}
    Excel{Selection & '.PasteSpecial'}
    Excel{prop:Release} = Selection
    Yield()

ExcelConvertFormula     Procedure(String    func:Formula)
    Code
    Return Excel{'Application.ConvertFormula("' & Clip(func:Formula) & '",' & 0FFFFEFCAh & ',' & 1 & ')'}

ExcelGetFilename        Procedure(Byte  func:DontAsk)
sav:Path        CString(255)
func:Desktop     CString(255)
    Code

        SHGetSpecialFolderPath( GetDesktopWindow(), func:Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
        func:Desktop = Clip(func:Desktop) & '\ServiceBase Export'

        !Does the Export Folder already Exists?
        If ~Exists(Clip(func:Desktop))
            If ~MkDir(func:Desktop)
                Return Level:Fatal

            End !If MkDir(func:Desktop)
        End !If Exists(Clip(tmp:Desktop))

        Error# = 0
        sav:Path = Path()
        SetPath(func:Desktop)

        func:Desktop = Clip(func:Desktop) & '\' & CLIP(Excel:ProgramName) & ' ' & FORMAT(TODAY(), @D12) & '.xls'

        If func:DontAsk = False
            IF NOT FILEDIALOG('Save Spreadsheet', func:Desktop, 'Microsoft Excel Workbook|*.XLS', |
                FILE:KeepDir + FILE:Save + FILE:NoError + FILE:LongName)
                Error# = 1
            End!IF NOT FILEDIALOG('Save Spreadsheet', tmp:Desktop, 'Microsoft Excel Workbook|*.XLS', |
        End !If func:DontAsk = True

        SetPath(sav:Path)

        If Error#
            Return Level:Fatal
        End !If Error#
        excel:FileName    = func:Desktop
        Return Level:Benign

ExcelGetDirectory       Procedure()
sav:Path        CString(255)
func:Desktop    CString(255)
    Code
        SHGetSpecialFolderPath( GetDesktopWindow(), func:Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
        func:Desktop = Clip(func:Desktop) & '\ServiceBase Export\'
        !Does the Export Folder already Exists?
        Error# = 0
        If ~Exists(Clip(func:Desktop))
            If ~MkDir(func:Desktop)
                If Not FileDialog('Save Spreadsheet To Folder', func:Desktop, ,FILE:KeepDir+ File:Save + File:NoError + File:LongName + File:Directory)
                    Return Level:Fatal
                End !+ File:LongName + File:Directory)
            End !If MkDir(func:Desktop)
        End !If Exists(Clip(tmp:Desktop))

        excel:FileName  = func:Desktop
        Return Level:Benign

ExcelColumnLetter     Procedure(Long func:ColumnNumber)
local:Over26        Long()
Code
    local:Over26 = 0
    If func:ColumnNumber > 26
        Loop Until func:ColumnNumber <= 26
            local:Over26 += 1
            func:ColumnNumber -= 26
        End !Loop Until ColumnNumber <= 26.
    End !If func:ColumnNumber > 26

    If local:Over26 > 26
        Stop('ExcelColumnLetter Procedure Out Of Range!')
    End !If local:Over26 > 26
    If local:Over26 > 0
        Return Clip(CHR(local:Over26 + 64)) & Clip(CHR(func:ColumnNumber + 64))
    Else !If local:Over26 > 0
        Return Clip(CHR(func:ColumnNumber + 64))
    End !If local:Over26 > 0
ExcelOpenDoc        Procedure(String func:FileName)
Code
    Excel{'Workbooks.Open("' & Clip(func:FileName) & '")'}
ExcelFreeze         Procedure(String func:Cell)
Code
    Excel{'Range("' & Clip(func:Cell) & '").Select'}
    Excel{'ActiveWindow.FreezePanes'} = True

BRW4.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = tra:Account_Number
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      tmp:Tag = ''
    ELSE
      tmp:Tag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:tag = '*')
    SELF.Q.tmp:Tag_Icon = 2
  ELSE
    SELF.Q.tmp:Tag_Icon = 1
  END


BRW4.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW4.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW4::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW4::RecordStatus=ReturnValue
  IF BRW4::RecordStatus NOT=Record:OK THEN RETURN BRW4::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = tra:Account_Number
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::5:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW4::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW4::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW4::RecordStatus
  RETURN ReturnValue

Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
