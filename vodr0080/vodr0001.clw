

   MEMBER('vodr0080.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('VODR0001.INC'),ONCE        !Local module procedure declarations
                     END


ProductivityReport PROCEDURE                          !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::9:TAGFLAG          BYTE(0)
DASBRW::9:TAGMOUSE         BYTE(0)
DASBRW::9:TAGDISPSTATUS    BYTE(0)
DASBRW::9:QUEUE           QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
tmp:Clipboard        ANY
Local                CLASS
DrawBox              Procedure(String func:TL,String func:TR,String func:BL,String func:BR,Long func:Colour)
WriteLine            Procedure(String func:Type,String func:EntryType)
UpdateProgressWindow Procedure(String  func:Text)
EngineerFromCurrentSite Procedure(String func:UserCode),Byte
OtherStatusExists    Procedure(String func:Type,String func:Status,Date func:Date,Long func:RecordNumber,String func:UserCode,Byte func:Previous), Byte
                     END
StatusQueue          QUEUE,PRE(staque)
StatusMessage        STRING(60)
                     END
save_aus_id          USHORT
TempFilePath         CSTRING(255)
tmp:StartDate        DATE
tmp:EndDate          DATE
tmp:UserName         STRING(70)
tmp:HeadAccountNumber STRING(30)
tmp:ARCLocation      STRING(30)
Progress:Thermometer BYTE(0)
tmp:ARCAccount       BYTE(0)
tmp:ARCCompanyName   STRING(30)
tmp:ARCUser          BYTE(0)
TempFileQueue        QUEUE,PRE(tmpque)
AccountNumber        STRING(30)
CompanyName          STRING(30)
Count                LONG
FileName             STRING(255)
JobsCount            LONG
ExcelFileName        STRING(255)
                     END
EngineerQueue        QUEUE,PRE(engque)
Surname              STRING(30),NAME('engqueSurname')
Forename             STRING(30),NAME('engqueForename')
Row                  STRING(20)
Allocations          LONG
JobCompleted         LONG
Exchange             LONG
Before3rdParty       LONG
After3rdParty        LONG
SparesRequested      LONG
NFF                  LONG
RNR                  LONG
RTM                  LONG
Level0               LONG
Level1               LONG
Level2               LONG
Level25              LONG
Level3               LONG
SoftwareUpgrade      LONG
Estimate             LONG
Unlock               LONG
AccessoryExchange    LONG
QA                   LONG
Weight               LONG
                     END
UniqueJobQueue       QUEUE,PRE(unijob)
JobNumber            LONG
                     END
tmp:AllAccounts      BYTE(0)
tmp:Tag              STRING(1)
tmp:CurrentSiteLocation STRING(30)
tmp:VersionNumber    STRING(30)
BRW8::View:Browse    VIEW(TRADEACC)
                       PROJECT(tra:Account_Number)
                       PROJECT(tra:Company_Name)
                       PROJECT(tra:RecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tmp:Tag                LIKE(tmp:Tag)                  !List box control field - type derived from local data
tmp:Tag_Icon           LONG                           !Entry's icon ID
tra:Account_Number     LIKE(tra:Account_Number)       !List box control field - type derived from field
tra:Company_Name       LIKE(tra:Company_Name)         !List box control field - type derived from field
tra:RecordNumber       LIKE(tra:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Excel                SIGNED !OLE Automation holder
excel:ProgramName    CString(255)
excel:ActiveWorkBook CString(20)
excel:Selected       CString(20)
excel:FileName       CString(255)
loc:Version          Cstring(30)
window               WINDOW('Productivity Report Criteria'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(60,38,560,360),USE(?PanelMain),FILL(0D6EAEFH)
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(64,56,552,44),USE(?PanelTip),FILL(0D6EAEFH)
                       GROUP('Top Tip'),AT(68,60,544,36),USE(?GroupTip),BOXED,TRN
                       END
                       STRING(@s255),AT(72,76,496,),USE(SRN:TipText),TRN
                       BUTTON,AT(576,64,36,32),USE(?ButtonHelp),TRN,FLAT,ICON('F1Help.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Productivity Report Criteria'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(64,104,552,258),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Criteria'),USE(?Tab1)
                           PROMPT('Date Completed From'),AT(241,114),USE(?tmp:StartDate:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(341,114,64,10),USE(tmp:StartDate),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Start Date'),TIP('Start Date'),REQ,UPR
                           BUTTON,AT(413,110),USE(?PopCalendar),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Date Completed To'),AT(241,132),USE(?tmp:EndDate:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(341,132,64,10),USE(tmp:EndDate),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Jobs Completed To'),TIP('Jobs Completed To'),REQ,UPR
                           BUTTON,AT(413,130),USE(?PopCalendar:2),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT(''),AT(68,152),USE(?StatusText)
                           CHECK('All Accounts'),AT(240,148),USE(tmp:AllAccounts),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('All Accounts'),TIP('All Accounts'),VALUE('1','0')
                           LIST,AT(240,162,206,194),USE(?List),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11L(2)I@s1@64L(2)|M~Account Number~@s15@120L(2)|M~Company Name~@s30@'),FROM(Queue:Browse)
                           BUTTON('&Rev tags'),AT(120,199,1,1),USE(?DASREVTAG),HIDE
                           BUTTON('sho&W tags'),AT(120,227,1,1),USE(?DASSHOWTAG),HIDE
                           BUTTON,AT(460,208),USE(?DASTAG),TRN,FLAT,ICON('tagitemp.jpg')
                           BUTTON,AT(460,242),USE(?DASTAGAll),TRN,FLAT,ICON('tagallp.jpg')
                           BUTTON,AT(460,280),USE(?DASUNTAGALL),TRN,FLAT,ICON('untagalp.jpg')
                         END
                       END
                       BUTTON,AT(472,366),USE(?Print),TRN,FLAT,ICON('printp.jpg'),DEFAULT
                       BUTTON,AT(544,368),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                       PROMPT('Report Version:'),AT(68,374),USE(?ReportVersion),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                     END

!Progress Window (DBH: 22-03-2004)
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progresswindow WINDOW('Progress...'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH), |
         CENTER,IMM,ICON('cellular3g.ico'),WALLPAPER('sbback.jpg'),TILED,TIMER(1),GRAY,DOUBLE
       PANEL,AT(160,64,360,300),USE(?Panel5),FILL(0D6E7EFH)
       PANEL,AT(164,68,352,12),USE(?Panel1),FILL(09A6A7CH)
       LIST,AT(208,88,268,206),USE(?List1),VSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH), |
           FORMAT('20L(2)|M'),FROM(StatusQueue),GRID(COLOR:White)
       PANEL,AT(164,84,352,246),USE(?Panel4),FILL(09A6A7CH)
       PROGRESS,USE(progress:thermometer),AT(206,314,268,12),RANGE(0,100)
       STRING(''),AT(259,300,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,080FFFFH,FONT:bold), |
           COLOR(09A6A7CH)
       STRING(''),AT(232,136,161,10),USE(?progress:pcttext),TRN,HIDE,CENTER,FONT('Arial',8,,)
       PANEL,AT(164,332,352,28),USE(?Panel2),FILL(09A6A7CH)
       PROMPT('Report Progress'),AT(168,70),USE(?WindowTitle2),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
       BUTTON,AT(444,332),USE(?ProgressCancel),TRN,FLAT,LEFT,ICON('Cancelp.jpg')
       BUTTON,AT(376,332),USE(?Button:OpenReportFolder),TRN,FLAT,HIDE,ICON('openrepp.jpg')
       BUTTON,AT(444,332),USE(?Finish),TRN,FLAT,HIDE,ICON('Finishp.jpg')
     END
!Export Files (DBH: 22-03-2004)
ExportFile    File,Driver('BASIC'),Pre(exp),Name(glo:ExportFile),Create,Bindable,Thread
Record                  Record
JobNumber               String(30)
BranchNumber            String(30)
DateBooked              String(30)
DateCompleted           String(30)
EngineerSurname         String(30)
EngineerForename        String(30)
EngineerSkillLevel      String(30)
JobSkillLevel           String(30)
EngineerOption          String(30)
CChargeType             String(30)
CRepairType             String(30)
WChargeType             String(30)
WRepairType             String(30)
ThirdParty              String(30)
Manufacturer            String(30)
ModelNumber             String(30)
UnitType                String(30)
IMEINumber              String(30)
MSN                     String(30)
SummaryColumn           String(30)
ExchangeDate            String(30)
SparesRequested         String(30)
                        End
                    End
ARCExportFile    File,Driver('BASIC'),Pre(arcexp),Name(glo:ARCExportFile),Create,Bindable,Thread
Record                  Record
JobNumber               String(30)
BranchNumber            String(30)
DateBooked              String(30)
DateCompleted           String(30)
EngineerSurname         String(30)
EngineerForename        String(30)
EngineerSkillLevel      String(30)
JobSkillLevel           String(30)
EngineerOption          String(30)
CChargeType             String(30)
CRepairType             String(30)
WChargeType             String(30)
WRepairType             String(30)
ThirdParty              String(30)
Manufacturer            String(30)
ModelNumber             String(30)
UnitType                String(30)
IMEINumber              String(30)
MSN                     String(30)
SummaryColumn           String(30)
ExchangeDate            String(30)
SparesRequested         String(30)
                        End
                    End
Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?Prog:CNPrompt),FONT(,14,,FONT:bold)
     END
***

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
E1                   Class(oiExcel)
Init                   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0),byte,proc,virtual,name('INIT@F17OIEXCEL')
Kill                   PROCEDURE (byte pUnloadCOM=1),byte,proc,virtual,name('KILL@F17OIEXCEL')
TakeEvent              PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0),virtual,name('TAKEEVENT@F17OIEXCEL')
                     End

BRW8                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW8::Sort0:Locator  StepLocatorClass                 !Default Locator
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
    Map
ExcelSetup          Procedure(Byte      func:Visible)

ExcelMakeWorkBook   Procedure(String    func:Title,String   func:Author,String  func:AppName)

ExcelMakeSheet      Procedure()

ExcelSheetType      Procedure(String    func:Type)

ExcelHorizontal     Procedure(String    func:Direction)

ExcelVertical        Procedure(String    func:Direction)

ExcelCell   Procedure(String    func:Text,Byte  func:Bold)

ExcelFormatCell     Procedure(String    func:Format)

ExcelFormatRange    Procedure(String    func:Range,String   func:Format)

ExcelNewLine    Procedure(Long  func:Number)

ExcelMoveDown   Procedure()

ExcelColumnWidth        Procedure(String    func:Range,Long   func:Width)

ExcelCellWidth          Procedure(Long  func:Width)

ExcelAutoFit            Procedure(String    func:Range)

ExcelGrayBox            Procedure(String    func:Range)

ExcelGrid   Procedure(String    func:Range,Byte  func:Left,Byte  func:Top,Byte   func:Right,Byte func:Bottom,Byte func:Colour)

ExcelSelectRange        Procedure(String    func:Range)

ExcelFontSize           Procedure(Byte  func:Size)

ExcelSheetName          Procedure(String    func:Name)

ExcelSelectSheet    Procedure(String    func:SheetName)

ExcelAutoFilter         Procedure(String    func:Range)

ExcelDropAllSheets      Procedure()

ExcelDeleteSheet        Procedure(String    func:SheetName)

ExcelClose              Procedure()

ExcelSaveWorkBook       Procedure(String    func:Name)

ExcelFontColour         Procedure(String    func:Range,Long func:Colour)

ExcelWrapText           Procedure(String    func:Range,Byte func:True)

ExcelGetFilename        Procedure(Byte      func:DontAsk),Byte

ExcelGetDirectory       Procedure(),Byte

ExcelCurrentColumn      Procedure(),String

ExcelCurrentRow         Procedure(),String

ExcelPasteSpecial       Procedure(String    func:Range)

ExcelConvertFormula     Procedure(String    func:Formula),String

ExcelColumnLetter Procedure(Long  func:ColumnNumber),String

ExcelOpenDoc            Procedure(String    func:FileName)

ExcelFreeze             Procedure(String    func:Cell)
    End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::9:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW8.UpdateBuffer
   glo:Queue.Pointer = tra:Account_Number
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = tra:Account_Number
     ADD(glo:Queue,glo:Queue.Pointer)
    tmp:Tag = '*'
  ELSE
    DELETE(glo:Queue)
    tmp:Tag = ''
  END
    Queue:Browse.tmp:Tag = tmp:Tag
  IF (tmp:tag = '*')
    Queue:Browse.tmp:Tag_Icon = 2
  ELSE
    Queue:Browse.tmp:Tag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::9:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW8.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW8::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = tra:Account_Number
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW8.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::9:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW8.Reset
  SETCURSOR
  BRW8.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::9:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::9:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::9:QUEUE = glo:Queue
    ADD(DASBRW::9:QUEUE)
  END
  FREE(glo:Queue)
  BRW8.Reset
  LOOP
    NEXT(BRW8::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::9:QUEUE.Pointer = tra:Account_Number
     GET(DASBRW::9:QUEUE,DASBRW::9:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = tra:Account_Number
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW8.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::9:DASSHOWTAG Routine
   CASE DASBRW::9:TAGDISPSTATUS
   OF 0
      DASBRW::9:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::9:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::9:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW8.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
Reporting       Routine  !Do the Report (DBH: 22-03-2004)
Data
local:LocalPath              String(255)
local:CurrentAccount    String(30)
local:ReportStartDate   Date()
local:ReportStartTime   Time()

local:UserCode          String(3)
local:Date              Date()

local:EntryType         String(60)

local:JobCount          Long()
local:ARCEngineerOccuranceCount        Long()
local:SubARCEngineerOccuranceCount      Long()
local:RRCEngineerOccuranceCount        Long()

local:SavePath          CString(255)

local:RepairTypeToUse   String(255)
local:FoundRepairType   Byte(0)
local:Desktop           cstring(255)
Code
    !Set the temp folder for the csv files (DBH: 10-03-2004)
    If GetTempPathA(255,TempFilePath)
        If Sub(TempFilePath,-1,1) = '\'
            local:LocalPath = Clip(TempFilePath) & ''
        Else !If Sub(TempFilePath,-1,1) = '\'
            local:LocalPath = Clip(TempFilePath) & '\'
        End !If Sub(TempFilePath,-1,1) = '\'
    End

    !Set the folder for the excel file (DBH: 10-03-2004)
    excel:ProgramName = 'Productivity Report'
    SHGetSpecialFolderPath( GetDesktopWindow(), local:Desktop, 5, FALSE )
    local:Desktop = Clip(local:Desktop) & '\ServiceBase Export'

    !Does the Export Folder already Exists?
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    local:Desktop = Clip(local:Desktop) & '\' & Clip(excel:ProgramName)
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    excel:FileName = Clip(local:Desktop) & '\'! & Clip(excel:ProgramName) & ' ' & Format(Today(),@d12)

!    If Exists(excel:FileName & '.xls')
!        Remove(excel:FileName & '.xls')
!        If Error()
!            Beep(Beep:SystemHand)  ;  Yield()
!            Case Missive('Cannot get access to the selected document:'&|
!                '|' & Clip(excel:FileName) & ''&|
!                '|'&|
!                '|Ensure the file is not in use and try again.','ServiceBase',|
!                           'mstop.jpg','/&OK')
!                Of 1 ! &OK Button
!            End!Case Message
!            Exit
!        End !If Error()
!    End !If Exists(excel:FileName)
    If COMMAND('/DEBUG')
        Remove('c:\debug.ini')
    End !If COMMAND('/DEBUG')

    !Open Program Window (DBH: 10-03-2004)
    recordspercycle         = 25
    recordsprocessed        = 0
    percentprogress         = 0
    progress:thermometer    = 0
    recordstoprocess        = 50
    Open(ProgressWindow)

    ?progress:userstring{prop:text} = 'Running...'
    ?progress:pcttext{prop:text} = '0% Completed'


    local:ReportStartDate = Today()
    local:ReportStartTime = Clock()

    If Command('/DEBUG')
        local.UpdateProgressWindow('========')
        local.UpdateProgressWindow('*** DEBUG MODE ***')
        local.UpdateProgressWindow('========')
        local.UpdateProgressWindow('')
    End !If Command('/DEBUG')

    local.UpdateProgressWindow('Report Started: ' & Format(local:ReportStartDate,@d6b) & ' ' & Format(local:ReportStartTime,@t1b))
    local.UpdateProgressWindow('')
    local.UpdateProgressWindow('Creating Export Files...')

    !_____________________________________________________________________

    !CREATE CSV FILES
    !_____________________________________________________________________

    !Create ARC Export File (DBH: 22-03-2004)
    glo:ARCExportFile = Clip(local:LocalPath) & 'PROD-' & Clip(tmp:HeadAccountNumber) & '.csv'
    Remove(ARCExportFile)
    Create(ARCExportFile)
    Open(ARCExportFile)

    local:ARCEngineerOccuranceCount = 0

    Set(tra:Account_Number_Key)
    Loop
        If Access:TRADEACC.Next()
            Break
        End !If Access:TRADEACC.Next()

        Do GetNextRecord2
        Do CancelCheck
        If tmp:Cancel
            Break
        End !If tmp:Cancel = 1

        If tra:BranchIdentification = ''
            Cycle
        End !If tra:BranchIdentification = ''

        If tmp:AllAccounts = False
            glo:Pointer = tra:Account_Number
            Get(glo:Queue,glo:Pointer)
            If Error()
                Cycle
            End !If Error()
        End !If tmp:AllAccounts = False

        tmp:CurrentSiteLocation = tra:SiteLocation

        If local:CurrentAccount = ''
            local:CurrentAccount = tra:Account_Number
        End !If local:CurrentAccount = ''

        !Is this the ARC, or RRC? (DBH: 10-03-2004)
        If tra:Account_Number = tmp:HeadAccountNumber
            tmp:ARCAccount = 1
            local:CurrentAccount = tra:Account_Number
        Else !If tra:Account_Number = tmp:HeadAccountNumber
            tmp:ARCAccount = 0
            !Create RRC Export File (DBH: 22-03-2004)
            glo:ExportFile = Clip(local:LocalPath) & 'PROD-' & Clip(tra:Account_Number) & '.csv'
            Remove(ExportFile)
            Create(ExportFile)
            Open(ExportFile)
            local:CurrentACcount = tra:Account_Number
        End !If tra:Account_Number = tmp:HeadAccountNumber

        !Open And Booked Jobs (DBH: 15-03-2004)
        local.UpdateProgressWindow('Reading Account: ' & Clip(tra:Account_Number) & ' - ' & Clip(tra:Company_Name))
    !_____________________________________________________________________

        If tmp:Cancel
            Break
        End !If tmp:Cancel
    !_____________________________________________________________________
        local:JobCount = 0
        local:RRCEngineerOccuranceCount = 0
        local:SubARCEngineerOccuranceCount = 0

        !Go Through WEBJOBS in date range (DBH: 10-03-2004)
        Access:WEBJOB.Clearkey(wob:DateCompletedKey)
        wob:HeadAccountNumber    = tra:Account_Number
        wob:DateCompleted        = tmp:StartDate
        Set(wob:DateCompletedKey,wob:DateCompletedKey)
        Loop 
            If Access:WEBJOB.Next()
                Break
            End !If Access:WEBJOB.Next()
            If wob:HeadAccountNumber <> tra:Account_Number
                Break
            End !If wob:HeadAccountNumber <> tra:Account_Number
            If wob:DateCompleted > tmp:EndDate
                Break
            End !If wob:DateCompleted > tmp:EndDate

            Do GetNextRecord2
            Do GetNextRecord2
            Do CancelCheck
            If tmp:Cancel
                Break
            End !If tmp:Cancel

            Access:JOBS.Clearkey(job:Ref_Number_Key)
            job:Ref_Number  = wob:RefNumber
            If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                !Found

            Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                !Error
                Cycle
            End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign

            Access:JOBSE.Clearkey(jobe:RefNumberKey)
            jobe:RefNumber  = wob:RefNumber
            If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                !Found

            Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                !Error
                Cycle
            End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

    !_____________________________________________________________________
            !Go through Status History for job (DBH: 27-04-2004)

            local:JobCount += 1

            Access:AUDSTATS.ClearKey(aus:RefDateRecordKey)
            aus:RefNumber    = job:Ref_Number
            Set(aus:RefDateRecordKey,aus:RefDateRecordKey)
            Loop
                If Access:AUDSTATS.NEXT()
                   Break
                End !If
                If aus:RefNumber    <> job:Ref_Number      |
                    Then Break.  ! End If

                Case Sub(aus:NewStatus,1,3)
                    Of '110'    !Despatch Exchange Unit  (Following Manual QA)
                        If aus:Type = 'EXC'
                            If Local.OtherStatusExists('EXC','620',aus:DateChanged,aus:RecordNumber,aus:UserCode,1) = True
                                Cycle
                            End !If OtherStatusExists(aus:Type,'620',aus:DateChanged,aus:UserCode,1) = True
                            !Write Lines Into Export File (DBH: 22-03-2004)
                            If tmp:ARCAccount = 1
                                local.WriteLine('ARC','Exchange')
                                local:ARCEngineerOccuranceCount += 1
                                local:SubARCEngineerOccuranceCount += 1
                            Else !If tmp:ARCAccount = 1
                                !If engineer is not from this site, then he must be an ARC engineer (DBH: 28-04-2004)
                                If Local.EngineerfromCurrentSite(aus:UserCode) = True
                                    local.WriteLine('RRC','Exchange')
                                    local:RRCEngineerOccuranceCount += 1
                                Else !If EngineerfromCurrentSite(aus:UserCode) = True
                                    local.WriteLine('ARC','Exchange')
                                    local:ARCEngineerOccuranceCount += 1
                                    local:SubARCEngineerOccuranceCount += 1
                                End !If EngineerfromCurrentSite(aus:UserCode) = True
                            End !If tmp:ARCAccount = 1
                        End !If aus:Type = 'EXC'

                    Of '315'    !In Repair
                        If Local.OtherStatusExists('JOB','405',aus:DateChanged,aus:RecordNumber,'',0) = True
                            !Before 3rd Party (DBH: 27-04-2004)
                            !Write Lines Into Export File (DBH: 22-03-2004)
                            If tmp:ARCAccount = 1
                                local:ARCEngineerOccuranceCount += 1
                                local:SubARCEngineerOccuranceCount += 1
                                local.WriteLine('ARC','Before 3rd Party')
                                
                            Else !If tmp:ARCAccount = 1
                                !If engineer is not from this site, then he must be an ARC engineer (DBH: 28-04-2004)
                                If Local.EngineerfromCurrentSite(aus:UserCode) = True
                                    local.WriteLine('RRC','Before 3rd Party')
                                    local:RRCEngineerOccuranceCount += 1
                                Else !If EngineerfromCurrentSite(aus:UserCode) = True
                                    local:ARCEngineerOccuranceCount += 1
                                    local:SubARCEngineerOccuranceCount += 1
                                    local.WriteLine('ARC','Before 3rd Party')
                                End !If EngineerfromCurrentSite(aus:UserCode) = True
                            End !If tmp:ARCAccount = 1
                        End !If OtherStatusExist('JOB','405',aus:DateChanged,'',0) = True
                        If Local.OtherStatusExists('JOB','420',aus:DateChanged,aus:RecordNumber,'',1) = True
                            !After 3rd Party (DBH: 27-04-2004)
                            !Write Lines Into Export File (DBH: 22-03-2004)
                            If tmp:ARCAccount = 1
                                local.WriteLine('ARC','After 3rd Party')
                                local:ARCEngineerOccuranceCount += 1
                                local:SubARCEngineerOccuranceCount += 1
                            Else !If tmp:ARCAccount = 1
                                !If engineer is not from this site, then he must be an ARC engineer (DBH: 28-04-2004)
                                If Local.EngineerfromCurrentSite(aus:UserCode) = True
                                    local.WriteLine('RRC','After 3rd Party')
                                    local:RRCEngineerOccuranceCount += 1
                                Else !If EngineerfromCurrentSite(aus:UserCode) = True
                                    local.WriteLine('ARC','After 3rd Party')
                                    local:ARCEngineerOccuranceCount += 1
                                    local:SubARCEngineerOccuranceCount += 1
                                End !If EngineerfromCurrentSite(aus:UserCode) = True
                            End !If tmp:ARCAccount = 1
                        End !If OtherStatusExists('JOB','405',aus:DateChanged,'',1) = True
                    Of '330'    !Spares Requested
                        !Write Lines Into Export File (DBH: 22-03-2004)
                        If tmp:ARCAccount = 1
                            local.WriteLine('ARC','Spares Requested')
                            local:ARCEngineerOccuranceCount += 1
                            local:SubARCEngineerOccuranceCount += 1
                        Else !If tmp:ARCAccount = 1
                            !If engineer is not from this site, then he must be an ARC engineer (DBH: 28-04-2004)
                            If Local.EngineerfromCurrentSite(aus:UserCode) = True
                                local.WriteLine('RRC','Spares Requested')
                                local:RRCEngineerOccuranceCount += 1
                            Else !If EngineerfromCurrentSite(aus:UserCode) = True
                                local.WriteLine('ARC','Spares Requested')
                                local:ARCEngineerOccuranceCount += 1
                                local:SubARCEngineerOccuranceCount += 1
                            End !If EngineerfromCurrentSite(aus:UserCode) = True
                        End !If tmp:ARCAccount = 1
                    Of '505'    !Estimate Required
                        !Write Lines Into Export File (DBH: 22-03-2004)
                        If tmp:ARCAccount = 1
                            local.WriteLine('ARC','Estimate')
                            local:ARCEngineerOccuranceCount += 1
                            local:SubARCEngineerOccuranceCount += 1
                        Else !If tmp:ARCAccount = 1
                            !If engineer is not from this site, then he must be an ARC engineer (DBH: 28-04-2004)
                            If Local.EngineerfromCurrentSite(aus:UserCode) = True
                                local.WriteLine('RRC','Estimate')
                                local:RRCEngineerOccuranceCount += 1
                            Else !If EngineerfromCurrentSite(aus:UserCode) = True
                                local.WriteLine('ARC','Estimate')
                                local:ARCEngineerOccuranceCount += 1
                                local:SubARCEngineerOccuranceCount += 1
                            End !If EngineerfromCurrentSite(aus:UserCode) = True
                        End !If tmp:ARCAccount = 1
                    Of '605'    !QA Check Required
                        ! Inserting (DBH 13/04/2006) #4597 - After 3rd Party Test
                        If Sub(aus:OldStatus,1,3) = '310' ! Allocated To Engineer
                            If Local.OtherStatusExists('JOB','420',aus:DateChanged,aus:RecordNumber,'',1) = True
                                !After 3rd Party (DBH: 27-04-2004)
                                !Write Lines Into Export File (DBH: 22-03-2004)
                                If tmp:ARCAccount = 1
                                    local.WriteLine('ARC','After 3rd Party')
                                    local:ARCEngineerOccuranceCount += 1
                                    local:SubARCEngineerOccuranceCount += 1
                                Else !If tmp:ARCAccount = 1
                                    !If engineer is not from this site, then he must be an ARC engineer (DBH: 28-04-2004)
                                    If Local.EngineerfromCurrentSite(aus:UserCode) = True
                                        local.WriteLine('RRC','After 3rd Party')
                                        local:RRCEngineerOccuranceCount += 1
                                    Else !If EngineerfromCurrentSite(aus:UserCode) = True
                                        local.WriteLine('ARC','After 3rd Party')
                                        local:ARCEngineerOccuranceCount += 1
                                        local:SubARCEngineerOccuranceCount += 1
                                    End !If EngineerfromCurrentSite(aus:UserCode) = True
                                End !If tmp:ARCAccount = 1
                            End !If OtherStatusExists('JOB','405',aus:DateChanged,'',1) = True
                        End ! If Sub(aus:OldStatus,1,3) = '310' ! Allocated To Engineer
                        ! End (DBH 13/04/2006) #4597
                    Of '610'    !Manual QA Passed
                        !Exchange? (DBH: 27-04-2004)
                        If aus:Type = 'EXC'
                            If Local.OtherStatusExists('EXC','620',aus:DateChanged,aus:RecordNumber,aus:UserCode,1) = True
                                Cycle
                            End !If OtherStatusExists(aus:Type,'620',aus:DateChanged,aus:UserCode,1) = True
                            !Write Lines Into Export File (DBH: 22-03-2004)
                            If tmp:ARCAccount = 1
                                local.WriteLine('ARC','Exchange')
                                local:ARCEngineerOccuranceCount += 1
                                local:SubARCEngineerOccuranceCount += 1
                            Else !If tmp:ARCAccount = 1
                                !If engineer is not from this site, then he must be an ARC engineer (DBH: 28-04-2004)
                                If Local.EngineerfromCurrentSite(aus:UserCode) = True
                                    local.WriteLine('RRC','Exchange')
                                    local:RRCEngineerOccuranceCount += 1
                                Else !If EngineerfromCurrentSite(aus:UserCode) = True
                                    local.WriteLine('ARC','Exchange')
                                    local:ARCEngineerOccuranceCount += 1
                                    local:SubARCEngineerOccuranceCount += 1
                                End !If EngineerfromCurrentSite(aus:UserCode) = True
                            End !If tmp:ARCAccount = 1
                        End !If aus:Type = 'EXC'
                    Of '620'    !Electronic QA Passed
                        If aus:Type = 'EXC'
                            !Electronic QA of Exchange Unit (DBH: 28-04-2004)
                            !Write Lines Into Export File (DBH: 22-03-2004)
                            If tmp:ARCAccount = 1
                                local.WriteLine('ARC','Exchange')
                                local:ARCEngineerOccuranceCount += 1
                                local:SubARCEngineerOccuranceCount += 1
                            Else !If tmp:ARCAccount = 1
                                !If engineer is not from this site, then he must be an ARC engineer (DBH: 28-04-2004)
                                If Local.EngineerfromCurrentSite(aus:UserCode) = True
                                    local.WriteLine('RRC','Exchange')
                                    local:RRCEngineerOccuranceCount += 1
                                Else !If EngineerfromCurrentSite(aus:UserCode) = True
                                    local.WriteLine('ARC','Exchange')
                                    local:ARCEngineerOccuranceCount += 1
                                    local:SubARCEngineerOccuranceCount += 1
                                End !If EngineerfromCurrentSite(aus:UserCode) = True
                            End !If tmp:ARCAccount = 1
                        End !If aus:Type = 'EXC'
                    Of '705'    !Completed
                        If Sub(aus:OldStatus,1,3) = '620' Or |
                            Sub(aus:OldStatus,1,3) = '625' Or |
                            Sub(aus:OldStatus,1,3) = '610' Or |
                            Sub(aus:OldStatus,1,3) = '615' Or |
                            Sub(aus:OldStatus,1,3) = '605'
                            !QA Engineer (DBH: 28-04-2004)
                            !Write Lines Into Export File (DBH: 22-03-2004)
                            If tmp:ARCAccount = 1
                                local.WriteLine('ARC','QA')
                                local:ARCEngineerOccuranceCount += 1
                                local:SubARCEngineerOccuranceCount += 1
                            Else !If tmp:ARCAccount = 1
                                !If engineer is not from this site, then he must be an ARC engineer (DBH: 28-04-2004)
                                If Local.EngineerfromCurrentSite(aus:UserCode) = True
                                    local.WriteLine('RRC','QA')
                                    local:RRCEngineerOccuranceCount += 1
                                Else !If EngineerfromCurrentSite(aus:UserCode) = True
                                    local.WriteLine('ARC','QA')
                                    local:ARCEngineerOccuranceCount += 1
                                    local:SubARCEngineerOccuranceCount += 1
                                End !If EngineerfromCurrentSite(aus:UserCode) = True
                            End !If tmp:ARCAccount = 1

                        End !Sub(aus:OldStatus,1,3) = '615'

                        
                End !Case Sub(aus:NewStatus,1,3)

            End !Loop
    !_____________________________________________________________________

        End !Loop WEBJOB

        local.UpdateProgressWindow('Jobs Found: ' & local:JobCount)
        local.UpdateProgressWindow('ARC Engineer Occurances Found: ' & local:SubARCEngineerOccuranceCount)
        local.UpdateProgressWindow('RRC Engineer Occurances Found: ' & local:RRCEngineerOccuranceCount)
        local.UpdateProgressWindow('')

        !Write the account and it's filename into a queue to use later (DBH: 22-03-2004)
        If tmp:ARCAccount = 0
            tmpque:AccountNumber = tra:Account_Number
            Get(TempFileQueue,tmpque:AccountNumber)
            If Error()
                tmpque:AccountNumber = tra:Account_Number
                tmpque:CompanyName  = tra:Company_Name
                tmpque:FileName     = glo:ExportFile
                tmpque:Count        = local:RRCEngineerOccuranceCount
                Add(TempFileQueue)
            Else !If Error()
                tmpque:FileName = glo:ExportFile
                tmpque:Count        += local:RRCEngineerOccuranceCount
                Put(TempFileQueue)
            End !If Error()
        End !If tmp:ARCAccount = 0

        tmpque:AccountNumber = tmp:HeadAccountNumber
        Get(TempFileQueue,tmpque:AccountNumber)
        If Error()
            tmpque:AccountNumber = tmp:HeadAccountNumber
            tmpque:CompanyName = tmp:ARCCompanyName
            tmpque:FileName = glo:ARCExportFile
            tmpque:Count        = local:ARCEngineerOccuranceCount
            Add(TempFileQueue)
        Else !If Error()
            tmpque:FileName = glo:ARCExportFile
            tmpque:Count        = local:ARCEngineerOccuranceCount
            Put(TempFileQueue)
        End !If Error()

        Close(ExportFile)

        If tmp:Cancel
            Break
        End !If tmp:Cancel
    End !Loop TRADEACC        
    Close(ARCExportFile)

    !_____________________________________________________________________

    If tmp:Cancel = 0 Or tmp:Cancel = 2

        ?ProgressCancel{prop:Disable} = 1

        local.UpdateProgressWindow('================')
        local.UpdateProgressWindow('Building Excel Document..')
    !_____________________________________________________________________

    !CREATE EXPORT DOCUMENT, CREATE SHEETS, COLUMNS and AUTOFILTER
    !_____________________________________________________________________

        local:SavePath = excel:FileName

        Loop x# = 1 To Records(TempFileQueue)
            Get(TempFileQueue,x#)
            If tmpque:Count = 0
                Cycle
            End !If tmpque:Count = 0
            excel:FileName = Clip(local:SavePath) & Clip(tmpque:AccountNumber) & ' ' & |
                                    CLIP(Excel:ProgramName) & ' ' & FORMAT(TODAY(), @D12)! & '.xls'
            !Save the filename, to get later (DBH: 29-04-2004)
            tmpque:ExcelFileName = excel:FileName
            Put(TempFileQueue)
            Do GetNextRecord2

            ExcelSetup(0)
            ExcelMakeWorkBook('Productivity Report','','Productivity Report')
            !Summary Tab (DBH: 29-04-2004)
            ExcelSheetName('Summary')
            ExcelSelectRange('A11')
            ExcelCell('Engineer Surname',1)
            ExcelCell('Engineer Forname',1)
            ExcelCell('Allocations',1)
            ExcelCell('Jobs Completed',1)
            ExcelCell('Exchange',1)
            ExcelCell('Attempt To Repair Before 3rd Party',1)
            ExcelCell('After 3rd Party Test',1)
            ExcelCell('Spares Requested',1)
            ExcelCell('N.F.F.',1)
            ExcelCell('RNR / Liquid Damage / BER / Scrap',1)
            ExcelCell('R.T.M.',1)
            ExcelCell('Level 0',1)
            ExcelCell('Level 1',1)
            ExcelCell('Level 2',1)
            ExcelCell('Level 2.5',1)
            ExcelCell('Level 3',1)
            ExcelCell('Software Upgrade',1)
            ExcelCell('Estimates',1)
            ExcelCell('Unlock',1)
            ExcelCell('Accessory Exchange',1)
            ExcelCell('QA',1)
            ExcelCell('Weight',1)
            ExcelCell('Engineer Hours',1)
            ExcelCell('Weight Per Hour',1)
            ExcelAutoFilter('A11:X11')
            ExcelFreeze('C12')
            ExcelWrapText('A11:X11',1)
            !Detail Tab (DBH: 29-04-2004)
            ExcelMakeSheet()
            ExcelSheetName('Detail')
            ExcelSelectRange('A11')
            ExcelCell('SB Job Number',1)
            ExcelCell('Branch Number',1)
            ExcelCell('Date Booked',1)
            ExcelCell('Date Completed',1)
            ExcelCell('Engineer Surname',1)
            ExcelCell('Engineer Forename',1)
            ExcelCell('Engineer Skill Level',1)
            ExcelCell('Job Skill Level',1)
            ExcelCell('Engineer Option',1)
            ExcelCell('Chargeable Charge Type',1)
            ExcelCell('Chargeable Repair Type',1)
            ExcelCell('Warranty Charge Type',1)
            ExcelCell('Warranty Repair Type',1)
            ExcelCell('3rd Party Repairer/ARC Repair',1)
            ExcelCell('Make',1)
            ExcelCell('Model Number',1)
            ExcelCell('Unit Type',1)
            ExcelCell('I.M.E.I. Number',1)
            ExcelCell('M.S.N.',1)
            ExcelCell('Summary Column (Designated By)',1)
            ExcelCell('Exchange Date',1)
            ExcelCell('Spares Requested',1)
            ExcelAutoFilter('A11:W11')
            ExcelFreeze('B12')
            ExcelWrapText('A11:W11',1)
            ExcelSaveWorkBook(excel:FileName)
            ExcelClose()
        End !Loop x# = 1 To Records(TempFileQueue)

        local.UpdateProgressWindow('Creating Excel Sheets...')
        local.UpdateProgressWindow('')

        If E1.Init(0,1) = 0
            Case Missive('An error has occurred creating your Excel document. Please quit and try again.','ServiceBase 3g',|
                           'mstop.jpg','/OK') 
                Of 1 ! OK Button
            End ! Case Missive
            Exit
        End !If E1.Init(0,0,1) = 0

        Loop x# = 1 To Records(TempFileQueue)
            Get(TempFileQueue,x#)
            Do GetNextRecord2
            Yield()
            If tmpque:Count = 0
                Cycle
            End !If tmpque:Count = 0

            local.UpdateProgressWindow('Writing Account: ' & Clip(tmpque:AccountNumber) & ' - ' & Clip(tmpque:CompanyName))

            !Open account csv file. Copy the data (DBH: 22-03-2004)
            E1.OpenWorkBook(Clip(tmpque:FileName))
            E1.Copy('A1','Z' & tmpque:Count)

            tmp:Clipboard = ClipBoard()
            SetClipBoard('')
            E1.CloseWorkBook(3)

            !Open "final" document and paste the data (DBH: 22-03-2004)
            E1.OpenWorkBook(tmpque:ExcelFileName)
            !E1.SelectWorkSheet(Clip(tmpque:AccountNumber) & '-' & Clip(tmpque:CompanyName))
            E1.SelectWorkSheet('Detail')
    !_____________________________________________________________________

            Do DrawDetailTitle
    !_____________________________________________________________________

            E1.SelectCells('A12')
            SetClipBoard(tmp:ClipBoard)
            E1.Paste()

            !Records and Records Shown (DBH: 22-03-2004)
            E1.WriteToCell(tmpque:Count,'E4')
            E1.WriteToCell('=SUBTOTAL(2, A12:A' & tmpque:Count + 11,'E5')
            

            E1.SelectCells('B12')
            !MUST Clear the clipboard (DBH: 22-03-2004)
            SetClipBoard('')
            
            Clear(EngineerQueue)
            Free(EngineerQueue)
            Clear(UniqueJobQueue)
            Free(UniqueJobQueue)
            Loop row# = 12 To tmpque:Count + 12
                If E1.ReadCell('E' & row#) <> ''
                    !Add to unique job list  (DBH: 04-05-2004)
                    Clear(UniqueJobQueue)
                    unijob:JobNumber = E1.ReadCell('A' & row#)
                    Get(UniqueJobQueue,unijob:JobNumber)
                    If Error()
                        Add(UniqueJobQueue)
                    End !If Error()

                    Clear(EngineerQueue)
                    engque:Surname = E1.ReadCell('E' & row#)
                    engque:Forename = E1.ReadCell('F' & row#)
                    Get(EngineerQueue,'engqueSurname,engqueForename')
                    If Error()
                        engque:Surname  = E1.ReadCell('E' & row#)
                        engque:Forename = E1.ReadCell('F' & row#)
                        Add(EngineerQueue)
                    End !If Error()
                    engque:Allocations += 1
                    If Command('/DEBUG')
                        PUTINI('Engineer :' & engque:Surname,'Allocations: ' & engque:Allocations,Upper(E1.ReadCell('U' & row#)),'c:\debug.ini')
                    End !If Command('/DEBUG')

                    Case Upper(E1.ReadCell('T' & row#))
                        Of 'JOB COMPLETED'
                            engque:JobCompleted += 1
                        Of 'QA'
                            engque:JobCompleted += 1
                            engque:QA += 1
                        Of 'BEFORE 3RD PARTY'
                            engque:Before3rdParty += 1
                        Of 'AFTER 3RD PARTY'
                            engque:After3rdParty += 1
                        Of 'EXCHANGE'
                            engque:Exchange += 1
                        Of 'SPARES REQUESTED'
                            engque:SparesRequested += 1
                        Of 'ESTIMATE'
                            engque:Estimate += 1
                    End !Case E1.ReadCell('U' & row#)
                    local:RepairTypeToUse = ''

                    !J = Chargeable Charge Type, L = Warranty Charge Type (DBH: 04-05-2004)
                    If E1.ReadCell('L' & row#) <> ''
                        !Warranty Job (DBH: 04-05-2004)
                        If E1.ReadCell('J' & row#) <> ''
                            !Split job. Use Chargeable Repair Type (DBH: 04-05-2004)
                            local:RepairTypeToUse = E1.ReadCell('K' & row#)
                        Else !If E1.ReadCel('J' & row#) <> ''
                            !Warranty Only (DBH: 04-05-2004)
                            local:RepairTypeToUse = E1.ReadCell('M' & row#)
                        End !If E1.ReadCel('J' & row#) <> ''
                    Else !If E1.ReadCel('J' & row#) <> ''
                        !Chargeable Only (DBH: 04-05-2004)
                        local:RepairTypeToUse = E1.ReadCell('K' & row#)
                    End !If E1.ReadCel('J' & row#) <> ''

                    !Check Repair Type for radio button option first  (DBH: 06-05-2004)
                    local:FoundRepairType = True
                    Access:REPTYDEF.ClearKey(rtd:ManRepairTypeKey)
                    rtd:Manufacturer = E1.ReadCell('O' & row#)
                    rtd:Repair_Type  = local:RepairTypeToUse
                    If Access:REPTYDEF.TryFetch(rtd:ManRepairTypeKey) = Level:Benign
                        !Found
                        Case rtd:BER
                            Of 5 !NFF
                                engque:NFF += 1
                            Of 1 !BER
                                engque:RNR += 1
                            Of 6 !RTM
                                engque:RTM += 1
                            Of 8 !RNR
                                engque:RNR += 1
                            Of 4 !Liquid Damage
                                engque:RNR += 1
                            Else
                                local:FoundRepairType = False
                        End !Case rtd:BER
                    Else !If Access:REPTYDEF.TryFetch(rtd:ManRepairTypeKey) = Level:Benign
                        !Error
                        local:FoundRepairType = False
                    End !If Access:REPTYDEF.TryFetch(rtd:ManRepairTypeKey) = Level:Benign

                    !Couldn't find a match, so try and match the exact work  (DBH: 06-05-2004)
                    If local:FoundRepairType = False
                        Case local:RepairTypeToUse
                            Of 'N.F.F.'
                                engque:NFF += 1
                            Of 'RNR' Orof 'LIQUID DAMAGE' Orof 'BER' Orof 'SCRAP'
                                engque:RNR += 1
                            Of 'R.T.M.'
                                engque:RTM += 1
                            Of 'LEVEL 0'
                                engque:Level0 += 1
                            Of 'LEVEL 1'
                                engque:Level1 += 1
                            Of 'LEVEL 2'
                                engque:Level2 += 1
                            Of 'LEVEL 2.5'
                                engque:Level25 += 1
                            Of 'LEVEL 3'
                                engque:Level3 += 1
                            Of 'SOFTWARE UPGRADE'
                                engque:SoftwareUpgrade += 1
                            Of 'NETWORK UNLOCK' OrOf 'PHONE UNLOCK'
                                engque:Unlock += 1
                            Of 'ACCESSORY EXCHANGE'
                                engque:AccessoryExchange += 1
                        End !Case local:RepairTypeToUse
                    End !If local:FoundRepairType = False

                    Put(EngineerQueue)
                End !If E1.ReadCell('E' & row#) <> ''
            End !Loop row# = 12 To tmpque:Count + 12
            E1.SelectCells('B12')
            E1.SelectWorkSheet('Summary')

    !_____________________________________________________________________

            Do DrawSummaryTitle
    !_____________________________________________________________________

            E1.WriteToCell('Total Jobs','D4')
            E1.WriteToCell(Records(UniqueJobQueue),'E4')
            E1.WriteToCell('Total Engineers','D5')
            E1.WriteToCell(Records(EngineerQueue),'E5')

            E1.SetCellFontStyle('Bold','D4','E5')

            Sort(EngineerQueue,engque:Surname)
            CurrentRow# = 12
            Loop row# = 1 To Records(EngineerQueue)
                Get(EngineerQueue,row#)
                E1.WriteToCell(engque:Surname           ,'A' & CurrentRow#)
                E1.WriteToCell(engque:Forename          ,'B' & CurrentRow#)
                E1.WriteToCell(engque:Allocations       ,'C' & CurrentRow#)
                E1.WriteToCell(engque:JobCompleted      ,'D' & CurrentRow#)
                E1.WriteToCell(engque:Exchange          ,'E' & CurrentRow#)
                E1.WriteToCell(engque:Before3rdParty    ,'F' & CurrentRow#)
                E1.WriteToCell(engque:After3rdParty     ,'G' & CurrentRow#)
                E1.WriteToCell(engque:SparesRequested   ,'H' & CurrentRow#)
                E1.WriteToCell(engque:NFF               ,'I' & CurrentRow#)
                E1.WriteToCell(engque:RNR               ,'J' & CurrentRow#)
                E1.WriteToCell(engque:RTM               ,'K' & CurrentRow#)
                E1.WriteToCell(engque:Level0            ,'L' & CurrentRow#)
                E1.WriteToCell(engque:Level1            ,'M' & CurrentRow#)
                E1.WriteToCell(engque:Level2            ,'N' & CurrentRow#)
                E1.WriteToCell(engque:Level25           ,'O' & CurrentRow#)
                E1.WriteToCell(engque:Level3            ,'P' & CurrentRow#)
                E1.WriteToCell(engque:SoftwareUpgrade   ,'Q' & CurrentRow#)
                E1.WriteToCell(engque:Estimate          ,'R' & CurrentRow#)
                E1.WriteToCell(engque:Unlock            ,'S' & CurrentRow#)
                E1.WriteToCell(engque:AccessoryExchange ,'T' & CurrentRow#)
                E1.WriteToCell(engque:QA                ,'U' & CurrentRow#)
                E1.WriteToCell('=SUM((H' & CurrentRow# & '*0.5) + (E' & CurrentRow# & ') + (F' & CurrentRow# & '*0.5) + (G' & CurrentRow# & |
                    '*0.5) + (I' & CurrentRow# & ') + (J' & CurrentRow# & '*0.5) + (K' & CurrentRow# & '*0.25) + (L' & CurrentRow# & |
                    ') + (M' & CurrentRow# & ') + (N' & CurrentRow# & '*2) + (O' & CurrentRow# & '*2.5) + (P' & CurrentRow# & |
                    '*3) + (Q' & CurrentRow# & ') + (R' & CurrentRow# & ') + (S' & CurrentRow# & '*0.5) + (T' & CurrentRow# & |
                    ') + (U' & CurrentRow# & '*0.5))'       ,'V' & CurrentRow#)
                E1.WriteToCell(''                       ,'W' & CurrentRow#)
                E1.WriteToCell('=(V' & CurrentRow# & |
                               '/W' & CurrentRow# & ')' ,'X' & CurrentRow#)
                CurrentRow# += 1
            End !Loop row# = 1 To Records(EngineerQueue)
            !Add the total line  (DBH: 04-05-2004)
            E1.WriteToCell('Totals','A' & CurrentRow# + 1)
            Loop column# = 3 to 24
                E1.WriteToCell('=SUM(' & E1.GetColumnAddressFromColumnNumber(column#) & '12:' & |
                        E1.GetColumnAddressFromColumnNumber(column#) & CurrentRow# & ')',|
                        E1.GetColumnAddressFromColumnNumber(column#) & CurrentRow# + 1)
            End !Loop column# = 3 to 24

            E1.SetCellFontStyle('Bold','A' & CurrentRow# + 1,'X' & CurrentRow# + 1)
            Local.DrawBox('A' & CurrentRow# + 1,'X' & CurrentRow# + 1,'A' & CurrentRow# + 1,'X' & CurrentRow# + 1,color:silver)

            E1.SetCellNumberFormat(oix:NumberFormatNumber,,2,,'X12','X' & CurrentRow# + 1)
            E1.SelectCells('C12')
            E1.CloseWorkBook(3)
            local.UpdateProgressWindow('Done.')
            local.UpdateProgressWindow('')
        End !Loop x# = 1 To Records(TempFileQueue)
        SetClipBoard('')


!        E1.OpenWorkBook(excel:FileName)
!    !_____________________________________________________________________
!
!    !SUMMARY
!    !_____________________________________________________________________
!
!        !Fill In Summary Sheet  (DBH: 03-03-2004)
!        local.UpdateProgressWindow('================')
!        local.UpdateProgressWindow('Building Summary..')
!        local.UpdateProgressWindow('')
!
!        E1.SelectWorkSheet('Summary')
!
!        Do DrawSummaryTitle
!    !_____________________________________________________________________
!
!    !FORMATTING
!    !_____________________________________________________________________
!
!        local.UpdatePRogressWindow('Finishing Off Formatting..')
!
!        Loop x# = 1 To Records(TempFileQueue)
!            Get(TempFileQueue,x#)
!            If tmpque:Count = 0
!                Cycle
!            End !If tmpque:Count = 0
!            Do GetNextRecord2
!            E1.SelectWorkSheet(Clip(tmpque:AccountNumber) & '-' & Clip(tmpque:CompanyName))
!            If tmpque:AccountNumber = tmp:HeadAccountNumber
!                tmp:ARCAccount = 1
!            Else !If tmpque:AccountNumber = tmp:HeadAccountNumber
!                tmp:ARCAccount = 0
!            End !If tmpque:AccountNumber = tmp:HeadAccountNumber
!            Do DrawDetailTitle
!
!            E1.SetCellFontSize(12,'A1')
!            E1.SetCellFontStyle('Bold','A1')
!            E1.WriteToCell('** REPORT NAME ** (' & Clip(tmpque:CompanyName) & ')'   ,'A1')
!
!            !Records and Records Shown (DBH: 22-03-2004)
!            E1.WriteToCell('=SUBTOTAL(2, A12:A' & tmpque:Count + 11,'E4')
!            E1.WriteToCell(tmpque:Count,'D4')
!
!
!            E1.SetCellFontStyle('Bold','A' & tmpque:Count + 12,'Z' & tmpque:Count + 13)
!            E1.SetCellFontStyle('Bold','K4','Z5')
!
!        End !Loop x# = 1 To Records(TempFileQueue)
!        SetClipBoard('')
!        E1.SelectWorkSheet('Summary')
!        E1.CloseWorkBook(3)
        E1.Kill()


        local.UpdateProgressWindow('================')
        local.UpdateProgressWindow('Report Finished: ' & Format(Today(),@d6b) & ' ' & Format(Clock(),@t1b))

    Else!If tmp:Cancel = False
        staque:StatusMessage = '=========='
        Add(StatusQueue)
        Select(?List1,Records(StatusQueue))

        staque:StatusMessage = 'Report CANCELLED: ' & Format(Today(),@d6b) & ' ' & Format(Clock(),@t1b)
        Add(StatusQueue)
        Select(?List1,Records(StatusQueue))

    End !If tmp:Cancel = False

    BHReturnDaysHoursMins(BHTimeDifference24Hr(local:ReportStartDate,Today(),local:ReportStartTime,Clock()),Days#,Hours#,Mins#)
    
    local.UpdateProgressWindow('Time To Finish: ' & Days# & ' Dys, ' & Format(Hours#,@n02) & ':' & Format(Mins#,@n02))
    Add(StatusQueue)
    Select(?List1,Records(StatusQueue))

    Do EndPrintRun
    ?progress:userstring{prop:text} = 'Finished...'

    Display()
    ?ProgressCancel{prop:Hide} = 1
    ?Finish{prop:Hide} = 0
    ?Button:OpenReportFolder{Prop:Hide} = 0
    Accept
        Case Field()
            Of ?Finish
                Case Event()
                    Of Event:Accepted
                        Break

                End !Case Event()
            Of ?Button:OpenReportFolder
                Case Event()
                Of Event:Accepted
                    RUN('EXPLORER.EXE ' & Clip(local:Desktop))
                End ! Case Event()
        End !Case Field()
    End !Accept
    Close(ProgressWindow)
    ?StatusText{prop:Text} = ''
    Post(Event:CloseWindow)
DrawSummaryTitle        Routine   !Set Summary Screen (DBH: 22-03-2004)
    E1.SetColumnWidth('A','X','15')
    E1.WriteToCell('Productivity Report','A1')
    E1.SetCellFontStyle('Bold','A1')
    E1.SetCellFontSize(12,'A1')

    E1.WriteToCell('Criteria','A3')
    E1.WriteToCell('VODR0080 - ' & loc:Version  ,'C3')
    E1.SetCellFontStyle('Bold','A3','C3')

    E1.WriteToCell('Date Completed From','A4')
    E1.WriteToCell(Format(tmp:StartDate,@d18),'B4')
    E1.SetCellNumberFormat(oix:NumberFormatDate,,,,'B4')

    E1.WriteToCell('Date Completed To','A5')
    E1.WriteToCell(Format(tmp:EndDate,@d18),'B5')
    E1.SetCellNumberFormat(oix:NumberFormatDate,,,,'B5')

    If tmp:UserName <> ''
        E1.WriteToCell('Created By','A6')
        E1.WriteToCell(tmp:UserName,'B6')
    End !If tmp:UserName <> ''

    E1.WriteToCell('Created Date','A7')
    E1.WriteToCell(Format(Today(),@d18),'B7')
    E1.SetCellNumberFormat(oix:NumberFormatDate,,,,'B7')

    E1.WriteToCell('Summary','A10')

    E1.SetCellFontStyle('Bold','A3','A10')

    Local.DrawBox('A1','X1','A1','X1',color:silver)

    Local.DrawBox('A3','X3','A3','X3',color:silver)

    Local.DrawBox('A4','X4','A7','X7',color:silver)

    Local.DrawBox('A10','X10','A10','X10',color:silver)

    Local.DrawBox('A11','X11','A11','X11',color:silver)
    !_____________________________________________________________________

    !Engineer Surname
    E1.SetColumnWidth('A','','24')
    !Engineer Forename
    E1.SetColumnWidth('B','','15')
    !Allocations / Jobs Completed
    E1.SetColumnWidth('C','D','13')
    !Exchange
    E1.SetColumnWidth('E','','12')
    !Before/After 3rd Party
    E1.SetColumnWidth('F','G','13')
    !Spares Requested
    E1.SetColumnWidth('H','','12')
    !NFF
    E1.SetColumnWidth('I','','8')
    !RNR
    E1.SetColumnWidth('J','','14')
    !RTM
    E1.SetColumnWidth('K','','8')
    !Levels
    E1.SetColumnWidth('L','P','11')
    !Software Upgrade
    E1.SetColumnWidth('Q','','10')
    !Estimate
    E1.SetColumnWidth('R','','11')
    !Unlock
    E1.SetColumnWidth('S','','14')
    !Accessory Exchange
    E1.SetColumnWidth('T','','12')
    !QA
    E1.SetColumnWidth('U','','6')
    !Weight
    E1.SetColumnWidth('V','','9')
    !Engineer Hours
    E1.SetColumnWidth('W','','10')
    !Weight Per Hour
    E1.SetColumnWidth('X','','11')
DrawDetailTitle     Routine     !Set Detail Title (DBH: 22-03-2004)
    E1.SetColumnWidth('A','V','15')
    E1.WriteToCell('Productivity Report','A1')
    E1.SetCellFontStyle('Bold','A1')
    E1.SetCellFontSize(12,'A1')

    E1.WriteToCell('Criteria','A3')
    E1.WriteToCell('VODR0080 - ' & loc:Version  ,'C3')
    E1.SetCellFontStyle('Bold','A3','C3')

    E1.WriteToCell('Date Completed From','A4')
    E1.WriteToCell(Format(tmp:StartDate,@d18),'B4')
    E1.SetCellNumberFormat(oix:NumberFormatDate,,,,'B4')

    E1.WriteToCell('Date Completed To','A5')
    E1.WriteToCell(Format(tmp:EndDate,@d18),'B5')
    E1.SetCellNumberFormat(oix:NumberFormatDate,,,,'B5')

    If tmp:UserName <> ''
        E1.WriteToCell('Created By','A6')
        E1.WriteToCell(tmp:UserName,'B6')
    End !If tmp:UserName <> ''

    E1.WriteToCell('Created Date','A7')
    E1.WriteToCell(Format(Today(),@d18),'B7')
    E1.SetCellNumberFormat(oix:NumberFormatDate,,,,'B7')

    E1.WriteToCell('Detail','A10')

    E1.SetCellFontStyle('Bold','A3','A10')

    E1.WriteToCell('In Report','D4')
    E1.WriteToCell('Shown','D5')
    E1.SetCellFontStyle('Bold','D4','E5')

    Local.DrawBox('A1','V1','A1','V1',color:silver)

    Local.DrawBox('A3','V3','A3','V3',color:silver)

    Local.DrawBox('A4','V4','A7','V7',color:silver)

    Local.DrawBox('A10','V10','A10','V10',color:silver)

    Local.DrawBox('A11','V11','A11','V11',color:silver)
    !_____________________________________________________________________

    !SB Job Number
    E1.SetColumnWidth('A','','24')
    !Branch Number
    E1.SetColumnWidth('B','','10')
    !Date Booked
    E1.SetColumnWidth('C','','11')
    !Date Completed
    E1.SetColumnWidth('D','','13')
    !Engineer Skill Level
    E1.SetColumnWidth('G','','12')
    !Job Skill Level
    E1.SetColumnWidth('H','','9')
    !Engineer Option
    E1.SetColumnWidth('I','','17')
    !Charge/Repair Types
    E1.SetColumnWidth('J','M','25')
    !3rd Party
    E1.SetColumnWidth('N','','23')
    !Model Number
    E1.SetColumnWidth('O','','15')
    !IMEI/MSN
    E1.SetColumnWidth('R','S','18')
    !Summary Column
    E1.SetColumnWidth('T','','18')
    !Exchange Date
    E1.SetColumnWidth('U','','13')
    !Spares Requested
    E1.SetColumnWidth('V','','12')



getnextrecord2      routine !Progress Window Routines (DBH: 22-03-2004)
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress >= 100
        recordsprocessed        = 0
        percentprogress         = 0
        progress:thermometer    = 0
        recordsthiscycle        = 0
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    !0{prop:Text} = ?progress:pcttext{prop:text}
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                Yield()
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case Missive('Do you want to FINISH building the Excel document with the data you have compiled so far, or just QUIT now?','ServiceBase 3g',|
                       'mquest.jpg','\Cancel|Quit|Finish') 
            Of 3 ! Finish Button
                tmp:Cancel = 2
            Of 2 ! Quit Button
                tmp:Cancel = 1
            Of 1 ! Cancel Button
        End ! Case Missive
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    display()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020651'&'0'
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ProductivityReport')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelMain
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:AUDIT.Open
  Relate:WEBJOB.Open
  Access:TRADEACC.UseFile
  Access:JOBSE.UseFile
  Access:JOBS.UseFile
  Access:USERS.UseFile
  Access:AUDSTATS.UseFile
  Access:JOBTHIRD.UseFile
  Access:REPTYDEF.UseFile
  SELF.FilesOpened = True
  !Initialize Dates and save Head Account Information (DBH: 22-03-2004)
  
  tmp:StartDate = Deformat('1/' & Month(Today()) & '/' & Year(Today()),@d6)
  tmp:EndDate = Today()
  
  tmp:HeadAccountNumber   = GETINI('BOOKING','HeadAccount',,CLIP(Path()) & '\SB2KDEF.INI')
  
  Access:TRADEACC.Clearkey(tra:Account_Number_Key)
  tra:Account_Number  = tmp:HeadAccountNumber
  If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      !Found
      tmp:ARCLocation = tra:SiteLocation
      tmp:ARCCompanyName = tra:Company_Name
  Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      !Error
  End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  
  !Put in user name IF run from ServiceBase (DBH: 25-03-2004)
  pos# = Instring('%',COMMAND(),1,1)
  If pos#
      Access:USERS.Clearkey(use:Password_Key)
      use:Password    = Clip(Sub(COMMAND(),pos#+1,30))
      If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
          !Found
          tmp:UserName = Clip(use:Forename) & ' ' & Clip(use:Surname)
      Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
          !Error
      End !If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
  End !pos#
  BRW8.Init(?List,Queue:Browse.ViewPosition,BRW8::View:Browse,Queue:Browse,Relate:TRADEACC,SELF)
  OPEN(window)
  SELF.Opened=True
  !========== Set The Version Number ============
  Include('..\ReportVersion.inc')
  tmp:VersionNumber = Clip(tmp:VersionNumber) & '5002'
  loc:Version = tmp:VersionNumber
  ?ReportVersion{Prop:Text} = 'Report Version: ' & Clip(loc:Version)
  Bryan.CompFieldColour()
  ?tmp:StartDate{Prop:Alrt,255} = MouseLeft2
  ?tmp:EndDate{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW8.Q &= Queue:Browse
  BRW8.AddSortOrder(,tra:Account_Number_Key)
  BRW8.AddLocator(BRW8::Sort0:Locator)
  BRW8::Sort0:Locator.Init(,tra:Account_Number,1,BRW8)
  BRW8.SetFilter('(Upper(tra:BranchIdentification) <<> '''')')
  BIND('tmp:Tag',tmp:Tag)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW8.AddField(tmp:Tag,BRW8.Q.tmp:Tag)
  BRW8.AddField(tra:Account_Number,BRW8.Q.tra:Account_Number)
  BRW8.AddField(tra:Company_Name,BRW8.Q.tra:Company_Name)
  BRW8.AddField(tra:RecordNumber,BRW8.Q.tra:RecordNumber)
  BRW8.AddToolbarTarget(Toolbar)
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:AUDIT.Close
    Relate:WEBJOB.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020651'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020651'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020651'&'0')
      ***
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:StartDate = TINCALENDARStyle1(tmp:StartDate)
          Display(?tmp:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:EndDate = TINCALENDARStyle1(tmp:EndDate)
          Display(?tmp:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?Print
      ThisWindow.Update
      Do Reporting
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeEvent()
    E1.TakeEvent ('', '')
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?tmp:StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?tmp:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::9:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Local.WriteLine         Procedure(String func:Type,String func:EntryType) !Write CSV Files (DBH: 22-03-2004)
Code
    Case func:Type
        Of 'ARC'
            Clear(arcexp:Record)
            !Job Number
            arcexp:JobNumber              = Clip(job:Ref_Number)
            !Branch ID
            arcexp:BranchNumber           = Clip(tra:BranchIdentification)
            !Date Booked
            arcexp:DateBooked             = Format(job:Date_Booked,@d018)
            !Date Completed
            arcexp:DateCompleted          = Format(job:Date_Completed,@d018)
            !Engineer Surname/Forename/Skill Level
            Access:USERS.Clearkey(use:User_Code_Key)
            use:User_Code   = aus:UserCode
            If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                !Found
                arcexp:EngineerSurname        = Clip(use:Surname)
                arcexp:EngineerForename       = Clip(use:Forename)
                arcexp:EngineerSkillLevel     = Clip(use:SkillLevel)
            Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                !Error
                arcexp:EngineerSurname        = '** UNKNOWN. USERCODE: ' & Clip(aus:UserCode) & ' **'
                arcexp:EngineerForename       = ''
                arcexp:EngineerSkillLevel     = ''
            End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
            !Job Skill Level
            arcexp:JobSkillLevel          = jobe:SkillLevel
            !Engineer Option
            Case jobe:Engineer48HourOption
                Of 1
                    arcexp:EngineerOption         = '48 Hour'
                Of 2
                    arcexp:EngineerOption         = 'ARC Repair'
                Of 3
                    arcexp:EngineerOption         = '7 Day TAT'
                Of 4
                    arcexp:EngineerOption         = 'Standard Repair'
                Else
                    arcexp:EngineerOption         = ''
            End !Case jobe:Engineer48HourOption
            !Chargeable Charge Type
            arcexp:CChargeType            = Clip(job:Charge_Type)
            !Chargeable Repair Type
            arcexp:CRepairType            = Clip(job:Repair_Type)
            !Warranty Charge Type
            arcexp:WChargeType            = Clip(job:Warranty_Charge_Type)
            !Warranty Repair Type
            arcexp:WRepairType            = Clip(job:Repair_Type_Warranty)
            !Third Party Repairer
            arcexp:ThirdParty             = Clip(job:Third_Party_Site)
            !Manufacturer
            arcexp:Manufacturer           = Clip(job:Manufacturer)
            !Model Number
            arcexp:ModelNumber            = Clip(job:Model_Number)
            !Unit Type
            arcexp:UnitType               = Clip(job:Unit_Type)
            !IMEI Number/MSN
            If job:Third_Party_Site <> ''
                Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
                jot:RefNumber = job:Ref_Number
                Set(jot:RefNumberKey,jot:RefNumberKey)
                If Access:JOBTHIRD.NEXT()
                    IMEIError# = 1
                Else !If Access:JOBTHIRD.NEXT()
                    If jot:RefNumber <> job:Ref_Number            
                        IMEIError# = 1
                    Else !If jot:RefNumber <> job:Ref_Number            
                        IMEIError# = 0                
                    End !If jot:RefNumber <> job:Ref_Number            
                End !If Access:JOBTHIRD.NEXT()
            Else !job:Third_Party_Site <> ''
                IMEIError# = 1    
            End !job:Third_Party_Site <> ''

            If IMEIError# = 1
                arcexp:IMEINumber  = Clip(job:ESN)
                arcexp:MSN         = Clip(job:MSN)
            Else !IMEIError# = 1
                arcexp:IMEINumber  = Clip(jot:OriginalIMEI)
                arcexp:MSN         = Clip(jot:OriginalMSN)
            End !IMEIError# = 1

            If arcexp:IMEINumber <> '' And arcexp:IMEINumber <> 'N/A'
                arcexp:IMEINumber = '''' & Clip(arcexp:IMEINumber)
            End !If arcexp:IMEINumber <> '' And arcexp:IMEINumber <> 'N/A'

            If arcexp:MSN <> '' And arcexp:MSN <> 'N/A'
                arcexp:MSN = '''' & Clip(arcexp:MSN)
            End !If arcexp:MSN <> '' And arcexp:MSN <> 'N/A'
            !Summary Column (Designated By)
            arcexp:SummaryColumn          = Clip(func:EntryType)
            !Exchange Date
            If func:EntryType = 'Exchange'
                arcexp:ExchangeDate       = Format(aus:DateChanged,@d018)
            Else !If func:Entry = 'Exchange
                arcexp:ExchangeDate       = ''
            End !If func:Entry = 'Exchange
            !Spares Requested
            If func:EntryType = 'Spares Requested'
                arcexp:SparesRequested    = Format(aus:DateChanged,@d018)
            Else !If func:Entry = 'Spares Requested'
                arcexp:SparesRequested    = ''
            End !If func:Entry = 'Spares Requested'
            Add(ARCExportFile)

        Of 'RRC'
            Clear(exp:Record)
            !Job Number
            exp:JobNumber              = Clip(job:Ref_Number)
            !Branch ID
            exp:BranchNumber           = Clip(tra:BranchIdentification)
            !Date Booked
            exp:DateBooked             = Format(job:Date_Booked,@d018)
            !Date Completed
            exp:DateCompleted          = Format(job:Date_Completed,@d018)
            !Engineer Surname/Forename/Skill Level
            Access:USERS.Clearkey(use:User_Code_Key)
            use:User_Code   = aus:UserCode
            If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                !Found
                exp:EngineerSurname        = Clip(use:Surname)
                exp:EngineerForename       = Clip(use:Forename)
                exp:EngineerSkillLevel     = Clip(use:SkillLevel)
            Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                !Error
                exp:EngineerSurname        = '** UNKNOWN. USERCODE: ' & Clip(aus:UserCode) & ' **'
                exp:EngineerForename       = ''
                exp:EngineerSkillLevel     = ''
            End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
            !Job Skill Level
            exp:JobSkillLevel          = jobe:SkillLevel
            !Engineer Option
            Case jobe:Engineer48HourOption
                Of 1
                    exp:EngineerOption         = '48 Hour'
                Of 2
                    exp:EngineerOption         = 'ARC Repair'
                Of 3
                    exp:EngineerOption         = '7 Day TAT'
                Of 4
                    exp:EngineerOption         = 'Standard Repair'
                Else
                    exp:EngineerOption         = ''
            End !Case jobe:Engineer48HourOption
            !Chargeable Charge Type
            exp:CChargeType            = Clip(job:Charge_Type)
            !Chargeable Repair Type
            exp:CRepairType            = Clip(job:Repair_Type)
            !Warranty Charge Type
            exp:WChargeType            = Clip(job:Warranty_Charge_Type)
            !Warranty Repair Type
            exp:WRepairType            = Clip(job:Repair_Type_Warranty)
            !Third Party Repairer
            exp:ThirdParty             = Clip(job:Third_Party_Site)
            !Manufacturer
            exp:Manufacturer           = Clip(job:Manufacturer)
            !Model Number
            exp:ModelNumber            = Clip(job:Model_Number)
            !Unit Type
            exp:UnitType               = Clip(job:Unit_Type)
            !IMEI Number/MSN
            If job:Third_Party_Site <> ''
                Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
                jot:RefNumber = job:Ref_Number
                Set(jot:RefNumberKey,jot:RefNumberKey)
                If Access:JOBTHIRD.NEXT()
                    IMEIError# = 1
                Else !If Access:JOBTHIRD.NEXT()
                    If jot:RefNumber <> job:Ref_Number            
                        IMEIError# = 1
                    Else !If jot:RefNumber <> job:Ref_Number            
                        IMEIError# = 0                
                    End !If jot:RefNumber <> job:Ref_Number            
                End !If Access:JOBTHIRD.NEXT()
            Else !job:Third_Party_Site <> ''
                IMEIError# = 1    
            End !job:Third_Party_Site <> ''

            If IMEIError# = 1
                exp:IMEINumber  = Clip(job:ESN)
                exp:MSN         = Clip(job:MSN)
            Else !IMEIError# = 1
                exp:IMEINumber  = Clip(jot:OriginalIMEI)
                exp:MSN         = Clip(jot:OriginalMSN)
            End !IMEIError# = 1

            If exp:IMEINumber <> '' And exp:IMEINumber <> 'N/A'
                exp:IMEINumber = '''' & Clip(exp:IMEINumber)
            End !If exp:IMEINumber <> '' And exp:IMEINumber <> 'N/A'

            If exp:MSN <> '' And exp:MSN <> 'N/A'
                exp:MSN = '''' & Clip(exp:MSN)
            End !If exp:MSN <> '' And exp:MSN <> 'N/A'
            !Summary Column (Designated By)
            exp:SummaryColumn          = Clip(func:EntryType)
            !Exchange Date
            If func:EntryType = 'Exchange'
                exp:ExchangeDate       = Format(aus:DateChanged,@d018)
            Else !If func:Entry = 'Exchange
                exp:ExchangeDate       = ''
            End !If func:Entry = 'Exchange
            !Spares Requested
            If func:EntryType = 'Spares Requested'
                exp:SparesRequested    = Format(aus:DateChanged,@d018)
            Else !If func:Entry = 'Spares Requested'
                exp:SparesRequested    = ''
            End !If func:Entry = 'Spares Requested'
            Add(ExportFile)
    End !Case func:Type

Local.DrawBox       Procedure(String func:TL,String func:TR,String func:BL,String func:BR,Long func:Colour)
Code
    If func:BR = ''
        func:BR = func:TR
    End !If func:BR = ''

    If func:BL = ''
        func:BL = func:TL
    End !If func:BL = ''

    If func:Colour = 0
        func:Colour = oix:ColorWhite
    End !If func:Colour = ''
    E1.SetCellBackgroundColor(func:Colour,func:TL,func:BR)
    E1.SetCellBorders(func:BL,func:BR,oix:BorderEdgeBottom,oix:LineStyleContinuous)
    E1.SetCellBorders(func:TL,func:TR,oix:BorderEdgeTop,oix:LineStyleContinuous)
    E1.SetCellBorders(func:TL,func:BL,oix:BorderEdgeLeft,oix:LineStyleContinuous)
    E1.SetCellBorders(func:TR,func:BR,oix:BorderEdgeRight,oix:LineStyleContinuous)
Local.UpdateProgressWindow      Procedure(String    func:Text)
Code
    staque:StatusMessage = Clip(func:Text)
    Add(StatusQueue)
    Select(?List1,Records(StatusQueue))
    Display()
Local.EngineerFromCurrentSite           Procedure(String func:UserCode)
Code
    Access:USERS.ClearKey(use:User_Code_Key)
    use:User_Code = func:UserCode
    If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
        !Found
        If use:Location = tmp:CurrentSiteLocation
            Return True            
        End !If use:SiteLocation = tmp:CurrentSiteLocation
    Else !If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
     !Error
    End !If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign

    Return False
Local.OtherStatusExists        Procedure(String func:Type,String func:Status,Date func:Date,Long func:RecordNumber,String func:UserCode,Byte func:Previous)
Code
    Found# = False
    Save_aus_ID = Access:AUDSTATS.SaveFile()
    Access:AUDSTATS.ClearKey(aus:RefDateRecordKey)
    aus:RefNumber   = job:Ref_Number
    aus:DateChanged = func:Date
    aus:RecordNumber = func:RecordNumber
    Set(aus:RefDateRecordKey,aus:RefDateRecordKey)
    Loop
        If func:Previous = True
            If Access:AUDSTATS.PREVIOUS()
               Break
            End !If
            If aus:RecordNumber > func:RecordNumber
                Break
            End !If aus:RecordNumber > func:RecordNumber
        Else !If func:Previous = True
            If Access:AUDSTATS.NEXT()
               Break
            End !If
            If aus:RecordNumber < func:RecordNumber
                Break
            End !If aus:RecordNumber < func:RecordNumber
        End !If func:Previous = True
        If aus:RefNumber <> job:Ref_Number
            Break
        End !If aus:RefNumber <> job:Ref_Number
        If aus:Type <> func:Type
            Cycle
        End !If aus:Type <> func:Type

        If func:Previous = True    
            If aus:DateChanged > func:Date
                Break
            End !If aus:DateChanged > func:Date
        Else !If func:Previous = True    
            If aus:Datechanged < func:Date
                Break
            End !If aus:Datechanged < func:Date
        End !If func:Previous = True

        If func:UserCode <> ''
            If aus:UserCode = func:UserCode
                Cycle
            End !If aus:UserCode <> func:UserCode
        End !If func:UserCode <> ''

        If Sub(aus:NewStatus,1,3) = func:Status
            Found# = True
            Break
        End !If Sub(aus:NewStatus,1,3) = func:Status

    End !Loop
    Access:AUDSTATS.RestoreFile(Save_aus_ID)

    Return Found#
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
!---------------------------------------------------------------------------------
E1.Init   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0)
ReturnValue   byte
  CODE
  ReturnValue = PARENT.Init (pStartVisible,pEnableEvents)
  self.TakeSnapShotOfWindowPos()
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
E1.Kill   PROCEDURE (byte pUnloadCOM=1)
ReturnValue   byte
  CODE
  self.RestoreSnapShotOfWindowPos()
  ReturnValue = PARENT.Kill (pUnloadCOM)
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
E1.TakeEvent   PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0)
  CODE
  PARENT.TakeEvent (pEventString1,pEventString2,pEventNumber,pEventType,pEventStatus)
  if pEventType = 0  ! Generated by CapeSoft Office Inside
    case event()
      of event:accepted
        case field()
      end
    end
  end
!Initialise
ExcelSetup          Procedure(Byte      func:Visible)
    Code
    Excel   = Create(0,Create:OLE)
    Excel{prop:Create} = 'Excel.Application'
    Excel{'ASYNC'}  = False
    Excel{'Application.DisplayAlerts'} = False

    Excel{'Application.ScreenUpdating'} = func:Visible
    Excel{'Application.Visible'} = func:Visible
    Excel{'Application.Calculation'} = 0FFFFEFD9h
    excel:ActiveWorkBook    = Excel{'ActiveWorkBook'}
    Yield()

!Make a WorkBook
ExcelMakeWorkBook   Procedure(String    func:Title,String   func:Author,String  func:AppName)
    Code
    Excel{prop:Release} = excel:ActiveWorkBook
    excel:ActiveWorkBook = Excel{'Application.Workbooks.Add()'}

    Excel{excel:ActiveWorkBook & '.BuiltinDocumentProperties("Title")'} = func:Title
    Excel{excel:ActiveWorkBook & '.BuiltinDocumentProperties("Author")'} = func:Author
    Excel{excel:ActiveWorkBook & '.BuiltinDocumentProperties("Application Name")'} = func:AppName

    excel:Selected = Excel{'Sheets("Sheet2").Select'}
    Excel{prop:Release} = excel:Selected

    Excel{'ActiveWindow.SelectedSheets.Delete'}

    excel:Selected = Excel{'Sheets("Sheet1").Select'}
    Excel{prop:Release} = excel:Selected
    Yield()

ExcelMakeSheet      Procedure()
ActiveWorkBook  CString(20)
    Code
    ActiveWorkBook = Excel{'ActiveWorkBook'}

    Excel{ActiveWorkBook & '.Sheets("Sheet3").Select'}
    Excel{prop:Release} = ActiveWorkBook

    Excel{excel:ActiveWorkBook & '.Sheets.Add'}
    Yield()
!Select A Sheet
ExcelSelectSheet    Procedure(String    func:SheetName)
    Code
    Excel{'Sheets("' & Clip(func:SheetName) & '").Select'}
    Yield()
!Setup Sheet Type (P = Portrait, L = Lanscape)
ExcelSheetType      Procedure(String    func:Type)
    Code
    Case func:Type
        Of 'L'
            Excel{'ActiveSheet.PageSetup.Orientation'}  = 2
        Of 'P'
            Excel{'ActiveSheet.PageSetup.Orientation'}  = 1
    End !Case func:Type
    Excel{'ActiveSheet.PageSetup.FitToPagesWide'}  = 1
    Excel{'ActiveSheet.PageSetup.FitToPagesTall'}  = 9999
    Excel{'ActiveSheet.PageSetup.Order'}  = 2

    Yield()
ExcelHorizontal     Procedure(String    func:Direction)
Selection   Cstring(20)
    Code
    Selection = Excel{'Selection'}
    Case func:Direction
        Of 'Centre'
            Excel{Selection & '.HorizontalAlignment'} = 0FFFFEFF4h
        Of 'Left'
            Excel{Selection & '.HorizontalAlignment'} = 0FFFFEFDDh
        Of 'Right'
            Excel{Selection & '.HorizontalAlignment'} = 0FFFFEFC8h
    End !Case tmp:Direction
    Excel{prop:Release} = Selection
    Yield()
ExcelVertical   Procedure(String func:Direction)
Selection   CString(20)
    Code
    Selection = Excel{'Selection'}
    Case func:Direction
        Of 'Top'
            Excel{Selection & '.VerticalAlignment'} = 0FFFFEFC0h
        Of 'Centre'
            Excel{Selection & '.VerticalAlignment'} = 0FFFFEFF4h
        Of 'Bottom'
            Excel{Selection & '.VerticalAlignment'} = 0FFFFEFF5h
    End ! Case func:Direction
    Excel{prop:Release} = Selection
    Yield()

ExcelCell   Procedure(String    func:Text,Byte    func:Bold)
Selection   Cstring(20)
    Code
    Selection = Excel{'Selection'}
    If func:Bold
        Excel{Selection & '.Font.Bold'} = True
    Else
        Excel{Selection & '.Font.Bold'} = False
    End !If func:Bold
    Excel{prop:Release} = Selection

    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.Formula'}  = func:Text
    Excel{excel:Selected & '.Offset(0, 1).Select'}
    Excel{prop:Release} = excel:Selected
    Yield()
ExcelFormatCell     Procedure(String    func:Format)
    Code
    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.NumberFormat'} = func:Format
    Excel{prop:Release} = excel:Selected
    Yield()
ExcelFormatRange    Procedure(String    func:Range,String   func:Format)
Selection       Cstring(20)
    Code

    ExcelSelectRange(func:Range)
    Selection   = Excel{'Selection'}
    Excel{Selection & '.NumberFormat'} = func:Format
    Excel{prop:Release} = Selection
    Yield()

ExcelNewLine    Procedure(Long func:Number)
    Code
    Loop excelloop# = 1 to func:Number
        ExcelSelectRange('A' & (ExcelCurrentRow() + 1))
    End !Loop excelloop# = 1 to func:Number
    !excel:Selected = Excel{'ActiveCell'}
    !Excel{excel:Selected & '.Offset(0, -' & Excel{excel:Selected & '.Column'} - 1 & ').Select'}
    !Excel{excel:Selected & '.Offset(1, 0).Select'}
    !Excel{prop:Release} = excel:Selected
    Yield()

ExcelMoveDown   Procedure()
    Code
    ExcelSelectRange(ExcelCurrentColumn() & (ExcelCurrentRow() + 1))
    Yield()
!Set Column Width

ExcelColumnWidth        Procedure(String    func:Range,Long   func:Width)
    Code
    Excel{'ActiveSheet.Columns("' & Clip(func:Range) & '").ColumnWidth'} = func:Width
    Yield()
ExcelCellWidth          Procedure(Long  func:Width)
    Code
    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.ColumnWidth'} = func:Width
    Excel{prop:Release} = excel:Selected
    Yield()
ExcelAutoFit            Procedure(String func:Range)
    Code
    Excel{'ActiveSheet.Columns("' & Clip(func:Range) & '").Columns.AutoFit'}
    Yield()
!Set Gray Box

ExcelGrayBox            Procedure(String    func:Range)
Selection   CString(20)
    Code
    Selection = Excel{'Selection'}
    Excel{'Range("' & Clip(func:Range) & '").Select'}
    Excel{Selection & '.Interior.ColorIndex'} = 15
    Excel{Selection & '.Interior.Pattern'} = 1
    Excel{Selection & '.Interior.PatternColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(7).LineStyle'} = 1
    Excel{Selection & '.Borders(7).Weight'} = 2
    Excel{Selection & '.Borders(7).ColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(10).LineStyle'} = 1
    Excel{Selection & '.Borders(10).Weight'} = 2
    Excel{Selection & '.Borders(10).ColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(8).LineStyle'} = 1
    Excel{Selection & '.Borders(8).Weight'} = 2
    Excel{Selection & '.Borders(8).ColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(9).LineStyle'} = 1
    Excel{Selection & '.Borders(9).Weight'} = 2
    Excel{Selection & '.Borders(9).ColorIndex'} = 0FFFFEFF7h
    Excel{prop:Release} = Selection
    Yield()
ExcelGrid   Procedure(String    func:Range,Byte  func:Left,Byte  func:Top,Byte   func:Right,Byte func:Bottom,Byte func:Colour)
Selection   Cstring(20)
    Code
    Excel{'Range("' & Clip(func:Range) & '").Select'}
    Selection = Excel{'Selection'}
    If func:Colour
        Excel{Selection & '.Interior.ColorIndex'} = func:Colour
        Excel{Selection & '.Interior.Pattern'} = 1
        Excel{Selection & '.Interior.PatternColorIndex'} = 0FFFFEFF7h
    End !If func:Colour
    If func:Left
        Excel{Selection & '.Borders(7).LineStyle'} = 1
        Excel{Selection & '.Borders(7).Weight'} = 2
        Excel{Selection & '.Borders(7).ColorIndex'} = 0FFFFEFF7h
    End !If func:Left

    If func:Right
        Excel{Selection & '.Borders(10).LineStyle'} = 1
        Excel{Selection & '.Borders(10).Weight'} = 2
        Excel{Selection & '.Borders(10).ColorIndex'} = 0FFFFEFF7h
    End !If func:Top

    If func:Top
        Excel{Selection & '.Borders(8).LineStyle'} = 1
        Excel{Selection & '.Borders(8).Weight'} = 2
        Excel{Selection & '.Borders(8).ColorIndex'} = 0FFFFEFF7h
    End !If func:Right

    If func:Bottom
        Excel{Selection & '.Borders(9).LineStyle'} = 1
        Excel{Selection & '.Borders(9).Weight'} = 2
        Excel{Selection & '.Borders(9).ColorIndex'} = 0FFFFEFF7h
    End !If func:Bottom
    Excel{prop:Release} = Selection
    Yield()
!Select a range of cells
ExcelSelectRange        Procedure(String    func:Range)
    Code
    Excel{'Range("' & Clip(func:Range) & '").Select'}
    Yield()
!Change font size
ExcelFontSize           Procedure(Byte  func:Size)
    Code
    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.Font.Size'}   = func:Size
    Excel{prop:Release} = excel:Selected
    Yield()
!Sheet Name
ExcelSheetName          Procedure(String    func:Name)
    Code
    Excel{'ActiveSheet.Name'} = func:Name
    Yield()
ExcelAutoFilter         Procedure(String    func:Range)
Selection   Cstring(20)
    Code
    ExcelSelectRange(func:Range)
    Selection = Excel{'Selection'}
    Excel{Selection & '.AutoFilter'}
    Excel{prop:Release} = Selection
    Yield()
ExcelDropAllSheets      Procedure()
    Code
    Excel{prop:Release} = excel:ActiveWorkBook
    Loop While Excel{'WorkBooks.Count'} > 0
        excel:ActiveWorkBook = Excel{'ActiveWorkBook'}
        Excel{'ActiveWorkBook.Close(1)'}
        Excel{prop:Release} = excel:ActiveWorkBook
    End !Loop While Excel{'WorkBooks.Count'} > 0
    Yield()
ExcelClose              Procedure()
!xlCalculationAutomatic
    Code
    Excel{'Application.Calculation'}= 0FFFFEFF7h
    Excel{'Application.Quit'}
    Excel{prop:Deactivate}
    Destroy(Excel)
    Yield()
ExcelDeleteSheet        Procedure(String    func:SheetName)
    Code
    ExcelSelectSheet(func:SheetName)
    Excel{'ActiveWindow.SelectedSheets.Delete'}
    Yield()
ExcelSaveWorkBook       Procedure(String    func:Name)
    Code
    Excel{'Application.ActiveWorkBook.SaveAs("' & LEFT(CLIP(func:Name)) & '")'}
    Excel{'Application.ActiveWorkBook.Close()'}
   Excel{'Application.Calculation'} = 0FFFFEFF7h
    Excel{'Application.Quit'}

    Excel{PROP:DEACTIVATE}
    YIELD()
ExcelFontColour         Procedure(String    func:Range,Long func:Colour)
    Code
    !16 = Gray
    ExcelSelectRange(func:Range)
    Excel{'Selection.Font.ColorIndex'} = func:Colour
    Yield()
ExcelWrapText           Procedure(String    func:Range,Byte func:True)
Selection   Cstring(20)
    Code
    ExcelSelectRange(func:Range)
    Selection = Excel{'Selection'}
    Excel{Selection & '.WrapText'} = func:True
    Excel{prop:Release} = Selection
    Yield()
ExcelCurrentColumn      Procedure()
CurrentColumn   String(20)
    Code
    excel:Selected = Excel{'ActiveCell'}
    CurrentColumn = Excel{excel:Selected & '.Column'}
    Excel{prop:Release} = excel:Selected
    Yield()
    Return CurrentColumn

ExcelCurrentRow         Procedure()
CurrentRow      String(20)
    Code
    excel:Selected = Excel{'ActiveCell'}
    CurrentRow = Excel{excel:Selected & '.Row'}
    Excel{prop:Release} = excel:Selected
    Yield()
    Return CurrentRow

ExcelPasteSpecial       Procedure(String    func:Range)
Selection       CString(20)
    Code
    ExcelSelectRange(func:Range)
    Selection   = Excel{'ActiveCell'}
    Excel{Selection & '.PasteSpecial'}
    Excel{prop:Release} = Selection
    Yield()

ExcelConvertFormula     Procedure(String    func:Formula)
    Code
    Return Excel{'Application.ConvertFormula("' & Clip(func:Formula) & '",' & 0FFFFEFCAh & ',' & 1 & ')'}

ExcelGetFilename        Procedure(Byte  func:DontAsk)
sav:Path        CString(255)
func:Desktop     CString(255)
    Code

        SHGetSpecialFolderPath( GetDesktopWindow(), func:Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
        func:Desktop = Clip(func:Desktop) & '\ServiceBase Export'

        !Does the Export Folder already Exists?
        If ~Exists(Clip(func:Desktop))
            If ~MkDir(func:Desktop)
                Return Level:Fatal

            End !If MkDir(func:Desktop)
        End !If Exists(Clip(tmp:Desktop))

        Error# = 0
        sav:Path = Path()
        SetPath(func:Desktop)

        func:Desktop = Clip(func:Desktop) & '\' & CLIP(Excel:ProgramName) & ' ' & FORMAT(TODAY(), @D12) & '.xls'

        If func:DontAsk = False
            IF NOT FILEDIALOG('Save Spreadsheet', func:Desktop, 'Microsoft Excel Workbook|*.XLS', |
                FILE:KeepDir + FILE:Save + FILE:NoError + FILE:LongName)
                Error# = 1
            End!IF NOT FILEDIALOG('Save Spreadsheet', tmp:Desktop, 'Microsoft Excel Workbook|*.XLS', |
        End !If func:DontAsk = True

        SetPath(sav:Path)

        If Error#
            Return Level:Fatal
        End !If Error#
        excel:FileName    = func:Desktop
        Return Level:Benign

ExcelGetDirectory       Procedure()
sav:Path        CString(255)
func:Desktop    CString(255)
    Code
        SHGetSpecialFolderPath( GetDesktopWindow(), func:Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
        func:Desktop = Clip(func:Desktop) & '\ServiceBase Export\'
        !Does the Export Folder already Exists?
        Error# = 0
        If ~Exists(Clip(func:Desktop))
            If ~MkDir(func:Desktop)
                If Not FileDialog('Save Spreadsheet To Folder', func:Desktop, ,FILE:KeepDir+ File:Save + File:NoError + File:LongName + File:Directory)
                    Return Level:Fatal
                End !+ File:LongName + File:Directory)
            End !If MkDir(func:Desktop)
        End !If Exists(Clip(tmp:Desktop))

        excel:FileName  = func:Desktop
        Return Level:Benign

ExcelColumnLetter     Procedure(Long func:ColumnNumber)
local:Over26        Long()
Code
    local:Over26 = 0
    If func:ColumnNumber > 26
        Loop Until func:ColumnNumber <= 26
            local:Over26 += 1
            func:ColumnNumber -= 26
        End !Loop Until ColumnNumber <= 26.
    End !If func:ColumnNumber > 26

    If local:Over26 > 26
        Stop('ExcelColumnLetter Procedure Out Of Range!')
    End !If local:Over26 > 26
    If local:Over26 > 0
        Return Clip(CHR(local:Over26 + 64)) & Clip(CHR(func:ColumnNumber + 64))
    Else !If local:Over26 > 0
        Return Clip(CHR(func:ColumnNumber + 64))
    End !If local:Over26 > 0
ExcelOpenDoc        Procedure(String func:FileName)
Code
    Excel{'Workbooks.Open("' & Clip(func:FileName) & '")'}
ExcelFreeze         Procedure(String func:Cell)
Code
    Excel{'Range("' & Clip(func:Cell) & '").Select'}
    Excel{'ActiveWindow.FreezePanes'} = True

BRW8.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = tra:Account_Number
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      tmp:Tag = ''
    ELSE
      tmp:Tag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:tag = '*')
    SELF.Q.tmp:Tag_Icon = 2
  ELSE
    SELF.Q.tmp:Tag_Icon = 1
  END


BRW8.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW8::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW8::RecordStatus=ReturnValue
  IF BRW8::RecordStatus NOT=Record:OK THEN RETURN BRW8::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = tra:Account_Number
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::9:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW8::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW8::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW8::RecordStatus
  RETURN ReturnValue

Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
