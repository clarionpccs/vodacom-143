  MEMBER('cellmain.clw')

  INCLUDE('ABFILE.INC'),ONCE

  MAP
CELLMBCA:DctInit    PROCEDURE
CELLMBCA:DctKill    PROCEDURE
CELLMBCA:FilesInit  PROCEDURE
  END

Hide:Access:LOGSERST CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:LOGSERST CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:LOGDEFLT CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:LOGDEFLT CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:LOGSTLOC CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:LOGSTLOC CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:MULTIDEF CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:MULTIDEF CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:TRAFAULO CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:TRAFAULO CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:WEBJOBNO CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:WEBJOBNO CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:TRAFAULT CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
ValidateFieldServer    PROCEDURE(UNSIGNED Id,BYTE HandleErrors),BYTE,PROC,DERIVED
                     END


Hide:Relate:TRAFAULT CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:OBFBROWSE CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:OBFBROWSE CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:EXMINLEV CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:EXMINLEV CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:ORACLECN CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:ORACLECN CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:PENDAUD  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:PENDAUD  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:ORACSPEX CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:ORACSPEX CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:LOGSTHII CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:LOGSTHII CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:LOGCLSTE CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:LOGCLSTE CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:RESUBMIT CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:RESUBMIT CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:OBFREASN CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:OBFREASN CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:STATCRIT CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:STATCRIT CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:LOGDESNO CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:LOGDESNO CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:CPNDPRTS CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:CPNDPRTS CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:LOGSALCD CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:LOGSALCD CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

CELLMBCA:DctInit PROCEDURE
  CODE
  Relate:LOGSERST &= Hide:Relate:LOGSERST
  Relate:LOGDEFLT &= Hide:Relate:LOGDEFLT
  Relate:LOGSTLOC &= Hide:Relate:LOGSTLOC
  Relate:MULTIDEF &= Hide:Relate:MULTIDEF
  Relate:TRAFAULO &= Hide:Relate:TRAFAULO
  Relate:WEBJOBNO &= Hide:Relate:WEBJOBNO
  Relate:TRAFAULT &= Hide:Relate:TRAFAULT
  Relate:OBFBROWSE &= Hide:Relate:OBFBROWSE
  Relate:EXMINLEV &= Hide:Relate:EXMINLEV
  Relate:ORACLECN &= Hide:Relate:ORACLECN
  Relate:PENDAUD &= Hide:Relate:PENDAUD
  Relate:ORACSPEX &= Hide:Relate:ORACSPEX
  Relate:LOGSTHII &= Hide:Relate:LOGSTHII
  Relate:LOGCLSTE &= Hide:Relate:LOGCLSTE
  Relate:RESUBMIT &= Hide:Relate:RESUBMIT
  Relate:OBFREASN &= Hide:Relate:OBFREASN
  Relate:STATCRIT &= Hide:Relate:STATCRIT
  Relate:LOGDESNO &= Hide:Relate:LOGDESNO
  Relate:CPNDPRTS &= Hide:Relate:CPNDPRTS
  Relate:LOGSALCD &= Hide:Relate:LOGSALCD

CELLMBCA:FilesInit PROCEDURE
  CODE
  Hide:Relate:LOGSERST.Init
  Hide:Relate:LOGDEFLT.Init
  Hide:Relate:LOGSTLOC.Init
  Hide:Relate:MULTIDEF.Init
  Hide:Relate:TRAFAULO.Init
  Hide:Relate:WEBJOBNO.Init
  Hide:Relate:TRAFAULT.Init
  Hide:Relate:OBFBROWSE.Init
  Hide:Relate:EXMINLEV.Init
  Hide:Relate:ORACLECN.Init
  Hide:Relate:PENDAUD.Init
  Hide:Relate:ORACSPEX.Init
  Hide:Relate:LOGSTHII.Init
  Hide:Relate:LOGCLSTE.Init
  Hide:Relate:RESUBMIT.Init
  Hide:Relate:OBFREASN.Init
  Hide:Relate:STATCRIT.Init
  Hide:Relate:LOGDESNO.Init
  Hide:Relate:CPNDPRTS.Init
  Hide:Relate:LOGSALCD.Init


CELLMBCA:DctKill PROCEDURE
  CODE
  Hide:Relate:LOGSERST.Kill
  Hide:Relate:LOGDEFLT.Kill
  Hide:Relate:LOGSTLOC.Kill
  Hide:Relate:MULTIDEF.Kill
  Hide:Relate:TRAFAULO.Kill
  Hide:Relate:WEBJOBNO.Kill
  Hide:Relate:TRAFAULT.Kill
  Hide:Relate:OBFBROWSE.Kill
  Hide:Relate:EXMINLEV.Kill
  Hide:Relate:ORACLECN.Kill
  Hide:Relate:PENDAUD.Kill
  Hide:Relate:ORACSPEX.Kill
  Hide:Relate:LOGSTHII.Kill
  Hide:Relate:LOGCLSTE.Kill
  Hide:Relate:RESUBMIT.Kill
  Hide:Relate:OBFREASN.Kill
  Hide:Relate:STATCRIT.Kill
  Hide:Relate:LOGDESNO.Kill
  Hide:Relate:CPNDPRTS.Kill
  Hide:Relate:LOGSALCD.Kill


Hide:Access:LOGSERST.Init PROCEDURE
  CODE
  SELF.Init(LOGSERST,GlobalErrors)
  SELF.Buffer &= logser:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(logser:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(logser:ESNKey,'By I.M.E.I.',0)
  SELF.AddKey(logser:RefNumberKey,'By Ref Number',0)
  SELF.AddKey(logser:ESNStatusKey,'By E.S.N. / I.M.E.I.',0)
  SELF.AddKey(logser:NokiaStatusKey,'By Club Nokia Site',0)
  SELF.LazyOpen = False
  Access:LOGSERST &= SELF


Hide:Relate:LOGSERST.Init PROCEDURE
  CODE
  Hide:Access:LOGSERST.Init
  SELF.Init(Access:LOGSERST,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:LOGSTHII,RI:CASCADE,RI:CASCADE,logsti:IMEIRefNoKey)
  SELF.AddRelationLink(logser:ESN,logsti:IMEI)
  SELF.AddRelation(Relate:LOGSTLOC)


Hide:Access:LOGSERST.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:LOGSERST &= NULL


Hide:Access:LOGSERST.PrimeFields PROCEDURE

  CODE
  logser:Status = 'AVAILABLE'
  PARENT.PrimeFields


Hide:Relate:LOGSERST.Kill PROCEDURE

  CODE
  Hide:Access:LOGSERST.Kill
  PARENT.Kill
  Relate:LOGSERST &= NULL


Hide:Access:LOGDEFLT.Init PROCEDURE
  CODE
  SELF.Init(LOGDEFLT,GlobalErrors)
  SELF.Buffer &= ldef:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(ldef:RecordNumberKey,'By Record Number',1)
  SELF.LazyOpen = False
  Access:LOGDEFLT &= SELF


Hide:Relate:LOGDEFLT.Init PROCEDURE
  CODE
  Hide:Access:LOGDEFLT.Init
  SELF.Init(Access:LOGDEFLT,1)


Hide:Access:LOGDEFLT.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:LOGDEFLT &= NULL


Hide:Relate:LOGDEFLT.Kill PROCEDURE

  CODE
  Hide:Access:LOGDEFLT.Kill
  PARENT.Kill
  Relate:LOGDEFLT &= NULL


Hide:Access:LOGSTLOC.Init PROCEDURE
  CODE
  SELF.Init(LOGSTLOC,GlobalErrors)
  SELF.Buffer &= lstl:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(lstl:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(lstl:RefLocationKey,'By Location',0)
  SELF.LazyOpen = False
  Access:LOGSTLOC &= SELF


Hide:Relate:LOGSTLOC.Init PROCEDURE
  CODE
  Hide:Access:LOGSTLOC.Init
  SELF.Init(Access:LOGSTLOC,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:LOGSERST,RI:CASCADE,RI:CASCADE,logser:RefNumberKey)
  SELF.AddRelationLink(lstl:RecordNumber,logser:RefNumber)
  SELF.AddRelation(Relate:LOGSTOCK)


Hide:Access:LOGSTLOC.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:LOGSTLOC &= NULL


Hide:Relate:LOGSTLOC.Kill PROCEDURE

  CODE
  Hide:Access:LOGSTLOC.Kill
  PARENT.Kill
  Relate:LOGSTLOC &= NULL


Hide:Access:MULTIDEF.Init PROCEDURE
  CODE
  SELF.Init(MULTIDEF,GlobalErrors)
  SELF.Buffer &= mul:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(mul:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(mul:DescriptionKey,'By Description',0)
  SELF.LazyOpen = False
  Access:MULTIDEF &= SELF


Hide:Relate:MULTIDEF.Init PROCEDURE
  CODE
  Hide:Access:MULTIDEF.Init
  SELF.Init(Access:MULTIDEF,1)


Hide:Access:MULTIDEF.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:MULTIDEF &= NULL


Hide:Relate:MULTIDEF.Kill PROCEDURE

  CODE
  Hide:Access:MULTIDEF.Kill
  PARENT.Kill
  Relate:MULTIDEF &= NULL


Hide:Access:TRAFAULO.Init PROCEDURE
  CODE
  SELF.Init(TRAFAULO,GlobalErrors)
  SELF.Buffer &= tfo:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(tfo:Field_Key,'By Field',0)
  SELF.AddKey(tfo:DescriptionKey,'By Description',0)
  SELF.LazyOpen = False
  Access:TRAFAULO &= SELF


Hide:Relate:TRAFAULO.Init PROCEDURE
  CODE
  Hide:Access:TRAFAULO.Init
  SELF.Init(Access:TRAFAULO,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:TRAFAULT)


Hide:Access:TRAFAULO.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:TRAFAULO &= NULL


Hide:Relate:TRAFAULO.Kill PROCEDURE

  CODE
  Hide:Access:TRAFAULO.Kill
  PARENT.Kill
  Relate:TRAFAULO &= NULL


Hide:Access:WEBJOBNO.Init PROCEDURE
  CODE
  SELF.Init(WEBJOBNO,GlobalErrors)
  SELF.Buffer &= wej:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(wej:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(wej:HeadAccountNumberKey,'By Head Account Number',0)
  SELF.AddKey(wej:JobNumberKey,'By Job Number',0)
  SELF.AddKey(wej:HeadJobNumberKey,'By Job Number',0)
  SELF.LazyOpen = False
  Access:WEBJOBNO &= SELF


Hide:Relate:WEBJOBNO.Init PROCEDURE
  CODE
  Hide:Access:WEBJOBNO.Init
  SELF.Init(Access:WEBJOBNO,1)


Hide:Access:WEBJOBNO.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:WEBJOBNO &= NULL


Hide:Relate:WEBJOBNO.Kill PROCEDURE

  CODE
  Hide:Access:WEBJOBNO.Kill
  PARENT.Kill
  Relate:WEBJOBNO &= NULL


Hide:Access:TRAFAULT.Init PROCEDURE
  CODE
  SELF.Init(TRAFAULT,GlobalErrors)
  SELF.Buffer &= taf:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(taf:RecordNumberKey,'taf:RecordNumberKey',1)
  SELF.AddKey(taf:Field_Number_Key,'By Field Number',0)
  SELF.LazyOpen = False
  Access:TRAFAULT &= SELF


Hide:Relate:TRAFAULT.Init PROCEDURE
  CODE
  Hide:Access:TRAFAULT.Init
  SELF.Init(Access:TRAFAULT,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:TRAFAULO,RI:CASCADE,RI:CASCADE,tfo:Field_Key)
  SELF.AddRelationLink(taf:AccountNumber,tfo:AccountNumber)
  SELF.AddRelationLink(taf:Field_Number,tfo:Field_Number)
  SELF.AddRelation(Relate:TRADEACC)


Hide:Access:TRAFAULT.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:TRAFAULT &= NULL


Hide:Access:TRAFAULT.PrimeFields PROCEDURE

  CODE
  taf:Compulsory = 'NO'
  taf:Compulsory_At_Booking = 'NO'
  taf:RestrictLength = 0
  PARENT.PrimeFields


Hide:Access:TRAFAULT.ValidateFieldServer PROCEDURE(UNSIGNED Id,BYTE HandleErrors)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.ValidateFieldServer(Id,HandleErrors)
  CASE Id
  OF 3
    GlobalErrors.SetField('Fault Code Field Number')
    IF NOT INRANGE(taf:Field_Number,1,12)
      ReturnValue = Level:Notify
    END
    IF ReturnValue <> Level:Benign
      IF HandleErrors
        ReturnValue = GlobalErrors.ThrowMessage(Msg:FieldOutOfRange,'1 .. 12')
      END
    END
  END
  RETURN ReturnValue


Hide:Relate:TRAFAULT.Kill PROCEDURE

  CODE
  Hide:Access:TRAFAULT.Kill
  PARENT.Kill
  Relate:TRAFAULT &= NULL


Hide:Access:OBFBROWSE.Init PROCEDURE
  CODE
  SELF.Init(OBFBROWSE,GlobalErrors)
  SELF.FileName &= glo:File_Name
  SELF.Buffer &= obftmp:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(obftmp:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(obftmp:JobNumberKey,'By Job Number',0)
  SELF.AddKey(obftmp:ManufacturerJobNoKey,'By Job Number',0)
  SELF.AddKey(obftmp:JobNumberOnlyKey,'By Job Number',0)
  SELF.LazyOpen = False
  Access:OBFBROWSE &= SELF


Hide:Relate:OBFBROWSE.Init PROCEDURE
  CODE
  Hide:Access:OBFBROWSE.Init
  SELF.Init(Access:OBFBROWSE,1)


Hide:Access:OBFBROWSE.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:OBFBROWSE &= NULL


Hide:Access:OBFBROWSE.PrimeFields PROCEDURE

  CODE
  obftmp:Exchanged = 0
  obftmp:Accessory = 0
  obftmp:Processed = 0
  PARENT.PrimeFields


Hide:Relate:OBFBROWSE.Kill PROCEDURE

  CODE
  Hide:Access:OBFBROWSE.Kill
  PARENT.Kill
  Relate:OBFBROWSE &= NULL


Hide:Access:EXMINLEV.Init PROCEDURE
  CODE
  SELF.Init(EXMINLEV,GlobalErrors)
  SELF.FileName &= glo:File_Name
  SELF.Buffer &= exm:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(exm:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(exm:ManufacturerKey,'By Model Number',0)
  SELF.AddKey(exm:ModelNumberKey,'By Model Number',0)
  SELF.LazyOpen = False
  Access:EXMINLEV &= SELF


Hide:Relate:EXMINLEV.Init PROCEDURE
  CODE
  Hide:Access:EXMINLEV.Init
  SELF.Init(Access:EXMINLEV,1)


Hide:Access:EXMINLEV.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:EXMINLEV &= NULL


Hide:Relate:EXMINLEV.Kill PROCEDURE

  CODE
  Hide:Access:EXMINLEV.Kill
  PARENT.Kill
  Relate:EXMINLEV &= NULL


Hide:Access:ORACLECN.Init PROCEDURE
  CODE
  SELF.Init(ORACLECN,GlobalErrors)
  SELF.Buffer &= orac:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(orac:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(orac:OracleNumberKey,'By Oracle Number',0)
  SELF.AddKey(orac:AuditRecordNumberKey,'By Audit Record Number',0)
  SELF.LazyOpen = False
  Access:ORACLECN &= SELF


Hide:Relate:ORACLECN.Init PROCEDURE
  CODE
  Hide:Access:ORACLECN.Init
  SELF.Init(Access:ORACLECN,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:ORACLEEX)


Hide:Access:ORACLECN.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:ORACLECN &= NULL


Hide:Relate:ORACLECN.Kill PROCEDURE

  CODE
  Hide:Access:ORACLECN.Kill
  PARENT.Kill
  Relate:ORACLECN &= NULL


Hide:Access:PENDAUD.Init PROCEDURE
  CODE
  SELF.Init(PENDAUD,GlobalErrors)
  SELF.FileNameValue = 'PENDAUD'
  SELF.Buffer &= PED:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(PED:KeyRecordNumber,'PED:KeyRecordNumber',1)
  SELF.AddKey(PED:KeyReqRecordNumber,'PED:KeyReqRecordNumber',0)
  SELF.AddKey(PED:KeyOrderDateTimeDesc,'PED:KeyOrderDateTimeDesc',0)
  SELF.LazyOpen = False
  Access:PENDAUD &= SELF


Hide:Relate:PENDAUD.Init PROCEDURE
  CODE
  Hide:Access:PENDAUD.Init
  SELF.Init(Access:PENDAUD,1)


Hide:Access:PENDAUD.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:PENDAUD &= NULL


Hide:Relate:PENDAUD.Kill PROCEDURE

  CODE
  Hide:Access:PENDAUD.Kill
  PARENT.Kill
  Relate:PENDAUD &= NULL


Hide:Access:ORACSPEX.Init PROCEDURE
  CODE
  SELF.Init(ORACSPEX,GlobalErrors)
  SELF.Buffer &= ora1:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(ora1:RecordNumberKey,'By Record Number',1)
  SELF.LazyOpen = False
  Access:ORACSPEX &= SELF


Hide:Relate:ORACSPEX.Init PROCEDURE
  CODE
  Hide:Access:ORACSPEX.Init
  SELF.Init(Access:ORACSPEX,1)


Hide:Access:ORACSPEX.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:ORACSPEX &= NULL


Hide:Access:ORACSPEX.PrimeFields PROCEDURE

  CODE
  ora1:TheDate = Today()
  ora1:TheTime = Clock()
  PARENT.PrimeFields


Hide:Relate:ORACSPEX.Kill PROCEDURE

  CODE
  Hide:Access:ORACSPEX.Kill
  PARENT.Kill
  Relate:ORACSPEX &= NULL


Hide:Access:LOGSTHII.Init PROCEDURE
  CODE
  SELF.Init(LOGSTHII,GlobalErrors)
  SELF.Buffer &= logsti:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(logsti:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(logsti:RefNumberKey,'By I.M.E.I.',0)
  SELF.AddKey(logsti:IMEIRefNoKey,'By I.M.E.I. Number',0)
  SELF.LazyOpen = False
  Access:LOGSTHII &= SELF


Hide:Relate:LOGSTHII.Init PROCEDURE
  CODE
  Hide:Access:LOGSTHII.Init
  SELF.Init(Access:LOGSTHII,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:LOGSERST)
  SELF.AddRelation(Relate:LOGSTHIS)


Hide:Access:LOGSTHII.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:LOGSTHII &= NULL


Hide:Relate:LOGSTHII.Kill PROCEDURE

  CODE
  Hide:Access:LOGSTHII.Kill
  PARENT.Kill
  Relate:LOGSTHII &= NULL


Hide:Access:LOGCLSTE.Init PROCEDURE
  CODE
  SELF.Init(LOGCLSTE,GlobalErrors)
  SELF.Buffer &= logclu:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(logclu:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(logclu:ClubNokiaKey,'By Club Nokia Site',0)
  SELF.LazyOpen = False
  Access:LOGCLSTE &= SELF


Hide:Relate:LOGCLSTE.Init PROCEDURE
  CODE
  Hide:Access:LOGCLSTE.Init
  SELF.Init(Access:LOGCLSTE,1)


Hide:Access:LOGCLSTE.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:LOGCLSTE &= NULL


Hide:Relate:LOGCLSTE.Kill PROCEDURE

  CODE
  Hide:Access:LOGCLSTE.Kill
  PARENT.Kill
  Relate:LOGCLSTE &= NULL


Hide:Access:RESUBMIT.Init PROCEDURE
  CODE
  SELF.Init(RESUBMIT,GlobalErrors)
  SELF.Buffer &= reb:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(reb:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(reb:TheDateKey,'By Date',0)
  SELF.AddKey(reb:MessageTypeKey,'By Date',0)
  SELF.LazyOpen = False
  Access:RESUBMIT &= SELF


Hide:Relate:RESUBMIT.Init PROCEDURE
  CODE
  Hide:Access:RESUBMIT.Init
  SELF.Init(Access:RESUBMIT,1)


Hide:Access:RESUBMIT.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:RESUBMIT &= NULL


Hide:Access:RESUBMIT.PrimeFields PROCEDURE

  CODE
  reb:TheDate = Today()
  reb:TheTime = Clock()
  PARENT.PrimeFields


Hide:Relate:RESUBMIT.Kill PROCEDURE

  CODE
  Hide:Access:RESUBMIT.Kill
  PARENT.Kill
  Relate:RESUBMIT &= NULL


Hide:Access:OBFREASN.Init PROCEDURE
  CODE
  SELF.Init(OBFREASN,GlobalErrors)
  SELF.Buffer &= obf:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(obf:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(obf:DescriptionKey,'By Description',0)
  SELF.AddKey(obf:ActiveDescriptionKey,'By Description',0)
  SELF.LazyOpen = False
  Access:OBFREASN &= SELF


Hide:Relate:OBFREASN.Init PROCEDURE
  CODE
  Hide:Access:OBFREASN.Init
  SELF.Init(Access:OBFREASN,1)


Hide:Access:OBFREASN.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:OBFREASN &= NULL


Hide:Access:OBFREASN.PrimeFields PROCEDURE

  CODE
  obf:Active = 0
  PARENT.PrimeFields


Hide:Relate:OBFREASN.Kill PROCEDURE

  CODE
  Hide:Access:OBFREASN.Kill
  PARENT.Kill
  Relate:OBFREASN &= NULL


Hide:Access:STATCRIT.Init PROCEDURE
  CODE
  SELF.Init(STATCRIT,GlobalErrors)
  SELF.Buffer &= stac:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(stac:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(stac:DescriptionKey,'By Description',0)
  SELF.AddKey(stac:DescriptionOptionKey,'By FieldValue',0)
  SELF.AddKey(stac:LocationDescriptionKey,'By Description',0)
  SELF.LazyOpen = False
  Access:STATCRIT &= SELF


Hide:Relate:STATCRIT.Init PROCEDURE
  CODE
  Hide:Access:STATCRIT.Init
  SELF.Init(Access:STATCRIT,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:STATREP)


Hide:Access:STATCRIT.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:STATCRIT &= NULL


Hide:Relate:STATCRIT.Kill PROCEDURE

  CODE
  Hide:Access:STATCRIT.Kill
  PARENT.Kill
  Relate:STATCRIT &= NULL


Hide:Access:LOGDESNO.Init PROCEDURE
  CODE
  SELF.Init(LOGDESNO,GlobalErrors)
  SELF.Buffer &= ldes:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(ldes:DespatchNoKey,'By Despatch Number',1)
  SELF.LazyOpen = False
  Access:LOGDESNO &= SELF


Hide:Relate:LOGDESNO.Init PROCEDURE
  CODE
  Hide:Access:LOGDESNO.Init
  SELF.Init(Access:LOGDESNO,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:LOGSTHIS)


Hide:Access:LOGDESNO.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:LOGDESNO &= NULL


Hide:Access:LOGDESNO.PrimeFields PROCEDURE

  CODE
  ldes:Date = Today()
  ldes:Time = clock()
  PARENT.PrimeFields


Hide:Relate:LOGDESNO.Kill PROCEDURE

  CODE
  Hide:Access:LOGDESNO.Kill
  PARENT.Kill
  Relate:LOGDESNO &= NULL


Hide:Access:CPNDPRTS.Init PROCEDURE
  CODE
  SELF.Init(CPNDPRTS,GlobalErrors)
  SELF.Buffer &= tmppen:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(tmppen:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(tmppen:JobNumberKey,'By Job Number',0)
  SELF.LazyOpen = False
  Access:CPNDPRTS &= SELF


Hide:Relate:CPNDPRTS.Init PROCEDURE
  CODE
  Hide:Access:CPNDPRTS.Init
  SELF.Init(Access:CPNDPRTS,1)


Hide:Access:CPNDPRTS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:CPNDPRTS &= NULL


Hide:Relate:CPNDPRTS.Kill PROCEDURE

  CODE
  Hide:Access:CPNDPRTS.Kill
  PARENT.Kill
  Relate:CPNDPRTS &= NULL


Hide:Access:LOGSALCD.Init PROCEDURE
  CODE
  SELF.Init(LOGSALCD,GlobalErrors)
  SELF.Buffer &= logsal:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(logsal:RefNumberKey,'By Ref Number',1)
  SELF.AddKey(logsal:SalesCodeKey,'By Sales Code',0)
  SELF.LazyOpen = False
  Access:LOGSALCD &= SELF


Hide:Relate:LOGSALCD.Init PROCEDURE
  CODE
  Hide:Access:LOGSALCD.Init
  SELF.Init(Access:LOGSALCD,1)


Hide:Access:LOGSALCD.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:LOGSALCD &= NULL


Hide:Relate:LOGSALCD.Kill PROCEDURE

  CODE
  Hide:Access:LOGSALCD.Kill
  PARENT.Kill
  Relate:LOGSALCD &= NULL

