

   MEMBER('cellmain.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('CELLM001.INC'),ONCE        !Local module procedure declarations
                     END


ds_Message PROCEDURE (STRING MessageTxt,<STRING HeadingTxt>,<STRING IconSent>,<STRING ButtonsPar>,UNSIGNED MsgDefaults=0,BOOL StylePar=FALSE) !Generated from procedure template - Window

FilesOpened          BYTE
Loc:ButtonPressed    UNSIGNED
EmailLink            CSTRING(4096)
DontShowThisAgain       byte(0)         !CapeSoft MessageBox Data
LocalMessageBoxdata     group,pre(LMBD) !CapeSoft MessageBox Data
MessageText               cstring(4096)
HeadingText               cstring(1024)
UseIcon                   cstring(1024)
Buttons                   cstring(1024)
Defaults                  unsigned
                        end
window               WINDOW('Caption'),AT(,,404,100),FONT('Tahoma',8,0101010H,),COLOR(COLOR:White),GRAY,DOUBLE
                       IMAGE,AT(11,16),USE(?Image1),HIDE
                       PROMPT(''''),AT(118,32),USE(?MainTextPrompt),FONT(,,COLOR:White,,CHARSET:ANSI)
                       STRING('HyperActive Link'),AT(91,46),USE(?HALink),HIDE,FONT(,,COLOR:White,,CHARSET:ANSI)
                       STRING('Time Out:'),AT(103,54),USE(?TimerCounter),HIDE,FONT(,,COLOR:White,,CHARSET:ANSI)
                       CHECK('Dont Show This Again'),AT(85,65),USE(DontShowThisAgain),HIDE,FONT(,,COLOR:White,,CHARSET:ANSI)
                       BUTTON('Button 1'),AT(9,81,45,14),USE(?Button1),HIDE,FONT(,,0101010H,,CHARSET:ANSI)
                       BUTTON('Button 2'),AT(59,81,45,14),USE(?Button2),HIDE,FONT(,,0101010H,,CHARSET:ANSI)
                       BUTTON('Button 3'),AT(109,81,45,14),USE(?Button3),HIDE,FONT(,,0101010H,,CHARSET:ANSI)
                       BUTTON('Button 4'),AT(159,81,45,14),USE(?Button4),HIDE,FONT(,,0101010H,,CHARSET:ANSI)
                       BUTTON('Button 5'),AT(209,81,45,14),USE(?Button5),HIDE,FONT(,,0101010H,,CHARSET:ANSI)
                       BUTTON('Button 6'),AT(259,81,45,14),USE(?Button6),HIDE,FONT(,,0101010H,,CHARSET:ANSI)
                       BUTTON('Button 7'),AT(309,81,45,14),USE(?Button7),HIDE,FONT(,,0101010H,,CHARSET:ANSI)
                       BUTTON('Button 8'),AT(359,81,45,14),USE(?Button8),HIDE,FONT(,,0101010H,,CHARSET:ANSI)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(Loc:ButtonPressed)

ThisMessageBoxLogInit          routine         !CapeSoft MessageBox Initialize properties routine
    if ~omitted(2) then LMBD:HeadingText = HeadingTxt .
    if omitted(3) or (clip(IconSent) = '') then
      LMBD:UseIcon = 'cellular3g.ico'
    else
      LMBD:UseIcon = IconSent
    end
    if ~omitted(4) then LMBD:Buttons = ButtonsPar .
    if ~omitted(5) then LMBD:Defaults = MsgDefaults .
    LMBD:MessageText = MessageTxt
    ThisMessageBox.FromExe = command(0)
    if instring('\',ThisMessageBox.FromExe,1,1)
      ThisMessageBox.FromExe = ThisMessageBox.FromExe[ (instring('\',ThisMessageBox.FromExe,-1,len(ThisMessageBox.FromExe)) + 1) : len(ThisMessageBox.FromExe) ]
    end
    ThisMessageBox.TrnStrings = 1
    ThisMessageBox.PromptControl = ?MainTextPrompt
    ThisMessageBox.IconControl = ?Image1
         !End of CapeSoft MessageBox Initialize properties routine

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
    ThisMessageBox.SavedResponse = GlobalResponse        !CapeSoft MessageBox Code - Preserves the GlobalResponse
  GlobalErrors.SetProcedureName('ds_Message')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Image1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
        GlobalRequest = ThisWindow.Request         !Keep GlobalRequest the correct value.
        if ThisMessageBox.MessagesUsed then
          return level:fatal
        end
        ThisMessageBox.MessagesUsed += 1
        do ThisMessageBoxLogInit                               !CapeSoft MessageBox Code
        ThisMessageBox.ReturnValue = ThisMessageBox.PreOpen(LMBD:MessageText,LMBD:HeadingText,LMBD:Defaults)
        if ThisMessageBox.ReturnValue then
          self.Response = RequestCompleted
          return level:notify
        end
        ThisMessageBox.DontShowThisAgain = ?DontShowThisAgain
        ThisMessageBox.TimeOutPrompt = ?TimerCounter
        ThisMessageBox.HAControl = ?HALink
  OPEN(window)
  SELF.Opened=True
  LMBD:HeadingText = 'ServiceBase Cellular'
  ThisMessageBox.open(LMBD:MessageText,LMBD:HeadingText,LMBD:UseIcon,LMBD:Buttons,LMBD:Defaults,StylePar)         !CapeSoft MessageBox Code
      alert(EscKey)
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
              !CapeSoft MessageBox EndCode
    Loc:ButtonPressed = ThisMessageBox.ReturnValue
    ThisMessageBox.Close()
    if ThisMessageBox.MessagesUsed > 0 then ThisMessageBox.MessagesUsed -= 1 .
              !End of CapeSoft MessageBox EndCode
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
    ReturnValue = ThisMessageBox.SavedResponse           !CapeSoft MessageBox Code - Preserves the GlobalResponse
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ThisMessageBox.TakeEvent()                             !CapeSoft MessageBox Code
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

