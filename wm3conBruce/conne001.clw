

   MEMBER('connect.clw')                              ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('CONNE001.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('CONNE002.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('CONNE003.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('CONNE006.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('CONNE007.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('CONNE010.INC'),ONCE        !Req'd for module callout resolution
                     END


ClarioNET:ConnectWindow PROCEDURE                     !Generated from procedure template - Window

FullProgramID        STRING(127)
Server_IP            STRING(20)
Port_Used            LONG
LocalPrinter         STRING(100)
UseGraphics          STRING(3)
ClipboardString      STRING(20)
OSstring             STRING(30)
DateString           DATE
TimeString           TIME
order_creation       BYTE
Part_Number          STRING(30)
Quantity             STRING(6)
LOC:AutoConnectTimer LONG(500)
LOC:ISAPI_Mode       BYTE(FALSE)
LOC:ISAPI_Broker_Ver STRING('5.5')
SBVersion            STRING(30)
LOC:UseBalancer      BYTE
ServerFail           BYTE
tmp:LicenseText      STRING(2000)
LogFilename          STRING(255)
Window               WINDOW('ServiceBase 3g Webmaster'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),ICON('Cellular3g.ico'),WALLPAPER('sbback.jpg'),TILED,TIMER(500),GRAY,DOUBLE,IMM
                       STRING('Bruce''s Version - Not to be released for general use'),AT(88,402),USE(?StringBruce),TRN,FONT('Arial Rounded MT Bold',24,COLOR:Red,,CHARSET:ANSI)
                       SHEET,AT(160,17,360,385),USE(?Sheet1),COLOR(0AFEFF7H,,0AFEFF7H)
                         TAB('Tab 1'),USE(?Tab:Logo),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(0D6E7EFH)
                           PANEL,AT(164,20,352,14),USE(?Panel5),FILL(09A6A7CH)
                           PROMPT('License Agreement'),AT(168,22),USE(?Prompt11),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PANEL,AT(164,37,352,147),USE(?Panel3),FILL(COLOR:White),BEVEL(-1,-1)
                           IMAGE('sbtitle.jpg'),AT(176,40),USE(?Image1),CENTERED
                           PANEL,AT(164,188,352,178),USE(?Panel4),FILL(09A6A7CH)
                           TEXT,AT(172,194,336,166),USE(tmp:LicenseText),SKIP,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White)
                           BUTTON,AT(380,370),USE(?NewJobButton),TRN,FLAT,LEFT,ICON('accept9p.jpg')
                           BUTTON,AT(448,370),USE(?Exit),TRN,FLAT,LEFT,FONT(,8,,,CHARSET:ANSI),ICON('declinep.jpg')
                           BUTTON('Monitor'),AT(252,376,56,16),USE(?MonitorContacts),HIDE
                           BUTTON,AT(168,370),USE(?Config_Button),TRN,FLAT,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI),ICON('configp.jpg')
                         END
                         TAB('Tab 2'),USE(?Tab:Options),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(0D6E7EFH)
                           GROUP('Logon Details'),AT(176,60,332,76),USE(?Logon_Group),BOXED,FONT('Tahoma',8,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('Company'),AT(180,72),USE(?Prompt1),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(256,72,132,10),USE(Company),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                             ENTRY(@s30),AT(256,88,133,10),USE(LicenceNumber,,?LicenceNumber:2),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                             ENTRY(@s30),AT(256,104,133,10),USE(LogonID),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                             BUTTON('Edit Receipt Text'),AT(408,110,62,24),USE(?EditTextButton),HIDE,LEFT
                             ENTRY(@s20),AT(256,120,133,10),USE(LogonPassword),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                             STRING('Trade Account'),AT(180,104),USE(?Logon_ID_String),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING('Account Password'),AT(180,120),USE(?Logon_Password_String),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING('Licence Number'),AT(180,88),USE(?L_No_string),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           END
                           PANEL,AT(164,22,352,344),USE(?Panel6),FILL(09A6A7CH)
                           GROUP('Local Details'),AT(176,170,332,54),USE(?Client_Group),BOXED,FONT('Tahoma',8,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s255),AT(268,206,136,10),USE(LocalSavePath),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),READONLY
                             BUTTON,AT(408,202),USE(?LookupFile),TRN,FLAT,ICON('lookupp.jpg')
                             PROMPT('Download Directory'),AT(180,206),USE(?SaveDirPrompt),RIGHT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             BUTTON,AT(268,175),USE(?Button16),TRN,FLAT,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI),ICON('setprnp.jpg')
                             CHECK('ISAPI Mode?'),AT(428,178,49,10),USE(LOC:ISAPI_Mode),TRN,HIDE,LEFT,FONT('Tahoma',,,,CHARSET:ANSI),TIP('Toggle between ISAPI and EXEBroker Calling')
                             STRING('Broker Ver. :'),AT(428,190,44,12),USE(?unnamed),TRN,HIDE,RIGHT,FONT('Tahoma',8,,,CHARSET:ANSI)
                             COMBO(@n3.1),AT(476,190,24,10),USE(LOC:ISAPI_Broker_Ver),HIDE,CENTER,DROP(2),FROM('5.0|5.5')
                           END
                           GROUP('Server Information'),AT(176,258,332,68),USE(?Server_Group),BOXED,FONT('Tahoma',8,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s50),AT(268,269,133,10),USE(CLRNT_C.HostProgramDirectory,,?HostProgramDirectory),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                             BUTTON,AT(476,264),USE(?ServerSetup),TRN,FLAT,ICON('lookupp.jpg')
                             PROMPT('Edit Server List'),AT(412,269),USE(?Prompt9),LEFT,FONT('Tahoma',8,COLOR:White,,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s50),AT(268,285,133,10),USE(CLRNT_C.ServerName,,?lpszServerName:2),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                             SPIN(@n4),AT(268,301,47,10),USE(CLRNT_C.ServerPort,,?ServerPort),RIGHT(2),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),STEP(1)
                             CHECK('Use Advanced Login Sytem?'),AT(372,301),USE(LOC:UseBalancer),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1','0')
                             STRING('Main Server Directory'),AT(180,269),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING('Main Server Name'),AT(180,285),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING('Main Server Port'),AT(180,301),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           END
                           STRING('Configuration - Do not change unless authorised by a supervisor'),AT(168,32,345,12),TRN,CENTER,FONT(,11,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(448,370),USE(?OK_Buttton),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT
                         END
                         TAB('Tab 3'),USE(?Tab:Local)
                         END
                         TAB('Tab 4'),USE(?Tab:Text)
                           STRING('Note:'),AT(212,52),USE(?String16),FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING('The text entered below will be shown at the foot of the'),AT(252,52),USE(?String17)
                           STRING('receipt printed when you accept a customer''s phone '),AT(252,60),USE(?String19)
                           STRING('Line breaks will be preserved and the text centred.'),AT(252,68),USE(?String20)
                           TEXT,AT(216,96,256,92),USE(Receipt_Text),CENTER,FONT('Arial',10,,,CHARSET:ANSI)
                           BUTTON,AT(428,350,56,16),USE(?TextOK_Button)
                         END
                       END
                       PANEL,AT(164,370,352,28),USE(?Panel2),FILL(09A6A7CH)
                     END

AppFrame             CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Reset                  PROCEDURE(BYTE Force=0),DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FileLookup5          SelectFileClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = AppFrame.Run()

Read_ini        Routine
 SBVersion                  = 'Version: '&clip(getini ('global', 'SBVERSION','Unknown',path()&'\conweb.ini'))
 CLRNT_C.ServerName         = getini ('global', 'servername',,path()&'\conweb.ini')    !'192.168.0.111'
 CLRNT_C.ServerPort         = getini ('global', 'serverport',,path()&'\conweb.ini')    !100
 LicenceNumber              = getini ('global', 'licencenumber',,path()&'\conweb.ini') !set as none_set
 LogonID                    = getini ('global', 'ID',,path()&'\conweb.ini')            !set as CC
 LogonPassword              = getini ('global', 'password',,path()&'\conweb.ini')      !set as CC
 Company                    = getini ('global', 'company',,path()&'\conweb.ini')       !set as ALL
 if company = 'DEBUGJ' then
    ?MonitorContacts{prop:hide}=false
 ELSE
    ?MonitorContacts{prop:hide}=true
 END
 Receipt_Text               = getini ('global', 'Text',,path()&'\conweb.ini')          !Set as anything you like
 loc:UseBalancer            = getini ('global' ,'UseAdvanced',,path()&'\conweb.ini')
 UseGraphics                = getini ('global', 'graphics',,path()&'\conweb.ini')
 LocalSavePath              = getini ('global', 'DownLoadPath',,path()&'\conweb.ini')
 If LocalSavePath = '' then
     LocalSavePath = path()
 END

 !make a note of the default printer
 DefaultPrinter = Printer{PROPPRINT:Device}
       
              
! !Restore || to carriage return/line feeds
! Loop x#=1 to len(clip(Receipt_Text))
!   if Receipt_Text[x#]   = '|' then
!      
!      Receipt_Text[x#]   = chr(13)
!      Receipt_Text[x#+1] = chr(10)
!   End!If '|'
! end !Loop
!-------------------------------------------------------------------------------------------------
! You can set up to FIVE "command line" strings that will be received by the server
!-------------------------------------------------------------------------------------------------
 CLRNT_C.Param1 = ''
 CLRNT_C.Param2 = LogonID&','&LogonPassword
 CLRNT_C.Param3 = LicenceNumber
 CLRNT_C.Param4 = UseGraphics
 CLRNT_C.Param5 = 'V3.0'

 supervisor = false

Handle:Window:Init              ROUTINE
 IF EXISTS('LABELS') THEN REMOVE('LABELS').
!---
 ?Sheet1{PROP:Wizard } = TRUE
 
!----------------------------------------------------------------------------------
 CLEAR(CLRNT_C)
!----------------------------------------------------------------------------------
!    Automatically set working directory for downloaded files to the Windows
!    default setting.
!    All ICO, GIF, JPG and printer WMF files are placed here.
!    WMF are always erased after use. Others depend on GraphicDownloadFlag
!----------------------------------------------------------------------------------
 !iF GetTempPath(FILE:MaxFilepath, CLRNT_C.ClientTempDirectory) = 0
    CLRNT_C.ClientTempDirectory = LONGPATH() & '\temp\'
    if (~exists(CLRNT_C.ClientTempDirectory))
        if  (mkDir( CLRNT_C.ClientTempDirectory ))
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occurred creating the  "temp" folder. Please check you have full read-write access to your webmaster folder.','Error',|
                           'mstop.jpg','/&OK') 
                Of 1 ! &OK Button
            End!Case Message
            POst(Event:CloseWindow)
        end ! if  (mkDir( CLRNT_C.ClientTempDirectory ))
        
    end ! if (~exists(CLRNT_C.ClientTempDirectory))
  !  MESSAGE('Windows did not report a temporary directory.||ClarioNET is assuming "' & CLIP(CLRNT_C.ClientTempDirectory) & '"  .|| Please verify & modify as needed.', |
!            'No TEMP Directory !','ServiceBase 3g Webmaster', ICON:Exclamation)
 !END
 CLRNT_C.GraphicDownloadFlag          = 12
 CLRNT_C.ConnectionTimeout            = 6000 ! Seconds
 CLRNT_C.ProgressUpdateFrequency      = 2  ! Seconds
 CLRNT_C.UseProxy                     = 0
 !DISABLE(?ProxyServer) ;DISABLE(?ProxyGroup)
 CLRNT_C.UserAgent                    = ''   ! Defaults to 'clarionet'
 CLRNT_C.ProxyServer                  = ''
 CLRNT_C.ProxyUserName                = ''
 CLRNT_C.ProxyPassword                = ''
 CLRNT_C.CharSet                      = CHARSET:ANSI
!-------------------------------------------------------------------------------------------------
! This sets to the ClarioNET server - to be modified to read settings from the .ini file -
!-------------------------------------------------------------------------------------------------
! message ('path is ' & path())  - confirmed that it points to the current directory
 CLRNT_C.HostProgramName    = GETINI ('global', 'hostprogram','',path()&'\conweb.ini')   !'WEBALL.EXE'
! if CLRNT_C.HostProgramName = 'qv7fail8zz' then
!    MESSAGE('Windows did not report an initialization file||You will need to configure the program before use||'|
!            &' Please enter the details you were given as defaults', |
!            'No ini file!', ICON:Exclamation)
!    CLRNT_C.HostProgramName = ''
! end !if fail

 do read_ini
 
!Redundant checks for hide and unhiding buttons
! if instring('/s', LicenceNumber,1,1) > 0 then unhide(?connect_system).
! if instring('/r', LicenceNumber,1,1) > 0 then unhide(?connect).
! if instring('/n', LicenceNumber,1,1) > 0 then unhide(?NewJobButton).


! Window{PROP:Timer} = LOC:AutoConnectTimer
!---
 DO Handle:Set:Connect:Mode
!---
 EXIT
!------------------------------------------------------------------------------
Handle:Button:Options       ROUTINE
!---
 SELECT(?Tab:Options)
 0{PROP:Timer} = 0
!---
 EXIT
!------------------------------------------------------------------------------
Handle:Button:Connect       ROUTINE
 DATA
LOC:ReturnVal     LONG
 CODE
!---

 SETCURSOR(CURSOR:Wait)
 0{PROP:Timer} = 0
 HIDE(0)

!!13185 - check if the Balancer serverIP and port are sensible and if not reget them - J - 07/02/14
!
!    ServerFail = false
!    Server_IP = getini('balancer', 'Server_IP',,path()&'\conweb.ini')
!    Port_Used = getini('balancer', 'Port_Used',,path()&'\conweb.ini')
!
!    !Check 1 - server ip must be xxx.xxx.xxx.xxx - each a number or a stop
!    Loop x# = 1 to len(clip(Server_IP))
!        if instring(Server_IP[x#],'1234567890.',1,1) = 0 then
!            ServerFail = 1
!            break
!        END
!    END !loop along Server_IP
!
!    !check 2 - port used must not be zero
!    if Port_used = 0 then ServerFail = 1.
!
!    If ServerFail = 1 then
!        !swap back to Graceful mode so it will reget serverID and port
!        PUTINI('balancer','Graceful','YES',path()&'\conweb.ini')
!
!    END !if
!
!!13185 - END 

!J - 02/06/14 - temporary logging added
 logfilename = clip(path())&'\Balancer.log'


 if loc:UseBalancer then

     IF CLIP(GETINI('balancer','Graceful',,path()&'\conweb.ini')) <> 'NO'

        !J - 02/06/14 - logging
        Lineprint(format(today(),@d06)&' - '&format(clock(),@t04)&' - Balancer is Graceful',LogFileName)
        !TB13185 - J - 28/05/14 - At Neil's suggestion,
        !Neil - Suggest if temp" returns something that doesn't match what is expected - retry the temp" call until it does.
        LOOP    !to repeat call to Nbalance.exe

            Lineprint(format(today(),@d06)&' - '&format(clock(),@t04)&' - server and port set to '&clip(Server_IP)&' - '&clip(Port_used),LogFileName)

            CLRNT_C.HostProgramName = 'Nbalance.exe'
            CLRNT_C.HostProgramDirectory = '\EXEC\'
            temp" = ClarioNet:QueryServerProgram('BALANCE:0:0')

            Lineprint(format(today(),@d06)&' - '&format(clock(),@t04)&' - Call to balancer returns '&clip(Temp"),LogFileName)

            !strip details from temp file
            LOOP g# = 1 TO LEN(CLIP(temp"))
                IF SUB(temp",g#,1) = ':'
                    server_ip = CLIP(SUB(temp",1,g#-1))
                    port_used = CLIP(SUB(temp",g#+1,LEN(CLIP(temp"))))
                    if company = 'DEBUGJ' then
                      MESSAGE('Graceful: '&GETINI('balancer','Graceful',,path()&'\conweb.ini'),'ServiceBase 3g Webmaster')
                      MESSAGE('IP '&server_ip,'ServiceBase 3g Webmaster')
                      MESSAGE('Port '&port_used,'ServiceBase 3g Webmaster')
                    END
                    BREAK !From loop to find ':'
                END  !if ':' found in temp"
            END !loop to find ':' in temp$

            !TB13185 Validate the IP and port returned
            ServerFail = false

            !Check 1 - server ip must be xxx.xxx.xxx.xxx - each a number or a stop
            Loop x# = 1 to len(clip(Server_IP))
                if instring(Server_IP[x#],'1234567890.',1,1) = 0 then
                    ServerFail = 1
                    Lineprint(format(today(),@d06)&' - '&format(clock(),@t04)&' - Illegal character at '& x# &' in '&clip(Server_IP),LogFileName)
                    break  !from loop along Server_IP
                END !if character matches
            END !loop along Server_IP

            !check 2 - port used must not be zero
            if Port_used = 0 then
                ServerFail = 1
                Lineprint(format(today(),@d06)&' - '&format(clock(),@t04)&' - Port is zero',LogFileName)
            END

            If ServerFail = 0 then break.   !Break from loop to retry the NBalancer call

            Lineprint(format(today(),@d06)&' - '&format(clock(),@t04)&' - Retry the call to balancer',LogFileName)

        END ! loop to repeat call to nbalance.exe   TB13185 - finished

     ELSE

         Loop ! loop to repeat call to nbalance.exe   TB13185 - start

            Lineprint(format(today(),@d06)&' - '&format(clock(),@t04)&' - Graceful - Balancer was Graceful',LogFileName)

            Server_IP = getini('balancer', 'Server_IP',,path()&'\conweb.ini')
            Port_Used = getini('balancer', 'Port_Used',,path()&'\conweb.ini')
            Lineprint(format(today(),@d06)&' - '&format(clock(),@t04)&' - Graceful - Server IP and port reset to '&clip(Server_IP)&' - '&Port_Used,LogFileName)

            CLRNT_C.HostProgramName = 'Nbalance.exe'
            CLRNT_C.HostProgramDirectory = '\EXEC\'

            temp" = ClarioNet:QueryServerProgram('BALANCE:'&CLIP(Server_IP)&':'&CLIP(Port_Used))
            Lineprint(format(today(),@d06)&' - '&format(clock(),@t04)&' - Graceful - Call to balancer returns '&clip(Temp"),LogFileName)
            !strip temp file!
            LOOP g# = 1 TO LEN(CLIP(temp"))
              IF SUB(temp",g#,1) = ':'
                server_ip = CLIP(SUB(temp",1,g#-1))
                port_used = CLIP(SUB(temp",g#+1,LEN(CLIP(temp"))))
                if company = 'DEBUGJ' then
                   MESSAGE('Graceful: '&GETINI('balancer','Graceful',,path()&'\conweb.ini'),'ServiceBase 3g Webmaster')
                   MESSAGE('IP '&server_ip,'ServiceBase 3g Webmaster')
                   MESSAGE('Port '&port_used,'ServiceBase 3g Webmaster')
                END
                BREAK !from loop along temp"
              END !if : found
            END !loop along temp"

            !TB13185 Validate the IP and port returned
            ServerFail = false

            !Check 1 - server ip must be xxx.xxx.xxx.xxx - each a number or a stop
            Loop x# = 1 to len(clip(Server_IP))
                if instring(Server_IP[x#],'1234567890.',1,1) = 0 then
                    ServerFail = 1
                    Lineprint(format(today(),@d06)&' - '&format(clock(),@t04)&' - Graceful - Illegal character at '& x# &' in '&clip(Server_IP),LogFileName)
                    break  !from loop along Server_IP
                END !if character matches
            END !loop along Server_IP

            !check 2 - port used must not be zero
            if Port_used = 0 then
                ServerFail = 1
                Lineprint(format(today(),@d06)&' - '&format(clock(),@t04)&' - Graceful - Port is '&Port_used,LogFileName)
            END !if port is zero

            If ServerFail = 0 then break.   !Break from loop to retry the NBalancer call

            Lineprint(format(today(),@d06)&' - '&format(clock(),@t04)&' - Graceful - Retry the call to balancer',LogFileName)

        END ! loop to repeat call to nbalance.exe   TB13185 - finished

     END

     Lineprint(format(today(),@d06)&' - '&format(clock(),@t04)&' - Break from call to balancer - server and port valid '&clip(Server_IP)&' - '&clip(Port_used),LogFileName)

     !Assume fail scenario in case of ctrl-alt-del!
     PUTINI ('balancer' ,'Server_IP'  ,Server_IP                ,path()&'\conweb.ini')
     PUTINI ('balancer' ,'Port_Used'  ,Port_Used                ,path()&'\conweb.ini')
     PUTINI ('balancer' ,'Graceful'  ,'NO'                ,path()&'\conweb.ini')

     CLRNT_C.HostProgramDirectory = getini('global', 'hostdirectory',,path()&'\conweb.ini') !'PUBLIC/WEBINONE'               !EXE Broker Version
     !CLRNT_C.HostProgramName = 'wm30job.exe'
     CLRNT_C.ServerName         = server_ip
     CLRNT_C.ServerPort         = port_used

     CLRNT_C.HostProgramName = 'wm30job.EXE'
     !CLRNT_C.Param1 = ''   !LogonUserPassword
     !CLRNT_C.Param5 = 'V3.0('& AppFrame{PROP:XPOS}&','&AppFrame{prop:ypos}&')'

     !create new param 5
     !Format needed 'http://192.168.0.111:100/directory/directory/'
     CLRNT_C.Param5 = 'http://' & clip(CLRNT_C.ServerName) & ':' & clip(CLRNT_C.ServerPort)
     if CLRNT_C.HostProgramDirectory[1]='/' then
       Clrnt_C.Param5 = clip(CLRNT_C.PARAM5)&clip(CLRNT_C.HostProgramDirectory)
     ELSE
       Clrnt_C.Param5 = clip(CLRNT_C.PARAM5)&'/'&clip(CLRNT_C.HostProgramDirectory)
     END
     x#=len(clip(CLRNT_C.HostProgramDirectory))
     if CLRNT_C.HostProgramDirectory[x#] <> '/' then
       Clrnt_C.Param5 = clip(Clrnt_C.Param5)&'/'
     END
!    constring" = CNManager:getBalancerConnection( CLRNT_C.serverName,CLRNT_C.ServerPort,clip(CLRNT_C.HostProgramName),30)
!    if company = 'DEBUGJ' then
!        message('Balancer Response ; '&constring"&|
!                '|Server Name : '& CLRNT_C.serverName &|
!                '|Directory : '&CLRNT_C.HostProgramDirectory &|
!                '|Port : '& CLRNT_C.ServerPort)
!    END !if debugJ
!    constring" = CNManager:GetServerConnection(clip(CLRNT_C.serverName),clip(CLRNT_C.ServerPort),clip(CLRNT_C.HostProgramName),30)
!    if company = 'DEBUGJ' then
!        message('Server Response ; '&constring"&|
!                '|Server Name : '& CLRNT_C.serverName &|
!                '|Directory : '&CLRNT_C.HostProgramDirectory &|
!                '|Port : '& CLRNT_C.ServerPort)
!    END !if debugJ
!    if clip(constring") <> '1' then
!         !System has failed - one last try with original settings?
!         do read_ini
!    END

 END !if glo:useBalancer                                                ! REQUIRED

 !
 !CLRNT_C.PingFlag = 1      (ClarioNET:StartServerProgram == Returns 99)
 !

 IF CLIP(server_ip) = '0.0.0.0' !Fail mode TOTAL
   !FAIL!
   MESSAGE('UNABLE TO LOCATE A WEBMASTER SERVER - PLEASE TRY AGAIN')
   UNHIDE(0)
   EXIT
 ELSE
   ! Start - Run splash window (DBH: 25-05-2005)
   RUN('CELLULAR.EXE ServiceBase Webmaster')
   ! End   - Run splash window (DBH: 25-05-2005)
   LOC:ReturnVal = ClarioNET:StartServerProgram()
   
 END

 SETCURSOR()

 if loc:ReturnVal AND LOC:ReturnVal <> CLRNT_ERR_VERSION_MISMATCH then
    
    Lineprint(format(today(),@d06)&' - '&format(clock(),@t04)&' - returned '&clip(loc:ReturnVal)&' - attempting the resubmissions',LogFileName)

     ! must now try thrice more for each server in the file if this has failed for "Could not connect " before letting it continue
     access:Server.clearkey(SRV:KeyPriority)
     SRV:Priority = 1
     set(SRV:KeyPriority,SRV:KeyPriority)
     loop
        !find the settings for the next server
         if access:server.next() then break.
         CLRNT_C.serverName           = SRV:IPAddrress
         CLRNT_C.ServerPort           = SRV:Port
         CLRNT_C.HostProgramDirectory = SRV:ProgramPath
         Lineprint(format(today(),@d06)&' - '&format(clock(),@t04)&' - Now trying next server ; '&|
                    ' Server Name : '& CLRNT_C.serverName &|
                    ' Directory : '&CLRNT_C.HostProgramDirectory &|
                    ' Port : '& CLRNT_C.ServerPort,LogFileName)
         if company = 'DEBUGJ' then
            message('Now trying next server ; '&|
                    '|Server Name : '& CLRNT_C.serverName &|
                    '|Directory : '&CLRNT_C.HostProgramDirectory &|
                    '|Port : '& CLRNT_C.ServerPort,'ServiceBase 3g Webmaster')
         END !if debugJ

         !now try up to thrice on these settings

         !Pause for a second and try again
         savetime# = clock()
         loop while clock() < Savetime#+200.
         LOC:ReturnVal = ClarioNET:StartServerProgram()         !ONE
         if LOC:ReturnVal <>  CLRNT_ERR_SESSION_FAILURE then break.

         !Pause for a second and try again
         savetime# = clock()
         loop while clock() < Savetime#+200.
         LOC:ReturnVal = ClarioNET:StartServerProgram()         !TWO
         if LOC:ReturnVal <>  CLRNT_ERR_SESSION_FAILURE then break.

         !Pause for a second and try again
         savetime# = clock()
         loop while clock() < Savetime#+200.
         LOC:ReturnVal = ClarioNET:StartServerProgram()         !THREE
         if LOC:ReturnVal <>  CLRNT_ERR_SESSION_FAILURE then break.


     END !Loop through servers
 END !If Loc:Returnval

 !SHould be if STILL Loc:ReturnVal
 IF LOC:ReturnVal
    PUTINI('STARTING','Run',True,)

    PUTINI ('balancer' ,'Graceful'  ,'YES'                ,path()&'\conweb.ini')
    CASE LOC:ReturnVal
     OF CLRNT_ERR_NO_CREATE_TEMP
        MESSAGE('Could not create temporary file'                 ,'ServiceBase 3g Webmaster',ICON:Exclamation)
     OF CLRNT_ERR_HTTP_FAILURE       
        MESSAGE('Could not open an internet session'              ,'ServiceBase 3g Webmaster',ICON:Exclamation)
     OF CLRNT_ERR_SESSION_FAILURE    
        MESSAGE('Could not connect to server program'             ,'ServiceBase 3g Webmaster',ICON:Exclamation)
     OF CLRNT_ERR_DECOMP_INIT_FAILURE
        MESSAGE('Could not initialize decompression library'      ,'ServiceBase 3g Webmaster',ICON:Exclamation)
     OF CLRNT_ERR_NO_HOSTNAME
        MESSAGE('No Host Name specified'                          ,'ServiceBase 3g Webmaster',ICON:Exclamation)
     OF CLRNT_ERR_NO_HOSTDIR
        MESSAGE('No Host Directory specified'                     ,'ServiceBase 3g Webmaster',ICON:Exclamation)
     OF CLRNT_ERR_NO_SERVERMAME
        MESSAGE('No Server Name specified'                        ,'ServiceBase 3g Webmaster',ICON:Exclamation)
     OF CLRNT_ERR_VERSION_MISMATCH
        MESSAGE('Version Mismatch'                                ,'ServiceBase 3g Webmaster',ICON:Exclamation)
     !this one don't work??
     OF 12152
        MESSAGE('The Session has been ended due to inactivity from the terminal|Please re-login','ServiceBase 3g Webmaster',ICON:Exclamation)
   ELSE
     if LOC:ReturnVal <> 12152
        MESSAGE('Unknown error occurred [ ' & LOC:ReturnVal & ' ]','ClarioNET Error1',ICON:Exclamation)
     END !If loc:ReturnVal was 12152
   END
   UNHIDE(0); DISPLAY; EXIT
 END
 !----------------------------------------------------------------------------------------------
 ! At this time CLRNT_C.ServerString1 -> 3 is available for inspection
 ! Also, CLRNT_C.HostSessionID has been set.
 !----------------------------------------------------------------------------------------------
 LOC:ReturnVal = ClarioNET:StartClientSession()                  ! REQUIRED

 PUTINI('STARTING','Run',True,)
 !In my understanding this is where control is returned to the local machine!

 CASE LOC:ReturnVal
   OF CLRNT_ERR_NONE
     IF loc:UseBalancer
       PUTINI ('balancer' ,'Graceful'  ,'YES'                ,path()&'\conweb.ini')
     END
   OF 12152
      MESSAGE('The Session has been ended due to inactivity from the terminal|Please re-login','ServiceBase 3g Webmaster',ICON:Exclamation)
      !Ungraceful - but controlled!
      IF loc:UseBalancer
        PUTINI ('balancer' ,'Server_IP'  ,Server_IP                ,path()&'\conweb.ini')
        PUTINI ('balancer' ,'Port_Used'  ,Port_Used                ,path()&'\conweb.ini')
        PUTINI ('balancer' ,'Graceful'  ,'YES'                ,path()&'\conweb.ini')
      END
   OF CLRNT_ERR_INACTIVE
      MESSAGE('Client was inactive and closed automatically'      ,'ServiceBase 3g Webmaster',ICON:Exclamation)
      !Ungraceful - but controlled!
      IF loc:UseBalancer
        PUTINI ('balancer' ,'Server_IP'  ,Server_IP                ,path()&'\conweb.ini')
        PUTINI ('balancer' ,'Port_Used'  ,Port_Used              ,path()&'\conweb.ini')
        PUTINI ('balancer' ,'Graceful'  ,'YES'                ,path()&'\conweb.ini')
      END
   OF CLRNT_ERR_HTTP_TIMEOUT
      MESSAGE('HTTP Timeout error occurred'                       ,'ServiceBase 3g Webmaster',ICON:Exclamation)
      !Ungraceful - needs logging
      IF loc:UseBalancer
        PUTINI ('balancer' ,'Server_IP'  ,Server_IP                ,path()&'\conweb.ini')
        PUTINI ('balancer' ,'Port_Used'  ,Port_Used                ,path()&'\conweb.ini')
        PUTINI ('balancer' ,'Graceful'  ,'NO'                ,path()&'\conweb.ini')
      END
 ELSE
      MESSAGE('Unknown error occurred [ ' & LOC:ReturnVal & ' ]'  ,'ServiceBase 3g Webmaster',ICON:Exclamation)
      !Ungraceful - needs logging
      IF loc:UseBalancer
        PUTINI ('balancer' ,'Server_IP'  ,Server_IP                ,path()&'\conweb.ini')
        PUTINI ('balancer' ,'Port_Used'  ,Port_Used                ,path()&'\conweb.ini')
        PUTINI ('balancer' ,'Graceful'  ,'NO'                ,path()&'\conweb.ini')
      END
 END
! This used to shut down the program on return
! POST(EVENT:Accepted,?Exit)
 UNHIDE(0)                                                 ! REQUIRED
 !Reset local variables
! LogonUserPassword = ''
! select(?LogonUserPassword)
! DO LabelPrinting
 DISPLAY

!---
 EXIT
!------------------------------------------------------------------------------
Handle:Button:Exit      ROUTINE
!---
 POST(EVENT:CloseWindow)
!---
 EXIT
!------------------------------------------------------------------------------
Handle:Button:FileDialog  ROUTINE
!---
! FILEDIALOG('Select Directory for Downloaded Files', CLRNT_C.ClientTempDirectory,, 100001b)
! DISPLAY(?ClientTempDirectory)
!---
 EXIT
!------------------------------------------------------------------------------
Handle:Event:Timer  ROUTINE
!---
! POST(EVENT:Accepted,?Connect)
!---
 EXIT
!------------------------------------------------------------------------------

Handle:Window:Reset           ROUTINE
!---
! IF CLRNT_C.UseProxy
!    ENABLE (?ProxyGroup)
! ELSE
!    DISABLE(?ProxyGroup)
! END
 DO Handle:MakeFullProgramIDForDisplay
!---
 EXIT
!------------------------------------------------------------------------------

Handle:Set:Connect:Mode             ROUTINE
!---
!--- This sets to the ClarioNET server
!---
 IF LOC:ISAPI_Mode
    UNHIDE(?LOC:ISAPI_Broker_Ver)
    IF LOC:ISAPI_Broker_Ver = '5.5'
      CLRNT_C.HostProgramDirectory = 'cws/c5launch.dll'   !ISAPI      Version
    ELSIF LOC:ISAPI_Broker_Ver = '5'
      CLRNT_C.HostProgramDirectory = 'cws/cwlaunch.dll'   !ISAPI      Version
    END
 ELSE
    HIDE(?LOC:ISAPI_Broker_Ver)
    CLRNT_C.HostProgramDirectory = getini('global', 'hostdirectory',,path()&'\conweb.ini') !'PUBLIC/WEBINONE'               !EXE Broker Version
    !Modified to read from .ini file
 END
!---
 DISPLAY(?HostProgramDirectory)
 DISPLAY(?ServerPort          )
!---
 DO Handle:MakeFullProgramIDForDisplay
!---
 EXIT
!------------------------------------------------------------------------------
Handle:MakeFullProgramIDForDisplay          ROUTINE
!---
 FullProgramID = '/' & CLIP(CLRNT_C.HostProgramName) & '.0'
!---
 IF CLIP(CLRNT_C.HostProgramDirectory) <> ''
    FullProgramID = '/' & CLIP(CLRNT_C.HostProgramDirectory) & CLIP(FullProgramID)
 END
!---
 FullProgramID = 'http://' & CLIP(CLRNT_C.ServerName) & ':' & CLIP(CLRNT_C.ServerPort) & CLIP(FullProgramID)
!---
! DISPLAY(?FullProgramID)
!---
 EXIT
!------------------------------------------------------------------------------

AppFrame.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ClarioNET:ConnectWindow')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?StringBruce
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:Server.Open
  SELF.FilesOpened = True
  !x# = BindToCPU()
  
  tmp:LicenseText = 'SOFTWARE LICENCE AGREEMENT'
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>Copying, duplicating or otherwise distributing any part of this product without the'
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>prior written consent of an authorised representative of the above is prohibited. '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>This Software is licensed subject to the following conditions. '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>'
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>LICENCE AGREEMENT. This is a legal agreement between you (either an '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>individual or an entity) and the manufacturer of the computer software  PC '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>Control Systems Ltd..  '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>If you operate this SOFTWARE  you are agreeing to be bound by the terms of '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>this agreement. If you do not agree to the terms of this agreement, promptly '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>return the SOFTWARE and the accompanying items (including written materials '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>and binders or other containers) to the place you obtained them from. '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>Discretionary compensation may be paid dependent upon the time elapsed since '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>the purchase date of the SOFTWARE in all events any form of compensation will '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>be at the sole discretion of and expressly set by PC Control Systems Ltd..'
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>'
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>GRANT OF LICENCE.  This Licence Agreement permits you to use one copy '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>of the enclosed  software program ( the "SOFTWARE") on a single computer '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>if you purchased a single user version or on a network of computers if you '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>purchased a network user version. The SOFTWARE is in "use" on a computer '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>when it is loaded into temporary memory (i.e. RAM) or installed into permanent '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>memory (i.e. hard disk, CD-ROM, or other  storage device) of that computer.  '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>Network users are limited to the number of  simultaneous accesses to the '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>SOFTWARE  by the network. This is dependent upon the user version '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>purchased.  The user version is clearly stated on the original SOFTWARE '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>purchase invoice.  '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>'
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>COPYRIGHT:  The enclosed SOFTWARE and Documentation is protected by '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>copyright laws and international treaty provisions and are the proprietary '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>products of PC Control Systems Ltd. and its third party suppliers from whom '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>PC Control Systems Ltd. has licensed  portions of the Software.  Such suppliers '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>are expressly  understood  to be beneficiaries of the terms and provisions of '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>this  Agreement.  All rights that are not expressly  granted are reserved by '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>PC Control Systems Ltd. or its suppliers. You must treat the SOFTWARE like '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>any other copyrighted material (e.g. a book or musical recording) except that '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>you may either (a) make one copy of the SOFTWARE solely for backup '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>or archival purposes, or (b) transfer the SOFTWARE to a single hard disk '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>provided you keep the original solely for backup or archival purposes.  '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>You may not copy the written materials accompanying the SOFTWARE.'
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>'
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>DUEL MEDIA SOFTWARE . If the SOFTWARE  package contains both 3.5" '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>and 5.25" or 3.5" and CD ROM disks, then you may use only the disks '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>appropriate for your single-user computer or your file server.  You may not use '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>the other disks on any other computer or loan, rent, lease, or transfer them to '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>another user except as part of the permanent transfer (as provided above) of all '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>SOFTWARE and written materials.'
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>'
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>LIMITED WARRANTY.  PC Control Systems Ltd. warrants that (a) the '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>SOFTWARE will perform substantially in accordance with the accompanying '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>written materials for a period of ninety (90) days from the date of receipt, '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>and (b) any hardware accompanying the SOFTWARE will be free of defects in '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>materials and workmanship under normal use and service for a period of one '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>(1) year from the date of receipt. Any implied warranties on the '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>SOFTWARE and hardware are limited to ninety (90) days and one  '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>(1) year, respectively.  Some jurisdictions do not allow limitations '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>duration of an implied warranty, so the above limitation may not '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>apply to you.'
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>'
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>CUSTOMER REMEDIES.  PC Control Systems Ltd. and its suppliers entire '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>liability and your exclusive remedy shall be, at PC Control Systems Ltd.  '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>option, either (a) return of the price paid , or (b) repair or replacement of '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>the SOFTWARE that does not meet PC Control Systems Ltd. Warranty and which '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>is returned to PC Control Systems Ltd. with a copy of your SOFTWARE '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>purchase invoice.  This Limited Warranty is void if failure of the SOFTWARE '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>has resulted from accident, abuse, or misapplication.  Any replacement '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>SOFTWARE will be warranted for the remainder of the original warranty period '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>or thirty (30) days, whichever is the longer.  '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>'
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>NO OTHER WARRANTIES.  To the maximum extent permitted by applicable '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>law, PC Control Systems Ltd. and its suppliers disclaim all other warranties, '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>either  express or implied, including, but not limited to implied warranties of '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>merchantability and fitness for a particular purpose, with regard to the '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>SOFTWARE, the accompanying written materials, and any accompanying '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>hardware.  This limited warranty gives you specific legal rights, and you may '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>also have other rights which vary from jurisdiction to jurisdiction.'
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>'
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>NO LIABILITY FOR CONSEQUENTIAL DAMAGES. To the maximum extent permitted '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>by applicable law, in no event shall PC Control Systems Ltd.  or its suppliers '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>be liable for any damage  whatsoever ( including without  limitation, '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>damages for loss of business profits, business interruption, loss of business '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>information, or any other pecuniary loss)  arising  out of the use of or '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>inability to use this product,  even if PC Control Systems Ltd. has been advised  '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>of the possibility  of such damages.  Because some jurisdictions do not allow the '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>exclusion or limitation of liability for consequential or incidental damages, the '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>above limitation may not apply to you.'
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>'
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>SUPPORT.   PC Control Systems Ltd.  will attempt to answer  your technical '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>support requests concerning the SOFTWARE; however, this service is offered '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>on a reasonable efforts basis only, and PC Control Systems Ltd. may not be '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>able to resolve every support request.  PC Control Systems Ltd. supports '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>the Software only if it is used under conditions and on operating systems '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>for which the Software is designed. '
  OPEN(Window)
  SELF.Opened=True
  !DO Handle:Window:Init and Secret notes
  DO Handle:Window:Init
  
  !that was the meat of this section
  !if you want to reset this to connect to another source
  !you need to edit the following proceedure
  !Handle.Window.Init
  !   CLRNT_C.HostProgramName      = WEBALL.EXE
  !   CLRNT_C.ServerName           = 192.168.0.111
  !   CLRNT_C.ServerPort           = 100
  
  !   CLRNT_C.HostProgramDirectory = EXEC/WEBINONE
  
  if clrnt_C.ServerName = '' then
      Case Missive('This is an unconfigured version of the program.'&|
        '<13,10>Before you can use this program you must set-up the defaults you were sent by your provider.'&|
        '<13,10>Click the "Configure" button to do this.','ServiceBase 3g',|
                     'mstop.jpg','/OK') 
          Of 1 ! OK Button
      End ! Case Missive
  END
  Bryan.CompFieldColour()
  FileLookup5.Init
  FileLookup5.Flags=BOR(FileLookup5.Flags,FILE:LongName)
  FileLookup5.Flags=BOR(FileLookup5.Flags,FILE:Directory)
  FileLookup5.SetMask('All Files','*.*')
  FileLookup5.DefaultDirectory='c:\'
  FileLookup5.WindowTitle='File Save Directory'
  SELF.SetAlerts()
  RETURN ReturnValue


AppFrame.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Server.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


AppFrame.Reset PROCEDURE(BYTE Force=0)

  CODE
  SELF.ForcedReset += Force
  IF Window{Prop:AcceptAll} THEN RETURN.
  DO Handle:Window:Reset
  PARENT.Reset(Force)


AppFrame.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?NewJobButton
            !set up progname for job progress
      
            CLRNT_C.HostProgramName = 'wm30job.EXE'
            CLRNT_C.Param1 = ''   !LogonUserPassword
            !CLRNT_C.Param5 = 'V3.0('& AppFrame{PROP:XPOS}&','&AppFrame{prop:ypos}&')'
      
            !create new param 5
            !Format needed 'http://192.168.0.111:100/directory/directory/'
            CLRNT_C.Param5 = 'http://' & clip(CLRNT_C.ServerName) & ':' & clip(CLRNT_C.ServerPort)
            if CLRNT_C.HostProgramDirectory[1]='/' then
              Clrnt_C.Param5 = clip(CLRNT_C.PARAM5)&clip(CLRNT_C.HostProgramDirectory)
            ELSE
              Clrnt_C.Param5 = clip(CLRNT_C.PARAM5)&'/'&clip(CLRNT_C.HostProgramDirectory)
            END
            x#=len(clip(CLRNT_C.HostProgramDirectory))
            if CLRNT_C.HostProgramDirectory[x#] <> '/' then
              Clrnt_C.Param5 = clip(Clrnt_C.Param5)&'/'
            END
            !connect to server
            loop
              if exists('LABELS') then remove('LABELS').
      
              DO Handle:Button:Connect
              CheckForLabels
      
              !reset variables
              do read_ini
      
              !Now look to see if there is a clipboard entry to lauch another programme
              ClipboardString = clipboard()
              if clipboardString = '' then break.
              if clipboardString = 'BREAK' then break.
              x# =  instring('%',clipboardString)
              if x#=0 then break.
              y# = len(clip(ClipboardString))
              CLRNT_C.HostProgramName = clipboardstring[1:x#-1]
              CLRNT_C.Param1 = clipboardstring[x#+1:y#]
              !CLRNT_C.Param5 = 'V3.0('& AppFrame{PROP:XPOS}&','&AppFrame{prop:ypos}&')'
              setclipboard('wm30sys.exe %'&CLRNT_C.Param1)
           END !loop
      
      !In case version number changed
      SBVersion = 'Version: '&clip(getini ('global', 'SBVERSION','Unknown',path()&'\conweb.ini'))
    OF ?MonitorContacts
      !set up progname for monitor browsing
       CLRNT_C.HostProgramName = 'MONITOR.EXE'
       CLRNT_C.Param1 = LogonUserPassword
       CLRNT_C.Param5 = 'V3.0('& AppFrame{PROP:XPOS}&','&AppFrame{prop:ypos}&')'
      ! CLRNT_C.HostProgramDirectory = 'EXEC\MONITOR'
       !connect to server
       DO Handle:Button:Connect
    OF ?Config_Button
      !Check for supervisor password
          supervisor = false
          getpassword
          if supervisor then
              do handle:button:options
          else
              cycle
          end
      
    OF ?Company
       if company = 'DEBUGJ' then
          ?MonitorContacts{prop:hide}=false
       ELSE
          ?MonitorContacts{prop:hide}=true
       END
    OF ?EditTextButton
      Select(?Tab:Text)
    OF ?ServerSetup
      !Before going check if the main server has changed
      Access:Server.clearkey(SRV:KeyMainServerFlag)
      SRV:MainServerFlag = 'Y'
      if access:server.fetch(SRV:KeyMainServerFlag)
          !error
          !Create entry of existing record
          Case Missive('Please confirm that the entry on this screen is the main server.','ServiceBase 3g',|
                         'mquest.jpg','\Cancel|/Confirm') 
              Of 2 ! Confirm Button
                  access:Server.primerecord()
                  SRV:ProgramPath = CLRNT_C.HostProgramDirectory
                  SRV:IPAddrress  = CLRNT_C.ServerName
                  SRV:Port = CLRNT_C.ServerPort
                  SRV:MainServerFlag = 'Y'
                  SRV:Priority = 1
                  Access:Server.update()
              Of 1 ! Cancel Button
          End ! Case Missive
      ELSE
          !got the main server - is the one on display?
          ServerFail = false
          if CLRNT_C.HostProgramDirectory <> SRV:ProgramPath then ServerFail = true.
          if CLRNT_C.ServerName <> SRV:IPAddrress then ServerFail = true.
          if CLRNT_C.ServerPort <> SRV:Port then ServerFail = true.
          if serverfail = true
              !Create entry of existing record
              Case Missive('Please confirm if the entry on this screen is correct for the main server.','ServiceBase 3g',|
                             'mquest.jpg','\Cancel|/Confirm') 
                  Of 2 ! Confirm Button
                      SRV:ProgramPath = CLRNT_C.HostProgramDirectory
                      SRV:IPAddrress  = CLRNT_C.ServerName
                      SRV:Port = CLRNT_C.ServerPort
                      SRV:MainServerFlag = 'Y'
                      SRV:Priority = 1
                      Access:Server.update()
                  Of 1 ! Cancel Button
              End ! Case Missive
          END !if serverfail = true
      END
    OF ?OK_Buttton
      !Save all settings to ini file
       putini ('global' ,'hostprogram'   ,'WEBMASTER by PCCS'          ,path()&'\conweb.ini')
       putini ('global' ,'servername'    ,CLRNT_C.ServerName           ,path()&'\conweb.ini')
       putini ('global' ,'serverport'    ,CLRNT_C.ServerPort           ,path()&'\conweb.ini')
       putini ('global' ,'hostdirectory' ,CLRNT_C.HostProgramDirectory ,path()&'\conweb.ini')
       putini ('global' ,'licencenumber' ,LicenceNumber                ,path()&'\conweb.ini')
       putini ('global' ,'id'            ,LogonID                      ,path()&'\conweb.ini')
       putini ('global' ,'password'      ,LogonPassword                ,path()&'\conweb.ini')
       putini ('global' ,'company'       ,Company                      ,path()&'\conweb.ini')
       putini ('global' ,'graphics'      ,UseGraphics                  ,path()&'\conweb.ini')
       putini ('global' ,'UseAdvanced'   ,loc:UseBalancer              ,path()&'\conweb.ini')
       putini ('global' ,'DownLoadPath'  ,LocalSavePath                ,path()&'\conweb.ini')
       
!       !replace line feeds from text with ||
!       loop x#=1 to len(Clip(Receipt_Text))
!        if val(Receipt_Text[x#]) = 10 or val(Receipt_Text[x#]) = 13 then Receipt_Text[x#]='|'.
!       End !Loop
!       putini ('global' ,'Text'          ,Receipt_Text                 ,path()&'\conweb.ini')
!       !Restore line breaks
!       Loop x#=1 to len(clip(Receipt_Text))
!         if Receipt_Text[x#]   = '|' then
!            Receipt_Text[x#]   = chr(13)
!            Receipt_Text[x#+1] = chr(10)
!         End!If '|'
!       end !Loop
!      

      
       Select (?tab:Logo)
      
       CLRNT_C.Param1 = ''
       CLRNT_C.Param2 = LogonID&','&LogonPassword
       CLRNT_C.Param3 = LicenceNumber
       CLRNT_C.Param4 = UseGraphics

      
       Flash_Save
      
      
    OF ?TextOK_Button
      !reset command line variable
      CLRNT_C.Param4 = Receipt_Text
      !return to options
      Select(?Tab:Options)
      
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Exit
      AppFrame.Update
      DO Handle:Button:Exit
    OF ?LookupFile
      AppFrame.Update
      LocalSavePath = FileLookup5.Ask(1)
      DISPLAY
    OF ?Button16
      AppFrame.Update
      SetReportPrinters
      AppFrame.Reset
    OF ?LOC:ISAPI_Mode
      DO Handle:Set:Connect:Mode
    OF ?LOC:ISAPI_Broker_Ver
      DO Handle:Set:Connect:Mode
    OF ?HostProgramDirectory
      Self.Reset()
    OF ?ServerSetup
      AppFrame.Update
      ServerSetup
      AppFrame.Reset
      !After return check if the main server has changed
      Access:Server.clearkey(SRV:KeyMainServerFlag)
      SRV:MainServerFlag = 'Y'
      if access:server.fetch(SRV:KeyMainServerFlag)
          !error
          Case Missive('You have no main server set.'&|
            '<13,10>'&|
            '<13,10>This program will not operate correctly until you do so.','ServiceBase 3g',|
                         'mstop.jpg','/OK') 
              Of 1 ! OK Button
          End ! Case Missive
          CLRNT_C.HostProgramDirectory = ''
          CLRNT_C.ServerName = ''
          CLRNT_C.ServerPort = 0
      ELSE
          !got it
          CLRNT_C.HostProgramDirectory = SRV:ProgramPath
          CLRNT_C.ServerName = SRV:IPAddrress
          CLRNT_C.ServerPort = SRV:Port
      END
      
      !thiswindow.update()
      display()
    OF ?ServerPort
      Self.Reset()
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


AppFrame.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      OSstring = OSVersion()
       DateString  = 77961
       TimeString  = 5383808
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        IF ?tmp:LicenseText{prop:Feq} = DBHControl{prop:Feq}
            Cycle
        End ! IF ?tmp:LicenseText{prop:Use} = DBHControl{prop:Use}
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
