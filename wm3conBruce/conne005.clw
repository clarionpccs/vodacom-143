

   MEMBER('connect.clw')                              ! This is a MEMBER module

                     MAP
                       INCLUDE('CONNE005.INC'),ONCE        !Local module procedure declarations
                     END


SetBarCode           PROCEDURE  (BarCodeString)       ! Declare Procedure
DummyString          STRING(50)
BCweight             SHORT
BCascii              SHORT
BCvalue              SHORT
  CODE
!Set up the bar code replacing sent string.

!Short and quick method will produce too long a code for a long string
!That could be shortened (if mainly numeric) by using code set C

 DummyString = BarCodeString    !Copy of what was sent

!Now to rebuild sent string with the barcode
 BarCodeString = chr(171)       !used for start code set B (Specific to font set)
 BCvalue       = 104            !Count up total for check value starts with 104
!BCweight                       !Check weighting - Also used to step through dummy string

!at each character add number*weight to value
!increment character by one & copy to barcode
 loop BCweight = 1 to len(CLIP(DummyString))
    BCascii   = val(DummyString[BCweight])
    BCvalue  += BCweight * (BCascii-32)
    BarCodeString = Clip(BarcodeString) & chr(BCascii +1)
 End!Loop along string

 BCascii =  (BCvalue%103)       !Mod divison to give check digit

 if BCascii < 95 then           !Change to a printable character
       BCascii += 33            !these details are specific to the font set used
 ELSE !>95                      !C128 High 12pt LJ3
    BCascii += 67
 END  !<95

 BarCodeString = clip(BarCodeString) & chr(BCascii) & chr(173)
!CHR(173) is used for the STOP code (C128 value=106)
!Note: CHR(166)is used to swap to code set B    (99)
!May want that later if I want to develop this ...
