

   MEMBER('vodr0072.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('VODR0001.INC'),ONCE        !Local module procedure declarations
                     END


BouncerReport PROCEDURE                                    ! Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::6:TAGFLAG          BYTE(0)
DASBRW::6:TAGDISPSTATUS    BYTE(0)
DASBRW::6:QUEUE           QUEUE
Pointer2                      LIKE(glo:Pointer2)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
tmp:VersionNumber    STRING(30)                            !Version Number
Parameter_Group      GROUP,PRE()                           !-------------------------------------------------
LOC:DayLeawayCount   LONG                                  !
LOC:EndDate          DATE                                  !
LOC:StartDate        DATE                                  !
                     END                                   !
Local_Group          GROUP,PRE(LOC)                        !-------------------------------------------------
Account_Name         STRING(100)                           !
AccountNumber        STRING(15)                            !
ApplicationName      STRING('ServiceBase')                 !
CompanyName          STRING(30)                            !
CountJobs            LONG                                  !
CountBouncers        LONG                                  !
CountThirdParty      LONG                                  !
CountThirdPartyBouncers LONG                               !
Courier              STRING(30)                            !
DesktopPath          STRING(255)                           !
ExcelVisible         BYTE                                  !
FileName             STRING(255)                           !
JobNumber            LONG                                  !
JobsExtendedInSync   BYTE                                  !
MaxTabNumber         BYTE(2)                               !
OriginalIMEI         STRING(30)                            !Original IMEI Number
Path                 STRING(255)                           !
ProgramName          STRING(100)                           !
SectionName          STRING(100)                           !
TabNumber            BYTE(1)                               !
Text                 STRING(255)                           !
UserCode             STRING(3)                             !
UserName             STRING(100)                           !
OriginalDateOut      DATE                                  !Original Date Out
OriginalRecordNumber LONG                                  !Original Record Number
Version              STRING('3.1.0001')                    !
                     END                                   !
Misc_Group           GROUP,PRE()                           !-------------------------------------------------
debug:Active         LONG                                  !
debug:Count          LONG                                  !
DoAll                STRING(1)                             !
GUIMode              BYTE                                  !
LocalHeadAccount     STRING(30)                            !
LocalHeadAccountName STRING(30)                            !
LocalTag             STRING(1)                             !
LocalTimeOut         LONG                                  !
OPTION1              SHORT                                 !
Progress:Text        STRING(100)                           !
RecordCount          LONG                                  !
Result               BYTE                                  !
ScanThirdParty_Result BYTE                                 !
tmp:Checked          LONG                                  !Checked
tmp:Found            LONG                                  !Found
                     END                                   !
Excel_Group          GROUP,PRE()                           !-------------------------------------------------
Excel                SIGNED                                !OLE Automation holder
excel:ActiveWorkBook CSTRING(20)                           !
excel:ColumnName     STRING(50)                            !
excel:ColumnWidth    REAL                                  !
excel:CommentText    STRING(100)                           !
excel:DateFormat     STRING('dd mmm yyyy')                 !
excel:OperatingSystem REAL                                 !
                     END                                   !
Account_Group        GROUP,PRE()                           !-------------------------------------------------
AccountValue         STRING(15)                            !
AccountTag           STRING(1)                             !
AccountCount         LONG                                  !
AccountChange        BYTE                                  !
                     END                                   !
HeadAccount_Queue    QUEUE,PRE(haQ)                        !================================================
AccountNumber        STRING(15)                            !
AccountName          STRING(30)                            !
BranchIdentification STRING(2)                             !
ExcludeBouncer       LONG                                  !
JobsBooked           LONG                                  !
                     END                                   !
SubAccount_Queue     QUEUE,PRE(saQ)                        !================================================
AccountNumber        STRING(15)                            !
AccountName          STRING(30)                            !
HeadAccountNumber    STRING(15)                            !
HeadAccountName      STRING(30)                            !
BranchIdentification STRING(2)                             !
ExcludeBouncer       LONG                                  !
JobsBooked           LONG                                  !
                     END                                   !
RepairTypeCategoryQueue QUEUE,PRE(cq)                      !=================================================
RepairType           STRING(30)                            !Repair Type
Category             STRING(30)                            !Category
                     END                                   !
ResultsQueue         QUEUE,PRE(resQ)                       !=================================================
CompanyName          STRING(30)                            !
JobsSent             LONG                                  !
JobsBounced          LONG                                  !
                     END                                   !
ThirdPartyJobNumberQueue QUEUE,PRE(tjnq)                   !=================================================
CompanyName          STRING(30)                            !
JobNumber            LONG                                  !
                     END                                   !
Worksheet_Group      GROUP,PRE(sheet)                      !-------------------------------------------------
TempLastCol          STRING(1)                             !
HeadLastCol          STRING('E')                           !
HeadSummaryRow       LONG(16)                              !
DataLastCol          STRING('U')                           !
DataSectionRow       LONG(10)                              !
DataHeaderRow        LONG(12)                              !
                     END                                   !
Sheet_Queue          QUEUE,PRE(shQ)                        !===============================================
AccountName          STRING(30)                            !! 25 Feb 2003 John L577
SheetName            STRING(30)                            !
RecordCount          LONG                                  !
                     END                                   !
Clipboard_Group      GROUP,PRE(clip)                       !-------------------------------------------------
OriginalValue        STRING(255)                           !255
Saved                BYTE                                  !
Value                CSTRING(200)                          !
                     END                                   !
HeaderQueue          QUEUE,PRE(head)                       !=================================================
ColumnName           STRING(30)                            !
ColumnWidth          REAL                                  !
NumberFormat         STRING(20)                            !
                     END                                   !
IMEI_Queue           QUEUE,PRE(imeiQ)                      !=================================================
IMEI                 STRING(30)                            !Printed I.M.E.I. Number
                     END                                   !
Calc_Group           GROUP,PRE(calc)                       !----------------------------------------------
Franchise            LIKE(tra:Account_Number)              !
FranchiseName        LIKE(tra:Company_Name)                !
Manufacturer         LIKE(job:Manufacturer)                !
ModelNumber          STRING(30)                            !
JobNumber            LIKE(job:Ref_Number)                  !
CompletedDate        DATE                                  !
Engineer             STRING(65)                            !
InFault              LIKE(jbn:Fault_Description)           !
OutFault             LIKE(jbn:Fault_Description)           !
BouncerJobNumber     LIKE(job:Ref_Number)                  !
BouncerCompletedDate DATE                                  !
BouncerEngineer      STRING(65)                            ! ! 25 Feb 2003 John L577
BouncerInFault       LIKE(jbn:Fault_Description)           !
BouncerOutFault      LIKE(jbn:Fault_Description)           !
IMEINumber           STRING(30)                            !IMEI Number
BouncerIMEINumber    STRING(30)                            !Bouncer IMEI Number
                     END                                   !
ManufacturerQueue    QUEUE,PRE(manQ)                       !=================================================
Manufacturer         LIKE(job:Manufacturer)                !
InFaultFieldNumber   LONG                                  !
OutFaultFieldNumber  LONG                                  !
JobsRepaired         LONG                                  !
JobsBounced          LONG                                  !
                     END                                   !
ModelQueue           QUEUE,PRE(modQ)                       !=================================================
Manufacturer         STRING(30)                            !
ModelNumber          LIKE(job:Manufacturer)                !
JobsRepaired         LONG                                  !
JobsBounced          LONG                                  !
                     END                                   !
EntityQueue          QUEUE,PRE(entQ)                       !=================================================
Entity               STRING(30)                            !! 25 Feb 2003 John L577
JobsRepaired         LONG                                  !
JobsBounced          LONG                                  !
                     END                                   !
EngineerQueue        QUEUE,PRE(engQ)                       !=================================================
HeadAccount          STRING(15)                            !
Engineer             LIKE(job:Engineer)                    !
EngineerName         STRING(60)                            !
JobsRepaired         LONG                                  !
JobsBounced          LONG                                  !
                     END                                   !
ChosenAccount_Queue  QUEUE,PRE(caQ)                        !=================================================
AccountName          STRING(30)                            !! 25 Feb 2003 John L577
                     END                                   !
ManufacturerFaultLookupQueue QUEUE,PRE(LookupQ)            !===============================================
Manufacturer         STRING(30)                            !
FieldNumber          REAL                                  !
Field                STRING(30)                            !
Description          STRING(60)                            !
                     END                                   !
tmp:OutFaultCode     STRING(20)                            !
tmp:ChOutFaultCode   STRING(20)                            !
True_Bounce          BYTE                                  !
RC_Group             GROUP,PRE(rrc)                        !-----------------------------------------------------------------------------------------------
ARCLocation          STRING(30)                            !
DespatchToCustomer   STRING(30)                            !
SentToARC            STRING(30)                            !
RRCLocation          STRING(30)                            !
StatusReceivedAtRRC  STRING(30)                            !
                     END                                   !
engineer_temp        STRING(20)                            !
bounce_number        LONG                                  !
tmp:ExportFile       STRING(255),STATIC                    !Export File Name
Automatic            BYTE                                  !
BRW5::View:Browse    VIEW(TRADEACC)
                       PROJECT(tra:Account_Number)
                       PROJECT(tra:Company_Name)
                       PROJECT(tra:RecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
LocalTag               LIKE(LocalTag)                 !List box control field - type derived from local data
LocalTag_Icon          LONG                           !Entry's icon ID
tra:Account_Number     LIKE(tra:Account_Number)       !List box control field - type derived from field
tra:Company_Name       LIKE(tra:Company_Name)         !List box control field - type derived from field
tra:RecordNumber       LIKE(tra:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
MainWindow           WINDOW('Report'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),TRN,FLAT,FONT(,,,FONT:regular),ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Bounce Report Criteria'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,84,352,246),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Engineer Performance Report Criteria'),USE(?Tab1)
                           STRING('Job Completed Start Date'),AT(240,174),USE(?String4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@D6b),AT(345,174,64,10),USE(LOC:StartDate),IMM,FONT('TAHOMA',8,010101H,FONT:bold),COLOR(COLOR:White),TIP('Enter the youngest job complete date<13,10>to search on.'),REQ
                           STRING('Job Completed End Date'),AT(240,194,96,12),USE(?String5),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@D6b),AT(345,194,64,10),USE(LOC:EndDate),IMM,FONT('TAHOMA',8,010101H,FONT:bold),COLOR(COLOR:White),TIP('Enter the oldest job complete date<13,10>to search on.'),REQ
                           STRING('Leeway Days'),AT(240,214,96,12),USE(?String5:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           SPIN(@n3),AT(345,214,64,10),USE(LOC:DayLeawayCount),IMM,FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),TIP('Max number of days counting back <13,10>from the completed date<13,10>to look for previous' &|
   ' 3rd party repairs.'),RANGE(1,365),STEP(1)
                           CHECK('Show Excel'),AT(432,264,,12),USE(LOC:ExcelVisible),LEFT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),TIP('Tick To Show Excel During Report Creation.<13,10>Only Tick if you are experiencing pr' &|
   'oblems <13,10>with Excel reports.'),VALUE('1','0')
                           BUTTON,AT(413,170),USE(?Calendar),TRN,FLAT,ICON('lookupp.jpg')
                           BUTTON,AT(413,190),USE(?Calendar:2),TRN,FLAT,ICON('lookupp.jpg')
                         END
                         TAB('Repair Categories'),USE(?Tab2)
                         END
                         TAB('Tab 3'),USE(?Tab3)
                           LIST,AT(184,156,240,28),USE(?List),IMM,HIDE,VSCROLL,MSG('Browsing Records'),FORMAT('12L(2)|FJ@s1@60L(2)|F~Account Number~@s15@120L(2)|F~Company Name~@s30@'),FROM(Queue:Browse)
                           BUTTON('sho&W tags'),AT(208,184,70,13),USE(?DASSHOWTAG),DISABLE,HIDE
                           BUTTON('&Rev tags'),AT(208,188,50,13),USE(?DASREVTAG),DISABLE,HIDE
                           BUTTON,AT(220,188,56,16),USE(?DASTAG),FLAT,HIDE,LEFT
                           BUTTON,AT(280,188,56,16),USE(?DASTAGAll),FLAT,HIDE,LEFT
                           BUTTON,AT(340,188,56,16),USE(?DASUNTAGALL),FLAT,HIDE,LEFT
                           CHECK('All Repair Centres'),AT(196,200),USE(DoAll),HIDE,FONT(,,,,CHARSET:ANSI),VALUE('Y','N')
                         END
                       END
                       BUTTON,AT(380,332),USE(?OkButton),FLAT,LEFT,TIP('Click to print report'),ICON('printp.jpg'),DEFAULT
                       BUTTON,AT(448,332),USE(?CancelButton),FLAT,LEFT,TIP('Click to close this form'),ICON('cancelp.jpg')
                       PROMPT('Report Version'),AT(168,342),USE(?ReportVersion),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                     END

Bryan       Class
CompFieldColour       Procedure()
            End
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED                 ! Method added to host embed code
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass
BRW5                 CLASS(BrowseClass)                    ! Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Fetch                  PROCEDURE(BYTE Direction),DERIVED   ! Method added to host embed code
SetQueueRecord         PROCEDURE(),DERIVED                 ! Method added to host embed code
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeNewSelection       PROCEDURE(),DERIVED                 ! Method added to host embed code
ValidateRecord         PROCEDURE(),BYTE,DERIVED            ! Method added to host embed code
                     END

BRW5::Sort0:Locator  StepLocatorClass                      ! Default Locator
Calendar11           CalendarClass
Calendar12           CalendarClass
ExportFile    File,Driver('ASCII'),Pre(expfil),Name(tmp:ExportFile),Create,Bindable,Thread
Record                  Record
Line                    String(30)
                        End
                    End
    MAP
AppendString         PROCEDURE( STRING, STRING, STRING ),STRING
CheckFranchise          PROCEDURE( STRING ), LONG ! BOOL
CreateSummarySheet    PROCEDURE( STRING )
DateToString        PROCEDURE( DATE ), STRING
GetHeadAccount PROCEDURE( STRING )
GetManufacturerFaultLookupQueue          PROCEDURE( STRING, REAL, STRING ), STRING
GetSubAccount PROCEDURE( STRING )
IsBouncer       PROCEDURE(), LONG ! BOOL
LoadBOUNCER      PROCEDURE( LONG ), LONG ! BOOL
LoadJOBS      PROCEDURE( LONG ), LONG ! BOOL
LoadLOCATLOG   PROCEDURE( LONG, *LONG ), LONG ! BOOL
LoadJOBS_ALIAS      PROCEDURE( LONG ), LONG ! BOOL
LoadJOBSE      PROCEDURE( LONG ), LONG ! BOOL
LoadJOBNOTES      PROCEDURE( LONG ), LONG ! BOOL
LoadMANFAULO      PROCEDURE( STRING, REAL, STRING ), LONG ! BOOL
LoadSUBTRACC                            PROCEDURE( STRING ), LONG ! BOOL
LoadTRADEACC                    PROCEDURE( STRING ), LONG ! BOOL
LoadUSERS                         PROCEDURE( STRING ), LONG ! BOOL
PrintIMEI        PROCEDURE( STRING ), LONG ! BOOL

SetSheetTo              PROCEDURE( STRING )
NumberToColumn PROCEDURE( LONG ), STRING
UpdateResultsQueue          PROCEDURE( STRING, LONG, LONG )
UpdateManufacturerQueue          PROCEDURE( STRING, LONG, LONG )
UpdateModelQueue          PROCEDURE( STRING, STRING, LONG, LONG )
UpdateEntityQueue          PROCEDURE( STRING, LONG, LONG )
UpdateEngineerQueue          PROCEDURE( STRING, STRING, LONG, LONG )
WriteColumn PROCEDURE( STRING, LONG=False )
WriteDebug      PROCEDURE( STRING )
GetDeliveryDateAtARC        PROCEDURE( LONG ), LONG ! BOOL
    END !MAP
! ProgressWindow Declarations

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progress:thermometer byte
progresswindow      WINDOW('Progress...'),AT(,,207,64),CENTER,GRAY,IMM,FONT('Tahoma', |
                        8,,FONT:Regular),TIMER(1),DOUBLE
                        PROGRESS,AT(5,14,195,12),USE(progress:thermometer),RANGE(0,100),SMOOTH
                        STRING(''),AT(0,3,208,10),USE(?progress:userstring),TRN,CENTER
                        STRING(''),AT(0,30,208,10),USE(?progress:pcttext),TRN,CENTER
                        BUTTON('Cancel'),AT(75,44,56,16),USE(?ProgressCancel)
                    END

!---   Excel EQUATES   -------------------------------------------------------------
xlCalculationManual    EQUATE(0FFFFEFD9h) ! XlCalculation
xlCalculationAutomatic EQUATE(0FFFFEFF7h) ! XlCalculation
!----------------------------------------------------

!----------------------------------------------------
xlNone       EQUATE(0FFFFEFD2h)
xlContinuous EQUATE(        1 ) ! xlFillStyle
xlThin       EQUATE(        2 ) ! XlBorderWeight
!----------------------------------------------------

!----------------------------------------------------
xlAutomatic   EQUATE(0FFFFEFF7h) ! Constants.
xlSolid       EQUATE(        1 ) ! Constants.
xlLeft        EQUATE(0FFFFEFDDh) ! Constants.
xlRight       EQUATE(0FFFFEFC8h) ! Constants.
xlCenter      EQUATE(0FFFFEFF4h) ! Constants.
xlLastCell    EQUATE(       11 ) ! Constants.
xlTop         EQUATE(0FFFFEFC0h) ! Constants.
xlBottom      EQUATE(0FFFFEFF5h) ! Constants.

xlTopToBottom EQUATE(        1 ) ! Constants.
!----------------------------------------------------

!----------------------------------------------------
xlDiagonalDown     EQUATE( 5) ! XlBordersIndex
xlDiagonalUp       EQUATE( 6) ! XlBordersIndex
xlEdgeLeft         EQUATE( 7) ! XlBordersIndex
xlEdgeTop          EQUATE( 8) ! XlBordersIndex
xlEdgeBottom       EQUATE( 9) ! XlBordersIndex
xlEdgeRight        EQUATE(10) ! XlBordersIndex
xlInsideVertical   EQUATE(11) ! XlBordersIndex
xlInsideHorizontal EQUATE(12) ! XlBordersIndex
!----------------------------------------------------

!----------------------------------------------------
xlA1         EQUATE(        1 ) ! XlReferenceStyle
xlR1C1       EQUATE(0FFFFEFCAh) ! XlReferenceStyle
!----------------------------------------------------

!----------------------------------------------------
xlFilterCopy    EQUATE(2) ! XlFilterAction
xlFilterInPlace EQUATE(1) ! XlFilterAction
!----------------------------------------------------

!----------------------------------------------------
xlGuess EQUATE(0) ! XlYesNoGuess
xlYes   EQUATE(1) ! XlYesNoGuess
xlNo    EQUATE(2) ! XlYesNoGuess
!----------------------------------------------------

!----------------------------------------------------
xlAscending  EQUATE(1) ! XlSortOrder
xlDescending EQUATE(2) ! XlSortOrder
!----------------------------------------------------

!----------------------------------------------------
xlLandscape EQUATE(2) ! XlPageOrientation
xlPortrait  EQUATE(1) ! XlPageOrientation
!----------------------------------------------------

!----------------------------------------------------
xlDownThenOver EQUATE( 1 ) ! XlOrder
xlOverThenDown EQUATE( 2 ) ! XlOrder
!-----------------------------------------------------------------------------------
! Office EQUATES

!----------------------------------------------------
msoCTrue          EQUATE(        1 ) ! MsoTriState
msoFalse          EQUATE(        0 ) ! MsoTriState
msoTriStateMixed  EQUATE(0FFFFFFFEh) ! MsoTriState
msoTriStateToggle EQUATE(0FFFFFFFDh) ! MsoTriState
msoTrue           EQUATE(0FFFFFFFFh) ! MsoTriState
!----------------------------------------------------

!----------------------------------------------------
msoScaleFromBottomRight EQUATE(2) ! MsoScaleFrom
msoScaleFromMiddle      EQUATE(1) ! MsoScaleFrom
msoScaleFromTopLeft     EQUATE(0) ! MsoScaleFrom
!----------------------------------------------------

!----------------------------------------------------
msoPropertyTypeBoolean EQUATE(2) ! MsoDocProperties
msoPropertyTypeDate    EQUATE(3) ! MsoDocProperties
msoPropertyTypeFloat   EQUATE(5) ! MsoDocProperties
msoPropertyTypeNumber  EQUATE(1) ! MsoDocProperties
msoPropertyTypeString  EQUATE(4) ! MsoDocProperties
!----------------------------------------------------

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::6:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW5.UpdateBuffer
   glo:Queue2.Pointer2 = tra:Account_Number
   GET(glo:Queue2,glo:Queue2.Pointer2)
  IF ERRORCODE()
     glo:Queue2.Pointer2 = tra:Account_Number
     ADD(glo:Queue2,glo:Queue2.Pointer2)
    LocalTag = 'Y'
  ELSE
    DELETE(glo:Queue2)
    LocalTag = ''
  END
    Queue:Browse.LocalTag = LocalTag
  IF (LocalTag = 'Y')
    Queue:Browse.LocalTag_Icon = 2
  ELSE
    Queue:Browse.LocalTag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW5.Reset
  FREE(glo:Queue2)
  LOOP
    NEXT(BRW5::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue2.Pointer2 = tra:Account_Number
     ADD(glo:Queue2,glo:Queue2.Pointer2)
  END
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue2)
  BRW5.Reset
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::6:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue2)
    GET(glo:Queue2,QR#)
    DASBRW::6:QUEUE = glo:Queue2
    ADD(DASBRW::6:QUEUE)
  END
  FREE(glo:Queue2)
  BRW5.Reset
  LOOP
    NEXT(BRW5::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::6:QUEUE.Pointer2 = tra:Account_Number
     GET(DASBRW::6:QUEUE,DASBRW::6:QUEUE.Pointer2)
    IF ERRORCODE()
       glo:Queue2.Pointer2 = tra:Account_Number
       ADD(glo:Queue2,glo:Queue2.Pointer2)
    END
  END
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASSHOWTAG Routine
   CASE DASBRW::6:TAGDISPSTATUS
   OF 0
      DASBRW::6:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::6:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::6:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW5.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
!-----------------------------------------------
OKButton_Pressed                             ROUTINE
    DATA
    CODE
        DoAll = 'Y'
        IF DoAll = 'Y'
          DO DASBRW::6:DASTAGALL
        END
        rrc:ARCLocation         = GETINI('RRC',         'ARCLocation',     'RECEIVED AT ARC', '.\SB2KDEF.INI') ! [RRC]ARCLocation=RECEIVED AT ARC
        rrc:RRCLocation         = GETINI('RRC',         'RRCLocation',        'AT FRANCHISE', '.\SB2KDEF.INI') ! [RRC]RRCLocation=AT FRANCHISE
        !-----------------------------------------------------------------
        DO ExportSetup
        DO ExportBody
        DO ExportFinalize

        If Command('/SCHEDULE')
            If Access:REPSCHLG.PrimeRecord() = Level:Benign
                rlg:REPSCHEDRecordNumber = rpd:RecordNumber
                rlg:Information = 'Report Name: ' & Clip(rpd:ReportName) & ' - ' & Clip(rpd:ReportCriteriaType) & |
                                  '<13,10>Report Finished'
                If Access:REPSCHLG.TryInsert() = Level:Benign
                    !Insert
                Else ! If Access:REPSCHLG.TryInsert() = Level:Benign
                    Access:REPSCHLG.CancelAutoInc()
                End ! If Access:REPSCHLG.TryInsert() = Level:Benign
            End ! If Access.REPSCHLG.PrimeRecord() = Level:Benign
        End

        POST(Event:CloseWindow)
        !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------
ExportSetup                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        SETCURSOR(CURSOR:Wait)

        CancelPressed = False
        !debug:Active  = True

        IF    LOC:DayLeawayCount < 0
            LOC:DayLeawayCount = 1
        ELSIF LOC:DayLeawayCount > 365
            LOC:DayLeawayCount = 365
        END !IF
        !-----------------------------------------------
        ! 23 Oct 2002 John
        ! R003, berrjo :Insert the report exe name EG VODR56.V01
        ! Do not attach the EXE extenstion but show the version number.  Increment the version number each time changed.
        ! Make this standard on all reports sent for correction.
        ! LOC:Version STRING(8) 3.1.0000 3.1 to match sevicebase version (and dictionary) .0000 increment before each compile
        !
        !LOC:Version = '3.1.0001'
        !-----------------------------------------------------------------
        FREE(HeadAccount_Queue)
        CLEAR(HeadAccount_Queue)

        FREE(SubAccount_Queue)
        CLEAR(SubAccount_Queue)

        FREE(RepairTypeCategoryQueue)
        CLEAR(RepairTypeCategoryQueue)

        FREE(ResultsQueue)
        CLEAR(ResultsQueue)

        FREE(ThirdPartyJobNumberQueue)
        CLEAR(ThirdPartyJobNumberQueue)

        FREE(HeaderQueue)
        CLEAR(HeaderQueue)

        LOC:CountJobs               = 0
        LOC:CountBouncers           = 0
        LOC:CountThirdParty         = 0
        LOC:CountThirdPartyBouncers = 0
        tmp:Checked                 = 0
        tmp:Found                   = 0
        !-----------------------------------------------------------------
        IF CLIP(LOC:FileName) = ''
            DO LoadFileDialog
        END !IF

!        IF LOC:FileName = ''
!            Case MessageEx('No filename chosen.<10,13>   Enter a filename then try again',LOC:ApplicationName,|
!                           'Styles\warn.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0)
!                Of 1 ! &OK Button
!
!            End!Case MessageEx
!
!            SETCURSOR()
!
!            EXIT
!        END !IF LOC:FileName = ''
        !-----------------------------------------------------------------
        IF LOC:FileName <> '' THEN
           IF LOWER(RIGHT(LOC:FileName, 4)) <> '.xls'
              LOC:FileName = CLIP(LOC:FileName) & '.xls'
           END !IF
           !-----------------------------------------------------------------
           DO ProgressBar_Setup
           !-----------------------------------------------------------------
           DO XL_Setup

           DO XL_AddWorkbook
           !-----------------------------------------------------------------
        END !IF
        !-----------------------------------------------------------------
        DO InitColumns

        DO InitialiseChosenAccountQueue
        !-----------------------------------------------------------------
    EXIT
ExportBody                              ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        !WriteDebug('ExportBody')

        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        DO CreateTitleSheet

        DO CreateDataSheet
        !-----------------------------------------------------------------
    EXIT
ExportFinalize                          ROUTINE
    DATA
FilenameLength LONG
StartAt        LONG
SUBLength      LONG
    CODE
        !-----------------------------------------------------------------
        !WriteDebug('ExportFinalize')

        DO ResetClipboard ! Win98 Operations

        IF CancelPressed
            !WriteDebug('ExportFinalize(CancelPressed)')

            DO XL_DropAllWorksheets
            DO XL_Finalize

            EXIT
        END !IF
        !-----------------------------------------------------------------
        !
        Excel{'Sheets("Sheet3").Select'}
        IF Excel{'ActiveSheet.Name'} = 'Sheet3'
            Excel{'ActiveWindow.SelectedSheets.Delete'}
        END !IF

        Excel{'Sheets("Sheet1").Select'}
        IF Excel{'ActiveSheet.Name'} = 'Sheet1'
            Excel{'ActiveWindow.SelectedSheets.Delete'}
        END !IF

        Excel{'Sheets("Summary").Select'}
        Excel{'Range("A1").Select'}

        Excel{'Application.ActiveWorkBook.SaveAs("' & LEFT(CLIP(LOC:FileName)) & '")'}
        Excel{'Application.ActiveWorkBook.Close()'}
        !-----------------------------------------------------------------
        DO XL_Finalize

        DO ProgressBar_Finalise
        !-----------------------------------------------------------------
        SETCURSOR()

!        IF MATCH(LOC:Filename, CLIP(LOC:DesktopPath) & '*')
!            FilenameLength = LEN(CLIP(LOC:Filename   ))
!            StartAt        = LEN(CLIP(LOC:DesktopPath)) + 1
!            SUBLength      = FilenameLength - StartAt + 1
!
!            Case MessageEx('Export Completed.'                                & |
!                           '<13,10>'                                          & |
!                           '<13,10>Spreadsheet saved to your Desktop'         & |
!                           '<13,10>'                                          & |
!                           '<13,10>' & SUB(LOC:Filename, StartAt, SUBLength),   |
!                         LOC:ApplicationName,                                   |
!                         'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0)
!            Of 1 ! &OK Button
!            End!Case MessageEx
!        ELSE
!            Case MessageEx('Export Completed.'                        & |
!                           '<13,10>'                                  & |
!                           '<13,10>Spreadsheet saved to '             & |
!                           '<13,10>'                                  & |
!                           '<13,10>' & CLIP(LOC:Filename),              |
!                         LOC:ApplicationName,                           |
!                         'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0)
!            Of 1 ! &OK Button
!            End!Case MessageEx
!        END !IF
        !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------
InitialiseChosenAccountQueue             ROUTINE
    DATA
LocalTag      LIKE(Queue:Browse.LocalTag)
!AccountNumber LIKE(tra:Account_Number) ! 25 Feb 2003 John L577
AccountName   LIKE(tra:Company_Name) ! 25 Feb 2003 John L577
QueueIndex    LONG
QueueCount    LONG
    CODE
        !
        !-----------------------------------------------------------------
        ! Unable to use GET(Q, Key) on (glo:Queue2, glo:Queue2.Pointer2)
        !   usng new qtete
        !WriteDebug('InitialiseChosenAccountQueue()')
        !-----------------------------------------------------------------
        SetSheetTo('THIRD PARTY') ! 2 Apr 2003 John, Separate TAB for 3rd party to be inserted as shown below after Engineer Summary
        !-----------------------------------------------------------------
        QueueCount = RECORDS(glo:Queue2)
        !WriteDebug('QueueCount"' & QueueCount & '" = RECORDS(Queue:Browse))=' & RECORDS(Queue:Browse))
        LOOP QueueIndex = 1 TO QueueCount
            GET(glo:Queue2, QueueIndex)

            !LocalTag    = Queue:Browse.LocalTag#
            Access:TradeAcc.ClearKey(tra:Account_Number_Key)
            tra:Account_Number = glo:Queue2.GLO:Pointer2
            IF Access:TradeAcc.Fetch(tra:Account_Number_Key) <> Level:Benign
              !Error!
              CYCLE
            ELSE
              AccountName = tra:Company_Name! 25 Feb 2003 John L577
            END
            !WriteDebug('QueueIndex"' & (QueueIndex) & '", LocalTag"' & CLIP(LocalTag) & '", AccountName"' & CLIP(AccountName) & '", QueueCount"' & QueueCount & '"')

            !WriteDebug('InitialiseChosenAccountQueue(' & QueueIndex & '/' & QueueCount & ', "' & LocalTag & '", "' & CLIP(AccountNumber) & '")')

            !IF DoAll = 'Y' OR LocalTag = 'Y'
                !WriteDebug('InitialiseChosenAccountQueue(DoAll = "Y"(' & CLIP(DoAll) & ') OR LocalTag = "Y"(' & CLIP(LocalTag) & '))ADD')

                !-----------------------------------------------
                ! 25 Feb 2003 John L577
                ! Use the entity name not the branch code in the tabs ie AA20 must be changed to [Vodacom john] ARC
                CLEAR(ChosenAccount_Queue)
                    caQ:AccountName = AccountName          ! 25 Feb 2003 John L577
                ADD(ChosenAccount_Queue, +caQ:AccountName) ! 25 Feb 2003 John L577
                !-----------------------------------------------
                ! 20 Feb 2003 John L548
                SetSheetTo(caQ:AccountName)
!                DO XL_AddSheet
!
!                LOC:SectionName   = CLIP(caQ:AccountName) ! 25 Feb 2003 John L577
!                excel:CommentText = ''
!                DO CreateSectionHeader
!
!                DO SetColumns
!
!                CLEAR(Sheet_Queue)
!                    shQ:AccountName = caQ:AccountName ! 25 Feb 2003 John L577
!                    shQ:SheetName   = Excel{'ActiveSheet.Name'}
!                ADD(Sheet_Queue, +shQ:AccountName) ! 25 Feb 2003 John L577

                !WriteDebug('InitialiseChosenAccountQueue(DoADD "' & CLIP(shQ:AccountNumber) & '")"' & CLIP(shQ:SheetName) & '"')

           ! END !IF

        END !LOOP

        SORT(ChosenAccount_Queue, +caQ:AccountName)! 25 Feb 2003 John L577
        !-----------------------------------------------------------------
    EXIT
CreateTitleSheet                            ROUTINE
    DATA
CurentRow LONG
    CODE
        !-----------------------------------------------------------------
        !WriteDebug('CreateTitleSheet')

        sheet:HeadLastCol = 'D'

        CreateSummarySheet('Summary')

        Excel{'ActiveSheet.Columns("A:A").ColumnWidth'} = 22
        Excel{'ActiveSheet.Columns("B:E").ColumnWidth'} = 15
        !-----------------------------------------------------------------
    EXIT
CreateDataSheet                        ROUTINE
    DATA
Save_job_ID USHORT
QueueIndex  LONG
QueueCount  LONG
    CODE
        !Create Debug File
        !-----------------------------------------------------------------
        !WriteDebug('CreateDataSheet')

        IF CancelPressed
            EXIT
        END !IF
        !------------------------------------------
        Progress:Text    = 'Checking Completed Jobs'
        RecordsToProcess = (loc:EndDate - loc:StartDate + 1) * 1000 ! Lets try and make the progress bar work a little better

        DO ProgressBar_LoopPre
        !-----------------------------------------------------------------
        ! DateCompletedKey KEY( job:Date_Completed ),DUP,NOCASE
        !
        Save_job_ID = Access:JOBS.SaveFile()
            Access:JOBS.ClearKey(job:DateCompletedKey)
                job:Date_Completed = LOC:EndDate
            Set(job:DateCompletedKey, job:DateCompletedKey)

            Loop WHILE Access:JOBS.PREVIOUS() = Level:Benign
                !---------------------------------------------------------
                IF job:Date_Completed < LOC:StartDate
                    !WriteDebug('CreateDataSheet(IF job:Date_Completed < LOC:StartDate)[' & job:Ref_Number & ']')

                    BREAK
                End !If
                !---------------------------------------------------------
                DO ProgressBar_Loop

                IF CancelPressed
                    BREAK
                END !IF
                !---------------------------------------------------------
                DO WriteColumns
                !---------------------------------------------------------
            End !Loop
        Access:JOBS.RestoreFile(Save_job_ID)


        IF CancelPressed
            EXIT
        END !IF

        DO ProgressBar_LoopPost
        !-----------------------------------------------------------------
        QueueCount = RECORDS(Sheet_Queue)

        Progress:Text    = 'Checking Completed Jobs'
        RecordsToProcess = QueueCount

        DO ProgressBar_LoopPre

        LOOP QueueIndex = 1 TO QueueCount
                !---------------------------------------------------------
                RecordCount = QueueIndex

                DO ProgressBar_Loop

                IF CancelPressed
                    BREAK
                END !IF
                !---------------------------------------------------------
            GET(Sheet_Queue, QueueIndex)

            Excel{'Sheets("' & CLIP(shQ:SheetName) & '").Select'}
            RecordCount = shQ:RecordCount

            DO WriteDataSummary

        END !LOOP
        IF CancelPressed
            EXIT
        END !IF

        DO ProgressBar_LoopPost
        !-----------------------------------------------------------------
        DO WriteHeadSummary
        !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------
CreateSectionHeader                                        ROUTINE
    DATA
CurrentRow LONG
    CODE
        !-----------------------------------------------
        LOC:Text          = CLIP(LOC:SectionName)
        sheet:TempLastCol = sheet:DataLastCol

        DO CreateWorksheetHeader
        !-----------------------------------------------
        Excel{'ActiveSheet.PageSetup.PrintTitleRows'} = '$' & sheet:DataSectionRow & ':$' & sheet:DataSectionRow

        Excel{'Range("A' & sheet:DataSectionRow & ':' & sheet:TempLastCol & sheet:DataSectionRow &'").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A' & sheet:DataSectionRow & '").Select'}
            Excel{'ActiveCell.Formula'}         = 'Section Name:'
            DO XL_ColRight
                Excel{'ActiveCell.Formula'}     = CLIP(LOC:SectionName)
                DO XL_SetBold
                DO XL_HorizontalAlignmentLeft

        Excel{'Range("E' & sheet:DataSectionRow & '").Select'}
            Excel{'ActiveCell.Formula'}         = 'Total Records:'
            DO XL_HorizontalAlignmentRight
            DO XL_ColRight
                Excel{'ActiveCell.Formula'}     = 0
                DO XL_HorizontalAlignmentLeft
        !-----------------------------------------------
    EXIT
CreateWorksheetHeader                                                               ROUTINE
    DATA
CurrentRow LONG(3)
    CODE
        !-----------------------------------------------
        Excel{'ActiveSheet.Name'} = CLIP(LOC:Text)
        !-----------------------------------------------
        Excel{'Range("A1:' & sheet:TempLastCol & '1").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A1").Select'}
            Excel{'ActiveCell.Font.Size'}              = 14
            DO XL_SetBold
            Excel{'ActiveCell.Formula'}                = CLIP(LOC:ProgramName) & ' ' & CLIP(tra_ali:Account_Number)
        !-----------------------------------------------
        Excel{'Range("A' & CurrentRow & ':' & sheet:TempLastCol & '3").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A' & CurrentRow & '").Select'}
            Excel{'ActiveCell.Formula'}                = 'Criteria'
            DO XL_SetBold
        !-----------------------------------------------
        CurrentRow += 1
            Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.FormulaR1C1'}        = 'Job Completed Start Date'
                Excel{'Range("B' & CurrentRow & '").Select'}
                    DO XL_FormatDate
                    DO XL_HorizontalAlignmentLeft
                    Excel{'ActiveCell.Formula'}        = DateToString(LOC:StartDate)

        CurrentRow += 1
            Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.FormulaR1C1'}        = 'Job Completed End Date'
                Excel{'Range("B' & CurrentRow & '").Select'}
                    DO XL_FormatDate
                    DO XL_HorizontalAlignmentLeft
                    Excel{'ActiveCell.Formula'}        = DateToString(LOC:EndDate)

        CurrentRow += 1
            Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.FormulaR1C1'}        = 'Number of Days Leeway'
                Excel{'Range("B' & CurrentRow & '").Select'}
                    DO XL_HorizontalAlignmentLeft
                    Excel{'ActiveCell.Formula'}        = LOC:DayLeawayCount

        CurrentRow += 1
            Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.Formula'}            = 'Created By'
                Excel{'Range("B' & CurrentRow & '").Select'}
                    Excel{'ActiveCell.Formula'}        = CLIP(LOC:UserName)

        CurrentRow += 1
            Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.Formula'}            = 'Date Created'
                Excel{'Range("B' & CurrentRow & '").Select'}
                    DO XL_FormatDate
                    DO XL_HorizontalAlignmentLeft
                    Excel{'ActiveCell.Formula'}        = DateToString(TODAY())
        !-----------------------------------------------
        Excel{'Range("A4:' & CLIP(sheet:TempLastCol) & CurrentRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder
        !-----------------------------------------------
        sheet:DataSectionRow = CurrentRow + 2
        sheet:DataHeaderRow  = CurrentRow + 4

        sheet:HeadSummaryRow = CurrentRow + 2
        !-----------------------------------------------
    EXIT
!-----------------------------------------------
InitColumns                                                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !WriteDebug('InitColumns')

        FREE(HeaderQueue)

        head:ColumnName       = 'Franchise Name'
            head:ColumnWidth  = 22.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Manufacturer'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Part Number'
            head:ColumnWidth  = 25.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)
        !-----------------------------------------------
        head:ColumnName       = 'Job Number'
            head:ColumnWidth  = 10.00
            head:NumberFormat = '###0'
            ADD(HeaderQueue)

        ! Inserting (DBH 03/10/2008) # 10492 - New column
        head:ColumnName       = 'IMEI Number'
            head:ColumnWidth  = 20.00
            head:NumberFormat = '###0'
            ADD(HeaderQueue)
        ! End (DBH 03/10/2008) #10492

        head:ColumnName       = 'Bouncer Job Number'
            head:ColumnWidth  = 10.00
            head:NumberFormat = '###0'
            ADD(HeaderQueue)

        head:ColumnName       = 'Bouncer Number'
            head:ColumnWidth  = 20.00
            head:NumberFormat = '###0'
            ADD(HeaderQueue)

        head:ColumnName       = 'Despatch Date' !'Completed Date'
            head:ColumnWidth  = 12.75
            head:NumberFormat = excel:DateFormat
            ADD(HeaderQueue)

        head:ColumnName       = 'Bouncer Booking Date' !'Bouncer Completed Date'
            head:ColumnWidth  = 12.75
            head:NumberFormat = excel:DateFormat
            ADD(HeaderQueue)

        head:ColumnName       = 'Engineer'
            head:ColumnWidth  = 10.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Bouncer Engineer'
            head:ColumnWidth  = 10.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'In Fault'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Bouncer In Fault'
            head:ColumnWidth  = 20.00
            head:NumberFormat = '###0'
            ADD(HeaderQueue)

        head:ColumnName       = 'Out Fault'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Bouncer Out Fault'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        !-----------------------------------------------
        sheet:DataLastCol = NumberToColumn(RECORDS(HeaderQueue))
        !-----------------------------------------------
    EXIT
FormatColumns                                                   ROUTINE
    DATA
QueueIndex LONG
QueueCount LONG
    CODE
        !-----------------------------------------------
        IF RecordCount < 1
            EXIT
        END !IF
        !-----------------------------------------------
        QueueCount = RECORDS(HeaderQueue)
        LOOP QueueIndex = 1 TO QueueCount
            !-------------------------------------------
            GET(HeaderQueue, QueueIndex)
            IF ERRORCODE()
                CYCLE
            END !IF

            Excel{'Range("' & NumberToColumn(QueueIndex) & sheet:DataHeaderRow+1 & ':' & NumberToColumn(QueueIndex) & sheet:DataHeaderRow+RecordCount & '").Select'}
                Excel{'Selection.NumberFormat'} = head:NumberFormat
                !DO XL_HighLight ! DEBUG
                DO XL_HorizontalAlignmentRight
            !-------------------------------------------
        END !LOOP
        !-----------------------------------------------
        Excel{'Range("A' & sheet:DataHeaderRow+1 & ':A' & sheet:DataHeaderRow+RecordCount & '").Select'}
            Excel{'Selection.RowHeight'} = 12.75
        !-----------------------------------------------
    EXIT
SetColumns                                                     ROUTINE
    DATA
QueueIndex LONG
QueueCount LONG
    CODE
        !-----------------------------------------------
        !WriteDebug('SetColumns(' & Excel{'ActiveSheet.Name'} & ')')
        !-----------------------------------------------
        Excel{'Range("A' & sheet:DataHeaderRow & ':' & CLIP(sheet:DataLastCol) & sheet:DataHeaderRow & '").Select'}
            DO XL_SetColumnHeader
        !-----------------------------------------------
        Excel{'Range("A' & sheet:DataHeaderRow  & '").Select'}

        QueueCount = RECORDS(HeaderQueue)
        LOOP QueueIndex = 1 TO QueueCount
            GET(HeaderQueue, QueueIndex)

!        Excel{'ActiveCell.Offset(0, ' & QueueIndex - 1 & ').Interior.ColorIndex'}        = 6 ! YELLOW  ! DEBUG
!        Excel{'ActiveCell.Offset(0, ' & QueueIndex - 1 & ').Interior.Pattern'}           = xlSolid     ! DEBUG
!        Excel{'ActiveCell.Offset(0, ' & QueueIndex - 1 & ').Interior.PatternColorIndex'} = xlAutomatic ! DEBUG

            Excel{'ActiveCell.Offset(0, ' & QueueIndex - 1 & ').Formula'}     = CLIP(head:ColumnName)
            Excel{'ActiveCell.Offset(0, ' & QueueIndex - 1 & ').ColumnWidth'} = head:ColumnWidth
        END !LOOP
        !-----------------------------------------------
        Excel{'Range("A' & sheet:DataHeaderRow+1 & '").Select'}

        Excel{'ActiveWindow.FreezePanes'} = True
        !-----------------------------------------------
    EXIT
WriteColumns        ROUTINE                             !this is the bit with the TB13236 change
DATA
CurrentRow  LONG
FranchiseName       LIKE(calc:FranchiseName) ! 1 Apr 2003 John, Column Franchise Name to reflect Entity Summary
SheetName   LIKE(calc:FranchiseName) ! 2 Apr 2003 John, Separate TAB for 3rd party to be inserted as shown below after Engineer Summary
CODE
    !-----------------------------------------------
    !WriteDebug('WriteColumns(Count=' & RecordsProcessed & ')')
    !IF (tra_ali:Account_Number = LocalHeadAccount) THEN
    !                    LOC:RepairCentreType = 'ARC'

    Access:WebJob.ClearKey(wob:RefNumberKey)
    wob:RefNumber = job:ref_number
    IF Access:WebJob.Fetch(wob:RefNumberKey)
        !Error!
    END

    GetSubAccount(job:Account_Number)


    ! IF GetDeliveryDateAtARC(job:Ref_Number)
    !date returned means it went to the ARC!



    IF saQ:ExcludeBouncer = True
        ! 3 Feb 2003 John
        ! 3.3 Response to PCCS quieries.
        !   4. The new job is not a bouncer if it is booked under a particular account.
        !      (There is already a default on the Trade Account "Exclude from Bouncer Table".
        !       This will stop bouncer jobs under this account passing to the Bouncer table.
        !       Is this suffiecient?)
        !      JMK response is YES
        !
        !
        !WriteDebug('WriteColumns(IF saQ:ExcludeBouncer = True))')

        EXIT
    END !IF

    IF NOT CheckFranchise(saQ:HeadAccountName)! 25 Feb 2003 John L577
        !WriteDebug('WriteColumns(IF NOT CheckFranchise(saQ:HeadAccountNumber"' & CLIP(saQ:HeadAccountNumber) & '"))')

        EXIT
    END !IF
    !WriteDebug('saQ:HeadAccountNumber"' & CLIP(saQ:HeadAccountNumber) & '", saQ:HeadAccountName"' & CLIP(saQ:HeadAccountName) & '"')

    !-----------------------------------------------
    UpdateManufacturerQueue(                          job:Manufacturer, 1, 0)
    UpdateModelQueue(            job:Manufacturer,    job:Model_Number, 1, 0)
    !-----------------------------------------------
    ! 24 mar 2003 John
    ! 03/04/03 Neil

    !It is still valid!
    engineer_temp = ''
    IF LocalHeadAccountName = saQ:HeadAccountName
        !ARC!
        GetSubAccount(job:Account_Number)
    ELSE
        Access:Jobse.ClearKey(jobe:RefNumberKey)
        jobe:refnumber = job:ref_number
        IF Access:Jobse.Fetch(jobe:RefNumberKey)
            !Error
        END

        !TB13236 - J - 18/02/14 - if this has been exchanged at franchise then count as a franchise job not a headaccount job
        !adding "and jobe:ExchangedATRRC = 0" to next line
        !debugging - 02/04/14 - added brackets round jobe:ExchangedAtRRC = false
        IF (VodacomClass.JobSentToHub(job:Ref_Number)) and (jobe:ExchangedATRRC = false)
            !Arrived - so swap this to the headaccount tally
            saQ:HeadAccountName = LocalHeadAccountName
            !Trick it into thinking this is so!
            !Get Engineer!
            Access:JobsEng.ClearKey(joe:JobNumberKey)
            joe:JobNumber = job:ref_number
            SET(joe:JobNumberKey,joe:JobNumberKey)
            !jx# = 0
            LOOP
                IF Access:Jobseng.Next()
                    BREAK
                END
                IF joe:JobNumber <> job:ref_number
                    BREAK
                END
                Access:Users.ClearKey(use:User_Code_Key)
                use:User_Code = joe:UserCode
                IF Access:Users.Fetch(use:User_Code_Key)
                    !"Error!
                ELSE
                    Access:TradeAcc.ClearKey(tra:Account_Number_Key)
                    tra:Account_Number = LocalHeadAccount
                    IF Access:TradeAcc.Fetch(tra:Account_Number_Key)
                        !Error!
                    ELSE
                        IF use:location <> tra:SiteLocation
                            CYCLE
                        ELSE
                            engineer_temp = joe:UserCode
                            BREAK
                        END
                    END
                END
            END
        ELSE
            Access:Users.ClearKey(use:User_Code_Key)
            use:User_Code = job:engineer
            IF Access:Users.Fetch(use:User_Code_Key)
                !"Error!
            ELSE
                Access:TradeAcc.ClearKey(tra:Account_Number_Key)
                tra:Account_Number = saQ:HeadAccountNumber
                IF Access:TradeAcc.Fetch(tra:Account_Number_Key)
                    !Error!
                    !MESSAGE(saq:headaccountnumber)
                ELSE
                    IF use:location <> tra:SiteLocation
                        !Not engineer for here!
                        Access:JobsEng.ClearKey(joe:JobNumberKey)
                        joe:JobNumber = job:ref_number
                        SET(joe:JobNumberKey,joe:JobNumberKey)
                        LOOP
                            IF Access:Jobseng.Next()
                                BREAK
                            END
                            IF joe:JobNumber <> job:ref_number
                                BREAK
                            END
                            Access:Users.ClearKey(use:User_Code_Key)
                            use:User_Code = joe:UserCode
                            IF Access:Users.Fetch(use:User_Code_Key)
                                !"Error!
                            ELSE
                                Access:TradeAcc.ClearKey(tra:Account_Number_Key)
                                tra:Account_Number = saQ:HeadAccountNumber
                                IF Access:TradeAcc.Fetch(tra:Account_Number_Key)
                                    !Error!
                                ELSE
                                    IF use:location <> tra:SiteLocation
                                        CYCLE
                                    ELSE
                                        engineer_temp = joe:usercode !fudge for now
                                        BREAK
                                    END
                                END
                            END
                        END
                    END
                END
            END
        END

    END

!        if SAQ:HEADACCOUNTNAME = 'VODAWORLD'
!          IF JOB:ENGINEER = 'TD'
!            MESSAGE(job:ref_number)
!          END
!        END
    Access:JobThird.ClearKey(jot:RefNumberKey)
    jot:RefNumber = job:ref_number
    IF Access:JobThird.Fetch(jot:RefNumberKey) <> Level:Benign
        !Can't find it - therefore *not* Third Party
        !IF job:ThirdPartyDateDesp = 0 Removed by Neil
        UpdateEntityQueue(                            saQ:HeadAccountName, 1, 0) ! 24 Feb 2003 John L577
        Clear(expfil:Record)
        expfil:Line = Clip(job:Ref_Number) & ',' & Clip(saq:HeadAccountName) & ','
        Add(ExportFile)

        IF engineer_temp = ''
            UpdateEngineerQueue(     saQ:HeadAccountName,        job:Engineer, 1, 0) ! 24 Feb 2003 John L577
        ELSE
            UpdateEngineerQueue(     saQ:HeadAccountName,        CLIP(engineer_temp), 1, 0) ! 24 Feb 2003 John L577
        END
        FranchiseName = saQ:HeadAccountName ! 1 Apr 2003 John, Column Franchise Name to reflect Entity Summary
        SheetName     = saQ:HeadAccountName ! 2 Apr 2003 John, Separate TAB for 3rd party to be inserted as shown below after Engineer Summary

    ELSE
        UpdateEntityQueue(   'THIRD PARTY ' & CLIP(job:Manufacturer), 1, 0) ! 24 Feb 2003 John L577
        UpdateEngineerQueue( 'THIRD PARTY',         job:Manufacturer, 1, 0) ! 24 Feb 2003 John L577
        !   FranchiseName = 'THIRD PARTY ' & CLIP(job:Manufacturer) ! 1 Apr 2003 John, Column Franchise Name to reflect Entity Summary
        !   SheetName     = 'THIRD PARTY' ! 2 Apr 2003 John, Separate TAB for 3rd party to be inserted as shown below after Engineer Summary

    END !IF
    !-----------------------------------------------

    !-----------------------------------------------
    IF PrintIMEI(job:ESN) = False
        EXIT
    END !IF


    IF NOT IsBouncer()
        EXIT
    END !IF




!        IF (job_ali:Warranty_Job   = 'YES') AND (RepairTypeExclusion(job_ali:Repair_Type_Warranty) = True)
!            EXIT
!        END !IF
!
!        IF (job_ali:Chargeable_Job = 'YES') AND (RepairTypeExclusion(job_ali:Repair_Type) = True)
!            EXIT
!        END !IF
    !===============================================
    !Get Wob!
    Access:WebJob.ClearKey(wob:RefNumberKey)
    wob:RefNumber = job_ali:ref_number
    IF Access:WebJob.Fetch(wob:RefNumberKey)
        !Error!
    END

    GetSubAccount(job_ali:Account_Number)


    engineer_temp = ''
    IF LocalHeadAccountName = saQ:HeadAccountName
        !ARC!
        GetSubAccount(job_ali:Account_Number)
    ELSE
        Access:Jobse.ClearKey(jobe:RefNumberKey)
        jobe:refnumber = job_ali:ref_number
        IF Access:Jobse.Fetch(jobe:RefNumberKey)
            !Error
        END

        !TB13236 - J - 03/04/14 - if this has been exchanged at franchise then count as a franchise job not a headaccount job
        !adding "and jobe:ExchangedATRRC = 0" to next line
        !IF (VodacomClass.JobSentToHub(job:Ref_Number)) and (jobe:ExchangedATRRC = false)
        IF (VodacomClass.JobSentToHub(job_Ali:Ref_Number)) and (jobe:ExchangedATRRC = false)       !TB13338 J - 29/07/14 - this was previously job_ali:Refnumber
            !Arrived!
            saQ:HeadAccountName = LocalHeadAccountName
            !Trick it into thinking this is so!
            !Get Engineer!
            Access:JobsEng.ClearKey(joe:JobNumberKey)
            joe:JobNumber = job_ali:ref_number
            SET(joe:JobNumberKey,joe:JobNumberKey)
            !jx# = 0
            LOOP
                IF Access:Jobseng.Next()
                    BREAK
                END
                IF joe:JobNumber <> job_ali:ref_number
                    BREAK
                END
                Access:Users.ClearKey(use:User_Code_Key)
                use:User_Code = joe:UserCode
                IF Access:Users.Fetch(use:User_Code_Key)
                    !"Error!
                ELSE
                    Access:TradeAcc.ClearKey(tra:Account_Number_Key)
                    tra:Account_Number = LocalHeadAccount
                    IF Access:TradeAcc.Fetch(tra:Account_Number_Key)
                        !Error!
                    ELSE
                        IF use:location <> tra:SiteLocation
                            CYCLE
                        ELSE
                            engineer_temp = joe:UserCode
                            BREAK
                        END
                    END
                END
            END
        ELSE
            Access:Users.ClearKey(use:User_Code_Key)
            use:User_Code = job_ali:engineer
            IF Access:Users.Fetch(use:User_Code_Key)
                !"Error!
            ELSE
                Access:TradeAcc.ClearKey(tra:Account_Number_Key)
                tra:Account_Number = saQ:HeadAccountNumber
                IF Access:TradeAcc.Fetch(tra:Account_Number_Key)
                    !Error!
                    !MESSAGE(saq:headaccountnumber)
                ELSE
                    IF use:location <> tra:SiteLocation
                        !Not engineer for here!
                        Access:JobsEng.ClearKey(joe:JobNumberKey)
                        joe:JobNumber = job_ali:ref_number
                        SET(joe:JobNumberKey,joe:JobNumberKey)
                        LOOP
                            IF Access:Jobseng.Next()
                                BREAK
                            END
                            IF joe:JobNumber <> job_ali:ref_number
                                BREAK
                            END
                            Access:Users.ClearKey(use:User_Code_Key)
                            use:User_Code = joe:UserCode
                            IF Access:Users.Fetch(use:User_Code_Key)
                                !"Error!
                            ELSE
                                Access:TradeAcc.ClearKey(tra:Account_Number_Key)
                                tra:Account_Number = saQ:HeadAccountNumber
                                IF Access:TradeAcc.Fetch(tra:Account_Number_Key)
                                    !Error!
                                ELSE
                                    IF use:location <> tra:SiteLocation
                                        CYCLE
                                    ELSE
                                        engineer_temp = joe:usercode !fudge for now
                                        BREAK
                                    END
                                END
                            END
                        END
                    END
                END
            END
        END

    END




!        engineer_temp = ''
!        IF LocalHeadAccountName = saQ:HeadAccountName
!          !ARC!
!          GetSubAccount(job_ali:Account_Number)
!        ELSE
!          GetSubAccount(job_ali:Account_Number)
!          IF GetDeliveryDateAtARC(job_ali:Ref_Number)
!            !Arrived!
!            saQ:HeadAccountName = LocalHeadAccountName
!            !Trick it into thinking this is so!
!            !Get Engineer!
!            Access:JobsEng.ClearKey(joe:JobNumberKey)
!            joe:JobNumber = job_ali:ref_number
!            SET(joe:JobNumberKey,joe:JobNumberKey)
!            jx# = 0
!            LOOP
!              IF Access:Jobseng.Next()
!                BREAK
!              END
!              IF joe:JobNumber <> job_ali:ref_number
!                BREAK
!              END
!              jx# +=1
!              IF jx# = 2
!                engineer_temp = joe:UserCode
!              !  MESSAGE(Engineer_Temp)
!                BREAK
!              END
!            END
!
!          END
!        END


    CLEAR(Calc_Group)
    !-------------------------------------------
    calc:Franchise            = saQ:HeadAccountNumber
    calc:FranchiseName        = FranchiseName ! 1 Apr 2003 John, Column Franchise Name to reflect Entity Summary
    calc:Manufacturer         = job:Manufacturer
    calc:ModelNumber          = job:Model_Number
    !-------------------------------------------
    calc:JobNumber            = job:Ref_Number
    ! -----------------------------------------
    ! DBH 03/10/2008 #10492
    ! Inserting: New column
    calc:IMEINumber           = job:ESN
    ! End: DBH 03/10/2008 #10492
    ! -----------------------------------------
    IF jobe:webjob
        calc:CompletedDate = Job:Date_Booked !wob:DateJobDespatched
    ELSE
        calc:CompletedDate  = job:Date_Booked
    END
    calc:Engineer             = engQ:EngineerName ! 25 Feb 2003 John L577
    calc:InFault              = GetManufacturerFaultLookupQueue(job:Manufacturer,  manQ:InFaultFieldNumber, EVALUATE('job:Fault_Code' & manQ:InFaultFieldNumber))  ! jbn:Fault_Description
    ! Changing (DBH 15/05/2006) #6733 - Do not lookup the fault code, use the one of the job
    ! !Added By Neil - get correct outfault
    !             Calculate_Billing2(x",job:Repair_Type_Warranty,tmp:ChOutFaultCode,tmp:OutFaultCode,job:ref_number)
    !             !MESSAGE(tmp:OutFaultCode)
    !             IF tmp:OutFaultCode = ''
    !               tmp:OutFaultCode = tmp:ChOutFaultCode
    !             END
    !             calc:OutFault             = GetManufacturerFaultLookupQueue(job:Manufacturer, manQ:OutFaultFieldNumber,tmp:OutFaultCode)!EVALUATE('tmp:OutFaultCode' & manQ:OutFaultFieldNumber)) ! jbn:Engineers_Notes
    ! to (DBH 15/05/2006) #6733
    calc:OutFault = ''
    Access:MANFAULT.ClearKey(maf:MainFaultKey)
    maf:Manufacturer = job:Manufacturer
    maf:MainFault    = 1
    If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
        !Found
        Access:MANFAULO.ClearKey(mfo:Field_Key)
        mfo:Manufacturer = job:Manufacturer
        mfo:Field_Number = maf:Field_Number
        mfo:Field        = Evaluate('job:Fault_Code' & maf:Field_Number)
        If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
            !Found
            calc:OutFault = mfo:Description
        Else !If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
            !Error
        End !If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

    Else !If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
        !Error
    End !If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
    If Clip(calc:OutFault) = ''
        calc:OutFault = '*Undefined Fault - "' & CLIP(Evaluate('job:Fault_Code' & maf:Field_Number)) & '"'
    End ! If Clip(cal:OutFault) = ''
    ! End (DBH 15/05/2006) #6733

    !-------------------------------------------
    calc:BouncerJobNumber     = job_ali:Ref_Number
    ! -----------------------------------------
    ! DBH 03/10/2008 #10492
    ! Inserting: New column
    calc:BouncerIMEINumber    = job_ali:ESN
    ! End: DBH 03/10/2008 #10492
    ! -----------------------------------------
    IF jobe:webjob
        calc:BouncerCompletedDate = wob:DateJobDespatched
    ELSE
        calc:BouncerCompletedDate  = job_ali:Date_Despatched!
    END

    !calc:BouncerCompletedDate = job_ali:Date_Completed
    !calc:BouncerEngineer      = job_ali:Engineer ! 25 Feb 2003 John L577

    calc:BouncerInFault       = GetManufacturerFaultLookupQueue(job_ali:Manufacturer,  manQ:InFaultFieldNumber, EVALUATE('job_ali:Fault_Code' & manQ:InFaultFieldNumber))  ! jbn:Fault_Description
    !Calculate_Billing2(x",job_ali:Repair_Type_Warranty,tmp:ChOutFaultCode,tmp:OutFaultCode,job_ali:ref_number)
    ! Changing (DBH 15/05/2006) #6733 - Do not lookup the outfault from the repair type. Use the job value
    !             !MESSAGE(tmp:OutFaultCode)
    !             IF tmp:OutFaultCode = ''
    !               tmp:OutFaultCode = tmp:ChOutFaultCode
    !             END
    !             calc:BouncerOutFault      = GetManufacturerFaultLookupQueue(job_ali:Manufacturer, manQ:OutFaultFieldNumber,tmp:OutFaultCode)!, EVALUATE('tmp:OutFaultCode' & manQ:OutFaultFieldNumber)) ! jbn:Engineers_Notes
    ! to (DBH 15/05/2006) #6733
    calc:BouncerOutFault = ''
    Access:MANFAULT.ClearKey(maf:MainFaultKey)
    maf:Manufacturer = job_ali:Manufacturer
    maf:MainFault    = 1
    If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
        !Found
        Access:MANFAULO.ClearKey(mfo:Field_Key)
        mfo:Manufacturer = job:Manufacturer
        mfo:Field_Number = maf:Field_Number
        mfo:Field = Evaluate('job_ali:Fault_Code' & maf:Field_Number)
        If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
            !Found
            calc:BouncerOutFault = mfo:Description
        Else !If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
            !Error
        End !If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

    Else !If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
        !Error
    End !If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
    If Clip(calc:BouncerOutFault) = ''
        calc:BouncerOutFault = '*Undefined Fault - "' & CLIP(Evaluate('job_ali:Fault_Code' & maf:Field_Number)) & '"'
    End ! If Clip(cal:OutFault) = ''
    ! End (DBH 15/05/2006) #6733

    !-----------------------------------------------
    ! 25 Feb 2003 John L577
    ! Use the entity name not the branch code in the tabs ie AA20 must be changed to [Vodacom john] ARC
    !SetSheetTo(CLIP(saQ:HeadAccountName))
    Access:JobThird.ClearKey(jot:RefNumberKey)
    jot:RefNumber = job_ali:ref_number
    IF Access:JobThird.Fetch(jot:RefNumberKey) <> Level:Benign
        UpdateEntityQueue(                             saQ:HeadAccountName, 0, 1) ! 24 Feb 2003 John L577
        IF engineer_temp = ''
            UpdateEngineerQueue(     saQ:HeadAccountName,        job_ali:Engineer, 0, 1) ! 24 Feb 2003 John L577
        ELSE
            UpdateEngineerQueue(     saQ:HeadAccountName,        CLIP(Engineer_Temp), 0, 1) ! 24 Feb 2003 John L577
        END
        Calc:FranchiseName = saQ:HeadAccountName ! 1 Apr 2003 John, Column Franchise Name to reflect Entity Summary
        SheetName     = saQ:HeadAccountName ! 2 Apr 2003 John, Separate TAB for 3rd party to be inserted as shown below after Engineer Summary            calc:FranchiseName = saQ:HeadAccountName ! 1 Apr 2003 John, Column Franchise Name to reflect Entity Summary
    ELSE
        UpdateEntityQueue(   'THIRD PARTY ' & CLIP(job_ali:Manufacturer), 0, 1) ! 24 Feb 2003 John L577
        UpdateEngineerQueue( 'THIRD PARTY',         job_ali:Manufacturer, 0, 1) ! 24 Feb 2003 John L577

        Calc:FranchiseName = 'THIRD PARTY ' & CLIP(job_ali:Manufacturer) ! 1 Apr 2003 John, Column Franchise Name to reflect Entity Summary
        SheetName     = 'THIRD PARTY' ! 2 Apr 2003 John, Separate TAB for 3rd party to be inserted as shown below after Engineer Summary
!
!            calc:FranchiseName = 'THIRD PARTY ' & CLIP(job_ali:Manufacturer) ! 1 Apr 2003 John, Column Franchise Name to reflect Entity Summary
    END !IF

    SetSheetTo(SheetName) ! 2 Apr 2003 John, Separate TAB for 3rd party to be inserted as shown below after Engineer Summary
    !-----------------------------------------------
    shQ:RecordCount += 1
    PUT(Sheet_Queue)

    RecordCount   += 1
    LOC:CountJobs += 1
    !-----------------------------------------------
    UpdateManufacturerQueue(                        job_ali:Manufacturer, 0, 1)
    UpdateModelQueue(         job_ali:Manufacturer, job_ali:Model_Number, 0, 1)
    !-----------------------------------------------
    ! 24 mar 2003 John

    !-----------------------------------------------


    calc:BouncerEngineer = engQ:EngineerName ! 24 Feb 2003 John L577
    !-----------------------------------------------
    CurrentRow = Excel{'ActiveCell.Row'}

    Excel{'Range(A' & CurrentRow & ':' & sheet:DataLastCol & CurrentRow & ').Select'}
    DO XL_HorizontalAlignmentRight

    Excel{'Range(A' & CurrentRow & ').Select'}
    !-----------------------------------------------
    WriteColumn( calc:FranchiseName, True ) ! Franchise Name
    WriteColumn( calc:Manufacturer        ) ! Manufacturer
    WriteColumn( calc:ModelNumber         ) ! Model Number
    !-------------------------------------------
    WriteColumn( calc:BouncerJobNumber                   ) ! Bouncer Job Number
    WriteColumn( calc:BouncerIMEINumber                  ) ! Bouncer IMEI
    WriteColumn( calc:JobNumber                   ) ! Job Number
    WriteColumn( calc:IMEINumber                  ) ! IMEI
    WriteColumn( DateToString(calc:BouncerCompletedDate) ) ! Bouncer Completed Date
    WriteColumn( DateToString(calc:CompletedDate) ) ! Completed Date
    WriteColumn( calc:BouncerEngineer                    ) ! Bouncer Engineer
    WriteColumn( calc:Engineer                    ) ! Engineer
    WriteColumn( calc:BouncerInFault                     ) ! Bouncer In Fault
    WriteColumn( calc:InFault                     ) ! In Fault
    WriteColumn( calc:BouncerOutFault                    ) ! Bouncer Out Fault
    WriteColumn( calc:OutFault                    ) ! Out Fault
    !-------------------------------------------
    !-----------------------------------------------
    IF excel:OperatingSystem < 5
        DO PassViaClipboard
    END !IF
    !-----------------------------------------------
    DO XL_ColFirst
    DO XL_RowDown
    !-----------------------------------------------
    EXIT
!-----------------------------------------------
WriteDataSummary                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        IF RecordCount = 0
            Excel{'ActiveCell.Formula'} = 'No Jobs Found'

            EXIT
        END !IF

        DO FormatColumns
        !-----------------------------------------------------------------
        ! summary details
        !
        Excel{'Range("F' & sheet:DataSectionRow & '").Select'}
        DO XL_HorizontalAlignmentLeft
        Excel{'ActiveCell.Formula'}             = RecordCount

        Excel{'Range("G' & sheet:DataSectionRow & '").Select'}
        DO XL_HorizontalAlignmentRight
        Excel{'ActiveCell.Formula'}             = 'Showing'

        Excel{'Range("H' & sheet:DataSectionRow & '").Select'}
        DO XL_HorizontalAlignmentLeft
        Excel{'ActiveCell.Formula'}             = '=SUBTOTAL(2, D' & sheet:DataHeaderRow+1 & ':D' & sheet:DataHeaderRow+RecordCount & ')'
        !-----------------------------------------------------------------
        Excel{'Range("A' & sheet:DataHeaderRow+1 & ':A' & sheet:DataHeaderRow+RecordCount & '").Select'}
            Excel{'Selection.RowHeight'} = 12.75

        Excel{'Range("A' & sheet:DataHeaderRow & '").Select'}
            DO XL_DataAutoFilter
        !-----------------------------------------------------------------
    EXIT
WriteHeadSummary                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        DO WriteManufacturerSummary
        DO WriteModelSummary
        DO WriteEntitySummary
        DO WriteEngineerSummary
        !-----------------------------------------------------------------
    EXIT
WriteManufacturerSummary                             ROUTINE
    DATA
QueueIndex   LONG
QueueCount LONG

TitleRow LONG
FirstRow LONG
LastRow  LONG
TotalRow LONG
    CODE
        !-----------------------------------------------------------------
        ! summary sheet details
        !
        Excel{'Sheets("Summary").Select'}

        Excel{'ActiveSheet.Columns("A:A").ColumnWidth'} = 22
        Excel{'ActiveSheet.Columns("B:E").ColumnWidth'} = 15

        sheet:HeadLastCol = 'D'
        !-----------------------------------------------------------------
        QueueCount = RECORDS(ManufacturerQueue)

        TitleRow = Excel{'ActiveCell.Row'}
        FirstRow = TitleRow + 1
        LastRow  = TitleRow + QueueCount
        TotalRow = LastRow  + 1
        !-----------------------------------------------------------------
        Progress:Text    = 'Writing Manufacturer Summary'
        RecordsToProcess = QueueCount

        DO ProgressBar_LoopPre
        !-----------------------------------------------------------------
        Excel{'Range("A' & TitleRow & '").Select'}
            Excel{'ActiveCell.Offset(0, 0).Formula'} = 'Manufacturer'
            Excel{'ActiveCell.Offset(0, 1).Formula'} = 'Repaired'
            Excel{'ActiveCell.Offset(0, 2).Formula'} = 'Bounced'
            Excel{'ActiveCell.Offset(0, 3).Formula'} = 'Percentage'

        Excel{'Range("A' & TitleRow & ':' & CLIP(sheet:HeadLastCol) & TitleRow & '").Select'}
            DO XL_SetColumnHeader

        Excel{'Range("A' & FirstRow & '").Select'}
            IF QueueCount = 0
                Excel{'ActiveCell.Offset(0, 0).Formula'} = 'No Data Found'
                Excel{'ActiveCell.Offset(2, 0).Select'}

                EXIT
            END !IF
        !-----------------------------------------------------------------
        SORT(ManufacturerQueue, +manQ:Manufacturer)
        LOOP QueueIndex = 1 TO QueueCount
            !-------------------------------------------------------------
            RecordCount += 1

            DO ProgressBar_Loop

            IF CancelPressed
                BREAK
            END !IF
            !-------------------------------------------------------------
            GET(ManufacturerQueue, QueueIndex)
            Excel{'ActiveCell.Formula'}                    = manQ:Manufacturer
            Excel{'ActiveCell.Offset(0,  1).Formula'}      = manQ:JobsRepaired
            Excel{'ActiveCell.Offset(0,  2).Formula'}      = manQ:JobsBounced
            Excel{'ActiveCell.Offset(0,  3).NumberFormat'} = '0.00%'
            Excel{'ActiveCell.Offset(0,  3).FormulaR1C1'}  = '=IF(rc[-2]=0, 0, rc[-1]/rc[-2])'

            DO XL_RowDown
        END !LOOP

        DO ProgressBar_LoopPost
        !-----------------------------------------------------------------
        ! Totals
        !
        Excel{'Range("A' & TotalRow & ':' & CLIP(sheet:HeadLastCol) & TotalRow & '").Select'}
            DO XL_SetColumnHeader

        Excel{'Range("A' & TotalRow & '").Select'}
            Excel{'ActiveCell.Offset(0, 0).Formula'}     = 'Total'
            Excel{'ActiveCell.Offset(0, 1).Formula'}     = '=SUBTOTAL(9,B' & FirstRow & ':B' & LastRow & ')'
            Excel{'ActiveCell.Offset(0, 2).Formula'}     = '=SUBTOTAL(9,C' & FirstRow & ':C' & LastRow & ')'

            Excel{'ActiveCell.Offset(0, 3).NumberFormat'} = '0.00%'
            Excel{'ActiveCell.Offset(0, 3).FormulaR1C1'} = '=IF(rc[-2]=0, 0, rc[-1]/rc[-2])'
        !-----------------------------------------------------------------

        Excel{'Range("A' & TotalRow+2 & '").Select'}
        !-----------------------------------------------------------------
    EXIT
WriteModelSummary                             ROUTINE
    DATA
QueueIndex   LONG
QueueCount LONG

TitleRow LONG
FirstRow LONG
LastRow  LONG
TotalRow LONG
    CODE
        !-----------------------------------------------------------------
        sheet:HeadLastCol = 'E'
        CreateSummarySheet('Model Summary')

        Excel{'ActiveSheet.Columns("A:B").ColumnWidth'} = 22
        Excel{'ActiveSheet.Columns("C:E").ColumnWidth'} = 15
        !-----------------------------------------------------------------
        QueueCount = RECORDS(ModelQueue)

        TitleRow = Excel{'ActiveCell.Row'}
        FirstRow = TitleRow + 1
        LastRow  = TitleRow + QueueCount
        TotalRow = LastRow  + 1
        !-----------------------------------------------------------------
        Progress:Text    = 'Writing Model Summary'
        RecordsToProcess = QueueCount

        DO ProgressBar_LoopPre
        !-----------------------------------------------------------------
        Excel{'Range("A' & TitleRow & '").Select'}
            Excel{'ActiveCell.Offset(0, 0).Formula'} = 'Manufacturer'
            Excel{'ActiveCell.Offset(0, 1).Formula'} = 'Model Number'
            Excel{'ActiveCell.Offset(0, 2).Formula'} = 'Repaired'
            Excel{'ActiveCell.Offset(0, 3).Formula'} = 'Bounced'
            Excel{'ActiveCell.Offset(0, 4).Formula'} = 'Percentage'

        Excel{'Range("A' & TitleRow & ':' & CLIP(sheet:HeadLastCol) & TitleRow & '").Select'}
            DO XL_SetColumnHeader

        Excel{'Range("A' & FirstRow & '").Select'}
            IF QueueCount = 0
                Excel{'ActiveCell.Offset(0, 0).Formula'} = 'No Data Found'
                Excel{'ActiveCell.Offset(2, 0).Select'}

                EXIT
            END !IF
        !-----------------------------------------------------------------
        SORT(ModelQueue, +modQ:Manufacturer, +modQ:ModelNumber)
        LOOP QueueIndex = 1 TO QueueCount
            !-------------------------------------------------------------
            RecordCount += 1

            DO ProgressBar_Loop

            IF CancelPressed
                BREAK
            END !IF
            !-------------------------------------------------------------
            GET(ModelQueue, QueueIndex)
            Excel{'ActiveCell.Offset(0,  0).Formula'}      = modQ:Manufacturer
            Excel{'ActiveCell.Offset(0,  1).Formula'}      = modQ:ModelNumber
            Excel{'ActiveCell.Offset(0,  2).Formula'}      = modQ:JobsRepaired
            Excel{'ActiveCell.Offset(0,  3).Formula'}      = modQ:JobsBounced

            Excel{'ActiveCell.Offset(0,  4).NumberFormat'} = '0.00%'
            Excel{'ActiveCell.Offset(0,  4).FormulaR1C1'}  = '=IF(rc[-2]=0, 0, rc[-1]/rc[-2])'

            DO XL_RowDown
        END !LOOP

        DO ProgressBar_LoopPost
        !-----------------------------------------------------------------
        ! Totals
        !
        Excel{'Range("A' & TotalRow & ':' & CLIP(sheet:HeadLastCol) & TotalRow & '").Select'}
            DO XL_SetColumnHeader

        Excel{'Range("A' & TotalRow & '").Select'}
            Excel{'ActiveCell.Offset(0, 0).Formula'}     = 'Total'
            Excel{'ActiveCell.Offset(0, 2).Formula'}     = '=SUBTOTAL(9,C' & FirstRow & ':C' & LastRow & ')'
            Excel{'ActiveCell.Offset(0, 3).Formula'}     = '=SUBTOTAL(9,D' & FirstRow & ':D' & LastRow & ')'

            Excel{'ActiveCell.Offset(0, 4).NumberFormat'} = '0.00%'
            Excel{'ActiveCell.Offset(0, 4).FormulaR1C1'} = '=IF(rc[-2]=0, 0, rc[-1]/rc[-2])'
        !-----------------------------------------------------------------
        Excel{'Range("A' & TitleRow & ':' & CLIP(sheet:HeadLastCol) & LastRow & '").Select'}
                        DO XL_DataAutoFilter

        Excel{'Range("A' & TotalRow+2 & '").Select'}
        !-----------------------------------------------------------------
    EXIT
WriteEntitySummary                             ROUTINE
    DATA
QueueIndex   LONG
QueueCount LONG

TitleRow LONG
FirstRow LONG
LastRow  LONG
TotalRow LONG
    CODE
        !-----------------------------------------------------------------
        sheet:HeadLastCol = 'D'
        Excel{'Sheets("Summary").Select'}

        !Excel{'ActiveSheet.Columns("A:A").ColumnWidth'} = 22
        !Excel{'ActiveSheet.Columns("B:D").ColumnWidth'} = 15
        !-----------------------------------------------------------------
        QueueCount = RECORDS(EntityQueue)

        TitleRow = Excel{'ActiveCell.Row'}
        FirstRow = TitleRow + 1
        LastRow  = TitleRow + QueueCount
        TotalRow = LastRow  + 1
        !-----------------------------------------------------------------
        Progress:Text    = 'Writing Entity Summary'
        RecordsToProcess = QueueCount

        DO ProgressBar_LoopPre
        !-----------------------------------------------------------------
        Excel{'Range("A' & TitleRow & '").Select'}
            Excel{'ActiveCell.Offset(0, 0).Formula'} = 'Entity'
            Excel{'ActiveCell.Offset(0, 1).Formula'} = 'Repaired'
            Excel{'ActiveCell.Offset(0, 2).Formula'} = 'Bounced'
            Excel{'ActiveCell.Offset(0, 3).Formula'} = 'Percentage'

        Excel{'Range("A' & TitleRow & ':' & CLIP(sheet:HeadLastCol) & TitleRow & '").Select'}
            DO XL_SetColumnHeader

        Excel{'Range("A' & FirstRow & '").Select'}
            IF QueueCount = 0
                Excel{'ActiveCell.Offset(0, 0).Formula'} = 'No Data Found'
                Excel{'ActiveCell.Offset(2, 0).Select'}

                EXIT
            END !IF
        !-----------------------------------------------------------------
        SORT(EntityQueue, +entQ:Entity)
        LOOP QueueIndex = 1 TO QueueCount
            !-------------------------------------------------------------
            RecordCount += 1

            DO ProgressBar_Loop

            IF CancelPressed
                BREAK
            END !IF
            !-------------------------------------------------------------
            GET(EntityQueue, QueueIndex)
            Excel{'ActiveCell.Offset(0,  0).Formula'}      = entQ:Entity ! 25 Feb 2003 John L577
            Excel{'ActiveCell.Offset(0,  1).Formula'}      = entQ:JobsRepaired
            Excel{'ActiveCell.Offset(0,  2).Formula'}      = entQ:JobsBounced
            Excel{'ActiveCell.Offset(0,  3).NumberFormat'} = '0.00%'
            Excel{'ActiveCell.Offset(0,  3).FormulaR1C1'}  = '=IF(rc[-2]=0, 0, rc[-1]/rc[-2])'

            DO XL_RowDown
        END !LOOP

        DO ProgressBar_LoopPost
        !-----------------------------------------------------------------
        ! Totals
        !
        Excel{'Range("A' & TotalRow & ':' & CLIP(sheet:HeadLastCol) & TotalRow & '").Select'}
            DO XL_SetColumnHeader

        Excel{'Range("A' & TotalRow & '").Select'}
            Excel{'ActiveCell.Offset(0, 0).Formula'}      = 'Total'
            Excel{'ActiveCell.Offset(0, 1).Formula'}      = '=SUBTOTAL(9,B' & FirstRow & ':B' & LastRow & ')'
            Excel{'ActiveCell.Offset(0, 2).Formula'}      = '=SUBTOTAL(9,C' & FirstRow & ':C' & LastRow & ')'

            Excel{'ActiveCell.Offset(0, 3).NumberFormat'} = '0.00%'
            Excel{'ActiveCell.Offset(0, 3).FormulaR1C1'}  = '=IF(rc[-2]=0, 0, rc[-1]/rc[-2])'
        !-----------------------------------------------------------------
        Excel{'Range("A' & TitleRow & ':' & CLIP(sheet:HeadLastCol) & LastRow & '").Select'}
                        DO XL_DataAutoFilter

        Excel{'Range("A' & TotalRow+2 & '").Select'}
        !-----------------------------------------------------------------
    EXIT
WriteEngineerSummary                             ROUTINE
    DATA
QueueIndex   LONG
QueueCount LONG

TitleRow LONG
FirstRow LONG
LastRow  LONG
TotalRow LONG
    CODE
        !-----------------------------------------------------------------
        sheet:HeadLastCol = 'E'
        CreateSummarySheet('Engineer Summary')

        Excel{'ActiveSheet.Columns("A:B").ColumnWidth'} = 22
        Excel{'ActiveSheet.Columns("C:D").ColumnWidth'} = 15
        !-----------------------------------------------------------------
        QueueCount = RECORDS(EngineerQueue)

        TitleRow = Excel{'ActiveCell.Row'}
        FirstRow = TitleRow + 1
        LastRow  = TitleRow + QueueCount
        TotalRow = LastRow  + 1
        !-----------------------------------------------------------------
        Progress:Text    = 'Writing Engineer Summary'
        RecordsToProcess = QueueCount

        DO ProgressBar_LoopPre
        !-----------------------------------------------------------------
        Excel{'Range("A' & TitleRow & '").Select'}
            Excel{'ActiveCell.Offset(0, 0).Formula'} = 'Entity'
            Excel{'ActiveCell.Offset(0, 1).Formula'} = 'Engineer'
            Excel{'ActiveCell.Offset(0, 2).Formula'} = 'Repaired'
            Excel{'ActiveCell.Offset(0, 3).Formula'} = 'Bounced'
            Excel{'ActiveCell.Offset(0, 4).Formula'} = 'Percentage'

        Excel{'Range("A' & TitleRow & ':' & CLIP(sheet:HeadLastCol) &TitleRow & '").Select'}
            DO XL_SetColumnHeader

        Excel{'Range("A' & FirstRow & '").Select'}
            IF QueueCount = 0
                Excel{'ActiveCell.Offset(0, 0).Formula'} = 'No Data Found'
                Excel{'ActiveCell.Offset(2, 0).Select'}

                EXIT
            END !IF
        !-----------------------------------------------------------------
        SORT(EngineerQueue, +engQ:HeadAccount, +engQ:EngineerName)
        LOOP QueueIndex = 1 TO QueueCount
            !-------------------------------------------------------------
            RecordCount += 1

            DO ProgressBar_Loop

            IF CancelPressed
                BREAK
            END !IF
            !-------------------------------------------------------------
            GET(EngineerQueue, QueueIndex)

            Excel{'ActiveCell.Offset(0,  0).Formula'}      = engQ:HeadAccount
            Excel{'ActiveCell.Offset(0,  1).Formula'}      = engQ:EngineerName
            Excel{'ActiveCell.Offset(0,  2).Formula'}      = engQ:JobsRepaired
            Excel{'ActiveCell.Offset(0,  3).Formula'}      = engQ:JobsBounced

            Excel{'ActiveCell.Offset(0,  4).NumberFormat'} = '0.00%'
            Excel{'ActiveCell.Offset(0,  4).FormulaR1C1'}  = '=IF(rc[-2]=0, 0, rc[-1]/rc[-2])'

            DO XL_RowDown
        END !LOOP

        DO ProgressBar_LoopPost
        !-----------------------------------------------------------------
        Excel{'Range("A' & TitleRow & ':' & CLIP(sheet:HeadLastCol) & LastRow & '").Select'}
            DO XL_DataAutoFilter
        !-----------------------------------------------------------------
        ! Totals
        !
        Excel{'Range("A' & TotalRow & ':' & CLIP(sheet:HeadLastCol) & TotalRow & '").Select'}
            DO XL_SetColumnHeader

        Excel{'Range("A' & TotalRow & '").Select'}
            Excel{'ActiveCell.Offset(0, 0).Formula'}      = 'Total'
            Excel{'ActiveCell.Offset(0, 2).Formula'}      = '=SUBTOTAL(9,C' & FirstRow & ':C' & LastRow & ')'
            Excel{'ActiveCell.Offset(0, 3).Formula'}      = '=SUBTOTAL(9,D' & FirstRow & ':D' & LastRow & ')'

            Excel{'ActiveCell.Offset(0, 4).NumberFormat'} = '0.00%'
            Excel{'ActiveCell.Offset(0, 4).FormulaR1C1'}  = '=IF(rc[-2]=0, 0, rc[-1]/rc[-2])'

        Excel{'Range("A' & TotalRow+2 & '").Select'}
        !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------
GetFileName                             ROUTINE ! Generate default file name
    DATA
local:Desktop         CString(255)
DeskTopExists   BYTE
ApplicationPath STRING(255)
excel:PRogramName       Cstring(255)
    CODE
    excel:ProgramName = 'Bounce Report'
    SHGetSpecialFolderPath( GetDesktopWindow(), local:Desktop, 5, FALSE )
    local:Desktop = Clip(local:Desktop) & '\ServiceBase Export'

    !Does the Export Folder already Exists?
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Message('An error has occurred finding, or creating the folder for the report.'&|
                '|'&|
                '|' & Clip(local:Desktop) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                Icon:Hand,'&OK',1)
            Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    local:Desktop = Clip(local:Desktop) & '\' & Clip(excel:ProgramName)
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Message('An error has occurred finding, or creating the folder for the report.'&|
                '|'&|
                '|' & Clip(local:Desktop) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                Icon:Hand,'&OK',1)
            Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    loc:Path = local:Desktop
    loc:FileName = Clip(local:Desktop) & '\' & Clip(excel:ProgramName) & ' ' & Format(Today(),@d12)

!    If Exists(excel:FileName & '.xls')
!        Remove(excel:FileName & '.xls')
!        If Error()
!            Beep(Beep:SystemHand)  ;  Yield()
!            Case Missive('Cannot get access to the selected document:'&|
!                '|' & Clip(excel:FileName) & ''&|
!                '|'&|
!                '|Ensure the file is not in use and try again.','ServiceBase',|
!                           'mstop.jpg','/&OK')
!                Of 1 ! &OK Button
!            End!Case Message
!            Exit
!        End !If Error()
!    End !If Exists(excel:FileName)
!        !-----------------------------------------------
!        ! Generate default file name
!        !
!        !-----------------------------------------------
!        ! 4 Dec 2001 John Stop using def:Export_Path as previously directed
!        ! 4 Dec 2001 John Use generated date to create file name not parameters LOC:StartDate,LOC:EndDate
!        !
!        IF CLIP(LOC:FileName) <> ''
!            ! Already generated
!            EXIT
!        END !IF
!
!        DeskTopExists = False
!
!        SHGetSpecialFolderPath( GetDesktopWindow(), Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
!        LOC:DesktopPath = Desktop
!
!        ApplicationPath = Desktop & '\' & LOC:ApplicationName & ' Export'
!        IF EXISTS(CLIP(ApplicationPath))
!            LOC:Path      = CLIP(ApplicationPath) & '\'
!            DeskTopExists = True
!
!        ELSE
!            Desktop  = CLIP(ApplicationPath)
!
!            IF MkDir( Desktop ) ! C runtime library 'clib.clw'
!!                MESSAGE('MkDir(' & Desktop & ')=[TRUE]')
!!            ELSE
!!                MESSAGE('MkDir(' & Desktop & ')=[FALSE]')
!            END !IF
!
!            IF EXISTS(CLIP(ApplicationPath))
!                LOC:Path      = CLIP(ApplicationPath) & '\'
!                DeskTopExists = True
!            END !IF
!
!        END !IF
!
!        IF DeskTopExists
!            ApplicationPath = CLIP(LOC:Path) & CLIP(LOC:ProgramName)
!            Desktop         = CLIP(ApplicationPath)
!
!            IF NOT EXISTS(CLIP(ApplicationPath))
!                IF MkDir( Desktop ) ! C runtime library 'clib.clw'
!!                    MESSAGE('MkDir(' & Desktop & ')=[TRUE]')
!!               ELSE
!!                    MESSAGE('MkDir(' & Desktop & ')=[FALSE]')
!                END !IF
!            END !IF
!
!            IF EXISTS(CLIP(ApplicationPath))
!                LOC:Path = CLIP(ApplicationPath) & '\'
!            END !IF
!        END !IF
!
!        !LOC:FileName = CLIP(SHORTPATH(LOC:Path)) & CLIP(LOC:ProgramName) & ' ' & FORMAT(TODAY(), @D12) & '.xls'
!        Relate:DEFAULTV.open()
!        Set(DEFAULTV)
!        Access:DEFAULTV.Next()
!
!        LOC:FileName = SHORTPATH(CLIP(LOC:Path)) & CLIP(LOC:ProgramName) & ' VODR0072 v' & CLIP(tmp:VersionNumber) & ' ' & FORMAT(TODAY(), @D12) & '.xls'
!
!        Access:DEFAULTV.Close()
!        !-----------------------------------------------
!    EXIT
GetUserName                            ROUTINE
    DATA
CommandLine STRING(255)
tmpPos      LONG
    CODE
        !-----------------------------------------------
        CommandLine = CLIP(COMMAND(''))

!        LOC:UserName = LOC:ApplicationName
!
!        Case MessageEx('Fail User Check For "' & CLIP(CommandLine) & '"?', LOC:ApplicationName & ' DEBUG',|
!                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
!            Of 1 ! &Yes Button
!                POST(Event:CloseWindow)
!
!                EXIT
!            Of 2 ! &No Button
!                EXIT
!        End!Case MessageEx
        !-----------------------------------------------
        tmpPos = INSTRING('%', CommandLine)
        IF NOT tmpPos
            Beep(Beep:SystemHand)  ;  Yield()
            Case Message('You must run this report from ServiceBase''s "Custom Reports".','ServiceBase',|
                Icon:Hand,'&OK',1)
            Of 1 ! &OK Button
            End!Case Message

            POST(Event:CloseWindow)

           EXIT
        END !IF tmpPos
        !-----------------------------------------------
        SET(USERS)
        Access:USERS.Clearkey(use:Password_Key)
        glo:Password = CLIP(SUB(CommandLine, tmpPos + 1, 30))

        Access:USERS.Clearkey(use:Password_Key)
        use:Password = glo:Password
        If Access:USERS.Tryfetch(use:Password_Key)
            !Assert(0,'<13,10>Fetch Error<13,10>')
!            Case MessageEx('Unable to find your logged in user details.', |
!                    LOC:ApplicationName,                                  |
!                    'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,LocalTimeOut)
!                Of 1 ! &OK Button
!            End!Case MessageEx

!            POST(Event:CloseWindow)

 !          EXIT
        End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        !-----------------------------------------------
        LOC:UserName = AppendString(use:Forename,                      use:Surname, ' ')
        LOC:UserName = AppendString(LOC:UserName, ' <' & CLIP(use:User_Code) & '>', ' ')
        !-----------------------------------------------
    EXIT
LoadFileDialog                          ROUTINE ! Ask user if file name is empty
    DATA
OriginalPath STRING(255)
    CODE
        !-----------------------------------------------
        IF CLIP(LOC:Filename) = ''
            DO GetFileName
        END !IF

        If Command('/SCHEDULE')
            Exit
        End ! If Command('/SCHEDULE')

        !Only call the file dialog, if there is sill no filename  (DBH: 07-05-2004)
        If loc:Filename = '' And Automatic <> True
            OriginalPath = PATH()
                IF NOT FILEDIALOG('Save Spreadsheet', LOC:Filename, 'Microsoft Excel Workbook|*.XLS', |
                    FILE:KeepDir + FILE:Save + FILE:NoError + FILE:LongName)

                    LOC:Filename = ''
                END !IF
            SETPATH(OriginalPath)
        End !If loc:Filename = ''



        UPDATE()
        DISPLAY()
        !-----------------------------------------------
    EXIT
!-----------------------------------------------
PassViaClipboard                                            ROUTINE
    DATA
ActiveWorkSheet CSTRING(20)
ActiveCell      CSTRING(20)
    CODE
        !-----------------------------------------------
        IF clip:Saved = False
            clip:OriginalValue = CLIPBOARD()
            clip:Saved         = True
        END !IF
        !-----------------------------------------------
        SETCLIPBOARD(CLIP(clip:Value))

        ActiveCell = Excel{'ActiveCell'}
            Excel{ActiveCell & '.PasteSpecial'}
!            !Excel{'ActiveWorkSheet.Paste()'}
!            IF RecordCount < 10
!                message('clip:Value="' & CLIP(clip:Value) & '"')
!            END !IF
        Excel{prop:Release} = ActiveCell
        !-----------------------------------------------
    EXIT
ResetClipboard                                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF clip:Saved = False
            EXIT
        END !IF

        SETCLIPBOARD(clip:OriginalValue)
        !-----------------------------------------------
    EXIT
!-----------------------------------------------
LoadTradeAccount                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        ! Account_Number_Key       KEY(                        sub:Account_Number),NOCASE
        ! Branch_Key               KEY(                        sub:Branch),DUP,NOCASE
        ! Company_Name_Key         KEY(                        sub:Company_Name),DUP,NOCASE
        ! Main_Account_Key         KEY(sub:Main_Account_Number,sub:Account_Number),NOCASE,PRIMARY
        ! Main_Branch_Key          KEY(sub:Main_Account_Number,sub:Branch),DUP,NOCASE
        ! Main_Name_Key            KEY(sub:Main_Account_Number,sub:Company_Name),DUP,NOCASE
        !
        Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
        sub:Account_Number = job:Account_Number
        !--------------------------------------------------------------------------------------
        IF Access:SUBTRACC.Fetch(sub:Account_Number_Key)
            LOC:Account_Name = '<Not Found>'
        END !IF

        IF sub:Account_Number <> job:Account_Number
            LOC:Account_Name = '<Not Found>'
        END !IF

        LOC:Account_Name = CLIP(sub:Company_Name)
        !-----------------------------------------------
    EXIT
LoadUSERS                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Result = False
        !-----------------------------------------------
        !
        Access:USERS.ClearKey(use:User_Code_Key)
        use:User_Code = LOC:UserCode

        IF Access:USERS.TryFetch(use:User_Code_Key)
            EXIT
        END !IF

        IF use:User_Code <> LOC:UserCode
            EXIT
        END !IF

        Result = True
        !-----------------------------------------------
    EXIT
!-----------------------------------------------
ProgressBar_Setup                       ROUTINE
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***
            recordspercycle      = 25
            recordsprocessed     = 0
            recordstoprocess     = 10 !***The Number Of Records, or at least a guess***
            percentprogress      = 0
            progress:thermometer = 0

            thiswindow.reset(1) !This may error, if so, remove this line.
            open(progresswindow)
            progresswindow{PROP:Timer} = 1

            ?progress:userstring{prop:text} = 'Running...'
            ?progress:pcttext{prop:text}    = '0% Completed'
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***

ProgressBar_LoopPre          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***
        RecordsProcessed = 0
        RecordCount      = 0

        ?Progress:UserString{PROP:Text} = CLIP(Progress:Text)
        !RecordsToProcess = RECORDS(JOBS) !***The Number Of Records, or at least a guess***
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***
        !-----------------------------------------------
    EXIT

ProgressBar_Loop             ROUTINE
    DATA
newPctText STRING(255)
    CODE
        !-----------------------------------------------
        !***Place the following code inside the loop. The first line updates the progress bar, and the rest allows the user to cancel the routine. This can be removed if necessary. ****
        recordsprocessed += 1
        recordsthiscycle += 1

        Do ProgressBar_GetNextRecord2 !Necessary

        Do ProgressBar_CancelCheck
        If CancelPressed
            close(progresswindow)
        End!If CancelPressed
        !-----------------------------------------------
        newPctText = 'Checked(' & RecordsProcessed & '), Found(' & RecordCount & ')'

        IF ?progress:pcttext{prop:text} <> newPctText
            ?progress:pcttext{prop:text} = newPctText

            Display()
        END !IF
        !***Place the following code inside the loop. The first line updates the progress bar, and the rest allows the user to cancel the routine. This can be removed if necessary. ****
        !-----------------------------------------------
    EXIT

ProgressBar_LoopPost         ROUTINE
    DATA
newPctText STRING(255)
    CODE
        !-----------------------------------------------
        ?Progress:UserString{PROP:Text} = CLIP(Progress:Text) & ' Done(' & RecordsProcessed & '/' & RecordCount & ')'

        newPctText = 'Checked(' & RecordsProcessed & '), Found(' & RecordCount & ')'

        IF ?progress:pcttext{prop:text} <> newPctText
            ?progress:pcttext{prop:text} = newPctText
        END !IF
        !-----------------------------------------------
    EXIT

ProgressBar_Finalise                        ROUTINE
        !**** End Of Loop ***
            Do ProgressBar_EndPrintRun
            close(progresswindow)
        !**** End Of Loop ***
        ! when in report procedure->LocalResponse = RequestCompleted

ProgressBar_GetNextRecord2                  routine


    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        !?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end

ProgressBar_CancelCheck                     routine
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    Beep(Beep:SystemQuestion)  ;  Yield()
                    Case Message('Are you sure you want to cancel?','ServiceBase',|
                        Icon:Question,'&Yes|&No',2)
                    Of 1 ! &Yes Button
                        CancelPressed = True
                        Of 1 ! No Button
                    Of 2 ! &No Button
                    End!Case Message

                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept

ProgressBar_EndPrintRun                     routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()
RefreshWindow                          ROUTINE
    !|
    !| This routine is used to keep all displays and control templates current.
    !|
    IF MainWindow{Prop:AcceptAll} THEN EXIT.
    DISPLAY()
    !ForceRefresh = False
XL_Setup                                                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel = Create(0, Create:OLE)
        Excel{PROP:Create} = 'Excel.Application'

        Excel{'Application.DisplayAlerts'}  = False  ! no alerts to the user (do you want to save the file?) ! MOVED 10 BDec 2001 John
        Excel{'Application.ScreenUpdating'} = LOC:ExcelVisible ! False
        Excel{'Application.Visible'}        = LOC:ExcelVisible ! False

        Excel{'Application.Calculation'}    = xlCalculationManual

        DO XL_GetOperatingSystem
        !-----------------------------------------------
    EXIT
XL_Finalize                                                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Application.Calculation'}    = xlCalculationAutomatic

        Excel{PROP:DEACTIVATE}
        !-----------------------------------------------
    EXIT
XL_AddSheet                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveWorkBook.Sheets("Sheet3").Select'}

        Excel{'ActiveWorkBook.Sheets.Add'}
        !-----------------------------------------------
    EXIT
XL_AddWorkbook               ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Application.Workbooks.Add()'}

        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Title")'}            = CLIP(LOC:ProgramName) & ' ' & CLIP(tra_ali:Account_Number)
        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Author")'}           = CLIP(LOC:UserName)
        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Application Name")'} = LOC:ApplicationName
        !-----------------------------------------------
        ! 4 Dec 2001 John
        ! Delete empty sheets

        Excel{'Sheets("Sheet2").Select'}
        Excel{'ActiveWindow.SelectedSheets.Delete'}

        Excel{'Sheets("Sheet1").Select'}
        !-----------------------------------------------
    EXIT
XL_SaveAllWorksheets                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOOP WHILE Excel{'WorkBooks.Count'} > 0
            Excel{'ActiveWorkBook.Save()'}
            Excel{'ActiveWorkBook.Close()'}
        END !LOOP
        !-----------------------------------------------
    EXIT
XL_DropAllWorksheets                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOOP WHILE Excel{'WorkBooks.Count'} > 0
            Excel{'ActiveWorkBook.Close(' & FALSE & ')'}
        END !LOOP
        !-----------------------------------------------
    EXIT
XL_AddComment                ROUTINE
    DATA
xlComment CSTRING(20)
    CODE
        !-----------------------------------------------
        xlComment = Excel{'Selection.AddComment("' & CLIP(excel:CommentText) & '")'}

!        xlComment{'Shape.IncrementLeft'} = 127.50
!        xlComment{'Shape.IncrementTop'}  =   8.25
!        xlComment{'Shape.ScaleWidth'}    =   1.76
!        xlComment{'Shape.ScaleHeight'}   =   0.69

        Excel{prop:Release} = xlComment
        !-----------------------------------------------
    EXIT
XL_RowDown                   ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(1, 0).Select'}
        !-----------------------------------------------
    EXIT
XL_ColLeft                   ROUTINE
    DATA
CurrentCol LONG
    CODE
        !-----------------------------------------------
        CurrentCol = Excel{'ActiveCell.Col'}
        IF CurrentCol = 1
            EXIT
        END !IF

        Excel{'ActiveCell.Offset(0, -1).Select'}
        !-----------------------------------------------
    EXIT
XL_ColRight                  ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(0, 1).Select'}
        !-----------------------------------------------
    EXIT
XL_ColFirstSelect                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range(Selection, Cells(' & Excel{'ActiveCell.Row'} & ', 1)).Select'}
        !Excel{'ActiveCell.Offset(0, -' & Excel{'ActiveCell.Column'} - 1 & ').Select'}
        !-----------------------------------------------
    EXIT
XL_ColFirst                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(0, -' & Excel{'ActiveCell.Column'} - 1 & ').Select'}
        !-----------------------------------------------
    EXIT
XL_SetLastCell                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range(Selection, ActiveCell.SpecialCells(' & xlLastCell & ')).Select'}
        !-----------------------------------------------
    EXIT
XL_SetGrid                              ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlDiagonalDown & ').LineStyle'} = xlNone
        Excel{'Selection.Borders(' & xlDiagonalUp   & ').LineStyle'} = xlNone

        DO XL_SetBorder

        Excel{'Selection.Borders(' & xlInsideVertical & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlInsideVertical & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlInsideVertical & ').ColorIndex'} = xlAutomatic

        Excel{'Selection.Borders(' & xlInsideHorizontal & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlInsideHorizontal & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlInsideHorizontal & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorder                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_SetBorderLeft
        DO XL_SetBorderRight
        DO XL_SetBorderBottom
        DO XL_SetBorderTop
        !-----------------------------------------------
    EXIT
XL_SetBorderLeft                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeLeft & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeLeft & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeLeft & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderRight                           ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeRight & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeRight & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeRight & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderBottom                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeBottom & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeBottom & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeBottom & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderTop                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeTop & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeTop & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeTop & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_HighLight                           ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Interior.ColorIndex'}        = 6 ! YELLOW
        Excel{'Selection.Interior.Pattern'}           = xlSolid
        Excel{'Selection.Interior.PatternColorIndex'} = xlAutomatic
        !-----------------------------------------------
XL_SetTitle                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Interior.ColorIndex'}        = 15 ! GREY
        Excel{'Selection.Interior.Pattern'}           = xlSolid
        Excel{'Selection.Interior.PatternColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetWrapText                         ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.WrapText'}    = True
        !-----------------------------------------------
    EXIT
XL_SetGreyText                         ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Font.ColorIndex'}    = 16 ! grey
        !-----------------------------------------------
    EXIT
XL_SetBold                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Font.Bold'} = True
        !-----------------------------------------------
    EXIT
XL_FormatDate                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.NumberFormat'} = excel:DateFormat
        !-----------------------------------------------
    EXIT
XL_FormatNumber                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.NumberFormat'} = '#,##0.00'
        !-----------------------------------------------
    EXIT
XL_FormatTextBox                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.ShapeRange.IncrementLeft'} = 127.5
        Excel{'Selection.ShapeRange.IncrementTop'}  =   8.25
!        Excel{'Selection.ShapeRange.ScaleWidth'}    =  '1.76, ' & msoFalse & ', ' & msoScaleFromBottomRight
!        Excel{'Selection.ShapeRange.ScaleHeight'}   =  '0.69, ' & msoFalse & ', ' & msoScaleFromTopLeft
        !-----------------------------------------------
    EXIT
XL_HorizontalAlignmentLeft              ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlLeft
        !-----------------------------------------------
    EXIT
XL_HorizontalAlignmentRight             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlRight
        !-----------------------------------------------
    EXIT
XL_SetColumnHeader                      ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_SetTitle
        DO XL_SetGrid
        DO XL_SetWrapText
        DO XL_SetBold
        !-----------------------------------------------
    EXIT
XL_SetColumn_Date                       ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = 'dd Mmm yyyy'

!        !Excel{'ActiveCell.FormulaR1C1'}  = LEFT(FORMAT(ret:Invoice_Date, @D8-))
        !-----------------------------------------------
    EXIT
XL_SetColumn_Number                     ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = '#,##0.00'
        !-----------------------------------------------
    EXIT
XL_SetColumn_Percent                    ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = '0.00%'
!        Excel{'ActiveCell.NumberFormat'} = CHR(64)       ! Text (AT) Sign
        !-----------------------------------------------
    EXIT
XL_SetColumn_Text                       ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'} = CHR(64) ! Text (AT) Sign
        !-----------------------------------------------
    EXIT
XL_DataSelectRange                      ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range(Selection, ActiveCell.SpecialCells(' & xlLastCell & ')).Select'}
        !-----------------------------------------------
    EXIT
XL_DataHideDuplicates                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.AdvancedFilter Action:=' & xlFilterInPlace & ', Unique:=' & True }
        !-----------------------------------------------
    EXIT
XL_DataSortRange                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_DataSelectRange
        Excel{'ActiveWindow.ScrollColumn'} = 1
        Excel{'Selection.Sort Key1:=Range("A11"), Order1:=' & xlAscending & ', Header:=' & xlGuess & |
            ', OrderCustom:=1, MatchCase:=' & False & ', Orientation:=' & xlTopToBottom}
        !-----------------------------------------------
    EXIT
XL_DataAutoFilter                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.AutoFilter'}
        !-----------------------------------------------
    EXIT
XL_SetWorksheetLandscape                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveSheet.PageSetup.Orientation'}     = xlLandscape
!        Excel{'ActiveSheet.PageSetup.FitToPagesWide'} = 1
!        Excel{'ActiveSheet.PageSetup.FitToPagesTall'} = 9999
!        Excel{'ActiveSheet.PageSetup.Order'}          = xlOverThenDown
        !-----------------------------------------------
    EXIT
XL_SetWorksheetPortrait                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveSheet.PageSetup.Orientation'} = xlPortrait
        !-----------------------------------------------
    EXIT
XL_GetCurrentCell                               ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOC:Text = Excel{'Application.ConvertFormula( "RC", ' & xlR1C1 & ', ' & xlA1 & ')'}
        !-----------------------------------------------
    EXIT
XL_MergeCells                                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlCenter
        Excel{'Selection.VerticalAlignment'}   = xlBottom
        Excel{'Selection.MergeCells'}          = True
        !-----------------------------------------------
    EXIT
XL_GetOperatingSystem                                                          ROUTINE
    DATA
OperatingSystem     STRING(50)
tmpLen              LONG
LEN_OperatingSystem LONG
QueueIndex          LONG
    CODE
        !-----------------------------------------------
        excel:OperatingSystem = 0.0 ! Default/error value
        OperatingSystem     = Excel{'Application.OperatingSystem'}
        LEN_OperatingSystem = LEN(CLIP(OperatingSystem))

        LOOP QueueIndex = LEN_OperatingSystem TO 1 BY -1

            CASE SUB(OperatingSystem, QueueIndex, 1)
            OF '0' OROF'1' OROF '2' OROF '3' OROF '4' OROF '5' OROF '6' OROF '7' OROF '8' OROF '9' OROF '.'
                ! NULL
            ELSE
                tmpLen                = LEN_OperatingSystem-QueueIndex+1
                excel:OperatingSystem = CLIP(SUB(OperatingSystem, QueueIndex+1, tmpLen))

                EXIT
            END !CASE
        END !LOOP
        !-----------------------------------------------
    EXIT
!-----------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          Access:SRNTExt.Clearkey(srn:KeySRN)
          srn:SRNumber = '020618'&'0'
          If Access:SRNText.Fetch(srn:KeySRN)
              !Error
              SRN:TipText = 'Press F1 to access detailed help'
          End !Access:SRNText.Fetch(stn:KeySRN) = Level:Benign
          ?SRNNumber{Prop:Text}= 'SRN:020618'&'0'
          if clip(SRN:TipText)='' then SRN:TipText = 'Press F1 to access detailed help'.
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BouncerReport')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('LocalHeadAccount',LocalHeadAccount)                ! Added by: BrowseBox(ABC)
  BIND('LocalTag',LocalTag)                                ! Added by: BrowseBox(ABC)
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:SRNText.Open()
  Relate:ACCESSOR.SetOpenRelated()
  Relate:ACCESSOR.Open                                     ! File ACCESSOR used by this procedure, so make sure it's RelationManager is open
  Relate:DEFAULTS.Open                                     ! File DEFAULTS used by this procedure, so make sure it's RelationManager is open
  Relate:JOBBATCH.Open                                     ! File JOBBATCH used by this procedure, so make sure it's RelationManager is open
  Relate:JOBS2_ALIAS.Open                                  ! File JOBS2_ALIAS used by this procedure, so make sure it's RelationManager is open
  Relate:REPSCHCR.SetOpenRelated()
  Relate:REPSCHCR.Open                                     ! File REPSCHCR used by this procedure, so make sure it's RelationManager is open
  Relate:TRADEACC_ALIAS.Open                               ! File TRADEACC_ALIAS used by this procedure, so make sure it's RelationManager is open
  Relate:WEBJOB.Open                                       ! File WEBJOB used by this procedure, so make sure it's RelationManager is open
  Access:USERS.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBS.UseFile                                      ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:SUBTRACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBTHIRD.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TRDPARTY.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TRDBATCH.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBS_ALIAS.UseFile                                ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:BOUNCER.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:AUDIT.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:REPTYDEF.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBSE.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:MANFAULT.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:MANFAULO.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:PARTS.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:WARPARTS.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:CHARTYPE.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:LOCATLOG.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBSENG.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:REPSCHED.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:REPSCHLG.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  BRW5.Init(?List,Queue:Browse.ViewPosition,BRW5::View:Browse,Queue:Browse,Relate:TRADEACC,SELF) ! Initialize the browse manager
  SELF.Open(MainWindow)                                    ! Open window
  !============== Version Number =====================
  Include('..\ReportVersion.inc')
  tmp:VersionNumber = Clip(tmp:VersionNumber) & '5007'
  ?ReportVersion{prop:Text} = 'Report Version: ' & Clip(tmp:VersionNumber)
    LOC:ProgramName         = 'Vodacare Bounce Report'        ! Job=2209       Cust=Vodacom

    LOC:ExcelVisible = False
    debug:Active     = False

    LOC:StartDate      = TODAY() !
    LOC:EndDate        = TODAY() !
    LOC:DayLeawayCount = 90

!      SET(defaults)
!      access:defaults.next()

    DO GetUserName
    IF GUIMode = 1 THEN
       MainWindow{PROP:ICONIZE} = TRUE
       LocalTimeOut = 500
    END !IF

    LocalHeadAccount = GETINI('BOOKING', 'HEADACCOUNT', '', CLIP(PATH()) & '\SB2KDEF.INI')

    !Check to see if the report has been run with one of the auto switches  (DBH: 07-05-2004)
    If Command('/DAILY') Or Command('/WEEKLY') Or Command('/MONTHLY')
        LOC:UserName = 'AUTOMATED PROCEDURE'
        Automatic = True
    Else
        Automatic = False
    End !If Command('/DAILY') Or Command('/WEEKLY') Or Command('/MONTHLY')

    Access:TradeAcc.ClearKey(tra:Account_Number_Key)
    tra:Account_Number = LocalHeadAccount
    IF Access:TradeAcc.Fetch(tra:Account_Number_Key)
      !Error!
    ELSE
      LocalHeadAccountName = tra:Company_Name
    END





  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
  ?List{Prop:LineHeight} = 0
  Do DefineListboxStyle
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
      Alert(0078H) !F9
      Alert(0070H) !F1
  INIMgr.Fetch('BouncerReport',MainWindow)                 ! Restore window settings from non-volatile store
  BRW5.Q &= Queue:Browse
  BRW5.AddSortOrder(,tra:Company_Name_Key)                 ! Add the sort order for tra:Company_Name_Key for sort order 1
  BRW5.AddLocator(BRW5::Sort0:Locator)                     ! Browse has a locator for sort order 1
  BRW5::Sort0:Locator.Init(,tra:Company_Name,1,BRW5)       ! Initialize the browse locator using  using key: tra:Company_Name_Key , tra:Company_Name
  BRW5.SetFilter('((tra:RemoteRepairCentre = 1 OR tra:Account_Number = LocalHeadAccount) AND tra:Account_Number <<> ''XXXRRC'')') ! Apply filter expression to browse
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW5.AddField(LocalTag,BRW5.Q.LocalTag)                  ! Field LocalTag is a hot field or requires assignment from browse
  BRW5.AddField(tra:Account_Number,BRW5.Q.tra:Account_Number) ! Field tra:Account_Number is a hot field or requires assignment from browse
  BRW5.AddField(tra:Company_Name,BRW5.Q.tra:Company_Name)  ! Field tra:Company_Name is a hot field or requires assignment from browse
  BRW5.AddField(tra:RecordNumber,BRW5.Q.tra:RecordNumber)  ! Field tra:RecordNumber is a hot field or requires assignment from browse
      !Set the report to automatic run  (DBH: 07-05-2004)
  
      IF Automatic = TRUE
          IF loc:startdate = ''
            LOC:StartDate = TODAY()
            LOC:EndDate = TODAY()
          END
  
          !Daily and Weekly Report not specified  (DBH: 07-05-2004)
  
          If Command('/DAILY')
              loc:StartDate = Today()
              loc:EndDate = Today()
          End !If Command('/DAILY')
  
          !Monthly Report to run from 1st to last day of month  (DBH: 07-05-2004)
  
          If Command('/WEEKLY')
              Loop day# = Today() To Today()-7 By -1
                  If day# %7 = 1
                      loc:StartDate = day#
                      Break
                  End !If day# %7 = 2
              End !Loop day# = Today() To Today()-7 By -1
              Loop day# = Today() To Today() + 7
                  IF day# %7 = 0
                      loc:EndDate = day#
                      Break
                  End !IF day# %7 = 0
              End !Loop day# = Today() To Today() + 7 By -1
          End !If Command('/WEEKLY')
          !Monthly - Start Date is 1st of Month, End Date is last of month  (DBH: 07-05-2004)
          If Command('/MONTHLY')
              loc:StartDate = Deformat('1/' & Month(Today()) & '/' & Year(Today()),@d6)
              loc:EndDate = Deformat('1/' & Month(Today()) + 1 & '/' & Year(Today()),@d6) - 1
          End !If Command('/MONTHLY')
  
          DO OKButton_Pressed
          POST(Event:CloseWindow)
      END
  SELF.SetAlerts()
  ! Inserting (DBH 31/08/2007) # 9125 - Is this an automatic report?
  If Command('/SCHEDULE')
      x# = Instring('%',Command(),1,1)
      Access:REPSCHED.ClearKey(rpd:RecordNumberKey)
      rpd:RecordNumber = Clip(Sub(Command(),x# + 1,20))
      If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
          !Found
          Access:REPSCHCR.ClearKey(rpc:ReportCriteriaKey)
          rpc:ReportName = rpd:ReportName
          rpc:ReportCriteriaType = rpd:ReportCriteriaType
          If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
              !Found
              Case rpc:DateRangeType
              !this has changed 1 = once only, 2 = daily, 3 = weekly, 4 = monthly if set to 4 this is going banananananas.
              Of 1 ! Today Only
                  loc:StartDate = Today()
                  loc:EndDate = Today()
              Of 2 ! 1st Of Month
                  loc:StartDate = Date(Month(Today()),1,Year(Today()))
                  loc:EndDate = Today()
              Of 3 ! Whole Month
  ! Changing (DBH 31/01/2008) # 9711 - Allow for change of year
  !                tmp:StartDate = Date(Month(Today()) - 1, 1, Year(Today()))
  !                tmp:EndDate = Date(Month(Today()),1,Year(Today())) - 1
  ! to (DBH 31/01/2008) # 9711
                  loc:EndDate = Date(Month(Today()),1,Year(Today())) - 1
                  loc:StartDate = Date(Month(loc:EndDate),1,Year(loc:EndDate))
  ! End (DBH 31/01/2008) #9711
              End ! Case rpc:DateRangeType
  
              LOC:DayLeawayCount = rpc:LeewayDays
  
              DO OKButton_Pressed
              POST(Event:CloseWindow)
          Else ! If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
              !Error
              POST(Event:CloseWindow)
          End ! If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
  
      Else ! If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
          !Error
      End ! If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
  
  End ! If Command('/SCHEDULE')
  ! End (DBH 31/08/2007) #9125
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?List{Prop:Alrt,239} = SpaceKey
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  Relate:SRNText.Close()
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:ACCESSOR.Close
    Relate:DEFAULTS.Close
    Relate:JOBBATCH.Close
    Relate:JOBS2_ALIAS.Close
    Relate:REPSCHCR.Close
    Relate:TRADEACC_ALIAS.Close
    Relate:WEBJOB.Close
  END
  IF SELF.Opened
    INIMgr.Update('BouncerReport',MainWindow)              ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?DoAll
      IF DoAll = 'Y' THEN
         HIDE(?List)
         HIDE(?DASTAG)
         HIDE(?DASTAGALL)
         HIDE(?DASUNTAGALL)
      ELSE
          UNHIDE(?List)
          UNHIDE(?DASTAG)
          UNHIDE(?DASTAGALL)
          UNHIDE(?DASUNTAGALL)
      END !IF
    OF ?OkButton
          DO OKButton_Pressed
    OF ?CancelButton
       POST(Event:CloseWindow)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & '020618'&'0')
          Else !glo:WebJob Then
              HelpBrowser('020618'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020618'&'0')
      ***
    OF ?Calendar
      ThisWindow.Update
      Calendar11.SelectOnClose = True
      Calendar11.Ask('Select a Date',LOC:StartDate)
      IF Calendar11.Response = RequestCompleted THEN
      LOC:StartDate=Calendar11.SelectedDate
      DISPLAY(?LOC:StartDate)
      END
      ThisWindow.Reset(True)
    OF ?Calendar:2
      ThisWindow.Update
      Calendar12.SelectOnClose = True
      Calendar12.Ask('Select a Date',LOC:EndDate)
      IF Calendar12.Response = RequestCompleted THEN
      LOC:EndDate=Calendar12.SelectedDate
      DISPLAY(?LOC:EndDate)
      END
      ThisWindow.Reset(True)
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !neil
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all field specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?List
    CASE EVENT()
    OF EVENT:AlertKey
      IF RECORDS(BRW5) <> 0 AND DoAll <> 'Y' AND KeyCode() = MouseLeft2 THEN
         POST(Event:Accepted,?DASTAG)
      END !IF
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
  !Of Event:AlertKey
      If KeyCode() = 0078H
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & '020618'&'0')
          Else !glo:WebJob Then
              HelpBrowser('020618'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020618'&'0')
      ***
      End !If KeyCode() F9
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

AppendString        PROCEDURE(IN:Start, IN:Add, IN:Separator)!STRING
ReturnValue STRING(255)
    CODE
        !-----------------------------------------------------------------
        ! IN:Start,     Accumulating string
        ! IN:Add,       String to add to accumulating string
        ! IN:Separator, Separator if both non null
        !               ie ',' for comma delimited, or ' AND ' for SQL
        !               Use (literals) or (variables without excesive trailing white space).
        !-----------------------------------------------------------------
        IF CLIP(IN:Start) = ''
            ReturnValue = IN:Add
        ELSIF CLIP(IN:Add) = ''
            ReturnValue = IN:Start
        ELSE
            ReturnValue = CLIP(IN:Start) & IN:Separator & CLIP(IN:Add)
        END !IF

        RETURN ReturnValue
        !-----------------------------------------------------------------
        ! Example
        !SQL STRING(255)
        !   CODE
        !   IF ?CHOICE1
        !       SQL = '"CANCELLED" = "YES"'
        !   END !IF
        !
        !   IF ?Date1 > 0
        !       SQL = AppendString( SQL, '"DATE_BOOKED" = "' LEFT(FORMAT(job:Date_Booked, @D10)) '"', ' AND ')
        !   END !IF
        !
        !   IF ?CHOICE2
        !       SQL = AppendString( SQL, '"COMPLETE" = "YES"', ' AND ')
        !   END !IF
        !-----------------------------------------------------------------

CheckFranchise          PROCEDURE( IN:AccountName )! LONG ! BOOL ! 25 Feb 2003 John L577
    CODE
        !-----------------------------------------------------------------
        !WriteDebug('CheckFranchise(' & CLIP(IN:AccountNumber) & ')')

        CLEAR(ChosenAccount_Queue)
            caQ:AccountName = CLIP(IN:AccountName)! 25 Feb 2003 John L577
        GET(ChosenAccount_Queue, +caQ:AccountName)! 25 Feb 2003 John L577

        CASE ERRORCODE()
        OF 00
            IF caQ:AccountName = CLIP(IN:AccountName)
                ! Found
                !WriteDebug('CheckFranchise(' & CLIP(IN:AccountNumber) & ')Found')

                RETURN True
            END !IF
            ! NOT Found, Partial Match
            !WriteDebug('CheckFranchise(NOT Found, Partial Match)')

        OF 30
            ! NULL

        ELSE
            CancelPressed = True

            !WriteDebug('CheckFranchise(CancelPressed = True), Error(' & ERRORCODE() & '), "' & ERROR() & '"')
        END !CASE
        !-----------------------------------------------------------------
        !WriteDebug('CheckFranchise(caQ:AccountNumber"' & CLIP(caQ:AccountNumber) & '"<>"' & CLIP(IN:AccountNumber) & '"IN:AccountNumber) FAIL')
        RETURN False
        !-----------------------------------------------------------------

CreateSummarySheet    PROCEDURE(IN:Name)
CurentRow LONG
    CODE
        !-----------------------------------------------------------------
        !WriteDebug('CreateSummarySheet')
        !-----------------------------------------------------------------

        !----------------------------------------------
        ! 24 Feb 2003 John
        ! New summary sheets hidden to left of Summary sheet generated erroronous customer issue.
        ! Changed from "Sheet3" To display all summarys to the right of main "Summary" sheet.
        Excel{'ActiveWorkBook.Sheets("Sheet1").Select'} !Summary").Select'}
        Excel{'ActiveWorkBook.Sheets.Add'}
        !----------------------------------------------

        LOC:Text          = IN:Name
        sheet:TempLastCol = sheet:HeadLastCol

        DO CreateWorksheetHeader
        !-----------------------------------------------------------------
        DO XL_SetWorksheetPortrait

        Excel{'ActiveSheet.Columns("A:A").ColumnWidth'} = 22
        Excel{'ActiveSheet.Columns("B:D").ColumnWidth'} = 15
        !-----------------------------------------------------------------
        ! Summary
        !
        Excel{'Range("A' & sheet:HeadSummaryRow & ':' & sheet:HeadLastCol & sheet:HeadSummaryRow & '").Select'}
            DO XL_SetBold
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A' & sheet:HeadSummaryRow & '").Select'}
            Excel{'ActiveCell.Formula'} = 'Summary'
        !-----------------------------------------------------------------
        ! Prepare For summary details
        !
        Excel{'Range("A' & sheet:HeadSummaryRow+1 & '").Select'}
        !-----------------------------------------------------------------
    RETURN
DateToString        PROCEDURE( IN:Date )! STRING
    CODE
        !-----------------------------------------------------------------
        RETURN LEFT(FORMAT(IN:Date, @D8))
        !-----------------------------------------------------------------
GetHeadAccount PROCEDURE( IN:AccountNumber )
DoAdd LONG(False)
    CODE
        !-----------------------------------------------------------------
        !WriteDebug('GetHeadAccount("' & CLIP(IN:AccountNumber) & '")')

        haq:AccountNumber  = IN:AccountNumber
        GET(HeadAccount_Queue, +haq:AccountNumber)

        CASE ERRORCODE()
        OF 00 ! FOUND
            IF haq:AccountNumber  = IN:AccountNumber
                ! Found
                !WriteDebug('GetHeadAccount(Found)')

                RETURN
            ELSE
                ! Partial Match - ADD
                DoAdd = True
            END !IF

        OF 30 ! NOT Found
            ! Not in queue - ADD
            DoAdd = True
        ELSE
            CancelPressed = True
        END !IF
        !-----------------------------------------------------------------
        IF DoAdd
            !WriteDebug('GetHeadAccount(NOT Found)')

            CLEAR(HeadAccount_Queue)
                haq:AccountNumber            = IN:AccountNumber

                IF LoadTRADEACC(IN:AccountNumber)
                    haq:AccountName          = tra:Company_Name
                    haq:BranchIdentification = tra:BranchIdentification
                    haq:ExcludeBouncer       = tra:ExcludeBouncer

!                    IF tra:UseTimingsFrom = 'TRA'
!                        haQ:IncludeSaturday        = tra:IncludeSaturday
!                        haQ:IncludeSunday          = tra:IncludeSunday
!                        haQ:StartWorkHours         = tra:StartWorkHours
!                        haQ:EndWorkHours           = tra:EndWorkHours
!
!                    ELSE
!                        IF def:Include_Saturday = 'YES'
!                            haQ:IncludeSaturday        = True
!                        ELSE
!                            haQ:IncludeSaturday        = False
!                        END !IF
!
!                        IF def:Include_Sunday = 'YES'
!                            haQ:IncludeSunday        = True
!                        ELSE
!                            haQ:IncludeSunday        = False
!                        END !IF
!
!                        haQ:StartWorkHours         = def:Start_Work_Hours
!                        haQ:EndWorkHours           = def:End_Work_Hours
!
!                    END !IF

!                    IF tra:Invoice_Sub_Accounts = 'YES'
!                        haQ:InvoiceSubAccounts = True
!                    ELSE
!                        haQ:InvoiceSubAccounts = False
!                    END !IF


!                    haQ:PartsVATCode         = tra:Parts_VAT_Code
!                    haQ:LabourVATCode        = tra:Labour_VAT_Code
!                    haQ:RetailVATCode        = tra:Retail_Discount_Code
!
!                    haQ:PartsVATRate         = GetVATRate(tra:Parts_VAT_Code)
!                    haQ:LabourVATRate        = GetVATRate(tra:Labour_VAT_Code)
!                    haQ:RetailVATRate        = GetVATRate(tra:Retail_Discount_Code)

                ELSE
                    haq:AccountName          = '*T'
                    haq:BranchIdentification = '*T'

!                    haQ:PartsVATCode         = ''
!                    haQ:LabourVATCode        = ''
!                    haQ:RetailVATCode        = ''
!
!                    haQ:PartsVATRate         = 0.00
!                    haQ:LabourVATRate        = 0.00
!                    haQ:RetailVATRate        = 0.00
                END !IF
            ADD(HeadAccount_Queue, +haq:AccountNumber)
        END !IF
        !-----------------------------------------------------------------
GetManufacturerFaultLookupQueue          PROCEDURE( IN:Manufacturer, IN:FieldNumber, IN:Field )! STRING
DoADD LONG(False)
    CODE
        !-----------------------------------------------
        !WriteDebug('UpdateManufacturerQueue(' & CLIP(IN:Manufacturer) & ')')

        Clear(ManufacturerFaultLookupQueue)
            LookupQ:Manufacturer = IN:Manufacturer
            LookupQ:FieldNumber  = IN:FieldNumber
            LookupQ:Field        = IN:Field
        Get(ManufacturerFaultLookupQueue, +LookupQ:Manufacturer, +LookupQ:FieldNumber, +LookupQ:Field)

        CASE ERRORCODE()
        OF 00
            IF LookupQ:Manufacturer = IN:Manufacturer AND LookupQ:FieldNumber  = IN:FieldNumber AND LookupQ:Field        = IN:Field
                ! Found
            ELSE
                ! NOT Found Partial Match - ADD
                DoADD = True
            END !IF

        OF 30
            ! NOT Found - ADD
            DoADD = True

        Else !If Error()
            !WriteDebug('UpdateManufacturerQueue(CancelPressed = True)')

            CancelPressed = True

            RETURN ''
        End !CASE
        !-----------------------------------------------
        IF DoADD = True
            !WriteDebug('UpdateManufacturerQueue(ADD"' & CLIP(IN:Manufacturer) & '")')

            CLEAR(ManufacturerFaultLookupQueue)
                LookupQ:Manufacturer = IN:Manufacturer
                LookupQ:FieldNumber  = IN:FieldNumber
                LookupQ:Field        = IN:Field
                IF LoadMANFAULO(IN:Manufacturer, IN:FieldNumber, IN:Field)
                    LookupQ:Description = mfo:Description
                ELSE
                    !LookupQ:Description = '*MISSING FieldNumber="' & CLIP(IN:FieldNumber) & '", Field="' & CLIP(IN:Field) & '"' ! '*No Lookup Found*'
                    LookupQ:Description = '*Undefined Fault - "' & CLIP(IN:Field) & '"' ! '*No Lookup Found*'
                END !IF
                !---------------------------------------
            Add(ManufacturerFaultLookupQueue, +LookupQ:Manufacturer, +LookupQ:FieldNumber, +LookupQ:Field)
        END !IF
        !-----------------------------------------------
        RETURN LookupQ:Description
        !Put(ManufacturerQueue)
        !-----------------------------------------------

GetDeliveryDateAtARC        PROCEDURE( IN:JobNumber )! LONG ! BOOL
First LONG(True)
    CODE
        !-----------------------------------------------------------------
        !WriteDebug('GetDeliveryDateAtARC(' & IN:JobNumber & ')')

        !calc:SentToARC         = 'NO'
        !calc:ARCDateBooked     = ''
        !calc:DeliveryDateAtARC = ''

        LOOP WHILE LoadLOCATLOG( IN:JobNumber, First)
            !-------------------------------------------------------------
            First = False

            IF CLIP(lot:NewLocation) = CLIP(rrc:ARCLocation)
                !calc:SentToARC         = 'YES'
                !calc:ARCDateBooked     = lot:TheDate
               ! calc:DeliveryDateAtARC = lot:TheDate

               ! WriteDebug('GetDeliveryDateAtARC(OK)"' & CLIP(lot:NewLocation) & '", calc:SentToARC="' & clip(calc:SentToARC) & '", lot:TheDate="' & DateToString(lot:TheDate) & '"')

                RETURN True
            END !IF
            !-------------------------------------------------------------
        END !LOOP

        !WriteDebug('GetDeliveryDateAtARC(FAIL)')

        RETURN False
        !-----------------------------------------------------------------



!GetDeliveryDateAtARC        PROCEDURE( IN:JobNumber )! LONG ! BOOL
!    CODE
!    SentToARC# = False
!    LOC:ARCDateBooked = ''
!    Access:LOCATLOG.Clearkey(lot:DateKey)
!    lot:RefNumber = in:JOBNumber
!    Set(lot:DateKey,lot:DateKey)
!    Loop
!        If Access:LOCATLOG.Next()
!            Break
!        End !
!        If lot:RefNumber <> in:JobNumber
!            Break
!        End !
!        If lot:NewLocation = loc:ARCLocation
!            SentToARC# = True
!            loc:ARCDateBooked = lot:TheDate
!            !MESSAGE('TRUE CONDITION MET')
!        Break
!        End!
!
!    End !Loop
!
!    Return SentToARC#



!GetDeliveryDateAtARC        PROCEDURE( IN:JobNumber )! LONG ! BOOL
!First LONG(True)
!    CODE
!        !-----------------------------------------------------------------
!        WriteDebug('GetDeliveryDateAtARC(' & IN:JobNumber & ')')
!
!        LOC:ARCDateBooked = ''
!        First = False
!    SentToARC# = False
!
!        LOOP WHILE LoadLOCATLOG_NewLocationKey( IN:JobNumber, LOC:ARCLocation, First)
!            !-------------------------------------------------------------
!            !First = False
!            IF CLIP(lot:NewLocation) = CLIP(LOC:ARCLocation)
!                WriteDebug('GetDeliveryDateAtARC(OK)"' & CLIP(lot:NewLocation) & '"')
!                LOC:ARCDateBooked = lot:TheDate
!                SentToARC# = True
!
!                RETURN True
!            END !IF
!            !-------------------------------------------------------------
!        END !LOOP
!
!        WriteDebug('GetDeliveryDateAtARC(FAIL)')
!        RETURN False
!        !-----------------------------------------------------------------
GetSubAccount PROCEDURE( IN:AccountNumber )
DoAdd LONG(False)
    CODE
        !-----------------------------------------------------------------
        !WriteDebug('GetSubAccount("' & CLIP(IN:AccountNumber) & '")')
        saQ:HeadAccountNumber = wob:HeadAccountNumber
        saQ:AccountNumber  = IN:AccountNumber
        GET(SubAccount_Queue, +saq:AccountNumber,+saQ:HeadAccountNumber)

        CASE ERRORCODE()
        OF 00 ! FOUND
            IF saQ:AccountNumber  = IN:AccountNumber
                ! Found
                !WriteDebug('GetSubAccount(Found)')

                RETURN
            ELSE
                ! Partial Match - ADD
                DoAdd = True
            END !IF

        OF 30 ! NOT Found
            ! Not in queue - ADD
            DoAdd = True

        ELSE
            CancelPressed = True

            RETURN
        END !CASE
        !-----------------------------------------------------------------
        IF DoAdd
            !WriteDebug('GetSubAccount(NOT Found)')

            CLEAR(SubAccount_Queue)
                saQ:AccountNumber            = IN:AccountNumber

                IF LoadSUBTRACC(IN:AccountNumber)

                    GetHeadAccount(wob:HeadAccountNumber)

                    saQ:AccountName          = sub:Company_Name
                    saQ:HeadAccountNumber    = haQ:AccountNumber
                    saQ:HeadAccountName      = haQ:AccountName
                    saQ:ExcludeBouncer       = haQ:ExcludeBouncer
                    saQ:BranchIdentification = haQ:BranchIdentification

!                    If haQ:InvoiceSubAccounts = True
!                        saQ:PartsVATCode         = sub:Parts_VAT_Code
!                        saQ:LabourVATCode        = sub:Labour_VAT_Code
!                        saQ:RetailVATCode        = sub:Retail_Discount_Code
!
!                        saQ:PartsVATRate         = GetVATRate(sub:Parts_VAT_Code)
!                        saQ:LabourVATRate        = GetVATRate(sub:Labour_VAT_Code)
!                        saQ:RetailVATRate        = GetVATRate(sub:Retail_Discount_Code)
!                    Else
!                        saQ:PartsVATCode         = haQ:PartsVATCode
!                        saQ:LabourVATCode        = haQ:LabourVATCode
!                        saQ:RetailVATCode        = haQ:RetailVATCode
!
!                        saQ:PartsVATRate         = haQ:PartsVATRate
!                        saQ:LabourVATRate        = haQ:LabourVATRate
!                        saQ:RetailVATRate        = haQ:RetailVATRate
!                    End!If tra:use_sub_accounts = 'YES'

                ELSE
                    saq:AccountName          = ''
                    saq:HeadAccountNumber    = ''
                    saQ:HeadAccountName      = ''
                    saQ:BranchIdentification = ''

!                    saQ:PartsVATCode         = ''
!                    saQ:LabourVATCode        = ''
!                    saQ:RetailVATCode        = ''
!
!                    saQ:PartsVATRate         = 0.00
!                    saQ:LabourVATRate        = 0.00
!                    saQ:RetailVATRate        = 0.00
                END !IF
            ADD(SubAccount_Queue, +saq:AccountNumber)
        END !IF
        !-----------------------------------------------------------------
IsBouncer           PROCEDURE()! LONG ! BOOL
CODE
    !-----------------------------------------------------------------
    !WriteDebug('IsBouncer')
    !-----------------------------------------------------------------
    !

    x# = VodacomClass.CountJobBouncers()

    True_Bounce = FALSE

    IF X# > 0
        SORT(glo:Queue20,-GLO:Pointer20)
        GET(glo:Queue20,1)
        Access:Jobs_Alias.ClearKey(job_ali:Ref_Number_Key)
        job_ali:ref_number = glo:Queue20.GLO:Pointer20
        IF Access:Jobs_Alias.Fetch(job_ali:Ref_Number_Key)
            !Error!
            RETURN False
        END
        Access:Jobse.ClearKey(jobe:RefNumberKey)
        jobe:refnumber = job_ali:ref_number
        IF Access:Jobse.Fetch(jobe:RefNumberKey)
            !Error

            RETURN False
        END
        IF jobe:webjob
            Access:WebJob.ClearKey(wob:RefNumberKey)
            wob:RefNumber = job_ali:ref_number
            IF Access:WebJob.Fetch(wob:RefNumberKey)
                !Error!
                RETURN False
            END
            IF job:date_booked - wob:DateJobDespatched > LOC:DayLeawayCount!!< (job:Date_Completed - LOC:DayLeawayCount)

                RETURN False
            ELSE

                Return True
            END
        ELSE
            IF job:date_booked - job_ali:Date_Despatched > LOC:DayLeawayCount!< (job:Date_Completed - LOC:DayLeawayCount)

                RETURN False
            ELSE

                RETURN True
            END
        END
    ELSE

        RETURN False
    END
    !wob:DateJobDespatched

    RETURN False
LoadBOUNCER      PROCEDURE( IN:BouncerJobNumber )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        ! Bouncer_Job_Only_Key     KEY( bou:Bouncer_Job_Number                          ),DUP,NOCASE
        ! Bouncer_Job_Number_Key   KEY( bou:Original_Ref_Number, bou:Bouncer_Job_Number ),NOCASE
        ! Record_Number_Key        KEY( bou:Record_Number                               ),NOCASE,PRIMARY
        !
        Access:BOUNCER.ClearKey(bou:Bouncer_Job_Only_Key)
            bou:Bouncer_Job_Number = IN:BouncerJobNumber
        SET(bou:Bouncer_Job_Only_Key, bou:Bouncer_Job_Only_Key)

        IF Access:BOUNCER.NEXT() <> Level:Benign
           !WriteDebug('LoadBOUNCER(' & IN:BouncerJobNumber & ')=False EOF')

            RETURN False
        END !IF

        IF bou:Bouncer_Job_Number <> IN:BouncerJobNumber
           !WriteDebug('LoadBOUNCER(' & IN:BouncerJobNumber & ')=False EOI')

            RETURN False
        END !IF

       !WriteDebug('LoadBOUNCER(' & IN:BouncerJobNumber & ')=True')

        RETURN True
        !-----------------------------------------------
LoadJOBS      PROCEDURE( IN:JobNumber )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        Access:JOBS.ClearKey(job:Ref_Number_Key)
        job:Ref_Number = IN:JobNumber

        IF Access:JOBSE.Fetch(job:Ref_Number_Key) <> Level:Benign
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
LoadJOBS_ALIAS      PROCEDURE( IN:JobNumber )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        Access:JOBS_ALIAS.ClearKey(job_ali:Ref_Number_Key)
        job_ali:Ref_Number = IN:JobNumber

        IF Access:JOBS_ALIAS.Fetch(job_ali:Ref_Number_Key) <> Level:Benign
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
LoadJOBSE      PROCEDURE( IN:JobNumber )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        Access:JOBSE.ClearKey(jobe:RefNumberKey)
        jobe:RefNumber = IN:JobNumber

        IF Access:JOBSE.Fetch(jobe:RefNumberKey) <> Level:Benign
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
LoadJOBNOTES      PROCEDURE( IN:JobNumber )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
        jbn:RefNumber = IN:JobNumber

        IF Access:JOBNOTES.Fetch(jbn:RefNumberKey) <> Level:Benign
           !WriteDebug('LoadJOBNOTES(' & IN:JobNumber & ') NOT Found')

            RETURN False
        END !IF

       !WriteDebug('LoadJOBNOTES(' & IN:JobNumber & ') Found')

        RETURN True
        !-----------------------------------------------
LoadLOCATLOG   PROCEDURE( IN:JobNumber, IN:First )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        WriteDebug('LoadLOCATLOG(' & IN:JobNumber & ', ' & IN:First & ')')

        IF IN:First
            IN:First = False
            WriteDebug('LoadLOCATLOG(First)')

            Access:LOCATLOG.ClearKey(lot:DateKey)
                lot:RefNumber = IN:JobNumber
            SET(lot:DateKey, lot:DateKey)
        END !IF

        IF Access:LOCATLOG.NEXT() <> Level:Benign
            WriteDebug('LoadLOCATLOG(NEXT=Fail)')

            RETURN False
        END !IF

        IF NOT lot:RefNumber = IN:JobNumber
            WriteDebug('LoadLOCATLOG(NOT Found)')

            RETURN False
        END !IF

        WriteDebug('LoadLOCATLOG(OK)')

        RETURN True
        !-----------------------------------------------
LoadMANFAULO      PROCEDURE( IN:Manufacturer, IN:FieldNumber, IN:Field )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        !WriteDebug('LoadMANFAULO("' & CLIP(IN:Manufacturer) & '", "' & CLIP(IN:FieldNumber) & '", "' & CLIP(IN:Field) & '")! LONG ! BOOL')

        Access:MANFAULO.ClearKey(mfo:Field_Key)
        mfo:Manufacturer = IN:Manufacturer
        mfo:Field_Number = IN:FieldNumber
        mfo:Field        = IN:Field

        IF Access:MANFAULO.Fetch(mfo:Field_Key) <> Level:Benign
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
LoadSUBTRACC                            PROCEDURE( IN:SubAccountNumber )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
        sub:Account_Number = IN:SubAccountNumber

        IF Access:SUBTRACC.TryFetch(sub:Account_Number_Key)
            Return False
        END !IF

        IF sub:Account_Number <> IN:SubAccountNumber
            Return False
        END !IF

        Return True
        !-----------------------------------------------

LoadTRADEACC                    PROCEDURE( IN:HeadAccountNumber )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = IN:HeadAccountNumber

        IF Access:TRADEACC.TryFetch(tra:Account_Number_Key)
            RETURN False
        END !IF

        IF tra:Account_Number <> IN:HeadAccountNumber
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
LoadUSERS                         PROCEDURE( IN:UserCode )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        Access:USERS.Clearkey(use:User_Code_Key)
        use:User_Code = IN:UserCode

        IF Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
            RETURN True
        End! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign

        RETURN False
        !-----------------------------------------------
PrintIMEI        PROCEDURE( IN:IMEI )! LONG ! BOOL
DoADD LONG(False)
    CODE
        !-----------------------------------------------
        !WriteDebug('PrintIMEI()')

        IF CLIP(job:ESN) = ''
            !WriteDebug('PrintIMEI(FAIL "")')

            RETURN False ! suppress printing

        ELSIF CLIP(job:ESN) = '000000000000000'
            !WriteDebug('PrintIMEI(FAIL "000000000000000")')

            RETURN False ! suppress printing

        END !IF
        !-----------------------------------------------
        CLEAR(IMEI_Queue)
            imeiQ:IMEI = IN:IMEI
        GET(IMEI_Queue, +imeiQ:IMEI)

        CASE ERRORCODE()
        OF 00
            IF imeiQ:IMEI = IN:IMEI
                !WriteDebug('PrintIMEI(FAIL Already Printed)')

                RETURN False ! Already Printed
            END !IF

            ! NOT Found - Partial Match
            DoADD = True

        OF 30
            ! NOT Found - Add
            DoADD = True

        ELSE
            ! ERROR
            !WriteDebug('PrintIMEI(CancelPressed = True), Error(' & ERRORCODE() & '), "' & ERROR() & '"')

            CancelPressed = True

            RETURN False
        END !CASE
        !-----------------------------------------------
        IF DoADD = True
            CLEAR(IMEI_Queue)
                imeiQ:IMEI = job:ESN
            ADD(IMEI_Queue, +imeiQ:IMEI)
        END !IF

        !WriteDebug('PrintIMEI(True "' & CLIP(job:ESN) & '")')

        RETURN True
        !-----------------------------------------------
SetSheetTo              PROCEDURE( IN:AccountName )
DoADD LONG(False)
    CODE
        !-----------------------------------------------
        !WriteDebug('SetSheetTo("' & CLIP(IN:AccountName) & '")')

        CLEAR(Sheet_Queue)
            shQ:AccountName = IN:AccountName
        GET(Sheet_Queue, +shQ:AccountName)

        CASE ERRORCODE()
        OF 00
            IF shQ:AccountName = IN:AccountName
                ! Found
                Excel{'Sheets("' & CLIP(shQ:SheetName) & '").Select'}
                !WriteDebug('SetSheetTo(Found "' & CLIP(shQ:SheetName) & '")"' & CLIP(Excel{'ActiveSheet.Name'}) & '"')

                RETURN
            ELSE
                ! NOT Found, Partial Match - ADD
                DoADD = True
            END !IF

        OF 30
            ! NOT Found - ADD
            DoADD = True

        ELSE
            CancelPressed = True

            RETURN
        END !CASE
        !-----------------------------------------------
        IF DoADD = True
            DO XL_AddSheet

            LOC:SectionName   = CLIP(IN:AccountName)
            excel:CommentText = ''
            DO CreateSectionHeader

            DO SetColumns

            CLEAR(Sheet_Queue)
                shQ:AccountName = IN:AccountName
                shQ:SheetName   = Excel{'ActiveSheet.Name'}
            ADD(Sheet_Queue, +shQ:AccountName)

            !WriteDebug('SetSheetTo(DoADD "' & CLIP(shQ:AccountName) & '")"' & CLIP(shQ:SheetName) & '"')

            RETURN
        END !IF
        !-----------------------------------------------
        !WriteDebug('SetSheetTo(Unreachable ERROR "' & CLIP(IN:AccountNumber) & '")"' & CLIP(Excel{'ActiveSheet.Name'}) & '"')
        !-----------------------------------------------
        RETURN
UpdateResultsQueue          PROCEDURE( IN:ThirdPartSite, IN:CountSent, IN:CountBounced )
DoADD LONG(False)
    CODE
        !-----------------------------------------------
        !WriteDebug('UpdateResultsQueue(' & CLIP(job:Third_Party_Site) & ')')

        Clear(ResultsQueue)
            resQ:CompanyName  = IN:ThirdPartSite
        Get(ResultsQueue, +resQ:CompanyName)

        CASE ERRORCODE()
        OF 00
            IF resQ:CompanyName  = IN:ThirdPartSite
                ! Found
                DoADD = False
            ELSE
                ! NOT Found Partial Match - ADD
                DoADD = True
            END !IF

        OF 30
            ! NOT Found - ADD
            DoADD = True

        Else !If Error()
            !WriteDebug('UpdateResultsQueue(CancelPressed = True)')

            CancelPressed = True

            RETURN
        End !CASE
        !-----------------------------------------------
        IF DoADD = True
            !WriteDebug('UpdateResultsQueue(ADD"' & CLIP(IN:ThirdPartSite) & '")')

            CLEAR(ResultsQueue)
                resQ:CompanyName = IN:ThirdPartSite
            Add(ResultsQueue, +resQ:CompanyName)
        END !IF
        !-----------------------------------------------
        resQ:JobsSent    += IN:CountSent
        resQ:JobsBounced += IN:CountBounced

        Put(ResultsQueue)
        !-----------------------------------------------
UpdateManufacturerQueue          PROCEDURE( IN:Manufacturer, IN:CountRepaired, IN:CountBounced )
DoADD LONG(False)
    CODE
        !-----------------------------------------------
        WriteDebug('UpdateManufacturerQueue(' & CLIP(IN:Manufacturer) & ')')

        Clear(ManufacturerQueue)
            manQ:Manufacturer  = IN:Manufacturer
        Get(ManufacturerQueue, manQ:Manufacturer)

        CASE ERRORCODE()
        OF 00
            IF manQ:Manufacturer  = IN:Manufacturer
                ! Found
            ELSE
                ! NOT Found Partial Match - ADD
                DoADD = True
            END !IF

        OF 30
            ! NOT Found - ADD
            DoADD = True

        Else !If Error()
            !WriteDebug('UpdateManufacturerQueue(CancelPressed = True)')

            CancelPressed = True

            RETURN
        End !CASE
        !-----------------------------------------------
        IF DoADD = True
            WriteDebug('UpdateManufacturerQueue(ADD"' & CLIP(IN:Manufacturer) & '")')

            CLEAR(ManufacturerQueue)
                manQ:Manufacturer  = IN:Manufacturer
                !---------------------------------------
                ! Field_Number_Key KEY( maf:Manufacturer, maf:Field_Number ),NOCASE,PRIMARY
                ! MainFaultKey     KEY( maf:Manufacturer, maf:MainFault    ),DUP,NOCASE
                ! InFaultKey       KEY( maf:Manufacturer, maf:InFault      ),DUP,NOCASE
                !---------------------------------------
                Access:MANFAULT.ClearKey(maf:Field_Number_Key)
                    maf:Manufacturer = IN:Manufacturer
                SET(maf:Field_Number_Key, maf:Field_Number_Key)

                LOOP WHILE Access:MANFAULT.NEXT() = level:Benign
                    IF NOT maf:Manufacturer = IN:Manufacturer
                        BREAK
                    End !IF

                    IF  maf:InFault = True ! maf:Field_Name = 'In Fault Code'
                        manQ:InFaultFieldNumber = maf:Field_Number
                        !WriteDebug('UpdateManufacturerQueue(' & CLIP(maf:Field_Name) & ')="' & CLIP(maf:Field_Number) & '"')

                    END !IF
                    IF maf:MainFault = True ! maf:Field_Name = 'Out Fault Code'
                        manQ:OutFaultFieldNumber = maf:Field_Number
                        !WriteDebug('UpdateManufacturerQueue(' & CLIP(maf:Field_Name) & ')="' & CLIP(maf:Field_Number) & '"')

                    END !IF
                END !LOOP
                !---------------------------------------
            Add(ManufacturerQueue, +manQ:Manufacturer)
        END !IF
        !-----------------------------------------------
        manQ:JobsRepaired += IN:CountRepaired
        manQ:JobsBounced  += IN:CountBounced

        Put(ManufacturerQueue)
        !-----------------------------------------------

UpdateModelQueue          PROCEDURE( IN:Manufacturer, IN:ModelNumber, IN:CountRepaired, IN:CountBounced )
DoADD LONG(False)
    CODE
        !-----------------------------------------------
        !WriteDebug('UpdateModelQueue(' & CLIP(IN:ModelNumber) & ')')

        Clear(ModelQueue)
            modQ:Manufacturer = IN:Manufacturer
            modQ:ModelNumber  = IN:ModelNumber
        Get(ModelQueue, modQ:ModelNumber)

        CASE ERRORCODE()
        OF 00
            IF modQ:Manufacturer = IN:Manufacturer AND modQ:ModelNumber  = IN:ModelNumber
                ! Found
                DoADD = False

            ELSE
                ! NOT Found Partial Match - ADD
                DoADD = True
            END !IF

        OF 30
            ! NOT Found - ADD
            DoADD = True

        Else !If Error()
            !WriteDebug('UpdateModelQueue(CancelPressed = True)')

            CancelPressed = True

            RETURN
        End !CASE
        !-----------------------------------------------
        IF DoADD = True
            !WriteDebug('UpdateModelQueue(ADD"' & CLIP(IN:ModelNumber) & '")')

            CLEAR(ModelQueue)
                modQ:Manufacturer = IN:Manufacturer
                modQ:ModelNumber  = IN:ModelNumber
            Add(ModelQueue, +modQ:ModelNumber)
        END !IF
        !-----------------------------------------------
        modQ:JobsRepaired += IN:CountRepaired
        modQ:JobsBounced  += IN:CountBounced

        Put(ModelQueue)
        !-----------------------------------------------

UpdateEntityQueue          PROCEDURE( IN:Entity, IN:CountRepaired, IN:CountBounced )
DoADD LONG(False)
    CODE
        !-----------------------------------------------
        !WriteDebug('UpdateEntityQueue(' & CLIP(IN:Entity) & ')')

        Clear(EntityQueue)
            entQ:Entity  = IN:Entity
        Get(EntityQueue, +entQ:Entity)

        CASE ERRORCODE()
        OF 00
            IF entQ:Entity  = IN:Entity
                ! Found
                DoADD = False
            ELSE
                ! NOT Found Partial Match - ADD
                DoADD = True
            END !IF

        OF 30
            ! NOT Found - ADD
            DoADD = True

        Else !If Error()
            !WriteDebug('UpdateEntityQueue(CancelPressed = True)')

            CancelPressed = True

            RETURN
        End !CASE
        !-----------------------------------------------
        IF DoADD = True
            !WriteDebug('UpdateEntityQueue(ADD"' & CLIP(IN:Entity) & '")')

            CLEAR(EntityQueue)
                entQ:Entity  = IN:Entity
            Add(EntityQueue, +entQ:Entity)
        END !IF
        !-----------------------------------------------
        entQ:JobsRepaired += IN:CountRepaired
        entQ:JobsBounced  += IN:CountBounced

        Put(EntityQueue)
        !-----------------------------------------------

UpdateEngineerQueue          PROCEDURE( IN:HeadAccount, IN:Engineer, IN:CountRepaired, IN:CountBounced )
DoADD LONG(False)
    CODE
        !-----------------------------------------------
        !WriteDebug('UpdateEngineerQueue(' & CLIP(IN:Engineer) & ')')
        SORT(EngineerQueue, +engQ:HeadAccount, +engQ:Engineer)
        Clear(EngineerQueue)
            engQ:HeadAccount = IN:HeadAccount
            engQ:Engineer    = IN:Engineer
        Get(EngineerQueue, +engQ:HeadAccount, +engQ:Engineer)

        CASE ERRORCODE()
        OF 00
!            IF (CLIP(engQ:HeadAccount) = CLIP(IN:HeadAccount)) AND (CLIP(engQ:Engineer) = CLIP(IN:Engineer))
!                ! Found
!            ELSE
!                ! NOT Found Partial Match - ADD
!                DoADD = True
!            END !IF

        OF 30
            ! NOT Found - ADD
            DoADD = True

        Else !If Error()
            !WriteDebug('UpdateEngineerQueue(CancelPressed = True)')

            CancelPressed = True

            RETURN
        End !CASE
        !-----------------------------------------------
        IF DoADD = True
            !WriteDebug('UpdateEngineerQueue(ADD"' & CLIP(IN:Engineer) & '")')

            CLEAR(EngineerQueue)
                engQ:HeadAccount  = IN:HeadAccount
                engQ:Engineer     = IN:Engineer
                engQ:EngineerName = IN:Engineer

                IF (CLIP(IN:HeadAccount) = 'THIRD PARTY')
                    ! NULL
                ELSIF LoadUSERS(IN:Engineer)
                    engQ:EngineerName = AppendString(use:Surname, use:Forename, ', ')
                    IF CLIP(engQ:EngineerName) = ''
                        engQ:EngineerName = IN:Engineer
                    END !IF
                END !IF
            Add(EngineerQueue, +engQ:HeadAccount, +engQ:Engineer)
        END !IF
        !-----------------------------------------------
        engQ:JobsRepaired += IN:CountRepaired
        engQ:JobsBounced  += IN:CountBounced

        Put(EngineerQueue)
        !-----------------------------------------------
NumberToColumn PROCEDURE( IN:Long )!STRING
FirstCol STRING(1)
Temp     LONG
    CODE
        !-----------------------------------------------
        Temp = IN:Long - 1

        FirstCol = Temp / 26
        IF FirstCol = 0
            RETURN CHR(Temp+65)
        END !IF

        RETURN CHR(FirstCol+64) & CHR( (Temp % 26)+65 )
        !-----------------------------------------------
WriteColumn PROCEDURE( IN:String, IN:StartNewRow )
Temp STRING(255)
    CODE
        !-----------------------------------------------
        Temp = IN:String
        IF CLIP(Temp) = ''
            Temp = ''' '
        END !IF
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            IF IN:StartNewRow
                clip:Value = Temp
            ELSE
                clip:Value = CLIP(clip:Value) & '<09>' & Temp
            END !IF

            RETURN
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = Temp

        DO XL_ColRight
        !-----------------------------------------------
WriteDebug      PROCEDURE( IN:Message )
    CODE
        !-----------------------------------------------
        !IF NOT debug:Active
            RETURN
       ! END !IF

        debug:Count += 1

        PUTINI(CLIP(LOC:ProgramName), debug:Count, IN:Message, 'C:\Debug.ini')
        !-----------------------------------------------
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW5.Fetch PROCEDURE(BYTE Direction)

GreenBarIndex   LONG
  CODE
  PARENT.Fetch(Direction)


BRW5.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = tra:Account_Number
     GET(glo:Queue2,glo:Queue2.Pointer2)
    IF ERRORCODE()
      LocalTag = ''
    ELSE
      LocalTag = 'Y'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  
  IF (LocalTag = 'Y')
    SELF.Q.LocalTag_Icon = 2                               ! Set icon from icon list
  ELSE
    SELF.Q.LocalTag_Icon = 1                               ! Set icon from icon list
  END


BRW5.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW5.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW5.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW5::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW5::RecordStatus=ReturnValue
  IF BRW5::RecordStatus NOT=Record:OK THEN RETURN BRW5::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = tra:Account_Number
     GET(glo:Queue2,glo:Queue2.Pointer2)
    EXECUTE DASBRW::6:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW5::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW5::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW5::RecordStatus
  RETURN ReturnValue

XFiles PROCEDURE                                           ! Generated from procedure template - Window

QuickWindow          WINDOW('Window'),AT(,,260,160),FONT('Tahoma',8,COLOR:Black,FONT:regular,CHARSET:ANSI),CENTER,IMM,HLP('XFiles'),SYSTEM,GRAY,RESIZE
                       BUTTON('&OK'),AT(140,140,56,16),USE(?Ok),MSG('Accept operation'),TIP('Accept Operation')
                       BUTTON('&Cancel'),AT(200,140,56,16),USE(?Cancel),MSG('Cancel Operation'),TIP('Cancel Operation')
                     END

Bryan       Class
CompFieldColour       Procedure()
            End
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED                 ! Method added to host embed code
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False) ! Method added to host embed code
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('XFiles')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Ok
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Ok,RequestCancelled)                    ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Ok,RequestCompleted)                    ! Add the close control to the window manger
  END
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:DEFAULTV.Open                                     ! File DEFAULTV used by this procedure, so make sure it's RelationManager is open
  Relate:JOBSE.Open                                        ! File JOBSE used by this procedure, so make sure it's RelationManager is open
  Relate:JOBSE_ALIAS.Open                                  ! File JOBSE_ALIAS used by this procedure, so make sure it's RelationManager is open
  Relate:MULDESP.SetOpenRelated()
  Relate:MULDESP.Open                                      ! File MULDESP used by this procedure, so make sure it's RelationManager is open
  Relate:SRNTEXT.Open                                      ! File SRNTEXT used by this procedure, so make sure it's RelationManager is open
  Access:MULDESPJ.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(QuickWindow)                                   ! Open window
  Bryan.CompFieldColour()
  Do DefineListboxStyle
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('XFiles',QuickWindow)                       ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTV.Close
    Relate:JOBSE.Close
    Relate:JOBSE_ALIAS.Close
    Relate:MULDESP.Close
    Relate:SRNTEXT.Close
  END
  IF SELF.Opened
    INIMgr.Update('XFiles',QuickWindow)                    ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

HelpBrowser PROCEDURE (func:Path)                          ! Generated from procedure template - Window

fepath               STRING(255)                           !
Window               WINDOW('Online Help'),AT(522,0,154,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(09A6A7CH),TILED,SYSTEM,GRAY,MAX,RESIZE,IMM
                       BUTTON,AT(0,0),USE(?Button1),TRN,FLAT,ICON('closep.jpg'),STD(STD:Close)
                       PROMPT('Close Online Help'),AT(72,8),USE(?Prompt1),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(-3,25,159,404),USE(?feControl)
                     END

Bryan       Class
CompFieldColour       Procedure()
            End
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED                 ! Method added to host embed code
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Open                   PROCEDURE(),DERIVED                 ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False) ! Method added to host embed code
Resize                 PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END


                     ! Start: FE Class Declaration
ThisViewer1          class(FeBrowser)
                     END
                     !End: FE Class Declaration


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
feGetFileFromUser_ThisViewer1  routine
  ThisViewer1.fe_AppPath = Path()           ! Used to restore current path after selection
  if ThisViewer1.fe_LastPath <> ''          ! Var set below
    SetPath (ThisViewer1.fe_LastPath)       ! Set path to location of last selected file
  end
  ThisViewer1.fe_RestorePath = ThisViewer1.fe_Path  ! current document's path
  ThisViewer1.fe_Path = ''
  if not FileDialog('Choose File to View', ThisViewer1.fe_Path, 'HTML Files|*.htm;*.html;*.mht|Text Files|*.txt|Image Files|*.gif;*.jpeg;*.jpg;*.png;*.art;*.au;*.aiff;*.xbm|XML Files|*.xml|EML Files|*.eml|All Files|*.*', 10000b)
    SetPath (ThisViewer1.fe_AppPath)        ! Restore path to app path if cancelled
    ThisViewer1.fe_Path = ThisViewer1.fe_RestorePath
  else
    ThisViewer1.fe_LastPath = Path()        ! Path of selected file
    SetPath (ThisViewer1.fe_AppPath)        ! Restore path
    ThisViewer1.Load (ThisViewer1.fe_Path)  ! Load the selected file
  end
 exit

fePathControlAccepted_ThisViewer1  routine
  ThisViewer1.fe_Path = ''
  ThisViewer1.Load (ThisViewer1.fe_Path)
  exit

feWindowClosing_ThisViewer1  routine
  ThisViewer1.Kill(2)  ! GJxxyy123 WIP test code, 5 Oct 2005
 exit

feRecoveryCode_ThisViewer1  routine
 ! This routine is obsolete.  Please read the docs or contact support@capesoft.com
 exit

feLoadInitialDocument_ThisViewer1  routine
  ! FileExplorer - Loading initial document as window opens
  ThisViewer1.fe_Path = fepath
  ThisViewer1.Load (ThisViewer1.fe_Path)
 exit

feWindowTakeFieldEvent_fePathControl_EventAlertKey_ThisViewer1  routine
  case keycode()
    of EnterKey
      post(event:accepted, )
    of CtrlEnter
      post(event:accepted, )
  end
 exit



ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('HelpBrowser')
  fepath = Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & Clip(func:Path) & '.htm'
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  ThisViewer1.TempDirLocation = 0    ! temp files stored with the exe
  
   ! The feBrowser object uses "Early Binding" at this stage.  At some point we might
   ! make it optional to use "Late Binding" instead.  This switch sets which approach
   ! we use.  Don't change it for now...
   ! ThisViewer1.UseLateBinding = Glo:UseLateBinding
  
  
   ! Uncomment this line of code to override the above code where the property is set...
   ! For testing purposes...  Set the switch in the ProgramOptions screen (disabled by default).
   ! ThisViewer1.AutoDialDefaultInternetConnection = Glo:DUNAutoDialOption
   ! 0 - do not dial the default dial up connection
   ! 1 - dial the "Windows Default" connection
   ! 2 - establish the default connection, then dial it
  
   ThisViewer1.BetaCallbacksOn = 0  ! GJxxyy FILEEXPLORER TEST CODE
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Button1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.Open(Window)                                        ! Open window
  Bryan.CompFieldColour()
  Do DefineListboxStyle
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Spread)                         ! Controls will spread out as the window gets bigger
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('HelpBrowser',Window)                       ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.Opened
    INIMgr.Update('HelpBrowser',Window)                    ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Open PROCEDURE

  CODE
  PARENT.Open
  ThisViewer1.Init (?feControl, , , , 1)


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
   ThisViewer1.TakeEvent()
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  
    OF EVENT:feCallback
      ! The File Explorer dll will post this event if you return fe:DivertNavigation from the
      ! (virtual) method ThisViewer::EventCallback.
  
    OF EVENT:feDelayedClose  ! GJxxyy123 WIP added 051005
      ThisViewer1.Kill()
  
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
        ThisViewer1._WindowClosing()  ! GJ added 280205
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:CloseWindow
      do feWindowClosing_ThisViewer1
    OF EVENT:OpenWindow
      do feLoadInitialDocument_ThisViewer1
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window
  SELF.SetStrategy(?Button1, Resize:FixLeft+Resize:FixTop, Resize:LockSize) ! Override strategy for ?Button1
  SELF.SetStrategy(?feControl, Resize:FixLeft+Resize:FixTop, Resize:ConstantRight+Resize:ConstantBottom) ! Override strategy for ?feControl
  SELF.SetStrategy(?Prompt1, Resize:FixLeft+Resize:FixTop, Resize:LockSize) ! Override strategy for ?Prompt1


Resizer.Resize PROCEDURE

ReturnValue          BYTE,AUTO


  CODE
  ReturnValue = PARENT.Resize()
  ! Auto-Resizing the File Explorer COM Object
  ThisViewer1.Resize ()
  RETURN ReturnValue


