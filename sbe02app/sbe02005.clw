

   MEMBER('sbe02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE02005.INC'),ONCE        !Local module procedure declarations
                     END


Loan_Order_Window PROCEDURE (strLocation)             !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:Manufacturer     STRING(30)
tmp:Model_Number     STRING(30)
tmp:Qty_Required     LONG
tmp:Notes            STRING(255)
tmp:CurrentCount     LONG
ModelQueue           QUEUE,PRE(modque)
ModelNumber          STRING(30)
Quantity             LONG
Count                LONG
Manufacturer         STRING(30)
                     END
window               WINDOW('Order Loan Units'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Order Loan Units'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Order Loan Units'),USE(?Tab1)
                           PROMPT('Manufacturer'),AT(205,116),USE(?tmp:Manufacturer:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(281,116,124,10),USE(tmp:Manufacturer),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(409,111),USE(?CallLookup),TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Model'),AT(205,135),USE(?tmp:Model_Number:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Quantity Required'),AT(205,158),USE(?tmp:Qty_Required:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           SPIN(@s8),AT(281,156,,10),USE(tmp:Qty_Required),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,RANGE(1,9999999),STEP(1)
                           LIST,AT(256,239,164,83),USE(?List1),VSCROLL,COLOR(COLOR:White),FORMAT('120L|M~Model Number~L(2)@s30@32L|M~Quantity~L(2)@s8@'),FROM(ModelQueue)
                           BUTTON,AT(413,190),USE(?OkButton),TRN,FLAT,LEFT,ICON('ordunitp.jpg'),DEFAULT
                           LINE,AT(255,223,169,0),USE(?Line1),COLOR(COLOR:White),LINEWIDTH(2)
                           PROMPT('Models Ordered'),AT(256,230),USE(?Prompt7),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Notes'),AT(205,178),USE(?Notes:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           TEXT,AT(281,178,123,36),USE(tmp:Notes),VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           ENTRY(@s30),AT(281,135,124,10),USE(tmp:Model_Number),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(409,132),USE(?CallLookup:2),TRN,FLAT,ICON('lookupp.jpg')
                         END
                       END
                       BUTTON,AT(448,332),USE(?CancelButton),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
PassModelQueue  Queue(DefModelQueue)
                End
!Save Entry Fields Incase Of Lookup
look:tmp:Manufacturer                Like(tmp:Manufacturer)
look:tmp:Model_Number                Like(tmp:Model_Number)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020308'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Loan_Order_Window')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:LOAORDR.Open
  Relate:MANUFACT.Open
  Access:MODELNUM.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ?List1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?tmp:Manufacturer{Prop:Tip} AND ~?CallLookup{Prop:Tip}
     ?CallLookup{Prop:Tip} = 'Select ' & ?tmp:Manufacturer{Prop:Tip}
  END
  IF ?tmp:Manufacturer{Prop:Msg} AND ~?CallLookup{Prop:Msg}
     ?CallLookup{Prop:Msg} = 'Select ' & ?tmp:Manufacturer{Prop:Msg}
  END
  IF ?tmp:Model_Number{Prop:Tip} AND ~?CallLookup:2{Prop:Tip}
     ?CallLookup:2{Prop:Tip} = 'Select ' & ?tmp:Model_Number{Prop:Tip}
  END
  IF ?tmp:Model_Number{Prop:Msg} AND ~?CallLookup:2{Prop:Msg}
     ?CallLookup:2{Prop:Msg} = 'Select ' & ?tmp:Model_Number{Prop:Msg}
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LOAORDR.Close
    Relate:MANUFACT.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_Manufacturers
      SelectModelKnownMake
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?OkButton
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      if tmp:Manufacturer = ''
          select(?tmp:Manufacturer)
          cycle
      end
      
      if tmp:Model_Number = ''
          select(?tmp:Model_Number)
          cycle
      end
      
      if tmp:Qty_Required = 0
          select(?tmp:Qty_Required)
          cycle
      end
      
      if not Access:LOAORDR.PrimeRecord()
          lor:Location = strLocation
          lor:Manufacturer = tmp:Manufacturer
          lor:Model_Number = tmp:Model_Number
          lor:Qty_Required = tmp:Qty_Required
          lor:Qty_Received = 0
          lor:Notes = Upper(tmp:Notes)
          if Access:LOAORDR.TryUpdate() = Level:Benign
              !Add to temp queue
              modque:ModelNumber  = tmp:Model_Number
              modque:Quantity     = tmp:Qty_Required
              modque:Count        = tmp:CurrentCount
              modque:Manufacturer = tmp:Manufacturer
              Add(ModelQueue)
              tmp:CurrentCount += 1
              Sort(ModelQueue,-modque:Count)
      
              !Add to queue, and start again
              tmp:Manufacturer = ''
              tmp:Model_Number = ''
              tmp:Qty_Required = 0
              tmp:Notes = ''
              Display()
              Select(?tmp:Manufacturer)
          end
      end
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    OF ?tmp:Model_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Model_Number, Accepted)
      glo:Select2 = tmp:Manufacturer
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Model_Number, Accepted)
    OF ?CallLookup:2
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CallLookup:2, Accepted)
      glo:Select2 = tmp:Manufacturer
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CallLookup:2, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020308'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020308'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020308'&'0')
      ***
    OF ?tmp:Manufacturer
      IF tmp:Manufacturer OR ?tmp:Manufacturer{Prop:Req}
        man:Manufacturer = tmp:Manufacturer
        !Save Lookup Field Incase Of error
        look:tmp:Manufacturer        = tmp:Manufacturer
        IF Access:MANUFACT.TryFetch(man:Manufacturer_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tmp:Manufacturer = man:Manufacturer
          ELSE
            !Restore Lookup On Error
            tmp:Manufacturer = look:tmp:Manufacturer
            SELECT(?tmp:Manufacturer)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup
      ThisWindow.Update
      man:Manufacturer = tmp:Manufacturer
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          tmp:Manufacturer = man:Manufacturer
          Select(?+1)
      ELSE
          Select(?tmp:Manufacturer)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:Manufacturer)
    OF ?tmp:Model_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Model_Number, Accepted)
      !glo:Select2 = ''
      IF tmp:Model_Number OR ?tmp:Model_Number{Prop:Req}
        mod:Model_Number = tmp:Model_Number
        mod:Manufacturer = tmp:Manufacturer
        GLO:Select2 = tmp:Manufacturer
        !Save Lookup Field Incase Of error
        look:tmp:Model_Number        = tmp:Model_Number
        IF Access:MODELNUM.TryFetch(mod:Manufacturer_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            tmp:Model_Number = mod:Model_Number
          ELSE
            CLEAR(mod:Manufacturer)
            CLEAR(GLO:Select2)
            !Restore Lookup On Error
            tmp:Model_Number = look:tmp:Model_Number
            SELECT(?tmp:Model_Number)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Model_Number, Accepted)
    OF ?CallLookup:2
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CallLookup:2, Accepted)
      !glo:Select2 = ''
      mod:Model_Number = tmp:Model_Number
      
      IF SELF.RUN(2,Selectrecord)  = RequestCompleted
          tmp:Model_Number = mod:Model_Number
          Select(?+1)
      ELSE
          Select(?tmp:Model_Number)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:Model_Number)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CallLookup:2, Accepted)
    OF ?CancelButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
      If Records(ModelQueue)
          Case Missive('Print Orders Created?','ServiceBase 3g',|
                         'mquest.jpg','\No|/Yes')
              Of 2 ! Yes Button
                  Loop x# = 1 To Records(ModelQueue)
                      Get(ModelQueue,x#)
                      PassModelQueue.Manufacturer = modque:Manufacturer
                      PassModelQueue.ModelNumber  = modque:ModelNumber
                      PassModelQueue.Quantity     = modque:Quantity
                      Add(PassModelQueue)
                  End !Loop x# = 1 To Records(ModelQueue)
                  LoanOrders(PassModelQueue)
              Of 1 ! No Button
          End ! Case Missive
      End !Records(ModelQueue)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Rapid_Loan_Unit PROCEDURE                             !Generated from procedure template - Window

tmp:StockType        STRING(30)
save_loa_ali_id      USHORT,AUTO
tmp:count            LONG
tmp:UnitQueue        QUEUE,PRE(untque)
esn                  STRING(16)
number               LONG
                     END
tmp:ModelNumber      STRING(30)
tmp:Location         STRING(30)
tmp:ShelfLocation    STRING(30)
tmp:Manufacturer     STRING(30)
tmp:MSN              STRING(30)
tmp:esn              STRING(16)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:AllocateAccessories BYTE(0)
window               WINDOW('Rapid Loan Item Inserting'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       SHEET,AT(164,82,352,248),USE(?Sheet1),BELOW,COLOR(0D6E7EFH),SPREAD
                         TAB,USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Common Fields'),AT(235,102),USE(?Prompt2),FONT(,,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Insert the fields that will be common to all the inserted Loan Units.'),AT(235,116,204,24),USE(?Prompt3),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(303,158,112,10),USE(tmp:StockType),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(419,154),USE(?CallLookup),TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Manufacturer'),AT(235,178),USE(?Prompt4),FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(303,178,112,10),USE(tmp:Manufacturer),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           BUTTON,AT(419,174),USE(?CallLookup:2),TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Stock Type'),AT(235,158),USE(?Prompt4:2),FONT(,,,FONT:bold,CHARSET:ANSI)
                           PROMPT('Model Number'),AT(235,198),USE(?Prompt4:3),FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(303,198,112,10),USE(tmp:ModelNumber),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(419,194),USE(?CallLookup:3),TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Location'),AT(235,218),USE(?Prompt4:4),FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(303,218,112,10),USE(tmp:Location),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           BUTTON,AT(419,214),USE(?CallLookup:4),TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Shelf Location'),AT(235,238),USE(?Prompt4:5),FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(303,238,112,10),USE(tmp:ShelfLocation),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           BUTTON,AT(419,234),USE(?CallLookup:5),TRN,FLAT,ICON('lookupp.jpg')
                           CHECK('Allocate Accessories'),AT(303,254),USE(tmp:AllocateAccessories),FONT(,,,FONT:bold,CHARSET:ANSI),MSG('Allocate Accessories'),TIP('Allocate Accessories'),VALUE('1','0')
                         END
                         TAB('Tab 2'),USE(?Tab2),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Create New Units'),AT(242,102),USE(?Prompt10),FONT(,,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Insert an I.M.E.I. to insert the unit into the Loan Stock.'),AT(242,112),USE(?Prompt9),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Units Successfully inserted.'),AT(276,178),USE(?Prompt9:2),FONT(,,0D0FFD0H,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('I.M.E.I. Number'),AT(242,138),USE(?tmp:esn:Prompt),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s16),AT(314,138,124,10),USE(tmp:esn),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('M.S.N.'),AT(242,154),USE(?tmp:MSN:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(314,154,124,10),USE(tmp:MSN),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('M.'),TIP('M.'),UPR
                           LIST,AT(276,188,128,126),USE(?List1),VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),FORMAT('64L(2)|M~I.M.E.I. Number~@s16@'),FROM(tmp:UnitQueue)
                         END
                       END
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Rapid Loan Insert Wizard'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                       BUTTON,AT(236,332),USE(?Next),TRN,FLAT,LEFT,ICON('nextp.jpg')
                       BUTTON,AT(168,332),USE(?back),DISABLE,TRN,FLAT,LEFT,ICON('backp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
look:tmp:StockType                Like(tmp:StockType)
look:tmp:Manufacturer                Like(tmp:Manufacturer)
look:tmp:ModelNumber                Like(tmp:ModelNumber)
look:tmp:Location                Like(tmp:Location)
look:tmp:ShelfLocation                Like(tmp:ShelfLocation)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
AddUnit     Routine

    error# = 0

    setcursor(cursor:wait)
    save_loa_ali_id = access:loan_alias.savefile()
    access:loan_alias.clearkey(loa_ali:esn_only_key)
    loa_ali:esn = tmp:esn
    set(loa_ali:esn_only_key,loa_ali:esn_only_key)
    loop
        if access:loan_alias.next()
           break
        end !if
        if loa_ali:esn <> tmp:esn      |
            then break.  ! end if
        yldcnt# += 1
        if yldcnt# > 25
           yield() ; yldcnt# = 0
        end !if
        Case Missive('This I.M.E.I. Number already exists in Loan Stock.','ServiceBase 3g',|
                       'mstop.jpg','/OK')
            Of 1 ! OK Button
        End ! Case Missive
        error# = 1
    end !loop
    access:loan_alias.restorefile(save_loa_ali_id)
    setcursor()

    If error# = 0
        access:exchange.clearkey(xch:esn_only_key)
        xch:esn = tmp:esn
        if access:exchange.fetch(xch:esn_only_key) = Level:Benign
            Case Missive('This I.M.E.I. Number has already been entered as an Exchange Stock Item.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
            error# = 1
        End!if access:loan.fetch(loa:esn_key) = Level:Benign
    End!If error# = 0
    If error# = 0
        access:jobs_alias.clearkey(job_ali:esn_key)
        job_ali:esn = tmp:esn
        if access:jobs_alias.fetch(job_ali:esn_key) = Level:Benign
           If job_ali:date_completed = ''
                error# = 1
                Case Missive('You are attempting to insert an Exchange Unit with the same I.M.E.I. Number as job number ' & Clip(job_ali:Ref_Number) & '.'&|
                  '<13,10>'&|
                  '<13,10>You must use Rapid Job Update if you wish to return this unit to stock.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
           End!If job:date_completed <> ''
        end!if access:jobs.fetch(job:esn_key) = Level:Benign
    end!IF error# = 0
    If error# = 0
        If CheckLength('IMEI',tmp:ModelNumber,tmp:ESN)
            Error# = 1
        End !If CheckLength('IMEI',tmp:ModelNumber,tmp:ESN)
        If ?tmp:MSN{prop:Hide} = 0 And Error# = 0
            If CheckLength('MSN',tmp:ModelNumber,tmp:MSN)
                Error# = 1
            End !If CheckLength('MSN',tmp:ModelNumber,tmp:MSN)
        End !If ?tmp:MSN{prop:Hide} = 0
!
!        access:modelnum.clearkey(mod:model_number_key)
!        mod:model_number = tmp:modelnumber
!        if access:modelnum.fetch(mod:model_number_key) = Level:benign
!            If Len(Clip(tmp:esn)) < mod:esn_length_from Or Len(Clip(tmp:esn)) > mod:esn_length_to
!                Case MessageEx('The E.S.N. / I.M.E.I. entered should be between '&clip(mod:esn_length_from)&' and '&clip(mod:esn_length_to)&' characters in length.','ServiceBase 2000',|
!                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
!                    Of 1 ! &OK Button
!                End!Case MessageEx
!                error# = 1
!            End !If Len(job:esn) < mod:esn_length_from Or Len(job:esn) > mod:esn_length_to
!    !        If man:use_msn = 'YES'
!    !            If len(Clip(xch:msn)) < mod:msn_length_from Or len(Clip(xch:msn)) > mod:msn_length_to
!    !                Case MessageEx('The M.S.N. entered should be between '&clip(mod:msn_length_from)&' and '&Clip(mod:msn_length_to)&' characters in length.','ServiceBase 2000',|
!    !                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
!    !                    Of 1 ! &OK Button
!    !                End!Case MessageEx
!    !                error# = 1
!    !            End!If len(job:msn) < mod:msn_length_from Or len(job:msn) > mod:msn_length_to
!    !        End!If man:use_msn = 'YES'
!        end !if access:modelnum.fetch(mod:model_number_key) = Level:benign
    End!IF error# = 0
    ! Inserting (DBH 1/06/2006) #6972 - Check the validity of the imei number
    If error# = 0
        If IsIMEIValid(tmp:ESN,tmp:ModelNumber,1) = False
            error# = 1
        End ! If IsIMEIValid(tmp:ESN,tmp:ModelNumber,1) = False
    End ! If error# = 0
    ! End (DBH ?1/06/2006) #6972

    If error# = 1
        Select(?tmp:esn)
    Else
        If tmp:AllocateAccessories
            glo:Select1 = tmp:ModelNumber
            Pick_Accessories
            glo:Select1 = ''
        End !If tmp:AllocateAccessories

        get(loan,0)
        if access:loan.primerecord() = Level:Benign
            loa:model_number   = tmp:modelnumber
            loa:manufacturer   = tmp:manufacturer
            loa:esn            = tmp:esn
            If ?tmp:MSN{prop:Hide} = 0
                loa:msn            = tmp:MSN
            End !If tmp:MSN{prop:Hide} = 0
            loa:location       = tmp:location
            loa:shelf_location = tmp:shelflocation
            loa:date_booked    = Today()
            loa:available      = 'AVL'
            loa:stock_type     = tmp:stocktype
            !Paul 02/06/2009 Log No 10684
            loa:StatusChangeDate = today()
            if access:loan.tryinsert() = Level:Benign
                !Add initial entry
                If Access:LOANHIST.PrimeRecord() = Level:Benign
                    loh:Ref_Number    = loa:Ref_Number
                    loh:Date          = Today()
                    loh:Time          = Clock()
                    Access:USERS.Clearkey(use:Password_Key)
                    use:Password    = glo:Password
                    If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                        !Found

                    Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                        !Error
                    End !If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                    loh:User          = use:User_Code
                    loh:Status        = 'INITIAL ENTRY'
                    If Access:LOANHIST.TryInsert() = Level:Benign
                        !Insert Successful
                    Else !If Access:LOANHIST.TryInsert() = Level:Benign
                        !Insert Failed
                        Access:LOANHIST.CancelAutoInc()
                    End !If Access:LOANHIST.TryInsert() = Level:Benign
                End !If Access:LOANHIST.PrimeRecord() = Level:Benign

                !Allocate accessories?
                If tmp:AllocateAccessories
                    If Records(glo:Q_Accessory)
                        Loop x# = 1 To Records(glo:Q_Accessory)
                            Get(glo:Q_Accessory,x#)
                            If Access:LOANACC.PrimeRecord() = Level:Benign
                                lac:Ref_Number = loa:Ref_Number
                                lac:Accessory   = glo:Accessory_Pointer
                                lac:Accessory_Status = '' ! What is this field for?
                                If Access:LOANACC.TryInsert() = Level:Benign
                                    !Insert Successful
                                Else !If Access:LOANACC.TryInsert() = Level:Benign
                                    !Insert Failed
                                End !If Access:LOANACC.TryInsert() = Level:Benign
                            End !If Access:LOANACC.PrimeRecord() = Level:Benign
                        End !Loop x# = 1 To Records(glo:Q_Accessory)
                    End !If Records(glo:Q_Accessory)
                End !If tmp:AllocateAccessories
            Else !if access:exchange.tryinsert() = Level:Benign
                access:loan.cancelautoinc()
            end

            untque:esn  = tmp:esn
            tmp:count   += 1
            untque:number = tmp:count
            Add(tmp:unitqueue)
            Sort(tmp:UnitQueue,-untque:number)
            tmp:esn = ''
            tmp:MSN = ''
            Select(?tmp:esn)
            Display()
        End!if access:exchange.primerecord() = Level:Benign
    End




! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020284'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Rapid_Loan_Unit')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt2
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:JOBS_ALIAS.Open
  Relate:LOANACC.Open
  Relate:LOAN_ALIAS.Open
  Relate:USERS_ALIAS.Open
  Access:LOANHIST.UseFile
  Access:STOCKTYP.UseFile
  Access:MANUFACT.UseFile
  Access:MODELNUM.UseFile
  Access:LOCATION.UseFile
  Access:LOCSHELF.UseFile
  Access:USERS.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  !For Development Purposes
  ?Sheet1{prop:wizard} = 1
  Access:USERS.Clearkey(use:Password_Key)
  use:Password    = glo:Password
  If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
      !Found
      If use:StockFromLocationOnly or glo:WebJob
          ?tmp:Location{prop:Disable} = 1
          ?CallLookup:4{prop:Disable} = 1
      End !If use:StockFromLocationOnly
      tmp:Location    = use:Location
  Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
      !Error
  End !If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
  ?List1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?tmp:StockType{Prop:Tip} AND ~?CallLookup{Prop:Tip}
     ?CallLookup{Prop:Tip} = 'Select ' & ?tmp:StockType{Prop:Tip}
  END
  IF ?tmp:StockType{Prop:Msg} AND ~?CallLookup{Prop:Msg}
     ?CallLookup{Prop:Msg} = 'Select ' & ?tmp:StockType{Prop:Msg}
  END
  IF ?tmp:Manufacturer{Prop:Tip} AND ~?CallLookup:2{Prop:Tip}
     ?CallLookup:2{Prop:Tip} = 'Select ' & ?tmp:Manufacturer{Prop:Tip}
  END
  IF ?tmp:Manufacturer{Prop:Msg} AND ~?CallLookup:2{Prop:Msg}
     ?CallLookup:2{Prop:Msg} = 'Select ' & ?tmp:Manufacturer{Prop:Msg}
  END
  IF ?tmp:ModelNumber{Prop:Tip} AND ~?CallLookup:3{Prop:Tip}
     ?CallLookup:3{Prop:Tip} = 'Select ' & ?tmp:ModelNumber{Prop:Tip}
  END
  IF ?tmp:ModelNumber{Prop:Msg} AND ~?CallLookup:3{Prop:Msg}
     ?CallLookup:3{Prop:Msg} = 'Select ' & ?tmp:ModelNumber{Prop:Msg}
  END
  IF ?tmp:Location{Prop:Tip} AND ~?CallLookup:4{Prop:Tip}
     ?CallLookup:4{Prop:Tip} = 'Select ' & ?tmp:Location{Prop:Tip}
  END
  IF ?tmp:Location{Prop:Msg} AND ~?CallLookup:4{Prop:Msg}
     ?CallLookup:4{Prop:Msg} = 'Select ' & ?tmp:Location{Prop:Msg}
  END
  IF ?tmp:ShelfLocation{Prop:Tip} AND ~?CallLookup:5{Prop:Tip}
     ?CallLookup:5{Prop:Tip} = 'Select ' & ?tmp:ShelfLocation{Prop:Tip}
  END
  IF ?tmp:ShelfLocation{Prop:Msg} AND ~?CallLookup:5{Prop:Msg}
     ?CallLookup:5{Prop:Msg} = 'Select ' & ?tmp:ShelfLocation{Prop:Msg}
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:JOBS_ALIAS.Close
    Relate:LOANACC.Close
    Relate:LOAN_ALIAS.Close
    Relate:USERS_ALIAS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Select_Stock_Type
      Browse_Manufacturers
      SelectModelKnownMake
      PickSiteLocations
      BrowseShelfLocations
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?CallLookup
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CallLookup, Accepted)
      glo:Select1 = 'LOA'
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CallLookup, Accepted)
    OF ?CallLookup:5
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CallLookup:5, Accepted)
      glo:Select1 = tmp:Location
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CallLookup:5, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tmp:StockType
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:StockType, Accepted)
      !Added by Paul 09/09/09 Log No 11059
      !check to see if the stock type is a restricted one
      Access:Stocktyp.clearkey(stp:Stock_Type_Key)
      stp:Stock_Type = clip(tmp:StockType)
      If Access:StockTyp.fetch(stp:Stock_Type_Key) = level:benign then
          !ok - now see if its restricted
          If stp:FranchiseViewOnly = 1 then
              !dont allow them to proceed
              Case Missive('You have chosen a restricted stock type. Please choose another stock type.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              tmp:StockType = ''
              select(?tmp:StockType)
              display()
          End
      End
      IF tmp:StockType OR ?tmp:StockType{Prop:Req}
        stp:Stock_Type = tmp:StockType
        !Save Lookup Field Incase Of error
        look:tmp:StockType        = tmp:StockType
        IF Access:STOCKTYP.TryFetch(stp:Use_Loan_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tmp:StockType = stp:Stock_Type
          ELSE
            !Restore Lookup On Error
            tmp:StockType = look:tmp:StockType
            SELECT(?tmp:StockType)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:StockType, Accepted)
    OF ?CallLookup
      ThisWindow.Update
      stp:Stock_Type = tmp:StockType
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          tmp:StockType = stp:Stock_Type
          Select(?+1)
      ELSE
          Select(?tmp:StockType)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:StockType)
    OF ?tmp:Manufacturer
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Manufacturer, Accepted)
      glo:Select2 = tmp:Manufacturer
      IF tmp:Manufacturer OR ?tmp:Manufacturer{Prop:Req}
        man:Manufacturer = tmp:Manufacturer
        !Save Lookup Field Incase Of error
        look:tmp:Manufacturer        = tmp:Manufacturer
        IF Access:MANUFACT.TryFetch(man:Manufacturer_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            tmp:Manufacturer = man:Manufacturer
          ELSE
            !Restore Lookup On Error
            tmp:Manufacturer = look:tmp:Manufacturer
            SELECT(?tmp:Manufacturer)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Manufacturer, Accepted)
    OF ?CallLookup:2
      ThisWindow.Update
      man:Manufacturer = tmp:Manufacturer
      
      IF SELF.RUN(2,Selectrecord)  = RequestCompleted
          tmp:Manufacturer = man:Manufacturer
          Select(?+1)
      ELSE
          Select(?tmp:Manufacturer)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:Manufacturer)
    OF ?tmp:ModelNumber
      IF tmp:ModelNumber OR ?tmp:ModelNumber{Prop:Req}
        mod:Model_Number = tmp:ModelNumber
        !Save Lookup Field Incase Of error
        look:tmp:ModelNumber        = tmp:ModelNumber
        IF Access:MODELNUM.TryFetch(mod:Manufacturer_Key)
          IF SELF.Run(3,SelectRecord) = RequestCompleted
            tmp:ModelNumber = mod:Model_Number
          ELSE
            !Restore Lookup On Error
            tmp:ModelNumber = look:tmp:ModelNumber
            SELECT(?tmp:ModelNumber)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup:3
      ThisWindow.Update
      mod:Model_Number = tmp:ModelNumber
      
      IF SELF.RUN(3,Selectrecord)  = RequestCompleted
          tmp:ModelNumber = mod:Model_Number
          Select(?+1)
      ELSE
          Select(?tmp:ModelNumber)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:ModelNumber)
    OF ?tmp:Location
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Location, Accepted)
      glo:Select1 = tmp:Location
      IF tmp:Location OR ?tmp:Location{Prop:Req}
        loc:Location = tmp:Location
        !Save Lookup Field Incase Of error
        look:tmp:Location        = tmp:Location
        IF Access:LOCATION.TryFetch(loc:Location_Key)
          IF SELF.Run(4,SelectRecord) = RequestCompleted
            tmp:Location = loc:Location
          ELSE
            !Restore Lookup On Error
            tmp:Location = look:tmp:Location
            SELECT(?tmp:Location)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Location, Accepted)
    OF ?CallLookup:4
      ThisWindow.Update
      loc:Location = tmp:Location
      
      IF SELF.RUN(4,Selectrecord)  = RequestCompleted
          tmp:Location = loc:Location
          Select(?+1)
      ELSE
          Select(?tmp:Location)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:Location)
    OF ?tmp:ShelfLocation
      IF tmp:ShelfLocation OR ?tmp:ShelfLocation{Prop:Req}
        los:Shelf_Location = tmp:ShelfLocation
        !Save Lookup Field Incase Of error
        look:tmp:ShelfLocation        = tmp:ShelfLocation
        IF Access:LOCSHELF.TryFetch(los:Shelf_Location_Key)
          IF SELF.Run(5,SelectRecord) = RequestCompleted
            tmp:ShelfLocation = los:Shelf_Location
          ELSE
            !Restore Lookup On Error
            tmp:ShelfLocation = look:tmp:ShelfLocation
            SELECT(?tmp:ShelfLocation)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup:5
      ThisWindow.Update
      los:Shelf_Location = tmp:ShelfLocation
      
      IF SELF.RUN(5,Selectrecord)  = RequestCompleted
          tmp:ShelfLocation = los:Shelf_Location
          Select(?+1)
      ELSE
          Select(?tmp:ShelfLocation)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:ShelfLocation)
    OF ?tmp:esn
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:esn, Accepted)
      ! Inserting (DBH 01/06/2006) #6972 - Double check the IMEI Number
      If Clip(tmp:ESN) <> ''
          If ValidateIMEI(tmp:ESN) = True
              Case Missive('An IMEI Number mismatch has occurred. The IMEI Number entered does not match the original one.'&|
                '|Please try again.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              tmp:ESN = ''
              Select(?tmp:ESN)
              Cycle
          Else ! If ValidateIMEI(tmp:ESN) = True
              If ?tmp:MSN{prop:hide} = 1
                  Do AddUnit
              End !?tmp:MSN{prop:hide} = 1
          End ! If ValidateIMEI(tmp:ESN) = True
      End ! If Clip(tmp:ESN) <> ''
      ! End (DBH 01/06/2006) #6972
      
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:esn, Accepted)
    OF ?tmp:MSN
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:MSN, Accepted)
      Do AddUnit
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:MSN, Accepted)
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020284'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020284'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020284'&'0')
      ***
    OF ?Next
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Next, Accepted)
      Case Choice(?Sheet1)
          Of 1
              If tmp:stocktype = '' Or |
                  tmp:manufacturer = '' Or |
                  tmp:modelnumber = '' Or |
                  tmp:location    = '' Or |
                  tmp:shelflocation = ''
                  Case Missive('You must complete all the fields before you can continue.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
              Else!If tmp:stocktype = '' Or |
                  ?tmp:MSN{prop:Hide} = 1
                  ?tmp:MSN:Prompt{prop:Hide} = 1
                  Access:MANUFACT.Clearkey(man:Manufacturer_Key)
                  man:Manufacturer    = tmp:Manufacturer
                  If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
                      !Found
                      If man:Use_MSN = 'YES'
                          ?tmp:MSN{prop:Hide} = 0
                          ?tmp:MSN:Prompt{prop:Hide} = 0
                      End !If man:Use_MSN = 'YES'
                  Else! If Access:MANUFACTURER.Tryfetch(man:Manufacturer_Key) = Level:Benign
                      !Error
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                  End! If Access:MANUFACTURER.Tryfetch(man:Manufacturer_Key) = Level:Benign
      
                  Unhide(?tab2)
                  Select(?Sheet1,2)
                  Disable(?Next)
                  Enable(?Back)
      
              End!If tmp:stocktype = '' Or |
      
          Of 2
      
      End!Case Choice(?Sheet1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Next, Accepted)
    OF ?back
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?back, Accepted)
      Case Choice(?Sheet1)
          Of 1
      
          Of 2
              hide(?tab2)
              Select(?Sheet1,1)
              Disable(?back)
              Enable(?Next)
      End!Case Choice(?Sheet1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?back, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
ReplaceLoan PROCEDURE (func:RefNumber)                !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:LoanUnitNumber   LONG
tmp:ModelNumber      STRING(30)
tmp:IMEINumber       STRING(30)
tmp:MSN              STRING(30)
tmp:NewIMEI          STRING(30)
tmp:NewMSN           STRING(30)
tmp:NewModelNumber   STRING(30)
tmp:Manufacturer     STRING(30)
window               WINDOW('Replace Loan Unit'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Replace Loan Unit'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('General'),USE(?Tab1)
                           PROMPT('Loan Unit Number'),AT(228,134),USE(?tmp:LoanUnitNumber:Prompt),TRN,FONT('Tahoma',,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s8),AT(304,134,64,10),USE(tmp:LoanUnitNumber),SKIP,TRN,FONT('Tahoma',8,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Loan Unit Number'),TIP('Loan Unit Number'),UPR,READONLY
                           PROMPT('Model Number'),AT(228,150),USE(?tmp:ModelNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,956,CHARSET:ANSI)
                           ENTRY(@s30),AT(304,150,124,10),USE(tmp:ModelNumber),SKIP,TRN,LEFT,FONT('Tahoma',8,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Model Number'),TIP('Model Number'),UPR,READONLY
                           PROMPT('I.M.E.I. Number'),AT(228,164),USE(?tmp:IMEINumber:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,956,CHARSET:ANSI)
                           ENTRY(@s30),AT(304,164,124,10),USE(tmp:IMEINumber),SKIP,TRN,LEFT,FONT('Tahoma',8,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('I.M.E.I. Number'),TIP('I.M.E.I. Number'),UPR,READONLY
                           PROMPT('M.S.N.'),AT(228,180),USE(?tmp:MSN:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,956,CHARSET:ANSI)
                           ENTRY(@s30),AT(304,178,124,10),USE(tmp:MSN),SKIP,TRN,LEFT,FONT('Tahoma',8,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('M.S.N.'),TIP('M.S.N.'),UPR,READONLY
                           PROMPT('New I.M.E.I. No'),AT(228,210),USE(?tmp:NewIMEI:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,956,CHARSET:ANSI)
                           ENTRY(@s30),AT(304,210,124,10),USE(tmp:NewIMEI),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('New I.M.E.I. No'),TIP('New I.M.E.I. No'),REQ,UPR
                           PROMPT('M.S.N.'),AT(228,234),USE(?tmp:NewMSN:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,956,CHARSET:ANSI)
                           ENTRY(@s30),AT(304,234,124,10),USE(tmp:NewMSN),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('M.S.N.'),TIP('M.S.N.'),REQ,UPR
                           PROMPT('New Model No'),AT(228,254),USE(?tmp:NewModelNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(304,254,124,10),USE(tmp:NewModelNumber),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('New Model No'),TIP('New Model No'),REQ,UPR
                           BUTTON,AT(428,250),USE(?LookupModelNumber),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                         END
                       END
                       BUTTON,AT(380,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
look:tmp:NewModelNumber                Like(tmp:NewModelNumber)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
msn_check       Routine
    ?tmp:NewMSN{prop:Hide} = 1
    ?tmp:NewMSN:Prompt{prop:Hide} = 1
    access:manufact.clearkey(man:manufacturer_key)
    man:manufacturer    = tmp:Manufacturer
    If access:manufact.fetch(man:manufacturer_key) = Level:Benign
        If man:use_msn = 'YES'
            ?tmp:NewMSN{prop:Hide} = 0
            ?tmp:NewMSN:Prompt{prop:Hide} = 0
        End
    End!If access:manufact.fetch(man:manufacturer_key)
    Display()
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020285'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('ReplaceLoan')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:ESNMODEL.Open
  Relate:LOAN.Open
  Relate:LOAN_ALIAS.Open
  Relate:MANUFACT.Open
  Relate:USERS_ALIAS.Open
  Access:MODELNUM.UseFile
  Access:LOANHIST.UseFile
  SELF.FilesOpened = True
  Access:LOAN.Clearkey(loa:Ref_Number_Key)
  loa:Ref_Number  = func:RefNumber
  If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
      !Found
      tmp:LoanUnitNumber  = loa:Ref_Number
      tmp:ModelNumber     = loa:Model_Number
      tmp:IMEINumber      = loa:ESN
      tmp:MSN             = loa:MSN
      tmp:NewModelNumber  = loa:Model_Number
      tmp:Manufacturer    = loa:Manufacturer
  Else ! If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
      !Error
  End !If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  If tmp:MSN = 'N/A' or tmp:MSN = ''
      ?tmp:MSN{prop:Hide} = 1
      ?tmp:MSN:Prompt{prop:Hide} = 1
  End !tmp:MSN = 'N/A' or tmp:MSN = ''
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?tmp:NewModelNumber{Prop:Tip} AND ~?LookupModelNumber{Prop:Tip}
     ?LookupModelNumber{Prop:Tip} = 'Select ' & ?tmp:NewModelNumber{Prop:Tip}
  END
  IF ?tmp:NewModelNumber{Prop:Msg} AND ~?LookupModelNumber{Prop:Msg}
     ?LookupModelNumber{Prop:Msg} = 'Select ' & ?tmp:NewModelNumber{Prop:Msg}
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:ESNMODEL.Close
    Relate:LOAN.Close
    Relate:LOAN_ALIAS.Close
    Relate:MANUFACT.Close
    Relate:USERS_ALIAS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    Browse_Model_Numbers
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020285'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020285'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020285'&'0')
      ***
    OF ?tmp:NewIMEI
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:NewIMEI, Accepted)
      tmp:NewModelNumber = IMEIModelRoutine(tmp:NEWIMEI,tmp:NewModelNumber)
      If IsIMEIValid(tmp:NewIMEI,tmp:NewModelNumber,1) = False
          tmp:NewIMEI = ''
          Select(?tmp:NewIMEI)
          Cycle
      End ! If IsIMEIValid(tmp:NewIMEI,tmp:NewModelNumber,1) = False
      access:modelnum.clearkey(mod:model_number_key)
      mod:model_number = tmp:NewModelNumber
      if access:modelnum.fetch(mod:model_number_key) = Level:Benign
          tmp:manufacturer = mod:manufacturer
      end
      Do msn_check
      
      !Added by Paul 12/08/2009 Log No 10976
      setcursor(cursor:wait)
      access:loan_alias.clearkey(loa_ali:esn_only_key)
      loa_ali:esn = tmp:NewIMEI
      set(loa_ali:esn_only_key,loa_ali:esn_only_key)
      loop
          if access:loan_alias.next()
             break
          end !if
          if loa_ali:esn <> tmp:NewIMEI      |
              then break.  ! end if
          yldcnt# += 1
          if yldcnt# > 25
             yield() ; yldcnt# = 0
          end !if
          Case Missive('This I.M.E.I. Number already exists in Loan Stock.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          cycle
      end !loop
      setcursor()
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:NewIMEI, Accepted)
    OF ?tmp:NewModelNumber
      IF tmp:NewModelNumber OR ?tmp:NewModelNumber{Prop:Req}
        mod:Model_Number = tmp:NewModelNumber
        !Save Lookup Field Incase Of error
        look:tmp:NewModelNumber        = tmp:NewModelNumber
        IF Access:MODELNUM.TryFetch(mod:Model_Number_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tmp:NewModelNumber = mod:Model_Number
          ELSE
            !Restore Lookup On Error
            tmp:NewModelNumber = look:tmp:NewModelNumber
            SELECT(?tmp:NewModelNumber)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupModelNumber
      ThisWindow.Update
      mod:Model_Number = tmp:NewModelNumber
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          tmp:NewModelNumber = mod:Model_Number
          Select(?+1)
      ELSE
          Select(?tmp:NewModelNumber)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:NewModelNumber)
    OF ?OK
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
      If tmp:NewIMEI = ''
          Select(tmp:NewIMEI)
          Error# = 1
      End !tmp:NewIMEI = ''
      
      If tmp:NewMSN = '' And ?tmp:NewMSN{prop:Hide} = 0 And Error# = 0
          Select(?tmp:NewMSN)
          Error# = 1
      End !tmp:NewMSN = '' And ?tmp:NewMSN{prop:Hide} = 0 And Error# = 0
      
      If tmp:NewModelNumber = '' And Error# =0
          Select(?tmp:NewModelNumber)
          Error# = 1
      End !tmp:NewModelNumber = '' And Error# =0
      
      !Added by Paul 12/08/2009 Log No 10976
      setcursor(cursor:wait)
      access:loan_alias.clearkey(loa_ali:esn_only_key)
      loa_ali:esn = tmp:NewIMEI
      set(loa_ali:esn_only_key,loa_ali:esn_only_key)
      loop
          if access:loan_alias.next()
             break
          end !if
          if loa_ali:esn <> tmp:NewIMEI      |
              then break.  ! end if
          yldcnt# += 1
          if yldcnt# > 25
             yield() ; yldcnt# = 0
          end !if
          Case Missive('This I.M.E.I. Number already exists in Loan Stock.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          Error# = 1
      end !loop
      setcursor()
      
      If Error# = 0
          Access:LOAN.Clearkey(loa:Ref_Number_Key)
          loa:Ref_Number  = func:RefNumber
          If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
              !Found
              loa:ESN = tmp:NewIMEI
              loa:MSN = tmp:NewMSN
              loa:Model_Number = tmp:NewModelNumber
              If Access:LOAN.Update() = Level:Benign
                  get(loanhist,0)
                  if access:loanhist.primerecord() = Level:Benign
                      loh:ref_number      = loa:ref_number
                      loh:date            = Today()
                      loh:time            = Clock()
                      access:users.clearkey(use:password_key)
                      use:password        = glo:password
                      access:users.fetch(use:password_key)
                      loh:user = use:user_code
                      loh:status          = 'REPLACE LOAN: PREV - ' & Clip(tmp:IMEINumber)
                      access:loanhist.insert()
                  end!if access:exchhist.primerecord() = Level:Benign
      
              End !If Access:LOAN.Update() = Level:Benign
          Else ! If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
              !Error
          End !If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
          Post(Event:CloseWindow)
      End !Error# = 0
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
ExchangeOrderNotes PROCEDURE                          !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Order Notes'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Order Notes'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Order Notes'),AT(236,159),USE(?Prompt3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           TEXT,AT(238,170,204,72),USE(exo:Notes),SKIP,FONT(,,,FONT:bold),COLOR(COLOR:White),READONLY
                         END
                       END
                       BUTTON,AT(448,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020277'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ExchangeOrderNotes')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?OK,RequestCancelled)
  Relate:EXCHORDR.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EXCHORDR.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020277'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020277'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020277'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
LoanOrderNotes PROCEDURE                              !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Order Notes'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Order Notes'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Order Notes'),AT(240,145),USE(?Prompt3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           TEXT,AT(240,156,204,72),USE(lor:Notes),SKIP,FONT(,,,FONT:bold),COLOR(COLOR:White),READONLY
                         END
                       END
                       BUTTON,AT(448,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020287'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('LoanOrderNotes')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?OK,RequestCancelled)
  Relate:LOAORDR.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LOAORDR.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020287'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020287'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020287'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Exchange48HourOrder PROCEDURE (func:JobNumber,func:DOP,func:Returning) !Generated from procedure template - Window

tmp:Manufacturer     STRING(30)
tmp:Model_Number     STRING(30)
tmp:Qty_Required     LONG
tmp:Notes            STRING(255)
tmp:CurrentCount     LONG
ModelQueue           QUEUE,PRE(modque)
ModelNumber          STRING(30)
Quantity             LONG
Count                LONG
Manufacturer         STRING(30)
                     END
tmp:JobNumber        LONG
tmp:DOP              DATE
tmp:Return           BYTE(0)
sav:ModelNumber      STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Order 48 Hour Exchange Units'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Order 48 Hour Exchange Unit'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Order 48 Hour Exchange Units'),USE(?Tab1)
                           PROMPT('Job Number'),AT(225,132),USE(?job:Ref_Number:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(301,132,64,10),USE(tmp:JobNumber),SKIP,RIGHT(1),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver),UPR,READONLY
                           PROMPT('Manufacturer'),AT(225,172),USE(?tmp:Manufacturer:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(301,172,124,10),USE(tmp:Manufacturer),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(429,170),USE(?LookupManufacturer),TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Model'),AT(225,194),USE(?tmp:Model_Number:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(301,194,124,10),USE(tmp:Model_Number),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(429,188),USE(?LookupModelNumber),TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Notes'),AT(225,212),USE(?Notes:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           TEXT,AT(301,212,148,60),USE(tmp:Notes),VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Activation Date'),AT(225,154),USE(?job:DOP:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(301,154,64,10),USE(tmp:DOP),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                         END
                       END
                       BUTTON,AT(380,332),USE(?OkButton),TRN,FLAT,LEFT,ICON('ordunitp.jpg'),DEFAULT
                       BUTTON,AT(448,332),USE(?CancelButton),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
PassModelQueue  Queue(DefModelQueue)
                End
!Save Entry Fields Incase Of Lookup
look:tmp:Manufacturer                Like(tmp:Manufacturer)
look:tmp:Model_Number                Like(tmp:Model_Number)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(tmp:Return)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020269'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Exchange48HourOrder')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:ACCAREAS_ALIAS.Open
  Relate:AUDIT.Open
  Relate:ESNMODAL.Open
  Relate:EXCHOR48.Open
  Relate:USERS_ALIAS.Open
  Access:MANUFACT.UseFile
  Access:MODELNUM.UseFile
  Access:ESNMODEL.UseFile
  SELF.FilesOpened = True
  tmp:JobNumber   = func:JobNumber
  tmp:DOP         = func:DOP
  Access:JOBS_ALIAS.Clearkey(job_ali:Ref_Number_Key)
  job_ali:Ref_Number  = tmp:JobNumber
  If Access:JOBS_ALIAS.Tryfetch(job_ali:Ref_Number_Key) = Level:Benign
      !Found
      tmp:Manufacturer    = job_ali:Manufacturer
      tmp:Model_Number    = job_ali:Model_Number
      sav:ModelNumber     = tmp:Model_Number
  Else ! If Access:JOBS_ALIAS.Tryfetch(job_ali:Ref_Number_Key) = Level:Benign
      !Error
  End !If Access:JOBS_ALIAS.Tryfetch(job_ali:Ref_Number_Key) = Level:Benign
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  !If no access, then do not allow the user to change
  !the model details - 4025 (DBH: 07-04-2004)
  If SecurityCheck('EXCHANGE - ORDER 48-HOUR')
      ?tmp:Manufacturer{prop:ReadOnly} = 1
      ?tmp:Model_Number{prop:ReadOnly} = 1
      ?LookupManufacturer{prop:Hide} = 1
      ?LookupModelNumber{prop:Hide} = 1
      !Bryan.CompFieldColour()
  End !SecurityCheck('EXCHANGE - ORDER 48-HOUR')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?tmp:Manufacturer{Prop:Tip} AND ~?LookupManufacturer{Prop:Tip}
     ?LookupManufacturer{Prop:Tip} = 'Select ' & ?tmp:Manufacturer{Prop:Tip}
  END
  IF ?tmp:Manufacturer{Prop:Msg} AND ~?LookupManufacturer{Prop:Msg}
     ?LookupManufacturer{Prop:Msg} = 'Select ' & ?tmp:Manufacturer{Prop:Msg}
  END
  IF ?tmp:Model_Number{Prop:Tip} AND ~?LookupModelNumber{Prop:Tip}
     ?LookupModelNumber{Prop:Tip} = 'Select ' & ?tmp:Model_Number{Prop:Tip}
  END
  IF ?tmp:Model_Number{Prop:Msg} AND ~?LookupModelNumber{Prop:Msg}
     ?LookupModelNumber{Prop:Msg} = 'Select ' & ?tmp:Model_Number{Prop:Msg}
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:AUDIT.Close
    Relate:ESNMODAL.Close
    Relate:EXCHOR48.Close
    Relate:USERS_ALIAS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_Manufacturers
      SelectModelKnownMake
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?tmp:Model_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Model_Number, Accepted)
      glo:Select2 = tmp:Manufacturer
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Model_Number, Accepted)
    OF ?LookupModelNumber
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupModelNumber, Accepted)
      glo:Select2 = tmp:Manufacturer
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupModelNumber, Accepted)
    OF ?OkButton
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      if tmp:Manufacturer = ''
          select(?tmp:Manufacturer)
          cycle
      end
      
      if tmp:Model_Number = ''
          select(?tmp:Model_Number)
          cycle
      end
      
      
      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
      If glo:WebJob
          tra:Account_Number  = Clarionet:Global.Param2
      Else !glo:WebJob
          tra:Account_Number  = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
      End !glo:WebJob
      
      If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          !Found
          Error# = 0
          !Does a request already exists? -  (DBH: 23-10-2003)
          If Is48HourOrderCreated(tra:SiteLocation,func:JobNumber) = True
              Case Missive('A 48 Hour Exchange has already been requested for this job.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              Error# = 1
      
          End !If Is48HourOrderCreated(tra:SiteLocation,func:JobNumber) = True
      
          If Error# = 0 And func:Returning = 0
              !Does a request already exists? -  (DBH: 23-10-2003)
              If Is48HourOrderProcessed(tra:SiteLocation,func:JobNumber) = True
                  Case Missive('A 48 Hour Exchange Unit has already been requested and despatched for this job.'&|
                    '<13,10>'&|
                    '<13,10>You must receive, allocate and then revmoe the unit from this job before you can order another.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  Error# = 1
      
              End !If Is48HourOrderProcessed(tra:SiteLocation,func:JobNumber) = True
      
          End !If Error# = 0 And func:Returning = 0
      
          If Error# = 0
              !Is this an legitimate Model Number? -  (DBH: 13-11-2003)
              If tmp:Model_Number <> sav:ModelNumber
                  !The model has been changed -  (DBH: 13-11-2003)
                  If IsThisModelAlternative(sav:ModelNumber,tmp:Model_Number) = False
                      Case Missive('Original Unit: ' & Clip(sav:ModelNumber) & ', Selected Unit: ' & Clip(tmp:Model_Number) & '.'&|
                        '<13,10>This unit is NOT an alternative Model Number for the requested unit.'&|
                        '<13,10>'&|
                        '<13,10>Do you wish to IGNORE and continue, or RE-ENTER another Model Number?'&|
                        '<13,10>','ServiceBase 3g',|
                                     'mquest.jpg','\Cancel|Re-enter|Ignore')
                          Of 3 ! Ignore Button
                          Of 2 ! Re-enter Button
                              Select(?tmp:Model_Number)
                              Error# = 1
                          Of 1 ! Cancel Button
                      End ! Case Missive
                  Else !If IsThisModelAlternative(sav:Model_Number,tmp:ModelNumber) = False
                      Case Missive('Are you sure you want to change the selected Model Number?','ServiceBase 3g',|
                                     'mquest.jpg','\No|/Yes')
                          Of 2 ! Yes Button
                          Of 1 ! No Button
                              Select(?tmp:Model_Number)
                              Error# = 1
                      End ! Case Missive
                  End !If IsThisModelAlternative(sav:Model_Number,tmp:ModelNumber) = False
              End !If tmp:Model_Number <> sav:Model_Number
              If Error# = 0
                  If Access:EXCHOR48.PrimeRecord() = Level:Benign
                      ex4:Location     = tra:SiteLocation
                      ex4:Manufacturer = tmp:Manufacturer
                      ex4:ModelNumber  = tmp:Model_Number
                      ex4:Received     = 0
                      ex4:Notes        = Upper(tmp:Notes)
                      ex4:JobNumber    = func:JobNumber
                      If Access:EXCHOR48.TryInsert() = Level:Benign
                          !Insert Successful
                          !Create an audit entry for exchange order.
                          !This will cover if the model number is changed - 4025 (DBH: 07-04-2004)
                          IF (AddToAudit(func:JobNumber,'EXC','48 HOUR EXCHANGE ORDER CREATED','MANUFACTURER: ' & Clip(tmp:Manufacturer) & |
                                              '<13,10>MODEL NUMBER: ' & Clip(tmp:Model_Number)))
                          END ! IF
      
                          Case Missive('Do you want to print the order?','ServiceBase 3g',|
                                         'mquest.jpg','\No|/Yes')
                              Of 2 ! Yes Button
                                  Clear(PassModelQueue)
                                  Free(PassModelQueue)
                                  PassModelQueue.Manufacturer = tmp:Manufacturer
                                  PassModelQueue.ModelNumber  = tmp:Model_Number
                                  PassModelQueue.Quantity     = 1
                                  Add(PassModelQueue)
                                  ExchangeOrders(PassModelQueue,tmp:JobNumber,tmp:DOP)
                             Of 1 ! No Button
                          End ! Case Missive
                      Else !If Access:EXCHOR48.TryInsert() = Level:Benign
                          !Insert Failed
                          Access:EXCHOR48.CancelAutoInc()
                      End !If Access:EXCHOR48.TryInsert() = Level:Benign
                  End !If Access:EXCHOR48.PrimeRecord() = Level:Benign
              End !If Error# = 0
          End !If Error# = 0
      
          tmp:Return = True
          Post(Event:CloseWindow)
      Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          !Error
          Case Missive('Cannot establish the Site Location.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020269'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020269'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020269'&'0')
      ***
    OF ?tmp:Manufacturer
      IF tmp:Manufacturer OR ?tmp:Manufacturer{Prop:Req}
        man:Manufacturer = tmp:Manufacturer
        !Save Lookup Field Incase Of error
        look:tmp:Manufacturer        = tmp:Manufacturer
        IF Access:MANUFACT.TryFetch(man:Manufacturer_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tmp:Manufacturer = man:Manufacturer
          ELSE
            !Restore Lookup On Error
            tmp:Manufacturer = look:tmp:Manufacturer
            SELECT(?tmp:Manufacturer)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupManufacturer
      ThisWindow.Update
      man:Manufacturer = tmp:Manufacturer
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          tmp:Manufacturer = man:Manufacturer
          Select(?+1)
      ELSE
          Select(?tmp:Manufacturer)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:Manufacturer)
    OF ?tmp:Model_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Model_Number, Accepted)
      glo:Select2 = ''
      IF tmp:Model_Number OR ?tmp:Model_Number{Prop:Req}
        mod:Model_Number = tmp:Model_Number
        mod:Manufacturer = tmp:Manufacturer
        GLO:Select2 = tmp:Manufacturer
        !Save Lookup Field Incase Of error
        look:tmp:Model_Number        = tmp:Model_Number
        IF Access:MODELNUM.TryFetch(mod:Manufacturer_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            tmp:Model_Number = mod:Model_Number
          ELSE
            CLEAR(mod:Manufacturer)
            CLEAR(GLO:Select2)
            !Restore Lookup On Error
            tmp:Model_Number = look:tmp:Model_Number
            SELECT(?tmp:Model_Number)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Model_Number, Accepted)
    OF ?LookupModelNumber
      ThisWindow.Update
      mod:Model_Number = tmp:Model_Number
      
      IF SELF.RUN(2,Selectrecord)  = RequestCompleted
          tmp:Model_Number = mod:Model_Number
          Select(?+1)
      ELSE
          Select(?tmp:Model_Number)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:Model_Number)
    OF ?CancelButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
      tmp:Return = False
      Post(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
RapidExchangeTransfer48Hour PROCEDURE                 !Generated from procedure template - Window

save_ex4_id          USHORT,AUTO
save_esn_id          USHORT,AUTO
save_esa_id          USHORT,AUTO
Requests_Q           QUEUE,PRE()
Requested_Location   STRING(30)
                     END
tmp:Scanned_IMEI     STRING(30)
tmp:courier          STRING(30)
tmp:From_Site_Location STRING(30)
tmp:From_Stock_Type  STRING('48-HOUR SWAP UNITS {12}')
tmp:To_Site_Location STRING(30)
tmp:To_Stock_Type    STRING('48-HOUR SWAP UNITS {12}')
tmp:Response         BYTE
tmp:Original_Location STRING(30)
tmp:ListPos          LONG
tmp:Notes            STRING(1)
tmp:SiteLocation     STRING(30)
tmp:Type             BYTE(0)
ProcessedUnitsQueue  QUEUE,PRE(unique)
UnitNumber           LONG
JobNumber            LONG
                     END
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:JobNumber        STRING(30)
BRW7::View:Browse    VIEW(EXCHOR48)
                       PROJECT(ex4:Manufacturer)
                       PROJECT(ex4:ModelNumber)
                       PROJECT(ex4:JobNumber)
                       PROJECT(ex4:RecordNumber)
                       PROJECT(ex4:OrderUnitNumber)
                       PROJECT(ex4:Notes)
                       PROJECT(ex4:Received)
                       PROJECT(ex4:Returning)
                       PROJECT(ex4:Location)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
ex4:Manufacturer       LIKE(ex4:Manufacturer)         !List box control field - type derived from field
ex4:Manufacturer_NormalFG LONG                        !Normal forground color
ex4:Manufacturer_NormalBG LONG                        !Normal background color
ex4:Manufacturer_SelectedFG LONG                      !Selected forground color
ex4:Manufacturer_SelectedBG LONG                      !Selected background color
ex4:ModelNumber        LIKE(ex4:ModelNumber)          !List box control field - type derived from field
ex4:ModelNumber_NormalFG LONG                         !Normal forground color
ex4:ModelNumber_NormalBG LONG                         !Normal background color
ex4:ModelNumber_SelectedFG LONG                       !Selected forground color
ex4:ModelNumber_SelectedBG LONG                       !Selected background color
tmp:Notes              LIKE(tmp:Notes)                !List box control field - type derived from local data
tmp:Notes_NormalFG     LONG                           !Normal forground color
tmp:Notes_NormalBG     LONG                           !Normal background color
tmp:Notes_SelectedFG   LONG                           !Selected forground color
tmp:Notes_SelectedBG   LONG                           !Selected background color
tmp:Notes_Icon         LONG                           !Entry's icon ID
tmp:JobNumber          LIKE(tmp:JobNumber)            !List box control field - type derived from local data
tmp:JobNumber_NormalFG LONG                           !Normal forground color
tmp:JobNumber_NormalBG LONG                           !Normal background color
tmp:JobNumber_SelectedFG LONG                         !Selected forground color
tmp:JobNumber_SelectedBG LONG                         !Selected background color
ex4:JobNumber          LIKE(ex4:JobNumber)            !List box control field - type derived from field
ex4:JobNumber_NormalFG LONG                           !Normal forground color
ex4:JobNumber_NormalBG LONG                           !Normal background color
ex4:JobNumber_SelectedFG LONG                         !Selected forground color
ex4:JobNumber_SelectedBG LONG                         !Selected background color
ex4:RecordNumber       LIKE(ex4:RecordNumber)         !List box control field - type derived from field
ex4:RecordNumber_NormalFG LONG                        !Normal forground color
ex4:RecordNumber_NormalBG LONG                        !Normal background color
ex4:RecordNumber_SelectedFG LONG                      !Selected forground color
ex4:RecordNumber_SelectedBG LONG                      !Selected background color
ex4:OrderUnitNumber    LIKE(ex4:OrderUnitNumber)      !List box control field - type derived from field
ex4:OrderUnitNumber_NormalFG LONG                     !Normal forground color
ex4:OrderUnitNumber_NormalBG LONG                     !Normal background color
ex4:OrderUnitNumber_SelectedFG LONG                   !Selected forground color
ex4:OrderUnitNumber_SelectedBG LONG                   !Selected background color
ex4:Notes              LIKE(ex4:Notes)                !List box control field - type derived from field
ex4:Notes_NormalFG     LONG                           !Normal forground color
ex4:Notes_NormalBG     LONG                           !Normal background color
ex4:Notes_SelectedFG   LONG                           !Selected forground color
ex4:Notes_SelectedBG   LONG                           !Selected background color
ex4:Received           LIKE(ex4:Received)             !Browse key field - type derived from field
ex4:Returning          LIKE(ex4:Returning)            !Browse key field - type derived from field
ex4:Location           LIKE(ex4:Location)             !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK2::ex4:Location         LIKE(ex4:Location)
HK2::ex4:Received         LIKE(ex4:Received)
HK2::ex4:Returning        LIKE(ex4:Returning)
! ---------------------------------------- Higher Keys --------------------------------------- !
window               WINDOW('Rapid Exchange Transfer 48 Hour'),AT(0,0,680,429),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       MENUBAR
                         MENU('Options'),USE(?Options)
                           ITEM('Set Courier'),USE(?OptionsSetCourier)
                         END
                       END
                       SHEET,AT(64,115,552,248),USE(?Sheet2),COLOR(0D6E7EFH),SPREAD
                         TAB('Requests'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           OPTION('Exchange Order Type'),AT(348,131,160,24),USE(tmp:Type),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('Order Req'),AT(352,141),USE(?tmp:Type:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                             RADIO('Returning Only'),AT(432,141),USE(?tmp:Type:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                           END
                           PROMPT('N - Notes'),AT(521,213),USE(?Prompt6),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Site Location'),AT(104,135),USE(?Prompt7),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(104,160,404,171),USE(?List),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('125L(2)|*~Manufacturer~@s30@126L(2)|*~Model Number~@s30@11C|*J~N~@s1@120L(2)|*~J' &|
   'ob Number~@s30@0R(2)*~Job Number~@s8@0R(2)*~Record Number~@s8@0R(2)*~Ref Number ' &|
   'Of Exchange Unit Returning~@s8@0R(2)*~Notes~@s255@'),FROM(Queue:Browse)
                           BUTTON,AT(513,159),USE(?ViewNotes),TRN,FLAT,LEFT,ICON('viewnotp.jpg')
                           BUTTON,AT(513,251),USE(?ReprintWaybillButton),TRN,FLAT,LEFT,ICON('prnwayp.jpg')
                           BUTTON,AT(404,335),USE(?Allocate_Button),TRN,FLAT,LEFT,ICON('allocatp.jpg')
                           PROMPT('I.M.E.I. Number To Transfer'),AT(104,343),USE(?IMEIText),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(272,343,124,10),USE(tmp:Scanned_IMEI),DISABLE,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           COMBO(@s30),AT(176,135,124,10),USE(tmp:SiteLocation),VSCROLL,FONT(,,,FONT:bold),COLOR(COLOR:White),FORMAT('120L(2)|M@s30@'),DROP(10),FROM(Requests_Q)
                         END
                       END
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Rapid Exchange Transfer - 48 Hour Process'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       SHEET,AT(64,55,276,56),USE(?Sheet4),COLOR(0D6E7EFH),SPREAD
                         TAB('FROM'),USE(?Tab4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Location'),AT(68,73),USE(?tmp:From_Site_Location:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(120,73,124,10),USE(tmp:From_Site_Location),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           BUTTON,AT(248,69),USE(?From_Location_Button),TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Stock Type'),AT(68,93),USE(?tmp:From_Stock_Type:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(120,93,124,10),USE(tmp:From_Stock_Type),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(248,89),USE(?From_Stock_Type_Button),TRN,FLAT,ICON('lookupp.jpg')
                         END
                       END
                       SHEET,AT(344,55,272,56),USE(?Sheet3),COLOR(0D6E7EFH),SPREAD
                         TAB('TO'),USE(?Tab5),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Location'),AT(348,73),USE(?tmp:To_Site_Location:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(400,73,124,10),USE(tmp:To_Site_Location),SKIP,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),READONLY
                           PROMPT('Stock Type'),AT(348,93),USE(?tmp:To_Stock_Type:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(400,93,124,10),USE(tmp:To_Stock_Type),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(524,89),USE(?To_Stock_Type_Button),TRN,FLAT,ICON('lookupp.jpg')
                         END
                       END
                       BUTTON,AT(548,366),USE(?OkButton),TRN,FLAT,LEFT,ICON('finishp.jpg'),DEFAULT
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW7                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW7::Sort0:Locator  StepLocatorClass                 !Default Locator
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
look:tmp:From_Site_Location                Like(tmp:From_Site_Location)
look:tmp:From_Stock_Type                Like(tmp:From_Stock_Type)
look:tmp:To_Stock_Type                Like(tmp:To_Stock_Type)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
AnyMoreUnitsToProcess       Routine
    ! Check if all exchange units have been processed for this location
    More_To_Process# = 0

    Save_ex4_ID = Access:EXCHOR48.SaveFile()
    Access:EXCHOR48.ClearKey(ex4:LocationModelKey)
    ex4:Location     = tmp:To_Site_Location
    Set(ex4:LocationModelKey,ex4:LocationModelKey)
    Loop
        If Access:EXCHOR48.NEXT()
           Break
        End !If
        If ex4:Location     <> tmp:To_Site_Location       |
            Then Break.  ! End If
        If ex4:Received = 0
            More_To_Process# = 1
            Break
        End !If ex4:Received = 0
    End !Loop
    Access:EXCHOR48.RestoreFile(Save_ex4_ID)

    if More_To_Process# = 1
        if Move_Down# = 1
            Select(?List,CHOICE(?List)+1)
        end
    else
        Case Missive('The requested quantity of units has been fulfilled.','ServiceBase 3g',|
                       'midea.jpg','/OK')
            Of 1 ! OK Button
        End ! Case Missive
        ! Print waybill
        do Print_Waybill
        !free(glo:q_JobNumber)

        do Create_Requests_Queue                                        ! Reload requests queue

        display()
    End !If Access:EXCHOR48.Tryfetch(ex4:RecordNumberKey) = Level:Benign
Transfer        Routine
    Error# = 0

    tmp:Record_ID# = ex4:RecordNumber
    Access:EXCHANGE.ClearKey(xch:ESN_Only_Key)
    xch:ESN = tmp:Scanned_IMEI
    if Access:EXCHANGE.Fetch(xch:ESN_Only_Key)
        Case Missive('No Exchange Unit exist with the selected I.M.E.I. Number.','ServiceBase 3g',|
                       'mstop.jpg','/OK')
            Of 1 ! OK Button
        End ! Case Missive
        Error# = 1
    else
        !Is the selected unit at this location? -  (DBH: 30-10-2003)
        If xch:Location <> tmp:From_Site_Location And Error# = 0
            Case Missive('The scanned unit is at a different location.'&|
              '<13,10>'&|
              '<13,10>Please scan another.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
            Error# = 1
        End !If xch:Location <> tmp:From_Site_Location

        If xch:Available <> 'AVL' And Error# = 0
            Case Missive('The scanned unit is not available.'&|
              '<13,10>'&|
              '<13,10>Please scan another.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
            Error# = 1
        End !If xch:Available <> 'AVL'

        If tmp:From_Stock_Type <> xch:Stock_Type And Error# = 0
            Case Missive('The selected I.M.E.I. Number corresponds to a unit in the ' & Clip(xch:Stock_Type) & ' location.'&|
              '<13,10>'&|
              '<13,10>Are you sure you want to continue?','ServiceBase 3g',|
                           'mquest.jpg','\No|/Yes')
                Of 2 ! Yes Button
                Of 1 ! No Button
                    Error# = 1
            End ! Case Missive
        End !If tmp:From_Stock_Type <> xch:Stock_Type

        !Assuming that the unit is available,
        !does the model match. If not does it match
        !one of the alternative models in the matrix? -  (DBH: 29-10-2003)

        If Error# = 0
            If ex4:ModelNumber <> xch:Model_Number
                Found# = 0
                !Get the TAC codes for the originating model number
                !i.e. the model that has been requested -  (DBH: 29-10-2003)

                If IsThisModelAlternative(ex4:ModelNumber,xch:Model_Number) = False

    !            Save_esn_ID = Access:ESNMODEL.SaveFile()
    !            Access:ESNMODEL.ClearKey(esn:Model_Number_Key)
    !            esn:Model_Number = ex4:ModelNumber
    !            Set(esn:Model_Number_Key,esn:Model_Number_Key)
    !            Loop
    !                If Access:ESNMODEL.NEXT()
    !                   Break
    !                End !If
    !                If esn:Model_Number <> ex4:ModelNumber      |
    !                    Then Break.  ! End If
    !                !Now check the alternative models to see if the
    !                !scanned model matches -  (DBH: 29-10-2003)
    !                Save_esa_ID = Access:ESNMODAL.SaveFile()
    !                Access:ESNMODAL.ClearKey(esa:TACCodeKey)
    !                esa:RefNumber = esn:Record_Number
    !                esa:TacCode   = Sub(tmp:Scanned_IMEI,1,6)
    !                Set(esa:TACCodeKey,esa:TACCodeKey)
    !                Loop
    !                    If Access:ESNMODAL.NEXT()
    !                       Break
    !                    End !If
    !                    If esa:RefNumber <> esn:Record_Number      |
    !                    Or esa:TacCode   <> Sub(tmp:Scanned_IMEI,1,6)      |
    !                        Then Break.  ! End If
    !                    If esa:ModelNumber = xch:Model_Number
    !                        Found# = 1
    !                        Break
    !                    End !If esa:ModelNumber = xch:Model_Number
    !                End !Loop
    !                Access:ESNMODAL.RestoreFile(Save_esa_ID)
    !            End !Loop
    !            Access:ESNMODEL.RestoreFile(Save_esn_ID)
    !
    !            If Found# = 0
                    Case Missive('Requested Unit: ' & Clip(ex4:ModelNumber) & ', Scanned Unit: ' & Clip(xch:Model_Number)& '.'&|
                      '<13,10>'&|
                      '<13,10>This unit is NOT an alternative Model for the requested unit. Do you want to ignore this mismatch, or re-enter another I.M.E.I. Number?','ServiceBase 3g',|
                                   'mquest.jpg','Re-Enter|Ignore')
                        Of 2 ! Ignore Button
                        Of 1 ! Re-Enter Button
                            Error# = 1
                    End ! Case Missive
                End !If Found# = 0

            End !If ex4:ModelNumber <> xch:Model_Number
        End !If Error# = 0
    end
    If Error# = 0
        !Move the exchange unit to the new location -  (DBH: 30-10-2003)
        xch:Location = tmp:To_Site_Location
        xch:Stock_Type  = tmp:To_Stock_Type

        unique:UnitNumber   = xch:Ref_Number
        unique:JobNumber    = ex4:JobNumber
        Add(ProcessedUnitsQueue)

    !
    !    glo:q_JobNumber.GLO:Job_Number_Pointer = xch:Ref_Number
    !    add(glo:q_JobNumber)

        if not Access:EXCHANGE.TryUpdate()
            If access:exchhist.primerecord() = Level:Benign
                exh:ref_number      = xch:ref_number
                exh:date            = Today()
                exh:time            = Clock()
                access:users.clearkey(use:password_key)
                use:password        = glo:password
                access:users.fetch(use:password_key)
                exh:user = use:user_code
                exh:status          = '48 HOUR UNIT TRANSFERRED TO ' & Clip(tmp:To_Site_Location)
                access:exchhist.insert()
            End!if access:exchhist.primerecord() = Level:Benign

            Access:EXCHOR48.Clearkey(ex4:RecordNumberKey)
            ex4:RecordNumber    = tmp:Record_ID#
            If Access:EXCHOR48.Tryfetch(ex4:RecordNumberKey) = Level:Benign
                !Found

                !Mark the unit order as being received
                !Also save the number to make sure this unit is
                !returned to the original job -  (DBH: 30-10-2003)
                ex4:Received = 1
                ex4:DateOrdered = Today()
                ex4:TimeOrdered = Clock()
                ex4:OrderUnitNumber = xch:Ref_Number
                Access:EXCHOR48.TryUpdate()

                Do AnyMoreUnitsToProcess

            end
        end
    End !Error# = 0

UnitReturning       Routine
    Error# = 0
    Access:EXCHOR48.ClearKey(ex4:RecordNumberKey)
    ex4:RecordNumber = brw7.q.ex4:RecordNumber
    If Access:EXCHOR48.TryFetch(ex4:RecordNumberKey) = Level:Benign
        !Found

    Else !If Access:EXCHOR48.TryFetch(ex4:RecordNumberKey) = Level:Benign
        !Error
    End !If Access:EXCHOR48.TryFetch(ex4:RecordNumberKey) = Level:Benign

    Access:EXCHANGE.ClearKey(xch:ESN_Only_Key)
    xch:ESN = tmp:Scanned_IMEI
    if Access:EXCHANGE.Fetch(xch:ESN_Only_Key)
        Case Missive('No Exchange Units exists with the selected I.M.E.I. Number.','ServiceBase 3g',|
                       'mstop.jpg','/OK')
            Of 1 ! OK Button
        End ! Case Missive
        Error# = 1
    Else !if Access:EXCHANGE.Fetch(xch:ESN_Only_Key)
        If xch:Ref_Number <> ex4:OrderUnitNumber
            Case Missive('The selected I.M.E.I. Number does not correspond to the selected job. '&|
              '<13,10>'&|
              '<13,10>Please scan the unit that was removed from this job.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
        Else !If xch:Ref_Number <> ex4:OrderUnitNumber
            xch:Location = 'MAIN STORE'
            If Access:EXCHANGE.Update() = Level:Benign
                If access:exchhist.primerecord() = Level:Benign
                    exh:ref_number      = xch:ref_number
                    exh:date            = Today()
                    exh:time            = Clock()
                    access:users.clearkey(use:password_key)
                    use:password        = glo:password
                    access:users.fetch(use:password_key)
                    exh:user = use:user_code
                    exh:status          = 'CONFIRMED RETURN OF 48 HOUR UNIT'
                    access:exchhist.insert()
                End!if access:exchhist.primerecord() = Level:Benign

                Relate:EXCHOR48.Delete(0)
                Do AnyMoreUnitsToProcess
            End !If Accesss:EXCHANGE.Update() = Level:Benign
        End !If xch:Ref_Number <> ex4:OrderUnitNumber
    End !if Access:EXCHANGE.Fetch(xch:ESN_Only_Key)
Create_Requests_Queue Routine
    ! Fill queue with location requests
    data
tmp:Current_Location string(30)
    code

    free(Requests_Q)

    !Build list of locations that required exchanges -  (DBH: 21-10-2003)
    if records(EXCHOR48) = 0 then exit.

    Save_ex4_ID = Access:EXCHOR48.SaveFile()
    Access:EXCHOR48.ClearKey(ex4:LocationJobKey)
    ex4:Received  = 0
    Set(ex4:LocationJobKey,ex4:LocationJobKey)
    Loop
        If Access:EXCHOR48.NEXT()
           Break
        End !If
        If ex4:Received  <> 0      |
            Then Break.  ! End If

        !TB12448 second part hide franchises - JC - 12/07/12
        access:Tradeacc_Alias.clearkey(tra_ali:SiteLocationKey)
        tra_ali:SiteLocation = ex4:Location
        if access:Tradeacc_Alias.fetch(tra_ali:SiteLocationKey)
            !eh?? - let it show
        ELSE
            if tra_ali:Stop_Account = 'YES' then cycle.
        END

        Requested_Location  = ex4:Location

        Get(Requests_Q,Requested_Location)
        If Error()
            Add(Requests_Q,Requested_Location)
        End !If Error()
    End !Loop
    Access:EXCHOR48.RestoreFile(Save_ex4_ID)
Enable_IMEI_Scan Routine
    ! Disable / Enable IMEI field
    Disable(?tmp:Scanned_IMEI)
    ?tmp:Scanned_IMEI{prop:FillColor} = COLOR:Silver

    if tmp:From_Site_Location = '' then exit.
    if tmp:From_Stock_Type = '' then exit.
    if tmp:To_Site_Location = '' then exit.
    if tmp:To_Stock_Type = '' then exit.

    Enable(?tmp:Scanned_IMEI)
    ?tmp:Scanned_IMEI{prop:FillColor} = COLOR:White
End_Of_Process Routine

    tmp:Response = 0

    ! Check if all exchange units have been processed for this location
    More_To_Process# = 0
    Save_ex4_ID = Access:EXCHOR48.SaveFile()
    Access:EXCHOR48.ClearKey(ex4:LocationModelKey)
    ex4:Location     = tmp:To_Site_Location
    Set(ex4:LocationModelKey,ex4:LocationModelKey)
    Loop
        If Access:EXCHOR48.NEXT()
           Break
        End !If
        If ex4:Location     <> tmp:To_Site_Location      |
            Then Break.  ! End If
        If ex4:Received = 0
            More_To_Process# = 1
            Break
        End !If ex4:Received = 0

    End !Loop
    Access:EXCHOR48.RestoreFile(Save_ex4_ID)

    if More_To_Process# = 1
        Case Missive('Not all requests from ' & Clip(tmp:To_Site_Location) & ' have been fulfilled. '&|
          '<13,10>'&|
          '<13,10>Do you wish to go back and ALLOCATE this shortage now, or ignore the shortage and allocate LATER?','ServiceBase 3g',|
                       'mquest.jpg','\Cancel|Later|Allocate')
            Of 3 ! Allocate Button

                tmp:Response = 1
            Of 2 ! Later Button

                tmp:Response = 2
                ! Print WayBill
                do Print_Waybill
                !free(glo:q_JobNumber)

                BRW7.ResetFromView()
                BRW7.ResetSort(1)

            Of 1 ! Cancel Button
                tmp:Response = 1
        End ! Case Missive

!        Case MessageEx('All requests from ' & Clip(tmp:To_Site_Location) & ' have not been fulfilled. Do you wish to:'&|
!          '<13,10>'&|
!          '<13,10>1) GO BACK and allocate the shortage'&|
!          '<13,10>2) Leave the shortage and allocate LATER'&|
!          '<13,10>3) DELETE the shortage?'&|
!          '<13,10>','ServiceBase 2000',|
!                       'Styles\question.ico','|&Go Back && Allocate|&Allocate Later|&Delete Shortage',3,3,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
!            Of 1 ! &Go Back && Allocate Button
!                tmp:Response = 1
!            Of 2 ! &Allocate Later Button
!                tmp:Response = 2
!
!!                ! Remove location requests
!!                Access:EXCHORDR.ClearKey(exo:By_Location_Key)
!!                exo:Location = tmp:To_Site_Location
!!                set(exo:By_Location_Key,exo:By_Location_Key)
!!                loop until Access:EXCHORDR.Next()
!!                    if exo:Location <> tmp:To_Site_Location then break.
!!                    if exo:Qty_Received < exo:Qty_Required then
!!                        if exo:Qty_Received <> 0
!!                            exo:Qty_Required = exo:Qty_Required - exo:Qty_Received
!!                            exo:Qty_Received = 0
!!                            Access:EXCHORDR.TryUpdate()
!!                        end
!!                    else
!!                        Relate:EXCHORDR.Delete(0)
!!                        Access:EXCHORDR.ClearKey(exo:By_Location_Key)               ! Reset the key
!!                        exo:Location = tmp:To_Site_Location
!!                        set(exo:By_Location_Key,exo:By_Location_Key)
!!                    end
!!                end
!
!                ! Print WayBill
!                do Print_Waybill
!                free(glo:q_JobNumber)
!
!                BRW7.ResetFromView()
!                BRW7.ResetSort(1)
!
!            Of 3 ! &Delete Shortage Button
!                tmp:Response = 3
!
!                ! Remove location requests
!                Access:EXCHORDR.ClearKey(exo:By_Location_Key)
!                exo:Location = tmp:To_Site_Location
!                set(exo:By_Location_Key,exo:By_Location_Key)
!                loop until Access:EXCHORDR.Next()
!                    if exo:Location <> tmp:To_Site_Location then break.
!                    Relate:EXCHORDR.Delete(0)
!                    Access:EXCHORDR.ClearKey(exo:By_Location_Key)               ! Reset the key
!                    exo:Location = tmp:To_Site_Location
!                    set(exo:By_Location_Key,exo:By_Location_Key)
!                end
!
!                ! Print WayBill
!                do Print_Waybill
!                free(glo:q_JobNumber)
!
!                do Create_Requests_Queue                                        ! Reload requests queue
!
!                BRW7.ResetFromView()
!                BRW7.ResetSort(1)
!        END !CASE
    end
Print_Waybill Routine
    data
tmp:Type            string(3)
tmp:WayBillNo       string(30)
tmp:AccountNumber   string(15)
    code

    loop a# = 1 to len(clip(tmp:To_Site_Location))
        if tmp:To_Site_Location[a#] = '<32>' then break.
        tmp:AccountNumber[a#] = tmp:To_Site_Location[a#]
    end

    Access:TRADEACC.ClearKey(tra:Account_Number_Key)
    tra:Account_Number = tmp:AccountNumber
    if not Access:TRADEACC.Fetch(tra:Account_Number_Key)
        tmp:Type = 'TRA'
    else
        Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
        sub:Account_Number = tmp:AccountNumber
        Access:SUBTRACC.Fetch(sub:Account_Number_Key)
        tmp:Type = 'SUB'
    end

    !In case the tmp:courier was not set up
    if tmp:courier = ''
        Case tmp:Type
            Of 'TRA'
                tmp:Courier = tra:Courier_Incoming
            Of 'SUB'
                tmp:Courier = sub:Courier_Incoming
        End !Case func:Type
    END

    access:courier.clearkey(cou:Courier_Key)
    cou:Courier = tmp:Courier
    if access:courier.fetch(cou:Courier_Key)
        !tmp:WayBillNo = Clip(tmp:AccountNumber) & |
        tmp:WayBillNo = Format(Month(Today()),@n02) & Format(Day(Today()),@n02) & Format(Year(Today()),@n04) & |
                    Format(Clock(),@t2)
    ELSE
        !Get the next waybill number - 3384 (DBH: 10-10-2003)
        tmp:WaybillNo = NextWayBillNumber()
        !Do not continue if waybill error - 3432 (DBH: 27-10-2003)
        If tmp:WayBillNo = 0
            Exit
        End !If tmp:WayBillNo = 0
    END !If could find a courier

    ARCAccount" = Clip(GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI'))

    Access:WAYBILLS.ClearKey(way:WayBillNumberKey)
    way:WayBillNumber = tmp:WayBillNo
    If Access:WAYBILLS.TryFetch(way:WayBillNumberKey) = Level:Benign
        !Found
        way:AccountNumber = ARCAccount"
        way:WaybillID = 100 ! Exchange waybill
        way:FromAccount = ARCAccount"
        way:ToAccount = tmp:AccountNumber
        if Access:WAYBILLS.TryUpdate()
            !Access:WAYBILLS.CancelAutoInc()
        else
            ! Add items onto waybill
            Loop x# = 1 To Records(ProcessedUnitsQueue)
                Get(ProcessedUnitsQueue,x#)
                Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
                xch:Ref_Number = unique:UnitNumber
                if Access:EXCHANGE.Fetch(xch:Ref_Number_Key) then cycle.

                if not Access:WAYITEMS.PrimeRecord()
                    wai:WayBillNumber = way:WayBillNumber
                    wai:IsExchange = true
                    wai:Ref_Number = xch:Ref_Number
                    wai:JobNumber48Hour = unique:JobNumber
                    ! Inserting (DBH 25/02/2008) # 9717 - Signify that this is a replenishment unit
                    If unique:JobNumber = 0
                        wai:JobNumber48Hour = 0
                        wai:IsReplenishment = 1
                    End ! If unique:JobNumber = 0
                    ! End (DBH 25/02/2008) #9717

                    if Access:WAYITEMS.TryUpdate()
                        Access:WAYITEMS.CancelAutoInc()
                    Else !if Access:WAYITEMS.TryUpdate()
                        !Show the waybill number in the exchange unit history - 3999 (DBH: 04-03-2004)
                        If Access:EXCHHIST.PrimeRecord() = Level:Benign
                            exh:Ref_Number    = xch:Ref_Number
                            Access:USERS.Clearkey(use:Password_Key)
                            use:Password    = glo:Password
                            If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                                !Found
                            Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                                !Error
                            End !If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                            exh:User          = use:User_Code
                            exh:Status        = 'UNIT DESPATCHED ON WAYBILL: ' & Clip(way:WayBillNumber)
                            exh:Date          = Today()
                            exh:Time          = Clock()
                            If Access:EXCHHIST.TryInsert() = Level:Benign
                                !Insert Successful

                            Else !If Access:EXCHHIST.TryInsert() = Level:Benign
                                !Insert Failed
                                Access:EXCHHIST.CancelAutoInc()
                            End !If Access:EXCHHIST.TryInsert() = Level:Benign
                        End !If Access:EXCHHIST.PrimeRecord() = Level:Benign


                    End !if Access:WAYITEMS.TryUpdate()
                end

            End !Loop x# = 1 To Records(ProcessedUnitsQueue)
        end
    end

    WayBillExchange(ARCAccount",'TRA',|
                    tmp:AccountNumber,tmp:Type,|
                    tmp:WayBillNo,tmp:Courier,1)
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020278'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('RapidExchangeTransfer48Hour')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?tmp:Type:Radio1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:COURIER.Open
  Relate:ESNMODAL.Open
  Relate:EXCHANGE.Open
  Relate:EXCHOR48.Open
  Relate:TRADEACC_ALIAS.Open
  Relate:WAYBILLS.Open
  Relate:WAYITEMS.Open
  Access:LOCATION.UseFile
  Access:STOCKTYP.UseFile
  Access:TRADEACC.UseFile
  Access:SUBTRACC.UseFile
  Access:ESNMODEL.UseFile
  Access:EXCHHIST.UseFile
  SELF.FilesOpened = True
  BRW7.Init(?List,Queue:Browse.ViewPosition,BRW7::View:Browse,Queue:Browse,Relate:EXCHOR48,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  !fetch default courier
  tmp:courier = getini('DESPATCH','RETcourier','',clip(path())&'\SB2KDEF.INI')
  if tmp:courier = '' then
      Case Missive('A Default Courier has not been setup for Exchange Transfers.'&|
        '<13,10>'&|
        '<13,10>Use the menu option to setup a Default Courier.','ServiceBase 3g',|
                     'mstop.jpg','/OK')
          Of 1 ! OK Button
      End ! Case Missive
  END
  do Create_Requests_Queue    ! Fill queue with location requests
  Free(ProcessedUnitsQueue)
  Clear(ProcessedUnitsQueue)
  
  Access:LOCATION.ClearKey(loc:Main_Store_Key)
  loc:Main_Store = 'YES'
  set(loc:Main_Store_Key,loc:Main_Store_Key)
  loop until Access:LOCATION.Next()
      if loc:Main_Store <> 'YES' then break.
      tmp:From_Site_Location = loc:Location
      break
  end
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
  ?List{prop:vcr} = False
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW7.Q &= Queue:Browse
  BRW7.AddSortOrder(,ex4:ReturningModelKey)
  BRW7.AddRange(ex4:Location)
  BRW7.AddLocator(BRW7::Sort0:Locator)
  BRW7::Sort0:Locator.Init(,ex4:Manufacturer,1,BRW7)
  BIND('tmp:Notes',tmp:Notes)
  BIND('tmp:JobNumber',tmp:JobNumber)
  BIND('tmp:To_Site_Location',tmp:To_Site_Location)
  ?List{PROP:IconList,1} = '~bluetick.ico'
  BRW7.AddField(ex4:Manufacturer,BRW7.Q.ex4:Manufacturer)
  BRW7.AddField(ex4:ModelNumber,BRW7.Q.ex4:ModelNumber)
  BRW7.AddField(tmp:Notes,BRW7.Q.tmp:Notes)
  BRW7.AddField(tmp:JobNumber,BRW7.Q.tmp:JobNumber)
  BRW7.AddField(ex4:JobNumber,BRW7.Q.ex4:JobNumber)
  BRW7.AddField(ex4:RecordNumber,BRW7.Q.ex4:RecordNumber)
  BRW7.AddField(ex4:OrderUnitNumber,BRW7.Q.ex4:OrderUnitNumber)
  BRW7.AddField(ex4:Notes,BRW7.Q.ex4:Notes)
  BRW7.AddField(ex4:Received,BRW7.Q.ex4:Received)
  BRW7.AddField(ex4:Returning,BRW7.Q.ex4:Returning)
  BRW7.AddField(ex4:Location,BRW7.Q.ex4:Location)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW7.AskProcedure = 0
      CLEAR(BRW7.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:COURIER.Close
    Relate:ESNMODAL.Close
    Relate:EXCHANGE.Close
    Relate:EXCHOR48.Close
    Relate:TRADEACC_ALIAS.Close
    Relate:WAYBILLS.Close
    Relate:WAYITEMS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      PickSiteLocations
      Select_Stock_Type
      Select_Stock_Type
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Allocate_Button
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Allocate_Button, Accepted)
      if ?tmp:Scanned_IMEI{prop:Disable} = False
          select(?tmp:Scanned_IMEI)
      end
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Allocate_Button, Accepted)
    OF ?tmp:From_Stock_Type
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:From_Stock_Type, Accepted)
      glo:select1    = 'EXC'
      stp:use_exchange = 'YES'
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:From_Stock_Type, Accepted)
    OF ?From_Stock_Type_Button
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?From_Stock_Type_Button, Accepted)
      glo:select1    = 'EXC'
      stp:use_exchange = 'YES'
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?From_Stock_Type_Button, Accepted)
    OF ?tmp:To_Stock_Type
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:To_Stock_Type, Accepted)
      glo:select1    = 'EXC'
      stp:use_exchange = 'YES'
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:To_Stock_Type, Accepted)
    OF ?To_Stock_Type_Button
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?To_Stock_Type_Button, Accepted)
      glo:select1    = 'EXC'
      stp:use_exchange = 'YES'
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?To_Stock_Type_Button, Accepted)
    OF ?OkButton
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      if records(ProcessedUnitsQueue)
          do End_Of_Process
      
          if tmp:Response = 1
              select(?tmp:Scanned_IMEI)
              cycle
          end
      
      end
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OptionsSetCourier
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OptionsSetCourier, Accepted)
      globalrequest = selectrecord
      browse_Courier
      tmp:courier = cou:Courier
      putiniEXT('DESPATCH','RETcourier',tmp:courier,clip(path())&'/SB2KDEF.INI')
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OptionsSetCourier, Accepted)
    OF ?tmp:Type
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Type, Accepted)
      !Only show the Confirm Return for "Returning Units"
      !i.e. no order is required to be sent back -  (DBH: 21-10-2003)
      Case tmp:Type
          Of 0
              ?IMEIText{prop:Text} = 'I.M.E.I. Number Of Unit To Transfer'
          Of 1
              ?IMEIText{prop:Text} = 'Confirm I.M.E.I. Number Of Returning Unit'
      End !tmp:Type
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse)
      BRW7.ApplyRange
      BRW7.ResetSort(1)
      Select(?List)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Type, Accepted)
    OF ?ViewNotes
      ThisWindow.Update
      Exchange48HourOrderNotes
      ThisWindow.Reset
    OF ?ReprintWaybillButton
      ThisWindow.Update
      AskWaybillNumber
      ThisWindow.Reset
    OF ?tmp:Scanned_IMEI
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Scanned_IMEI, Accepted)
      BRW7.UpdateBuffer()
      Case tmp:Type
          Of 0 !Normal Transfer
              Do Transfer
          Of 1 !Unit Returning
              Do UnitReturning
      End !tmp:Type
      
      tmp:Scanned_IMEI = ''
      Brw7.ResetSort(1)
      display()
      select(?tmp:Scanned_IMEI)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Scanned_IMEI, Accepted)
    OF ?tmp:SiteLocation
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:SiteLocation, Accepted)
      if records(glo:q_JobNumber)
          tmp:Original_Location = tmp:To_Site_Location

!          GET(Requests_Q,CHOICE(?List1))
          if tmp:Original_Location <> tmp:SiteLocation
              do End_Of_Process

              if tmp:Response = 1
                  tmp:SiteLocation = tmp:Original_Location
      !            GET(Requests_Q,tmp:ListPos)
      !            Select(?List1,tmp:ListPos)
                  Select(tmp:SiteLocation)
                  cycle
              end
          end

      end

      !tmp:ListPos = CHOICE(?List1)
      !GET(Requests_Q,CHOICE(?List1))
      tmp:To_Site_Location = tmp:SiteLocation
      display(?tmp:To_Site_Location)
      do Enable_IMEI_Scan
      !BRW7.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse)
      BRW7.ApplyRange
      BRW7.ResetSort(1)
      Select(?List)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:SiteLocation, Accepted)
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020278'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020278'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020278'&'0')
      ***
    OF ?tmp:From_Site_Location
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:From_Site_Location, Accepted)
      if ~0{prop:AcceptAll}
          do Enable_IMEI_Scan
      end
      IF tmp:From_Site_Location OR ?tmp:From_Site_Location{Prop:Req}
        loc:Location = tmp:From_Site_Location
        !Save Lookup Field Incase Of error
        look:tmp:From_Site_Location        = tmp:From_Site_Location
        IF Access:LOCATION.TryFetch(loc:Location_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tmp:From_Site_Location = loc:Location
          ELSE
            !Restore Lookup On Error
            tmp:From_Site_Location = look:tmp:From_Site_Location
            SELECT(?tmp:From_Site_Location)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:From_Site_Location, Accepted)
    OF ?From_Location_Button
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickSiteLocations
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?From_Location_Button, Accepted)
      case GlobalResponse
          of RequestCompleted
              tmp:From_Site_Location = loc:Location
              display(?tmp:From_Site_Location)
      end
      
      do Enable_IMEI_Scan
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?From_Location_Button, Accepted)
    OF ?tmp:From_Stock_Type
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:From_Stock_Type, Accepted)
      glo:select1    = ''
      
      if ~0{prop:AcceptAll}
          do Enable_IMEI_Scan
      end
      IF tmp:From_Stock_Type OR ?tmp:From_Stock_Type{Prop:Req}
        stp:Stock_Type = tmp:From_Stock_Type
        !Save Lookup Field Incase Of error
        look:tmp:From_Stock_Type        = tmp:From_Stock_Type
        IF Access:STOCKTYP.TryFetch(stp:Use_Exchange_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            tmp:From_Stock_Type = stp:Stock_Type
          ELSE
            !Restore Lookup On Error
            tmp:From_Stock_Type = look:tmp:From_Stock_Type
            SELECT(?tmp:From_Stock_Type)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:From_Stock_Type, Accepted)
    OF ?From_Stock_Type_Button
      ThisWindow.Update
      GlobalRequest = SelectRecord
      Select_Stock_Type
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?From_Stock_Type_Button, Accepted)
      glo:select1    = ''
      
      case GlobalResponse
          of RequestCompleted
              tmp:From_Stock_Type = stp:Stock_Type
              display(?tmp:From_Stock_Type)
      end
      
      do Enable_IMEI_Scan
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?From_Stock_Type_Button, Accepted)
    OF ?tmp:To_Stock_Type
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:To_Stock_Type, Accepted)
      glo:select1    = ''
      
      if ~0{prop:AcceptAll}
          do Enable_IMEI_Scan
      end
      IF tmp:To_Stock_Type OR ?tmp:To_Stock_Type{Prop:Req}
        stp:Stock_Type = tmp:To_Stock_Type
        !Save Lookup Field Incase Of error
        look:tmp:To_Stock_Type        = tmp:To_Stock_Type
        IF Access:STOCKTYP.TryFetch(stp:Use_Exchange_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            tmp:To_Stock_Type = stp:Stock_Type
          ELSE
            !Restore Lookup On Error
            tmp:To_Stock_Type = look:tmp:To_Stock_Type
            SELECT(?tmp:To_Stock_Type)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:To_Stock_Type, Accepted)
    OF ?To_Stock_Type_Button
      ThisWindow.Update
      GlobalRequest = SelectRecord
      Select_Stock_Type
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?To_Stock_Type_Button, Accepted)
      glo:select1    = ''
      
      case GlobalResponse
          of RequestCompleted
              tmp:To_Stock_Type = stp:Stock_Type
              display(?tmp:To_Stock_Type)
      end
      
      do Enable_IMEI_Scan
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?To_Stock_Type_Button, Accepted)
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?List
    ! Before Embed Point: %ControlHandling) DESC(Control Handling) ARG(?List)
    If ex4:JobNumber = 0
        tmp:To_Stock_Type = 'REPLENISHMENT'
    Else ! If ex4:JobNumber = 0
        tmp:To_Stock_Type = '48-HOUR SWAP UNITS'
    End ! If ex4:JobNumber = 0
    Display(?tmp:To_Stock_Type)
    ! After Embed Point: %ControlHandling) DESC(Control Handling) ARG(?List)
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?tmp:SiteLocation
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse)
      BRW7.ApplyRange
      BRW7.ResetSort(1)
      Select(?List)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(CloseWindow)
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
      if keycode() = EscKey
          cycle
      end
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(CloseWindow)
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW7.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  GET(SELF.Order.RangeList.List,1)
  Self.Order.RangeList.List.Right = 0
  GET(SELF.Order.RangeList.List,2)
  Self.Order.RangeList.List.Right = tmp:Type
  GET(SELF.Order.RangeList.List,3)
  Self.Order.RangeList.List.Right = tmp:SiteLocation
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW7.SetQueueRecord PROCEDURE

  CODE
  IF (ex4:JobNumber > 0)
    tmp:JobNumber = ex4:JobNumber
  ELSE
    tmp:JobNumber = 'Replenishment Unit'
  END
  PARENT.SetQueueRecord
  SELF.Q.ex4:Manufacturer_NormalFG = -1
  SELF.Q.ex4:Manufacturer_NormalBG = -1
  SELF.Q.ex4:Manufacturer_SelectedFG = -1
  SELF.Q.ex4:Manufacturer_SelectedBG = -1
  SELF.Q.ex4:ModelNumber_NormalFG = -1
  SELF.Q.ex4:ModelNumber_NormalBG = -1
  SELF.Q.ex4:ModelNumber_SelectedFG = -1
  SELF.Q.ex4:ModelNumber_SelectedBG = -1
  SELF.Q.tmp:Notes_NormalFG = -1
  SELF.Q.tmp:Notes_NormalBG = -1
  SELF.Q.tmp:Notes_SelectedFG = -1
  SELF.Q.tmp:Notes_SelectedBG = -1
  IF (ex4:Notes <> '')
    SELF.Q.tmp:Notes_Icon = 1
  ELSE
    SELF.Q.tmp:Notes_Icon = 0
  END
  SELF.Q.tmp:JobNumber_NormalFG = -1
  SELF.Q.tmp:JobNumber_NormalBG = -1
  SELF.Q.tmp:JobNumber_SelectedFG = -1
  SELF.Q.tmp:JobNumber_SelectedBG = -1
  SELF.Q.ex4:JobNumber_NormalFG = -1
  SELF.Q.ex4:JobNumber_NormalBG = -1
  SELF.Q.ex4:JobNumber_SelectedFG = -1
  SELF.Q.ex4:JobNumber_SelectedBG = -1
  SELF.Q.ex4:RecordNumber_NormalFG = -1
  SELF.Q.ex4:RecordNumber_NormalBG = -1
  SELF.Q.ex4:RecordNumber_SelectedFG = -1
  SELF.Q.ex4:RecordNumber_SelectedBG = -1
  SELF.Q.ex4:OrderUnitNumber_NormalFG = -1
  SELF.Q.ex4:OrderUnitNumber_NormalBG = -1
  SELF.Q.ex4:OrderUnitNumber_SelectedFG = -1
  SELF.Q.ex4:OrderUnitNumber_SelectedBG = -1
  SELF.Q.ex4:Notes_NormalFG = -1
  SELF.Q.ex4:Notes_NormalBG = -1
  SELF.Q.ex4:Notes_SelectedFG = -1
  SELF.Q.ex4:Notes_SelectedBG = -1
  SELF.Q.tmp:JobNumber = tmp:JobNumber                !Assign formula result to display queue


BRW7.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

Exchange48HourOrderNotes PROCEDURE                    !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Order Notes'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Order Notes'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Order Notes'),AT(244,158),USE(?Prompt3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           TEXT,AT(244,168,204,72),USE(ex4:Notes),SKIP,FONT(,,,FONT:bold),COLOR(COLOR:White),READONLY
                         END
                       END
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(448,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020276'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Exchange48HourOrderNotes')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?OK,RequestCancelled)
  Relate:EXCHORDR.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EXCHORDR.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020276'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020276'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020276'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
UpdateTradeHoursOfBusiness PROCEDURE                  !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ActionMessage        CSTRING(40)
History::tbh:Record  LIKE(tbh:RECORD),STATIC
QuickWindow          WINDOW('Update Courier Hours Of Business'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert / Amend Trade Account Hours Of Business'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Date'),AT(234,149),USE(?tbh:TheDate:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(322,149,64,10),USE(tbh:TheDate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Date'),TIP('Date'),REQ,UPR
                           BUTTON,AT(392,146),USE(?LookupDate),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           OPTION('Exception Type'),AT(234,173,212,28),USE(tbh:ExceptionType),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Exception Type')
                             RADIO('Override Default Hours'),AT(238,186),USE(?Option1:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                             RADIO('Mark As "Day Off"'),AT(354,186),USE(?Option1:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                           END
                           GROUP,AT(216,216,220,32),USE(?TimeGroup),DISABLE
                             PROMPT('Start Time'),AT(234,218),USE(?tbh:StartTime:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@t1b),AT(322,218,64,10),USE(tbh:StartTime),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Start Time'),TIP('Start Time'),UPR
                             PROMPT('(HH:MM)'),AT(394,218),USE(?Prompt4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('End Time'),AT(234,234),USE(?tbh:EndTime:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@t1b),AT(322,234,64,10),USE(tbh:EndTime),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('End Time'),TIP('End Time'),UPR
                             PROMPT('(HH:MM)'),AT(394,234),USE(?Prompt4:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           END
                         END
                       END
                       BUTTON,AT(376,332),USE(?OK),TRN,FLAT,ICON('okp.jpg'),DEFAULT
                       BUTTON,AT(444,332),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
ExceptionTypeDisplay  Routine
    Case tbh:ExceptionType
        Of 0
            ?TimeGroup{prop:Disable} = 0
        OF 1
            ?TimeGroup{prop:Disable} = 1
    End !Case tbh:ExceptionType
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020300'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('UpdateTradeHoursOfBusiness')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(tbh:Record,History::tbh:Record)
  SELF.AddHistoryField(?tbh:TheDate,3)
  SELF.AddHistoryField(?tbh:ExceptionType,4)
  SELF.AddHistoryField(?tbh:StartTime,5)
  SELF.AddHistoryField(?tbh:EndTime,6)
  SELF.AddUpdateFile(Access:TRABUSHR)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:TRABUSHR.Open
  Access:TRADEACC.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:TRABUSHR
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Do ExceptionTypeDisplay
  
  Bryan.CompFieldColour()
  ?tbh:TheDate{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:TRABUSHR.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020300'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020300'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020300'&'0')
      ***
    OF ?LookupDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tbh:TheDate = TINCALENDARStyle1(tbh:TheDate)
          Display(?tbh:TheDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?tbh:ExceptionType
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tbh:ExceptionType, Accepted)
      Do ExceptionTypeDisplay
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tbh:ExceptionType, Accepted)
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?tbh:TheDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupDate)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

InsertTradeHoursOfBusiness PROCEDURE (func:RecordNumber) !Generated from procedure template - Window

save_tra_id          USHORT,AUTO
Local                CLASS
AssignDates          Procedure(Long func:RecordNumber)
                     END
tmp:StartDate        DATE
save_cou_id          USHORT,AUTO
tmp:EndDate          DATE
tmp:ExceptionType    BYTE(0)
tmp:StartTime        TIME
tmp:EndTime          TIME
tmp:Replicate        BYTE(0)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Insert Courier Hours Of Business'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('General'),USE(?Tab1)
                           PROMPT('Start Date'),AT(255,124),USE(?tmp:StartDate:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(331,124,64,10),USE(tmp:StartDate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Start Date'),TIP('Start Date'),REQ,UPR
                           BUTTON,AT(399,120),USE(?LookupStartDate),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('End Date'),AT(255,146),USE(?tmp:EndDate:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(331,146,64,10),USE(tmp:EndDate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('End Date'),TIP('End Date'),REQ,UPR
                           BUTTON,AT(399,142),USE(?LookupEndDate),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           OPTION('Exception Type'),AT(231,162,219,34),USE(tmp:ExceptionType),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('Override Default Hours'),AT(237,176),USE(?tmp:ExceptionType:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                             RADIO('Mark As "Day Off"'),AT(357,176),USE(?tmp:ExceptionType:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('2')
                           END
                           GROUP,AT(248,206,204,36),USE(?TimeGroup),DISABLE
                             PROMPT('Start Time'),AT(252,211),USE(?tmp:StartTime:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@t1b),AT(328,211,64,10),USE(tmp:StartTime),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Start Time'),TIP('Start Time'),REQ,UPR
                             PROMPT('(HH:MM)'),AT(400,211),USE(?Prompt5),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('End Time'),AT(252,227),USE(?tmp:EndTime:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@t1b),AT(328,227,64,10),USE(tmp:EndTime),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('End Time'),TIP('End Time'),REQ,UPR
                             PROMPT('(HH:MM)'),AT(400,227),USE(?Prompt5:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           END
                           CHECK('Replicate To All Accounts'),AT(285,256),USE(tmp:Replicate),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Replicate To All Couriers'),TIP('Replicate To All Couriers'),VALUE('1','0')
                         END
                       END
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert Hours Of Business'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(372,332),USE(?Button4),TRN,FLAT,ICON('okp.jpg')
                       BUTTON,AT(444,332),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                     END

Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?Prog:CNPrompt),FONT(,14,,FONT:bold)
     END
***

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020295'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('InsertTradeHoursOfBusiness')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?tmp:StartDate:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:TRABUSHR.Open
  Access:TRADEACC.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Bryan.CompFieldColour()
  ?tmp:StartDate{Prop:Alrt,255} = MouseLeft2
  ?tmp:EndDate{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:TRABUSHR.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?LookupStartDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:StartDate = TINCALENDARStyle1(tmp:StartDate)
          Display(?tmp:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?LookupEndDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:EndDate = TINCALENDARStyle1(tmp:EndDate)
          Display(?tmp:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?tmp:ExceptionType
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ExceptionType, Accepted)
      Case tmp:ExceptionType
          Of 1
              ?TimeGroup{prop:Disable} = 0
          Else
              ?TimeGroup{prop:Disable} = 1
      End !tmp:ExceptionType
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ExceptionType, Accepted)
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020295'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020295'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020295'&'0')
      ***
    OF ?Button4
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4, Accepted)
      Error# = 0
      If tmp:StartDate = '' And Error# = 0
          Case Missive('Please select a Start Date.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          Select(?tmp:StartDate)
          Error# = 1
      End !tmp:StartDate = ''
      
      If tmp:EndDate = '' And Error# = 0
          Case Missive('Please select an End Date.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          Select(?tmp:EndDate)
          Error# = 1
      End !tmp:EndDate = '' And Error# = 0
      
      If tmp:ExceptionType = 0 And Error# = 0
          Case Missive('Please select an Exception Type.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          Select(?tmp:ExceptionType)
          Error# = 1
      End !tmp:ExceptionType = 0 And Error# = 0
      
      If tmp:ExceptionType = 1 And tmp:StartTime = 0 And Error# = 0
          Case Missive('Please select a Start Time.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          Select(?tmp:Starttime)
          Error# = 1
      End !tmp:ExceptionType = 1 And tmp:StartTime = 0
      
      If tmp:ExceptionType = 1 And tmp:EndTime = 0  And Error# = 0
          Case Missive('Please select an End Time.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          Select(?tmp:Endtime)
          Error# = 1
      End !tmp:ExceptionType = 1 And tmp:StartTime = 0
      
      If tmp:EndDate < tmp:StartDate And Error# = 0
          Case Missive('Invalid Date Range.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          Select(?tmp:StartDate)
          Error# = 1
      End !tmp:EndDate < tmp:StartDate And Error# = 0
      
      If tmp:ExceptionType =1 And tmp:EndTime < tmp:StartTime And Error# = 0
          Case Missive('Invalid Time Range.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          Select(?tmp:StartTime)
          Error# = 1
      End !tmp:ExceptionType =1 And tmp:EndTime < tmp:StartTime And Error# = 0
      
      If tmp:Replicate  And Error# = 0
          Case Missive('Are you sure you want to replicate this information to ALL accounts?','ServiceBase 3g',|
                         'mquest.jpg','\No|/Yes')
              Of 2 ! Yes Button
              Of 1 ! No Button
                  Error# = 1
          End ! Case Missive
      End !tmp:Replicate  And Error# = 0
      
      If Error#
          Cycle
      End !Error#
      
          !_____________________________________________________________________
      
      
      If tmp:Replicate
          Prog.ProgressSetup(Records(TRADEACC) * (tmp:EndDate - tmp:StartDate))
      
          Save_tra_ID = Access:TRADEACC.SaveFile()
          Set(tra:RecordNumberKey)
          Loop
              If Access:TRADEACC.NEXT()
                 Break
              End !If
              Local.AssignDates(tra:RecordNumber)
          End !Loop
          Access:TRADEACC.RestoreFile(Save_tra_ID)
      Else !tmp:Replicate
          Prog.ProgressSetup(tmp:EndDate - tmp:StartDate)
      
          Local.AssignDates(func:RecordNumber)
      
      End !tmp:Replicate
      Prog.ProgressFinish()
      
      Post(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?tmp:StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupStartDate)
      CYCLE
    END
  OF ?tmp:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupEndDate)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! Before Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
Local.AssignDates    Procedure(Long func:LocalRecordNumber)
Code

    Loop x# = tmp:StartDate to tmp:EndDate
        If Prog.InsideLoop()
            Break
        End !If Prog.InsideLoop()
        !Has this date already been inserted? -  (DBH: 12-11-2003)
        Access:TRABUSHR.ClearKey(tbh:TypeDateKey)
        tbh:RefNumber = func:LocalRecordNumber
        tbh:TheDate = x#
        If Access:TRABUSHR.TryFetch(tbh:TypeDateKey) = Level:Benign
            !Found
            Cycle
        Else !If Access:COUBUSHR.TryFetch(tbh:TypeDateKey) = Level:Benign
            !Error
        End !If Access:COUBUSHR.TryFetch(tbh:TypeDateKey) = Level:Benign

        If Access:TRABUSHR.PrimeRecord() = Level:Benign
            tbh:RefNumber       = func:LocalRecordNumber
            tbh:TheDate       = x#
            Case tmp:ExceptionType
                Of 1
                    tbh:ExceptionType   = 0
                    tbh:StartTime       = tmp:StartTime
                    tbh:EndTime         = tmp:EndTime

                Of 2
                    tbh:ExceptionType   = 1
                    tbh:StartTime       = 0
                    tbh:EndTime         = 0
            End !Case tmp:ExceptionType
            If Access:TRABUSHR.TryInsert() = Level:Benign
                !Insert Successful

            Else !If Access:COUBUSHR.TryInsert() = Level:Benign
                !Insert Failed
                Access:TRABUSHR.CancelAutoInc()
            End !If Access:COUBUSHR.TryInsert() = Level:Benign
        End !If Access:COUBUSHR.PrimeRecord() = Level:Benign

    End !x# = tmp:StartDate to tmp:EndDate
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
! After Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
