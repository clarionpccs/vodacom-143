

   MEMBER('WebServer_Phase4.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSE083.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSE013.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE014.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE021.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE026.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE036.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE037.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE063.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE068.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE074.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE077.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE078.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE093.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE094.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE099.INC'),ONCE        !Req'd for module callout resolution
                     END


FormChargeableParts  PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
tmp:Location         STRING(30)                            !
tmp:ShelfLocation    STRING(30)                            !
tmp:SecondLocation   STRING(30)                            !
tmp:PurchaseCost     REAL                                  !
tmp:OutWarrantyCost  REAL                                  !
tmp:OutWarrantyMarkup REAL                                 !
tmp:ARCPart          BYTE                                  !
tmp:FaultCodesChecked BYTE                                 !
tmp:FaultCodes2      STRING(30)                            !
tmp:FaultCodes4      STRING(30)                            !
tmp:FaultCodes5      STRING(30)                            !
tmp:FaultCodes6      STRING(30)                            !
tmp:FaultCodes7      STRING(30)                            !
tmp:FaultCodes8      STRING(30)                            !
tmp:FaultCodes9      STRING(30)                            !
tmp:FaultCodes10     STRING(30)                            !
tmp:FaultCodes11     STRING(30)                            !
tmp:FaultCodes12     STRING(30)                            !
tmp:FaultCodes3      STRING(30)                            !
tmp:FaultCodes1      STRING(30)                            !
tmp:CreateOrder      BYTE                                  !
tmp:UnallocatePart   BYTE                                  !
locUserCode          STRING(3)                             !
FilesOpened     Long
PARTS::State  USHORT
STOMODEL::State  USHORT
STOCK::State  USHORT
SUPPLIER::State  USHORT
LOCATION::State  USHORT
CHARTYPE::State  USHORT
MANFAUPA::State  USHORT
MANFAULT::State  USHORT
MANFPALO::State  USHORT
DEFAULTS::State  USHORT
PARTS_ALIAS::State  USHORT
STOHIST::State  USHORT
par:Order_Number:IsInvalid  Long
par:Date_Ordered:IsInvalid  Long
par:Date_Received:IsInvalid  Long
locOutFaultCode:IsInvalid  Long
par:Part_Number:IsInvalid  Long
par:Description:IsInvalid  Long
par:Despatch_Note_Number:IsInvalid  Long
par:Quantity:IsInvalid  Long
tmp:Location:IsInvalid  Long
tmp:SecondLocation:IsInvalid  Long
tmp:ShelfLocation:IsInvalid  Long
tmp:PurchaseCost:IsInvalid  Long
tmp:OutWarrantyCost:IsInvalid  Long
tmp:OutWarrantyMarkup:IsInvalid  Long
tmp:FixedPrice:IsInvalid  Long
par:Supplier:IsInvalid  Long
par:Exclude_From_Order:IsInvalid  Long
par:PartAllocated:IsInvalid  Long
tmp:UnallocatePart:IsInvalid  Long
text:OrderRequired:IsInvalid  Long
text:OrderRequired2:IsInvalid  Long
tmp:CreateOrder:IsInvalid  Long
tmp:FaultCodesChecked:IsInvalid  Long
tmp:FaultCodes1:IsInvalid  Long
tmp:FaultCode2:IsInvalid  Long
tmp:FaultCode3:IsInvalid  Long
tmp:FaultCode4:IsInvalid  Long
tmp:FaultCode5:IsInvalid  Long
tmp:FaultCode6:IsInvalid  Long
tmp:FaultCode7:IsInvalid  Long
tmp:FaultCode8:IsInvalid  Long
tmp:FaultCode9:IsInvalid  Long
tmp:FaultCode10:IsInvalid  Long
tmp:FaultCode11:IsInvalid  Long
tmp:FaultCode12:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
local       Class
AfterFaultCodeLookup Procedure(Long fNumber)
SetLookupButton      Procedure(Long fNumber)
            End
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormChargeableParts')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'FormChargeableParts_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('FormChargeableParts','')
    p_web.DivHeader('FormChargeableParts',p_web.combine(p_web.site.style.formdiv,'fdiv'))
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('FormChargeableParts') = 0
        p_web.AddPreCall('FormChargeableParts')
        p_web.DivHeader('popup_FormChargeableParts','nt-hidden')
        p_web.DivHeader('FormChargeableParts',p_web.combine(p_web.site.style.formdiv,'fdiv'))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_FormChargeableParts_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_FormChargeableParts_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormChargeableParts',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:StagePost
    do RestoreMem
    do PostInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_FormChargeableParts',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormChargeableParts',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:StagePost
    do RestoreMem
    do PostCopy
  of Net:CopyRecord + NET:WEB:Populate
    If p_web.IfExistsValue('par:Record_Number') = 0 then p_web.SetValue('par:Record_Number',p_web.GSV('par:Record_Number')).
    do PreCopy
    p_web.setsessionvalue('showtab_FormChargeableParts',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormChargeableParts',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormChargeableParts',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do PostInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do PostUpdate
    End
  of Net:ChangeRecord + NET:WEB:Populate
    If p_web.IfExistsValue('par:Record_Number') = 0 then p_web.SetValue('par:Record_Number',p_web.GSV('par:Record_Number')).
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_FormChargeableParts',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormChargeableParts',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:DeleteRecord + NET:WEB:StagePost
    do RestoreMem
    do PostDelete
  of Net:ViewRecord + NET:WEB:Populate
    If p_web.IfExistsValue('par:Record_Number') = 0 then p_web.SetValue('par:Record_Number',p_web.GSV('par:Record_Number')).
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_FormChargeableParts',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormChargeableParts',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormChargeableParts',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormChargeableParts',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('FormChargeableParts')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
buildFaultCodes    Routine
data
locFoundFault      Byte(0)
code
    ! Clear Variables
    if (p_web.GSV('FormChargeableParts:FirstTime') = 0)
        loop x# = 1 To 12
            p_web.SSV('Hide:PartFaultCode' & x#,1)
            p_web.SSV('Req:PartFaultCode' & x#,0)
            p_web.SSV('ReadOnly:PartFaultCode' & x#,0)
            p_web.SSV('Prompt:PartFaultCode' & x#,'Fault Code ' & x#)
            p_web.SSV('Picture:PartFaultCode' & x#,'@s30')
            p_web.SSV('ShowDate:PartFaultCode' & x#,0)
            p_web.SSV('Lookup:PartFaultCode' & x#,0)
            p_web.SSV('Comment:PartFaultCode' & x#,'')
        end ! loop x# = 1 To 12
        p_web.SSV('Hide:FaultCodesChecked',1)
    end ! if (p_web.GSV('FormChargeableParts:FirstTime') = 0)

    locMainFaultOnly# = 0
    Access:MANFAUPA.Clearkey(map:MainFaultKey)
    map:Manufacturer    = p_web.GSV('job:Manufacturer')
    map:MainFault    = 1
    if (Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign)
        ! Found
        if (sto:Accessory <> 'YES' and man:ForceAccessoryCode)
            !Main Fault Only
            locMainFaultOnly# = 1
        end ! if (sto:Accessory <> 'YES' and man:ForceAccessoryCode)
    else ! if (Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign)
        ! Error
    end ! if (Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign)

    Access:MANFAUPA.Clearkey(map:ScreenOrderKey)
    map:Manufacturer    = p_web.GSV('job:Manufacturer')
    map:ScreenOrder    = 0
    set(map:ScreenOrderKey,map:ScreenOrderKey)
    loop
        if (Access:MANFAUPA.Next())
            Break
        end ! if (Access:MANFAUPA.Next())
        if (map:Manufacturer    <> p_web.GSV('job:Manufacturer'))
            Break
        end ! if (map:Manufacturer    <> p_web.GSV('job:Manufacturer'))

        if (map:ScreenOrder = 0)
            cycle
        end ! if (map:ScreenOrder = 0)

        if (locMainFaultOnly# = 1)
            if (map:MainFault = 0)
                cycle
            end ! if (map:MainFault = 0)
        end ! if (locMainFaultOnly# = 1)

        p_web.SSV('Hide:PartFaultCode' & map:ScreenOrder,0)
        p_web.SSV('Prompt:PartFaultCode' & map:ScreenOrder,map:Field_Name)

        if (map:Compulsory = 'YES')
            if (p_web.GSV('par:Adjustment') = 'YES')
                if (map:CompulsoryForAdjustment)
                    p_web.SSV('Req:PartFaultCode' & map:ScreenOrder,1)
                    p_web.SSV('Comment:PartFaultCode' & map:ScreenOrder,'Required')
                end ! if (map:CompulsoryForAdjustment)
            else !if (p_web.GSV('par:Adjustment') = 'YES')
                p_web.SSV('Req:PartFaultCode' & map:ScreenOrder,1)
                p_web.SSV('Comment:PartFaultCode' & map:ScreenOrder,'Required')
            end ! if (p_web.GSV('par:Adjustment') = 'YES')
        else ! if (map:Compulsory = 'YES')
            p_web.SSV('Req:PartFaultCode' & map:ScreenOrder,0)
        end ! if (map:Compulsory = 'YES')

        if (map:MainFault)
            !This is the main fault, use the job main fault
            Access:MANFAULT.Clearkey(maf:MainFaultKey)
            maf:Manufacturer    = p_web.GSV('job:Manufacturer')
            maf:MainFault    = 1
            if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)
                ! Found
                case maf:Field_Type
                of 'DATE'
                    p_web.SSV('Picture:PartFaultCode' & map:ScreenOrder,clip(maf:DateType))
                    ! Date Lookup Required
                of 'STRING'
                    if (maf:RestrictLength)
                        p_web.SSV('Picture:PartFaultCode' & map:ScreenOrder,'@s' & maf:LengthTo)
                    end !~ if (maf:RestrictLength)

                    ! Lookup Required
                of 'NUMBER'
                    if (maf:RestrictLength)
                        p_web.SSV('Picture:PartFaultCode' & map:ScreenOrder,'@n_' & maf:LengthTo)
                    else !end !~ if (maf:RestrictLength)
                        p_web.SSV('Picture:PartFaultCode' & map:ScreenOrder,'@n_9')
                    end !~ if (maf:RestrictLength)
                end ! case maf:Field_Type

                if (maf:Lookup = 'YES')
                    p_web.SSV('Lookup:PartFaultCode' & map:ScreenOrder,1)
                end ! if (map:Lookup = 'YES')
            else ! if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)
                ! Error
            end ! if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)
        else ! if (map:MainFault)
            case map:Field_Type
            of 'DATE'
                p_web.SSV('Picture:PartFaultCode' & map:ScreenOrder,clip(map:DateType))
                p_web.SSV('ShowDate:PartFaultCode' & map:ScreenOrder,1)
                ! Date Lookup Required
            of 'STRING'
                if (map:RestrictLength)
                    p_web.SSV('Picture:PartFaultCode' & map:ScreenOrder,'@s' & map:LengthTo)
                end !~ if (maf:RestrictLength)

                ! Lookup Required
            of 'NUMBER'
                if (map:RestrictLength)
                    p_web.SSV('Picture:PartFaultCode' & map:ScreenOrder,'@n_' & map:LengthTo)
                else !end !~ if (maf:RestrictLength)
                    p_web.SSV('Picture:PartFaultCode' & map:ScreenOrder,'@n_9')
                end !~ if (maf:RestrictLength)
            end ! case maf:Field_Type
        end !if (map:MainFault)

        if (map:Lookup = 'YES')
            p_web.SSV('Lookup:PartFaultCode' & map:ScreenOrder,1)

        end ! if (map:Lookup = 'YES')

        if (map:NotAvailable = 1)
            if (p_web.GSV('tmp:FaultCode' & map:ScreenOrder) = '')
                p_web.SSV('Hide:PartFaultCode' & map:ScreenOrder,1)
            else ! if (p_web.GSV('tmp:FaultCode' & map:ScreenOrder) = '')
                p_web.SSV('ReadOnly:PartFaultCode' & map:ScreenOrder,1)
            end ! if (p_web.GSV('tmp:FaultCode' & map:ScreenOrder) = '')
        end ! if (map:NotAvailable = 1)

        if (map:CopyFromJobFaultCode And p_web.GSV('tmp:FaulCode' & map:ScreenOrder) = '')
            if (map:ScreenOrder < 13)
                p_web.SSV('tmp:FaultCode' & map:ScreenOrder,p_web.GSV('job:Fault_Code' & map:CopyJobFaultCode))
            else ! if (map:ScreenOrder < 13)
                p_web.SSV('tmp:FaultCode' & map:ScreenOrder,p_web.GSV('wob:FaultCode' & map:CopyJobFaultCode))
            end ! if (map:ScreenOrder < 13)
        end ! if (map:CopyFromJobFaultCode And p_web.GSV('tmp:FaulCode' & map:ScreenOrder) = '')

        if (p_web.GSV('tmp:FaultCode' & map:ScreenOrder) = '' And sto:Assign_Fault_Codes = 'YES')
            Access:STOMODEL.Clearkey(stm:Model_Number_Key)
            stm:Ref_Number    = sto:Ref_Number
            stm:Manufacturer    = p_web.GSV('job:Manufacturer')
            stm:Model_Number    = p_web.GSV('job:Model_Number')
            if (Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign)
                ! Found
                Case map:Field_Number
                Of 1
                    If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode1)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 2
                    If stm:FaultCode2 <> '' And stm:FaultCode2 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode2)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 3
                    If stm:FaultCode3 <> '' And stm:FaultCode3 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode3)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 4
                    If stm:FaultCode4 <> '' And stm:FaultCode4 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode4)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 5
                    If stm:FaultCode5 <> '' And stm:FaultCode5 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode5)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 6
                    If stm:FaultCode6 <> '' And stm:FaultCode6 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode6)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 7
                    If stm:FaultCode7 <> '' And stm:FaultCode7 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode7)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 8
                    If stm:FaultCode8 <> '' And stm:FaultCode8 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode8)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 9
                    If stm:FaultCode9 <> '' And stm:FaultCode9 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode9)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 10
                    If stm:FaultCode10 <> '' And stm:FaultCode10 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode10)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 11
                    If stm:FaultCode11 <> '' And stm:FaultCode11 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode11)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 12
                    If stm:FaultCode12 <> '' And stm:FaultCode12 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode12)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                End ! Case map:Field_Number
            else ! if (Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign)
                ! Error
            end ! if (Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign)
        end ! if (p_web.GSV('tmp:FaultCode' & map:ScreenOrder) = '' And sto:Assign_Fault_Codes = 'YES')
        locFoundFault = 1
    end ! loop

    !Check if the part main fault is set to assign value to anouther fault code

    Access:MANFAUPA.Clearkey(map:MainFaultKey)
    map:Manufacturer    = p_web.GSV('job:Manufacturer')
    map:MainFault    = 1
    if (Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign)
        ! Found
        Access:MANFPALO.Clearkey(mfp:Field_Key)
        mfp:Manufacturer    = p_web.GSV('job:Manufacturer')
        mfp:Field_Number    = map:Field_Number
        mfp:Field    = p_web.GSV('tmp:FaultCode' & map:ScreenOrder)
        if (Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign)
            ! Found
            if (mfp:SetPartFaultCode)
                Access:MANFAUPA.Clearkey(map:Field_Number_Key)
                map:Manufacturer    = p_web.GSV('job:Manufacturer')
                map:Field_Number    = mfp:SelectPartFaultCode
                if (Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign)
                    ! Found
                    if (p_web.GSV('Hide:PartFaultCode' & map:ScreenOrder) <> 1 And |
                       p_web.GSV('tmp:FaultCode' & map:ScreenOrder) = '')
                       p_web.SSV('tmp:FaultCode' & map:ScreenOrder,mfp:PartFaultCodeValue)
                    end ! if (p_web.GSV('Hide:PartFaultCode' & map:ScreenOrder) <> 1 And |
                else ! if (Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign)
                    ! Error
                end ! if (Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign)
            end ! if (mfp:SetPartFaultCode)
        else ! if (Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign)
            ! Error
        end ! if (Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign)
    else ! if (Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign)
        ! Error
    end ! if (Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign)

    if (locFoundFault)
        p_web.SSV('Hide:FaultCodesChecked',0)
    end ! if (locFoundFault)
deleteSessionValues     Routine
    p_web.deleteSessionValue('adjustment')
    p_web.deleteSessionValue('FormChargeableParts:FirstTime')
    p_web.deleteSessionValue('locOrderRequired')
    p_web.deleteSessionValue('locOutFaultCode')

    p_web.deleteSessionValue('ReadOnly:OutWarrantyMarkup')
    p_web.deleteSessionValue('ReadOnly:PurchaseCost')
    p_web.deleteSessionValue('ReadOnly:OutWarrantyCost')
    p_web.deleteSessionValue('ReadOnly:Quantity')

    p_web.deleteSessionValue('Parts:ViewOnly')

    p_web.deleteSessionValue('Show:UnallocatePart')
enableDisableCosts    Routine
    p_web.SSV('tmp:FixedPrice','')
    if (p_web.GSV('tmp:OutWarrantyMarkup') = 0)
        p_web.SSV('ReadOnly:OutWarrantyCost',0)
    else ! if (p_web.GSV('tmp:OutWarrantyMarkup') = 0)
        p_web.SSV('ReadOnly:OutWarrantyCost',1)
    end ! if (p_web.GSV('tmp:OutWarrantyMarkup') = 0)

    if (p_web.GSV('job:Invoice_Number') = 0)
        if (p_web.GSV('tmp:InWarrantyMarkup') > 0)
            p_web.SSV('tmp:InWarrantyCost',VodacomClass.Markup(p_web.GSV('tmp:InWarrantyCost'),|
                                                p_web.GSV('tmp:PurchaseCost'),|
                                                p_web.GSV('tmp:InWarrantyMarkup')))
        end ! if (p_web.GSV('tmp:InWarrantyMarkup') > 0)
        if (p_web.GSV('tmp:OutWarrantyMarkup') > 0)
            p_web.SSV('tmp:OutWarrantyCost',VodacomClass.Markup(p_web.GSV('tmp:OutWarrantyCost'),|
                                                p_web.GSV('tmp:PurchaseCost'),|
                                                p_web.GSV('tmp:OutWarrantyMarkup')))
        end ! if (p_web.GSV('tmp:OutWarrantyMarkup') > 0)
    end ! if (p_web.GSV('job:Invoice_Number') = 0)

    Access:CHARTYPE.Clearkey(cha:Charge_Type_Key)
    cha:Charge_Type    = p_web.GSV('job:Charge_Type')
    if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)
        ! Found
        if (cha:Zero_Parts_ARC)
           if (p_web.GSV('tmp:ARCPart') = 1)
               p_web.SSV('tmp:FixedPrice','FIXED PRICE')
               p_web.SSV('ReadOnly:OutWarrantyMarkup',1)
               p_web.SSV('ReadOnly:OutWarrantyCost',1)
               p_web.SSV('tmp:OutWarrantyCost',0)
           end ! if (p_web.GSV('tmp:ARCPart'))
        end ! if (cha:Zero_Parts_ARC)

        if (cha:Zero_Parts = 'YES')
           if (p_web.GSV('tmp:ARCPart') = 0)
               p_web.SSV('tmp:FixedPrice','FIXED PRICE')
               p_web.SSV('ReadOnly:OutWarrantyMarkup',1)
               p_web.SSV('ReadOnly:OutWarrantyCost',1)
               p_web.SSV('tmp:OutWarrantyCost',0)
           end ! if (p_web.GSV('tmp:ARCPart'))
        end ! if (cha:Zero_Parts = 'YES')
    else ! if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)
        ! Error
    end ! if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)
lookupLocation    Routine
    if (p_web.GSV('par:Part_Ref_Number') <> '')
        Access:STOCK.Clearkey(sto:Ref_Number_Key)
        sto:Ref_Number    = p_web.GSV('par:Part_Ref_Number')
        if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
            ! Found
            p_web.SSV('tmp:Location',sto:Location)
            p_web.SSV('tmp:ShelfLocation',sto:Shelf_Location)
            p_web.SSV('tmp:SecondLocation',sto:Second_Location)
        else ! if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
            ! Error
            p_web.SSV('tmp:Location','')
            p_web.SSV('tmp:ShelfLocation','')
            p_web.SSV('tmp:SecondLocation','')
        end ! if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
    else ! if (p_web.GSV('par:Part_Ref_Number') <> '')
        p_web.SSV('tmp:Location','')
        p_web.SSV('tmp:ShelfLocation','')
        p_web.SSV('tmp:SecondLocation','')
    end ! if (p_web.GSV('par:Part_Ref_Number') <> '')
lookupMainFault  Routine
    p_web.SSV('locOutFaultCode','')
    Access:MANFAUPA.Clearkey(map:MainFaultKey)
    map:Manufacturer    = p_web.GSV('job:Manufacturer')
    map:MainFault    = 1
    if (Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign)
        ! Found
        Access:MANFAULT.Clearkey(maf:MainFaultKey)
        maf:Manufacturer    = p_web.GSV('job:Manufacturer')
        maf:MainFault    = 1
        if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)
            ! Found

            Access:MANFAULO.Clearkey(mfo:Field_Key)
            mfo:Manufacturer    = p_web.GSV('job:Manufacturer')
            mfo:Field_Number    = maf:Field_Number
            mfo:Field    = p_web.GSV('tmp:FaultCodes' & map:ScreenOrder)
            if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                ! Found
                p_web.SSV('locOutFaultCode','Out Fault Code: ' & mfo:Description)
            else ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                ! Error
            end ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)

        else ! if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)
            ! Error
        end ! if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)

    else ! if (Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign)
        ! Error
    end ! if (Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign)
showCosts      Routine
    Access:STOCK.Clearkey(sto:Ref_Number_Key)
    sto:Ref_Number    = p_web.GSV('par:Part_Ref_Number')
    if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
        ! Found
        if (sto:Location <> p_web.GSV('ARC:SiteLocation'))
            p_web.SSV('tmp:ARCPart',0)
            if (p_web.GSV('BookingSite') <> 'RRC')
                if (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'AMEND RRC PART'))
                    p_web.SSV('Parts:ViewOnly',1)
                end ! if (SecurityCheckFailed(p_web.GSV('BookingUser'),'AMEND RRC PART'))
            end ! if (p_web.GSV('BookingSite') <> 'RRC')
        else ! if (sto:Location <> p_web.GSV('Default:SiteLocation'))
            p_web.SSV('tmp:ARCPart',1)
            if (p_web.GSV('BookingSite') <> 'ARC')
                p_web.SSV('Parts:ViewOnly',1)
            end ! if (p_web.GSV('BookingSite') <> 'ARC')
        end ! if (sto:Location <> p_web.GSV('Default:SiteLocation'))
    else ! if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
        ! Error
    end ! if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
    if (p_web.GSV('tmp:ARCPart') = 1)
        p_web.SSV('tmp:PurchaseCost',p_web.GSV('par:AveragePurchaseCost'))
        p_web.SSV('tmp:InWarrantyCost',p_web.GSV('par:Purchase_Cost'))
        p_web.SSV('tmp:OutWarrantyCost',p_web.GSV('par:Sale_Cost'))
        p_web.SSV('tmp:InWarrantyMarkup',p_web.GSV('par:InWarrantyMarkup'))
        p_web.SSV('tmp:OutWarrantyMarkup',p_web.GSV('par:OutWarrantyMarkup'))
    else ! if (tmp:ARCPart)
        if (p_web.GSV('par:RRCAveragePurchaseCost') > 0)
            p_web.SSV('tmp:PurchaseCost',p_web.GSV('par:RRCAveragePurchaseCost'))
        else ! if (p_web.GSV('par:RRCAveragePurchaseCost') > 0)
            p_web.SSV('tmp:PurchaseCost',p_web.GSV('par:RRCPurchaseCost'))
        end ! if (p_web.GSV('par:RRCAveragePurchaseCost') > 0)

        p_web.SSV('tmp:InWarrantyCost',p_web.GSV('par:RRCPurchaseCost'))
        p_web.SSV('tmp:OutWarrantyCost',p_web.GSV('par:RRCSaleCost'))
        p_web.SSV('tmp:InWarrantyMarkup',p_web.GSV('par:RRCInWarrantyMarkup'))
        p_web.SSV('tmp:OutWarrantyMarkup',p_web.GSV('par:RRCOutWarrantyMarkup'))
    end !if (tmp:ARCPart)
UpdateComments    Routine
    loop x# = 1 to 12
        if (p_web.GSV('Hide:PartFaultCode' & x#) = 1)
            cycle
        end ! if (p_web.GSV('Hide:PartFaultCode' & x#) = 1)
        Access:MANFAUPA.Clearkey(map:ScreenOrderKey)
        map:Manufacturer    = p_web.GSV('job:Manufacturer')
        map:ScreenOrder    = x#
        if (Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign)
            ! Found
            if (map:MainFault)
                Access:MANFAULT.Clearkey(maf:MainFaultKey)
                maf:Manufacturer    = p_web.GSV('job:Manufacturer')
                maf:MainFault    = 1
                if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)
                    ! Found
                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                    mfo:Manufacturer    = p_web.GSV('job:Manufacturer')
                    mfo:Field_Number    = maf:Field_Number
                    mfo:Field    = p_web.GSV('tmp:FaultCodes' & x#)
                    if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                        ! Found
                        p_web.SSV('Comment:PartFaultCode' & x#,mfo:Description)
                    else ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                        ! Error
                        p_web.SSV('Comment:PartFaultCode' & x#,'')
                    end ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                else ! if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)
                    ! Error
                end ! if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)
            else ! if (map:MainFault)
                Access:MANFPALO.Clearkey(mfp:Field_Key)
                mfp:Manufacturer    = p_web.GSV('job:Manufacturer')
                mfp:Field_Number    = map:Field_Number
                mfp:Field    = p_web.GSV('tmp:FaultCodes' & x#)
                if (Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign)
                    ! Found
                    p_web.SSV('Comment:PartFaultCode' & x#,mfp:Description)
                else ! if (Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign)
                    ! Error
                    p_web.SSV('Comment:PartFaultCode' & x#,'')
                end ! if (Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign)
            end ! if (map:MainFault)
        else ! if (Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign)
            ! Error
        end ! if (Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign)
    end ! loop x# = 1 to 12


updatePartDetails      routine
! Update Part Details
    Access:STOCK.Clearkey(sto:Ref_Number_Key)
    sto:Ref_Number    = stm:Ref_Number
    if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
        ! Found
    else ! if (Access:STOCK.TryFetch(sto:Ref_Number) = Level:Benign)
        ! Error
    end ! if (Access:STOCK.TryFetch(sto:Ref_Number) = Level:Benign)

    Access:LOCATION.Clearkey(loc:Location_Key)
    loc:Location    = sto:Location
    if (Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign)
        ! Found
    else ! if (Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign)
        ! Error
    end ! if (Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign)

    p_web.SSV('par:Description',stm:Description)
    p_web.SSV('par:Part_Ref_Number',stm:Ref_Number)
    p_web.SSV('par:Supplier',sto:Supplier)
    p_web.SSV('par:Purchase_Cost',sto:Purchase_Cost)
    p_web.SSV('par:Sale_Cost',sto:Sale_Cost)
    p_web.SSV('par:Retail_Cost',sto:Retail_Cost)
    p_web.SSV('par:InWarrantyMarkup',sto:PurchaseMarkup)
    p_web.SSV('par:OutWarrantyMarkup',sto:Percentage_Mark_Up)

    if (p_web.GSV('BookingSite') = 'RRC')
        p_web.SSV('par:RRCAveragePurchaseCost',sto:AveragePurchaseCost)
        p_web.SSV('par:PurchaseCost',sto:Purchase_Cost)
        p_web.SSV('par:RRCSaleCost',sto:Sale_Cost)

        Access:CHARTYPE.Clearkey(cha:Charge_Type_Key)
        cha:Charge_Type    = p_web.GSV('job:Charge_Type')
        if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)
            ! Found
            if (cha:Zero_Parts = 'YES')
                p_web.SSV('par:RRCSaleCost',0)
            end ! if (cha:Zero_Parts = 'YES')
        else ! if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)
            ! Error
        end ! if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)

        p_web.SSV('par:RRCInWarrantyMarkup',sto:PurchaseMarkUp)
        p_web.SSV('par:RRCOutWarrantyMarkup',sto:Percentage_Mark_Up)
        p_web.SSV('par:Purchase_Cost',par:RRCPurchaseCost)
        p_web.SSV('par:Sale_Cost',par:RRCSaleCost)
        p_web.SSV('par:AveragePurchaseCost',par:RRCAveragePurchaseCost)
    end ! if (p_web.GSV('BookingSite') = 'RRC')

    if (p_web.GSV('BookingSite') = 'ARC')
        p_web.SSV('par:AveragePurchaseCost',sto:AveragePurchaseCost)
        p_web.SSV('par:Purchase_Cost',sto:Purchase_Cost)
        p_web.SSV('par:Sale_Code',sto:Sale_Cost)

        Access:CHARTYPE.Clearkey(cha:Charge_Type_Key)
        cha:Charge_Type    = p_web.GSV('job:Charge_Type')
        if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)
            ! Found
            if (cha:Zero_Parts_ARC)
                p_web.SSV('par:Sale_Cost',0)
            end ! if (cha:Zero_Parts = 'YES')
        else ! if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)
            ! Error
        end ! if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)

        p_web.SSV('par:RRCPurchaseCost',0)
        p_web.SSV('par:RRCSaleCost',0)

        if (p_web.GSV('jobe:WebJob') = 1)
            p_web.SSV('par:RRCAveragePurchaseCost',par:Sale_Cost)
            p_web.SSV('par:RRCPurchaseCost',VodacomClass.Markup(p_web.GSV('par:RRCPurchaseCost'),|
                                                p_web.GSV('par:RRCAveragePurchaseCost'),|
                                                InWarrantyMarkup(p_web.GSV('job:Manufacturer'),|
                                                sto:Location)))
            p_web.SSV('par:RRCSaleCost',VodacomClass.Markup(p_web.GSV('par:RRCSaleCost'),|
                                            p_web.GSV('par:RRCAveragePurchaseCost'),|
                                            loc:OutWarrantyMarkup))
            p_web.SSV('par:RRCInWarrantyMarkup',p_web.GSV('par:InWarrantyMarkup'))
            p_web.SSV('par:RRCOutWarrantyMarkup',p_web.GSV('par:OutWarrantyMarkup'))
        end ! if (p_web.GSV('jobe:WebJob') = 1)
    end ! if (p_web.GSV('BookingSite') = 'ARC')

    if (sto:Assign_Fault_Codes = 'YES')
        p_web.SSV('tmp:FaultCode1',stm:FaultCode1)
        p_web.SSV('tmp:FaultCode2',stm:FaultCode2)
        p_web.SSV('tmp:FaultCode3',stm:FaultCode3)
        p_web.SSV('tmp:FaultCode4',stm:FaultCode4)
        p_web.SSV('tmp:FaultCode5',stm:FaultCode5)
        p_web.SSV('tmp:FaultCode6',stm:FaultCode6)
        p_web.SSV('tmp:FaultCode7',stm:FaultCode7)
        p_web.SSV('tmp:FaultCode8',stm:FaultCode8)
        p_web.SSV('tmp:FaultCode9',stm:FaultCode9)
        p_web.SSV('tmp:FaultCode10',stm:FaultCode10)
        p_web.SSV('tmp:FaultCode11',stm:FaultCode11)
        p_web.SSV('tmp:FaultCode12',stm:FaultCode12)
    end ! if (sto:Assign_Fault_Codes = 'YES')

    p_web.SSV('par:Part_Ref_Number',sto:Ref_Number)
    p_web.SSV('locOrderRequired',0)

    do ShowCosts
    do lookupLocation
    do enableDisableCosts
OpenFiles  ROUTINE
  p_web._OpenFile(PARTS)
  p_web._OpenFile(STOMODEL)
  p_web._OpenFile(STOCK)
  p_web._OpenFile(SUPPLIER)
  p_web._OpenFile(LOCATION)
  p_web._OpenFile(CHARTYPE)
  p_web._OpenFile(MANFAUPA)
  p_web._OpenFile(MANFAULT)
  p_web._OpenFile(MANFPALO)
  p_web._OpenFile(DEFAULTS)
  p_web._OpenFile(PARTS_ALIAS)
  p_web._OpenFile(STOHIST)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(PARTS)
  p_Web._CloseFile(STOMODEL)
  p_Web._CloseFile(STOCK)
  p_Web._CloseFile(SUPPLIER)
  p_Web._CloseFile(LOCATION)
  p_Web._CloseFile(CHARTYPE)
  p_Web._CloseFile(MANFAUPA)
  p_Web._CloseFile(MANFAULT)
  p_Web._CloseFile(MANFPALO)
  p_Web._CloseFile(DEFAULTS)
  p_Web._CloseFile(PARTS_ALIAS)
  p_Web._CloseFile(STOHIST)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
      !Initialize
      do buildFaultCodes
  
      p_web.SSV('Comment:OutWarrantyMarkup','')
      p_web.SSV('Comment:PartNumber','Required')
  p_web.SetValue('FormChargeableParts_form:inited_',1)
  p_web.formsettings.file = 'PARTS'
  p_web.formsettings.key = 'par:recordnumberkey'
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = 'PARTS'
    p_web.formsettings.key = 'par:recordnumberkey'
      clear(p_web.formsettings.FieldName)
    p_web.formsettings.recordid[1] = par:Record_Number
    p_web.formsettings.FieldName[1] = 'par:Record_Number'
    do SetAction
    if p_web.GetSessionValue('FormChargeableParts:Primed') = 1
      p_web.formsettings.action = Net:ChangeRecord
    Else
      p_web.formsettings.action = Loc:Act
    End
    p_web.formsettings.OriginalAction = Loc:Act
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'FormChargeableParts'
    end
    p_web.formsettings.proc = 'FormChargeableParts'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine
  IF p_web.GetSessionValue('FormChargeableParts:Primed') = 1
    p_web._deleteFile(PARTS)
    p_web.SetSessionValue('FormChargeableParts:Primed',0)
  End
      do deleteSessionValues
  

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine
  p_web.SetValue('UpdateFile','PARTS')
  p_web.SetValue('UpdateKey','par:recordnumberkey')
  If p_web.IfExistsValue('par:Order_Number')
    p_web.SetPicture('par:Order_Number','@n08b')
  End
  p_web.SetSessionPicture('par:Order_Number','@n08b')
  If p_web.IfExistsValue('par:Date_Ordered')
    p_web.SetPicture('par:Date_Ordered','@d6b')
  End
  p_web.SetSessionPicture('par:Date_Ordered','@d6b')
  If p_web.IfExistsValue('par:Date_Received')
    p_web.SetPicture('par:Date_Received','@d6b')
  End
  p_web.SetSessionPicture('par:Date_Received','@d6b')
  If p_web.IfExistsValue('par:Part_Number')
    p_web.SetPicture('par:Part_Number','@s30')
  End
  p_web.SetSessionPicture('par:Part_Number','@s30')
  If p_web.IfExistsValue('par:Description')
    p_web.SetPicture('par:Description','@s30')
  End
  p_web.SetSessionPicture('par:Description','@s30')
  If p_web.IfExistsValue('par:Despatch_Note_Number')
    p_web.SetPicture('par:Despatch_Note_Number','@s30')
  End
  p_web.SetSessionPicture('par:Despatch_Note_Number','@s30')
  If p_web.IfExistsValue('par:Quantity')
    p_web.SetPicture('par:Quantity','@n4')
  End
  p_web.SetSessionPicture('par:Quantity','@n4')
  If p_web.IfExistsValue('tmp:PurchaseCost')
    p_web.SetPicture('tmp:PurchaseCost','@n_14.2')
  End
  p_web.SetSessionPicture('tmp:PurchaseCost','@n_14.2')
  If p_web.IfExistsValue('tmp:OutWarrantyCost')
    p_web.SetPicture('tmp:OutWarrantyCost','@n_14.2')
  End
  p_web.SetSessionPicture('tmp:OutWarrantyCost','@n_14.2')
  If p_web.IfExistsValue('tmp:OutWarrantyMarkup')
    p_web.SetPicture('tmp:OutWarrantyMarkup','@n3')
  End
  p_web.SetSessionPicture('tmp:OutWarrantyMarkup','@n3')
  If p_web.IfExistsValue('par:Supplier')
    p_web.SetPicture('par:Supplier','@s30')
  End
  p_web.SetSessionPicture('par:Supplier','@s30')
  If p_web.IfExistsValue('tmp:FaultCodes1')
    p_web.SetPicture('tmp:FaultCodes1',p_web.GSV('Picture:PartFaultCode1'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes1',p_web.GSV('Picture:PartFaultCode1'))
  If p_web.IfExistsValue('tmp:FaultCodes2')
    p_web.SetPicture('tmp:FaultCodes2',p_web.GSV('Picture:PartFaultCode2'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes2',p_web.GSV('Picture:PartFaultCode2'))
  If p_web.IfExistsValue('tmp:FaultCodes3')
    p_web.SetPicture('tmp:FaultCodes3',p_web.GSV('Picture:PartFaultCode3'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes3',p_web.GSV('Picture:PartFaultCode3'))
  If p_web.IfExistsValue('tmp:FaultCodes4')
    p_web.SetPicture('tmp:FaultCodes4',p_web.GSV('Picture:PartFaultCode4'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes4',p_web.GSV('Picture:PartFaultCode4'))
  If p_web.IfExistsValue('tmp:FaultCodes5')
    p_web.SetPicture('tmp:FaultCodes5',p_web.GSV('Picture:PartFaultCode5'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes5',p_web.GSV('Picture:PartFaultCode5'))
  If p_web.IfExistsValue('tmp:FaultCodes6')
    p_web.SetPicture('tmp:FaultCodes6',p_web.GSV('Picture:PartFaultCode6'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes6',p_web.GSV('Picture:PartFaultCode6'))
  If p_web.IfExistsValue('tmp:FaultCodes7')
    p_web.SetPicture('tmp:FaultCodes7',p_web.GSV('Picture:PartFaultCode7'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes7',p_web.GSV('Picture:PartFaultCode7'))
  If p_web.IfExistsValue('tmp:FaultCodes8')
    p_web.SetPicture('tmp:FaultCodes8',p_web.GSV('Picture:PartFaultCode8'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes8',p_web.GSV('Picture:PartFaultCode8'))
  If p_web.IfExistsValue('tmp:FaultCodes9')
    p_web.SetPicture('tmp:FaultCodes9',p_web.GSV('Picture:PartFaultCode9'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes9',p_web.GSV('Picture:PartFaultCode9'))
  If p_web.IfExistsValue('tmp:FaultCodes10')
    p_web.SetPicture('tmp:FaultCodes10',p_web.GSV('Picture:PartFaultCode10'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes10',p_web.GSV('Picture:PartFaultCode10'))
  If p_web.IfExistsValue('tmp:FaultCodes11')
    p_web.SetPicture('tmp:FaultCodes11',p_web.GSV('Picture:PartFaultCode11'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes11',p_web.GSV('Picture:PartFaultCode11'))
  If p_web.IfExistsValue('tmp:FaultCodes12')
    p_web.SetPicture('tmp:FaultCodes12',p_web.GSV('Picture:PartFaultCode12'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes12',p_web.GSV('Picture:PartFaultCode12'))

AfterLookup Routine
  loc:TabNumber = -1
  If loc:act = ChangeRecord
    loc:TabNumber += 1
  End
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'par:Part_Number'
    p_web.setsessionvalue('showtab_FormChargeableParts',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(STOMODEL)
      ! After Lookup
      ! After Lookup Assignments
      do updatePartDetails
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.par:Description')
  of 'tmp:FaultCodes1'
      local.afterFaultCodeLookup(1)
  of 'tmp:FaultCodes2'
      local.afterFaultCodeLookup(2)
  of 'tmp:FaultCodes3'
      local.afterFaultCodeLookup(3)
  of 'tmp:FaultCodes4'
      local.afterFaultCodeLookup(4)
  of 'tmp:FaultCodes5'
      local.afterFaultCodeLookup(5)
  of 'tmp:FaultCodes6'
      local.afterFaultCodeLookup(6)
  of 'tmp:FaultCodes7'
      local.afterFaultCodeLookup(7)
  of 'tmp:FaultCodes8'
      local.afterFaultCodeLookup(8)
  of 'tmp:FaultCodes9'
      local.afterFaultCodeLookup(9)
  of 'tmp:FaultCodes10'
      local.afterFaultCodeLookup(10)
  of 'tmp:FaultCodes11'
      local.afterFaultCodeLookup(11)
  of 'tmp:FaultCodes12'
      local.afterFaultCodeLookup(12)
  
  
  Of 'par:Supplier'
    p_web.setsessionvalue('showtab_FormChargeableParts',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(SUPPLIER)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.par:Exclude_From_Order')
  of 'tmp:FaultCodes1'
      local.afterFaultCodeLookup(1)
  of 'tmp:FaultCodes2'
      local.afterFaultCodeLookup(2)
  of 'tmp:FaultCodes3'
      local.afterFaultCodeLookup(3)
  of 'tmp:FaultCodes4'
      local.afterFaultCodeLookup(4)
  of 'tmp:FaultCodes5'
      local.afterFaultCodeLookup(5)
  of 'tmp:FaultCodes6'
      local.afterFaultCodeLookup(6)
  of 'tmp:FaultCodes7'
      local.afterFaultCodeLookup(7)
  of 'tmp:FaultCodes8'
      local.afterFaultCodeLookup(8)
  of 'tmp:FaultCodes9'
      local.afterFaultCodeLookup(9)
  of 'tmp:FaultCodes10'
      local.afterFaultCodeLookup(10)
  of 'tmp:FaultCodes11'
      local.afterFaultCodeLookup(11)
  of 'tmp:FaultCodes12'
      local.afterFaultCodeLookup(12)
  
  
  End
  If p_web.GSV('locOrderRequired') = 1
    loc:TabNumber += 1
  End
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  if p_web.IfExistsValue('tmp:Location') = 0
    p_web.SetSessionValue('tmp:Location',tmp:Location)
  Else
    tmp:Location = p_web.GetSessionValue('tmp:Location')
  End
  if p_web.IfExistsValue('tmp:SecondLocation') = 0
    p_web.SetSessionValue('tmp:SecondLocation',tmp:SecondLocation)
  Else
    tmp:SecondLocation = p_web.GetSessionValue('tmp:SecondLocation')
  End
  if p_web.IfExistsValue('tmp:ShelfLocation') = 0
    p_web.SetSessionValue('tmp:ShelfLocation',tmp:ShelfLocation)
  Else
    tmp:ShelfLocation = p_web.GetSessionValue('tmp:ShelfLocation')
  End
  if p_web.IfExistsValue('tmp:PurchaseCost') = 0
    p_web.SetSessionValue('tmp:PurchaseCost',tmp:PurchaseCost)
  Else
    tmp:PurchaseCost = p_web.GetSessionValue('tmp:PurchaseCost')
  End
  if p_web.IfExistsValue('tmp:OutWarrantyCost') = 0
    p_web.SetSessionValue('tmp:OutWarrantyCost',tmp:OutWarrantyCost)
  Else
    tmp:OutWarrantyCost = p_web.GetSessionValue('tmp:OutWarrantyCost')
  End
  if p_web.IfExistsValue('tmp:OutWarrantyMarkup') = 0
    p_web.SetSessionValue('tmp:OutWarrantyMarkup',tmp:OutWarrantyMarkup)
  Else
    tmp:OutWarrantyMarkup = p_web.GetSessionValue('tmp:OutWarrantyMarkup')
  End
  if p_web.IfExistsValue('tmp:UnallocatePart') = 0
    p_web.SetSessionValue('tmp:UnallocatePart',tmp:UnallocatePart)
  Else
    tmp:UnallocatePart = p_web.GetSessionValue('tmp:UnallocatePart')
  End
  if p_web.IfExistsValue('tmp:CreateOrder') = 0
    p_web.SetSessionValue('tmp:CreateOrder',tmp:CreateOrder)
  Else
    tmp:CreateOrder = p_web.GetSessionValue('tmp:CreateOrder')
  End
  if p_web.IfExistsValue('tmp:FaultCodesChecked') = 0
    p_web.SetSessionValue('tmp:FaultCodesChecked',tmp:FaultCodesChecked)
  Else
    tmp:FaultCodesChecked = p_web.GetSessionValue('tmp:FaultCodesChecked')
  End
  if p_web.IfExistsValue('tmp:FaultCodes1') = 0
    p_web.SetSessionValue('tmp:FaultCodes1',tmp:FaultCodes1)
  Else
    tmp:FaultCodes1 = p_web.GetSessionValue('tmp:FaultCodes1')
  End
  if p_web.IfExistsValue('tmp:FaultCodes2') = 0
    p_web.SetSessionValue('tmp:FaultCodes2',tmp:FaultCodes2)
  Else
    tmp:FaultCodes2 = p_web.GetSessionValue('tmp:FaultCodes2')
  End
  if p_web.IfExistsValue('tmp:FaultCodes3') = 0
    p_web.SetSessionValue('tmp:FaultCodes3',tmp:FaultCodes3)
  Else
    tmp:FaultCodes3 = p_web.GetSessionValue('tmp:FaultCodes3')
  End
  if p_web.IfExistsValue('tmp:FaultCodes4') = 0
    p_web.SetSessionValue('tmp:FaultCodes4',tmp:FaultCodes4)
  Else
    tmp:FaultCodes4 = p_web.GetSessionValue('tmp:FaultCodes4')
  End
  if p_web.IfExistsValue('tmp:FaultCodes5') = 0
    p_web.SetSessionValue('tmp:FaultCodes5',tmp:FaultCodes5)
  Else
    tmp:FaultCodes5 = p_web.GetSessionValue('tmp:FaultCodes5')
  End
  if p_web.IfExistsValue('tmp:FaultCodes6') = 0
    p_web.SetSessionValue('tmp:FaultCodes6',tmp:FaultCodes6)
  Else
    tmp:FaultCodes6 = p_web.GetSessionValue('tmp:FaultCodes6')
  End
  if p_web.IfExistsValue('tmp:FaultCodes7') = 0
    p_web.SetSessionValue('tmp:FaultCodes7',tmp:FaultCodes7)
  Else
    tmp:FaultCodes7 = p_web.GetSessionValue('tmp:FaultCodes7')
  End
  if p_web.IfExistsValue('tmp:FaultCodes8') = 0
    p_web.SetSessionValue('tmp:FaultCodes8',tmp:FaultCodes8)
  Else
    tmp:FaultCodes8 = p_web.GetSessionValue('tmp:FaultCodes8')
  End
  if p_web.IfExistsValue('tmp:FaultCodes9') = 0
    p_web.SetSessionValue('tmp:FaultCodes9',tmp:FaultCodes9)
  Else
    tmp:FaultCodes9 = p_web.GetSessionValue('tmp:FaultCodes9')
  End
  if p_web.IfExistsValue('tmp:FaultCodes10') = 0
    p_web.SetSessionValue('tmp:FaultCodes10',tmp:FaultCodes10)
  Else
    tmp:FaultCodes10 = p_web.GetSessionValue('tmp:FaultCodes10')
  End
  if p_web.IfExistsValue('tmp:FaultCodes11') = 0
    p_web.SetSessionValue('tmp:FaultCodes11',tmp:FaultCodes11)
  Else
    tmp:FaultCodes11 = p_web.GetSessionValue('tmp:FaultCodes11')
  End
  if p_web.IfExistsValue('tmp:FaultCodes12') = 0
    p_web.SetSessionValue('tmp:FaultCodes12',tmp:FaultCodes12)
  Else
    tmp:FaultCodes12 = p_web.GetSessionValue('tmp:FaultCodes12')
  End

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=File
  if p_web.IfExistsValue('tmp:Location')
    tmp:Location = p_web.GetValue('tmp:Location')
    p_web.SetSessionValue('tmp:Location',tmp:Location)
  Else
    tmp:Location = p_web.GetSessionValue('tmp:Location')
  End
  if p_web.IfExistsValue('tmp:SecondLocation')
    tmp:SecondLocation = p_web.GetValue('tmp:SecondLocation')
    p_web.SetSessionValue('tmp:SecondLocation',tmp:SecondLocation)
  Else
    tmp:SecondLocation = p_web.GetSessionValue('tmp:SecondLocation')
  End
  if p_web.IfExistsValue('tmp:ShelfLocation')
    tmp:ShelfLocation = p_web.GetValue('tmp:ShelfLocation')
    p_web.SetSessionValue('tmp:ShelfLocation',tmp:ShelfLocation)
  Else
    tmp:ShelfLocation = p_web.GetSessionValue('tmp:ShelfLocation')
  End
  if p_web.IfExistsValue('tmp:PurchaseCost')
    tmp:PurchaseCost = p_web.dformat(clip(p_web.GetValue('tmp:PurchaseCost')),'@n_14.2')
    p_web.SetSessionValue('tmp:PurchaseCost',tmp:PurchaseCost)
  Else
    tmp:PurchaseCost = p_web.GetSessionValue('tmp:PurchaseCost')
  End
  if p_web.IfExistsValue('tmp:OutWarrantyCost')
    tmp:OutWarrantyCost = p_web.dformat(clip(p_web.GetValue('tmp:OutWarrantyCost')),'@n_14.2')
    p_web.SetSessionValue('tmp:OutWarrantyCost',tmp:OutWarrantyCost)
  Else
    tmp:OutWarrantyCost = p_web.GetSessionValue('tmp:OutWarrantyCost')
  End
  if p_web.IfExistsValue('tmp:OutWarrantyMarkup')
    tmp:OutWarrantyMarkup = p_web.dformat(clip(p_web.GetValue('tmp:OutWarrantyMarkup')),'@n3')
    p_web.SetSessionValue('tmp:OutWarrantyMarkup',tmp:OutWarrantyMarkup)
  Else
    tmp:OutWarrantyMarkup = p_web.GetSessionValue('tmp:OutWarrantyMarkup')
  End
  if p_web.IfExistsValue('tmp:UnallocatePart')
    tmp:UnallocatePart = p_web.GetValue('tmp:UnallocatePart')
    p_web.SetSessionValue('tmp:UnallocatePart',tmp:UnallocatePart)
  Else
    tmp:UnallocatePart = p_web.GetSessionValue('tmp:UnallocatePart')
  End
  if p_web.IfExistsValue('tmp:CreateOrder')
    tmp:CreateOrder = p_web.GetValue('tmp:CreateOrder')
    p_web.SetSessionValue('tmp:CreateOrder',tmp:CreateOrder)
  Else
    tmp:CreateOrder = p_web.GetSessionValue('tmp:CreateOrder')
  End
  if p_web.IfExistsValue('tmp:FaultCodesChecked')
    tmp:FaultCodesChecked = p_web.GetValue('tmp:FaultCodesChecked')
    p_web.SetSessionValue('tmp:FaultCodesChecked',tmp:FaultCodesChecked)
  Else
    tmp:FaultCodesChecked = p_web.GetSessionValue('tmp:FaultCodesChecked')
  End
  if p_web.IfExistsValue('tmp:FaultCodes1')
    tmp:FaultCodes1 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCodes1')),p_web.GSV('Picture:PartFaultCode1'))
    p_web.SetSessionValue('tmp:FaultCodes1',tmp:FaultCodes1)
  Else
    tmp:FaultCodes1 = p_web.GetSessionValue('tmp:FaultCodes1')
  End
  if p_web.IfExistsValue('tmp:FaultCodes2')
    tmp:FaultCodes2 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCodes2')),p_web.GSV('Picture:PartFaultCode2'))
    p_web.SetSessionValue('tmp:FaultCodes2',tmp:FaultCodes2)
  Else
    tmp:FaultCodes2 = p_web.GetSessionValue('tmp:FaultCodes2')
  End
  if p_web.IfExistsValue('tmp:FaultCodes3')
    tmp:FaultCodes3 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCodes3')),p_web.GSV('Picture:PartFaultCode3'))
    p_web.SetSessionValue('tmp:FaultCodes3',tmp:FaultCodes3)
  Else
    tmp:FaultCodes3 = p_web.GetSessionValue('tmp:FaultCodes3')
  End
  if p_web.IfExistsValue('tmp:FaultCodes4')
    tmp:FaultCodes4 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCodes4')),p_web.GSV('Picture:PartFaultCode4'))
    p_web.SetSessionValue('tmp:FaultCodes4',tmp:FaultCodes4)
  Else
    tmp:FaultCodes4 = p_web.GetSessionValue('tmp:FaultCodes4')
  End
  if p_web.IfExistsValue('tmp:FaultCodes5')
    tmp:FaultCodes5 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCodes5')),p_web.GSV('Picture:PartFaultCode5'))
    p_web.SetSessionValue('tmp:FaultCodes5',tmp:FaultCodes5)
  Else
    tmp:FaultCodes5 = p_web.GetSessionValue('tmp:FaultCodes5')
  End
  if p_web.IfExistsValue('tmp:FaultCodes6')
    tmp:FaultCodes6 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCodes6')),p_web.GSV('Picture:PartFaultCode6'))
    p_web.SetSessionValue('tmp:FaultCodes6',tmp:FaultCodes6)
  Else
    tmp:FaultCodes6 = p_web.GetSessionValue('tmp:FaultCodes6')
  End
  if p_web.IfExistsValue('tmp:FaultCodes7')
    tmp:FaultCodes7 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCodes7')),p_web.GSV('Picture:PartFaultCode7'))
    p_web.SetSessionValue('tmp:FaultCodes7',tmp:FaultCodes7)
  Else
    tmp:FaultCodes7 = p_web.GetSessionValue('tmp:FaultCodes7')
  End
  if p_web.IfExistsValue('tmp:FaultCodes8')
    tmp:FaultCodes8 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCodes8')),p_web.GSV('Picture:PartFaultCode8'))
    p_web.SetSessionValue('tmp:FaultCodes8',tmp:FaultCodes8)
  Else
    tmp:FaultCodes8 = p_web.GetSessionValue('tmp:FaultCodes8')
  End
  if p_web.IfExistsValue('tmp:FaultCodes9')
    tmp:FaultCodes9 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCodes9')),p_web.GSV('Picture:PartFaultCode9'))
    p_web.SetSessionValue('tmp:FaultCodes9',tmp:FaultCodes9)
  Else
    tmp:FaultCodes9 = p_web.GetSessionValue('tmp:FaultCodes9')
  End
  if p_web.IfExistsValue('tmp:FaultCodes10')
    tmp:FaultCodes10 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCodes10')),p_web.GSV('Picture:PartFaultCode10'))
    p_web.SetSessionValue('tmp:FaultCodes10',tmp:FaultCodes10)
  Else
    tmp:FaultCodes10 = p_web.GetSessionValue('tmp:FaultCodes10')
  End
  if p_web.IfExistsValue('tmp:FaultCodes11')
    tmp:FaultCodes11 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCodes11')),p_web.GSV('Picture:PartFaultCode11'))
    p_web.SetSessionValue('tmp:FaultCodes11',tmp:FaultCodes11)
  Else
    tmp:FaultCodes11 = p_web.GetSessionValue('tmp:FaultCodes11')
  End
  if p_web.IfExistsValue('tmp:FaultCodes12')
    tmp:FaultCodes12 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCodes12')),p_web.GSV('Picture:PartFaultCode12'))
    p_web.SetSessionValue('tmp:FaultCodes12',tmp:FaultCodes12)
  Else
    tmp:FaultCodes12 = p_web.GetSessionValue('tmp:FaultCodes12')
  End

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('FormChargeableParts_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'ViewJob'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormChargeableParts_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormChargeableParts_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormChargeableParts_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'ViewJob'

GenerateForm   Routine
  do LoadRelatedRecords
      if (p_web.GSV('FormChargeableParts:FirstTime') = 0)
          !Write Fault Codes
          p_web.SSV('par:Fault_Code1',par:Fault_Code1)
          p_web.SSV('par:Fault_Code2',par:Fault_Code2)
          p_web.SSV('par:Fault_Code3',par:Fault_Code3)
          p_web.SSV('par:Fault_Code4',par:Fault_Code4)
          p_web.SSV('par:Fault_Code5',par:Fault_Code5)
          p_web.SSV('par:Fault_Code6',par:Fault_Code6)
          p_web.SSV('par:Fault_Code7',par:Fault_Code7)
          p_web.SSV('par:Fault_Code8',par:Fault_Code8)
          p_web.SSV('par:Fault_Code9',par:Fault_Code9)
          p_web.SSV('par:Fault_Code10',par:Fault_Code10)
          p_web.SSV('par:Fault_Code11',par:Fault_Code11)
          p_web.SSV('par:Fault_Code12',par:Fault_Code12)
  
  
          Access:MANFAUPA.Clearkey(map:ScreenOrderKey)
          map:Manufacturer    = p_web.GSV('job:Manufacturer')
          map:ScreenOrder    = 0
          set(map:ScreenOrderKey,map:ScreenOrderKey)
          loop
              if (Access:MANFAUPA.Next())
                  Break
              end ! if (Access:MANFAUPA.Next())
              if (map:Manufacturer    <> p_web.GSV('job:Manufacturer'))
                  Break
              end ! if (map:Manufacturer    <> p_web.GSV('job:Manufacturer'))
              if (map:ScreenOrder    = 0)
                  cycle
              end ! if (map:ScreenOrder    <> 0)
  
              p_web.SSV('tmp:FaultCodes' & map:ScreenOrder,p_web.GSV('par:Fault_Code' & map:Field_Number))
          end ! loop
  
  !        loop x# = 1 To 12
  !            p_web.SSV('tmp:FaultCodes' & x#,p_web.GSV('par:Fault_Code' & x#))
  !            linePrint('tmp:FaultCode' & x# & ' - ' & p_web.GSV('tmp:FaultCode' & x#),'c:\log.log')
  !        end ! loop x# = 1 To 12
          p_web.SSV('FormChargeableParts:FirstTime',1)
          !p_web.SSV('locOrderRequired',0)
  
          ! Save For Later
          p_web.SSV('Save:Quantity',par:Quantity)
      end ! if (p_web.GSV('FormChargeableParts:FirstTime',0))
      do updateComments
  
 tmp:Location = p_web.RestoreValue('tmp:Location')
 tmp:SecondLocation = p_web.RestoreValue('tmp:SecondLocation')
 tmp:ShelfLocation = p_web.RestoreValue('tmp:ShelfLocation')
 tmp:PurchaseCost = p_web.RestoreValue('tmp:PurchaseCost')
 tmp:OutWarrantyCost = p_web.RestoreValue('tmp:OutWarrantyCost')
 tmp:OutWarrantyMarkup = p_web.RestoreValue('tmp:OutWarrantyMarkup')
 tmp:UnallocatePart = p_web.RestoreValue('tmp:UnallocatePart')
 tmp:CreateOrder = p_web.RestoreValue('tmp:CreateOrder')
 tmp:FaultCodesChecked = p_web.RestoreValue('tmp:FaultCodesChecked')
 tmp:FaultCodes1 = p_web.RestoreValue('tmp:FaultCodes1')
 tmp:FaultCodes2 = p_web.RestoreValue('tmp:FaultCodes2')
 tmp:FaultCodes3 = p_web.RestoreValue('tmp:FaultCodes3')
 tmp:FaultCodes4 = p_web.RestoreValue('tmp:FaultCodes4')
 tmp:FaultCodes5 = p_web.RestoreValue('tmp:FaultCodes5')
 tmp:FaultCodes6 = p_web.RestoreValue('tmp:FaultCodes6')
 tmp:FaultCodes7 = p_web.RestoreValue('tmp:FaultCodes7')
 tmp:FaultCodes8 = p_web.RestoreValue('tmp:FaultCodes8')
 tmp:FaultCodes9 = p_web.RestoreValue('tmp:FaultCodes9')
 tmp:FaultCodes10 = p_web.RestoreValue('tmp:FaultCodes10')
 tmp:FaultCodes11 = p_web.RestoreValue('tmp:FaultCodes11')
 tmp:FaultCodes12 = p_web.RestoreValue('tmp:FaultCodes12')
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
    loc:viewonly = Choose(p_web.GSV('Parts:ViewOnly') = 1,1,0)
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Insert / Amend Chargeable Parts') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Insert / Amend Chargeable Parts',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_FormChargeableParts',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      If loc:act = ChangeRecord
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_FormChargeableParts0_div')&'">'&p_web.Translate('Part Status')&'</a></li>'& CRLF
      End
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_FormChargeableParts1_div')&'">'&p_web.Translate('Part Details')&'</a></li>'& CRLF
      If p_web.GSV('locOrderRequired') = 1
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_FormChargeableParts2_div')&'">'&p_web.Translate('Order Required')&'</a></li>'& CRLF
      End
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_FormChargeableParts3_div')&'">'&p_web.Translate('Fault Codes')&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  do GenerateTab3
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="FormChargeableParts_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.cancel();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CancelButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="FormChargeableParts_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'FormChargeableParts_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="FormChargeableParts_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'FormChargeableParts_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
    If upper(p_web.getvalue('LookupFile'))='STOMODEL'
          If Not (1=0)
            p_web.SetValue('SelectField',clip(loc:formname) & '.par:Description')
          End
    End
    If upper(p_web.getvalue('LookupFile'))='SUPPLIER'
        If p_web.GSV('Hide:PartFaultCode1') <> 1
          If Not (1=0)
            p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:FaultCodes1')
          End
        End
    End
  Else
    If False
    ElsIf loc:act = ChangeRecord
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.par:Part_Number')
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_FormChargeableParts')>0,p_web.GSV('showtab_FormChargeableParts'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_FormChargeableParts'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_FormChargeableParts') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_FormChargeableParts'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_FormChargeableParts')>0,p_web.GSV('showtab_FormChargeableParts'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_FormChargeableParts') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
      If loc:act = ChangeRecord
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Part Status') & ''''
      End
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Part Details') & ''''
      If p_web.GSV('locOrderRequired') = 1
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Order Required') & ''''
      End
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Fault Codes') & ''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_FormChargeableParts_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_FormChargeableParts')>0,p_web.GSV('showtab_FormChargeableParts'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"FormChargeableParts",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_FormChargeableParts')>0,p_web.GSV('showtab_FormChargeableParts'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_FormChargeableParts_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('FormChargeableParts') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('FormChargeableParts')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
  If loc:act = ChangeRecord
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Part Status')&'</a></h3>' & CRLF & p_web.DivHeader('tab_FormChargeableParts0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_FormChargeableParts0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_FormChargeableParts0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_FormChargeableParts0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Part Status')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_FormChargeableParts0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Part Status')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_FormChargeableParts0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Part Status')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_FormChargeableParts0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::par:Order_Number
        do Value::par:Order_Number
        do Comment::par:Order_Number
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::par:Date_Ordered
        do Value::par:Date_Ordered
        do Comment::par:Date_Ordered
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::par:Date_Received
        do Value::par:Date_Received
        do Comment::par:Date_Received
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::locOutFaultCode
        do Comment::locOutFaultCode
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
  end
GenerateTab1  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Part Details')&'</a></h3>' & CRLF & p_web.DivHeader('tab_FormChargeableParts1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_FormChargeableParts1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_FormChargeableParts1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_FormChargeableParts1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Part Details')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_FormChargeableParts1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Part Details')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_FormChargeableParts1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Part Details')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_FormChargeableParts1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::par:Part_Number
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::par:Part_Number
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::par:Part_Number
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::par:Description
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::par:Description
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::par:Description
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::par:Despatch_Note_Number
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::par:Despatch_Note_Number
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::par:Despatch_Note_Number
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::par:Quantity
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::par:Quantity
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::par:Quantity
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        !Set Width
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:Location
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:Location
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:Location
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:SecondLocation
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:SecondLocation
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:SecondLocation
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:ShelfLocation
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:ShelfLocation
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:ShelfLocation
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:PurchaseCost
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:PurchaseCost
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:PurchaseCost
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:OutWarrantyCost
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:OutWarrantyCost
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:OutWarrantyCost
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:OutWarrantyMarkup
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:OutWarrantyMarkup
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:OutWarrantyMarkup
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:FixedPrice
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:FixedPrice
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:FixedPrice
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::par:Supplier
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::par:Supplier
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::par:Supplier
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::par:Exclude_From_Order
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::par:Exclude_From_Order
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::par:Exclude_From_Order
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::par:PartAllocated
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::par:PartAllocated
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::par:PartAllocated
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      If p_web.GSV('Show:UnallocatePart') = 1
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:UnallocatePart
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:UnallocatePart
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:UnallocatePart
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab2  Routine
  If p_web.GSV('locOrderRequired') = 1
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Order Required')&'</a></h3>' & CRLF & p_web.DivHeader('tab_FormChargeableParts2',p_web.combine(p_web.site.style.FormTabInner,,'red bold'),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_FormChargeableParts2',p_web.combine(p_web.site.style.FormTabInner,,'red bold'),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_FormChargeableParts2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,'red bold'),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_FormChargeableParts2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,'red bold'),Net:NoSend,'Order Required')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_FormChargeableParts2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,'red bold'),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Order Required')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_FormChargeableParts2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,'red bold'),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Order Required')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_FormChargeableParts2',p_web.combine(p_web.site.style.FormTabInner,,'red bold'),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::text:OrderRequired
        do Comment::text:OrderRequired
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::text:OrderRequired2
        do Comment::text:OrderRequired2
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:CreateOrder
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:CreateOrder
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:CreateOrder
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
  end
GenerateTab3  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Fault Codes')&'</a></h3>' & CRLF & p_web.DivHeader('tab_FormChargeableParts3',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_FormChargeableParts3',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_FormChargeableParts3',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_FormChargeableParts3',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Fault Codes')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_FormChargeableParts3',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Fault Codes')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_FormChargeableParts3',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Fault Codes')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_FormChargeableParts3',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
      If 0
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:FaultCodesChecked
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:FaultCodesChecked
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:FaultCodesChecked
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
      If p_web.GSV('Hide:PartFaultCode1') <> 1
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:FaultCodes1
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:FaultCodes1
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:FaultCodes1
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
      If p_web.GSV('Hide:PartFaultCode2') <> 1
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:FaultCode2
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:FaultCode2
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:FaultCode2
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
      If p_web.GSV('Hide:PartFaultCode3') <> 1
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:FaultCode3
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:FaultCode3
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:FaultCode3
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
      If p_web.GSV('Hide:PartFaultCode4') <> 1
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:FaultCode4
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:FaultCode4
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:FaultCode4
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
      If p_web.GSV('Hide:PartFaultCode5') <> 1
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:FaultCode5
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:FaultCode5
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:FaultCode5
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
      If p_web.GSV('Hide:PartFaultCode6') <> 1
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:FaultCode6
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:FaultCode6
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:FaultCode6
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
      If p_web.GSV('Hide:PartFaultCode7') <> 1
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:FaultCode7
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:FaultCode7
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:FaultCode7
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
      If p_web.GSV('Hide:PartFaultCode8') <> 1
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:FaultCode8
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:FaultCode8
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:FaultCode8
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
      If p_web.GSV('Hide:PartFaultCode9') <> 1
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:FaultCode9
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:FaultCode9
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:FaultCode9
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
      If p_web.GSV('Hide:PartFaultCode10') <> 1
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:FaultCode10
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:FaultCode10
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:FaultCode10
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
      If p_web.GSV('Hide:PartFaultCode11') <> 1
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:FaultCode11
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:FaultCode11
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:FaultCode11
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
      If p_web.GSV('Hide:PartFaultCode12') <> 1
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:FaultCode12
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:FaultCode12
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:FaultCode12
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Prompt::par:Order_Number  Routine
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('par:Order_Number') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Order Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::par:Order_Number  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    par:Order_Number = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n08b'  !FieldPicture = @s8
    par:Order_Number = p_web.Dformat(p_web.GetValue('Value'),'@n08b')
  End
  do ValidateValue::par:Order_Number  ! copies value to session value if valid.
  do Comment::par:Order_Number ! allows comment style to be updated.

ValidateValue::par:Order_Number  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('par:Order_Number',par:Order_Number).
    End

Value::par:Order_Number  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('par:Order_Number') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- par:Order_Number
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    p_web._jsok(format(p_web.GetSessionValue('par:Order_Number'),'@n08b')) & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::par:Order_Number  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if par:Order_Number:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('par:Order_Number') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::par:Date_Ordered  Routine
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('par:Date_Ordered') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Date Ordered'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::par:Date_Ordered  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    par:Date_Ordered = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @d6b
    par:Date_Ordered = p_web.Dformat(p_web.GetValue('Value'),'@d6b')
  End
  do ValidateValue::par:Date_Ordered  ! copies value to session value if valid.
  do Comment::par:Date_Ordered ! allows comment style to be updated.

ValidateValue::par:Date_Ordered  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('par:Date_Ordered',par:Date_Ordered).
    End

Value::par:Date_Ordered  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('par:Date_Ordered') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- par:Date_Ordered
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('par:Date_Ordered'),) & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::par:Date_Ordered  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if par:Date_Ordered:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('par:Date_Ordered') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::par:Date_Received  Routine
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('par:Date_Received') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Date Received'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::par:Date_Received  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    par:Date_Received = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @d6b
    par:Date_Received = p_web.Dformat(p_web.GetValue('Value'),'@d6b')
  End
  do ValidateValue::par:Date_Received  ! copies value to session value if valid.
  do Comment::par:Date_Received ! allows comment style to be updated.

ValidateValue::par:Date_Received  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('par:Date_Received',par:Date_Received).
    End

Value::par:Date_Received  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('par:Date_Received') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- par:Date_Received
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('par:Date_Received'),) & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::par:Date_Received  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if par:Date_Received:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('par:Date_Received') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::locOutFaultCode  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::locOutFaultCode  ! copies value to session value if valid.
  do Comment::locOutFaultCode ! allows comment style to be updated.

ValidateValue::locOutFaultCode  Routine
    If not (1=0)
    End

Value::locOutFaultCode  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('locOutFaultCode') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="locOutFaultCode" class="'&clip('green bold')&'"'&clip(loc:extra)&'>' & p_web.Translate(p_web.GSV('locOutFaultCode'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locOutFaultCode  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locOutFaultCode:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('locOutFaultCode') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::par:Part_Number  Routine
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('par:Part_Number') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Part Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::par:Part_Number  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    par:Part_Number = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    par:Part_Number = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  ElsIf p_web.RequestAjax = 1 and p_web.IfExistsValue('stm:Part_Number')
    par:Part_Number = p_web.GetValue('stm:Part_Number')
  ElsIf p_web.RequestAjax = 1
    par:Part_Number = stm:Part_Number
  End
  do ValidateValue::par:Part_Number  ! copies value to session value if valid.
      if (p_web.GSV('job:Engineer') <> '')
          locUserCode = p_web.GSV('job:Engineer')
      else !
          locUserCode = p_web.GSV('BookingUSerCOde')
      end !if (p_web.GSV('job:Engineer') <> '')
  
      case validFreeTextPart('C',locUserCode,p_web.GSV('job:Manufacturer'),|
                                  p_web.GSV('job:Model_Number'),p_web.GSV('par:part_Number'))
      of 0
          p_web.SSV('Comment:PartNumber','')
  
          Access:STOMODEL.Clearkey(stm:Location_Part_Number_Key)
          stm:Model_Number    = p_web.GSV('job:Model_Number')
          stm:Location    = p_web.GSV('BookingSiteLocation')
          stm:Part_Number    = p_web.GSV('par:Part_Number')
          if (Access:STOMODEL.TryFetch(stm:Location_Part_Number_Key) = Level:Benign)
              ! Found
          else ! if (Access:STOMODEL.TryFetch(stm:Location_Part_Number_Key) = Level:Benign)
              ! Error
          end ! if (Access:STOMODEL.TryFetch(stm:Location_Part_Number_Key) = Level:Benign)
  
          do updatePartDetails
      of 1
          p_web.SSV('par:Part_Number','')
          p_web.SSV('Comment:PartNumber','Invalid User')
      of 2
          p_web.SSV('par:Part_Number','')
          p_web.SSV('Comment:PartNumber','Error! Selected Part Is Not In Stock Location')
      of 3
          p_web.SSV('par:Part_Number','')
          p_web.SSV('Comment:PartNumber','Error! Selected Part Is Suspended')
      of 4
          p_web.SSV('Comment:PartNumber','Warning! Cannot Find The Selected Part In Stock')
      of 5
          p_web.SSV('par:part_Number','')
          p_web.SSV('Comment:PartNumber','Error! Access Level Is Not High Enough For Part')
  
      end ! case
  
  p_Web.SetValue('lookupfield','par:Part_Number')
  do AfterLookup
  do Value::par:Part_Number
  do SendAlert
  do Comment::par:Part_Number
  do Comment::par:Part_Number
  do Value::par:Description  !1
  do Value::par:Supplier  !1
  do Value::tmp:PurchaseCost  !1
  do Value::tmp:OutWarrantyCost  !1
  do Value::tmp:OutWarrantyMarkup  !1
  do Prompt::tmp:SecondLocation
  do Value::tmp:SecondLocation  !1
  do Prompt::tmp:ShelfLocation
  do Value::tmp:ShelfLocation  !1
  do Prompt::tmp:Location
  do Value::tmp:Location  !1

ValidateValue::par:Part_Number  Routine
    If not (1=0)
  If par:Part_Number = ''
    loc:Invalid = 'par:Part_Number'
    par:Part_Number:IsInvalid = true
    loc:alert = p_web.translate('Part Number') & ' ' & p_web.site.RequiredText
  End
    par:Part_Number = Upper(par:Part_Number)
      if loc:invalid = '' then p_web.SetSessionValue('par:Part_Number',par:Part_Number).
    End

Value::par:Part_Number  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('par:Part_Number') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.getValue('adjustment') = 1 OR loc:act = ChangeRecord or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,'formrqd')
  If loc:retrying
    par:Part_Number = p_web.RestoreValue('par:Part_Number')
    do ValidateValue::par:Part_Number
    If par:Part_Number:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- par:Part_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.getValue('adjustment') = 1 OR loc:act = ChangeRecord,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''par:Part_Number'',''formchargeableparts_par:part_number_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('par:Part_Number')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','par:Part_Number',p_web.GetSessionValue('par:Part_Number'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s30',loc:javascript,p_web.PicLength('@s30'),,,) & '<13,10>'!4
    if not loc:viewonly and not loc:readonly
      loc:fieldclass = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('BrowseModelStock')&'?LookupField=par:Part_Number&Tab=3&ForeignField=stm:Part_Number&_sort=stm:Description&Refresh=sort'),,,,,loc:fieldclass)
    End
  do SendPacket
  End
  p_web.DivFooter()
Comment::par:Part_Number  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if par:Part_Number:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:PartNumber'))
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('par:Part_Number') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::par:Description  Routine
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('par:Description') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Description'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::par:Description  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    par:Description = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    par:Description = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::par:Description  ! copies value to session value if valid.
  do Value::par:Description
  do SendAlert
  do Comment::par:Description ! allows comment style to be updated.

ValidateValue::par:Description  Routine
    If not (1=0)
  If par:Description = ''
    loc:Invalid = 'par:Description'
    par:Description:IsInvalid = true
    loc:alert = p_web.translate('Description') & ' ' & p_web.site.RequiredText
  End
    par:Description = Upper(par:Description)
      if loc:invalid = '' then p_web.SetSessionValue('par:Description',par:Description).
    End

Value::par:Description  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('par:Description') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.getValue('adjustment') = 1 OR loc:act = ChangeRecord or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,'formrqd')
  If loc:retrying
    par:Description = p_web.RestoreValue('par:Description')
    do ValidateValue::par:Description
    If par:Description:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- par:Description
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.getValue('adjustment') = 1 OR loc:act = ChangeRecord,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''par:Description'',''formchargeableparts_par:description_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','par:Description',p_web.GetSessionValueFormat('par:Description'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::par:Description  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if par:Description:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web._jsok(p_web.site.RequiredText)
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('par:Description') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::par:Despatch_Note_Number  Routine
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('par:Despatch_Note_Number') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Despatch Note Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::par:Despatch_Note_Number  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    par:Despatch_Note_Number = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    par:Despatch_Note_Number = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::par:Despatch_Note_Number  ! copies value to session value if valid.
  do Value::par:Despatch_Note_Number
  do SendAlert
  do Comment::par:Despatch_Note_Number ! allows comment style to be updated.

ValidateValue::par:Despatch_Note_Number  Routine
    If not (1=0)
    par:Despatch_Note_Number = Upper(par:Despatch_Note_Number)
      if loc:invalid = '' then p_web.SetSessionValue('par:Despatch_Note_Number',par:Despatch_Note_Number).
    End

Value::par:Despatch_Note_Number  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('par:Despatch_Note_Number') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    par:Despatch_Note_Number = p_web.RestoreValue('par:Despatch_Note_Number')
    do ValidateValue::par:Despatch_Note_Number
    If par:Despatch_Note_Number:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- par:Despatch_Note_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''par:Despatch_Note_Number'',''formchargeableparts_par:despatch_note_number_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','par:Despatch_Note_Number',p_web.GetSessionValueFormat('par:Despatch_Note_Number'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::par:Despatch_Note_Number  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if par:Despatch_Note_Number:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('par:Despatch_Note_Number') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::par:Quantity  Routine
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('par:Quantity') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Quantity'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::par:Quantity  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    par:Quantity = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n4'  !FieldPicture = @n8
    par:Quantity = p_web.Dformat(p_web.GetValue('Value'),'@n4')
  End
  do ValidateValue::par:Quantity  ! copies value to session value if valid.
  do Value::par:Quantity
  do SendAlert
  do Comment::par:Quantity ! allows comment style to be updated.

ValidateValue::par:Quantity  Routine
    If not (1=0)
  If par:Quantity = ''
    loc:Invalid = 'par:Quantity'
    par:Quantity:IsInvalid = true
    loc:alert = p_web.translate('Quantity') & ' ' & p_web.site.RequiredText
  End
    par:Quantity = Upper(par:Quantity)
      if loc:invalid = '' then p_web.SetSessionValue('par:Quantity',par:Quantity).
    End

Value::par:Quantity  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('par:Quantity') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.getValue('adjustment') = 1 OR p_web.GSV('ReadOnly:Quantity') = 1 or loc:act = ChangeRecord or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,'formrqd')
  If loc:retrying
    par:Quantity = p_web.RestoreValue('par:Quantity')
    do ValidateValue::par:Quantity
    If par:Quantity:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- par:Quantity
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.getValue('adjustment') = 1 OR p_web.GSV('ReadOnly:Quantity') = 1 or loc:act = ChangeRecord,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''par:Quantity'',''formchargeableparts_par:quantity_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','par:Quantity',p_web.GetSessionValue('par:Quantity'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n4',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::par:Quantity  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if par:Quantity:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web._jsok(p_web.site.RequiredText)
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('par:Quantity') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:Location  Routine
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:Location') & '_prompt',Choose(p_web.GSV('tmp:Location') = '','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('tmp:Location') = '','',p_web.Translate('Location'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:Location  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:Location = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:Location = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:Location  ! copies value to session value if valid.
  do Comment::tmp:Location ! allows comment style to be updated.

ValidateValue::tmp:Location  Routine
    If not (p_web.GSV('tmp:Location') = '')
      if loc:invalid = '' then p_web.SetSessionValue('tmp:Location',tmp:Location).
    End

Value::tmp:Location  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('tmp:Location') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:Location') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (p_web.GSV('tmp:Location') = '')
  ! --- DISPLAY --- tmp:Location
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('tmp:Location'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:Location  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:Location:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('tmp:Location') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:Location') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('tmp:Location') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:SecondLocation  Routine
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:SecondLocation') & '_prompt',Choose(p_web.GSV('tmp:SecondLocation') = '','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('tmp:SecondLocation') = '','',p_web.Translate('Second Location'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:SecondLocation  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:SecondLocation = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:SecondLocation = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:SecondLocation  ! copies value to session value if valid.
  do Comment::tmp:SecondLocation ! allows comment style to be updated.

ValidateValue::tmp:SecondLocation  Routine
    If not (p_web.GSV('tmp:SecondLocation') = '')
      if loc:invalid = '' then p_web.SetSessionValue('tmp:SecondLocation',tmp:SecondLocation).
    End

Value::tmp:SecondLocation  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('tmp:SecondLocation') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:SecondLocation') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (p_web.GSV('tmp:SecondLocation') = '')
  ! --- DISPLAY --- tmp:SecondLocation
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('tmp:SecondLocation'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:SecondLocation  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:SecondLocation:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('tmp:SecondLocation') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:SecondLocation') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('tmp:SecondLocation') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:ShelfLocation  Routine
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:ShelfLocation') & '_prompt',Choose(p_web.GSV('tmp:ShelfLocation') = '','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('tmp:ShelfLocation') = '','',p_web.Translate('Shelf Location'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:ShelfLocation  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:ShelfLocation = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:ShelfLocation = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:ShelfLocation  ! copies value to session value if valid.
  do Comment::tmp:ShelfLocation ! allows comment style to be updated.

ValidateValue::tmp:ShelfLocation  Routine
    If not (p_web.GSV('tmp:ShelfLocation') = '')
      if loc:invalid = '' then p_web.SetSessionValue('tmp:ShelfLocation',tmp:ShelfLocation).
    End

Value::tmp:ShelfLocation  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('tmp:ShelfLocation') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:ShelfLocation') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (p_web.GSV('tmp:ShelfLocation') = '')
  ! --- DISPLAY --- tmp:ShelfLocation
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('tmp:ShelfLocation'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:ShelfLocation  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:ShelfLocation:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('tmp:ShelfLocation') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:ShelfLocation') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('tmp:ShelfLocation') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:PurchaseCost  Routine
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:PurchaseCost') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Purchase Cost'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:PurchaseCost  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:PurchaseCost = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n_14.2'  !FieldPicture = 
    tmp:PurchaseCost = p_web.Dformat(p_web.GetValue('Value'),'@n_14.2')
  End
  do ValidateValue::tmp:PurchaseCost  ! copies value to session value if valid.
  do Value::tmp:PurchaseCost
  do SendAlert
  do Comment::tmp:PurchaseCost ! allows comment style to be updated.

ValidateValue::tmp:PurchaseCost  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:PurchaseCost',tmp:PurchaseCost).
    End

Value::tmp:PurchaseCost  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:PurchaseCost') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If p_web.getValue('adjustment') = 1 or p_web.GSV('ReadOnly:PurchaseCost') = 1 OR loc:act = ChangeRecord or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    tmp:PurchaseCost = p_web.RestoreValue('tmp:PurchaseCost')
    do ValidateValue::tmp:PurchaseCost
    If tmp:PurchaseCost:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- tmp:PurchaseCost
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.getValue('adjustment') = 1 or p_web.GSV('ReadOnly:PurchaseCost') = 1 OR loc:act = ChangeRecord,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:PurchaseCost'',''formchargeableparts_tmp:purchasecost_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:PurchaseCost',p_web.GetSessionValue('tmp:PurchaseCost'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n_14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:PurchaseCost  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:PurchaseCost:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:PurchaseCost') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:OutWarrantyCost  Routine
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:OutWarrantyCost') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Out Of Warranty Cost'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:OutWarrantyCost  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:OutWarrantyCost = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n_14.2'  !FieldPicture = 
    tmp:OutWarrantyCost = p_web.Dformat(p_web.GetValue('Value'),'@n_14.2')
  End
  do ValidateValue::tmp:OutWarrantyCost  ! copies value to session value if valid.
  do enableDisableCosts
  do Value::tmp:OutWarrantyCost
  do SendAlert
  do Comment::tmp:OutWarrantyCost ! allows comment style to be updated.
  do Value::tmp:OutWarrantyMarkup  !1

ValidateValue::tmp:OutWarrantyCost  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:OutWarrantyCost',tmp:OutWarrantyCost).
    End

Value::tmp:OutWarrantyCost  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:OutWarrantyCost') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If p_web.GSV('ReadOnly:OutWarrantyCost') = 1 Or p_web.GSV('tmp:OutWarrantyMarkup') > 0 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    tmp:OutWarrantyCost = p_web.RestoreValue('tmp:OutWarrantyCost')
    do ValidateValue::tmp:OutWarrantyCost
    If tmp:OutWarrantyCost:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- tmp:OutWarrantyCost
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:OutWarrantyCost') = 1 Or p_web.GSV('tmp:OutWarrantyMarkup') > 0,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:OutWarrantyCost'',''formchargeableparts_tmp:outwarrantycost_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:OutWarrantyCost')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:OutWarrantyCost',p_web.GetSessionValue('tmp:OutWarrantyCost'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n_14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:OutWarrantyCost  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:OutWarrantyCost:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:OutWarrantyCost') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:OutWarrantyMarkup  Routine
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:OutWarrantyMarkup') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Markup'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:OutWarrantyMarkup  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:OutWarrantyMarkup = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n3'  !FieldPicture = 
    tmp:OutWarrantyMarkup = p_web.Dformat(p_web.GetValue('Value'),'@n3')
  End
  do ValidateValue::tmp:OutWarrantyMarkup  ! copies value to session value if valid.
  !Out Warranty Markup
  markup# = GETINI('STOCK','MaxPartMarkup',,CLIP(PATH())&'\SB2KDEF.INI')
  if (p_web.GSV('tmp:OutWarrantyMarkup') > markup#)
      p_web.SSV('Comment:OutWarrantyMarkup','You cannot markup more than ' & markup# & '%')
      p_web.SSV('tmp:OutWarrantyMarkup',markup#)
  else! if (p_web.GSV('tmp:OutWarrantyMarkup') > GETINI('STOCK','MaxPartMarkup',,CLIP(PATH())&'\SB2KDEF.INI'))
      p_web.SSV('Comment:OutWarrantyMarkup','')
  end ! if (p_web.GSV('tmp:OutWarrantyMarkup') > GETINI('STOCK','MaxPartMarkup',,CLIP(PATH())&'\SB2KDEF.INI'))
  
  if (p_web.GSV('tmp:OutWarrantyMarkup') > 0)
      p_web.SSV('tmp:OutWarrantyCost',format(p_web.GSV('tmp:PurchaseCost') + (p_web.GSV('tmp:PurchaseCost') * (p_web.GSV('tmp:OutWarrantyMarkup')/100)),@n_14.2))
  end !if (p_web.GSV('tmp:OutWarrantyMarkup') > 0)
  do Value::tmp:OutWarrantyMarkup
  do SendAlert
  do Comment::tmp:OutWarrantyMarkup ! allows comment style to be updated.
  do Value::tmp:OutWarrantyCost  !1
  do Comment::tmp:OutWarrantyMarkup

ValidateValue::tmp:OutWarrantyMarkup  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:OutWarrantyMarkup',tmp:OutWarrantyMarkup).
    End

Value::tmp:OutWarrantyMarkup  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:OutWarrantyMarkup') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If p_web.GSV('ReadOnly:OutWarrantyMarkup') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    tmp:OutWarrantyMarkup = p_web.RestoreValue('tmp:OutWarrantyMarkup')
    do ValidateValue::tmp:OutWarrantyMarkup
    If tmp:OutWarrantyMarkup:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- tmp:OutWarrantyMarkup
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:OutWarrantyMarkup') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:OutWarrantyMarkup'',''formchargeableparts_tmp:outwarrantymarkup_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:OutWarrantyMarkup')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:OutWarrantyMarkup',p_web.GetSessionValue('tmp:OutWarrantyMarkup'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n3',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:OutWarrantyMarkup  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:OutWarrantyMarkup:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:OutWarrantyMarkup'))
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:OutWarrantyMarkup') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:FixedPrice  Routine
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FixedPrice') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:FixedPrice  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::tmp:FixedPrice  ! copies value to session value if valid.
  do Comment::tmp:FixedPrice ! allows comment style to be updated.

ValidateValue::tmp:FixedPrice  Routine
    If not (1=0)
    End

Value::tmp:FixedPrice  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FixedPrice') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:FixedPrice  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:FixedPrice:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FixedPrice') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::par:Supplier  Routine
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('par:Supplier') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Supplier'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::par:Supplier  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    par:Supplier = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    par:Supplier = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  ElsIf p_web.RequestAjax = 1 and p_web.IfExistsValue('sup:Company_Name')
    par:Supplier = p_web.GetValue('sup:Company_Name')
  ElsIf p_web.RequestAjax = 1
    par:Supplier = sup:Company_Name
  End
  do ValidateValue::par:Supplier  ! copies value to session value if valid.
  p_Web.SetValue('lookupfield','par:Supplier')
  do AfterLookup
  do Value::par:Supplier
  do SendAlert
  do Comment::par:Supplier

ValidateValue::par:Supplier  Routine
    If not (1=0)
    par:Supplier = Upper(par:Supplier)
      if loc:invalid = '' then p_web.SetSessionValue('par:Supplier',par:Supplier).
    End

Value::par:Supplier  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('par:Supplier') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:act = ChangeRecord or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    par:Supplier = p_web.RestoreValue('par:Supplier')
    do ValidateValue::par:Supplier
    If par:Supplier:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- par:Supplier
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:act = ChangeRecord,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''par:Supplier'',''formchargeableparts_par:supplier_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon(par:Supplier)&''',2);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','par:Supplier',p_web.GetSessionValue('par:Supplier'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s30',loc:javascript,p_web.PicLength('@s30'),,,) & '<13,10>'!4
    if not loc:viewonly and not loc:readonly
      loc:fieldclass = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('SelectSuppliers')&'?LookupField=par:Supplier&Tab=3&ForeignField=sup:Company_Name&_sort=sup:Company_Name&Refresh=sort'),,,,,loc:fieldclass)
    End
  do SendPacket
  End
  p_web.DivFooter()
Comment::par:Supplier  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if par:Supplier:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('par:Supplier') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::par:Exclude_From_Order  Routine
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('par:Exclude_From_Order') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Exclude From Order'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::par:Exclude_From_Order  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    par:Exclude_From_Order = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s3
    par:Exclude_From_Order = p_web.Dformat(p_web.GetValue('Value'),'@s3')
  End
  do ValidateValue::par:Exclude_From_Order  ! copies value to session value if valid.
  do Value::par:Exclude_From_Order
  do SendAlert
  do Comment::par:Exclude_From_Order ! allows comment style to be updated.

ValidateValue::par:Exclude_From_Order  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('par:Exclude_From_Order',par:Exclude_From_Order).
    End

Value::par:Exclude_From_Order  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('par:Exclude_From_Order') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If loc:retrying
    par:Exclude_From_Order = p_web.RestoreValue('par:Exclude_From_Order')
    do ValidateValue::par:Exclude_From_Order
    If par:Exclude_From_Order:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- RADIO --- par:Exclude_From_Order
  loc:readonly = Choose(p_web.GSV('ReadOnly:ExcludeFromOrder') = 1,'disabled','')
    if p_web.GetSessionValue('par:Exclude_From_Order') = 'YES'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''par:Exclude_From_Order'',''formchargeableparts_par:exclude_from_order_value'',1,'''&p_web._jsok('YES')&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','par:Exclude_From_Order',clip('YES'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'par:Exclude_From_Order_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Yes') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(p_web.GSV('ReadOnly:ExcludeFromOrder') = 1,'disabled','')
    if p_web.GetSessionValue('par:Exclude_From_Order') = 'NO'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''par:Exclude_From_Order'',''formchargeableparts_par:exclude_from_order_value'',1,'''&p_web._jsok('NO')&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','par:Exclude_From_Order',clip('NO'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'par:Exclude_From_Order_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('No') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::par:Exclude_From_Order  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if par:Exclude_From_Order:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('par:Exclude_From_Order') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::par:PartAllocated  Routine
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('par:PartAllocated') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Part Allocated'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::par:PartAllocated  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    par:PartAllocated = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @n1
    par:PartAllocated = p_web.Dformat(p_web.GetValue('Value'),'@n1')
  End
  do ValidateValue::par:PartAllocated  ! copies value to session value if valid.
  do Value::par:PartAllocated
  do SendAlert
  do Comment::par:PartAllocated ! allows comment style to be updated.

ValidateValue::par:PartAllocated  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('par:PartAllocated',par:PartAllocated).
    End

Value::par:PartAllocated  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('par:PartAllocated') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If loc:retrying
    par:PartAllocated = p_web.RestoreValue('par:PartAllocated')
    do ValidateValue::par:PartAllocated
    If par:PartAllocated:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- RADIO --- par:PartAllocated
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartAllocated') = 1,'disabled','')
    if p_web.GetSessionValue('par:PartAllocated') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''par:PartAllocated'',''formchargeableparts_par:partallocated_value'',1,'''&p_web._jsok(1)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','par:PartAllocated',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,'Part Allocated','par:PartAllocated_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Yes') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartAllocated') = 1,'disabled','')
    if p_web.GetSessionValue('par:PartAllocated') = 0
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''par:PartAllocated'',''formchargeableparts_par:partallocated_value'',1,'''&p_web._jsok(0)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','par:PartAllocated',clip(0),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,'Part Allocated','par:PartAllocated_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('No') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::par:PartAllocated  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if par:PartAllocated:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('par:PartAllocated') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:UnallocatePart  Routine
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:UnallocatePart') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Unallocate Part'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:UnallocatePart  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:UnallocatePart = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    tmp:UnallocatePart = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:UnallocatePart  ! copies value to session value if valid.
  IF (p_web.GSV('tmp:UnAllocatePart') = 1)
      p_web.SSV('Comment:UnallocatePart','Part will appear in "Parts On Order" section of Stock Allocation')
  ELSE
      p_web.SSV('Comment:UnallocatePart','Return Part To Stock')
  END
  
  
  do Value::tmp:UnallocatePart
  do SendAlert
  do Comment::tmp:UnallocatePart ! allows comment style to be updated.
  do Comment::tmp:UnallocatePart

ValidateValue::tmp:UnallocatePart  Routine
  If p_web.GSV('Show:UnallocatePart') = 1
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:UnallocatePart',tmp:UnallocatePart).
    End
  End

Value::tmp:UnallocatePart  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:UnallocatePart') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If loc:retrying
    tmp:UnallocatePart = p_web.RestoreValue('tmp:UnallocatePart')
    do ValidateValue::tmp:UnallocatePart
    If tmp:UnallocatePart:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- CHECKBOX --- tmp:UnallocatePart
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:UnallocatePart'',''formchargeableparts_tmp:unallocatepart_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:UnallocatePart')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('tmp:UnallocatePart') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','tmp:UnallocatePart',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:UnallocatePart  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:UnallocatePart:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:UnallocatePart'))
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:UnallocatePart') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::text:OrderRequired  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::text:OrderRequired  ! copies value to session value if valid.
  do Comment::text:OrderRequired ! allows comment style to be updated.

ValidateValue::text:OrderRequired  Routine
    If not (1=0)
    End

Value::text:OrderRequired  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('text:OrderRequired') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="text:OrderRequired" class="'&clip('red bold large')&'"'&clip(loc:extra)&'>' & p_web.Translate(p_web.GSV('text:OrderRequired'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::text:OrderRequired  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if text:OrderRequired:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('text:OrderRequired') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::text:OrderRequired2  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::text:OrderRequired2  ! copies value to session value if valid.
  do Comment::text:OrderRequired2 ! allows comment style to be updated.

ValidateValue::text:OrderRequired2  Routine
    If not (1=0)
    End

Value::text:OrderRequired2  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('text:OrderRequired2') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    p_web.Translate('Either reduce the quanitity required, or select the check box to create an order for the excess items',) & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::text:OrderRequired2  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if text:OrderRequired2:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('text:OrderRequired2') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:CreateOrder  Routine
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:CreateOrder') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Create Order For Selected Part'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:CreateOrder  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:CreateOrder = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    tmp:CreateOrder = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:CreateOrder  ! copies value to session value if valid.
  do Value::tmp:CreateOrder
  do SendAlert
  do Comment::tmp:CreateOrder ! allows comment style to be updated.

ValidateValue::tmp:CreateOrder  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:CreateOrder',tmp:CreateOrder).
    End

Value::tmp:CreateOrder  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:CreateOrder') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If loc:retrying
    tmp:CreateOrder = p_web.RestoreValue('tmp:CreateOrder')
    do ValidateValue::tmp:CreateOrder
    If tmp:CreateOrder:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- CHECKBOX --- tmp:CreateOrder
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:CreateOrder'',''formchargeableparts_tmp:createorder_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('tmp:CreateOrder') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','tmp:CreateOrder',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:CreateOrder  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:CreateOrder:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:CreateOrder') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:FaultCodesChecked  Routine
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCodesChecked') & '_prompt',Choose(p_web.GSV('Hide:FaultCodesChecked') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:FaultCodesChecked') = 1,'',p_web.Translate('Fault Codes Checked'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:FaultCodesChecked  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:FaultCodesChecked = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    tmp:FaultCodesChecked = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:FaultCodesChecked  ! copies value to session value if valid.
  do Value::tmp:FaultCodesChecked
  do SendAlert
  do Comment::tmp:FaultCodesChecked ! allows comment style to be updated.

ValidateValue::tmp:FaultCodesChecked  Routine
  If 0
    If not (p_web.GSV('Hide:FaultCodesChecked') = 1)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:FaultCodesChecked',tmp:FaultCodesChecked).
    End
  End

Value::tmp:FaultCodesChecked  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:FaultCodesChecked') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCodesChecked') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If loc:retrying
    tmp:FaultCodesChecked = p_web.RestoreValue('tmp:FaultCodesChecked')
    do ValidateValue::tmp:FaultCodesChecked
    If tmp:FaultCodesChecked:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:FaultCodesChecked') = 1)
  ! --- CHECKBOX --- tmp:FaultCodesChecked
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:FaultCodesChecked'',''formchargeableparts_tmp:faultcodeschecked_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('tmp:FaultCodesChecked') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','tmp:FaultCodesChecked',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:FaultCodesChecked  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:FaultCodesChecked:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:FaultCodesChecked') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCodesChecked') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:FaultCodesChecked') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:FaultCodes1  Routine
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCodes1') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate(p_web.GSV('Prompt:PartFaultCode1')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:FaultCodes1  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:FaultCodes1 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = p_web.GSV('Picture:PartFaultCode1')  !FieldPicture = 
    tmp:FaultCodes1 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode1'))
  End
  do ValidateValue::tmp:FaultCodes1  ! copies value to session value if valid.
  do Value::tmp:FaultCodes1
  do SendAlert
  do Comment::tmp:FaultCodes1 ! allows comment style to be updated.

ValidateValue::tmp:FaultCodes1  Routine
  If p_web.GSV('Hide:PartFaultCode1') <> 1
    If not (1=0)
  If tmp:FaultCodes1 = '' and p_web.GSV('Req:PartFautlCode1') = 1
    loc:Invalid = 'tmp:FaultCodes1'
    tmp:FaultCodes1:IsInvalid = true
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode1')) & ' ' & p_web.site.RequiredText
  End
    tmp:FaultCodes1 = Upper(tmp:FaultCodes1)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:FaultCodes1',tmp:FaultCodes1).
    End
  End

Value::tmp:FaultCodes1  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCodes1') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.GSV('ReadOnly:PartFaultCode1') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If (p_web.GSV('Req:PartFautlCode1') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,'formrqd')
  End
  If loc:retrying
    tmp:FaultCodes1 = p_web.RestoreValue('tmp:FaultCodes1')
    do ValidateValue::tmp:FaultCodes1
    If tmp:FaultCodes1:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- tmp:FaultCodes1
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode1') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCodes1'',''formchargeableparts_tmp:faultcodes1_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes1',p_web.GetSessionValue('tmp:FaultCodes1'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode1'),loc:javascript,,,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(1)
  End
  p_web.DivFooter()
Comment::tmp:FaultCodes1  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:FaultCodes1:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode1'))
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCodes1') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:FaultCode2  Routine
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode2') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate(p_web.GSV('Prompt:PartFaultCode2')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:FaultCode2  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:FaultCodes2 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = p_web.GSV('Picture:PartFaultCode2')  !FieldPicture = 
    tmp:FaultCodes2 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode2'))
  End
  do ValidateValue::tmp:FaultCode2  ! copies value to session value if valid.
  do Value::tmp:FaultCode2
  do SendAlert
  do Comment::tmp:FaultCode2 ! allows comment style to be updated.

ValidateValue::tmp:FaultCode2  Routine
  If p_web.GSV('Hide:PartFaultCode2') <> 1
    If not (1=0)
  If tmp:FaultCodes2 = '' and p_web.GSV('Req:PartFaultCode2') = 1
    loc:Invalid = 'tmp:FaultCodes2'
    tmp:FaultCode2:IsInvalid = true
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode2')) & ' ' & p_web.site.RequiredText
  End
    tmp:FaultCodes2 = Upper(tmp:FaultCodes2)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:FaultCodes2',tmp:FaultCodes2).
    End
  End

Value::tmp:FaultCode2  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode2') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.GSV('ReadOnly:PartFaultCode2') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If (p_web.GSV('Req:PartFaultCode2') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,'formrqd')
  End
  If loc:retrying
    tmp:FaultCodes2 = p_web.RestoreValue('tmp:FaultCodes2')
    do ValidateValue::tmp:FaultCode2
    If tmp:FaultCode2:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- tmp:FaultCodes2
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode2') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode2'',''formchargeableparts_tmp:faultcode2_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes2',p_web.GetSessionValue('tmp:FaultCodes2'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode2'),loc:javascript,,,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(2)
  End
  p_web.DivFooter()
Comment::tmp:FaultCode2  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:FaultCode2:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode2'))
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode2') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:FaultCode3  Routine
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode3') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate(p_web.GSV('Prompt:PartFaultCode3')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:FaultCode3  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:FaultCodes3 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = p_web.GSV('Picture:PartFaultCode3')  !FieldPicture = 
    tmp:FaultCodes3 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode3'))
  End
  do ValidateValue::tmp:FaultCode3  ! copies value to session value if valid.
  do Value::tmp:FaultCode3
  do SendAlert
  do Comment::tmp:FaultCode3 ! allows comment style to be updated.

ValidateValue::tmp:FaultCode3  Routine
  If p_web.GSV('Hide:PartFaultCode3') <> 1
    If not (1=0)
  If tmp:FaultCodes3 = '' and p_web.GSV('Req:PartFaultCode3') = 1
    loc:Invalid = 'tmp:FaultCodes3'
    tmp:FaultCode3:IsInvalid = true
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode3')) & ' ' & p_web.site.RequiredText
  End
    tmp:FaultCodes3 = Upper(tmp:FaultCodes3)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:FaultCodes3',tmp:FaultCodes3).
    End
  End

Value::tmp:FaultCode3  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode3') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.GSV('ReadOnly:PartFaultCode3') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If (p_web.GSV('Req:PartFaultCode3') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,'formrqd')
  End
  If loc:retrying
    tmp:FaultCodes3 = p_web.RestoreValue('tmp:FaultCodes3')
    do ValidateValue::tmp:FaultCode3
    If tmp:FaultCode3:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- tmp:FaultCodes3
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode3') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode3'',''formchargeableparts_tmp:faultcode3_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes3',p_web.GetSessionValue('tmp:FaultCodes3'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode3'),loc:javascript,,,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(3)
  End
  p_web.DivFooter()
Comment::tmp:FaultCode3  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:FaultCode3:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode3'))
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode3') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:FaultCode4  Routine
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode4') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate(p_web.GSV('Prompt:PartFaultCode4')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:FaultCode4  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:FaultCodes4 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = p_web.GSV('Picture:PartFaultCode4')  !FieldPicture = 
    tmp:FaultCodes4 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode4'))
  End
  do ValidateValue::tmp:FaultCode4  ! copies value to session value if valid.
  do Value::tmp:FaultCode4
  do SendAlert
  do Comment::tmp:FaultCode4 ! allows comment style to be updated.

ValidateValue::tmp:FaultCode4  Routine
  If p_web.GSV('Hide:PartFaultCode4') <> 1
    If not (1=0)
  If tmp:FaultCodes4 = '' and p_web.GSV('Req:PartFaultCode4') = 1
    loc:Invalid = 'tmp:FaultCodes4'
    tmp:FaultCode4:IsInvalid = true
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode4')) & ' ' & p_web.site.RequiredText
  End
    tmp:FaultCodes4 = Upper(tmp:FaultCodes4)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:FaultCodes4',tmp:FaultCodes4).
    End
  End

Value::tmp:FaultCode4  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode4') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.GSV('ReadOnly:PartFaultCode4') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If (p_web.GSV('Req:PartFaultCode4') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,'formrqd')
  End
  If loc:retrying
    tmp:FaultCodes4 = p_web.RestoreValue('tmp:FaultCodes4')
    do ValidateValue::tmp:FaultCode4
    If tmp:FaultCode4:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- tmp:FaultCodes4
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode4') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode4'',''formchargeableparts_tmp:faultcode4_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes4',p_web.GetSessionValue('tmp:FaultCodes4'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode4'),loc:javascript,,,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(4)
  End
  p_web.DivFooter()
Comment::tmp:FaultCode4  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:FaultCode4:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode4'))
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode4') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:FaultCode5  Routine
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode5') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate(p_web.GSV('Prompt:PartFaultCode5')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:FaultCode5  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:FaultCodes5 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = p_web.GSV('Picture:PartFaultCode5')  !FieldPicture = 
    tmp:FaultCodes5 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode5'))
  End
  do ValidateValue::tmp:FaultCode5  ! copies value to session value if valid.
  do Value::tmp:FaultCode5
  do SendAlert
  do Comment::tmp:FaultCode5 ! allows comment style to be updated.

ValidateValue::tmp:FaultCode5  Routine
  If p_web.GSV('Hide:PartFaultCode5') <> 1
    If not (1=0)
  If tmp:FaultCodes5 = '' and p_web.GSV('Req:PartFaultCode5') = 1
    loc:Invalid = 'tmp:FaultCodes5'
    tmp:FaultCode5:IsInvalid = true
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode5')) & ' ' & p_web.site.RequiredText
  End
    tmp:FaultCodes5 = Upper(tmp:FaultCodes5)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:FaultCodes5',tmp:FaultCodes5).
    End
  End

Value::tmp:FaultCode5  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode5') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.GSV('ReadOnly:PartFaultCode5') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If (p_web.GSV('Req:PartFaultCode5') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,'formrqd')
  End
  If loc:retrying
    tmp:FaultCodes5 = p_web.RestoreValue('tmp:FaultCodes5')
    do ValidateValue::tmp:FaultCode5
    If tmp:FaultCode5:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- tmp:FaultCodes5
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode5') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode5'',''formchargeableparts_tmp:faultcode5_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes5',p_web.GetSessionValue('tmp:FaultCodes5'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode5'),loc:javascript,,,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(5)
  End
  p_web.DivFooter()
Comment::tmp:FaultCode5  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:FaultCode5:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode5'))
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode5') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:FaultCode6  Routine
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode6') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate(p_web.GSV('Prompt:PartFaultCode6')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:FaultCode6  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:FaultCodes6 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = p_web.GSV('Picture:PartFaultCode6')  !FieldPicture = 
    tmp:FaultCodes6 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode6'))
  End
  do ValidateValue::tmp:FaultCode6  ! copies value to session value if valid.
  do Value::tmp:FaultCode6
  do SendAlert
  do Comment::tmp:FaultCode6 ! allows comment style to be updated.

ValidateValue::tmp:FaultCode6  Routine
  If p_web.GSV('Hide:PartFaultCode6') <> 1
    If not (1=0)
  If tmp:FaultCodes6 = '' and p_web.GSV('Req:PartFaultCode6') = 1
    loc:Invalid = 'tmp:FaultCodes6'
    tmp:FaultCode6:IsInvalid = true
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode6')) & ' ' & p_web.site.RequiredText
  End
    tmp:FaultCodes6 = Upper(tmp:FaultCodes6)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:FaultCodes6',tmp:FaultCodes6).
    End
  End

Value::tmp:FaultCode6  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode6') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.GSV('ReadOnly:PartFaultCode6') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If (p_web.GSV('Req:PartFaultCode6') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,'formrqd')
  End
  If loc:retrying
    tmp:FaultCodes6 = p_web.RestoreValue('tmp:FaultCodes6')
    do ValidateValue::tmp:FaultCode6
    If tmp:FaultCode6:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- tmp:FaultCodes6
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode6') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode6'',''formchargeableparts_tmp:faultcode6_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes6',p_web.GetSessionValue('tmp:FaultCodes6'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode6'),loc:javascript,,,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(6)
  End
  p_web.DivFooter()
Comment::tmp:FaultCode6  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:FaultCode6:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode6'))
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode6') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:FaultCode7  Routine
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode7') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate(p_web.GSV('Prompt:PartFaultCode7')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:FaultCode7  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:FaultCodes7 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = p_web.GSV('Picture:PartFaultCode7')  !FieldPicture = 
    tmp:FaultCodes7 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode7'))
  End
  do ValidateValue::tmp:FaultCode7  ! copies value to session value if valid.
  do Value::tmp:FaultCode7
  do SendAlert
  do Comment::tmp:FaultCode7 ! allows comment style to be updated.

ValidateValue::tmp:FaultCode7  Routine
  If p_web.GSV('Hide:PartFaultCode7') <> 1
    If not (1=0)
  If tmp:FaultCodes7 = '' and p_web.GSV('Req:PartFaultCode7') = 1
    loc:Invalid = 'tmp:FaultCodes7'
    tmp:FaultCode7:IsInvalid = true
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode7')) & ' ' & p_web.site.RequiredText
  End
    tmp:FaultCodes7 = Upper(tmp:FaultCodes7)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:FaultCodes7',tmp:FaultCodes7).
    End
  End

Value::tmp:FaultCode7  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode7') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.GSV('ReadOnly:PartFaultCode7') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If (p_web.GSV('Req:PartFaultCode7') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,'formrqd')
  End
  If loc:retrying
    tmp:FaultCodes7 = p_web.RestoreValue('tmp:FaultCodes7')
    do ValidateValue::tmp:FaultCode7
    If tmp:FaultCode7:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- tmp:FaultCodes7
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode7') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode7'',''formchargeableparts_tmp:faultcode7_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes7',p_web.GetSessionValue('tmp:FaultCodes7'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode7'),loc:javascript,,,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(7)
  End
  p_web.DivFooter()
Comment::tmp:FaultCode7  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:FaultCode7:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode7'))
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode7') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:FaultCode8  Routine
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode8') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate(p_web.GSV('Prompt:PartFaultCode8')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:FaultCode8  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:FaultCodes8 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = p_web.GSV('Picture:PartFaultCode8')  !FieldPicture = 
    tmp:FaultCodes8 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode8'))
  End
  do ValidateValue::tmp:FaultCode8  ! copies value to session value if valid.
  do Value::tmp:FaultCode8
  do SendAlert
  do Comment::tmp:FaultCode8 ! allows comment style to be updated.

ValidateValue::tmp:FaultCode8  Routine
  If p_web.GSV('Hide:PartFaultCode8') <> 1
    If not (1=0)
  If tmp:FaultCodes8 = '' and p_web.GSV('Req:PartFaultCode8') = 1
    loc:Invalid = 'tmp:FaultCodes8'
    tmp:FaultCode8:IsInvalid = true
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode8')) & ' ' & p_web.site.RequiredText
  End
    tmp:FaultCodes8 = Upper(tmp:FaultCodes8)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:FaultCodes8',tmp:FaultCodes8).
    End
  End

Value::tmp:FaultCode8  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode8') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.GSV('ReadOnly:PartFaultCode8') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If (p_web.GSV('Req:PartFaultCode8') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,'formrqd')
  End
  If loc:retrying
    tmp:FaultCodes8 = p_web.RestoreValue('tmp:FaultCodes8')
    do ValidateValue::tmp:FaultCode8
    If tmp:FaultCode8:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- tmp:FaultCodes8
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode8') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode8'',''formchargeableparts_tmp:faultcode8_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes8',p_web.GetSessionValue('tmp:FaultCodes8'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode8'),loc:javascript,,,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(8)
  End
  p_web.DivFooter()
Comment::tmp:FaultCode8  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:FaultCode8:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode8'))
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode8') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:FaultCode9  Routine
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode9') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate(p_web.GSV('Prompt:PartFaultCode9')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:FaultCode9  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:FaultCodes9 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = p_web.GSV('Picture:PartFaultCode9')  !FieldPicture = 
    tmp:FaultCodes9 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode9'))
  End
  do ValidateValue::tmp:FaultCode9  ! copies value to session value if valid.
  do Value::tmp:FaultCode9
  do SendAlert
  do Comment::tmp:FaultCode9 ! allows comment style to be updated.

ValidateValue::tmp:FaultCode9  Routine
  If p_web.GSV('Hide:PartFaultCode9') <> 1
    If not (1=0)
  If tmp:FaultCodes9 = '' and p_web.GSV('Req:PartFaultCode9') = 1
    loc:Invalid = 'tmp:FaultCodes9'
    tmp:FaultCode9:IsInvalid = true
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode9')) & ' ' & p_web.site.RequiredText
  End
    tmp:FaultCodes9 = Upper(tmp:FaultCodes9)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:FaultCodes9',tmp:FaultCodes9).
    End
  End

Value::tmp:FaultCode9  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode9') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.GSV('ReadOnly:PartFaultCode9') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If (p_web.GSV('Req:PartFaultCode9') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,'formrqd')
  End
  If loc:retrying
    tmp:FaultCodes9 = p_web.RestoreValue('tmp:FaultCodes9')
    do ValidateValue::tmp:FaultCode9
    If tmp:FaultCode9:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- tmp:FaultCodes9
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode9') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode9'',''formchargeableparts_tmp:faultcode9_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes9',p_web.GetSessionValue('tmp:FaultCodes9'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode9'),loc:javascript,,,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(9)
  End
  p_web.DivFooter()
Comment::tmp:FaultCode9  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:FaultCode9:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode9'))
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode9') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:FaultCode10  Routine
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode10') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate(p_web.GSV('Prompt:PartFaultCode10')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:FaultCode10  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:FaultCodes10 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = p_web.GSV('Picture:PartFaultCode10')  !FieldPicture = 
    tmp:FaultCodes10 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode10'))
  End
  do ValidateValue::tmp:FaultCode10  ! copies value to session value if valid.
  do Value::tmp:FaultCode10
  do SendAlert
  do Comment::tmp:FaultCode10 ! allows comment style to be updated.

ValidateValue::tmp:FaultCode10  Routine
  If p_web.GSV('Hide:PartFaultCode10') <> 1
    If not (1=0)
  If tmp:FaultCodes10 = '' and p_web.GSV('Req:PartFaultCode10') = 1
    loc:Invalid = 'tmp:FaultCodes10'
    tmp:FaultCode10:IsInvalid = true
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode10')) & ' ' & p_web.site.RequiredText
  End
    tmp:FaultCodes10 = Upper(tmp:FaultCodes10)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:FaultCodes10',tmp:FaultCodes10).
    End
  End

Value::tmp:FaultCode10  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode10') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.GSV('ReadOnly:PartFaultCode10') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If (p_web.GSV('Req:PartFaultCode10') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,'formrqd')
  End
  If loc:retrying
    tmp:FaultCodes10 = p_web.RestoreValue('tmp:FaultCodes10')
    do ValidateValue::tmp:FaultCode10
    If tmp:FaultCode10:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- tmp:FaultCodes10
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode10') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode10'',''formchargeableparts_tmp:faultcode10_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes10',p_web.GetSessionValue('tmp:FaultCodes10'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode10'),loc:javascript,,,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(10)
  End
  p_web.DivFooter()
Comment::tmp:FaultCode10  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:FaultCode10:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode10'))
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode10') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:FaultCode11  Routine
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode11') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate(p_web.GSV('Prompt:PartFaultCode11')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:FaultCode11  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:FaultCodes11 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = p_web.GSV('Picture:PartFaultCode11')  !FieldPicture = 
    tmp:FaultCodes11 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode11'))
  End
  do ValidateValue::tmp:FaultCode11  ! copies value to session value if valid.
  do Value::tmp:FaultCode11
  do SendAlert
  do Comment::tmp:FaultCode11 ! allows comment style to be updated.

ValidateValue::tmp:FaultCode11  Routine
  If p_web.GSV('Hide:PartFaultCode11') <> 1
    If not (1=0)
  If tmp:FaultCodes11 = '' and p_web.GSV('Req:PartFaultCode11') = 1
    loc:Invalid = 'tmp:FaultCodes11'
    tmp:FaultCode11:IsInvalid = true
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode11')) & ' ' & p_web.site.RequiredText
  End
    tmp:FaultCodes11 = Upper(tmp:FaultCodes11)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:FaultCodes11',tmp:FaultCodes11).
    End
  End

Value::tmp:FaultCode11  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode11') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.GSV('ReadOnly:PartFaultCode11') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If (p_web.GSV('Req:PartFaultCode11') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,'formrqd')
  End
  If loc:retrying
    tmp:FaultCodes11 = p_web.RestoreValue('tmp:FaultCodes11')
    do ValidateValue::tmp:FaultCode11
    If tmp:FaultCode11:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- tmp:FaultCodes11
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode11') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode11'',''formchargeableparts_tmp:faultcode11_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes11',p_web.GetSessionValue('tmp:FaultCodes11'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode11'),loc:javascript,,,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(11)
  End
  p_web.DivFooter()
Comment::tmp:FaultCode11  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:FaultCode11:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode11'))
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode11') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:FaultCode12  Routine
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode12') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate(p_web.GSV('Prompt:PartFaultCode12')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:FaultCode12  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:FaultCodes12 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = p_web.GSV('Picture:PartFaultCode12')  !FieldPicture = 
    tmp:FaultCodes12 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode12'))
  End
  do ValidateValue::tmp:FaultCode12  ! copies value to session value if valid.
  do Value::tmp:FaultCode12
  do SendAlert
  do Comment::tmp:FaultCode12 ! allows comment style to be updated.

ValidateValue::tmp:FaultCode12  Routine
  If p_web.GSV('Hide:PartFaultCode12') <> 1
    If not (1=0)
  If tmp:FaultCodes12 = '' and p_web.GSV('Req:PartFaultCode12') = 1
    loc:Invalid = 'tmp:FaultCodes12'
    tmp:FaultCode12:IsInvalid = true
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode12')) & ' ' & p_web.site.RequiredText
  End
    tmp:FaultCodes12 = Upper(tmp:FaultCodes12)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:FaultCodes12',tmp:FaultCodes12).
    End
  End

Value::tmp:FaultCode12  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode12') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.GSV('ReadOnly:PartFaultCode12') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If (p_web.GSV('Req:PartFaultCode12') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,'formrqd')
  End
  If loc:retrying
    tmp:FaultCodes12 = p_web.RestoreValue('tmp:FaultCodes12')
    do ValidateValue::tmp:FaultCode12
    If tmp:FaultCode12:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- tmp:FaultCodes12
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode12') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode12'',''formchargeableparts_tmp:faultcode12_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes12',p_web.GetSessionValue('tmp:FaultCodes12'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode12'),loc:javascript,,,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(12)
  End
  p_web.DivFooter()
Comment::tmp:FaultCode12  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:FaultCode12:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode12'))
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormChargeableParts_' & p_web._nocolon('tmp:FaultCode12') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('FormChargeableParts_nexttab_' & 0)
    par:Order_Number = p_web.GSV('par:Order_Number')
    do ValidateValue::par:Order_Number
    If loc:Invalid
      loc:retrying = 1
      do Value::par:Order_Number
      !do SendAlert
      do Comment::par:Order_Number ! allows comment style to be updated.
      !exit
    End
    par:Date_Ordered = p_web.GSV('par:Date_Ordered')
    do ValidateValue::par:Date_Ordered
    If loc:Invalid
      loc:retrying = 1
      do Value::par:Date_Ordered
      !do SendAlert
      do Comment::par:Date_Ordered ! allows comment style to be updated.
      !exit
    End
    par:Date_Received = p_web.GSV('par:Date_Received')
    do ValidateValue::par:Date_Received
    If loc:Invalid
      loc:retrying = 1
      do Value::par:Date_Received
      !do SendAlert
      do Comment::par:Date_Received ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('FormChargeableParts_nexttab_' & 1)
    par:Part_Number = p_web.GSV('par:Part_Number')
    do ValidateValue::par:Part_Number
    If loc:Invalid
      loc:retrying = 1
      do Value::par:Part_Number
      !do SendAlert
      do Comment::par:Part_Number ! allows comment style to be updated.
      !exit
    End
    par:Description = p_web.GSV('par:Description')
    do ValidateValue::par:Description
    If loc:Invalid
      loc:retrying = 1
      do Value::par:Description
      !do SendAlert
      do Comment::par:Description ! allows comment style to be updated.
      !exit
    End
    par:Despatch_Note_Number = p_web.GSV('par:Despatch_Note_Number')
    do ValidateValue::par:Despatch_Note_Number
    If loc:Invalid
      loc:retrying = 1
      do Value::par:Despatch_Note_Number
      !do SendAlert
      do Comment::par:Despatch_Note_Number ! allows comment style to be updated.
      !exit
    End
    par:Quantity = p_web.GSV('par:Quantity')
    do ValidateValue::par:Quantity
    If loc:Invalid
      loc:retrying = 1
      do Value::par:Quantity
      !do SendAlert
      do Comment::par:Quantity ! allows comment style to be updated.
      !exit
    End
    tmp:Location = p_web.GSV('tmp:Location')
    do ValidateValue::tmp:Location
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:Location
      !do SendAlert
      do Comment::tmp:Location ! allows comment style to be updated.
      !exit
    End
    tmp:SecondLocation = p_web.GSV('tmp:SecondLocation')
    do ValidateValue::tmp:SecondLocation
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:SecondLocation
      !do SendAlert
      do Comment::tmp:SecondLocation ! allows comment style to be updated.
      !exit
    End
    tmp:ShelfLocation = p_web.GSV('tmp:ShelfLocation')
    do ValidateValue::tmp:ShelfLocation
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:ShelfLocation
      !do SendAlert
      do Comment::tmp:ShelfLocation ! allows comment style to be updated.
      !exit
    End
    tmp:PurchaseCost = p_web.GSV('tmp:PurchaseCost')
    do ValidateValue::tmp:PurchaseCost
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:PurchaseCost
      !do SendAlert
      do Comment::tmp:PurchaseCost ! allows comment style to be updated.
      !exit
    End
    tmp:OutWarrantyCost = p_web.GSV('tmp:OutWarrantyCost')
    do ValidateValue::tmp:OutWarrantyCost
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:OutWarrantyCost
      !do SendAlert
      do Comment::tmp:OutWarrantyCost ! allows comment style to be updated.
      !exit
    End
    tmp:OutWarrantyMarkup = p_web.GSV('tmp:OutWarrantyMarkup')
    do ValidateValue::tmp:OutWarrantyMarkup
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:OutWarrantyMarkup
      !do SendAlert
      do Comment::tmp:OutWarrantyMarkup ! allows comment style to be updated.
      !exit
    End
    par:Supplier = p_web.GSV('par:Supplier')
    do ValidateValue::par:Supplier
    If loc:Invalid
      loc:retrying = 1
      do Value::par:Supplier
      !do SendAlert
      do Comment::par:Supplier ! allows comment style to be updated.
      !exit
    End
    par:Exclude_From_Order = p_web.GSV('par:Exclude_From_Order')
    do ValidateValue::par:Exclude_From_Order
    If loc:Invalid
      loc:retrying = 1
      do Value::par:Exclude_From_Order
      !do SendAlert
      do Comment::par:Exclude_From_Order ! allows comment style to be updated.
      !exit
    End
    par:PartAllocated = p_web.GSV('par:PartAllocated')
    do ValidateValue::par:PartAllocated
    If loc:Invalid
      loc:retrying = 1
      do Value::par:PartAllocated
      !do SendAlert
      do Comment::par:PartAllocated ! allows comment style to be updated.
      !exit
    End
    tmp:UnallocatePart = p_web.GSV('tmp:UnallocatePart')
    do ValidateValue::tmp:UnallocatePart
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:UnallocatePart
      !do SendAlert
      do Comment::tmp:UnallocatePart ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('FormChargeableParts_nexttab_' & 2)
    tmp:CreateOrder = p_web.GSV('tmp:CreateOrder')
    do ValidateValue::tmp:CreateOrder
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:CreateOrder
      !do SendAlert
      do Comment::tmp:CreateOrder ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('FormChargeableParts_nexttab_' & 3)
    tmp:FaultCodesChecked = p_web.GSV('tmp:FaultCodesChecked')
    do ValidateValue::tmp:FaultCodesChecked
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:FaultCodesChecked
      !do SendAlert
      do Comment::tmp:FaultCodesChecked ! allows comment style to be updated.
      !exit
    End
    tmp:FaultCodes1 = p_web.GSV('tmp:FaultCodes1')
    do ValidateValue::tmp:FaultCodes1
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:FaultCodes1
      !do SendAlert
      do Comment::tmp:FaultCodes1 ! allows comment style to be updated.
      !exit
    End
    tmp:FaultCodes2 = p_web.GSV('tmp:FaultCodes2')
    do ValidateValue::tmp:FaultCode2
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:FaultCode2
      !do SendAlert
      do Comment::tmp:FaultCode2 ! allows comment style to be updated.
      !exit
    End
    tmp:FaultCodes3 = p_web.GSV('tmp:FaultCodes3')
    do ValidateValue::tmp:FaultCode3
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:FaultCode3
      !do SendAlert
      do Comment::tmp:FaultCode3 ! allows comment style to be updated.
      !exit
    End
    tmp:FaultCodes4 = p_web.GSV('tmp:FaultCodes4')
    do ValidateValue::tmp:FaultCode4
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:FaultCode4
      !do SendAlert
      do Comment::tmp:FaultCode4 ! allows comment style to be updated.
      !exit
    End
    tmp:FaultCodes5 = p_web.GSV('tmp:FaultCodes5')
    do ValidateValue::tmp:FaultCode5
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:FaultCode5
      !do SendAlert
      do Comment::tmp:FaultCode5 ! allows comment style to be updated.
      !exit
    End
    tmp:FaultCodes6 = p_web.GSV('tmp:FaultCodes6')
    do ValidateValue::tmp:FaultCode6
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:FaultCode6
      !do SendAlert
      do Comment::tmp:FaultCode6 ! allows comment style to be updated.
      !exit
    End
    tmp:FaultCodes7 = p_web.GSV('tmp:FaultCodes7')
    do ValidateValue::tmp:FaultCode7
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:FaultCode7
      !do SendAlert
      do Comment::tmp:FaultCode7 ! allows comment style to be updated.
      !exit
    End
    tmp:FaultCodes8 = p_web.GSV('tmp:FaultCodes8')
    do ValidateValue::tmp:FaultCode8
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:FaultCode8
      !do SendAlert
      do Comment::tmp:FaultCode8 ! allows comment style to be updated.
      !exit
    End
    tmp:FaultCodes9 = p_web.GSV('tmp:FaultCodes9')
    do ValidateValue::tmp:FaultCode9
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:FaultCode9
      !do SendAlert
      do Comment::tmp:FaultCode9 ! allows comment style to be updated.
      !exit
    End
    tmp:FaultCodes10 = p_web.GSV('tmp:FaultCodes10')
    do ValidateValue::tmp:FaultCode10
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:FaultCode10
      !do SendAlert
      do Comment::tmp:FaultCode10 ! allows comment style to be updated.
      !exit
    End
    tmp:FaultCodes11 = p_web.GSV('tmp:FaultCodes11')
    do ValidateValue::tmp:FaultCode11
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:FaultCode11
      !do SendAlert
      do Comment::tmp:FaultCode11 ! allows comment style to be updated.
      !exit
    End
    tmp:FaultCodes12 = p_web.GSV('tmp:FaultCodes12')
    do ValidateValue::tmp:FaultCode12
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:FaultCode12
      !do SendAlert
      do Comment::tmp:FaultCode12 ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_FormChargeableParts_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('FormChargeableParts_tab_' & 0)
    do GenerateTab0
  of lower('FormChargeableParts_tab_' & 1)
    do GenerateTab1
  of lower('FormChargeableParts_par:Part_Number_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::par:Part_Number
      of event:timer
        do Value::par:Part_Number
        do Comment::par:Part_Number
      else
        do Value::par:Part_Number
      end
  of lower('FormChargeableParts_par:Description_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::par:Description
      of event:timer
        do Value::par:Description
        do Comment::par:Description
      else
        do Value::par:Description
      end
  of lower('FormChargeableParts_par:Despatch_Note_Number_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::par:Despatch_Note_Number
      of event:timer
        do Value::par:Despatch_Note_Number
        do Comment::par:Despatch_Note_Number
      else
        do Value::par:Despatch_Note_Number
      end
  of lower('FormChargeableParts_par:Quantity_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::par:Quantity
      of event:timer
        do Value::par:Quantity
        do Comment::par:Quantity
      else
        do Value::par:Quantity
      end
  of lower('FormChargeableParts_tmp:PurchaseCost_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:PurchaseCost
      of event:timer
        do Value::tmp:PurchaseCost
        do Comment::tmp:PurchaseCost
      else
        do Value::tmp:PurchaseCost
      end
  of lower('FormChargeableParts_tmp:OutWarrantyCost_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:OutWarrantyCost
      of event:timer
        do Value::tmp:OutWarrantyCost
        do Comment::tmp:OutWarrantyCost
      else
        do Value::tmp:OutWarrantyCost
      end
  of lower('FormChargeableParts_tmp:OutWarrantyMarkup_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:OutWarrantyMarkup
      of event:timer
        do Value::tmp:OutWarrantyMarkup
        do Comment::tmp:OutWarrantyMarkup
      else
        do Value::tmp:OutWarrantyMarkup
      end
  of lower('FormChargeableParts_par:Supplier_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::par:Supplier
      of event:timer
        do Value::par:Supplier
        do Comment::par:Supplier
      else
        do Value::par:Supplier
      end
  of lower('FormChargeableParts_par:Exclude_From_Order_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::par:Exclude_From_Order
      of event:timer
        do Value::par:Exclude_From_Order
        do Comment::par:Exclude_From_Order
      else
        do Value::par:Exclude_From_Order
      end
  of lower('FormChargeableParts_par:PartAllocated_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::par:PartAllocated
      of event:timer
        do Value::par:PartAllocated
        do Comment::par:PartAllocated
      else
        do Value::par:PartAllocated
      end
  of lower('FormChargeableParts_tmp:UnallocatePart_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:UnallocatePart
      of event:timer
        do Value::tmp:UnallocatePart
        do Comment::tmp:UnallocatePart
      else
        do Value::tmp:UnallocatePart
      end
  of lower('FormChargeableParts_tab_' & 2)
    do GenerateTab2
  of lower('FormChargeableParts_tmp:CreateOrder_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:CreateOrder
      of event:timer
        do Value::tmp:CreateOrder
        do Comment::tmp:CreateOrder
      else
        do Value::tmp:CreateOrder
      end
  of lower('FormChargeableParts_tab_' & 3)
    do GenerateTab3
  of lower('FormChargeableParts_tmp:FaultCodesChecked_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCodesChecked
      of event:timer
        do Value::tmp:FaultCodesChecked
        do Comment::tmp:FaultCodesChecked
      else
        do Value::tmp:FaultCodesChecked
      end
  of lower('FormChargeableParts_tmp:FaultCodes1_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCodes1
      of event:timer
        do Value::tmp:FaultCodes1
        do Comment::tmp:FaultCodes1
      else
        do Value::tmp:FaultCodes1
      end
  of lower('FormChargeableParts_tmp:FaultCode2_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode2
      of event:timer
        do Value::tmp:FaultCode2
        do Comment::tmp:FaultCode2
      else
        do Value::tmp:FaultCode2
      end
  of lower('FormChargeableParts_tmp:FaultCode3_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode3
      of event:timer
        do Value::tmp:FaultCode3
        do Comment::tmp:FaultCode3
      else
        do Value::tmp:FaultCode3
      end
  of lower('FormChargeableParts_tmp:FaultCode4_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode4
      of event:timer
        do Value::tmp:FaultCode4
        do Comment::tmp:FaultCode4
      else
        do Value::tmp:FaultCode4
      end
  of lower('FormChargeableParts_tmp:FaultCode5_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode5
      of event:timer
        do Value::tmp:FaultCode5
        do Comment::tmp:FaultCode5
      else
        do Value::tmp:FaultCode5
      end
  of lower('FormChargeableParts_tmp:FaultCode6_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode6
      of event:timer
        do Value::tmp:FaultCode6
        do Comment::tmp:FaultCode6
      else
        do Value::tmp:FaultCode6
      end
  of lower('FormChargeableParts_tmp:FaultCode7_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode7
      of event:timer
        do Value::tmp:FaultCode7
        do Comment::tmp:FaultCode7
      else
        do Value::tmp:FaultCode7
      end
  of lower('FormChargeableParts_tmp:FaultCode8_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode8
      of event:timer
        do Value::tmp:FaultCode8
        do Comment::tmp:FaultCode8
      else
        do Value::tmp:FaultCode8
      end
  of lower('FormChargeableParts_tmp:FaultCode9_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode9
      of event:timer
        do Value::tmp:FaultCode9
        do Comment::tmp:FaultCode9
      else
        do Value::tmp:FaultCode9
      end
  of lower('FormChargeableParts_tmp:FaultCode10_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode10
      of event:timer
        do Value::tmp:FaultCode10
        do Comment::tmp:FaultCode10
      else
        do Value::tmp:FaultCode10
      end
  of lower('FormChargeableParts_tmp:FaultCode11_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode11
      of event:timer
        do Value::tmp:FaultCode11
        do Comment::tmp:FaultCode11
      else
        do Value::tmp:FaultCode11
      end
  of lower('FormChargeableParts_tmp:FaultCode12_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode12
      of event:timer
        do Value::tmp:FaultCode12
        do Comment::tmp:FaultCode12
      else
        do Value::tmp:FaultCode12
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('FormChargeableParts_form:ready_',1)

  p_web.SetSessionValue('FormChargeableParts_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_FormChargeableParts',0)
  par:Fault_Codes_Checked = 'NO'
  p_web.SetSessionValue('par:Fault_Codes_Checked',par:Fault_Codes_Checked)
  par:Credit = 'NO'
  p_web.SetSessionValue('par:Credit',par:Credit)
  par:Requested = 0
  p_web.SetSessionValue('par:Requested',par:Requested)
  par:WebOrder = 0
  p_web.SetSessionValue('par:WebOrder',par:WebOrder)
  par:PartAllocated = 0
  p_web.SetSessionValue('par:PartAllocated',par:PartAllocated)
  par:ExchangeUnit = 0
  p_web.SetSessionValue('par:ExchangeUnit',par:ExchangeUnit)
  par:SecondExchangeUnit = 0
  p_web.SetSessionValue('par:SecondExchangeUnit',par:SecondExchangeUnit)
  par:Correction = 0
  p_web.SetSessionValue('par:Correction',par:Correction)
  par:Ref_Number = p_web.GSV('wob:RefNumber')
  p_web.SetSessionValue('par:Ref_Number',par:Ref_Number)
  par:Quantity = 1
  p_web.SetSessionValue('par:Quantity',par:Quantity)
  par:Exclude_From_Order = 'NO'
  p_web.SetSessionValue('par:Exclude_From_Order',par:Exclude_From_Order)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('FormChargeableParts_form:ready_',1)
  p_web.SetSessionValue('FormChargeableParts_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormChargeableParts',0)
  p_web._PreCopyRecord(PARTS,par:recordnumberkey)
  ! here we need to copy the non-unique fields across
  par:Ref_Number = p_web.GSV('wob:RefNumber')
  p_web.SetSessionValue('par:Ref_Number',p_web.GSV('wob:RefNumber'))
  par:Quantity = 1
  p_web.SetSessionValue('par:Quantity',1)
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('FormChargeableParts_form:ready_',1)
  p_web.SetSessionValue('FormChargeableParts_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('FormChargeableParts:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('FormChargeableParts_form:ready_',1)
  p_web.SetSessionValue('FormChargeableParts_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('FormChargeableParts:Primed',0)
  p_web.setsessionvalue('showtab_FormChargeableParts',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  if (p_web.ifExistsValue('adjustment'))
      p_web.storeValue('adjustment')
  !        p_web.SSV('adjustment',p_web.getValue('adjustment'))
      if (p_web.getValue('adjustment') = 1)
          par:Part_Number = 'ADJUSTMENT'
          par:Description = 'ADJUSTMENT'
          par:Adjustment = 'YES'
          par:Quantity = 1
          par:Warranty_Part = 'NO'
          par:Exclude_From_Order = 'YES'
  
          p_web.SSV('par:Part_Number',par:Part_Number)
          p_web.SSV('par:Description',par:Description)
          p_web.SSV('par:Adjustment',par:Adjustment)
          p_web.SSV('par:Quantity',par:Quantity)
          p_web.SSV('par:Warranty_Part',par:Warranty_Part)
          p_web.SSV('par:Exclude_From_Order',par:Exclude_From_Order)
  
      end ! if (p_web.getValue('adjustment') = 1)
  end !if (p_web.ifExistsValue('adjustment'))
  
  if (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'JOB PART COSTS - EDIT'))
      p_web.SSV('ReadOnly:OutWarrantyMarkup',1)
      p_web.SSV('ReadOnly:PurchaseCost',1)
      p_web.SSV('ReadOnly:OutWarrantyCOst',1)
  else !if (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'JOB PART COSTS - EDIT'))
      p_web.SSV('ReadOnly:OutWarrantyMarkup',0)
      p_web.SSV('ReadOnly:PurchaseCost',0)
      p_web.SSV('ReadOnly:OutWarrantyCOst',0)
  end ! if (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'JOB PART COSTS - EDIT'))
  
  if (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'PART ALLOCATED'))
      p_web.SSV('ReadOnly:PartAllocated',1)
  else ! if (SecurityCheckFailed(p_web.GSV('BookedUser'),'PART ALLOCATED'))
      p_web.SSV('ReadOnly:PartAllocated',0)
  end ! if (SecurityCheckFailed(p_web.GSV('BookedUser'),'PART ALLOCATED'))
  
  if (par:WebOrder = 1)
      p_web.SSV('ReadOnly:Quantity',1)
  end ! if (par:WebOrder = 1)
  p_web.SSV('ReadOnly:ExcludeFromOrder',0)
  IF (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'PARTS - EXCLUDE FROM ORDER'))
      p_web.SSV('ReadOnly:ExcludeFromOrder',1)
  END
  
  if (loc:Act = ChangeRecord)
      if (p_web.GSV('job:Date_Completed') > 0)
          if (~SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'JOBS COSTS - EDIT POST COMPLETE'))
              p_web.SSV('ReadOnly:OutWarrantyCost',1)
              p_web.SSV('ReadOnly:Quantity',1)
          end !if (SecurityCheck('JOBS COSTS - EDIT POST COMPLETE'))
      end ! if (p_web.GSV('job:Date_Completed') > 0)
  
      if (p_web.GSV('job:Estimate') = 'YES')
          Access:ESTPARTS.Clearkey(epr:part_Ref_Number_Key)
          epr:Ref_Number    = p_web.GSV('par:Ref_Number')
          epr:Part_Ref_Number    = p_web.GSV('par:Part_Ref_Number')
          if (Access:ESTPARTS.TryFetch(epr:part_Ref_Number_Key) = Level:Benign)
              ! Found
              ! This is same part as on the estimate
              p_web.SSV('ReadOnly:OutWarrantyCost',1)
              p_web.SSV('ReadOnly:OutWarrantyMarkup',1)
          else ! if (Access:ESTPARTS.TryFetch(epr:part_Ref_Number_Key) = Level:Benign)
              ! Error
          end ! if (Access:ESTPARTS.TryFetch(epr:part_Ref_Number_Key) = Level:Benign)
      end ! if (p_web.GSV('job:Estimate') = 'YES')
      p_web.SSV('ReadOnly:ExcludeFromOrder',1)
  end ! if (loc:Act = ChangeRecord)
  
  do enableDisableCosts
  do showCosts
  do lookupLocation
  do lookupMainFault
  
  
  if (p_web.GSV('Part:ViewOnly') <> 1)
      ! Show unallodate part tick box
  
      if (loc:Act = ChangeRecord)
          if (p_web.GSV('par:Part_Ref_Number') <> '' And |
              p_web.GSV('par:PartAllocated') = 1 And |
                p_web.GSV('par:WebOrder') = 0)
  
              IF (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'UNALLOCATE PART'))
              ELSE
                  p_web.SSV('Show:UnallocatePart',1)
                  p_web.SSV('Comment:UnallocatePart','Return Part To Stock')
              END
          end ! if (p_web.GSV('par:Part_Ref_Number') <> '' And |
      end ! if (loc:Act = ChangeRecord)
  end ! if (p_web.GSV('Part:ViewOnly') <> 1)
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
  If loc:act = ChangeRecord
  End
      If not (1=0)
        If not (p_web.getValue('adjustment') = 1 OR loc:act = ChangeRecord)
          If p_web.IfExistsValue('par:Part_Number')
            par:Part_Number = p_web.GetValue('par:Part_Number')
          End
        End
      End
      If not (1=0)
        If not (p_web.getValue('adjustment') = 1 OR loc:act = ChangeRecord)
          If p_web.IfExistsValue('par:Description')
            par:Description = p_web.GetValue('par:Description')
          End
        End
      End
      If not (1=0)
          If p_web.IfExistsValue('par:Despatch_Note_Number')
            par:Despatch_Note_Number = p_web.GetValue('par:Despatch_Note_Number')
          End
      End
      If not (1=0)
        If not (p_web.getValue('adjustment') = 1 OR p_web.GSV('ReadOnly:Quantity') = 1 or loc:act = ChangeRecord)
          If p_web.IfExistsValue('par:Quantity')
            par:Quantity = p_web.GetValue('par:Quantity')
          End
        End
      End
      If not (1=0)
        If not (p_web.getValue('adjustment') = 1 or p_web.GSV('ReadOnly:PurchaseCost') = 1 OR loc:act = ChangeRecord)
          If p_web.IfExistsValue('tmp:PurchaseCost')
            tmp:PurchaseCost = p_web.GetValue('tmp:PurchaseCost')
          End
        End
      End
      If not (1=0)
        If not (p_web.GSV('ReadOnly:OutWarrantyCost') = 1 Or p_web.GSV('tmp:OutWarrantyMarkup') > 0)
          If p_web.IfExistsValue('tmp:OutWarrantyCost')
            tmp:OutWarrantyCost = p_web.GetValue('tmp:OutWarrantyCost')
          End
        End
      End
      If not (1=0)
        If not (p_web.GSV('ReadOnly:OutWarrantyMarkup') = 1)
          If p_web.IfExistsValue('tmp:OutWarrantyMarkup')
            tmp:OutWarrantyMarkup = p_web.GetValue('tmp:OutWarrantyMarkup')
          End
        End
      End
      If not (1=0)
        If not (loc:act = ChangeRecord)
          If p_web.IfExistsValue('par:Supplier')
            par:Supplier = p_web.GetValue('par:Supplier')
          End
        End
      End
      If not (1=0)
        If not (p_web.GSV('ReadOnly:ExcludeFromOrder') = 1)
          If p_web.IfExistsValue('par:Exclude_From_Order')
            par:Exclude_From_Order = p_web.GetValue('par:Exclude_From_Order')
          End
        End
      End
      If not (1=0)
        If not (p_web.GSV('ReadOnly:PartAllocated') = 1)
          If p_web.IfExistsValue('par:PartAllocated')
            par:PartAllocated = p_web.GetValue('par:PartAllocated')
          End
        End
      End
    If (p_web.GSV('Show:UnallocatePart') = 1)
      If not (1=0)
          If p_web.IfExistsValue('tmp:UnallocatePart') = 0
            p_web.SetValue('tmp:UnallocatePart',0)
            tmp:UnallocatePart = 0
          Else
            tmp:UnallocatePart = p_web.GetValue('tmp:UnallocatePart')
          End
      End
    End
  If p_web.GSV('locOrderRequired') = 1
      If not (1=0)
          If p_web.IfExistsValue('tmp:CreateOrder') = 0
            p_web.SetValue('tmp:CreateOrder',0)
            tmp:CreateOrder = 0
          Else
            tmp:CreateOrder = p_web.GetValue('tmp:CreateOrder')
          End
      End
  End
    If (0)
      If not (p_web.GSV('Hide:FaultCodesChecked') = 1)
          If p_web.IfExistsValue('tmp:FaultCodesChecked') = 0
            p_web.SetValue('tmp:FaultCodesChecked',0)
            tmp:FaultCodesChecked = 0
          Else
            tmp:FaultCodesChecked = p_web.GetValue('tmp:FaultCodesChecked')
          End
      End
    End
    If (p_web.GSV('Hide:PartFaultCode1') <> 1)
      If not (1=0)
        If not (p_web.GSV('ReadOnly:PartFaultCode1') = 1)
          If p_web.IfExistsValue('tmp:FaultCodes1')
            tmp:FaultCodes1 = p_web.GetValue('tmp:FaultCodes1')
          End
        End
      End
    End
    If (p_web.GSV('Hide:PartFaultCode2') <> 1)
      If not (1=0)
        If not (p_web.GSV('ReadOnly:PartFaultCode2') = 1)
          If p_web.IfExistsValue('tmp:FaultCodes2')
            tmp:FaultCodes2 = p_web.GetValue('tmp:FaultCodes2')
          End
        End
      End
    End
    If (p_web.GSV('Hide:PartFaultCode3') <> 1)
      If not (1=0)
        If not (p_web.GSV('ReadOnly:PartFaultCode3') = 1)
          If p_web.IfExistsValue('tmp:FaultCodes3')
            tmp:FaultCodes3 = p_web.GetValue('tmp:FaultCodes3')
          End
        End
      End
    End
    If (p_web.GSV('Hide:PartFaultCode4') <> 1)
      If not (1=0)
        If not (p_web.GSV('ReadOnly:PartFaultCode4') = 1)
          If p_web.IfExistsValue('tmp:FaultCodes4')
            tmp:FaultCodes4 = p_web.GetValue('tmp:FaultCodes4')
          End
        End
      End
    End
    If (p_web.GSV('Hide:PartFaultCode5') <> 1)
      If not (1=0)
        If not (p_web.GSV('ReadOnly:PartFaultCode5') = 1)
          If p_web.IfExistsValue('tmp:FaultCodes5')
            tmp:FaultCodes5 = p_web.GetValue('tmp:FaultCodes5')
          End
        End
      End
    End
    If (p_web.GSV('Hide:PartFaultCode6') <> 1)
      If not (1=0)
        If not (p_web.GSV('ReadOnly:PartFaultCode6') = 1)
          If p_web.IfExistsValue('tmp:FaultCodes6')
            tmp:FaultCodes6 = p_web.GetValue('tmp:FaultCodes6')
          End
        End
      End
    End
    If (p_web.GSV('Hide:PartFaultCode7') <> 1)
      If not (1=0)
        If not (p_web.GSV('ReadOnly:PartFaultCode7') = 1)
          If p_web.IfExistsValue('tmp:FaultCodes7')
            tmp:FaultCodes7 = p_web.GetValue('tmp:FaultCodes7')
          End
        End
      End
    End
    If (p_web.GSV('Hide:PartFaultCode8') <> 1)
      If not (1=0)
        If not (p_web.GSV('ReadOnly:PartFaultCode8') = 1)
          If p_web.IfExistsValue('tmp:FaultCodes8')
            tmp:FaultCodes8 = p_web.GetValue('tmp:FaultCodes8')
          End
        End
      End
    End
    If (p_web.GSV('Hide:PartFaultCode9') <> 1)
      If not (1=0)
        If not (p_web.GSV('ReadOnly:PartFaultCode9') = 1)
          If p_web.IfExistsValue('tmp:FaultCodes9')
            tmp:FaultCodes9 = p_web.GetValue('tmp:FaultCodes9')
          End
        End
      End
    End
    If (p_web.GSV('Hide:PartFaultCode10') <> 1)
      If not (1=0)
        If not (p_web.GSV('ReadOnly:PartFaultCode10') = 1)
          If p_web.IfExistsValue('tmp:FaultCodes10')
            tmp:FaultCodes10 = p_web.GetValue('tmp:FaultCodes10')
          End
        End
      End
    End
    If (p_web.GSV('Hide:PartFaultCode11') <> 1)
      If not (1=0)
        If not (p_web.GSV('ReadOnly:PartFaultCode11') = 1)
          If p_web.IfExistsValue('tmp:FaultCodes11')
            tmp:FaultCodes11 = p_web.GetValue('tmp:FaultCodes11')
          End
        End
      End
    End
    If (p_web.GSV('Hide:PartFaultCode12') <> 1)
      If not (1=0)
        If not (p_web.GSV('ReadOnly:PartFaultCode12') = 1)
          If p_web.IfExistsValue('tmp:FaultCodes12')
            tmp:FaultCodes12 = p_web.GetValue('tmp:FaultCodes12')
          End
        End
      End
    End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord
  ! Insert Record Validation
  IF (loc:Invalid <> '')
      EXIT
  END
  
  
  p_web.SSV('AddToStockAllocation:Type','')
  par:PartAllocated = 1
  
  Access:DEFAULTS.Clearkey(def:RecordNumberKey)
  def:Record_Number    = 1
  set(def:RecordNumberKey,def:RecordNumberKey)
  loop
      if (Access:DEFAULTS.Next())
          Break
      end ! if (Access:DEFAULTS.Next())
      break
  end ! loop
  
  stockPart# = 0
  if (par:part_Ref_Number <> '')
      Access:STOCK.Clearkey(sto:Ref_Number_Key)
      sto:Ref_Number = par:part_Ref_Number
      if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
          ! Found
          ! Check for duplicate?
          stockPart# = 1
          found# = 0
          if (sto:AllowDuplicate = 0)
  
              Access:PARTS_ALIAS.Clearkey(par_ali:Part_Number_Key)
              par_ali:Ref_Number    = p_web.GSV('job:Ref_Number')
              par_Ali:Part_Number = par:part_Number
              set(par_ali:Part_Number_Key,par_ali:Part_Number_Key)
              loop
                  if (Access:PARTS_ALIAS.Next())
                      Break
                  end ! if (Access:PARTS.Next())
                  if (par_ali:Ref_Number    <> p_web.GSV('job:Ref_Number'))
                      Break
                  end ! if (par:Ref_Number    <> p_web.GSV('job:Ref_Number'))
                  if (par_ali:part_Number <> par:Part_Number)
                      Break
                  end ! if (par:Part_Number    <> p_web.GSV('par:Part_Number'))
                  if (par_ali:Date_Received = '')
                      found# = 1
                      break
                  end ! if (par:Date_Received = '')
              end ! loop
          end ! if (sto:AllowDuplicate = 0)
  
          if (found# = 1)
              loc:Invalid = 'par:Part_Number'
              loc:Alert = 'This part is already attached to this job.'
              exit
          end ! if (par:Date_Received = '')
      else ! if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
          ! Error
      end ! if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
  else ! if (p_web.GSV('par:Part_Ref_Number') <> '')
  end ! if (p_web.GSV('par:Part_Ref_Number') <> '')
  
  ! update stock details
  p_web.SSV('locOrderRequired',0)
  if (par:part_Number <> 'ADJUSTMENT')
      if (par:exclude_From_Order <> 'YES')
          if (stockPart# = 1)
              if (sto:Sundry_Item <> 'YES')
                  if (par:Quantity > sto:Quantity_Stock)
                      if (p_web.GSV('locOrderRequired') <> 1 and p_web.GSV('tmp:CreateOrder') = 0)
                          p_web.SSV('locOrderRequired',1)
                          loc:Invalid = 'tmp:OrderPart'
                          loc:Alert = 'Insufficient Items In Stock'
                          p_web.SSV('text:OrderRequired','Qty Required: ' & p_web.GSV('par:Quantity') & ', Qty In Stock: ' & sto:Quantity_Stock)
                      else ! if (p_web.GSV('locOrderRequired') <> 1)
                          ! Have selected to create an order
                          if (virtualSite(sto:Location))
                              createWebOrder(p_web.GSV('BookingAccount'),par:Part_Number,|
                                  par:Description,(par:Quantity - sto:Quantity_Stock),par:Retail_Cost)
  
                              if (sto:Quantity_Stock > 0)
                                  ! New Part
                                  stockQty# = sto:Quantity_Stock
                                  sto:Quantity_Stock = 0
                                  if (access:STOCK.tryUpdate() = level:benign)
                                      rtn# = AddToStockHistory(sto:Ref_Number, |
                                          'DEC', |
                                          par:Despatch_Note_Number, |
                                          p_web.GSV('job:Ref_Number'), |
                                          0, |
                                          sto:Quantity_Stock, |
                                          p_web.GSV('tmp:InWarrantyCost'), |
                                          p_web.GSV('tmp:OutWarrantyCost'), |
                                          par:Retail_Cost, |
                                          'STOCK DECREMENTED', |
                                          '', |
                                          p_web.GSV('BookingUserCode'), |
                                          sto:Quantity_Stock)
  
                                      if (Access:PARTS_ALIAS.PrimeRecord() = Level:Benign)
                                          recNo# = par_ali:Record_Number
                                          par_ali:Record :=: par:Record
                                          par_ali:Record_Number = recNo#
                                          par:Quantity = par:Quantity - stockQty#
                                          par:WebOrder = 1
                                          if (Access:PARTS_ALIAS.TryInsert() = Level:Benign)
                                              ! Inserted
                                              p_web.SSV('AddToStockAllocation:Type','CHA')
                                              p_web.SSV('AddToStockAllocation:Status','WEB')
                                              p_web.SSV('AddToStockAllocation:Qty',par:Quantity)
  
                                          else ! if (Access:PARTS_ALIAS.TryInsert() = Level:Benign)
                                              ! Error
                                              Access:PARTS_ALIAS.CancelAutoInc()
                                          end ! if (Access:PARTS_ALIAS.TryInsert() = Level:Benign)
                                      end ! if (Access:PARTS_ALIAS.PrimeRecord() = Level:Benign)
                                      par:Quantity = stockQty#
                                      par:Date_Ordered = Today()
                                  end ! if (access:STOCK.tryUpdate() = level:benign)
                              else ! if (sto:Quantity_Stock > 0)
                                  par:WebOrder = 1
                                  par:PartAllocated = 0
                                  p_web.SSV('AddToStockAllocation:Type','CHA')
                                  p_web.SSV('AddToStockAllocation:Status','WEB')
                                  p_web.SSV('AddToStockAllocation:Qty',par:Quantity)
  
                              end ! if (sto:Quantity_Stock > 0)
                          else ! if (virtualSite(sto:Location))
                              ! this doesn't apply to vodacom
                          end ! if (virtualSite(sto:Location))
                      end ! if (p_web.GSV('locOrderRequired') <> 1)
                  else ! if (par:Quantity > sto:Quantity_Stock)
                      par:date_Ordered = Today()
                      if rapidLocation(sto:Location)
                          par:PartAllocated = 0
                          p_web.SSV('AddToStockAllocation:Type','CHA')
                          p_web.SSV('AddToStockAllocation:Status','')
                          p_web.SSV('AddToStockAllocation:Qty',par:Quantity)
  
                      else
                          par:PartAllocated = 1 ! #11704 Allocate the part if not using Rapid Stock Allocation (Bryan: 01/10/2010)
                      end ! if rapidLocation(sto:Location)
  
                      sto:quantity_Stock -= par:Quantity
                      if (sto:quantity_Stock < 0)
                          sto:quantity_Stock = 0
                      end ! if rapidLocation(sto:Location)
                      if (access:STOCK.tryUpdate() = level:Benign)
                          rtn# = AddToStockHistory(sto:Ref_Number, |
                              'DEC', |
                              par:Despatch_Note_Number, |
                              p_web.GSV('job:Ref_Number'), |
                              0, |
                              sto:Quantity_Stock, |
                              p_web.GSV('tmp:InWarrantyCost'), |
                              p_web.GSV('tmp:OutWarrantyCost'), |
                              par:Retail_Cost, |
                              'STOCK DECREMENTED', |
                              '', |
                              p_web.GSV('BookingUserCode'), |
                              sto:Quantity_Stock)
                      end ! if (access:STOCK.tryUpdate() = level:Benign)
                  end ! if (par:Quantity > sto:Quantity_Stock)
              else ! if (sto:Sundry_Item <> 'YES')
                  par:date_Ordered = Today()
              end ! if (sto:Sundry_Item <> 'YES')
          else ! if (stockPart# = 1)
  
          end ! if (stockPart# = 1)
      else ! if (par:exclude_From_Order <> 'YES')
          par:date_Ordered = Today()
      end ! if (par:exclude_From_Order <> 'YES')
  else ! if (par:part_Number <> 'ADJUSTMENT')
      par:date_Ordered = Today()
  end !if (par:part_Number <> 'ADJUSTMENT')

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  ! Change Record Validation
  IF (p_web.GSV('Show:UnallocatePart') = 1 AND p_web.GSV('tmp:UnAllocatePart') = 1)
      ! Unallocate Part
      Access:STOCK.ClearKey(sto:Ref_Number_Key)
      sto:Ref_Number = par:Part_Ref_Number
      IF (Access:STOCK.TryFetch(sto:Ref_Number_Key))
      ELSE
          Pointer# = Pointer(STOCK)
          Hold(STOCK,1)
          Get(STOCK,Pointer#)
          If Errorcode() = 43
              loc:Invalid = 'tmp:UnallocatePart'
              loc:Alert = 'Cannot unallocate part, the stock item is in use.'
              EXIT
          End !If Errorcode() = 43
          sto:Quantity_Stock += par:Quantity
          If Access:STOCK.Update() = Level:Benign
              If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                  'REC', | ! Transaction_Type
                  par:Despatch_Note_Number, | ! Depatch_Note_Number
                  job:Ref_Number, | ! Job_Number
                  0, | ! Sales_Number
                  par:Quantity, | ! Quantity
                  par:Purchase_Cost, | ! Purchase_Cost
                  par:Sale_Cost, | ! Sale_Cost
                  par:Retail_Cost, | ! Retail_Cost
                  'STOCK UNALLOCATED', | ! Notes
                  '', |
                  p_web.GSV('BookingUserCode'), |
                  sto:Quantity_Stock) ! Information
                  ! Added OK
  
              Else ! AddToStockHistory
                  ! Error
              End ! AddToStockHistory
              par:Status = 'WEB'
              par:PartAllocated = 0
              par:WebOrder = 1
          End !If Access:STOCK.Update() = Level:Benign
      END
  
  END
  
  do CompleteForm
  do ValidateRecord
  ! Change Record Validation

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormChargeableParts_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormChargeableParts_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 4
  If loc:act = ChangeRecord
    loc:InvalidTab += 1
    do ValidateValue::par:Order_Number
    If loc:Invalid then exit.
    do ValidateValue::par:Date_Ordered
    If loc:Invalid then exit.
    do ValidateValue::par:Date_Received
    If loc:Invalid then exit.
    do ValidateValue::locOutFaultCode
    If loc:Invalid then exit.
  End
  ! tab = 1
    loc:InvalidTab += 1
    do ValidateValue::par:Part_Number
    If loc:Invalid then exit.
    do ValidateValue::par:Description
    If loc:Invalid then exit.
    do ValidateValue::par:Despatch_Note_Number
    If loc:Invalid then exit.
    do ValidateValue::par:Quantity
    If loc:Invalid then exit.
    do ValidateValue::tmp:Location
    If loc:Invalid then exit.
    do ValidateValue::tmp:SecondLocation
    If loc:Invalid then exit.
    do ValidateValue::tmp:ShelfLocation
    If loc:Invalid then exit.
    do ValidateValue::tmp:PurchaseCost
    If loc:Invalid then exit.
    do ValidateValue::tmp:OutWarrantyCost
    If loc:Invalid then exit.
    do ValidateValue::tmp:OutWarrantyMarkup
    If loc:Invalid then exit.
        if (p_web.GSV('tmp:OutWarrantyMarkup') > GETINI('STOCK','MaxPartMarkup',,CLIP(PATH())&'\SB2KDEF.INI'))
            loc:Invalid = 'tmp:OutWarrantyMarkup'
            loc:Alert = 'You cannot mark-up parts more than ' & GETINI('STOCK','MaxPartMarkup',,CLIP(PATH())&'\SB2KDEF.INI') & '%.'
        end !if p_web.GSV('tmp:OutWarrantyMarkup') > GETINI('STOCK','MaxPartMarkup',,CLIP(PATH())&'\SB2KDEF.INI')
    do ValidateValue::tmp:FixedPrice
    If loc:Invalid then exit.
    do ValidateValue::par:Supplier
    If loc:Invalid then exit.
    do ValidateValue::par:Exclude_From_Order
    If loc:Invalid then exit.
    do ValidateValue::par:PartAllocated
    If loc:Invalid then exit.
    do ValidateValue::tmp:UnallocatePart
    If loc:Invalid then exit.
  ! tab = 3
  If p_web.GSV('locOrderRequired') = 1
    loc:InvalidTab += 1
    do ValidateValue::text:OrderRequired
    If loc:Invalid then exit.
    do ValidateValue::text:OrderRequired2
    If loc:Invalid then exit.
    do ValidateValue::tmp:CreateOrder
    If loc:Invalid then exit.
  End
  ! tab = 2
    loc:InvalidTab += 1
    do ValidateValue::tmp:FaultCodesChecked
    If loc:Invalid then exit.
    do ValidateValue::tmp:FaultCodes1
    If loc:Invalid then exit.
    do ValidateValue::tmp:FaultCode2
    If loc:Invalid then exit.
    do ValidateValue::tmp:FaultCode3
    If loc:Invalid then exit.
    do ValidateValue::tmp:FaultCode4
    If loc:Invalid then exit.
    do ValidateValue::tmp:FaultCode5
    If loc:Invalid then exit.
    do ValidateValue::tmp:FaultCode6
    If loc:Invalid then exit.
    do ValidateValue::tmp:FaultCode7
    If loc:Invalid then exit.
    do ValidateValue::tmp:FaultCode8
    If loc:Invalid then exit.
    do ValidateValue::tmp:FaultCode9
    If loc:Invalid then exit.
    do ValidateValue::tmp:FaultCode10
    If loc:Invalid then exit.
    do ValidateValue::tmp:FaultCode11
    If loc:Invalid then exit.
    do ValidateValue::tmp:FaultCode12
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
  ! Write Fields
  
  
      !Write Fault Codes
      Access:MANFAUPA.Clearkey(map:ScreenOrderKey)
      map:Manufacturer    = p_web.GSV('job:Manufacturer')
      map:ScreenOrder    = 0
      set(map:ScreenOrderKey,map:ScreenOrderKey)
      loop
          if (Access:MANFAUPA.Next())
              Break
          end ! if (Access:MANFAUPA.Next())
          if (map:Manufacturer    <> p_web.GSV('job:Manufacturer'))
              Break
          end ! if (map:Manufacturer    <> p_web.GSV('job:Manufacturer'))
          if (map:ScreenOrder    = 0)
              cycle
          end ! if (map:ScreenOrder    <> 0)
  
          p_web.SSV('par:Fault_Code' & map:Field_Number,p_web.GSV('tmp:FaultCodes' & map:ScreenOrder))
      end ! loop
  
  !    loop x# = 1 To 12
  !        p_web.SSV('par:Fault_Code' & x#,p_web.GSV('tmp:FaultCodes' & x#))
  !        linePrint('par:Fault_Code' & x# & ' - ' & p_web.GSV('tmp:FaultCodes' & x#),'c:\log.log')
  !    end ! loop x# = 1 To 12
  
      par:Fault_Code1 = p_web.GSV('par:Fault_Code1')
      par:Fault_Code2 = p_web.GSV('par:Fault_Code2')
      par:Fault_Code3 = p_web.GSV('par:Fault_Code3')
      par:Fault_Code4 = p_web.GSV('par:Fault_Code4')
      par:Fault_Code5 = p_web.GSV('par:Fault_Code5')
      par:Fault_Code6 = p_web.GSV('par:Fault_Code6')
      par:Fault_Code7 = p_web.GSV('par:Fault_Code7')
      par:Fault_Code8 = p_web.GSV('par:Fault_Code8')
      par:Fault_Code9 = p_web.GSV('par:Fault_Code9')
      par:Fault_Code10 = p_web.GSV('par:Fault_Code10')
      par:Fault_Code11 = p_web.GSV('par:Fault_Code11')
      par:Fault_Code12 = p_web.GSV('par:Fault_Code12')
  
      If p_web.GSV('tmp:ARCPart') = 1
          par:AveragePurchaseCost = p_web.GSV('tmp:PurchaseCost')
          par:Purchase_Cost       = p_web.GSV('tmp:InWarrantyCost')
          par:Sale_Cost           = p_web.GSV('tmp:OutWarrantyCost')
          par:InWarrantyMarkup    = p_web.GSV('tmp:InWarrantyMarkup')
          par:OutWarrantyMarkup   = p_web.GSV('tmp:OutWarrantyMarkup')
      Else !If tmp:ARCPart
          par:RRCAveragePurchaseCost  = p_web.GSV('tmp:PurchaseCost')
          par:RRCPurchaseCost     = p_web.GSV('tmp:InWarrantyCost')
          par:RRCSaleCost         = p_web.GSV('tmp:OutWarrantyCost')
          par:RRCInWarrantyMarkup = p_web.GSV('tmp:InWarrantyMarkup')
          par:RRCOutWarrantyMarkup   = p_web.GSV('tmp:OutWarrantyMarkup')
          par:Purchase_Cost       = par:RRCPurchaseCost
          par:Sale_Cost           = par:RRCSaleCost
      End !If tmp:ARCPart
  
  
  
! NET:WEB:StagePOST
PostInsert      Routine
  IF (p_web.GSV('AddToStockAllocation:Type') <> '')
      AddToStockAllocation(p_web.GSV('par:Record_Number'),'CHA',p_web.GSV('par:Quantity'),p_web.GSV('AddToStockAllocation:Status'),p_web.GSV('job:Engineer'),p_web)
  END
  do deleteSessionValues
PostCopy        Routine
  p_web.SetSessionValue('FormChargeableParts:Primed',0)
PostUpdate      Routine
  do deleteSessionValues
  p_web.SetSessionValue('FormChargeableParts:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('tmp:Location')
  p_web.StoreValue('tmp:SecondLocation')
  p_web.StoreValue('tmp:ShelfLocation')
  p_web.StoreValue('tmp:PurchaseCost')
  p_web.StoreValue('tmp:OutWarrantyCost')
  p_web.StoreValue('tmp:OutWarrantyMarkup')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:UnallocatePart')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:CreateOrder')
  p_web.StoreValue('tmp:FaultCodesChecked')
  p_web.StoreValue('tmp:FaultCodes1')
  p_web.StoreValue('tmp:FaultCodes2')
  p_web.StoreValue('tmp:FaultCodes3')
  p_web.StoreValue('tmp:FaultCodes4')
  p_web.StoreValue('tmp:FaultCodes5')
  p_web.StoreValue('tmp:FaultCodes6')
  p_web.StoreValue('tmp:FaultCodes7')
  p_web.StoreValue('tmp:FaultCodes8')
  p_web.StoreValue('tmp:FaultCodes9')
  p_web.StoreValue('tmp:FaultCodes10')
  p_web.StoreValue('tmp:FaultCodes11')
  p_web.StoreValue('tmp:FaultCodes12')


PostDelete      Routine
local.AfterFaultCodeLookup        Procedure(Long fNumber)
code
    p_web.setsessionvalue('showtab_FormChargeableParts',Loc:TabNumber)
    if loc:LookupDone

!        Access:MANFAUPA.Clearkey(map:ScreenOrderKey)
!        map:ScreenOrder    = fNumber
!        map:Manufacturer   = p_web.GSV('job:Manufacturer')
!        if (Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign)
!            ! Found
!        else ! if (Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign)
!            ! Error
!        end ! if (Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign)
!
!        if (map:MainFault)
!            p_web.FileToSessionQueue(MANFAULO)
!            p_web.SSV('Comment:PartFaultCode' & fNumber,mfo:Description)
!
!        else ! if (map:MainFault)
!            p_web.FileToSessionQueue(MANFPALO)
!            p_web.SSV('Comment:PartFaultCode' & fNumber,mfp:Description)
!        end ! if (map:MainFault)
        do buildFaultCodes
        do UpdateComments
    end
    p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:FaultCodes' & fNumber)
local.SetLookupButton      Procedure(Long fNumber)
locUseRelatedPart           String(30)
Code
    if (p_web.GSV('ShowDate:PartFaultCode' & fNumber) = 1)
        packet = clip(packet) & '<button id="lookup_btn" class="LookupButton" onclick="displayCalendar(tmp__FaultCode' & fNumber & ',''dd/mm/yyyy'',this); ' & |
                  'Date.disabled=false;sv(''...'',''FormChargeableParts_pickdate_value'',1,FieldValue(this,1));nextFocus(FormChargeableParts_frm,'''',0);"' & |
                  'value="Select Date" name="Date" type="button">...</button>'
        do SendPacket
    end ! if (p_web.GSV('ShowDate:PartFaultCode1') = 1)
    if (p_web.GSV('Lookup:PartFaultCode' & fNumber) = 1)

        Access:MANFAUPA.Clearkey(map:ScreenOrderKey)
        map:ScreenOrder    = fNumber
        map:Manufacturer    = p_web.GSV('job:Manufacturer')
        if (Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign)
            ! Found
        else ! if (Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign)
            ! Error
        end ! if (Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign)
        if (map:UseRelatedJobCode <> 0)
            locUseRelatedPart = 'relatedPartCode=' & map:Field_Number
        else
            locUseRelatedPart = ''
        end ! if (map:UseRelatedJobCode <> 0)

        if (map:MainFault)

            Access:MANFAULT.Clearkey(maf:MainFaultKey)
            maf:Manufacturer    = p_web.GSV('job:Manufacturer')
            maf:MainFault    = 1
            if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)
                ! Found
            else ! if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)
                ! Error
            end ! if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)
            packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('BrowseJobFaultCodeLookup')&|
                        '?LookupField=tmp:FaultCodes' & fNumber & '&Tab=2&ForeignField=mfo:Field&_sort=mfo:Field&Refresh=' & |
                        'sort&LookupFrom=FormChargeableParts&' & |
                        'fieldNumber=' & maf:Field_Number & '&partType=C&partMainFault=1&' & clip(locUseRelatedPart)),) !lookupextra

        else ! if (map:MainFault)
            found# = 0
            Access:STOCK.Clearkey(sto:Ref_Number_Key)
            sto:Ref_Number    = p_web.GSV('par:Part_Ref_Number')
            if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
                ! Found
                ! Check if any of the fault codes have been restricted by the Stock Part (DBH: 29/10/2007)
                If sto:Assign_Fault_Codes = 'YES'
                    Access:STOMODEL.Clearkey(stm:Model_Number_Key)
                    stm:Ref_Number = sto:Ref_Number
                    stm:Manufacturer = p_web.GSV('job:Manufacturer')
                    stm:Model_Number = p_web.GSV('job:Model_Number')
                    If Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign

                        Case map:Field_Number
                        Of 1
                            If stm:FaultCode1 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        Of 2
                            If stm:FaultCode2 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        Of 3
                            If stm:FaultCode3 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        Of 4
                            If stm:FaultCode4 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        Of 5
                            If stm:FaultCode5 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        Of 6
                            If stm:FaultCode6 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        Of 7
                            If stm:FaultCode7 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        Of 8
                            If stm:FaultCode8 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        Of 9
                            If stm:FaultCode9 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        Of 10
                            If stm:FaultCode10 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        Of 11
                            If stm:FaultCode11 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        Of 12
                            If stm:FaultCode12 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        End ! Case map:Field_Number
                        If Found# = 1
                            packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('BrowseStockPartFaultCodeLookup')&|
                                        '?LookupField=tmp:FaultCodes' & fNumber & '&Tab=2&ForeignField=stu:Field&_sort=stu:Field&Refresh=' & |
                                        'sort&LookupFrom=FormChargeableParts&' & |
                                        'fieldNumber=' & map:Field_Number & '&stockRefNumber=' & stm:RecordNumber),) !lookupextra

                        End ! If Found# = 1
                    End ! If Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign
                End ! If sto:Assign_Fault_Codes = 'YES'
            else ! if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
                ! Error
            end ! if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)

            if (found# = 0)
                packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('BrowsePartFaultCodeLookup')&|
                            '?LookupField=tmp:FaultCodes' & fNumber & '&Tab=2&ForeignField=mfp:Field&_sort=mfp:Field&Refresh=' & |
                            'sort&LookupFrom=FormChargeableParts&' & |
                            'fieldNumber=' & map:Field_Number & '&partType=C&'),) !lookupextra
            end ! if (found# = 0)
        end ! if (map:MainFault)
        do sendPacket
    end !if (p_web.GSV('Lookup:PartFaultCode1') = 1)
DisplayBrowsePayments PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
FilesOpened     Long
WEBJOB::State  USHORT
INVOICE::State  USHORT
JOBSE2::State  USHORT
TRADEACC::State  USHORT
SUBTRACC::State  USHORT
JOBSE::State  USHORT
JOBS::State  USHORT
BrowsePayments:IsInvalid  Long
buttonViewCosts:IsInvalid  Long
buttonIssueRefund:IsInvalid  Long
buttonCreateCreditNote:IsInvalid  Long
buttonCreateInvoice:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
    MAP
showHideCreateInvoice       PROCEDURE(),BYTE
    END
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('DisplayBrowsePayments')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'DisplayBrowsePayments_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('DisplayBrowsePayments','')
    p_web.DivHeader('DisplayBrowsePayments',p_web.combine(p_web.site.style.formdiv,))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('DisplayBrowsePayments') = 0
        p_web.AddPreCall('DisplayBrowsePayments')
        p_web.DivHeader('popup_DisplayBrowsePayments','nt-hidden')
        p_web.DivHeader('DisplayBrowsePayments',p_web.combine(p_web.site.style.formdiv,))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_DisplayBrowsePayments_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_DisplayBrowsePayments_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferDisplayBrowsePayments',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_DisplayBrowsePayments',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferDisplayBrowsePayments',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_DisplayBrowsePayments',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferDisplayBrowsePayments',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_DisplayBrowsePayments',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_DisplayBrowsePayments',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferDisplayBrowsePayments',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_DisplayBrowsePayments',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferDisplayBrowsePayments',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_DisplayBrowsePayments',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_DisplayBrowsePayments',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('DisplayBrowsePayments')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
DeleteSessionvalues  ROUTINE
    p_web.DeleteSessionValue('IgnoreMessage')
    p_web.DeleteSessionValue('Job:ViewOnly')
    p_web.DeleteSessionValue('DisplayBrowsePaymentsReturnURL')
OpenFiles  ROUTINE
  p_web._OpenFile(WEBJOB)
  p_web._OpenFile(INVOICE)
  p_web._OpenFile(JOBSE2)
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(SUBTRACC)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(JOBS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(INVOICE)
  p_Web._CloseFile(JOBSE2)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(JOBS)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE
  loc:EipClm = upper(p_web.GetValue('_EIPClm'))
  p_web.DeleteValue('_EIPClm')
  case loc:EipClm
  of ''
    case upper(p_web.GetValue('_calledfrom_'))
    of upper('BrowsePayments')
      do Value::BrowsePayments
    end
  end

InitForm       Routine
  DATA
LF  &FILE
  CODE
  ! Return URL
  IF (p_web.IfExistsValue('DisplayBrowsePaymentsReturnURL'))
      p_web.StoreValue('DisplayBrowsePaymentsReturnURL')
  END
  ! Open Files
  ! Assume JOBS is in a session queue
  
  Access:JOBSE.ClearKey(jobe:RefNumberKey)
  jobe:RefNumber = p_web.GSV('job:Ref_Number')
  IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
      p_web.FileToSessionQueue(JOBSE)
  END
  
  Access:JOBSE2.ClearKey(jobe2:RefNumberKey)
  jobe2:RefNumber = p_web.GSV('job:Ref_Number')
  IF (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
      p_web.FileToSessionQueue(JOBSE2)
  END
  
  Access:WEBJOB.ClearKey(wob:RefNumberKey)
  wob:RefNumber = p_web.GSV('job:Ref_Number')
  IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
      p_web.FileToSessionQueue(WEBJOB)
  END
  
  
  IF (p_web.IfExistsValue('IgnoreMessage'))
      p_web.StoreValue('IgnoreMessage')
  END
  
  ! Force the cost screen to be view only
  p_web.SSV('Job:ViewOnly',1)
  
  
  p_web.SetValue('DisplayBrowsePayments_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'DisplayBrowsePayments'
    end
    p_web.formsettings.proc = 'DisplayBrowsePayments'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('DisplayBrowsePayments_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.GSV('DisplayBrowsePaymentsReturnURL')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('DisplayBrowsePayments_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('DisplayBrowsePayments_ChainTo')
    loc:formaction = p_web.GetSessionValue('DisplayBrowsePayments_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = p_web.GSV('DisplayBrowsePaymentsReturnURL')

GenerateForm   Routine
  do LoadRelatedRecords
  p_web.site.SaveButton.TextValue = 'Close'
  
  IF (p_web.GSV('job:Date_Completed') = 0)
      IF (p_web.GSV('IgnoreMessage') <> 1)
          p_web.SSV('Message:Text','Warning! This job has not been completed!\n\nClick ''OK'' To Continue.')
          p_web.SSV('Message:PassURL','DisplayBrowsePayments?IgnoreMessage=1')
          p_web.SSV('Message:FailURL',p_web.GSV('DisplayBrowsePaymentsReturnURL'))
          MessageQuestion(p_web)
          Exit
      END
  END
  
  p_web.SSV('Hide:CreateCreditNoteButton',1)
  
  
  IF (p_web.GSV('BookingSite') = 'RRC')
      ! Only credit if job has been invoiced
      IF (p_web.GSV('job:Invoice_Number') = 0)
      ELSE
          Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
          inv:Invoice_Number = p_web.GSV('job:Invoice_Number')
          IF (Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign)
              IF (inv:RRCInvoiceDate = 0)
              ELSE
                  creditAllowed# = 1
                  Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
                  sub:Account_Number = p_web.GSV('job:Account_Number')
                  IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
                      IF (sub:Generic_Account <> 1)
                          Access:TRADEACC.clearkey(tra:Account_Number_Key)
                          tra:Account_Number = p_web.GSV('BookingAccount')
                          IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
                              IF (tra:CompanyOwned = 1)
                                  creditAllowed# = 0
                              END
                          END
                      ELSE
                          creditAllowed# = 0
                      END
  
                  END
  
                  IF (creditAllowed# = 1)
                      Access:TRADEACC.ClearKey(tra:Account_Number_Key)
                      tra:Account_Number = p_web.GSV('BookingAccount')
                      IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
                          IF (tra:AllowCreditNotes)
                              p_web.SSV('Hide:CreateCreditNoteButton',0)
                          END
                      END
                  END
  
              END
  
          END
      END
  
  
  ELSE ! IF (p_web.GSV('BookingSite') = 'RRC')
  END ! IF (p_web.GSV('BookingSite') = 'RRC')
  
  
  
  p_web.SSV('Hide:IssueRefundButton',1)
  IF (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'ISSUE REFUND') = 0)
      p_web.SSV('Hide:IssueRefundButton',0)
  END
  
  
  p_web.SSV('Hide:CreateInvoiceButton',showHideCreateInvoice())
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Amend Payments') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Amend Payments',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_DisplayBrowsePayments',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_DisplayBrowsePayments0_div')&'">'&p_web.Translate('Browse Payments')&'</a></li>'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_DisplayBrowsePayments1_div')&'">'&p_web.Translate('Actions')&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  do GenerateTab1
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="DisplayBrowsePayments_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('DisplayBrowsePayments_BrowsePayments_embedded_div')&''');'
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="DisplayBrowsePayments_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'DisplayBrowsePayments_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="DisplayBrowsePayments_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('DisplayBrowsePayments_BrowsePayments_embedded_div')&''');'
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'DisplayBrowsePayments_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_DisplayBrowsePayments')>0,p_web.GSV('showtab_DisplayBrowsePayments'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_DisplayBrowsePayments'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_DisplayBrowsePayments') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_DisplayBrowsePayments'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_DisplayBrowsePayments')>0,p_web.GSV('showtab_DisplayBrowsePayments'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_DisplayBrowsePayments') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Browse Payments') & ''''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Actions') & ''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_DisplayBrowsePayments_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_DisplayBrowsePayments')>0,p_web.GSV('showtab_DisplayBrowsePayments'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"DisplayBrowsePayments",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_DisplayBrowsePayments')>0,p_web.GSV('showtab_DisplayBrowsePayments'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_DisplayBrowsePayments_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('DisplayBrowsePayments') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('DisplayBrowsePayments')
    p_web.SetValue('_CallPopups',2)
    If p_web.GetPreCall('BrowsePayments') = 0
      p_web.SetValue('BrowsePayments:NoForm',1)
      p_web.SetValue('BrowsePayments:FormName',loc:formname)
      p_web.SetValue('BrowsePayments:parentIs','Form')
      p_web.SetValue('_parentProc','DisplayBrowsePayments')
      BrowsePayments(p_web)
      p_web.SetValue('_CallPopups',0)
      p_web.DeleteValue('BrowsePayments:NoForm')
      p_web.DeleteValue('BrowsePayments:FormName')
      p_web.DeleteValue('BrowsePayments:parentIs')
      p_web.DeleteValue('_parentProc')
    End
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Browse Payments')&'</a></h3>' & CRLF & p_web.DivHeader('tab_DisplayBrowsePayments0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_DisplayBrowsePayments0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_DisplayBrowsePayments0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_DisplayBrowsePayments0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Browse Payments')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_DisplayBrowsePayments0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Browse Payments')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_DisplayBrowsePayments0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Browse Payments')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_DisplayBrowsePayments0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::BrowsePayments
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab1  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Actions')&'</a></h3>' & CRLF & p_web.DivHeader('tab_DisplayBrowsePayments1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_DisplayBrowsePayments1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_DisplayBrowsePayments1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_DisplayBrowsePayments1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Actions')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_DisplayBrowsePayments1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Actions')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_DisplayBrowsePayments1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Actions')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_DisplayBrowsePayments1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::buttonViewCosts
        do Comment::buttonViewCosts
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::buttonIssueRefund
        do Comment::buttonIssueRefund
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::buttonCreateCreditNote
        do Comment::buttonCreateCreditNote
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::buttonCreateInvoice
        do Comment::buttonCreateInvoice
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Validate::BrowsePayments  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  Elsif true
    p_web.StoreValue('jpt:Record_Number')
  End
  do ValidateValue::BrowsePayments  ! copies value to session value if valid.
  do Comment::BrowsePayments ! allows comment style to be updated.

ValidateValue::BrowsePayments  Routine
    If not (1=0)
    End

Value::BrowsePayments  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(1=0,1,0))
  ! --- BROWSE ---  BrowsePayments --
  p_web.SetValue('BrowsePayments:NoForm',1)
  p_web.SetValue('BrowsePayments:FormName',loc:formname)
  p_web.SetValue('BrowsePayments:parentIs','Form')
  p_web.SetValue('_parentProc','DisplayBrowsePayments')
  if p_web.RequestAjax = 0
    p_web.SSV('DisplayBrowsePayments:_popup_',p_web.GetValue('_popup_')) ! stores the current browse popup state
    packet = clip(packet) & '<div id="'&lower('DisplayBrowsePayments_BrowsePayments_embedded_div')&'"><!-- Net:BrowsePayments --></div><13,10>'
    do SendPacket
    p_web.DivHeader('DisplayBrowsePayments_' & lower('BrowsePayments') & '_value')
    p_web.DivFooter()
  else
    if p_web.GetValue('_popup_') = 1
      p_web.SSV('DisplayBrowsePayments:_popup_',1)
    elsif p_web.GSV('DisplayBrowsePayments:_popup_') = 1
      p_web.SetValue('_popup_',1)
    end
    packet = clip(packet) & '<!-- Net:BrowsePayments --><13,10>'
  end
  do SendPacket
Comment::BrowsePayments  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if BrowsePayments:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('DisplayBrowsePayments_' & p_web._nocolon('BrowsePayments') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonViewCosts  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonViewCosts  ! copies value to session value if valid.
  do Comment::buttonViewCosts ! allows comment style to be updated.

ValidateValue::buttonViewCosts  Routine
    If not (1=0)
    End

Value::buttonViewCosts  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('DisplayBrowsePayments_' & p_web._nocolon('buttonViewCosts') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','ViewCosts','Job Costs',p_web.combine(Choose('Job Costs' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.WindowOpen(clip('ViewCosts?ViewCostsReturnURL=DisplayBrowsePayments')&''&'','_self'),,loc:disabled,'images/moneys.png',,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonViewCosts  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonViewCosts:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('DisplayBrowsePayments_' & p_web._nocolon('buttonViewCosts') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonIssueRefund  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonIssueRefund  ! copies value to session value if valid.
  do Comment::buttonIssueRefund ! allows comment style to be updated.

ValidateValue::buttonIssueRefund  Routine
    If not (p_web.GSV('Hide:IssueRefundButton') = 1)
    End

Value::buttonIssueRefund  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:IssueRefundButton') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('DisplayBrowsePayments_' & p_web._nocolon('buttonIssueRefund') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('Hide:IssueRefundButton') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','IssueRefund','Issue Refund',p_web.combine(Choose('Issue Refund' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.WindowOpen(clip('FormPayments?Insert_btn=Insert&PassedType=REFUND')&''&'','_self'),,loc:disabled,'images/money_delete.png',,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonIssueRefund  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonIssueRefund:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:IssueRefundButton') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('DisplayBrowsePayments_' & p_web._nocolon('buttonIssueRefund') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:IssueRefundButton') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonCreateCreditNote  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonCreateCreditNote  ! copies value to session value if valid.
  do Comment::buttonCreateCreditNote ! allows comment style to be updated.

ValidateValue::buttonCreateCreditNote  Routine
    If not (p_web.GSV('Hide:CreateCreditNoteButton') = 1)
    End

Value::buttonCreateCreditNote  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:CreateCreditNoteButton') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('DisplayBrowsePayments_' & p_web._nocolon('buttonCreateCreditNote') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('Hide:CreateCreditNoteButton') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','CreateCreditNote','Create Credit Note',p_web.combine(Choose('Create Credit Note' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.WindowOpen(clip('CreateCreditNote')&''&'','_self'),,loc:disabled,'images/money_add.png',,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonCreateCreditNote  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonCreateCreditNote:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:CreateCreditNoteButton') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('DisplayBrowsePayments_' & p_web._nocolon('buttonCreateCreditNote') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:CreateCreditNoteButton') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonCreateInvoice  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonCreateInvoice  ! copies value to session value if valid.
  do Comment::buttonCreateInvoice ! allows comment style to be updated.

ValidateValue::buttonCreateInvoice  Routine
    If not (p_web.GSV('Hide:CreateInvoiceButton') = 1)
    End

Value::buttonCreateInvoice  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:CreateInvoiceButton') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('DisplayBrowsePayments_' & p_web._nocolon('buttonCreateInvoice') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('Hide:CreateInvoiceButton') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','CreateInvoice',p_web.GSV('URL:CreateInvoiceText'),p_web.combine(Choose(p_web.GSV('URL:CreateInvoiceText') <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.WindowOpen(clip(p_web.GSV('URL:CreateInvoice'))&''&'',p_web.GSV('URL:CreateInvoiceTarget')),,loc:disabled,'images/moneys.png',,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonCreateInvoice  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonCreateInvoice:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:CreateInvoiceButton') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('DisplayBrowsePayments_' & p_web._nocolon('buttonCreateInvoice') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:CreateInvoiceButton') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('DisplayBrowsePayments_nexttab_' & 0)
    If loc:Invalid then exit.
  of lower('DisplayBrowsePayments_nexttab_' & 1)
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_DisplayBrowsePayments_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('DisplayBrowsePayments_tab_' & 0)
    do GenerateTab0
  of lower('DisplayBrowsePayments_tab_' & 1)
    do GenerateTab1
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('DisplayBrowsePayments_form:ready_',1)

  p_web.SetSessionValue('DisplayBrowsePayments_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_DisplayBrowsePayments',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('DisplayBrowsePayments_form:ready_',1)
  p_web.SetSessionValue('DisplayBrowsePayments_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_DisplayBrowsePayments',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('DisplayBrowsePayments_form:ready_',1)
  p_web.SetSessionValue('DisplayBrowsePayments_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('DisplayBrowsePayments:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('DisplayBrowsePayments_form:ready_',1)
  p_web.SetSessionValue('DisplayBrowsePayments_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('DisplayBrowsePayments:Primed',0)
  p_web.setsessionvalue('showtab_DisplayBrowsePayments',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('DisplayBrowsePayments_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('DisplayBrowsePayments_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
    loc:InvalidTab += 1
    do ValidateValue::BrowsePayments
    If loc:Invalid then exit.
  ! tab = 3
    loc:InvalidTab += 1
    do ValidateValue::buttonViewCosts
    If loc:Invalid then exit.
    do ValidateValue::buttonIssueRefund
    If loc:Invalid then exit.
    do ValidateValue::buttonCreateCreditNote
    If loc:Invalid then exit.
    do ValidateValue::buttonCreateInvoice
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
  DO DeleteSessionValues
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('DisplayBrowsePayments:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')

showHideCreateInvoice       PROCEDURE()
CODE
    IF (p_web.gsv('job:Chargeable_Job') <> 'YES')
        RETURN TRUE
    END

    SentToHub(p_web)
    IF (p_web.GSV('BookingSite') = 'ARC' AND p_web.GSV('SendToHub') <> 1)
        RETURN TRUE
    END

    IF (p_web.GSV('job:Bouncer') = 'X')
        RETURN TRUE
    END

    ! Job not completed
    IF (p_web.GSV('job:Date_Completed') = 0 AND p_web.GSV('job:Exchange_Unit_Number') = 0)
        RETURN TRUE
    END

    ! Job has not been priced
    IF (p_web.GSV('job:ignore_Chargeable_Charges') = 'YES')
        IF (p_web.GSV('BookingSite') = 'RRC')
            IF (p_web.GSV('jobe:RRCSubTotal') = 0)
                RETURN TRUE
            END
        ELSE
            IF (p_web.GSV('job:Sub_Total') = 0)
                RETURN TRUE
            END

        END
    END
    p_web.SSV('URL:CreateInvoice','CreateInvoice?returnURL=DisplayBrowsePayments')
    p_web.SSV('URL:CreateInvoiceTarget','_self')
    p_web.SSV('URL:CreateInvoiceText','Create Invoice')

    IF (p_web.GSV('job:Invoice_Number') > 0)
        Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
        inv:Invoice_Number = p_web.GSV('job:Invoice_Number')
        IF (Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign)
            IF (inv:Invoice_Type <> 'SIN')
                RETURN TRUE
            END
            ! If job has been invoiced, just print the invoice, rather than asking to create one
            IF (p_web.GSV('BookingSite') = 'RRC')
                IF (inv:RRCInvoiceDate > 0)
                    p_web.SSV('URL:CreateInvoice','InvoiceNote?var=' & RANDOM(1,9999999))
                    p_web.SSV('URL:CreateInvoiceTarget','_blank')
                    p_web.SSV('URL:CreateInvoiceText','Print Invoice')
                END
            ELSE
                If (inv:ARCInvoiceDate > 0)
                    p_web.SSV('URL:CreateInvoice','InvoiceNote?var=' & RANDOM(1,9999999))
                    p_web.SSV('URL:CreateInvoiceTarget','_blank')
                    p_web.SSV('URL:CreateInvoiceText','Print Invoice')
                END
            END
        END

    END
    RETURN FALSE



DespatchConfirmation PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locCompanyName       STRING(30)                            !
locCourierCost       REAL                                  !
locLabourCost        REAL                                  !
locPartsCost         REAL                                  !
locSubTotal          REAL                                  !
locVAT               REAL                                  !
locTotal             REAL                                  !
locInvoiceNumber     REAL                                  !
locInvoiceDate       DATE                                  !
locLabourVatRate     REAL                                  !
locPartsVatRate      REAL                                  !
locCourier           STRING(30)                            !
FilesOpened     Long
EXCHHIST::State  USHORT
LOANHIST::State  USHORT
EXCHANGE::State  USHORT
LOAN::State  USHORT
JOBSCONS::State  USHORT
COURIER::State  USHORT
VATCODE::State  USHORT
INVOICE::State  USHORT
TRADEACC::State  USHORT
SUBTRACC::State  USHORT
JOBSE::State  USHORT
JOBS::State  USHORT
job:Account_Number:IsInvalid  Long
locCompanyName:IsInvalid  Long
job:Charge_Type:IsInvalid  Long
job:Repair_Type:IsInvalid  Long
aLine:IsInvalid  Long
textBillingDetails:IsInvalid  Long
textInvoiceDetails:IsInvalid  Long
locCourierCost:IsInvalid  Long
locLabourCost:IsInvalid  Long
locInvoiceNumber:IsInvalid  Long
locPartsCost:IsInvalid  Long
locSubTotal:IsInvalid  Long
locInvoiceDate:IsInvalid  Long
locVAT:IsInvalid  Long
locTotal:IsInvalid  Long
job:Courier:IsInvalid  Long
buttonCreateInvoice:IsInvalid  Long
buttonPaymentDetails:IsInvalid  Long
buttonPrintWaybill:IsInvalid  Long
buttonPrintDespatchNote:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
job:Courier_OptionView   View(COURIER)
                          Project(cou:Courier)
                        End
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('DespatchConfirmation')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'DespatchConfirmation_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('DespatchConfirmation','')
    p_web.DivHeader('DespatchConfirmation',p_web.combine(p_web.site.style.formdiv,))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('DespatchConfirmation') = 0
        p_web.AddPreCall('DespatchConfirmation')
        p_web.DivHeader('popup_DespatchConfirmation','nt-hidden')
        p_web.DivHeader('DespatchConfirmation',p_web.combine(p_web.site.style.formdiv,))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_DespatchConfirmation_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_DespatchConfirmation_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferDespatchConfirmation',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_DespatchConfirmation',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferDespatchConfirmation',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_DespatchConfirmation',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferDespatchConfirmation',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_DespatchConfirmation',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_DespatchConfirmation',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferDespatchConfirmation',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_DespatchConfirmation',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferDespatchConfirmation',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_DespatchConfirmation',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_DespatchConfirmation',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('DespatchConfirmation')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
DeleteSessionValues ROUTINE ! Auto generated by Bryan's Template
    ! Local Variables
    p_web.DeleteSessionValue('Ans')
    p_web.DeleteSessionValue('locCompanyName')
    p_web.DeleteSessionValue('locCourierCost')
    p_web.DeleteSessionValue('locLabourCost')
    p_web.DeleteSessionValue('locPartsCost')
    p_web.DeleteSessionValue('locSubTotal')
    p_web.DeleteSessionValue('locVAT')
    p_web.DeleteSessionValue('locTotal')
    p_web.DeleteSessionValue('locInvoiceNumber')
    p_web.DeleteSessionValue('locInvoiceDate')
    p_web.DeleteSessionValue('locLabourVatRate')
    p_web.DeleteSessionValue('locPartsVatRate')
    p_web.DeleteSessionValue('locCourier')

    ! Other Variables
    p_web.DeleteSessionValue('DespatchConfirmation:FirstTime')
DespatchProcess     Routine
DATA
locWaybillNumber    STRING(30)
CODE
    p_web.SSV('Hide:PrintDespatchNote',1)
    p_web.SSV('Hide:PrintWaybill',1)

    IF (p_web.GSV('cou:PrintWaybill') = 1)

        p_web.SSV('Hide:PrintWaybill',0)

        locWaybillNumber = NextWaybillNumber()
        IF (locWaybillNumber = 0)
            EXIT
        END

        Access:WAYBILLS.ClearKey(way:WayBillNumberKey)
        way:WayBillNumber = locWaybillNumber
        IF (Access:WAYBILLS.TryFetch(way:WayBillNumberKey))
            EXIT
        END

        IF (p_web.GSV('BookingSite') = 'RRC')

            way:AccountNumber = p_web.GSV('BookingAccount')
            CASE p_web.GSV('DespatchType')
            OF 'JOB'
                way:WaybillID = 2
                p_web.SSV('wob:JobWaybillNumber',locWaybillNumber)
            OF 'EXC'
                way:WaybillID = 3
                p_web.SSV('wob:ExcWaybillNumber',way:WayBillNumber)
            OF 'LOA'
                way:WaybillID = 4
                p_web.SSV('wob:LoaWaybillNumber',way:WayBillNumber)
            END

            IF (p_web.GSV('job:Who_Booked') = 'WEB')
                ! PUP Job
                way:WayBillType = 9
                way:WaybillID = 21
            ELSE
                way:WayBillType = 2
            END

            way:FromAccount = p_web.GSV('BookingAccount')
            way:ToAccount = p_web.GSV('job:Account_Number')

        ELSE
            way:WayBillType = 1 ! Created From RRC
            way:AccountNumber = p_web.GSV('wob:HeadAccountNumber')
            way:FromAccount = p_web.GSV('ARC:AccountNumber')
            IF (p_web.GSV('jobe:WebJob') = 1)
                way:ToAccount = p_web.GSV('wob:HeadAccountNumber')
                Case p_web.GSV('DespatchType')
                of 'JOB'
                    way:WaybillID = 5 ! ARC to RRC (JOB)
                of 'EXC'
                    way:WaybillID = 6 ! ARC to RRC (EXC)
                END

            ELSE
                way:ToAccount = job:Account_Number
                Case p_web.GSV('DespatchType')
                OF 'JOB'
                    way:WaybillID = 7 ! ARC To Customer (JOB)
                of 'EXC'
                    way:WaybillID = 8 ! ARC To Customer (EXC)
                of 'LOA'
                    way:WaybillID = 9 ! ARC To Customer (LOA)
                END

            END

        END


        IF (Access:WAYBILLS.TryUpdate())
            EXIT
        END

        IF (Access:WAYBILLJ.PrimeRecord() = Level:Benign)
            waj:WayBillNumber = way:WayBillNumber
            waj:JobNumber = p_web.GSV('job:Ref_Number')
            CASE p_web.GSV('DespatchType')
            OF 'JOB'
                waj:IMEINumber = p_web.GSV('job:ESN')
            OF 'EXC'
                Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
                xch:Ref_Number = p_web.GSV('job:Exchange_Unit_Number')
                IF (Access:EXCHANGE.TryFetch(xch:Ref_Available_Key) = Level:Benign)
                    waj:IMEINumber = xch:ESN
                END

            OF 'LOA'
                Access:LOAN.ClearKey(loa:Ref_Number_Key)
                loa:Ref_Number = p_web.GSV('job:Loan_Unit_Number')
                IF (Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign)
                    waj:IMEINumber = loa:ESN
                END

            END
            waj:OrderNumber = p_web.GSV('job:Order_Number')
            waj:SecurityPackNumber = p_web.GSV('locSecurityPackID')
            waj:JobType = p_web.GSV('DespatchType')
            IF (Access:WAYBILLJ.TryUpdate() = Level:Benign)
            ELSE
                Access:WAYBILLJ.CancelAutoInc()
            END
        END

    ELSE ! IF (p_web.GSV('cou:PrintWaybill') = 1)
        if (p_web.GSV('cou:AutoConsignmentNo') = 1)
            Access:COURIER.Clearkey(cou:Courier_Key)
            cou:Courier = p_web.GSV('cou:Courier')
            if (Access:COURIER.Tryfetch(cou:Courier_Key) = Level:Benign)
                cou:LastConsignmentNo += 1
                Access:COURIER.TryUpdate()
                locWaybillNumber = cou:LastConsignmentNo
            end
        ELSE
            locWaybillNumber = p_web.GSV('locConsignmentNumber')
        end
        p_web.SSV('Hide:PrintDespatchNote',0)

    END ! IF (p_web.GSV('cou:PrintWaybill') = 1)

    p_web.SSV('locWaybillNumber',locWaybillNumber)

    CASE p_web.GSV('DespatchType')
    OF 'JOB'
        !Despatch:Job(locWayBillNumber)
        Despatch:Job(p_web)

    OF 'EXC'
        Despatch:Exc(p_web)

    OF 'LOA'
        Despatch:Loa(p_web)

    END

    ! Save Files
    Access:JOBS.ClearKey(job:Ref_Number_Key)
    job:Ref_Number = p_web.GSV('job:Ref_Number')
    IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
        p_web.SessionQueueToFile(JOBS)
        IF (Access:JOBS.TryUpdate() = Level:Benign)

            Access:WEBJOB.ClearKey(wob:RefNumberKey)
            wob:RefNumber = job:Ref_Number
            IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
                p_web.SessionQueueToFile(WEBJOB)
                IF (Access:WEBJOB.TryUpdate() = Level:Benign)

                    Access:JOBSE.ClearKey(jobe:RefNumberKey)
                    jobe:RefNumber = job:Ref_Number
                    IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
                        p_web.SessionQueueToFile(JOBSE)
                        IF (Access:JOBSE.TryUpdate() = Level:Benign)

                        END
                    END
                END
            END
        END
    END




ShowHidePaymentDetails      ROUTINE
DATA
locPaymentStringRRC EQUATE('PAYMENT TYPES')
locPaymentStringARC EQUATE('PAYMENT DETAILS')
locPaymentString    CSTRING(30)
CODE

    paymentFail# = 0

    IF (p_web.GSV('BookingSite') = 'RRC')
        locPaymentString = locPaymentStringRRC
    ELSE
        locPaymentString = locPaymentStringARC
    END

    IF (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),locPaymentString) = 0)
        IF (p_web.GSV('job:Loan_Unit_Number') = 0)
            IF (p_web.GSV('job:Warranty_Job') = 'YES' AND p_web.GSV('job:Chargeable_Job') <> 'YES')
                paymentFail# = 1
            END
        END
    ELSE
        paymentFail# = 1
    END
    IF (paymentFail# = 1)
        p_web.SSV('Hide:PaymentDetails',1)
    ELSE
        p_web.SSV('Hide:PaymentDetails',0)
    END

OpenFiles  ROUTINE
  p_web._OpenFile(EXCHHIST)
  p_web._OpenFile(LOANHIST)
  p_web._OpenFile(EXCHANGE)
  p_web._OpenFile(LOAN)
  p_web._OpenFile(JOBSCONS)
  p_web._OpenFile(COURIER)
  p_web._OpenFile(VATCODE)
  p_web._OpenFile(INVOICE)
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(SUBTRACC)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(JOBS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(EXCHHIST)
  p_Web._CloseFile(LOANHIST)
  p_Web._CloseFile(EXCHANGE)
  p_Web._CloseFile(LOAN)
  p_Web._CloseFile(JOBSCONS)
  p_Web._CloseFile(COURIER)
  p_Web._CloseFile(VATCODE)
  p_Web._CloseFile(INVOICE)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(JOBS)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('DespatchConfirmation_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'DespatchConfirmation'
    end
    p_web.formsettings.proc = 'DespatchConfirmation'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine
  DO deletesessionvalues

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('job:Account_Number')
    p_web.SetPicture('job:Account_Number','@s15')
  End
  p_web.SetSessionPicture('job:Account_Number','@s15')
  If p_web.IfExistsValue('job:Charge_Type')
    p_web.SetPicture('job:Charge_Type','@s30')
  End
  p_web.SetSessionPicture('job:Charge_Type','@s30')
  If p_web.IfExistsValue('job:Repair_Type')
    p_web.SetPicture('job:Repair_Type','@s30')
  End
  p_web.SetSessionPicture('job:Repair_Type','@s30')
  If p_web.IfExistsValue('locCourierCost')
    p_web.SetPicture('locCourierCost','@n14.2')
  End
  p_web.SetSessionPicture('locCourierCost','@n14.2')
  If p_web.IfExistsValue('locLabourCost')
    p_web.SetPicture('locLabourCost','@n14.2')
  End
  p_web.SetSessionPicture('locLabourCost','@n14.2')
  If p_web.IfExistsValue('locPartsCost')
    p_web.SetPicture('locPartsCost','@n14.2')
  End
  p_web.SetSessionPicture('locPartsCost','@n14.2')
  If p_web.IfExistsValue('locSubTotal')
    p_web.SetPicture('locSubTotal','@n14.2')
  End
  p_web.SetSessionPicture('locSubTotal','@n14.2')
  If p_web.IfExistsValue('locInvoiceDate')
    p_web.SetPicture('locInvoiceDate','@d06')
  End
  p_web.SetSessionPicture('locInvoiceDate','@d06')
  If p_web.IfExistsValue('locVAT')
    p_web.SetPicture('locVAT','@n14.2')
  End
  p_web.SetSessionPicture('locVAT','@n14.2')
  If p_web.IfExistsValue('locTotal')
    p_web.SetPicture('locTotal','@n14.2')
  End
  p_web.SetSessionPicture('locTotal','@n14.2')

AfterLookup Routine
  loc:TabNumber = -1
  If p_web.GSV('Show:DespatchInvoiceDetails') = 1
    loc:TabNumber += 1
  End
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  if p_web.IfExistsValue('job:Account_Number') = 0
    p_web.SetSessionValue('job:Account_Number',job:Account_Number)
  Else
    job:Account_Number = p_web.GetSessionValue('job:Account_Number')
  End
  if p_web.IfExistsValue('locCompanyName') = 0
    p_web.SetSessionValue('locCompanyName',locCompanyName)
  Else
    locCompanyName = p_web.GetSessionValue('locCompanyName')
  End
  if p_web.IfExistsValue('job:Charge_Type') = 0
    p_web.SetSessionValue('job:Charge_Type',job:Charge_Type)
  Else
    job:Charge_Type = p_web.GetSessionValue('job:Charge_Type')
  End
  if p_web.IfExistsValue('job:Repair_Type') = 0
    p_web.SetSessionValue('job:Repair_Type',job:Repair_Type)
  Else
    job:Repair_Type = p_web.GetSessionValue('job:Repair_Type')
  End
  if p_web.IfExistsValue('locCourierCost') = 0
    p_web.SetSessionValue('locCourierCost',locCourierCost)
  Else
    locCourierCost = p_web.GetSessionValue('locCourierCost')
  End
  if p_web.IfExistsValue('locLabourCost') = 0
    p_web.SetSessionValue('locLabourCost',locLabourCost)
  Else
    locLabourCost = p_web.GetSessionValue('locLabourCost')
  End
  if p_web.IfExistsValue('locInvoiceNumber') = 0
    p_web.SetSessionValue('locInvoiceNumber',locInvoiceNumber)
  Else
    locInvoiceNumber = p_web.GetSessionValue('locInvoiceNumber')
  End
  if p_web.IfExistsValue('locPartsCost') = 0
    p_web.SetSessionValue('locPartsCost',locPartsCost)
  Else
    locPartsCost = p_web.GetSessionValue('locPartsCost')
  End
  if p_web.IfExistsValue('locSubTotal') = 0
    p_web.SetSessionValue('locSubTotal',locSubTotal)
  Else
    locSubTotal = p_web.GetSessionValue('locSubTotal')
  End
  if p_web.IfExistsValue('locInvoiceDate') = 0
    p_web.SetSessionValue('locInvoiceDate',locInvoiceDate)
  Else
    locInvoiceDate = p_web.GetSessionValue('locInvoiceDate')
  End
  if p_web.IfExistsValue('locVAT') = 0
    p_web.SetSessionValue('locVAT',locVAT)
  Else
    locVAT = p_web.GetSessionValue('locVAT')
  End
  if p_web.IfExistsValue('locTotal') = 0
    p_web.SetSessionValue('locTotal',locTotal)
  Else
    locTotal = p_web.GetSessionValue('locTotal')
  End
  if p_web.IfExistsValue('job:Courier') = 0
    p_web.SetSessionValue('job:Courier',job:Courier)
  Else
    job:Courier = p_web.GetSessionValue('job:Courier')
  End

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('job:Account_Number')
    job:Account_Number = p_web.GetValue('job:Account_Number')
    p_web.SetSessionValue('job:Account_Number',job:Account_Number)
  Else
    job:Account_Number = p_web.GetSessionValue('job:Account_Number')
  End
  if p_web.IfExistsValue('locCompanyName')
    locCompanyName = p_web.GetValue('locCompanyName')
    p_web.SetSessionValue('locCompanyName',locCompanyName)
  Else
    locCompanyName = p_web.GetSessionValue('locCompanyName')
  End
  if p_web.IfExistsValue('job:Charge_Type')
    job:Charge_Type = p_web.GetValue('job:Charge_Type')
    p_web.SetSessionValue('job:Charge_Type',job:Charge_Type)
  Else
    job:Charge_Type = p_web.GetSessionValue('job:Charge_Type')
  End
  if p_web.IfExistsValue('job:Repair_Type')
    job:Repair_Type = p_web.GetValue('job:Repair_Type')
    p_web.SetSessionValue('job:Repair_Type',job:Repair_Type)
  Else
    job:Repair_Type = p_web.GetSessionValue('job:Repair_Type')
  End
  if p_web.IfExistsValue('locCourierCost')
    locCourierCost = p_web.dformat(clip(p_web.GetValue('locCourierCost')),'@n14.2')
    p_web.SetSessionValue('locCourierCost',locCourierCost)
  Else
    locCourierCost = p_web.GetSessionValue('locCourierCost')
  End
  if p_web.IfExistsValue('locLabourCost')
    locLabourCost = p_web.dformat(clip(p_web.GetValue('locLabourCost')),'@n14.2')
    p_web.SetSessionValue('locLabourCost',locLabourCost)
  Else
    locLabourCost = p_web.GetSessionValue('locLabourCost')
  End
  if p_web.IfExistsValue('locInvoiceNumber')
    locInvoiceNumber = p_web.GetValue('locInvoiceNumber')
    p_web.SetSessionValue('locInvoiceNumber',locInvoiceNumber)
  Else
    locInvoiceNumber = p_web.GetSessionValue('locInvoiceNumber')
  End
  if p_web.IfExistsValue('locPartsCost')
    locPartsCost = p_web.dformat(clip(p_web.GetValue('locPartsCost')),'@n14.2')
    p_web.SetSessionValue('locPartsCost',locPartsCost)
  Else
    locPartsCost = p_web.GetSessionValue('locPartsCost')
  End
  if p_web.IfExistsValue('locSubTotal')
    locSubTotal = p_web.dformat(clip(p_web.GetValue('locSubTotal')),'@n14.2')
    p_web.SetSessionValue('locSubTotal',locSubTotal)
  Else
    locSubTotal = p_web.GetSessionValue('locSubTotal')
  End
  if p_web.IfExistsValue('locInvoiceDate')
    locInvoiceDate = p_web.dformat(clip(p_web.GetValue('locInvoiceDate')),'@d06')
    p_web.SetSessionValue('locInvoiceDate',locInvoiceDate)
  Else
    locInvoiceDate = p_web.GetSessionValue('locInvoiceDate')
  End
  if p_web.IfExistsValue('locVAT')
    locVAT = p_web.dformat(clip(p_web.GetValue('locVAT')),'@n14.2')
    p_web.SetSessionValue('locVAT',locVAT)
  Else
    locVAT = p_web.GetSessionValue('locVAT')
  End
  if p_web.IfExistsValue('locTotal')
    locTotal = p_web.dformat(clip(p_web.GetValue('locTotal')),'@n14.2')
    p_web.SetSessionValue('locTotal',locTotal)
  Else
    locTotal = p_web.GetSessionValue('locTotal')
  End
  if p_web.IfExistsValue('job:Courier')
    job:Courier = p_web.GetValue('job:Courier')
    p_web.SetSessionValue('job:Courier',job:Courier)
  Else
    job:Courier = p_web.GetSessionValue('job:Courier')
  End

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('DespatchConfirmation_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'IndividualDespatch'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('DespatchConfirmation_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('DespatchConfirmation_ChainTo')
    loc:formaction = p_web.GetSessionValue('DespatchConfirmation_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'IndividualDespatch'

GenerateForm   Routine
  do LoadRelatedRecords
  ! Start
  p_web.site.SaveButton.TextValue = 'OK'
  
  IF (p_web.GSV('DespatchConfirmation:FirstTime') = 0)
      p_web.SSV('DespatchConfirmation:FirstTime',1)
      ! Call this code once
      IF (p_web.GSV('Show:DespatchInvoiceDetails') = 1)
  
          IF (p_web.GSV('CreateDespatchInvoice') = 1 )
              CreateTheInvoice(p_web)
          END
      END
      DO DespatchProcess
  
  
  END
  ! Hideous Calculations to work out invoice/costs
  p_web.SSV('URL:CreateInvoice','CreateInvoice?returnURL=DespatchConfirmation')
  p_web.SSV('URL:CreateInvoiceTarget','_self')
  p_web.SSV('URL:CreateInvoiceText','Create Invoice')
  
  isJobInvoiced(p_web)
  IF (p_web.GSV('IsJobInvoiced') = 1)
      p_web.SSV('URL:CreateInvoice','InvoiceNote?var=' & RANDOM(1,9999999))
      p_web.SSV('URL:CreateInvoiceTarget','_blank')
      p_web.SSV('URL:CreateInvoiceText','Print Invoice')
  
      IF (p_web.GSV('BookingSite') = 'RRC')
          p_web.SSV('locCourierCost',p_web.GSV('job:Invoice_Courier_Cost'))
          p_web.SSV('locLabourCost',p_web.GSV('jobe:InvRRCCLabourCost'))
          p_web.SSV('locPartsCost',p_web.GSV('jobe:InvRRCCPartsCost'))
          Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
          inv:Invoice_Number = p_web.GSV('job:Invoice_Number')
          IF (Access:INVOICE.TryFetch(inv:Invoice_Number_Key))
          END
          p_web.SSV('locVAT',(p_web.GSV('job:Invoice_Courier_Cost') * inv:Vat_Rate_Labour/100) + |
              (p_web.GSV('jobe:InvRRCCLabourCost') * inv:Vat_Rate_Labour/100) + |
              (p_web.GSV('jobe:InvRRCCPartsCost') * inv:Vat_Rate_Parts/100))
          Access:TRADEACC.ClearKey(tra:Account_Number_Key)
          tra:Account_Number = p_web.GSV('BookingAccount')
          IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
          END
  
          p_web.SSV('locInvoiceNumber',inv:Invoice_Number & '-' & tra:BranchIdentification)
          p_web.SSV('locInvoiceDate',inv:RRCInvoiceDate)
      else
          p_web.SSV('locCourierCost',p_web.GSV('job:Invoice_Courier_Cost'))
          p_web.SSV('locLabourCost',p_web.GSV('job:Invoice_Labour_Cost'))
          p_web.SSV('locPartsCost',p_web.GSV('job:Invoice_Parts_Cost'))
  
          Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
          inv:Invoice_Number = p_web.GSV('job:Invoice_Number')
          IF (Access:INVOICE.TryFetch(inv:Invoice_Number_Key))
          END
          p_web.SSV('locVAT',(p_web.GSV('job:Invoice_Courier_Cost') * inv:Vat_Rate_Labour/100) + |
              (p_web.GSV('job:Invoice_Labour_Cost') * inv:Vat_Rate_Labour/100) + |
              (p_web.GSV('job:Invoice_Parts_Cost') * inv:Vat_Rate_Parts/100))
          Access:TRADEACC.ClearKey(tra:Account_Number_Key)
          tra:Account_Number = p_web.GSV('Default:AccountNumber')
          IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
          END
  
          p_web.SSV('locInvoiceNumber',inv:Invoice_Number & '-' & tra:BranchIdentification)
          p_web.SSV('locInvoiceDate',inv:ARCInvoiceDate)
      end
  
  ELSE
      IF (p_web.GSV('BookingSite') = 'RRC')
          p_web.SSV('locCourierCost',p_web.GSV('job:Courier_Cost'))
          p_web.SSV('locLabourCost',p_web.GSV('jobe:RRCCLabourCost'))
          p_web.SSV('locPartsCost',p_web.GSV('jobe:RRCCPartsCost'))
  
  
      else
          p_web.SSV('locCourierCost',p_web.GSV('job:Courier_Cost'))
          p_web.SSV('locLabourCost',p_web.GSV('job:Labour_Cost'))
          p_web.SSV('locPartsCost',p_web.GSV('job:Parts_Cost'))
  
      end
      IF (InvoiceSubAccounts(p_web.GSV('job:Account_Number')))
          Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
          sub:Account_Number = p_web.GSV('job:Account_Number')
          IF (Access:subtracc.TryFetch(sub:Account_Number_Key))
          END
  
          Access:VATCODE.ClearKey(vat:Vat_code_Key)
          vat:VAT_Code = sub:Labour_VAT_Code
          IF (Access:VATCODE.TryFetch(vat:Vat_code_Key) = Level:Benign)
              locLabourVatRate = vat:VAT_Rate
          END
          Access:VATCODE.ClearKey(vat:Vat_code_Key)
          vat:VAT_Code = sub:Parts_VAT_Code
          IF (Access:VATCODE.TryFetch(vat:Vat_code_Key) = Level:Benign)
              locPartsVatRate = vat:VAT_Rate
          END
      ELSE
          Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
          sub:Account_Number = p_web.GSV('job:Account_Number')
          IF (Access:subtracc.TryFetch(sub:Account_Number_Key))
          END
          Access:TRADEACC.ClearKey(tra:Account_Number_Key)
          tra:Account_Number = sub:Main_Account_Number
          IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
          END
  
  
          Access:VATCODE.ClearKey(vat:Vat_code_Key)
          vat:VAT_Code = tra:Labour_VAT_Code
          IF (Access:VATCODE.TryFetch(vat:Vat_code_Key) = Level:Benign)
              locLabourVatRate = vat:VAT_Rate
          END
          Access:VATCODE.ClearKey(vat:Vat_code_Key)
          vat:VAT_Code = tra:Parts_VAT_Code
          IF (Access:VATCODE.TryFetch(vat:Vat_code_Key) = Level:Benign)
              locPartsVatRate = vat:VAT_Rate
          END
  
      END
  
      IF (p_web.GSV('BookingSite') = 'RRC')
          p_web.SSV('locVAT',(p_web.GSV('job:Courier_Cost') * locLabourVatRate/100) + |
              (p_web.GSV('jobe:RRCCLabourCost') * locLabourVatRate/100) + |
              (p_web.GSV('jobe:RRCCPartsCost') * locPartsVatRate/100))
      ELSE
  
          p_web.SSV('locVAT',(p_web.GSV('job:Courier_Cost') * locLabourVatRate/100) + |
              (p_web.GSV('job:Labour_Cost') * locLabourVatRate/100) + |
              (p_web.GSV('job:Parts_Cost') * locPartsVatRate/100))
      END
  
  
  END
  p_web.SSV('locSubTotal',p_web.GSV('locCourierCost') + |
      p_web.GSV('locLabourCost') + |
      p_web.GSV('locPartsCost'))
  p_web.SSV('locTotal',p_web.GSV('locSubTotal') + |
      p_web.GSV('locVAT'))
  Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
  sub:Account_Number = p_web.GSV('job:Account_Number')
  IF (Access:subtracc.TryFetch(sub:Account_Number_Key))
  END
  p_web.SSV('locCompanyName',sub:Company_Name)
  
  
  IF (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'PAYMENT TYPES'))
      p_web.SSV('Hide:PaymentDetails',1)
  ELSE
      p_web.SSV('Hide:PaymentDetails',0)
  END
  
 locCompanyName = p_web.RestoreValue('locCompanyName')
 locCourierCost = p_web.RestoreValue('locCourierCost')
 locLabourCost = p_web.RestoreValue('locLabourCost')
 locInvoiceNumber = p_web.RestoreValue('locInvoiceNumber')
 locPartsCost = p_web.RestoreValue('locPartsCost')
 locSubTotal = p_web.RestoreValue('locSubTotal')
 locInvoiceDate = p_web.RestoreValue('locInvoiceDate')
 locVAT = p_web.RestoreValue('locVAT')
 locTotal = p_web.RestoreValue('locTotal')
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Despatch Confirmation') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Despatch Confirmation',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_DespatchConfirmation',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      If p_web.GSV('Show:DespatchInvoiceDetails') = 1
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_DespatchConfirmation0_div')&'">'&p_web.Translate('Invoice Details')&'</a></li>'& CRLF
      End
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_DespatchConfirmation1_div')&'">'&p_web.Translate('Despatch')&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  do GenerateTab1
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="DespatchConfirmation_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="DespatchConfirmation_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'DespatchConfirmation_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="DespatchConfirmation_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'DespatchConfirmation_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    ElsIf p_web.GSV('Show:DespatchInvoiceDetails') = 1
        p_web.SetValue('SelectField',clip(loc:formname) & '.job:Courier')
    Else
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_DespatchConfirmation')>0,p_web.GSV('showtab_DespatchConfirmation'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_DespatchConfirmation'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_DespatchConfirmation') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_DespatchConfirmation'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_DespatchConfirmation')>0,p_web.GSV('showtab_DespatchConfirmation'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_DespatchConfirmation') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
      If p_web.GSV('Show:DespatchInvoiceDetails') = 1
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Invoice Details') & ''''
      End
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Despatch') & ''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_DespatchConfirmation_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_DespatchConfirmation')>0,p_web.GSV('showtab_DespatchConfirmation'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"DespatchConfirmation",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_DespatchConfirmation')>0,p_web.GSV('showtab_DespatchConfirmation'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_DespatchConfirmation_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('DespatchConfirmation') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('DespatchConfirmation')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
  If p_web.GSV('Show:DespatchInvoiceDetails') = 1
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Invoice Details')&'</a></h3>' & CRLF & p_web.DivHeader('tab_DespatchConfirmation0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_DespatchConfirmation0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_DespatchConfirmation0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_DespatchConfirmation0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Invoice Details')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_DespatchConfirmation0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Invoice Details')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_DespatchConfirmation0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Invoice Details')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_DespatchConfirmation0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Account_Number
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Account_Number
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::job:Account_Number
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locCompanyName
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locCompanyName
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locCompanyName
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Charge_Type
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Charge_Type
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::job:Charge_Type
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Repair_Type
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Repair_Type
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::job:Repair_Type
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::aLine
        do Comment::aLine
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::textBillingDetails
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::textBillingDetails
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::textBillingDetails
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::textInvoiceDetails
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::textInvoiceDetails
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::textInvoiceDetails
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locCourierCost
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locCourierCost
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locCourierCost
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locLabourCost
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locLabourCost
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locLabourCost
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locInvoiceNumber
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locInvoiceNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locInvoiceNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locPartsCost
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locPartsCost
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locPartsCost
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locSubTotal
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locSubTotal
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locSubTotal
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locInvoiceDate
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locInvoiceDate
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locInvoiceDate
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locVAT
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locVAT
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locVAT
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locTotal
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locTotal
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locTotal
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Courier
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Courier
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::job:Courier
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::buttonCreateInvoice
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::buttonCreateInvoice
        do Comment::buttonCreateInvoice
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::buttonPaymentDetails
        do Comment::buttonPaymentDetails
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
  end
GenerateTab1  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Despatch')&'</a></h3>' & CRLF & p_web.DivHeader('tab_DespatchConfirmation1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_DespatchConfirmation1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_DespatchConfirmation1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_DespatchConfirmation1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Despatch')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_DespatchConfirmation1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Despatch')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_DespatchConfirmation1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Despatch')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_DespatchConfirmation1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::buttonPrintWaybill
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::buttonPrintWaybill
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'28%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::buttonPrintDespatchNote
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'1%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::buttonPrintDespatchNote
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Prompt::job:Account_Number  Routine
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Account_Number') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Account Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Account_Number  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Account_Number = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s15
    job:Account_Number = p_web.Dformat(p_web.GetValue('Value'),'@s15')
  End
  do ValidateValue::job:Account_Number  ! copies value to session value if valid.
  do Comment::job:Account_Number ! allows comment style to be updated.

ValidateValue::job:Account_Number  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('job:Account_Number',job:Account_Number).
    End

Value::job:Account_Number  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Account_Number') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- job:Account_Number
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Account_Number'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::job:Account_Number  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if job:Account_Number:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Account_Number') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locCompanyName  Routine
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('locCompanyName') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Account Name'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locCompanyName  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locCompanyName = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locCompanyName = p_web.GetValue('Value')
  End
  do ValidateValue::locCompanyName  ! copies value to session value if valid.
  do Comment::locCompanyName ! allows comment style to be updated.

ValidateValue::locCompanyName  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locCompanyName',locCompanyName).
    End

Value::locCompanyName  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('locCompanyName') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- locCompanyName
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locCompanyName'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locCompanyName  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locCompanyName:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('locCompanyName') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::job:Charge_Type  Routine
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Charge_Type') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Charge Type'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Charge_Type  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Charge_Type = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    job:Charge_Type = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::job:Charge_Type  ! copies value to session value if valid.
  do Comment::job:Charge_Type ! allows comment style to be updated.

ValidateValue::job:Charge_Type  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('job:Charge_Type',job:Charge_Type).
    End

Value::job:Charge_Type  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Charge_Type') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- job:Charge_Type
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Charge_Type'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::job:Charge_Type  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if job:Charge_Type:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Charge_Type') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::job:Repair_Type  Routine
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Repair_Type') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Repair Type'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Repair_Type  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Repair_Type = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    job:Repair_Type = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::job:Repair_Type  ! copies value to session value if valid.
  do Comment::job:Repair_Type ! allows comment style to be updated.

ValidateValue::job:Repair_Type  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('job:Repair_Type',job:Repair_Type).
    End

Value::job:Repair_Type  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Repair_Type') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- job:Repair_Type
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Repair_Type'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::job:Repair_Type  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if job:Repair_Type:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Repair_Type') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::aLine  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::aLine  ! copies value to session value if valid.
  do Comment::aLine ! allows comment style to be updated.

ValidateValue::aLine  Routine
    If not (1=0)
    End

Value::aLine  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine('nt-width-100')
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('aLine') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & '<hr></hr><13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::aLine  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if aLine:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('aLine') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::textBillingDetails  Routine
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('textBillingDetails') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Billing Details:'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::textBillingDetails  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::textBillingDetails  ! copies value to session value if valid.
  do Comment::textBillingDetails ! allows comment style to be updated.

ValidateValue::textBillingDetails  Routine
    If not (1=0)
    End

Value::textBillingDetails  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('textBillingDetails') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::textBillingDetails  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if textBillingDetails:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('textBillingDetails') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::textInvoiceDetails  Routine
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('textInvoiceDetails') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Invoice Details:'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::textInvoiceDetails  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::textInvoiceDetails  ! copies value to session value if valid.
  do Comment::textInvoiceDetails ! allows comment style to be updated.

ValidateValue::textInvoiceDetails  Routine
    If not (1=0)
    End

Value::textInvoiceDetails  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('textInvoiceDetails') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::textInvoiceDetails  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if textInvoiceDetails:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('textInvoiceDetails') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locCourierCost  Routine
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('locCourierCost') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Courier Cost'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locCourierCost  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locCourierCost = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    locCourierCost = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::locCourierCost  ! copies value to session value if valid.
  do Comment::locCourierCost ! allows comment style to be updated.

ValidateValue::locCourierCost  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locCourierCost',locCourierCost).
    End

Value::locCourierCost  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('locCourierCost') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  If loc:retrying
    locCourierCost = p_web.RestoreValue('locCourierCost')
    do ValidateValue::locCourierCost
    If locCourierCost:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- locCourierCost
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locCourierCost',p_web.GetSessionValue('locCourierCost'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locCourierCost  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locCourierCost:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('locCourierCost') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locLabourCost  Routine
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('locLabourCost') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Labour Cost'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locLabourCost  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locLabourCost = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    locLabourCost = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::locLabourCost  ! copies value to session value if valid.
  do Comment::locLabourCost ! allows comment style to be updated.

ValidateValue::locLabourCost  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locLabourCost',locLabourCost).
    End

Value::locLabourCost  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('locLabourCost') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  If loc:retrying
    locLabourCost = p_web.RestoreValue('locLabourCost')
    do ValidateValue::locLabourCost
    If locLabourCost:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- locLabourCost
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locLabourCost',p_web.GetSessionValue('locLabourCost'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locLabourCost  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locLabourCost:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('locLabourCost') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locInvoiceNumber  Routine
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('locInvoiceNumber') & '_prompt',Choose(p_web.GSV('IsJobInvoiced') <> 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('IsJobInvoiced') <> 1,'',p_web.Translate('Invoice Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locInvoiceNumber  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locInvoiceNumber = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locInvoiceNumber = p_web.GetValue('Value')
  End
  do ValidateValue::locInvoiceNumber  ! copies value to session value if valid.
  do Comment::locInvoiceNumber ! allows comment style to be updated.

ValidateValue::locInvoiceNumber  Routine
    If not (p_web.GSV('IsJobInvoiced') <> 1)
      if loc:invalid = '' then p_web.SetSessionValue('locInvoiceNumber',locInvoiceNumber).
    End

Value::locInvoiceNumber  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('IsJobInvoiced') <> 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('locInvoiceNumber') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('IsJobInvoiced') <> 1)
  ! --- DISPLAY --- locInvoiceNumber
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locInvoiceNumber'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locInvoiceNumber  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locInvoiceNumber:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('IsJobInvoiced') <> 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('locInvoiceNumber') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('IsJobInvoiced') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locPartsCost  Routine
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('locPartsCost') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Parts Cost'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locPartsCost  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locPartsCost = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    locPartsCost = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::locPartsCost  ! copies value to session value if valid.
  do Comment::locPartsCost ! allows comment style to be updated.

ValidateValue::locPartsCost  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locPartsCost',locPartsCost).
    End

Value::locPartsCost  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('locPartsCost') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  If loc:retrying
    locPartsCost = p_web.RestoreValue('locPartsCost')
    do ValidateValue::locPartsCost
    If locPartsCost:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- locPartsCost
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locPartsCost',p_web.GetSessionValue('locPartsCost'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locPartsCost  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locPartsCost:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('locPartsCost') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locSubTotal  Routine
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('locSubTotal') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Sub Total'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locSubTotal  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locSubTotal = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    locSubTotal = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::locSubTotal  ! copies value to session value if valid.
  do Comment::locSubTotal ! allows comment style to be updated.

ValidateValue::locSubTotal  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locSubTotal',locSubTotal).
    End

Value::locSubTotal  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('locSubTotal') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  If loc:retrying
    locSubTotal = p_web.RestoreValue('locSubTotal')
    do ValidateValue::locSubTotal
    If locSubTotal:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- locSubTotal
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locSubTotal',p_web.GetSessionValue('locSubTotal'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locSubTotal  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locSubTotal:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('locSubTotal') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locInvoiceDate  Routine
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('locInvoiceDate') & '_prompt',Choose(p_web.GSV('IsJobInvoiced') <> 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('IsJobInvoiced') <> 1,'',p_web.Translate('Invoice Date'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locInvoiceDate  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locInvoiceDate = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@d06'  !FieldPicture = 
    locInvoiceDate = p_web.Dformat(p_web.GetValue('Value'),'@d06')
  End
  do ValidateValue::locInvoiceDate  ! copies value to session value if valid.
  do Comment::locInvoiceDate ! allows comment style to be updated.

ValidateValue::locInvoiceDate  Routine
    If not (p_web.GSV('IsJobInvoiced') <> 1)
      if loc:invalid = '' then p_web.SetSessionValue('locInvoiceDate',locInvoiceDate).
    End

Value::locInvoiceDate  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('IsJobInvoiced') <> 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('locInvoiceDate') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('IsJobInvoiced') <> 1)
  ! --- DISPLAY --- locInvoiceDate
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(format(p_web.GetSessionValue('locInvoiceDate'),'@d06')) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locInvoiceDate  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locInvoiceDate:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('IsJobInvoiced') <> 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('locInvoiceDate') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('IsJobInvoiced') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locVAT  Routine
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('locVAT') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('VAT'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locVAT  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locVAT = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    locVAT = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::locVAT  ! copies value to session value if valid.
  do Comment::locVAT ! allows comment style to be updated.

ValidateValue::locVAT  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locVAT',locVAT).
    End

Value::locVAT  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('locVAT') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  If loc:retrying
    locVAT = p_web.RestoreValue('locVAT')
    do ValidateValue::locVAT
    If locVAT:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- locVAT
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locVAT',p_web.GetSessionValue('locVAT'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locVAT  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locVAT:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('locVAT') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locTotal  Routine
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('locTotal') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Total'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locTotal  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locTotal = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    locTotal = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::locTotal  ! copies value to session value if valid.
  do Comment::locTotal ! allows comment style to be updated.

ValidateValue::locTotal  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locTotal',locTotal).
    End

Value::locTotal  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('locTotal') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  If loc:retrying
    locTotal = p_web.RestoreValue('locTotal')
    do ValidateValue::locTotal
    If locTotal:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- locTotal
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locTotal',p_web.GetSessionValue('locTotal'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locTotal  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locTotal:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('locTotal') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::job:Courier  Routine
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Courier') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Courier'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Courier  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Courier = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    job:Courier = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::job:Courier  ! copies value to session value if valid.
  do Value::job:Courier
  do SendAlert
  do Comment::job:Courier ! allows comment style to be updated.

ValidateValue::job:Courier  Routine
    If not (1=0)
    job:Courier = Upper(job:Courier)
      if loc:invalid = '' then p_web.SetSessionValue('job:Courier',job:Courier).
    End

Value::job:Courier  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Courier') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,p_web.site.style.formselect,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:retrying
    job:Courier = p_web.RestoreValue('job:Courier')
    do ValidateValue::job:Courier
    If job:Courier:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- DROPLIST ---
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Courier'',''despatchconfirmation_job:courier_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('job:Courier',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
  loc:fieldclass = ''
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(EXCHHIST)
  bind(exh:Record)
  p_web._OpenFile(LOANHIST)
  bind(loh:Record)
  p_web._OpenFile(EXCHANGE)
  bind(xch:Record)
  p_web._OpenFile(LOAN)
  bind(loa:Record)
  p_web._OpenFile(JOBSCONS)
  bind(joc:Record)
  p_web._OpenFile(COURIER)
  bind(cou:Record)
  p_web._OpenFile(VATCODE)
  bind(vat:Record)
  p_web._OpenFile(INVOICE)
  bind(inv:Record)
  p_web._OpenFile(TRADEACC)
  bind(tra:Record)
  p_web._OpenFile(SUBTRACC)
  bind(sub:Record)
  p_web._OpenFile(JOBSE)
  bind(jobe:Record)
  p_web._OpenFile(JOBS)
  bind(job:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(job:Courier_OptionView)
  job:Courier_OptionView{prop:order} = p_web.CleanFilter(job:Courier_OptionView,'UPPER(cou:Courier)')
  Set(job:Courier_OptionView)
  Loop
    Next(job:Courier_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('job:Courier') = 0
      p_web.SetSessionValue('job:Courier',cou:Courier)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,cou:Courier,choose(cou:Courier = p_web.getsessionvalue('job:Courier')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(job:Courier_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(EXCHHIST)
  p_Web._CloseFile(LOANHIST)
  p_Web._CloseFile(EXCHANGE)
  p_Web._CloseFile(LOAN)
  p_Web._CloseFile(JOBSCONS)
  p_Web._CloseFile(COURIER)
  p_Web._CloseFile(VATCODE)
  p_Web._CloseFile(INVOICE)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(JOBS)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web.DivFooter()
Comment::job:Courier  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if job:Courier:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Courier') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::buttonCreateInvoice  Routine
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('buttonCreateInvoice') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::buttonCreateInvoice  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonCreateInvoice  ! copies value to session value if valid.
  do Value::buttonCreateInvoice
  do Comment::buttonCreateInvoice ! allows comment style to be updated.

ValidateValue::buttonCreateInvoice  Routine
    If not (1=0)
    End

Value::buttonCreateInvoice  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('buttonCreateInvoice') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonCreateInvoice'',''despatchconfirmation_buttoncreateinvoice_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','CreateInvoice',p_web.GSV('URL:CreateInvoiceText'),p_web.combine(Choose(p_web.GSV('URL:CreateInvoiceText') <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.WindowOpen(clip(p_web.GSV('URL:CreateInvoice'))&''&'',p_web.GSV('URL:CreateInvoiceTarget')),,loc:disabled,'images/moneys.png',,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonCreateInvoice  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonCreateInvoice:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('buttonCreateInvoice') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonPaymentDetails  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonPaymentDetails  ! copies value to session value if valid.
  do Comment::buttonPaymentDetails ! allows comment style to be updated.

ValidateValue::buttonPaymentDetails  Routine
    If not (p_web.GSV('Hide:PaymentDetails') = 1)
    End

Value::buttonPaymentDetails  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:PaymentDetails') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('buttonPaymentDetails') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('Hide:PaymentDetails') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','PaymentDetails','PaymentDetails',p_web.combine(Choose('PaymentDetails' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.WindowOpen(clip('DisplayBrowsePayments?DisplayBrowsePaymentsReturnURL=DespatchConfirmation')&''&'','_self'),,loc:disabled,'images/moneys.png',,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonPaymentDetails  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonPaymentDetails:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:PaymentDetails') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('buttonPaymentDetails') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:PaymentDetails') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonPrintWaybill  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonPrintWaybill  ! copies value to session value if valid.
  do Value::buttonPrintWaybill
  do Comment::buttonPrintWaybill ! allows comment style to be updated.

ValidateValue::buttonPrintWaybill  Routine
    If not (p_web.GSV('Hide:PrintWaybill') = 1)
    End

Value::buttonPrintWaybill  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:PrintWaybill') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('buttonPrintWaybill') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('Hide:PrintWaybill') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonPrintWaybill'',''despatchconfirmation_buttonprintwaybill_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','PrintWaybill','Print Waybill',p_web.combine(Choose('Print Waybill' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.WindowOpen(clip('Waybill?var=' & RANDOM(1,9999999))&''&'','_blank'),,loc:disabled,'images/printer.png',,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonPrintWaybill  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonPrintWaybill:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:PrintWaybill') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('buttonPrintWaybill') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:PrintWaybill') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonPrintDespatchNote  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonPrintDespatchNote  ! copies value to session value if valid.
  do Value::buttonPrintDespatchNote
  do Comment::buttonPrintDespatchNote ! allows comment style to be updated.

ValidateValue::buttonPrintDespatchNote  Routine
    If not (p_web.GSV('Hide:PrintDespatchNote') = 1)
    End

Value::buttonPrintDespatchNote  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:PrintDespatchNote') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('buttonPrintDespatchNote') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('Hide:PrintDespatchNote') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonPrintDespatchNote'',''despatchconfirmation_buttonprintdespatchnote_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','PrintDespatchNote','Print Despatch Note',p_web.combine(Choose('Print Despatch Note' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.WindowOpen(clip('DespatchNote?var=' & RANDOM(1,9999999))&''&'','_blank'),,loc:disabled,'images/printer.png',,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonPrintDespatchNote  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonPrintDespatchNote:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:PrintDespatchNote') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('DespatchConfirmation_' & p_web._nocolon('buttonPrintDespatchNote') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:PrintDespatchNote') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('DespatchConfirmation_nexttab_' & 0)
    job:Account_Number = p_web.GSV('job:Account_Number')
    do ValidateValue::job:Account_Number
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Account_Number
      !do SendAlert
      do Comment::job:Account_Number ! allows comment style to be updated.
      !exit
    End
    locCompanyName = p_web.GSV('locCompanyName')
    do ValidateValue::locCompanyName
    If loc:Invalid
      loc:retrying = 1
      do Value::locCompanyName
      !do SendAlert
      do Comment::locCompanyName ! allows comment style to be updated.
      !exit
    End
    job:Charge_Type = p_web.GSV('job:Charge_Type')
    do ValidateValue::job:Charge_Type
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Charge_Type
      !do SendAlert
      do Comment::job:Charge_Type ! allows comment style to be updated.
      !exit
    End
    job:Repair_Type = p_web.GSV('job:Repair_Type')
    do ValidateValue::job:Repair_Type
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Repair_Type
      !do SendAlert
      do Comment::job:Repair_Type ! allows comment style to be updated.
      !exit
    End
    locCourierCost = p_web.GSV('locCourierCost')
    do ValidateValue::locCourierCost
    If loc:Invalid
      loc:retrying = 1
      do Value::locCourierCost
      !do SendAlert
      do Comment::locCourierCost ! allows comment style to be updated.
      !exit
    End
    locLabourCost = p_web.GSV('locLabourCost')
    do ValidateValue::locLabourCost
    If loc:Invalid
      loc:retrying = 1
      do Value::locLabourCost
      !do SendAlert
      do Comment::locLabourCost ! allows comment style to be updated.
      !exit
    End
    locInvoiceNumber = p_web.GSV('locInvoiceNumber')
    do ValidateValue::locInvoiceNumber
    If loc:Invalid
      loc:retrying = 1
      do Value::locInvoiceNumber
      !do SendAlert
      do Comment::locInvoiceNumber ! allows comment style to be updated.
      !exit
    End
    locPartsCost = p_web.GSV('locPartsCost')
    do ValidateValue::locPartsCost
    If loc:Invalid
      loc:retrying = 1
      do Value::locPartsCost
      !do SendAlert
      do Comment::locPartsCost ! allows comment style to be updated.
      !exit
    End
    locSubTotal = p_web.GSV('locSubTotal')
    do ValidateValue::locSubTotal
    If loc:Invalid
      loc:retrying = 1
      do Value::locSubTotal
      !do SendAlert
      do Comment::locSubTotal ! allows comment style to be updated.
      !exit
    End
    locInvoiceDate = p_web.GSV('locInvoiceDate')
    do ValidateValue::locInvoiceDate
    If loc:Invalid
      loc:retrying = 1
      do Value::locInvoiceDate
      !do SendAlert
      do Comment::locInvoiceDate ! allows comment style to be updated.
      !exit
    End
    locVAT = p_web.GSV('locVAT')
    do ValidateValue::locVAT
    If loc:Invalid
      loc:retrying = 1
      do Value::locVAT
      !do SendAlert
      do Comment::locVAT ! allows comment style to be updated.
      !exit
    End
    locTotal = p_web.GSV('locTotal')
    do ValidateValue::locTotal
    If loc:Invalid
      loc:retrying = 1
      do Value::locTotal
      !do SendAlert
      do Comment::locTotal ! allows comment style to be updated.
      !exit
    End
    job:Courier = p_web.GSV('job:Courier')
    do ValidateValue::job:Courier
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Courier
      !do SendAlert
      do Comment::job:Courier ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('DespatchConfirmation_nexttab_' & 1)
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_DespatchConfirmation_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('DespatchConfirmation_tab_' & 0)
    do GenerateTab0
  of lower('DespatchConfirmation_job:Courier_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Courier
      of event:timer
        do Value::job:Courier
        do Comment::job:Courier
      else
        do Value::job:Courier
      end
  of lower('DespatchConfirmation_buttonCreateInvoice_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonCreateInvoice
      of event:timer
        do Value::buttonCreateInvoice
        do Comment::buttonCreateInvoice
      else
        do Value::buttonCreateInvoice
      end
  of lower('DespatchConfirmation_tab_' & 1)
    do GenerateTab1
  of lower('DespatchConfirmation_buttonPrintWaybill_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonPrintWaybill
      of event:timer
        do Value::buttonPrintWaybill
        do Comment::buttonPrintWaybill
      else
        do Value::buttonPrintWaybill
      end
  of lower('DespatchConfirmation_buttonPrintDespatchNote_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonPrintDespatchNote
      of event:timer
        do Value::buttonPrintDespatchNote
        do Comment::buttonPrintDespatchNote
      else
        do Value::buttonPrintDespatchNote
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('DespatchConfirmation_form:ready_',1)

  p_web.SetSessionValue('DespatchConfirmation_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_DespatchConfirmation',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('DespatchConfirmation_form:ready_',1)
  p_web.SetSessionValue('DespatchConfirmation_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_DespatchConfirmation',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('DespatchConfirmation_form:ready_',1)
  p_web.SetSessionValue('DespatchConfirmation_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('DespatchConfirmation:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('DespatchConfirmation_form:ready_',1)
  p_web.SetSessionValue('DespatchConfirmation_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('DespatchConfirmation:Primed',0)
  p_web.setsessionvalue('showtab_DespatchConfirmation',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
  If p_web.GSV('Show:DespatchInvoiceDetails') = 1
      If not (1=0)
          If p_web.IfExistsValue('job:Courier')
            job:Courier = p_web.GetValue('job:Courier')
          End
      End
  End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('DespatchConfirmation_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  DO deletesessionvalues
  p_web.DeleteSessionValue('DespatchConfirmation_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
  If p_web.GSV('Show:DespatchInvoiceDetails') = 1
    loc:InvalidTab += 1
    do ValidateValue::job:Account_Number
    If loc:Invalid then exit.
    do ValidateValue::locCompanyName
    If loc:Invalid then exit.
    do ValidateValue::job:Charge_Type
    If loc:Invalid then exit.
    do ValidateValue::job:Repair_Type
    If loc:Invalid then exit.
    do ValidateValue::aLine
    If loc:Invalid then exit.
    do ValidateValue::textBillingDetails
    If loc:Invalid then exit.
    do ValidateValue::textInvoiceDetails
    If loc:Invalid then exit.
    do ValidateValue::locCourierCost
    If loc:Invalid then exit.
    do ValidateValue::locLabourCost
    If loc:Invalid then exit.
    do ValidateValue::locInvoiceNumber
    If loc:Invalid then exit.
    do ValidateValue::locPartsCost
    If loc:Invalid then exit.
    do ValidateValue::locSubTotal
    If loc:Invalid then exit.
    do ValidateValue::locInvoiceDate
    If loc:Invalid then exit.
    do ValidateValue::locVAT
    If loc:Invalid then exit.
    do ValidateValue::locTotal
    If loc:Invalid then exit.
    do ValidateValue::job:Courier
    If loc:Invalid then exit.
    do ValidateValue::buttonCreateInvoice
    If loc:Invalid then exit.
    do ValidateValue::buttonPaymentDetails
    If loc:Invalid then exit.
  End
  ! tab = 3
    loc:InvalidTab += 1
    do ValidateValue::buttonPrintWaybill
    If loc:Invalid then exit.
    do ValidateValue::buttonPrintDespatchNote
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('DespatchConfirmation:Primed',0)
  p_web.StoreValue('locCompanyName')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locCourierCost')
  p_web.StoreValue('locLabourCost')
  p_web.StoreValue('locInvoiceNumber')
  p_web.StoreValue('locPartsCost')
  p_web.StoreValue('locSubTotal')
  p_web.StoreValue('locInvoiceDate')
  p_web.StoreValue('locVAT')
  p_web.StoreValue('locTotal')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')

