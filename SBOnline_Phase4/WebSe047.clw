

   MEMBER('WebServer_Phase4.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSE047.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSE013.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE021.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE022.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE042.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE048.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE050.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE060.INC'),ONCE        !Req'd for module callout resolution
                     END


FormEstimateParts    PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
tmp:Location         STRING(30)                            !
tmp:ShelfLocation    STRING(30)                            !
tmp:SecondLocation   STRING(30)                            !
tmp:PurchaseCost     REAL                                  !
tmp:OutWarrantyCost  REAL                                  !
tmp:OutWarrantyMarkup REAL                                 !
tmp:ARCPart          BYTE                                  !
tmp:FaultCodesChecked BYTE                                 !
tmp:FaultCodes2      STRING(30)                            !
tmp:FaultCodes4      STRING(30)                            !
tmp:FaultCodes5      STRING(30)                            !
tmp:FaultCodes6      STRING(30)                            !
tmp:FaultCodes7      STRING(30)                            !
tmp:FaultCodes8      STRING(30)                            !
tmp:FaultCodes9      STRING(30)                            !
tmp:FaultCodes10     STRING(30)                            !
tmp:FaultCodes11     STRING(30)                            !
tmp:FaultCodes12     STRING(30)                            !
tmp:FaultCodes3      STRING(30)                            !
tmp:FaultCodes1      STRING(30)                            !
tmp:CreateOrder      BYTE                                  !
tmp:UnallocatePart   BYTE                                  !
locUserCode          STRING(3)                             !
locPartUsedOnRepair  BYTE                                  !
FilesOpened     Long
ESTPARTS::State  USHORT
STOMODEL::State  USHORT
STOCK::State  USHORT
SUPPLIER::State  USHORT
LOCATION::State  USHORT
CHARTYPE::State  USHORT
MANFAUPA::State  USHORT
MANFAULT::State  USHORT
MANFPALO::State  USHORT
DEFAULTS::State  USHORT
PARTS_ALIAS::State  USHORT
STOHIST::State  USHORT
epr:Order_Number:IsInvalid  Long
epr:Date_Ordered:IsInvalid  Long
epr:Date_Received:IsInvalid  Long
locOutFaultCode:IsInvalid  Long
epr:Part_Number:IsInvalid  Long
epr:Description:IsInvalid  Long
epr:Despatch_Note_Number:IsInvalid  Long
epr:Quantity:IsInvalid  Long
tmp:Location:IsInvalid  Long
tmp:SecondLocation:IsInvalid  Long
tmp:ShelfLocation:IsInvalid  Long
tmp:PurchaseCost:IsInvalid  Long
tmp:OutWarrantyCost:IsInvalid  Long
tmp:OutWarrantyMarkup:IsInvalid  Long
tmp:FixedPrice:IsInvalid  Long
epr:Supplier:IsInvalid  Long
epr:Exclude_From_Order:IsInvalid  Long
epr:PartAllocated:IsInvalid  Long
tmp:UnallocatePart:IsInvalid  Long
text:OrderRequired:IsInvalid  Long
text:OrderRequired2:IsInvalid  Long
tmp:CreateOrder:IsInvalid  Long
tmp:FaultCodesChecked:IsInvalid  Long
tmp:FaultCodes1:IsInvalid  Long
tmp:FaultCode2:IsInvalid  Long
tmp:FaultCode3:IsInvalid  Long
tmp:FaultCode4:IsInvalid  Long
tmp:FaultCode5:IsInvalid  Long
tmp:FaultCode6:IsInvalid  Long
tmp:FaultCode7:IsInvalid  Long
tmp:FaultCode8:IsInvalid  Long
tmp:FaultCode9:IsInvalid  Long
tmp:FaultCode10:IsInvalid  Long
tmp:FaultCode11:IsInvalid  Long
tmp:FaultCode12:IsInvalid  Long
epr:UsedOnRepair:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
local       Class
AfterFaultCodeLookup Procedure(Long fNumber)
SetLookupButton      Procedure(Long fNumber)
            End
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormEstimateParts')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'FormEstimateParts_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('FormEstimateParts','')
    p_web.DivHeader('FormEstimateParts',p_web.combine(p_web.site.style.formdiv,'fdiv'))
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('FormEstimateParts') = 0
        p_web.AddPreCall('FormEstimateParts')
        p_web.DivHeader('popup_FormEstimateParts','nt-hidden')
        p_web.DivHeader('FormEstimateParts',p_web.combine(p_web.site.style.formdiv,'fdiv'))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_FormEstimateParts_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_FormEstimateParts_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormEstimateParts',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:StagePost
    do RestoreMem
    do PostInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_FormEstimateParts',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormEstimateParts',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:StagePost
    do RestoreMem
    do PostCopy
  of Net:CopyRecord + NET:WEB:Populate
    If p_web.IfExistsValue('epr:Record_Number') = 0 then p_web.SetValue('epr:Record_Number',p_web.GSV('epr:Record_Number')).
    do PreCopy
    p_web.setsessionvalue('showtab_FormEstimateParts',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormEstimateParts',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormEstimateParts',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do PostInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do PostUpdate
    End
  of Net:ChangeRecord + NET:WEB:Populate
    If p_web.IfExistsValue('epr:Record_Number') = 0 then p_web.SetValue('epr:Record_Number',p_web.GSV('epr:Record_Number')).
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_FormEstimateParts',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormEstimateParts',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:DeleteRecord + NET:WEB:StagePost
    do RestoreMem
    do PostDelete
  of Net:ViewRecord + NET:WEB:Populate
    If p_web.IfExistsValue('epr:Record_Number') = 0 then p_web.SetValue('epr:Record_Number',p_web.GSV('epr:Record_Number')).
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_FormEstimateParts',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormEstimateParts',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormEstimateParts',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormEstimateParts',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('FormEstimateParts')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
buildFaultCodes    Routine
data
locFoundFault      Byte(0)
code
    ! Clear Variables
    if (p_web.GSV('FormEstimateParts:FirstTime') = 0)
        loop x# = 1 To 12
            p_web.SSV('Hide:PartFaultCode' & x#,1)
            p_web.SSV('Req:PartFaultCode' & x#,0)
            p_web.SSV('ReadOnly:PartFaultCode' & x#,0)
            p_web.SSV('Prompt:PartFaultCode' & x#,'Fault Code ' & x#)
            p_web.SSV('Picture:PartFaultCode' & x#,'@s30')
            p_web.SSV('ShowDate:PartFaultCode' & x#,0)
            p_web.SSV('Lookup:PartFaultCode' & x#,0)
            p_web.SSV('Comment:PartFaultCode' & x#,'')
        end ! loop x# = 1 To 12
        p_web.SSV('Hide:FaultCodesChecked',1)
    end ! if (p_web.GSV('FormEstimateParts:FirstTime') = 0)

    locMainFaultOnly# = 0
    Access:MANFAUPA.Clearkey(map:MainFaultKey)
    map:Manufacturer    = p_web.GSV('job:Manufacturer')
    map:MainFault    = 1
    if (Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign)
        ! Found
        if (sto:Accessory <> 'YES' and man:ForceAccessoryCode)
            !Main Fault Only
            locMainFaultOnly# = 1
        end ! if (sto:Accessory <> 'YES' and man:ForceAccessoryCode)
    else ! if (Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign)
        ! Error
    end ! if (Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign)

    Access:MANFAUPA.Clearkey(map:ScreenOrderKey)
    map:Manufacturer    = p_web.GSV('job:Manufacturer')
    map:ScreenOrder    = 0
    set(map:ScreenOrderKey,map:ScreenOrderKey)
    loop
        if (Access:MANFAUPA.Next())
            Break
        end ! if (Access:MANFAUPA.Next())
        if (map:Manufacturer    <> p_web.GSV('job:Manufacturer'))
            Break
        end ! if (map:Manufacturer    <> p_web.GSV('job:Manufacturer'))

        if (map:ScreenOrder = 0)
            cycle
        end ! if (map:ScreenOrder = 0)

        if (locMainFaultOnly# = 1)
            if (map:MainFault = 0)
                cycle
            end ! if (map:MainFault = 0)
        end ! if (locMainFaultOnly# = 1)

        p_web.SSV('Hide:PartFaultCode' & map:ScreenOrder,0)
        p_web.SSV('Prompt:PartFaultCode' & map:ScreenOrder,map:Field_Name)

        ! #11659 Don't force estimate fault codes (Bryan: 23/08/2010)
!        if (map:Compulsory = 'YES')
!            if (p_web.GSV('epr:Adjustment') = 'YES')
!                if (map:CompulsoryForAdjustment)
!                    p_web.SSV('Req:PartFaultCode' & map:ScreenOrder,1)
!                    p_web.SSV('Comment:PartFaultCode' & map:ScreenOrder,'Required')
!                end ! if (map:CompulsoryForAdjustment)
!            else !if (p_web.GSV('epr:Adjustment') = 'YES')
!                p_web.SSV('Req:PartFaultCode' & map:ScreenOrder,1)
!                p_web.SSV('Comment:PartFaultCode' & map:ScreenOrder,'Required')
!            end ! if (p_web.GSV('epr:Adjustment') = 'YES')
!        else ! if (map:Compulsory = 'YES')
!            p_web.SSV('Req:PartFaultCode' & map:ScreenOrder,0)
!        end ! if (map:Compulsory = 'YES')

        if (map:MainFault)
            !This is the main fault, use the job main fault
            Access:MANFAULT.Clearkey(maf:MainFaultKey)
            maf:Manufacturer    = p_web.GSV('job:Manufacturer')
            maf:MainFault    = 1
            if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)
                ! Found
                case maf:Field_Type
                of 'DATE'
                    p_web.SSV('Picture:PartFaultCode' & map:ScreenOrder,clip(maf:DateType))
                    ! Date Lookup Required
                of 'STRING'
                    if (maf:RestrictLength)
                        p_web.SSV('Picture:PartFaultCode' & map:ScreenOrder,'@s' & maf:LengthTo)
                    end !~ if (maf:RestrictLength)

                    ! Lookup Required
                of 'NUMBER'
                    if (maf:RestrictLength)
                        p_web.SSV('Picture:PartFaultCode' & map:ScreenOrder,'@n_' & maf:LengthTo)
                    else !end !~ if (maf:RestrictLength)
                        p_web.SSV('Picture:PartFaultCode' & map:ScreenOrder,'@n_9')
                    end !~ if (maf:RestrictLength)
                end ! case maf:Field_Type

                if (maf:Lookup = 'YES')
                    p_web.SSV('Lookup:PartFaultCode' & map:ScreenOrder,1)
                end ! if (map:Lookup = 'YES')
            else ! if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)
                ! Error
            end ! if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)
        else ! if (map:MainFault)
            case map:Field_Type
            of 'DATE'
                p_web.SSV('Picture:PartFaultCode' & map:ScreenOrder,clip(map:DateType))
                p_web.SSV('ShowDate:PartFaultCode' & map:ScreenOrder,1)
                ! Date Lookup Required
            of 'STRING'
                if (map:RestrictLength)
                    p_web.SSV('Picture:PartFaultCode' & map:ScreenOrder,'@s' & map:LengthTo)
                end !~ if (maf:RestrictLength)

                ! Lookup Required
            of 'NUMBER'
                if (map:RestrictLength)
                    p_web.SSV('Picture:PartFaultCode' & map:ScreenOrder,'@n_' & map:LengthTo)
                else !end !~ if (maf:RestrictLength)
                    p_web.SSV('Picture:PartFaultCode' & map:ScreenOrder,'@n_9')
                end !~ if (maf:RestrictLength)
            end ! case maf:Field_Type
        end !if (map:MainFault)

        if (map:Lookup = 'YES')
            p_web.SSV('Lookup:PartFaultCode' & map:ScreenOrder,1)

        end ! if (map:Lookup = 'YES')

        if (map:NotAvailable = 1)
            if (p_web.GSV('tmp:FaultCode' & map:ScreenOrder) = '')
                p_web.SSV('Hide:PartFaultCode' & map:ScreenOrder,1)
            else ! if (p_web.GSV('tmp:FaultCode' & map:ScreenOrder) = '')
                p_web.SSV('ReadOnly:PartFaultCode' & map:ScreenOrder,1)
            end ! if (p_web.GSV('tmp:FaultCode' & map:ScreenOrder) = '')
        end ! if (map:NotAvailable = 1)

        if (map:CopyFromJobFaultCode And p_web.GSV('tmp:FaulCode' & map:ScreenOrder) = '')
            if (map:ScreenOrder < 13)
                p_web.SSV('tmp:FaultCode' & map:ScreenOrder,p_web.GSV('job:Fault_Code' & map:CopyJobFaultCode))
            else ! if (map:ScreenOrder < 13)
                p_web.SSV('tmp:FaultCode' & map:ScreenOrder,p_web.GSV('wob:FaultCode' & map:CopyJobFaultCode))
            end ! if (map:ScreenOrder < 13)
        end ! if (map:CopyFromJobFaultCode And p_web.GSV('tmp:FaulCode' & map:ScreenOrder) = '')

        if (p_web.GSV('tmp:FaultCode' & map:ScreenOrder) = '' And sto:Assign_Fault_Codes = 'YES')
            Access:STOMODEL.Clearkey(stm:Model_Number_Key)
            stm:Ref_Number    = sto:Ref_Number
            stm:Manufacturer    = p_web.GSV('job:Manufacturer')
            stm:Model_Number    = p_web.GSV('job:Model_Number')
            if (Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign)
                ! Found
                Case map:Field_Number
                Of 1
                    If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode1)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 2
                    If stm:FaultCode2 <> '' And stm:FaultCode2 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode2)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 3
                    If stm:FaultCode3 <> '' And stm:FaultCode3 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode3)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 4
                    If stm:FaultCode4 <> '' And stm:FaultCode4 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode4)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 5
                    If stm:FaultCode5 <> '' And stm:FaultCode5 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode5)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 6
                    If stm:FaultCode6 <> '' And stm:FaultCode6 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode6)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 7
                    If stm:FaultCode7 <> '' And stm:FaultCode7 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode7)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 8
                    If stm:FaultCode8 <> '' And stm:FaultCode8 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode8)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 9
                    If stm:FaultCode9 <> '' And stm:FaultCode9 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode9)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 10
                    If stm:FaultCode10 <> '' And stm:FaultCode10 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode10)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 11
                    If stm:FaultCode11 <> '' And stm:FaultCode11 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode11)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 12
                    If stm:FaultCode12 <> '' And stm:FaultCode12 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode12)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                End ! Case map:Field_Number
            else ! if (Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign)
                ! Error
            end ! if (Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign)
        end ! if (p_web.GSV('tmp:FaultCode' & map:ScreenOrder) = '' And sto:Assign_Fault_Codes = 'YES')
        locFoundFault = 1
    end ! loop

    !Check if the part main fault is set to assign value to anouther fault code

    Access:MANFAUPA.Clearkey(map:MainFaultKey)
    map:Manufacturer    = p_web.GSV('job:Manufacturer')
    map:MainFault    = 1
    if (Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign)
        ! Found
        Access:MANFPALO.Clearkey(mfp:Field_Key)
        mfp:Manufacturer    = p_web.GSV('job:Manufacturer')
        mfp:Field_Number    = map:Field_Number
        mfp:Field    = p_web.GSV('tmp:FaultCode' & map:ScreenOrder)
        if (Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign)
            ! Found
            if (mfp:SetPartFaultCode)
                Access:MANFAUPA.Clearkey(map:Field_Number_Key)
                map:Manufacturer    = p_web.GSV('job:Manufacturer')
                map:Field_Number    = mfp:SelectPartFaultCode
                if (Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign)
                    ! Found
                    if (p_web.GSV('Hide:PartFaultCode' & map:ScreenOrder) <> 1 And |
                       p_web.GSV('tmp:FaultCode' & map:ScreenOrder) = '')
                       p_web.SSV('tmp:FaultCode' & map:ScreenOrder,mfp:PartFaultCodeValue)
                    end ! if (p_web.GSV('Hide:PartFaultCode' & map:ScreenOrder) <> 1 And |
                else ! if (Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign)
                    ! Error
                end ! if (Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign)
            end ! if (mfp:SetPartFaultCode)
        else ! if (Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign)
            ! Error
        end ! if (Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign)
    else ! if (Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign)
        ! Error
    end ! if (Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign)

    if (locFoundFault)
        p_web.SSV('Hide:FaultCodesChecked',0)
    end ! if (locFoundFault)
deleteSessionValues     Routine
    p_web.deleteSessionValue('adjustment')
    p_web.deleteSessionValue('FormEstimateParts:FirstTime')
    p_web.deleteSessionValue('locOrderRequired')
    p_web.deleteSessionValue('locOutFaultCode')

    p_web.deleteSessionValue('ReadOnly:OutWarrantyMarkup')
    p_web.deleteSessionValue('ReadOnly:PurchaseCost')
    p_web.deleteSessionValue('ReadOnly:OutWarrantyCost')
    p_web.deleteSessionValue('ReadOnly:Quantity')

    p_web.deleteSessionValue('Parts:ViewOnly')

    p_web.deleteSessionValue('Show:UnallocatePart')
enableDisableCosts    Routine
    p_web.SSV('tmp:FixedPrice','')
    if (p_web.GSV('tmp:OutWarrantyMarkup') = 0)
        p_web.SSV('ReadOnly:OutWarrantyCost',0)
    else ! if (p_web.GSV('tmp:OutWarrantyMarkup') = 0)
        p_web.SSV('ReadOnly:OutWarrantyCost',1)
    end ! if (p_web.GSV('tmp:OutWarrantyMarkup') = 0)

    if (p_web.GSV('job:Invoice_Number') = 0)
        if (p_web.GSV('tmp:InWarrantyMarkup') > 0)
            p_web.SSV('tmp:InWarrantyCost',VodacomClass.Markup(p_web.GSV('tmp:InWarrantyCost'),|
                                                p_web.GSV('tmp:PurchaseCost'),|
                                                p_web.GSV('tmp:InWarrantyMarkup')))
        end ! if (p_web.GSV('tmp:InWarrantyMarkup') > 0)
        if (p_web.GSV('tmp:OutWarrantyMarkup') > 0)
            p_web.SSV('tmp:OutWarrantyCost',VOdacomClass.Markup(p_web.GSV('tmp:OutWarrantyCost'),|
                                                p_web.GSV('tmp:PurchaseCost'),|
                                                p_web.GSV('tmp:OutWarrantyMarkup')))
        end ! if (p_web.GSV('tmp:OutWarrantyMarkup') > 0)
    end ! if (p_web.GSV('job:Invoice_Number') = 0)

    Access:CHARTYPE.Clearkey(cha:Charge_Type_Key)
    cha:Charge_Type    = p_web.GSV('job:Charge_Type')
    if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)
        ! Found
        if (cha:Zero_Parts_ARC)
           if (p_web.GSV('tmp:ARCPart') = 1)
               p_web.SSV('tmp:FixedPrice','FIXED PRICE')
               p_web.SSV('ReadOnly:OutWarrantyMarkup',1)
               p_web.SSV('ReadOnly:OutWarrantyCost',1)
               p_web.SSV('tmp:OutWarrantyCost',0)
           end ! if (p_web.GSV('tmp:ARCPart'))
        end ! if (cha:Zero_Parts_ARC)

        if (cha:Zero_Parts = 'YES')
           if (p_web.GSV('tmp:ARCPart') = 0)
               p_web.SSV('tmp:FixedPrice','FIXED PRICE')
               p_web.SSV('ReadOnly:OutWarrantyMarkup',1)
               p_web.SSV('ReadOnly:OutWarrantyCost',1)
               p_web.SSV('tmp:OutWarrantyCost',0)
           end ! if (p_web.GSV('tmp:ARCPart'))
        end ! if (cha:Zero_Parts = 'YES')
    else ! if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)
        ! Error
    end ! if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)
lookupLocation    Routine
    if (p_web.GSV('epr:Part_Ref_Number') <> '')
        Access:STOCK.Clearkey(sto:Ref_Number_Key)
        sto:Ref_Number    = p_web.GSV('epr:Part_Ref_Number')
        if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
            ! Found
            p_web.SSV('tmp:Location',sto:Location)
            p_web.SSV('tmp:ShelfLocation',sto:Shelf_Location)
            p_web.SSV('tmp:SecondLocation',sto:Second_Location)
        else ! if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
            ! Error
            p_web.SSV('tmp:Location','')
            p_web.SSV('tmp:ShelfLocation','')
            p_web.SSV('tmp:SecondLocation','')
        end ! if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
    else ! if (p_web.GSV('epr:Part_Ref_Number') <> '')
        p_web.SSV('tmp:Location','')
        p_web.SSV('tmp:ShelfLocation','')
        p_web.SSV('tmp:SecondLocation','')
    end ! if (p_web.GSV('epr:Part_Ref_Number') <> '')
lookupMainFault  Routine
    p_web.SSV('locOutFaultCode','')
    Access:MANFAUPA.Clearkey(map:MainFaultKey)
    map:Manufacturer    = p_web.GSV('job:Manufacturer')
    map:MainFault    = 1
    if (Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign)
        ! Found
        Access:MANFAULT.Clearkey(maf:MainFaultKey)
        maf:Manufacturer    = p_web.GSV('job:Manufacturer')
        maf:MainFault    = 1
        if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)
            ! Found

            Access:MANFAULO.Clearkey(mfo:Field_Key)
            mfo:Manufacturer    = p_web.GSV('job:Manufacturer')
            mfo:Field_Number    = maf:Field_Number
            mfo:Field    = p_web.GSV('tmp:FaultCodes' & map:ScreenOrder)
            if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                ! Found
                p_web.SSV('locOutFaultCode','Out Fault Code: ' & mfo:Description)
            else ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                ! Error
            end ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)

        else ! if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)
            ! Error
        end ! if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)

    else ! if (Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign)
        ! Error
    end ! if (Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign)
showCosts      Routine
    Access:STOCK.Clearkey(sto:Ref_Number_Key)
    sto:Ref_Number    = p_web.GSV('epr:Part_Ref_Number')
    if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
        ! Found
        if (sto:Location <> p_web.GSV('ARC:SiteLocation'))
            p_web.SSV('tmp:ARCPart',0)
            if (p_web.GSV('BookingSite') <> 'RRC')
                if (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'AMEND RRC PART'))
                    p_web.SSV('Parts:ViewOnly',1)
                end ! if (SecurityCheckFailed(p_web.GSV('BookingUser'),'AMEND RRC PART'))
            end ! if (p_web.GSV('BookingSite') <> 'RRC')
        else ! if (sto:Location <> p_web.GSV('Default:SiteLocation'))
            p_web.SSV('tmp:ARCPart',1)
            if (p_web.GSV('BookingSite') <> 'ARC')
                p_web.SSV('Parts:ViewOnly',1)
            end ! if (p_web.GSV('BookingSite') <> 'ARC')
        end ! if (sto:Location <> p_web.GSV('Default:SiteLocation'))
    else ! if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
        ! Error
    end ! if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
    if (p_web.GSV('tmp:ARCPart') = 1)
        p_web.SSV('tmp:PurchaseCost',p_web.GSV('epr:AveragePurchaseCost'))
        p_web.SSV('tmp:InWarrantyCost',p_web.GSV('epr:Purchase_Cost'))
        p_web.SSV('tmp:OutWarrantyCost',p_web.GSV('epr:Sale_Cost'))
        p_web.SSV('tmp:InWarrantyMarkup',p_web.GSV('epr:InWarrantyMarkup'))
        p_web.SSV('tmp:OutWarrantyMarkup',p_web.GSV('epr:OutWarrantyMarkup'))
    else ! if (tmp:ARCPart)
        if (p_web.GSV('epr:RRCAveragePurchaseCost') > 0)
            p_web.SSV('tmp:PurchaseCost',p_web.GSV('epr:RRCAveragePurchaseCost'))
        else ! if (p_web.GSV('epr:RRCAveragePurchaseCost') > 0)
            p_web.SSV('tmp:PurchaseCost',p_web.GSV('epr:RRCPurchaseCost'))
        end ! if (p_web.GSV('epr:RRCAveragePurchaseCost') > 0)

        p_web.SSV('tmp:InWarrantyCost',p_web.GSV('epr:RRCPurchaseCost'))
        p_web.SSV('tmp:OutWarrantyCost',p_web.GSV('epr:RRCSaleCost'))
        p_web.SSV('tmp:InWarrantyMarkup',p_web.GSV('epr:RRCInWarrantyMarkup'))
        p_web.SSV('tmp:OutWarrantyMarkup',p_web.GSV('epr:RRCOutWarrantyMarkup'))
    end !if (tmp:ARCPart)
UpdateComments    Routine
    loop x# = 1 to 12
        if (p_web.GSV('Hide:PartFaultCode' & x#) = 1)
            cycle
        end ! if (p_web.GSV('Hide:PartFaultCode' & x#) = 1)
        Access:MANFAUPA.Clearkey(map:ScreenOrderKey)
        map:Manufacturer    = p_web.GSV('job:Manufacturer')
        map:ScreenOrder    = x#
        if (Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign)
            ! Found
            if (map:MainFault)
                Access:MANFAULT.Clearkey(maf:MainFaultKey)
                maf:Manufacturer    = p_web.GSV('job:Manufacturer')
                maf:MainFault    = 1
                if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)
                    ! Found
                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                    mfo:Manufacturer    = p_web.GSV('job:Manufacturer')
                    mfo:Field_Number    = maf:Field_Number
                    mfo:Field    = p_web.GSV('tmp:FaultCodes' & x#)
                    if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                        ! Found
                        p_web.SSV('Comment:PartFaultCode' & x#,mfo:Description)
                    else ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                        ! Error
                        p_web.SSV('Comment:PartFaultCode' & x#,'')
                    end ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                else ! if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)
                    ! Error
                end ! if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)
            else ! if (map:MainFault)
                Access:MANFPALO.Clearkey(mfp:Field_Key)
                mfp:Manufacturer    = p_web.GSV('job:Manufacturer')
                mfp:Field_Number    = map:Field_Number
                mfp:Field    = p_web.GSV('tmp:FaultCodes' & x#)
                if (Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign)
                    ! Found
                    p_web.SSV('Comment:PartFaultCode' & x#,mfp:Description)
                else ! if (Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign)
                    ! Error
                    p_web.SSV('Comment:PartFaultCode' & x#,'')
                end ! if (Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign)
            end ! if (map:MainFault)
        else ! if (Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign)
            ! Error
        end ! if (Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign)
    end ! loop x# = 1 to 12


updatePartDetails      routine
! Update Part Details
    Access:STOCK.Clearkey(sto:Ref_Number_Key)
    sto:Ref_Number    = stm:Ref_Number
    if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
        ! Found
    else ! if (Access:STOCK.TryFetch(sto:Ref_Number) = Level:Benign)
        ! Error
    end ! if (Access:STOCK.TryFetch(sto:Ref_Number) = Level:Benign)

    Access:LOCATION.Clearkey(loc:Location_Key)
    loc:Location    = sto:Location
    if (Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign)
        ! Found
    else ! if (Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign)
        ! Error
    end ! if (Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign)

    p_web.SSV('epr:Description',stm:Description)
    p_web.SSV('epr:Part_Ref_Number',stm:Ref_Number)
    p_web.SSV('epr:Supplier',sto:Supplier)
    p_web.SSV('epr:Purchase_Cost',sto:Purchase_Cost)
    p_web.SSV('epr:Sale_Cost',sto:Sale_Cost)
    p_web.SSV('epr:Retail_Cost',sto:Retail_Cost)
    p_web.SSV('epr:InWarrantyMarkup',sto:PurchaseMarkup)
    p_web.SSV('epr:OutWarrantyMarkup',sto:Percentage_Mark_Up)

    if (p_web.GSV('BookingSite') = 'RRC')
        p_web.SSV('epr:RRCAveragePurchaseCost',sto:AveragePurchaseCost)
        p_web.SSV('epr:PurchaseCost',sto:Purchase_Cost)
        p_web.SSV('epr:RRCSaleCost',sto:Sale_Cost)

        Access:CHARTYPE.Clearkey(cha:Charge_Type_Key)
        cha:Charge_Type    = p_web.GSV('job:Charge_Type')
        if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)
            ! Found
            if (cha:Zero_Parts = 'YES')
                p_web.SSV('epr:RRCSaleCost',0)
            end ! if (cha:Zero_Parts = 'YES')
        else ! if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)
            ! Error
        end ! if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)

        p_web.SSV('epr:RRCInWarrantyMarkup',sto:PurchaseMarkUp)
        p_web.SSV('epr:RRCOutWarrantyMarkup',sto:Percentage_Mark_Up)
        p_web.SSV('epr:Purchase_Cost',epr:RRCPurchaseCost)
        p_web.SSV('epr:Sale_Cost',epr:RRCSaleCost)
        p_web.SSV('epr:AveragePurchaseCost',epr:RRCAveragePurchaseCost)
    end ! if (p_web.GSV('BookingSite') = 'RRC')

    if (p_web.GSV('BookingSite') = 'ARC')
        p_web.SSV('epr:AveragePurchaseCost',sto:AveragePurchaseCost)
        p_web.SSV('epr:Purchase_Cost',sto:Purchase_Cost)
        p_web.SSV('epr:Sale_Code',sto:Sale_Cost)

        Access:CHARTYPE.Clearkey(cha:Charge_Type_Key)
        cha:Charge_Type    = p_web.GSV('job:Charge_Type')
        if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)
            ! Found
            if (cha:Zero_Parts_ARC)
                p_web.SSV('epr:Sale_Cost',0)
            end ! if (cha:Zero_Parts = 'YES')
        else ! if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)
            ! Error
        end ! if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)

        p_web.SSV('epr:RRCPurchaseCost',0)
        p_web.SSV('epr:RRCSaleCost',0)

        if (p_web.GSV('jobe:WebJob') = 1)
            p_web.SSV('epr:RRCAveragePurchaseCost',epr:Sale_Cost)
            p_web.SSV('epr:RRCPurchaseCost',VodacomClass.Markup(p_web.GSV('epr:RRCPurchaseCost'),|
                                                p_web.GSV('epr:RRCAveragePurchaseCost'),|
                                                InWarrantyMarkup(p_web.GSV('job:Manufacturer'),|
                                                sto:Location)))
            p_web.SSV('epr:RRCSaleCost',VodacomClass.Markup(p_web.GSV('epr:RRCSaleCost'),|
                                            p_web.GSV('epr:RRCAveragePurchaseCost'),|
                                            loc:OutWarrantyMarkup))
            p_web.SSV('epr:RRCInWarrantyMarkup',p_web.GSV('epr:InWarrantyMarkup'))
            p_web.SSV('epr:RRCOutWarrantyMarkup',p_web.GSV('epr:OutWarrantyMarkup'))
        end ! if (p_web.GSV('jobe:WebJob') = 1)
    end ! if (p_web.GSV('BookingSite') = 'ARC')

    if (sto:Assign_Fault_Codes = 'YES')
        p_web.SSV('tmp:FaultCode1',stm:FaultCode1)
        p_web.SSV('tmp:FaultCode2',stm:FaultCode2)
        p_web.SSV('tmp:FaultCode3',stm:FaultCode3)
        p_web.SSV('tmp:FaultCode4',stm:FaultCode4)
        p_web.SSV('tmp:FaultCode5',stm:FaultCode5)
        p_web.SSV('tmp:FaultCode6',stm:FaultCode6)
        p_web.SSV('tmp:FaultCode7',stm:FaultCode7)
        p_web.SSV('tmp:FaultCode8',stm:FaultCode8)
        p_web.SSV('tmp:FaultCode9',stm:FaultCode9)
        p_web.SSV('tmp:FaultCode10',stm:FaultCode10)
        p_web.SSV('tmp:FaultCode11',stm:FaultCode11)
        p_web.SSV('tmp:FaultCode12',stm:FaultCode12)
    end ! if (sto:Assign_Fault_Codes = 'YES')

    p_web.SSV('epr:Part_Ref_Number',sto:Ref_Number)
    p_web.SSV('locOrderRequired',0)

    do ShowCosts
    do lookupLocation
    do enableDisableCosts
OpenFiles  ROUTINE
  p_web._OpenFile(ESTPARTS)
  p_web._OpenFile(STOMODEL)
  p_web._OpenFile(STOCK)
  p_web._OpenFile(SUPPLIER)
  p_web._OpenFile(LOCATION)
  p_web._OpenFile(CHARTYPE)
  p_web._OpenFile(MANFAUPA)
  p_web._OpenFile(MANFAULT)
  p_web._OpenFile(MANFPALO)
  p_web._OpenFile(DEFAULTS)
  p_web._OpenFile(PARTS_ALIAS)
  p_web._OpenFile(STOHIST)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(ESTPARTS)
  p_Web._CloseFile(STOMODEL)
  p_Web._CloseFile(STOCK)
  p_Web._CloseFile(SUPPLIER)
  p_Web._CloseFile(LOCATION)
  p_Web._CloseFile(CHARTYPE)
  p_Web._CloseFile(MANFAUPA)
  p_Web._CloseFile(MANFAULT)
  p_Web._CloseFile(MANFPALO)
  p_Web._CloseFile(DEFAULTS)
  p_Web._CloseFile(PARTS_ALIAS)
  p_Web._CloseFile(STOHIST)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
      !Initialize
      do buildFaultCodes
  
      p_web.SSV('Comment:OutWarrantyMarkup','')
      p_web.SSV('Comment:PartNumber','Required')
  p_web.SetValue('FormEstimateParts_form:inited_',1)
  p_web.formsettings.file = 'ESTPARTS'
  p_web.formsettings.key = 'epr:record_number_key'
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = 'ESTPARTS'
    p_web.formsettings.key = 'epr:record_number_key'
      clear(p_web.formsettings.FieldName)
    p_web.formsettings.recordid[1] = epr:Record_Number
    p_web.formsettings.FieldName[1] = 'epr:Record_Number'
    do SetAction
    if p_web.GetSessionValue('FormEstimateParts:Primed') = 1
      p_web.formsettings.action = Net:ChangeRecord
    Else
      p_web.formsettings.action = Loc:Act
    End
    p_web.formsettings.OriginalAction = Loc:Act
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'FormEstimateParts'
    end
    p_web.formsettings.proc = 'FormEstimateParts'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine
  IF p_web.GetSessionValue('FormEstimateParts:Primed') = 1
    p_web._deleteFile(ESTPARTS)
    p_web.SetSessionValue('FormEstimateParts:Primed',0)
  End
      do deleteSessionValues
  

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine
  p_web.SetValue('UpdateFile','ESTPARTS')
  p_web.SetValue('UpdateKey','epr:record_number_key')
  If p_web.IfExistsValue('epr:Order_Number')
    p_web.SetPicture('epr:Order_Number','@n08b')
  End
  p_web.SetSessionPicture('epr:Order_Number','@n08b')
  If p_web.IfExistsValue('epr:Date_Ordered')
    p_web.SetPicture('epr:Date_Ordered','@d6b')
  End
  p_web.SetSessionPicture('epr:Date_Ordered','@d6b')
  If p_web.IfExistsValue('epr:Date_Received')
    p_web.SetPicture('epr:Date_Received','@d6b')
  End
  p_web.SetSessionPicture('epr:Date_Received','@d6b')
  If p_web.IfExistsValue('epr:Part_Number')
    p_web.SetPicture('epr:Part_Number','@s30')
  End
  p_web.SetSessionPicture('epr:Part_Number','@s30')
  If p_web.IfExistsValue('epr:Description')
    p_web.SetPicture('epr:Description','@s30')
  End
  p_web.SetSessionPicture('epr:Description','@s30')
  If p_web.IfExistsValue('epr:Despatch_Note_Number')
    p_web.SetPicture('epr:Despatch_Note_Number','@s30')
  End
  p_web.SetSessionPicture('epr:Despatch_Note_Number','@s30')
  If p_web.IfExistsValue('epr:Quantity')
    p_web.SetPicture('epr:Quantity','@n4')
  End
  p_web.SetSessionPicture('epr:Quantity','@n4')
  If p_web.IfExistsValue('tmp:PurchaseCost')
    p_web.SetPicture('tmp:PurchaseCost','@n_14.2')
  End
  p_web.SetSessionPicture('tmp:PurchaseCost','@n_14.2')
  If p_web.IfExistsValue('tmp:OutWarrantyCost')
    p_web.SetPicture('tmp:OutWarrantyCost','@n_14.2')
  End
  p_web.SetSessionPicture('tmp:OutWarrantyCost','@n_14.2')
  If p_web.IfExistsValue('tmp:OutWarrantyMarkup')
    p_web.SetPicture('tmp:OutWarrantyMarkup','@n3')
  End
  p_web.SetSessionPicture('tmp:OutWarrantyMarkup','@n3')
  If p_web.IfExistsValue('epr:Supplier')
    p_web.SetPicture('epr:Supplier','@s30')
  End
  p_web.SetSessionPicture('epr:Supplier','@s30')
  If p_web.IfExistsValue('tmp:FaultCodes1')
    p_web.SetPicture('tmp:FaultCodes1',p_web.GSV('Picture:PartFaultCode1'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes1',p_web.GSV('Picture:PartFaultCode1'))
  If p_web.IfExistsValue('tmp:FaultCodes2')
    p_web.SetPicture('tmp:FaultCodes2',p_web.GSV('Picture:PartFaultCode2'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes2',p_web.GSV('Picture:PartFaultCode2'))
  If p_web.IfExistsValue('tmp:FaultCodes3')
    p_web.SetPicture('tmp:FaultCodes3',p_web.GSV('Picture:PartFaultCode3'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes3',p_web.GSV('Picture:PartFaultCode3'))
  If p_web.IfExistsValue('tmp:FaultCodes4')
    p_web.SetPicture('tmp:FaultCodes4',p_web.GSV('Picture:PartFaultCode4'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes4',p_web.GSV('Picture:PartFaultCode4'))
  If p_web.IfExistsValue('tmp:FaultCodes5')
    p_web.SetPicture('tmp:FaultCodes5',p_web.GSV('Picture:PartFaultCode5'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes5',p_web.GSV('Picture:PartFaultCode5'))
  If p_web.IfExistsValue('tmp:FaultCodes6')
    p_web.SetPicture('tmp:FaultCodes6',p_web.GSV('Picture:PartFaultCode6'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes6',p_web.GSV('Picture:PartFaultCode6'))
  If p_web.IfExistsValue('tmp:FaultCodes7')
    p_web.SetPicture('tmp:FaultCodes7',p_web.GSV('Picture:PartFaultCode7'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes7',p_web.GSV('Picture:PartFaultCode7'))
  If p_web.IfExistsValue('tmp:FaultCodes8')
    p_web.SetPicture('tmp:FaultCodes8',p_web.GSV('Picture:PartFaultCode8'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes8',p_web.GSV('Picture:PartFaultCode8'))
  If p_web.IfExistsValue('tmp:FaultCodes9')
    p_web.SetPicture('tmp:FaultCodes9',p_web.GSV('Picture:PartFaultCode9'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes9',p_web.GSV('Picture:PartFaultCode9'))
  If p_web.IfExistsValue('tmp:FaultCodes10')
    p_web.SetPicture('tmp:FaultCodes10',p_web.GSV('Picture:PartFaultCode10'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes10',p_web.GSV('Picture:PartFaultCode10'))
  If p_web.IfExistsValue('tmp:FaultCodes11')
    p_web.SetPicture('tmp:FaultCodes11',p_web.GSV('Picture:PartFaultCode11'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes11',p_web.GSV('Picture:PartFaultCode11'))
  If p_web.IfExistsValue('tmp:FaultCodes12')
    p_web.SetPicture('tmp:FaultCodes12',p_web.GSV('Picture:PartFaultCode12'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes12',p_web.GSV('Picture:PartFaultCode12'))

AfterLookup Routine
  loc:TabNumber = -1
  If loc:act = ChangeRecord
    loc:TabNumber += 1
  End
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'epr:Part_Number'
    p_web.setsessionvalue('showtab_FormEstimateParts',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(STOMODEL)
      ! After Lookup
      ! After Lookup Assignments
      do updatePartDetails
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.epr:Description')
  of 'tmp:FaultCodes1'
      local.afterFaultCodeLookup(1)
  of 'tmp:FaultCodes2'
      local.afterFaultCodeLookup(2)
  of 'tmp:FaultCodes3'
      local.afterFaultCodeLookup(3)
  of 'tmp:FaultCodes4'
      local.afterFaultCodeLookup(4)
  of 'tmp:FaultCodes5'
      local.afterFaultCodeLookup(5)
  of 'tmp:FaultCodes6'
      local.afterFaultCodeLookup(6)
  of 'tmp:FaultCodes7'
      local.afterFaultCodeLookup(7)
  of 'tmp:FaultCodes8'
      local.afterFaultCodeLookup(8)
  of 'tmp:FaultCodes9'
      local.afterFaultCodeLookup(9)
  of 'tmp:FaultCodes10'
      local.afterFaultCodeLookup(10)
  of 'tmp:FaultCodes11'
      local.afterFaultCodeLookup(11)
  of 'tmp:FaultCodes12'
      local.afterFaultCodeLookup(12)
  
  
  Of 'epr:Supplier'
    p_web.setsessionvalue('showtab_FormEstimateParts',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(SUPPLIER)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.epr:Exclude_From_Order')
  of 'tmp:FaultCodes1'
      local.afterFaultCodeLookup(1)
  of 'tmp:FaultCodes2'
      local.afterFaultCodeLookup(2)
  of 'tmp:FaultCodes3'
      local.afterFaultCodeLookup(3)
  of 'tmp:FaultCodes4'
      local.afterFaultCodeLookup(4)
  of 'tmp:FaultCodes5'
      local.afterFaultCodeLookup(5)
  of 'tmp:FaultCodes6'
      local.afterFaultCodeLookup(6)
  of 'tmp:FaultCodes7'
      local.afterFaultCodeLookup(7)
  of 'tmp:FaultCodes8'
      local.afterFaultCodeLookup(8)
  of 'tmp:FaultCodes9'
      local.afterFaultCodeLookup(9)
  of 'tmp:FaultCodes10'
      local.afterFaultCodeLookup(10)
  of 'tmp:FaultCodes11'
      local.afterFaultCodeLookup(11)
  of 'tmp:FaultCodes12'
      local.afterFaultCodeLookup(12)
  
  
  End
  If p_web.GSV('locOrderRequired') = 1
    loc:TabNumber += 1
  End
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  if p_web.IfExistsValue('tmp:Location') = 0
    p_web.SetSessionValue('tmp:Location',tmp:Location)
  Else
    tmp:Location = p_web.GetSessionValue('tmp:Location')
  End
  if p_web.IfExistsValue('tmp:SecondLocation') = 0
    p_web.SetSessionValue('tmp:SecondLocation',tmp:SecondLocation)
  Else
    tmp:SecondLocation = p_web.GetSessionValue('tmp:SecondLocation')
  End
  if p_web.IfExistsValue('tmp:ShelfLocation') = 0
    p_web.SetSessionValue('tmp:ShelfLocation',tmp:ShelfLocation)
  Else
    tmp:ShelfLocation = p_web.GetSessionValue('tmp:ShelfLocation')
  End
  if p_web.IfExistsValue('tmp:PurchaseCost') = 0
    p_web.SetSessionValue('tmp:PurchaseCost',tmp:PurchaseCost)
  Else
    tmp:PurchaseCost = p_web.GetSessionValue('tmp:PurchaseCost')
  End
  if p_web.IfExistsValue('tmp:OutWarrantyCost') = 0
    p_web.SetSessionValue('tmp:OutWarrantyCost',tmp:OutWarrantyCost)
  Else
    tmp:OutWarrantyCost = p_web.GetSessionValue('tmp:OutWarrantyCost')
  End
  if p_web.IfExistsValue('tmp:OutWarrantyMarkup') = 0
    p_web.SetSessionValue('tmp:OutWarrantyMarkup',tmp:OutWarrantyMarkup)
  Else
    tmp:OutWarrantyMarkup = p_web.GetSessionValue('tmp:OutWarrantyMarkup')
  End
  if p_web.IfExistsValue('tmp:UnallocatePart') = 0
    p_web.SetSessionValue('tmp:UnallocatePart',tmp:UnallocatePart)
  Else
    tmp:UnallocatePart = p_web.GetSessionValue('tmp:UnallocatePart')
  End
  if p_web.IfExistsValue('tmp:CreateOrder') = 0
    p_web.SetSessionValue('tmp:CreateOrder',tmp:CreateOrder)
  Else
    tmp:CreateOrder = p_web.GetSessionValue('tmp:CreateOrder')
  End
  if p_web.IfExistsValue('tmp:FaultCodesChecked') = 0
    p_web.SetSessionValue('tmp:FaultCodesChecked',tmp:FaultCodesChecked)
  Else
    tmp:FaultCodesChecked = p_web.GetSessionValue('tmp:FaultCodesChecked')
  End
  if p_web.IfExistsValue('tmp:FaultCodes1') = 0
    p_web.SetSessionValue('tmp:FaultCodes1',tmp:FaultCodes1)
  Else
    tmp:FaultCodes1 = p_web.GetSessionValue('tmp:FaultCodes1')
  End
  if p_web.IfExistsValue('tmp:FaultCodes2') = 0
    p_web.SetSessionValue('tmp:FaultCodes2',tmp:FaultCodes2)
  Else
    tmp:FaultCodes2 = p_web.GetSessionValue('tmp:FaultCodes2')
  End
  if p_web.IfExistsValue('tmp:FaultCodes3') = 0
    p_web.SetSessionValue('tmp:FaultCodes3',tmp:FaultCodes3)
  Else
    tmp:FaultCodes3 = p_web.GetSessionValue('tmp:FaultCodes3')
  End
  if p_web.IfExistsValue('tmp:FaultCodes4') = 0
    p_web.SetSessionValue('tmp:FaultCodes4',tmp:FaultCodes4)
  Else
    tmp:FaultCodes4 = p_web.GetSessionValue('tmp:FaultCodes4')
  End
  if p_web.IfExistsValue('tmp:FaultCodes5') = 0
    p_web.SetSessionValue('tmp:FaultCodes5',tmp:FaultCodes5)
  Else
    tmp:FaultCodes5 = p_web.GetSessionValue('tmp:FaultCodes5')
  End
  if p_web.IfExistsValue('tmp:FaultCodes6') = 0
    p_web.SetSessionValue('tmp:FaultCodes6',tmp:FaultCodes6)
  Else
    tmp:FaultCodes6 = p_web.GetSessionValue('tmp:FaultCodes6')
  End
  if p_web.IfExistsValue('tmp:FaultCodes7') = 0
    p_web.SetSessionValue('tmp:FaultCodes7',tmp:FaultCodes7)
  Else
    tmp:FaultCodes7 = p_web.GetSessionValue('tmp:FaultCodes7')
  End
  if p_web.IfExistsValue('tmp:FaultCodes8') = 0
    p_web.SetSessionValue('tmp:FaultCodes8',tmp:FaultCodes8)
  Else
    tmp:FaultCodes8 = p_web.GetSessionValue('tmp:FaultCodes8')
  End
  if p_web.IfExistsValue('tmp:FaultCodes9') = 0
    p_web.SetSessionValue('tmp:FaultCodes9',tmp:FaultCodes9)
  Else
    tmp:FaultCodes9 = p_web.GetSessionValue('tmp:FaultCodes9')
  End
  if p_web.IfExistsValue('tmp:FaultCodes10') = 0
    p_web.SetSessionValue('tmp:FaultCodes10',tmp:FaultCodes10)
  Else
    tmp:FaultCodes10 = p_web.GetSessionValue('tmp:FaultCodes10')
  End
  if p_web.IfExistsValue('tmp:FaultCodes11') = 0
    p_web.SetSessionValue('tmp:FaultCodes11',tmp:FaultCodes11)
  Else
    tmp:FaultCodes11 = p_web.GetSessionValue('tmp:FaultCodes11')
  End
  if p_web.IfExistsValue('tmp:FaultCodes12') = 0
    p_web.SetSessionValue('tmp:FaultCodes12',tmp:FaultCodes12)
  Else
    tmp:FaultCodes12 = p_web.GetSessionValue('tmp:FaultCodes12')
  End
  if p_web.IfExistsValue('locPartUsedOnRepair') = 0
    p_web.SetSessionValue('locPartUsedOnRepair',locPartUsedOnRepair)
  Else
    locPartUsedOnRepair = p_web.GetSessionValue('locPartUsedOnRepair')
  End

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=File
  if p_web.IfExistsValue('tmp:Location')
    tmp:Location = p_web.GetValue('tmp:Location')
    p_web.SetSessionValue('tmp:Location',tmp:Location)
  Else
    tmp:Location = p_web.GetSessionValue('tmp:Location')
  End
  if p_web.IfExistsValue('tmp:SecondLocation')
    tmp:SecondLocation = p_web.GetValue('tmp:SecondLocation')
    p_web.SetSessionValue('tmp:SecondLocation',tmp:SecondLocation)
  Else
    tmp:SecondLocation = p_web.GetSessionValue('tmp:SecondLocation')
  End
  if p_web.IfExistsValue('tmp:ShelfLocation')
    tmp:ShelfLocation = p_web.GetValue('tmp:ShelfLocation')
    p_web.SetSessionValue('tmp:ShelfLocation',tmp:ShelfLocation)
  Else
    tmp:ShelfLocation = p_web.GetSessionValue('tmp:ShelfLocation')
  End
  if p_web.IfExistsValue('tmp:PurchaseCost')
    tmp:PurchaseCost = p_web.dformat(clip(p_web.GetValue('tmp:PurchaseCost')),'@n_14.2')
    p_web.SetSessionValue('tmp:PurchaseCost',tmp:PurchaseCost)
  Else
    tmp:PurchaseCost = p_web.GetSessionValue('tmp:PurchaseCost')
  End
  if p_web.IfExistsValue('tmp:OutWarrantyCost')
    tmp:OutWarrantyCost = p_web.dformat(clip(p_web.GetValue('tmp:OutWarrantyCost')),'@n_14.2')
    p_web.SetSessionValue('tmp:OutWarrantyCost',tmp:OutWarrantyCost)
  Else
    tmp:OutWarrantyCost = p_web.GetSessionValue('tmp:OutWarrantyCost')
  End
  if p_web.IfExistsValue('tmp:OutWarrantyMarkup')
    tmp:OutWarrantyMarkup = p_web.dformat(clip(p_web.GetValue('tmp:OutWarrantyMarkup')),'@n3')
    p_web.SetSessionValue('tmp:OutWarrantyMarkup',tmp:OutWarrantyMarkup)
  Else
    tmp:OutWarrantyMarkup = p_web.GetSessionValue('tmp:OutWarrantyMarkup')
  End
  if p_web.IfExistsValue('tmp:UnallocatePart')
    tmp:UnallocatePart = p_web.GetValue('tmp:UnallocatePart')
    p_web.SetSessionValue('tmp:UnallocatePart',tmp:UnallocatePart)
  Else
    tmp:UnallocatePart = p_web.GetSessionValue('tmp:UnallocatePart')
  End
  if p_web.IfExistsValue('tmp:CreateOrder')
    tmp:CreateOrder = p_web.GetValue('tmp:CreateOrder')
    p_web.SetSessionValue('tmp:CreateOrder',tmp:CreateOrder)
  Else
    tmp:CreateOrder = p_web.GetSessionValue('tmp:CreateOrder')
  End
  if p_web.IfExistsValue('tmp:FaultCodesChecked')
    tmp:FaultCodesChecked = p_web.GetValue('tmp:FaultCodesChecked')
    p_web.SetSessionValue('tmp:FaultCodesChecked',tmp:FaultCodesChecked)
  Else
    tmp:FaultCodesChecked = p_web.GetSessionValue('tmp:FaultCodesChecked')
  End
  if p_web.IfExistsValue('tmp:FaultCodes1')
    tmp:FaultCodes1 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCodes1')),p_web.GSV('Picture:PartFaultCode1'))
    p_web.SetSessionValue('tmp:FaultCodes1',tmp:FaultCodes1)
  Else
    tmp:FaultCodes1 = p_web.GetSessionValue('tmp:FaultCodes1')
  End
  if p_web.IfExistsValue('tmp:FaultCodes2')
    tmp:FaultCodes2 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCodes2')),p_web.GSV('Picture:PartFaultCode2'))
    p_web.SetSessionValue('tmp:FaultCodes2',tmp:FaultCodes2)
  Else
    tmp:FaultCodes2 = p_web.GetSessionValue('tmp:FaultCodes2')
  End
  if p_web.IfExistsValue('tmp:FaultCodes3')
    tmp:FaultCodes3 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCodes3')),p_web.GSV('Picture:PartFaultCode3'))
    p_web.SetSessionValue('tmp:FaultCodes3',tmp:FaultCodes3)
  Else
    tmp:FaultCodes3 = p_web.GetSessionValue('tmp:FaultCodes3')
  End
  if p_web.IfExistsValue('tmp:FaultCodes4')
    tmp:FaultCodes4 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCodes4')),p_web.GSV('Picture:PartFaultCode4'))
    p_web.SetSessionValue('tmp:FaultCodes4',tmp:FaultCodes4)
  Else
    tmp:FaultCodes4 = p_web.GetSessionValue('tmp:FaultCodes4')
  End
  if p_web.IfExistsValue('tmp:FaultCodes5')
    tmp:FaultCodes5 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCodes5')),p_web.GSV('Picture:PartFaultCode5'))
    p_web.SetSessionValue('tmp:FaultCodes5',tmp:FaultCodes5)
  Else
    tmp:FaultCodes5 = p_web.GetSessionValue('tmp:FaultCodes5')
  End
  if p_web.IfExistsValue('tmp:FaultCodes6')
    tmp:FaultCodes6 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCodes6')),p_web.GSV('Picture:PartFaultCode6'))
    p_web.SetSessionValue('tmp:FaultCodes6',tmp:FaultCodes6)
  Else
    tmp:FaultCodes6 = p_web.GetSessionValue('tmp:FaultCodes6')
  End
  if p_web.IfExistsValue('tmp:FaultCodes7')
    tmp:FaultCodes7 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCodes7')),p_web.GSV('Picture:PartFaultCode7'))
    p_web.SetSessionValue('tmp:FaultCodes7',tmp:FaultCodes7)
  Else
    tmp:FaultCodes7 = p_web.GetSessionValue('tmp:FaultCodes7')
  End
  if p_web.IfExistsValue('tmp:FaultCodes8')
    tmp:FaultCodes8 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCodes8')),p_web.GSV('Picture:PartFaultCode8'))
    p_web.SetSessionValue('tmp:FaultCodes8',tmp:FaultCodes8)
  Else
    tmp:FaultCodes8 = p_web.GetSessionValue('tmp:FaultCodes8')
  End
  if p_web.IfExistsValue('tmp:FaultCodes9')
    tmp:FaultCodes9 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCodes9')),p_web.GSV('Picture:PartFaultCode9'))
    p_web.SetSessionValue('tmp:FaultCodes9',tmp:FaultCodes9)
  Else
    tmp:FaultCodes9 = p_web.GetSessionValue('tmp:FaultCodes9')
  End
  if p_web.IfExistsValue('tmp:FaultCodes10')
    tmp:FaultCodes10 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCodes10')),p_web.GSV('Picture:PartFaultCode10'))
    p_web.SetSessionValue('tmp:FaultCodes10',tmp:FaultCodes10)
  Else
    tmp:FaultCodes10 = p_web.GetSessionValue('tmp:FaultCodes10')
  End
  if p_web.IfExistsValue('tmp:FaultCodes11')
    tmp:FaultCodes11 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCodes11')),p_web.GSV('Picture:PartFaultCode11'))
    p_web.SetSessionValue('tmp:FaultCodes11',tmp:FaultCodes11)
  Else
    tmp:FaultCodes11 = p_web.GetSessionValue('tmp:FaultCodes11')
  End
  if p_web.IfExistsValue('tmp:FaultCodes12')
    tmp:FaultCodes12 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCodes12')),p_web.GSV('Picture:PartFaultCode12'))
    p_web.SetSessionValue('tmp:FaultCodes12',tmp:FaultCodes12)
  Else
    tmp:FaultCodes12 = p_web.GetSessionValue('tmp:FaultCodes12')
  End
  if p_web.IfExistsValue('locPartUsedOnRepair')
    locPartUsedOnRepair = p_web.GetValue('locPartUsedOnRepair')
    p_web.SetSessionValue('locPartUsedOnRepair',locPartUsedOnRepair)
  Else
    locPartUsedOnRepair = p_web.GetSessionValue('locPartUsedOnRepair')
  End

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('FormEstimateParts_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'ViewJob'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormEstimateParts_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormEstimateParts_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormEstimateParts_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'ViewJob'

GenerateForm   Routine
  do LoadRelatedRecords
      if (p_web.GSV('FormEstimateParts:FirstTime') = 0)
          !Write Fault Codes
          p_web.SSV('epr:Fault_Code1',epr:Fault_Code1)
          p_web.SSV('epr:Fault_Code2',epr:Fault_Code2)
          p_web.SSV('epr:Fault_Code3',epr:Fault_Code3)
          p_web.SSV('epr:Fault_Code4',epr:Fault_Code4)
          p_web.SSV('epr:Fault_Code5',epr:Fault_Code5)
          p_web.SSV('epr:Fault_Code6',epr:Fault_Code6)
          p_web.SSV('epr:Fault_Code7',epr:Fault_Code7)
          p_web.SSV('epr:Fault_Code8',epr:Fault_Code8)
          p_web.SSV('epr:Fault_Code9',epr:Fault_Code9)
          p_web.SSV('epr:Fault_Code10',epr:Fault_Code10)
          p_web.SSV('epr:Fault_Code11',epr:Fault_Code11)
          p_web.SSV('epr:Fault_Code12',epr:Fault_Code12)
  
  
          Access:MANFAUPA.Clearkey(map:ScreenOrderKey)
          map:Manufacturer    = p_web.GSV('job:Manufacturer')
          map:ScreenOrder    = 0
          set(map:ScreenOrderKey,map:ScreenOrderKey)
          loop
              if (Access:MANFAUPA.Next())
                  Break
              end ! if (Access:MANFAUPA.Next())
              if (map:Manufacturer    <> p_web.GSV('job:Manufacturer'))
                  Break
              end ! if (map:Manufacturer    <> p_web.GSV('job:Manufacturer'))
              if (map:ScreenOrder    = 0)
                  cycle
              end ! if (map:ScreenOrder    <> 0)
  
              p_web.SSV('tmp:FaultCodes' & map:ScreenOrder,p_web.GSV('epr:Fault_Code' & map:Field_Number))
          end ! loop
  
  !        loop x# = 1 To 12
  !            p_web.SSV('tmp:FaultCodes' & x#,p_web.GSV('epr:Fault_Code' & x#))
  !            linePrint('tmp:FaultCode' & x# & ' - ' & p_web.GSV('tmp:FaultCode' & x#),'c:\log.log')
  !        end ! loop x# = 1 To 12
          p_web.SSV('FormEstimateParts:FirstTime',1)
          !p_web.SSV('locOrderRequired',0)
  
          ! Save For Later
          p_web.SSV('Save:Quantity',epr:Quantity)
      end ! if (p_web.GSV('FormEstimateParts:FirstTime',0))
      do updateComments
  
 tmp:Location = p_web.RestoreValue('tmp:Location')
 tmp:SecondLocation = p_web.RestoreValue('tmp:SecondLocation')
 tmp:ShelfLocation = p_web.RestoreValue('tmp:ShelfLocation')
 tmp:PurchaseCost = p_web.RestoreValue('tmp:PurchaseCost')
 tmp:OutWarrantyCost = p_web.RestoreValue('tmp:OutWarrantyCost')
 tmp:OutWarrantyMarkup = p_web.RestoreValue('tmp:OutWarrantyMarkup')
 tmp:UnallocatePart = p_web.RestoreValue('tmp:UnallocatePart')
 tmp:CreateOrder = p_web.RestoreValue('tmp:CreateOrder')
 tmp:FaultCodesChecked = p_web.RestoreValue('tmp:FaultCodesChecked')
 tmp:FaultCodes1 = p_web.RestoreValue('tmp:FaultCodes1')
 tmp:FaultCodes2 = p_web.RestoreValue('tmp:FaultCodes2')
 tmp:FaultCodes3 = p_web.RestoreValue('tmp:FaultCodes3')
 tmp:FaultCodes4 = p_web.RestoreValue('tmp:FaultCodes4')
 tmp:FaultCodes5 = p_web.RestoreValue('tmp:FaultCodes5')
 tmp:FaultCodes6 = p_web.RestoreValue('tmp:FaultCodes6')
 tmp:FaultCodes7 = p_web.RestoreValue('tmp:FaultCodes7')
 tmp:FaultCodes8 = p_web.RestoreValue('tmp:FaultCodes8')
 tmp:FaultCodes9 = p_web.RestoreValue('tmp:FaultCodes9')
 tmp:FaultCodes10 = p_web.RestoreValue('tmp:FaultCodes10')
 tmp:FaultCodes11 = p_web.RestoreValue('tmp:FaultCodes11')
 tmp:FaultCodes12 = p_web.RestoreValue('tmp:FaultCodes12')
 locPartUsedOnRepair = p_web.RestoreValue('locPartUsedOnRepair')
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
    loc:viewonly = Choose(p_web.GSV('Parts:ViewOnly') = 1,1,0)
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Insert / Amend Estimate Parts') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Insert / Amend Estimate Parts',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_FormEstimateParts',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      If loc:act = ChangeRecord
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_FormEstimateParts0_div')&'">'&p_web.Translate('Part Status')&'</a></li>'& CRLF
      End
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_FormEstimateParts1_div')&'">'&p_web.Translate('Part Details')&'</a></li>'& CRLF
      If p_web.GSV('locOrderRequired') = 1
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_FormEstimateParts2_div')&'">'&p_web.Translate('Order Required')&'</a></li>'& CRLF
      End
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_FormEstimateParts3_div')&'">'&p_web.Translate('Fault Codes')&'</a></li>'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_FormEstimateParts4_div')&'">'&p_web.Translate('Select When Part Will Be Used')&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  do GenerateTab3
  do GenerateTab4
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="FormEstimateParts_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.cancel();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CancelButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="FormEstimateParts_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'FormEstimateParts_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="FormEstimateParts_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'FormEstimateParts_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
    If upper(p_web.getvalue('LookupFile'))='STOMODEL'
          If Not (1=0)
            p_web.SetValue('SelectField',clip(loc:formname) & '.epr:Description')
          End
    End
    If upper(p_web.getvalue('LookupFile'))='SUPPLIER'
        If p_web.GSV('Hide:PartFaultCode1') <> 1
          If Not (1=0)
            p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:FaultCodes1')
          End
        End
    End
  Else
    If False
    ElsIf loc:act = ChangeRecord
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.epr:Part_Number')
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_FormEstimateParts')>0,p_web.GSV('showtab_FormEstimateParts'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_FormEstimateParts'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_FormEstimateParts') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_FormEstimateParts'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_FormEstimateParts')>0,p_web.GSV('showtab_FormEstimateParts'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_FormEstimateParts') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
      If loc:act = ChangeRecord
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Part Status') & ''''
      End
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Part Details') & ''''
      If p_web.GSV('locOrderRequired') = 1
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Order Required') & ''''
      End
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Fault Codes') & ''''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Select When Part Will Be Used') & ''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_FormEstimateParts_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_FormEstimateParts')>0,p_web.GSV('showtab_FormEstimateParts'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"FormEstimateParts",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_FormEstimateParts')>0,p_web.GSV('showtab_FormEstimateParts'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_FormEstimateParts_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('FormEstimateParts') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('FormEstimateParts')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
  If loc:act = ChangeRecord
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Part Status')&'</a></h3>' & CRLF & p_web.DivHeader('tab_FormEstimateParts0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEstimateParts0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEstimateParts0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEstimateParts0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Part Status')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEstimateParts0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Part Status')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEstimateParts0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Part Status')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEstimateParts0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::epr:Order_Number
        do Value::epr:Order_Number
        do Comment::epr:Order_Number
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::epr:Date_Ordered
        do Value::epr:Date_Ordered
        do Comment::epr:Date_Ordered
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::epr:Date_Received
        do Value::epr:Date_Received
        do Comment::epr:Date_Received
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::locOutFaultCode
        do Comment::locOutFaultCode
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
  end
GenerateTab1  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Part Details')&'</a></h3>' & CRLF & p_web.DivHeader('tab_FormEstimateParts1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEstimateParts1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEstimateParts1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEstimateParts1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Part Details')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEstimateParts1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Part Details')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEstimateParts1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Part Details')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEstimateParts1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::epr:Part_Number
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::epr:Part_Number
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::epr:Part_Number
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::epr:Description
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::epr:Description
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::epr:Description
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::epr:Despatch_Note_Number
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::epr:Despatch_Note_Number
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::epr:Despatch_Note_Number
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::epr:Quantity
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::epr:Quantity
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::epr:Quantity
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        !Set Width
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:Location
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:Location
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:Location
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:SecondLocation
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:SecondLocation
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:SecondLocation
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:ShelfLocation
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:ShelfLocation
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:ShelfLocation
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:PurchaseCost
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:PurchaseCost
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:PurchaseCost
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:OutWarrantyCost
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:OutWarrantyCost
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:OutWarrantyCost
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:OutWarrantyMarkup
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:OutWarrantyMarkup
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:OutWarrantyMarkup
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:FixedPrice
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:FixedPrice
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:FixedPrice
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::epr:Supplier
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::epr:Supplier
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::epr:Supplier
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::epr:Exclude_From_Order
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::epr:Exclude_From_Order
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::epr:Exclude_From_Order
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::epr:PartAllocated
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::epr:PartAllocated
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::epr:PartAllocated
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      If p_web.GSV('Show:UnallocatePart') = 1
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:UnallocatePart
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:UnallocatePart
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:UnallocatePart
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab2  Routine
  If p_web.GSV('locOrderRequired') = 1
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Order Required')&'</a></h3>' & CRLF & p_web.DivHeader('tab_FormEstimateParts2',p_web.combine(p_web.site.style.FormTabInner,,'red bold'),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEstimateParts2',p_web.combine(p_web.site.style.FormTabInner,,'red bold'),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEstimateParts2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,'red bold'),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEstimateParts2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,'red bold'),Net:NoSend,'Order Required')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEstimateParts2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,'red bold'),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Order Required')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEstimateParts2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,'red bold'),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Order Required')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEstimateParts2',p_web.combine(p_web.site.style.FormTabInner,,'red bold'),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::text:OrderRequired
        do Comment::text:OrderRequired
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::text:OrderRequired2
        do Comment::text:OrderRequired2
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:CreateOrder
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:CreateOrder
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:CreateOrder
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
  end
GenerateTab3  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Fault Codes')&'</a></h3>' & CRLF & p_web.DivHeader('tab_FormEstimateParts3',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEstimateParts3',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEstimateParts3',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEstimateParts3',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Fault Codes')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEstimateParts3',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Fault Codes')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEstimateParts3',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Fault Codes')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEstimateParts3',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
      If 0
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:FaultCodesChecked
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:FaultCodesChecked
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:FaultCodesChecked
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
      If p_web.GSV('Hide:PartFaultCode1') <> 1
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:FaultCodes1
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:FaultCodes1
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:FaultCodes1
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
      If p_web.GSV('Hide:PartFaultCode2') <> 1
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:FaultCode2
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:FaultCode2
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:FaultCode2
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
      If p_web.GSV('Hide:PartFaultCode3') <> 1
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:FaultCode3
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:FaultCode3
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:FaultCode3
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
      If p_web.GSV('Hide:PartFaultCode4') <> 1
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:FaultCode4
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:FaultCode4
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:FaultCode4
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
      If p_web.GSV('Hide:PartFaultCode5') <> 1
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:FaultCode5
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:FaultCode5
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:FaultCode5
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
      If p_web.GSV('Hide:PartFaultCode6') <> 1
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:FaultCode6
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:FaultCode6
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:FaultCode6
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
      If p_web.GSV('Hide:PartFaultCode7') <> 1
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:FaultCode7
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:FaultCode7
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:FaultCode7
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
      If p_web.GSV('Hide:PartFaultCode8') <> 1
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:FaultCode8
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:FaultCode8
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:FaultCode8
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
      If p_web.GSV('Hide:PartFaultCode9') <> 1
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:FaultCode9
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:FaultCode9
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:FaultCode9
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
      If p_web.GSV('Hide:PartFaultCode10') <> 1
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:FaultCode10
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:FaultCode10
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:FaultCode10
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
      If p_web.GSV('Hide:PartFaultCode11') <> 1
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:FaultCode11
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:FaultCode11
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:FaultCode11
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
      If p_web.GSV('Hide:PartFaultCode12') <> 1
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:FaultCode12
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:FaultCode12
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:FaultCode12
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab4  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Select When Part Will Be Used')&'</a></h3>' & CRLF & p_web.DivHeader('tab_FormEstimateParts4',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEstimateParts4',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEstimateParts4',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEstimateParts4',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Select When Part Will Be Used')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEstimateParts4',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Select When Part Will Be Used')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEstimateParts4',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Select When Part Will Be Used')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEstimateParts4',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'20%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::epr:UsedOnRepair
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'35%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::epr:UsedOnRepair
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Prompt::epr:Order_Number  Routine
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Order_Number') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Order Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::epr:Order_Number  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    epr:Order_Number = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n08b'  !FieldPicture = @s8
    epr:Order_Number = p_web.Dformat(p_web.GetValue('Value'),'@n08b')
  End
  do ValidateValue::epr:Order_Number  ! copies value to session value if valid.
  do Comment::epr:Order_Number ! allows comment style to be updated.

ValidateValue::epr:Order_Number  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('epr:Order_Number',epr:Order_Number).
    End

Value::epr:Order_Number  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Order_Number') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- epr:Order_Number
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    p_web._jsok(format(p_web.GetSessionValue('epr:Order_Number'),'@n08b')) & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::epr:Order_Number  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if epr:Order_Number:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Order_Number') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::epr:Date_Ordered  Routine
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Date_Ordered') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Date Ordered'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::epr:Date_Ordered  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    epr:Date_Ordered = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @d6b
    epr:Date_Ordered = p_web.Dformat(p_web.GetValue('Value'),'@d6b')
  End
  do ValidateValue::epr:Date_Ordered  ! copies value to session value if valid.
  do Comment::epr:Date_Ordered ! allows comment style to be updated.

ValidateValue::epr:Date_Ordered  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('epr:Date_Ordered',epr:Date_Ordered).
    End

Value::epr:Date_Ordered  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Date_Ordered') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- epr:Date_Ordered
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('epr:Date_Ordered'),) & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::epr:Date_Ordered  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if epr:Date_Ordered:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Date_Ordered') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::epr:Date_Received  Routine
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Date_Received') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Date Received'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::epr:Date_Received  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    epr:Date_Received = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @d6b
    epr:Date_Received = p_web.Dformat(p_web.GetValue('Value'),'@d6b')
  End
  do ValidateValue::epr:Date_Received  ! copies value to session value if valid.
  do Comment::epr:Date_Received ! allows comment style to be updated.

ValidateValue::epr:Date_Received  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('epr:Date_Received',epr:Date_Received).
    End

Value::epr:Date_Received  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Date_Received') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- epr:Date_Received
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('epr:Date_Received'),) & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::epr:Date_Received  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if epr:Date_Received:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Date_Received') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::locOutFaultCode  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::locOutFaultCode  ! copies value to session value if valid.
  do Comment::locOutFaultCode ! allows comment style to be updated.

ValidateValue::locOutFaultCode  Routine
    If not (1=0)
    End

Value::locOutFaultCode  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('locOutFaultCode') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="locOutFaultCode" class="'&clip('green bold')&'"'&clip(loc:extra)&'>' & p_web.Translate(p_web.GSV('locOutFaultCode'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locOutFaultCode  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locOutFaultCode:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('locOutFaultCode') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::epr:Part_Number  Routine
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Part_Number') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Part Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::epr:Part_Number  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    epr:Part_Number = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    epr:Part_Number = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  ElsIf p_web.RequestAjax = 1 and p_web.IfExistsValue('stm:Part_Number')
    epr:Part_Number = p_web.GetValue('stm:Part_Number')
  ElsIf p_web.RequestAjax = 1
    epr:Part_Number = stm:Part_Number
  End
  do ValidateValue::epr:Part_Number  ! copies value to session value if valid.
      if (p_web.GSV('job:Engineer') <> '')
          locUserCode = p_web.GSV('job:Engineer')
      else !
          locUserCode = p_web.GSV('BookingUSerCOde')
      end !if (p_web.GSV('job:Engineer') <> '')
  
      case validFreeTextPart('C',locUserCode,p_web.GSV('job:Manufacturer'),|
                                  p_web.GSV('job:Model_Number'),p_web.GSV('epr:part_Number'))
      of 0
          p_web.SSV('Comment:PartNumber','')
  
          Access:STOMODEL.Clearkey(stm:Location_Part_Number_Key)
          stm:Model_Number    = p_web.GSV('job:Model_Number')
          stm:Location    = p_web.GSV('BookingSiteLocation')
          stm:Part_Number    = p_web.GSV('epr:Part_Number')
          if (Access:STOMODEL.TryFetch(stm:Location_Part_Number_Key) = Level:Benign)
              ! Found
          else ! if (Access:STOMODEL.TryFetch(stm:Location_Part_Number_Key) = Level:Benign)
              ! Error
          end ! if (Access:STOMODEL.TryFetch(stm:Location_Part_Number_Key) = Level:Benign)
  
          do updatePartDetails
      of 1
          p_web.SSV('epr:Part_Number','')
          p_web.SSV('Comment:PartNumber','Invalid User')
      of 2
          p_web.SSV('epr:Part_Number','')
          p_web.SSV('Comment:PartNumber','Error! Selected Part Is Not In Stock Location')
      of 3
          p_web.SSV('epr:Part_Number','')
          p_web.SSV('Comment:PartNumber','Error! Selected Part Is Suspended')
      of 4
          p_web.SSV('Comment:PartNumber','Warning! Cannot Find The Selected Part In Stock')
      of 5
          p_web.SSV('epr:part_Number','')
          p_web.SSV('Comment:PartNumber','Error! Access Level Is Not High Enough For Part')
  
      end ! case
  
  p_Web.SetValue('lookupfield','epr:Part_Number')
  do AfterLookup
  do Value::epr:Part_Number
  do SendAlert
  do Comment::epr:Part_Number
  do Comment::epr:Part_Number
  do Value::epr:Description  !1
  do Value::epr:Supplier  !1
  do Value::tmp:PurchaseCost  !1
  do Value::tmp:OutWarrantyCost  !1
  do Value::tmp:OutWarrantyMarkup  !1
  do Prompt::tmp:SecondLocation
  do Value::tmp:SecondLocation  !1
  do Prompt::tmp:ShelfLocation
  do Value::tmp:ShelfLocation  !1
  do Prompt::tmp:Location
  do Value::tmp:Location  !1

ValidateValue::epr:Part_Number  Routine
    If not (1=0)
  If epr:Part_Number = ''
    loc:Invalid = 'epr:Part_Number'
    epr:Part_Number:IsInvalid = true
    loc:alert = p_web.translate('Part Number') & ' ' & p_web.site.RequiredText
  End
    epr:Part_Number = Upper(epr:Part_Number)
      if loc:invalid = '' then p_web.SetSessionValue('epr:Part_Number',epr:Part_Number).
    End

Value::epr:Part_Number  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Part_Number') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.getValue('adjustment') = 1 OR loc:act = ChangeRecord or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,'formrqd')
  If loc:retrying
    epr:Part_Number = p_web.RestoreValue('epr:Part_Number')
    do ValidateValue::epr:Part_Number
    If epr:Part_Number:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- epr:Part_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.getValue('adjustment') = 1 OR loc:act = ChangeRecord,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''epr:Part_Number'',''formestimateparts_epr:part_number_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('epr:Part_Number')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','epr:Part_Number',p_web.GetSessionValue('epr:Part_Number'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s30',loc:javascript,p_web.PicLength('@s30'),,,) & '<13,10>'!4
    if not loc:viewonly and not loc:readonly
      loc:fieldclass = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('BrowseModelStock')&'?LookupField=epr:Part_Number&Tab=4&ForeignField=stm:Part_Number&_sort=stm:Description&Refresh=sort'),,,,,loc:fieldclass)
    End
  do SendPacket
  End
  p_web.DivFooter()
Comment::epr:Part_Number  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if epr:Part_Number:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:PartNumber'))
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Part_Number') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::epr:Description  Routine
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Description') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Description'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::epr:Description  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    epr:Description = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    epr:Description = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::epr:Description  ! copies value to session value if valid.
  do Value::epr:Description
  do SendAlert
  do Comment::epr:Description ! allows comment style to be updated.

ValidateValue::epr:Description  Routine
    If not (1=0)
  If epr:Description = ''
    loc:Invalid = 'epr:Description'
    epr:Description:IsInvalid = true
    loc:alert = p_web.translate('Description') & ' ' & p_web.site.RequiredText
  End
    epr:Description = Upper(epr:Description)
      if loc:invalid = '' then p_web.SetSessionValue('epr:Description',epr:Description).
    End

Value::epr:Description  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Description') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.getValue('adjustment') = 1 OR loc:act = ChangeRecord or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,'formrqd')
  If loc:retrying
    epr:Description = p_web.RestoreValue('epr:Description')
    do ValidateValue::epr:Description
    If epr:Description:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- epr:Description
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.getValue('adjustment') = 1 OR loc:act = ChangeRecord,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''epr:Description'',''formestimateparts_epr:description_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','epr:Description',p_web.GetSessionValueFormat('epr:Description'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::epr:Description  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if epr:Description:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web._jsok(p_web.site.RequiredText)
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Description') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::epr:Despatch_Note_Number  Routine
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Despatch_Note_Number') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Despatch Note Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::epr:Despatch_Note_Number  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    epr:Despatch_Note_Number = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    epr:Despatch_Note_Number = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::epr:Despatch_Note_Number  ! copies value to session value if valid.
  do Value::epr:Despatch_Note_Number
  do SendAlert
  do Comment::epr:Despatch_Note_Number ! allows comment style to be updated.

ValidateValue::epr:Despatch_Note_Number  Routine
    If not (1=0)
    epr:Despatch_Note_Number = Upper(epr:Despatch_Note_Number)
      if loc:invalid = '' then p_web.SetSessionValue('epr:Despatch_Note_Number',epr:Despatch_Note_Number).
    End

Value::epr:Despatch_Note_Number  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Despatch_Note_Number') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    epr:Despatch_Note_Number = p_web.RestoreValue('epr:Despatch_Note_Number')
    do ValidateValue::epr:Despatch_Note_Number
    If epr:Despatch_Note_Number:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- epr:Despatch_Note_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''epr:Despatch_Note_Number'',''formestimateparts_epr:despatch_note_number_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','epr:Despatch_Note_Number',p_web.GetSessionValueFormat('epr:Despatch_Note_Number'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::epr:Despatch_Note_Number  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if epr:Despatch_Note_Number:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Despatch_Note_Number') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::epr:Quantity  Routine
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Quantity') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Quantity'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::epr:Quantity  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    epr:Quantity = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n4'  !FieldPicture = @n8
    epr:Quantity = p_web.Dformat(p_web.GetValue('Value'),'@n4')
  End
  do ValidateValue::epr:Quantity  ! copies value to session value if valid.
  do Value::epr:Quantity
  do SendAlert
  do Comment::epr:Quantity ! allows comment style to be updated.

ValidateValue::epr:Quantity  Routine
    If not (1=0)
  If epr:Quantity = ''
    loc:Invalid = 'epr:Quantity'
    epr:Quantity:IsInvalid = true
    loc:alert = p_web.translate('Quantity') & ' ' & p_web.site.RequiredText
  End
    epr:Quantity = Upper(epr:Quantity)
      if loc:invalid = '' then p_web.SetSessionValue('epr:Quantity',epr:Quantity).
    End

Value::epr:Quantity  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Quantity') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.getValue('adjustment') = 1 OR p_web.GSV('ReadOnly:Quantity') = 1 or loc:act = ChangeRecord or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,'formrqd')
  If loc:retrying
    epr:Quantity = p_web.RestoreValue('epr:Quantity')
    do ValidateValue::epr:Quantity
    If epr:Quantity:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- epr:Quantity
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.getValue('adjustment') = 1 OR p_web.GSV('ReadOnly:Quantity') = 1 or loc:act = ChangeRecord,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''epr:Quantity'',''formestimateparts_epr:quantity_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','epr:Quantity',p_web.GetSessionValue('epr:Quantity'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n4',loc:javascript,,'Quantity',,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::epr:Quantity  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if epr:Quantity:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web._jsok(p_web.site.RequiredText)
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Quantity') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:Location  Routine
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:Location') & '_prompt',Choose(p_web.GSV('tmp:Location') = '','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('tmp:Location') = '','',p_web.Translate('Location'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:Location  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:Location = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:Location = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:Location  ! copies value to session value if valid.
  do Comment::tmp:Location ! allows comment style to be updated.

ValidateValue::tmp:Location  Routine
    If not (p_web.GSV('tmp:Location') = '')
      if loc:invalid = '' then p_web.SetSessionValue('tmp:Location',tmp:Location).
    End

Value::tmp:Location  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('tmp:Location') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:Location') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (p_web.GSV('tmp:Location') = '')
  ! --- DISPLAY --- tmp:Location
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('tmp:Location'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:Location  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:Location:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('tmp:Location') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:Location') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('tmp:Location') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:SecondLocation  Routine
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:SecondLocation') & '_prompt',Choose(p_web.GSV('tmp:SecondLocation') = '','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('tmp:SecondLocation') = '','',p_web.Translate('Second Location'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:SecondLocation  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:SecondLocation = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:SecondLocation = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:SecondLocation  ! copies value to session value if valid.
  do Comment::tmp:SecondLocation ! allows comment style to be updated.

ValidateValue::tmp:SecondLocation  Routine
    If not (p_web.GSV('tmp:SecondLocation') = '')
      if loc:invalid = '' then p_web.SetSessionValue('tmp:SecondLocation',tmp:SecondLocation).
    End

Value::tmp:SecondLocation  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('tmp:SecondLocation') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:SecondLocation') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (p_web.GSV('tmp:SecondLocation') = '')
  ! --- DISPLAY --- tmp:SecondLocation
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('tmp:SecondLocation'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:SecondLocation  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:SecondLocation:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('tmp:SecondLocation') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:SecondLocation') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('tmp:SecondLocation') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:ShelfLocation  Routine
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:ShelfLocation') & '_prompt',Choose(p_web.GSV('tmp:ShelfLocation') = '','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('tmp:ShelfLocation') = '','',p_web.Translate('Shelf Location'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:ShelfLocation  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:ShelfLocation = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:ShelfLocation = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:ShelfLocation  ! copies value to session value if valid.
  do Comment::tmp:ShelfLocation ! allows comment style to be updated.

ValidateValue::tmp:ShelfLocation  Routine
    If not (p_web.GSV('tmp:ShelfLocation') = '')
      if loc:invalid = '' then p_web.SetSessionValue('tmp:ShelfLocation',tmp:ShelfLocation).
    End

Value::tmp:ShelfLocation  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('tmp:ShelfLocation') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:ShelfLocation') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (p_web.GSV('tmp:ShelfLocation') = '')
  ! --- DISPLAY --- tmp:ShelfLocation
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('tmp:ShelfLocation'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:ShelfLocation  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:ShelfLocation:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('tmp:ShelfLocation') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:ShelfLocation') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('tmp:ShelfLocation') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:PurchaseCost  Routine
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:PurchaseCost') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Purchase Cost'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:PurchaseCost  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:PurchaseCost = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n_14.2'  !FieldPicture = 
    tmp:PurchaseCost = p_web.Dformat(p_web.GetValue('Value'),'@n_14.2')
  End
  do ValidateValue::tmp:PurchaseCost  ! copies value to session value if valid.
  do Value::tmp:PurchaseCost
  do SendAlert
  do Comment::tmp:PurchaseCost ! allows comment style to be updated.

ValidateValue::tmp:PurchaseCost  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:PurchaseCost',tmp:PurchaseCost).
    End

Value::tmp:PurchaseCost  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:PurchaseCost') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If p_web.getValue('adjustment') = 1 or p_web.GSV('ReadOnly:PurchaseCost') = 1 OR loc:act = ChangeRecord or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    tmp:PurchaseCost = p_web.RestoreValue('tmp:PurchaseCost')
    do ValidateValue::tmp:PurchaseCost
    If tmp:PurchaseCost:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- tmp:PurchaseCost
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.getValue('adjustment') = 1 or p_web.GSV('ReadOnly:PurchaseCost') = 1 OR loc:act = ChangeRecord,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:PurchaseCost'',''formestimateparts_tmp:purchasecost_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:PurchaseCost',p_web.GetSessionValue('tmp:PurchaseCost'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n_14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:PurchaseCost  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:PurchaseCost:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:PurchaseCost') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:OutWarrantyCost  Routine
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:OutWarrantyCost') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Out Of Warranty Cost'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:OutWarrantyCost  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:OutWarrantyCost = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n_14.2'  !FieldPicture = 
    tmp:OutWarrantyCost = p_web.Dformat(p_web.GetValue('Value'),'@n_14.2')
  End
  do ValidateValue::tmp:OutWarrantyCost  ! copies value to session value if valid.
  do enableDisableCosts
  do Value::tmp:OutWarrantyCost
  do SendAlert
  do Comment::tmp:OutWarrantyCost ! allows comment style to be updated.
  do Value::tmp:OutWarrantyMarkup  !1

ValidateValue::tmp:OutWarrantyCost  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:OutWarrantyCost',tmp:OutWarrantyCost).
    End

Value::tmp:OutWarrantyCost  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:OutWarrantyCost') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If p_web.GSV('ReadOnly:OutWarrantyCost') = 1 Or p_web.GSV('tmp:OutWarrantyMarkup') > 0 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    tmp:OutWarrantyCost = p_web.RestoreValue('tmp:OutWarrantyCost')
    do ValidateValue::tmp:OutWarrantyCost
    If tmp:OutWarrantyCost:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- tmp:OutWarrantyCost
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:OutWarrantyCost') = 1 Or p_web.GSV('tmp:OutWarrantyMarkup') > 0,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:OutWarrantyCost'',''formestimateparts_tmp:outwarrantycost_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:OutWarrantyCost')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:OutWarrantyCost',p_web.GetSessionValue('tmp:OutWarrantyCost'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n_14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:OutWarrantyCost  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:OutWarrantyCost:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:OutWarrantyCost') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:OutWarrantyMarkup  Routine
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:OutWarrantyMarkup') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Markup'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:OutWarrantyMarkup  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:OutWarrantyMarkup = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n3'  !FieldPicture = 
    tmp:OutWarrantyMarkup = p_web.Dformat(p_web.GetValue('Value'),'@n3')
  End
  do ValidateValue::tmp:OutWarrantyMarkup  ! copies value to session value if valid.
  !Out Warranty Markup
  markup# = GETINI('STOCK','MaxPartMarkup',,CLIP(PATH())&'\SB2KDEF.INI')
  if (p_web.GSV('tmp:OutWarrantyMarkup') > markup#)
      p_web.SSV('Comment:OutWarrantyMarkup','You cannot markup more than ' & markup# & '%')
      p_web.SSV('tmp:OutWarrantyMarkup',markup#)
  else! if (p_web.GSV('tmp:OutWarrantyMarkup') > GETINI('STOCK','MaxPartMarkup',,CLIP(PATH())&'\SB2KDEF.INI'))
      p_web.SSV('Comment:OutWarrantyMarkup','')
  end ! if (p_web.GSV('tmp:OutWarrantyMarkup') > GETINI('STOCK','MaxPartMarkup',,CLIP(PATH())&'\SB2KDEF.INI'))
  
  if (p_web.GSV('tmp:OutWarrantyMarkup') > 0)
      p_web.SSV('tmp:OutWarrantyCost',format(p_web.GSV('tmp:PurchaseCost') + (p_web.GSV('tmp:PurchaseCost') * (p_web.GSV('tmp:OutWarrantyMarkup')/100)),@n_14.2))
  end !if (p_web.GSV('tmp:OutWarrantyMarkup') > 0)
  do Value::tmp:OutWarrantyMarkup
  do SendAlert
  do Comment::tmp:OutWarrantyMarkup ! allows comment style to be updated.
  do Value::tmp:OutWarrantyCost  !1
  do Comment::tmp:OutWarrantyMarkup

ValidateValue::tmp:OutWarrantyMarkup  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:OutWarrantyMarkup',tmp:OutWarrantyMarkup).
    End

Value::tmp:OutWarrantyMarkup  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:OutWarrantyMarkup') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If p_web.GSV('ReadOnly:OutWarrantyMarkup') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    tmp:OutWarrantyMarkup = p_web.RestoreValue('tmp:OutWarrantyMarkup')
    do ValidateValue::tmp:OutWarrantyMarkup
    If tmp:OutWarrantyMarkup:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- tmp:OutWarrantyMarkup
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:OutWarrantyMarkup') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:OutWarrantyMarkup'',''formestimateparts_tmp:outwarrantymarkup_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:OutWarrantyMarkup')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:OutWarrantyMarkup',p_web.GetSessionValue('tmp:OutWarrantyMarkup'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n3',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:OutWarrantyMarkup  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:OutWarrantyMarkup:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:OutWarrantyMarkup'))
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:OutWarrantyMarkup') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:FixedPrice  Routine
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FixedPrice') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:FixedPrice  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::tmp:FixedPrice  ! copies value to session value if valid.
  do Comment::tmp:FixedPrice ! allows comment style to be updated.

ValidateValue::tmp:FixedPrice  Routine
    If not (1=0)
    End

Value::tmp:FixedPrice  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FixedPrice') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:FixedPrice  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:FixedPrice:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FixedPrice') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::epr:Supplier  Routine
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Supplier') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Supplier'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::epr:Supplier  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    epr:Supplier = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    epr:Supplier = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  ElsIf p_web.RequestAjax = 1 and p_web.IfExistsValue('sup:Company_Name')
    epr:Supplier = p_web.GetValue('sup:Company_Name')
  ElsIf p_web.RequestAjax = 1
    epr:Supplier = sup:Company_Name
  End
  do ValidateValue::epr:Supplier  ! copies value to session value if valid.
  p_Web.SetValue('lookupfield','epr:Supplier')
  do AfterLookup
  do Value::epr:Supplier
  do SendAlert
  do Comment::epr:Supplier

ValidateValue::epr:Supplier  Routine
    If not (1=0)
    epr:Supplier = Upper(epr:Supplier)
      if loc:invalid = '' then p_web.SetSessionValue('epr:Supplier',epr:Supplier).
    End

Value::epr:Supplier  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Supplier') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:act = ChangeRecord or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    epr:Supplier = p_web.RestoreValue('epr:Supplier')
    do ValidateValue::epr:Supplier
    If epr:Supplier:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- epr:Supplier
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:act = ChangeRecord,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''epr:Supplier'',''formestimateparts_epr:supplier_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon(epr:Supplier)&''',2);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','epr:Supplier',p_web.GetSessionValue('epr:Supplier'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s30',loc:javascript,p_web.PicLength('@s30'),,,) & '<13,10>'!4
    if not loc:viewonly and not loc:readonly
      loc:fieldclass = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('SelectSuppliers')&'?LookupField=epr:Supplier&Tab=4&ForeignField=sup:Company_Name&_sort=sup:Company_Name&Refresh=sort'),,,,,loc:fieldclass)
    End
  do SendPacket
  End
  p_web.DivFooter()
Comment::epr:Supplier  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if epr:Supplier:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Supplier') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::epr:Exclude_From_Order  Routine
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Exclude_From_Order') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Exclude From Order'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::epr:Exclude_From_Order  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    epr:Exclude_From_Order = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s3
    epr:Exclude_From_Order = p_web.Dformat(p_web.GetValue('Value'),'@s3')
  End
  do ValidateValue::epr:Exclude_From_Order  ! copies value to session value if valid.
  do Value::epr:Exclude_From_Order
  do SendAlert
  do Comment::epr:Exclude_From_Order ! allows comment style to be updated.

ValidateValue::epr:Exclude_From_Order  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('epr:Exclude_From_Order',epr:Exclude_From_Order).
    End

Value::epr:Exclude_From_Order  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Exclude_From_Order') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If loc:retrying
    epr:Exclude_From_Order = p_web.RestoreValue('epr:Exclude_From_Order')
    do ValidateValue::epr:Exclude_From_Order
    If epr:Exclude_From_Order:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- RADIO --- epr:Exclude_From_Order
  loc:readonly = Choose(p_web.GSV('ReadOnly:ExcludeFromOrder') = 1,'disabled','')
    if p_web.GetSessionValue('epr:Exclude_From_Order') = 'YES'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''epr:Exclude_From_Order'',''formestimateparts_epr:exclude_from_order_value'',1,'''&p_web._jsok('YES')&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','epr:Exclude_From_Order',clip('YES'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'epr:Exclude_From_Order_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Yes') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(p_web.GSV('ReadOnly:ExcludeFromOrder') = 1,'disabled','')
    if p_web.GetSessionValue('epr:Exclude_From_Order') = 'NO'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''epr:Exclude_From_Order'',''formestimateparts_epr:exclude_from_order_value'',1,'''&p_web._jsok('NO')&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','epr:Exclude_From_Order',clip('NO'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'epr:Exclude_From_Order_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('No') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::epr:Exclude_From_Order  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if epr:Exclude_From_Order:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Exclude_From_Order') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::epr:PartAllocated  Routine
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('epr:PartAllocated') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Part Allocated'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::epr:PartAllocated  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    epr:PartAllocated = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @n1
    epr:PartAllocated = p_web.Dformat(p_web.GetValue('Value'),'@n1')
  End
  do ValidateValue::epr:PartAllocated  ! copies value to session value if valid.
  do Value::epr:PartAllocated
  do SendAlert
  do Comment::epr:PartAllocated ! allows comment style to be updated.

ValidateValue::epr:PartAllocated  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('epr:PartAllocated',epr:PartAllocated).
    End

Value::epr:PartAllocated  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('epr:PartAllocated') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If loc:retrying
    epr:PartAllocated = p_web.RestoreValue('epr:PartAllocated')
    do ValidateValue::epr:PartAllocated
    If epr:PartAllocated:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- RADIO --- epr:PartAllocated
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartAllocated') = 1,'disabled','')
    if p_web.GetSessionValue('epr:PartAllocated') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''epr:PartAllocated'',''formestimateparts_epr:partallocated_value'',1,'''&p_web._jsok(1)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','epr:PartAllocated',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,'Part Allocated','epr:PartAllocated_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Yes') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartAllocated') = 1,'disabled','')
    if p_web.GetSessionValue('epr:PartAllocated') = 0
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''epr:PartAllocated'',''formestimateparts_epr:partallocated_value'',1,'''&p_web._jsok(0)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','epr:PartAllocated',clip(0),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,'Part Allocated','epr:PartAllocated_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('No') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::epr:PartAllocated  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if epr:PartAllocated:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('epr:PartAllocated') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:UnallocatePart  Routine
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:UnallocatePart') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Unallocate Part'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:UnallocatePart  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:UnallocatePart = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    tmp:UnallocatePart = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:UnallocatePart  ! copies value to session value if valid.
  do Value::tmp:UnallocatePart
  do SendAlert
  do Comment::tmp:UnallocatePart ! allows comment style to be updated.

ValidateValue::tmp:UnallocatePart  Routine
  If p_web.GSV('Show:UnallocatePart') = 1
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:UnallocatePart',tmp:UnallocatePart).
    End
  End

Value::tmp:UnallocatePart  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:UnallocatePart') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If loc:retrying
    tmp:UnallocatePart = p_web.RestoreValue('tmp:UnallocatePart')
    do ValidateValue::tmp:UnallocatePart
    If tmp:UnallocatePart:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- CHECKBOX --- tmp:UnallocatePart
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:UnallocatePart'',''formestimateparts_tmp:unallocatepart_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('tmp:UnallocatePart') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','tmp:UnallocatePart',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:UnallocatePart  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:UnallocatePart:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate('Return part to stock')
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:UnallocatePart') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::text:OrderRequired  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::text:OrderRequired  ! copies value to session value if valid.
  do Comment::text:OrderRequired ! allows comment style to be updated.

ValidateValue::text:OrderRequired  Routine
    If not (1=0)
    End

Value::text:OrderRequired  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('text:OrderRequired') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="text:OrderRequired" class="'&clip('red bold large')&'"'&clip(loc:extra)&'>' & p_web.Translate(p_web.GSV('text:OrderRequired'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::text:OrderRequired  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if text:OrderRequired:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('text:OrderRequired') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::text:OrderRequired2  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::text:OrderRequired2  ! copies value to session value if valid.
  do Comment::text:OrderRequired2 ! allows comment style to be updated.

ValidateValue::text:OrderRequired2  Routine
    If not (1=0)
    End

Value::text:OrderRequired2  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('text:OrderRequired2') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    p_web.Translate('Either reduce the quanitity required, or select the check box to create an order for the excess items',) & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::text:OrderRequired2  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if text:OrderRequired2:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('text:OrderRequired2') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:CreateOrder  Routine
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:CreateOrder') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Create Order For Selected Part'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:CreateOrder  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:CreateOrder = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    tmp:CreateOrder = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:CreateOrder  ! copies value to session value if valid.
  do Value::tmp:CreateOrder
  do SendAlert
  do Comment::tmp:CreateOrder ! allows comment style to be updated.

ValidateValue::tmp:CreateOrder  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:CreateOrder',tmp:CreateOrder).
    End

Value::tmp:CreateOrder  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:CreateOrder') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If loc:retrying
    tmp:CreateOrder = p_web.RestoreValue('tmp:CreateOrder')
    do ValidateValue::tmp:CreateOrder
    If tmp:CreateOrder:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- CHECKBOX --- tmp:CreateOrder
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:CreateOrder'',''formestimateparts_tmp:createorder_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('tmp:CreateOrder') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','tmp:CreateOrder',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:CreateOrder  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:CreateOrder:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:CreateOrder') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:FaultCodesChecked  Routine
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCodesChecked') & '_prompt',Choose(p_web.GSV('Hide:FaultCodesChecked') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:FaultCodesChecked') = 1,'',p_web.Translate('Fault Codes Checked'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:FaultCodesChecked  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:FaultCodesChecked = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    tmp:FaultCodesChecked = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:FaultCodesChecked  ! copies value to session value if valid.
  do Value::tmp:FaultCodesChecked
  do SendAlert
  do Comment::tmp:FaultCodesChecked ! allows comment style to be updated.

ValidateValue::tmp:FaultCodesChecked  Routine
  If 0
    If not (p_web.GSV('Hide:FaultCodesChecked') = 1)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:FaultCodesChecked',tmp:FaultCodesChecked).
    End
  End

Value::tmp:FaultCodesChecked  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:FaultCodesChecked') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCodesChecked') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If loc:retrying
    tmp:FaultCodesChecked = p_web.RestoreValue('tmp:FaultCodesChecked')
    do ValidateValue::tmp:FaultCodesChecked
    If tmp:FaultCodesChecked:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:FaultCodesChecked') = 1)
  ! --- CHECKBOX --- tmp:FaultCodesChecked
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:FaultCodesChecked'',''formestimateparts_tmp:faultcodeschecked_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('tmp:FaultCodesChecked') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','tmp:FaultCodesChecked',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:FaultCodesChecked  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:FaultCodesChecked:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:FaultCodesChecked') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCodesChecked') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:FaultCodesChecked') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:FaultCodes1  Routine
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCodes1') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate(p_web.GSV('Prompt:PartFaultCode1')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:FaultCodes1  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:FaultCodes1 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = p_web.GSV('Picture:PartFaultCode1')  !FieldPicture = 
    tmp:FaultCodes1 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode1'))
  End
  do ValidateValue::tmp:FaultCodes1  ! copies value to session value if valid.
  do Value::tmp:FaultCodes1
  do SendAlert
  do Comment::tmp:FaultCodes1 ! allows comment style to be updated.

ValidateValue::tmp:FaultCodes1  Routine
  If p_web.GSV('Hide:PartFaultCode1') <> 1
    If not (1=0)
  If tmp:FaultCodes1 = '' and p_web.GSV('Req:PartFautlCode1') = 1
    loc:Invalid = 'tmp:FaultCodes1'
    tmp:FaultCodes1:IsInvalid = true
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode1')) & ' ' & p_web.site.RequiredText
  End
    tmp:FaultCodes1 = Upper(tmp:FaultCodes1)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:FaultCodes1',tmp:FaultCodes1).
    End
  End

Value::tmp:FaultCodes1  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCodes1') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.GSV('ReadOnly:PartFaultCode1') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If (p_web.GSV('Req:PartFautlCode1') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,'formrqd')
  End
  If loc:retrying
    tmp:FaultCodes1 = p_web.RestoreValue('tmp:FaultCodes1')
    do ValidateValue::tmp:FaultCodes1
    If tmp:FaultCodes1:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- tmp:FaultCodes1
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode1') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCodes1'',''formestimateparts_tmp:faultcodes1_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes1',p_web.GetSessionValue('tmp:FaultCodes1'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode1'),loc:javascript,,,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(1)
  End
  p_web.DivFooter()
Comment::tmp:FaultCodes1  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:FaultCodes1:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode1'))
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCodes1') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:FaultCode2  Routine
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode2') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate(p_web.GSV('Prompt:PartFaultCode2')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:FaultCode2  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:FaultCodes2 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = p_web.GSV('Picture:PartFaultCode2')  !FieldPicture = 
    tmp:FaultCodes2 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode2'))
  End
  do ValidateValue::tmp:FaultCode2  ! copies value to session value if valid.
  do Value::tmp:FaultCode2
  do SendAlert
  do Comment::tmp:FaultCode2 ! allows comment style to be updated.

ValidateValue::tmp:FaultCode2  Routine
  If p_web.GSV('Hide:PartFaultCode2') <> 1
    If not (1=0)
  If tmp:FaultCodes2 = '' and p_web.GSV('Req:PartFaultCode2') = 1
    loc:Invalid = 'tmp:FaultCodes2'
    tmp:FaultCode2:IsInvalid = true
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode2')) & ' ' & p_web.site.RequiredText
  End
    tmp:FaultCodes2 = Upper(tmp:FaultCodes2)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:FaultCodes2',tmp:FaultCodes2).
    End
  End

Value::tmp:FaultCode2  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode2') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.GSV('ReadOnly:PartFaultCode2') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If (p_web.GSV('Req:PartFaultCode2') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,'formrqd')
  End
  If loc:retrying
    tmp:FaultCodes2 = p_web.RestoreValue('tmp:FaultCodes2')
    do ValidateValue::tmp:FaultCode2
    If tmp:FaultCode2:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- tmp:FaultCodes2
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode2') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode2'',''formestimateparts_tmp:faultcode2_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes2',p_web.GetSessionValue('tmp:FaultCodes2'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode2'),loc:javascript,,,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(2)
  End
  p_web.DivFooter()
Comment::tmp:FaultCode2  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:FaultCode2:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode2'))
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode2') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:FaultCode3  Routine
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode3') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate(p_web.GSV('Prompt:PartFaultCode3')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:FaultCode3  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:FaultCodes3 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = p_web.GSV('Picture:PartFaultCode3')  !FieldPicture = 
    tmp:FaultCodes3 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode3'))
  End
  do ValidateValue::tmp:FaultCode3  ! copies value to session value if valid.
  do Value::tmp:FaultCode3
  do SendAlert
  do Comment::tmp:FaultCode3 ! allows comment style to be updated.

ValidateValue::tmp:FaultCode3  Routine
  If p_web.GSV('Hide:PartFaultCode3') <> 1
    If not (1=0)
  If tmp:FaultCodes3 = '' and p_web.GSV('Req:PartFaultCode3') = 1
    loc:Invalid = 'tmp:FaultCodes3'
    tmp:FaultCode3:IsInvalid = true
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode3')) & ' ' & p_web.site.RequiredText
  End
    tmp:FaultCodes3 = Upper(tmp:FaultCodes3)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:FaultCodes3',tmp:FaultCodes3).
    End
  End

Value::tmp:FaultCode3  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode3') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.GSV('ReadOnly:PartFaultCode3') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If (p_web.GSV('Req:PartFaultCode3') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,'formrqd')
  End
  If loc:retrying
    tmp:FaultCodes3 = p_web.RestoreValue('tmp:FaultCodes3')
    do ValidateValue::tmp:FaultCode3
    If tmp:FaultCode3:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- tmp:FaultCodes3
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode3') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode3'',''formestimateparts_tmp:faultcode3_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes3',p_web.GetSessionValue('tmp:FaultCodes3'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode3'),loc:javascript,,,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(3)
  End
  p_web.DivFooter()
Comment::tmp:FaultCode3  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:FaultCode3:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode3'))
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode3') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:FaultCode4  Routine
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode4') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate(p_web.GSV('Prompt:PartFaultCode4')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:FaultCode4  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:FaultCodes4 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = p_web.GSV('Picture:PartFaultCode4')  !FieldPicture = 
    tmp:FaultCodes4 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode4'))
  End
  do ValidateValue::tmp:FaultCode4  ! copies value to session value if valid.
  do Value::tmp:FaultCode4
  do SendAlert
  do Comment::tmp:FaultCode4 ! allows comment style to be updated.

ValidateValue::tmp:FaultCode4  Routine
  If p_web.GSV('Hide:PartFaultCode4') <> 1
    If not (1=0)
  If tmp:FaultCodes4 = '' and p_web.GSV('Req:PartFaultCode4') = 1
    loc:Invalid = 'tmp:FaultCodes4'
    tmp:FaultCode4:IsInvalid = true
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode4')) & ' ' & p_web.site.RequiredText
  End
    tmp:FaultCodes4 = Upper(tmp:FaultCodes4)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:FaultCodes4',tmp:FaultCodes4).
    End
  End

Value::tmp:FaultCode4  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode4') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.GSV('ReadOnly:PartFaultCode4') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If (p_web.GSV('Req:PartFaultCode4') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,'formrqd')
  End
  If loc:retrying
    tmp:FaultCodes4 = p_web.RestoreValue('tmp:FaultCodes4')
    do ValidateValue::tmp:FaultCode4
    If tmp:FaultCode4:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- tmp:FaultCodes4
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode4') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode4'',''formestimateparts_tmp:faultcode4_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes4',p_web.GetSessionValue('tmp:FaultCodes4'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode4'),loc:javascript,,,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(4)
  End
  p_web.DivFooter()
Comment::tmp:FaultCode4  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:FaultCode4:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode4'))
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode4') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:FaultCode5  Routine
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode5') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate(p_web.GSV('Prompt:PartFaultCode5')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:FaultCode5  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:FaultCodes5 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = p_web.GSV('Picture:PartFaultCode5')  !FieldPicture = 
    tmp:FaultCodes5 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode5'))
  End
  do ValidateValue::tmp:FaultCode5  ! copies value to session value if valid.
  do Value::tmp:FaultCode5
  do SendAlert
  do Comment::tmp:FaultCode5 ! allows comment style to be updated.

ValidateValue::tmp:FaultCode5  Routine
  If p_web.GSV('Hide:PartFaultCode5') <> 1
    If not (1=0)
  If tmp:FaultCodes5 = '' and p_web.GSV('Req:PartFaultCode5') = 1
    loc:Invalid = 'tmp:FaultCodes5'
    tmp:FaultCode5:IsInvalid = true
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode5')) & ' ' & p_web.site.RequiredText
  End
    tmp:FaultCodes5 = Upper(tmp:FaultCodes5)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:FaultCodes5',tmp:FaultCodes5).
    End
  End

Value::tmp:FaultCode5  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode5') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.GSV('ReadOnly:PartFaultCode5') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If (p_web.GSV('Req:PartFaultCode5') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,'formrqd')
  End
  If loc:retrying
    tmp:FaultCodes5 = p_web.RestoreValue('tmp:FaultCodes5')
    do ValidateValue::tmp:FaultCode5
    If tmp:FaultCode5:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- tmp:FaultCodes5
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode5') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode5'',''formestimateparts_tmp:faultcode5_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes5',p_web.GetSessionValue('tmp:FaultCodes5'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode5'),loc:javascript,,,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(5)
  End
  p_web.DivFooter()
Comment::tmp:FaultCode5  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:FaultCode5:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode5'))
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode5') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:FaultCode6  Routine
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode6') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate(p_web.GSV('Prompt:PartFaultCode6')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:FaultCode6  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:FaultCodes6 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = p_web.GSV('Picture:PartFaultCode6')  !FieldPicture = 
    tmp:FaultCodes6 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode6'))
  End
  do ValidateValue::tmp:FaultCode6  ! copies value to session value if valid.
  do Value::tmp:FaultCode6
  do SendAlert
  do Comment::tmp:FaultCode6 ! allows comment style to be updated.

ValidateValue::tmp:FaultCode6  Routine
  If p_web.GSV('Hide:PartFaultCode6') <> 1
    If not (1=0)
  If tmp:FaultCodes6 = '' and p_web.GSV('Req:PartFaultCode6') = 1
    loc:Invalid = 'tmp:FaultCodes6'
    tmp:FaultCode6:IsInvalid = true
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode6')) & ' ' & p_web.site.RequiredText
  End
    tmp:FaultCodes6 = Upper(tmp:FaultCodes6)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:FaultCodes6',tmp:FaultCodes6).
    End
  End

Value::tmp:FaultCode6  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode6') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.GSV('ReadOnly:PartFaultCode6') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If (p_web.GSV('Req:PartFaultCode6') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,'formrqd')
  End
  If loc:retrying
    tmp:FaultCodes6 = p_web.RestoreValue('tmp:FaultCodes6')
    do ValidateValue::tmp:FaultCode6
    If tmp:FaultCode6:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- tmp:FaultCodes6
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode6') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode6'',''formestimateparts_tmp:faultcode6_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes6',p_web.GetSessionValue('tmp:FaultCodes6'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode6'),loc:javascript,,,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(6)
  End
  p_web.DivFooter()
Comment::tmp:FaultCode6  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:FaultCode6:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode6'))
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode6') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:FaultCode7  Routine
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode7') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate(p_web.GSV('Prompt:PartFaultCode7')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:FaultCode7  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:FaultCodes7 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = p_web.GSV('Picture:PartFaultCode7')  !FieldPicture = 
    tmp:FaultCodes7 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode7'))
  End
  do ValidateValue::tmp:FaultCode7  ! copies value to session value if valid.
  do Value::tmp:FaultCode7
  do SendAlert
  do Comment::tmp:FaultCode7 ! allows comment style to be updated.

ValidateValue::tmp:FaultCode7  Routine
  If p_web.GSV('Hide:PartFaultCode7') <> 1
    If not (1=0)
  If tmp:FaultCodes7 = '' and p_web.GSV('Req:PartFaultCode7') = 1
    loc:Invalid = 'tmp:FaultCodes7'
    tmp:FaultCode7:IsInvalid = true
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode7')) & ' ' & p_web.site.RequiredText
  End
    tmp:FaultCodes7 = Upper(tmp:FaultCodes7)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:FaultCodes7',tmp:FaultCodes7).
    End
  End

Value::tmp:FaultCode7  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode7') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.GSV('ReadOnly:PartFaultCode7') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If (p_web.GSV('Req:PartFaultCode7') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,'formrqd')
  End
  If loc:retrying
    tmp:FaultCodes7 = p_web.RestoreValue('tmp:FaultCodes7')
    do ValidateValue::tmp:FaultCode7
    If tmp:FaultCode7:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- tmp:FaultCodes7
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode7') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode7'',''formestimateparts_tmp:faultcode7_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes7',p_web.GetSessionValue('tmp:FaultCodes7'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode7'),loc:javascript,,,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(7)
  End
  p_web.DivFooter()
Comment::tmp:FaultCode7  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:FaultCode7:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode7'))
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode7') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:FaultCode8  Routine
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode8') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate(p_web.GSV('Prompt:PartFaultCode8')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:FaultCode8  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:FaultCodes8 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = p_web.GSV('Picture:PartFaultCode8')  !FieldPicture = 
    tmp:FaultCodes8 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode8'))
  End
  do ValidateValue::tmp:FaultCode8  ! copies value to session value if valid.
  do Value::tmp:FaultCode8
  do SendAlert
  do Comment::tmp:FaultCode8 ! allows comment style to be updated.

ValidateValue::tmp:FaultCode8  Routine
  If p_web.GSV('Hide:PartFaultCode8') <> 1
    If not (1=0)
  If tmp:FaultCodes8 = '' and p_web.GSV('Req:PartFaultCode8') = 1
    loc:Invalid = 'tmp:FaultCodes8'
    tmp:FaultCode8:IsInvalid = true
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode8')) & ' ' & p_web.site.RequiredText
  End
    tmp:FaultCodes8 = Upper(tmp:FaultCodes8)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:FaultCodes8',tmp:FaultCodes8).
    End
  End

Value::tmp:FaultCode8  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode8') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.GSV('ReadOnly:PartFaultCode8') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If (p_web.GSV('Req:PartFaultCode8') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,'formrqd')
  End
  If loc:retrying
    tmp:FaultCodes8 = p_web.RestoreValue('tmp:FaultCodes8')
    do ValidateValue::tmp:FaultCode8
    If tmp:FaultCode8:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- tmp:FaultCodes8
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode8') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode8'',''formestimateparts_tmp:faultcode8_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes8',p_web.GetSessionValue('tmp:FaultCodes8'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode8'),loc:javascript,,,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(8)
  End
  p_web.DivFooter()
Comment::tmp:FaultCode8  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:FaultCode8:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode8'))
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode8') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:FaultCode9  Routine
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode9') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate(p_web.GSV('Prompt:PartFaultCode9')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:FaultCode9  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:FaultCodes9 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = p_web.GSV('Picture:PartFaultCode9')  !FieldPicture = 
    tmp:FaultCodes9 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode9'))
  End
  do ValidateValue::tmp:FaultCode9  ! copies value to session value if valid.
  do Value::tmp:FaultCode9
  do SendAlert
  do Comment::tmp:FaultCode9 ! allows comment style to be updated.

ValidateValue::tmp:FaultCode9  Routine
  If p_web.GSV('Hide:PartFaultCode9') <> 1
    If not (1=0)
  If tmp:FaultCodes9 = '' and p_web.GSV('Req:PartFaultCode9') = 1
    loc:Invalid = 'tmp:FaultCodes9'
    tmp:FaultCode9:IsInvalid = true
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode9')) & ' ' & p_web.site.RequiredText
  End
    tmp:FaultCodes9 = Upper(tmp:FaultCodes9)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:FaultCodes9',tmp:FaultCodes9).
    End
  End

Value::tmp:FaultCode9  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode9') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.GSV('ReadOnly:PartFaultCode9') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If (p_web.GSV('Req:PartFaultCode9') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,'formrqd')
  End
  If loc:retrying
    tmp:FaultCodes9 = p_web.RestoreValue('tmp:FaultCodes9')
    do ValidateValue::tmp:FaultCode9
    If tmp:FaultCode9:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- tmp:FaultCodes9
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode9') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode9'',''formestimateparts_tmp:faultcode9_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes9',p_web.GetSessionValue('tmp:FaultCodes9'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode9'),loc:javascript,,,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(9)
  End
  p_web.DivFooter()
Comment::tmp:FaultCode9  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:FaultCode9:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode9'))
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode9') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:FaultCode10  Routine
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode10') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate(p_web.GSV('Prompt:PartFaultCode10')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:FaultCode10  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:FaultCodes10 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = p_web.GSV('Picture:PartFaultCode10')  !FieldPicture = 
    tmp:FaultCodes10 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode10'))
  End
  do ValidateValue::tmp:FaultCode10  ! copies value to session value if valid.
  do Value::tmp:FaultCode10
  do SendAlert
  do Comment::tmp:FaultCode10 ! allows comment style to be updated.

ValidateValue::tmp:FaultCode10  Routine
  If p_web.GSV('Hide:PartFaultCode10') <> 1
    If not (1=0)
  If tmp:FaultCodes10 = '' and p_web.GSV('Req:PartFaultCode10') = 1
    loc:Invalid = 'tmp:FaultCodes10'
    tmp:FaultCode10:IsInvalid = true
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode10')) & ' ' & p_web.site.RequiredText
  End
    tmp:FaultCodes10 = Upper(tmp:FaultCodes10)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:FaultCodes10',tmp:FaultCodes10).
    End
  End

Value::tmp:FaultCode10  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode10') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.GSV('ReadOnly:PartFaultCode10') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If (p_web.GSV('Req:PartFaultCode10') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,'formrqd')
  End
  If loc:retrying
    tmp:FaultCodes10 = p_web.RestoreValue('tmp:FaultCodes10')
    do ValidateValue::tmp:FaultCode10
    If tmp:FaultCode10:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- tmp:FaultCodes10
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode10') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode10'',''formestimateparts_tmp:faultcode10_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes10',p_web.GetSessionValue('tmp:FaultCodes10'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode10'),loc:javascript,,,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(10)
  End
  p_web.DivFooter()
Comment::tmp:FaultCode10  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:FaultCode10:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode10'))
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode10') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:FaultCode11  Routine
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode11') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate(p_web.GSV('Prompt:PartFaultCode11')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:FaultCode11  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:FaultCodes11 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = p_web.GSV('Picture:PartFaultCode11')  !FieldPicture = 
    tmp:FaultCodes11 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode11'))
  End
  do ValidateValue::tmp:FaultCode11  ! copies value to session value if valid.
  do Value::tmp:FaultCode11
  do SendAlert
  do Comment::tmp:FaultCode11 ! allows comment style to be updated.

ValidateValue::tmp:FaultCode11  Routine
  If p_web.GSV('Hide:PartFaultCode11') <> 1
    If not (1=0)
  If tmp:FaultCodes11 = '' and p_web.GSV('Req:PartFaultCode11') = 1
    loc:Invalid = 'tmp:FaultCodes11'
    tmp:FaultCode11:IsInvalid = true
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode11')) & ' ' & p_web.site.RequiredText
  End
    tmp:FaultCodes11 = Upper(tmp:FaultCodes11)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:FaultCodes11',tmp:FaultCodes11).
    End
  End

Value::tmp:FaultCode11  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode11') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.GSV('ReadOnly:PartFaultCode11') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If (p_web.GSV('Req:PartFaultCode11') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,'formrqd')
  End
  If loc:retrying
    tmp:FaultCodes11 = p_web.RestoreValue('tmp:FaultCodes11')
    do ValidateValue::tmp:FaultCode11
    If tmp:FaultCode11:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- tmp:FaultCodes11
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode11') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode11'',''formestimateparts_tmp:faultcode11_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes11',p_web.GetSessionValue('tmp:FaultCodes11'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode11'),loc:javascript,,,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(11)
  End
  p_web.DivFooter()
Comment::tmp:FaultCode11  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:FaultCode11:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode11'))
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode11') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:FaultCode12  Routine
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode12') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate(p_web.GSV('Prompt:PartFaultCode12')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:FaultCode12  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:FaultCodes12 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = p_web.GSV('Picture:PartFaultCode12')  !FieldPicture = 
    tmp:FaultCodes12 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode12'))
  End
  do ValidateValue::tmp:FaultCode12  ! copies value to session value if valid.
  do Value::tmp:FaultCode12
  do SendAlert
  do Comment::tmp:FaultCode12 ! allows comment style to be updated.

ValidateValue::tmp:FaultCode12  Routine
  If p_web.GSV('Hide:PartFaultCode12') <> 1
    If not (1=0)
  If tmp:FaultCodes12 = '' and p_web.GSV('Req:PartFaultCode12') = 1
    loc:Invalid = 'tmp:FaultCodes12'
    tmp:FaultCode12:IsInvalid = true
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode12')) & ' ' & p_web.site.RequiredText
  End
    tmp:FaultCodes12 = Upper(tmp:FaultCodes12)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:FaultCodes12',tmp:FaultCodes12).
    End
  End

Value::tmp:FaultCode12  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode12') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.GSV('ReadOnly:PartFaultCode12') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If (p_web.GSV('Req:PartFaultCode12') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,'formrqd')
  End
  If loc:retrying
    tmp:FaultCodes12 = p_web.RestoreValue('tmp:FaultCodes12')
    do ValidateValue::tmp:FaultCode12
    If tmp:FaultCode12:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- tmp:FaultCodes12
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode12') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode12'',''formestimateparts_tmp:faultcode12_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes12',p_web.GetSessionValue('tmp:FaultCodes12'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode12'),loc:javascript,,,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(12)
  End
  p_web.DivFooter()
Comment::tmp:FaultCode12  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:FaultCode12:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode12'))
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode12') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::epr:UsedOnRepair  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locPartUsedOnRepair = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locPartUsedOnRepair = p_web.GetValue('Value')
  End
  do ValidateValue::epr:UsedOnRepair  ! copies value to session value if valid.
  do Value::epr:UsedOnRepair
  do SendAlert
  do Comment::epr:UsedOnRepair ! allows comment style to be updated.

ValidateValue::epr:UsedOnRepair  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locPartUsedOnRepair',locPartUsedOnRepair).
    End

Value::epr:UsedOnRepair  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('epr:UsedOnRepair') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If loc:retrying
    locPartUsedOnRepair = p_web.RestoreValue('locPartUsedOnRepair')
    do ValidateValue::epr:UsedOnRepair
    If epr:UsedOnRepair:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- RADIO --- locPartUsedOnRepair
  loc:readonly = Choose(loc:act = ChangeRecord,'disabled','')
    if p_web.GetSessionValue('locPartUsedOnRepair') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''epr:UsedOnRepair'',''formestimateparts_epr:usedonrepair_value'',1,'''&p_web._jsok(1)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locPartUsedOnRepair',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,'Used On Repair','locPartUsedOnRepair_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Use Part NOW') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(loc:act = ChangeRecord,'disabled','')
    if p_web.GetSessionValue('locPartUsedOnRepair') = 2
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''epr:UsedOnRepair'',''formestimateparts_epr:usedonrepair_value'',1,'''&p_web._jsok(2)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locPartUsedOnRepair',clip(2),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,'Used On Repair','locPartUsedOnRepair_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Use Part When Estimate ACCEPTED') & '<13,10>'
    packet = clip(packet) & p_web.br
  do SendPacket
  End
  p_web.DivFooter()
Comment::epr:UsedOnRepair  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if epr:UsedOnRepair:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEstimateParts_' & p_web._nocolon('epr:UsedOnRepair') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('FormEstimateParts_nexttab_' & 0)
    epr:Order_Number = p_web.GSV('epr:Order_Number')
    do ValidateValue::epr:Order_Number
    If loc:Invalid
      loc:retrying = 1
      do Value::epr:Order_Number
      !do SendAlert
      do Comment::epr:Order_Number ! allows comment style to be updated.
      !exit
    End
    epr:Date_Ordered = p_web.GSV('epr:Date_Ordered')
    do ValidateValue::epr:Date_Ordered
    If loc:Invalid
      loc:retrying = 1
      do Value::epr:Date_Ordered
      !do SendAlert
      do Comment::epr:Date_Ordered ! allows comment style to be updated.
      !exit
    End
    epr:Date_Received = p_web.GSV('epr:Date_Received')
    do ValidateValue::epr:Date_Received
    If loc:Invalid
      loc:retrying = 1
      do Value::epr:Date_Received
      !do SendAlert
      do Comment::epr:Date_Received ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('FormEstimateParts_nexttab_' & 1)
    epr:Part_Number = p_web.GSV('epr:Part_Number')
    do ValidateValue::epr:Part_Number
    If loc:Invalid
      loc:retrying = 1
      do Value::epr:Part_Number
      !do SendAlert
      do Comment::epr:Part_Number ! allows comment style to be updated.
      !exit
    End
    epr:Description = p_web.GSV('epr:Description')
    do ValidateValue::epr:Description
    If loc:Invalid
      loc:retrying = 1
      do Value::epr:Description
      !do SendAlert
      do Comment::epr:Description ! allows comment style to be updated.
      !exit
    End
    epr:Despatch_Note_Number = p_web.GSV('epr:Despatch_Note_Number')
    do ValidateValue::epr:Despatch_Note_Number
    If loc:Invalid
      loc:retrying = 1
      do Value::epr:Despatch_Note_Number
      !do SendAlert
      do Comment::epr:Despatch_Note_Number ! allows comment style to be updated.
      !exit
    End
    epr:Quantity = p_web.GSV('epr:Quantity')
    do ValidateValue::epr:Quantity
    If loc:Invalid
      loc:retrying = 1
      do Value::epr:Quantity
      !do SendAlert
      do Comment::epr:Quantity ! allows comment style to be updated.
      !exit
    End
    tmp:Location = p_web.GSV('tmp:Location')
    do ValidateValue::tmp:Location
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:Location
      !do SendAlert
      do Comment::tmp:Location ! allows comment style to be updated.
      !exit
    End
    tmp:SecondLocation = p_web.GSV('tmp:SecondLocation')
    do ValidateValue::tmp:SecondLocation
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:SecondLocation
      !do SendAlert
      do Comment::tmp:SecondLocation ! allows comment style to be updated.
      !exit
    End
    tmp:ShelfLocation = p_web.GSV('tmp:ShelfLocation')
    do ValidateValue::tmp:ShelfLocation
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:ShelfLocation
      !do SendAlert
      do Comment::tmp:ShelfLocation ! allows comment style to be updated.
      !exit
    End
    tmp:PurchaseCost = p_web.GSV('tmp:PurchaseCost')
    do ValidateValue::tmp:PurchaseCost
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:PurchaseCost
      !do SendAlert
      do Comment::tmp:PurchaseCost ! allows comment style to be updated.
      !exit
    End
    tmp:OutWarrantyCost = p_web.GSV('tmp:OutWarrantyCost')
    do ValidateValue::tmp:OutWarrantyCost
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:OutWarrantyCost
      !do SendAlert
      do Comment::tmp:OutWarrantyCost ! allows comment style to be updated.
      !exit
    End
    tmp:OutWarrantyMarkup = p_web.GSV('tmp:OutWarrantyMarkup')
    do ValidateValue::tmp:OutWarrantyMarkup
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:OutWarrantyMarkup
      !do SendAlert
      do Comment::tmp:OutWarrantyMarkup ! allows comment style to be updated.
      !exit
    End
    epr:Supplier = p_web.GSV('epr:Supplier')
    do ValidateValue::epr:Supplier
    If loc:Invalid
      loc:retrying = 1
      do Value::epr:Supplier
      !do SendAlert
      do Comment::epr:Supplier ! allows comment style to be updated.
      !exit
    End
    epr:Exclude_From_Order = p_web.GSV('epr:Exclude_From_Order')
    do ValidateValue::epr:Exclude_From_Order
    If loc:Invalid
      loc:retrying = 1
      do Value::epr:Exclude_From_Order
      !do SendAlert
      do Comment::epr:Exclude_From_Order ! allows comment style to be updated.
      !exit
    End
    epr:PartAllocated = p_web.GSV('epr:PartAllocated')
    do ValidateValue::epr:PartAllocated
    If loc:Invalid
      loc:retrying = 1
      do Value::epr:PartAllocated
      !do SendAlert
      do Comment::epr:PartAllocated ! allows comment style to be updated.
      !exit
    End
    tmp:UnallocatePart = p_web.GSV('tmp:UnallocatePart')
    do ValidateValue::tmp:UnallocatePart
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:UnallocatePart
      !do SendAlert
      do Comment::tmp:UnallocatePart ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('FormEstimateParts_nexttab_' & 2)
    tmp:CreateOrder = p_web.GSV('tmp:CreateOrder')
    do ValidateValue::tmp:CreateOrder
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:CreateOrder
      !do SendAlert
      do Comment::tmp:CreateOrder ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('FormEstimateParts_nexttab_' & 3)
    tmp:FaultCodesChecked = p_web.GSV('tmp:FaultCodesChecked')
    do ValidateValue::tmp:FaultCodesChecked
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:FaultCodesChecked
      !do SendAlert
      do Comment::tmp:FaultCodesChecked ! allows comment style to be updated.
      !exit
    End
    tmp:FaultCodes1 = p_web.GSV('tmp:FaultCodes1')
    do ValidateValue::tmp:FaultCodes1
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:FaultCodes1
      !do SendAlert
      do Comment::tmp:FaultCodes1 ! allows comment style to be updated.
      !exit
    End
    tmp:FaultCodes2 = p_web.GSV('tmp:FaultCodes2')
    do ValidateValue::tmp:FaultCode2
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:FaultCode2
      !do SendAlert
      do Comment::tmp:FaultCode2 ! allows comment style to be updated.
      !exit
    End
    tmp:FaultCodes3 = p_web.GSV('tmp:FaultCodes3')
    do ValidateValue::tmp:FaultCode3
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:FaultCode3
      !do SendAlert
      do Comment::tmp:FaultCode3 ! allows comment style to be updated.
      !exit
    End
    tmp:FaultCodes4 = p_web.GSV('tmp:FaultCodes4')
    do ValidateValue::tmp:FaultCode4
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:FaultCode4
      !do SendAlert
      do Comment::tmp:FaultCode4 ! allows comment style to be updated.
      !exit
    End
    tmp:FaultCodes5 = p_web.GSV('tmp:FaultCodes5')
    do ValidateValue::tmp:FaultCode5
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:FaultCode5
      !do SendAlert
      do Comment::tmp:FaultCode5 ! allows comment style to be updated.
      !exit
    End
    tmp:FaultCodes6 = p_web.GSV('tmp:FaultCodes6')
    do ValidateValue::tmp:FaultCode6
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:FaultCode6
      !do SendAlert
      do Comment::tmp:FaultCode6 ! allows comment style to be updated.
      !exit
    End
    tmp:FaultCodes7 = p_web.GSV('tmp:FaultCodes7')
    do ValidateValue::tmp:FaultCode7
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:FaultCode7
      !do SendAlert
      do Comment::tmp:FaultCode7 ! allows comment style to be updated.
      !exit
    End
    tmp:FaultCodes8 = p_web.GSV('tmp:FaultCodes8')
    do ValidateValue::tmp:FaultCode8
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:FaultCode8
      !do SendAlert
      do Comment::tmp:FaultCode8 ! allows comment style to be updated.
      !exit
    End
    tmp:FaultCodes9 = p_web.GSV('tmp:FaultCodes9')
    do ValidateValue::tmp:FaultCode9
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:FaultCode9
      !do SendAlert
      do Comment::tmp:FaultCode9 ! allows comment style to be updated.
      !exit
    End
    tmp:FaultCodes10 = p_web.GSV('tmp:FaultCodes10')
    do ValidateValue::tmp:FaultCode10
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:FaultCode10
      !do SendAlert
      do Comment::tmp:FaultCode10 ! allows comment style to be updated.
      !exit
    End
    tmp:FaultCodes11 = p_web.GSV('tmp:FaultCodes11')
    do ValidateValue::tmp:FaultCode11
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:FaultCode11
      !do SendAlert
      do Comment::tmp:FaultCode11 ! allows comment style to be updated.
      !exit
    End
    tmp:FaultCodes12 = p_web.GSV('tmp:FaultCodes12')
    do ValidateValue::tmp:FaultCode12
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:FaultCode12
      !do SendAlert
      do Comment::tmp:FaultCode12 ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('FormEstimateParts_nexttab_' & 4)
    locPartUsedOnRepair = p_web.GSV('locPartUsedOnRepair')
    do ValidateValue::epr:UsedOnRepair
    If loc:Invalid
      loc:retrying = 1
      do Value::epr:UsedOnRepair
      !do SendAlert
      do Comment::epr:UsedOnRepair ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_FormEstimateParts_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('FormEstimateParts_tab_' & 0)
    do GenerateTab0
  of lower('FormEstimateParts_tab_' & 1)
    do GenerateTab1
  of lower('FormEstimateParts_epr:Part_Number_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::epr:Part_Number
      of event:timer
        do Value::epr:Part_Number
        do Comment::epr:Part_Number
      else
        do Value::epr:Part_Number
      end
  of lower('FormEstimateParts_epr:Description_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::epr:Description
      of event:timer
        do Value::epr:Description
        do Comment::epr:Description
      else
        do Value::epr:Description
      end
  of lower('FormEstimateParts_epr:Despatch_Note_Number_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::epr:Despatch_Note_Number
      of event:timer
        do Value::epr:Despatch_Note_Number
        do Comment::epr:Despatch_Note_Number
      else
        do Value::epr:Despatch_Note_Number
      end
  of lower('FormEstimateParts_epr:Quantity_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::epr:Quantity
      of event:timer
        do Value::epr:Quantity
        do Comment::epr:Quantity
      else
        do Value::epr:Quantity
      end
  of lower('FormEstimateParts_tmp:PurchaseCost_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:PurchaseCost
      of event:timer
        do Value::tmp:PurchaseCost
        do Comment::tmp:PurchaseCost
      else
        do Value::tmp:PurchaseCost
      end
  of lower('FormEstimateParts_tmp:OutWarrantyCost_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:OutWarrantyCost
      of event:timer
        do Value::tmp:OutWarrantyCost
        do Comment::tmp:OutWarrantyCost
      else
        do Value::tmp:OutWarrantyCost
      end
  of lower('FormEstimateParts_tmp:OutWarrantyMarkup_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:OutWarrantyMarkup
      of event:timer
        do Value::tmp:OutWarrantyMarkup
        do Comment::tmp:OutWarrantyMarkup
      else
        do Value::tmp:OutWarrantyMarkup
      end
  of lower('FormEstimateParts_epr:Supplier_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::epr:Supplier
      of event:timer
        do Value::epr:Supplier
        do Comment::epr:Supplier
      else
        do Value::epr:Supplier
      end
  of lower('FormEstimateParts_epr:Exclude_From_Order_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::epr:Exclude_From_Order
      of event:timer
        do Value::epr:Exclude_From_Order
        do Comment::epr:Exclude_From_Order
      else
        do Value::epr:Exclude_From_Order
      end
  of lower('FormEstimateParts_epr:PartAllocated_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::epr:PartAllocated
      of event:timer
        do Value::epr:PartAllocated
        do Comment::epr:PartAllocated
      else
        do Value::epr:PartAllocated
      end
  of lower('FormEstimateParts_tmp:UnallocatePart_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:UnallocatePart
      of event:timer
        do Value::tmp:UnallocatePart
        do Comment::tmp:UnallocatePart
      else
        do Value::tmp:UnallocatePart
      end
  of lower('FormEstimateParts_tab_' & 2)
    do GenerateTab2
  of lower('FormEstimateParts_tmp:CreateOrder_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:CreateOrder
      of event:timer
        do Value::tmp:CreateOrder
        do Comment::tmp:CreateOrder
      else
        do Value::tmp:CreateOrder
      end
  of lower('FormEstimateParts_tab_' & 3)
    do GenerateTab3
  of lower('FormEstimateParts_tmp:FaultCodesChecked_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCodesChecked
      of event:timer
        do Value::tmp:FaultCodesChecked
        do Comment::tmp:FaultCodesChecked
      else
        do Value::tmp:FaultCodesChecked
      end
  of lower('FormEstimateParts_tmp:FaultCodes1_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCodes1
      of event:timer
        do Value::tmp:FaultCodes1
        do Comment::tmp:FaultCodes1
      else
        do Value::tmp:FaultCodes1
      end
  of lower('FormEstimateParts_tmp:FaultCode2_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode2
      of event:timer
        do Value::tmp:FaultCode2
        do Comment::tmp:FaultCode2
      else
        do Value::tmp:FaultCode2
      end
  of lower('FormEstimateParts_tmp:FaultCode3_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode3
      of event:timer
        do Value::tmp:FaultCode3
        do Comment::tmp:FaultCode3
      else
        do Value::tmp:FaultCode3
      end
  of lower('FormEstimateParts_tmp:FaultCode4_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode4
      of event:timer
        do Value::tmp:FaultCode4
        do Comment::tmp:FaultCode4
      else
        do Value::tmp:FaultCode4
      end
  of lower('FormEstimateParts_tmp:FaultCode5_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode5
      of event:timer
        do Value::tmp:FaultCode5
        do Comment::tmp:FaultCode5
      else
        do Value::tmp:FaultCode5
      end
  of lower('FormEstimateParts_tmp:FaultCode6_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode6
      of event:timer
        do Value::tmp:FaultCode6
        do Comment::tmp:FaultCode6
      else
        do Value::tmp:FaultCode6
      end
  of lower('FormEstimateParts_tmp:FaultCode7_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode7
      of event:timer
        do Value::tmp:FaultCode7
        do Comment::tmp:FaultCode7
      else
        do Value::tmp:FaultCode7
      end
  of lower('FormEstimateParts_tmp:FaultCode8_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode8
      of event:timer
        do Value::tmp:FaultCode8
        do Comment::tmp:FaultCode8
      else
        do Value::tmp:FaultCode8
      end
  of lower('FormEstimateParts_tmp:FaultCode9_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode9
      of event:timer
        do Value::tmp:FaultCode9
        do Comment::tmp:FaultCode9
      else
        do Value::tmp:FaultCode9
      end
  of lower('FormEstimateParts_tmp:FaultCode10_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode10
      of event:timer
        do Value::tmp:FaultCode10
        do Comment::tmp:FaultCode10
      else
        do Value::tmp:FaultCode10
      end
  of lower('FormEstimateParts_tmp:FaultCode11_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode11
      of event:timer
        do Value::tmp:FaultCode11
        do Comment::tmp:FaultCode11
      else
        do Value::tmp:FaultCode11
      end
  of lower('FormEstimateParts_tmp:FaultCode12_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode12
      of event:timer
        do Value::tmp:FaultCode12
        do Comment::tmp:FaultCode12
      else
        do Value::tmp:FaultCode12
      end
  of lower('FormEstimateParts_tab_' & 4)
    do GenerateTab4
  of lower('FormEstimateParts_epr:UsedOnRepair_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::epr:UsedOnRepair
      of event:timer
        do Value::epr:UsedOnRepair
        do Comment::epr:UsedOnRepair
      else
        do Value::epr:UsedOnRepair
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('FormEstimateParts_form:ready_',1)

  p_web.SetSessionValue('FormEstimateParts_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_FormEstimateParts',0)
  epr:Fault_Codes_Checked = 'NO'
  p_web.SetSessionValue('epr:Fault_Codes_Checked',epr:Fault_Codes_Checked)
  epr:Credit = 'NO'
  p_web.SetSessionValue('epr:Credit',epr:Credit)
  epr:Requested = 0
  p_web.SetSessionValue('epr:Requested',epr:Requested)
  epr:UsedOnRepair = 0
  p_web.SetSessionValue('epr:UsedOnRepair',epr:UsedOnRepair)
  epr:PartAllocated = 0
  p_web.SetSessionValue('epr:PartAllocated',epr:PartAllocated)
  epr:Ref_Number = p_web.GSV('wob:RefNumber')
  p_web.SetSessionValue('epr:Ref_Number',epr:Ref_Number)
  epr:Quantity = 1
  p_web.SetSessionValue('epr:Quantity',epr:Quantity)
  epr:Exclude_From_Order = 'NO'
  p_web.SetSessionValue('epr:Exclude_From_Order',epr:Exclude_From_Order)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('FormEstimateParts_form:ready_',1)
  p_web.SetSessionValue('FormEstimateParts_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormEstimateParts',0)
  p_web._PreCopyRecord(ESTPARTS,epr:record_number_key)
  ! here we need to copy the non-unique fields across
  epr:Ref_Number = p_web.GSV('wob:RefNumber')
  p_web.SetSessionValue('epr:Ref_Number',p_web.GSV('wob:RefNumber'))
  epr:Quantity = 1
  p_web.SetSessionValue('epr:Quantity',1)
  epr:Exclude_From_Order = 'NO'
  p_web.SetSessionValue('epr:Exclude_From_Order','NO')
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('FormEstimateParts_form:ready_',1)
  p_web.SetSessionValue('FormEstimateParts_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('FormEstimateParts:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('FormEstimateParts_form:ready_',1)
  p_web.SetSessionValue('FormEstimateParts_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('FormEstimateParts:Primed',0)
  p_web.setsessionvalue('showtab_FormEstimateParts',0)
  do SetFormSettings

LoadRelatedRecords  Routine
    if (p_web.ifExistsValue('adjustment'))
        p_web.storeValue('adjustment')
        !        p_web.SSV('adjustment',p_web.getValue('adjustment'))
        if (p_web.getValue('adjustment') = 1)
            epr:Part_Number = 'ADJUSTMENT'
            epr:Description = 'ADJUSTMENT'
            epr:Adjustment = 'YES'
            epr:Quantity = 1
            epr:Warranty_Part = 'NO'
            epr:Exclude_From_Order = 'YES'

            p_web.SSV('epr:Part_Number',epr:Part_Number)
            p_web.SSV('epr:Description',epr:Description)
            p_web.SSV('epr:Adjustment',epr:Adjustment)
            p_web.SSV('epr:Quantity',epr:Quantity)
            p_web.SSV('epr:Warranty_Part',epr:Warranty_Part)
            p_web.SSV('epr:Exclude_From_Order',epr:Exclude_From_Order)

        end ! if (p_web.getValue('adjustment') = 1)
    end !if (p_web.ifExistsValue('adjustment'))

    if (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'JOB PART COSTS - EDIT'))
        p_web.SSV('ReadOnly:OutWarrantyMarkup',1)
        p_web.SSV('ReadOnly:PurchaseCost',1)
        p_web.SSV('ReadOnly:OutWarrantyCOst',1)
    else !if (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'JOB PART COSTS - EDIT'))
        p_web.SSV('ReadOnly:OutWarrantyMarkup',0)
        p_web.SSV('ReadOnly:PurchaseCost',0)
        p_web.SSV('ReadOnly:OutWarrantyCOst',0)
    end ! if (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'JOB PART COSTS - EDIT'))

    if (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'PART ALLOCATED'))
        p_web.SSV('ReadOnly:PartAllocated',1)
    else ! if (SecurityCheckFailed(p_web.GSV('BookedUser'),'PART ALLOCATED'))
        p_web.SSV('ReadOnly:PartAllocated',0)
    end ! if (SecurityCheckFailed(p_web.GSV('BookedUser'),'PART ALLOCATED'))

    !      if (epr:WebOrder = 1)
    !          p_web.SSV('ReadOnly:Quantity',1)
    !      end ! if (epr:WebOrder = 1)

    p_web.SSV('ReadOnly:ExcludeFromOrder',0)
    IF (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'PARTS - EXCLUDE FROM ORDER'))
        p_web.SSV('ReadOnly:ExcludeFromOrder',1)
    END

    if (loc:Act = ChangeRecord)
        if (p_web.GSV('job:Date_Completed') > 0)
            if (~SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'JOBS COSTS - EDIT POST COMPLETE'))
                p_web.SSV('ReadOnly:OutWarrantyCost',1)
                p_web.SSV('ReadOnly:Quantity',1)
            end !if (SecurityCheck('JOBS COSTS - EDIT POST COMPLETE'))
        end ! if (p_web.GSV('job:Date_Completed') > 0)

        if (p_web.GSV('job:Estimate') = 'YES')
            Access:ESTPARTS.Clearkey(epr:part_Ref_Number_Key)
            epr:Ref_Number    = p_web.GSV('epr:Ref_Number')
            epr:Part_Ref_Number    = p_web.GSV('epr:Part_Ref_Number')
            if (Access:ESTPARTS.TryFetch(epr:part_Ref_Number_Key) = Level:Benign)
                ! Found
                ! This is same part as on the estimate
                p_web.SSV('ReadOnly:OutWarrantyCost',1)
                p_web.SSV('ReadOnly:OutWarrantyMarkup',1)
            else ! if (Access:ESTPARTS.TryFetch(epr:part_Ref_Number_Key) = Level:Benign)
                ! Error
            end ! if (Access:ESTPARTS.TryFetch(epr:part_Ref_Number_Key) = Level:Benign)
        end ! if (p_web.GSV('job:Estimate') = 'YES')
        p_web.SSV('ReadOnly:ExcludeFromOrder',1)
    end ! if (loc:Act = ChangeRecord)

    do enableDisableCosts
    do showCosts
    do lookupLocation
    do lookupMainFault


    if (p_web.GSV('Part:ViewOnly') <> 1)
        ! Show unallodate part tick box

        if (loc:Act = ChangeRecord)
            if (p_web.GSV('epr:Part_Ref_Number') <> '' And |
                p_web.GSV('epr:PartAllocated') = 1 And |
                p_web.GSV('epr:WebOrder') = 0)
                p_web.SSV('Show:UnallocatePart',1)
            end ! if (p_web.GSV('epr:Part_Ref_Number') <> '' And |
        end ! if (loc:Act = ChangeRecord)
    end ! if (p_web.GSV('Part:ViewOnly') <> 1)
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
  If loc:act = ChangeRecord
  End
      If not (1=0)
        If not (p_web.getValue('adjustment') = 1 OR loc:act = ChangeRecord)
          If p_web.IfExistsValue('epr:Part_Number')
            epr:Part_Number = p_web.GetValue('epr:Part_Number')
          End
        End
      End
      If not (1=0)
        If not (p_web.getValue('adjustment') = 1 OR loc:act = ChangeRecord)
          If p_web.IfExistsValue('epr:Description')
            epr:Description = p_web.GetValue('epr:Description')
          End
        End
      End
      If not (1=0)
          If p_web.IfExistsValue('epr:Despatch_Note_Number')
            epr:Despatch_Note_Number = p_web.GetValue('epr:Despatch_Note_Number')
          End
      End
      If not (1=0)
        If not (p_web.getValue('adjustment') = 1 OR p_web.GSV('ReadOnly:Quantity') = 1 or loc:act = ChangeRecord)
          If p_web.IfExistsValue('epr:Quantity')
            epr:Quantity = p_web.GetValue('epr:Quantity')
          End
        End
      End
      If not (1=0)
        If not (p_web.getValue('adjustment') = 1 or p_web.GSV('ReadOnly:PurchaseCost') = 1 OR loc:act = ChangeRecord)
          If p_web.IfExistsValue('tmp:PurchaseCost')
            tmp:PurchaseCost = p_web.GetValue('tmp:PurchaseCost')
          End
        End
      End
      If not (1=0)
        If not (p_web.GSV('ReadOnly:OutWarrantyCost') = 1 Or p_web.GSV('tmp:OutWarrantyMarkup') > 0)
          If p_web.IfExistsValue('tmp:OutWarrantyCost')
            tmp:OutWarrantyCost = p_web.GetValue('tmp:OutWarrantyCost')
          End
        End
      End
      If not (1=0)
        If not (p_web.GSV('ReadOnly:OutWarrantyMarkup') = 1)
          If p_web.IfExistsValue('tmp:OutWarrantyMarkup')
            tmp:OutWarrantyMarkup = p_web.GetValue('tmp:OutWarrantyMarkup')
          End
        End
      End
      If not (1=0)
        If not (loc:act = ChangeRecord)
          If p_web.IfExistsValue('epr:Supplier')
            epr:Supplier = p_web.GetValue('epr:Supplier')
          End
        End
      End
      If not (1=0)
        If not (p_web.GSV('ReadOnly:ExcludeFromOrder') = 1)
          If p_web.IfExistsValue('epr:Exclude_From_Order')
            epr:Exclude_From_Order = p_web.GetValue('epr:Exclude_From_Order')
          End
        End
      End
      If not (1=0)
        If not (p_web.GSV('ReadOnly:PartAllocated') = 1)
          If p_web.IfExistsValue('epr:PartAllocated')
            epr:PartAllocated = p_web.GetValue('epr:PartAllocated')
          End
        End
      End
    If (p_web.GSV('Show:UnallocatePart') = 1)
      If not (1=0)
          If p_web.IfExistsValue('tmp:UnallocatePart') = 0
            p_web.SetValue('tmp:UnallocatePart',0)
            tmp:UnallocatePart = 0
          Else
            tmp:UnallocatePart = p_web.GetValue('tmp:UnallocatePart')
          End
      End
    End
  If p_web.GSV('locOrderRequired') = 1
      If not (1=0)
          If p_web.IfExistsValue('tmp:CreateOrder') = 0
            p_web.SetValue('tmp:CreateOrder',0)
            tmp:CreateOrder = 0
          Else
            tmp:CreateOrder = p_web.GetValue('tmp:CreateOrder')
          End
      End
  End
    If (0)
      If not (p_web.GSV('Hide:FaultCodesChecked') = 1)
          If p_web.IfExistsValue('tmp:FaultCodesChecked') = 0
            p_web.SetValue('tmp:FaultCodesChecked',0)
            tmp:FaultCodesChecked = 0
          Else
            tmp:FaultCodesChecked = p_web.GetValue('tmp:FaultCodesChecked')
          End
      End
    End
    If (p_web.GSV('Hide:PartFaultCode1') <> 1)
      If not (1=0)
        If not (p_web.GSV('ReadOnly:PartFaultCode1') = 1)
          If p_web.IfExistsValue('tmp:FaultCodes1')
            tmp:FaultCodes1 = p_web.GetValue('tmp:FaultCodes1')
          End
        End
      End
    End
    If (p_web.GSV('Hide:PartFaultCode2') <> 1)
      If not (1=0)
        If not (p_web.GSV('ReadOnly:PartFaultCode2') = 1)
          If p_web.IfExistsValue('tmp:FaultCodes2')
            tmp:FaultCodes2 = p_web.GetValue('tmp:FaultCodes2')
          End
        End
      End
    End
    If (p_web.GSV('Hide:PartFaultCode3') <> 1)
      If not (1=0)
        If not (p_web.GSV('ReadOnly:PartFaultCode3') = 1)
          If p_web.IfExistsValue('tmp:FaultCodes3')
            tmp:FaultCodes3 = p_web.GetValue('tmp:FaultCodes3')
          End
        End
      End
    End
    If (p_web.GSV('Hide:PartFaultCode4') <> 1)
      If not (1=0)
        If not (p_web.GSV('ReadOnly:PartFaultCode4') = 1)
          If p_web.IfExistsValue('tmp:FaultCodes4')
            tmp:FaultCodes4 = p_web.GetValue('tmp:FaultCodes4')
          End
        End
      End
    End
    If (p_web.GSV('Hide:PartFaultCode5') <> 1)
      If not (1=0)
        If not (p_web.GSV('ReadOnly:PartFaultCode5') = 1)
          If p_web.IfExistsValue('tmp:FaultCodes5')
            tmp:FaultCodes5 = p_web.GetValue('tmp:FaultCodes5')
          End
        End
      End
    End
    If (p_web.GSV('Hide:PartFaultCode6') <> 1)
      If not (1=0)
        If not (p_web.GSV('ReadOnly:PartFaultCode6') = 1)
          If p_web.IfExistsValue('tmp:FaultCodes6')
            tmp:FaultCodes6 = p_web.GetValue('tmp:FaultCodes6')
          End
        End
      End
    End
    If (p_web.GSV('Hide:PartFaultCode7') <> 1)
      If not (1=0)
        If not (p_web.GSV('ReadOnly:PartFaultCode7') = 1)
          If p_web.IfExistsValue('tmp:FaultCodes7')
            tmp:FaultCodes7 = p_web.GetValue('tmp:FaultCodes7')
          End
        End
      End
    End
    If (p_web.GSV('Hide:PartFaultCode8') <> 1)
      If not (1=0)
        If not (p_web.GSV('ReadOnly:PartFaultCode8') = 1)
          If p_web.IfExistsValue('tmp:FaultCodes8')
            tmp:FaultCodes8 = p_web.GetValue('tmp:FaultCodes8')
          End
        End
      End
    End
    If (p_web.GSV('Hide:PartFaultCode9') <> 1)
      If not (1=0)
        If not (p_web.GSV('ReadOnly:PartFaultCode9') = 1)
          If p_web.IfExistsValue('tmp:FaultCodes9')
            tmp:FaultCodes9 = p_web.GetValue('tmp:FaultCodes9')
          End
        End
      End
    End
    If (p_web.GSV('Hide:PartFaultCode10') <> 1)
      If not (1=0)
        If not (p_web.GSV('ReadOnly:PartFaultCode10') = 1)
          If p_web.IfExistsValue('tmp:FaultCodes10')
            tmp:FaultCodes10 = p_web.GetValue('tmp:FaultCodes10')
          End
        End
      End
    End
    If (p_web.GSV('Hide:PartFaultCode11') <> 1)
      If not (1=0)
        If not (p_web.GSV('ReadOnly:PartFaultCode11') = 1)
          If p_web.IfExistsValue('tmp:FaultCodes11')
            tmp:FaultCodes11 = p_web.GetValue('tmp:FaultCodes11')
          End
        End
      End
    End
    If (p_web.GSV('Hide:PartFaultCode12') <> 1)
      If not (1=0)
        If not (p_web.GSV('ReadOnly:PartFaultCode12') = 1)
          If p_web.IfExistsValue('tmp:FaultCodes12')
            tmp:FaultCodes12 = p_web.GetValue('tmp:FaultCodes12')
          End
        End
      End
    End
      If not (1=0)
        If not (loc:act = ChangeRecord)
          If p_web.IfExistsValue('locPartUsedOnRepair')
            locPartUsedOnRepair = p_web.GetValue('locPartUsedOnRepair')
          End
        End
      End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
      if (p_web.GSV('locPartUsedOnRepair') = 0)
          loc:alert = 'Select When The Part Will Be Used'
          loc:invalid = 'locPartUsedOnRepair'
      END
  ! Insert Record Validation
  p_web.SSV('AddToStockAllocation:Type','')
  epr:PartAllocated = 1
  
  Access:DEFAULTS.Clearkey(def:RecordNumberKey)
  def:Record_Number    = 1
  set(def:RecordNumberKey,def:RecordNumberKey)
  loop
      if (Access:DEFAULTS.Next())
          Break
      end ! if (Access:DEFAULTS.Next())
      break
  end ! loop
  
  stockPart# = 0
  if (epr:part_Ref_Number <> '')
      Access:STOCK.Clearkey(sto:Ref_Number_Key)
      sto:Ref_Number = p_web.GSV('epr:part_Ref_Number')
      if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
          ! Found
          ! Check for duplicate?
          stockPart# = 1
          found# = 0
          if (sto:AllowDuplicate = 0)
  
              Access:PARTS_ALIAS.Clearkey(par_ali:Part_Number_Key)
              par_ali:Ref_Number    = p_web.GSV('job:Ref_Number')
              par_Ali:Part_Number = p_web.GSV('epr:part_Number')
              set(par_ali:Part_Number_Key,par_ali:Part_Number_Key)
              loop
                  if (Access:PARTS_ALIAS.Next())
                      Break
                  end ! if (Access:PARTS.Next())
                  if (par_ali:Ref_Number    <> p_web.GSV('job:Ref_Number'))
                      Break
                  end ! if (epr:Ref_Number    <> p_web.GSV('job:Ref_Number'))
                  if (par_ali:part_Number <> p_web.GSV('epr:Part_Number'))
                      Break
                  end ! if (epr:Part_Number    <> p_web.GSV('epr:Part_Number'))
                  if (par_ali:Date_Received = '')
                      found# = 1
                      break
                  end ! if (epr:Date_Received = '')
              end ! loop
          end ! if (sto:AllowDuplicate = 0)
  
          if (found# = 1)
              loc:Invalid = 'epr:Part_Number'
              loc:Alert = 'This part is already attached to this job.'
              exit
          end ! if (epr:Date_Received = '')
      else ! if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
          ! Error
      end ! if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
  else ! if (p_web.GSV('epr:Part_Ref_Number') <> '')
  end ! if (p_web.GSV('epr:Part_Ref_Number') <> '')
  
  ! update stock details
  p_web.SSV('locOrderRequired',0)
  if (p_web.GSV('epr:part_Number') <> 'ADJUSTMENT')
      if (p_web.GSV('epr:exclude_From_Order') <> 'YES')
          if (stockPart# = 1)
  
              IF (p_web.GSV('locPartUsedOnRepair') = 1)
  
                  p_web.SSV('epr:UsedOnRepair',1)
                  if (sto:Sundry_Item <> 'YES')
                      if (p_web.GSV('epr:Quantity') > sto:Quantity_Stock)
                          if (p_web.GSV('locOrderRequired') <> 1 and p_web.GSV('tmp:CreateOrder') = 0)
                              p_web.SSV('locOrderRequired',1)
                              loc:Invalid = 'tmp:OrderPart'
                              loc:Alert = 'Insufficient Items In Stock'
                              p_web.SSV('text:OrderRequired','Qty Required: ' & p_web.GSV('epr:Quantity') & ', Qty In Stock: ' & sto:Quantity_Stock)
                          else ! if (p_web.GSV('locOrderRequired') <> 1)
  
                          end ! if (p_web.GSV('locOrderRequired') <> 1)
                      else ! if (epr:Quantity > sto:Quantity_Stock)
                          p_web.SSV('epr:date_Ordered',Today())
                          if rapidLocation(sto:Location)
                              epr:PartAllocated = 0
                              p_web.SSV('AddToStockAllocation:Type','EST')
                              p_web.SSV('AddToStockAllocation:Status','')
                              p_web.SSV('AddToStockAllocation:Qty',p_web.GSV('epr:Quantity'))
                          end ! if rapidLocation(sto:Location)
  
                          sto:quantity_Stock -= epr:Quantity
                          if (sto:quantity_Stock < 0)
                              sto:quantity_Stock = 0
                          end ! if rapidLocation(sto:Location)
                          if (access:STOCK.tryUpdate() = level:Benign)
                              rtn# = AddToStockHistory(sto:Ref_Number, |
                                  'DEC', |
                                  epr:Despatch_Note_Number, |
                                  p_web.GSV('job:Ref_Number'), |
                                  0, |
                                  sto:Quantity_Stock, |
                                  p_web.GSV('tmp:InWarrantyCost'), |
                                  p_web.GSV('tmp:OutWarrantyCost'), |
                                  epr:Retail_Cost, |
                                  'STOCK DECREMENTED', |
                                  '', |
                                  p_web.GSV('BookingUserCode'), |
                                  sto:Quantity_Stock)
                          end ! if (access:STOCK.tryUpdate() = level:Benign)
                      end ! if (epr:Quantity > sto:Quantity_Stock)
                  else ! if (sto:Sundry_Item <> 'YES')
                      p_web.SSV('epr:date_Ordered',Today())
                  end ! if (sto:Sundry_Item <> 'YES')
              ELSE
  
              end
          else ! if (stockPart# = 1)
  
          end ! if (stockPart# = 1)
      else ! if (epr:exclude_From_Order <> 'YES')
          p_web.SSV('epr:date_Ordered',Today())
      end ! if (epr:exclude_From_Order <> 'YES')
  else ! if (epr:part_Number <> 'ADJUSTMENT')
      p_web.SSV('epr:date_Ordered',Today())
  end !if (epr:part_Number <> 'ADJUSTMENT')
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord
  ! Change Record Validation

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormEstimateParts_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  ! Write Fields
      do deleteSessionValues
  
      !Write Fault Codes
      Access:MANFAUPA.Clearkey(map:ScreenOrderKey)
      map:Manufacturer    = p_web.GSV('job:Manufacturer')
      map:ScreenOrder    = 0
      set(map:ScreenOrderKey,map:ScreenOrderKey)
      loop
          if (Access:MANFAUPA.Next())
              Break
          end ! if (Access:MANFAUPA.Next())
          if (map:Manufacturer    <> p_web.GSV('job:Manufacturer'))
              Break
          end ! if (map:Manufacturer    <> p_web.GSV('job:Manufacturer'))
          if (map:ScreenOrder    = 0)
              cycle
          end ! if (map:ScreenOrder    <> 0)
  
          p_web.SSV('epr:Fault_Code' & map:Field_Number,p_web.GSV('tmp:FaultCodes' & map:ScreenOrder))
      end ! loop
  
  !    loop x# = 1 To 12
  !        p_web.SSV('epr:Fault_Code' & x#,p_web.GSV('tmp:FaultCodes' & x#))
  !        linePrint('epr:Fault_Code' & x# & ' - ' & p_web.GSV('tmp:FaultCodes' & x#),'c:\log.log')
  !    end ! loop x# = 1 To 12
  
      epr:Fault_Code1 = p_web.GSV('epr:Fault_Code1')
      epr:Fault_Code2 = p_web.GSV('epr:Fault_Code2')
      epr:Fault_Code3 = p_web.GSV('epr:Fault_Code3')
      epr:Fault_Code4 = p_web.GSV('epr:Fault_Code4')
      epr:Fault_Code5 = p_web.GSV('epr:Fault_Code5')
      epr:Fault_Code6 = p_web.GSV('epr:Fault_Code6')
      epr:Fault_Code7 = p_web.GSV('epr:Fault_Code7')
      epr:Fault_Code8 = p_web.GSV('epr:Fault_Code8')
      epr:Fault_Code9 = p_web.GSV('epr:Fault_Code9')
      epr:Fault_Code10 = p_web.GSV('epr:Fault_Code10')
      epr:Fault_Code11 = p_web.GSV('epr:Fault_Code11')
      epr:Fault_Code12 = p_web.GSV('epr:Fault_Code12')
  
      If p_web.GSV('tmp:ARCPart') = 1
          epr:AveragePurchaseCost = p_web.GSV('tmp:PurchaseCost')
          epr:Purchase_Cost       = p_web.GSV('tmp:InWarrantyCost')
          epr:Sale_Cost           = p_web.GSV('tmp:OutWarrantyCost')
          epr:InWarrantyMarkup    = p_web.GSV('tmp:InWarrantyMarkup')
          epr:OutWarrantyMarkup   = p_web.GSV('tmp:OutWarrantyMarkup')
      Else !If tmp:ARCPart
          epr:RRCAveragePurchaseCost  = p_web.GSV('tmp:PurchaseCost')
          epr:RRCPurchaseCost     = p_web.GSV('tmp:InWarrantyCost')
          epr:RRCSaleCost         = p_web.GSV('tmp:OutWarrantyCost')
          epr:RRCInWarrantyMarkup = p_web.GSV('tmp:InWarrantyMarkup')
          epr:RRCOutWarrantyMarkup   = p_web.GSV('tmp:OutWarrantyMarkup')
          epr:Purchase_Cost       = epr:RRCPurchaseCost
          epr:Sale_Cost           = epr:RRCSaleCost
      End !If tmp:ARCPart
  
  
  
  p_web.DeleteSessionValue('FormEstimateParts_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 4
  If loc:act = ChangeRecord
    loc:InvalidTab += 1
    do ValidateValue::epr:Order_Number
    If loc:Invalid then exit.
    do ValidateValue::epr:Date_Ordered
    If loc:Invalid then exit.
    do ValidateValue::epr:Date_Received
    If loc:Invalid then exit.
    do ValidateValue::locOutFaultCode
    If loc:Invalid then exit.
  End
  ! tab = 1
    loc:InvalidTab += 1
    do ValidateValue::epr:Part_Number
    If loc:Invalid then exit.
    do ValidateValue::epr:Description
    If loc:Invalid then exit.
    do ValidateValue::epr:Despatch_Note_Number
    If loc:Invalid then exit.
    do ValidateValue::epr:Quantity
    If loc:Invalid then exit.
    do ValidateValue::tmp:Location
    If loc:Invalid then exit.
    do ValidateValue::tmp:SecondLocation
    If loc:Invalid then exit.
    do ValidateValue::tmp:ShelfLocation
    If loc:Invalid then exit.
    do ValidateValue::tmp:PurchaseCost
    If loc:Invalid then exit.
    do ValidateValue::tmp:OutWarrantyCost
    If loc:Invalid then exit.
    do ValidateValue::tmp:OutWarrantyMarkup
    If loc:Invalid then exit.
        if (p_web.GSV('tmp:OutWarrantyMarkup') > GETINI('STOCK','MaxPartMarkup',,CLIP(PATH())&'\SB2KDEF.INI'))
            loc:Invalid = 'tmp:OutWarrantyMarkup'
            loc:Alert = 'You cannot mark-up parts more than ' & GETINI('STOCK','MaxPartMarkup',,CLIP(PATH())&'\SB2KDEF.INI') & '%.'
        end !if p_web.GSV('tmp:OutWarrantyMarkup') > GETINI('STOCK','MaxPartMarkup',,CLIP(PATH())&'\SB2KDEF.INI')
    do ValidateValue::tmp:FixedPrice
    If loc:Invalid then exit.
    do ValidateValue::epr:Supplier
    If loc:Invalid then exit.
    do ValidateValue::epr:Exclude_From_Order
    If loc:Invalid then exit.
    do ValidateValue::epr:PartAllocated
    If loc:Invalid then exit.
    do ValidateValue::tmp:UnallocatePart
    If loc:Invalid then exit.
  ! tab = 3
  If p_web.GSV('locOrderRequired') = 1
    loc:InvalidTab += 1
    do ValidateValue::text:OrderRequired
    If loc:Invalid then exit.
    do ValidateValue::text:OrderRequired2
    If loc:Invalid then exit.
    do ValidateValue::tmp:CreateOrder
    If loc:Invalid then exit.
  End
  ! tab = 2
    loc:InvalidTab += 1
    do ValidateValue::tmp:FaultCodesChecked
    If loc:Invalid then exit.
    do ValidateValue::tmp:FaultCodes1
    If loc:Invalid then exit.
    do ValidateValue::tmp:FaultCode2
    If loc:Invalid then exit.
    do ValidateValue::tmp:FaultCode3
    If loc:Invalid then exit.
    do ValidateValue::tmp:FaultCode4
    If loc:Invalid then exit.
    do ValidateValue::tmp:FaultCode5
    If loc:Invalid then exit.
    do ValidateValue::tmp:FaultCode6
    If loc:Invalid then exit.
    do ValidateValue::tmp:FaultCode7
    If loc:Invalid then exit.
    do ValidateValue::tmp:FaultCode8
    If loc:Invalid then exit.
    do ValidateValue::tmp:FaultCode9
    If loc:Invalid then exit.
    do ValidateValue::tmp:FaultCode10
    If loc:Invalid then exit.
    do ValidateValue::tmp:FaultCode11
    If loc:Invalid then exit.
    do ValidateValue::tmp:FaultCode12
    If loc:Invalid then exit.
  ! tab = 6
    loc:InvalidTab += 1
    do ValidateValue::epr:UsedOnRepair
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostInsert      Routine
  IF (p_web.GSV('AddToStockAllocation:Type') <> '')
      AddToStockAllocation(p_web.GSV('epr:Record_Number'),'EST',p_web.GSV('epr:Quantity'),p_web.GSV('AddToStockAllocation:Status'),p_web.GSV('job:Engineer'),p_web)
  END
PostCopy        Routine
  p_web.SetSessionValue('FormEstimateParts:Primed',0)
PostUpdate      Routine
  p_web.SetSessionValue('FormEstimateParts:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('tmp:Location')
  p_web.StoreValue('tmp:SecondLocation')
  p_web.StoreValue('tmp:ShelfLocation')
  p_web.StoreValue('tmp:PurchaseCost')
  p_web.StoreValue('tmp:OutWarrantyCost')
  p_web.StoreValue('tmp:OutWarrantyMarkup')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:UnallocatePart')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:CreateOrder')
  p_web.StoreValue('tmp:FaultCodesChecked')
  p_web.StoreValue('tmp:FaultCodes1')
  p_web.StoreValue('tmp:FaultCodes2')
  p_web.StoreValue('tmp:FaultCodes3')
  p_web.StoreValue('tmp:FaultCodes4')
  p_web.StoreValue('tmp:FaultCodes5')
  p_web.StoreValue('tmp:FaultCodes6')
  p_web.StoreValue('tmp:FaultCodes7')
  p_web.StoreValue('tmp:FaultCodes8')
  p_web.StoreValue('tmp:FaultCodes9')
  p_web.StoreValue('tmp:FaultCodes10')
  p_web.StoreValue('tmp:FaultCodes11')
  p_web.StoreValue('tmp:FaultCodes12')
  p_web.StoreValue('locPartUsedOnRepair')


PostDelete      Routine
local.AfterFaultCodeLookup        Procedure(Long fNumber)
code
    p_web.setsessionvalue('showtab_FormEstimateParts',Loc:TabNumber)
    if loc:LookupDone

!        Access:MANFAUPA.Clearkey(map:ScreenOrderKey)
!        map:ScreenOrder    = fNumber
!        map:Manufacturer   = p_web.GSV('job:Manufacturer')
!        if (Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign)
!            ! Found
!        else ! if (Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign)
!            ! Error
!        end ! if (Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign)
!
!        if (map:MainFault)
!            p_web.FileToSessionQueue(MANFAULO)
!            p_web.SSV('Comment:PartFaultCode' & fNumber,mfo:Description)
!
!        else ! if (map:MainFault)
!            p_web.FileToSessionQueue(MANFPALO)
!            p_web.SSV('Comment:PartFaultCode' & fNumber,mfp:Description)
!        end ! if (map:MainFault)
        do buildFaultCodes
        do UpdateComments
    end
    p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:FaultCodes' & fNumber)
local.SetLookupButton      Procedure(Long fNumber)
locUseRelatedPart           String(30)
Code
    if (p_web.GSV('ShowDate:PartFaultCode' & fNumber) = 1)
        packet = clip(packet) & '<button id="lookup_btn" class="LookupButton" onclick="displayCalendar(tmp__FaultCode' & fNumber & ',''dd/mm/yyyy'',this); ' & |
                  'Date.disabled=false;sv(''...'',''FormEstimateParts_pickdate_value'',1,FieldValue(this,1));nextFocus(FormEstimateParts_frm,'''',0);"' & |
                  'value="Select Date" name="Date" type="button">...</button>'
        do SendPacket
    end ! if (p_web.GSV('ShowDate:PartFaultCode1') = 1)
    if (p_web.GSV('Lookup:PartFaultCode' & fNumber) = 1)

        Access:MANFAUPA.Clearkey(map:ScreenOrderKey)
        map:ScreenOrder    = fNumber
        map:Manufacturer    = p_web.GSV('job:Manufacturer')
        if (Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign)
            ! Found
        else ! if (Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign)
            ! Error
        end ! if (Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign)
        if (map:UseRelatedJobCode <> 0)
            locUseRelatedPart = 'relatedPartCode=' & map:Field_Number
        else
            locUseRelatedPart = ''
        end ! if (map:UseRelatedJobCode <> 0)

        if (map:MainFault)

            Access:MANFAULT.Clearkey(maf:MainFaultKey)
            maf:Manufacturer    = p_web.GSV('job:Manufacturer')
            maf:MainFault    = 1
            if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)
                ! Found
            else ! if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)
                ! Error
            end ! if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)
            packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('BrowseJobFaultCodeLookup')&|
                        '?LookupField=tmp:FaultCodes' & fNumber & '&Tab=2&ForeignField=mfo:Field&_sort=mfo:Field&Refresh=' & |
                        'sort&LookupFrom=FormEstimateParts&' & |
                        'fieldNumber=' & maf:Field_Number & '&partType=C&partMainFault=1&' & clip(locUseRelatedPart)),) !lookupextra

        else ! if (map:MainFault)
            found# = 0
            Access:STOCK.Clearkey(sto:Ref_Number_Key)
            sto:Ref_Number    = p_web.GSV('epr:Part_Ref_Number')
            if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
                ! Found
                ! Check if any of the fault codes have been restricted by the Stock Part (DBH: 29/10/2007)
                If sto:Assign_Fault_Codes = 'YES'
                    Access:STOMODEL.Clearkey(stm:Model_Number_Key)
                    stm:Ref_Number = sto:Ref_Number
                    stm:Manufacturer = p_web.GSV('job:Manufacturer')
                    stm:Model_Number = p_web.GSV('job:Model_Number')
                    If Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign

                        Case map:Field_Number
                        Of 1
                            If stm:FaultCode1 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        Of 2
                            If stm:FaultCode2 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        Of 3
                            If stm:FaultCode3 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        Of 4
                            If stm:FaultCode4 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        Of 5
                            If stm:FaultCode5 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        Of 6
                            If stm:FaultCode6 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        Of 7
                            If stm:FaultCode7 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        Of 8
                            If stm:FaultCode8 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        Of 9
                            If stm:FaultCode9 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        Of 10
                            If stm:FaultCode10 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        Of 11
                            If stm:FaultCode11 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        Of 12
                            If stm:FaultCode12 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        End ! Case map:Field_Number
                        If Found# = 1
                            packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('BrowseStockPartFaultCodeLookup')&|
                                        '?LookupField=tmp:FaultCodes' & fNumber & '&Tab=2&ForeignField=stu:Field&_sort=stu:Field&Refresh=' & |
                                        'sort&LookupFrom=FormEstimateParts&' & |
                                        'fieldNumber=' & map:Field_Number & '&stockRefNumber=' & stm:RecordNumber),) !lookupextra

                        End ! If Found# = 1
                    End ! If Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign
                End ! If sto:Assign_Fault_Codes = 'YES'
            else ! if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
                ! Error
            end ! if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)

            if (found# = 0)
                packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('BrowsePartFaultCodeLookup')&|
                            '?LookupField=tmp:FaultCodes' & fNumber & '&Tab=2&ForeignField=mfp:Field&_sort=mfp:Field&Refresh=' & |
                            'sort&LookupFrom=FormEstimateParts&' & |
                            'fieldNumber=' & map:Field_Number & '&partType=C&'),) !lookupextra
            end ! if (found# = 0)
        end ! if (map:MainFault)
        do sendPacket
    end !if (p_web.GSV('Lookup:PartFaultCode1') = 1)
BrowseStockPartFaultCodeLookup PROCEDURE  (NetWebServerWorker p_web)
FaultQueue           QUEUE,PRE()                           !
ID                   STRING(20)                            !
RecordNumber         LONG                                  !
FaultType            STRING(1)                             !
                     END                                   !
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256),dim(Net:MaxKeyFields)
idx           String(Net:HashSize)
sub           Long
            End
Select:IsInvalid  Long
stu:Field:IsInvalid  Long
stu:Description:IsInvalid  Long
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:DefaultSelection    String(Net:HashSize)
loc:ActualSelection     String(Net:HashSize)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
!loc:pagename            String(256)
loc:NavButtonPosition    Long
loc:UpdateButtonPosition Long
loc:SelectionMethod      Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorCase         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
loc:lookupdone          Long
loc:FormPopup           Long
ThisView            View(STOMPFAU)
                      Project(stu:RecordNumber)
                      Project(stu:Field)
                      Project(stu:Description)
                      Project(stu:Manufacturer)
                      Project(stu:FieldNumber)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
Loc:popup             Long
loc:poppedup          Long,static,thread
FilesOpened     Long
MANFAUPA::State  USHORT
MODELCCT::State  USHORT
MANFAULO_ALIAS::State  USHORT
MANFPARL::State  USHORT
MANFAUPA_ALIAS::State  USHORT
MANFPALO_ALIAS::State  USHORT
MANFAURL::State  USHORT
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return 
  End
  GlobalErrors.SetProcedureName('BrowseStockPartFaultCodeLookup')
  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseStockPartFaultCodeLookup:NoForm')
      loc:NoForm = p_web.GetValue('BrowseStockPartFaultCodeLookup:NoForm')
      loc:FormName = p_web.GetValue('BrowseStockPartFaultCodeLookup:FormName')
    else
      loc:FormName = 'BrowseStockPartFaultCodeLookup_frm'
    End
    p_web.SSV('BrowseStockPartFaultCodeLookup:NoForm',loc:NoForm)
    p_web.SSV('BrowseStockPartFaultCodeLookup:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseStockPartFaultCodeLookup:NoForm')
    loc:FormName = p_web.GSV('BrowseStockPartFaultCodeLookup:FormName')
  end
  case p_web.site.formpopups
  of -1 ; loc:FormPopup = Net:Page
  of 0 ; loc:FormPopup = Net:Page
  of 1 ; loc:FormPopup = Net:Popup
  End
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseStockPartFaultCodeLookup') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseStockPartFaultCodeLookup')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if 1 = 2
  elsif p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  elsif p_web.IfExistsValue('_EIPRow_')
    do CallRow
  elsif p_web.GetValue('_CallPopups') > 0
    do CallPopups
  elsif p_web.IfExistsValue('_GetSecwinSettings')
      p_web.SetValue('Secwin_' & 'BrowseStockPartFaultCodeLookup' & 'AccessGroupsArray','1') 
      p_web.SetValue('Secwin_AccessWindowName','BrowseStockPartFaultCodeLookup')
      
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallPopups  Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
loc:CallPopups   Long
loc:name         String(255)
  code
  if loc:poppedup = 0
    loc:CallPopups = p_web.GetValue('_CallPopups')
    if loc:CallPopups = 1 !and p_web.GetPreCall('BrowseStockPartFaultCodeLookup') = 0! for = 1 include popup divs and scripts for this browse
      !p_web.AddPreCall('BrowseStockPartFaultCodeLookup')
      p_web.DivHeader('popup_BrowseStockPartFaultCodeLookup','nt-hidden')
      p_web.DivHeader('BrowseStockPartFaultCodeLookup',p_web.combine(p_web.site.style.browsediv,'fdiv'))
      p_web.DivFooter()
      p_web.DivFooter()
      loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(400)&', modal: true, position: [''center'',15]'
      packet =  p_web.jQuery('#' & lower('popup_BrowseStockPartFaultCodeLookup_div'),'dialog',loc:options) & |
      p_web.jQuery('#' & lower('popup_BrowseStockPartFaultCodeLookup_div'),'removeClass','''nt-hidden''')
    End
    ! for > 0 add in form, EIP-lookups and browse object
    do Popups
    loc:popup = 1
    do ClosingScripts
    do SendPacket
    loc:poppedup = 1
  end

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web.DivHeader('BrowseStockPartFaultCodeLookup',p_web.combine(p_web.site.style.browsediv,'fdiv'))
  if loc:ParentSilent = 0
    do GenerateBrowse
    if p_web.RequestAjax = 1
      p_web.Script('BrowseProducts.pcv();')
    end
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web.DivFooter()
  do Children
  do Popups
  if loc:poppedup = 0
    do ClosingScripts
  end
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(STOMPFAU,stu:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'STU:FIELD') then p_web.SetValue('BrowseStockPartFaultCodeLookup_sort','1')
    ElsIf (loc:vorder = 'STU:DESCRIPTION') then p_web.SetValue('BrowseStockPartFaultCodeLookup_sort','2')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseStockPartFaultCodeLookup:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseStockPartFaultCodeLookup:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseStockPartFaultCodeLookup:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseStockPartFaultCodeLookup:LookupField')
    loc:selecting = 0
  End

Popups Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  If loc:popup = 0
  End
SetFormAction  Routine
  loc:formaction = 'BrowseStockPartFaultCodeLookup'

GotFocusBack   Routine

GenerateBrowse Routine
  data
loc:viewoptions  Long
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  ! Set general Browse options
  loc:NavButtonPosition   = Net:Below
  loc:UpdateButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:PageLoad
  loc:Sorting          = Net:ServerSort
  if p_web.GetValue('_popup_') = 1
    loc:popup = 1
  end
  ! Set Locator Options
  loc:LocatorPosition  = Net:Above
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  p_web.ClearBrowse('BrowseStockPartFaultCodeLookup')
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  if (p_web.ifExistsValue('fieldNumber'))
      p_web.storeValue('fieldNumber')
  end !if (p_web.ifExistsValue('fieldNumber'))
  if (p_web.ifExistsValue('stockRefNumber'))
      p_web.storeValue('stockRefNumber')
  end ! if (p_web.ifExistsValue('partType'))
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 15
  End
  loc:ActualSelection = ''
  !loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseStockPartFaultCodeLookup_sort',net:DontEvaluate)
  p_web.SetSessionValue('BrowseStockPartFaultCodeLookup_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 3
    Loc:LocateField = ''
    Loc:LocatorCase = 0
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(stu:Field)','-UPPER(stu:Field)')
    Loc:LocateField = 'stu:Field'
    Loc:LocatorCase = 0
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(stu:Description)','-UPPER(stu:Description)')
    Loc:LocateField = 'stu:Description'
    Loc:LocatorCase = 0
  end
  if loc:vorder = ''
    loc:vorder = '+stu:RefNumber,+stu:FieldNumber,+UPPER(stu:Field)'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('stu:Field')
    loc:SortHeader = p_web.Translate('Field')
    p_web.SetSessionValue('BrowseStockPartFaultCodeLookup_LocatorPic','@s30')
  Of upper('stu:Description')
    loc:SortHeader = p_web.Translate('Description')
    p_web.SetSessionValue('BrowseStockPartFaultCodeLookup_LocatorPic','@s60')
  End
  If loc:selecting = 1
    p_web.GetSettings(p_web.GetSessionValue('Push1'))
    loc:selectaction = p_web.FormSettings.ParentPage !p_web.GetSessionValue('BrowseStockPartFaultCodeLookup:LookupFrom')
  End!Else
  do SendPacket
  do SetFormAction
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseStockPartFaultCodeLookup:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseStockPartFaultCodeLookup:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseStockPartFaultCodeLookup:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="STOMPFAU"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="stu:RecordNumberKey"></input><13,10>'
  end
  If p_web.Translate('Select Stock Part Fault Codes') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.browseSubHeading,)&'">'&p_web.Translate('Select Stock Part Fault Codes',0)&'</div>'&CRLF
  End
  case p_web.GetValue('refresh') ; of 'next' orof 'previous' orof 'first' orof 'last'
    p_web.Script('try{{BrowseStockPartFaultCodeLookup.restoreFocus();} catch(err){{};')
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseStockPartFaultCodeLookup',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      case loc:locatortype
      of Net:Position
      orof Net:Date
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
      of Net:Contains
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
      of Net:Begins
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
      End
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,'Locator')&'"><tr>' &|
      Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
      Case Loc:LocatorType
      of Net:Date
        packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator2BrowseStockPartFaultCodeLookup','',p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,, p_web.GSV('BrowseStockPartFaultCodeLookup_LocatorPic'),,,'onchange="BrowseStockPartFaultCodeLookup.locate(''Locator2BrowseStockPartFaultCodeLookup'',this.value);"',,0,,,) & '</div></td>'
      Else
        packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator2BrowseStockPartFaultCodeLookup',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,'size="30" onchange="BrowseStockPartFaultCodeLookup.locate(''Locator2BrowseStockPartFaultCodeLookup'',this.value);" ',,,,,,) & '</td>'
      End
      If loc:LocatorSearchButton or loc:LocatorClearButton
        packet = clip(packet) & '<td><div id="BrowseStockPartFaultCodeLookup_locate_a" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
        If loc:LocatorSearchButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
        End
        If loc:LocatorClearButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseStockPartFaultCodeLookup.cl(''BrowseStockPartFaultCodeLookup'');')
        End
        packet = clip(packet) & '</div><13,10></td><13,10>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
      If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'BrowseStockPartFaultCodeLookup_locate_a','buttonset',loc:options)
      End
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & p_web.DivHeader('BrowseStockPartFaultCodeLookup_table',p_web.Combine(p_web.site.style.BrowseTableDiv,'BrowseLookup'),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,,'sortable')&'" id="BrowseStockPartFaultCodeLookup_tbl">'
  Else
    packet = clip(packet) & p_web.DivHeader('BrowseStockPartFaultCodeLookup_table',p_web.Combine(p_web.site.style.BrowseTableDiv,'BrowseLookup'),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,)&'" id="BrowseStockPartFaultCodeLookup_tbl">'
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
      If loc:Selecting = 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'3','BrowseStockPartFaultCodeLookup',p_web.Translate('Pick'),,,,,,,0,loc:Sorting,'Button')
          do AddPacket
          loc:columns += 1
      End ! Selecting
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'1','BrowseStockPartFaultCodeLookup',p_web.Translate('Field'),'Click here to sort by Field',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'2','BrowseStockPartFaultCodeLookup',p_web.Translate('Description'),'Click here to sort by Description',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,15,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  If Instring('stu:recordnumber',lower(loc:vorder),1,1) = 0 !and STOMPFAU{prop:SQLDriver} = 1
    loc:vorder = Choose(loc:vorder='','stu:RecordNumber',clip(loc:vorder) & ',' & 'stu:RecordNumber')
  End
  Loc:Selected = Choose(p_web.IfExistsValue('stu:RecordNumber'),p_web.GetValue('stu:RecordNumber'),p_web.GetSessionValue('stu:RecordNumber'))
  ThisView{prop:order} = p_web.CleanFilter(ThisView,clip(loc:vorder))
      loc:FilterWas = 'stu:RefNumber = ' & p_web.GSV('stockRefNumber') & 'AND stu:FieldNumber = ' & p_web.GSV('fieldNumber')
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseStockPartFaultCodeLookup',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseStockPartFaultCodeLookup_Filter')
    p_web.SetSessionValue('BrowseStockPartFaultCodeLookup_FirstValue','')
    p_web.SetSessionValue('BrowseStockPartFaultCodeLookup_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,STOMPFAU,stu:RecordNumberKey,loc:PageRows,'BrowseStockPartFaultCodeLookup',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,loc:ViewOptions,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled,Loc:LocatorCase)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or Loc:LocatorType = Net:Date or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position or loc:LocatorType = Net:Date
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If STOMPFAU{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(STOMPFAU,loc:firstvalue)
              Reset(ThisView,STOMPFAU)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If STOMPFAU{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(STOMPFAU,loc:lastvalue)
            Reset(ThisView,STOMPFAU)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(stu:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td class="'&p_web.combine(p_web.site.style.BrowseEmpty,)&'">'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:NavButtonPosition=Net:Above or (loc:NavButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseStockPartFaultCodeLookup_nav_a" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseStockPartFaultCodeLookup.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseStockPartFaultCodeLookup.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseStockPartFaultCodeLookup.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseStockPartFaultCodeLookup.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseStockPartFaultCodeLookup_nav_a','buttonset',loc:options)
          End
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:UpdateButtonPosition=Net:Above or (loc:UpdateButtonPosition=Net:Both and loc:found > 0))
    If loc:selecting = 1
      If loc:found
        packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SelectButton,'BrowseStockPartFaultCodeLookup',,,loc:popup)
      End
      TableQueue.Kind = Net:BeforeTable
      do AddPacket
    End
    If loc:selecting = 1 !and loc:parent = ''
      if loc:popup
        packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:BrowseCancelButton,'BrowseStockPartFaultCodeLookup',,,loc:popup)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
      end
      TableQueue.Kind = Net:BeforeTable
      do AddPacket
    End
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseStockPartFaultCodeLookup',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseStockPartFaultCodeLookup_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseStockPartFaultCodeLookup_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          case loc:locatortype
          of Net:Position
          orof Net:Date
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
          of Net:Contains
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
          of Net:Begins
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
          End
          packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,'Locator')&'"><tr>' &|
          Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
          Case Loc:LocatorType
          of Net:Date
            packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator1BrowseStockPartFaultCodeLookup','',p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,, p_web.GSV('BrowseStockPartFaultCodeLookup_LocatorPic'),,,'onchange="BrowseStockPartFaultCodeLookup.locate(''Locator1BrowseStockPartFaultCodeLookup'',this.value);"',,0,,,) & '</div></td>'
          Else
            packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator1BrowseStockPartFaultCodeLookup',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,'size="30" onchange="BrowseStockPartFaultCodeLookup.locate(''Locator1BrowseStockPartFaultCodeLookup'',this.value);" ',,,,,,) & '</td>'
          End
          If loc:LocatorSearchButton or loc:LocatorClearButton
            packet = clip(packet) & '<td><div id="BrowseStockPartFaultCodeLookup_locate_b" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
            If loc:LocatorSearchButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
            End
            If loc:LocatorClearButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseStockPartFaultCodeLookup.cl(''BrowseStockPartFaultCodeLookup'');')
            End
            packet = clip(packet) & '</div><13,10></td><13,10>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
          If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseStockPartFaultCodeLookup_locate_b','buttonset',loc:options)
          End
      End
    End
  End
  p_web.SetSessionValue('BrowseStockPartFaultCodeLookup_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseStockPartFaultCodeLookup_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:NavButtonPosition=Net:Below or loc:NavButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseStockPartFaultCodeLookup_nav_b" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseStockPartFaultCodeLookup.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseStockPartFaultCodeLookup.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseStockPartFaultCodeLookup.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseStockPartFaultCodeLookup.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseStockPartFaultCodeLookup_nav_b','buttonset',loc:options)
          End
        end
        do SendPacket
  End
  If loc:UpdateButtonPosition=Net:Below or loc:UpdateButtonPosition=Net:Both
  If loc:selecting = 1
    If loc:found
      packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SelectButton,'BrowseStockPartFaultCodeLookup',,,loc:popup)
    End
    do SendPacket
  End
  If loc:selecting = 1 !and loc:parent = ''
    if loc:popup
      packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:BrowseCancelButton,'BrowseStockPartFaultCodeLookup',,,loc:popup)
    else
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
    end
    do SendPacket
  End
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
  data
loc:rowclick  string(1024)
  code
    loc:field = p_web.AddBrowseValue('BrowseStockPartFaultCodeLookup','STOMPFAU',stu:RecordNumberKey) !stu:RecordNumber
    p_web._thisrow = p_web._nocolon('stu:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if Loc:LocatorValue <> '' and loc:ActualSelection = ''
        loc:checked = 'checked'
        do SetSelection
      elsif loc:ActualSelection = '' and stu:RecordNumber = p_web.GetValue('stu:RecordNumber')
         loc:checked = 'checked'
         do SetSelection
      elsif loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseStockPartFaultCodeLookup:LookupField')) = stu:RecordNumber and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      else
        loc:checked = Choose((stu:RecordNumber = loc:selected) and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      end
      If(loc:SelectionMethod  = Net:Radio)
        loc:RowStyle = 'class="' & p_web.combine(p_web.site.style.browserow,) &'"'
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'data-nt-id="'& p_web.AddBrowseValue('BrowseStockPartFaultCodeLookup','STOMPFAU',stu:RecordNumberKey) &'" onclick="'
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If STOMPFAU{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(STOMPFAU)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            loc:DefaultSelection = loc:field
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If STOMPFAU{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(STOMPFAU)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            loc:DefaultSelection = loc:field
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'>'&p_web.CreateInput('radio','_bidv_',clip(loc:field),,loc:checked,'')&'</td>'&CRLF
          If loc:DefaultSelection = ''
            loc:DefaultSelection = loc:field
          End
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          If loc:DefaultSelection = '' or loc:direction < 0
            loc:DefaultSelection = loc:field
          End
        End
    end ! loc:eip = 0
        If Loc:Selecting = 1
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::Select
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        End     !Selecting
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::stu:Field
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::stu:Description
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr ' & clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Idx = p_web.AddBrowseValue('BrowseStockPartFaultCodeLookup','STOMPFAU',stu:RecordNumberKey)
  TableQueue.Id[1] = stu:RecordNumber

ClosingScripts  Routine
  data
Rtn_SecwinProcedureName         string(252)
  code
  If p_web.RequestAjax = 0
    do SetFormAction
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var btiBrowseStockPartFaultCodeLookup;if (btiBrowseStockPartFaultCodeLookup != 1){{var BrowseStockPartFaultCodeLookup=new browseTable(''BrowseStockPartFaultCodeLookup'','''&clip(loc:formname)&|
      ''','''&p_web._jsok('stu:RecordNumber',Net:Parameter)&''',2,'''&clip(loc:divname)&''',1,1,1,'''&|
      clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&|
      p_web.Translate('Are you sure you want to delete this record?')&''','''&clip(loc:ActualSelection)&''','''&clip(loc:selectaction)&''','''&|
      clip(loc:formactiontarget)&''','''','&loc:popup&',0,'''','''',"' & clip(Rtn_SecwinProcedureName) & '");<13,10>'&|
      'BrowseStockPartFaultCodeLookup.setGreenBar('''&p_web.ColorWeb(p_web.Site.Style.BrowseHighlightColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOneColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseTwoColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOverColor)&''');<13,10>' &|
      'BrowseStockPartFaultCodeLookup.applyGreenBar();btiBrowseStockPartFaultCodeLookup=1};<13,10>' &|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseStockPartFaultCodeLookup')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseStockPartFaultCodeLookup')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseStockPartFaultCodeLookup')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseStockPartFaultCodeLookup')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(STOMPFAU)
  p_web._CloseFile(MANFAUPA)
  p_web._CloseFile(MODELCCT)
  p_web._CloseFile(MANFAULO_ALIAS)
  p_web._CloseFile(MANFPARL)
  p_web._CloseFile(MANFAUPA_ALIAS)
  p_web._CloseFile(MANFPALO_ALIAS)
  p_web._CloseFile(MANFAURL)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(STOMPFAU)
  Bind(stu:Record)
  Clear(stu:Record)
  NetWebSetSessionPics(p_web,STOMPFAU)
  p_web._OpenFile(MANFAUPA)
  Bind(map:Record)
  NetWebSetSessionPics(p_web,MANFAUPA)
  p_web._OpenFile(MODELCCT)
  Bind(mcc:Record)
  NetWebSetSessionPics(p_web,MODELCCT)
  p_web._OpenFile(MANFAULO_ALIAS)
  Bind(mfo_ali:Record)
  NetWebSetSessionPics(p_web,MANFAULO_ALIAS)
  p_web._OpenFile(MANFPARL)
  Bind(mpr:Record)
  NetWebSetSessionPics(p_web,MANFPARL)
  p_web._OpenFile(MANFAUPA_ALIAS)
  Bind(map_ali:Record)
  NetWebSetSessionPics(p_web,MANFAUPA_ALIAS)
  p_web._OpenFile(MANFPALO_ALIAS)
  Bind(mfp_ali:Record)
  NetWebSetSessionPics(p_web,MANFPALO_ALIAS)
  p_web._OpenFile(MANFAURL)
  Bind(mnr:Record)
  NetWebSetSessionPics(p_web,MANFAURL)

Children Routine
  if loc:selecting = 0
    If p_web.RequestAjax = 0
      do StartChildren
    Else
      do AjaxChildren
    End
  end

AjaxChildren  Routine
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue('stu:RecordNumber',p_web.GetValue('stu:RecordNumber'))
   p_web.DeleteValue('_Clicked')
   loc:found = 1
   do Children
! ----------------------------------------------------------------------------------------
CallRow  Routine
  data
loc:result  long
  code
  do OpenFilesB
  stu:RecordNumber = p_web.GSV('stu:RecordNumber')
  loc:result = p_web._GetFile(STOMPFAU,stu:RecordNumberKey)
  loc:eip = 1
  loc:viewstate = p_web.escape(p_web.Base64Encode(clip(stu:RecordNumber)))
  do BrowseRow
  do SendPacket
  do ClosefilesB

! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(STOMPFAU)
  Case upper(p_web.GetValue('_EIPClm'))
  Else
    case p_web.GetValue('_action_')
    of Net:InsertRecord
    orof Net:ChangeRecord
    orof Net:DeleteRecord
    orof Net:CopyRecord
      loc:eip = 0
      do CallBrowse
    end
  End
  do GotFocusBack
  p_web._CloseFile(STOMPFAU)
! ----------------------------------------------------------------------------------------
value::Select   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    p_web.site.SmallSelectButton.TextValue = p_web.Translate('>>')
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseStockPartFaultCodeLookup_Select_'&stu:RecordNumber,,net:crc,,loc:extra)
        packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'BrowseStockPartFaultCodeLookup',p_web.AddBrowseValue('BrowseStockPartFaultCodeLookup','STOMPFAU',stu:RecordNumberKey),,loc:popup)
    End
    p_web.site.SmallSelectButton = p_web.RequestData.WebServer._SitesQueue.Defaults.SmallSelectButton  !bruce
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::stu:Field   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseStockPartFaultCodeLookup_stu:Field_'&stu:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(stu:Field,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::stu:Description   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseStockPartFaultCodeLookup_stu:Description_'&stu:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(stu:Description,'@s60')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  p_web._OpenFile(MANFAUPA)
  p_web._OpenFile(MODELCCT)
  p_web._OpenFile(MANFAULO_ALIAS)
  p_web._OpenFile(MANFPARL)
  p_web._OpenFile(MANFAUPA_ALIAS)
  p_web._OpenFile(MANFPALO_ALIAS)
  p_web._OpenFile(MANFAURL)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(MANFAUPA)
  p_Web._CloseFile(MODELCCT)
  p_Web._CloseFile(MANFAULO_ALIAS)
  p_Web._CloseFile(MANFPARL)
  p_Web._CloseFile(MANFAUPA_ALIAS)
  p_Web._CloseFile(MANFPALO_ALIAS)
  p_Web._CloseFile(MANFAURL)
     FilesOpened = False
  END
  p_web.deleteSessionValue('fieldNumber')
  p_web.deleteSessionValue('stockRefNumber')
  return
!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
CheckForDuplicate  Routine
SetSelection  Routine
  loc:ActualSelection = loc:field
  p_web.SetSessionValue('stu:RecordNumber',stu:RecordNumber)

MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:ActualSelection = ''
    loc:ActualSelection = loc:DefaultSelection
    p_web.GetBrowseValue(loc:ActualSelection,Net:Web:SessionQueue) ! so children are primed with correct sessionValue
  End

  If loc:ActualSelection <> ''
    TableQueue.Kind = Net:RowTable
    get(TableQueue,TableQueue.Kind)
    if Errorcode() = 0
      TableQueue.Row = sub(TableQueue.Row,1,len(clip(TableQueue.Row))-1) &  ' data-nt-rows="'&loc:RowsHigh&'" data-nt-value="'&clip(loc:ActualSelection)&'"' & '>'
      Put(TableQueue)
    End
  End

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead class="'&p_web.combine(p_web.site.style.BrowseHeader,'')&'"><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot class="'&p_web.combine(p_web.site.style.BrowseFooter,'BrowseFooter')&'"><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody class="'&p_web.combine(p_web.site.style.BrowseBody,)&'"><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table><13,10>' & p_web.DivFooter(Net:NoSend) ! Table Div
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
BrowsePartFaultCodeLookup PROCEDURE  (NetWebServerWorker p_web)
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256),dim(Net:MaxKeyFields)
idx           String(Net:HashSize)
sub           Long
            End
Select:IsInvalid  Long
mfp:Field:IsInvalid  Long
mfp:Description:IsInvalid  Long
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:DefaultSelection    String(Net:HashSize)
loc:ActualSelection     String(Net:HashSize)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
!loc:pagename            String(256)
loc:NavButtonPosition    Long
loc:UpdateButtonPosition Long
loc:SelectionMethod      Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorCase         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
loc:lookupdone          Long
loc:FormPopup           Long
ThisView            View(MANFPALO)
                      Project(mfp:RecordNumber)
                      Project(mfp:Field)
                      Project(mfp:Description)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
Loc:popup             Long
loc:poppedup          Long,static,thread
FilesOpened     Long
MANFAUPA::State  USHORT
MODELCCT::State  USHORT
MANFAULO_ALIAS::State  USHORT
MANFPARL::State  USHORT
MANFAUPA_ALIAS::State  USHORT
MANFPALO_ALIAS::State  USHORT
MANFAURL::State  USHORT
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return 
  End
  GlobalErrors.SetProcedureName('BrowsePartFaultCodeLookup')
  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowsePartFaultCodeLookup:NoForm')
      loc:NoForm = p_web.GetValue('BrowsePartFaultCodeLookup:NoForm')
      loc:FormName = p_web.GetValue('BrowsePartFaultCodeLookup:FormName')
    else
      loc:FormName = 'BrowsePartFaultCodeLookup_frm'
    End
    p_web.SSV('BrowsePartFaultCodeLookup:NoForm',loc:NoForm)
    p_web.SSV('BrowsePartFaultCodeLookup:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowsePartFaultCodeLookup:NoForm')
    loc:FormName = p_web.GSV('BrowsePartFaultCodeLookup:FormName')
  end
  case p_web.site.formpopups
  of -1 ; loc:FormPopup = Net:Page
  of 0 ; loc:FormPopup = Net:Page
  of 1 ; loc:FormPopup = Net:Popup
  End
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowsePartFaultCodeLookup') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowsePartFaultCodeLookup')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if 1 = 2
  elsif p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  elsif p_web.IfExistsValue('_EIPRow_')
    do CallRow
  elsif p_web.GetValue('_CallPopups') > 0
    do CallPopups
  elsif p_web.IfExistsValue('_GetSecwinSettings')
      p_web.SetValue('Secwin_' & 'BrowsePartFaultCodeLookup' & 'AccessGroupsArray','1') 
      p_web.SetValue('Secwin_AccessWindowName','BrowsePartFaultCodeLookup')
      
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallPopups  Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
loc:CallPopups   Long
loc:name         String(255)
  code
  if loc:poppedup = 0
    loc:CallPopups = p_web.GetValue('_CallPopups')
    if loc:CallPopups = 1 !and p_web.GetPreCall('BrowsePartFaultCodeLookup') = 0! for = 1 include popup divs and scripts for this browse
      !p_web.AddPreCall('BrowsePartFaultCodeLookup')
      p_web.DivHeader('popup_BrowsePartFaultCodeLookup','nt-hidden')
      p_web.DivHeader('BrowsePartFaultCodeLookup',p_web.combine(p_web.site.style.browsediv,'fdiv'))
      p_web.DivFooter()
      p_web.DivFooter()
      loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(400)&', modal: true, position: [''center'',15]'
      packet =  p_web.jQuery('#' & lower('popup_BrowsePartFaultCodeLookup_div'),'dialog',loc:options) & |
      p_web.jQuery('#' & lower('popup_BrowsePartFaultCodeLookup_div'),'removeClass','''nt-hidden''')
    End
    ! for > 0 add in form, EIP-lookups and browse object
    do Popups
    loc:popup = 1
    do ClosingScripts
    do SendPacket
    loc:poppedup = 1
  end

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web.DivHeader('BrowsePartFaultCodeLookup',p_web.combine(p_web.site.style.browsediv,'fdiv'))
  if loc:ParentSilent = 0
    do GenerateBrowse
    if p_web.RequestAjax = 1
      p_web.Script('BrowseProducts.pcv();')
    end
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web.DivFooter()
  do Children
  do Popups
  if loc:poppedup = 0
    do ClosingScripts
  end
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(MANFPALO,mfp:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'MFP:FIELD') then p_web.SetValue('BrowsePartFaultCodeLookup_sort','1')
    ElsIf (loc:vorder = 'MFP:DESCRIPTION') then p_web.SetValue('BrowsePartFaultCodeLookup_sort','2')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowsePartFaultCodeLookup:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowsePartFaultCodeLookup:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowsePartFaultCodeLookup:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowsePartFaultCodeLookup:LookupField')
    loc:selecting = 0
  End

Popups Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  If loc:popup = 0
  End
SetFormAction  Routine
  loc:formaction = 'BrowsePartFaultCodeLookup'

GotFocusBack   Routine

GenerateBrowse Routine
  data
loc:viewoptions  Long
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  ! Set general Browse options
  loc:NavButtonPosition   = Net:Below
  loc:UpdateButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:PageLoad
  loc:Sorting          = Net:ServerSort
  if p_web.GetValue('_popup_') = 1
    loc:popup = 1
  end
  ! Set Locator Options
  loc:LocatorPosition  = Net:Above
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  p_web.ClearBrowse('BrowsePartFaultCodeLookup')
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
    if (p_web.ifExistsValue('fieldNumber'))
        p_web.storeValue('fieldNumber')
    end !if (p_web.ifExistsValue('fieldNumber'))
    if (p_web.ifExistsValue('partType'))
        p_web.storeValue('partType')
    end ! if (p_web.ifExistsValue('partType'))
  
  ! Inserting (DBH 30/04/2008) # 9723 - Limit by Model Number if it's a CCT Reference
  p_web.SSV('CCT',0)
  Access:MANFAUPA.Clearkey(map:Field_Number_Key)
  map:Manufacturer = p_web.GSV('job:Manufacturer')
  map:Field_Number = p_web.GSV('fieldNumber')
  If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
      ! Found
      ! Is this fault a CCT? (DBH: 30/04/2008)
      If map:CCTReferenceFaultCode
          p_web.SSV('CCT',1)
      End ! If map:CCTReferenceFaultCode
  End ! If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
  ! End (DBH 30/04/2008) #9723
  
  ! Build Associated Fault Codes Queue
  
  !Clear queue for this user
  clear(tmpfau:Record)
  tmpfau:sessionID = p_web.SessionID
  set(tmpfau:keySessionID,tmpfau:keySessionID)
  loop
      next(tempFaultCodes)
      if (error())
          break
      end ! if (error())
      if (tmpfau:sessionID <> p_web.sessionID)
          break
      end ! if (tmpfau:sessionID <> p_web.sessionID)
      delete(tempFaultCodes)
  end ! loop
  
  loop x# = 1 to 12
      if (x# = p_web.GSV('fieldNumber'))
          cycle
      end ! if (x# = fieldNumber)
  
      if (p_web.GSV('Hide:PartFaultCode' & x#))
          cycle
      end ! if (p_web.GSV('Hide:PartFaultCode' & x#))
  
      if (p_web.GSV('tmp:FaultCodes' & x#) = '')
          cycle
      end ! if (p_web.GSV('tmp:FaultCodes' & x#) = '')
  
      Access:MANFAUPA_ALIAS.Clearkey(map_ali:ScreenOrderKey)
      map_ali:Manufacturer    = p_web.GSV('job:Manufacturer')
      map_ali:Field_Number    = x#
      if (Access:MANFAUPA_ALIAS.TryFetch(map_ali:ScreenOrderKey) = Level:Benign)
          ! Found
          Access:MANFPALO_ALIAS.Clearkey(mfp_ali:Field_Key)
          mfp_ali:Manufacturer    = p_web.GSV('job:Manufacturer')
          mfp_ali:Field_Number    = map_ali:Field_Number
          mfp_ali:Field    = p_web.GSV('tmp:FaultCodes' & x#)
          if (Access:MANFPALO_ALIAS.TryFetch(mfp_ali:Field_Key) = Level:Benign)
              ! Found
              found# = 0
  
              Access:MANFPARL.Clearkey(mpr:FieldKey)
              mpr:MANFPALORecordNumber    = mfp_ali:RecordNumber
              mpr:FieldNumber    = p_web.GSV('fieldNumber')
              set(mpr:FieldKey,mpr:FieldKey)
              loop
                  if (Access:MANFPARL.Next())
                      Break
                  end ! if (Access:MANFPARL.Next())
                  if (mpr:MANFPALORecordNumber    <> mfp_ali:RecordNumber)
                      Break
                  end ! if (mfp:MANFPALORecordNumber    <> mfp_ali:RecordNumber)
                  if (mpr:FieldNumber    <> p_web.GSV('fieldNumber'))
                      Break
                  end ! if (mfp:FieldNumber    <> p_web.GSV('fieldNumber'))
                  found# = 1
                  break
              end ! loop
  
              if (found# = 1)
                  tmpfau:sessionID = p_web.sessionID
                  tmpfau:recordNumber = mfp_ali:recordNumber
                  tmpfau:faultType = 'P'
                  add(tempFaultCodes)
              end ! if (found# = 1)
  
          else ! if (Access:MANFPALO_ALIAS.TryFetch(mfp_ali:Field_Key) = Level:Benign)
              ! Error
          end ! if (Access:MANFPALO_ALIAS.TryFetch(mfp_ali:Field_Key) = Level:Benign)
      else ! if (Access:MANFAUPA_ALIAS.TryFetch(map_ali:Field_Number_Key) = Level:Benign)
          ! Error
      end ! if (Access:MANFAUPA_ALIAS.TryFetch(map_ali:Field_Number_Key) = Level:Benign)
  
  
      Access:MANFAULT.Clearkey(maf:Field_Number_Key)
      maf:Manufacturer    = p_web.GSV('job:Manufacturer')
      maf:Field_Number    = x#
      if (Access:MANFAULT.TryFetch(maf:Field_Number_Key) = Level:Benign)
          ! Found
          if (maf:Lookup = 'YES')
  
              Access:MANFAULO.Clearkey(mfo:Field_Key)
              mfo:Manufacturer    = p_web.GSV('job:Manufacturer')
              mfo:Field_Number    = maf:Field_Number
              if (x# < 13)
                  mfo:Field = p_web.GSV('job:Fault_Code' & x#)
              else ! if (x# < 13)
                  mfo:Field = p_web.GSV('wob:FaultCode' & x#)
              end !if (x# < 13)
              if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                  ! Found
                  found# = 0
                  Access:MANFAURL.Clearkey(mnr:FieldKey)
                  mnr:MANFAULORecordNumber    = mfo:RecordNumber
                  mnr:PartFaultCode    = 1
                  mnr:FieldNumber    = p_web.GSV('fieldNumber')
                  set(mnr:FieldKey,mnr:FieldKey)
                  loop
                      if (Access:MANFAURL.Next())
                          Break
                      end ! if (Access:MANFAURL.Next())
                      if (mnr:MANFAULORecordNumber    <> mfo:RecordNumber)
                          Break
                      end ! if (mnr:MANFALORecordNumber    <> mfo:RecordNumber)
                      if (mnr:PartFaultCode    <> 1)
                          Break
                      end ! if (mnr:PartFaultCode    <> 1)
                      if (mnr:FieldNumber    <> p_web.GSV('fieldNumber'))
                          Break
                      end ! if (mnr:FieldNumber    <> fieldNumber)
                      found# = 1
                      break
                  end ! loop
                  if (found# = 1)
                      tmpfau:sessionID = p_web.sessionID
                      tmpfau:recordNumber = mfo:recordNumber
                      tmpfau:faultType = 'J'
                      add(tempFaultCodes)
                  end ! if (found# = 1)
              else ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                  ! Error
              end ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
          end ! if (maf:Lookup = 'YES')
      else ! if (Access:MANFAULT.TryFetch(maf:Field_Number_Key) = Level:Benign)
          ! Error
      end ! if (Access:MANFAULT.TryFetch(maf:Field_Number_Key) = Level:Benign)
  
  end ! loop x# = 1 to 12
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 15
  End
  loc:ActualSelection = ''
  !loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowsePartFaultCodeLookup_sort',net:DontEvaluate)
  p_web.SetSessionValue('BrowsePartFaultCodeLookup_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 3
    Loc:LocateField = ''
    Loc:LocatorCase = 0
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(mfp:Field)','-UPPER(mfp:Field)')
    Loc:LocateField = 'mfp:Field'
    Loc:LocatorCase = 0
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(mfp:Description)','-UPPER(mfp:Description)')
    Loc:LocateField = 'mfp:Description'
    Loc:LocatorCase = 0
  end
  if loc:vorder = ''
    loc:vorder = '+UPPER(mfp:Manufacturer),+mfp:Field_Number,+UPPER(mfp:Field)'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('mfp:Field')
    loc:SortHeader = p_web.Translate('Field')
    p_web.SetSessionValue('BrowsePartFaultCodeLookup_LocatorPic','@s30')
  Of upper('mfp:Description')
    loc:SortHeader = p_web.Translate('Description')
    p_web.SetSessionValue('BrowsePartFaultCodeLookup_LocatorPic','@s60')
  End
  If loc:selecting = 1
    p_web.GetSettings(p_web.GetSessionValue('Push1'))
    loc:selectaction = p_web.FormSettings.ParentPage !p_web.GetSessionValue('BrowsePartFaultCodeLookup:LookupFrom')
  End!Else
  do SendPacket
  do SetFormAction
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowsePartFaultCodeLookup:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowsePartFaultCodeLookup:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowsePartFaultCodeLookup:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="MANFPALO"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="mfp:RecordNumberKey"></input><13,10>'
  end
  If p_web.Translate('Select Part Fault Codes') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.browseSubHeading,)&'">'&p_web.Translate('Select Part Fault Codes',0)&'</div>'&CRLF
  End
  case p_web.GetValue('refresh') ; of 'next' orof 'previous' orof 'first' orof 'last'
    p_web.Script('try{{BrowsePartFaultCodeLookup.restoreFocus();} catch(err){{};')
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowsePartFaultCodeLookup',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      case loc:locatortype
      of Net:Position
      orof Net:Date
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
      of Net:Contains
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
      of Net:Begins
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
      End
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,'Locator')&'"><tr>' &|
      Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
      Case Loc:LocatorType
      of Net:Date
        packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator2BrowsePartFaultCodeLookup','',p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,, p_web.GSV('BrowsePartFaultCodeLookup_LocatorPic'),,,'onchange="BrowsePartFaultCodeLookup.locate(''Locator2BrowsePartFaultCodeLookup'',this.value);"',,0,,,) & '</div></td>'
      Else
        packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator2BrowsePartFaultCodeLookup',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,'size="30" onchange="BrowsePartFaultCodeLookup.locate(''Locator2BrowsePartFaultCodeLookup'',this.value);" ',,,,,,) & '</td>'
      End
      If loc:LocatorSearchButton or loc:LocatorClearButton
        packet = clip(packet) & '<td><div id="BrowsePartFaultCodeLookup_locate_a" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
        If loc:LocatorSearchButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
        End
        If loc:LocatorClearButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowsePartFaultCodeLookup.cl(''BrowsePartFaultCodeLookup'');')
        End
        packet = clip(packet) & '</div><13,10></td><13,10>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
      If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'BrowsePartFaultCodeLookup_locate_a','buttonset',loc:options)
      End
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & p_web.DivHeader('BrowsePartFaultCodeLookup_table',p_web.Combine(p_web.site.style.BrowseTableDiv,'BrowseLookup'),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,,'sortable')&'" id="BrowsePartFaultCodeLookup_tbl">'
  Else
    packet = clip(packet) & p_web.DivHeader('BrowsePartFaultCodeLookup_table',p_web.Combine(p_web.site.style.BrowseTableDiv,'BrowseLookup'),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,)&'" id="BrowsePartFaultCodeLookup_tbl">'
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
      If loc:Selecting = 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'3','BrowsePartFaultCodeLookup',p_web.Translate('Pick'),,,,,,,0,loc:Sorting,'Button')
          do AddPacket
          loc:columns += 1
      End ! Selecting
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'1','BrowsePartFaultCodeLookup',p_web.Translate('Field'),'Click here to sort by Field',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'2','BrowsePartFaultCodeLookup',p_web.Translate('Description'),'Click here to sort by Description',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,15,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  If Instring('mfp:recordnumber',lower(loc:vorder),1,1) = 0 !and MANFPALO{prop:SQLDriver} = 1
    loc:vorder = Choose(loc:vorder='','mfp:RecordNumber',clip(loc:vorder) & ',' & 'mfp:RecordNumber')
  End
  Loc:Selected = Choose(p_web.IfExistsValue('mfp:RecordNumber'),p_web.GetValue('mfp:RecordNumber'),p_web.GetSessionValue('mfp:RecordNumber'))
  ThisView{prop:order} = p_web.CleanFilter(ThisView,clip(loc:vorder))
      loc:FilterWas = 'Upper(mfp:Manufacturer) = Upper(''' & p_web.GSV('job:Manufacturer') & ''') AND Upper(mfp:Field_Number) = ' & p_web.GSV('fieldNumber')
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowsePartFaultCodeLookup',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowsePartFaultCodeLookup_Filter')
    p_web.SetSessionValue('BrowsePartFaultCodeLookup_FirstValue','')
    p_web.SetSessionValue('BrowsePartFaultCodeLookup_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,MANFPALO,mfp:RecordNumberKey,loc:PageRows,'BrowsePartFaultCodeLookup',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,loc:ViewOptions,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled,Loc:LocatorCase)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or Loc:LocatorType = Net:Date or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position or loc:LocatorType = Net:Date
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If MANFPALO{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(MANFPALO,loc:firstvalue)
              Reset(ThisView,MANFPALO)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If MANFPALO{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(MANFPALO,loc:lastvalue)
            Reset(ThisView,MANFPALO)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      if (p_web.GSV('partType') = 'C')
          if (mfp:RestrictLookup = 1)
              if (p_web.GSV('par:Adjustment') = 'YES')
                  if (mfp:RestrictLookupType <> 3)
                      Cycle
                  end ! if (mfp:RestrictLookupType <> 3)
              else ! if (p_web.GSV('par:Adjustment') = 'YES')
                  if (p_web.GSV('par:Correction') = 1)
                      if (mfp:RestrictLookupType <> 2)
                          Cycle
                      end ! if (mfp:RestrictLookupType <> 2)
                  else ! if (p_web.GSV('par:Correction') = 1)
                      if (mfp:RestrictLookupType <> 1)
                          Cycle
                      end ! if (mfp:RestrictLookupType <> 1)
                  end ! if (p_web.GSV('par:Correction') = 1)
              end ! if (p_web.GSV('par:Adjustment') = 'YES')
          end ! if (mfp:RestrictLookup = 1)
      end ! if (p_web.GSV('partType') = 'C')
      if (p_web.GSV('partType') = 'W')
          if (mfp:RestrictLookup = 1)
              if (p_web.GSV('wpr:Adjustment') = 'YES')
                  if (mfp:RestrictLookupType <> 3)
                      Cycle
                  end ! if (mfp:RestrictLookupType <> 3)
              else ! if (p_web.GSV('par:Adjustment') = 'YES')
                  if (p_web.GSV('wpr:Correction') = 1)
                      if (mfp:RestrictLookupType <> 2)
                          Cycle
                      end ! if (mfp:RestrictLookupType <> 2)
                  else ! if (p_web.GSV('par:Correction') = 1)
                      if (mfp:RestrictLookupType <> 1)
                          Cycle
                      end ! if (mfp:RestrictLookupType <> 1)
                  end ! if (p_web.GSV('par:Correction') = 1)
              end ! if (p_web.GSV('par:Adjustment') = 'YES')
          end ! if (mfp:RestrictLookup = 1)
      end ! if (p_web.GSV('partType') = 'C')
      
      if (mfp:JobTypeAvailability = 1)
          if (p_web.GSV('partType') <> 'C')
              Cycle
          end ! if (p_web.GSV('job:Warranty_Job') = 'YES')
      end ! if (mfp:JobTypeAvailability = 1)
      
      if (mfp:JobTypeAvailability = 2)
          if (p_web.GSV('partType') <> 'W')
              Cycle
          end ! if (p_web.GSV('job:Chargeable_Job') = 'YES')
      end ! if (mfp:JobTypeAvailability = 2)
      
      if (p_web.GSV('CCT') = 1)
          Access:MODELCCT.Clearkey(mcc:CCTRefKey)
          mcc:ModelNumber    = p_web.GSV('job:Model_Number')
          mcc:CCTReferenceNumber    = mfp:Field
          if (Access:MODELCCT.TryFetch(mcc:CCTRefKey) = Level:Benign)
              ! Found
          else ! if (Access:MODELCCT.TryFetch(mcc:CCTRefKey) = Level:Benign)
              ! Error
              Cycle
          end ! if (Access:MODELCCT.TryFetch(mcc:CCTRefKey) = Level:Benign)
      end !if (p_web.GSV('CCT') = 1)
      
      found# = 1
      clear(tmpfau:record)
      tmpfau:sessionID = p_web.sessionID
      set(tmpfau:keySessionID,tmpfau:keySessionID)
      loop
          next(tempFaultCodes)
          if (error())
              break
          end ! if (error())
          if (tmpfau:sessionID <> p_web.sessionID)
              break
          end ! if (tmpfau:sessionID <> p_web.sessionID)
      
          found# = 0
          case tmpfau:FaultType
          of 'P' ! Part Fault Code
              Access:MANFPARL.Clearkey(mpr:LinkedRecordNumberKey)
              mpr:MANFPALORecordNumber    = tmpfau:RecordNumber
              mpr:LinkedRecordNumber    = mfp:RecordNumber
              mpr:JobFaultCode    = 0
              if (Access:MANFPARL.TryFetch(mpr:LinkedRecordNumberKey) = Level:Benign)
                  ! Found
                  found# = 1
                  break
              else ! if (Access:MANFPARL.TryFetch(mpr:LinkedRecordNumberKey) = Level:Benign)
                  ! Error
              end ! if (Access:MANFPARL.TryFetch(mpr:LinkedRecordNumberKey) = Level:Benign)
      
          of 'J' ! Job Fault Code
      
              Access:MANFAURL.Clearkey(mnr:LinkedRecordNumberKey)
              mnr:MANFAULORecordNumber    = tmpfau:RecordNumber
              mnr:LinkedRecordNumber    = mfp:RecordNumber
              mnr:PartFaultCode    = 1
              if (Access:MANFAURL.TryFetch(mnr:LinkedRecordNumberKey) = Level:Benign)
                  ! Found
                  found# = 1
                  break
              else ! if (Access:MANFAURL.TryFetch(mnr:LinkedRecordNumberKey) = Level:Benign)
                  ! Error
              end ! if (Access:MANFAURL.TryFetch(mnr:LinkedRecordNumberKey) = Level:Benign)
          end ! case faultQueue.FaultType
      end ! loop
      
      if (found# = 0)
          cycle
      end ! if (found# = 0)
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(mfp:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td class="'&p_web.combine(p_web.site.style.BrowseEmpty,)&'">'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:NavButtonPosition=Net:Above or (loc:NavButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowsePartFaultCodeLookup_nav_a" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowsePartFaultCodeLookup.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowsePartFaultCodeLookup.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowsePartFaultCodeLookup.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowsePartFaultCodeLookup.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowsePartFaultCodeLookup_nav_a','buttonset',loc:options)
          End
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:UpdateButtonPosition=Net:Above or (loc:UpdateButtonPosition=Net:Both and loc:found > 0))
    If loc:selecting = 1
      If loc:found
        packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SelectButton,'BrowsePartFaultCodeLookup',,,loc:popup)
      End
      TableQueue.Kind = Net:BeforeTable
      do AddPacket
    End
    If loc:selecting = 1 !and loc:parent = ''
      if loc:popup
        packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:BrowseCancelButton,'BrowsePartFaultCodeLookup',,,loc:popup)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
      end
      TableQueue.Kind = Net:BeforeTable
      do AddPacket
    End
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowsePartFaultCodeLookup',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowsePartFaultCodeLookup_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowsePartFaultCodeLookup_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          case loc:locatortype
          of Net:Position
          orof Net:Date
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
          of Net:Contains
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
          of Net:Begins
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
          End
          packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,'Locator')&'"><tr>' &|
          Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
          Case Loc:LocatorType
          of Net:Date
            packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator1BrowsePartFaultCodeLookup','',p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,, p_web.GSV('BrowsePartFaultCodeLookup_LocatorPic'),,,'onchange="BrowsePartFaultCodeLookup.locate(''Locator1BrowsePartFaultCodeLookup'',this.value);"',,0,,,) & '</div></td>'
          Else
            packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator1BrowsePartFaultCodeLookup',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,'size="30" onchange="BrowsePartFaultCodeLookup.locate(''Locator1BrowsePartFaultCodeLookup'',this.value);" ',,,,,,) & '</td>'
          End
          If loc:LocatorSearchButton or loc:LocatorClearButton
            packet = clip(packet) & '<td><div id="BrowsePartFaultCodeLookup_locate_b" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
            If loc:LocatorSearchButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
            End
            If loc:LocatorClearButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowsePartFaultCodeLookup.cl(''BrowsePartFaultCodeLookup'');')
            End
            packet = clip(packet) & '</div><13,10></td><13,10>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
          If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowsePartFaultCodeLookup_locate_b','buttonset',loc:options)
          End
      End
    End
  End
  p_web.SetSessionValue('BrowsePartFaultCodeLookup_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowsePartFaultCodeLookup_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:NavButtonPosition=Net:Below or loc:NavButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowsePartFaultCodeLookup_nav_b" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowsePartFaultCodeLookup.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowsePartFaultCodeLookup.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowsePartFaultCodeLookup.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowsePartFaultCodeLookup.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowsePartFaultCodeLookup_nav_b','buttonset',loc:options)
          End
        end
        do SendPacket
  End
  If loc:UpdateButtonPosition=Net:Below or loc:UpdateButtonPosition=Net:Both
  If loc:selecting = 1
    If loc:found
      packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SelectButton,'BrowsePartFaultCodeLookup',,,loc:popup)
    End
    do SendPacket
  End
  If loc:selecting = 1 !and loc:parent = ''
    if loc:popup
      packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:BrowseCancelButton,'BrowsePartFaultCodeLookup',,,loc:popup)
    else
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
    end
    do SendPacket
  End
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
  data
loc:rowclick  string(1024)
  code
    loc:field = p_web.AddBrowseValue('BrowsePartFaultCodeLookup','MANFPALO',mfp:RecordNumberKey) !mfp:RecordNumber
    p_web._thisrow = p_web._nocolon('mfp:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if Loc:LocatorValue <> '' and loc:ActualSelection = ''
        loc:checked = 'checked'
        do SetSelection
      elsif loc:ActualSelection = '' and mfp:RecordNumber = p_web.GetValue('mfp:RecordNumber')
         loc:checked = 'checked'
         do SetSelection
      elsif loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowsePartFaultCodeLookup:LookupField')) = mfp:RecordNumber and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      else
        loc:checked = Choose((mfp:RecordNumber = loc:selected) and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      end
      If(loc:SelectionMethod  = Net:Radio)
        loc:RowStyle = 'class="' & p_web.combine(p_web.site.style.browserow,) &'"'
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'data-nt-id="'& p_web.AddBrowseValue('BrowsePartFaultCodeLookup','MANFPALO',mfp:RecordNumberKey) &'" onclick="'
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If MANFPALO{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(MANFPALO)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            loc:DefaultSelection = loc:field
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If MANFPALO{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(MANFPALO)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            loc:DefaultSelection = loc:field
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'>'&p_web.CreateInput('radio','_bidv_',clip(loc:field),,loc:checked,'')&'</td>'&CRLF
          If loc:DefaultSelection = ''
            loc:DefaultSelection = loc:field
          End
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          If loc:DefaultSelection = '' or loc:direction < 0
            loc:DefaultSelection = loc:field
          End
        End
    end ! loc:eip = 0
        If Loc:Selecting = 1
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::Select
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        End     !Selecting
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::mfp:Field
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::mfp:Description
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr ' & clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Idx = p_web.AddBrowseValue('BrowsePartFaultCodeLookup','MANFPALO',mfp:RecordNumberKey)
  TableQueue.Id[1] = mfp:RecordNumber

ClosingScripts  Routine
  data
Rtn_SecwinProcedureName         string(252)
  code
  If p_web.RequestAjax = 0
    do SetFormAction
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var btiBrowsePartFaultCodeLookup;if (btiBrowsePartFaultCodeLookup != 1){{var BrowsePartFaultCodeLookup=new browseTable(''BrowsePartFaultCodeLookup'','''&clip(loc:formname)&|
      ''','''&p_web._jsok('mfp:RecordNumber',Net:Parameter)&''',2,'''&clip(loc:divname)&''',1,1,1,'''&|
      clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&|
      p_web.Translate('Are you sure you want to delete this record?')&''','''&clip(loc:ActualSelection)&''','''&clip(loc:selectaction)&''','''&|
      clip(loc:formactiontarget)&''','''','&loc:popup&',0,'''','''',"' & clip(Rtn_SecwinProcedureName) & '");<13,10>'&|
      'BrowsePartFaultCodeLookup.setGreenBar('''&p_web.ColorWeb(p_web.Site.Style.BrowseHighlightColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOneColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseTwoColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOverColor)&''');<13,10>' &|
      'BrowsePartFaultCodeLookup.applyGreenBar();btiBrowsePartFaultCodeLookup=1};<13,10>' &|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowsePartFaultCodeLookup')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowsePartFaultCodeLookup')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowsePartFaultCodeLookup')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowsePartFaultCodeLookup')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(MANFPALO)
  p_web._CloseFile(MANFAUPA)
  p_web._CloseFile(MODELCCT)
  p_web._CloseFile(MANFAULO_ALIAS)
  p_web._CloseFile(MANFPARL)
  p_web._CloseFile(MANFAUPA_ALIAS)
  p_web._CloseFile(MANFPALO_ALIAS)
  p_web._CloseFile(MANFAURL)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(MANFPALO)
  Bind(mfp:Record)
  Clear(mfp:Record)
  NetWebSetSessionPics(p_web,MANFPALO)
  p_web._OpenFile(MANFAUPA)
  Bind(map:Record)
  NetWebSetSessionPics(p_web,MANFAUPA)
  p_web._OpenFile(MODELCCT)
  Bind(mcc:Record)
  NetWebSetSessionPics(p_web,MODELCCT)
  p_web._OpenFile(MANFAULO_ALIAS)
  Bind(mfo_ali:Record)
  NetWebSetSessionPics(p_web,MANFAULO_ALIAS)
  p_web._OpenFile(MANFPARL)
  Bind(mpr:Record)
  NetWebSetSessionPics(p_web,MANFPARL)
  p_web._OpenFile(MANFAUPA_ALIAS)
  Bind(map_ali:Record)
  NetWebSetSessionPics(p_web,MANFAUPA_ALIAS)
  p_web._OpenFile(MANFPALO_ALIAS)
  Bind(mfp_ali:Record)
  NetWebSetSessionPics(p_web,MANFPALO_ALIAS)
  p_web._OpenFile(MANFAURL)
  Bind(mnr:Record)
  NetWebSetSessionPics(p_web,MANFAURL)

Children Routine
  if loc:selecting = 0
    If p_web.RequestAjax = 0
      do StartChildren
    Else
      do AjaxChildren
    End
  end

AjaxChildren  Routine
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue('mfp:RecordNumber',p_web.GetValue('mfp:RecordNumber'))
   p_web.DeleteValue('_Clicked')
   loc:found = 1
   do Children
! ----------------------------------------------------------------------------------------
CallRow  Routine
  data
loc:result  long
  code
  do OpenFilesB
  mfp:RecordNumber = p_web.GSV('mfp:RecordNumber')
  loc:result = p_web._GetFile(MANFPALO,mfp:RecordNumberKey)
  loc:eip = 1
  loc:viewstate = p_web.escape(p_web.Base64Encode(clip(mfp:RecordNumber)))
  do BrowseRow
  do SendPacket
  do ClosefilesB

! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(MANFPALO)
  Case upper(p_web.GetValue('_EIPClm'))
  Else
    case p_web.GetValue('_action_')
    of Net:InsertRecord
    orof Net:ChangeRecord
    orof Net:DeleteRecord
    orof Net:CopyRecord
      loc:eip = 0
      do CallBrowse
    end
  End
  do GotFocusBack
  p_web._CloseFile(MANFPALO)
! ----------------------------------------------------------------------------------------
value::Select   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    p_web.site.SmallSelectButton.TextValue = p_web.Translate('>>')
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowsePartFaultCodeLookup_Select_'&mfp:RecordNumber,,net:crc,,loc:extra)
        packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'BrowsePartFaultCodeLookup',p_web.AddBrowseValue('BrowsePartFaultCodeLookup','MANFPALO',mfp:RecordNumberKey),,loc:popup)
    End
    p_web.site.SmallSelectButton = p_web.RequestData.WebServer._SitesQueue.Defaults.SmallSelectButton  !bruce
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::mfp:Field   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowsePartFaultCodeLookup_mfp:Field_'&mfp:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(mfp:Field,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::mfp:Description   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowsePartFaultCodeLookup_mfp:Description_'&mfp:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(mfp:Description,'@s60')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  p_web._OpenFile(MANFAUPA)
  p_web._OpenFile(MODELCCT)
  p_web._OpenFile(MANFAULO_ALIAS)
  p_web._OpenFile(MANFPARL)
  p_web._OpenFile(MANFAUPA_ALIAS)
  p_web._OpenFile(MANFPALO_ALIAS)
  p_web._OpenFile(MANFAURL)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(MANFAUPA)
  p_Web._CloseFile(MODELCCT)
  p_Web._CloseFile(MANFAULO_ALIAS)
  p_Web._CloseFile(MANFPARL)
  p_Web._CloseFile(MANFAUPA_ALIAS)
  p_Web._CloseFile(MANFPALO_ALIAS)
  p_Web._CloseFile(MANFAURL)
     FilesOpened = False
  END
  p_web.deleteSessionValue('fieldNumber')
  p_web.deleteSessionValue('partType')
  return
!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
CheckForDuplicate  Routine
  If loc:invalid <> '' then exit. ! no need to check, record is already invalid
  If Duplicate(mfp:Field_Key)
    loc:Invalid = 'mfp:Manufacturer'
    loc:Alert = clip(p_web.site.DuplicateText) & ' Field_Key --> mfp:Manufacturer, mfp:Field_Number, '&clip('Field')&''
  End
SetSelection  Routine
  loc:ActualSelection = loc:field
  p_web.SetSessionValue('mfp:RecordNumber',mfp:RecordNumber)

MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:ActualSelection = ''
    loc:ActualSelection = loc:DefaultSelection
    p_web.GetBrowseValue(loc:ActualSelection,Net:Web:SessionQueue) ! so children are primed with correct sessionValue
  End

  If loc:ActualSelection <> ''
    TableQueue.Kind = Net:RowTable
    get(TableQueue,TableQueue.Kind)
    if Errorcode() = 0
      TableQueue.Row = sub(TableQueue.Row,1,len(clip(TableQueue.Row))-1) &  ' data-nt-rows="'&loc:RowsHigh&'" data-nt-value="'&clip(loc:ActualSelection)&'"' & '>'
      Put(TableQueue)
    End
  End

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead class="'&p_web.combine(p_web.site.style.BrowseHeader,'')&'"><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot class="'&p_web.combine(p_web.site.style.BrowseFooter,'BrowseFooter')&'"><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody class="'&p_web.combine(p_web.site.style.BrowseBody,)&'"><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table><13,10>' & p_web.DivFooter(Net:NoSend) ! Table Div
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
BrowseModelStock     PROCEDURE  (NetWebServerWorker p_web)
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256),dim(Net:MaxKeyFields)
idx           String(Net:HashSize)
sub           Long
            End
Select:IsInvalid  Long
stm:Description:IsInvalid  Long
stm:Part_Number:IsInvalid  Long
sto:Purchase_Cost:IsInvalid  Long
sto:Sale_Cost:IsInvalid  Long
sto:Quantity_Stock:IsInvalid  Long
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:DefaultSelection    String(Net:HashSize)
loc:ActualSelection     String(Net:HashSize)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
!loc:pagename            String(256)
loc:NavButtonPosition    Long
loc:UpdateButtonPosition Long
loc:SelectionMethod      Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorCase         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
loc:lookupdone          Long
loc:FormPopup           Long
ThisView            View(STOMODEL)
                      Project(stm:RecordNumber)
                      Project(stm:Description)
                      Project(stm:Part_Number)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
Loc:popup             Long
loc:poppedup          Long,static,thread
FilesOpened     Long
STOCK::State  USHORT
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return 
  End
  GlobalErrors.SetProcedureName('BrowseModelStock')
  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseModelStock:NoForm')
      loc:NoForm = p_web.GetValue('BrowseModelStock:NoForm')
      loc:FormName = p_web.GetValue('BrowseModelStock:FormName')
    else
      loc:FormName = 'BrowseModelStock_frm'
    End
    p_web.SSV('BrowseModelStock:NoForm',loc:NoForm)
    p_web.SSV('BrowseModelStock:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseModelStock:NoForm')
    loc:FormName = p_web.GSV('BrowseModelStock:FormName')
  end
  case p_web.site.formpopups
  of -1 ; loc:FormPopup = Net:Page
  of 0 ; loc:FormPopup = Net:Page
  of 1 ; loc:FormPopup = Net:Popup
  End
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseModelStock') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseModelStock')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if 1 = 2
  elsif p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  elsif p_web.IfExistsValue('_EIPRow_')
    do CallRow
  elsif p_web.GetValue('_CallPopups') > 0
    do CallPopups
  elsif p_web.IfExistsValue('_GetSecwinSettings')
      p_web.SetValue('Secwin_' & 'BrowseModelStock' & 'AccessGroupsArray','1') 
      p_web.SetValue('Secwin_AccessWindowName','BrowseModelStock')
      
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallPopups  Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
loc:CallPopups   Long
loc:name         String(255)
  code
  if loc:poppedup = 0
    loc:CallPopups = p_web.GetValue('_CallPopups')
    if loc:CallPopups = 1 !and p_web.GetPreCall('BrowseModelStock') = 0! for = 1 include popup divs and scripts for this browse
      !p_web.AddPreCall('BrowseModelStock')
      p_web.DivHeader('popup_BrowseModelStock','nt-hidden')
      p_web.DivHeader('BrowseModelStock',p_web.combine(p_web.site.style.browsediv,))
      p_web.DivFooter()
      p_web.DivFooter()
      loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(400)&', modal: true, position: [''center'',15]'
      packet =  p_web.jQuery('#' & lower('popup_BrowseModelStock_div'),'dialog',loc:options) & |
      p_web.jQuery('#' & lower('popup_BrowseModelStock_div'),'removeClass','''nt-hidden''')
    End
    ! for > 0 add in form, EIP-lookups and browse object
    do Popups
    loc:popup = 1
    do ClosingScripts
    do SendPacket
    loc:poppedup = 1
  end

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web.DivHeader('BrowseModelStock',p_web.combine(p_web.site.style.browsediv,))
  if loc:ParentSilent = 0
    do GenerateBrowse
    if p_web.RequestAjax = 1
      p_web.Script('BrowseProducts.pcv();')
    end
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web.DivFooter()
  do Children
  do Popups
  if loc:poppedup = 0
    do ClosingScripts
  end
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(STOMODEL,stm:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'STM:DESCRIPTION') then p_web.SetValue('BrowseModelStock_sort','2')
    ElsIf (loc:vorder = 'STM:PART_NUMBER') then p_web.SetValue('BrowseModelStock_sort','7')
    ElsIf (loc:vorder = 'STO:PURCHASE_COST') then p_web.SetValue('BrowseModelStock_sort','5')
    ElsIf (loc:vorder = 'STO:SALE_COST') then p_web.SetValue('BrowseModelStock_sort','6')
    ElsIf (loc:vorder = 'STO:QUANTITY_STOCK') then p_web.SetValue('BrowseModelStock_sort','3')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseModelStock:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseModelStock:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseModelStock:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseModelStock:LookupField')
    loc:selecting = 0
  End

Popups Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  If loc:popup = 0
  End
SetFormAction  Routine
  loc:formaction = 'BrowseModelStock'

GotFocusBack   Routine

GenerateBrowse Routine
  data
loc:viewoptions  Long
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  ! Set general Browse options
  loc:NavButtonPosition   = Net:Below
  loc:UpdateButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:PageLoad
  loc:Sorting          = Net:ServerSort
  if p_web.GetValue('_popup_') = 1
    loc:popup = 1
  end
  ! Set Locator Options
  loc:LocatorPosition  = Net:Above
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  p_web.ClearBrowse('BrowseModelStock')
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 15
  End
  loc:ActualSelection = ''
  !loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseModelStock_sort',net:DontEvaluate)
  If loc:vordernumber = 0
    loc:vordernumber = 2
  End
  p_web.SetSessionValue('BrowseModelStock_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 4
    Loc:LocateField = ''
    Loc:LocatorCase = 0
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(stm:Description)','-UPPER(stm:Description)')
    Loc:LocateField = 'stm:Description'
    Loc:LocatorCase = 0
  of 7
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(stm:Part_Number)','-UPPER(stm:Part_Number)')
    Loc:LocateField = 'stm:Part_Number'
    Loc:LocatorCase = 0
  of 5
    loc:vorder = Choose(Loc:SortDirection=1,'sto:Purchase_Cost','-sto:Purchase_Cost')
    Loc:LocateField = 'sto:Purchase_Cost'
    Loc:LocatorCase = 0
  of 6
    loc:vorder = Choose(Loc:SortDirection=1,'sto:Sale_Cost','-sto:Sale_Cost')
    Loc:LocateField = 'sto:Sale_Cost'
    Loc:LocatorCase = 0
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'sto:Quantity_Stock','-sto:Quantity_Stock')
    Loc:LocateField = 'sto:Quantity_Stock'
    Loc:LocatorCase = 0
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('stm:Description')
    loc:SortHeader = p_web.Translate('Description')
    p_web.SetSessionValue('BrowseModelStock_LocatorPic','@s30')
  Of upper('stm:Part_Number')
    loc:SortHeader = p_web.Translate('Part Number')
    p_web.SetSessionValue('BrowseModelStock_LocatorPic','@s30')
  Of upper('sto:Purchase_Cost')
    loc:SortHeader = p_web.Translate('In Warranty')
    p_web.SetSessionValue('BrowseModelStock_LocatorPic','@n14.2')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
  Of upper('sto:Sale_Cost')
    loc:SortHeader = p_web.Translate('Out Warranty')
    p_web.SetSessionValue('BrowseModelStock_LocatorPic','@n14.2')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
  Of upper('sto:Quantity_Stock')
    loc:SortHeader = p_web.Translate('Quantity In Stock')
    p_web.SetSessionValue('BrowseModelStock_LocatorPic','@N8')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
  End
  If loc:selecting = 1
    p_web.GetSettings(p_web.GetSessionValue('Push1'))
    loc:selectaction = p_web.FormSettings.ParentPage !p_web.GetSessionValue('BrowseModelStock:LookupFrom')
  End!Else
  do SendPacket
  do SetFormAction
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseModelStock:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseModelStock:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseModelStock:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="STOMODEL"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="stm:RecordNumberKey"></input><13,10>'
  end
  If p_web.Translate('Browse Stock') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.browseSubHeading,)&'">'&p_web.Translate('Browse Stock',0)&'</div>'&CRLF
  End
  case p_web.GetValue('refresh') ; of 'next' orof 'previous' orof 'first' orof 'last'
    p_web.Script('try{{BrowseModelStock.restoreFocus();} catch(err){{};')
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseModelStock',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      case loc:locatortype
      of Net:Position
      orof Net:Date
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
      of Net:Contains
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
      of Net:Begins
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
      End
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
      Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
      Case Loc:LocatorType
      of Net:Date
        packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator2BrowseModelStock','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('BrowseModelStock_LocatorPic'),,,'onchange="BrowseModelStock.locate(''Locator2BrowseModelStock'',this.value);"',,0,,,) & '</div></td>'
      Else
        packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator2BrowseModelStock',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="BrowseModelStock.locate(''Locator2BrowseModelStock'',this.value);" ',,,,,,) & '</td>'
      End
      If loc:LocatorSearchButton or loc:LocatorClearButton
        packet = clip(packet) & '<td><div id="BrowseModelStock_locate_a" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
        If loc:LocatorSearchButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
        End
        If loc:LocatorClearButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseModelStock.cl(''BrowseModelStock'');')
        End
        packet = clip(packet) & '</div><13,10></td><13,10>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
      If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'BrowseModelStock_locate_a','buttonset',loc:options)
      End
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & p_web.DivHeader('BrowseModelStock_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'','sortable')&'" id="BrowseModelStock_tbl">'
  Else
    packet = clip(packet) & p_web.DivHeader('BrowseModelStock_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'')&'" id="BrowseModelStock_tbl">'
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
      If loc:Selecting = 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'4','BrowseModelStock',p_web.Translate('Pick'),,,,,,,0,loc:Sorting,'Button')
          do AddPacket
          loc:columns += 1
      End ! Selecting
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'2','BrowseModelStock',p_web.Translate('Description'),'Click here to sort by Description',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'7','BrowseModelStock',p_web.Translate('Part Number'),'Click here to sort by Part Number',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'5','BrowseModelStock',p_web.Translate('In Warranty'),'Click here to sort by In Warranty Cost',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'6','BrowseModelStock',p_web.Translate('Out Warranty'),'Click here to sort by Out Warranty Cost',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'3','BrowseModelStock',p_web.Translate('Quantity In Stock'),'Click here to sort by Quantity In Stock',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,15,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  If Instring('stm:recordnumber',lower(loc:vorder),1,1) = 0 !and STOMODEL{prop:SQLDriver} = 1
    loc:vorder = Choose(loc:vorder='','stm:RecordNumber',clip(loc:vorder) & ',' & 'stm:RecordNumber')
  End
  Loc:Selected = Choose(p_web.IfExistsValue('stm:RecordNumber'),p_web.GetValue('stm:RecordNumber'),p_web.GetSessionValue('stm:RecordNumber'))
  ThisView{prop:order} = p_web.CleanFilter(ThisView,clip(loc:vorder))
      loc:FilterWas = 'Upper(stm:Model_Number) = Upper(''' & p_web.GSV('job:Model_Number') & ''') AND Upper(stm:Location)  = Upper(''' & p_web.GSV('BookingSiteLocation') & ''')'
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseModelStock',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseModelStock_Filter')
    p_web.SetSessionValue('BrowseModelStock_FirstValue','')
    p_web.SetSessionValue('BrowseModelStock_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,STOMODEL,stm:RecordNumberKey,loc:PageRows,'BrowseModelStock',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,loc:ViewOptions,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled,Loc:LocatorCase)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or Loc:LocatorType = Net:Date or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position or loc:LocatorType = Net:Date
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If STOMODEL{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(STOMODEL,loc:firstvalue)
              Reset(ThisView,STOMODEL)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If STOMODEL{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(STOMODEL,loc:lastvalue)
            Reset(ThisView,STOMODEL)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      if (stm:Part_Number = 'EXCH')
          cycle
      end !if (stm:Part_Number = 'EXCH')
      
      Access:STOCK.ClearKey(sto:Ref_Number_Key)
      sto:Ref_Number = stm:Ref_Number
      If (Access:STOCK.TryFetch(sto:Ref_Number_Key))
          cycle
      end
      
      if (p_web.GSV('BookingRestrictParts') = 1 and |
          p_web.GSV('BookingRestrictChargeable') = 1 and |
          sto:SkillLevel > p_web.GSV('BookingSkillLevel'))
          cycle
      end ! if (p_web.GSV('BookingRestrictParts') = 1
      
      if (p_web.GSV('BookingSkillLevel') > 0)
          case p_web.GSV('BookingSkillLevel')
          of 1
              if (~sto:E1)
                  cycle
              end ! if (~sto:E1)
          of 2
              if ~(sto:E1 or sto:E2)
                  cycle
              end!if ~(sto:E1 or sto:E2)
          of 3
              if ~(sto:E1 or sto:E2 or sto:E3)
                  cycle
              end ! if ~(sto:E1 or sto:E2 or sto:E3)
          end ! case
      
      end ! if (p_web.GSV('BookingSkillLevel') > 0)
      
      
      if (sto:Suspend)
          cycle
      end ! if (stm:Part_Number = 'EXCH')
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(stm:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td class="'&p_web.combine(p_web.site.style.BrowseEmpty,)&'">'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:NavButtonPosition=Net:Above or (loc:NavButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseModelStock_nav_a" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseModelStock.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseModelStock.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseModelStock.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseModelStock.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseModelStock_nav_a','buttonset',loc:options)
          End
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:UpdateButtonPosition=Net:Above or (loc:UpdateButtonPosition=Net:Both and loc:found > 0))
    If loc:selecting = 1 !and loc:parent = ''
      if loc:popup
        packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:BrowseCancelButton,'BrowseModelStock',,,loc:popup)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
      end
      TableQueue.Kind = Net:BeforeTable
      do AddPacket
    End
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseModelStock',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseModelStock_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseModelStock_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          case loc:locatortype
          of Net:Position
          orof Net:Date
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
          of Net:Contains
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
          of Net:Begins
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
          End
          packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
          Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
          Case Loc:LocatorType
          of Net:Date
            packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator1BrowseModelStock','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('BrowseModelStock_LocatorPic'),,,'onchange="BrowseModelStock.locate(''Locator1BrowseModelStock'',this.value);"',,0,,,) & '</div></td>'
          Else
            packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator1BrowseModelStock',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="BrowseModelStock.locate(''Locator1BrowseModelStock'',this.value);" ',,,,,,) & '</td>'
          End
          If loc:LocatorSearchButton or loc:LocatorClearButton
            packet = clip(packet) & '<td><div id="BrowseModelStock_locate_b" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
            If loc:LocatorSearchButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
            End
            If loc:LocatorClearButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseModelStock.cl(''BrowseModelStock'');')
            End
            packet = clip(packet) & '</div><13,10></td><13,10>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
          If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseModelStock_locate_b','buttonset',loc:options)
          End
      End
    End
  End
  p_web.SetSessionValue('BrowseModelStock_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseModelStock_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:NavButtonPosition=Net:Below or loc:NavButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseModelStock_nav_b" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseModelStock.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseModelStock.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseModelStock.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseModelStock.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseModelStock_nav_b','buttonset',loc:options)
          End
        end
        do SendPacket
  End
  If loc:UpdateButtonPosition=Net:Below or loc:UpdateButtonPosition=Net:Both
  If loc:selecting = 1 !and loc:parent = ''
    if loc:popup
      packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:BrowseCancelButton,'BrowseModelStock',,,loc:popup)
    else
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
    end
    do SendPacket
  End
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
  data
loc:rowclick  string(1024)
  code
    loc:field = p_web.AddBrowseValue('BrowseModelStock','STOMODEL',stm:RecordNumberKey) !stm:RecordNumber
    p_web._thisrow = p_web._nocolon('stm:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if Loc:LocatorValue <> '' and loc:ActualSelection = ''
        loc:checked = 'checked'
        do SetSelection
      elsif loc:ActualSelection = '' and stm:RecordNumber = p_web.GetValue('stm:RecordNumber')
         loc:checked = 'checked'
         do SetSelection
      elsif loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseModelStock:LookupField')) = stm:RecordNumber and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      else
        loc:checked = Choose((stm:RecordNumber = loc:selected) and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      end
      If(loc:SelectionMethod  = Net:Radio)
        loc:RowStyle = 'class="' & p_web.combine(p_web.site.style.browserow,) &'"'
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'data-nt-id="'& p_web.AddBrowseValue('BrowseModelStock','STOMODEL',stm:RecordNumberKey) &'" onclick="'
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If STOMODEL{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(STOMODEL)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            loc:DefaultSelection = loc:field
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If STOMODEL{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(STOMODEL)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            loc:DefaultSelection = loc:field
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'>'&p_web.CreateInput('radio','_bidv_',clip(loc:field),,loc:checked,'')&'</td>'&CRLF
          If loc:DefaultSelection = ''
            loc:DefaultSelection = loc:field
          End
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          If loc:DefaultSelection = '' or loc:direction < 0
            loc:DefaultSelection = loc:field
          End
        End
    end ! loc:eip = 0
        If Loc:Selecting = 1
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::Select
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        End     !Selecting
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::stm:Description
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::stm:Part_Number
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::sto:Purchase_Cost
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::sto:Sale_Cost
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::sto:Quantity_Stock
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr ' & clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Idx = p_web.AddBrowseValue('BrowseModelStock','STOMODEL',stm:RecordNumberKey)
  TableQueue.Id[1] = stm:RecordNumber

ClosingScripts  Routine
  data
Rtn_SecwinProcedureName         string(252)
  code
  If p_web.RequestAjax = 0
    do SetFormAction
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var btiBrowseModelStock;if (btiBrowseModelStock != 1){{var BrowseModelStock=new browseTable(''BrowseModelStock'','''&clip(loc:formname)&|
      ''','''&p_web._jsok('stm:RecordNumber',Net:Parameter)&''',2,'''&clip(loc:divname)&''',1,1,1,'''&|
      clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&|
      p_web.Translate('Are you sure you want to delete this record?')&''','''&clip(loc:ActualSelection)&''','''&clip(loc:selectaction)&''','''&|
      clip(loc:formactiontarget)&''','''','&loc:popup&',0,'''','''',"' & clip(Rtn_SecwinProcedureName) & '");<13,10>'&|
      'BrowseModelStock.setGreenBar('''&p_web.ColorWeb(p_web.Site.Style.BrowseHighlightColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOneColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseTwoColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOverColor)&''');<13,10>' &|
      'BrowseModelStock.applyGreenBar();btiBrowseModelStock=1};<13,10>' &|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseModelStock')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseModelStock')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseModelStock')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseModelStock')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(STOMODEL)
  p_web._CloseFile(STOCK)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(STOMODEL)
  Bind(stm:Record)
  Clear(stm:Record)
  NetWebSetSessionPics(p_web,STOMODEL)
  p_web._OpenFile(STOCK)
  Bind(sto:Record)
  NetWebSetSessionPics(p_web,STOCK)

Children Routine
  if loc:selecting = 0
    If p_web.RequestAjax = 0
      do StartChildren
    Else
      do AjaxChildren
    End
  end

AjaxChildren  Routine
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue('stm:RecordNumber',p_web.GetValue('stm:RecordNumber'))
   p_web.DeleteValue('_Clicked')
   loc:found = 1
   do Children
! ----------------------------------------------------------------------------------------
CallRow  Routine
  data
loc:result  long
  code
  do OpenFilesB
  stm:RecordNumber = p_web.GSV('stm:RecordNumber')
  loc:result = p_web._GetFile(STOMODEL,stm:RecordNumberKey)
  loc:eip = 1
  loc:viewstate = p_web.escape(p_web.Base64Encode(clip(stm:RecordNumber)))
  do BrowseRow
  do SendPacket
  do ClosefilesB

! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(STOMODEL)
  Case upper(p_web.GetValue('_EIPClm'))
  Else
    case p_web.GetValue('_action_')
    of Net:InsertRecord
    orof Net:ChangeRecord
    orof Net:DeleteRecord
    orof Net:CopyRecord
      loc:eip = 0
      do CallBrowse
    end
  End
  do GotFocusBack
  p_web._CloseFile(STOMODEL)
! ----------------------------------------------------------------------------------------
value::Select   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    p_web.site.SmallSelectButton.TextValue = p_web.Translate('>>')
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseModelStock_Select_'&stm:RecordNumber,,net:crc,,loc:extra)
        packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'BrowseModelStock',p_web.AddBrowseValue('BrowseModelStock','STOMODEL',stm:RecordNumberKey),,loc:popup)
    End
    p_web.site.SmallSelectButton = p_web.RequestData.WebServer._SitesQueue.Defaults.SmallSelectButton  !bruce
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::stm:Description   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseModelStock_stm:Description_'&stm:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(stm:Description,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::stm:Part_Number   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseModelStock_stm:Part_Number_'&stm:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(stm:Part_Number,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sto:Purchase_Cost   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseModelStock_sto:Purchase_Cost_'&stm:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(sto:Purchase_Cost,'@n14.2')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sto:Sale_Cost   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseModelStock_sto:Sale_Cost_'&stm:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(sto:Sale_Cost,'@n14.2')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sto:Quantity_Stock   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseModelStock_sto:Quantity_Stock_'&stm:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(sto:Quantity_Stock,'@N8')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  p_web._OpenFile(STOCK)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(STOCK)
     FilesOpened = False
  END
  return
!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
CheckForDuplicate  Routine
  If loc:invalid <> '' then exit. ! no need to check, record is already invalid
  If Duplicate(stm:Model_Number_Key)
    loc:Invalid = 'stm:Ref_Number'
    loc:Alert = clip(p_web.site.DuplicateText) & ' Model_Number_Key --> stm:Ref_Number, stm:Manufacturer, stm:Model_Number'
  End
SetSelection  Routine
  loc:ActualSelection = loc:field
  p_web.SetSessionValue('stm:RecordNumber',stm:RecordNumber)

MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:ActualSelection = ''
    loc:ActualSelection = loc:DefaultSelection
    p_web.GetBrowseValue(loc:ActualSelection,Net:Web:SessionQueue) ! so children are primed with correct sessionValue
  End

  If loc:ActualSelection <> ''
    TableQueue.Kind = Net:RowTable
    get(TableQueue,TableQueue.Kind)
    if Errorcode() = 0
      TableQueue.Row = sub(TableQueue.Row,1,len(clip(TableQueue.Row))-1) &  ' data-nt-rows="'&loc:RowsHigh&'" data-nt-value="'&clip(loc:ActualSelection)&'"' & '>'
      Put(TableQueue)
    End
  End

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead class="'&p_web.combine(p_web.site.style.BrowseHeader,'')&'"><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot class="'&p_web.combine(p_web.site.style.BrowseFooter,)&'"><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody class="'&p_web.combine(p_web.site.style.BrowseBody,)&'"><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table><13,10>' & p_web.DivFooter(Net:NoSend) ! Table Div
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
BrowseWarrantyParts  PROCEDURE  (NetWebServerWorker p_web)
tmp:WARPARTStatus    STRING(20)                            !
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256),dim(Net:MaxKeyFields)
idx           String(Net:HashSize)
sub           Long
            End
wpr:Part_Number:IsInvalid  Long
wpr:Description:IsInvalid  Long
wpr:Quantity:IsInvalid  Long
tmp:WARPARTStatus:IsInvalid  Long
Edit:IsInvalid  Long
MyDelete:IsInvalid  Long
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:DefaultSelection    String(Net:HashSize)
loc:ActualSelection     String(Net:HashSize)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
!loc:pagename            String(256)
loc:NavButtonPosition    Long
loc:UpdateButtonPosition Long
loc:SelectionMethod      Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorCase         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
loc:lookupdone          Long
loc:FormPopup           Long
ThisView            View(WARPARTS)
                      Project(wpr:Record_Number)
                      Project(wpr:Part_Number)
                      Project(wpr:Description)
                      Project(wpr:Quantity)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
Loc:popup             Long
loc:poppedup          Long,static,thread
FilesOpened     Long
STOCK::State  USHORT
USERS::State  USHORT
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return 
  End
  GlobalErrors.SetProcedureName('BrowseWarrantyParts')
  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseWarrantyParts:NoForm')
      loc:NoForm = p_web.GetValue('BrowseWarrantyParts:NoForm')
      loc:FormName = p_web.GetValue('BrowseWarrantyParts:FormName')
    else
      loc:FormName = 'BrowseWarrantyParts_frm'
    End
    p_web.SSV('BrowseWarrantyParts:NoForm',loc:NoForm)
    p_web.SSV('BrowseWarrantyParts:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseWarrantyParts:NoForm')
    loc:FormName = p_web.GSV('BrowseWarrantyParts:FormName')
  end
  case p_web.site.formpopups
  of -1 ; loc:FormPopup = Net:Page
  of 0 ; loc:FormPopup = Net:Page
  of 1 ; loc:FormPopup = Net:Popup
  End
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseWarrantyParts') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseWarrantyParts')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if 1 = 2
  elsif p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  elsif p_web.IfExistsValue('_EIPRow_')
    do CallRow
  elsif p_web.GetValue('_CallPopups') > 0
    do CallPopups
  elsif p_web.IfExistsValue('_GetSecwinSettings')
      p_web.SetValue('Secwin_' & 'BrowseWarrantyParts' & 'AccessGroupsArray','1') 
      p_web.SetValue('Secwin_AccessWindowName','BrowseWarrantyParts')
      
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallPopups  Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
loc:CallPopups   Long
loc:name         String(255)
  code
  if loc:poppedup = 0
    loc:CallPopups = p_web.GetValue('_CallPopups')
    if loc:CallPopups = 1 !and p_web.GetPreCall('BrowseWarrantyParts') = 0! for = 1 include popup divs and scripts for this browse
      !p_web.AddPreCall('BrowseWarrantyParts')
      p_web.DivHeader('popup_BrowseWarrantyParts','nt-hidden')
      p_web.DivHeader('BrowseWarrantyParts',p_web.combine(p_web.site.style.browsediv,))
      p_web.DivFooter()
      p_web.DivFooter()
      loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(400)&', modal: true, position: [''center'',15]'
      packet =  p_web.jQuery('#' & lower('popup_BrowseWarrantyParts_div'),'dialog',loc:options) & |
      p_web.jQuery('#' & lower('popup_BrowseWarrantyParts_div'),'removeClass','''nt-hidden''')
    End
    ! for > 0 add in form, EIP-lookups and browse object
    do Popups
    loc:popup = 1
    do ClosingScripts
    do SendPacket
    loc:poppedup = 1
  end

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web.DivHeader('BrowseWarrantyParts',p_web.combine(p_web.site.style.browsediv,))
  if loc:ParentSilent = 0
    do GenerateBrowse
    if p_web.RequestAjax = 1
      p_web.Script('BrowseProducts.pcv();')
    end
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web.DivFooter()
  do Children
  do Popups
  if loc:poppedup = 0
    do ClosingScripts
  end
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(WARPARTS,wpr:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'WPR:PART_NUMBER') then p_web.SetValue('BrowseWarrantyParts_sort','1')
    ElsIf (loc:vorder = 'WPR:DESCRIPTION') then p_web.SetValue('BrowseWarrantyParts_sort','2')
    ElsIf (loc:vorder = 'WPR:QUANTITY') then p_web.SetValue('BrowseWarrantyParts_sort','3')
    ElsIf (loc:vorder = 'TMP:WARPARTSTATUS') then p_web.SetValue('BrowseWarrantyParts_sort','4')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseWarrantyParts:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseWarrantyParts:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseWarrantyParts:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseWarrantyParts:LookupField')
    loc:selecting = 0
  End

Popups Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  If loc:popup = 0
  End
SetFormAction  Routine
  loc:formaction = 'FormWarrantyParts'
  loc:formactiontarget = '_self'

GotFocusBack   Routine

GenerateBrowse Routine
  data
loc:viewoptions  Long
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  ! Set general Browse options
  loc:NavButtonPosition   = Net:Below
  loc:UpdateButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:FileLoad
  loc:FillBack         = 0
  loc:Sorting          = Net:NoSort
  if p_web.GetValue('_popup_') = 1
    loc:popup = 1
  end
  ! Set Locator Options
  loc:LocatorPosition  = Net:Below
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  p_web.ClearBrowse('BrowseWarrantyParts')
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  p_web.SSV('Hide:InsertButton',0)
  p_web.SSV('Hide:ChangeButton',0)
  p_web.SSV('Hide:DeleteButton',0)
  
  if (p_web.GSV('job:Date_Completed') > 0)
      p_web.SSV('Hide:InsertButton',1)
      p_web.SSV('Hide:DeleteButton',1)
  end ! if (p_web.GSV('job:Date_Completed') > 0)
  
  if (p_web.GSV('job:Invoice_Number_Warranty') > 0)
      p_web.SSV('Hide:InsertButton',1)
      p_web.SSV('Hide:DeleteButton',1)
  end ! if (p_web.GSV('job:Invoice_Number_Warranty') > 0)
  
  if (p_web.GSV('Hide:InsertButton') = 0)
      IF (p_web.GSV('locEngineeringOption') = 'Not Set')
          p_web.SSV('Hide:InsertButton',1)
      ELSE
  
          Access:USERS.Clearkey(use:user_Code_Key)
          use:user_code    = p_web.GSV('job:Engineer')
          if (Access:USERS.TryFetch(use:user_Code_Key) = Level:Benign)
              ! Found
              ! The Site That Attached The Part Must Remove The Part
              if (p_web.GSV('BookingSite') = 'RRC')
                  if (use:location = p_web.GSV('ARC:SiteLocation'))
                      p_web.SSV('Hide:InsertButton',1)
                  end ! if (use:location = p_web.GSV('ARC:SiteLocation'))
              else ! if (p_web.GSV('BookingSite') = 'RRC')
                  if (use:location <> p_web.GSV('ARC:SiteLocation'))
                      p_web.SSV('Hide:InsertButton',1)
                  end ! if (use:location <> p_web.GSV('ARC:SiteLocation'))
              end ! if (p_web.GSV('BookingSite') = 'RRC')
  
  
              Access:LOCATION.Clearkey(loc:location_Key)
              loc:location    = use:Location
              if (Access:LOCATION.TryFetch(loc:location_Key) = Level:Benign)
                  ! Found
                  if (loc:Active = 0)
                      ! can add parts from an inactive location
                      p_web.SSV('Hide:InsertButton',1)
                  end !if (loc:Active = 0)
              else ! if (Access:LOCATION.TryFetch(loc:location_Key) = Level:Benign)
                  ! Error
              end ! if (Access:LOCATION.TryFetch(loc:location_Key) = Level:Benign)
          else ! if (Access:USERS.TryFetch(use:user_Code_Key) = Level:Benign)
              ! Error
              p_web.SSV('Hide:InsertButton',1)
          end ! if (Access:USERS.TryFetch(use:user_Code_Key) = Level:Benign)
      END
  
  end ! if (error# = 0)
  
  ! DBH #10544 - Liquid Damage. Don't amend parts
  IF (p_web.GSV('jobe:Booking48HourOption') = 4) OR (p_web.GSV('Job:ViewOnly') = 1)
      p_web.SSV('Hide:InsertButton',1)
      p_web.SSV('Hide:ChangeBUtton',1)
      p_web.SSV('Hide:DeleteButton',1)
  END ! IF (p_web.GSV('jobe:Booking48HourOption') = 4)
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 10
  End
  loc:ActualSelection = ''
  !loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseWarrantyParts_sort',net:DontEvaluate)
  p_web.SetSessionValue('BrowseWarrantyParts_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(wpr:Part_Number)','-UPPER(wpr:Part_Number)')
    Loc:LocateField = 'wpr:Part_Number'
    Loc:LocatorCase = 0
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(wpr:Description)','-UPPER(wpr:Description)')
    Loc:LocateField = 'wpr:Description'
    Loc:LocatorCase = 0
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'wpr:Quantity','-wpr:Quantity')
    Loc:LocateField = 'wpr:Quantity'
    Loc:LocatorCase = 0
  of 4
    loc:vorder = Choose(Loc:SortDirection=1,'tmp:WARPARTStatus','-tmp:WARPARTStatus')
    Loc:LocateField = 'tmp:WARPARTStatus'
    Loc:LocatorCase = 0
  of 5
    Loc:LocateField = ''
    Loc:LocatorCase = 0
  of 7
    Loc:LocateField = ''
    Loc:LocatorCase = 0
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('wpr:Part_Number')
    loc:SortHeader = p_web.Translate('Part Number')
    p_web.SetSessionValue('BrowseWarrantyParts_LocatorPic','@s30')
  Of upper('wpr:Description')
    loc:SortHeader = p_web.Translate('Description')
    p_web.SetSessionValue('BrowseWarrantyParts_LocatorPic','@s30')
  Of upper('wpr:Quantity')
    loc:SortHeader = p_web.Translate('Quantity')
    p_web.SetSessionValue('BrowseWarrantyParts_LocatorPic','@s8')
  Of upper('tmp:WARPARTStatus')
    loc:SortHeader = p_web.Translate('Status')
    p_web.SetSessionValue('BrowseWarrantyParts_LocatorPic','@s20')
  End
  If loc:selecting = 1
    p_web.GetSettings(p_web.GetSessionValue('Push1'))
    loc:selectaction = p_web.FormSettings.ParentPage !p_web.GetSessionValue('BrowseWarrantyParts:LookupFrom')
  End!Else
  do SendPacket
  do SetFormAction
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseWarrantyParts:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseWarrantyParts:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseWarrantyParts:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="WARPARTS"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="wpr:RecordNumberKey"></input><13,10>'
  end
  if p_web.Translate('Warranty Parts') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.browseHeading,)&'">'&p_web.Translate('Warranty Parts',0)&'</div>'&CRLF
  end
  case p_web.GetValue('refresh') ; of 'next' orof 'previous' orof 'first' orof 'last'
    p_web.Script('try{{BrowseWarrantyParts.restoreFocus();} catch(err){{};')
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseWarrantyParts',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      case loc:locatortype
      of Net:Position
      orof Net:Date
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
      of Net:Contains
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
      of Net:Begins
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
      End
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
      Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
      Case Loc:LocatorType
      of Net:Date
        packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator2BrowseWarrantyParts','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('BrowseWarrantyParts_LocatorPic'),,,'onchange="BrowseWarrantyParts.locate(''Locator2BrowseWarrantyParts'',this.value);"',,0,,,) & '</div></td>'
      Else
        packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator2BrowseWarrantyParts',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="BrowseWarrantyParts.locate(''Locator2BrowseWarrantyParts'',this.value);" ',,,,,,) & '</td>'
      End
      If loc:LocatorSearchButton or loc:LocatorClearButton
        packet = clip(packet) & '<td><div id="BrowseWarrantyParts_locate_a" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
        If loc:LocatorSearchButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
        End
        If loc:LocatorClearButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseWarrantyParts.cl(''BrowseWarrantyParts'');')
        End
        packet = clip(packet) & '</div><13,10></td><13,10>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
      If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'BrowseWarrantyParts_locate_a','buttonset',loc:options)
      End
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & p_web.DivHeader('BrowseWarrantyParts_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'','sortable')&'" id="BrowseWarrantyParts_tbl">'
  Else
    packet = clip(packet) & p_web.DivHeader('BrowseWarrantyParts_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'')&'" id="BrowseWarrantyParts_tbl">'
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'1','BrowseWarrantyParts',p_web.Translate('Part Number'),,,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'2','BrowseWarrantyParts',p_web.Translate('Description'),,,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'3','BrowseWarrantyParts',p_web.Translate('Quantity'),,,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'4','BrowseWarrantyParts',p_web.Translate('Status'),,,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
  If (p_web.GSV('Hide:ChangeButton') <> 1 AND wpr:Part_Number <> 'EXCH') AND  true ! [A]
      If loc:Selecting = 0
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'5','BrowseWarrantyParts',p_web.Translate(),,,,,,,0,loc:Sorting,'Button')
          do AddPacket
          loc:columns += 1
      End ! Selecting
  End ! Field condition [A]
  If (p_web.GSV('Hide:DeleteButton') = 0) AND  true ! [A]
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'7','BrowseWarrantyParts',p_web.Translate(),,,,,,,0,loc:Sorting,'Button')
          do AddPacket
          loc:columns += 1
  End ! Field condition [A]
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,10,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  If Instring('wpr:record_number',lower(loc:vorder),1,1) = 0 !and WARPARTS{prop:SQLDriver} = 1
    loc:vorder = Choose(loc:vorder='','wpr:Record_Number',clip(loc:vorder) & ',' & 'wpr:Record_Number')
  End
  Loc:Selected = Choose(p_web.IfExistsValue('wpr:Record_Number'),p_web.GetValue('wpr:Record_Number'),p_web.GetSessionValue('wpr:Record_Number'))
  ThisView{prop:order} = p_web.CleanFilter(ThisView,clip(loc:vorder))
      loc:FilterWas = 'wpr:Ref_Number = ' & p_web.GetSessionValue('job:Ref_Number')
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseWarrantyParts',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseWarrantyParts_Filter')
    p_web.SetSessionValue('BrowseWarrantyParts_FirstValue','')
    p_web.SetSessionValue('BrowseWarrantyParts_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,WARPARTS,wpr:RecordNumberKey,loc:PageRows,'BrowseWarrantyParts',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,loc:ViewOptions,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled,Loc:LocatorCase)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or Loc:LocatorType = Net:Date or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position or loc:LocatorType = Net:Date
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If WARPARTS{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(WARPARTS,loc:firstvalue)
              Reset(ThisView,WARPARTS)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If WARPARTS{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(WARPARTS,loc:lastvalue)
            Reset(ThisView,WARPARTS)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      tmp:WARPARTStatus = getPartStatus('W')!Work out the colour
      
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(wpr:Record_Number)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td class="'&p_web.combine(p_web.site.style.BrowseEmpty,)&'">'&p_web.Translate('No Warranty Parts')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:NavButtonPosition=Net:Above or (loc:NavButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseWarrantyParts_nav_a" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseWarrantyParts.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseWarrantyParts.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseWarrantyParts.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseWarrantyParts.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseWarrantyParts_nav_a','buttonset',loc:options)
          End
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:UpdateButtonPosition=Net:Above or (loc:UpdateButtonPosition=Net:Both and loc:found > 0))
    packet = clip(packet) & '<div id="BrowseWarrantyParts_update_a" class="'&p_web.combine(p_web.site.style.BrowseUpdateButtonSet,)&'">'
    If loc:selecting = 0 or loc:popup
      if p_web.GSV('Hide:InsertButton') = 0 and loc:viewOnly = 0
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:InsertButton,'BrowseWarrantyParts',,,loc:FormPopup,'FormWarrantyParts')
          TableQueue.Kind = Net:BeforeTable
          do AddPacket
      End
    End
    If loc:found
          TableQueue.Kind = Net:BeforeTable
          do AddPacket
    End
    packet = clip(packet) & '</div><13,10>'
    If p_web.site.UseUpdateButtonSet
      loc:options = ''
      packet = clip(packet) & p_web.jQuery('#' & 'BrowseWarrantyParts_update_a','buttonset',loc:options)
    End ! If p_web.site.UseUpdateButtonSet
      TableQueue.Kind = Net:BeforeTable
      do AddPacket
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseWarrantyParts',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseWarrantyParts_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseWarrantyParts_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          case loc:locatortype
          of Net:Position
          orof Net:Date
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
          of Net:Contains
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
          of Net:Begins
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
          End
          packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
          Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
          Case Loc:LocatorType
          of Net:Date
            packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator1BrowseWarrantyParts','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('BrowseWarrantyParts_LocatorPic'),,,'onchange="BrowseWarrantyParts.locate(''Locator1BrowseWarrantyParts'',this.value);"',,0,,,) & '</div></td>'
          Else
            packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator1BrowseWarrantyParts',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="BrowseWarrantyParts.locate(''Locator1BrowseWarrantyParts'',this.value);" ',,,,,,) & '</td>'
          End
          If loc:LocatorSearchButton or loc:LocatorClearButton
            packet = clip(packet) & '<td><div id="BrowseWarrantyParts_locate_b" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
            If loc:LocatorSearchButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
            End
            If loc:LocatorClearButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseWarrantyParts.cl(''BrowseWarrantyParts'');')
            End
            packet = clip(packet) & '</div><13,10></td><13,10>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
          If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseWarrantyParts_locate_b','buttonset',loc:options)
          End
      End
    End
  End
  p_web.SetSessionValue('BrowseWarrantyParts_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseWarrantyParts_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:NavButtonPosition=Net:Below or loc:NavButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseWarrantyParts_nav_b" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseWarrantyParts.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseWarrantyParts.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseWarrantyParts.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseWarrantyParts.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseWarrantyParts_nav_b','buttonset',loc:options)
          End
        end
        do SendPacket
  End
  If loc:UpdateButtonPosition=Net:Below or loc:UpdateButtonPosition=Net:Both
  packet = clip(packet) & '<div id="BrowseWarrantyParts_update_b" class="'&p_web.combine(p_web.site.style.BrowseUpdateButtonSet,)&'">'
  If loc:selecting = 0 or loc:popup
    if p_web.GSV('Hide:InsertButton') = 0 and loc:viewOnly = 0
          packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:InsertButton,'BrowseWarrantyParts',,,loc:FormPopup,'FormWarrantyParts')
        do SendPacket
    End
  End
  If loc:found
        do SendPacket
  End
  packet = clip(packet) & '</div><13,10>'
  If p_web.site.UseUpdateButtonSet
    loc:options = ''
    packet = clip(packet) & p_web.jQuery('#' & 'BrowseWarrantyParts_update_b','buttonset',loc:options)
  End ! If p_web.site.UseUpdateButtonSet
    do SendPacket
  End
      IF (p_web.GSV('Hide:InsertButton') = 0)
          packet = clip(packet) & p_web.br
          Packet = clip(Packet) & |
              p_web.CreateButton('button','Adjustment','Adjustment','MainButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormWarrantyParts?&Insert_btn=Insert&adjustment=1')) & ''','''&clip('_self')&''')',,0,,,,,)
          packet = clip(packet) & p_web.br
          Do SendPacket
      END
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
  data
loc:rowclick  string(1024)
  code
    loc:field = p_web.AddBrowseValue('BrowseWarrantyParts','WARPARTS',wpr:RecordNumberKey) !wpr:Record_Number
    p_web._thisrow = p_web._nocolon('wpr:Record_Number')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if Loc:LocatorValue <> '' and loc:ActualSelection = ''
        loc:checked = 'checked'
        do SetSelection
      elsif loc:ActualSelection = '' and wpr:Record_Number = p_web.GetValue('wpr:Record_Number')
         loc:checked = 'checked'
         do SetSelection
      elsif loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseWarrantyParts:LookupField')) = wpr:Record_Number and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      else
        loc:checked = Choose((wpr:Record_Number = loc:selected) and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      end
      If(loc:SelectionMethod  = Net:Radio)
        loc:RowStyle = 'class="' & p_web.combine(p_web.site.style.browserow,) &'"'
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'data-nt-id="'& p_web.AddBrowseValue('BrowseWarrantyParts','WARPARTS',wpr:RecordNumberKey) &'" onclick="'
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If WARPARTS{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(WARPARTS)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            loc:DefaultSelection = loc:field
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If WARPARTS{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(WARPARTS)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            loc:DefaultSelection = loc:field
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'>'&p_web.CreateInput('radio','_bidv_',clip(loc:field),,loc:checked,'')&'</td>'&CRLF
          If loc:DefaultSelection = ''
            loc:DefaultSelection = loc:field
          End
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          If loc:DefaultSelection = '' or loc:direction < 0
            loc:DefaultSelection = loc:field
          End
        End
    end ! loc:eip = 0
      !Show hide Buttons
      error# = 0
      if (wpr:Status = 'RET' or wpr:Status = 'RTS')
          if (securityCheckFailed(p_web.GSV('BookingUserPassword'),'JOB - DELETE PART AW RETURN'))
              p_web.SSV('Hide:DeleteButton',1)
          end ! if (securityCheckFailed(p_web.GSV('BookingUserPassword'),'JOB - DELETE PART AW RETURN'))
      end ! if (wpr:Status = 'RET' or wpr:Status = 'RTS')
      
      if (p_web.GSV('Hide:DeleteButton') = 0)
          Access:USERS.Clearkey(use:user_Code_Key)
          use:user_code    = p_web.GSV('job:Engineer')
          if (Access:USERS.TryFetch(use:user_Code_Key) = Level:Benign)
              ! Found
              ! The Site That Attached The Part Must Remove The Part
              if (p_web.GSV('BookingSite') = 'RRC')
                  if (use:location = p_web.GSV('ARC:SiteLocation'))
                      p_web.SSV('Hide:DeleteButton',1)
                  end ! if (use:location = p_web.GSV('ARC:SiteLocation'))
              else ! if (p_web.GSV('BookingSite') = 'RRC')
                  if (use:location <> p_web.GSV('ARC:SiteLocation'))
                      p_web.SSV('Hide:DeleteButton',1)
                  end ! if (use:location <> p_web.GSV('ARC:SiteLocation'))
              end ! if (p_web.GSV('BookingSite') = 'RRC')
          else ! if (Access:USERS.TryFetch(use:user_Code_Key) = Level:Benign)
              ! Error
              p_web.SSV('Hide:DeleteButton',1)
          end ! if (Access:USERS.TryFetch(use:user_Code_Key) = Level:Benign)
      end ! if (error# = 0)
      
      !if (p_web.GSV('Hide:DeleteButton') = 0)
      !    if (wpr:Part_Number = 'EXCH')
      !        ! Can't delete exchange button
      !        p_web.SSV('Hide:DeleteButton',1)
      !    end ! if (wpr:Part_Number = 'EXCH')
      !end ! if (error# = 0)
      
      if (p_web.GSV('Hide:DeleteButton') = 0)
          ! Job Complete, don't let delete.
          if (p_web.GSV('job:Date_Completed') > 0)
              p_web.SSV('Hide:DeleteButton',1)
          end ! if (p_web.GSV('job:Date_Completed') > 0)
      
          if (p_web.GSV('job:Invoice_Number_Warranty') > 0)
              p_web.SSV('Hide:DeleteButton',1)
          end ! if (p_web.GSV('job:Invoice_Number_Warranty') > 0)
      end ! if (error# = 0)
      
          If Loc:Eip = 0
              packet = clip(packet) & '<td width="'&clip(100)&'"><13,10>'
          end ! loc:eip = 0
          do value::wpr:Part_Number
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td width="'&clip(200)&'"><13,10>'
          end ! loc:eip = 0
          do value::wpr:Description
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
            if false
            elsif wpr:Correction = 1
              packet = clip(packet) & '<td class="'&p_web.combine('CenterJustify',)&'"><13,10>'
            else
              packet = clip(packet) & '<td class="'&p_web.combine('CenterJustify')&'"><13,10>'
            end  !False
          end ! loc:eip = 0
          do value::wpr:Quantity
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
            if false
            elsif tmp:WARPARTStatus = 'Requested'
              packet = clip(packet) & '<td class="'&p_web.combine('GreenRegular')&'"><13,10>'
            elsif tmp:WARPARTStatus = 'Picked'
              packet = clip(packet) & '<td class="'&p_web.combine('BlueRegular')&'"><13,10>'
            elsif tmp:WARPARTStatus = 'On Order'
              packet = clip(packet) & '<td class="'&p_web.combine('PurpleRegular')&'"><13,10>'
            elsif tmp:WARPARTStatus = 'Awaiting Picking'
              packet = clip(packet) & '<td class="'&p_web.combine('PinkRegular')&'"><13,10>'
            elsif tmp:WARPARTStatus = 'Awaiting Return'
              packet = clip(packet) & '<td class="'&p_web.combine('OrangeRegular')&'"><13,10>'
            else
              packet = clip(packet) & '<td><13,10>'
            end  !False
          end ! loc:eip = 0
          do value::tmp:WARPARTStatus
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      If (p_web.GSV('Hide:ChangeButton') <> 1 AND wpr:Part_Number <> 'EXCH') AND  true
        If Loc:Selecting = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::Edit
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        End     !Selecting
      End ! Field Condition
      If (p_web.GSV('Hide:DeleteButton') = 0) AND  true
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::MyDelete
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      End ! Field Condition
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr ' & clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Idx = p_web.AddBrowseValue('BrowseWarrantyParts','WARPARTS',wpr:RecordNumberKey)
  TableQueue.Id[1] = wpr:Record_Number

ClosingScripts  Routine
  data
Rtn_SecwinProcedureName         string(252)
  code
  If p_web.RequestAjax = 0
    do SetFormAction
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var btiBrowseWarrantyParts;if (btiBrowseWarrantyParts != 1){{var BrowseWarrantyParts=new browseTable(''BrowseWarrantyParts'','''&clip(loc:formname)&|
      ''','''&p_web._jsok('wpr:Record_Number',Net:Parameter)&''',2,'''&clip(loc:divname)&''',1,1,1,'''&|
      clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&|
      p_web.Translate('Are you sure you want to delete this record?')&''','''&clip(loc:ActualSelection)&''','''&clip(loc:selectaction)&''','''&|
      clip(loc:formactiontarget)&''',''FormWarrantyParts'','&loc:popup&',0,'''','''',"' & clip(Rtn_SecwinProcedureName) & '");<13,10>'&|
      'BrowseWarrantyParts.setGreenBar('''&p_web.ColorWeb(p_web.Site.Style.BrowseHighlightColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOneColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseTwoColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOverColor)&''');<13,10>' &|
      'BrowseWarrantyParts.applyGreenBar();btiBrowseWarrantyParts=1};<13,10>' &|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseWarrantyParts')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseWarrantyParts')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseWarrantyParts')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseWarrantyParts')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(WARPARTS)
  p_web._CloseFile(STOCK)
  p_web._CloseFile(USERS)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(WARPARTS)
  Bind(wpr:Record)
  Clear(wpr:Record)
  NetWebSetSessionPics(p_web,WARPARTS)
  p_web._OpenFile(STOCK)
  Bind(sto:Record)
  NetWebSetSessionPics(p_web,STOCK)
  p_web._OpenFile(USERS)
  Bind(use:Record)
  NetWebSetSessionPics(p_web,USERS)

Children Routine
  if loc:selecting = 0
    If p_web.RequestAjax = 0
      do StartChildren
    Else
      do AjaxChildren
    End
  end

AjaxChildren  Routine
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue('wpr:Record_Number',p_web.GetValue('wpr:Record_Number'))
   p_web.DeleteValue('_Clicked')
   loc:found = 1
   do Children
! ----------------------------------------------------------------------------------------
CallRow  Routine
  data
loc:result  long
  code
  do OpenFilesB
  wpr:Record_Number = p_web.GSV('wpr:Record_Number')
  loc:result = p_web._GetFile(WARPARTS,wpr:RecordNumberKey)
  loc:eip = 1
  loc:viewstate = p_web.escape(p_web.Base64Encode(clip(wpr:Record_Number)))
  do BrowseRow
  do SendPacket
  do ClosefilesB

! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(WARPARTS)
  Case upper(p_web.GetValue('_EIPClm'))
  of upper('MyDelete')
    do Validate::MyDelete
  Else
    case p_web.GetValue('_action_')
    of Net:InsertRecord
    orof Net:ChangeRecord
    orof Net:DeleteRecord
    orof Net:CopyRecord
      loc:eip = 0
      do CallBrowse
    end
  End
  do GotFocusBack
  p_web._CloseFile(WARPARTS)
! ----------------------------------------------------------------------------------------
value::wpr:Part_Number   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseWarrantyParts_wpr:Part_Number_'&wpr:Record_Number,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(wpr:Part_Number,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::wpr:Description   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseWarrantyParts_wpr:Description_'&wpr:Record_Number,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(wpr:Description,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::wpr:Quantity   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    elsif wpr:Correction = 1 ! alternative condition for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseWarrantyParts_wpr:Quantity_'&wpr:Record_Number,p_web.combine('CenterJustify',),net:crc,,loc:extra)
              packet = clip(packet) &  p_web.CreateHyperLink(Left(Format('COR','@s8')),,,,loc:javascript,,,0)
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseWarrantyParts_wpr:Quantity_'&wpr:Record_Number,'CenterJustify',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(wpr:Quantity,'@s8')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::tmp:WARPARTStatus   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    elsif tmp:WARPARTStatus = 'Requested' ! alternative condition for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseWarrantyParts_tmp:WARPARTStatus_'&wpr:Record_Number,,net:crc,,loc:extra)
              packet = clip(packet) &  p_web.CreateHyperLink(Left(Format(tmp:WARPARTStatus,'@s20')),,,,loc:javascript,,,0)
    elsif tmp:WARPARTStatus = 'Picked' ! alternative condition for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseWarrantyParts_tmp:WARPARTStatus_'&wpr:Record_Number,,net:crc,,loc:extra)
              packet = clip(packet) &  p_web.CreateHyperLink(Left(Format(tmp:WARPARTStatus,'@s20')),,,,loc:javascript,,,0)
    elsif tmp:WARPARTStatus = 'On Order' ! alternative condition for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseWarrantyParts_tmp:WARPARTStatus_'&wpr:Record_Number,,net:crc,,loc:extra)
              packet = clip(packet) &  p_web.CreateHyperLink(Left(Format(tmp:WARPARTStatus,'@s20')),,,,loc:javascript,,,0)
    elsif tmp:WARPARTStatus = 'Awaiting Picking' ! alternative condition for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseWarrantyParts_tmp:WARPARTStatus_'&wpr:Record_Number,,net:crc,,loc:extra)
              packet = clip(packet) &  p_web.CreateHyperLink(Left(Format(tmp:WARPARTStatus,'@s20')),,,,loc:javascript,,,0)
    elsif tmp:WARPARTStatus = 'Awaiting Return' ! alternative condition for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseWarrantyParts_tmp:WARPARTStatus_'&wpr:Record_Number,,net:crc,,loc:extra)
              packet = clip(packet) &  p_web.CreateHyperLink(Left(Format(tmp:WARPARTStatus,'@s20')),,,,loc:javascript,,,0)
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseWarrantyParts_tmp:WARPARTStatus_'&wpr:Record_Number,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(tmp:WARPARTStatus,'@s20')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::Edit   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
  If (p_web.GSV('Hide:ChangeButton') <> 1 AND wpr:Part_Number <> 'EXCH')
    p_web.site.SmallChangeButton.TextValue = p_web.Translate('Edit')
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseWarrantyParts_Edit_'&wpr:Record_Number,,net:crc,,loc:extra)
          If loc:viewonly = 0
             packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallChangeButton,'BrowseWarrantyParts',p_web.AddBrowseValue('BrowseWarrantyParts','WARPARTS',wpr:RecordNumberKey),,loc:FormPopup,'FormWarrantyParts') & '<13,10>'
          End
    End
    p_web.site.SmallChangeButton = p_web.RequestData.WebServer._SitesQueue.Defaults.SmallChangeButton  !bruce
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
  End
! ----------------------------------------------------------------------------------------
Validate::MyDelete  Routine
  data
loc:was                    Any
loc:result                 Long
loc:ok                     Long
loc:lookupdone             Long
  code
  do OpenFilesB
  loc:ViewState = p_web.GetValue('ViewState')
  wpr:Record_Number = p_web.Base64Decode(p_web._Unescape(loc:ViewState,Net:NoPlus))
  loc:result = p_web._GetFile(WARPARTS,wpr:RecordNumberKey)
  p_web.FileToSessionQueue(WARPARTS)
  do ClosefilesB
! ----------------------------------------------------------------------------------------
value::MyDelete   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
  If (p_web.GSV('Hide:DeleteButton') = 0)
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseWarrantyParts_MyDelete_'&wpr:Record_Number,,net:crc,,loc:extra)
      loc:disabled = ''
      packet = clip(packet) & p_web.CreateButton('button','Delete','Delete',p_web.combine(Choose('Delete' <> '',p_web.site.style.BrowseOtherButtonWithText,p_web.site.style.BrowseOtherButtonWithOutText),'SmallButtonIcon'),loc:formname,,,p_web.WindowOpen('FormDeletePart&_bidv_=' & p_web.AddBrowseValue('BrowseWarrantyParts','WARPARTS',wpr:RecordNumberKey) &'&' & p_web._jsok('FromURL=ViewJob&DelType=WAR&ReturnURL=ViewJob') &''&'','_self'),,loc:disabled,,,,,,0,,,,'nt-left') !e
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
  End
OpenFiles  ROUTINE
  p_web._OpenFile(STOCK)
  p_web._OpenFile(USERS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(STOCK)
  p_Web._CloseFile(USERS)
     FilesOpened = False
  END
  return
!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
CheckForDuplicate  Routine
SetSelection  Routine
  loc:ActualSelection = loc:field
  p_web.SetSessionValue('wpr:Record_Number',wpr:Record_Number)

MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:ActualSelection = ''
    loc:ActualSelection = loc:DefaultSelection
    p_web.GetBrowseValue(loc:ActualSelection,Net:Web:SessionQueue) ! so children are primed with correct sessionValue
  End

  If loc:ActualSelection <> ''
    TableQueue.Kind = Net:RowTable
    get(TableQueue,TableQueue.Kind)
    if Errorcode() = 0
      TableQueue.Row = sub(TableQueue.Row,1,len(clip(TableQueue.Row))-1) &  ' data-nt-rows="'&loc:RowsHigh&'" data-nt-value="'&clip(loc:ActualSelection)&'"' & '>'
      Put(TableQueue)
    End
  End

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead class="'&p_web.combine(p_web.site.style.BrowseHeader,'')&'"><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot class="'&p_web.combine(p_web.site.style.BrowseFooter,)&'"><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody class="'&p_web.combine(p_web.site.style.BrowseBody,)&'"><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table><13,10>' & p_web.DivFooter(Net:NoSend) ! Table Div
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
