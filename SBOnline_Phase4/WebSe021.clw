

   MEMBER('WebServer_Phase4.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSE021.INC'),ONCE        !Local module procedure declarations
                     END


ForceAuthorityNumber PROCEDURE  (func:Type)                ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
  CODE
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    !Authority Number
    If (def:Force_Authority_Number = 'B' And func:Type = 'B') Or |
        (def:Force_Authority_Number <> 'I' and func:Type = 'C')
        Return Level:Fatal
    End!If def:Force_Authority_Number = 'B'
    Return Level:Benign
ForceAccessories     PROCEDURE  (func:Type)                ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
  CODE
    If (Clip(GETINI('COMPULSORY','JobAccessories',,CLIP(PATH())&'\SB2KDEF.INI')) = 'B' And func:Type = 'B') Or |
        (Clip(GETINI('COMPULSORY','JobAccessories',,CLIP(PATH())&'\SB2KDEF.INI')) <> '0' And func:Type = 'C')
        Return Level:Fatal
    End !Clip(GETINI('COMPULSORY','JobAccessories',,CLIP(PATH())&'\SB2KDEF.INI')) <> '0' And func:Type = 'C')
    Return Level:Benign
CheckParts           PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
locReturnValue       BYTE                                  !
FilesOpened     BYTE(0)
  CODE
    p_web.SSV('CheckParts:ReturnValue',0)

    do openFiles

    do CheckParts

    do closeFiles

    p_web.SSV('CheckParts:ReturnValue',locReturnValue)
checkParts    routine
    Data
tmp:FoundReceived   Byte()
    Code
!Return (1) = Pending Order
!Return (2) = On Order
!Return (3) = Back Order Spares
!Return (4) = Parts Recevied
!Return (0) = Received Order Or No Parts With Anything To Do WIth Orders
    tmp:FoundReceived = False
    locReturnValue = 0
    Case p_web.GSV('CheckParts:Type')
        Of 'C'
            If p_web.GSV('job:Chargeable_Job') = 'YES'
                access:parts.clearkey(par:part_number_key)
                par:ref_number  = p_web.GSV('job:ref_number')
                set(par:part_number_key,par:part_number_key)
                loop
                    if access:parts.next()
                       break
                    end !if
                    if par:ref_number  <> p_web.GSV('job:ref_number')      |
                        then break.  ! end if
    !Start - If web order mark as Spares Requested, unless there are non in stock, and non in Main Store - TrkBs: 4625 (DBH: 18-08-2004 49)
                    If par:WebOrder = True
                        Access:STOCK.ClearKey(sto:Ref_Number_Key)
                        sto:Ref_Number = par:Part_Ref_Number
                        If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                            !Found
                            If sto:Quantity_Stock > 0
                                locReturnValue = 1
                                exit
                            End !If sto:Quantity_Stock > 1
                        Else !If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                            !Error
                        End !If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                        Access:STOCK.ClearKey(sto:Location_Key)
                        sto:Location    = p_web.GSV('ARC:SiteLocation')
                        sto:Part_Number = par:Part_Number
                        If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                            !Found
                            !Make '340 BACK ORDER SPARES' if one, or less parts in main store - TrkBs: 4625 (DBH: 14-02-2005)
                            If sto:Quantity_Stock < 2
                                locReturnValue = 3
                                exit
                            End !IF sto:Quantity < 1
                        Else !If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                            !Error
                        End !If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                        locReturnValue = 1
                        exit
                    End !If par:WebOrder = True
    !End   - If web order mark as Spares Requested, unless there are non in stock, and non in Main Store - TrkBs: 4625 (DBH: 18-08-2004 49)
                    If par:pending_ref_number <> '' and par:Order_Number = ''
                        locReturnValue = 1
                        exit
                    End
                    If par:order_number <> ''
                        If par:date_received = ''
                            locReturnValue = 2
                            exit

                        Else
                            tmp:FoundReceived = True
                        End!If par:date_received = ''
                    End!If par:order_number <> ''
                end !loop
            End ! If p_web.GSV('job:Chargeable_Job = 'YES'

        Of 'W'
            If p_web.GSV('job:Warranty_Job') = 'YES'
                access:warparts.clearkey(wpr:part_number_key)
                wpr:ref_number  = p_web.GSV('job:ref_number')
                set(wpr:part_number_key,wpr:part_number_key)
                loop
                    if access:warparts.next()
                       break
                    end !if
                    if wpr:ref_number  <> p_web.GSV('job:ref_number')      |
                        then break.  ! end if
    !Start - If web order mark as Spares Requested, unless there are non in stock, and non in Main Store - TrkBs: 4625 (DBH: 18-08-2004 49)
                    If wpr:WebOrder = True
                        Access:STOCK.ClearKey(sto:Ref_Number_Key)
                        sto:Ref_Number = wpr:Part_Ref_Number
                        If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                            !Found
                            If sto:Quantity_Stock > 0
                                locReturnValue = 1
                                exit
                            End !If sto:Quantity_Stock > 1
                        Else !If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                            !Error
                        End !If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                        Access:STOCK.ClearKey(sto:Location_Key)
                        sto:Location    = p_web.GSV('ARC:SiteLocation')
                        sto:Part_Number = wpr:Part_Number
                        If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                            !Found
                            !Make '340 BACK ORDER SPARES' if one, or less parts in main store - TrkBs: 4625 (DBH: 14-02-2005)
                            If sto:Quantity_Stock < 2
                                locReturnValue = 3
                                exit
                            End !IF sto:Quantity < 1
                        Else !If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                            !Error
                        End !If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                        locReturnValue = 1
                        exit
                    End !If par:WebOrder = True
    !End   - If web order mark as Spares Requested, unless there are non in stock, and non in Main Store - TrkBs: 4625 (DBH: 18-08-2004 49)

                    If wpr:pending_ref_number <> '' and wpr:Order_Number = ''
                        locReturnValue = 1
                        exit
                    End
                    If wpr:order_number <> ''
                        If wpr:date_received = ''
                            locReturnValue = 2
                            exit
                        Else!If wpr:date_recieved = ''
                            tmp:FoundReceived = True
                        End!If wpr:date_recieved = ''
                    End!If wpr:order_number <> ''
                end !loop

            End ! If p_web.GSV('job:Warranty_Job = 'YES'

    End!Case f_type

    If tmp:FoundReceived = True
        !All parts received - TrkBs: 4625 (DBH: 17-02-2005)
        locReturnValue = 4
        exit
    Else ! If tmp:FoundReceived = True
        !No parts attached - TrkBs: 4625 (DBH: 17-02-2005)
        locReturnValue = 0
    End ! If tmp:FoundReceived = True


!--------------------------------------
OpenFiles  ROUTINE
  Access:PARTS.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:PARTS.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:WARPARTS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:WARPARTS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:STOCK.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:STOCK.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:PARTS.Close
     Access:WARPARTS.Close
     Access:STOCK.Close
     FilesOpened = False
  END
RapidLocation        PROCEDURE  (STRING fLocation)         ! Declare Procedure
LOCATION::State  USHORT
FilesOpened     BYTE(0)
  CODE
    RETURN vod.IsRapidLocation(fLocation)
!--------------------------------------
OpenFiles  ROUTINE
  Access:LOCATION.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:LOCATION.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:LOCATION.Close
     FilesOpened = False
  END
SaveFiles  ROUTINE
  LOCATION::State = Access:LOCATION.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF LOCATION::State <> 0
    Access:LOCATION.RestoreFile(LOCATION::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
getPartStatus        PROCEDURE  (fType)                    ! Declare Procedure
  CODE
!Assume in part, or warpart record
    colourFlag# = 0

    case fType
    of 'C' ! Chargeable part

        Access:STOCK.Clearkey(sto:Ref_Number_Key)
        sto:Ref_Number  = par:Part_Ref_Number
        If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
            !Found
        !    tmp:CShelfLocation  = Clip(sto:Shelf_Location) & ' / ' & Clip(sto:Second_Location)
        Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
            !Error
        !    tmp:CShelfLocation  = ''
        End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign


        If par:Part_Number = 'EXCH'
            Case par:Status
                Of 'ORD'    !Order
                    colourFlag# = 6
                Of 'REQ'    !Allocate
                    colourFlag# = 1
                Of 'RTS'
                    colourFlag# = 7
            End !Case par:Status
        Else !par:Part_Number = 'EXCH'
            If par:WebOrder = 1
                colourFlag# = 6
            Else

                If par:PartAllocated Or ~(rapidLocation(sto:Location) And sto:Location <> '')
                    If par:pending_ref_number <> '' And par:order_number = '' And par:date_ordered = ''
                        colourFlag# = 1 !Green
                    End !If par:pending_ref_number <> '' And par:order_number = '' And par:date_ordered = ''

                    If par:order_number <> '' And par:date_received =''
                        colourFlag# = 3 !Red
                    End !If par:order_number <> '' And par:date_received =''

                    If par:Order_Number <> '' And par:Date_Received <> ''
                        colourFlag# = 4 !Gray
                    End !If par:Order_Number <> '' And par:Date_Received <> ''
                Else !par:PartAllocated
                    If par:Status = ''
                        colourFlag# = 1 !Green
                    End !If par:Status = ''

                    Case par:Status
                        Of 'PRO'
                            colourFlag# = 5 !Yellow
                        Of 'PIK'
                            colourFlag# = 2 !Navy
                        Of 'RET'
                            colourFlag# = 7 !Pink?
                    End !Case par:Status
                End !par:PartAllocated

            End !par:WebOrder = 1
        End !par:Part_Number = 'EXCH'

    of 'W'
        Access:STOCK.Clearkey(sto:Ref_Number_Key)
        sto:Ref_Number  = wpr:Part_Ref_Number
        If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
            !Found
        !    tmp:CShelfLocation  = Clip(sto:Shelf_Location) & ' / ' & Clip(sto:Second_Location)
        Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
            !Error
        !    tmp:CShelfLocation  = ''
        End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign


        If wpr:Part_Number = 'EXCH'
            Case wpr:Status
                Of 'ORD'    !Order
                    colourFlag# = 6
                Of 'REQ'    !Allocate
                    colourFlag# = 1
                Of 'RTS'
                    colourFlag# = 7
            End !Case wpr:Status
        Else !wpr:Part_Number = 'EXCH'
            If wpr:WebOrder = 1
                colourFlag# = 6
            Else

                If wpr:PartAllocated Or ~(rapidLocation(sto:Location) And sto:Location <> '')
                    If wpr:pending_ref_number <> '' And wpr:order_number = '' And wpr:date_ordered = ''
                        colourFlag# = 1 !Green
                    End !If wpr:pending_ref_number <> '' And wpr:order_number = '' And wpr:date_ordered = ''

                    If wpr:order_number <> '' And wpr:date_received =''
                        colourFlag# = 3 !Red
                    End !If wpr:order_number <> '' And wpr:date_received =''

                    If wpr:Order_Number <> '' And wpr:Date_Received <> ''
                        colourFlag# = 4 !Gray
                    End !If wpr:Order_Number <> '' And wpr:Date_Received <> ''
                Else !wpr:PartAllocated
                    If wpr:Status = ''
                        colourFlag# = 1 !Green
                    End !If wpr:Status = ''

                    Case wpr:Status
                        Of 'PRO'
                            colourFlag# = 5 !Yellow
                        Of 'PIK'
                            colourFlag# = 2 !Navy
                        Of 'RET'
                            colourFlag# = 7 !Pink?
                    End !Case wpr:Status
                End !wpr:PartAllocated

            End !wpr:WebOrder = 1
        End !wpr:Part_Number = 'EXCH'


    !1 = green
    !2 = navy
    !3 = red
    !4 = gray
    !5 = fuschia
    !6 = purple
    !7 = 08080FFH

    end ! case fType


    case colourFlag#
    of 1
        return 'Requested'
    of 2
        return 'Picked'
    of 3
        return 'On Order'
    of 4
        !tmp:partStatus = 'Requested'
    of 5
        return 'Awaiting Picking'
    of 6
        return 'On Order'
    of 7
        return 'Awaiting Return'
    else
        return 'Allocated'
    end ! case colourFlag#
    return ''
