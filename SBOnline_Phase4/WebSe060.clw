

   MEMBER('WebServer_Phase4.clw')                          ! This is a MEMBER module


   INCLUDE('ABPRPDF.INC'),ONCE
   INCLUDE('ABRPPSEL.INC'),ONCE
   INCLUDE('ABWMFPAR.INC'),ONCE

                     MAP
                       INCLUDE('WEBSE060.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSE061.INC'),ONCE        !Req'd for module callout resolution
                     END


ClearSBOPartsList    PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
FilesOpened     BYTE(0)
  CODE
    DO OpenFiles
    Access:SBO_OutParts.Clearkey(sout:PartNumberKey)
    sout:SessionID = p_web.SessionID
    SET(sout:PartNumberKey,sout:PartNumberKey)
    LOOP UNTIL Access:SBO_OutParts.Next()
        IF (sout:SessionID <> p_web.SessionID)
            BREAK
        END

        Access:SBO_OutParts.DeleteRecord(0)
    END

    DO CloseFiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:SBO_OutParts.Open                                 ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:SBO_OutParts.UseFile                              ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:SBO_OutParts.Close
     FilesOpened = False
  END

Goods_Received_Note_Retail PROCEDURE(<NetWebServerWorker p_web>)
Default_Invoice_Company_Name_Temp STRING(30)
tmp:RecordsCount     LONG
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
Default_Invoice_Address_Line1_Temp STRING(30)
Default_Invoice_Address_Line2_Temp STRING(30)
Default_Invoice_Address_Line3_Temp STRING(30)
Default_Invoice_Postcode_Temp STRING(15)
Default_Invoice_Telephone_Number_Temp STRING(15)
Default_Invoice_Fax_Number_Temp STRING(15)
Default_Invoice_VAT_Number_Temp STRING(30)
RejectRecord         LONG,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:PrintedBy        STRING(60)
first_page_temp      BYTE(1)
pos                  STRING(255)
Part_Queue           QUEUE,PRE()
Order_Number_Temp    REAL
Part_Number_Temp     STRING(30)
Description_Temp     STRING(30)
Quantity_Temp        LONG
Purchase_cost_temp   REAL
Sale_Cost_temp       REAL
                     END
Order_Temp           REAL
Address_Line1_Temp   STRING(30)
Address_Line2_Temp   STRING(30)
Address_Line3_Temp   STRING(30)
Address_Line4_Temp   STRING(30)
Total_Quantity_Temp  LONG
total_cost_temp      REAL
total_cost_total_temp REAL
Total_Lines_Temp     REAL
user_name_temp       STRING(22)
no_temp              STRING('NO')
tmp:Order_No_Filter  REAL
tmp:Date_Received_Filter DATE
Parts_Q              QUEUE,PRE(pq)
PartNo               STRING(30)
Description          STRING(30)
QtyOrdered           REAL
QtyReceived          LONG
Location             STRING(30)
Cost                 REAL
OrderNumber          LONG
LineCost             REAL
                     END
LOC:SaveToQueue      PrintPreviewFileQueue
!-----------------------------------------------------------------------------
Process:View         VIEW(RETSALES)
                       PROJECT(ret:Date_Despatched)
                       PROJECT(ret:Ref_Number)
                     END
Report               REPORT('Goods Received Note'),AT(396,4604,7521,4125),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,479,7521,4167),USE(?unnamed)
                         STRING(@D6b),AT(5844,760),USE(ReportRunDate),TRN,LEFT,FONT(,8,,)
                         STRING('Date Printed:'),AT(5083,760),USE(?RunPrompt),TRN,FONT(,8,,)
                         STRING(@T3),AT(6521,760),USE(ReportRunTime),TRN,LEFT,FONT(,8,,)
                         STRING(@s30),AT(156,323,3135,201),USE(tra:Address_Line1,,?tra:Address_Line1:2),TRN
                         STRING(@s30),AT(156,104,4156,263),USE(tra:Company_Name,,?tra:Company_Name:2),TRN,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(156,635,3135,201),USE(tra:Address_Line3,,?tra:Address_Line3:2),TRN
                         STRING('REPRINT'),AT(3385,677),USE(?Reprint),TRN,HIDE,FONT(,12,,FONT:bold)
                         STRING(@s30),AT(156,802,3135,201),USE(tra:Postcode,,?tra:Postcode:2),TRN
                         STRING('Tel: '),AT(156,958),USE(?String15),TRN
                         STRING('Fax:'),AT(156,1094),USE(?String16),TRN
                         STRING(@s30),AT(573,1094,2495,201),USE(tra:Fax_Number,,?tra:Fax_Number:2),TRN
                         STRING('Email:'),AT(156,1250,521,156),USE(?String16:2),TRN
                         STRING(@s255),AT(573,1250,2500,201),USE(tra:EmailAddress),TRN
                         STRING(@s30),AT(156,1667),USE(def:OrderCompanyName,,?def:OrderCompanyName:2),TRN,FONT(,8,,)
                         STRING(@s30),AT(4083,1667),USE(tra:Company_Name),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,1823),USE(def:OrderAddressLine1,,?def:OrderAddressLine1:2),TRN,FONT(,8,,)
                         STRING(@s30),AT(4083,1823),USE(tra:Address_Line1),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,1979),USE(def:OrderAddressLine2,,?def:OrderAddressLine2:2),TRN,FONT(,8,,)
                         STRING(@s30),AT(4083,1979),USE(tra:Address_Line2),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,2135),USE(def:OrderAddressLine3,,?def:OrderAddressLine3:2),TRN,FONT(,8,,)
                         STRING(@s30),AT(4063,2135),USE(tra:Address_Line3),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,2292),USE(def:OrderPostcode,,?def:OrderPostcode:2),TRN,FONT(,8,,)
                         STRING(@s15),AT(4083,2292),USE(tra:Postcode),TRN,FONT(,8,,)
                         STRING('Tel:'),AT(156,2448),USE(?String26),TRN,FONT(,8,,)
                         STRING(@s15),AT(521,2448),USE(def:OrderTelephoneNumber,,?def:OrderTelephoneNumber:3),TRN,FONT(,8,,)
                         STRING('Tel:'),AT(4083,2448),USE(?String26:2),TRN,FONT(,8,,)
                         STRING(@s15),AT(4396,2448,990,188),USE(tra:Telephone_Number),TRN,FONT(,8,,)
                         STRING('Fax:'),AT(156,2604),USE(?String28),TRN,FONT(,8,,)
                         STRING(@s30),AT(521,2604,990,188),USE(def:OrderFaxNumber,,?def:OrderFaxNumber:2),TRN,FONT(,8,,)
                         STRING('Fax:'),AT(4083,2604),USE(?String28:2),TRN,FONT(,8,,)
                         STRING(@s15),AT(4396,2604,990,188),USE(tra:Fax_Number),TRN,FONT(,8,,)
                         STRING(@s15),AT(156,3385),USE(tra:Account_Number),TRN,FONT(,8,,)
                         STRING(@s8),AT(1615,3385),USE(ret:Ref_Number),TRN,FONT(,8,,FONT:bold)
                         STRING(@d6b),AT(3083,3385),USE(ret:Date_Despatched),TRN,FONT(,8,,)
                         STRING(@s20),AT(5844,938,1406,156),USE(tmp:PrintedBy),TRN,FONT(,8,,)
                         STRING(@s30),AT(5990,3385,1563,208),USE(def:OrderTelephoneNumber,,?def:OrderTelephoneNumber:2),TRN,FONT(,8,,)
                         STRING('RRC GOODS RECEIVED NOTE'),AT(4396,104,2917,313),USE(?String19),TRN,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(573,958,2495,201),USE(tra:Telephone_Number,,?tra:Telephone_Number:2),TRN
                         STRING('Printed by'),AT(5083,938),USE(?String67),TRN,FONT(,8,COLOR:Black,,CHARSET:ANSI)
                         STRING(@s30),AT(156,479,3135,201),USE(tra:Address_Line2,,?tra:Address_Line2:2),TRN
                       END
EndOfReportBreak       BREAK(EndOfReport),USE(?BREAK1)
DETAIL                   DETAIL,AT(,,,198),USE(?DetailBand)
                           STRING(@s8),AT(125,0),USE(pq:QtyOrdered),TRN,RIGHT,FONT(,7,COLOR:Black,)
                           STRING(@s30),AT(1833,0),USE(pq:PartNo),TRN,FONT(,7,COLOR:Black,)
                           STRING(@s30),AT(3333,0),USE(pq:Description),TRN,FONT(,7,COLOR:Black,)
                           STRING(@s8),AT(833,0),USE(pq:QtyReceived),TRN,RIGHT,FONT(,7,COLOR:Black,)
                           STRING(@s30),AT(5052,0),USE(pq:Location),TRN,LEFT,FONT(,7,COLOR:Black,)
                           STRING(@n14.2),AT(6719,0),USE(total_cost_temp),TRN,FONT(,7,COLOR:Black,,CHARSET:ANSI)
                         END
detail1                  DETAIL,PAGEAFTER(-1),AT(,,,42),USE(?detail1),ABSOLUTE
                         END
Totals                   DETAIL,AT(396,9396),USE(?Totals),ABSOLUTE
                           STRING('Total Items: '),AT(396,83),USE(?String47),TRN,FONT(,10,,FONT:bold)
                           STRING('Total Lines: '),AT(396,323),USE(?String40),TRN,FONT(,,,FONT:bold)
                           STRING(@p<<<<<<<#p),AT(1323,323,773,201),USE(Total_Lines_Temp),TRN,RIGHT,FONT(,10,,FONT:bold)
                           STRING(@n14.2),AT(6156,156),USE(total_cost_total_temp),TRN,RIGHT,FONT(,8,,FONT:bold)
                           STRING(@p<<<<<<<#p),AT(1323,83,773,201),USE(Total_Quantity_Temp),TRN,RIGHT,FONT(,10,,FONT:bold)
                           STRING('Total GRN Value:'),AT(4844,156),USE(?String41),TRN,FONT(,,,FONT:bold)
                         END
Continue                 DETAIL,AT(396,9396),USE(?Continue),ABSOLUTE
                           STRING('Number of Items On Page : 20'),AT(313,104),USE(?String60),TRN,FONT(,,,FONT:bold)
                           STRING('Continued Over -'),AT(5990,104),USE(?String60:2),TRN,FONT(,,,FONT:bold)
                         END
detailExchange           DETAIL,AT(,,,188),USE(?detailExchange)
                           STRING(@n_8),AT(156,0),USE(pq:OrderNumber),TRN,RIGHT(1),FONT(,7,,,CHARSET:ANSI)
                           STRING(@n_8),AT(833,0),USE(pq:QtyReceived,,?pq:QtyReceived:2),TRN,FONT(,7,,,CHARSET:ANSI)
                           STRING(@s60),AT(1354,0,2917,156),USE(pq:PartNo,,?pq:PartNo:2),TRN,FONT(,7,,,CHARSET:ANSI)
                           STRING(@s30),AT(4427,0,1406,156),USE(pq:Description,,?pq:Description:2),TRN,FONT(,7,,,CHARSET:ANSI)
                           STRING(@n14.2),AT(5990,0),USE(pq:Cost),TRN,RIGHT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@n10.2),AT(6823,0),USE(pq:LineCost),TRN,RIGHT,FONT(,7,,,CHARSET:ANSI)
                         END
                       END
                       FOOTER,AT(396,10156,7521,1125),USE(?FOOTER1)
                         TEXT,AT(104,521,7344,521),USE(stt:Text),FONT(,8,,)
                       END
                       FORM,AT(396,479,7521,10802),USE(?unnamed:2)
                         IMAGE('RINVDET.GIF'),AT(0,0,7521,11156),USE(?Image1)
                         STRING('Sale Number'),AT(1604,3177),USE(?String32),TRN,FONT(,8,,FONT:bold)
                         STRING('Sales Date'),AT(3083,3177),USE(?String44),TRN,FONT(,8,,FONT:bold)
                         STRING('Contact'),AT(4531,3177),USE(?String45),TRN,FONT(,8,,FONT:bold)
                         STRING('Contact Number'),AT(5990,3177),USE(?String58),TRN,FONT(,8,,FONT:bold)
                         STRING('Account Number'),AT(125,3177),USE(?String30),TRN,FONT(,8,,FONT:bold)
                         STRING('DELIVERY ADDRESS'),AT(4010,1458),USE(?String42),TRN,FONT(,9,,FONT:bold)
                         GROUP,AT(104,3698,7344,500),USE(?groupTitle1)
                           STRING('Qty Ordered'),AT(156,3854),USE(?strQtyOrdered),TRN,FONT(,8,,FONT:bold)
                           STRING('Qty Received'),AT(990,3854),USE(?strQtyReceived),TRN,FONT(,8,,FONT:bold)
                           STRING('Part Number'),AT(1875,3854),USE(?String34),TRN,FONT(,8,,FONT:bold)
                           STRING('Description'),AT(3333,3854),USE(?String33),TRN,FONT(,8,,FONT:bold)
                           STRING('Shelf Location'),AT(5052,3854),USE(?String36),TRN,FONT(,8,,FONT:bold)
                           STRING('GRN Value'),AT(6719,3854),USE(?String66),TRN,FONT('Arial',8,COLOR:Black,FONT:bold,CHARSET:ANSI)
                         END
                         GROUP,AT(52,3646,7344,500),USE(?groupTitle2),TRN,HIDE
                           STRING('Line Cost'),AT(6792,3854),USE(?String69:7),TRN,FONT(,8,,FONT:bold)
                           STRING('Order No'),AT(156,3854),USE(?String69),TRN,FONT(,8,,FONT:bold)
                           STRING('Quantity'),AT(823,3854),USE(?String69:2),TRN,FONT(,8,,FONT:bold)
                           STRING('Part Number / Model Number'),AT(1354,3854),USE(?String69:3),TRN,FONT(,8,,FONT:bold)
                           STRING('Description'),AT(4427,3854),USE(?String69:4),TRN,FONT(,8,,FONT:bold)
                           STRING('Item Cost'),AT(6135,3854),USE(?String69:6),TRN,FONT(,8,,FONT:bold)
                         END
                         STRING('Received By'),AT(104,9844),USE(?String64),TRN,FONT('Arial',9,COLOR:Black,FONT:bold,CHARSET:ANSI)
                         LINE,AT(937,9990,2656,0),USE(?Line1),COLOR(COLOR:Black)
                         STRING('SUPPLIER ADDRESS'),AT(104,1458),USE(?String25),TRN,FONT(,9,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END


LocalTargetSelector  ReportTargetSelectorClass             ! TargetSelector for the Report Processors
LocalReportTarget    &IReportGenerator                     ! ReportTarget for the Report Processors
LocalAttribute       ReportAttributeManager                ! Attribute manager for the Report Processors
LocalWMFParser       WMFDocumentParser                     ! WMFParser for the Report Processors
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED                 ! Method added to host embed code
                     END

  ! The NetTalk Extension to report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
Loc:Html      String(1024)


PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       PrintPreviewFileQueue

PreviewQueueIndex       BYTE


CPCSEmailDialog         BYTE(0)
PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          LONG(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(128)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE



LocalOutputFileQueue PrintPreviewFileQueue

! CPCS Template version  v6.30
! CW Template version    v6.3
! CW Version             6300
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Goods_Received_Note_Retail')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
  SETCURSOR
  PreviewReq = True
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:RETSALES.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:EXCHANGE.Open
  Relate:STANTEXT.Open
  Relate:STOCK.Open
  Access:SUPPLIER.UseFile
  Access:USERS.UseFile
  Access:RETSTOCK.UseFile
  Access:TRADEACC.UseFile
  DO BindFileFields
  IF (p_web.IfExistsValue('GRN'))
      p_web.StoreValue('GRN')
  END
  IF (p_web.IfExistsValue('INV'))
      p_web.StoreValue('INV')
  END
  IF (p_web.IfExistsValue('GRD'))
      p_web.StoreValue('GRD')
  END
  IF (p_web.IfExistsValue('REP'))
      p_web.StoreValue('REP')
  END
  
  tmp:Order_No_Filter = p_web.GSV('INV')
  tmp:Date_Received_Filter = p_web.GSV('GRD')
  
  !Fetch this traders address
  Access:tradeacc.clearkey(tra:Account_Number_Key)
  tra:Account_Number = p_web.GSV('BookingAccount')
  access:tradeacc.fetch(tra:account_Number_key)
  
  
  !fetch the record
  Access:RETSALES.ClearKey(ret:Invoice_Number_Key)
  ret:Invoice_Number = tmp:Order_No_Filter
  Access:RETSALES.TryFetch(ret:Invoice_Number_Key)
  
  
  Access:USERS.Clearkey(use:Password_Key)
  use:Password = p_web.GSV('BookingUserPassword')
  Access:USERS.TryFetch(use:Password_Key)
  tmp:PrintedBy = CLIP(use:Forename) & ' ' & CLIP(use:Surname)
  
  Access:STANTEXT.Clearkey(stt:Description_Key)
  stt:Description = 'DESPATCH NOTE - RETAIL'
  Access:STANTEXT.Tryfetch(stt:Description_Key)
  
  
  LocalTargetSelector.AddItem(PDFReporter.IReportGenerator)
  IF PreviewReq = True
    LocalReportTarget &= PDFReporter.IReportGenerator
  END
  RecordsToProcess = RECORDS(RETSALES)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  WindowOpened = True
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    PreviewReq = True
    CPCS:SVOutSkipPreview# = True
    ProgressWindow{prop:hide} = 1
    loc:PDFName = clip(p_web.site.WebFolderPath) & '\reports\$$$' & format(random(1,99999),@n05) &'.pdf'
  End
  SEND(RETSALES,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END
    OF Event:OpenWindow
      SET(ret:Invoice_Number_Key)
      Process:View{Prop:Filter} = |
      'ret:Invoice_Number = tmp:Order_No_Filter'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
      
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        IF (ret:ExchangeOrder = 1 OR ret:LoanOrder = 1)
            SETTARGET(REPORT)
            ?groupTitle1{prop:hide} = 1
            ?groupTitle2{prop:hide} = 0
            SETTARGET()
        END
        
        Access:RETSTOCK.ClearKey(res:Part_Number_Key)
        res:Ref_Number  = ret:Ref_Number
        Set(res:Part_Number_Key,res:Part_Number_Key)
        Loop
            If Access:RETSTOCK.NEXT()
               Break
            End !If
            If res:Ref_Number  <> ret:Ref_Number      |
                Then Break.  ! End If
            If ~res:Received
                Cycle
            End !If ~res:Received
            If res:GRNNumber <> p_web.GSV('GRN')
                Cycle
            End !If res:GRNNumber <> glo:Select1
        
            IF (ret:ExchangeOrder = 1 OR ret:LoanOrder = 1)
                Parts_Q.PartNo = res:Part_Number
        
                Access:STOCK.Clearkey(sto:Location_Key)
                sto:Location     = VodacomClass.MainStoreLocation()
                sto:Part_Number  = res:Part_Number
                IF (Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign)
        
                END ! IF (Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign)
        
                IF (ret:ExchangeOrder = 1)
                    Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
                    xch:Ref_Number = res:ExchangeRefNumber
                    IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
                        Parts_Q.Description = xch:ESN
                    ELSE ! IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
                        Parts_Q.Description = ''
                    END ! IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
                    Parts_Q.PartNo  = CLIP(Parts_Q.PartNo) & ' ' & CLIP(sto:ExchangeModelNumber)
                END
                IF (ret:LoanOrder = 1)
                    Access:LOAN.ClearKey(loa:Ref_Number_Key)
                    loa:Ref_Number = res:LoanRefNumber
                    IF (Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign)
                        Parts_Q.Description = loa:ESN
                    ELSE
                        Parts_Q.Description = ''
                    END
                    Parts_Q.PartNo  = CLIP(Parts_Q.PartNo) & ' ' & CLIP(sto:LoanModelNumber)
                END
        
        
                Parts_Q.QtyReceived = res:Quantity
                Parts_Q.Cost        = res:Item_Cost
                Parts_Q.LineCost    = res:Item_Cost
                Parts_Q.OrderNumber = res:Purchase_Order_Number
        
                ADD(Parts_Q)
            ElSE ! IF (ret:ExchangeOrder = 1)
        
        
                Parts_Q.pq:PartNo = res:Part_Number
                Parts_Q.pq:Cost = res:Purchase_Cost
                get(Parts_Q, Parts_Q.pq:PartNo, Parts_Q:pq:Cost)
                if error()  ! Insert queue entry
                    Parts_Q.pq:PartNo = res:Part_Number
                    Parts_Q.pq:Description = res:Description
                    Parts_Q.pq:QtyOrdered = res:Quantity   !=====================================
                    Parts_Q.pq:Cost = res:Item_Cost        ! Was purchase_cost    ref G132 / l277
                    if res:Despatched = 'YES'              !=====================================
            !            if res:QuantityReceived <> 0
                            Parts_Q.pq:QtyReceived = res:QuantityReceived
            !            else
            !                Parts_Q.pq:QtyReceived = res:Quantity
            !            end
                    else
                        Parts_Q.pq:QtyReceived = 0
                    end
                    Parts_Q.pq:Location = ''
        
                    !Find the shelf locatin of the RRC's part
                    Access:USERS.Clearkey(use:Password_Key)
                    use:Password    = p_web.GSV('BookingUserPassword')
                    If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                        !Found
                        Access:STOCK.Clearkey(sto:Location_Key)
                        sto:Location    = use:Location
                        sto:Part_Number = res:Part_Number
                        If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                            !Found
                            Parts_Q.pq:Location = sto:Shelf_Location
                        Else ! If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                            !Error
                        End !If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                    Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                        !Error
                    End !If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        
        
                    add(Parts_Q, Parts_Q.pq:PartNo, Parts_Q.pq:Cost)
                else
                    Parts_Q.pq:QtyOrdered += res:Quantity
                    if res:Despatched = 'YES'
            !            if res:QuantityReceived <> 0
                            Parts_Q.pq:QtyReceived += res:QuantityReceived
            !            else
            !                Parts_Q.pq:QtyReceived += res:Quantity
            !            end
                    end
                    put(Parts_Q, Parts_Q.pq:PartNo, Parts_Q.pq:Cost)
                end
            END ! IF (ret:ExchangeOrder = 1)
        end
        
        sort(Parts_Q, Parts_Q.pq:PartNo)
        
        loop a# = 1 to records(Parts_Q)
            get(Parts_Q,a#)
        
            If order_temp <> res:Ref_Number And first_page_temp <> 1
                Print(rpt:detail1)
            End
        
            tmp:RecordsCount += 1
            count# += 1
            If count# > 20
                Print(rpt:continue)
                Print(rpt:detail1)
                count# = 1
            End!If count# > 25
        
            total_cost_temp = Parts_Q.pq:Cost * Parts_Q.pq:QtyReceived
            total_cost_total_temp += total_cost_temp
            total_quantity_temp   += Parts_Q.pq:QtyReceived
            total_lines_temp      += 1
        
            IF (ret:ExchangeOrder = 1 OR ret:LoanOrder = 1)
                Print(rpt:detailExchange)
            ELSE
                Print(rpt:detail)
            END
            order_temp = res:Ref_Number
            first_page_temp = 0
            print# = 1
        end
        
        If print# = 1
            Print(rpt:totals)
        End !If print# = 1
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,CPCS:ButtonYesNo,2)
          OF 2
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,CPCS:ButtonYesNoIgnore,2)
            OF 2
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF 3
              CancelRequested = False
              CYCLE
            OF 1
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(RETSALES,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        if not p_web &= null   ! NetTalk, Report procedure should have prototype of (<NetWebServerWorker p_web>)
          loc:noRecords = 1
          CLOSE(Report)
          DO ProcedureReturn
        End
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation,CPCS:ButtonOk)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        IF ~CPCS:SVOutSkipPreview#
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','',,,,,,,,,,,,,LOC:SaveToQueue)
        ELSE
          LOOP PP# = 1 To RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, PP#)
            LOC:SaveToQueue = PrintPreviewQueue
            ADD(LOC:SaveToQueue)
          END
          GlobalResponse = RequestCompleted
          FREE(PrintPreviewQueue)
        END
          DO GenerateSVReportOutput
  
  
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      if not p_web &= null   ! NetTalk, Report procedure should have prototype of (<NetWebServerWorker p_web>)
        loc:noRecords = 1
        CLOSE(Report)
        DO ProcedureReturn
      End
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation,CPCS:ButtonOk)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        if not p_web &= null   ! NetTalk, Report procedure should have prototype of (<NetWebServerWorker p_web>)
          loc:noRecords = 1
          CLOSE(Report)
          DO ProcedureReturn
        End
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation,CPCS:ButtonOk)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      if not p_web &= null   ! NetTalk, Report procedure should have prototype of (<NetWebServerWorker p_web>)
        loc:noRecords = 1
        CLOSE(Report)
        DO ProcedureReturn
      End
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation,CPCS:ButtonOk)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(Report)
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    CLOSE(Process:View)
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:EXCHANGE.Close
    Relate:RETSALES.Close
    Relate:STANTEXT.Close
    Relate:STOCK.Close
  END
  IF WindowOpened
    ProgressWindow{PROP:HIDE}=False
    CLOSE(ProgressWindow)
  END
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      Loc:html = '<script type="text/javascript">alert('''&clip('No Records')&''');top.close();</script>'
      p_web.ParseHTML(loc:html)
    Else
      p_web.ForceNoCache = 1
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(loc:PDFName)
    End
  End

  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  FREE(LOC:SaveToQueue)
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
    If Not p_web &= Null
      p_web.NoOp()
    End
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'RETSALES')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  !Alternative Invoice Address
  Set(Defaults)
  access:Defaults.next()
  
  !removed ref G132/L277
  
  !If def:use_invoice_address = 'YES' And def:use_for_order = 'YES'
  !    Default_Invoice_Company_Name_Temp       = DEF:Invoice_Company_Name
  !    Default_Invoice_Address_Line1_Temp      = DEF:Invoice_Address_Line1
  !    Default_Invoice_Address_Line2_Temp      = DEF:Invoice_Address_Line2
  !    Default_Invoice_Address_Line3_Temp      = DEF:Invoice_Address_Line3
  !    Default_Invoice_Postcode_Temp           = DEF:Invoice_Postcode
  !    Default_Invoice_Telephone_Number_Temp   = DEF:Invoice_Telephone_Number
  !    Default_Invoice_Fax_Number_Temp         = DEF:Invoice_Fax_Number
  !    Default_Invoice_VAT_Number_Temp         = DEF:Invoice_VAT_Number
  !Else!If def:use_invoice_address = 'YES'
  !    Default_Invoice_Company_Name_Temp       = DEF:User_Name
  !    Default_Invoice_Address_Line1_Temp      = DEF:Address_Line1
  !    Default_Invoice_Address_Line2_Temp      = DEF:Address_Line2
  !    Default_Invoice_Address_Line3_Temp      = DEF:Address_Line3
  !    Default_Invoice_Postcode_Temp           = DEF:Postcode
  !    Default_Invoice_Telephone_Number_Temp   = DEF:Telephone_Number
  !    Default_Invoice_Fax_Number_Temp         = DEF:Fax_Number
  !    Default_Invoice_VAT_Number_Temp         = DEF:VAT_Number
  !End!If def:use_invoice_address = 'YES'
  
  ! Display standard text (DBH: 10/08/2006)
  Access:STANTEXT.ClearKey(stt:Description_Key)
  stt:Description = 'PARTS ORDER'
  If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
      !Found
  Else ! If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
      !Error
  End ! If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
  
  SYSTEM{PROP:PrintMode} = 3
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  If p_web.GSV('REP') = 'Y' ! Reprint
      SetTarget(Report)
      ?Reprint{prop:Hide} = 0
      SetTarget()
  End !glo:Select4 = 'REPRINT'
  Report{PROPPRINT:Extend}=True
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='Goods_Received_Note_Retail'
  END
  Report{Prop:Preview} = PrintPreviewQueue.PrintPreviewImage
  LocalAttribute.Init(Report)
  Do SetStaticControlsAttributes
  



GenerateSVReportOutput        ROUTINE
  IF NOT LocalReportTarget &= NULL THEN
    IF RECORDS(LOC:SaveToQueue) THEN
      IF LocalReportTarget.SupportResultQueue()=True THEN
        LocalReportTarget.SetResultQueue(LocalOutputFileQueue)
      END
       ! The false parameter indicates that the AskProperties must ask for a name
       ! only if the target name is blank
      IF LocalReportTarget.AskProperties(False)=Level:Benign THEN
        LocalWMFParser.Init(LOC:SaveToQueue, LocalReportTarget)
        IF LocalWMFParser.GenerateReport()=Level:Benign THEN
          IF LocalReportTarget.SupportResultQueue()=True THEN
            Do ProcessOutputFileQueue
          END
        END
      END
    END
  ELSE
    FREE(LocalOutputFileQueue)
    LOOP PreviewQueueIndex=1 TO RECORDS(LOC:SaveToQueue)
      GET(LOC:SaveToQueue,PreviewQueueIndex)
      IF NOT ERRORCODE() THEN
        LocalOutputFileQueue.FileName = LOC:SaveToQueue.FileName
        ADD(LocalOutputFileQueue)
      END
    END
    Do ProcessOutputFileQueue
    FREE(LocalOutputFileQueue)
  END

ProcessOutputFileQueue          ROUTINE

SetStaticControlsAttributes     ROUTINE

SetDynamicControlsAttributes    ROUTINE


BindFileFields ROUTINE
      BIND('tmp:Order_No_Filter',tmp:Order_No_Filter)
  BIND(def:RECORD)
  BIND(dep:RECORD)
  BIND(xch:RECORD)
  BIND(ret:RECORD)
  BIND(res:RECORD)
  BIND(stt:RECORD)
  BIND(sto:RECORD)
  BIND(sup:RECORD)
  BIND(tra:RECORD)
  BIND(use:RECORD)
UnBindFields ROUTINE





PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','WebServer_Phase4','Goods_Received_Note_Retail','Goods_Received_Note_Retail','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.CompressText   = True
  SELF.CompressImages = True
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    SELF.SetFileName(clip(loc:PDFName))
  End

AddToStockAllocation PROCEDURE  (LONG func:PartRecordNumber,STRING func:PartType,LONG func:Quantity,STRING func:Status,STRING func:Engineer,<NetWebServerWorker p_web>) ! Declare Procedure
  CODE
    vod.AddPartToStockAllocation(func:PartRecordNumber,func:PartType,func:Quantity,func:Status,func:Engineer,,p_web)
GetStatus            PROCEDURE  (LONG f_number,BYTE f_audit,STRING f_type,<NetWebServerWorker p_web>) ! Declare Procedure
  CODE
    VodacomClass.SetStatus(f_number,f_audit,f_type,p_web)

    SendSMSText(f_type[1],'Y','N',p_web)
ds_Stop              PROCEDURE  (<string StopText>)        ! Declare Procedure
returned              long
TempTimeOut           long(0)
TempNoLogging         long(0)
Loc:StopText    string(4096)
  CODE
  if omitted(1) or StopText = '' then
    if ThisMessageBox.GetGlobalSetting('TranslationFile') <> ''
      Loc:StopText = getini('MessageBox_Text','StopDefault','Exit?',ThisMessageBox.GetGlobalSetting('TranslationFile'))
    else
      Loc:StopText = 'Exit?'
    end
  else
    Loc:StopText = StopText
  end
  if ThisMessageBox.GetGlobalSetting('TimeOut') > 0
    TempTimeOut = ThisMessageBox.GetGlobalSetting('TimeOut')
    ThisMessageBox.SetGlobalSetting('TimeOut', 0)
  end
  if ThisMessageBox.GetGlobalSetting('TranslationFile') <> ''
    returned = ds_Message(Loc:StopText,getini('MessageBox_Text','StopHeader','Stop',ThisMessageBox.GetGlobalSetting('TranslationFile')),ICON:Hand,BUTTON:Abort+BUTTON:Ignore,BUTTON:Abort)
  else
    returned = ds_Message(Loc:StopText,'Stop',ICON:Hand,BUTTON:Abort+BUTTON:Ignore,BUTTON:Abort)
  end
  if returned = BUTTON:Abort then halt .
  if TempTimeOut
    ThisMessageBox.SetGlobalSetting('TimeOut',TempTimeOut)
  end
