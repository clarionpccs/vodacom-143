

   MEMBER('WebServer_Phase4.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSE064.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSE020.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE021.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE022.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE063.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE065.INC'),ONCE        !Req'd for module callout resolution
                     END


BannerVodacom        PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
FilesOpened     Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  GlobalErrors.SetProcedureName('BannerVodacom')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'BannerVodacom_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('BannerVodacom','')
    p_web.DivHeader('BannerVodacom',p_web.combine(p_web.site.style.formdiv,'fdiv'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('BannerVodacom') = 0
        p_web.AddPreCall('BannerVodacom')
        p_web.DivHeader('popup_BannerVodacom','nt-hidden')
        p_web.DivHeader('BannerVodacom',p_web.combine(p_web.site.style.formdiv,'fdiv'))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_BannerVodacom_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_BannerVodacom_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferBannerVodacom',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_BannerVodacom',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBannerVodacom',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_BannerVodacom',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBannerVodacom',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_BannerVodacom',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_BannerVodacom',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBannerVodacom',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_BannerVodacom',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBannerVodacom',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_BannerVodacom',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_BannerVodacom',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('BannerVodacom')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('BannerVodacom_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'BannerVodacom'
    end
    p_web.formsettings.proc = 'BannerVodacom'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine

AfterLookup Routine
  loc:TabNumber = -1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('BannerVodacom_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferBannerVodacom')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('BannerVodacom_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('BannerVodacom_ChainTo')
    loc:formaction = p_web.GetSessionValue('BannerVodacom_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction

GenerateForm   Routine
  do LoadRelatedRecords
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
    do SendPacket
    Do heading
    do SendPacket
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
    Packet = clip(Packet) & p_web.DivHeader('Tab_BannerVodacom',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="BannerVodacom_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      packet = clip(packet) & '</div><13,10>' ! end id="BannerVodacom_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'BannerVodacom_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="BannerVodacom_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'BannerVodacom_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_BannerVodacom')>0,p_web.GSV('showtab_BannerVodacom'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_BannerVodacom'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_BannerVodacom') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_BannerVodacom'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_BannerVodacom')>0,p_web.GSV('showtab_BannerVodacom'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_BannerVodacom') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_BannerVodacom_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_BannerVodacom')>0,p_web.GSV('showtab_BannerVodacom'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"BannerVodacom",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_BannerVodacom')>0,p_web.GSV('showtab_BannerVodacom'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_BannerVodacom_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('BannerVodacom') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('BannerVodacom')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine


NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_BannerVodacom_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('BannerVodacom_form:ready_',1)

  p_web.SetSessionValue('BannerVodacom_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_BannerVodacom',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('BannerVodacom_form:ready_',1)
  p_web.SetSessionValue('BannerVodacom_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_BannerVodacom',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('BannerVodacom_form:ready_',1)
  p_web.SetSessionValue('BannerVodacom_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('BannerVodacom:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('BannerVodacom_form:ready_',1)
  p_web.SetSessionValue('BannerVodacom_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('BannerVodacom:Primed',0)
  p_web.setsessionvalue('showtab_BannerVodacom',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('BannerVodacom_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('BannerVodacom_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('BannerVodacom:Primed',0)

heading  Routine
  packet = clip(packet) & p_web.AsciiToUTF(|
    '<<table class="bannerHeading"><13,10>'&|
    '    <<tr><13,10>'&|
    '        <<td width="810" align="center"><<img src="/images/header.gif" width="810" height="124" /><</td><13,10>'&|
    '    <</tr><13,10>'&|
    '    <<tr><13,10>'&|
    '        <<td width="810" align="right" class="SmallText"><<!-- Net:s:VersionNumber --><</td><13,10>'&|
    '    <</tr><13,10>'&|
    '<</table><13,10>'&|
    '',net:OnlyIfUTF)
BrowseJobCredits     PROCEDURE  (NetWebServerWorker p_web)
tmp:CreditNumber     STRING(30)                            !
tmp:CreditType       STRING(4)                             !
tmp:Amount           REAL                                  !
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256),dim(Net:MaxKeyFields)
idx           String(Net:HashSize)
sub           Long
            End
tmp:CreditNumber:IsInvalid  Long
tmp:CreditType:IsInvalid  Long
jov:DateCreated:IsInvalid  Long
tmp:Amount:IsInvalid  Long
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:DefaultSelection    String(Net:HashSize)
loc:ActualSelection     String(Net:HashSize)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
!loc:pagename            String(256)
loc:NavButtonPosition    Long
loc:UpdateButtonPosition Long
loc:SelectionMethod      Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorCase         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
loc:lookupdone          Long
loc:FormPopup           Long
ThisView            View(JOBSINV)
                      Project(jov:RecordNumber)
                      Project(jov:DateCreated)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
Loc:popup             Long
loc:poppedup          Long,static,thread
FilesOpened     Long
JOBS::State  USHORT
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return 
  End
  GlobalErrors.SetProcedureName('BrowseJobCredits')
  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseJobCredits:NoForm')
      loc:NoForm = p_web.GetValue('BrowseJobCredits:NoForm')
      loc:FormName = p_web.GetValue('BrowseJobCredits:FormName')
    else
      loc:FormName = 'BrowseJobCredits_frm'
    End
    p_web.SSV('BrowseJobCredits:NoForm',loc:NoForm)
    p_web.SSV('BrowseJobCredits:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseJobCredits:NoForm')
    loc:FormName = p_web.GSV('BrowseJobCredits:FormName')
  end
  case p_web.site.formpopups
  of -1 ; loc:FormPopup = Net:Page
  of 0 ; loc:FormPopup = Net:Page
  of 1 ; loc:FormPopup = Net:Popup
  End
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseJobCredits') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseJobCredits')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if 1 = 2
  elsif p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  elsif p_web.IfExistsValue('_EIPRow_')
    do CallRow
  elsif p_web.GetValue('_CallPopups') > 0
    do CallPopups
  elsif p_web.IfExistsValue('_GetSecwinSettings')
      p_web.SetValue('Secwin_' & 'BrowseJobCredits' & 'AccessGroupsArray','1') 
      p_web.SetValue('Secwin_AccessWindowName','BrowseJobCredits')
      
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallPopups  Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
loc:CallPopups   Long
loc:name         String(255)
  code
  if loc:poppedup = 0
    loc:CallPopups = p_web.GetValue('_CallPopups')
    if loc:CallPopups = 1 !and p_web.GetPreCall('BrowseJobCredits') = 0! for = 1 include popup divs and scripts for this browse
      !p_web.AddPreCall('BrowseJobCredits')
      p_web.DivHeader('popup_BrowseJobCredits','nt-hidden')
      p_web.DivHeader('BrowseJobCredits',p_web.combine(p_web.site.style.browsediv,))
      p_web.DivFooter()
      p_web.DivFooter()
      loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(400)&', modal: true, position: [''center'',15]'
      packet =  p_web.jQuery('#' & lower('popup_BrowseJobCredits_div'),'dialog',loc:options) & |
      p_web.jQuery('#' & lower('popup_BrowseJobCredits_div'),'removeClass','''nt-hidden''')
    End
    ! for > 0 add in form, EIP-lookups and browse object
    do Popups
    loc:popup = 1
    do ClosingScripts
    do SendPacket
    loc:poppedup = 1
  end

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web.DivHeader('BrowseJobCredits',p_web.combine(p_web.site.style.browsediv,))
  if loc:ParentSilent = 0
    do GenerateBrowse
    if p_web.RequestAjax = 1
      p_web.Script('BrowseProducts.pcv();')
    end
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web.DivFooter()
  do Children
  do Popups
  if loc:poppedup = 0
    do ClosingScripts
  end
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(JOBSINV,jov:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'TMP:CREDITNUMBER') then p_web.SetValue('BrowseJobCredits_sort','1')
    ElsIf (loc:vorder = 'TMP:CREDITTYPE') then p_web.SetValue('BrowseJobCredits_sort','2')
    ElsIf (loc:vorder = 'JOV:DATECREATED') then p_web.SetValue('BrowseJobCredits_sort','3')
    ElsIf (loc:vorder = 'TMP:AMOUNT') then p_web.SetValue('BrowseJobCredits_sort','4')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseJobCredits:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseJobCredits:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseJobCredits:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseJobCredits:LookupField')
    loc:selecting = 0
  End

Popups Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  If loc:popup = 0
  End
SetFormAction  Routine
  loc:formaction = 'BrowseJobCredits'

GotFocusBack   Routine

GenerateBrowse Routine
  data
loc:viewoptions  Long
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  ! Set general Browse options
  loc:NavButtonPosition   = Net:Below
  loc:UpdateButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:FileLoad
  loc:FillBack         = 0
  loc:Sorting          = Net:ServerSort
  if p_web.GetValue('_popup_') = 1
    loc:popup = 1
  end
  ! Set Locator Options
  loc:LocatorPosition  = Net:Below
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  p_web.ClearBrowse('BrowseJobCredits')
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 10
  End
  loc:ActualSelection = ''
  !loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseJobCredits_sort',net:DontEvaluate)
  p_web.SetSessionValue('BrowseJobCredits_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'tmp:CreditNumber','-tmp:CreditNumber')
    Loc:LocateField = 'tmp:CreditNumber'
    Loc:LocatorCase = 0
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'tmp:CreditType','-tmp:CreditType')
    Loc:LocateField = 'tmp:CreditType'
    Loc:LocatorCase = 0
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'jov:DateCreated','-jov:DateCreated')
    Loc:LocateField = 'jov:DateCreated'
    Loc:LocatorCase = 0
  of 4
    loc:vorder = Choose(Loc:SortDirection=1,'tmp:Amount','-tmp:Amount')
    Loc:LocateField = 'tmp:Amount'
    Loc:LocatorCase = 0
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('tmp:CreditNumber')
    loc:SortHeader = p_web.Translate('Credit Number')
    p_web.SetSessionValue('BrowseJobCredits_LocatorPic','@s30')
  Of upper('tmp:CreditType')
    loc:SortHeader = p_web.Translate('Type')
    p_web.SetSessionValue('BrowseJobCredits_LocatorPic','@s4')
  Of upper('jov:DateCreated')
    loc:SortHeader = p_web.Translate('Date Created')
    p_web.SetSessionValue('BrowseJobCredits_LocatorPic','@d6')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
    loc:LocatorType = Net:Date
  Of upper('tmp:Amount')
    loc:SortHeader = p_web.Translate('Amount')
    p_web.SetSessionValue('BrowseJobCredits_LocatorPic','@n-14.2')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
  End
  If loc:selecting = 1
    p_web.GetSettings(p_web.GetSessionValue('Push1'))
    loc:selectaction = p_web.FormSettings.ParentPage !p_web.GetSessionValue('BrowseJobCredits:LookupFrom')
  End!Else
  do SendPacket
  do SetFormAction
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseJobCredits:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseJobCredits:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseJobCredits:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="JOBSINV"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="jov:RecordNumberKey"></input><13,10>'
  end
  case p_web.GetValue('refresh') ; of 'next' orof 'previous' orof 'first' orof 'last'
    p_web.Script('try{{BrowseJobCredits.restoreFocus();} catch(err){{};')
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseJobCredits',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      case loc:locatortype
      of Net:Position
      orof Net:Date
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
      of Net:Contains
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
      of Net:Begins
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
      End
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
      Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
      Case Loc:LocatorType
      of Net:Date
        packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator2BrowseJobCredits','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('BrowseJobCredits_LocatorPic'),,,'onchange="BrowseJobCredits.locate(''Locator2BrowseJobCredits'',this.value);"',,0,,,) & '</div></td>'
      Else
        packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator2BrowseJobCredits',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="BrowseJobCredits.locate(''Locator2BrowseJobCredits'',this.value);" ',,,,,,) & '</td>'
      End
      If loc:LocatorSearchButton or loc:LocatorClearButton
        packet = clip(packet) & '<td><div id="BrowseJobCredits_locate_a" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
        If loc:LocatorSearchButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
        End
        If loc:LocatorClearButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseJobCredits.cl(''BrowseJobCredits'');')
        End
        packet = clip(packet) & '</div><13,10></td><13,10>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
      If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'BrowseJobCredits_locate_a','buttonset',loc:options)
      End
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & p_web.DivHeader('BrowseJobCredits_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'','sortable')&'" id="BrowseJobCredits_tbl">'
  Else
    packet = clip(packet) & p_web.DivHeader('BrowseJobCredits_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'')&'" id="BrowseJobCredits_tbl">'
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'1','BrowseJobCredits',p_web.Translate('Credit Number'),,,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'2','BrowseJobCredits',p_web.Translate('Type'),,,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'3','BrowseJobCredits',p_web.Translate('Date Created'),'Click here to sort by Date Created',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'4','BrowseJobCredits',p_web.Translate('Amount'),,,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:LocateField = 'jov:DateCreated' then Loc:NoBuffer = 1.
  If Loc:NoBuffer = 0
    Buffer(ThisView,10,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  If Instring('jov:recordnumber',lower(loc:vorder),1,1) = 0 !and JOBSINV{prop:SQLDriver} = 1
    loc:vorder = Choose(loc:vorder='','jov:RecordNumber',clip(loc:vorder) & ',' & 'jov:RecordNumber')
  End
  Loc:Selected = Choose(p_web.IfExistsValue('jov:RecordNumber'),p_web.GetValue('jov:RecordNumber'),p_web.GetSessionValue('jov:RecordNumber'))
  ThisView{prop:order} = p_web.CleanFilter(ThisView,clip(loc:vorder))
      loc:FilterWas = 'jov:RefNumber = ' & p_web.GSV('job:Ref_Number')
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseJobCredits',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseJobCredits_Filter')
    p_web.SetSessionValue('BrowseJobCredits_FirstValue','')
    p_web.SetSessionValue('BrowseJobCredits_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,JOBSINV,jov:RecordNumberKey,loc:PageRows,'BrowseJobCredits',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,loc:ViewOptions,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled,Loc:LocatorCase)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or Loc:LocatorType = Net:Date or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position or loc:LocatorType = Net:Date
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If JOBSINV{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(JOBSINV,loc:firstvalue)
              Reset(ThisView,JOBSINV)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If JOBSINV{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(JOBSINV,loc:lastvalue)
            Reset(ThisView,JOBSINV)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      if (jov:Type = 'C')
          tmp:CreditType = 'Cred'
          tmp:CreditNumber = 'CN' & CLIP(jov:InvoiceNumber) & '-' & CLIP(tra:BranchIdentification) & jov:Suffix
          tmp:Amount = jov:CreditAmount * -1
      else ! if (job:Type = 'C')
          tmp:CreditType = 'Inv'
          tmp:CreditNumber = CLIP(jov:InvoiceNumber) & '-' & CLIP(tra:BranchIdentification) & jov:Suffix
          tmp:Amount = jov:NewTOtalCost
      end !if (job:Type = 'C')
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(jov:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td class="'&p_web.combine(p_web.site.style.BrowseEmpty,)&'">'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:NavButtonPosition=Net:Above or (loc:NavButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseJobCredits_nav_a" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseJobCredits.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseJobCredits.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseJobCredits.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseJobCredits.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseJobCredits_nav_a','buttonset',loc:options)
          End
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:UpdateButtonPosition=Net:Above or (loc:UpdateButtonPosition=Net:Both and loc:found > 0))
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseJobCredits',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseJobCredits_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseJobCredits_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          case loc:locatortype
          of Net:Position
          orof Net:Date
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
          of Net:Contains
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
          of Net:Begins
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
          End
          packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
          Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
          Case Loc:LocatorType
          of Net:Date
            packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator1BrowseJobCredits','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('BrowseJobCredits_LocatorPic'),,,'onchange="BrowseJobCredits.locate(''Locator1BrowseJobCredits'',this.value);"',,0,,,) & '</div></td>'
          Else
            packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator1BrowseJobCredits',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="BrowseJobCredits.locate(''Locator1BrowseJobCredits'',this.value);" ',,,,,,) & '</td>'
          End
          If loc:LocatorSearchButton or loc:LocatorClearButton
            packet = clip(packet) & '<td><div id="BrowseJobCredits_locate_b" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
            If loc:LocatorSearchButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
            End
            If loc:LocatorClearButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseJobCredits.cl(''BrowseJobCredits'');')
            End
            packet = clip(packet) & '</div><13,10></td><13,10>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
          If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseJobCredits_locate_b','buttonset',loc:options)
          End
      End
    End
  End
  p_web.SetSessionValue('BrowseJobCredits_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseJobCredits_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:NavButtonPosition=Net:Below or loc:NavButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseJobCredits_nav_b" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseJobCredits.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseJobCredits.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseJobCredits.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseJobCredits.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseJobCredits_nav_b','buttonset',loc:options)
          End
        end
        do SendPacket
  End
  If loc:UpdateButtonPosition=Net:Below or loc:UpdateButtonPosition=Net:Both
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
  data
loc:rowclick  string(1024)
  code
    loc:field = p_web.AddBrowseValue('BrowseJobCredits','JOBSINV',jov:RecordNumberKey) !jov:RecordNumber
    p_web._thisrow = p_web._nocolon('jov:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if Loc:LocatorValue <> '' and loc:ActualSelection = ''
        loc:checked = 'checked'
        do SetSelection
      elsif loc:ActualSelection = '' and jov:RecordNumber = p_web.GetValue('jov:RecordNumber')
         loc:checked = 'checked'
         do SetSelection
      elsif loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseJobCredits:LookupField')) = jov:RecordNumber and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      else
        loc:checked = Choose((jov:RecordNumber = loc:selected) and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      end
      If(loc:SelectionMethod  = Net:Radio)
        loc:RowStyle = 'class="' & p_web.combine(p_web.site.style.browserow,) &'"'
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'data-nt-id="'& p_web.AddBrowseValue('BrowseJobCredits','JOBSINV',jov:RecordNumberKey) &'" onclick="'
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If JOBSINV{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(JOBSINV)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            loc:DefaultSelection = loc:field
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If JOBSINV{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(JOBSINV)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            loc:DefaultSelection = loc:field
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'>'&p_web.CreateInput('radio','_bidv_',clip(loc:field),,loc:checked,'')&'</td>'&CRLF
          If loc:DefaultSelection = ''
            loc:DefaultSelection = loc:field
          End
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          If loc:DefaultSelection = '' or loc:direction < 0
            loc:DefaultSelection = loc:field
          End
        End
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::tmp:CreditNumber
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::tmp:CreditType
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::jov:DateCreated
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::tmp:Amount
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr ' & clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Idx = p_web.AddBrowseValue('BrowseJobCredits','JOBSINV',jov:RecordNumberKey)
  TableQueue.Id[1] = jov:RecordNumber

ClosingScripts  Routine
  data
Rtn_SecwinProcedureName         string(252)
  code
  If p_web.RequestAjax = 0
    do SetFormAction
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var btiBrowseJobCredits;if (btiBrowseJobCredits != 1){{var BrowseJobCredits=new browseTable(''BrowseJobCredits'','''&clip(loc:formname)&|
      ''','''&p_web._jsok('jov:RecordNumber',Net:Parameter)&''',2,'''&clip(loc:divname)&''',1,1,1,'''&|
      clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&|
      p_web.Translate('Are you sure you want to delete this record?')&''','''&clip(loc:ActualSelection)&''','''&clip(loc:selectaction)&''','''&|
      clip(loc:formactiontarget)&''','''','&loc:popup&',0,'''','''',"' & clip(Rtn_SecwinProcedureName) & '");<13,10>'&|
      'BrowseJobCredits.setGreenBar('''&p_web.ColorWeb(p_web.Site.Style.BrowseHighlightColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOneColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseTwoColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOverColor)&''');<13,10>' &|
      'BrowseJobCredits.applyGreenBar();btiBrowseJobCredits=1};<13,10>' &|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseJobCredits')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseJobCredits')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseJobCredits')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseJobCredits')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(JOBSINV)
  p_web._CloseFile(JOBS)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(JOBSINV)
  Bind(jov:Record)
  Clear(jov:Record)
  NetWebSetSessionPics(p_web,JOBSINV)
  p_web._OpenFile(JOBS)
  Bind(job:Record)
  NetWebSetSessionPics(p_web,JOBS)

Children Routine
  if loc:selecting = 0
    If p_web.RequestAjax = 0
      do StartChildren
    Else
      do AjaxChildren
    End
  end

AjaxChildren  Routine
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue('jov:RecordNumber',p_web.GetValue('jov:RecordNumber'))
   p_web.DeleteValue('_Clicked')
   loc:found = 1
   do Children
! ----------------------------------------------------------------------------------------
CallRow  Routine
  data
loc:result  long
  code
  do OpenFilesB
  jov:RecordNumber = p_web.GSV('jov:RecordNumber')
  loc:result = p_web._GetFile(JOBSINV,jov:RecordNumberKey)
  loc:eip = 1
  loc:viewstate = p_web.escape(p_web.Base64Encode(clip(jov:RecordNumber)))
  do BrowseRow
  do SendPacket
  do ClosefilesB

! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(JOBSINV)
  Case upper(p_web.GetValue('_EIPClm'))
  Else
    case p_web.GetValue('_action_')
    of Net:InsertRecord
    orof Net:ChangeRecord
    orof Net:DeleteRecord
    orof Net:CopyRecord
      loc:eip = 0
      do CallBrowse
    end
  End
  do GotFocusBack
  p_web._CloseFile(JOBSINV)
! ----------------------------------------------------------------------------------------
value::tmp:CreditNumber   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseJobCredits_tmp:CreditNumber_'&jov:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(tmp:CreditNumber,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::tmp:CreditType   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseJobCredits_tmp:CreditType_'&jov:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(tmp:CreditType,'@s4')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::jov:DateCreated   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseJobCredits_jov:DateCreated_'&jov:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(jov:DateCreated,'@d6')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::tmp:Amount   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseJobCredits_tmp:Amount_'&jov:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(tmp:Amount,'@n-14.2')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  p_web._OpenFile(JOBS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(JOBS)
     FilesOpened = False
  END
  return
!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
CheckForDuplicate  Routine
SetSelection  Routine
  loc:ActualSelection = loc:field
  p_web.SetSessionValue('jov:RecordNumber',jov:RecordNumber)

MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:ActualSelection = ''
    loc:ActualSelection = loc:DefaultSelection
    p_web.GetBrowseValue(loc:ActualSelection,Net:Web:SessionQueue) ! so children are primed with correct sessionValue
  End

  If loc:ActualSelection <> ''
    TableQueue.Kind = Net:RowTable
    get(TableQueue,TableQueue.Kind)
    if Errorcode() = 0
      TableQueue.Row = sub(TableQueue.Row,1,len(clip(TableQueue.Row))-1) &  ' data-nt-rows="'&loc:RowsHigh&'" data-nt-value="'&clip(loc:ActualSelection)&'"' & '>'
      Put(TableQueue)
    End
  End

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead class="'&p_web.combine(p_web.site.style.BrowseHeader,'')&'"><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot class="'&p_web.combine(p_web.site.style.BrowseFooter,)&'"><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody class="'&p_web.combine(p_web.site.style.BrowseBody,)&'"><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table><13,10>' & p_web.DivFooter(Net:NoSend) ! Table Div
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
BillingConfirmation  PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
tmp:CChargeType      STRING(30)                            !
tmp:CRepairType      STRING(30)                            !
tmp:CConfirmRepairType STRING(30)                          !
tmp:WChargeType      STRING(30)                            !
tmp:WRepairType      STRING(30)                            !
tmp:WConfirmRepairType BYTE                                !
tmp:Password         STRING(30)                            !
FilesOpened     Long
CHARTYPE::State  USHORT
REPTYDEF::State  USHORT
USERS::State  USHORT
tmp:CChargeType:IsInvalid  Long
tmp:CRepairType:IsInvalid  Long
tmp:CConfirmRepairType:IsInvalid  Long
tmp:WChargeType:IsInvalid  Long
tmp:WRepairType:IsInvalid  Long
tmp:WConfirmRepairType:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
tmp:CChargeType_OptionView   View(CHARTYPE)
                          Project(cha:Charge_Type)
                        End
tmp:CRepairType_OptionView   View(REPTYDEF)
                          Project(rtd:Repair_Type)
                        End
tmp:WChargeType_OptionView   View(CHARTYPE)
                          Project(cha:Charge_Type)
                        End
tmp:WRepairType_OptionView   View(REPTYDEF)
                          Project(rtd:Repair_Type)
                        End
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('BillingConfirmation')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'BillingConfirmation_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('BillingConfirmation','')
    p_web.DivHeader('BillingConfirmation',p_web.combine(p_web.site.style.formdiv,))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('BillingConfirmation') = 0
        p_web.AddPreCall('BillingConfirmation')
        p_web.DivHeader('popup_BillingConfirmation','nt-hidden')
        p_web.DivHeader('BillingConfirmation',p_web.combine(p_web.site.style.formdiv,))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_BillingConfirmation_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_BillingConfirmation_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferBillingConfirmation',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_BillingConfirmation',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBillingConfirmation',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_BillingConfirmation',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBillingConfirmation',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_BillingConfirmation',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_BillingConfirmation',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBillingConfirmation',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_BillingConfirmation',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBillingConfirmation',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_BillingConfirmation',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_BillingConfirmation',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('BillingConfirmation')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(CHARTYPE)
  p_web._OpenFile(REPTYDEF)
  p_web._OpenFile(USERS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(CHARTYPE)
  p_Web._CloseFile(REPTYDEF)
  p_Web._CloseFile(USERS)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('BillingConfirmation_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'BillingConfirmation'
    end
    p_web.formsettings.proc = 'BillingConfirmation'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine

AfterLookup Routine
  loc:TabNumber = -1
  If p_web.GSV('job:Chargeable_Job') = 'YES'
    loc:TabNumber += 1
  End
  Case p_Web.GetValue('lookupfield')
  Of 'tmp:CChargeType'
    p_web.setsessionvalue('showtab_BillingConfirmation',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(CHARTYPE)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:CRepairType')
  End
  If p_web.GSV('job:Warranty_job') = 'YES'
    loc:TabNumber += 1
  End
  Case p_Web.GetValue('lookupfield')
  Of 'tmp:WChargeType'
    p_web.setsessionvalue('showtab_BillingConfirmation',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(CHARTYPE)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:WRepairType')
  End
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  if p_web.IfExistsValue('tmp:CChargeType') = 0
    p_web.SetSessionValue('tmp:CChargeType',tmp:CChargeType)
  Else
    tmp:CChargeType = p_web.GetSessionValue('tmp:CChargeType')
  End
  if p_web.IfExistsValue('tmp:CRepairType') = 0
    p_web.SetSessionValue('tmp:CRepairType',tmp:CRepairType)
  Else
    tmp:CRepairType = p_web.GetSessionValue('tmp:CRepairType')
  End
  if p_web.IfExistsValue('tmp:CConfirmRepairType') = 0
    p_web.SetSessionValue('tmp:CConfirmRepairType',tmp:CConfirmRepairType)
  Else
    tmp:CConfirmRepairType = p_web.GetSessionValue('tmp:CConfirmRepairType')
  End
  if p_web.IfExistsValue('tmp:WChargeType') = 0
    p_web.SetSessionValue('tmp:WChargeType',tmp:WChargeType)
  Else
    tmp:WChargeType = p_web.GetSessionValue('tmp:WChargeType')
  End
  if p_web.IfExistsValue('tmp:WRepairType') = 0
    p_web.SetSessionValue('tmp:WRepairType',tmp:WRepairType)
  Else
    tmp:WRepairType = p_web.GetSessionValue('tmp:WRepairType')
  End
  if p_web.IfExistsValue('tmp:WConfirmRepairType') = 0
    p_web.SetSessionValue('tmp:WConfirmRepairType',tmp:WConfirmRepairType)
  Else
    tmp:WConfirmRepairType = p_web.GetSessionValue('tmp:WConfirmRepairType')
  End

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('tmp:CChargeType')
    tmp:CChargeType = p_web.GetValue('tmp:CChargeType')
    p_web.SetSessionValue('tmp:CChargeType',tmp:CChargeType)
  Else
    tmp:CChargeType = p_web.GetSessionValue('tmp:CChargeType')
  End
  if p_web.IfExistsValue('tmp:CRepairType')
    tmp:CRepairType = p_web.GetValue('tmp:CRepairType')
    p_web.SetSessionValue('tmp:CRepairType',tmp:CRepairType)
  Else
    tmp:CRepairType = p_web.GetSessionValue('tmp:CRepairType')
  End
  if p_web.IfExistsValue('tmp:CConfirmRepairType')
    tmp:CConfirmRepairType = p_web.GetValue('tmp:CConfirmRepairType')
    p_web.SetSessionValue('tmp:CConfirmRepairType',tmp:CConfirmRepairType)
  Else
    tmp:CConfirmRepairType = p_web.GetSessionValue('tmp:CConfirmRepairType')
  End
  if p_web.IfExistsValue('tmp:WChargeType')
    tmp:WChargeType = p_web.GetValue('tmp:WChargeType')
    p_web.SetSessionValue('tmp:WChargeType',tmp:WChargeType)
  Else
    tmp:WChargeType = p_web.GetSessionValue('tmp:WChargeType')
  End
  if p_web.IfExistsValue('tmp:WRepairType')
    tmp:WRepairType = p_web.GetValue('tmp:WRepairType')
    p_web.SetSessionValue('tmp:WRepairType',tmp:WRepairType)
  Else
    tmp:WRepairType = p_web.GetSessionValue('tmp:WRepairType')
  End
  if p_web.IfExistsValue('tmp:WConfirmRepairType')
    tmp:WConfirmRepairType = p_web.GetValue('tmp:WConfirmRepairType')
    p_web.SetSessionValue('tmp:WConfirmRepairType',tmp:WConfirmRepairType)
  Else
    tmp:WConfirmRepairType = p_web.GetSessionValue('tmp:WConfirmRepairType')
  End

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('BillingConfirmation_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.GSV('NextURL')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('BillingConfirmation_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('BillingConfirmation_ChainTo')
    loc:formaction = p_web.GetSessionValue('BillingConfirmation_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'ViewJob'

GenerateForm   Routine
  do LoadRelatedRecords
      CalculateBilling(p_web)
  
      p_web.SSV('tmp:CChargeType',p_web.GSV('job:Charge_Type'))
      p_web.SSV('tmp:WChargeType',p_web.GSV('job:Warranty_Charge_Type'))
      p_web.SSV('tmp:CConfirmRepairType',p_web.GSV('jobe:COverwriteRepairType'))
      p_web.SSV('tmp:WConfirmRepairType',p_web.GSV('jobe:WOverwriteRepairType'))
      p_web.SSV('tmp:CRepairType',p_web.GSV('job:Repair_Type'))
      p_web.SSV('tmp:WRepairType',p_web.GSV('job:Repair_Type_Warranty'))
  
      if (securityCheckFailed(p_web.GSV('BookingUserPassword'),'JOBS - AMEND REPAIR TYPES'))
          p_web.SSV('ReadOnly:RepairType',1)
      else!if (securityCheckFailed('JOBS - AMEND REPAIR TYPES',p_web.GSV('BookingPassword'))
          p_web.SSV('ReadOnly:RepairType',0)
      end !if (securityCheckFailed('JOBS - AMEND REPAIR TYPES',p_web.GSV('BookingPassword'))
  
      p_web.SSV('locEngineerSkillLevel',0)
      if (p_web.GSV('job:Engineer') <> '')
          Access:USERS.Clearkey(use:user_Code_Key)
          use:user_Code    = p_web.GSV('job:Engineer')
          if (Access:USERS.TryFetch(use:user_Code_Key) = Level:Benign)
              ! Found
              p_web.SSV('locEngineerSkillLevel',use:SkillLevel)
          else ! if (Access:USERS.TryFetch(use:user_Code_Key) = Level:Benign)
              ! Error
          end ! if (Access:USERS.TryFetch(use:user_Code_Key) = Level:Benign)
      end ! if (p_web.GSV('job:Engineer') <> '')
  
      p_web.storeValue('JobCompleteProcess')
  
      if (p_web.GSV('JobCompleteProcess') = 1)
          if (p_web.GSV('job:Estimate') = 'YES' and p_web.GSV('job:Chargeable_Job') = 'YES')
              if (p_web.GSV('job:Estimate_Accepted') <> 'YES' and |
                  p_web.GSV('job:Estimate_Rejected') <> 'YES' and |
                  p_web.GSV('job:Estimate_Ready') <> 'YES')
  
                  jobPricingRoutine(p_web)
  
                  p_web.SSV('TotalPrice:Type','E')
  
                  totalPrice(p_web)
  
                  if (p_web.GSV('TotalPrice:Total') < p_web.GSV('job:Estimate_If_Over'))
                      p_web.SSV('nextURL','EstimateQuery')
                      p_web.SSV('JobCompleteProcess',0)
                  else ! if (p_web.GSV('TotalRepair:Total') < p_web.GSV('job:Estimate_If_Over'))
                      p_web.SSV('nextURL','ViewJob')
                      p_web.SSV('job:Courier_Cost',p_web.GSV('job:Courier_Cost_Estimate'))
                      p_web.SSV('job:Estimate_Ready','YES')
  
                      p_web.SSV('JobCompleteProcess',0)
  
                      p_web.SSV('Hide:EstimateReady',0)
  
                      getStatus(510,0,'JOB',p_web)
  
                  end ! if (p_web.GSV('TotalRepair:Total') < p_web.GSV('job:Estimate_If_Over'))
              end ! if (p_web.GSV('job:Estimate_Accepted') <> 'YES' and |
          else !
              p_web.SSV('nextURL','ViewJob')
          end ! if (p_web.GSV('job:Estimate') = 'YES' and p_web.GSV('job:Chargeable_Job') = 'YES')
      else !
          p_web.StoreValue('passedURL')
          IF (p_web.GSV('passedURL') <> '')
              p_web.SSV('nextURL',p_web.GSV('passedURL'))
          ELSE
              p_web.SSV('nextURL','ViewCosts')
          END
  
      end ! if (p_web.GSV('Job:CompleteProcess') = 1)
 tmp:CChargeType = p_web.RestoreValue('tmp:CChargeType')
 tmp:CRepairType = p_web.RestoreValue('tmp:CRepairType')
 tmp:CConfirmRepairType = p_web.RestoreValue('tmp:CConfirmRepairType')
 tmp:WChargeType = p_web.RestoreValue('tmp:WChargeType')
 tmp:WRepairType = p_web.RestoreValue('tmp:WRepairType')
 tmp:WConfirmRepairType = p_web.RestoreValue('tmp:WConfirmRepairType')
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Billing Confirmation') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Billing Confirmation',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_BillingConfirmation',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      If p_web.GSV('job:Chargeable_Job') = 'YES'
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_BillingConfirmation0_div')&'">'&p_web.Translate('Chargeable')&'</a></li>'& CRLF
      End
      If p_web.GSV('job:Warranty_job') = 'YES'
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_BillingConfirmation1_div')&'">'&p_web.Translate('Warranty')&'</a></li>'& CRLF
      End
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  do GenerateTab1
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="BillingConfirmation_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.cancel();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CancelButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="BillingConfirmation_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'BillingConfirmation_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="BillingConfirmation_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'BillingConfirmation_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    ElsIf p_web.GSV('job:Chargeable_Job') = 'YES'
        p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:CChargeType')
    ElsIf p_web.GSV('job:Warranty_job') = 'YES'
        p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:WChargeType')
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_BillingConfirmation')>0,p_web.GSV('showtab_BillingConfirmation'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_BillingConfirmation'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_BillingConfirmation') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_BillingConfirmation'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_BillingConfirmation')>0,p_web.GSV('showtab_BillingConfirmation'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_BillingConfirmation') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
      If p_web.GSV('job:Chargeable_Job') = 'YES'
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Chargeable') & ''''
      End
      If p_web.GSV('job:Warranty_job') = 'YES'
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Warranty') & ''''
      End
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_BillingConfirmation_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_BillingConfirmation')>0,p_web.GSV('showtab_BillingConfirmation'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"BillingConfirmation",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_BillingConfirmation')>0,p_web.GSV('showtab_BillingConfirmation'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_BillingConfirmation_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('BillingConfirmation') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('BillingConfirmation')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
  If p_web.GSV('job:Chargeable_Job') = 'YES'
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Chargeable')&'</a></h3>' & CRLF & p_web.DivHeader('tab_BillingConfirmation0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_BillingConfirmation0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_BillingConfirmation0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_BillingConfirmation0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Chargeable')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_BillingConfirmation0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Chargeable')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_BillingConfirmation0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Chargeable')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_BillingConfirmation0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:CChargeType
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:CChargeType
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:CChargeType
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:CRepairType
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:CRepairType
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:CRepairType
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:CConfirmRepairType
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:CConfirmRepairType
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:CConfirmRepairType
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
  end
GenerateTab1  Routine
  If p_web.GSV('job:Warranty_job') = 'YES'
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Warranty')&'</a></h3>' & CRLF & p_web.DivHeader('tab_BillingConfirmation1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_BillingConfirmation1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_BillingConfirmation1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_BillingConfirmation1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Warranty')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_BillingConfirmation1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Warranty')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_BillingConfirmation1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Warranty')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_BillingConfirmation1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:WChargeType
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:WChargeType
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:WChargeType
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:WRepairType
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:WRepairType
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:WRepairType
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:WConfirmRepairType
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:WConfirmRepairType
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:WConfirmRepairType
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
  end


Prompt::tmp:CChargeType  Routine
  packet = clip(packet) & p_web.DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:CChargeType') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Charge Type'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:CChargeType  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:CChargeType = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:CChargeType = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:CChargeType  ! copies value to session value if valid.
  do Value::tmp:CChargeType
  do SendAlert
  do Comment::tmp:CChargeType ! allows comment style to be updated.
  do Value::tmp:CRepairType  !1

ValidateValue::tmp:CChargeType  Routine
    If not (1=0)
    tmp:CChargeType = Upper(tmp:CChargeType)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:CChargeType',tmp:CChargeType).
    End

Value::tmp:CChargeType  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:CChargeType') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,p_web.site.style.formselect,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:retrying
    tmp:CChargeType = p_web.RestoreValue('tmp:CChargeType')
    do ValidateValue::tmp:CChargeType
    If tmp:CChargeType:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- DROPLIST ---
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:CChargeType'',''billingconfirmation_tmp:cchargetype_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:CChargeType')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('Job:ViewOnly') = 1,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:CChargeType',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
  loc:fieldclass = ''
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('tmp:CChargeType') = 0
    p_web.SetSessionValue('tmp:CChargeType','')
  end
    packet = clip(packet) & p_web.CreateOption('-------------------------------------','',choose('' = p_web.getsessionvalue('tmp:CChargeType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(CHARTYPE)
  bind(cha:Record)
  p_web._OpenFile(REPTYDEF)
  bind(rtd:Record)
  p_web._OpenFile(USERS)
  bind(use:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(tmp:CChargeType_OptionView)
  tmp:CChargeType_OptionView{prop:filter} = p_web.CleanFilter(tmp:CChargeType_OptionView,'upper(cha:Warranty ) <> ''YES''')
  tmp:CChargeType_OptionView{prop:order} = p_web.CleanFilter(tmp:CChargeType_OptionView,'UPPER(cha:Charge_Type)')
  Set(tmp:CChargeType_OptionView)
  Loop
    Next(tmp:CChargeType_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('tmp:CChargeType') = 0
      p_web.SetSessionValue('tmp:CChargeType',cha:Charge_Type)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,cha:Charge_Type,choose(cha:Charge_Type = p_web.getsessionvalue('tmp:CChargeType')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(tmp:CChargeType_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(CHARTYPE)
  p_Web._CloseFile(REPTYDEF)
  p_Web._CloseFile(USERS)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:CChargeType  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:CChargeType:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:CChargeType') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:CRepairType  Routine
  packet = clip(packet) & p_web.DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:CRepairType') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Repair Type'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:CRepairType  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:CRepairType = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:CRepairType = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:CRepairType  ! copies value to session value if valid.
  do Value::tmp:CRepairType
  do SendAlert
  do Comment::tmp:CRepairType ! allows comment style to be updated.

ValidateValue::tmp:CRepairType  Routine
    If not (1=0)
    tmp:CRepairType = Upper(tmp:CRepairType)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:CRepairType',tmp:CRepairType).
    End

Value::tmp:CRepairType  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:CRepairType') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,p_web.site.style.formselect,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:retrying
    tmp:CRepairType = p_web.RestoreValue('tmp:CRepairType')
    do ValidateValue::tmp:CRepairType
    If tmp:CRepairType:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- DROPLIST ---
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:CRepairType'',''billingconfirmation_tmp:crepairtype_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('ReadOnly:RepairType') = 1 OR p_web.GSV('Job:ViewOnly') = 1,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:CRepairType',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
  loc:fieldclass = ''
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('tmp:CRepairType') = 0
    p_web.SetSessionValue('tmp:CRepairType','')
  end
    packet = clip(packet) & p_web.CreateOption('-------------------------------------','',choose('' = p_web.getsessionvalue('tmp:CRepairType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(CHARTYPE)
  bind(cha:Record)
  p_web._OpenFile(REPTYDEF)
  bind(rtd:Record)
  p_web._OpenFile(USERS)
  bind(use:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(tmp:CRepairType_OptionView)
  tmp:CRepairType_OptionView{prop:filter} = p_web.CleanFilter(tmp:CRepairType_OptionView,'upper(rtd:manufacturer) = upper(''' & p_web.GSV('job:manufacturer') & ''') and upper(rtd:chargeable) = ''YES'' and rtd:NotAvailable = 0 and rtd:SkillLevel <= ' & p_web.GSV('locEngineerSkillLevel'))
  tmp:CRepairType_OptionView{prop:order} = p_web.CleanFilter(tmp:CRepairType_OptionView,'UPPER(rtd:Repair_Type)')
  Set(tmp:CRepairType_OptionView)
  Loop
    Next(tmp:CRepairType_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('tmp:CRepairType') = 0
      p_web.SetSessionValue('tmp:CRepairType',rtd:Repair_Type)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,rtd:Repair_Type,choose(rtd:Repair_Type = p_web.getsessionvalue('tmp:CRepairType')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(tmp:CRepairType_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(CHARTYPE)
  p_Web._CloseFile(REPTYDEF)
  p_Web._CloseFile(USERS)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:CRepairType  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:CRepairType:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:CRepairType') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:CConfirmRepairType  Routine
  packet = clip(packet) & p_web.DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:CConfirmRepairType') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Confirm Repair Type'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:CConfirmRepairType  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:CConfirmRepairType = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    tmp:CConfirmRepairType = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:CConfirmRepairType  ! copies value to session value if valid.
  do Value::tmp:CConfirmRepairType
  do SendAlert
  do Comment::tmp:CConfirmRepairType ! allows comment style to be updated.

ValidateValue::tmp:CConfirmRepairType  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:CConfirmRepairType',tmp:CConfirmRepairType).
    End

Value::tmp:CConfirmRepairType  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:CConfirmRepairType') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If loc:retrying
    tmp:CConfirmRepairType = p_web.RestoreValue('tmp:CConfirmRepairType')
    do ValidateValue::tmp:CConfirmRepairType
    If tmp:CConfirmRepairType:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- CHECKBOX --- tmp:CConfirmRepairType
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:CConfirmRepairType'',''billingconfirmation_tmp:cconfirmrepairtype_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('Job:ViewOnly') = 1,'disabled','')
  If p_web.GetSessionValue('tmp:CConfirmRepairType') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','tmp:CConfirmRepairType',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:CConfirmRepairType  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:CConfirmRepairType:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:CConfirmRepairType') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:WChargeType  Routine
  packet = clip(packet) & p_web.DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:WChargeType') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Charge Type'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:WChargeType  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:WChargeType = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:WChargeType = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:WChargeType  ! copies value to session value if valid.
  do Value::tmp:WChargeType
  do SendAlert
  do Comment::tmp:WChargeType ! allows comment style to be updated.
  do Value::tmp:WRepairType  !1

ValidateValue::tmp:WChargeType  Routine
    If not (1=0)
    tmp:WChargeType = Upper(tmp:WChargeType)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:WChargeType',tmp:WChargeType).
    End

Value::tmp:WChargeType  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:WChargeType') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,p_web.site.style.formselect,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:retrying
    tmp:WChargeType = p_web.RestoreValue('tmp:WChargeType')
    do ValidateValue::tmp:WChargeType
    If tmp:WChargeType:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- DROPLIST ---
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:WChargeType'',''billingconfirmation_tmp:wchargetype_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:WChargeType')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('Job:ViewOnly') = 1,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:WChargeType',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
  loc:fieldclass = ''
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('tmp:WChargeType') = 0
    p_web.SetSessionValue('tmp:WChargeType','')
  end
    packet = clip(packet) & p_web.CreateOption('-------------------------------------','',choose('' = p_web.getsessionvalue('tmp:WChargeType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(CHARTYPE)
  bind(cha:Record)
  p_web._OpenFile(REPTYDEF)
  bind(rtd:Record)
  p_web._OpenFile(USERS)
  bind(use:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(tmp:WChargeType_OptionView)
  tmp:WChargeType_OptionView{prop:filter} = p_web.CleanFilter(tmp:WChargeType_OptionView,'cha:Warranty = ''YES''')
  tmp:WChargeType_OptionView{prop:order} = p_web.CleanFilter(tmp:WChargeType_OptionView,'UPPER(cha:Charge_Type)')
  Set(tmp:WChargeType_OptionView)
  Loop
    Next(tmp:WChargeType_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('tmp:WChargeType') = 0
      p_web.SetSessionValue('tmp:WChargeType',cha:Charge_Type)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,cha:Charge_Type,choose(cha:Charge_Type = p_web.getsessionvalue('tmp:WChargeType')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(tmp:WChargeType_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(CHARTYPE)
  p_Web._CloseFile(REPTYDEF)
  p_Web._CloseFile(USERS)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:WChargeType  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:WChargeType:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:WChargeType') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:WRepairType  Routine
  packet = clip(packet) & p_web.DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:WRepairType') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Repair Type'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:WRepairType  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:WRepairType = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:WRepairType = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:WRepairType  ! copies value to session value if valid.
  do Value::tmp:WRepairType
  do SendAlert
  do Comment::tmp:WRepairType ! allows comment style to be updated.

ValidateValue::tmp:WRepairType  Routine
    If not (1=0)
    tmp:WRepairType = Upper(tmp:WRepairType)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:WRepairType',tmp:WRepairType).
    End

Value::tmp:WRepairType  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:WRepairType') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,p_web.site.style.formselect,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:retrying
    tmp:WRepairType = p_web.RestoreValue('tmp:WRepairType')
    do ValidateValue::tmp:WRepairType
    If tmp:WRepairType:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- DROPLIST ---
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:WRepairType'',''billingconfirmation_tmp:wrepairtype_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('ReadOnly:RepairType') = 1 OR p_web.GSV('Job:ViewOnly') = 1,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:WRepairType',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
  loc:fieldclass = ''
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('tmp:WRepairType') = 0
    p_web.SetSessionValue('tmp:WRepairType','')
  end
    packet = clip(packet) & p_web.CreateOption('-------------------------------------','',choose('' = p_web.getsessionvalue('tmp:WRepairType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(CHARTYPE)
  bind(cha:Record)
  p_web._OpenFile(REPTYDEF)
  bind(rtd:Record)
  p_web._OpenFile(USERS)
  bind(use:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(tmp:WRepairType_OptionView)
  tmp:WRepairType_OptionView{prop:filter} = p_web.CleanFilter(tmp:WRepairType_OptionView,'upper(rtd:manufacturer) = upper(''' & p_web.GSV('job:manufacturer') & ''') and upper(rtd:warranty) = ''YES'' and rtd:NotAvailable = 0 and rtd:SkillLevel <= ' & p_web.GSV('locEngineerSkillLevel'))
  tmp:WRepairType_OptionView{prop:order} = p_web.CleanFilter(tmp:WRepairType_OptionView,'UPPER(rtd:Repair_Type)')
  Set(tmp:WRepairType_OptionView)
  Loop
    Next(tmp:WRepairType_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('tmp:WRepairType') = 0
      p_web.SetSessionValue('tmp:WRepairType',rtd:Repair_Type)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,rtd:Repair_Type,choose(rtd:Repair_Type = p_web.getsessionvalue('tmp:WRepairType')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(tmp:WRepairType_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(CHARTYPE)
  p_Web._CloseFile(REPTYDEF)
  p_Web._CloseFile(USERS)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:WRepairType  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:WRepairType:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:WRepairType') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:WConfirmRepairType  Routine
  packet = clip(packet) & p_web.DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:WConfirmRepairType') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Confirm Repair Type'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:WConfirmRepairType  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:WConfirmRepairType = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    tmp:WConfirmRepairType = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:WConfirmRepairType  ! copies value to session value if valid.
  do Value::tmp:WConfirmRepairType
  do SendAlert
  do Comment::tmp:WConfirmRepairType ! allows comment style to be updated.

ValidateValue::tmp:WConfirmRepairType  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:WConfirmRepairType',tmp:WConfirmRepairType).
    End

Value::tmp:WConfirmRepairType  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:WConfirmRepairType') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If loc:retrying
    tmp:WConfirmRepairType = p_web.RestoreValue('tmp:WConfirmRepairType')
    do ValidateValue::tmp:WConfirmRepairType
    If tmp:WConfirmRepairType:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- CHECKBOX --- tmp:WConfirmRepairType
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:WConfirmRepairType'',''billingconfirmation_tmp:wconfirmrepairtype_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('Job:ViewOnly') = 1,'disabled','')
  If p_web.GetSessionValue('tmp:WConfirmRepairType') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','tmp:WConfirmRepairType',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:WConfirmRepairType  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:WConfirmRepairType:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:WConfirmRepairType') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('BillingConfirmation_nexttab_' & 0)
    tmp:CChargeType = p_web.GSV('tmp:CChargeType')
    do ValidateValue::tmp:CChargeType
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:CChargeType
      !do SendAlert
      do Comment::tmp:CChargeType ! allows comment style to be updated.
      !exit
    End
    tmp:CRepairType = p_web.GSV('tmp:CRepairType')
    do ValidateValue::tmp:CRepairType
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:CRepairType
      !do SendAlert
      do Comment::tmp:CRepairType ! allows comment style to be updated.
      !exit
    End
    tmp:CConfirmRepairType = p_web.GSV('tmp:CConfirmRepairType')
    do ValidateValue::tmp:CConfirmRepairType
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:CConfirmRepairType
      !do SendAlert
      do Comment::tmp:CConfirmRepairType ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('BillingConfirmation_nexttab_' & 1)
    tmp:WChargeType = p_web.GSV('tmp:WChargeType')
    do ValidateValue::tmp:WChargeType
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:WChargeType
      !do SendAlert
      do Comment::tmp:WChargeType ! allows comment style to be updated.
      !exit
    End
    tmp:WRepairType = p_web.GSV('tmp:WRepairType')
    do ValidateValue::tmp:WRepairType
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:WRepairType
      !do SendAlert
      do Comment::tmp:WRepairType ! allows comment style to be updated.
      !exit
    End
    tmp:WConfirmRepairType = p_web.GSV('tmp:WConfirmRepairType')
    do ValidateValue::tmp:WConfirmRepairType
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:WConfirmRepairType
      !do SendAlert
      do Comment::tmp:WConfirmRepairType ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_BillingConfirmation_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('BillingConfirmation_tab_' & 0)
    do GenerateTab0
  of lower('BillingConfirmation_tmp:CChargeType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:CChargeType
      of event:timer
        do Value::tmp:CChargeType
        do Comment::tmp:CChargeType
      else
        do Value::tmp:CChargeType
      end
  of lower('BillingConfirmation_tmp:CRepairType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:CRepairType
      of event:timer
        do Value::tmp:CRepairType
        do Comment::tmp:CRepairType
      else
        do Value::tmp:CRepairType
      end
  of lower('BillingConfirmation_tmp:CConfirmRepairType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:CConfirmRepairType
      of event:timer
        do Value::tmp:CConfirmRepairType
        do Comment::tmp:CConfirmRepairType
      else
        do Value::tmp:CConfirmRepairType
      end
  of lower('BillingConfirmation_tab_' & 1)
    do GenerateTab1
  of lower('BillingConfirmation_tmp:WChargeType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:WChargeType
      of event:timer
        do Value::tmp:WChargeType
        do Comment::tmp:WChargeType
      else
        do Value::tmp:WChargeType
      end
  of lower('BillingConfirmation_tmp:WRepairType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:WRepairType
      of event:timer
        do Value::tmp:WRepairType
        do Comment::tmp:WRepairType
      else
        do Value::tmp:WRepairType
      end
  of lower('BillingConfirmation_tmp:WConfirmRepairType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:WConfirmRepairType
      of event:timer
        do Value::tmp:WConfirmRepairType
        do Comment::tmp:WConfirmRepairType
      else
        do Value::tmp:WConfirmRepairType
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('BillingConfirmation_form:ready_',1)

  p_web.SetSessionValue('BillingConfirmation_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_BillingConfirmation',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('BillingConfirmation_form:ready_',1)
  p_web.SetSessionValue('BillingConfirmation_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_BillingConfirmation',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('BillingConfirmation_form:ready_',1)
  p_web.SetSessionValue('BillingConfirmation_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('BillingConfirmation:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('BillingConfirmation_form:ready_',1)
  p_web.SetSessionValue('BillingConfirmation_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('BillingConfirmation:Primed',0)
  p_web.setsessionvalue('showtab_BillingConfirmation',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
  If p_web.GSV('job:Chargeable_Job') = 'YES'
      If not (1=0)
        If not (p_web.GSV('Job:ViewOnly') = 1)
          If p_web.IfExistsValue('tmp:CChargeType')
            tmp:CChargeType = p_web.GetValue('tmp:CChargeType')
          End
        End
      End
      If not (1=0)
        If not (p_web.GSV('ReadOnly:RepairType') = 1 OR p_web.GSV('Job:ViewOnly') = 1)
          If p_web.IfExistsValue('tmp:CRepairType')
            tmp:CRepairType = p_web.GetValue('tmp:CRepairType')
          End
        End
      End
      If not (1=0)
        If not (p_web.GSV('Job:ViewOnly') = 1)
          If p_web.IfExistsValue('tmp:CConfirmRepairType') = 0
            p_web.SetValue('tmp:CConfirmRepairType',0)
            tmp:CConfirmRepairType = 0
          Else
            tmp:CConfirmRepairType = p_web.GetValue('tmp:CConfirmRepairType')
          End
        End
      End
  End
  If p_web.GSV('job:Warranty_job') = 'YES'
      If not (1=0)
        If not (p_web.GSV('Job:ViewOnly') = 1)
          If p_web.IfExistsValue('tmp:WChargeType')
            tmp:WChargeType = p_web.GetValue('tmp:WChargeType')
          End
        End
      End
      If not (1=0)
        If not (p_web.GSV('ReadOnly:RepairType') = 1 OR p_web.GSV('Job:ViewOnly') = 1)
          If p_web.IfExistsValue('tmp:WRepairType')
            tmp:WRepairType = p_web.GetValue('tmp:WRepairType')
          End
        End
      End
      If not (1=0)
        If not (p_web.GSV('Job:ViewOnly') = 1)
          If p_web.IfExistsValue('tmp:WConfirmRepairType') = 0
            p_web.SetValue('tmp:WConfirmRepairType',0)
            tmp:WConfirmRepairType = 0
          Else
            tmp:WConfirmRepairType = p_web.GetValue('tmp:WConfirmRepairType')
          End
        End
      End
  End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('BillingConfirmation_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  if (p_web.GSV('JobCompleteProcess') = 1)
      if (p_web.GSV('job:Chargeable_Job') = 'YES')
          if (p_web.GSV('tmp:CConfirmRepairType') = 0)
              loc:alert = 'You must confirm the chargeable repair type.'
              loc:invalid = 'tmp:CConfirmRepairType'
              exit
          end ! if (p_web.GSV('tmp:CConfirmRepairType') = 0)
      end ! if (p_web.GSV('job:Chargeable_Job') = 'YES')
  
      if (p_web.GSV('job:Warranty_Job') = 'YES')
          if (p_web.GSV('tmp:WConfirmRepairType') = 0)
              loc:alert = 'You must confirm the warranty repair type.'
              loc:invalid = 'tmp:WConfirmRepairType'
              exit
          end ! if (p_web.GSV('tmp:CConfirmRepairType') = 0)
      end ! if (p_web.GSV('job:Chargeable_Job') = 'YES')
  end ! if (p_web.GSV('JobCompleteProcess') = 1)
  
  IF (vod.RepairTypesNoParts(p_web.GSV('job:Chargeable_Job'), |
      p_web.GSV('job:Warranty_Job'), |
      p_web.GSV('job:Manufacturer'), |
      p_web.GSV('tmp:CRepairType'), |
      p_web.GSV('tmp:WRepairType')))
      ! #11762 Repair Type does not allow parts.
      ! Check if there are any (Bryan: 24/11/2010)
  
      IF (vod.JobHasSparesAttached(p_web.GSV('job:Ref_Number',), |
          p_web.GSV('job:Estimate'), |
          p_web.GSV('job:Chargeable_Job'), |
          p_web.GSV('job:Warranty_Job')))
          loc:Invalid = 'tmp:ConfirmRepairType'
          loc:Alert = 'The selected Repair Type(s) require that there are no parts attached to the job.'
          EXIT
      END
  END
  
  
  p_web.DeleteSessionValue('BillingConfirmation_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
  If p_web.GSV('job:Chargeable_Job') = 'YES'
    loc:InvalidTab += 1
    do ValidateValue::tmp:CChargeType
    If loc:Invalid then exit.
    do ValidateValue::tmp:CRepairType
    If loc:Invalid then exit.
    do ValidateValue::tmp:CConfirmRepairType
    If loc:Invalid then exit.
  End
  ! tab = 2
  If p_web.GSV('job:Warranty_job') = 'YES'
    loc:InvalidTab += 1
    do ValidateValue::tmp:WChargeType
    If loc:Invalid then exit.
    do ValidateValue::tmp:WRepairType
    If loc:Invalid then exit.
    do ValidateValue::tmp:WConfirmRepairType
    If loc:Invalid then exit.
  End
  ! The following fields are not on the form, but need to be checked anyway.
  if (p_web.GSV('job:Chargeable_Job') = 'YES')
      p_web.SSV('job:Charge_Type',p_web.GSV('tmp:CChargeType'))
      p_web.SSV('job:Repair_Type',p_web.GSV('tmp:CRepairType'))
  
      if (p_web.GSV('job:Estimate') <> 'YES') ! #11659 No need to set to estimate. If it already is. (Bryan: 31/08/2010)
          CASE vod.EstimateRequired(p_web.GSV('job:Charge_Type'),p_web.GSV('job:Account_Number'))
          OF 1 ! Make Esimate
              p_web.SSV('job:Estimate','YES')
          OF 2 ! Don't make esimate
              p_web.SSV('job:Estimate','NO')
          ELSE
          END
      END
  
  end ! if (p_web.GSV('job:Chargeable_Job') = 'YES')
  
  if (p_web.GSV('job:Warranty_job') = 'YES')
      if (p_web.GSV('job:Warranty_Charge_Type') <> p_web.GSV('tmp:WChargeType') Or |
          p_web.GSV('job:Repair_Type_Warranty') <> p_web.GSV('tmp:WRepairType'))
  
          p_web.SSV('job:Warranty_Charge_Type',p_web.GSV('tmp:WChargeType'))
          p_web.SSV('job:Repair_Type_Warranty',p_web.GSV('tmp:WRepairType'))
          p_web.SSV('JobPricingRoutine:ForceWarranty',1)
          JobPricingRoutine(p_web)
      end !
  end ! if (p_web.GSV('job:Warranty_job') = 'YES')
  
  p_web.SSV('jobe:COverwriteRepairType',p_web.GSV('tmp:CConfirmRepairType'))
  p_web.SSV('jobe:WOverwriteRepairType',p_web.GSV('tmp:WConfirmRepairType'))
  
  
  CalculateBilling(p_web)
  
  p_web.DeleteSessionValue('passedURL')
  
  
  
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('BillingConfirmation:Primed',0)
  p_web.StoreValue('tmp:CChargeType')
  p_web.StoreValue('tmp:CRepairType')
  p_web.StoreValue('tmp:CConfirmRepairType')
  p_web.StoreValue('tmp:WChargeType')
  p_web.StoreValue('tmp:WRepairType')
  p_web.StoreValue('tmp:WConfirmRepairType')

