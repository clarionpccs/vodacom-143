

   MEMBER('WebServer_Phase4.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSE043.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSE003.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE004.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE015.INC'),ONCE        !Req'd for module callout resolution
                     END


FormEngineeringOption PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locUserPassword      STRING(30)                            !
locEngineeringOption STRING(30)                            !
locExchangeManufacturer STRING(30)                         !
locExchangeModelNumber STRING(30)                          !
locExchangeNotes     STRING(255)                           !
locNewEngineeringOption STRING(20)                         !
locSplitJob          BYTE                                  !
FilesOpened     Long
USERS::State  USHORT
ACCAREAS::State  USHORT
MODELNUM::State  USHORT
JOBS::State  USHORT
MANUFACT::State  USHORT
STOCK::State  USHORT
WARPARTS::State  USHORT
PARTS::State  USHORT
STOMODEL::State  USHORT
AUDIT::State  USHORT
STOCKALL::State  USHORT
JOBSE::State  USHORT
JOBSENG::State  USHORT
REPTYDEF::State  USHORT
WEBJOB::State  USHORT
EXCHOR48::State  USHORT
locUserPassword:IsInvalid  Long
locErrorMessage:IsInvalid  Long
locEngineeringOption:IsInvalid  Long
locNewEngineeringOption:IsInvalid  Long
text:ExchangeOrder:IsInvalid  Long
job:DOP:IsInvalid  Long
locExchangeManufacturer:IsInvalid  Long
locExchangeModelNumber:IsInvalid  Long
locExchangeNotes:IsInvalid  Long
locWarningMessage:IsInvalid  Long
locSplitJob:IsInvalid  Long
button:CreateOrder:IsInvalid  Long
button:PrintOrder:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormEngineeringOption')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'FormEngineeringOption_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('FormEngineeringOption','Change')
    p_web.DivHeader('FormEngineeringOption',p_web.combine(p_web.site.style.formdiv,))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('FormEngineeringOption') = 0
        p_web.AddPreCall('FormEngineeringOption')
        p_web.DivHeader('popup_FormEngineeringOption','nt-hidden')
        p_web.DivHeader('FormEngineeringOption',p_web.combine(p_web.site.style.formdiv,))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_FormEngineeringOption_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_FormEngineeringOption_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormEngineeringOption',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_FormEngineeringOption',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormEngineeringOption',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_FormEngineeringOption',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormEngineeringOption',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormEngineeringOption',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_FormEngineeringOption',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormEngineeringOption',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_FormEngineeringOption',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormEngineeringOption',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormEngineeringOption',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormEngineeringOption',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('FormEngineeringOption')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
add:ExchangeOrderPart         Routine
data
local:foundUnit             Byte(0)
code
    !Check to see if a part already exists

    If p_web.GSV('job:Warranty_Job') = 'YES'
        Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
        wpr:Ref_Number  = p_web.GSV('job:Ref_Number')
        wpr:Part_Number = 'EXCH'
        Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
        Loop
            If Access:WARPARTS.NEXT()
               Break
            End !If
            If wpr:Ref_Number  <> p_web.GSV('job:Ref_Number')      |
            Or wpr:Part_Number <> 'EXCH'      |
                Then Break.  ! End If
            local:FoundUnit = True
            If local:FoundUnit
                wpr:Status  = 'ORD'
                Access:WARPARTS.Update()

                Access:STOCKALL.Clearkey(stl:PartRecordTypeKey)
                stl:PartType    = 'WAR'
                stl:PartRecordNumber    = wpr:Record_Number
                if (Access:STOCKALL.TryFetch(stl:PartRecordTypeKey) = Level:Benign)
                    ! Found
                    relate:STOCKALL.delete(0)
                else ! if (Access:STOCKALL.TryFetch(stl:PartRecordTypeKey) = Level:Benign)
                    ! Error
                end ! if (Access:STOCKALL.TryFetch(stl:PartRecordTypeKey) = Level:Benign)
                Break
            End !If local:FoundUnit
        End !Loop

    End !If job:Warranty_Job = 'YES'

    If local:FoundUnit = True
        If p_web.GSV('ob:Chargeable_Job') = 'YES'
            Access:PARTS.ClearKey(par:Part_Number_Key)
            par:Ref_Number  = p_web.GSV('job:Ref_Number')
            par:Part_Number = 'EXCH'
            Set(par:Part_Number_Key,par:Part_Number_Key)
            Loop
                If Access:PARTS.NEXT()
                   Break
                End !If
                If par:Ref_Number  <> p_web.GSV('job:Ref_Number')      |
                Or par:Part_Number <> 'EXCH'      |
                    Then Break.  ! End If
                local:FoundUnit = True
                If local:FoundUnit
                    par:Status  = 'ORD'
                    Access:PARTS.Update()
                    Access:STOCKALL.Clearkey(stl:PartRecordTypeKey)
                    stl:PartType    = 'CHA'
                    stl:PartRecordNumber    = par:Record_Number
                    if (Access:STOCKALL.TryFetch(stl:PartRecordTypeKey) = Level:Benign)
                        ! Found
                        relate:STOCKALL.delete(0)
                    else ! if (Access:STOCKALL.TryFetch(stl:PartRecordTypeKey) = Level:Benign)
                        ! Error
                    end ! if (Access:STOCKALL.TryFetch(stl:PartRecordTypeKey) = Level:Benign)
                    Break
                End !If local:FoundUnit
            End !Loop
        End !If job:Chargeable_Job = 'YES'
    End !If FoundEXCHPart# = 0

    If local:FoundUnit = False
        If p_web.GSV('job:Engineer') = ''
            Access:USERS.Clearkey(use:Password_Key)
            use:Password    = p_web.GSV('BookingUserPassword')
            If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                !Found

            Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                !Error
            End !If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        Else !If job:Engineer = ''
            Access:USERS.Clearkey(use:User_Code_Key)
            use:User_Code   = p_web.GSV('job:Engineer')
            If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign

            End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
        End !If job:Engineer = ''

         !Found
        Access:STOCK.Clearkey(sto:Location_Key)
        sto:Location    = use:Location
        sto:Part_Number = 'EXCH'
        If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
            !Found
            Access:LOCATION.Clearkey(loc:Location_Key)
            loc:Location    = sto:Location
            If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
                !Found
            Else ! If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
                !Error
            End !If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
            If p_web.GSV('job:Warranty_Job') = 'YES'
               get(warparts,0)
               if access:warparts.primerecord() = level:benign
                   wpr:PArt_Ref_Number      = sto:Ref_Number
                   wpr:ref_number            = p_web.GSV('job:ref_number')
                   wpr:adjustment            = 'YES'
                   wpr:part_number           = 'EXCH'
                   wpr:description           = p_web.GSV('job:Manufacturer') & ' EXCHANGE UNIT'
                   wpr:quantity              = 1
                   wpr:warranty_part         = 'NO'
                   wpr:exclude_from_order    = 'YES'
                   wpr:PartAllocated         = 1
                   wpr:Status                = 'ORD'
                   wpr:ExchangeUnit          = True
                   wpr:SecondExchangeUnit    = 0
                   If sto:Assign_Fault_Codes = 'YES'
                       !Try and get the fault codes. This key should get the only record
                       Access:STOMODEL.ClearKey(stm:Ref_Part_Description)
                       stm:Ref_Number  = sto:Ref_Number
                       stm:Part_Number = sto:Part_Number
                       stm:Location    = sto:Location
                       stm:Description = sto:Description
                       If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                           !Found
                           wpr:Fault_Code1  = stm:FaultCode1
                           wpr:Fault_Code2  = stm:FaultCode2
                           wpr:Fault_Code3  = stm:FaultCode3
                           wpr:Fault_Code4  = stm:FaultCode4
                           wpr:Fault_Code5  = stm:FaultCode5
                           wpr:Fault_Code6  = stm:FaultCode6
                           wpr:Fault_Code7  = stm:FaultCode7
                           wpr:Fault_Code8  = stm:FaultCode8
                           wpr:Fault_Code9  = stm:FaultCode9
                           wpr:Fault_Code10 = stm:FaultCode10
                           wpr:Fault_Code11 = stm:FaultCode11
                           wpr:Fault_Code12 = stm:FaultCode12
                       Else!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                       !Error
                       !Assert(0,'<13,10>Fetch Error<13,10>')
                       End!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                   end !If sto:Assign_Fault_Codes = 'YES'
                   if access:warparts.insert()
                       access:warparts.cancelautoinc()
                   end
                End !If Prime

             Else !If job:Warranty_Job = 'YES'
                If p_web.GSV('job:Chargeable_Job') = 'YES'
                    get(parts,0)
                    if access:parts.primerecord() = level:benign
                        !message('At break2')
                        par:PArt_Ref_Number      = sto:Ref_Number
                        par:ref_number            = p_web.GSV('job:ref_number')
                        par:adjustment            = 'YES'
                        par:part_number           = 'EXCH'
                        par:description           = p_web.GSV('job:Manufacturer') & ' EXCHANGE UNIT'
                        par:quantity              = 1
                        par:warranty_part         = 'NO'
                        par:exclude_from_order    = 'YES'
                        par:PartAllocated         = 1
                        par:Status                = 'ORD'
                        par:ExchangeUnit          = True
                        par:SecondExchangeUnit    = 0
                        If sto:Assign_Fault_Codes = 'YES'
                           !Try and get the fault codes. This key should get the only record
                           Access:STOMODEL.ClearKey(stm:Ref_Part_Description)
                           stm:Ref_Number  = sto:Ref_Number
                           stm:Part_Number = sto:Part_Number
                           stm:Location    = sto:Location
                           stm:Description = sto:Description
                           If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                               !Found
                               par:Fault_Code1  = stm:FaultCode1
                               par:Fault_Code2  = stm:FaultCode2
                               par:Fault_Code3  = stm:FaultCode3
                               par:Fault_Code4  = stm:FaultCode4
                               par:Fault_Code5  = stm:FaultCode5
                               par:Fault_Code6  = stm:FaultCode6
                               par:Fault_Code7  = stm:FaultCode7
                               par:Fault_Code8  = stm:FaultCode8
                               par:Fault_Code9  = stm:FaultCode9
                               par:Fault_Code10 = stm:FaultCode10
                               par:Fault_Code11 = stm:FaultCode11
                               par:Fault_Code12 = stm:FaultCode12
                           Else!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                               !Error
                               !Assert(0,'<13,10>Fetch Error<13,10>')
                           End!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                        End !If sto:Assign_Fault_Codes = 'YES'
                        if access:parts.insert()
                            access:parts.cancelautoinc()
                        end
                    End !If access:Prime

                End !If job:Chargeable_Job = 'YES'
             End !If job:Warranty_Job = 'YES'

        Else ! If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
             !Error
        End !If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
    End !If FoundEXCHPart# = 0
create:ExchangeOrder        Routine
    if (Access:EXCHOR48.PrimeRecord() = Level:Benign)
        ex4:Location    = p_web.GSV('Default:SiteLocation')
        ex4:Manufacturer    = p_web.GSV('locExchangeManufacturer')
        ex4:ModelNumber    = p_web.GSV('locExchangeModelNumber')
        ex4:Received    = 0
        ex4:Notes    = p_web.GSV('locExchangeNotes')
        ex4:JobNumber    = p_web.GSV('wob:RefNumber')

        if (Access:EXCHOR48.TryInsert() = Level:Benign)
                    ! Inserted

            p_web.SSV('jobe:Engineer48HourOption',1)
            p_web.SSV('locEngineeringOption','48 Hour Exchange')

            p_web.SSV('AddToAudit:Type','JOB')
            p_web.SSV('AddToAudit:Action','48 HOUR EXCHANGE ORDER CREATED')
            p_web.SSV('AddToAudit:Notes','MANUFACTURER: ' & p_web.GSV('locExchangeManufacturer') & |
                '<13,10>MODEL NUMBER: ' & p_web.GSV('locExchangeModelNumber'))
            AddToAudit(p_web)

            p_web.SSV('AddToAudit:Type','JOB')
            p_web.SSV('AddToAudit:Action','ENGINEERING OPTION SELECTED: 48 HOUR EXCHANGE')
            p_web.SSV('AddToAudit:Notes','')
            AddToAudit(p_web)




            GetStatus(360,0,'EXC',p_web)



            GetStatus(355,0,'JOB',p_web)

! #11939 Don't force split (even though original spec states you should) (Bryan: 01/03/2011)
!            !If warranty job, auto change to spilt chargeable - 3876 (DBH: 29-03-2004)
!            if (p_web.GSV('job:Warranty_Job') = 'YES' and p_web.GSV('job:Chargeable_Job') <> 'YES')
!                p_web.SSV('job:Chargeable_Job','YES')
!                if p_web.GSV('BookingSite') = 'RRC'
!                    p_web.SSV('job:Charge_Type','NON-WARR SERVICE FEE')
!                else ! if p_web.GSV('BookingSite') = 'RRC'
!                    p_web.SSV('job:Charge_Type','NON-WARRANTY')
!                end ! if p_web.GSV('BookingSite') = 'RRC'
!                p_web.SSV('job:Repair_Type','48-HOUR SERVICE FEE')
!            end ! if ( p_web.GSV('job:Warranty_Job') = 'YES' and p_web.GSV('job:Chargeable_Job') <> 'YES')

            CASE (p_web.GSV('locSplitJob'))
            OF 1 ! Chargeable Only
                p_web.SSV('job:Chargeable_Job','YES')
                p_web.SSV('job:Warranty_Job','NO')
            OF 2 ! Warranty Only
                p_web.SSV('job:Chargeable_Job','NO')
                p_web.SSV('job:Warranty_Job','YES')
            OF 3 ! Split
                p_web.SSV('job:Chargeable_Job','YES')
                p_web.SSV('job:Warranty_Job','YES')
            END

            IF (p_web.GSV('job:Chargeable_Job') = 'YES')
                if p_web.GSV('BookingSite') = 'RRC'
                    p_web.SSV('job:Charge_Type','NON-WARR SERVICE FEE')
                else ! if p_web.GSV('BookingSite') = 'RRC'
                    p_web.SSV('job:Charge_Type','NON-WARRANTY')
                end ! if p_web.GSV('BookingSite') = 'RRC'
                p_web.SSV('job:Repair_Type','48-HOUR SERVICE FEE')
            END

            do add:ExchangeOrderPart

            Access:JOBSE.Clearkey(jobe:RefNumberKey)
            jobe:RefNumber    = p_web.GSV('wob:RefNumber')
            if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
                ! Found
                p_web.SessionQueueToFile(JOBSE)
                Access:JOBSE.TryUpdate()
            else ! if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
                ! Error
            end ! if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)

            Access:JOBS.Clearkey(job:Ref_Number_Key)
            job:Ref_Number    = p_web.GSV('wob:RefNumber')
            if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                            ! Found
                p_web.SessionQueueToFile(JOBS)
                Access:JOBS.TryUpdate()
            else ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                            ! Error
            end ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
        else ! if (Access:EXCHOR48.TryInsert() = Level:Benign)
                    ! Error
            Access:EXCHOR48.CancelAutoInc()
        end ! if (Access:EXCHOR48.TryInsert() = Level:Benign)
    end ! if (Access:EXCHOR48.PrimeRecord() = Level:Benign)

    p_web.SSV('exchangeOrderCreated',1)
    p_web.SSV('ExchangeOrder:RecordNumber',ex4:RecordNumber)
    p_web.SSV('locWarningMessage','Exchange Order Created')

set:HubRepair      routine
    p_web.SSV('jobe:HubRepairDate',Today())
    p_web.SSV('jobe:HubRepairTime',Clock())

    p_web.SSV('GetStatus:StatusNumber',sub(GETINI('RRC','StatusSendToARC',,CLIP(PATH())&'\SB2KDEF.INI'),1,3))
    p_web.SSV('GetStatus:Type','JOB')
    GetStatus(p_web.GSV('GetStatus:StatusNumber'),0,p_web.GSV('GetStatus:Type'),p_web)

    if (p_web.GSV('BookingSite') = 'RRC')
        if (p_web.GSV('job:Exchange_Unit_Number') > 0)
            Access:REPTYDEF.Clearkey(rtd:ManRepairTypeKey)
            rtd:Manufacturer    = p_web.GSV('job:Manufacturer')
            set(rtd:ManRepairTypeKey,rtd:ManRepairTypeKey)
            loop
                if (Access:REPTYDEF.Next())
                    Break
                end ! if (Access:REPTYDEF.Next())
                if (rtd:Manufacturer    <> p_web.GSV('job:Manufacturer'))
                    Break
                end ! if (rtd:Manufacturer    <> p_web.GSV('job:Manufacturer'))
                if (rtd:BER = 11)
                    if (p_web.GSV('job:Chargeable_Job') = 'YES' and p_web.GSV('job:Repair_Type') = '')
                        p_web.SSV('job:Repair_Type',rtd:Repair_Type)
                    end ! if (p_web.GSV('job:Chargeable_Job') = 'YES' and p_web.GSV('job:Repair_Type') = '')
                    if (p_web.GSV('job:Warranty_Job') = 'YES' and p_web.GSV('job:Repair_Type_Warranty') = '')
                        p_web.SSV('job:Repair_Type_Warranty',rtd:Repair_Type)
                    end ! if (p_web.GSV('job:Chargeable_Job') = 'YES' and p_web.GSV('job:Repair_Type') = '')
                    break
                end ! if (rtd:BER = 11)
            end ! loop
        end ! if (p_web.GSV('job:Exchange_Unit_Number') > 0)
    end ! if (p_web.GSV('BookingSite') = 'RRC')


    Access:JOBSENG.Clearkey(joe:UserCodeKey)
    joe:JobNumber    = p_web.GSV('job:Ref_Number')
    joe:UserCode    = p_web.GSV('job:Engineer')
    joe:DateAllocated    = Today()
    set(joe:UserCodeKey,joe:UserCodeKey)
    loop
        if (Access:JOBSENG.Previous())
            Break
        end ! if (Access:JOBSENG.Next())
        if (joe:JobNumber    <> p_web.GSV('job:Ref_Number'))
            Break
        end ! if (joe:JobNumber    <> p_web.GSV('job:Ref_Number'))
        if (joe:UserCode    <> p_web.GSV('job:Engineer'))
            Break
        end ! if (joe:UserCode    <> p_web.GSV('job:Engineer'))
        if (joe:DateAllocated    > Today())
            Break
        end ! if (joe:DateAllocated    <> Today())
        joe:Status = 'HUB'
        joe:StatusDate = Today()
        joe:StatusTime = Clock()
        access:JOBSENG.update()
        break
    end ! loop


    if (p_web.GSV('jobe2:SMSNotification'))
        if (p_web.GSV('job:Who_Booked') = 'WEB')
            AddEmailSMS(p_web.GSV('job:Ref_Number'),p_web.GSV('job:Account_Number'),'2ARC','SMS',p_web.GSV('jobe2:SMSAlertNumber'),'',0,'')
        else ! if (p_web.GSV('job:Who_Booked') = 'WEB')
            AddEmailSMS(p_web.GSV('job:Ref_Number'),p_web.GSV('BookingAccount'),'2ARC','SMS',p_web.GSV('jobe2:SMSAlertNumber'),'',0,'')
        end ! if (p_web.GSV('job:Who_Booked') = 'WEB')
    end ! if (jobe2:SMSNotification)
    if (p_web.GSV('jobe2:EmailNotification'))
        if (p_web.GSV('job:Who_Booked') = 'WEB')
            AddEmailSMS(p_web.GSV('job:Ref_Number'),p_web.GSV('job:Account_Number'),'2ARC','EMAIL','',p_web.GSV('jobe2:EmailAlertNumber'),0,'')
        else ! if (p_web.GSV('job:Who_Booked') = 'WEB')
            AddEmailSMS(p_web.GSV('job:Ref_Number'),p_web.GSV('BookingAccount'),'2ARC','EMAIL','',p_web.GSV('jobe2:EmailAlertNumber'),0,'')
        end ! if (p_web.GSV('job:Who_Booked') = 'WEB')
    end ! if (jobe2:SMSNotification)
Validate:locEngineeringOption           Routine
    p_web.SSV('locErrorMessage','')
    case p_web.GSV('locNewEngineeringOption')
    of 'Standard Repair'
        if (p_web.GSV('BookingSite') = 'RRC')
            Access:MODELNUM.Clearkey(mod:Model_Number_Key)
            mod:Model_Number    = p_web.GSV('job:Model_Number')
            if (Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign)
                ! Found
                if (mod:ExcludedRRCRepair)
                    p_web.SSV('locErrorMessage','Warning! This model should not be repaired by an RRC and should be sent to the Hub.')
                    if (isUnitLiquidDamaged(p_web.GSV('job:Ref_Number'),p_web.GSV('job:ESN')))
                        p_web.SSV('locErrorMessage','Error! Cannot send to ARC. This Unit Is Liquid Damaged.')
                        p_web.SSV('locValidationFailed',1)
                        p_web.SSV('locNewEngineeringOption','')
                        Exit
                    end ! if (isUnitLiquidDamage(p_web.GSV('job:Ref_Number'),p_web.GSV('job:ESN'))

                end ! if (mod:ExcludedRRCRepair)
            else ! if (Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign)
                ! Error
            end ! if (Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign)
        end ! if (p_web.GSV('BookingSite') = 'RRC')
    of '48 Hour Exchange'
        if (is48HourOrderCreated(p_web.GSV('BookingSiteLocation'),p_web.GSV('job:Ref_Number')) = 1)
            p_web.SSV('locErrorMessage','Error! A 48 Hour Exchange has already been requested for this job.')
            p_web.SSV('locValidationFailed',1)
            Exit
        end ! if (is48HourOrderCreated(p_web.GSV('BookingSiteLocation'),p_web.GSV('job:Ref_Number')) = 1)
        if (is48HourOrderProcessed(p_web.GSV('BookingSiteLocation'),p_web.GSV('job:Ref_Number')) = 1)
            p_web.SSV('locErrorMessage','Error! A 48 Hour Exchange Unit has already been requested and despatched for this job.')
            p_web.SSV('locValidationFailed',1)
            Exit
        end ! if (is48HourOrderProcessed(p_web.GSV('BookingSiteLocation'),p_web.GSV('job:Ref_Number')) = 1)
        IF (p_web.GSV('job:Chargeable_Job') <> 'YES')
            ! #11939 Job is warranty only, show the "split job" option. (Bryan: 01/03/2011)
            p_web.SSV('locSplitJob',0)
            p_web.SSV('Hide:SplitJob',0)
        ELSE
            ! #11980 Job is chargeable so no need to show split option. (Bryan: 01/03/2011)
            IF (p_web.GSV('job:Warranty_Job') = 'YES')
                p_web.SSV('locSplitJob',3)
            ELSE
                p_web.SSV('locSplitJob',1)
            END
        END

    of 'ARC Repair'
        if (isUnitLiquidDamaged(p_web.GSV('job:Ref_Number'),p_web.GSV('job:ESN')))
            p_web.SSV('locErrorMessage','Error! Cannot send to ARC. This Unit Is Liquid Damaged.')
            p_web.SSV('locValidationFailed',1)
            p_web.SSV('locNewEngineeringOption','')
            Exit
        end ! if (isUnitLiquidDamage(p_web.GSV('job:Ref_Number'),p_web.GSV('job:ESN'))
    of '7 Day TAT'
        if (isUnitLiquidDamaged(p_web.GSV('job:Ref_Number'),p_web.GSV('job:ESN')))
            p_web.SSV('locErrorMessage','Error! Cannot send to ARC. This Unit Is Liquid Damaged.')
            p_web.SSV('locValidationFailed',1)
            p_web.SSV('locNewEngineeringOption','')
            Exit
        end ! if (isUnitLiquidDamage(p_web.GSV('job:Ref_Number'),p_web.GSV('job:ESN'))
    end ! case p_web.GSV('locEngineeringOption')
Validate:locExchangeModelNumber           Routine
    if (p_web.GSV('locExchangeModelNumber') <> p_web.GSV('job:Model_Number'))
        if (isThisModelAlternative(p_web.GSV('job:Model_Number'),p_web.GSV('locExchangeModelNumber')) = 0)
            p_web.SSV('locWarningMessage','Warning! The selected unit is not an "alternative" Model Number for this job!')
        else ! if (isThisModelAlternative(p_web.GSV('job:Model_Number'),p_web.GSV('locExchangeModelNumber')) = 0)
            p_web.SSV('locWarningMessage','Warning! You have selected a different Model Number for this job!')
        end ! if (isThisModelAlternative(p_web.GSV('job:Model_Number'),p_web.GSV('locExchangeModelNumber')) = 0)
    else ! if (p_web.GSV('locExchangeModelNumber') <> p_web.GSV('job:Model_Number'))
        p_web.SSV('locWarningMessage','')
    end ! if (p_web.GSV('locExchangeModelNumber') <> p_web.GSV('job:Model_Number'))

OpenFiles  ROUTINE
  p_web._OpenFile(USERS)
  p_web._OpenFile(ACCAREAS)
  p_web._OpenFile(MODELNUM)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(MANUFACT)
  p_web._OpenFile(STOCK)
  p_web._OpenFile(WARPARTS)
  p_web._OpenFile(PARTS)
  p_web._OpenFile(STOMODEL)
  p_web._OpenFile(AUDIT)
  p_web._OpenFile(STOCKALL)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(JOBSENG)
  p_web._OpenFile(REPTYDEF)
  p_web._OpenFile(WEBJOB)
  p_web._OpenFile(EXCHOR48)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(USERS)
  p_Web._CloseFile(ACCAREAS)
  p_Web._CloseFile(MODELNUM)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(MANUFACT)
  p_Web._CloseFile(STOCK)
  p_Web._CloseFile(WARPARTS)
  p_Web._CloseFile(PARTS)
  p_Web._CloseFile(STOMODEL)
  p_Web._CloseFile(AUDIT)
  p_Web._CloseFile(STOCKALL)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(JOBSENG)
  p_Web._CloseFile(REPTYDEF)
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(EXCHOR48)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  !Init Form
  if (p_web.GSV('FormEngineeringOption:FirstTime') = 1)
      p_web.SSV('locPasswordValidated',0)
      p_web.SSV('locValidationFailed',0)
      p_web.SSV('locUserPassword','')
      p_web.SSV('locErrorMessage','')
      p_web.SSV('locWarningMessage','')
      p_web.SSV('Comment:UserPassword','Enter User Password and press [TAB]')
      p_web.SSV('filter:Manufacturer','')
      p_web.SSV('locNewEngineeringOption','')
      p_web.SSV('locExchangeManufacturer',p_web.GSV('job:Manufacturer'))
      p_web.SSV('locExchangeModelNumber',p_web.GSV('job:Model_number'))
      p_web.SSV('locExchangeNotes','')
      p_web.SSV('locSplitJob',0)
      p_web.SSV('Hide:SplitJob',1)
  
      p_web.SSV('exchangeOrderCreated',0)
  
      p_web.SSV('FormEngineeringOption:FirstTime',0)
  
      ! Activate the 48 Hour option and show the booking option (DBH: 24-03-2005)
      p_web.SSV('hide:48HourOption',0)
      If AccountActivate48Hour(p_web.GSV('wob:HeadAccountNumber')) = True
          If Allow48Hour(p_web.GSV('job:ESN'),p_web.GSV('job:Model_Number'),p_web.GSV('wob:HeadAccountNumber')) = True
  
          Else ! If Allow48Hour(job:ESN,job:Model_Number) = True
              p_web.SSV('hide:48HourOption',1)
          End ! If Allow48Hour(job:ESN,job:Model_Number) = True
      Else ! AccountActivate48Hour(wob:HeadAccountNumber) = True
          p_web.SSV('hide:48HourOption',1)
      End ! AccountActivate48Hour(wob:HeadAccountNumber) = True
  
  end ! if (p_web.GSV('FormEngineeringOption:FirstTime') = 1)
  p_web.SetValue('FormEngineeringOption_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'FormEngineeringOption'
    end
    p_web.formsettings.proc = 'FormEngineeringOption'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('job:DOP')
    p_web.SetPicture('job:DOP',p_web.site.DatePicture)
  End
  p_web.SetSessionPicture('job:DOP',p_web.site.DatePicture)

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'locExchangeManufacturer'
    p_web.setsessionvalue('showtab_FormEngineeringOption',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(MANUFACT)
      p_web.SSV('filter:ModelNumber','Upper(mod:Manufacturer) = Upper(<39>' & clip(man:Manufacturer) & '<39>)')
      
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.locExchangeModelNumber')
  Of 'locExchangeModelNumber'
    p_web.setsessionvalue('showtab_FormEngineeringOption',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(MODELNUM)
      do Validate:locExchangeModelNumber
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.locExchangeNotes')
  End
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  if p_web.IfExistsValue('locUserPassword') = 0
    p_web.SetSessionValue('locUserPassword',locUserPassword)
  Else
    locUserPassword = p_web.GetSessionValue('locUserPassword')
  End
  if p_web.IfExistsValue('locEngineeringOption') = 0
    p_web.SetSessionValue('locEngineeringOption',locEngineeringOption)
  Else
    locEngineeringOption = p_web.GetSessionValue('locEngineeringOption')
  End
  if p_web.IfExistsValue('locNewEngineeringOption') = 0
    p_web.SetSessionValue('locNewEngineeringOption',locNewEngineeringOption)
  Else
    locNewEngineeringOption = p_web.GetSessionValue('locNewEngineeringOption')
  End
  if p_web.IfExistsValue('job:DOP') = 0
    p_web.SetSessionValue('job:DOP',job:DOP)
  Else
    job:DOP = p_web.GetSessionValue('job:DOP')
  End
  if p_web.IfExistsValue('locExchangeManufacturer') = 0
    p_web.SetSessionValue('locExchangeManufacturer',locExchangeManufacturer)
  Else
    locExchangeManufacturer = p_web.GetSessionValue('locExchangeManufacturer')
  End
  if p_web.IfExistsValue('locExchangeModelNumber') = 0
    p_web.SetSessionValue('locExchangeModelNumber',locExchangeModelNumber)
  Else
    locExchangeModelNumber = p_web.GetSessionValue('locExchangeModelNumber')
  End
  if p_web.IfExistsValue('locExchangeNotes') = 0
    p_web.SetSessionValue('locExchangeNotes',locExchangeNotes)
  Else
    locExchangeNotes = p_web.GetSessionValue('locExchangeNotes')
  End
  if p_web.IfExistsValue('locSplitJob') = 0
    p_web.SetSessionValue('locSplitJob',locSplitJob)
  Else
    locSplitJob = p_web.GetSessionValue('locSplitJob')
  End

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locUserPassword')
    locUserPassword = p_web.GetValue('locUserPassword')
    p_web.SetSessionValue('locUserPassword',locUserPassword)
  Else
    locUserPassword = p_web.GetSessionValue('locUserPassword')
  End
  if p_web.IfExistsValue('locEngineeringOption')
    locEngineeringOption = p_web.GetValue('locEngineeringOption')
    p_web.SetSessionValue('locEngineeringOption',locEngineeringOption)
  Else
    locEngineeringOption = p_web.GetSessionValue('locEngineeringOption')
  End
  if p_web.IfExistsValue('locNewEngineeringOption')
    locNewEngineeringOption = p_web.GetValue('locNewEngineeringOption')
    p_web.SetSessionValue('locNewEngineeringOption',locNewEngineeringOption)
  Else
    locNewEngineeringOption = p_web.GetSessionValue('locNewEngineeringOption')
  End
  if p_web.IfExistsValue('job:DOP')
    job:DOP = p_web.dformat(clip(p_web.GetValue('job:DOP')),p_web.site.DatePicture)
    p_web.SetSessionValue('job:DOP',job:DOP)
  Else
    job:DOP = p_web.GetSessionValue('job:DOP')
  End
  if p_web.IfExistsValue('locExchangeManufacturer')
    locExchangeManufacturer = p_web.GetValue('locExchangeManufacturer')
    p_web.SetSessionValue('locExchangeManufacturer',locExchangeManufacturer)
  Else
    locExchangeManufacturer = p_web.GetSessionValue('locExchangeManufacturer')
  End
  if p_web.IfExistsValue('locExchangeModelNumber')
    locExchangeModelNumber = p_web.GetValue('locExchangeModelNumber')
    p_web.SetSessionValue('locExchangeModelNumber',locExchangeModelNumber)
  Else
    locExchangeModelNumber = p_web.GetSessionValue('locExchangeModelNumber')
  End
  if p_web.IfExistsValue('locExchangeNotes')
    locExchangeNotes = p_web.GetValue('locExchangeNotes')
    p_web.SetSessionValue('locExchangeNotes',locExchangeNotes)
  Else
    locExchangeNotes = p_web.GetSessionValue('locExchangeNotes')
  End
  if p_web.IfExistsValue('locSplitJob')
    locSplitJob = p_web.GetValue('locSplitJob')
    p_web.SetSessionValue('locSplitJob',locSplitJob)
  Else
    locSplitJob = p_web.GetSessionValue('locSplitJob')
  End

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('FormEngineeringOption_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'ViewJob'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormEngineeringOption_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormEngineeringOption_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormEngineeringOption_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'ViewJob'

GenerateForm   Routine
  do LoadRelatedRecords
 locUserPassword = p_web.RestoreValue('locUserPassword')
 locEngineeringOption = p_web.RestoreValue('locEngineeringOption')
 locNewEngineeringOption = p_web.RestoreValue('locNewEngineeringOption')
 locExchangeManufacturer = p_web.RestoreValue('locExchangeManufacturer')
 locExchangeModelNumber = p_web.RestoreValue('locExchangeModelNumber')
 locExchangeNotes = p_web.RestoreValue('locExchangeNotes')
 locSplitJob = p_web.RestoreValue('locSplitJob')
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  loc:FormHeading = p_web.Translate('Change Engineering Option',0)
  If loc:FormHeading
    if loc:popup
      packet = clip(packet) & p_web.jQuery('#popup_'&lower('FormEngineeringOption')&'_div','dialog','"option","title",'&p_web.jsString(loc:FormHeading,0),,0)
    else
      packet = clip(packet) & lower('<div id="form-access-FormEngineeringOption"></div>')
      packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formheading,)&'">'&clip(loc:FormHeading)&'</div>'&CRLF
    end
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_FormEngineeringOption',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_FormEngineeringOption0_div')&'">'&p_web.Translate('Confirm User')&'</a></li>'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_FormEngineeringOption1_div')&'">'&p_web.Translate('Engineering Option')&'</a></li>'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_FormEngineeringOption2_div')&'">'&p_web.Translate()&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="FormEngineeringOption_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.cancel();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CancelButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="FormEngineeringOption_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'FormEngineeringOption_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="FormEngineeringOption_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'FormEngineeringOption_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
    If upper(p_web.getvalue('LookupFile'))='MANUFACT'
          If Not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
            p_web.SetValue('SelectField',clip(loc:formname) & '.locExchangeModelNumber')
          End
    End
    If upper(p_web.getvalue('LookupFile'))='MODELNUM'
          If Not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
            p_web.SetValue('SelectField',clip(loc:formname) & '.locExchangeNotes')
          End
    End
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locUserPassword')
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_FormEngineeringOption')>0,p_web.GSV('showtab_FormEngineeringOption'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_FormEngineeringOption'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_FormEngineeringOption') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_FormEngineeringOption'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_FormEngineeringOption')>0,p_web.GSV('showtab_FormEngineeringOption'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_FormEngineeringOption') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Confirm User') & ''''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Engineering Option') & ''''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_FormEngineeringOption_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_FormEngineeringOption')>0,p_web.GSV('showtab_FormEngineeringOption'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"FormEngineeringOption",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_FormEngineeringOption')>0,p_web.GSV('showtab_FormEngineeringOption'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_FormEngineeringOption_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('FormEngineeringOption') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('FormEngineeringOption')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Confirm User')&'</a></h3>' & CRLF & p_web.DivHeader('tab_FormEngineeringOption0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEngineeringOption0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEngineeringOption0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEngineeringOption0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Confirm User')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEngineeringOption0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Confirm User')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEngineeringOption0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Confirm User')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEngineeringOption0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locUserPassword
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locUserPassword
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locUserPassword
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab1  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Engineering Option')&'</a></h3>' & CRLF & p_web.DivHeader('tab_FormEngineeringOption1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEngineeringOption1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEngineeringOption1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEngineeringOption1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Engineering Option')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEngineeringOption1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Engineering Option')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEngineeringOption1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Engineering Option')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEngineeringOption1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::locErrorMessage
        do Comment::locErrorMessage
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locEngineeringOption
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locEngineeringOption
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locEngineeringOption
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locNewEngineeringOption
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locNewEngineeringOption
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locNewEngineeringOption
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab2  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate()&'</a></h3>' & CRLF & p_web.DivHeader('tab_FormEngineeringOption2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEngineeringOption2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEngineeringOption2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEngineeringOption2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,)
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEngineeringOption2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend)
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEngineeringOption2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain">' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEngineeringOption2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::text:ExchangeOrder
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::text:ExchangeOrder
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:DOP
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:DOP
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::job:DOP
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locExchangeManufacturer
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locExchangeManufacturer
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locExchangeManufacturer
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locExchangeModelNumber
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locExchangeModelNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locExchangeModelNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locExchangeNotes
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locExchangeNotes
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locExchangeNotes
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locWarningMessage
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locWarningMessage
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locSplitJob
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locSplitJob
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locSplitJob
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::button:CreateOrder
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::button:CreateOrder
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::button:PrintOrder
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::button:PrintOrder
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Prompt::locUserPassword  Routine
  packet = clip(packet) & p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('locUserPassword') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Enter Password'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locUserPassword  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locUserPassword = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locUserPassword = p_web.GetValue('Value')
  End
  do ValidateValue::locUserPassword  ! copies value to session value if valid.
      p_web.SSV('locPasswordValidated',0)
      p_web.SSV('Comment:UserPassword','User Does Not Have Access')
  
      Access:USERS.Clearkey(use:Password_Key)
      use:Password    = p_web.GSV('locUserPassword')
      if (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
          ! Found
          Access:ACCAREAS.Clearkey(acc:Access_Level_Key)
          acc:User_Level  = use:User_Level
          acc:Access_Area = 'AMEND ENGINEERING OPTION'
          If Access:ACCAREAS.Tryfetch(acc:Access_Level_Key) = Level:Benign
              !Found
              p_web.SSV('locPasswordValidated',1)
              p_web.SSV('Comment:UserPassword','')
              p_web.SSV('headingExchangeOrder','Exchange Order')
          Else ! If Access:ACCAREAS.Tryfetch(Access_Level_Key) = Level:Benign
              !Error
          End !If Access:ACCAREAS.Tryfetch(Access_Level_Key) = Level:Benign
      else ! if (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
          ! Error
      end ! if (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
  do Value::locUserPassword
  do SendAlert
  do Comment::locUserPassword ! allows comment style to be updated.
  do Prompt::locNewEngineeringOption
  do Value::locNewEngineeringOption  !1
  do Comment::locNewEngineeringOption
  do Comment::locUserPassword
  do Prompt::locEngineeringOption
  do Value::locEngineeringOption  !1
  do Comment::locEngineeringOption

ValidateValue::locUserPassword  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locUserPassword',locUserPassword).
    End

Value::locUserPassword  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('locUserPassword') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    locUserPassword = p_web.RestoreValue('locUserPassword')
    do ValidateValue::locUserPassword
    If locUserPassword:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- locUserPassword
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locUserPassword'',''formengineeringoption_locuserpassword_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locUserPassword')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('password','locUserPassword',p_web.GetSessionValueFormat('locUserPassword'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locUserPassword  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locUserPassword:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:UserPassword'))
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('locUserPassword') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::locErrorMessage  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::locErrorMessage  ! copies value to session value if valid.
  do Comment::locErrorMessage ! allows comment style to be updated.

ValidateValue::locErrorMessage  Routine
    If not (1=0)
    End

Value::locErrorMessage  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('locErrorMessage') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="locErrorMessage" class="'&clip('red bold large')&'"'&clip(loc:extra)&'>' & p_web.Translate(p_web.GSV('locErrorMessage'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locErrorMessage  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locErrorMessage:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('locErrorMessage') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locEngineeringOption  Routine
  packet = clip(packet) & p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('locEngineeringOption') & '_prompt',Choose(p_web.GSV('locPasswordValidated') = 0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('locPasswordValidated') = 0,'',p_web.Translate('Current Option'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locEngineeringOption  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locEngineeringOption = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locEngineeringOption = p_web.GetValue('Value')
  End
  do ValidateValue::locEngineeringOption  ! copies value to session value if valid.
  do Comment::locEngineeringOption ! allows comment style to be updated.

ValidateValue::locEngineeringOption  Routine
    If not (p_web.GSV('locPasswordValidated') = 0)
      if loc:invalid = '' then p_web.SetSessionValue('locEngineeringOption',locEngineeringOption).
    End

Value::locEngineeringOption  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('locPasswordValidated') = 0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('locEngineeringOption') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('locPasswordValidated') = 0)
  ! --- DISPLAY --- locEngineeringOption
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locEngineeringOption'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locEngineeringOption  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locEngineeringOption:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('locPasswordValidated') = 0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('locEngineeringOption') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('locPasswordValidated') = 0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locNewEngineeringOption  Routine
  packet = clip(packet) & p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('locNewEngineeringOption') & '_prompt',Choose(p_web.GSV('locPasswordValidated') = 0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('locPasswordValidated') = 0,'',p_web.Translate('New Engineering Option'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locNewEngineeringOption  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locNewEngineeringOption = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locNewEngineeringOption = p_web.GetValue('Value')
  End
  do ValidateValue::locNewEngineeringOption  ! copies value to session value if valid.
  do validate:locEngineeringOption
  p_web.SSV('locExchangeNotes',BHStripReplace(p_web.GSV('locExchangeNotes'),'<9>',''))
  p_web.SSV('locExchangeNotes',BHStripReplace(p_web.GSV('locExchangeNotes'),'<</DIV>',''))
  
  
  do Value::locNewEngineeringOption
  do SendAlert
  do Comment::locNewEngineeringOption ! allows comment style to be updated.
  do Value::locErrorMessage  !1
  do Prompt::job:DOP
  do Value::job:DOP  !1
  do Comment::job:DOP
  do Prompt::locExchangeManufacturer
  do Value::locExchangeManufacturer  !1
  do Comment::locExchangeManufacturer
  do Prompt::locExchangeModelNumber
  do Value::locExchangeModelNumber  !1
  do Comment::locExchangeModelNumber
  do Prompt::locExchangeNotes
  do Value::locExchangeNotes  !1
  do Comment::locExchangeNotes
  do Value::text:ExchangeOrder  !1
  do Value::button:CreateOrder  !1
  do Prompt::locSplitJob
  do Value::locSplitJob  !1

ValidateValue::locNewEngineeringOption  Routine
    If not (p_web.GSV('locPasswordValidated') = 0)
  If locNewEngineeringOption = ''
    loc:Invalid = 'locNewEngineeringOption'
    locNewEngineeringOption:IsInvalid = true
    loc:alert = p_web.translate('New Engineering Option') & ' ' & p_web.site.RequiredText
  End
      if loc:invalid = '' then p_web.SetSessionValue('locNewEngineeringOption',locNewEngineeringOption).
    End

Value::locNewEngineeringOption  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('locPasswordValidated') = 0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('locNewEngineeringOption') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,p_web.site.style.formselect,,)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  If loc:retrying
    locNewEngineeringOption = p_web.RestoreValue('locNewEngineeringOption')
    do ValidateValue::locNewEngineeringOption
    If locNewEngineeringOption:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('locPasswordValidated') = 0)
  ! --- DROPLIST ---
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locNewEngineeringOption'',''formengineeringoption_locnewengineeringoption_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locNewEngineeringOption')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('exchangeOrderCreated') = 1,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('locNewEngineeringOption',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
  loc:fieldclass = ''
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('locNewEngineeringOption') = 0
    p_web.SetSessionValue('locNewEngineeringOption','')
  end
    packet = clip(packet) & p_web.CreateOption('-------------------------------------','',choose('' = p_web.getsessionvalue('locNewEngineeringOption')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('locEngineeringOption') <> 'Standard Repair'
    packet = clip(packet) & p_web.CreateOption('Standard Repair','Standard Repair',choose('Standard Repair' = p_web.getsessionvalue('locNewEngineeringOption')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('locEngineeringOption') <> '48 Hour Exchange'  and p_web.GSV('hide:48HourOption') <> 1
    packet = clip(packet) & p_web.CreateOption('48 Hour Exchange','48 Hour Exchange',choose('48 Hour Exchange' = p_web.getsessionvalue('locNewEngineeringOption')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('locEngineeringOption') <> 'ARC Repair'
    packet = clip(packet) & p_web.CreateOption('ARC Repair','ARC Repair',choose('ARC Repair' = p_web.getsessionvalue('locNewEngineeringOption')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('locEngineeringOption') <> '7 Day TAT'
    packet = clip(packet) & p_web.CreateOption('7 Day TAT','7 Day TAT',choose('7 Day TAT' = p_web.getsessionvalue('locNewEngineeringOption')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web.DivFooter()
Comment::locNewEngineeringOption  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locNewEngineeringOption:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('locPasswordValidated') = 0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('locNewEngineeringOption') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('locPasswordValidated') = 0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::text:ExchangeOrder  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::text:ExchangeOrder  ! copies value to session value if valid.
  do Comment::text:ExchangeOrder ! allows comment style to be updated.

ValidateValue::text:ExchangeOrder  Routine
    If not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
    End

Value::text:ExchangeOrder  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('text:ExchangeOrder') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="text:ExchangeOrder" class="'&clip('blue bold')&'"'&clip(loc:extra)&'>' & p_web.Translate('Exchange Order',) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::text:ExchangeOrder  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if text:ExchangeOrder:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('text:ExchangeOrder') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange'
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::job:DOP  Routine
  packet = clip(packet) & p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('job:DOP') & '_prompt',Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','',p_web.Translate('Date Of Purchase'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:DOP  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:DOP = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value')
    job:DOP = p_web.dformat(clip(p_web.GetValue('Value')),p_web.site.DatePicture)
  End
  do ValidateValue::job:DOP  ! copies value to session value if valid.
  do Value::job:DOP
  do SendAlert
  do Comment::job:DOP ! allows comment style to be updated.

ValidateValue::job:DOP  Routine
    If not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
      if loc:invalid = '' then p_web.SetSessionValue('job:DOP',job:DOP).
    End

Value::job:DOP  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('job:DOP') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  If loc:retrying
    job:DOP = p_web.RestoreValue('job:DOP')
    do ValidateValue::job:DOP
    If job:DOP:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
  ! --- DATE --- job:DOP
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:DOP'',''formengineeringoption_job:dop_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = '1'
  loc:options = p_web.site.Dateoptions
  ! example of loc:options; loc:options = Choose(loc:options='','',clip(loc:options) & ',') & 'numberOfMonths: 3,showButtonPanel: true' ! see http://jqueryui.com/demos/datepicker/#options
  packet = clip(packet) & p_web.CreateDateInput ('job:DOP',p_web.GetSessionValue('job:DOP'),loc:fieldclass,loc:readonly,,,loc:javascript,loc:options,loc:extra,,15,,,,0)
  do SendPacket
  End
  p_web.DivFooter()
Comment::job:DOP  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if job:DOP:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web._DateFormat()
  loc:class = Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('job:DOP') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange'
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locExchangeManufacturer  Routine
  packet = clip(packet) & p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('locExchangeManufacturer') & '_prompt',Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','',p_web.Translate('Manufacturer'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locExchangeManufacturer  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locExchangeManufacturer = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locExchangeManufacturer = p_web.GetValue('Value')
  ElsIf p_web.RequestAjax = 1 and p_web.IfExistsValue('man:Manufacturer')
    locExchangeManufacturer = p_web.GetValue('man:Manufacturer')
  ElsIf p_web.RequestAjax = 1
    locExchangeManufacturer = man:Manufacturer
  End
  do ValidateValue::locExchangeManufacturer  ! copies value to session value if valid.
  p_Web.SetValue('lookupfield','locExchangeManufacturer')
  do AfterLookup
  do Value::locExchangeManufacturer
  do SendAlert
  do Comment::locExchangeManufacturer

ValidateValue::locExchangeManufacturer  Routine
    If not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
  If locExchangeManufacturer = ''
    loc:Invalid = 'locExchangeManufacturer'
    locExchangeManufacturer:IsInvalid = true
    loc:alert = p_web.translate('Manufacturer') & ' ' & p_web.site.RequiredText
  End
      if loc:invalid = '' then p_web.SetSessionValue('locExchangeManufacturer',locExchangeManufacturer).
    End

Value::locExchangeManufacturer  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('locExchangeManufacturer') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If p_web.GSV('exchangeOrderCreated') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  If loc:retrying
    locExchangeManufacturer = p_web.RestoreValue('locExchangeManufacturer')
    do ValidateValue::locExchangeManufacturer
    If locExchangeManufacturer:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
  ! --- STRING --- locExchangeManufacturer
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('exchangeOrderCreated') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locExchangeManufacturer'',''formengineeringoption_locexchangemanufacturer_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locExchangeManufacturer')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = 'readonly'
    packet = clip(packet) & p_web.CreateInput('text','locExchangeManufacturer',p_web.GetSessionValue('locExchangeManufacturer'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'!4
    if not loc:viewonly and not loc:readonly
      loc:fieldclass = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('SelectManufacturers')&'?LookupField=locExchangeManufacturer&Tab=2&ForeignField=man:Manufacturer&_sort=man:Manufacturer&Refresh=sort'),,,,,loc:fieldclass)
    End
  do SendPacket
  End
  p_web.DivFooter()
Comment::locExchangeManufacturer  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locExchangeManufacturer:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web._jsok(p_web.site.RequiredText)
  loc:class = Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('locExchangeManufacturer') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange'
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locExchangeModelNumber  Routine
  packet = clip(packet) & p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('locExchangeModelNumber') & '_prompt',Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','',p_web.Translate('Model Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locExchangeModelNumber  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locExchangeModelNumber = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locExchangeModelNumber = p_web.GetValue('Value')
  ElsIf p_web.RequestAjax = 1 and p_web.IfExistsValue('mod:Model_Number')
    locExchangeModelNumber = p_web.GetValue('mod:Model_Number')
  ElsIf p_web.RequestAjax = 1
    locExchangeModelNumber = mod:Model_Number
  End
  do ValidateValue::locExchangeModelNumber  ! copies value to session value if valid.
  p_Web.SetValue('lookupfield','locExchangeModelNumber')
  do AfterLookup
  do Value::locExchangeModelNumber
  do SendAlert
  do Comment::locExchangeModelNumber
  do Value::locWarningMessage  !1

ValidateValue::locExchangeModelNumber  Routine
    If not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
  If locExchangeModelNumber = ''
    loc:Invalid = 'locExchangeModelNumber'
    locExchangeModelNumber:IsInvalid = true
    loc:alert = p_web.translate('Model Number') & ' ' & p_web.site.RequiredText
  End
      if loc:invalid = '' then p_web.SetSessionValue('locExchangeModelNumber',locExchangeModelNumber).
    End

Value::locExchangeModelNumber  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('locExchangeModelNumber') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If p_web.GSV('exchangeOrderCreated') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  If loc:retrying
    locExchangeModelNumber = p_web.RestoreValue('locExchangeModelNumber')
    do ValidateValue::locExchangeModelNumber
    If locExchangeModelNumber:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
  ! --- STRING --- locExchangeModelNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('exchangeOrderCreated') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locExchangeModelNumber'',''formengineeringoption_locexchangemodelnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locExchangeModelNumber')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = 'readonly'
    packet = clip(packet) & p_web.CreateInput('text','locExchangeModelNumber',p_web.GetSessionValue('locExchangeModelNumber'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'!4
    if not loc:viewonly and not loc:readonly
      loc:fieldclass = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('SelectModelNumbers')&'?LookupField=locExchangeModelNumber&Tab=2&ForeignField=mod:Model_Number&_sort=mod:Model_Number&Refresh=sort'),,,,,loc:fieldclass)
    End
  do SendPacket
  End
  p_web.DivFooter()
Comment::locExchangeModelNumber  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locExchangeModelNumber:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web._jsok(p_web.site.RequiredText)
  loc:class = Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('locExchangeModelNumber') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange'
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locExchangeNotes  Routine
  packet = clip(packet) & p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('locExchangeNotes') & '_prompt',Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','',p_web.Translate('Notes'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locExchangeNotes  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locExchangeNotes = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locExchangeNotes = p_web.GetValue('Value')
  End
  do ValidateValue::locExchangeNotes  ! copies value to session value if valid.
  p_web.SSV('locExchangeNotes',BHStripReplace(p_web.GSV('locExchangeNotes'),'<9>',''))
  do Value::locExchangeNotes
  do SendAlert
  do Comment::locExchangeNotes ! allows comment style to be updated.

ValidateValue::locExchangeNotes  Routine
    If not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
    locExchangeNotes = Upper(locExchangeNotes)
      if loc:invalid = '' then p_web.SetSessionValue('locExchangeNotes',locExchangeNotes).
    End

Value::locExchangeNotes  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('locExchangeNotes') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,'TextEntry')
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    locExchangeNotes = p_web.RestoreValue('locExchangeNotes')
    do ValidateValue::locExchangeNotes
    If locExchangeNotes:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
  ! --- TEXT --- locExchangeNotes
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locExchangeNotes'',''formengineeringoption_locexchangenotes_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locExchangeNotes')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'readonly','')
  do SendPacket
  p_web.CreateTextArea('locExchangeNotes',p_web.GetSessionValue('locExchangeNotes'),3,40,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(locExchangeNotes),,,,Net:Send)
  do SendPacket
  End
  p_web.DivFooter()
Comment::locExchangeNotes  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locExchangeNotes:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('locExchangeNotes') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange'
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::locWarningMessage  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::locWarningMessage  ! copies value to session value if valid.
  do Comment::locWarningMessage ! allows comment style to be updated.

ValidateValue::locWarningMessage  Routine
    If not (1=0)
    End

Value::locWarningMessage  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('locWarningMessage') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="locWarningMessage" class="'&clip('green bold')&'"'&clip(loc:extra)&'>' & p_web.Translate(p_web.GSV('locWarningMessage'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locWarningMessage  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locWarningMessage:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('locWarningMessage') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locSplitJob  Routine
  packet = clip(packet) & p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('locSplitJob') & '_prompt',Choose(p_web.GSV('Hide:SplitJob') = 1 OR p_web.GSV('exchangeOrderCreated') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:SplitJob') = 1 OR p_web.GSV('exchangeOrderCreated') = 1,'',p_web.Translate('Select Job Type'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locSplitJob  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locSplitJob = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locSplitJob = p_web.GetValue('Value')
  End
  do ValidateValue::locSplitJob  ! copies value to session value if valid.
  do Value::locSplitJob
  do SendAlert
  do Comment::locSplitJob ! allows comment style to be updated.
  do Value::button:CreateOrder  !1

ValidateValue::locSplitJob  Routine
    If not (p_web.GSV('Hide:SplitJob') = 1 OR p_web.GSV('exchangeOrderCreated') = 1)
      if loc:invalid = '' then p_web.SetSessionValue('locSplitJob',locSplitJob).
    End

Value::locSplitJob  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:SplitJob') = 1 OR p_web.GSV('exchangeOrderCreated') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('locSplitJob') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If loc:retrying
    locSplitJob = p_web.RestoreValue('locSplitJob')
    do ValidateValue::locSplitJob
    If locSplitJob:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:SplitJob') = 1 OR p_web.GSV('exchangeOrderCreated') = 1)
  ! --- RADIO --- locSplitJob
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locSplitJob') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locSplitJob'',''formengineeringoption_locsplitjob_value'',1,'''&p_web._jsok(1)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locSplitJob')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locSplitJob',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locSplitJob_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Chargeable Only') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locSplitJob') = 2
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locSplitJob'',''formengineeringoption_locsplitjob_value'',1,'''&p_web._jsok(2)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locSplitJob')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locSplitJob',clip(2),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locSplitJob_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Warranty Only') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locSplitJob') = 3
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locSplitJob'',''formengineeringoption_locsplitjob_value'',1,'''&p_web._jsok(3)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locSplitJob')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locSplitJob',clip(3),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locSplitJob_3') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Split Warranty/Chargeable') & '<13,10>'
    packet = clip(packet) & p_web.br
  do SendPacket
  End
  p_web.DivFooter()
Comment::locSplitJob  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locSplitJob:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:SplitJob') = 1 OR p_web.GSV('exchangeOrderCreated') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('locSplitJob') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:SplitJob') = 1 OR p_web.GSV('exchangeOrderCreated') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::button:CreateOrder  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::button:CreateOrder  ! copies value to session value if valid.
  if (p_web.GSV('locExchangeManufacturer') <> '' and p_web.GSV('locExchangeModelNumber') <> '')
      do create:ExchangeOrder
  end ! if (p_web.GSV('locExchangeManufacturer') <> '' and |
  do Value::button:CreateOrder
  do Comment::button:CreateOrder ! allows comment style to be updated.
  do Value::button:PrintOrder  !1
  do Value::locExchangeManufacturer  !1
  do Value::locExchangeModelNumber  !1
  do Value::locExchangeNotes  !1
  do Value::locWarningMessage  !1
  do Prompt::locSplitJob
  do Value::locSplitJob  !1

ValidateValue::button:CreateOrder  Routine
    If not (p_web.GSV('exchangeOrderCreated') = 1 Or p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange' OR p_web.GSV('locSplitJob') = 0)
    End

Value::button:CreateOrder  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('exchangeOrderCreated') = 1 Or p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange' OR p_web.GSV('locSplitJob') = 0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('button:CreateOrder') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('exchangeOrderCreated') = 1 Or p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange' OR p_web.GSV('locSplitJob') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''button:CreateOrder'',''formengineeringoption_button:createorder_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','CreateExchangeOrder','Create Exch. Order',p_web.combine(Choose('Create Exch. Order' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,,loc:javascript,loc:disabled,'images\star.png',,,,,,,,,'nt-left')!h
  do SendPacket
  End
  p_web.DivFooter()
Comment::button:CreateOrder  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if button:CreateOrder:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('exchangeOrderCreated') = 1 Or p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange' OR p_web.GSV('locSplitJob') = 0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('button:CreateOrder') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('exchangeOrderCreated') = 1 Or p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange' OR p_web.GSV('locSplitJob') = 0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::button:PrintOrder  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::button:PrintOrder  ! copies value to session value if valid.
  do Comment::button:PrintOrder ! allows comment style to be updated.

ValidateValue::button:PrintOrder  Routine
    If not (p_web.GSV('exchangeOrderCreated') = 0)
    End

Value::button:PrintOrder  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('exchangeOrderCreated') = 0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('button:PrintOrder') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('exchangeOrderCreated') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','PrintOrder','Print Order',p_web.combine(Choose('Print Order' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.WindowOpen(clip('ExchangeOrder')&''&'','_blank'),,loc:disabled,'images\printer.png',,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::button:PrintOrder  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if button:PrintOrder:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('exchangeOrderCreated') = 0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('button:PrintOrder') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('exchangeOrderCreated') = 0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('FormEngineeringOption_nexttab_' & 0)
    locUserPassword = p_web.GSV('locUserPassword')
    do ValidateValue::locUserPassword
    If loc:Invalid
      loc:retrying = 1
      do Value::locUserPassword
      !do SendAlert
      do Comment::locUserPassword ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('FormEngineeringOption_nexttab_' & 1)
    locEngineeringOption = p_web.GSV('locEngineeringOption')
    do ValidateValue::locEngineeringOption
    If loc:Invalid
      loc:retrying = 1
      do Value::locEngineeringOption
      !do SendAlert
      do Comment::locEngineeringOption ! allows comment style to be updated.
      !exit
    End
    locNewEngineeringOption = p_web.GSV('locNewEngineeringOption')
    do ValidateValue::locNewEngineeringOption
    If loc:Invalid
      loc:retrying = 1
      do Value::locNewEngineeringOption
      !do SendAlert
      do Comment::locNewEngineeringOption ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('FormEngineeringOption_nexttab_' & 2)
    job:DOP = p_web.GSV('job:DOP')
    do ValidateValue::job:DOP
    If loc:Invalid
      loc:retrying = 1
      do Value::job:DOP
      !do SendAlert
      do Comment::job:DOP ! allows comment style to be updated.
      !exit
    End
    locExchangeManufacturer = p_web.GSV('locExchangeManufacturer')
    do ValidateValue::locExchangeManufacturer
    If loc:Invalid
      loc:retrying = 1
      do Value::locExchangeManufacturer
      !do SendAlert
      do Comment::locExchangeManufacturer ! allows comment style to be updated.
      !exit
    End
    locExchangeModelNumber = p_web.GSV('locExchangeModelNumber')
    do ValidateValue::locExchangeModelNumber
    If loc:Invalid
      loc:retrying = 1
      do Value::locExchangeModelNumber
      !do SendAlert
      do Comment::locExchangeModelNumber ! allows comment style to be updated.
      !exit
    End
    locExchangeNotes = p_web.GSV('locExchangeNotes')
    do ValidateValue::locExchangeNotes
    If loc:Invalid
      loc:retrying = 1
      do Value::locExchangeNotes
      !do SendAlert
      do Comment::locExchangeNotes ! allows comment style to be updated.
      !exit
    End
    locSplitJob = p_web.GSV('locSplitJob')
    do ValidateValue::locSplitJob
    If loc:Invalid
      loc:retrying = 1
      do Value::locSplitJob
      !do SendAlert
      do Comment::locSplitJob ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_FormEngineeringOption_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('FormEngineeringOption_tab_' & 0)
    do GenerateTab0
  of lower('FormEngineeringOption_locUserPassword_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locUserPassword
      of event:timer
        do Value::locUserPassword
        do Comment::locUserPassword
      else
        do Value::locUserPassword
      end
  of lower('FormEngineeringOption_tab_' & 1)
    do GenerateTab1
  of lower('FormEngineeringOption_locNewEngineeringOption_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locNewEngineeringOption
      of event:timer
        do Value::locNewEngineeringOption
        do Comment::locNewEngineeringOption
      else
        do Value::locNewEngineeringOption
      end
  of lower('FormEngineeringOption_tab_' & 2)
    do GenerateTab2
  of lower('FormEngineeringOption_job:DOP_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:DOP
      of event:timer
        do Value::job:DOP
        do Comment::job:DOP
      else
        do Value::job:DOP
      end
  of lower('FormEngineeringOption_locExchangeManufacturer_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locExchangeManufacturer
      of event:timer
        do Value::locExchangeManufacturer
        do Comment::locExchangeManufacturer
      else
        do Value::locExchangeManufacturer
      end
  of lower('FormEngineeringOption_locExchangeModelNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locExchangeModelNumber
      of event:timer
        do Value::locExchangeModelNumber
        do Comment::locExchangeModelNumber
      else
        do Value::locExchangeModelNumber
      end
  of lower('FormEngineeringOption_locExchangeNotes_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locExchangeNotes
      of event:timer
        do Value::locExchangeNotes
        do Comment::locExchangeNotes
      else
        do Value::locExchangeNotes
      end
  of lower('FormEngineeringOption_locSplitJob_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locSplitJob
      of event:timer
        do Value::locSplitJob
        do Comment::locSplitJob
      else
        do Value::locSplitJob
      end
  of lower('FormEngineeringOption_button:CreateOrder_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::button:CreateOrder
      of event:timer
        do Value::button:CreateOrder
        do Comment::button:CreateOrder
      else
        do Value::button:CreateOrder
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('FormEngineeringOption_form:ready_',1)

  p_web.SetSessionValue('FormEngineeringOption_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_FormEngineeringOption',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('FormEngineeringOption_form:ready_',1)
  p_web.SetSessionValue('FormEngineeringOption_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormEngineeringOption',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('FormEngineeringOption_form:ready_',1)
  p_web.SetSessionValue('FormEngineeringOption_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('FormEngineeringOption:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('FormEngineeringOption_form:ready_',1)
  p_web.SetSessionValue('FormEngineeringOption_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('FormEngineeringOption:Primed',0)
  p_web.setsessionvalue('showtab_FormEngineeringOption',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
      If not (1=0)
          If p_web.IfExistsValue('locUserPassword')
            locUserPassword = p_web.GetValue('locUserPassword')
          End
      End
      If not (p_web.GSV('locPasswordValidated') = 0)
        If not (p_web.GSV('exchangeOrderCreated') = 1)
          If p_web.IfExistsValue('locNewEngineeringOption')
            locNewEngineeringOption = p_web.GetValue('locNewEngineeringOption')
          End
        End
      End
      If not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
          If p_web.IfExistsValue('job:DOP')
            job:DOP = p_web.GetValue('job:DOP')
          End
      End
      If not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
        If not (p_web.GSV('exchangeOrderCreated') = 1)
          If p_web.IfExistsValue('locExchangeManufacturer')
            locExchangeManufacturer = p_web.GetValue('locExchangeManufacturer')
          End
        End
      End
      If not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
        If not (p_web.GSV('exchangeOrderCreated') = 1)
          If p_web.IfExistsValue('locExchangeModelNumber')
            locExchangeModelNumber = p_web.GetValue('locExchangeModelNumber')
          End
        End
      End
      If not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
          If p_web.IfExistsValue('locExchangeNotes')
            locExchangeNotes = p_web.GetValue('locExchangeNotes')
          End
      End
      If not (p_web.GSV('Hide:SplitJob') = 1 OR p_web.GSV('exchangeOrderCreated') = 1)
          If p_web.IfExistsValue('locSplitJob')
            locSplitJob = p_web.GetValue('locSplitJob')
          End
      End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord
      ! Validate Form
      if (p_web.GSV('locPasswordValidated') = 0)
          loc:Invalid = 'locUserPassword'
          loc:Alert = 'Your password has not yet been validated'
          p_web.SSV('locUserPassword','')
          exit
      end ! if (p_web.GSV('local:PasswordValidated') = 0)
  
      if (p_web.GSV('locValidationFailed') = 1)
          loc:Invalid = 'locUserPassword'
          loc:Alert = p_web.GSV('locErrorMessage')
          exit
      end ! if (p_web.GSV('locValidationFailed') = 1)
  
  
  
      if ((p_web.GSV('locEngineeringOption') <> p_web.GSV('locNewEngineeringOption')) and p_web.GSV('locNewEngineeringOption') <> '')
          p_web.SSV('locEngineeringOption',p_web.GSV('locNewEngineeringOption'))
  
          case p_web.GSV('locNewEngineeringOption')
          of 'ARC Repair'
              if (p_web.GSV('BookingSite') = 'RRC' and p_web.GSV('jobe:HubRepair') = 0)
                  p_web.SSV('jobe:HubRepair',1)
                  do set:HubRepair
              end ! if (p_web.GSV('BookingSite') = 'RRC' and p_web.GSV('jobe:HubRepair') = 0)
                  p_web.SSV('AddToAudit:Type','JOB')
                  p_web.SSV('AddToAudit:Action','ENGINEERING OPTION SELECTED: ARC REPAIR')
                  p_web.SSV('AddToAudit:Notes','')
                  AddToAudit(p_web)
  
                  p_web.SSV('jobe:Engineer48HourOption',2)
  
  
          of '7 Day TAT'
              if (p_web.GSV('BookingSite') = 'RRC' and p_web.GSV('jobe:HubRepair') = 0)
                  p_web.SSV('jobe:HubRepair',1)
                  do set:HubRepair
              end ! if (p_web.GSV('BookingSite') = 'RRC' and p_web.GSV('jobe:HubRepair') = 0)
                  p_web.SSV('AddToAudit:Type','JOB')
                  p_web.SSV('AddToAudit:Action','ENGINEERING OPTION SELECTED: 7 DAY TAT')
                  p_web.SSV('AddToAudit:Notes','')
                  AddToAudit(p_web)
  
                  p_web.SSV('jobe:Engineer48HourOption',3)
  
          of 'Standard Repair'
              p_web.SSV('AddToAudit:Type','JOB')
              p_web.SSV('AddToAudit:Action','ENGINEERING OPTION SELECTED: STANDARD REPAIR')
              p_web.SSV('AddToAudit:Notes','')
              AddToAudit(p_web)
              p_web.SSV('jobe:Engineer48HourOption',4)
          end !case p_web.GSV('locNewEngineeringOption')
  
          Access:JOBSE.Clearkey(jobe:RefNumberKey)
          jobe:RefNumber    = p_web.GSV('wob:RefNumber')
          if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
              p_web.SessionQueueToFile(JOBSE)
              Access:JOBSE.tryUpdate()
          else ! if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
              ! Error
          end ! if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
  
          Access:JOBS.Clearkey(job:Ref_Number_Key)
          job:Ref_Number    = p_web.GSV('wob:RefNumber')
          if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
              ! Found
              p_web.SessionQueueToFile(JOBS)
              Access:JOBS.tryUpdate()
          else ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
              ! Error
          end ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
  
          Access:WEBJOB.Clearkey(wob:RefNumberKey)
          wob:RefNumber = p_web.GSV('wob:RefNumber')
          if (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
              ! Found
              p_web.SessionQueueToFile(WEBJOB)
              Access:WEBJOB.TryUpdate()
          end ! if (Access:WEBJOB.TryFetch(wob:Ref_Number_Key) = Level:Benign)
  
  
          p_web.SSV('FirstTime',1)
  
  !
  !        p_web.SSV('locMessage','Your changes have been saved')
  !        p_web.SSV('loc:Alert','Your changes have been saved')
  
      end ! if (p_web.GSV('locEngineeringOption') <> p_web.GSV('locNewEngineeringOption'))
  
  
  
  

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormEngineeringOption_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormEngineeringOption_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
    loc:InvalidTab += 1
    do ValidateValue::locUserPassword
    If loc:Invalid then exit.
  ! tab = 1
    loc:InvalidTab += 1
    do ValidateValue::locErrorMessage
    If loc:Invalid then exit.
    do ValidateValue::locEngineeringOption
    If loc:Invalid then exit.
    do ValidateValue::locNewEngineeringOption
    If loc:Invalid then exit.
  ! tab = 3
    loc:InvalidTab += 1
    do ValidateValue::text:ExchangeOrder
    If loc:Invalid then exit.
    do ValidateValue::job:DOP
    If loc:Invalid then exit.
    do ValidateValue::locExchangeManufacturer
    If loc:Invalid then exit.
    do ValidateValue::locExchangeModelNumber
    If loc:Invalid then exit.
    do ValidateValue::locExchangeNotes
    If loc:Invalid then exit.
    do ValidateValue::locWarningMessage
    If loc:Invalid then exit.
    do ValidateValue::locSplitJob
    If loc:Invalid then exit.
    do ValidateValue::button:CreateOrder
    If loc:Invalid then exit.
    do ValidateValue::button:PrintOrder
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('FormEngineeringOption:Primed',0)
  p_web.StoreValue('locUserPassword')
  p_web.StoreValue('')
  p_web.StoreValue('locEngineeringOption')
  p_web.StoreValue('locNewEngineeringOption')
  p_web.StoreValue('')
  p_web.StoreValue('job:DOP')
  p_web.StoreValue('locExchangeManufacturer')
  p_web.StoreValue('locExchangeModelNumber')
  p_web.StoreValue('locExchangeNotes')
  p_web.StoreValue('')
  p_web.StoreValue('locSplitJob')
  p_web.StoreValue('')
  p_web.StoreValue('')

BannerNewJobBooking  PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
FilesOpened     Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  GlobalErrors.SetProcedureName('BannerNewJobBooking')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'BannerNewJobBooking_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('BannerNewJobBooking','')
    p_web.DivHeader('BannerNewJobBooking',p_web.combine(p_web.site.style.formdiv,'fdiv'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('BannerNewJobBooking') = 0
        p_web.AddPreCall('BannerNewJobBooking')
        p_web.DivHeader('popup_BannerNewJobBooking','nt-hidden')
        p_web.DivHeader('BannerNewJobBooking',p_web.combine(p_web.site.style.formdiv,'fdiv'))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_BannerNewJobBooking_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_BannerNewJobBooking_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferBannerNewJobBooking',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_BannerNewJobBooking',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBannerNewJobBooking',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_BannerNewJobBooking',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBannerNewJobBooking',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_BannerNewJobBooking',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_BannerNewJobBooking',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBannerNewJobBooking',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_BannerNewJobBooking',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBannerNewJobBooking',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_BannerNewJobBooking',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_BannerNewJobBooking',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('BannerNewJobBooking')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('BannerNewJobBooking_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'BannerNewJobBooking'
    end
    p_web.formsettings.proc = 'BannerNewJobBooking'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine

AfterLookup Routine
  loc:TabNumber = -1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('BannerNewJobBooking_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferBannerNewJobBooking')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('BannerNewJobBooking_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('BannerNewJobBooking_ChainTo')
    loc:formaction = p_web.GetSessionValue('BannerNewJobBooking_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction

GenerateForm   Routine
  do LoadRelatedRecords
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
    do SendPacket
    Do heading
    do SendPacket
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
    Packet = clip(Packet) & p_web.DivHeader('Tab_BannerNewJobBooking',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="BannerNewJobBooking_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      packet = clip(packet) & '</div><13,10>' ! end id="BannerNewJobBooking_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'BannerNewJobBooking_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="BannerNewJobBooking_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'BannerNewJobBooking_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_BannerNewJobBooking')>0,p_web.GSV('showtab_BannerNewJobBooking'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_BannerNewJobBooking'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_BannerNewJobBooking') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_BannerNewJobBooking'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_BannerNewJobBooking')>0,p_web.GSV('showtab_BannerNewJobBooking'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_BannerNewJobBooking') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_BannerNewJobBooking_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_BannerNewJobBooking')>0,p_web.GSV('showtab_BannerNewJobBooking'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"BannerNewJobBooking",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_BannerNewJobBooking')>0,p_web.GSV('showtab_BannerNewJobBooking'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_BannerNewJobBooking_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('BannerNewJobBooking') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('BannerNewJobBooking')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine


NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_BannerNewJobBooking_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('BannerNewJobBooking_form:ready_',1)

  p_web.SetSessionValue('BannerNewJobBooking_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_BannerNewJobBooking',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('BannerNewJobBooking_form:ready_',1)
  p_web.SetSessionValue('BannerNewJobBooking_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_BannerNewJobBooking',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('BannerNewJobBooking_form:ready_',1)
  p_web.SetSessionValue('BannerNewJobBooking_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('BannerNewJobBooking:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('BannerNewJobBooking_form:ready_',1)
  p_web.SetSessionValue('BannerNewJobBooking_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('BannerNewJobBooking:Primed',0)
  p_web.setsessionvalue('showtab_BannerNewJobBooking',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('BannerNewJobBooking_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('BannerNewJobBooking_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('BannerNewJobBooking:Primed',0)

heading  Routine
  packet = clip(packet) & p_web.AsciiToUTF(|
    '<<table class="TopBanner"><13,10>'&|
    '    <<tr><13,10>'&|
    '        <<td width="140" aligh="left"><<img src="/images/bannerleft.gif" width="140" heigh="30"/><</td><13,10>'&|
    '        <<td width="670" align="center" class="BannerText">New Job Booking<</td><13,10>'&|
    '        <<td width="140" aligh="right"><<img src="/images/bannerright.gif" width="140" heigh="30"/><</td><13,10>'&|
    '    <</tr><13,10>'&|
    '    <<tr><13,10>'&|
    '        <<td width="140"><</td><13,10>'&|
    '        <<td width="670"><</td><13,10>'&|
    '        <<td width="140" align="right" class="SmallText"><<!-- Net:s:VersionNumber --><</td><13,10>'&|
    '    <</tr><13,10>'&|
    '<</table><13,10>'&|
    '<13,10>'&|
    '',net:OnlyIfUTF)
BrowseLocationHistory PROCEDURE  (NetWebServerWorker p_web)
locUserName          STRING(60)                            !
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256),dim(Net:MaxKeyFields)
idx           String(Net:HashSize)
sub           Long
            End
lot:TheDate:IsInvalid  Long
lot:TheTime:IsInvalid  Long
locUserName:IsInvalid  Long
lot:PreviousLocation:IsInvalid  Long
lot:NewLocation:IsInvalid  Long
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:DefaultSelection    String(Net:HashSize)
loc:ActualSelection     String(Net:HashSize)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
!loc:pagename            String(256)
loc:NavButtonPosition    Long
loc:UpdateButtonPosition Long
loc:SelectionMethod      Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorCase         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
loc:lookupdone          Long
loc:FormPopup           Long
ThisView            View(LOCATLOG)
                      Project(lot:RecordNumber)
                      Project(lot:TheDate)
                      Project(lot:TheTime)
                      Project(lot:PreviousLocation)
                      Project(lot:NewLocation)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
Loc:popup             Long
loc:poppedup          Long,static,thread
FilesOpened     Long
JOBS::State  USHORT
USERS::State  USHORT
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return 
  End
  GlobalErrors.SetProcedureName('BrowseLocationHistory')
  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseLocationHistory:NoForm')
      loc:NoForm = p_web.GetValue('BrowseLocationHistory:NoForm')
      loc:FormName = p_web.GetValue('BrowseLocationHistory:FormName')
    else
      loc:FormName = 'BrowseLocationHistory_frm'
    End
    p_web.SSV('BrowseLocationHistory:NoForm',loc:NoForm)
    p_web.SSV('BrowseLocationHistory:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseLocationHistory:NoForm')
    loc:FormName = p_web.GSV('BrowseLocationHistory:FormName')
  end
  case p_web.site.formpopups
  of -1 ; loc:FormPopup = Net:Page
  of 0 ; loc:FormPopup = Net:Page
  of 1 ; loc:FormPopup = Net:Popup
  End
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseLocationHistory') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseLocationHistory')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if 1 = 2
  elsif p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  elsif p_web.IfExistsValue('_EIPRow_')
    do CallRow
  elsif p_web.GetValue('_CallPopups') > 0
    do CallPopups
  elsif p_web.IfExistsValue('_GetSecwinSettings')
      p_web.SetValue('Secwin_' & 'BrowseLocationHistory' & 'AccessGroupsArray','1') 
      p_web.SetValue('Secwin_AccessWindowName','BrowseLocationHistory')
      
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallPopups  Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
loc:CallPopups   Long
loc:name         String(255)
  code
  if loc:poppedup = 0
    loc:CallPopups = p_web.GetValue('_CallPopups')
    if loc:CallPopups = 1 !and p_web.GetPreCall('BrowseLocationHistory') = 0! for = 1 include popup divs and scripts for this browse
      !p_web.AddPreCall('BrowseLocationHistory')
      p_web.DivHeader('popup_BrowseLocationHistory','nt-hidden')
      p_web.DivHeader('BrowseLocationHistory',p_web.combine(p_web.site.style.browsediv,'fdiv'))
      p_web.DivFooter()
      p_web.DivFooter()
      loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(400)&', modal: true, position: [''center'',15]'
      packet =  p_web.jQuery('#' & lower('popup_BrowseLocationHistory_div'),'dialog',loc:options) & |
      p_web.jQuery('#' & lower('popup_BrowseLocationHistory_div'),'removeClass','''nt-hidden''')
    End
    ! for > 0 add in form, EIP-lookups and browse object
    do Popups
    loc:popup = 1
    do ClosingScripts
    do SendPacket
    loc:poppedup = 1
  end

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web.DivHeader('BrowseLocationHistory',p_web.combine(p_web.site.style.browsediv,'fdiv'))
  if loc:ParentSilent = 0
    do GenerateBrowse
    if p_web.RequestAjax = 1
      p_web.Script('BrowseProducts.pcv();')
    end
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web.DivFooter()
  do Children
  do Popups
  if loc:poppedup = 0
    do ClosingScripts
  end
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(LOCATLOG,lot:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'LOT:THEDATE') then p_web.SetValue('BrowseLocationHistory_sort','1')
    ElsIf (loc:vorder = 'LOT:THETIME') then p_web.SetValue('BrowseLocationHistory_sort','2')
    ElsIf (loc:vorder = 'LOCUSERNAME') then p_web.SetValue('BrowseLocationHistory_sort','3')
    ElsIf (loc:vorder = 'LOT:PREVIOUSLOCATION') then p_web.SetValue('BrowseLocationHistory_sort','4')
    ElsIf (loc:vorder = 'LOT:NEWLOCATION') then p_web.SetValue('BrowseLocationHistory_sort','5')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseLocationHistory:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseLocationHistory:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseLocationHistory:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseLocationHistory:LookupField')
    loc:selecting = 0
  End

Popups Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  If loc:popup = 0
  End
SetFormAction  Routine
  loc:formaction = 'BrowseLocationHistory'

GotFocusBack   Routine

GenerateBrowse Routine
  data
loc:viewoptions  Long
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  ! Set general Browse options
  loc:NavButtonPosition   = Net:Below
  loc:UpdateButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:FileLoad
  loc:FillBack         = 0
  loc:Sorting          = Net:ServerSort
  if p_web.GetValue('_popup_') = 1
    loc:popup = 1
  end
  ! Set Locator Options
  loc:LocatorPosition  = Net:Below
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  p_web.ClearBrowse('BrowseLocationHistory')
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 10
  End
  loc:ActualSelection = ''
  !loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseLocationHistory_sort',net:DontEvaluate)
  p_web.SetSessionValue('BrowseLocationHistory_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'lot:TheDate','-lot:TheDate')
    Loc:LocateField = 'lot:TheDate'
    Loc:LocatorCase = 0
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'lot:TheTime','-lot:TheTime')
    Loc:LocateField = 'lot:TheTime'
    Loc:LocatorCase = 0
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'locUserName','-locUserName')
    Loc:LocateField = 'locUserName'
    Loc:LocatorCase = 0
  of 4
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(lot:PreviousLocation)','-UPPER(lot:PreviousLocation)')
    Loc:LocateField = 'lot:PreviousLocation'
    Loc:LocatorCase = 0
  of 5
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(lot:NewLocation)','-UPPER(lot:NewLocation)')
    Loc:LocateField = 'lot:NewLocation'
    Loc:LocatorCase = 0
  end
  if loc:vorder = ''
    loc:vorder = '-lot:RecordNumber'
  end
  If False ! add range fields to sort order
  Else
    If Instring('LOT:REFNUMBER',upper(loc:vOrder),1,1) = 0
      loc:vOrder = 'lot:RefNumber,' & loc:vorder
    End
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('lot:TheDate')
    loc:SortHeader = p_web.Translate('Date')
    p_web.SetSessionValue('BrowseLocationHistory_LocatorPic','@d6')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
    loc:LocatorType = Net:Date
  Of upper('lot:TheTime')
    loc:SortHeader = p_web.Translate('Time')
    p_web.SetSessionValue('BrowseLocationHistory_LocatorPic','@t1b')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
  Of upper('locUserName')
    loc:SortHeader = p_web.Translate('Username')
    p_web.SetSessionValue('BrowseLocationHistory_LocatorPic','@s60')
  Of upper('lot:PreviousLocation')
    loc:SortHeader = p_web.Translate('Previous Location')
    p_web.SetSessionValue('BrowseLocationHistory_LocatorPic','@s30')
  Of upper('lot:NewLocation')
    loc:SortHeader = p_web.Translate('New Location')
    p_web.SetSessionValue('BrowseLocationHistory_LocatorPic','@s30')
  End
  If loc:selecting = 1
    p_web.GetSettings(p_web.GetSessionValue('Push1'))
    loc:selectaction = p_web.FormSettings.ParentPage !p_web.GetSessionValue('BrowseLocationHistory:LookupFrom')
  End!Else
  do SendPacket
  do SetFormAction
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseLocationHistory:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseLocationHistory:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseLocationHistory:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="LOCATLOG"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="lot:RecordNumberKey"></input><13,10>'
  end
  case p_web.GetValue('refresh') ; of 'next' orof 'previous' orof 'first' orof 'last'
    p_web.Script('try{{BrowseLocationHistory.restoreFocus();} catch(err){{};')
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseLocationHistory',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      case loc:locatortype
      of Net:Position
      orof Net:Date
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
      of Net:Contains
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
      of Net:Begins
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
      End
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,'Locator')&'"><tr>' &|
      Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
      Case Loc:LocatorType
      of Net:Date
        packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator2BrowseLocationHistory','',p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,, p_web.GSV('BrowseLocationHistory_LocatorPic'),,,'onchange="BrowseLocationHistory.locate(''Locator2BrowseLocationHistory'',this.value);"',,0,,,) & '</div></td>'
      Else
        packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator2BrowseLocationHistory',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,'size="30" onchange="BrowseLocationHistory.locate(''Locator2BrowseLocationHistory'',this.value);" ',,,,,,) & '</td>'
      End
      If loc:LocatorSearchButton or loc:LocatorClearButton
        packet = clip(packet) & '<td><div id="BrowseLocationHistory_locate_a" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
        If loc:LocatorSearchButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
        End
        If loc:LocatorClearButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseLocationHistory.cl(''BrowseLocationHistory'');')
        End
        packet = clip(packet) & '</div><13,10></td><13,10>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
      If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'BrowseLocationHistory_locate_a','buttonset',loc:options)
      End
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & p_web.DivHeader('BrowseLocationHistory_table',p_web.Combine(p_web.site.style.BrowseTableDiv,''),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,,'sortable')&'" id="BrowseLocationHistory_tbl">'
  Else
    packet = clip(packet) & p_web.DivHeader('BrowseLocationHistory_table',p_web.Combine(p_web.site.style.BrowseTableDiv,''),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,)&'" id="BrowseLocationHistory_tbl">'
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'1','BrowseLocationHistory',p_web.Translate('Date'),'Click here to sort by Date',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'2','BrowseLocationHistory',p_web.Translate('Time'),'Click here to sort by Time',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'3','BrowseLocationHistory',p_web.Translate('Username'),'Username',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'4','BrowseLocationHistory',p_web.Translate('Previous Location'),'Click here to sort by Previous Location',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'5','BrowseLocationHistory',p_web.Translate('New Location'),'Click here to sort by New Location',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:LocateField = 'lot:TheDate' then Loc:NoBuffer = 1.
  If Loc:NoBuffer = 0
    Buffer(ThisView,10,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  If Instring('lot:recordnumber',lower(loc:vorder),1,1) = 0 !and LOCATLOG{prop:SQLDriver} = 1
    loc:vorder = Choose(loc:vorder='','lot:RecordNumber',clip(loc:vorder) & ',' & 'lot:RecordNumber')
  End
  Loc:Selected = Choose(p_web.IfExistsValue('lot:RecordNumber'),p_web.GetValue('lot:RecordNumber'),p_web.GetSessionValue('lot:RecordNumber'))
  ThisView{prop:order} = p_web.CleanFilter(ThisView,clip(loc:vorder))
    job:Ref_Number = p_web.RestoreValue('job:Ref_Number')
    loc:FilterWas = 'lot:RefNumber = ' & job:Ref_Number
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseLocationHistory',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseLocationHistory_Filter')
    p_web.SetSessionValue('BrowseLocationHistory_FirstValue','')
    p_web.SetSessionValue('BrowseLocationHistory_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,LOCATLOG,lot:RecordNumberKey,loc:PageRows,'BrowseLocationHistory',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,loc:ViewOptions,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled,Loc:LocatorCase)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or Loc:LocatorType = Net:Date or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position or loc:LocatorType = Net:Date
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If LOCATLOG{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(LOCATLOG,loc:firstvalue)
              Reset(ThisView,LOCATLOG)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If LOCATLOG{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(LOCATLOG,loc:lastvalue)
            Reset(ThisView,LOCATLOG)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(lot:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td class="'&p_web.combine(p_web.site.style.BrowseEmpty,)&'">'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:NavButtonPosition=Net:Above or (loc:NavButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseLocationHistory_nav_a" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseLocationHistory.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseLocationHistory.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseLocationHistory.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseLocationHistory.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseLocationHistory_nav_a','buttonset',loc:options)
          End
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:UpdateButtonPosition=Net:Above or (loc:UpdateButtonPosition=Net:Both and loc:found > 0))
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseLocationHistory',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseLocationHistory_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseLocationHistory_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          case loc:locatortype
          of Net:Position
          orof Net:Date
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
          of Net:Contains
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
          of Net:Begins
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
          End
          packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,'Locator')&'"><tr>' &|
          Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
          Case Loc:LocatorType
          of Net:Date
            packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator1BrowseLocationHistory','',p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,, p_web.GSV('BrowseLocationHistory_LocatorPic'),,,'onchange="BrowseLocationHistory.locate(''Locator1BrowseLocationHistory'',this.value);"',,0,,,) & '</div></td>'
          Else
            packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator1BrowseLocationHistory',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,'size="30" onchange="BrowseLocationHistory.locate(''Locator1BrowseLocationHistory'',this.value);" ',,,,,,) & '</td>'
          End
          If loc:LocatorSearchButton or loc:LocatorClearButton
            packet = clip(packet) & '<td><div id="BrowseLocationHistory_locate_b" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
            If loc:LocatorSearchButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
            End
            If loc:LocatorClearButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseLocationHistory.cl(''BrowseLocationHistory'');')
            End
            packet = clip(packet) & '</div><13,10></td><13,10>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
          If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseLocationHistory_locate_b','buttonset',loc:options)
          End
      End
    End
  End
  p_web.SetSessionValue('BrowseLocationHistory_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseLocationHistory_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:NavButtonPosition=Net:Below or loc:NavButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseLocationHistory_nav_b" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseLocationHistory.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseLocationHistory.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseLocationHistory.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseLocationHistory.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseLocationHistory_nav_b','buttonset',loc:options)
          End
        end
        do SendPacket
  End
  If loc:UpdateButtonPosition=Net:Below or loc:UpdateButtonPosition=Net:Both
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
  data
loc:rowclick  string(1024)
  code
    Access:USERS.Clearkey(use:User_Code_Key)
    use:User_Code    = lot:UserCode
    if (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
        ! Found
        locUserName = clip(use:Forename) & ' ' & clip(use:Surname)
    else ! if (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
        ! Error
        locUserName = ''
    end ! if (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
    loc:field = p_web.AddBrowseValue('BrowseLocationHistory','LOCATLOG',lot:RecordNumberKey) !lot:RecordNumber
    p_web._thisrow = p_web._nocolon('lot:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if Loc:LocatorValue <> '' and loc:ActualSelection = ''
        loc:checked = 'checked'
        do SetSelection
      elsif loc:ActualSelection = '' and lot:RecordNumber = p_web.GetValue('lot:RecordNumber')
         loc:checked = 'checked'
         do SetSelection
      elsif loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseLocationHistory:LookupField')) = lot:RecordNumber and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      else
        loc:checked = Choose((lot:RecordNumber = loc:selected) and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      end
      If(loc:SelectionMethod  = Net:Radio)
        loc:RowStyle = 'class="' & p_web.combine(p_web.site.style.browserow,) &'"'
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'data-nt-id="'& p_web.AddBrowseValue('BrowseLocationHistory','LOCATLOG',lot:RecordNumberKey) &'" onclick="'
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If LOCATLOG{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(LOCATLOG)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            loc:DefaultSelection = loc:field
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If LOCATLOG{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(LOCATLOG)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            loc:DefaultSelection = loc:field
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'>'&p_web.CreateInput('radio','_bidv_',clip(loc:field),,loc:checked,'')&'</td>'&CRLF
          If loc:DefaultSelection = ''
            loc:DefaultSelection = loc:field
          End
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          If loc:DefaultSelection = '' or loc:direction < 0
            loc:DefaultSelection = loc:field
          End
        End
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('RightJustify')&'" width="'&clip(60)&'"><13,10>'
          end ! loc:eip = 0
          do value::lot:TheDate
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('RightJustify')&'" width="'&clip(60)&'"><13,10>'
          end ! loc:eip = 0
          do value::lot:TheTime
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td width="'&clip(120)&'"><13,10>'
          end ! loc:eip = 0
          do value::locUserName
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td width="'&clip(150)&'"><13,10>'
          end ! loc:eip = 0
          do value::lot:PreviousLocation
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td width="'&clip(150)&'"><13,10>'
          end ! loc:eip = 0
          do value::lot:NewLocation
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr ' & clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Idx = p_web.AddBrowseValue('BrowseLocationHistory','LOCATLOG',lot:RecordNumberKey)
  TableQueue.Id[1] = lot:RecordNumber

ClosingScripts  Routine
  data
Rtn_SecwinProcedureName         string(252)
  code
  If p_web.RequestAjax = 0
    do SetFormAction
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var btiBrowseLocationHistory;if (btiBrowseLocationHistory != 1){{var BrowseLocationHistory=new browseTable(''BrowseLocationHistory'','''&clip(loc:formname)&|
      ''','''&p_web._jsok('lot:RecordNumber',Net:Parameter)&''',2,'''&clip(loc:divname)&''',1,1,1,'''&|
      clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',0,'''&|
      p_web.Translate('Are you sure you want to delete this record?')&''','''&clip(loc:ActualSelection)&''','''&clip(loc:selectaction)&''','''&|
      clip(loc:formactiontarget)&''','''','&loc:popup&',0,'''','''',"' & clip(Rtn_SecwinProcedureName) & '");<13,10>'&|
      'BrowseLocationHistory.setGreenBar('''&p_web.ColorWeb(p_web.Site.Style.BrowseHighlightColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOneColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseTwoColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOverColor)&''');<13,10>' &|
      'BrowseLocationHistory.applyGreenBar();btiBrowseLocationHistory=1};<13,10>' &|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseLocationHistory')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseLocationHistory')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseLocationHistory')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseLocationHistory')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(LOCATLOG)
  p_web._CloseFile(JOBS)
  p_web._CloseFile(USERS)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(LOCATLOG)
  Bind(lot:Record)
  Clear(lot:Record)
  NetWebSetSessionPics(p_web,LOCATLOG)
  p_web._OpenFile(JOBS)
  Bind(job:Record)
  NetWebSetSessionPics(p_web,JOBS)
  p_web._OpenFile(USERS)
  Bind(use:Record)
  NetWebSetSessionPics(p_web,USERS)

Children Routine
  if loc:selecting = 0
    If p_web.RequestAjax = 0
      do StartChildren
    Else
      do AjaxChildren
    End
  end

AjaxChildren  Routine
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue('lot:RecordNumber',p_web.GetValue('lot:RecordNumber'))
   p_web.DeleteValue('_Clicked')
   loc:found = 1
   do Children
! ----------------------------------------------------------------------------------------
CallRow  Routine
  data
loc:result  long
  code
  do OpenFilesB
  lot:RecordNumber = p_web.GSV('lot:RecordNumber')
  loc:result = p_web._GetFile(LOCATLOG,lot:RecordNumberKey)
  loc:eip = 1
  loc:viewstate = p_web.escape(p_web.Base64Encode(clip(lot:RecordNumber)))
  do BrowseRow
  do SendPacket
  do ClosefilesB

! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(LOCATLOG)
  Case upper(p_web.GetValue('_EIPClm'))
  Else
    case p_web.GetValue('_action_')
    of Net:InsertRecord
    orof Net:ChangeRecord
    orof Net:DeleteRecord
    orof Net:CopyRecord
      loc:eip = 0
      do CallBrowse
    end
  End
  do GotFocusBack
  p_web._CloseFile(LOCATLOG)
! ----------------------------------------------------------------------------------------
value::lot:TheDate   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseLocationHistory_lot:TheDate_'&lot:RecordNumber,'RightJustify',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(lot:TheDate,'@d6')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::lot:TheTime   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseLocationHistory_lot:TheTime_'&lot:RecordNumber,'RightJustify',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(lot:TheTime,'@t1b')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::locUserName   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseLocationHistory_locUserName_'&lot:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(locUserName,'@s60')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::lot:PreviousLocation   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseLocationHistory_lot:PreviousLocation_'&lot:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(lot:PreviousLocation,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::lot:NewLocation   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseLocationHistory_lot:NewLocation_'&lot:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(lot:NewLocation,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  p_web._OpenFile(JOBS)
  p_web._OpenFile(USERS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(USERS)
     FilesOpened = False
  END
  return
!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
CheckForDuplicate  Routine
SetSelection  Routine
  loc:ActualSelection = loc:field
  p_web.SetSessionValue('lot:RecordNumber',lot:RecordNumber)

MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:ActualSelection = ''
    loc:ActualSelection = loc:DefaultSelection
    p_web.GetBrowseValue(loc:ActualSelection,Net:Web:SessionQueue) ! so children are primed with correct sessionValue
  End

  If loc:ActualSelection <> ''
    TableQueue.Kind = Net:RowTable
    get(TableQueue,TableQueue.Kind)
    if Errorcode() = 0
      TableQueue.Row = sub(TableQueue.Row,1,len(clip(TableQueue.Row))-1) &  ' data-nt-rows="'&loc:RowsHigh&'" data-nt-value="'&clip(loc:ActualSelection)&'"' & '>'
      Put(TableQueue)
    End
  End

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead class="'&p_web.combine(p_web.site.style.BrowseHeader,'')&'"><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot class="'&p_web.combine(p_web.site.style.BrowseFooter,'BrowseFooter')&'"><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody class="'&p_web.combine(p_web.site.style.BrowseBody,)&'"><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table><13,10>' & p_web.DivFooter(Net:NoSend) ! Table Div
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
FormBrowseLocationHistory PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
FilesOpened     Long
BrowseLocationHistory:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormBrowseLocationHistory')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'FormBrowseLocationHistory_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('FormBrowseLocationHistory','')
    p_web.DivHeader('FormBrowseLocationHistory',p_web.combine(p_web.site.style.formdiv,))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('FormBrowseLocationHistory') = 0
        p_web.AddPreCall('FormBrowseLocationHistory')
        p_web.DivHeader('popup_FormBrowseLocationHistory','nt-hidden')
        p_web.DivHeader('FormBrowseLocationHistory',p_web.combine(p_web.site.style.formdiv,))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_FormBrowseLocationHistory_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_FormBrowseLocationHistory_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormBrowseLocationHistory',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_FormBrowseLocationHistory',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormBrowseLocationHistory',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_FormBrowseLocationHistory',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormBrowseLocationHistory',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormBrowseLocationHistory',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_FormBrowseLocationHistory',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormBrowseLocationHistory',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_FormBrowseLocationHistory',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormBrowseLocationHistory',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormBrowseLocationHistory',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormBrowseLocationHistory',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('FormBrowseLocationHistory')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE
  loc:EipClm = upper(p_web.GetValue('_EIPClm'))
  p_web.DeleteValue('_EIPClm')
  case loc:EipClm
  of ''
    case upper(p_web.GetValue('_calledfrom_'))
    of upper('BrowseLocationHistory')
      do Value::BrowseLocationHistory
    end
  end

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FormBrowseLocationHistory_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'FormBrowseLocationHistory'
    end
    p_web.formsettings.proc = 'FormBrowseLocationHistory'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('FormBrowseLocationHistory_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'ViewJob'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormBrowseLocationHistory_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormBrowseLocationHistory_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormBrowseLocationHistory_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'ViewJob'

GenerateForm   Routine
  do LoadRelatedRecords
      p_web.site.CancelButton.TextValue = 'Close'
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  loc:FormHeading = p_web.Translate('Location History',0)
  If loc:FormHeading
    if loc:popup
      packet = clip(packet) & p_web.jQuery('#popup_'&lower('FormBrowseLocationHistory')&'_div','dialog','"option","title",'&p_web.jsString(loc:FormHeading,0),,0)
    else
      packet = clip(packet) & lower('<div id="form-access-FormBrowseLocationHistory"></div>')
      packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formheading,)&'">'&clip(loc:FormHeading)&'</div>'&CRLF
    end
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_FormBrowseLocationHistory',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_FormBrowseLocationHistory0_div')&'">'&p_web.Translate()&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="FormBrowseLocationHistory_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('FormBrowseLocationHistory_BrowseLocationHistory_embedded_div')&''');'
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.cancel();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CancelButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="FormBrowseLocationHistory_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'FormBrowseLocationHistory_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="FormBrowseLocationHistory_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'FormBrowseLocationHistory_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_FormBrowseLocationHistory')>0,p_web.GSV('showtab_FormBrowseLocationHistory'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_FormBrowseLocationHistory'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_FormBrowseLocationHistory') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_FormBrowseLocationHistory'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_FormBrowseLocationHistory')>0,p_web.GSV('showtab_FormBrowseLocationHistory'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_FormBrowseLocationHistory') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_FormBrowseLocationHistory_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_FormBrowseLocationHistory')>0,p_web.GSV('showtab_FormBrowseLocationHistory'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"FormBrowseLocationHistory",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_FormBrowseLocationHistory')>0,p_web.GSV('showtab_FormBrowseLocationHistory'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_FormBrowseLocationHistory_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('FormBrowseLocationHistory') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('FormBrowseLocationHistory')
    p_web.SetValue('_CallPopups',2)
    If p_web.GetPreCall('BrowseLocationHistory') = 0
      p_web.SetValue('BrowseLocationHistory:NoForm',1)
      p_web.SetValue('BrowseLocationHistory:FormName',loc:formname)
      p_web.SetValue('BrowseLocationHistory:parentIs','Form')
      p_web.SetValue('_parentProc','FormBrowseLocationHistory')
      BrowseLocationHistory(p_web)
      p_web.SetValue('_CallPopups',0)
      p_web.DeleteValue('BrowseLocationHistory:NoForm')
      p_web.DeleteValue('BrowseLocationHistory:FormName')
      p_web.DeleteValue('BrowseLocationHistory:parentIs')
      p_web.DeleteValue('_parentProc')
    End
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate()&'</a></h3>' & CRLF & p_web.DivHeader('tab_FormBrowseLocationHistory0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_FormBrowseLocationHistory0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_FormBrowseLocationHistory0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_FormBrowseLocationHistory0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,)
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_FormBrowseLocationHistory0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend)
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_FormBrowseLocationHistory0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain">' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_FormBrowseLocationHistory0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::BrowseLocationHistory
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Validate::BrowseLocationHistory  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  Elsif true
    p_web.StoreValue('lot:RecordNumber')
  End
  do ValidateValue::BrowseLocationHistory  ! copies value to session value if valid.
  do Comment::BrowseLocationHistory ! allows comment style to be updated.

ValidateValue::BrowseLocationHistory  Routine
    If not (1=0)
    End

Value::BrowseLocationHistory  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(1=0,1,0))
  ! --- BROWSE ---  BrowseLocationHistory --
  p_web.SetValue('BrowseLocationHistory:NoForm',1)
  p_web.SetValue('BrowseLocationHistory:FormName',loc:formname)
  p_web.SetValue('BrowseLocationHistory:parentIs','Form')
  p_web.SetValue('_parentProc','FormBrowseLocationHistory')
  if p_web.RequestAjax = 0
    p_web.SSV('FormBrowseLocationHistory:_popup_',p_web.GetValue('_popup_')) ! stores the current browse popup state
    packet = clip(packet) & '<div id="'&lower('FormBrowseLocationHistory_BrowseLocationHistory_embedded_div')&'"><!-- Net:BrowseLocationHistory --></div><13,10>'
    do SendPacket
    p_web.DivHeader('FormBrowseLocationHistory_' & lower('BrowseLocationHistory') & '_value')
    p_web.DivFooter()
  else
    if p_web.GetValue('_popup_') = 1
      p_web.SSV('FormBrowseLocationHistory:_popup_',1)
    elsif p_web.GSV('FormBrowseLocationHistory:_popup_') = 1
      p_web.SetValue('_popup_',1)
    end
    packet = clip(packet) & '<!-- Net:BrowseLocationHistory --><13,10>'
  end
  do SendPacket
Comment::BrowseLocationHistory  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if BrowseLocationHistory:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormBrowseLocationHistory_' & p_web._nocolon('BrowseLocationHistory') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('FormBrowseLocationHistory_nexttab_' & 0)
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_FormBrowseLocationHistory_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('FormBrowseLocationHistory_tab_' & 0)
    do GenerateTab0
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('FormBrowseLocationHistory_form:ready_',1)

  p_web.SetSessionValue('FormBrowseLocationHistory_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_FormBrowseLocationHistory',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('FormBrowseLocationHistory_form:ready_',1)
  p_web.SetSessionValue('FormBrowseLocationHistory_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormBrowseLocationHistory',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('FormBrowseLocationHistory_form:ready_',1)
  p_web.SetSessionValue('FormBrowseLocationHistory_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('FormBrowseLocationHistory:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('FormBrowseLocationHistory_form:ready_',1)
  p_web.SetSessionValue('FormBrowseLocationHistory_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('FormBrowseLocationHistory:Primed',0)
  p_web.setsessionvalue('showtab_FormBrowseLocationHistory',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormBrowseLocationHistory_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormBrowseLocationHistory_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
    loc:InvalidTab += 1
    do ValidateValue::BrowseLocationHistory
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('FormBrowseLocationHistory:Primed',0)
  p_web.StoreValue('')

SaveSessionVars  ROUTINE
     p_web.SSV('Ans',Ans) ! LONG
RestoreSessionVars  ROUTINE
     Ans = p_web.GSV('Ans') ! LONG
BrowseEngineerHistory PROCEDURE  (NetWebServerWorker p_web)
joeEngineersName     STRING(60)                            !
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256),dim(Net:MaxKeyFields)
idx           String(Net:HashSize)
sub           Long
            End
joeEngineersName:IsInvalid  Long
joe:DateAllocated:IsInvalid  Long
joe:AllocatedBy:IsInvalid  Long
joe:EngSkillLevel:IsInvalid  Long
joe:JobSkillLevel:IsInvalid  Long
joe:Status:IsInvalid  Long
joe:StatusDate:IsInvalid  Long
joe:StatusTime:IsInvalid  Long
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:DefaultSelection    String(Net:HashSize)
loc:ActualSelection     String(Net:HashSize)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
!loc:pagename            String(256)
loc:NavButtonPosition    Long
loc:UpdateButtonPosition Long
loc:SelectionMethod      Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorCase         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
loc:lookupdone          Long
loc:FormPopup           Long
ThisView            View(JOBSENG)
                      Project(joe:RecordNumber)
                      Project(joe:DateAllocated)
                      Project(joe:AllocatedBy)
                      Project(joe:EngSkillLevel)
                      Project(joe:JobSkillLevel)
                      Project(joe:Status)
                      Project(joe:StatusDate)
                      Project(joe:StatusTime)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
Loc:popup             Long
loc:poppedup          Long,static,thread
FilesOpened     Long
JOBS::State  USHORT
USERS::State  USHORT
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return 
  End
  GlobalErrors.SetProcedureName('BrowseEngineerHistory')
  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseEngineerHistory:NoForm')
      loc:NoForm = p_web.GetValue('BrowseEngineerHistory:NoForm')
      loc:FormName = p_web.GetValue('BrowseEngineerHistory:FormName')
    else
      loc:FormName = 'BrowseEngineerHistory_frm'
    End
    p_web.SSV('BrowseEngineerHistory:NoForm',loc:NoForm)
    p_web.SSV('BrowseEngineerHistory:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseEngineerHistory:NoForm')
    loc:FormName = p_web.GSV('BrowseEngineerHistory:FormName')
  end
  case p_web.site.formpopups
  of -1 ; loc:FormPopup = Net:Page
  of 0 ; loc:FormPopup = Net:Page
  of 1 ; loc:FormPopup = Net:Popup
  End
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseEngineerHistory') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseEngineerHistory')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if 1 = 2
  elsif p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  elsif p_web.IfExistsValue('_EIPRow_')
    do CallRow
  elsif p_web.GetValue('_CallPopups') > 0
    do CallPopups
  elsif p_web.IfExistsValue('_GetSecwinSettings')
      p_web.SetValue('Secwin_' & 'BrowseEngineerHistory' & 'AccessGroupsArray','1') 
      p_web.SetValue('Secwin_AccessWindowName','BrowseEngineerHistory')
      
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallPopups  Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
loc:CallPopups   Long
loc:name         String(255)
  code
  if loc:poppedup = 0
    loc:CallPopups = p_web.GetValue('_CallPopups')
    if loc:CallPopups = 1 !and p_web.GetPreCall('BrowseEngineerHistory') = 0! for = 1 include popup divs and scripts for this browse
      !p_web.AddPreCall('BrowseEngineerHistory')
      p_web.DivHeader('popup_BrowseEngineerHistory','nt-hidden')
      p_web.DivHeader('BrowseEngineerHistory',p_web.combine(p_web.site.style.browsediv,))
      p_web.DivFooter()
      p_web.DivFooter()
      loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(400)&', modal: true, position: [''center'',15]'
      packet =  p_web.jQuery('#' & lower('popup_BrowseEngineerHistory_div'),'dialog',loc:options) & |
      p_web.jQuery('#' & lower('popup_BrowseEngineerHistory_div'),'removeClass','''nt-hidden''')
    End
    ! for > 0 add in form, EIP-lookups and browse object
    do Popups
    loc:popup = 1
    do ClosingScripts
    do SendPacket
    loc:poppedup = 1
  end

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web.DivHeader('BrowseEngineerHistory',p_web.combine(p_web.site.style.browsediv,))
  if loc:ParentSilent = 0
    do GenerateBrowse
    if p_web.RequestAjax = 1
      p_web.Script('BrowseProducts.pcv();')
    end
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web.DivFooter()
  do Children
  do Popups
  if loc:poppedup = 0
    do ClosingScripts
  end
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(JOBSENG,joe:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'JOEENGINEERSNAME') then p_web.SetValue('BrowseEngineerHistory_sort','8')
    ElsIf (loc:vorder = 'JOEENGINEERSNAME') then p_web.SetValue('BrowseEngineerHistory_sort','8')
    ElsIf (loc:vorder = 'JOE:DATEALLOCATED') then p_web.SetValue('BrowseEngineerHistory_sort','1')
    ElsIf (loc:vorder = 'JOE:ALLOCATEDBY') then p_web.SetValue('BrowseEngineerHistory_sort','2')
    ElsIf (loc:vorder = 'JOE:ENGSKILLLEVEL') then p_web.SetValue('BrowseEngineerHistory_sort','3')
    ElsIf (loc:vorder = 'JOE:JOBSKILLLEVEL') then p_web.SetValue('BrowseEngineerHistory_sort','4')
    ElsIf (loc:vorder = 'JOE:STATUS') then p_web.SetValue('BrowseEngineerHistory_sort','5')
    ElsIf (loc:vorder = 'JOE:STATUSDATE') then p_web.SetValue('BrowseEngineerHistory_sort','6')
    ElsIf (loc:vorder = 'JOE:STATUSTIME') then p_web.SetValue('BrowseEngineerHistory_sort','7')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseEngineerHistory:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseEngineerHistory:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseEngineerHistory:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseEngineerHistory:LookupField')
    loc:selecting = 0
  End

Popups Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  If loc:popup = 0
  End
SetFormAction  Routine
  loc:formaction = 'BrowseEngineerHistory'

GotFocusBack   Routine

GenerateBrowse Routine
  data
loc:viewoptions  Long
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  ! Set general Browse options
  loc:NavButtonPosition   = Net:Both
  loc:UpdateButtonPosition   = Net:Both
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:FileLoad
  loc:FillBack         = 0
  loc:Sorting          = Net:ServerSort
  if p_web.GetValue('_popup_') = 1
    loc:popup = 1
  end
  ! Set Locator Options
  loc:LocatorPosition  = Net:Below
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  p_web.ClearBrowse('BrowseEngineerHistory')
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  p_web.site.closeButton.image = '/images/listback.png'
  p_web.site.closeButton.textValue = 'Back'
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 10
  End
  loc:ActualSelection = ''
  !loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseEngineerHistory_sort',net:DontEvaluate)
  If loc:vordernumber = 0
    loc:vordernumber = 8
  End
  p_web.SetSessionValue('BrowseEngineerHistory_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 8
    loc:vorder = Choose(Loc:SortDirection=1,'joeEngineersName','joeEngineersName')
    Loc:LocateField = 'joeEngineersName'
    Loc:LocatorCase = 0
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'joe:DateAllocated','-joe:DateAllocated')
    Loc:LocateField = 'joe:DateAllocated'
    Loc:LocatorCase = 0
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(joe:AllocatedBy)','-UPPER(joe:AllocatedBy)')
    Loc:LocateField = 'joe:AllocatedBy'
    Loc:LocatorCase = 0
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'joe:EngSkillLevel','-joe:EngSkillLevel')
    Loc:LocateField = 'joe:EngSkillLevel'
    Loc:LocatorCase = 0
  of 4
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(joe:JobSkillLevel)','-UPPER(joe:JobSkillLevel)')
    Loc:LocateField = 'joe:JobSkillLevel'
    Loc:LocatorCase = 0
  of 5
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(joe:Status)','-UPPER(joe:Status)')
    Loc:LocateField = 'joe:Status'
    Loc:LocatorCase = 0
  of 6
    loc:vorder = Choose(Loc:SortDirection=1,'joe:StatusDate','-joe:StatusDate')
    Loc:LocateField = 'joe:StatusDate'
    Loc:LocatorCase = 0
  of 7
    loc:vorder = Choose(Loc:SortDirection=1,'joe:StatusTime','-joe:StatusTime')
    Loc:LocateField = 'joe:StatusTime'
    Loc:LocatorCase = 0
  end
  if loc:vorder = ''
    Loc:LocateField = 'joe:RecordNumber'
    loc:sortheader = 'Record Number'
    loc:vorder = '-joe:RecordNumber'
  end
  If False ! add range fields to sort order
  Else
    If Instring('JOE:JOBNUMBER',upper(loc:vOrder),1,1) = 0
      loc:vOrder = 'joe:JobNumber,' & loc:vorder
    End
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('joeEngineersName')
    loc:SortHeader = p_web.Translate('Engineer Name')
    p_web.SetSessionValue('BrowseEngineerHistory_LocatorPic','@s60')
  Of upper('joe:DateAllocated')
    loc:SortHeader = p_web.Translate('Allocated')
    p_web.SetSessionValue('BrowseEngineerHistory_LocatorPic','@d6')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
    loc:LocatorType = Net:Date
  Of upper('joe:AllocatedBy')
    loc:SortHeader = p_web.Translate('Allocated By')
    p_web.SetSessionValue('BrowseEngineerHistory_LocatorPic','@s3')
  Of upper('joe:EngSkillLevel')
    loc:SortHeader = p_web.Translate('Eng Skill Level')
    p_web.SetSessionValue('BrowseEngineerHistory_LocatorPic','@s8')
  Of upper('joe:JobSkillLevel')
    loc:SortHeader = p_web.Translate('Job Skill Level')
    p_web.SetSessionValue('BrowseEngineerHistory_LocatorPic','@s30')
  Of upper('joe:Status')
    loc:SortHeader = p_web.Translate('Status')
    p_web.SetSessionValue('BrowseEngineerHistory_LocatorPic','@s30')
  Of upper('joe:StatusDate')
    loc:SortHeader = p_web.Translate('Status Date')
    p_web.SetSessionValue('BrowseEngineerHistory_LocatorPic','@d6')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
    loc:LocatorType = Net:Date
  Of upper('joe:StatusTime')
    loc:SortHeader = p_web.Translate('Status Time')
    p_web.SetSessionValue('BrowseEngineerHistory_LocatorPic','@t1b')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
  End
  If loc:selecting = 1
    p_web.GetSettings(p_web.GetSessionValue('Push1'))
    loc:selectaction = p_web.FormSettings.ParentPage !p_web.GetSessionValue('BrowseEngineerHistory:LookupFrom')
  End!Else
  do SendPacket
  do SetFormAction
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseEngineerHistory:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseEngineerHistory:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseEngineerHistory:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="JOBSENG"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="joe:RecordNumberKey"></input><13,10>'
  end
  case p_web.GetValue('refresh') ; of 'next' orof 'previous' orof 'first' orof 'last'
    p_web.Script('try{{BrowseEngineerHistory.restoreFocus();} catch(err){{};')
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseEngineerHistory',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      case loc:locatortype
      of Net:Position
      orof Net:Date
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
      of Net:Contains
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
      of Net:Begins
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
      End
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
      Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
      Case Loc:LocatorType
      of Net:Date
        packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator2BrowseEngineerHistory','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('BrowseEngineerHistory_LocatorPic'),,,'onchange="BrowseEngineerHistory.locate(''Locator2BrowseEngineerHistory'',this.value);"',,0,,,) & '</div></td>'
      Else
        packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator2BrowseEngineerHistory',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="BrowseEngineerHistory.locate(''Locator2BrowseEngineerHistory'',this.value);" ',,,,,,) & '</td>'
      End
      If loc:LocatorSearchButton or loc:LocatorClearButton
        packet = clip(packet) & '<td><div id="BrowseEngineerHistory_locate_a" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
        If loc:LocatorSearchButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
        End
        If loc:LocatorClearButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseEngineerHistory.cl(''BrowseEngineerHistory'');')
        End
        packet = clip(packet) & '</div><13,10></td><13,10>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
      If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'BrowseEngineerHistory_locate_a','buttonset',loc:options)
      End
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & p_web.DivHeader('BrowseEngineerHistory_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'','sortable')&'" id="BrowseEngineerHistory_tbl">'
  Else
    packet = clip(packet) & p_web.DivHeader('BrowseEngineerHistory_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'')&'" id="BrowseEngineerHistory_tbl">'
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'8','BrowseEngineerHistory',p_web.Translate('Engineer Name'),,,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'1','BrowseEngineerHistory',p_web.Translate('Allocated'),'Click here to sort by Date Allocated',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'2','BrowseEngineerHistory',p_web.Translate('Allocated By'),'Click here to sort by Allocated By',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'3','BrowseEngineerHistory',p_web.Translate('Eng Skill Level'),'Click here to sort by Engineer Skill Level',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'4','BrowseEngineerHistory',p_web.Translate('Job Skill Level'),'Click here to sort by Job Skill Level',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'5','BrowseEngineerHistory',p_web.Translate('Status'),'Click here to sort by Status',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'6','BrowseEngineerHistory',p_web.Translate('Status Date'),'Click here to sort by Status Date',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'7','BrowseEngineerHistory',p_web.Translate('Status Time'),'Click here to sort by Status Time',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:LocateField = 'joe:DateAllocated' then Loc:NoBuffer = 1.
  If Loc:LocateField = 'joe:StatusDate' then Loc:NoBuffer = 1.
  If Loc:NoBuffer = 0
    Buffer(ThisView,10,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  If Instring('joe:recordnumber',lower(loc:vorder),1,1) = 0 !and JOBSENG{prop:SQLDriver} = 1
    loc:vorder = Choose(loc:vorder='','joe:RecordNumber',clip(loc:vorder) & ',' & 'joe:RecordNumber')
  End
  Loc:Selected = Choose(p_web.IfExistsValue('joe:RecordNumber'),p_web.GetValue('joe:RecordNumber'),p_web.GetSessionValue('joe:RecordNumber'))
  ThisView{prop:order} = p_web.CleanFilter(ThisView,clip(loc:vorder))
    job:Ref_Number = p_web.RestoreValue('job:Ref_Number')
    loc:FilterWas = 'joe:JobNumber = ' & job:Ref_Number
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseEngineerHistory',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseEngineerHistory_Filter')
    p_web.SetSessionValue('BrowseEngineerHistory_FirstValue','')
    p_web.SetSessionValue('BrowseEngineerHistory_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,JOBSENG,joe:RecordNumberKey,loc:PageRows,'BrowseEngineerHistory',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,loc:ViewOptions,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled,Loc:LocatorCase)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or Loc:LocatorType = Net:Date or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position or loc:LocatorType = Net:Date
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If JOBSENG{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(JOBSENG,loc:firstvalue)
              Reset(ThisView,JOBSENG)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If JOBSENG{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(JOBSENG,loc:lastvalue)
            Reset(ThisView,JOBSENG)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(joe:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td class="'&p_web.combine(p_web.site.style.BrowseEmpty,)&'">'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:NavButtonPosition=Net:Above or (loc:NavButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseEngineerHistory_nav_a" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseEngineerHistory.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseEngineerHistory.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseEngineerHistory.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseEngineerHistory.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseEngineerHistory_nav_a','buttonset',loc:options)
          End
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:UpdateButtonPosition=Net:Above or (loc:UpdateButtonPosition=Net:Both and loc:found > 0))
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseEngineerHistory',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseEngineerHistory_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseEngineerHistory_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          case loc:locatortype
          of Net:Position
          orof Net:Date
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
          of Net:Contains
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
          of Net:Begins
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
          End
          packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
          Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
          Case Loc:LocatorType
          of Net:Date
            packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator1BrowseEngineerHistory','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('BrowseEngineerHistory_LocatorPic'),,,'onchange="BrowseEngineerHistory.locate(''Locator1BrowseEngineerHistory'',this.value);"',,0,,,) & '</div></td>'
          Else
            packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator1BrowseEngineerHistory',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="BrowseEngineerHistory.locate(''Locator1BrowseEngineerHistory'',this.value);" ',,,,,,) & '</td>'
          End
          If loc:LocatorSearchButton or loc:LocatorClearButton
            packet = clip(packet) & '<td><div id="BrowseEngineerHistory_locate_b" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
            If loc:LocatorSearchButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
            End
            If loc:LocatorClearButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseEngineerHistory.cl(''BrowseEngineerHistory'');')
            End
            packet = clip(packet) & '</div><13,10></td><13,10>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
          If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseEngineerHistory_locate_b','buttonset',loc:options)
          End
      End
    End
  End
  p_web.SetSessionValue('BrowseEngineerHistory_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseEngineerHistory_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:NavButtonPosition=Net:Below or loc:NavButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseEngineerHistory_nav_b" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseEngineerHistory.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseEngineerHistory.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseEngineerHistory.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseEngineerHistory.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseEngineerHistory_nav_b','buttonset',loc:options)
          End
        end
        do SendPacket
  End
  If loc:UpdateButtonPosition=Net:Below or loc:UpdateButtonPosition=Net:Both
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
  data
loc:rowclick  string(1024)
  code
    Access:USERS.Clearkey(use:User_Code_Key)
    use:User_Code    = joe:UserCode
    if (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
        ! Found
        joeEngineersName = clip(use:Forename) & ' ' & clip(use:Surname)
    else ! if (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
        ! Error
        joeEngineersName = ''
    end ! if (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
    loc:field = p_web.AddBrowseValue('BrowseEngineerHistory','JOBSENG',joe:RecordNumberKey) !joe:RecordNumber
    p_web._thisrow = p_web._nocolon('joe:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if Loc:LocatorValue <> '' and loc:ActualSelection = ''
        loc:checked = 'checked'
        do SetSelection
      elsif loc:ActualSelection = '' and joe:RecordNumber = p_web.GetValue('joe:RecordNumber')
         loc:checked = 'checked'
         do SetSelection
      elsif loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseEngineerHistory:LookupField')) = joe:RecordNumber and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      else
        loc:checked = Choose((joe:RecordNumber = loc:selected) and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      end
      If(loc:SelectionMethod  = Net:Radio)
        loc:RowStyle = 'class="' & p_web.combine(p_web.site.style.browserow,) &'"'
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'data-nt-id="'& p_web.AddBrowseValue('BrowseEngineerHistory','JOBSENG',joe:RecordNumberKey) &'" onclick="'
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If JOBSENG{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(JOBSENG)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            loc:DefaultSelection = loc:field
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If JOBSENG{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(JOBSENG)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            loc:DefaultSelection = loc:field
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'>'&p_web.CreateInput('radio','_bidv_',clip(loc:field),,loc:checked,'')&'</td>'&CRLF
          If loc:DefaultSelection = ''
            loc:DefaultSelection = loc:field
          End
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          If loc:DefaultSelection = '' or loc:direction < 0
            loc:DefaultSelection = loc:field
          End
        End
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::joeEngineersName
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::joe:DateAllocated
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::joe:AllocatedBy
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('CenterJustify')&'"><13,10>'
          end ! loc:eip = 0
          do value::joe:EngSkillLevel
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('CenterJustify')&'"><13,10>'
          end ! loc:eip = 0
          do value::joe:JobSkillLevel
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::joe:Status
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::joe:StatusDate
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::joe:StatusTime
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr ' & clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Idx = p_web.AddBrowseValue('BrowseEngineerHistory','JOBSENG',joe:RecordNumberKey)
  TableQueue.Id[1] = joe:RecordNumber

ClosingScripts  Routine
  data
Rtn_SecwinProcedureName         string(252)
  code
  If p_web.RequestAjax = 0
    do SetFormAction
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var btiBrowseEngineerHistory;if (btiBrowseEngineerHistory != 1){{var BrowseEngineerHistory=new browseTable(''BrowseEngineerHistory'','''&clip(loc:formname)&|
      ''','''&p_web._jsok('joe:RecordNumber',Net:Parameter)&''',2,'''&clip(loc:divname)&''',1,1,1,'''&|
      clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',0,'''&|
      p_web.Translate('Are you sure you want to delete this record?')&''','''&clip(loc:ActualSelection)&''','''&clip(loc:selectaction)&''','''&|
      clip(loc:formactiontarget)&''','''','&loc:popup&',0,'''','''',"' & clip(Rtn_SecwinProcedureName) & '");<13,10>'&|
      'BrowseEngineerHistory.setGreenBar('''&p_web.ColorWeb(p_web.Site.Style.BrowseHighlightColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOneColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseTwoColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOverColor)&''');<13,10>' &|
      'BrowseEngineerHistory.applyGreenBar();btiBrowseEngineerHistory=1};<13,10>' &|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseEngineerHistory')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseEngineerHistory')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseEngineerHistory')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseEngineerHistory')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(JOBSENG)
  p_web._CloseFile(JOBS)
  p_web._CloseFile(USERS)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(JOBSENG)
  Bind(joe:Record)
  Clear(joe:Record)
  NetWebSetSessionPics(p_web,JOBSENG)
  p_web._OpenFile(JOBS)
  Bind(job:Record)
  NetWebSetSessionPics(p_web,JOBS)
  p_web._OpenFile(USERS)
  Bind(use:Record)
  NetWebSetSessionPics(p_web,USERS)

Children Routine
  if loc:selecting = 0
    If p_web.RequestAjax = 0
      do StartChildren
    Else
      do AjaxChildren
    End
  end

AjaxChildren  Routine
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue('joe:RecordNumber',p_web.GetValue('joe:RecordNumber'))
   p_web.DeleteValue('_Clicked')
   loc:found = 1
   do Children
! ----------------------------------------------------------------------------------------
CallRow  Routine
  data
loc:result  long
  code
  do OpenFilesB
  joe:RecordNumber = p_web.GSV('joe:RecordNumber')
  loc:result = p_web._GetFile(JOBSENG,joe:RecordNumberKey)
  loc:eip = 1
  loc:viewstate = p_web.escape(p_web.Base64Encode(clip(joe:RecordNumber)))
  do BrowseRow
  do SendPacket
  do ClosefilesB

! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(JOBSENG)
  Case upper(p_web.GetValue('_EIPClm'))
  Else
    case p_web.GetValue('_action_')
    of Net:InsertRecord
    orof Net:ChangeRecord
    orof Net:DeleteRecord
    orof Net:CopyRecord
      loc:eip = 0
      do CallBrowse
    end
  End
  do GotFocusBack
  p_web._CloseFile(JOBSENG)
! ----------------------------------------------------------------------------------------
value::joeEngineersName   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseEngineerHistory_joeEngineersName_'&joe:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(joeEngineersName,'@s60')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::joe:DateAllocated   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseEngineerHistory_joe:DateAllocated_'&joe:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(joe:DateAllocated,'@d6')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::joe:AllocatedBy   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseEngineerHistory_joe:AllocatedBy_'&joe:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(joe:AllocatedBy,'@s3')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::joe:EngSkillLevel   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseEngineerHistory_joe:EngSkillLevel_'&joe:RecordNumber,'CenterJustify',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(joe:EngSkillLevel,'@s8')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::joe:JobSkillLevel   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseEngineerHistory_joe:JobSkillLevel_'&joe:RecordNumber,'CenterJustify',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(joe:JobSkillLevel,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::joe:Status   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseEngineerHistory_joe:Status_'&joe:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(joe:Status,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::joe:StatusDate   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseEngineerHistory_joe:StatusDate_'&joe:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(joe:StatusDate,'@d6')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::joe:StatusTime   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseEngineerHistory_joe:StatusTime_'&joe:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(joe:StatusTime,'@t1b')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  p_web._OpenFile(JOBS)
  p_web._OpenFile(USERS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(USERS)
     FilesOpened = False
  END
  return
!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
CheckForDuplicate  Routine
SetSelection  Routine
  loc:ActualSelection = loc:field
  p_web.SetSessionValue('joe:RecordNumber',joe:RecordNumber)

MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:ActualSelection = ''
    loc:ActualSelection = loc:DefaultSelection
    p_web.GetBrowseValue(loc:ActualSelection,Net:Web:SessionQueue) ! so children are primed with correct sessionValue
  End

  If loc:ActualSelection <> ''
    TableQueue.Kind = Net:RowTable
    get(TableQueue,TableQueue.Kind)
    if Errorcode() = 0
      TableQueue.Row = sub(TableQueue.Row,1,len(clip(TableQueue.Row))-1) &  ' data-nt-rows="'&loc:RowsHigh&'" data-nt-value="'&clip(loc:ActualSelection)&'"' & '>'
      Put(TableQueue)
    End
  End

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead class="'&p_web.combine(p_web.site.style.BrowseHeader,'')&'"><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot class="'&p_web.combine(p_web.site.style.BrowseFooter,)&'"><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody class="'&p_web.combine(p_web.site.style.BrowseBody,)&'"><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table><13,10>' & p_web.DivFooter(Net:NoSend) ! Table Div
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
