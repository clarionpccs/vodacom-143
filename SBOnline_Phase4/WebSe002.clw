

   MEMBER('WebServer_Phase4.clw')                          ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPRPDF.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE
   INCLUDE('ABRPPSEL.INC'),ONCE

                     MAP
                       INCLUDE('WEBSE002.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSE003.INC'),ONCE        !Req'd for module callout resolution
                     END


TotalPrice           PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
paid_chargeable_temp REAL                                  !
paid_warranty_temp   REAL                                  !
labour_rate_temp     REAL                                  !
parts_rate_temp      REAL                                  !
FilesOpened     BYTE(0)
  CODE
!How much has been paid?
! Parameters
! TotalPrice:VAT
! TotalPrice:Total
! TotalPrice:Balance
! TotalPrice:Type

    do openFiles
    p_web.SSV('TotalPrice:VAT',0)
    p_web.SSV('TotalPrice:Total',0)
    p_web.SSV('TotalPrice:Balance',0)


    Paid_Warranty_Temp = 0
    Paid_Chargeable_Temp = 0
    Access:JOBPAYMT_ALIAS.Clearkey(jpt_ali:All_Date_Key)
    jpt_ali:Ref_Number = p_web.GSV('job:Ref_Number')
    Set(jpt_ali:All_Date_Key,jpt_ali:All_Date_Key)
    Loop ! Begin Loop
        If Access:JOBPAYMT_ALIAS.Next()
            Break
        End ! If Access:JOBPAYMT_ALIAS.Next()
        If jpt_ali:Ref_Number <> p_web.GSV('job:Ref_Number')
            Break
        End ! If jpt_ali:Ref_Number <> p_web.GSV('job:Ref_Number

        Paid_Chargeable_Temp += jpt_ali:Amount
    End ! Loop

    !Look up VAT Rates
    Labour_Rate_Temp = 0
    Parts_Rate_Temp = 0

    Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
    sub:Account_Number = p_web.GSV('job:Account_Number')
    If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
        !Found
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = sub:Main_Account_Number
        If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
            !Found
            If tra:Invoice_Sub_Accounts = 'YES'
                Access:VATCODE.ClearKey(vat:Vat_Code_Key)
                vat:VAT_Code = sub:Labour_Vat_Code
                If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Found
                    Labour_Rate_Temp = vat:VAT_Rate
                Else ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Error
                End ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                Access:VATCODE.ClearKey(vat:Vat_Code_Key)
                vat:VAT_Code = sub:Parts_Vat_Code
                If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Found
                    Parts_Rate_Temp = vat:VAT_Rate
                Else ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Error
                End ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
            Else ! If tra:Invoice_Sub_Accounts = 'YES'
                Access:VATCODE.ClearKey(vat:Vat_Code_Key)
                vat:VAT_Code = tra:Labour_Vat_Code
                If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Found
                    Labour_Rate_Temp = vat:VAT_Rate
                Else ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Error
                End ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                Access:VATCODE.ClearKey(vat:Vat_Code_Key)
                vat:VAT_Code = tra:Parts_Vat_Code
                If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Found
                    Parts_Rate_Temp = vat:VAT_Rate
                Else ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Error
                End ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
            End ! If tra:Invoice_Sub_Accounts = 'YES'
        Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
            !Error
        End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
    Else ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
        !Error
    End ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign

    Case p_web.GSV('TotalPrice:Type')
    Of 'C' !Chargeable
        If p_web.GSV('BookingSite') = 'RRC'
            p_web.SSV('TotalPrice:VAT',p_web.GSV('jobe:RRCCLabourCost') * (labour_rate_temp/100) + |
                        p_web.GSV('jobe:RRCCPartsCost') * (parts_rate_temp/100) + |
                        p_web.GSV('job:courier_cost') * (labour_rate_temp/100))

            p_web.SSV('TotalPrice:Total',p_web.GSV('jobe:RRCCLabourCost') + |
                        p_web.GSV('jobe:RRCCPartsCost') + p_web.GSV('job:courier_cost') + p_web.GSV('TotalPrice:VAT'))

            p_web.SSV('TotalPrice:Balance',p_web.GSV('TotalPrice:Total') - paid_chargeable_temp)

        Else !If glo:WebJob
            p_web.SSV('TotalPrice:VAT',p_web.GSV('job:labour_cost') * (labour_rate_temp/100) + |
                        p_web.GSV('job:parts_cost') * (parts_rate_temp/100) + |
                        p_web.GSV('job:courier_cost') * (labour_rate_temp/100))

            p_web.SSV('TotalPrice:Total',p_web.GSV('job:labour_cost') + |
                    p_web.GSV('job:parts_cost') + p_web.GSV('job:courier_cost') + p_web.GSV('TotalPrice:VAT'))

            p_web.SSV('TotalPrice:Balance',p_web.GSV('TotalPrice:Total') - paid_chargeable_temp)
        End !If glo:WebJob
    Of 'W' ! Warranty
        If p_web.GSV('BookingSite') = 'RRC'
            p_web.SSV('TotalPrice:VAT',p_web.GSV('jobe:RRCWLabourCost') * (labour_rate_temp/100) + |
                        p_web.GSV('jobe:RRCWPartsCost') * (parts_rate_temp/100) + |
                        p_web.GSV('job:courier_cost_warranty') * (labour_rate_temp/100))

            p_web.SSV('TotalPrice:Total',p_web.GSV('jobe:RRCWLabourCost') + |
                        p_web.GSV('jobe:RRCWPartsCost') + p_web.GSV('job:courier_cost_warranty') + p_web.GSV('TotalPrice:VAT'))

        Else !If glo:WebJob
            p_web.SSV('TotalPrice:VAT',p_web.GSV('job:labour_cost_warranty') * (labour_rate_temp/100) + |
                        p_web.GSV('job:parts_cost_warranty') * (parts_rate_temp/100) + |
                        p_web.GSV('job:courier_cost_warranty') * (labour_rate_temp/100))

            p_web.SSV('TotalPrice:Total',p_web.GSV('job:labour_cost_warranty') + |
                        p_web.GSV('job:parts_cost_warranty') + p_web.GSV('job:courier_cost_warranty') + p_web.GSV('TotalPrice:VAT'))

        End !If glo:WebJob
    Of 'E' !Estimate
        If p_web.GSV('jobe:WebJob') = 1
            p_web.SSV('TotalPrice:VAT',p_web.GSV('jobe:RRCELabourCost') * (labour_rate_temp/100) + |
                        p_web.GSV('jobe:RRCEPartsCost') * (parts_rate_temp/100) + |
                        p_web.GSV('job:courier_cost_estimate') * (labour_rate_temp/100))

            p_web.SSV('TotalPrice:Total',p_web.GSV('jobe:RRCELabourCost') + |
                        p_web.GSV('jobe:RRCEPartsCost') + p_web.GSV('job:courier_cost_estimate') + p_web.GSV('TotalPrice:VAT'))

        Else !If p_web.GSV('jobe:WebJob
            p_web.SSV('TotalPrice:VAT',p_web.GSV('job:labour_cost_estimate') * (labour_rate_temp/100) + |
                        p_web.GSV('job:parts_cost_estimate') * (parts_rate_temp/100) + |
                        p_web.GSV('job:courier_cost_estimate') * (labour_rate_temp/100))

            p_web.SSV('TotalPrice:Total',p_web.GSV('job:labour_cost_estimate') + |
                        p_web.GSV('job:parts_cost_estimate') + p_web.GSV('job:courier_cost_estimate') + p_web.GSV('TotalPrice:VAT'))

        End !If p_web.GSV('jobe:WebJob
    Of 'I' ! Chargealbe Invoice
        Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
        inv:Invoice_Number = p_web.GSV('job:Invoice_Number')
        If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
            !Found
            If p_web.GSV('BookingSite') = 'RRC'
                p_web.SSV('TotalPrice:VAT',p_web.GSV('jobe:InvRRCCLabourCost') * (inv:vat_rate_labour/100) + |
                            p_web.GSV('jobe:InvRRCCPartsCost') * (inv:vat_rate_parts/100) + |
                            p_web.GSV('job:invoice_courier_cost') * (inv:vat_rate_labour/100))

                p_web.SSV('TotalPrice:Total',p_web.GSV('jobe:InvRRCCLabourCost') + |
                        p_web.GSV('jobe:InvRRCCPartsCost') + p_web.GSV('job:invoice_courier_cost') + p_web.GSV('TotalPrice:VAT'))

                p_web.SSV('TotalPrice:Balance',p_web.GSV('TotalPrice:Total') - paid_chargeable_temp)
            Else !If glo:webJOb
                p_web.SSV('TotalPrice:VAT',p_web.GSV('job:invoice_labour_cost') * (inv:vat_rate_labour/100) + |
                            p_web.GSV('job:invoice_parts_cost') * (inv:vat_rate_parts/100) + |
                            p_web.GSV('job:invoice_courier_cost') * (inv:vat_rate_labour/100))

                p_web.SSV('TotalPrice:Total',p_web.GSV('job:invoice_labour_cost') + |
                            p_web.GSV('job:invoice_parts_cost') + p_web.GSV('job:invoice_courier_cost') + p_web.GSV('TotalPrice:VAT'))

                p_web.SSV('TotalPrice:Balance',p_web.GSV('TotalPrice:Total') - paid_chargeable_temp)
            End !If glo:webJOb

        Else ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
            !Error
        End ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
    Of 'V' ! Warranty Invoice
        Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
        inv:Invoice_Number = p_web.GSV('job:Invoice_Number_Warranty')
        If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
            !Found
            If p_web.GSV('BookingSite') = 'RRC'
                p_web.SSV('TotalPrice:VAT',p_web.GSV('jobe:InvRRCWLabourCost') * (inv:vat_rate_labour/100) + |
                            p_web.GSV('jobe:InvRRCWPartsCost') * (inv:vat_rate_parts/100) + |
                            p_web.GSV('job:Winvoice_courier_cost') * (inv:vat_rate_labour/100))
                p_web.SSV('TotalPrice:Total',p_web.GSV('jobe:InvRRCWLabourCost') + p_web.GSV('jobe:InvRRCWPartsCost') + |
                            p_web.GSV('job:Winvoice_courier_cost') + p_web.GSV('TotalPrice:VAT'))

                p_web.SSV('TotalPrice:Balance',p_web.GSV('TotalPrice:Total') - paid_chargeable_temp)
            Else !If glo:WebJob
                p_web.SSV('TotalPrice:VAT',p_web.GSV('job:Winvoice_labour_cost') * (inv:vat_rate_labour/100) + |
                            p_web.GSV('job:Winvoice_parts_cost') * (inv:vat_rate_parts/100) + |
                            p_web.GSV('job:Winvoice_courier_cost') * (inv:vat_rate_labour/100))
                p_web.SSV('TotalPrice:Total',p_web.GSV('job:Winvoice_labour_cost') + p_web.GSV('job:Winvoice_parts_cost') + |
                            p_web.GSV('job:Winvoice_courier_cost') + p_web.GSV('TotalPrice:VAT'))

                p_web.SSV('TotalPrice:Balance',p_web.GSV('TotalPrice:Total') - paid_chargeable_temp)
            End !If glo:WebJob
        Else ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
            !Error
        End ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
    End ! Case f:Type

    p_web.SSV('TotalPrice:VAT',Round(p_web.GSV('TotalPrice:VAT'),.01))
    p_web.SSV('TotalPrice:Total',Round(p_web.GSV('TotalPrice:Total'),.01))
    p_web.SSV('TotalPrice:Balance',Round(p_web.GSV('TotalPrice:Balance'),.01))
    p_web.SSV('TotalPrice:Type','')

    do closeFiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:SUBTRACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBTRACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:VATCODE.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:VATCODE.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBPAYMT_ALIAS.Open                               ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBPAYMT_ALIAS.UseFile                            ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:SUBTRACC.Close
     Access:TRADEACC.Close
     Access:VATCODE.Close
     Access:JOBPAYMT_ALIAS.Close
     FilesOpened = False
  END
Estimate PROCEDURE (<NetWebServerWorker p_web>)            ! Generated from procedure template - Report

  ! The NetTalk Extension to report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
Loc:Html      String(1024)
Progress:Thermometer BYTE                                  !
RejectRecord         LONG,AUTO                             !
tmp:Ref_Number       STRING(20)                            !
tmp:PrintedBy        STRING(255)                           !
save_epr_id          USHORT,AUTO                           !
save_joo_id          USHORT,AUTO                           !
LocalRequest         LONG,AUTO                             !
LocalResponse        LONG,AUTO                             !
FilesOpened          LONG                                  !
WindowOpened         LONG                                  !
RecordsToProcess     LONG,AUTO                             !
RecordsProcessed     LONG,AUTO                             !
RecordsPerCycle      LONG,AUTO                             !
RecordsThisCycle     LONG,AUTO                             !
PercentProgress      BYTE                                  !
RecordStatus         BYTE,AUTO                             !
EndOfReport          BYTE,AUTO                             !
ReportRunDate        LONG,AUTO                             !
ReportRunTime        LONG,AUTO                             !
ReportPageNo         SHORT,AUTO                            !
FileOpensReached     BYTE                                  !
PartialPreviewReq    BYTE                                  !
DisplayProgress      BYTE                                  !
InitialPath          CSTRING(128)                          !
IniFileToUse         STRING(64)                            !
code_temp            BYTE                                  !
option_temp          BYTE                                  !
Bar_code_string_temp CSTRING(21)                           !
Bar_Code_Temp        CSTRING(21)                           !
Bar_Code2_Temp       CSTRING(21)                           !
Address_Line1_Temp   STRING(30)                            !
Address_Line2_Temp   STRING(30)                            !
Address_Line3_Temp   STRING(30)                            !
Address_Line4_Temp   STRING(30)                            !
Invoice_Name_Temp    STRING(30)                            !
Delivery_Company_Name_Temp STRING(30)                      !Delivery Company Name
Delivery_Address1_Temp STRING(30)                          !
Delivery_address2_temp STRING(30)                          !
Delivery_address3_temp STRING(30)                          !
Delivery_address4_temp STRING(30)                          !
Delivery_Telephone_Number_Temp STRING(30)                  !Delivery Telephone Number
Invoice_Company_Temp STRING(30)                            !
Invoice_address1_temp STRING(30)                           !
invoice_address2_temp STRING(30)                           !
invoice_address3_temp STRING(30)                           !
invoice_address4_temp STRING(30)                           !
accessories_temp     STRING(30),DIM(6)                     !
estimate_value_temp  STRING(40)                            !
despatched_user_temp STRING(40)                            !
vat_temp             REAL                                  !
total_temp           REAL                                  !
part_number_temp     STRING(30)                            !
line_cost_temp       REAL                                  !
job_number_temp      STRING(20)                            !
esn_temp             STRING(24)                            !
charge_type_temp     STRING(22)                            !
repair_type_temp     STRING(22)                            !
labour_temp          REAL                                  !
parts_temp           REAL                                  !
courier_cost_temp    REAL                                  !
Quantity_temp        REAL                                  !
Description_temp     STRING(30)                            !
Cost_Temp            REAL                                  !
customer_name_temp   STRING(60)                            !
sub_total_temp       REAL                                  !
invoice_company_name_temp STRING(30)                       !
invoice_telephone_number_temp STRING(15)                   !
invoice_fax_number_temp STRING(15)                         !
tmp:DefaultTelephone STRING(20)                            !
tmp:DefaultFax       STRING(20)                            !
tmp:InvoiceText      STRING(255)                           !Invoice Text
DefaultAddress       GROUP,PRE(address)                    !
Location             STRING(40)                            !
SiteName             STRING(40)                            !
Name2                STRING(40)                            !
RegistrationNo       STRING(40)                            !
VATNumber            STRING(30)                            !
Name                 STRING(40)                            !Name
AddressLine1         STRING(40)                            !Address Line 1
AddressLine2         STRING(40)                            !Address Line 2
AddressLine3         STRING(40)                            !Address Line 3
AddressLine4         STRING(40)                            !Postcode
Telephone            STRING(30)                            !Telephone
Fax                  STRING(30)                            !Fax
EmailAddress         STRING(255)                           !Email Address
                     END                                   !
locRefNumber         LONG                                  !
locAccountNumber     STRING(30)                            !
locOrderNumber       STRING(30)                            !
locAuthorityNumber   STRING(20)                            !
locChargeType        STRING(20)                            !
locRepairType        STRING(20)                            !
locModelNumber       STRING(30)                            !
locManufacturer      STRING(30)                            !
locUnitType          STRING(30)                            !
locESN               STRING(20)                            !
locMSN               STRING(20)                            !
Process:View         VIEW(JOBS)
                       PROJECT(job:Ref_Number)
                     END
ProgressWindow       WINDOW('Report JOBS'),AT(,,142,59),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY,DOUBLE
                       PROGRESS,USE(Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),FLAT,LEFT,MSG('Cancel Report'),TIP('Cancel Report'),ICON('WACANCEL.ICO')
                     END

Report               REPORT,AT(396,6917,7521,1927),PAPER(PAPER:A4),PRE(RPT),FONT('Tahoma',10,,FONT:regular),THOUS
                       HEADER,AT(396,635,7521,6313),USE(?unnamed)
                         STRING('Job No:'),AT(4896,365),USE(?String25),TRN,FONT(,12,,)
                         STRING(@s16),AT(5729,365),USE(tmp:Ref_Number),TRN,LEFT,FONT(,12,,FONT:bold)
                         STRING('Estimate Date:'),AT(4896,573),USE(?String58),TRN,FONT(,8,,)
                         STRING('<<-- Date Stamp -->'),AT(5729,573),USE(?ReportDateStamp),TRN,FONT(,8,,FONT:bold)
                         STRING(@s60),AT(156,1563),USE(customer_name_temp),TRN,FONT(,8,,)
                         STRING(@s60),AT(4063,1563),USE(customer_name_temp,,?customer_name_temp:2),TRN,FONT(,8,,)
                         STRING(@s20),AT(1604,3240),USE(locOrderNumber),TRN,LEFT,FONT(,8,,)
                         STRING(@s20),AT(4531,3240),USE(locChargeType),TRN,LEFT,FONT(,8,,)
                         STRING(@s20),AT(3156,3240),USE(locAuthorityNumber),TRN,FONT(,8,,)
                         STRING(@s20),AT(6042,3229),USE(locRepairType),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,3917,1000,156),USE(locModelNumber),TRN,LEFT,FONT(,8,,)
                         STRING(@s30),AT(1604,3917,1323,156),USE(locManufacturer),TRN,LEFT,FONT(,8,,)
                         STRING(@s30),AT(3156,3917,1396,156),USE(locUnitType),TRN,LEFT,FONT(,8,,)
                         STRING(@s20),AT(4604,3917,1396,156),USE(locESN),TRN,LEFT,FONT(,8,,)
                         STRING(@s20),AT(6083,3917),USE(locMSN),TRN,FONT(,8,,)
                         STRING('Reported Fault: '),AT(156,4323),USE(?String64),TRN,FONT(,8,,FONT:bold)
                         TEXT,AT(1615,4323,5625,417),USE(jbn:Fault_Description),TRN,FONT(,8,,,CHARSET:ANSI)
                         TEXT,AT(1615,5052,5625,885),USE(tmp:InvoiceText),TRN,FONT(,8,,,CHARSET:ANSI)
                         STRING('Engineers Report:'),AT(156,5000),USE(?String88),TRN,FONT(,8,,FONT:bold)
                         GROUP,AT(104,5938,7500,208),USE(?PartsHeadingGroup),FONT('Tahoma',,,)
                           STRING('PARTS REQUIRED'),AT(156,6042),USE(?String79),TRN,FONT(,9,,FONT:bold)
                           STRING('Qty'),AT(1510,6042),USE(?String80),TRN,FONT(,8,,FONT:bold)
                           STRING('Part Number'),AT(1917,6042),USE(?String81),TRN,FONT(,8,,FONT:bold)
                           STRING('Description'),AT(3677,6042),USE(?String82),TRN,FONT(,8,,FONT:bold)
                           STRING('Unit Cost'),AT(5719,6042),USE(?UnitCost),TRN,FONT(,8,,FONT:bold)
                           STRING('Line Cost'),AT(6677,6042),USE(?LineCost),TRN,FONT(,8,,FONT:bold)
                           LINE,AT(1406,6197,6042,0),USE(?Line1),COLOR(COLOR:Black)
                         END
                         STRING(@s30),AT(156,2344),USE(invoice_address4_temp),TRN,FONT(,8,,)
                         STRING('Tel: '),AT(4063,2500),USE(?String32:2),TRN,FONT(,8,,)
                         STRING(@s30),AT(4323,2500),USE(Delivery_Telephone_Number_Temp),FONT(,8,,)
                         STRING('Tel:'),AT(156,2500),USE(?String34:2),TRN,FONT(,8,,)
                         STRING(@s15),AT(417,2500),USE(invoice_telephone_number_temp),TRN,LEFT,FONT(,8,,)
                         STRING('Fax: '),AT(1927,2500),USE(?String35:2),TRN,FONT(,8,,)
                         STRING(@s15),AT(2240,2500),USE(invoice_fax_number_temp),TRN,LEFT,FONT(,8,,)
                         STRING(@s20),AT(156,3240),USE(locAccountNumber),TRN,FONT(,8,,)
                         STRING(@s30),AT(4063,1719),USE(Delivery_Company_Name_Temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(4063,1875,1917,156),USE(Delivery_Address1_Temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(4063,2031),USE(Delivery_address2_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(4063,2188),USE(Delivery_address3_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(4063,2344),USE(Delivery_address4_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,1719),USE(invoice_company_name_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,1875),USE(Invoice_address1_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,2188),USE(invoice_address3_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,2031),USE(invoice_address2_temp),TRN,FONT(,8,,)
                       END
DETAIL                 DETAIL,AT(,,,198),USE(?DetailBand)
                         STRING(@n8b),AT(1156,0),USE(Quantity_temp),TRN,RIGHT,FONT(,8,,)
                         STRING(@s25),AT(1917,0),USE(part_number_temp),TRN,FONT(,8,,)
                         STRING(@s25),AT(3677,0),USE(Description_temp),TRN,FONT(,8,,)
                         STRING(@n14.2b),AT(5208,0),USE(Cost_Temp),TRN,RIGHT,FONT(,8,,)
                         STRING(@n14.2b),AT(6198,0),USE(line_cost_temp),TRN,RIGHT,FONT(,8,,)
                       END
                       FOOTER,AT(385,9177,7521,2146),USE(?unnamed:4)
                         STRING('Labour:'),AT(4844,52),USE(?labour_string),TRN,FONT(,8,,)
                         STRING(@n14.2b),AT(6250,52),USE(labour_temp),TRN,RIGHT,FONT(,8,,)
                         STRING(@s20),AT(2865,313,1771,156),USE(job_number_temp),TRN,CENTER,FONT(,8,,FONT:bold)
                         STRING(@s20),AT(2875,83,1760,198),USE(Bar_Code_Temp),CENTER,FONT('C128 High 12pt LJ3',12,,)
                         STRING('Carriage:'),AT(4844,365),USE(?carriage_string),TRN,FONT(,8,,)
                         STRING(@n14.2b),AT(6250,208),USE(parts_temp),TRN,RIGHT,FONT(,8,,)
                         STRING('V.A.T.'),AT(4844,677),USE(?vat_String),TRN,FONT(,8,,)
                         STRING(@n14.2b),AT(6250,365),USE(courier_cost_temp),TRN,RIGHT,FONT(,8,,)
                         STRING('Total:'),AT(4844,833),USE(?total_string),TRN,FONT(,8,,FONT:bold)
                         STRING('<128>'),AT(6146,833,156,208),USE(?Euro),TRN,HIDE,FONT('Tahoma',8,COLOR:Black,FONT:bold,CHARSET:ANSI)
                         LINE,AT(6354,833,1000,0),USE(?line),COLOR(COLOR:Black)
                         LINE,AT(6354,521,1000,0),USE(?line:2),COLOR(COLOR:Black)
                         STRING(@n14.2b),AT(6198,833),USE(total_temp),TRN,RIGHT,FONT(,8,,FONT:bold)
                         STRING('Sub Total:'),AT(4844,521),USE(?String92),TRN,FONT(,8,,)
                         STRING(@n14.2b),AT(6250,521),USE(sub_total_temp),TRN,RIGHT,FONT(,8,,)
                         STRING(@s20),AT(2865,521,1760,198),USE(Bar_Code2_Temp),CENTER,FONT('C128 High 12pt LJ3',12,,)
                         STRING(@n14.2b),AT(6250,677),USE(vat_temp),TRN,RIGHT,FONT(,8,,)
                         STRING(@s24),AT(2875,719,1760,240),USE(esn_temp),TRN,CENTER,FONT(,8,,FONT:bold)
                         STRING('Parts:'),AT(4844,208),USE(?parts_string),TRN,FONT(,8,,)
                         TEXT,AT(104,1094,7365,958),USE(stt:Text),FONT(,8,,)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE('RINVDET.GIF'),AT(0,0,7552,11198),USE(?Image1)
                         STRING('ESTIMATE'),AT(5521,0,1917,240),USE(?Chargeable_Repair_Type:2),TRN,RIGHT,FONT(,16,,FONT:bold)
                         STRING(@s40),AT(104,0,4167,260),USE(address:SiteName),LEFT,FONT(,14,,FONT:bold)
                         STRING(@s40),AT(104,313,3073,208),USE(address:Name2),TRN,FONT(,8,COLOR:Black,FONT:bold)
                         STRING(@s40),AT(104,208,3073,208),USE(address:Name),TRN,FONT(,8,COLOR:Black,FONT:bold)
                         STRING(@s40),AT(104,417,2760,208),USE(address:Location),TRN,FONT(,8,COLOR:Black,FONT:bold)
                         STRING('REG NO:'),AT(104,573),USE(?stringREGNO),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s40),AT(625,573,1667,208),USE(address:RegistrationNo),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING('VAT NO: '),AT(104,677),USE(?stringVATNO),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s20),AT(625,677,1771,156),USE(address:VATNumber),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s40),AT(104,833,2240,156),USE(address:AddressLine1),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s40),AT(104,938,2240,156),USE(address:AddressLine2),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s40),AT(104,1042,2240,156),USE(address:AddressLine3),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s40),AT(104,1146),USE(address:AddressLine4),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING('TEL:'),AT(104,1250),USE(?stringTEL),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s30),AT(469,1250,1458,208),USE(address:Telephone),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING('FAX:'),AT(2083,1250,313,208),USE(?stringFAX),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s30),AT(2396,1250,1510,188),USE(address:Fax),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING('EMAIL:'),AT(104,1354,417,208),USE(?stringEMAIL),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s255),AT(469,1354,3333,208),USE(address:EmailAddress),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING('JOB DETAILS'),AT(4844,198),USE(?String57),TRN,FONT(,9,,FONT:bold)
                         STRING('INVOICE ADDRESS'),AT(156,1500),USE(?String24),TRN,FONT(,9,,FONT:bold)
                         STRING('DELIVERY ADDRESS'),AT(4010,1510,1677,156),USE(?String28),TRN,FONT(,9,,FONT:bold)
                         STRING('GENERAL DETAILS'),AT(156,2958),USE(?String91),TRN,FONT(,9,,FONT:bold)
                         STRING('AUTHORISATION DETAILS'),AT(156,8479),USE(?String73),TRN,FONT(,9,,FONT:bold)
                         STRING('CHARGE DETAILS'),AT(4760,8479),USE(?String74),TRN,FONT(,9,,FONT:bold)
                         STRING('Estimate Accepted'),AT(208,8750),USE(?String94),TRN,FONT(,8,,FONT:bold)
                         BOX,AT(1406,8750,156,156),USE(?Box1),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING('Estimate Refused'),AT(208,8958),USE(?String95),TRN,FONT(,8,,FONT:bold)
                         BOX,AT(1406,8958,156,156),USE(?Box2),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING('Name (Capitals)'),AT(208,9219),USE(?String96),TRN,FONT(,8,,FONT:bold)
                         LINE,AT(1198,9323,1354,0),USE(?Line3),COLOR(COLOR:Black)
                         STRING('Signature'),AT(208,9479),USE(?String97),TRN,FONT(,8,,FONT:bold)
                         LINE,AT(833,9583,1719,0),USE(?Line4),COLOR(COLOR:Black)
                         STRING('Model'),AT(156,3844),USE(?String40),TRN,FONT(,8,,FONT:bold)
                         STRING('Account Number'),AT(156,3156),USE(?String40:2),TRN,FONT(,8,,FONT:bold)
                         STRING('Order Number'),AT(1604,3156),USE(?String40:3),TRN,FONT(,8,,FONT:bold)
                         STRING('Authority Number'),AT(3083,3156),USE(?String40:4),TRN,FONT(,8,,FONT:bold)
                         STRING('Chargeable Type'),AT(4531,3156),USE(?Chargeable_Type),TRN,FONT(,8,,FONT:bold)
                         STRING('Chargeable Repair Type'),AT(6000,3156),USE(?Repair_Type),TRN,FONT(,8,,FONT:bold)
                         STRING('Make'),AT(1635,3844),USE(?String41),TRN,FONT(,8,,FONT:bold)
                         STRING('Unit Type'),AT(3125,3844),USE(?String42),TRN,FONT(,8,,FONT:bold)
                         STRING('I.M.E.I. Number'),AT(4563,3844),USE(?String43),TRN,FONT(,8,,FONT:bold)
                         STRING('M.S.N.'),AT(6083,3844),USE(?String44),TRN,FONT(,8,,FONT:bold)
                         STRING('REPAIR DETAILS'),AT(156,3635),USE(?String50),TRN,FONT(,9,,FONT:bold)
                       END
                     END
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeNoRecords          PROCEDURE(),DERIVED                 ! Method added to host embed code
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

ProgressMgr          StepLongClass                         ! Progress Manager
Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED                 ! Method added to host embed code
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Estimate')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:AUDIT.SetOpenRelated()
  Relate:AUDIT.Open                                        ! File AUDIT used by this procedure, so make sure it's RelationManager is open
  Relate:DEFAULT2.Open                                     ! File DEFAULT2 used by this procedure, so make sure it's RelationManager is open
  Relate:DEFAULTS.Open                                     ! File DEFAULTS used by this procedure, so make sure it's RelationManager is open
  Relate:STANTEXT.Open                                     ! File STANTEXT used by this procedure, so make sure it's RelationManager is open
  Relate:VATCODE.Open                                      ! File VATCODE used by this procedure, so make sure it's RelationManager is open
  Relate:WEBJOB.Open                                       ! File WEBJOB used by this procedure, so make sure it's RelationManager is open
  Access:JOBNOTES.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBACC.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:USERS.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:PARTS.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:SUBTRACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TRADEACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:INVOICE.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBOUTFL.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:MANUFACT.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:MANFAUPA.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:MANFAULO.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:MANFAULT.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBSE.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:CHARTYPE.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  locRefNumber = p_web.GSV('job:Ref_Number')
  
  set(DEFAULTS)
  access:DEFAULTS.next()
  
  set(DEFAULT2)
  access:DEFAULT2.next()
  
  
  locAccountNumber = p_web.GSV('job:Account_Number')
  locOrderNumber = p_web.GSV('job:Order_Number')
  locAuthorityNumber = p_web.GSV('job:Authority_Number')
  locChargeType = p_web.GSV('job:Charge_Type')
  locRepairType = p_web.GSV('job:Repair_Type')
  locModelNumber = p_web.GSV('job:Model_Number')
  locManufacturer = p_web.GSV('job:Manufacturer')
  locUnitType = p_web.GSV('job:Unit_Type')
  locESN = p_web.GSV('job:ESN')
  locMSN = p_web.GSV('job:MSN')
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('Estimate',ProgressWindow)                  ! Restore window settings from non-volatile store
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ProgressWindow{Prop:Timer} = 10                          ! Assign timer interval
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:JOBS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, job:Ref_Number)
  ThisReport.AddSortOrder(job:Ref_Number_Key)
  ThisReport.AddRange(job:Ref_Number,locRefNumber)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{Prop:Text}=''
  Relate:JOBS.SetQuickScan(1,Propagate:OneMany)
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom=True
  Previewer.Maximize=True
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
      SELF.SetReportTarget(PDFReporter.IReportGenerator)
    self.SkipPreview = True
    ProgressWindow{prop:hide} = 1
    loc:PDFName = clip(p_web.site.WebFolderPath) & '\reports\$$$' & format(random(1,99999),@n05) &'.pdf'
  End
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:STANTEXT.Close
    Relate:VATCODE.Close
    Relate:WEBJOB.Close
  END
  IF SELF.Opened
    INIMgr.Update('Estimate',ProgressWindow)               ! Save window data to non-volatile store
  END
  ProgressMgr.Kill()
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      Loc:html = '<script type="text/javascript">alert('''&clip('No Records')&''');top.close();</script>'
      p_web.ParseHTML(loc:html)
    Else
      p_web.ForceNoCache = 1
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(loc:PDFName)
    End
  End
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text}=FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
    If Not p_Web &= NULL
      loc:NoRecords = 1
      Return
    End
  PARENT.TakeNoRecords


ThisWindow.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
      Delivery_Company_Name_Temp = p_web.GSV('job:Company_Name_Delivery')
      delivery_address1_temp     = p_web.GSV('job:address_line1_delivery')
      delivery_address2_temp     = p_web.GSV('job:address_line2_delivery')
      If p_web.GSV('job:address_line3_delivery')   = ''
          delivery_address3_temp = p_web.GSV('job:postcode_delivery')
          delivery_address4_temp = ''
      Else
          delivery_address3_temp  = p_web.GSV('job:address_line3_delivery')
          delivery_address4_temp  = p_web.GSV('job:postcode_delivery')
      End
      Delivery_Telephone_Number_Temp = p_web.GSV('job:Telephone_Delivery')
  
      access:subtracc.clearkey(sub:account_number_key)
      sub:account_number = p_web.GSV('job:account_number')
      access:subtracc.fetch(sub:account_number_key)
      access:tradeacc.clearkey(tra:account_number_key)
      tra:account_number = sub:main_account_number
      access:tradeacc.fetch(tra:account_number_key)
      if tra:invoice_sub_accounts = 'YES'
          If sub:invoice_customer_address = 'YES'
              invoice_address1_temp     = p_web.GSV('job:address_line1')
              invoice_address2_temp     = p_web.GSV('job:address_line2')
              If p_web.GSV('job:address_line3_delivery')   = ''
                  invoice_address3_temp = p_web.GSV('job:postcode')
                  invoice_address4_temp = ''
              Else
                  invoice_address3_temp  = p_web.GSV('job:address_line3')
                  invoice_address4_temp  = p_web.GSV('job:postcode')
              End
              invoice_company_name_temp   = p_web.GSV('job:company_name')
              invoice_telephone_number_temp   = p_web.GSV('job:telephone_number')
              invoice_fax_number_temp = p_web.GSV('job:fax_number')
          Else!If sub:invoice_customer_address = 'YES'
              invoice_address1_temp     = sub:address_line1
              invoice_address2_temp     = sub:address_line2
              If p_web.GSV('job:address_line3_delivery')   = ''
                  invoice_address3_temp = sub:postcode
                  invoice_address4_temp = ''
              Else
                  invoice_address3_temp  = sub:address_line3
                  invoice_address4_temp  = sub:postcode
              End
              invoice_company_name_temp   = sub:company_name
              invoice_telephone_number_temp   = sub:telephone_number
              invoice_fax_number_temp = sub:fax_number
  
  
          End!If sub:invoice_customer_address = 'YES'
  
      else!if tra:use_sub_accounts = 'YES'
          If tra:invoice_customer_address = 'YES'
              invoice_address1_temp     = p_web.GSV('job:address_line1')
              invoice_address2_temp     = p_web.GSV('job:address_line2')
              If p_web.GSV('job:address_line3_delivery')   = ''
                  invoice_address3_temp = p_web.GSV('job:postcode')
                  invoice_address4_temp = ''
              Else
                  invoice_address3_temp  = p_web.GSV('job:address_line3')
                  invoice_address4_temp  = p_web.GSV('job:postcode')
              End
              invoice_company_name_temp   = p_web.GSV('job:company_name')
              invoice_telephone_number_temp   = p_web.GSV('job:telephone_number')
              invoice_fax_number_temp = p_web.GSV('job:fax_number')
  
          Else!If tra:invoice_customer_address = 'YES'
              invoice_address1_temp     = tra:address_line1
              invoice_address2_temp     = tra:address_line2
              If p_web.GSV('job:address_line3_delivery')   = ''
                  invoice_address3_temp = tra:postcode
                  invoice_address4_temp = ''
              Else
                  invoice_address3_temp  = tra:address_line3
                  invoice_address4_temp  = tra:postcode
              End
              invoice_company_name_temp   = tra:company_name
              invoice_telephone_number_temp   = tra:telephone_number
              invoice_fax_number_temp = tra:fax_number
  
          End!If tra:invoice_customer_address = 'YES'
      End!!if tra:use_sub_accounts = 'YES'
  
  
      if p_web.GSV('job:title') = '' and p_web.GSV('job:initial') = ''
          customer_name_temp = clip(p_web.GSV('job:surname'))
      elsif p_web.GSV('job:title') = '' and p_web.GSV('job:initial') <> ''
          customer_name_temp = clip(p_web.GSV('job:initial')) & ' ' & clip(p_web.GSV('job:surname'))
      elsif p_web.GSV('job:title') <> '' and p_web.GSV('job:initial') = ''
          customer_name_temp = clip(p_web.GSV('job:title')) & ' ' & clip(p_web.GSV('job:surname'))
      elsif p_web.GSV('job:title') <> '' and p_web.GSV('job:initial') <> ''
          customer_name_temp = clip(p_web.GSV('job:title')) & ' ' & clip(p_web.GSV('job:initial')) & ' ' & clip(p_web.GSV('job:surname'))
      else
          customer_name_temp = ''
      end
  
      access:stantext.clearkey(stt:description_key)
      stt:description = 'ESTIMATE'
      access:stantext.fetch(stt:description_key)
  
  
      courier_cost_temp = p_web.GSV('job:courier_cost_estimate')
  
      !If booked at RRC, use RRC costs for estimate
      If p_web.GSV('jobe:WebJob') = 1
          Labour_Temp = p_web.GSV('jobe:RRCELabourCost')
          Parts_Temp  = p_web.GSV('jobe:RRCEPartsCost')
          Sub_Total_Temp = p_web.GSV('jobe:RRCELabourCost') + p_web.GSV('jobe:RRCEPartsCost') + p_web.GSV('job:Courier_Cost_Estimate')
      Else !p_web.GSV('jobe:WebJob')
          labour_temp = p_web.GSV('job:labour_cost_estimate')
          parts_temp  = p_web.GSV('job:parts_cost_estimate')
          sub_total_temp  = p_web.GSV('job:labour_cost_estimate') + p_web.GSV('job:parts_cost_estimate') + p_web.GSV('job:courier_cost_estimate')
      End !p_web.GSV('jobe:WebJob')
  
  
        ! Inserting (DBH 03/12/2007) # 8218 - Use the ARC details, if it's an RRC-ARC estimate job
        sentToHub(p_web)
        If p_web.GSV('SentToHub') = 1 And p_web.GSV('jobe:WebJob') = 1
            ! ARC Estimate, use ARC Details
            If p_web.GSV('BookingSite') <> 'RRC'
                ! Use ARC Details
                Access:WEBJOB.Clearkey(wob:RefNumberKey)
                wob:RefNumber = p_web.GSV('job:Ref_Number')
                If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                    tra:Account_Number = wob:HeadAccountNumber
                    If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                        Customer_Name_Temp = ''
                        Invoice_Company_Name_Temp = tra:Company_Name
                        Invoice_Address1_Temp = tra:Address_Line1
                        Invoice_Address2_Temp = tra:Address_Line2
                        Invoice_Address3_Temp = tra:Address_Line3
                        Invoice_Address4_Temp = tra:Postcode
                        Invoice_Telephone_Number_Temp = tra:Telephone_Number
                        Invoice_Fax_Number_Temp = tra:Fax_Number
  
                        Delivery_Company_Name_Temp = tra:Company_Name
                        Delivery_Address1_Temp = tra:Address_Line1
                        Delivery_Address2_Temp = tra:Address_Line2
                        Delivery_Address3_Temp = tra:Address_Line3
                        Delivery_Address4_Temp = tra:Postcode
                        Delivery_Telephone_Number_Temp = tra:Telephone_Number
  
                        labour_temp = p_web.GSV('job:labour_cost_estimate')
                        parts_temp  = p_web.GSV('job:parts_cost_estimate')
                        sub_total_temp  = p_web.GSV('job:labour_cost_estimate') + p_web.GSV('job:parts_cost_estimate')+ p_web.GSV('job:courier_cost_estimate')
  
                    End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
            End ! If glo:WebJob
        End ! If SentToHub(p_web.GSV('job:Ref_Number) And p_web.GSV('jobe:WebJob
        ! End (DBH 03/12/2007) #8218
  
      p_web.SSV('TotalPrice:Type','E')
      totalPrice(p_web)
      vat_temp = p_web.GSV('TotalPrice:VAT')
      total_temp = p_web.GSV('TotalPrice:Total')
  
      Access:MANUFACT.Clearkey(man:Manufacturer_Key)
      man:Manufacturer    = p_web.GSV('job:Manufacturer')
      If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
          !Found
          If ~man:UseInvTextForFaults
              tmp:InvoiceText = jbn:Invoice_Text
          Else !If ~man:UseInvTextForFaults
              tmp:InvoiceText = ''
              Access:JOBOUTFL.ClearKey(joo:JobNumberKey)
              joo:JobNumber = p_web.GSV('job:Ref_Number')
              Set(joo:JobNumberKey,joo:JobNumberKey)
              Loop
                  If Access:JOBOUTFL.NEXT()
                     Break
                  End !If
                  If joo:JobNumber <> p_web.GSV('job:Ref_Number')      |
                      Then Break.  ! End If
                  ! Inserting (DBH 16/10/2006) # 8059 - Do not show the IMEI Validation text on the paperwork
                  If Instring('IMEI VALIDATION: ',joo:Description,1,1)
                      Cycle
                  End ! If Instring('IMEI VALIDATION: ',joo:Description,1,1)
                  ! End (DBH 16/10/2006) #8059
                  If tmp:InvoiceText = ''
                      tmp:InvoiceText = Clip(joo:FaultCode) & ' ' & Clip(joo:Description)
                  Else !If tmp:Invoice_Text = ''
                      tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(joo:FaultCode) & ' ' & Clip(joo:Description)
                  End !If tmp:Invoice_Text = ''
              End !Loop
  
              Access:MANFAUPA.ClearKey(map:MainFaultKey)
              map:Manufacturer = p_web.GSV('job:Manufacturer')
              map:MainFault    = 1
              If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
                  !Found
                  Access:MANFAULT.ClearKey(maf:MainFaultKey)
                  maf:Manufacturer = p_web.GSV('job:Manufacturer')
                  maf:MainFault    = 1
                  If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                      !Found
                      Save_epr_ID = Access:ESTPARTS.SaveFile()
                      Access:ESTPARTS.ClearKey(epr:Part_Number_Key)
                      epr:Ref_Number  = p_web.GSV('job:Ref_Number')
                      Set(epr:Part_Number_Key,epr:Part_Number_Key)
                      Loop
                          If Access:ESTPARTS.NEXT()
                             Break
                          End !If
                          If epr:Ref_Number  <> p_web.GSV('job:Ref_Number')      |
                              Then Break.  ! End If
                          Access:MANFAULO.ClearKey(mfo:Field_Key)
                          mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                          mfo:Field_Number = maf:Field_Number
  
                          Case map:Field_Number
                              Of 1
                                  mfo:Field   = epr:Fault_Code1
                                  If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Found
                                      tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code1) & ' ' & Clip(mfp:Description)
                                  Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                              Of 2
                                  mfo:Field   = epr:Fault_Code2
                                  If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Found
                                      tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code2) & ' ' & Clip(mfp:Description)
                                  Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                              Of 3
                                  mfo:Field   = epr:Fault_Code3
                                  If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Found
                                      tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code3) & ' ' & Clip(mfp:Description)
                                  Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                              Of 4
                                  mfo:Field   = epr:Fault_Code4
                                  If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Found
                                      tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code4) & ' ' & Clip(mfp:Description)
                                  Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                              Of 5
                                  mfo:Field   = epr:Fault_Code5
                                  If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Found
                                      tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code5) & ' ' & Clip(mfp:Description)
                                  Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                              Of 6
                                  mfo:Field   = epr:Fault_Code6
                                  If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Found
                                      tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code6) & ' ' & Clip(mfp:Description)
                                  Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                              Of 7
                                  mfo:Field   = epr:Fault_Code7
                                  If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Found
                                      tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code7) & ' ' & Clip(mfp:Description)
                                  Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                              Of 8
                                  mfo:Field   = epr:Fault_Code8
                                  If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Found
                                      tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code8) & ' ' & Clip(mfp:Description)
                                  Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                              Of 9
                                  mfo:Field   = epr:Fault_Code9
                                  If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Found
                                      tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code9) & ' ' & Clip(mfp:Description)
                                  Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                              Of 10
                                  mfo:Field   = epr:Fault_Code10
                                  If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Found
                                      tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code10) & ' ' & Clip(mfp:Description)
                                  Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                              Of 11
                                  mfo:Field   = epr:Fault_Code11
                                  If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Found
                                      tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code11) & ' ' & Clip(mfp:Description)
                                  Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                              Of 12
                                  mfo:Field   = epr:Fault_Code12
                                  If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Found
                                      tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code12) & ' ' & Clip(mfp:Description)
                                  Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                          End !Case map:Field_Number
                      End !Loop
  
                  Else!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                      !Error
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                  End!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
              Else!If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
              End!If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
  
          End !If ~man:UseInvTextForFaults
      Else ! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
          !Error
      End !If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
  
      job_Number_Temp = locRefNumber
      esn_Temp = locESN
      Bar_Code_Temp = '*' & clip(job_Number_Temp) & '*'
      Bar_Code2_Temp = '*' & clip(esn_Temp) & '*'
  
      job_Number_temp = 'Job No: ' & clip(job_Number_Temp)
      esn_Temp = 'IMEI No: ' & clip(esn_Temp)
  !*********CHANGE LICENSE ADDRESS*********
  
  Access:TRADEACC.ClearKey(tra:Account_Number_Key)
  tra:Account_Number = p_web.GSV('wob:HeadAccountNumber')
  IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
  END
  
  tmp:Ref_Number = p_web.GSV('Job:Ref_Number') & '-' & |
      tra:BranchIdentification & p_web.GSV('wob:JobNumber')
  
  Access:TRADEACC.Clearkey(tra:Account_Number_Key)
  If (jobe:WebJob = 1)
      tra:Account_Number = p_web.GSV('wob:HeadAccountNumber')
  ELSE ! If (glo:WebJob = 1)
      tra:Account_Number = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
  END ! If (glo:WebJob = 1)
  If (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
      address:SiteName        = tra:Company_Name
      address:Name            = tra:coTradingName
      address:Name2           = tra:coTradingName2  ! #12079 New address fields. (Bryan: 13/04/2011)
      address:Location        = tra:coLocation
      address:RegistrationNo  = tra:coRegistrationNo
      address:AddressLine1    = tra:coAddressLine1
      address:AddressLine2    = tra:coAddressLine2
      address:AddressLine3    = tra:coAddressLine3
      address:AddressLine4    = tra:coAddressLine4
      address:Telephone       = tra:coTelephoneNumber
      address:Fax             = tra:coFaxNumber
      address:EmailAddress    = tra:coEmailAddress
      address:VatNumber       = tra:coVATNumber
  END
      jbn:Fault_Description = p_web.GSV('jbn:Fault_Description')
  
  
      count# = 0
      FixedCharge# = 0
      Access:CHARTYPE.ClearKey(cha:Warranty_Key)
      cha:Warranty    = 'NO'
      cha:Charge_Type = p_web.GSV('job:Charge_Type')
      If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
          !Found
          If glo:WebJob
              If cha:Zero_Parts
                  FixedCharge# = 1
              End !If cha:Fixed_Charge
          Else !If glo:WebJob
              If cha:Zero_Parts_ARC
                  FixedCharge# = 1
              End !If cha:Fixed_Charge_ARC
          End !If glo:WebJob
      Else!If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End!If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
  
      If FixedCharge#
          SetTarget(Report)
          ?Cost_Temp{prop:Hide} = 1
          ?Line_Cost_Temp{prop:Hide} = 1
          ?UnitCost{prop:Hide} = 1
          ?LineCost{prop:Hide} = 1
          SetTarget()
      End !FixedCharge#
  
      access:estparts.clearkey(epr:part_number_key)
      epr:ref_number  = p_web.GSV('job:ref_number')
      set(epr:part_number_key,epr:part_number_key)
      loop
          if access:estparts.next()
             break
          end !if
          if epr:ref_number  <> p_web.GSV('job:ref_number')      |
              then break.  ! end if
          yldcnt# += 1
          if yldcnt# > 25
             yield() ; yldcnt# = 0
          end !if
          count# += 1
          part_number_temp = epr:part_number
          description_temp = epr:description
          quantity_temp = epr:quantity
          cost_temp = epr:sale_cost
          line_cost_temp = epr:quantity * epr:sale_cost
          Print(rpt:detail)
  
      end !loop
  
      If count# = 0
          SetTarget(Report)
          ?PartsHeadingGroup{Prop:Hide} = 1
          Part_Number_Temp = ''
          !Print Blank Line
          Print(Rpt:Detail)
          SetTarget()
      End!If count# = 0
  
  
      If p_web.GSV('job:Estimate') = 'YES' And p_web.GSV('job:estimate_accepted') <> 'YES' and p_web.GSV('job:estimate_rejected') <> 'YES'
  !        p_web.SSV('GetStatus:Type','JOB')
  !        p_web.SSV('GetStatus:StatusNumber',520)
  !        getStatus(p_web)
  !
  !        Access:JOBS.Clearkey(job:ref_Number_Key)
  !        job:ref_Number    = p_web.GSV('job:Ref_Number')
  !        if (Access:JOBS.TryFetch(job:ref_Number_Key) = Level:Benign)
  !            ! Found
  !            p_web.SessionQueueToFile(JOBS)
  !            access:JOBS.tryUpdate()
  !        else ! if (Access:JOBS.TryFetch(job:ref_Number_Key) = Level:Benign)
  !            ! Error
  !        end ! if (Access:JOBS.TryFetch(job:ref_Number_Key) = Level:Benign)
  !
  !
  !        Access:WEBJOB.Clearkey(wob:refNumberKey)
  !        wob:refNumber    = p_web.GSV('job:Ref_Number')
  !        if (Access:WEBJOB.TryFetch(wob:refNumberKey) = Level:Benign)
  !            ! Found
  !            p_web.SessionQueueToFile(WEBJOB)
  !            access:WEBJOB.tryUpdate()
  !        else ! if (Access:WEBJOB.TryFetch(wob:refNumberKey) = Level:Benign)
  !            ! Error
  !        end ! if (Access:WEBJOB.TryFetch(wob:refNumberKey) = Level:Benign)
  !
  !        p_web.SSV('AddToAudit:Type','JOB')
  !        p_web.SSV('AddToAudit:Notes','ESTIMATE VALUE: ' & Format(Total_Temp,@n14.2))
  !        p_web.SSV('AddToAudit:Action','ESTIMATE SENT')
  !        addToAudit(p_web)
  
      End!If p_web.GSV('job:estimate = 'YES' And p_web.GSV('job:estimate_accepted <> 'YES' and p_web.GSV('job:estimate_refused <> 'YES'
  
  
  ReturnValue = PARENT.TakeRecord()
    If Not p_web &= Null
      p_web.NoOp()
    End
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
  IF 0
    PRINT(RPT:DETAIL)
  END
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','WebServer_Phase4','Estimate','Estimate','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.CompressText   = True
  SELF.CompressImages = True
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    SELF.SetFileName(clip(loc:PDFName))
  End

SentToHub            PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
FilesOpened     BYTE(0)
  CODE
    do openFiles
    p_web.SSV('SentToHub',0)
    if (p_web.GSV('jobe:HubRepair') = 1 Or p_web.GSV('jobe:HubRepairDate') > 0)
        p_web.SSV('SentToHub',1)
    Else !If jobe:HubRepair
        Access:LOCATLOG.ClearKey(lot:NewLocationKey)
        lot:RefNumber   = p_web.GSV('job:Ref_Number')
        lot:NewLocation = p_web.GSV('Default:ARCLocation')
        If Access:LOCATLOG.TryFetch(lot:NewLocationKey) = Level:Benign
            p_web.SSV('SentToHub',1)
        End !If Access:LOCATLOG.TryFetch(lot:NewLocationKey)
    End !If jobe:HubRepair
    do closeFiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:LOCATLOG.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:LOCATLOG.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSE.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSE.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:LOCATLOG.Close
     Access:JOBSE.Close
     FilesOpened = False
  END
AddToAudit           PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
  CODE
    if (p_web.GSV('AddToAudit:Action') <> '' and p_web.GSV('AddToAudit:Type') <> '')
        relate:AUDIT.open()
        relate:AUDITE.open()

        If Access:AUDIT.PrimeRecord() = Level:Benign
            aud:Date        = Today()
            aud:Time        = Clock()
            aud:Ref_Number  = p_web.GSV('job:Ref_Number')
            aud:User        = p_web.GSV('BookingUserCode')
            aud:Action      = p_web.GSV('AddToAudit:Action')
            aud:Notes       = p_web.GSV('AddToAudit:Notes')
            aud:Type        = p_web.GSV('AddToAudit:Type')
            If Access:AUDIT.TryInsert() = Level:Benign
                ! Insert Successful
                Access:AUDITE.Open()
                Access:AUDITE.UseFile()
                If Access:AUDITE.PrimeRecord() = Level:Benign
                    aude:RefNumber = aud:Record_Number
                    aude:IPAddress = p_web.RequestData.FromIP
                    aude:HostName = 'SB Online'
                    If Access:AUDITE.TryInsert() = Level:Benign
                        ! Insert Successful
                   Else ! If Access:AUDITE.TryInsert() = Level:Benign
                        ! Insert Failed
                        Access:AUDITE.CancelAutoInc()
                    End ! If Access:AUDITE.TryInsert() = Level:Benign
                End !If Access:AUDITE.PrimeRecord() = Level:Benign
                Access:AUDITE.Close()
            Else ! If Access:AUDIT.TryInsert() = Level:Benign
                ! Insert Failed
                Access:AUDIT.CancelAutoInc()
            End ! If Access:AUDIT.TryInsert() = Level:Benign
        End !If Access:AUDIT.PrimeRecord() = Level:Benign
        ! Clear fields to stop being written incorrectly (DBH: 02/03/2009)

        relate:AUDITE.close()
        relate:AUDIT.close()
    end ! if (p_web.GSV('AddToAudit:Action') <> '' and p_web.GSV('AddToAudit:Type') <> '')

    p_web.deleteSessionValue('AddToAudit:Action')
    p_web.deleteSessionValue('AddToAudit:Notes')
    p_web.deleteSessionValue('AddToAudit:Type')
DespatchNote PROCEDURE (<NetWebServerWorker p_web>)        ! Generated from procedure template - Report

  ! The NetTalk Extension to report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
Loc:Html      String(1024)
Progress:Thermometer BYTE                                  !
locJobNumber         LONG                                  !
locFullJobNumber     STRING(30)                            !
locChargeType        STRING(30)                            !
locRepairType        STRING(30)                            !
locOriginalIMEI      STRING(30)                            !
locFinalIMEI         STRING(30)                            !
locEngineerReport    STRING(255)                           !
locAccessories       STRING(255)                           !
locLoanExchangeUnit  STRING(100)                           !
locIDNumber          STRING(20)                            !
locQuantity          LONG                                  !
locPartNumber        STRING(30)                            !
locDescription       STRING(30)                            !
locUnitCost          REAL                                  !
locLineCost          REAL                                  !
locLoanReplacementValue REAL                               !
locLabour            REAL                                  !
locParts             REAL                                  !
locCarriage          REAL                                  !
locVAT               REAL                                  !
locTotal             REAL                                  !
locTerms             STRING(1000)                          !
locBarCodeJobNumber  STRING(30)                            !
locBarCodeIMEINumber STRING(30)                            !
locDisplayJobNumber  STRING(30)                            !
locDisplayIMEINumber STRING(30)                            !
locCustomerName      STRING(60)                            !
locEndUserTelNo      STRING(60)                            !
locDespatchUser      STRING(60)                            !
locClientName        STRING(60)                            !
qOutFaults           QUEUE,PRE()                           !
Description          STRING(255)                           !
                     END                                   !
DefaultAddress       GROUP,PRE(address)                    !
Name                 STRING(40)                            !Name
SiteName             STRING(40)                            !
Name2                STRING(40)                            !
Location             STRING(40)                            !
RegistrationNo       STRING(40)                            !
VATNumber            STRING(40)                            !
AddressLine1         STRING(40)                            !Address Line 1
AddressLine2         STRING(40)                            !Address Line 2
AddressLine3         STRING(40)                            !Address Line 3
AddressLine4         STRING(40)                            !Postcode
Telephone            STRING(30)                            !Telephone
Fax                  STRING(30)                            !Fax
EmailAddress         STRING(255)                           !Email Address
                     END                                   !
InvoiceAddress       GROUP,PRE(invoice)                    !
Name                 STRING(30)                            !
AddressLine1         STRING(30)                            !
AddressLine2         STRING(30)                            !
AddressLine3         STRING(30)                            !
AddressLine4         STRING(30)                            !
TelephoneNumber      STRING(30)                            !
FaxNumber            STRING(30)                            !
MobileNumber         STRING(30)                            !
                     END                                   !
DeliveryAddress      GROUP,PRE(delivery)                   !
Name                 STRING(30)                            !
AddressLine1         STRING(30)                            !
AddressLine2         STRING(30)                            !
AddressLine3         STRING(30)                            !
AddressLine4         STRING(30)                            !
TelephoneNumber      STRING(30)                            !
                     END                                   !
locTermsText         STRING(255)                           !
Process:View         VIEW(JOBS)
                       PROJECT(job:Account_Number)
                       PROJECT(job:Authority_Number)
                       PROJECT(job:Consignment_Number)
                       PROJECT(job:Courier)
                       PROJECT(job:Date_Completed)
                       PROJECT(job:Despatch_Number)
                       PROJECT(job:MSN)
                       PROJECT(job:Manufacturer)
                       PROJECT(job:Model_Number)
                       PROJECT(job:Order_Number)
                       PROJECT(job:Ref_Number)
                       PROJECT(job:Unit_Type)
                       PROJECT(job:date_booked)
                     END
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,TIMER(1),GRAY,DOUBLE
                       PROGRESS,USE(Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

Report               REPORT('ORDERS Report'),AT(250,6792,7760,1417),PAPER(PAPER:A4),PRE(RPT),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),THOUS
                       HEADER,AT(250,292,7750,10000),USE(?Header),FONT('Tahoma',8,,FONT:regular)
                         STRING('Job Number:'),AT(5125,375),USE(?STRING1)
                         STRING('Despach Batch No:'),AT(5125,625,1042,167),USE(?STRING1:2)
                         STRING('Date Booked:'),AT(5125,875),USE(?STRING2)
                         STRING('Date Completed:'),AT(5125,1125),USE(?STRING3)
                         STRING(@s8),AT(6250,625),USE(job:Despatch_Number),LEFT(1)
                         STRING(@d6b),AT(6250,875),USE(job:date_booked),LEFT
                         STRING(@D6b),AT(6250,1125),USE(job:Date_Completed),TRN,LEFT
                         STRING(@s30),AT(167,1708,2250,),USE(invoice:Name)
                         STRING(@s30),AT(167,1875,2208,),USE(invoice:AddressLine1)
                         STRING(@s30),AT(167,2042,2208,),USE(invoice:AddressLine2)
                         STRING(@s30),AT(167,2208,2250,),USE(invoice:AddressLine3)
                         STRING(@s30),AT(167,2375,2208,),USE(invoice:AddressLine4)
                         STRING('Tel:'),AT(2500,1875),USE(?STRING4)
                         STRING('Fax:'),AT(2500,2042,302,167),USE(?STRING4:2)
                         STRING('Mobile:'),AT(2500,2208,417,167),USE(?STRING4:3)
                         STRING(@s30),AT(2958,1875,927,),USE(invoice:TelephoneNumber)
                         STRING(@s30),AT(2958,2042,927,),USE(invoice:FaxNumber)
                         STRING(@s30),AT(2958,2208,927,),USE(invoice:MobileNumber)
                         STRING(@s30),AT(4125,1708,2250,),USE(delivery:Name)
                         STRING(@s30),AT(4125,1875,2250,),USE(delivery:AddressLine1)
                         STRING(@s30),AT(4125,2042,2250,),USE(delivery:AddressLine2)
                         STRING(@s30),AT(4125,2208,2250,),USE(delivery:AddressLine3)
                         STRING(@s30),AT(4125,2375,2250,),USE(delivery:AddressLine4)
                         STRING('Tel:'),AT(4125,2542,198,167),USE(?STRING4:4)
                         STRING(@s30),AT(4375,2542,2250,),USE(delivery:TelephoneNumber)
                         STRING(@s30),AT(6250,375,1375,),USE(locFullJobNumber)
                         STRING(@s15),AT(156,3406),USE(job:Account_Number),TRN,LEFT
                         STRING(@s30),AT(1677,3406,1125,),USE(job:Order_Number),TRN,LEFT
                         STRING(@s30),AT(3187,3406,1323,),USE(job:Authority_Number),TRN,LEFT
                         STRING(@s30),AT(4698,3406,1437,),USE(locChargeType)
                         STRING(@s30),AT(6208,3406,1500,),USE(locRepairType)
                         STRING(@s30),AT(156,4104,1344,),USE(job:Model_Number),TRN,LEFT
                         STRING(@s30),AT(1562,4104,1323,),USE(job:Manufacturer),TRN,LEFT
                         STRING(@s30),AT(2958,4104,1042,),USE(job:Unit_Type),TRN,LEFT
                         STRING(@s20),AT(4115,4104,1042,),USE(locOriginalIMEI),TRN,LEFT
                         STRING(@s30),AT(5219,4104,1125,),USE(locFinalIMEI),HIDE
                         STRING(@s20),AT(6479,4104,1187,),USE(job:MSN),TRN,LEFT
                         STRING('REPORTED FAULT'),AT(167,4333),USE(?STRING5),FONT(,,,FONT:bold)
                         TEXT,AT(1490,4333,5948,427),USE(jbn:Fault_Description),TRN
                         STRING('ENGINEER REPORT'),AT(167,4792),USE(?STRING6),FONT(,,,FONT:bold)
                         TEXT,AT(1500,4792,5948,677),USE(locEngineerReport),TRN
                         STRING('ACCESSORIES'),AT(167,5500),USE(?STRING7),FONT(,,,FONT:bold)
                         TEXT,AT(1500,5500,5948,302),USE(locAccessories),FONT(,,,FONT:regular+FONT:underline)
                         STRING('EXCHANGE UNIT'),AT(167,5833),USE(?strExchangeUnit),HIDE,FONT(,,,FONT:bold)
                         STRING(@s100),AT(1500,5833,5948,),USE(locLoanExchangeUnit),HIDE
                         STRING('PARTS USED'),AT(167,6292),USE(?STRING9),FONT(,,,FONT:bold)
                         STRING('Qty'),AT(1500,6250),USE(?STRING10),FONT(,,,FONT:bold)
                         STRING('Part Number'),AT(2000,6250),USE(?STRING11),FONT(,,,FONT:bold)
                         STRING('ID NUMBER'),AT(167,6083),USE(?STRING12),FONT(,,,FONT:bold)
                         STRING(@s20),AT(1500,6042),USE(locIDNumber)
                         STRING('Description'),AT(3750,6250),USE(?STRING13),FONT(,,,FONT:bold)
                         STRING('Unit Type'),AT(5833,6250),USE(?STRING14),FONT(,,,FONT:bold)
                         STRING('Line Cost'),AT(6792,6250),USE(?STRING15),FONT(,,,FONT:bold)
                         LINE,AT(1510,6469,5875,0),USE(?LINE1)
                         LINE,AT(1510,8385,5875,0),USE(?lineTerms)
                         STRING('Customer Signature'),AT(167,8208),USE(?strTerms2),FONT(,,,FONT:bold)
                         STRING('Date'),AT(6000,8208),USE(?strTerms3),FONT(,,,FONT:bold)
                         STRING('Courier:'),AT(167,8917,792,167),USE(?STRING21:2)
                         STRING(@s30),AT(1083,8917,1625,),USE(job:Courier),FONT(,,,FONT:bold)
                         STRING('Despatch Date:'),AT(167,8750),USE(?ReportDatePrompt),TRN
                         STRING('<<-- Date Stamp -->'),AT(1083,8750),USE(?ReportDateStamp),TRN,FONT(,,,FONT:bold)
                         STRING('Consignment No:'),AT(167,9083),USE(?STRING21)
                         STRING(@s30),AT(1083,9083,1625,167),USE(job:Consignment_Number),FONT(,,,FONT:bold)
                         STRING('Loan Replacement Value:'),AT(167,9333),USE(?strLoanReplacementValue),HIDE
                         STRING(@n10.2),AT(1500,9333),USE(locLoanReplacementValue),HIDE,RIGHT(2),FONT(,,,FONT:bold)
                         STRING('Vodacom Repairs Loan Phone Terms And Conditions'),AT(167,9844),USE(?strLoanTerms),HIDE,FONT(,7,,FONT:bold+FONT:underline)
                         STRING('Labour:'),AT(5458,8750),USE(?strLabour)
                         STRING('Parts:'),AT(5458,8917),USE(?strParts)
                         STRING('Carriage:'),AT(5458,9083),USE(?strCarriage)
                         STRING('V.A.T.:'),AT(5458,9250),USE(?strVAT)
                         STRING('Total:'),AT(5458,9500),USE(?strTotal),FONT(,,,FONT:bold)
                         LINE,AT(6344,9469,1000,0),USE(?LINE2)
                         STRING(@n14.2b),AT(6292,8750),USE(locLabour),RIGHT(2)
                         STRING(@n14.2b),AT(6292,8917),USE(locParts),RIGHT(2)
                         STRING(@n14.2b),AT(6292,9083),USE(locCarriage),RIGHT(2)
                         STRING(@n14.2b),AT(6292,9250),USE(locVAT),RIGHT(2)
                         STRING(@n14.2b),AT(6156,9500),USE(locTotal),RIGHT(2),FONT(,,,FONT:bold)
                         STRING(@s20),AT(2573,8750),USE(locBarCodeJobNumber),CENTER,FONT('C39 High 12pt LJ3',12,,)
                         STRING(@s20),AT(2573,9167,2594,198),USE(locBarCodeIMEINumber),CENTER,FONT('C39 High 12pt LJ3',12,,)
                         STRING(@s30),AT(2729,8917),USE(locDisplayJobNumber),CENTER,FONT(,,,FONT:bold)
                         STRING(@s30),AT(2729,9333),USE(locDisplayIMEINumber),CENTER,FONT(,,,FONT:bold)
                         TEXT,AT(156,7917,5885,260),USE(locTermsText),FONT(,7,,)
                       END
Detail                 DETAIL,AT(,,7750,208),USE(?Detail)
                         STRING(@n-14b),AT(667,0),USE(locQuantity),RIGHT(2)
                         STRING(@s30),AT(2000,0),USE(locPartNumber)
                         STRING(@s30),AT(3750,0),USE(locDescription)
                         STRING(@n14.2b),AT(5302,0),USE(locUnitCost),RIGHT(2)
                         STRING(@n14.2b),AT(6229,0),USE(locLineCost),RIGHT(2)
                       END
                       FOOTER,AT(260,10208,7750,833),USE(?Footer)
                         TEXT,AT(125,0,7458,792),USE(stt:Text)
                       END
                       FORM,AT(250,250,7750,11188),USE(?Text:CurrencyItemCost:2),FONT('Tahoma',8,,FONT:regular)
                         STRING('DESPATCH NOTE'),AT(5708,-42,1917,240),USE(?strTitle),TRN,RIGHT,FONT(,16,,FONT:bold)
                         STRING(@s40),AT(104,0,4167,260),USE(address:SiteName),LEFT,FONT(,14,,FONT:bold)
                         STRING(@s40),AT(104,313,3073,208),USE(address:Name2),TRN,FONT(,8,COLOR:Black,FONT:bold)
                         STRING(@s40),AT(104,208,3073,208),USE(address:Name),TRN,FONT(,8,COLOR:Black,FONT:bold)
                         STRING(@s40),AT(104,417,2760,208),USE(address:Location),TRN,FONT(,8,COLOR:Black,FONT:bold)
                         STRING('REG NO:'),AT(104,573),USE(?stringREGNO),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s40),AT(625,573,1667,208),USE(address:RegistrationNo),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING('VAT NO: '),AT(104,677),USE(?stringVATNO),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s20),AT(625,677,1771,156),USE(address:VATNumber),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s40),AT(104,833,2240,156),USE(address:AddressLine1),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s40),AT(104,938,2240,156),USE(address:AddressLine2),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s40),AT(104,1042,2240,156),USE(address:AddressLine3),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s40),AT(104,1146),USE(address:AddressLine4),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING('TEL:'),AT(104,1250),USE(?stringTEL),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s30),AT(469,1250,1458,208),USE(address:Telephone),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING('FAX:'),AT(2083,1250,313,208),USE(?stringFAX),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s30),AT(2396,1250,1510,188),USE(address:Fax),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING('EMAIL:'),AT(104,1354,417,208),USE(?stringEMAIL),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s255),AT(469,1354,3333,208),USE(address:EmailAddress),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING('JOB DETAILS'),AT(5000,208),USE(?String57),TRN,FONT(,8,,FONT:bold)
                         STRING('CUSTOMER ADDRESS'),AT(156,1510),USE(?String24),TRN,FONT(,8,,FONT:bold)
                         STRING('DELIVERY ADDRESS'),AT(4115,1510,1677,156),USE(?DeliveryAddress),TRN,FONT(,8,,FONT:bold)
                         STRING('GENERAL DETAILS'),AT(156,2990),USE(?String91),TRN,FONT(,8,,FONT:bold)
                         STRING('DELIVERY DETAILS'),AT(156,8563),USE(?String73),TRN,FONT(,8,,FONT:bold)
                         STRING('CHARGE DETAILS'),AT(5365,8563),USE(?String74),TRN,FONT(,8,,FONT:bold)
                         STRING('REPAIR DETAILS'),AT(156,3667),USE(?String50),TRN,FONT(,8,,FONT:bold)
                         BOX,AT(5000,365,2646,1042),USE(?Box:TopDetails),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H),LINEWIDTH(1)
                         BOX,AT(104,1667,3604,1302),USE(?Box:Address1),ROUND,COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1)
                         BOX,AT(4063,1667,3604,1302),USE(?Box:Address2),ROUND,COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1)
                         BOX,AT(104,3125,1510,260),USE(?Box:Title1),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H),LINEWIDTH(1)
                         BOX,AT(1615,3125,1510,260),USE(?Box:Title2),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H),LINEWIDTH(1)
                         BOX,AT(3125,3125,1510,260),USE(?Box:Title3),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H),LINEWIDTH(1)
                         BOX,AT(4635,3125,1510,260),USE(?Box:Title4),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H),LINEWIDTH(1)
                         BOX,AT(6146,3125,1510,260),USE(?Box:Title5),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H),LINEWIDTH(1)
                         BOX,AT(104,3385,1510,260),USE(?Box:Title1a),ROUND,COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1)
                         BOX,AT(1615,3385,1510,260),USE(?Box:Title2a),ROUND,COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1)
                         BOX,AT(3125,3385,1510,260),USE(?Box:Title3a),ROUND,COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1)
                         BOX,AT(4635,3385,1510,260),USE(?Box:Title4a),ROUND,COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1)
                         BOX,AT(6146,3385,1510,260),USE(?Box:Title5a),ROUND,COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1)
                         BOX,AT(104,3802,7552,260),USE(?Box:Heading),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H),LINEWIDTH(1)
                         BOX,AT(104,4063,7552,4479),USE(?Box:Detail),ROUND,COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1)
                         BOX,AT(104,8698,2344,1042),USE(?Box:Total1),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H),LINEWIDTH(1)
                         BOX,AT(5313,8698,2344,1042),USE(?Box:Total2),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H),LINEWIDTH(1)
                         STRING('Account Number'),AT(156,3156,1042,167),USE(?String91:2),TRN,FONT(,8,,FONT:bold)
                         STRING('Order Number'),AT(1677,3156,1042,167),USE(?String91:3),TRN,FONT(,8,,FONT:bold)
                         STRING('Authority Number'),AT(3187,3156,1177,167),USE(?String91:4),TRN,FONT(,8,,FONT:bold)
                         STRING('Chargeable Type'),AT(4698,3156,1042,167),USE(?strChargeableType),TRN,FONT(,8,,FONT:bold)
                         STRING('Chargeable Repair Type'),AT(6208,3156,1437,),USE(?strRepairType),TRN,FONT(,8,,FONT:bold)
                         STRING('Model'),AT(156,3833,1042,167),USE(?String91:7),TRN,FONT(,8,,FONT:bold)
                         STRING('Make'),AT(1562,3833,1042,167),USE(?String91:8),TRN,FONT(,8,,FONT:bold)
                         STRING('Unit Type'),AT(2958,3833,1042,167),USE(?String91:9),TRN,FONT(,8,,FONT:bold)
                         STRING('I.M.E.I. Number'),AT(4115,3833,1042,167),USE(?strIMEINumber),TRN,FONT(,8,,FONT:bold)
                         STRING('Exch IMEI No'),AT(5219,3833,1042,167),USE(?strExchangeIMEINumber),TRN,HIDE,FONT(,8,,FONT:bold)
                         STRING('M.S.N.'),AT(6479,3833,1042,167),USE(?String91:12),TRN,FONT(,8,,FONT:bold)
                       END
                     END
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeNoRecords          PROCEDURE(),DERIVED                 ! Method added to host embed code
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

ProgressMgr          StepLongClass                         ! Progress Manager
Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED                 ! Method added to host embed code
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('DespatchNote')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:DEFAULT2.Open                                     ! File DEFAULT2 used by this procedure, so make sure it's RelationManager is open
  Relate:DEFAULTS.Open                                     ! File DEFAULTS used by this procedure, so make sure it's RelationManager is open
  Relate:EXCHANGE.Open                                     ! File EXCHANGE used by this procedure, so make sure it's RelationManager is open
  Relate:INVOICE.SetOpenRelated()
  Relate:INVOICE.Open                                      ! File INVOICE used by this procedure, so make sure it's RelationManager is open
  Relate:STANTEXT.Open                                     ! File STANTEXT used by this procedure, so make sure it's RelationManager is open
  Relate:WEBJOB.Open                                       ! File WEBJOB used by this procedure, so make sure it's RelationManager is open
  Access:JOBTHIRD.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:LOAN.UseFile                                      ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBOUTFL.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBACC.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:USERS.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBSE2.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBSE.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:SUBTRACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:WARPARTS.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:PARTS.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBNOTES.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TRADEACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  locJobNumber = p_web.GSV('locJobNumber')
  
  SET(DEFAULT2)
  Access:DEFAULT2.Next()
  SET(DEFAULTS)
  Access:DEFAULTS.Next()
  
  Access:JOBS.ClearKey(job:Ref_Number_Key)
  job:Ref_Number = locJobNumber
  IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
  END
  
  Access:WEBJOB.ClearKey(wob:RefNumberKey)
  wob:RefNumber = job:Ref_Number
  IF (Access:WEBJOB.TryFetch(wob:RefNumberKey))
  END
  
  Access:JOBSE.ClearKey(jobe:RefNumberKey)
  jobe:RefNumber = job:Ref_Number
  IF (Access:JOBSE.TryFetch(jobe:RefNumberKey))
  END
  
  Access:JOBSE2.ClearKey(jobe2:RefNumberKey)
  jobe2:RefNumber = job:Ref_Number
  IF (Access:JOBSE2.TryFetch(jobe2:RefNumberKey))
  END
  
  Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
  jbn:RefNumber = job:Ref_Number
  IF (Access:JOBNOTES.TryFetch(jbn:RefNumberKey))
  END
  
  
  Access:TRADEACC.ClearKey(tra:Account_Number_Key)
  tra:Account_Number = wob:HeadAccountNumber
  IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key)) = Level:Benign
      delivery:Name = tra:Company_Name
      delivery:AddressLine1 = tra:Address_Line1
      delivery:AddressLine2 = tra:Address_Line2
      delivery:AddressLine3 = tra:Address_Line3
      delivery:AddressLine4 = tra:Postcode
      delivery:TelephoneNumber = tra:Telephone_Number
  END
  
  locFullJobNumber = job:Ref_Number & '-' & tra:BranchIdentification & wob:JobNumber
  
  Access:TRADEACC.Clearkey(tra:Account_Number_Key)
  If (p_web.GSV('BookingSite') = 'RRC')
      tra:Account_Number = p_web.GSV('wob:HeadAccountNumber')
  ELSE ! If (glo:WebJob = 1)
      tra:Account_Number = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
  END ! If (glo:WebJob = 1)
  If (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
      address:SiteName        = tra:Company_Name
      address:Name            = tra:coTradingName
      address:Name2           = tra:coTradingName2  ! #12079 New address fields. (Bryan: 13/04/2011)
      address:Location        = tra:coLocation
      address:RegistrationNo  = tra:coRegistrationNo
      address:AddressLine1    = tra:coAddressLine1
      address:AddressLine2    = tra:coAddressLine2
      address:AddressLine3    = tra:coAddressLine3
      address:AddressLine4    = tra:coAddressLine4
      address:Telephone       = tra:coTelephoneNumber
      address:Fax             = tra:coFaxNumber
      address:EmailAddress    = tra:coEmailAddress
      address:VatNumber       = tra:coVATNumber
  END
  
  
  IF (job:Warranty_Job = 'YES' AND job:Chargeable_Job <> 'YES')
      SETTARGET(Report)
      ?strChargeableType{PROP:Text} = 'Warranty Type'
      ?strRepairType{PROP:Text} = 'Warranty Repair Type'
      SETTARGET()
      locChargeType = job:Warranty_Charge_Type
      locRepairType = job:Repair_Type_Warranty
  ELSE
      locChargeType = job:Charge_Type
      locRepairType = job:Repair_Type
  END
  
  Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
  sub:Account_Number = job:Account_Number
  IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key))
  END
  
  Access:TRADEACC.ClearKey(tra:Account_Number_Key)
  tra:Account_Number = sub:Main_Account_Number
  IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
  END
  
  IF (tra:Price_Despatch <> 'YES' AND sub:PriceDespatchNotes <> 1)
      SETTARGET(Report)
      ?locUnitCost{PROP:Hide} = 1
      ?locLineCost{PROP:Hide} = 1
      ?locLabour{PROP:Hide} = 1
      ?locParts{PROP:Hide} = 1
      ?locCarriage{PROP:Hide} = 1
      ?locVAT{PROP:Hide} = 1
      ?locTotal{PROP:Hide} = 1
      ?strLabour{PROP:Hide} = 1
      ?strParts{PROP:Hide} = 1
      ?strCarriage{PROP:Hide} = 1
      ?strVAT{PROP:Hide} = 1
      ?strTotal{PROP:Hide} = 1
      SETTARGET()
  END
  
  hideDespatchAddress# = 0
  IF (tra:Invoice_Sub_Accounts = 'YES')
      IF (sub:HideDespAdd = 1)
          hideDespatchAddress# = 1
      END
      IF (sub:UseCustDespAdd = 'YES')
          invoice:AddressLine1 = job:Address_Line1
          invoice:AddressLine2 = job:Address_Line2
          invoice:AddressLine3 = job:Address_Line3
          invoice:AddressLine4 = job:Postcode
          invoice:Name = job:Company_Name
          invoice:TelephoneNumber = job:Telephone_Number
          invoice:FaxNumber = job:Fax_Number
  
      ELSE
          invoice:AddressLine1 = sub:Address_Line1
          invoice:AddressLine2 = sub:Address_Line2
          invoice:AddressLine3 = sub:Address_Line3
          invoice:AddressLine4 = sub:Postcode
          invoice:TelephoneNumber = sub:Telephone_Number
          invoice:Name = sub:Company_Name
          invoice:FaxNumber = sub:Fax_Number
      END
  
  ELSE
      IF (tra:HideDespAdd = 1)
          hideDespatchAddress# = 1
      END
      IF (tra:UseCustDespAdd = 'YES')
          invoice:AddressLine1 = job:Address_Line1
          invoice:AddressLine2 = job:Address_Line2
          invoice:AddressLine3 = job:Address_Line3
          invoice:AddressLine4 = job:Postcode
          invoice:Name = job:Company_Name
          invoice:TelephoneNumber = job:Telephone_Number
          invoice:FaxNumber = job:Fax_Number
      ELSE
          invoice:AddressLine1 = tra:Address_Line1
          invoice:AddressLine2 = tra:Address_Line2
          invoice:AddressLine3 = tra:Address_Line3
          invoice:AddressLine4 = tra:Postcode
          invoice:TelephoneNumber = tra:Telephone_Number
          invoice:Name = tra:Company_Name
          invoice:FaxNumber = tra:Fax_Number
      END
  END
  IF (hideDespatchAddress# = 1)
      SETTARGET(Report)
      ?delivery:Name{prop:Hide} = 1
      ?delivery:AddressLine1{PROP:Hide} = 1
      ?delivery:AddressLine2{PROP:Hide} = 1
      ?delivery:AddressLine3{PROP:Hide} = 1
      ?delivery:AddressLine4{PROP:Hide} = 1
      ?delivery:TelephoneNumber{prop:Hide} = 1
      SETTARGET()
  END
  
  locIDNumber = jobe2:IDNumber
  
  IF (job:Title = '')
      locCustomerName = job:Initial
  ELSIF (job:Initial = '')
      locCustomerName = job:Title
  ELSE
      locCustomerName = CLIP(job:Title) & ' ' & CLIP(job:Initial)
  END
  
  IF (locCustomerName = '')
      locCustomerName = job:Surname
  ELSIF(job:Surname = '')
  ELSE
      locCustomerName = CLIP(locCustomerName) & ' ' & CLIP(locCustomerName)
  END
  
  locEndUserTelNo = CLIP(jobe:EndUserTelNo)
  
  IF (locCustomerName <> '')
      locClientName = 'Client: ' & CLIP(locCustomerName) & '    Tel: ' & CLIP(locEndUserTelNo)
  ELSE
      IF (locEndUserTelNo <> '')
          locClientName = 'Tel: ' & CLIP(locEndUserTelNo)
      END
  END
  
  IF NOT (p_web.GSV('BookingSite') <> 'RRC' AND jobe:WebJob = 1)
      delivery:Name = job:Company_Name_Delivery
      delivery:AddressLine1 = job:Address_Line1_Delivery
      delivery:AddressLine2 = job:Address_Line2_Delivery
      delivery:AddressLine3 = job:Address_Line3_Delivery
      delivery:AddressLine4 = job:Postcode_Delivery
      delivery:TelephoneNumber = job:Telephone_Delivery
  END
  
  IF (job:Chargeable_Job = 'YES')
      IF (job:Invoice_Number <> '')
          Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
          inv:Invoice_Number = job:Invoice_Number
          IF (Access:INVOICE.TryFetch(inv:INvoice_Number_Key))
          END
  
          IF (p_web.GSV('BookingSite') = 'RRC')
              IF (inv:ExportedRRCOracle)
                  locLabour = jobe:InvRRCCLabourCost
                  locParts = jobe:InvRRCCPartsCost
                  locCarriage = job:Invoice_Courier_Cost
                  locVAT = (jobe:InvRRCCLabourCost * inv:Vat_Rate_Labour / 100) + |
                      (jobe:InvRRCCPartsCost * inv:Vat_Rate_Parts / 100) + |
                      (job:Invoice_Courier_Cost * inv:Vat_Rate_Labour / 100)
              ELSE
                  locLabour = jobe:RRCCLabourCost
                  locParts = jobe:RRCCPartsCost
                  locCarriage = job:Courier_Cost
                  locVAT = (jobe:RRCCLabourCost * GetVATRate(job:Account_Number, 'L') / 100) + |
                      (jobe:RRCCPartsCost * GetVATRate(job:Account_Number, 'P') / 100) + |
                      (job:Courier_Cost * GetVATRate(job:Account_Number, 'L') / 100)
              END
          ELSE
              locLabour = job:Invoice_Labour_Cost
              locParts = job:Invoice_Parts_Cost
              locCarriage = job:Invoice_Courier_Cost
              locVAT = (job:Invoice_Labour_Cost * inv:Vat_Rate_Labour / 100) + |
                  (job:Invoice_Parts_Cost * inv:Vat_Rate_Parts / 100) + |
                  (job:Invoice_Courier_Cost * inv:Vat_Rate_Labour / 100)
  
          END
  
      ELSE
          IF (p_web.GSV('BookingSite') = 'RRC')
              locLabour = jobe:RRCCLabourCost
              locParts = jobe:RRCCPartsCost
              locCarriage = job:Courier_Cost
              locVAT = (jobe:RRCCLabourCost * GetVATRate(job:Account_Number, 'L') / 100) + |
                  (jobe:RRCCPartsCost * GetVATRate(job:Account_Number, 'P') / 100) + |
                  (job:Courier_Cost * GetVATRate(job:Account_Number, 'L') / 100)
          ELSE
              locLabour = job:Labour_Cost
              locParts = job:Parts_Cost
              locCarriage = job:Courier_Cost
              locVAT = (job:Labour_Cost * GetVATRate(job:Account_Number, 'L') / 100) + |
                  (job:Parts_Cost * GetVATRate(job:Account_Number, 'P') / 100) + |
                  (job:Courier_Cost * GetVATRate(job:Account_Number, 'L') / 100)
          END
  
      END
  
  END
  
  IF (job:Warranty_Job = 'YES')
      IF (job:Invoice_Number_Warranty <> '')
          Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
          inv:Invoice_Number = job:Invoice_Number_Warranty
          IF (Access:INVOICE.TryFetch(inv:Invoice_NUmber_Key))
          END
  
          IF (p_web.GSV('BookingSite') = 'RRC')
              locLabour       = jobe:InvRRCWLabourCost
              locParts        = jobe:InvRRCWPartsCost
              locCarriage = job:WInvoice_Courier_Cost
              locVAT          = (jobe:InvRRCWLabourCost * inv:Vat_Rate_Labour / 100) + |
                  (jobe:InvRRCWPartsCost * inv:Vat_Rate_Parts / 100) + |
                  (job:WInvoice_Courier_Cost * inv:Vat_Rate_Labour / 100)
          ELSE
              locLabour       = jobe:InvoiceClaimValue
              locParts        = job_ali:Winvoice_parts_cost
              locCarriage = job_ali:Winvoice_courier_cost
              locVAT          = (jobe:InvoiceClaimValue * inv:Vat_Rate_Labour / 100) + |
                  (job:WInvoice_Parts_Cost * inv:Vat_Rate_Parts / 100) +                 |
                  (job:WInvoice_Courier_Cost * inv:Vat_Rate_Labour / 100)
          END
  
      ELSE
          IF (p_web.GSV('BookingSite') = 'RRC')
              locLabour       = jobe:RRCWLabourCost
              locParts        = jobe:RRCWPartsCost
              locCarriage = job_ali:Courier_Cost_Warranty
              locVAT          = (jobe:RRCWLabourCost * GetVATRate(job:Account_Number, 'L') / 100) + |
                  (jobe:RRCWPartsCost * GetVATRate(job:Account_Number, 'P') / 100) + |
                  (job:Courier_Cost_Warranty * GetVATRate(job:Account_Number, 'L') / 100)
  
          ELSE
              locLabour       = jobe:ClaimValue
              locParts        = job_ali:Parts_Cost_Warranty
              locCarriage = job_ali:Courier_Cost_Warranty
              locVAT          = (jobe:ClaimValue * GetVATRate(job:Account_Number, 'L') / 100) + |
                  (job:Parts_Cost_Warranty * GetVATRate(job:Account_Number, 'P') / 100) +         |
                  (job:Courier_Cost_Warranty * GetVATRate(job:Account_Number, 'L') / 100)
  
          END
  
      END
  
  
  END
  
  locTotal = locLabour + locParts + locCarriage + locVAT
  
  Access:USERS.ClearKey(use:User_Code_Key)
  use:User_Code = job:Despatch_User
  IF (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
      locDespatchUser = CLIP(use:Forename) & ' ' & CLIP(use:Surname)
  END
  
  
  
  
  
  
  ! Accessories & Engineer Report
  locAccessories = ''
  Access:JOBACC.ClearKey(jac:Ref_Number_Key)
  jac:Ref_Number = job:Ref_Number
  SET(jac:Ref_Number_Key,jac:Ref_Number_Key)
  LOOP UNTIL Access:JOBACC.Next()
      IF (jac:Ref_Number <> job:Ref_Number)
          BREAK
      END
  
      IF (locAccessories = '')
          locAccessories = jac:Accessory
      ELSE
          locAccessories = CLIP(locAccessories) & ', ' & CLIP(jac:Accessory)
      END
  END
  
  FREE(qOutfaults)
  Access:JOBOUTFL.ClearKey(joo:LevelKey)
  joo:JobNumber = job:Ref_Number
  SET(joo:LevelKey,joo:LevelKey)
  LOOP UNTIL Access:JOBOUTFL.Next()
      IF (joo:JobNumber <> job:Ref_Number)
          BREAK
      END
      qOutfaults.Description = joo:Description
      GET(qOutfaults,qOutfaults.Description)
      IF (ERROR())
          locEngineerReport = CLIP(locEngineerReport) & ' ' & CLIP(joo:Description)
          qOutFaults.Description = joo:Description
          Add(qOutFaults)
      END
  END
  FREE(qOutfaults)
  
  
  
  ! Exchange Note
  
  exchangeNote# = 0
  IF (job:Exchange_Unit_Number <> '')
      exchangeNote# = 1
  END
  
  ! Check Parts For "Exchange Part"
  Access:PARTS.ClearKey(par:Part_Number_Key)
  par:Ref_Number = job:Ref_Number
  SET(par:Part_Number_Key,par:Part_Number_Key)
  LOOP UNTIL Access:PARTS.Next()
      IF (par:Ref_Number <> job:Ref_Number)
          BREAK
      END
  
      Access:STOCK.ClearKey(sto:Ref_Number_Key)
      sto:Ref_Number = par:Part_Ref_Number
      IF (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
          IF (sto:ExchangeUnit = 'YES')
              exchangeNote# = 1
              BREAK
          END
      END
  END
  
  IF (exchangeNote# = 0)
      ! Check Warranty Parts for "Exchange Part"
      Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
      wpr:Ref_Number = job:Ref_Number
      SET(wpr:Part_Number_Key,wpr:Part_Number_Key)
      LOOP UNTIL Access:WARPARTS.next()
          IF (wpr:Ref_Number <> job:Ref_Number)
              BREAK
          END
          Access:STOCK.ClearKey(sto:Ref_Number_Key)
          sto:Ref_Number = par:Part_Ref_Number
          IF (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
              IF (sto:ExchangeUnit = 'YES')
                  exchangeNote# = 1
                  BREAK
              END
          END
      END
  END
  
  IF (exchangeNote# = 1)
      ! Do not put exchange unit repair on the
      ! Despatch Note Heading if it's a failed assessment
      IF (jobe:WebJob = 1 AND p_web.GSV('BookingSite') <> 'RRC' AND jobe:Engineer48HourOption = 1)
          IF (PassExchangeAssessment(job:Ref_Number) = 0)
              exchangeNote# = 0
          END
      END
  
  END
  
  ! #12084 New standard texts (Bryan: 17/05/2011)
  Access:STANTEXT.Clearkey(stt:Description_Key)
  stt:Description = 'DESPATCH NOTE'
  IF (Access:STANTEXT.TryFetch(stt:Description_Key))
  END
  
  IF (job:Loan_Unit_Number <> '')
      SETTARGET(Report)
      ?strExchangeUnit{PROP:Text} = 'LOAN UNIT'
      IF (p_web.GSV('BookingSite') = 'RRC' AND jobe:DespatchType = 'LOA') OR |
                      (p_web.GSV('BookingSite') <> 'RRC' AND job:Despatch_Type = 'LOA')
          locIDNumber = jobe2:LoanIDNumber
          ?strTitle{PROP:Text} = 'LOAN DESPATCH NOTE'
      END
      ?strExchangeUnit{PROP:Hide} = 0
      ?locLoanExchangeUnit{PROP:Hide} = 0
      Access:LOAN.ClearKey(loa:Ref_Number_Key)
      loa:Ref_Number = job:Loan_Unit_Number
      IF (Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign)
          locLoanExchangeUnit = '(' & loa:Ref_Number & ') ' & CLIP(loa:Manufacturer) & |
                          ' ' & CLIP(loa:Model_Number) & ' - ' & CLIP(loa:ESN)
      END
      SETTARGET()
      ! #12084 New standard texts (Bryan: 17/05/2011)
      Access:STANTEXT.Clearkey(stt:Description_Key)
      stt:Description = 'LOAN DESPATCH NOTE'
      IF (Access:STANTEXT.TryFetch(stt:Description_Key))
      END
  END
  
  locOriginalIMEI = job:ESN
  
  ! Send To THird Party?
  IF (job:Third_Party_Site <> '')
      IMEIError# = 0
      Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
      jot:RefNumber = job:Ref_Number
      SET(jot:RefNumberKey,jot:RefNumberKey)
      IF (Access:JOBTHIRD.Next())
          IMEIError# = 1
      ELSE
          IF (jot:RefNumber <> job:Ref_Number)
              IMEIError# = 1
          ELSE
              IF (jot:OriginalIMEI <> job:ESN)
                  IMEIError# = 0
              ELSE
                  IMEIError# = 1
              END
          END
      END
  ELSE
      IMEIError# = 1
  END
  
  IF (IMEIError# = 1)
      locOriginalIMEI = job:ESN
  ELSE
      SETTARGET(Report)
      locOriginalIMEI = jot:OriginalIMEI
      locFinalIMEI = job:ESN
      ?strIMEINumber{PROP:Text} = 'Orig I.M.E.I. Number'
      ?strExchangeIMEINumber{PROP:Hide} = 0
      ?locFinalIMEI{PROP:Hide} = 0
      SETTARGET()
  END
  
  
  IF (exchangeNote# = 1)
      SETTARGET(Report)
      ?strTitle{prop:Text} = 'EXCHANGE DESPATCH NOTE'
      ?locLoanExchangeUnit{PROP:Hide} = 0
      ?strExchangeUnit{PROP:Hide} = 0
      ?locPartNumber{PROP:Hide} = 1
      ?locDescription{PROP:Hide} = 1
      ?locQuantity{PROP:Hide} = 1
      ?locUnitCost{prop:Hide} = 1
      ?locLineCost{PROP:Hide} = 1
  
      Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
      xch:Ref_Number = job:Exchange_Unit_Number
      IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
          locLoanExchangeUnit = '(' & Clip(xch:Ref_number) & ') ' & CLip(xch:manufacturer) & ' ' & CLip(xch:model_number) & |
                          ' - ' & CLip(xch:esn)
          locOriginalIMEI = job:ESN
          locFinalIMEI = xch:ESN
          ?locFinalIMEI{PROP:Hide} = 0
          ?strExchangeIMEINumber{PROP:Hide} = 0
      END
      SETTARGET()
  END
  
  
  
  
  
  
  ! Terms
  locTerms = ''
  Access:STANTEXT.ClearKey(stt:Description_Key)
  stt:Description = 'DESPATCH NOTE'
  IF (Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign)
      locTerms = stt:Text
  END
  
  IF (job:Loan_Unit_Number <> '' AND job:Exchange_Unit_Number = 0)
      SETTARGET(Report)
      ?locLoanReplacementValue{PROP:Hide} = 0
      ?strLoanReplacementValue{PROP:Hide} = 0
      locLoanReplacementValue = jobe:LoanReplacementValue
      ?strLoanTerms{PROP:Hide} = 0
      !?strTerms1{PROP:Hide} = 0
      ?strTerms2{PROP:Hide} = 0
      ?strTerms3{PROP:Hide} = 0
      ?lineTerms{PROP:Hide} = 0
      SETTARGET()
      locTerms = '1) The loan phone and its accessories remain the property of Vodacom Service Provider Company and is given to the customer to use while his / her phone is being repaired. It is the customers responsibility to return the loan phone and its accessories in proper working condition.<13,10>'
      locTerms = Clip(locTerms) & '2) In the event of damage to the loan phone and/or its accessories, the customer will be liable for any costs incurred to repair or replace the loan phone and/or its accessories. <13,10>'
      locTerms = Clip(locTerms) & '3) In the event of loss or theft of the loan phone and/or its accessories the customer will be liable for the replacement cost of the loan phone and/or its accessories or he/she may replace the lost/stolen phone and/or its accessories with new ones of the same or similar make and model, or same replacement value.<13,10>'
      locTerms = Clip(locTerms) & '4) Any phone replaced by the customer must be able to operate on the Vodacom network. <13,10>'
      locTerms = Clip(locTerms) & '5) The customers repaired phone will not be returned until the loan phone and its accessories has been returned, repaired or replaced. '
  
  END
  locTermsText = p_web.GSV('Default:TermsText')
  ! Barcode Bit
  locBarCodeJobNumber = '*' & job:Ref_Number & '*'
  locBarCodeIMEINumber = '*' & job:ESN & '*'
  locDisplayIMEINumber = 'I.M.E.I. No: ' & job:ESN
  locDisplayJobNumber = 'Job No: ' & job:Ref_Number
  Do DefineListboxStyle
  INIMgr.Fetch('DespatchNote',ProgressWindow)              ! Restore window settings from non-volatile store
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.SetReportTarget(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ProgressWindow{Prop:Timer} = 10                          ! Assign timer interval
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:JOBS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, job:Ref_Number)
  ThisReport.AddSortOrder(job:Ref_Number_Key)
  ThisReport.AddRange(job:Ref_Number,locJobNumber)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{Prop:Text}=''
  Relate:JOBS.SetQuickScan(1,Propagate:OneMany)
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom=True
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
      SELF.SetReportTarget(PDFReporter.IReportGenerator)
    self.SkipPreview = True
    ProgressWindow{prop:hide} = 1
    loc:PDFName = clip(p_web.site.WebFolderPath) & '\reports\$$$' & format(random(1,99999),@n05) &'.pdf'
  End
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:EXCHANGE.Close
    Relate:INVOICE.Close
    Relate:STANTEXT.Close
    Relate:WEBJOB.Close
  END
  IF SELF.Opened
    INIMgr.Update('DespatchNote',ProgressWindow)           ! Save window data to non-volatile store
  END
  ProgressMgr.Kill()
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      Loc:html = '<script type="text/javascript">alert('''&clip('No Records')&''');top.close();</script>'
      p_web.ParseHTML(loc:html)
    Else
      p_web.ForceNoCache = 1
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(loc:PDFName)
    End
  End
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text}=FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
    If Not p_Web &= NULL
      loc:NoRecords = 1
      Return
    End
  PARENT.TakeNoRecords


ThisWindow.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeRecord()
    If Not p_web &= Null
      p_web.NoOp()
    End
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  countParts# = 0
  IF (job:Warranty_Job = 'YES')
      Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
      wpr:Ref_Number = job:Ref_Number
      SET(wpr:Part_Number_Key,wpr:Part_Number_Key)
      LOOP UNTIL Access:WARPARTS.Next()
          IF (wpr:Ref_Number <> job:Ref_Number)
              BREAK
          END
          locPartNumber = wpr:Part_Number
          locDescription = wpr:Description
          locQuantity = wpr:Quantity
          locUnitCost = wpr:Purchase_Cost
          locLineCost = wpr:Quantity * wpr:Purchase_Cost
          Print(rpt:Detail)
          countParts# += 1
      END
  END
  
  IF (countParts# = 0)
      locPartNumber = ''
      Print(rpt:Detail)
  END
  ReturnValue = PARENT.TakeRecord()
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','WebServer_Phase4','DespatchNote','DespatchNote','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.CompressText   = True
  SELF.CompressImages = True
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    SELF.SetFileName(clip(loc:PDFName))
  End

