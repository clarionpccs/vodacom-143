

   MEMBER('WebServer_Phase4.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSE079.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSE005.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE012.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE013.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE015.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE058.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE059.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE083.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE099.INC'),ONCE        !Req'd for module callout resolution
                     END


LoginForm            PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
Loc:Login            STRING(20)                            !
Loc:Password         STRING(20)                            !
Loc:Remember         LONG                                  !
loc:BranchID         STRING(30)                            !
tmp:LicenseText      STRING(10000)                         !
locNewPassword       STRING(30)                            !
locConfirmNewPassword STRING(30)                           !
FilesOpened     Long
USERS_ALIAS::State  USHORT
SUBTRACC::State  USHORT
TRADEACC::State  USHORT
USERS::State  USHORT
licence:IsInvalid  Long
loc:BranchID:IsInvalid  Long
Loc:Password:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  GlobalErrors.SetProcedureName('LoginForm')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'LoginForm_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('LoginForm','')
    p_web.DivHeader('LoginForm',p_web.combine(p_web.site.style.formdiv,))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('LoginForm') = 0
        p_web.AddPreCall('LoginForm')
        p_web.DivHeader('popup_LoginForm','nt-hidden')
        p_web.DivHeader('LoginForm',p_web.combine(p_web.site.style.formdiv,))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_LoginForm_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_LoginForm_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferLoginForm',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_LoginForm',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferLoginForm',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_LoginForm',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferLoginForm',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_LoginForm',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_LoginForm',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferLoginForm',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_LoginForm',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferLoginForm',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_LoginForm',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  p_web._MessageDone = 1 ! suppress any messages on this form.
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_LoginForm',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('LoginForm')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(USERS_ALIAS)
  p_web._OpenFile(SUBTRACC)
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(USERS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(USERS_ALIAS)
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(USERS)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('LoginForm_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'LoginForm'
    end
    p_web.formsettings.proc = 'LoginForm'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine

AfterLookup Routine
  loc:TabNumber = -1
  If false
    loc:TabNumber += 1
  End
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  if p_web.IfExistsValue('tmp:LicenseText') = 0
    p_web.SetSessionValue('tmp:LicenseText',tmp:LicenseText)
  Else
    tmp:LicenseText = p_web.GetSessionValue('tmp:LicenseText')
  End
  if p_web.IfExistsValue('loc:BranchID') = 0
    p_web.SetSessionValue('loc:BranchID',loc:BranchID)
  Else
    loc:BranchID = p_web.GetSessionValue('loc:BranchID')
  End
  if p_web.IfExistsValue('Loc:Password') = 0
    p_web.SetSessionValue('Loc:Password',Loc:Password)
  Else
    Loc:Password = p_web.GetSessionValue('Loc:Password')
  End

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('tmp:LicenseText')
    tmp:LicenseText = p_web.GetValue('tmp:LicenseText')
    p_web.SetSessionValue('tmp:LicenseText',tmp:LicenseText)
  Else
    tmp:LicenseText = p_web.GetSessionValue('tmp:LicenseText')
  End
  if p_web.IfExistsValue('loc:BranchID')
    loc:BranchID = p_web.GetValue('loc:BranchID')
    p_web.SetSessionValue('loc:BranchID',loc:BranchID)
  Else
    loc:BranchID = p_web.GetSessionValue('loc:BranchID')
  End
  if p_web.IfExistsValue('Loc:Password')
    Loc:Password = p_web.GetValue('Loc:Password')
    p_web.SetSessionValue('Loc:Password',Loc:Password)
  Else
    Loc:Password = p_web.GetSessionValue('Loc:Password')
  End

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('LoginForm_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'IndexPage'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('LoginForm_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('LoginForm_ChainTo')
    loc:formaction = p_web.GetSessionValue('LoginForm_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'LoginForm'

GenerateForm   Routine
  do LoadRelatedRecords
  p_web.site.SaveButton.TextValue = 'Login'
  p_web.site.CancelButton.TextValue = 'Reset'
  do RestoreMem
  p_web.SSV('NewPasswordRequired','')
  p_web.SSV('UserMobileRequired','')
  p_web.SSV('locCurrentUser','')
  p_web.SSV('loc:Password','')
  p_web.SSV('loc:BranchID','')
  p_web.SSV('BookingName','')
  p_web.SSV('BookingAccount','')
  
  
  ClearJobVariables(p_web)
  
  p_web.SetSessionValue('VersionNumber',glo:VersionNumber)
  
  ! Save the phase number to show/hide features.
  IF INSTRING('PH3',UPPER(glo:VersionNumber),1,1)
      p_web.SSV('PH',3)
  ELSIF INSTRING('PH2',UPPER(glo:VersionNumber),1,1)
      p_web.SSV('PH',2)
  ELSE
      p_web.SSV('PH',4)
  END
  
  
  p_web.SSV('tmp:LicenseText','<b>SOFTWARE LICENCE AGREEMENT</b>' & |
      '<br>' & |
      'Copying, duplicating or otherwise distributing any part of this product without the' & |
      'prior               written consent of an authorised representative of the above is prohibited.' & |
      'This                Software is licensed subject to the following conditions.' & |
      '<br>' & |
      '<b>LICENCE AGREEMENT.</b> This is a legal agreement between you (either an' & |
      'individual          or an entity) and the manufacturer of the computer software PC' & |
      'Control             Systems Ltd..' & |
      'If you operate this SOFTWARE you are agreeing to be bound by the terms of' & |
      'this        agreement. If you do not agree to the terms of this agreement, promptly' & |
      '    return the SOFTWARE and the accompanying items (including written materials' & |
      '    and binders or other containers) to the place you obtained them from.' & |
      'Discretionary       compensation may be paid dependent upon the time elapsed since' & |
      'the                 purchase date of the SOFTWARE in all events any form of compensation will' & |
      'be                  at the sole discretion of and expressly set by PC Control Systems Ltd..' & |
      '<br>' & |
      '<b>GRANT OF LICENCE.</b> This Licence Agreement permits you to use one copy' & |
      'of the enclosed software program ( the "SOFTWARE") on a single computer' & |
      'if you purchased a single user version or on a network of computers if you' & |
      'purchased   a network user version. The SOFTWARE is in "use" on a computer' & |
      'when        it is loaded into temporary memory (i.e. RAM) or installed into permanent' & |
      '    memory (i.e. hard disk, CD-ROM, or other storage device) of that computer.' & |
      'Network             users are limited to the number of simultaneous accesses to the' & |
      'SOFTWARE            by the network. This is dependent upon the user version' & |
      'purchased.          The user version is clearly stated on the original SOFTWARE' & |
      'purchase            invoice.' & |
      '<br>' & |
      '<b>COPYRIGHT:</b> The enclosed SOFTWARE and Documentation is protected by' & |
      'copyright           laws and international treaty provisions and are the proprietary' & |
      'products        of PC Control Systems Ltd. and its third party suppliers from whom' & |
      'PC                  Control Systems Ltd. has licensed portions of the Software. Such suppliers' & |
      'are                 expressly understood to be beneficiaries of the terms and provisions of' & |
      'this                Agreement. All rights that are not expressly granted are reserved by' & |
      'PC                  Control Systems Ltd. or its suppliers. You must treat the SOFTWARE like' & |
      'any                 other copyrighted material (e.g. a book or musical recording) except that' & |
      'you                 may either (a) make one copy of the SOFTWARE solely for backup' & |
      'or archival purposes, or (b) transfer the SOFTWARE to a single hard disk' & |
      'provided            you keep the original solely for backup or archival purposes.' & |
      'You                 may not copy the written materials accompanying the SOFTWARE.' & |
      '<br>' & |
      '<b>DUEL MEDIA SOFTWARE.</b> If the SOFTWARE package contains both 3.5"' & |
      'and 5.25" or 3.5" and CD ROM disks, then you may use only the disks' & |
      'appropriate         for your single-user computer or your file server. You may not use' & |
      'the                 other disks on any other computer or loan, rent, lease, or transfer them to' & |
      'another             user except as part of the permanent transfer (as provided above) of all' & |
      'SOFTWARE            and written materials.' & |
      '<br>' & |
      '<b>LIMITED WARRANTY.</b> PC Control Systems Ltd. warrants that (a) the' & |
      'SOFTWARE            will perform substantially in accordance with the accompanying' & |
      'written             materials for a period of ninety (90) days from the date of receipt,' & |
      'and (b) any hardware accompanying the SOFTWARE will be free of defects in' & |
      'materials           and workmanship under normal use and service for a period of one' & |
      '(1) year from the date of receipt. Any implied warranties on the' & |
      'SOFTWARE            and hardware are limited to ninety (90) days and one' & |
      '(1) year, respectively. Some jurisdictions do not allow limitations' & |
      'duration        of an implied warranty, so the above limitation may not' & |
      'apply               to you.' & |
      '<br>' & |
      '<b>CUSTOMER REMEDIES.</b> PC Control Systems Ltd. and its suppliers entire' & |
      'liability           and your exclusive remedy shall be, at PC Control Systems Ltd.' & |
      'option, either (a) return of the price paid , or (b) repair or replacement of' & |
      'the SOFTWARE that does not meet PC Control Systems Ltd. Warranty and which' & |
      'is  returned to PC Control Systems Ltd. with a copy of your SOFTWARE' & |
      'purchase    invoice. This Limited Warranty is void if failure of the SOFTWARE' & |
      'has resulted from accident, abuse, or misapplication. Any replacement' & |
      'SOFTWARE    will be warranted for the remainder of the original warranty period' & |
      '    or thirty (30) days, whichever is the longer.' & |
      '<br>' & |
      '<b>NO OTHER WARRANTIES.</b> To the maximum extent permitted by applicable' & |
      'law, PC Control Systems Ltd. and its suppliers disclaim all other warranties,' & |
      'either              express or implied, including, but not limited to implied warranties of' & |
      'merchantability     and fitness for a particular purpose, with regard to the' & |
      'SOFTWARE, the accompanying written materials, and any accompanying' & |
      'hardware.           This limited warranty gives you specific legal rights, and you may' & |
      'also                have other rights which vary from jurisdiction to jurisdiction.' & |
      '<br>' & |
      '<b>NO LIABILITY FOR CONSEQUENTIAL DAMAGES.</b> To the maximum extent permitted' & |
      'by applicable law, in no event shall PC Control Systems Ltd. or its suppliers' & |
      'be                  liable for any damage whatsoever ( including without limitation,' & |
      'damages             for loss of business profits, business interruption, loss of business' & |
      'information, or any other pecuniary loss) arising out of the use of or' & |
      'inability           to use this product, even if PC Control Systems Ltd. has been advised' & |
      'of the possibility of such damages. Because some jurisdictions do not allow the' & |
      'exclusion           or limitation of liability for consequential or incidental damages, the' & |
      'above               limitation may not apply to you.' & |
      '<br>' & |
      '<b>SUPPORT.</b> PC Control Systems Ltd. will attempt to answer your technical' & |
      'support             requests concerning the SOFTWARE; however, this service is offered' & |
      'on                  a reasonable efforts basis only, and PC Control Systems Ltd. may not be' & |
      'able                to resolve every support request. PC Control Systems Ltd. supports' & |
      'the                 Software only if it is used under conditions and on operating systems' & |
      'for                 which the Software is designed.' & |
      '<br>' & |
      'By Logging in you are agreeing to the above agreement.')
  
  !p_web.SetSessionValue('tmp:LicenseText','<body style="color: rgb(0, 0, 0); background-color: rgb(255, 255, 255);" alink="#000099" link="#000099" vlink="#990099">' & |
  !'<div style="text-align: center;"><small><span style="font-family: Tahoma; font-weight: bold; font-size: 12px;">SOFTWARE LICENCE AGREEMENT' & |
  !'</span><br style="font-family: Tahoma;"><small><span style="font-family: Tahoma;">Copying, duplicating or otherwise ' & |
  !'distributing any part of this product without the</span><br style="font-family: Tahoma;"></small><small>' & |
  !'<span style="font-family: Tahoma;">prior written consent of an authorised representative of the above is ' & |
  !'prohibited.</span><br style="font-family: Tahoma;"></small><small><span style="font-family: Tahoma;">' & |
  !'This Software is licensed subject to the following conditions.</span><br style="font-family: Tahoma;">' & |
  !'</small><br style="font-family: Tahoma;"><small><span style="font-family: Tahoma;"><span style=' & |
  !'"font-weight: bold;">LICENCE AGREEMENT.</span> This is a legal agreement between you ' & |
  !'(either an</span><br style="font-family: Tahoma;"></small><small><span style="font-family: Tahoma;">' & |
  !'individual or an entity) and the manufacturer of the computer software&nbsp; PC</span><br style="font-family: ' & |
  !'Tahoma;"></small><small><span style="font-family: Tahoma;">Control Systems Ltd..</span><br style=' & |
  !'"font-family: Tahoma;"></small><small><span style="font-family: Tahoma;">If you operate this ' & |
  !'SOFTWARE&nbsp; you are agreeing to be bound by the terms of</span><br style="font-family: Tahoma;">' & |
  !'</small><small><span style="font-family: Tahoma;">this agreement. If you do not agree to the ' & |
  !'terms of this agreement, promptly</span><br style="font-family: Tahoma;"></small><small><span style=' & |
  !'"font-family: Tahoma;">return the SOFTWARE and the accompanying items (including written materials' & |
  !'</span><br style="font-family: Tahoma;"></small><small><span style="font-family: Tahoma;">' & |
  !'and binders or other containers) to the place you obtained them from.</span><br style=' & |
  !'"font-family: Tahoma;"></small><small><span style="font-family: Tahoma;">Discretionary ' & |
  !'compensation may be paid dependent upon the time elapsed since</span><br style=' & |
  !'"font-family: Tahoma;"></small><small><span style="font-family: Tahoma;">the purchase ' & |
  !'date of the SOFTWARE in all events any form of compensation will</span><br style=' & |
  !'"font-family: Tahoma;"></small><small><span style="font-family: Tahoma;">be at the ' & |
  !'sole discretion of and expressly set by PC Control Systems Ltd..</span><br style=' & |
  !'"font-family: Tahoma;"></small><br style="font-family: Tahoma;"><small><span style=' & |
  !'"font-family: Tahoma;"><span style="font-weight: bold;">GRANT OF LICENCE.</span>&nbsp; ' & |
  !'This Licence Agreement permits you to use one copy</span><br style="font-family: Tahoma;"' & |
  !'></small><small><span style="font-family: Tahoma;">of the enclosed&nbsp; software ' & |
  !'program ( the "SOFTWARE") on a single computer</span><br style="font-family: Tahoma;">' & |
  !'</small><small><span style="font-family: Tahoma;">if you purchased a single user version or ' & |
  !'on a network of computers if you</span><br style="font-family: Tahoma;"></small><small><span ' & |
  !'style="font-family: Tahoma;">purchased a network user version. The SOFTWARE is in "use" on a ' & |
  !'computer</span><br style="font-family: Tahoma;"></small><small><span style="font-family: ' & |
  !'Tahoma;">when it is loaded into temporary memory (i.e. RAM) or installed into permanent</span><br ' & |
  !'style="font-family: Tahoma;"></small><small><span style="font-family: Tahoma;">memory (i.e. hard disk, ' & |
  !'CD-ROM, or other&nbsp; storage device) of that computer.</span><br style="font-family: Tahoma;"></small>' & |
  !'<small><span style="font-family: Tahoma;">Network users are limited to the number of&nbsp; simultaneous ' & |
  !'accesses to the</span><br style="font-family: Tahoma;"></small><small><span style="font-family: Tahoma;">' & |
  !'SOFTWARE&nbsp; by the network. This is dependent upon the user version</span><br style="font-family: ' & |
  !'Tahoma;"></small><small><span style="font-family: Tahoma;">purchased.&nbsp; The user version is clearly ' & |
  !'stated on the original SOFTWARE</span><br style="font-family: Tahoma;"></small><small><span style=' & |
  !'"font-family: Tahoma;">purchase invoice.</span><br style="font-family: Tahoma;"></small><br style="font-family:' & |
  !' Tahoma;"><small><span style="font-family: Tahoma;"><span style="font-weight: bold;">COPYRIGHT:' & |
  !'</span>&nbsp; The enclosed SOFTWARE and Documentation is protected by</span><br style="font-family: ' & |
  !'Tahoma;"></small><small><span style="font-family: Tahoma;">copyright laws and international treaty ' & |
  !'provisions and are the proprietary</span><br style="font-family: Tahoma;"></small><small><span style=' & |
  !'"font-family: Tahoma;">products of PC Control Systems Ltd. and its third party suppliers from ' & |
  !'whom</span><br style="font-family: Tahoma;"></small><small><span style="font-family: Tahoma;">' & |
  !'PC Control Systems Ltd. has licensed&nbsp; portions of the Software.&nbsp; Such suppliers</span><br ' & |
  !'style="font-family: Tahoma;"></small><small><span style="font-family: Tahoma;">are expressly&nbsp; ' & |
  !'understood&nbsp; to be beneficiaries of the terms and provisions of</span><br style="font-family: ' & |
  !'Tahoma;"></small><small><span style="font-family: Tahoma;">this&nbsp; Agreement.&nbsp; All rights ' & |
  !'that are not expressly&nbsp; granted are reserved by</span><br style="font-family: Tahoma;"></small>' & |
  !'<small><span style="font-family: Tahoma;">PC Control Systems Ltd. or its suppliers. You must treat ' & |
  !'the SOFTWARE like</span><br style="font-family: Tahoma;"></small><small><span style="font-family: ' & |
  !'Tahoma;">any other copyrighted material (e.g. a book or musical recording) except that</span><br ' & |
  !'style="font-family: Tahoma;"></small><small><span style="font-family: Tahoma;">you may either (a) ' & |
  !'make one copy of the SOFTWARE solely for backup</span><br style="font-family: Tahoma;"></small>' & |
  !'<small><span style="font-family: Tahoma;">or archival purposes, or (b) transfer the SOFTWARE to a ' & |
  !'single hard disk</span><br style="font-family: Tahoma;"></small><small><span style="font-family: ' & |
  !'Tahoma;">provided you keep the original solely for backup or archival purposes.</span><br style=' & |
  !'"font-family: Tahoma;"></small><small><span style="font-family: Tahoma;">You may not copy the ' & |
  !'written materials accompanying the SOFTWARE.</span><br style="font-family: Tahoma;"></small>' & |
  !'<br style="font-family: Tahoma;"><small><span style="font-family: Tahoma;"><span style="font-' & |
  !'weight: bold;">DUEL MEDIA SOFTWARE .</span> If the SOFTWARE&nbsp; package contains both 3.5"' & |
  !'</span><br style="font-family: Tahoma;"></small><small><span style="font-family: Tahoma;">and ' & |
  !'5.25" or 3.5" and CD ROM disks, then you may use only the disks</span><br style="font-family: ' & |
  !'Tahoma;"></small><small><span style="font-family: Tahoma;">appropriate for your single-user ' & |
  !'computer or your file server.&nbsp; You may not use</span><br style="font-family: Tahoma;">' & |
  !'</small><small><span style="font-family: Tahoma;">the other disks on any other computer or ' & |
  !'loan, rent, lease, or transfer them to</span><br style="font-family: Tahoma;"></small><small>' & |
  !'<span style="font-family: Tahoma;">another user except as part of the permanent transfer (as provided above)' & |
  !' of all</span><br style="font-family: Tahoma;"></small><small><span style="font-family: ' & |
  !'Tahoma;">SOFTWARE and written materials.</span><br style="font-family: Tahoma;"></small><br style=' & |
  !'"font-family: Tahoma;"><small><span style="font-family: Tahoma;"><span style="font-weight: bold;"' & |
  !'>LIMITED WARRANTY.</span>&nbsp; PC Control Systems Ltd. warrants that (a) the</span><br sty' & |
  !'le="font-family: Tahoma;"></small><small><span style="font-family: Tahoma;">SOFTWARE will perf' & |
  !'orm substantially in accordance with the accompanying</span><br style="font-family: Tahoma;"></sma' & |
  !'ll><small><span style="font-family: Tahoma;">written materials for a period of ninety (90) day' & |
  !'s from the date of receipt,</span><br style="font-family: Tahoma;"></small><small><span style=' & |
  !'"font-family: Tahoma;">and (b) any hardware accompanying the SOFTWARE will be free of defects ' & |
  !'in</span><br style="font-family: Tahoma;"></small><small><span style="font-family: Tahoma;">ma' & |
  !'terials and workmanship under normal use and service for a period of one</span><br style="font-' & |
  !'family: Tahoma;"></small><small><span style="font-family: Tahoma;">(1) year from the date of rec' & |
  !'eipt. Any implied warranties on the</span><br style="font-family: Tahoma;"></small><small><span ' & |
  !'style="font-family: Tahoma;">SOFTWARE and hardware are limited to ninety (90) days and one</span' & |
  !'><br style="font-family: Tahoma;"></small><small><span style="font-family: Tahoma;">(1) year, re' & |
  !'spectively.&nbsp; Some jurisdictions do not allow limitations</span><br style="font-family: Taho' & |
  !'ma;"></small><small><span style="font-family: Tahoma;">duration of an implied warranty, so the a' & |
  !'bove limitation may not</span><br style="font-family: Tahoma;"></small><small><span style="font-' & |
  !'family: Tahoma;">apply to you.</span><br style="font-family: Tahoma;"></small><br style="font-fa' & |
  !'mily: Tahoma;"><small><span style="font-family: Tahoma;"><span style="font-weight: bold;">CUSTOM' & |
  !'ER REMEDIES.</span>&nbsp; PC Control Systems Ltd. and its suppliers entire</span><br style="font' & |
  !'-family: Tahoma;"></small><small><span style="font-family: Tahoma;">liability and your exclusive' & |
  !' remedy shall be, at PC Control Systems Ltd.</span><br style="font-family: Tahoma;"></small><sma' & |
  !'ll><span style="font-family: Tahoma;">option, either (a) return of the price paid , or (b) repai' & |
  !'r or replacement of</span><br style="font-family: Tahoma;"></small><small><span style="font-fami' & |
  !'ly: Tahoma;">the SOFTWARE that does not meet PC Control Systems Ltd. Warranty and which</span><b' & |
  !'r style="font-family: Tahoma;"></small><small><span style="font-family: Tahoma;">is returned to ' & |
  !'PC Control Systems Ltd. with a copy of your SOFTWARE</span><br style="font-family: Tahoma;"></s' & |
  !'mall><small><span style="font-family: Tahoma;">purchase invoice.&nbsp; This Limited Warranty is' & |
  !' void if failure of the SOFTWARE</span><br style="font-family: Tahoma;"></small><small><span st' & |
  !'yle="font-family: Tahoma;">has resulted from accident, abuse, or misapplication.&nbsp; Any repl' & |
  !'acement</span><br style="font-family: Tahoma;"></small><small><span style="font-family: Tahoma;' & |
  !'">SOFTWARE will be warranted for the remainder of the original warranty period</span><br style=' & |
  !'"font-family: Tahoma;"></small><small><span style="font-family: Tahoma;">or thirty (30) days, w' & |
  !'hichever is the longer.</span><br style="font-family: Tahoma;"></small><br style="font-family: ' & |
  !'Tahoma;"><small><span style="font-family: Tahoma;"><span style="font-weight: bold;">NO OTHER WA' & |
  !'RRANTIES.&nbsp;</span> To the maximum extent permitted by applicable</span><br style="font-fami' & |
  !'ly: Tahoma;"></small><small><span style="font-family: Tahoma;">law, PC Control Systems Ltd. and' & |
  !' its suppliers disclaim all other warranties,</span><br style="font-family: Tahoma;"></small><s' & |
  !'mall><span style="font-family: Tahoma;">either&nbsp; express or implied, including, but not lim' & |
  !'ited to implied warranties of</span><br style="font-family: Tahoma;"></small><small><span style' & |
  !'="font-family: Tahoma;">merchantability and fitness for a particular purpose, with regard to th' & |
  !'e</span><br style="font-family: Tahoma;"></small><small><span style="font-family: Tahoma;">SOFT' & |
  !'WARE, the accompanying written materials, and any accompanying</span><br style="font-family: Ta' & |
  !'homa;"></small><small><span style="font-family: Tahoma;">hardware.&nbsp; This limited warranty ' & |
  !'gives you specific legal rights, and you may</span><br style="font-family: Tahoma;"></small><sm' & |
  !'all><span style="font-family: Tahoma;">also have other rights which vary from jurisdiction to j' & |
  !'urisdiction.</span><br style="font-family: Tahoma;"></small><br style="font-family: Tahoma;"><s' & |
  !'mall><span style="font-family: Tahoma;"><span style="font-weight: bold;">NO LIABILITY FOR CONSE' & |
  !'QUENTIAL DAMAGES.</span> To the maximum extent permitted</span><br style="font-family: Tahoma;"' & |
  !'></small><small><span style="font-family: Tahoma;">by applicable law, in no event shall PC Cont' & |
  !'rol Systems Ltd.&nbsp; or its suppliers</span><br style="font-family: Tahoma;"></small><small><' & |
  !'span style="font-family: Tahoma;">be liable for any damage&nbsp; whatsoever ( including without' & |
  !'&nbsp; limitation,</span><br style="font-family: Tahoma;"></small><small><span style="font-fami' & |
  !'ly: Tahoma;">damages for loss of business profits, business interruption, loss of business</spa' & |
  !'n><br style="font-family: Tahoma;"></small><small><span style="font-family: Tahoma;">informatio' & |
  !'n, or any other pecuniary loss)&nbsp; arising&nbsp; out of the use of or</span><br style="font-' & |
  !'family: Tahoma;"></small><small><span style="font-family: Tahoma;">inability to use this produc' & |
  !'t,&nbsp; even if PC Control Systems Ltd. has been advised</span><br style="font-family: Tahoma;' & |
  !'"></small><small><span style="font-family: Tahoma;">of the possibility&nbsp; of such damages.&n' & |
  !'bsp; Because some jurisdictions do not allow the</span><br style="font-family: Tahoma;"></small' & |
  !'><small><span style="font-family: Tahoma;">exclusion or limitation of liability for consequenti' & |
  !'al or incidental damages, the</span><br style="font-family: Tahoma;"></small><small><span style' & |
  !'="font-family: Tahoma;">above limitation may not apply to you.</span><br style="font-family: Ta' & |
  !'homa;"></small><br style="font-family: Tahoma;"><small><span style="font-family: Tahoma;"><span' & |
  !' style="font-weight: bold;">SUPPORT.</span>&nbsp;&nbsp; PC Control Systems Ltd.&nbsp; will atte' & |
  !'mpt to answer&nbsp; your technical</span><br style="font-family: Tahoma;"></small><small><span ' & |
  !'style="font-family: Tahoma;">support requests concerning the SOFTWARE; however, this service is' & |
  !' offered</span><br style="font-family: Tahoma;"></small><small><span style="font-family: Tahoma' & |
  !';">on a reasonable efforts basis only, and PC Control Systems Ltd. may not be</span><br style="' & |
  !'font-family: Tahoma;"></small><small><span style="font-family: Tahoma;">able to resolve every s' & |
  !'upport request.&nbsp; PC Control Systems Ltd. supports</span><br style="font-family: Tahoma;"><' & |
  !'/small><small><span style="font-family: Tahoma;">the Software only if it is used under conditio' & |
  !'ns and on operating systems</span><br style="font-family: Tahoma;"></small><small><span style="' & |
  !'font-family: Tahoma;">for which the Software is designed.<br><br>By Logging in you are agreeing' & |
  !' to the above agreement.</span></small></small></div></body>')
 tmp:LicenseText = p_web.RestoreValue('tmp:LicenseText')
 loc:BranchID = p_web.RestoreValue('loc:BranchID')
 Loc:Password = p_web.RestoreValue('Loc:Password')
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  loc:formaction = 'IndexPage'
  loc:formactiontarget = '_top'
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Login') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Login',0)&'</div>'&CRLF
  End
  do SendPacket
    do SendPacket
    Do License
    do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_LoginForm',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      If false
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_LoginForm0_div')&'">'&p_web.Translate('Licence Agreement')&'</a></li>'& CRLF
      End
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_LoginForm1_div')&'">'&p_web.Translate('ServiceBase Online Login')&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  do GenerateTab1
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="LoginForm_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="LoginForm_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'LoginForm_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="LoginForm_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'LoginForm_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    ElsIf false
        p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:LicenseText')
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.loc:BranchID')
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_LoginForm')>0,p_web.GSV('showtab_LoginForm'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_LoginForm'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_LoginForm') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_LoginForm'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_LoginForm')>0,p_web.GSV('showtab_LoginForm'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_LoginForm') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
      If false
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Licence Agreement') & ''''
      End
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('ServiceBase Online Login') & ''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_LoginForm_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_LoginForm')>0,p_web.GSV('showtab_LoginForm'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"LoginForm",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_LoginForm')>0,p_web.GSV('showtab_LoginForm'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_LoginForm_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('LoginForm') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('LoginForm')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
  If false
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Licence Agreement')&'</a></h3>' & CRLF & p_web.DivHeader('tab_LoginForm0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_LoginForm0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_LoginForm0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_LoginForm0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Licence Agreement')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_LoginForm0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Licence Agreement')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_LoginForm0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Licence Agreement')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_LoginForm0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::licence
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
  end
GenerateTab1  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('ServiceBase Online Login')&'</a></h3>' & CRLF & p_web.DivHeader('tab_LoginForm1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_LoginForm1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_LoginForm1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_LoginForm1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'ServiceBase Online Login')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_LoginForm1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('ServiceBase Online Login')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_LoginForm1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('ServiceBase Online Login')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_LoginForm1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::loc:BranchID
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::loc:BranchID
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::Loc:Password
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::Loc:Password
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Validate::licence  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:LicenseText = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:LicenseText = p_web.GetValue('Value')
  End
  do ValidateValue::licence  ! copies value to session value if valid.

ValidateValue::licence  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:LicenseText',tmp:LicenseText).
    End

Value::licence  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('LoginForm_' & p_web._nocolon('licence') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    tmp:LicenseText = p_web.RestoreValue('tmp:LicenseText')
    do ValidateValue::licence
    If licence:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- URL --- tmp:LicenseText
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('url','tmp:LicenseText',p_web.GetSessionValueFormat('tmp:LicenseText'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::loc:BranchID  Routine
  packet = clip(packet) & p_web.DivHeader('LoginForm_' & p_web._nocolon('loc:BranchID') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Branch ID'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::loc:BranchID  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    loc:BranchID = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    loc:BranchID = p_web.GetValue('Value')
  End
  do ValidateValue::loc:BranchID  ! copies value to session value if valid.
  p_web.SSV('LoginMessage','')
  do Value::loc:BranchID
  do SendAlert

ValidateValue::loc:BranchID  Routine
    If not (1=0)
  If loc:BranchID = ''
    loc:Invalid = 'loc:BranchID'
    loc:BranchID:IsInvalid = true
    loc:alert = p_web.translate('Branch ID') & ' ' & p_web.site.RequiredText
  End
    loc:BranchID = Upper(loc:BranchID)
      if loc:invalid = '' then p_web.SetSessionValue('loc:BranchID',loc:BranchID).
    End

Value::loc:BranchID  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('LoginForm_' & p_web._nocolon('loc:BranchID') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  If loc:retrying
    loc:BranchID = p_web.RestoreValue('loc:BranchID')
    do ValidateValue::loc:BranchID
    If loc:BranchID:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- loc:BranchID
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''loc:BranchID'',''loginform_loc:branchid_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon(loc:Password)&''',2);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','loc:BranchID',p_web.GetSessionValueFormat('loc:BranchID'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::Loc:Password  Routine
  packet = clip(packet) & p_web.DivHeader('LoginForm_' & p_web._nocolon('Loc:Password') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Password'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::Loc:Password  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    Loc:Password = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    Loc:Password = p_web.GetValue('Value')
  End
  do ValidateValue::Loc:Password  ! copies value to session value if valid.
  p_web.SSV('LoginMessage','')
  do Value::Loc:Password
  do SendAlert

ValidateValue::Loc:Password  Routine
    If not (1=0)
  If Loc:Password = ''
    loc:Invalid = 'Loc:Password'
    Loc:Password:IsInvalid = true
    loc:alert = p_web.translate('Password') & ' ' & p_web.site.RequiredText
  End
    Loc:Password = Upper(Loc:Password)
      if loc:invalid = '' then p_web.SetSessionValue('Loc:Password',Loc:Password).
    End

Value::Loc:Password  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('LoginForm_' & p_web._nocolon('Loc:Password') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  If loc:retrying
    Loc:Password = p_web.RestoreValue('Loc:Password')
    do ValidateValue::Loc:Password
    If Loc:Password:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- Loc:Password
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''Loc:Password'',''loginform_loc:password_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('password','Loc:Password',p_web.GetSessionValueFormat('Loc:Password'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('LoginForm_nexttab_' & 0)
    tmp:LicenseText = p_web.GSV('tmp:LicenseText')
    do ValidateValue::licence
    If loc:Invalid
      loc:retrying = 1
      do Value::licence
      !do SendAlert
      !exit
    End
    If loc:Invalid then exit.
  of lower('LoginForm_nexttab_' & 1)
    loc:BranchID = p_web.GSV('loc:BranchID')
    do ValidateValue::loc:BranchID
    If loc:Invalid
      loc:retrying = 1
      do Value::loc:BranchID
      !do SendAlert
      !exit
    End
    Loc:Password = p_web.GSV('Loc:Password')
    do ValidateValue::Loc:Password
    If loc:Invalid
      loc:retrying = 1
      do Value::Loc:Password
      !do SendAlert
      !exit
    End
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_LoginForm_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('LoginForm_tab_' & 0)
    do GenerateTab0
  of lower('LoginForm_tab_' & 1)
    do GenerateTab1
  of lower('LoginForm_loc:BranchID_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::loc:BranchID
      of event:timer
        do Value::loc:BranchID
      else
        do Value::loc:BranchID
      end
  of lower('LoginForm_Loc:Password_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::Loc:Password
      of event:timer
        do Value::Loc:Password
      else
        do Value::Loc:Password
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('LoginForm_form:ready_',1)

  p_web.SetSessionValue('LoginForm_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_LoginForm',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('LoginForm_form:ready_',1)
  p_web.SetSessionValue('LoginForm_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_LoginForm',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('LoginForm_form:ready_',1)
  p_web.SetSessionValue('LoginForm_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('LoginForm:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('LoginForm_form:ready_',1)
  p_web.SetSessionValue('LoginForm_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('LoginForm:Primed',0)
  p_web.setsessionvalue('showtab_LoginForm',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
  If false
      If not (1=0)
          If p_web.IfExistsValue('tmp:LicenseText')
            tmp:LicenseText = p_web.GetValue('tmp:LicenseText')
          End
      End
  End
      If not (1=0)
          If p_web.IfExistsValue('loc:BranchID')
            loc:BranchID = p_web.GetValue('loc:BranchID')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('Loc:Password')
            Loc:Password = p_web.GetValue('Loc:Password')
          End
      End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord
      ! Validation
      IF (loc:Invalid)
          EXIT
      END
      p_web.SSV('NewPasswordRequired','')
      p_web.SSV('UserMobileRequired','')
      error# = FALSE
      
      Access:USERS.Clearkey(use:Password_Key)
      use:Password = UPPER(p_web.GSV('loc:Password'))
      IF (Access:USERS.Tryfetch(use:Password_Key) = Level:Benign)
          IF (use:Active <> 'YES')
              loc:Invalid = 'loc:Password'
              loc:Alert = 'Entered password is no longer active'
              p_web.SSV('loc:Password','')
              EXIT
          END
          
          Access:TRADEACC.Clearkey(tra:SiteLocationKey)
          tra:SiteLocation = use:Location
          IF (Access:TRADEACC.Tryfetch(tra:SiteLocationKey) = Level:Benign)
              IF (tra:Account_Number <> p_web.GSV('loc:BranchID'))
                  loc:Invalid = 'loc:BranchID'
                  loc:Alert = 'Branch ID And Password Do Not Match'
                  p_web.SSV('loc:Password','')
                  EXIT
              END ! IF
              
              ! Renew Password?
              IF (use:RenewPassword > 0)
                  IF (use:RenewPassword = '')
                      use:RenewPassword = TODAY()
                      Access:USERS.TryUpdate()
                  ELSE ! IF
                      IF ((use:PasswordLastChanged + use:RenewPassword) < TODAY() OR |
                          use:PasswordLastChanged > TODAY())
                          p_web.SSV('NextURL','FormNewPassword')
                          p_web.SSV('NewPasswordRequired',1)
                          error# = TRUE
                      END !IF
                  END ! IF
              END ! IF
  
              ! Mobile?
              If (use:MobileNumber = '')
                  p_web.SSV('UserMobileRequired',1)
                  error# = TRUE
              END
  
              p_web.SSV('LoginDetails1','Site: ' & clip(tra:Company_Name))
              p_web.SSV('LoginDetails2','User: ' & clip(use:forename) & ' ' & clip(use:Surname))
          ELSE ! IF
              loc:Invalid = 'loc:Password'
              loc:Alert = 'Incorrect Password'
              EXIT
          END ! IF
      ELSE ! IF 
          loc:Invalid = 'loc:Password'
          loc:Alert = 'Incorrect Password'
          EXIT
      END ! IF
      
      IF (error# = FALSE)
          SetLoginDefaults(p_web)
      END ! IF
  
      p_web.ValidateLogin()
  

ValidateDelete  Routine
  p_web.DeleteSessionValue('LoginForm_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('LoginForm_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
  If false
    loc:InvalidTab += 1
    do ValidateValue::licence
    If loc:Invalid then exit.
  End
  ! tab = 1
    loc:InvalidTab += 1
    do ValidateValue::loc:BranchID
    If loc:Invalid then exit.
    do ValidateValue::Loc:Password
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('LoginForm:Primed',0)
  p_web.StoreValue('tmp:LicenseText')
  p_web.StoreValue('loc:BranchID')
  p_web.StoreValue('Loc:Password')

License  Routine
  packet = clip(packet) & p_web.AsciiToUTF(|
    '<<div align="center" class="nt-licence-text"><13,10>'&|
    '<<iframe id="waitframe" src="la.htm"><</iframe><13,10>'&|
    '<</div><13,10>'&|
    '',net:OnlyIfUTF)
JobSearch            PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locJobPassword       STRING(30)                            !
locSearchJobNumber   STRING(20)                            !
locPasswordMessage   STRING(100)                           !
FilesOpened     Long
USERS::State  USHORT
JOBSE::State  USHORT
JOBS::State  USHORT
WEBJOB::State  USHORT
locSearchJobNumber:IsInvalid  Long
locPasswordMessage:IsInvalid  Long
locJobPassword:IsInvalid  Long
button:OpenJob:IsInvalid  Long
buttonViewJob:IsInvalid  Long
buttonPaymentDetails:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('JobSearch')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'JobSearch_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('JobSearch','Change')
    p_web.DivHeader('JobSearch',p_web.combine(p_web.site.style.formdiv,))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('JobSearch') = 0
        p_web.AddPreCall('JobSearch')
        p_web.DivHeader('popup_JobSearch','nt-hidden')
        p_web.DivHeader('JobSearch',p_web.combine(p_web.site.style.formdiv,))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_JobSearch_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_JobSearch_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferJobSearch',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_JobSearch',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferJobSearch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_JobSearch',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferJobSearch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_JobSearch',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_JobSearch',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferJobSearch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_JobSearch',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferJobSearch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_JobSearch',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_JobSearch',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('JobSearch')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
DeleteSessionValues ROUTINE ! Auto generated by Bryan's Template
    ! Local Variables
    p_web.DeleteSessionValue('Ans')
    p_web.DeleteSessionValue('locJobPassword')
    p_web.DeleteSessionValue('locSearchJobNumber')
    p_web.DeleteSessionValue('locPasswordMessage')

    ! Other Variables
ShowHidePaymentDetails      ROUTINE
DATA
locPaymentStringRRC EQUATE('PAYMENT TYPES')
locPaymentStringARC EQUATE('PAYMENT DETAILS')
locPaymentString CSTRING(30)
CODE

    IF (p_web.GSV('locJobFound') = 1)
        paymentFail# = 0

        IF (p_web.GSV('BookingSite') = 'RRC')
            locPaymentString = locPaymentStringRRC
        ELSE
            locPaymentString = locPaymentStringARC
        END

        IF (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),locPaymentString) = 0)
            Access:JOBS.ClearKey(job:Ref_Number_Key)
            job:Ref_Number = p_web.GSV('locSearchJobNumber')
            IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                IF (job:Loan_Unit_Number = 0)
                    IF (job:Warranty_Job = 'YES' AND job:Chargeable_Job <> 'YES')
                        paymentFail# = 1
                    END
                END
            END
        ELSE
            paymentFail# = 1
        END
        IF (paymentFail# = 1)
            p_web.SSV('Hide:PaymentDetails',1)
            p_web.SSV('Comment:PaymentDetails','')
        ELSE
            p_web.SSV('Hide:PaymentDetails',0)
        END
    END

OpenFiles  ROUTINE
  p_web._OpenFile(USERS)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(WEBJOB)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(USERS)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(WEBJOB)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  ! Clear Fields
  DO DeleteSessionValues
  p_web.SSV('Hide:PaymentDetails',1)
  p_web.SSV('Hide:ViewJobButton',1)
  p_web.SSV('comment:locSearchJobNumber','Enter Job Number and press [TAB]')
  p_web.SSV('comment:locJobPassword','Enter Password and press [TAB]')
  p_web.SSV('locJobFound',0)
  p_web.SSV('locPasswordRequired',0)
  
  p_web.site.CancelButton.TextValue = 'Close'
  
  ClearUpdateJobVariables(p_web)
  p_web.SetValue('JobSearch_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'JobSearch'
    end
    p_web.formsettings.proc = 'JobSearch'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine
  DO DeleteSessionValues

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  if p_web.IfExistsValue('locSearchJobNumber') = 0
    p_web.SetSessionValue('locSearchJobNumber',locSearchJobNumber)
  Else
    locSearchJobNumber = p_web.GetSessionValue('locSearchJobNumber')
  End
  if p_web.IfExistsValue('locPasswordMessage') = 0
    p_web.SetSessionValue('locPasswordMessage',locPasswordMessage)
  Else
    locPasswordMessage = p_web.GetSessionValue('locPasswordMessage')
  End
  if p_web.IfExistsValue('locJobPassword') = 0
    p_web.SetSessionValue('locJobPassword',locJobPassword)
  Else
    locJobPassword = p_web.GetSessionValue('locJobPassword')
  End

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locSearchJobNumber')
    locSearchJobNumber = p_web.GetValue('locSearchJobNumber')
    p_web.SetSessionValue('locSearchJobNumber',locSearchJobNumber)
  Else
    locSearchJobNumber = p_web.GetSessionValue('locSearchJobNumber')
  End
  if p_web.IfExistsValue('locPasswordMessage')
    locPasswordMessage = p_web.GetValue('locPasswordMessage')
    p_web.SetSessionValue('locPasswordMessage',locPasswordMessage)
  Else
    locPasswordMessage = p_web.GetSessionValue('locPasswordMessage')
  End
  if p_web.IfExistsValue('locJobPassword')
    locJobPassword = p_web.GetValue('locJobPassword')
    p_web.SetSessionValue('locJobPassword',locJobPassword)
  Else
    locJobPassword = p_web.GetSessionValue('locJobPassword')
  End

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('JobSearch_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferJobSearch')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('JobSearch_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('JobSearch_ChainTo')
    loc:formaction = p_web.GetSessionValue('JobSearch_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'IndexPage'

GenerateForm   Routine
  do LoadRelatedRecords
 locSearchJobNumber = p_web.RestoreValue('locSearchJobNumber')
 locPasswordMessage = p_web.RestoreValue('locPasswordMessage')
 locJobPassword = p_web.RestoreValue('locJobPassword')
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Job Search') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Job Search',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_JobSearch',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_JobSearch0_div')&'">'&p_web.Translate('Enter Job Number')&'</a></li>'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_JobSearch1_div')&'">'&p_web.Translate()&'</a></li>'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_JobSearch2_div')&'">'&p_web.Translate()&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="JobSearch_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.cancel();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CancelButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="JobSearch_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'JobSearch_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="JobSearch_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'JobSearch_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locSearchJobNumber')
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_JobSearch')>0,p_web.GSV('showtab_JobSearch'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_JobSearch'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_JobSearch') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_JobSearch'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_JobSearch')>0,p_web.GSV('showtab_JobSearch'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_JobSearch') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Enter Job Number') & ''''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_JobSearch_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_JobSearch')>0,p_web.GSV('showtab_JobSearch'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"JobSearch",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_JobSearch')>0,p_web.GSV('showtab_JobSearch'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_JobSearch_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('JobSearch') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('JobSearch')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Enter Job Number')&'</a></h3>' & CRLF & p_web.DivHeader('tab_JobSearch0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_JobSearch0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_JobSearch0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_JobSearch0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Enter Job Number')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_JobSearch0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Enter Job Number')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_JobSearch0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Enter Job Number')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_JobSearch0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&150&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locSearchJobNumber
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&250&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locSearchJobNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locSearchJobNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab1  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate()&'</a></h3>' & CRLF & p_web.DivHeader('tab_JobSearch1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_JobSearch1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_JobSearch1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_JobSearch1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,)
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_JobSearch1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend)
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_JobSearch1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain">' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_JobSearch1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&150&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&250&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locPasswordMessage
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locPasswordMessage
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&150&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locJobPassword
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&250&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locJobPassword
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locJobPassword
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab2  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate()&'</a></h3>' & CRLF & p_web.DivHeader('tab_JobSearch2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_JobSearch2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_JobSearch2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_JobSearch2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,)
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_JobSearch2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend)
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_JobSearch2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain">' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_JobSearch2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&150&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::button:OpenJob
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&250&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::button:OpenJob
        do Comment::button:OpenJob
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&150&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&250&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::buttonViewJob
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::buttonViewJob
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
      If p_web.GSV('PH') > 2
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::buttonPaymentDetails
        do Comment::buttonPaymentDetails
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Prompt::locSearchJobNumber  Routine
  packet = clip(packet) & p_web.DivHeader('JobSearch_' & p_web._nocolon('locSearchJobNumber') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Job Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locSearchJobNumber  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locSearchJobNumber = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locSearchJobNumber = p_web.GetValue('Value')
  End
  do ValidateValue::locSearchJobNumber  ! copies value to session value if valid.
  ! Validate job
  p_web.SSV('Hide:ViewJobButton',1)
  fail# = 0
  Access:WEBJOB.Clearkey(wob:RefNumberKey)
  wob:RefNumber    =  p_web.GSV('locSearchJobNumber')
  if (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
      ! Found
      p_web.SSV('locSearchRecordNumber',wob:RecordNumber)
      if (p_web.GSV('BookingSite') = 'RRC')
          if( wob:HeadAccountNumber <> p_web.GSV('BookingAccount'))
              p_web.SSV('comment:locSearchJobNumber','Error: The selected job was not booked at this site')
              fail# = 1
          end ! if( wob:HeadAccountNumber <> p_web.GSV('BookingAccount'))
      end ! if (p_web.GSV('BookingSite') = 'RRC')
  else ! if (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
      ! Error
      p_web.SSV('comment:locSearchJobNumber','Error: Cannot find selected Job Number')
      fail# = 1
  end ! if (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
  
  if (fail# = 1)
      p_web.SSV('locJobFound',0)
      P_web.SSV('locSearchRecordNumber','')
      p_web.SSV('Hide:PaymentDetails',1)
  else ! if (fail# = 1)
  
      p_web.SSV('comment:locSearchJobNumber','Job Found')
  
      p_web.SSV('locPasswordRequired',0)
      p_web.SSV('locJobFound',1)
      p_web.SSV('Hide:ViewJobButton',1)
  
  
      Access:JOBS.ClearKey(job:Ref_Number_Key)
      job:Ref_Number = wob:RefNumber
      IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
      END
      p_web.FileToSessionQueue(JOBS)
  
      Access:JOBSE.ClearKey(jobe:RefNumberKey)
      jobe:RefNumber = job:Ref_Number
      IF (Access:JOBSE.TryFetch(jobe:RefNumberKey))
      END
      p_web.FileToSessionQueue(JOBSE)
  
  
  
      IF (p_web.GSV('BookingSite') = 'RRC')
  
          IF (Inlist(job:Location,p_web.GSV('Default:RRCLocation'), |
              p_web.GSV('Default:DespatchToCustomer'), |
              p_web.GSV('Default:InTransitPUP'), |
              p_web.GSV('Default:PUPLocation')) > 0)
              SentToHub(p_web)
              ! Job is RRC and at RRC
              IF (job:Warranty_Job = 'YES' AND wob:EDI <> 'XXX')
                  IF (job:Date_Completed <> '' AND p_web.GSV('SentToHub') = 0)
                      IF (wob:EDI = 'NO' OR wob:EDI = 'YES' OR wob:EDI = 'PAY' |
                          OR wob:EDI = 'APP')
                          ! Job should be disabled
                      ELSE
                          p_web.SSV('locJobFound',0)
                          p_web.SSV('locPasswordRequired',1)
  
                          p_web.SSV('comment:locSearchJobNumber','Job Found: Password required.')
  
                          p_web.SSV('locPasswordRequired',1)
                          p_web.SSV('locJobFound',0)
                          p_web.SSV('Hide:ViewJobButton',0)
                          p_web.SSV('locPasswordMessage','The selected warranty job has been rejected. Enter password to EDIT the job, or click "View Job" to view the job.')
                      END
                  END
              END
          END
      END ! IF (p_web.GSV('BookingSite') = 'RRC')
  
  
      DO ShowHidePaymentDetails
  
  end ! if (fail# = 0)
  do Value::locSearchJobNumber
  do SendAlert
  do Comment::locSearchJobNumber ! allows comment style to be updated.
  do Comment::locSearchJobNumber
  do Value::button:OpenJob  !1
  do Value::buttonPaymentDetails  !1
  do Comment::buttonPaymentDetails
  do Prompt::locJobPassword
  do Value::locJobPassword  !1
  do Comment::locJobPassword
  do Value::buttonViewJob  !1
  do Value::locPasswordMessage  !1

ValidateValue::locSearchJobNumber  Routine
    If not (1=0)
  If locSearchJobNumber = ''
    loc:Invalid = 'locSearchJobNumber'
    locSearchJobNumber:IsInvalid = true
    loc:alert = p_web.translate('Job Number') & ' ' & p_web.site.RequiredText
  End
      if loc:invalid = '' then p_web.SetSessionValue('locSearchJobNumber',locSearchJobNumber).
    End

Value::locSearchJobNumber  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobSearch_' & p_web._nocolon('locSearchJobNumber') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  If loc:retrying
    locSearchJobNumber = p_web.RestoreValue('locSearchJobNumber')
    do ValidateValue::locSearchJobNumber
    If locSearchJobNumber:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- locSearchJobNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locSearchJobNumber'',''jobsearch_locsearchjobnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locSearchJobNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locSearchJobNumber',p_web.GetSessionValueFormat('locSearchJobNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locSearchJobNumber  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locSearchJobNumber:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('comment:locSearchJobNumber'))
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('JobSearch_' & p_web._nocolon('locSearchJobNumber') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::locPasswordMessage  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locPasswordMessage = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locPasswordMessage = p_web.GetValue('Value')
  End
  do ValidateValue::locPasswordMessage  ! copies value to session value if valid.
  do Comment::locPasswordMessage ! allows comment style to be updated.

ValidateValue::locPasswordMessage  Routine
    If not (p_web.GSV('locPasswordMessage') = '')
      if loc:invalid = '' then p_web.SetSessionValue('locPasswordMessage',locPasswordMessage).
    End

Value::locPasswordMessage  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('locPasswordMessage') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobSearch_' & p_web._nocolon('locPasswordMessage') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('locPasswordMessage') = '')
  ! --- DISPLAY --- locPasswordMessage
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('red bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locPasswordMessage'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locPasswordMessage  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locPasswordMessage:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('locPasswordMessage') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('JobSearch_' & p_web._nocolon('locPasswordMessage') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('locPasswordMessage') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locJobPassword  Routine
  packet = clip(packet) & p_web.DivHeader('JobSearch_' & p_web._nocolon('locJobPassword') & '_prompt',Choose(p_web.GSV('locPasswordRequired') <> 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('locPasswordRequired') <> 1,'',p_web.Translate('Enter Password'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locJobPassword  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locJobPassword = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locJobPassword = p_web.GetValue('Value')
  End
  do ValidateValue::locJobPassword  ! copies value to session value if valid.
  ! Check password
  p_web.SSV('locJobFound',0)
  Access:USERS.ClearKey(use:password_key)
  use:password = p_web.GSV('locJobPassword')
  IF (Access:USERS.tryfetch(use:password_key) = Level:Benign)
      Access:ACCAREAS.ClearKey(acc:Access_level_key)
      acc:User_Level = use:User_Level
      acc:Access_Area = 'EDIT REJECTED WARRANTY JOB'
      IF (Access:ACCAREAS.TryFetch(acc:Access_level_key) = Level:Benign)
          p_web.SSV('locJobFound',1)
          p_web.SSV('comment:locJobPassword','Password accepted')
  
          DO ShowHidePaymentDetails
      ELSE
          p_web.SSV('comment:locJobPassword','User does not have access to edit job.')
      END
  
      ELSE
      p_web.SSV('comment:locJobPassword','Invalid password')
  END
  do Value::locJobPassword
  do SendAlert
  do Comment::locJobPassword ! allows comment style to be updated.
  do Value::buttonPaymentDetails  !1
  do Value::button:OpenJob  !1
  do Comment::locJobPassword
  do Value::locSearchJobNumber  !1

ValidateValue::locJobPassword  Routine
    If not (p_web.GSV('locPasswordRequired') <> 1)
    locJobPassword = Upper(locJobPassword)
      if loc:invalid = '' then p_web.SetSessionValue('locJobPassword',locJobPassword).
    End

Value::locJobPassword  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('locPasswordRequired') <> 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobSearch_' & p_web._nocolon('locJobPassword') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.GSV('locJobFound') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    locJobPassword = p_web.RestoreValue('locJobPassword')
    do ValidateValue::locJobPassword
    If locJobPassword:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('locPasswordRequired') <> 1)
  ! --- STRING --- locJobPassword
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('locJobFound') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locJobPassword'',''jobsearch_locjobpassword_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locJobPassword')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('password','locJobPassword',p_web.GetSessionValueFormat('locJobPassword'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locJobPassword  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locJobPassword:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('comment:locJobPassword'))
  loc:class = Choose(p_web.GSV('locPasswordRequired') <> 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('JobSearch_' & p_web._nocolon('locJobPassword') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('locPasswordRequired') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::button:OpenJob  Routine
  packet = clip(packet) & p_web.DivHeader('JobSearch_' & p_web._nocolon('button:OpenJob') & '_prompt',Choose(p_web.GSV('locJobFound') = 0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('locJobFound') = 0,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::button:OpenJob  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::button:OpenJob  ! copies value to session value if valid.
  do Comment::button:OpenJob ! allows comment style to be updated.

ValidateValue::button:OpenJob  Routine
    If not (p_web.GSV('locJobFound') = 0)
    End

Value::button:OpenJob  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('locJobFound') = 0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobSearch_' & p_web._nocolon('button:OpenJob') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('locJobFound') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','OpenJob','Edit Job',p_web.combine(Choose('Edit Job' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.WindowOpen(clip('ViewJob?&wob__RecordNumber=' & p_web.GSV('locSearchRecordNumber') & '&Change_btn=Change&BackURL=JobSearch')&''&'','_self'),,loc:disabled,'images/zoom.png',,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::button:OpenJob  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if button:OpenJob:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('locJobFound') = 0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('JobSearch_' & p_web._nocolon('button:OpenJob') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('locJobFound') = 0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonViewJob  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonViewJob  ! copies value to session value if valid.
  do Comment::buttonViewJob ! allows comment style to be updated.

ValidateValue::buttonViewJob  Routine
    If not (p_web.GSV('Hide:ViewJobButton') = 1)
    End

Value::buttonViewJob  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:ViewJobButton') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobSearch_' & p_web._nocolon('buttonViewJob') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('Hide:ViewJobButton') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','ViewJob','View Job',p_web.combine(Choose('View Job' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.WindowOpen(clip('ViewJob?&wob__RecordNumber=' & p_web.GSV('locSearchRecordNumber') & '&Change_btn=Change&BackURL=JobSearch&ViewOnly=1')&''&'','_self'),,loc:disabled,'images/zoom.png',,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonViewJob  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonViewJob:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:ViewJobButton') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('JobSearch_' & p_web._nocolon('buttonViewJob') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:ViewJobButton') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonPaymentDetails  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonPaymentDetails  ! copies value to session value if valid.
  do Comment::buttonPaymentDetails ! allows comment style to be updated.

ValidateValue::buttonPaymentDetails  Routine
  If p_web.GSV('PH') > 2
    If not (p_web.GSV('locJobFound') = 0 Or p_web.GSV('Hide:PaymentDetails') = 1)
    End
  End

Value::buttonPaymentDetails  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('locJobFound') = 0 Or p_web.GSV('Hide:PaymentDetails') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobSearch_' & p_web._nocolon('buttonPaymentDetails') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('locJobFound') = 0 Or p_web.GSV('Hide:PaymentDetails') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','PaymentDetails','Payment Details',p_web.combine(Choose('Payment Details' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.WindowOpen('DisplayBrowsePayments&' & p_web._jsok('DisplayBrowsePaymentsReturnURL=JobSearch') &''&'','_self'),,loc:disabled,'images/moneyp.png',,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonPaymentDetails  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonPaymentDetails:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:PaymentDetails'))
  loc:class = Choose(p_web.GSV('locJobFound') = 0 Or p_web.GSV('Hide:PaymentDetails') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('JobSearch_' & p_web._nocolon('buttonPaymentDetails') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('locJobFound') = 0 Or p_web.GSV('Hide:PaymentDetails') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('JobSearch_nexttab_' & 0)
    locSearchJobNumber = p_web.GSV('locSearchJobNumber')
    do ValidateValue::locSearchJobNumber
    If loc:Invalid
      loc:retrying = 1
      do Value::locSearchJobNumber
      !do SendAlert
      do Comment::locSearchJobNumber ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('JobSearch_nexttab_' & 1)
    locPasswordMessage = p_web.GSV('locPasswordMessage')
    do ValidateValue::locPasswordMessage
    If loc:Invalid
      loc:retrying = 1
      do Value::locPasswordMessage
      !do SendAlert
      do Comment::locPasswordMessage ! allows comment style to be updated.
      !exit
    End
    locJobPassword = p_web.GSV('locJobPassword')
    do ValidateValue::locJobPassword
    If loc:Invalid
      loc:retrying = 1
      do Value::locJobPassword
      !do SendAlert
      do Comment::locJobPassword ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('JobSearch_nexttab_' & 2)
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_JobSearch_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('JobSearch_tab_' & 0)
    do GenerateTab0
  of lower('JobSearch_locSearchJobNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locSearchJobNumber
      of event:timer
        do Value::locSearchJobNumber
        do Comment::locSearchJobNumber
      else
        do Value::locSearchJobNumber
      end
  of lower('JobSearch_tab_' & 1)
    do GenerateTab1
  of lower('JobSearch_locJobPassword_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locJobPassword
      of event:timer
        do Value::locJobPassword
        do Comment::locJobPassword
      else
        do Value::locJobPassword
      end
  of lower('JobSearch_tab_' & 2)
    do GenerateTab2
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('JobSearch_form:ready_',1)

  p_web.SetSessionValue('JobSearch_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_JobSearch',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('JobSearch_form:ready_',1)
  p_web.SetSessionValue('JobSearch_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_JobSearch',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('JobSearch_form:ready_',1)
  p_web.SetSessionValue('JobSearch_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('JobSearch:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('JobSearch_form:ready_',1)
  p_web.SetSessionValue('JobSearch_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('JobSearch:Primed',0)
  p_web.setsessionvalue('showtab_JobSearch',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
      If not (1=0)
          If p_web.IfExistsValue('locSearchJobNumber')
            locSearchJobNumber = p_web.GetValue('locSearchJobNumber')
          End
      End
      If not (p_web.GSV('locPasswordRequired') <> 1)
        If not (p_web.GSV('locJobFound') = 1)
          If p_web.IfExistsValue('locJobPassword')
            locJobPassword = p_web.GetValue('locJobPassword')
          End
        End
      End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('JobSearch_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('JobSearch_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
    do ValidateValue::locSearchJobNumber
    If loc:Invalid then exit.
  ! tab = 3
    loc:InvalidTab += 1
    do ValidateValue::locPasswordMessage
    If loc:Invalid then exit.
    do ValidateValue::locJobPassword
    If loc:Invalid then exit.
  ! tab = 2
    loc:InvalidTab += 1
    do ValidateValue::button:OpenJob
    If loc:Invalid then exit.
    do ValidateValue::buttonViewJob
    If loc:Invalid then exit.
    do ValidateValue::buttonPaymentDetails
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('JobSearch:Primed',0)
  p_web.StoreValue('locSearchJobNumber')
  p_web.StoreValue('locPasswordMessage')
  p_web.StoreValue('locJobPassword')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')

IndividualDespatch   PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
tmp:LoanModelNumber  STRING(30)                            !
Ans                  LONG                                  !
locAccessoryErrorMessage STRING(255)                       !
locIMEINumber        STRING(30)                            !
locJobNumber         LONG                                  !
locErrorMessage      STRING(255)                           !
locMessage           STRING(255)                           !
locAccessoryMessage  STRING(255)                           !
locSecurityPackID    STRING(30)                            !
locAccessoryPassword STRING(30)                            !
locAccessoryPasswordMessage STRING(255)                    !
locAuditTrail        STRING(255)                           !
locConsignmentNumber STRING(30)                            !
FilesOpened     Long
LOANACC::State  USHORT
JOBACC::State  USHORT
WEBJOB::State  USHORT
WAYBILLJ::State  USHORT
WAYBILLS::State  USHORT
TagFile::State  USHORT
COURIER::State  USHORT
MULDESPJ::State  USHORT
SUBTRACC::State  USHORT
LOAN::State  USHORT
TRADEACC::State  USHORT
EXCHANGE::State  USHORT
JOBS::State  USHORT
locJobNumber:IsInvalid  Long
locIMEINumber:IsInvalid  Long
buttonValidateJobDetails:IsInvalid  Long
locErrorMessage:IsInvalid  Long
TagValidateLoanAccessories:IsInvalid  Long
buttonValidateAccessories:IsInvalid  Long
locAccessoryMessage:IsInvalid  Long
locAccessoryErrorMessage:IsInvalid  Long
locAccessoryPassword:IsInvalid  Long
buttonConfirmMismatch:IsInvalid  Long
buttonFailAccessory:IsInvalid  Long
locAccessoryPasswordMessage:IsInvalid  Long
cou:Courier:IsInvalid  Long
locConsignmentNumber:IsInvalid  Long
locSecurityPackID:IsInvalid  Long
buttonConfirmDespatch:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
cou:Courier_OptionView   View(COURIER)
                          Project(cou:Courier)
                        End
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('IndividualDespatch')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'IndividualDespatch_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('IndividualDespatch','')
    p_web.DivHeader('IndividualDespatch',p_web.combine(p_web.site.style.formdiv,'fdiv'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('IndividualDespatch') = 0
        p_web.AddPreCall('IndividualDespatch')
        p_web.DivHeader('popup_IndividualDespatch','nt-hidden')
        p_web.DivHeader('IndividualDespatch',p_web.combine(p_web.site.style.formdiv,'fdiv'))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_IndividualDespatch_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_IndividualDespatch_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferIndividualDespatch',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_IndividualDespatch',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferIndividualDespatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_IndividualDespatch',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferIndividualDespatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_IndividualDespatch',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_IndividualDespatch',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferIndividualDespatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_IndividualDespatch',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferIndividualDespatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_IndividualDespatch',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_IndividualDespatch',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('IndividualDespatch')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
ClearVariables      Routine
    p_web.SSV('UnitValidated',0)
    p_web.SSV('locJobNumber','')
    p_web.SSV('locIMEINumber','')
    p_web.SSV('ValidateButtonText','Validate Unit Details')
    p_web.SSV('locAccessoryMessage','')
    p_web.SSV('locAccessoryErrorMessage','')
    p_web.SSV('ValidateAccessories',0)
    p_web.SSV('Hide:ValidateAccessoriesButton',1)
    p_web.SSV('AccessoryConfirmationRequired',0)
    p_web.SSV('AccessoryPasswordRequired',0)
    p_web.SSV('locAccessoryPasswordMessage','')
    p_web.SSV('locAccessoryPassword','')
    p_web.SSV('AccessoriesValidated',0)
    p_web.SSV('Show:DespatchInvoiceDetails',0) ! Show invoice details on despatch screen
    p_web.SSV('CreateDespatchInvoice',0) ! Auto create invoice
    p_web.SSV('Show:ConsignmentNumber',0)

ValidateUnitDetails Routine
    ClearTagFile(p_web)
    if (p_web.GSV('UnitValidated') = 1)
        Do ClearVariables
        exit
    end
    p_web.SSV('tmp:LoanModelNumber','')
    p_web.SSV('UnitValidated',0)
    p_web.SSV('locErrorMessage','')
    p_web.SSV('locMessage','')
    p_web.SSV('DespatchType','')
    p_web.SSV('ValidateAccessories',0)
    p_web.SSV('locSecurityPackID','')
    p_web.SSV('Hide:ValidateAccessoriesButton',1)
    p_web.SSV('AccessoryConfirmationRequired',0)
    p_web.SSV('AccessoryPasswordRequired',0)
    p_web.SSV('locAccessoryPasswordMessage','')
    p_web.SSV('locAccessoryPassword','')
    p_web.SSV('AccessoriesValidated',0)

    if (p_web.GSV('locJobNumber')= '')
        exit
    end
    if (p_web.GSV('locIMEINumber') = '')
        exit
    end

    Access:JOBS.Clearkey(job:Ref_Number_Key)
    job:Ref_Number = p_web.GSV('locJobNumber')
    If (Access:JOBS.TryFetch(job:Ref_Number_Key))
        p_web.SSV('locErrorMessage','Cannot find selected Job Number!')
        Exit
    end


    if (JobInUse(job:Ref_Number))
        p_web.SSV('locErrorMessage','Cannot despatch the selected job is in use.')
        exit
    end

    p_web.FileToSessionQueue(JOBS)

    Access:JOBSE.Clearkey(jobe:RefNumberKey)
    jobe:RefNumber = job:Ref_Number
    If (Access:JOBSE.TryFetch(jobe:RefNumberKey))
    End
    p_web.FileToSessionQueue(JOBSE)

    Access:WEBJOB.ClearKey(wob:RefNumberKey)
    wob:RefNumber  = job:Ref_Number
    IF (Access:WEBJOB.TryFetch(wob:RefNumberKey))
    END
    p_web.FileToSessionQueue(WEBJOB)

    if (p_web.GSV('BookingSite') = 'RRC')
        ! RRC Despatch

        IF (wob:ReadyToDespatch <> 1)
            p_web.SSV('locErrorMessage','The selected job is not ready for despatch.')
            EXIT
        END

        IF (wob:HeadAccountNumber <> p_web.GSV('BookingAccount'))
            p_web.SSV('locErrorMessage','The selected job is not from your RRC.')
            EXIT
        END

        Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
        sub:Account_Number = job:Account_Number
        if (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
            Access:JOBSE2.Clearkey(jobe2:RefNumberKey)
            jobe2:RefNumber = job:Ref_Number
            If (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
                if (sub:UseCustDespAdd = 'YES')
                    if (HubOutOfRegion(p_web.GSV('BookingAccount'),jobe2:HubCustomer) = 1)
                        if (ReleasedForDespatch(job:Ref_Number) = 0)
                            p_web.SSV('locErrorMessage','Unable to despatch. The delivery address is outside your region.')
                            exit
                        end
                    end
                else
                    if (HubOutOfRegion(p_web.GSV('BookingAccount'),jobe2:HubDelivery) = 1)
                        if (ReleasedForDespatch(job:Ref_Number) = 0)
                            p_web.SSV('locErrorMessage','Unable to despatch. The delivery address is outside your region.')
                            exit
                        end
                    end
                end
            end
        end

        found# = 0
        Access:MULDESPJ.Clearkey(mulj:JobNumberOnlyKey)
        mulj:JobNumber = job:Ref_Number
        set(mulj:JobNumberOnlyKey,mulj:JobNumberOnlyKey)
        Loop Until Access:MULDESPJ.Next()
            if (mulj:JobNumber <> job:Ref_Number)
                break
            end
            Access:MULDESP.Clearkey(muld:RecordNumberKey)
            muld:RecordNumber = mulj:RefNumber
            if (Access:MULDESP.TryFetch(muld:RecordNumberKey)= Level:Benign)
                if (muld:HeadAccountNumber = p_web.GSV('BookingAccount'))
                    found# = 1
                    break
                end
            end
        end
        if (found# = 1)
            p_web.SSV('locErrorMessage','Cannot despatch! This job is part of a multiple batch that has not yet been despatched.')
            exit
        end

        if (GETINI('DESPATCH','DoNotDespatchLoan',,CLIP(PATH())&'\SB2KDEF.INI') = 1 And jobe:Despatchtype = 'JOB')
            IF (p_web.GSV('BookingSite') = 'RRC' OR (p_web.GSV('BookingSite') = 'ARC' AND jobe:WebJob <> 1))
                ! #11817 Only stop despatch if RRC, or ARC back to customer. (Bryan: 11/05/2011)
                if (job:Loan_Unit_Number <> 0)
                    p_web.SSV('locErrorMessage','Cannot despatch! The attached loan unit has not been returned')
                    exit
                end
            END !IF (jobe:WebJob <> 1)

        end

        Access:COURIER.ClearKey(cou:Courier_Key)
        CASE jobe:DespatchType
        OF 'JOB'
            cou:Courier = job:Courier
        OF 'EXC'
            cou:Courier = job:Exchange_Courier
        of 'LOA'
            cou:Courier = job:Loan_Courier
        end
        if (Access:COURIER.TryFetch(cou:Courier_Key))
            p_web.SSV('locErrorMessage','Cannot despatch! Cannot find the attached courier.')
            exit
        end


        p_web.SSV('DespatchType',jobe:DespatchType)



    else ! if (p_web.GSV('BookingSite') = 'RRC')
        ! ARC Despatch

        ! Can job be despatched?
        if (job:Chargeable_Job = 'YES')
            Access:TRADEACC.Clearkey(tra:Account_Number_Key)
            tra:Account_Number = p_web.GSV('Default:ARCLocation')
            if (Access:TRADEACC.TryFetch(tra:Account_Number_Key) )

            End
            if (tra:Despatch_Paid_Jobs = 'YES' And NOT JobPaid(job:Ref_Number))
                p_web.SSV('locErrorMessage','Unable to despatch. The selected job has not been paid.')
                exit
            end
            if (tra:Despatch_Invoiced_Jobs = 'YES' And NOT JobPaid(job:Ref_Number))
                p_web.SSV('locErrorMessage','Unble to despatch. The selected job has not been invoiced.')
                exit
            end
        end

        if (GETINI('DESPATCH','DoNotDespatchLoan',,CLIP(PATH())&'\SB2KDEF.INI') = 1 And job:Despatch_type = 'JOB')
            if (job:Loan_Unit_Number <> 0)
                p_web.SSV('locErrorMessage','Cannot despatch! The attached loan unit has not been returned')
                exit
            end
        end

        Access:MULDESPJ.Clearkey(mulj:JobNumberOnlyKey)
        mulj:JobNumber = job:Ref_Number
        if (Access:MULDESPJ.TryFetch(mulj:JobNumberOnlyKey) = Level:benign)
            p_web.SSV('locErrorMessage','Cannot despatch! This job is part of a multiple batch that has not yet been despatched.')
            exit
        end

        if (job:Despatched <> 'REA')
            p_web.SSV('locErrorMessage','The selected job is not ready for despatch.')
            exit
        end

        Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
        sub:Account_Number = job:Account_Number
        if (Access:SUBTRACC.TryFetch(sub:Account_NUmber_Key) = Level:Benign)
            Access:TRADEACC.Clearkey(tra:Account_Number_Key)
            tra:Account_Number = sub:Main_Account_Number
            if (access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign)
                if (tra:Use_Sub_Accounts = 'YES' and tra:Invoice_Sub_Accounts = 'YES')
                    if (sub:Stop_Account = 'YES' and job:Warranty_Job <> 'YES')
                        p_web.SSV('locErrorMessage','Cannot despatch! The Trade Account is on stop and this is not a warranty job.')
                        exit
                    end

                else
                    if (tra:Stop_Account = 'YES' and job:Warranty_Job <> 'YES')
                        p_web.SSV('locErrorMessage','Cannot despatch! The Trade Account is on stop and this is not a warranty job.')
                        exit
                    end
                end
            end
        end

        Access:COURIER.Clearkey(cou:Courier_Key)
        cou:Courier = job:Current_Courier
        if (Access:COURIER.TryFetch(cou:Courier_Key))
            p_web.SSV('locErrorMessage','Cannot despatch! Cannot find the attached courier.')
            exit
        end

        p_web.SSV('DespatchType',job:Despatch_Type)

    end

    p_web.FileToSessionQueue(COURIER)

    case p_web.GSV('DespatchType')
    of 'JOB'
        if (job:ESN <> p_web.GSV('locIMEINumber'))
            p_web.SSV('locErrorMessage','The selected I.M.E.I. Number does not match the selected job.')
            exit
        end
        p_web.SSV('tmp:LoanModelNumber',job:Model_Number)
        p_web.SSV('AccessoryRefNumber',job:Ref_Number)
        p_web.SSV('Hide:ValidateAccessoriesButton',0)
        p_web.SSV('ValidateAccessories',1)
        p_web.SSV('AccessoriesValidated',0)
    of 'EXC'
        Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
        xch:Ref_Number = job:Exchange_Unit_Number
        if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key))
            p_web.SSV('locErrorMessage','Exchange Despatch! Unable to find Exchange Unit on selected job.')
            exit
        else
            if (xch:ESN <> p_web.GSV('locIMEINumber'))
                p_web.SSV('locErrorMessage','Exchange Despatch! The selected I.M.E.I. Number does not match the Exchange Unit attached to the selected job.')
                exit
            end
        end
        p_web.SSV('tmp:LoanModelNumber',xch:Model_Number)
        p_web.SSV('AccessoryRefNumber',job:Ref_Number)
        p_web.SSV('ValidateAccessories',0)
        p_web.SSV('Hide:ValidateAccessoriesButton',1)
        p_web.SSV('AccessoriesValidated',1)
    of 'LOA'
        Access:LOAN.Clearkey(loa:Ref_Number_Key)
        loa:Ref_Number = job:Loan_Unit_Number
        if (Access:LOAN.TryFetch(loa:Ref_Number_Key))
            p_web.SSV('locErrorMessage','Loan Despatch! Unable to find Loan Unit on selected job.')
            exit
        else
            if (loa:ESN <> p_web.GSV('locIMEINumber'))
                p_web.SSV('locErrorMessage','Loan Despatch! The selected I.M.E.I. Number does not match the Loan Unit attached to the selected job.')
                exit
            end
        end
        p_web.SSV('tmp:LoanModelNumber',loa:Model_Number)
        p_web.SSV('AccessoryRefNumber',job:Loan_Unit_Number)
        p_web.SSV('Hide:ValidateAccessoriesButton',0)
        p_web.SSV('ValidateAccessories',1)
        p_web.SSV('AccessoriesValidated',0)
    end


    p_web.SSV('UnitValidated',1)

    p_web.SSV('ValidateButtonText','Despatch Another Unit')

    ! Can the job be invoiced?
    IF (job:Bouncer <> 'X' AND job:Chargeable_Job = 'YES')
        p_web.SSV('Show:DespatchInvoiceDetails',1)
        IF (InvoiceSubAccounts(job:Account_Number))
            IF (sub:InvoiceAtDespatch AND job:Despatch_Type = 'JOB')
                CASE sub:InvoiceType
                OF 0 ! Manual invoice
                    p_web.SSV('CreateDespatchInvoice',0)
                OF 1 !Auto Invoice
                    IF (job:Invoice_Number = '')
                        ! Create Invoice
                        p_web.SSV('CreateDespatchInvoice',1)
                    END

                END


            END

        ELSE
            IF (tra:InvoiceAtDespatch AND job:Despatch_Type = 'JOB')
                CASE tra:InvoiceType
                OF 0 ! Manual Invoice
                    p_web.SSV('CreateDespatchInvoice',0)
                OF 1 ! Auto Invoice
                    p_web.SSV('CreateDespatchInvoice',1)
                END

            END

        END

    END



ShowConsignmentNumber       ROUTINE
    Access:COURIER.ClearKey(cou:Courier_Key)
    cou:Courier = p_web.GSV('cou:Courier')
    IF (Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign)
        p_web.FileToSessionQueue(COURIER)

        IF (p_web.GSV('cou:PrintWaybill') <> 1 AND p_web.GSV('cou:AutoConsignmentNo') <> 1)
            p_web.SSV('Show:ConsignmentNumber',1)
        ELSE
            p_web.SSV('Show:ConsignmentNumber',0)
        END
    END



DeleteSessionValues ROUTINE ! Auto generated by Bryan's Template
    ! Local Variables
    p_web.DeleteSessionValue('tmp:LoanModelNumber')
    p_web.DeleteSessionValue('Ans')
    p_web.DeleteSessionValue('locAccessoryErrorMessage')
    p_web.DeleteSessionValue('locIMEINumber')
    p_web.DeleteSessionValue('locJobNumber')
    p_web.DeleteSessionValue('locErrorMessage')
    p_web.DeleteSessionValue('locMessage')
    p_web.DeleteSessionValue('locAccessoryMessage')
    p_web.DeleteSessionValue('locSecurityPackID')
    p_web.DeleteSessionValue('locAccessoryPassword')
    p_web.DeleteSessionValue('locAccessoryPasswordMessage')
    p_web.DeleteSessionValue('locAuditTrail')
    p_web.DeleteSessionValue('locConsignmentNumber')

    ! Other Variables
    p_web.DeleteSessionValue('UnitValidated')
    p_web.DeleteSessionValue('ValidateButtonText')
    p_web.DeleteSessionValue('AccessoryRefNumber')
    p_web.DeleteSessionValue('DespatchType')
    p_web.DeleteSessionValue('Hide:ValidateAccessoriesButton')
    p_web.DeleteSessionValue('ValidateAccessories')
    p_web.DeleteSessionValue('AccessoryConfirmationRequired')
    p_web.DeleteSessionValue('AccessoryPasswordRequired')
    p_web.DeleteSessionValue('AccessoriesValidated')
    p_web.DeleteSessionValue('Show:DespatchInvoiceDetails')
    p_web.DeleteSessionValue('CreateDespatchInvoice')
OpenFiles  ROUTINE
  p_web._OpenFile(LOANACC)
  p_web._OpenFile(JOBACC)
  p_web._OpenFile(WEBJOB)
  p_web._OpenFile(WAYBILLJ)
  p_web._OpenFile(WAYBILLS)
  p_web._OpenFile(TagFile)
  p_web._OpenFile(COURIER)
  p_web._OpenFile(MULDESPJ)
  p_web._OpenFile(SUBTRACC)
  p_web._OpenFile(LOAN)
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(EXCHANGE)
  p_web._OpenFile(JOBS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(LOANACC)
  p_Web._CloseFile(JOBACC)
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(WAYBILLJ)
  p_Web._CloseFile(WAYBILLS)
  p_Web._CloseFile(TagFile)
  p_Web._CloseFile(COURIER)
  p_Web._CloseFile(MULDESPJ)
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(LOAN)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(EXCHANGE)
  p_Web._CloseFile(JOBS)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE
  loc:EipClm = upper(p_web.GetValue('_EIPClm'))
  p_web.DeleteValue('_EIPClm')
  case loc:EipClm
  of ''
    case upper(p_web.GetValue('_calledfrom_'))
    of upper('TagValidateLoanAccessories')
      do Value::TagValidateLoanAccessories
    end
  end

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.site.CancelButton.TextValue = 'Close'
  p_web.SSV('ValidateButtonText','Validate Unit Details')
  Do ClearVariables
  p_web.SetValue('IndividualDespatch_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'IndividualDespatch'
    end
    p_web.formsettings.proc = 'IndividualDespatch'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine
  do deletesessionvalues

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'cou:Courier'
    p_web.setsessionvalue('showtab_IndividualDespatch',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(COURIER)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.locConsignmentNumber')
  End
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  if p_web.IfExistsValue('locJobNumber') = 0
    p_web.SetSessionValue('locJobNumber',locJobNumber)
  Else
    locJobNumber = p_web.GetSessionValue('locJobNumber')
  End
  if p_web.IfExistsValue('locIMEINumber') = 0
    p_web.SetSessionValue('locIMEINumber',locIMEINumber)
  Else
    locIMEINumber = p_web.GetSessionValue('locIMEINumber')
  End
  if p_web.IfExistsValue('locErrorMessage') = 0
    p_web.SetSessionValue('locErrorMessage',locErrorMessage)
  Else
    locErrorMessage = p_web.GetSessionValue('locErrorMessage')
  End
  if p_web.IfExistsValue('locAccessoryMessage') = 0
    p_web.SetSessionValue('locAccessoryMessage',locAccessoryMessage)
  Else
    locAccessoryMessage = p_web.GetSessionValue('locAccessoryMessage')
  End
  if p_web.IfExistsValue('locAccessoryErrorMessage') = 0
    p_web.SetSessionValue('locAccessoryErrorMessage',locAccessoryErrorMessage)
  Else
    locAccessoryErrorMessage = p_web.GetSessionValue('locAccessoryErrorMessage')
  End
  if p_web.IfExistsValue('locAccessoryPassword') = 0
    p_web.SetSessionValue('locAccessoryPassword',locAccessoryPassword)
  Else
    locAccessoryPassword = p_web.GetSessionValue('locAccessoryPassword')
  End
  if p_web.IfExistsValue('locAccessoryPasswordMessage') = 0
    p_web.SetSessionValue('locAccessoryPasswordMessage',locAccessoryPasswordMessage)
  Else
    locAccessoryPasswordMessage = p_web.GetSessionValue('locAccessoryPasswordMessage')
  End
  if p_web.IfExistsValue('cou:Courier') = 0
    p_web.SetSessionValue('cou:Courier',cou:Courier)
  Else
    cou:Courier = p_web.GetSessionValue('cou:Courier')
  End
  if p_web.IfExistsValue('locConsignmentNumber') = 0
    p_web.SetSessionValue('locConsignmentNumber',locConsignmentNumber)
  Else
    locConsignmentNumber = p_web.GetSessionValue('locConsignmentNumber')
  End
  if p_web.IfExistsValue('locSecurityPackID') = 0
    p_web.SetSessionValue('locSecurityPackID',locSecurityPackID)
  Else
    locSecurityPackID = p_web.GetSessionValue('locSecurityPackID')
  End

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locJobNumber')
    locJobNumber = p_web.GetValue('locJobNumber')
    p_web.SetSessionValue('locJobNumber',locJobNumber)
  Else
    locJobNumber = p_web.GetSessionValue('locJobNumber')
  End
  if p_web.IfExistsValue('locIMEINumber')
    locIMEINumber = p_web.GetValue('locIMEINumber')
    p_web.SetSessionValue('locIMEINumber',locIMEINumber)
  Else
    locIMEINumber = p_web.GetSessionValue('locIMEINumber')
  End
  if p_web.IfExistsValue('locErrorMessage')
    locErrorMessage = p_web.GetValue('locErrorMessage')
    p_web.SetSessionValue('locErrorMessage',locErrorMessage)
  Else
    locErrorMessage = p_web.GetSessionValue('locErrorMessage')
  End
  if p_web.IfExistsValue('locAccessoryMessage')
    locAccessoryMessage = p_web.GetValue('locAccessoryMessage')
    p_web.SetSessionValue('locAccessoryMessage',locAccessoryMessage)
  Else
    locAccessoryMessage = p_web.GetSessionValue('locAccessoryMessage')
  End
  if p_web.IfExistsValue('locAccessoryErrorMessage')
    locAccessoryErrorMessage = p_web.GetValue('locAccessoryErrorMessage')
    p_web.SetSessionValue('locAccessoryErrorMessage',locAccessoryErrorMessage)
  Else
    locAccessoryErrorMessage = p_web.GetSessionValue('locAccessoryErrorMessage')
  End
  if p_web.IfExistsValue('locAccessoryPassword')
    locAccessoryPassword = p_web.GetValue('locAccessoryPassword')
    p_web.SetSessionValue('locAccessoryPassword',locAccessoryPassword)
  Else
    locAccessoryPassword = p_web.GetSessionValue('locAccessoryPassword')
  End
  if p_web.IfExistsValue('locAccessoryPasswordMessage')
    locAccessoryPasswordMessage = p_web.GetValue('locAccessoryPasswordMessage')
    p_web.SetSessionValue('locAccessoryPasswordMessage',locAccessoryPasswordMessage)
  Else
    locAccessoryPasswordMessage = p_web.GetSessionValue('locAccessoryPasswordMessage')
  End
  if p_web.IfExistsValue('cou:Courier')
    cou:Courier = p_web.GetValue('cou:Courier')
    p_web.SetSessionValue('cou:Courier',cou:Courier)
  Else
    cou:Courier = p_web.GetSessionValue('cou:Courier')
  End
  if p_web.IfExistsValue('locConsignmentNumber')
    locConsignmentNumber = p_web.GetValue('locConsignmentNumber')
    p_web.SetSessionValue('locConsignmentNumber',locConsignmentNumber)
  Else
    locConsignmentNumber = p_web.GetSessionValue('locConsignmentNumber')
  End
  if p_web.IfExistsValue('locSecurityPackID')
    locSecurityPackID = p_web.GetValue('locSecurityPackID')
    p_web.SetSessionValue('locSecurityPackID',locSecurityPackID)
  Else
    locSecurityPackID = p_web.GetSessionValue('locSecurityPackID')
  End

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('IndividualDespatch_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'IndexPage'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('IndividualDespatch_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('IndividualDespatch_ChainTo')
    loc:formaction = p_web.GetSessionValue('IndividualDespatch_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'IndexPage'

GenerateForm   Routine
  do LoadRelatedRecords
 locJobNumber = p_web.RestoreValue('locJobNumber')
 locIMEINumber = p_web.RestoreValue('locIMEINumber')
 locErrorMessage = p_web.RestoreValue('locErrorMessage')
 locAccessoryMessage = p_web.RestoreValue('locAccessoryMessage')
 locAccessoryErrorMessage = p_web.RestoreValue('locAccessoryErrorMessage')
 locAccessoryPassword = p_web.RestoreValue('locAccessoryPassword')
 locAccessoryPasswordMessage = p_web.RestoreValue('locAccessoryPasswordMessage')
 locConsignmentNumber = p_web.RestoreValue('locConsignmentNumber')
 locSecurityPackID = p_web.RestoreValue('locSecurityPackID')
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Individual Despatch') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Individual Despatch',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_IndividualDespatch',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_IndividualDespatch0_div')&'">'&p_web.Translate('Confirm Unit Details')&'</a></li>'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_IndividualDespatch1_div')&'">'&p_web.Translate()&'</a></li>'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_IndividualDespatch2_div')&'">'&p_web.Translate('Confirm Despatch')&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="IndividualDespatch_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('IndividualDespatch_TagValidateLoanAccessories_embedded_div')&''');'
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.cancel();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CancelButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="IndividualDespatch_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'IndividualDespatch_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="IndividualDespatch_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'IndividualDespatch_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locJobNumber')
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_IndividualDespatch')>0,p_web.GSV('showtab_IndividualDespatch'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_IndividualDespatch'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_IndividualDespatch') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_IndividualDespatch'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_IndividualDespatch')>0,p_web.GSV('showtab_IndividualDespatch'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_IndividualDespatch') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Confirm Unit Details') & ''''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Confirm Despatch') & ''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_IndividualDespatch_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_IndividualDespatch')>0,p_web.GSV('showtab_IndividualDespatch'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"IndividualDespatch",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_IndividualDespatch')>0,p_web.GSV('showtab_IndividualDespatch'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_IndividualDespatch_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('IndividualDespatch') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('IndividualDespatch')
    p_web.SetValue('_CallPopups',2)
    If p_web.GetPreCall('TagValidateLoanAccessories') = 0
      p_web.SetValue('TagValidateLoanAccessories:NoForm',1)
      p_web.SetValue('TagValidateLoanAccessories:FormName',loc:formname)
      p_web.SetValue('TagValidateLoanAccessories:parentIs','Form')
      p_web.SetValue('_parentProc','IndividualDespatch')
      TagValidateLoanAccessories(p_web)
      p_web.SetValue('_CallPopups',0)
      p_web.DeleteValue('TagValidateLoanAccessories:NoForm')
      p_web.DeleteValue('TagValidateLoanAccessories:FormName')
      p_web.DeleteValue('TagValidateLoanAccessories:parentIs')
      p_web.DeleteValue('_parentProc')
    End
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Confirm Unit Details')&'</a></h3>' & CRLF & p_web.DivHeader('tab_IndividualDespatch0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_IndividualDespatch0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_IndividualDespatch0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_IndividualDespatch0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Confirm Unit Details')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_IndividualDespatch0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Confirm Unit Details')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_IndividualDespatch0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Confirm Unit Details')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_IndividualDespatch0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locJobNumber
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'50%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locJobNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locJobNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locIMEINumber
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'50%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locIMEINumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locIMEINumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'50%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::buttonValidateJobDetails
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::buttonValidateJobDetails
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locErrorMessage
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'50%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locErrorMessage
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locErrorMessage
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab1  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate()&'</a></h3>' & CRLF & p_web.DivHeader('tab_IndividualDespatch1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_IndividualDespatch1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_IndividualDespatch1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_IndividualDespatch1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,)
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_IndividualDespatch1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend)
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_IndividualDespatch1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain">' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_IndividualDespatch1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::TagValidateLoanAccessories
        do Value::TagValidateLoanAccessories
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'50%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::buttonValidateAccessories
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::buttonValidateAccessories
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locAccessoryMessage
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'50%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locAccessoryMessage
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locAccessoryMessage
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locAccessoryErrorMessage
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'50%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locAccessoryErrorMessage
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locAccessoryErrorMessage
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locAccessoryPassword
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'50%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locAccessoryPassword
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locAccessoryPassword
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'50%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::buttonConfirmMismatch
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::buttonConfirmMismatch
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'50%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::buttonFailAccessory
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::buttonFailAccessory
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locAccessoryPasswordMessage
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'50%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locAccessoryPasswordMessage
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locAccessoryPasswordMessage
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab2  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Confirm Despatch')&'</a></h3>' & CRLF & p_web.DivHeader('tab_IndividualDespatch2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_IndividualDespatch2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_IndividualDespatch2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_IndividualDespatch2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Confirm Despatch')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_IndividualDespatch2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Confirm Despatch')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_IndividualDespatch2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Confirm Despatch')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_IndividualDespatch2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::cou:Courier
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'50%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::cou:Courier
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::cou:Courier
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locConsignmentNumber
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'50%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locConsignmentNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locConsignmentNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locSecurityPackID
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'50%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locSecurityPackID
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locSecurityPackID
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'50%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::buttonConfirmDespatch
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::buttonConfirmDespatch
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Prompt::locJobNumber  Routine
  packet = clip(packet) & p_web.DivHeader('IndividualDespatch_' & p_web._nocolon('locJobNumber') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Job Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locJobNumber  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locJobNumber = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locJobNumber = p_web.GetValue('Value')
  End
  do ValidateValue::locJobNumber  ! copies value to session value if valid.
  p_web.SSV('locErrorMessage','')
  do Value::locJobNumber
  do SendAlert
  do Comment::locJobNumber ! allows comment style to be updated.
  do Value::locErrorMessage  !1

ValidateValue::locJobNumber  Routine
    If not (1=0)
  If locJobNumber = ''
    loc:Invalid = 'locJobNumber'
    locJobNumber:IsInvalid = true
    loc:alert = p_web.translate('Job Number') & ' ' & p_web.site.RequiredText
  End
      if loc:invalid = '' then p_web.SetSessionValue('locJobNumber',locJobNumber).
    End

Value::locJobNumber  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('IndividualDespatch_' & p_web._nocolon('locJobNumber') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If p_web.GSV('UnitValidated') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,'formrqd')
  If loc:retrying
    locJobNumber = p_web.RestoreValue('locJobNumber')
    do ValidateValue::locJobNumber
    If locJobNumber:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- locJobNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('UnitValidated') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locJobNumber'',''individualdespatch_locjobnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locJobNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locJobNumber',p_web.GetSessionValueFormat('locJobNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locJobNumber  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locJobNumber:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web._jsok(p_web.site.RequiredText)
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('IndividualDespatch_' & p_web._nocolon('locJobNumber') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locIMEINumber  Routine
  packet = clip(packet) & p_web.DivHeader('IndividualDespatch_' & p_web._nocolon('locIMEINumber') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('I.M.E.I. Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locIMEINumber  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locIMEINumber = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locIMEINumber = p_web.GetValue('Value')
  End
  do ValidateValue::locIMEINumber  ! copies value to session value if valid.
  p_web.SSV('locErrorMessage','')
  do Value::locIMEINumber
  do SendAlert
  do Comment::locIMEINumber ! allows comment style to be updated.
  do Value::locErrorMessage  !1

ValidateValue::locIMEINumber  Routine
    If not (1=0)
  If locIMEINumber = ''
    loc:Invalid = 'locIMEINumber'
    locIMEINumber:IsInvalid = true
    loc:alert = p_web.translate('I.M.E.I. Number') & ' ' & p_web.site.RequiredText
  End
      if loc:invalid = '' then p_web.SetSessionValue('locIMEINumber',locIMEINumber).
    End

Value::locIMEINumber  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('IndividualDespatch_' & p_web._nocolon('locIMEINumber') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If p_web.GSV('UnitValidated') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,'formrqd')
  If loc:retrying
    locIMEINumber = p_web.RestoreValue('locIMEINumber')
    do ValidateValue::locIMEINumber
    If locIMEINumber:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- locIMEINumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('UnitValidated') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locIMEINumber'',''individualdespatch_locimeinumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locIMEINumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locIMEINumber',p_web.GetSessionValueFormat('locIMEINumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locIMEINumber  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locIMEINumber:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web._jsok(p_web.site.RequiredText)
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('IndividualDespatch_' & p_web._nocolon('locIMEINumber') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonValidateJobDetails  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonValidateJobDetails  ! copies value to session value if valid.
  do ValidateUnitDetails
  do Value::buttonValidateJobDetails
  do Comment::buttonValidateJobDetails ! allows comment style to be updated.
  do Value::locErrorMessage  !1
  do Value::TagValidateLoanAccessories  !1
  do Value::locJobNumber  !1
  do Value::locIMEINumber  !1
  do Value::buttonValidateAccessories  !1
  do Value::buttonConfirmDespatch  !1
  do Prompt::locSecurityPackID
  do Value::locSecurityPackID  !1
  do Value::locAccessoryErrorMessage  !1
  do Value::locAccessoryMessage  !1
  do Value::buttonConfirmMismatch  !1
  do Prompt::locAccessoryPassword
  do Value::locAccessoryPassword  !1
  do Value::locAccessoryPasswordMessage  !1
  do Value::buttonFailAccessory  !1
  do Prompt::cou:Courier
  do Value::cou:Courier  !1
  do Prompt::locConsignmentNumber
  do Value::locConsignmentNumber  !1

ValidateValue::buttonValidateJobDetails  Routine
    If not (1=0)
    End

Value::buttonValidateJobDetails  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('IndividualDespatch_' & p_web._nocolon('buttonValidateJobDetails') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonValidateJobDetails'',''individualdespatch_buttonvalidatejobdetails_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','ValidateJob',p_web.GSV('ValidateButtonText'),p_web.combine(Choose(p_web.GSV('ValidateButtonText') <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'button-entryfield'),loc:formname,,,,loc:javascript,loc:disabled,,,,,,,,,,'nt-left')!h
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonValidateJobDetails  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonValidateJobDetails:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('IndividualDespatch_' & p_web._nocolon('buttonValidateJobDetails') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locErrorMessage  Routine
  packet = clip(packet) & p_web.DivHeader('IndividualDespatch_' & p_web._nocolon('locErrorMessage') & '_prompt',Choose(p_web.GSV('locErrorMessage') = '','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('locErrorMessage') = '','',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locErrorMessage  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locErrorMessage = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locErrorMessage = p_web.GetValue('Value')
  End
  do ValidateValue::locErrorMessage  ! copies value to session value if valid.
  do Comment::locErrorMessage ! allows comment style to be updated.

ValidateValue::locErrorMessage  Routine
    If not (p_web.GSV('locErrorMessage') = '')
      if loc:invalid = '' then p_web.SetSessionValue('locErrorMessage',locErrorMessage).
    End

Value::locErrorMessage  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('locErrorMessage') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('IndividualDespatch_' & p_web._nocolon('locErrorMessage') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (p_web.GSV('locErrorMessage') = '')
  ! --- DISPLAY --- locErrorMessage
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('red bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locErrorMessage'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locErrorMessage  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locErrorMessage:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('locErrorMessage') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('IndividualDespatch_' & p_web._nocolon('locErrorMessage') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('locErrorMessage') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::TagValidateLoanAccessories  Routine
  packet = clip(packet) & p_web.DivHeader('IndividualDespatch_' & p_web._nocolon('TagValidateLoanAccessories') & '_prompt',Choose(NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('ValidateAccessories') = 1),'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('ValidateAccessories') = 1),'',p_web.Translate('Validate Accessories'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::TagValidateLoanAccessories  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  Elsif true
    p_web.StoreValue('acr:Accessory')
  End
  do ValidateValue::TagValidateLoanAccessories  ! copies value to session value if valid.
  do Comment::TagValidateLoanAccessories ! allows comment style to be updated.

ValidateValue::TagValidateLoanAccessories  Routine
    If not (NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('ValidateAccessories') = 1))
    End

Value::TagValidateLoanAccessories  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('ValidateAccessories') = 1),1,0))
  ! --- BROWSE ---  TagValidateLoanAccessories --
  p_web.SetValue('TagValidateLoanAccessories:NoForm',1)
  p_web.SetValue('TagValidateLoanAccessories:FormName',loc:formname)
  p_web.SetValue('TagValidateLoanAccessories:parentIs','Form')
  p_web.SetValue('_parentProc','IndividualDespatch')
  if p_web.RequestAjax = 0
    p_web.SSV('IndividualDespatch:_popup_',p_web.GetValue('_popup_')) ! stores the current browse popup state
    packet = clip(packet) & '<div id="'&lower('IndividualDespatch_TagValidateLoanAccessories_embedded_div')&'"><!-- Net:TagValidateLoanAccessories --></div><13,10>'
    do SendPacket
    p_web.DivHeader('IndividualDespatch_' & lower('TagValidateLoanAccessories') & '_value')
    p_web.DivFooter()
  else
    if p_web.GetValue('_popup_') = 1
      p_web.SSV('IndividualDespatch:_popup_',1)
    elsif p_web.GSV('IndividualDespatch:_popup_') = 1
      p_web.SetValue('_popup_',1)
    end
    packet = clip(packet) & '<!-- Net:TagValidateLoanAccessories --><13,10>'
  end
  do SendPacket
Comment::TagValidateLoanAccessories  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if TagValidateLoanAccessories:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('ValidateAccessories') = 1),'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('IndividualDespatch_' & p_web._nocolon('TagValidateLoanAccessories') & '_comment',loc:class,Net:NoSend)
  If NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('ValidateAccessories') = 1)
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonValidateAccessories  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonValidateAccessories  ! copies value to session value if valid.
  ! Validate Accessories
  p_web.SSV('locAccessoryErrorMessage','')
  p_web.SSV('locAccessoryMessage','')
  p_web.SSV('Hide:ValidateAccessoriesButton',1)
  p_web.SSV('AccessoryConfirmationRequired',0)
  p_web.SSV('AccessoryPasswordRequired',0)
  p_web.SSV('AccessoriesValidated',0)
  
  ! Validate
  p_web.SSV('AccessoryCheck:Type',p_web.GSV('DespatchType'))
  p_web.SSV('AccessoryCheck:RefNumber',p_web.GSV('AccessoryRefNumber'))
  AccessoryCheck(p_web)
  Case p_web.GSV('AccessoryCheck:Return')
  Of 1 ! Missing
      p_web.SSV('locAccessoryErrorMessage','The selected unit has a missing accessory.')
      p_web.SSV('AccessoryConfirmationRequired',1)
      p_web.SSV('ConfirmMismatchText','Confirm Access. Validation')
  
      if (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'ACCESSORY MISMATCH - ACCEPT'))
          p_web.SSV('AccessoryPasswordRequired',1)
      end
  Of 2 ! Mismatch
      p_web.SSV('locAccessoryErrorMessage','There is a mismatch between the selected unit''s accessories.')
      p_web.SSV('AccessoryConfirmationRequired',1)
      p_web.SSV('ConfirmMismatchText','Confirm Access. Validation')
      if (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'ACCESSORY MISMATCH - ACCEPT'))
          p_web.SSV('AccessoryPasswordRequired',1)
      end
  Else ! A Ok
      p_web.SSV('locAccessoryMessage','Accessory Validated')
      p_web.SSV('AccessoryConfirmationRequired',0)
      p_web.SSV('AccessoriesValidated',1)
      DO ShowConsignmentNumber
  End
  do Value::buttonValidateAccessories
  do Comment::buttonValidateAccessories ! allows comment style to be updated.
  do Value::locAccessoryErrorMessage  !1
  do Value::locAccessoryMessage  !1
  do Value::buttonConfirmDespatch  !1
  do Prompt::locSecurityPackID
  do Value::locSecurityPackID  !1
  do Value::TagValidateLoanAccessories  !1
  do Value::buttonConfirmMismatch  !1
  do Prompt::locAccessoryPassword
  do Value::locAccessoryPassword  !1
  do Value::buttonFailAccessory  !1
  do Prompt::locConsignmentNumber
  do Value::locConsignmentNumber  !1
  do Prompt::cou:Courier
  do Value::cou:Courier  !1

ValidateValue::buttonValidateAccessories  Routine
    If not (NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('ValidateAccessories') = 1 AND p_web.GSV('AccessoryConfirmationRequired') <> 1 AND p_web.GSV('Hide:ValidateAccessoriesButton') <> 1))
    End

Value::buttonValidateAccessories  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('ValidateAccessories') = 1 AND p_web.GSV('AccessoryConfirmationRequired') <> 1 AND p_web.GSV('Hide:ValidateAccessoriesButton') <> 1),'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('IndividualDespatch_' & p_web._nocolon('buttonValidateAccessories') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('ValidateAccessories') = 1 AND p_web.GSV('AccessoryConfirmationRequired') <> 1 AND p_web.GSV('Hide:ValidateAccessoriesButton') <> 1))
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonValidateAccessories'',''individualdespatch_buttonvalidateaccessories_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','ValidateAccessories','Validate Accessories',p_web.combine(Choose('Validate Accessories' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'button-entryfield'),loc:formname,,,,loc:javascript,loc:disabled,,,,,,,,,,'nt-left')!h
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonValidateAccessories  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonValidateAccessories:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('ValidateAccessories') = 1 AND p_web.GSV('AccessoryConfirmationRequired') <> 1 AND p_web.GSV('Hide:ValidateAccessoriesButton') <> 1),'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('IndividualDespatch_' & p_web._nocolon('buttonValidateAccessories') & '_comment',loc:class,Net:NoSend)
  If NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('ValidateAccessories') = 1 AND p_web.GSV('AccessoryConfirmationRequired') <> 1 AND p_web.GSV('Hide:ValidateAccessoriesButton') <> 1)
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locAccessoryMessage  Routine
  packet = clip(packet) & p_web.DivHeader('IndividualDespatch_' & p_web._nocolon('locAccessoryMessage') & '_prompt',Choose(p_web.GSV('locAccessoryMessage') = '','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('locAccessoryMessage') = '','',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locAccessoryMessage  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locAccessoryMessage = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locAccessoryMessage = p_web.GetValue('Value')
  End
  do ValidateValue::locAccessoryMessage  ! copies value to session value if valid.
  do Comment::locAccessoryMessage ! allows comment style to be updated.

ValidateValue::locAccessoryMessage  Routine
    If not (p_web.GSV('locAccessoryMessage') = '')
      if loc:invalid = '' then p_web.SetSessionValue('locAccessoryMessage',locAccessoryMessage).
    End

Value::locAccessoryMessage  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('locAccessoryMessage') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('IndividualDespatch_' & p_web._nocolon('locAccessoryMessage') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (p_web.GSV('locAccessoryMessage') = '')
  ! --- DISPLAY --- locAccessoryMessage
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locAccessoryMessage'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locAccessoryMessage  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locAccessoryMessage:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('locAccessoryMessage') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('IndividualDespatch_' & p_web._nocolon('locAccessoryMessage') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('locAccessoryMessage') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locAccessoryErrorMessage  Routine
  packet = clip(packet) & p_web.DivHeader('IndividualDespatch_' & p_web._nocolon('locAccessoryErrorMessage') & '_prompt',Choose(p_web.GSV('locAccessoryErrorMessage') = '','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('locAccessoryErrorMessage') = '','',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locAccessoryErrorMessage  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locAccessoryErrorMessage = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locAccessoryErrorMessage = p_web.GetValue('Value')
  End
  do ValidateValue::locAccessoryErrorMessage  ! copies value to session value if valid.
  do Comment::locAccessoryErrorMessage ! allows comment style to be updated.

ValidateValue::locAccessoryErrorMessage  Routine
    If not (p_web.GSV('locAccessoryErrorMessage') = '')
      if loc:invalid = '' then p_web.SetSessionValue('locAccessoryErrorMessage',locAccessoryErrorMessage).
    End

Value::locAccessoryErrorMessage  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('locAccessoryErrorMessage') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('IndividualDespatch_' & p_web._nocolon('locAccessoryErrorMessage') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (p_web.GSV('locAccessoryErrorMessage') = '')
  ! --- DISPLAY --- locAccessoryErrorMessage
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('red bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locAccessoryErrorMessage'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locAccessoryErrorMessage  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locAccessoryErrorMessage:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('locAccessoryErrorMessage') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('IndividualDespatch_' & p_web._nocolon('locAccessoryErrorMessage') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('locAccessoryErrorMessage') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locAccessoryPassword  Routine
  packet = clip(packet) & p_web.DivHeader('IndividualDespatch_' & p_web._nocolon('locAccessoryPassword') & '_prompt',Choose(p_web.GSV('AccessoryPasswordRequired') <> 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('AccessoryPasswordRequired') <> 1,'',p_web.Translate('Password Required For Confirmation'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locAccessoryPassword  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locAccessoryPassword = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locAccessoryPassword = p_web.GetValue('Value')
  End
  do ValidateValue::locAccessoryPassword  ! copies value to session value if valid.
  do Value::locAccessoryPassword
  do SendAlert
  do Comment::locAccessoryPassword ! allows comment style to be updated.

ValidateValue::locAccessoryPassword  Routine
    If not (p_web.GSV('AccessoryPasswordRequired') <> 1)
  If locAccessoryPassword = ''
    loc:Invalid = 'locAccessoryPassword'
    locAccessoryPassword:IsInvalid = true
    loc:alert = p_web.translate('Password Required For Confirmation') & ' ' & p_web.site.RequiredText
  End
    locAccessoryPassword = Upper(locAccessoryPassword)
      if loc:invalid = '' then p_web.SetSessionValue('locAccessoryPassword',locAccessoryPassword).
    End

Value::locAccessoryPassword  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('AccessoryPasswordRequired') <> 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('IndividualDespatch_' & p_web._nocolon('locAccessoryPassword') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,'formrqd')
  If loc:retrying
    locAccessoryPassword = p_web.RestoreValue('locAccessoryPassword')
    do ValidateValue::locAccessoryPassword
    If locAccessoryPassword:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('AccessoryPasswordRequired') <> 1)
  ! --- STRING --- locAccessoryPassword
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locAccessoryPassword'',''individualdespatch_locaccessorypassword_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('password','locAccessoryPassword',p_web.GetSessionValueFormat('locAccessoryPassword'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locAccessoryPassword  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locAccessoryPassword:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web._jsok(p_web.site.RequiredText)
  loc:class = Choose(p_web.GSV('AccessoryPasswordRequired') <> 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('IndividualDespatch_' & p_web._nocolon('locAccessoryPassword') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('AccessoryPasswordRequired') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonConfirmMismatch  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonConfirmMismatch  ! copies value to session value if valid.
  ! Mismatch Confirmation
  if (p_web.GSV('AccessoryPasswordRequired') = 1)
      if (p_web.GSV('locAccessoryPassword') = '')
          p_web.SSV('locAccessoryPasswordMessage','Password Required')
      else
          If (SecurityCheckFailed(p_web.GSV('locAccessoryPassword'),'ACCESSORY MISMATCH - ACCEPT'))
              p_web.SSV('locAccessoryPasswordMessage','The selected password does not have access to this option')
          else
              p_web.SSV('locAccessoryMessage','Accessory Validated')
              p_web.SSV('locAccessoryErrorMessage','')
              p_web.SSV('Hide:ValidateAccessoriesButton',1)
              p_web.SSV('AccessoryConfirmationRequired',0)
              p_web.SSV('AccessoriesValidated',1)
              p_web.SSV('AccessoryPasswordRequired',0)
              p_web.SSV('locAccessoryPasswordMessage','')
          end
      end
  else
      p_web.SSV('locAccessoryMessage','Accessory Validated')
      p_web.SSV('locAccessoryErrorMessage','')
      p_web.SSV('Hide:ValidateAccessoriesButton',1)
      p_web.SSV('AccessoryConfirmationRequired',0)
      p_web.SSV('AccessoriesValidated',1)
      p_web.SSV('AccessoryPasswordRequired',0)
      p_web.SSV('locAccessoryPasswordMessage','')
      DO ShowConsignmentNumber
  end
  do Value::buttonConfirmMismatch
  do Comment::buttonConfirmMismatch ! allows comment style to be updated.
  do Value::locAccessoryPasswordMessage  !1
  do Value::buttonValidateAccessories  !1
  do Value::TagValidateLoanAccessories  !1
  do Value::locAccessoryMessage  !1
  do Value::buttonConfirmDespatch  !1
  do Prompt::locSecurityPackID
  do Value::locSecurityPackID  !1
  do Prompt::locAccessoryPassword
  do Value::locAccessoryPassword  !1
  do Value::locAccessoryErrorMessage  !1
  do Value::buttonFailAccessory  !1
  do Prompt::cou:Courier
  do Value::cou:Courier  !1
  do Prompt::locConsignmentNumber
  do Value::locConsignmentNumber  !1

ValidateValue::buttonConfirmMismatch  Routine
    If not (NOT (p_web.GSV('AccessoryConfirmationRequired') = 1 AND p_web.GSV('AccessoriesValidated') <> 1))
    End

Value::buttonConfirmMismatch  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(NOT (p_web.GSV('AccessoryConfirmationRequired') = 1 AND p_web.GSV('AccessoriesValidated') <> 1),'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('IndividualDespatch_' & p_web._nocolon('buttonConfirmMismatch') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (NOT (p_web.GSV('AccessoryConfirmationRequired') = 1 AND p_web.GSV('AccessoriesValidated') <> 1))
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonConfirmMismatch'',''individualdespatch_buttonconfirmmismatch_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','ConfirmMismatch',p_web.GSV('ConfirmMismatchText'),p_web.combine(Choose(p_web.GSV('ConfirmMismatchText') <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'button-entryfield'),loc:formname,,,,loc:javascript,loc:disabled,'images\tick.png',,,,,,,,,'nt-left')!h
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonConfirmMismatch  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonConfirmMismatch:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(NOT (p_web.GSV('AccessoryConfirmationRequired') = 1 AND p_web.GSV('AccessoriesValidated') <> 1),'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('IndividualDespatch_' & p_web._nocolon('buttonConfirmMismatch') & '_comment',loc:class,Net:NoSend)
  If NOT (p_web.GSV('AccessoryConfirmationRequired') = 1 AND p_web.GSV('AccessoriesValidated') <> 1)
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonFailAccessory  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonFailAccessory  ! copies value to session value if valid.
  ! Fail Accessory Check
  
  GetStatus(850,0,p_web.GSV('DespatchType'),p_web)
  
  Access:JOBS.ClearKey(job:Ref_Number_Key)
  job:Ref_Number = p_web.GSV('job:Ref_Number')
  IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
      p_web.SessionQueueToFile(JOBS)
      Access:JOBS.TryUpdate()
  END
  
  Access:WEBJOB.ClearKey(wob:RefNumberKey)
  wob:RefNumber = job:Ref_Number
  IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
      p_web.SessionQueueToFile(WEBJOB)
      Access:WEBJOB.TryUpdate()
  END
  
  locAuditTrail = 'ACCESSORY MISMATCH<13,10,13,10>ACCESSORIES BOOKED IN:'
  IF (p_web.GSV('DespatchType') <> 'Loan')
      Access:JOBACC.ClearKey(jac:Ref_Number_Key)
      jac:Ref_Number = job:Ref_Number
      Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
      Loop
          If Access:JOBACC.NEXT()
              Break
          End !If
          If jac:Ref_Number <> job:Ref_Number      |
              Then Break.  ! End If
          locAuditTrail = CLIP(locAuditTrail) & '<13,10>' & Clip(jac:Accessory)
      End !Loop
  ELSE
      Access:LOANACC.ClearKey(lac:Ref_Number_Key)
      lac:Ref_Number = job:Ref_Number
      Set(lac:Ref_Number_Key,lac:Ref_Number_Key)
      Loop
          If Access:LOANACC.NEXT()
              Break
          End !If
          If lac:Ref_Number <> job:Ref_Number      |
              Then Break.  ! End If
          locAuditTrail = CLIP(locAuditTrail) & '<13,10>' & Clip(lac:Accessory)
      End !Loop
  END
  locAuditTrail = CLIP(locAuditTrail) & '<13,10,13,10>ACCESSORIES BOOKED OUT: '
  
  Access:TagFile.ClearKey(tag:keyTagged)
  tag:sessionID = p_web.SessionID
  SET(tag:keyTagged,tag:keyTagged)
  LOOP UNTIL Access:TagFile.Next()
      IF (tag:sessionID <> p_web.SessionID)
          BREAK
      END
      IF (tag:tagged = 1)
          locAuditTrail = CLIP(locAuditTrail) & '<13,10>' & Clip(tag:TaggedValue)
      END
  END
  
  p_web.SSV('AddToAudit:Type',p_web.GSV('DespatchType'))
  p_web.SSV('AddToAudit:Action','FAILED DESPATCH VALIDATION')
  p_web.SSV('AddToAudit:Notes',CLIP(locAuditTrail))
  AddToAudit(p_web)
  
  
  DO ValidateUnitDetails
  
  do Value::buttonFailAccessory
  do Comment::buttonFailAccessory ! allows comment style to be updated.
  do Value::TagValidateLoanAccessories  !1
  do Value::buttonConfirmDespatch  !1
  do Value::buttonConfirmMismatch  !1
  do Value::buttonValidateAccessories  !1
  do Value::buttonValidateJobDetails  !1
  do Value::locErrorMessage  !1
  do Value::locIMEINumber  !1
  do Comment::locIMEINumber
  do Value::locJobNumber  !1
  do Comment::locJobNumber
  do Value::locAccessoryMessage  !1
  do Value::locAccessoryErrorMessage  !1
  do Prompt::cou:Courier
  do Value::cou:Courier  !1
  do Prompt::locConsignmentNumber
  do Value::locConsignmentNumber  !1

ValidateValue::buttonFailAccessory  Routine
    If not (NOT (p_web.GSV('AccessoryConfirmationRequired') = 1 AND p_web.GSV('AccessoriesValidated') <> 1))
    End

Value::buttonFailAccessory  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(NOT (p_web.GSV('AccessoryConfirmationRequired') = 1 AND p_web.GSV('AccessoriesValidated') <> 1),'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('IndividualDespatch_' & p_web._nocolon('buttonFailAccessory') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (NOT (p_web.GSV('AccessoryConfirmationRequired') = 1 AND p_web.GSV('AccessoriesValidated') <> 1))
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonFailAccessory'',''individualdespatch_buttonfailaccessory_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','FailAccessory','Fail Accessory Validation',p_web.combine(Choose('Fail Accessory Validation' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'button-entryfield'),loc:formname,,,,loc:javascript,loc:disabled,'\images\cross.png',,,,,,,,,'nt-left')!h
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonFailAccessory  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonFailAccessory:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(NOT (p_web.GSV('AccessoryConfirmationRequired') = 1 AND p_web.GSV('AccessoriesValidated') <> 1),'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('IndividualDespatch_' & p_web._nocolon('buttonFailAccessory') & '_comment',loc:class,Net:NoSend)
  If NOT (p_web.GSV('AccessoryConfirmationRequired') = 1 AND p_web.GSV('AccessoriesValidated') <> 1)
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locAccessoryPasswordMessage  Routine
  packet = clip(packet) & p_web.DivHeader('IndividualDespatch_' & p_web._nocolon('locAccessoryPasswordMessage') & '_prompt',Choose(p_web.GSV('locAccessoryPasswordMessage') = '','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('locAccessoryPasswordMessage') = '','',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locAccessoryPasswordMessage  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locAccessoryPasswordMessage = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locAccessoryPasswordMessage = p_web.GetValue('Value')
  End
  do ValidateValue::locAccessoryPasswordMessage  ! copies value to session value if valid.
  do Comment::locAccessoryPasswordMessage ! allows comment style to be updated.

ValidateValue::locAccessoryPasswordMessage  Routine
    If not (p_web.GSV('locAccessoryPasswordMessage') = '')
      if loc:invalid = '' then p_web.SetSessionValue('locAccessoryPasswordMessage',locAccessoryPasswordMessage).
    End

Value::locAccessoryPasswordMessage  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('locAccessoryPasswordMessage') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('IndividualDespatch_' & p_web._nocolon('locAccessoryPasswordMessage') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (p_web.GSV('locAccessoryPasswordMessage') = '')
  ! --- DISPLAY --- locAccessoryPasswordMessage
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('red bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locAccessoryPasswordMessage'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locAccessoryPasswordMessage  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locAccessoryPasswordMessage:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('locAccessoryPasswordMessage') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('IndividualDespatch_' & p_web._nocolon('locAccessoryPasswordMessage') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('locAccessoryPasswordMessage') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::cou:Courier  Routine
  packet = clip(packet) & p_web.DivHeader('IndividualDespatch_' & p_web._nocolon('cou:Courier') & '_prompt',Choose(NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1),'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1),'',p_web.Translate('Courier'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::cou:Courier  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    cou:Courier = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    cou:Courier = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::cou:Courier  ! copies value to session value if valid.
  DO ShowConsignmentNumber
  do Value::cou:Courier
  do SendAlert
  do Comment::cou:Courier ! allows comment style to be updated.
  do Prompt::locConsignmentNumber
  do Value::locConsignmentNumber  !1

ValidateValue::cou:Courier  Routine
    If not (NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1))
      if loc:invalid = '' then p_web.SetSessionValue('cou:Courier',cou:Courier).
    End

Value::cou:Courier  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1),'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('IndividualDespatch_' & p_web._nocolon('cou:Courier') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,p_web.site.style.formselect,'FormEntry',)
  If loc:retrying
    cou:Courier = p_web.RestoreValue('cou:Courier')
    do ValidateValue::cou:Courier
    If cou:Courier:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1))
  ! --- DROPLIST ---
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''cou:Courier'',''individualdespatch_cou:courier_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('cou:Courier')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('cou:Courier',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
  loc:fieldclass = ''
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('cou:Courier') = 0
    p_web.SetSessionValue('cou:Courier','')
  end
    packet = clip(packet) & p_web.CreateOption('','',choose('' = p_web.getsessionvalue('cou:Courier')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(LOANACC)
  bind(lac:Record)
  p_web._OpenFile(JOBACC)
  bind(jac:Record)
  p_web._OpenFile(WEBJOB)
  bind(wob:Record)
  p_web._OpenFile(WAYBILLJ)
  bind(waj:Record)
  p_web._OpenFile(WAYBILLS)
  bind(way:Record)
  p_web._OpenFile(TagFile)
  bind(tag:Record)
  p_web._OpenFile(COURIER)
  bind(cou:Record)
  p_web._OpenFile(MULDESPJ)
  bind(mulj:Record)
  p_web._OpenFile(SUBTRACC)
  bind(sub:Record)
  p_web._OpenFile(LOAN)
  bind(loa:Record)
  p_web._OpenFile(TRADEACC)
  bind(tra:Record)
  p_web._OpenFile(EXCHANGE)
  bind(xch:Record)
  p_web._OpenFile(JOBS)
  bind(job:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(cou:Courier_OptionView)
  cou:Courier_OptionView{prop:order} = p_web.CleanFilter(cou:Courier_OptionView,'UPPER(cou:Courier)')
  Set(cou:Courier_OptionView)
  Loop
    Next(cou:Courier_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('cou:Courier') = 0
      p_web.SetSessionValue('cou:Courier',cou:Courier)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,cou:Courier,choose(cou:Courier = p_web.getsessionvalue('cou:Courier')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(cou:Courier_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(LOANACC)
  p_Web._CloseFile(JOBACC)
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(WAYBILLJ)
  p_Web._CloseFile(WAYBILLS)
  p_Web._CloseFile(TagFile)
  p_Web._CloseFile(COURIER)
  p_Web._CloseFile(MULDESPJ)
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(LOAN)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(EXCHANGE)
  p_Web._CloseFile(JOBS)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web.DivFooter()
Comment::cou:Courier  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if cou:Courier:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1),'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('IndividualDespatch_' & p_web._nocolon('cou:Courier') & '_comment',loc:class,Net:NoSend)
  If NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1)
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locConsignmentNumber  Routine
  packet = clip(packet) & p_web.DivHeader('IndividualDespatch_' & p_web._nocolon('locConsignmentNumber') & '_prompt',Choose(p_web.GSV('Show:ConsignmentNumber') <> 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Show:ConsignmentNumber') <> 1,'',p_web.Translate('Consignment Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locConsignmentNumber  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locConsignmentNumber = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locConsignmentNumber = p_web.GetValue('Value')
  End
  do ValidateValue::locConsignmentNumber  ! copies value to session value if valid.
  do Value::locConsignmentNumber
  do SendAlert
  do Comment::locConsignmentNumber ! allows comment style to be updated.

ValidateValue::locConsignmentNumber  Routine
    If not (p_web.GSV('Show:ConsignmentNumber') <> 1)
    locConsignmentNumber = Upper(locConsignmentNumber)
      if loc:invalid = '' then p_web.SetSessionValue('locConsignmentNumber',locConsignmentNumber).
    End

Value::locConsignmentNumber  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Show:ConsignmentNumber') <> 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('IndividualDespatch_' & p_web._nocolon('locConsignmentNumber') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    locConsignmentNumber = p_web.RestoreValue('locConsignmentNumber')
    do ValidateValue::locConsignmentNumber
    If locConsignmentNumber:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('Show:ConsignmentNumber') <> 1)
  ! --- STRING --- locConsignmentNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locConsignmentNumber'',''individualdespatch_locconsignmentnumber_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locConsignmentNumber',p_web.GetSessionValueFormat('locConsignmentNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locConsignmentNumber  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locConsignmentNumber:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('Show:ConsignmentNumber') <> 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('IndividualDespatch_' & p_web._nocolon('locConsignmentNumber') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Show:ConsignmentNumber') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locSecurityPackID  Routine
  packet = clip(packet) & p_web.DivHeader('IndividualDespatch_' & p_web._nocolon('locSecurityPackID') & '_prompt',Choose(NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1),'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1),'',p_web.Translate('Security Pack ID'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locSecurityPackID  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locSecurityPackID = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locSecurityPackID = p_web.GetValue('Value')
  End
  do ValidateValue::locSecurityPackID  ! copies value to session value if valid.
  do Value::locSecurityPackID
  do SendAlert
  do Comment::locSecurityPackID ! allows comment style to be updated.

ValidateValue::locSecurityPackID  Routine
    If not (NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1))
      if loc:invalid = '' then p_web.SetSessionValue('locSecurityPackID',locSecurityPackID).
    End

Value::locSecurityPackID  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1),'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('IndividualDespatch_' & p_web._nocolon('locSecurityPackID') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    locSecurityPackID = p_web.RestoreValue('locSecurityPackID')
    do ValidateValue::locSecurityPackID
    If locSecurityPackID:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1))
  ! --- STRING --- locSecurityPackID
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locSecurityPackID'',''individualdespatch_locsecuritypackid_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locSecurityPackID',p_web.GetSessionValueFormat('locSecurityPackID'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locSecurityPackID  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locSecurityPackID:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1),'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('IndividualDespatch_' & p_web._nocolon('locSecurityPackID') & '_comment',loc:class,Net:NoSend)
  If NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1)
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonConfirmDespatch  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonConfirmDespatch  ! copies value to session value if valid.
  do Value::buttonConfirmDespatch
  do Comment::buttonConfirmDespatch ! allows comment style to be updated.

ValidateValue::buttonConfirmDespatch  Routine
    If not (NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1))
    End

Value::buttonConfirmDespatch  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1),'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('IndividualDespatch_' & p_web._nocolon('buttonConfirmDespatch') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1))
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonConfirmDespatch'',''individualdespatch_buttonconfirmdespatch_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','ConfirmDespatch','Confirm Despatch',p_web.combine(Choose('Confirm Despatch' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'button-entryfield'),loc:formname,,,p_web.WindowOpen(clip('DespatchConfirmation')&''&'','_self'),,loc:disabled,,,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonConfirmDespatch  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonConfirmDespatch:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1),'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('IndividualDespatch_' & p_web._nocolon('buttonConfirmDespatch') & '_comment',loc:class,Net:NoSend)
  If NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1)
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('IndividualDespatch_nexttab_' & 0)
    locJobNumber = p_web.GSV('locJobNumber')
    do ValidateValue::locJobNumber
    If loc:Invalid
      loc:retrying = 1
      do Value::locJobNumber
      !do SendAlert
      do Comment::locJobNumber ! allows comment style to be updated.
      !exit
    End
    locIMEINumber = p_web.GSV('locIMEINumber')
    do ValidateValue::locIMEINumber
    If loc:Invalid
      loc:retrying = 1
      do Value::locIMEINumber
      !do SendAlert
      do Comment::locIMEINumber ! allows comment style to be updated.
      !exit
    End
    locErrorMessage = p_web.GSV('locErrorMessage')
    do ValidateValue::locErrorMessage
    If loc:Invalid
      loc:retrying = 1
      do Value::locErrorMessage
      !do SendAlert
      do Comment::locErrorMessage ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('IndividualDespatch_nexttab_' & 1)
    locAccessoryMessage = p_web.GSV('locAccessoryMessage')
    do ValidateValue::locAccessoryMessage
    If loc:Invalid
      loc:retrying = 1
      do Value::locAccessoryMessage
      !do SendAlert
      do Comment::locAccessoryMessage ! allows comment style to be updated.
      !exit
    End
    locAccessoryErrorMessage = p_web.GSV('locAccessoryErrorMessage')
    do ValidateValue::locAccessoryErrorMessage
    If loc:Invalid
      loc:retrying = 1
      do Value::locAccessoryErrorMessage
      !do SendAlert
      do Comment::locAccessoryErrorMessage ! allows comment style to be updated.
      !exit
    End
    locAccessoryPassword = p_web.GSV('locAccessoryPassword')
    do ValidateValue::locAccessoryPassword
    If loc:Invalid
      loc:retrying = 1
      do Value::locAccessoryPassword
      !do SendAlert
      do Comment::locAccessoryPassword ! allows comment style to be updated.
      !exit
    End
    locAccessoryPasswordMessage = p_web.GSV('locAccessoryPasswordMessage')
    do ValidateValue::locAccessoryPasswordMessage
    If loc:Invalid
      loc:retrying = 1
      do Value::locAccessoryPasswordMessage
      !do SendAlert
      do Comment::locAccessoryPasswordMessage ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('IndividualDespatch_nexttab_' & 2)
    cou:Courier = p_web.GSV('cou:Courier')
    do ValidateValue::cou:Courier
    If loc:Invalid
      loc:retrying = 1
      do Value::cou:Courier
      !do SendAlert
      do Comment::cou:Courier ! allows comment style to be updated.
      !exit
    End
    locConsignmentNumber = p_web.GSV('locConsignmentNumber')
    do ValidateValue::locConsignmentNumber
    If loc:Invalid
      loc:retrying = 1
      do Value::locConsignmentNumber
      !do SendAlert
      do Comment::locConsignmentNumber ! allows comment style to be updated.
      !exit
    End
    locSecurityPackID = p_web.GSV('locSecurityPackID')
    do ValidateValue::locSecurityPackID
    If loc:Invalid
      loc:retrying = 1
      do Value::locSecurityPackID
      !do SendAlert
      do Comment::locSecurityPackID ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_IndividualDespatch_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('IndividualDespatch_tab_' & 0)
    do GenerateTab0
  of lower('IndividualDespatch_locJobNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locJobNumber
      of event:timer
        do Value::locJobNumber
        do Comment::locJobNumber
      else
        do Value::locJobNumber
      end
  of lower('IndividualDespatch_locIMEINumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locIMEINumber
      of event:timer
        do Value::locIMEINumber
        do Comment::locIMEINumber
      else
        do Value::locIMEINumber
      end
  of lower('IndividualDespatch_buttonValidateJobDetails_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonValidateJobDetails
      of event:timer
        do Value::buttonValidateJobDetails
        do Comment::buttonValidateJobDetails
      else
        do Value::buttonValidateJobDetails
      end
  of lower('IndividualDespatch_tab_' & 1)
    do GenerateTab1
  of lower('IndividualDespatch_buttonValidateAccessories_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonValidateAccessories
      of event:timer
        do Value::buttonValidateAccessories
        do Comment::buttonValidateAccessories
      else
        do Value::buttonValidateAccessories
      end
  of lower('IndividualDespatch_locAccessoryPassword_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locAccessoryPassword
      of event:timer
        do Value::locAccessoryPassword
        do Comment::locAccessoryPassword
      else
        do Value::locAccessoryPassword
      end
  of lower('IndividualDespatch_buttonConfirmMismatch_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonConfirmMismatch
      of event:timer
        do Value::buttonConfirmMismatch
        do Comment::buttonConfirmMismatch
      else
        do Value::buttonConfirmMismatch
      end
  of lower('IndividualDespatch_buttonFailAccessory_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonFailAccessory
      of event:timer
        do Value::buttonFailAccessory
        do Comment::buttonFailAccessory
      else
        do Value::buttonFailAccessory
      end
  of lower('IndividualDespatch_tab_' & 2)
    do GenerateTab2
  of lower('IndividualDespatch_cou:Courier_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::cou:Courier
      of event:timer
        do Value::cou:Courier
        do Comment::cou:Courier
      else
        do Value::cou:Courier
      end
  of lower('IndividualDespatch_locConsignmentNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locConsignmentNumber
      of event:timer
        do Value::locConsignmentNumber
        do Comment::locConsignmentNumber
      else
        do Value::locConsignmentNumber
      end
  of lower('IndividualDespatch_locSecurityPackID_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locSecurityPackID
      of event:timer
        do Value::locSecurityPackID
        do Comment::locSecurityPackID
      else
        do Value::locSecurityPackID
      end
  of lower('IndividualDespatch_buttonConfirmDespatch_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonConfirmDespatch
      of event:timer
        do Value::buttonConfirmDespatch
        do Comment::buttonConfirmDespatch
      else
        do Value::buttonConfirmDespatch
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('IndividualDespatch_form:ready_',1)

  p_web.SetSessionValue('IndividualDespatch_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_IndividualDespatch',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('IndividualDespatch_form:ready_',1)
  p_web.SetSessionValue('IndividualDespatch_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_IndividualDespatch',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('IndividualDespatch_form:ready_',1)
  p_web.SetSessionValue('IndividualDespatch_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('IndividualDespatch:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('IndividualDespatch_form:ready_',1)
  p_web.SetSessionValue('IndividualDespatch_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('IndividualDespatch:Primed',0)
  p_web.setsessionvalue('showtab_IndividualDespatch',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
      If not (1=0)
        If not (p_web.GSV('UnitValidated') = 1)
          If p_web.IfExistsValue('locJobNumber')
            locJobNumber = p_web.GetValue('locJobNumber')
          End
        End
      End
      If not (1=0)
        If not (p_web.GSV('UnitValidated') = 1)
          If p_web.IfExistsValue('locIMEINumber')
            locIMEINumber = p_web.GetValue('locIMEINumber')
          End
        End
      End
      If not (p_web.GSV('AccessoryPasswordRequired') <> 1)
          If p_web.IfExistsValue('locAccessoryPassword')
            locAccessoryPassword = p_web.GetValue('locAccessoryPassword')
          End
      End
      If not (NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1))
          If p_web.IfExistsValue('cou:Courier')
            cou:Courier = p_web.GetValue('cou:Courier')
          End
      End
      If not (p_web.GSV('Show:ConsignmentNumber') <> 1)
          If p_web.IfExistsValue('locConsignmentNumber')
            locConsignmentNumber = p_web.GetValue('locConsignmentNumber')
          End
      End
      If not (NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1))
          If p_web.IfExistsValue('locSecurityPackID')
            locSecurityPackID = p_web.GetValue('locSecurityPackID')
          End
      End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('IndividualDespatch_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('IndividualDespatch_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
    loc:InvalidTab += 1
    do ValidateValue::locJobNumber
    If loc:Invalid then exit.
    do ValidateValue::locIMEINumber
    If loc:Invalid then exit.
    do ValidateValue::buttonValidateJobDetails
    If loc:Invalid then exit.
    do ValidateValue::locErrorMessage
    If loc:Invalid then exit.
  ! tab = 4
    loc:InvalidTab += 1
    do ValidateValue::TagValidateLoanAccessories
    If loc:Invalid then exit.
    do ValidateValue::buttonValidateAccessories
    If loc:Invalid then exit.
    do ValidateValue::locAccessoryMessage
    If loc:Invalid then exit.
    do ValidateValue::locAccessoryErrorMessage
    If loc:Invalid then exit.
    do ValidateValue::locAccessoryPassword
    If loc:Invalid then exit.
    do ValidateValue::buttonConfirmMismatch
    If loc:Invalid then exit.
    do ValidateValue::buttonFailAccessory
    If loc:Invalid then exit.
    do ValidateValue::locAccessoryPasswordMessage
    If loc:Invalid then exit.
  ! tab = 5
    loc:InvalidTab += 1
    do ValidateValue::cou:Courier
    If loc:Invalid then exit.
    do ValidateValue::locConsignmentNumber
    If loc:Invalid then exit.
    do ValidateValue::locSecurityPackID
    If loc:Invalid then exit.
    do ValidateValue::buttonConfirmDespatch
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('IndividualDespatch:Primed',0)
  p_web.StoreValue('locJobNumber')
  p_web.StoreValue('locIMEINumber')
  p_web.StoreValue('')
  p_web.StoreValue('locErrorMessage')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locAccessoryMessage')
  p_web.StoreValue('locAccessoryErrorMessage')
  p_web.StoreValue('locAccessoryPassword')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locAccessoryPasswordMessage')
  p_web.StoreValue('cou:Courier')
  p_web.StoreValue('locConsignmentNumber')
  p_web.StoreValue('locSecurityPackID')
  p_web.StoreValue('')

