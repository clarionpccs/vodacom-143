

   MEMBER('WebServer_Phase4.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSE008.INC'),ONCE        !Local module procedure declarations
                     END


JobInUse             PROCEDURE  (fSessionValue)            ! Declare Procedure
  CODE
    rtnValue# = 0
    pointer# = Pointer(JOBS)
    Hold(JOBS,1)
    Get(JOBS,pointer#)
    if (errorcode()  = 43)
        rtnValue# = 1
    else
        relate:JOBSLOCK.open()

        ! First step. Remove any blanks from the file
        Access:JOBSLOCK.ClearKey(lock:JobNumberKey)
        lock:JobNumber = 0
        SET(lock:JobNumberKey,lock:JobNumberKey)
        LOOP
            IF (Access:JOBSLOCK.Next())
                BREAK
            END
            IF (lock:JobNumber <> 0)
                BREAK
            END
            Access:JOBSLOCK.DeleteRecord(0)
        END

        access:JOBSLOCK.clearkey(lock:jobNumberKey)
        lock:jobNUmber = job:ref_number
        if (access:JOBSLOCK.tryfetch(lock:jobNumberKey) = level:benign)
            if (lock:SessionValue <> fSessionValue)
                if (TODAY() = lock:DateLocked and clock() < (lock:timelocked + 60000)) ! 10 Minutes Time Out
                ! If it's the same user then there's no need to give the locked error?!
                    rtnValue# = 1
                end

            end
        end
        relate:JOBSLOCK.close()


    end ! if (errorcode()  = 43)
    return rtnValue#
isJobInvoiced        PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
FilesOpened     BYTE(0)
  CODE
    do openFiles

    p_web.SSV('IsJobInvoiced',0)
    if (p_web.GSV('job:Invoice_Number') > 0)
        Access:INVOICE.Clearkey(inv:invoice_Number_Key)
        inv:invoice_Number    = p_web.GSV('job:Invoice_Number')
        if (Access:INVOICE.TryFetch(inv:invoice_Number_Key) = Level:Benign)
            ! Found
            if (p_web.GSV('BookingSite') = 'RRC')
                if (inv:RRCINvoiceDate > 0)
                    p_web.SSV('IsJobInvoiced',1)
                end ! if (inv:RRCINvoiceDate > 0)
            else ! if (p_web.GSV('BookingSite') = 'RRC')
                if (inv:ARCInvoiceDate > 0)
                    p_web.SSV('IsJobInvoiced',1)
                end ! if (inv:ARCInvoiceDate > 0)
            end ! if (p_web.GSV('BookingSite') = 'RRC')
        else ! if (Access:INVOICE.TryFetch(inv:invoice_Number_Key) = Level:Benign)
            ! Error
        end ! if (Access:INVOICE.TryFetch(inv:invoice_Number_Key) = Level:Benign)
    end !if (p_web.GSV('job:Invoice_Number') = 0)

    do closeFiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:INVOICE.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:INVOICE.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:INVOICE.Close
     FilesOpened = False
  END
HubOutOfRegion       PROCEDURE  (f:HeadAccountNumber,f:Hub) ! Declare Procedure
TRAHUBS::State  USHORT
FilesOpened     BYTE(0)
  CODE
    If f:HeadAccountNumber = ''
        Return 0
    End ! If f:HeadAccountNumber = ''
    If f:Hub = ''
        Return 0
    End ! If f:Hub = ''
    Do OpenFiles
    Do SaveFiles

    Found# = 0
    Access:TRAHUBS.Clearkey(trh:HubKey)
    trh:TRADEACCAccountNumber = f:HeadAccountNumber
    Set(trh:HubKey,trh:HubKey)
    Loop
        If Access:TRAHUBS.Next()
            Break
        End ! If Access:TRAHUBS.Next()
        If trh:TRADEACCAccountNumber <> f:HeadAccountNumber
            Break
        End ! If trh:TRADEACCAccountNumber <> wob:HeadAccountNumber
        Found# = 1
        If trh:Hub = f:Hub
            Found# = 2
            Break
        End ! If trh:Hub = f:Hub
    End ! Loop

    Do RestoreFiles
    Do CloseFiles
    !0 = No Relevant Hub Data
    !1 = Hub is out of region
    !2 = Found Hub.
    Return Found#

!--------------------------------------
OpenFiles  ROUTINE
  Access:TRAHUBS.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRAHUBS.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:TRAHUBS.Close
     FilesOpened = False
  END
SaveFiles  ROUTINE
  TRAHUBS::State = Access:TRAHUBS.SaveFile()               ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF TRAHUBS::State <> 0
    Access:TRAHUBS.RestoreFile(TRAHUBS::State)             ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
NextWaybillNumber    PROCEDURE                             ! Declare Procedure
locKeyField          LONG                                  !
locWaybillNumber     LONG                                  !
WAYBILLS::State  USHORT
FilesOpened     BYTE(0)
  CODE
    DO OpenFiles
    DO SaveFiles

    ! Remove blanks
    Access:WAYBILLS.ClearKey(way:WayBillNumberKey)
    way:WayBillNumber = 0
    SET(way:WayBillNumberKey,way:WayBillNumberKey)
    LOOP UNTIL Access:WAYBILLS.Next()
        IF (way:WayBillNumber <> 0)
            BREAK
        END
        Relate:WAYBILLS.delete(0)
    END

    locWaybillNumber = 0
    locKeyField = 0
    LOOP
        Access:WAYBILLS.ClearKey(way:WayBillNumberKey)
        way:WayBillNumber = 99999999
        SET(way:WayBillNumberKey,way:WayBillNumberKey)
        LOOP
            IF (Access:WAYBILLS.Previous())
                locKeyfield = 1
                BREAK
            END
            locKeyfield = way:WayBillNumber + 1
            BREAK
        END

        Access:WAYBILLS.ClearKey(way:WayBillNumberKey)
        way:WayBillNumber = locKeyField
        IF (Access:WAYBILLS.TryFetch(way:WayBillNumberKey) = Level:Benign)
            ! Double check the number doesn't already exists
            IF locKeyField = 1

                BREAK
            END
            CYCLE
        ELSE
            ! Doesn't exist. Add it
            IF (Access:WAYBILLS.PrimeRecord() = Level:Benign)
                way:WayBillNumber = locKeyField
                IF (Access:WAYBILLS.TryInsert() = Level:Benign)
                    locWaybillNumber = way:WayBillNumber
                    BREAK
                ELSE
                    Access:WAYBILLS.CancelAutoInc()
                END
            END
        END

    END

    DO RestoreFiles
    DO CloseFiles

    RETURN locWayBillNumber


!--------------------------------------
OpenFiles  ROUTINE
  Access:WAYBILLS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:WAYBILLS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:WAYBILLS.Close
     FilesOpened = False
  END
SaveFiles  ROUTINE
  WAYBILLS::State = Access:WAYBILLS.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF WAYBILLS::State <> 0
    Access:WAYBILLS.RestoreFile(WAYBILLS::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
Despatch:Loa         PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
FilesOpened     BYTE(0)
  CODE
    Do OpenFiles
    if (p_web.GSV('BookingSite') = 'RRC')


        ! Add To COnsignment History
        IF (Access:JOBSCONS.PrimeRecord() = Level:Benign)
            joc:RefNumber = p_web.GSV('job:Ref_Number')
            joc:TheDate = TODAY()
            joc:TheTime = CLOCK()
            joc:UserCode = p_web.GSV('BookingUserCode')
            joc:DespatchFrom = 'RRC'
            joc:DespatchTo = 'CUSTOMER'
            joc:Courier = p_web.GSV('job:Loan_Courier')
            joc:ConsignmentNumber = p_web.GSV('locWaybillNumber')
            joc:DespatchType = 'LOA'
            IF (Access:JOBSCONS.TryInsert())
                Access:JOBSCONS.CancelAutoInc()
            END
        END


        p_web.SSV('wob:ReadyToDespatch',0)

        p_web.SSV('AddToAudit:Action','DESPATCH FROM RRC')
        p_web.SSV('AddToAudit:Notes','UNIT NO: ' & p_web.GSV('job:Loan_Unit_Number') & |
            '<13,10>COURIER: ' & p_web.GSV('job:Loan_Courier') & |
            '<13,10>WAYBILL NO: ' & p_web.GSV('locWaybillNumber'))



    ELSE ! if (p_web.GSV('BookingSite') = 'RRC')
        p_web.SSV('job:Loan_Consignment_Number',p_web.GSV('locWaybillNumber'))
        p_web.SSV('job:Loan_Despatched',Today())

        ! Add To COnsignment History
        IF (Access:JOBSCONS.PrimeRecord() = Level:Benign)
            joc:RefNumber = p_web.GSV('job:Ref_Number')
            joc:TheDate = TODAY()
            joc:TheTime = CLOCK()
            joc:UserCode = p_web.GSV('BookingUserCode')
            joc:DespatchFrom = 'ARC'
            joc:DespatchTo = 'CUSTOMER'
            joc:Courier = p_web.GSV('job:Loan_Courier')
            joc:ConsignmentNumber = p_web.GSV('locWaybillNumber')
            joc:DespatchType = 'LOA'
            IF (Access:JOBSCONS.TryInsert())
                Access:JOBSCONS.CancelAutoInc()
            END
        END

        p_web.SSV('AddToAudit:Action','LOAN UNIT DESPATCHED VIA ' & p_web.GSV('job:Loan_Courier'))
        p_web.SSV('AddToAudit:Notes','CONSIGNMENT NOTE NUMBER: ' & p_web.GSV('locWaybillNumber'))

        Access:LOAN.Clearkey(loa:Ref_Number_Key)
        loa:Ref_Number = p_web.GSV('job:Loan_Unit_Number')
        if (Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign)
            loa:Available = 'DES'
            if (Access:LOAN.TryUpdate() = Level:Benign)
                If (Access:LOANHIST.PrimeRecord() = Level:Benign)
                    loh:Ref_Number = loa:Ref_Number
                    loh:Date = Today()
                    loh:Time = Clock()
                    loh:User = p_web.GSV('BookingUserCode')
                    loh:Status = 'UNIT DESPATCHED ON JOB: ' & p_web.GSV('job:Ref_Number')
                    Access:LOANHIST.TryInsert()
                END
            END
        END


    END ! if (p_web.GSV('BookingSite') = 'RRC')

        GetStatus(910,0,'LOA',p_web)

    p_web.SSV('jobe:LoaSecurityPackNo',p_web.GSV('locSecurityPackID'))


    ! Add To Audit
    p_web.SSV('AddToAudit:Type','LOA')
    IF (p_web.GSV('locSecurityPackID') <> '')
        p_web.SSV('AddToAudit:Notes',p_web.GSV('AddToAudit:Notes') & |
            '<13,10>SECURITY PACK NO: ' & p_web.GSV('locSecurityPackID'))
    END
    AddToAudit(p_web)
    Do CloseFiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:LOANHIST.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:LOANHIST.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:LOAN.Open                                         ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:LOAN.UseFile                                      ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSCONS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSCONS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:LOANHIST.Close
     Access:LOAN.Close
     Access:JOBSCONS.Close
     FilesOpened = False
  END
