///////////////////////////////////////////////////////
//   
//   jQuery Plugin to turn form into wizard.
//   Part of NetTalk by CapeSoft 
//   (c) 2010 
//
///////////////////////////////////////////////////////

// first a small plugin to get, or set, the maximum height of a collection of elements.
(function( $ ){
  $.fn.maxHeight = function(h) {
		if (arguments.length > 0){
			this.each(function() {
				$(this).height(h);
			});
			return this;
		} else {
			var max = 0;
			this.each(function() {
				max = Math.max( max, $(this).height() );
			});
			return max;
		}	
  };
})( jQuery );

///////////////////////////////////////////////////////
// now the main ntwiz plugin.
///////////////////////////////////////////////////////

(function( $, undefined ) {

$.widget( "ui.ntwiz", {
	options: {
	  procedure: '',
	  validateOnNext: 0,
		activeTab: 0,
		maxTab: 0,
		wizTabs: [],
		minHeight: 0,
		hidden: []
	},

//------------------------------------------------------
	_create: function() {
	  var _this = this;
		this.element.addClass( "ui-widget ui-widget-content ui-corner-all" );
		if (this.options.validateOnNext == 0){	
			$('[name="wiznext_btn"]').unbind('click.wiz').bind('click.wiz',function(e){
					_this.next();
				});
		} else {
			$('[name="wiznext_btn"]').unbind('click.wiz').bind('click.wiz',function(e){
					_this.tryNext();
				});
		}		
		$('[name="wizprevious_btn"]').unbind('click.wiz').bind('click.wiz',function(e){
			_this.previous();
			});
		this.options.wizTabs = this.element.find('> div');
		this.options.maxTab=this.options.wizTabs.length-1;
		
		var wizHeight = this.element.find('> div').maxHeight();
		if (this.options.minHeight > wizHeight){
		  wizHeight = this.options.minHeight;
		}
		// set them all to be the same height.
		this.element.find('> div').maxHeight(wizHeight).hide();
		
	},

//------------------------------------------------------
	_init: function() {
		this.activeTab(this.activeTab());
	},	

//------------------------------------------------------
	destroy: function() {
		this.element.removeClass( "ui-widget ui-widget-content ui-corner-all" );
		$.Widget.prototype.destroy.apply( this, arguments );
	},

//------------------------------------------------------
// shortcut to // .option("activeTab", //
	activeTab: function( newValue ) { 
		if ( newValue === undefined ) {
			return this.options.activeTab;
		}
		this._setOption( "activeTab", newValue );
		return this;
	},

//------------------------------------------------------
	_setOption: function( key, value ) {
		switch (key){
		case "activeTab":
			$(this.options.wizTabs[this.options.activeTab]).hide();
			this.options.activeTab = value;
			$(this.options.wizTabs[this.options.activeTab]).show();
			this.setButtons();
			$(this.options.wizTabs[this.options.activeTab]).find(':input:enabled:visible:first').focus();
			break;
		case "maxTab":	
			this.options.maxTab = value;
			break;
		case "hideTab":
			this.options.hidden[value] = 1;
			break;
		case "unhideTab":
			this.options.hidden[value] = 0;
			break
		} 
		$.Widget.prototype._setOption.apply( this, arguments );
	},

//------------------------------------------------------
  setButtons : function(){
		if (this.activeTab() == 0){
			$('[name="wizprevious_btn"]').button( "option", "disabled", true ).removeClass('ui-state-focus ui-state-hover');
		}  else {
			$('[name="wizprevious_btn"]').button( "option", "disabled", false ).removeClass('ui-state-focus ui-state-hover');
		}
		if (this.activeTab() == this.options.maxTab){
			$('[name="wiznext_btn"]').button( "option", "disabled", true ).removeClass('ui-state-focus ui-state-hover');
			$('[name="save_btn"]').button( "option", "disabled", false ).removeClass('ui-state-focus ui-state-hover');
		}  else {
			$('[name="wiznext_btn"]').button( "option", "disabled", false ).removeClass('ui-state-focus ui-state-hover');
			$('[name="save_btn"]').button( "option", "disabled", true ).removeClass('ui-state-focus ui-state-hover');
		} 
		return this;
  },
//------------------------------------------------------  
  tryNext : function() {  
    var parms = '';
		jQuery.get(this.options.procedure + '_nexttab_' + this.options.activeTab,parms,function(data){xmlProcess(data);});
  },

//------------------------------------------------------  
  next : function() {  
		for(var n = this.activeTab()+1; n <= this.options.maxTab; n++){
			if (this.options.hidden[n] != 1){
				this.activeTab(n);
				break;
			} 
		}
  },
//------------------------------------------------------  
  previous : function() {  
		for(var n = this.activeTab()-1; n >= 0; n--){
			if (this.options.hidden[n] != 1){
				this.activeTab(n);
				break;
			} 
		}
		return this;
  }
//------------------------------------------------------
});

$.extend( $.ui.ntwiz, {
	version: "@VERSION"
});

})( jQuery );

///////////////////////////////////////////////////////
// end ntwiz
///////////////////////////////////////////////////////
