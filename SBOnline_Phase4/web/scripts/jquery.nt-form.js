var ntLookupKey = 113;

(function( $, undefined ) {

// var equates and global variables here/

var tAccordion = 1;
var tTab       = 2;
var tPlain     = 3;
var tRound     = 4;
var tTabXP     = 5;
var tNone      = 6;
var tWizard    = 7;

$.widget("ui.ntform", {
   // default options
	options: {
		defaultButton: '',
		tabType: 0,
		procedure: ''
   },
//------------------------------------------------------    
		_create: function() {
			var _this = this;
			$(this.element).find('input').not('.nt-locator')
				.bind('keypress.ntform',function(e){return _this._onKeyPress(this,e)})
				.bind('keydown.ntform',function(e){return _this._onKeyDown(this,e)});
			this.firstFocus();
			
		},	
//------------------------------------------------------      		
		_onKeyPress: function(inp,e) {
			switch (e.which) {
				case 13:{
					return(this._onEnter(e));
					break;
				}  
			}	
		},	
//------------------------------------------------------      		
		_onKeyDown: function(inp,e) {
			if ((e.which == 191) && (e.shiftKey == true)){
				e.which = ntLookupKey;
			}
			switch (e.which) {
			  case 8:{ // explicity handle backspace on readonly fields for benefit of IE.
			    if (jQuery(inp).attr('readonly') == 'readonly'){ 
			      return false;
			    }
			    break;
			  }
				//case 191:  // ?
				case ntLookupKey: {// F2 by default
					jQuery("#"+inp.id+".hasDatepicker").each(
						function(i,v){
							e.preventDefault();
							jQuery(inp).datepicker("show");
							return false;
						}
					);
					jQuery("#"+inp.id).next(':button').each(
						function(i,v){
							jQuery(this).click();
							return false;
						}	
					)
				}
			}	
			return true;
		},			
//------------------------------------------------------      		
		_onEnter: function(e) {
			var _this = this;
			$(this.element).find('[data-nt-default="1"]').each(function(){
				$(this).click();
				e.preventDefault();
				return false;
			})
			return true;	
		},	
//------------------------------------------------------    
		hideTab: function(index){
			var id='';
			switch (this.options.tabType){
			case tNone:
      case tPlain:
			case tRound:
				jQuery('#tab_' + this.options.procedure + index + '_div').hide();
				break;
			case tWizard:
				jQuery('#tab_' + this.options.procedure + '_div').ntwiz("option","hideTab",index);
				break;	
			case tAccordion:
				jQuery('#tab_' + this.options.procedure + '_div').find('h3').eq(index).hide();
				break;	
			case tTab:
				jQuery('#tab_' + this.options.procedure + '_div > ul').find('li').eq(index).hide();
				break;	
			}	
		},
//------------------------------------------------------    		
		showTab: function(index){
			var id='';
			switch (this.options.tabType){
			case tNone:
      case tPlain:
			case tRound:
				jQuery('#tab_' + this.options.procedure + index + '_div').show();
				break;
			case tWizard:
				jQuery('#tab_' + this.options.procedure + '_div').ntwiz("option","unhideTab",index);
			case tAccordion:
				jQuery('#tab_' + this.options.procedure + '_div').find('h3').eq(index).show();
				break;	
			case tTab:
				jQuery('#tab_' + this.options.procedure + '_div > ul').find('li').eq(index).show();
				break;	
			}
		},
//------------------------------------------------------    
		firstFocus: function(){
			var e;
			var t = 4000000000;
			$(this.element).find(' :input').not('[readonly],[disabled],[type="hidden"]').each(function(){
				tx = $(this).offset().top
				if (tx < t && tx != 0){
					e = this;
					t = tx;
				}
			})
			$(e).focus();
		},
		
//------------------------------------------------------    
   destroy: function() {
       $.Widget.prototype.destroy.apply(this, arguments); // default destroy
       // now do other stuff particular to this widget
   }
 });

$.extend( $.ui.ntform, {
	version: "@VERSION"
});

})( jQuery );