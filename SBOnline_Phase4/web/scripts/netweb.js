;
var NetTalkVersion = 5.51;
// ---------------------------------------------

var cnt=0;
var tcnt=0;
var fcnt='';
var icnt='';
function countDown(){
  hh = parseInt( cnt / 3600 );
  mm = parseInt( cnt / 60 ) % 60;
  ss = cnt % 60;
	var t = hh + ":" + (mm < 10 ? "0" + mm : mm) + ":" + (ss < 10 ? "0" + ss : ss);
  jQuery('#' + icnt).html(t);
  cnt -= 1;
  if (cnt ==0){
    window.open(fcnt,'_top');
  } else {
		setTimeout("countDown();",1000);  
  }
};

function resetCountDown(){
  cnt = tcnt;
}

function startCountDown(t,f,i){
  if (t){
		tcnt = t;
	}	
	if (f){
		fcnt = f;
	}
	if (i){
		icnt = i;
	}
	cnt = tcnt;
  countDown();
};

function versionCheck(v){
  var s = v + '';
  s = s.replace('.','');
  v = Number(v);
  if (v != NetTalkVersion){
    jQuery('#_ver' + s).html('UPDATE OF WEB FOLDER REQUIRED - Try pressing Ctrl-F5. Server is on version ' + v + ' but web folder is on version ' +  NetTalkVersion);
  } else {
		jQuery('#_ver' + s).hide();
	}	
}
// ---------------------------------------------
// functions to handle busy graphic
var busyCounter=0;
jQuery(function(){
        busyCounter = 0;
        jQuery("#_busy").hide();
});

jQuery("html").bind("ajaxSend",function(){
        //jQuery("#_busy").position({my: "center", at:"center", of: window});
        jQuery("#_busy").show();
        busyCounter += 1;
        if (jQuery.browser.msie) {
                jQuery('input:radio, input:checkbox').unbind('click.iefix');
        };
});
jQuery("html").bind("ajaxComplete",function(){;
        busyCounter -= 1;
        //if (busyCounter == 0){
                jQuery("#_busy").hide();
        //};
        if (jQuery.browser.msie) {
                jQuery('input:radio, input:checkbox').bind('click.iefix',function () {
                        this.blur();
                        this.focus();
                });
        }
});
// ---------------------------------------------
function ntAlert(m){
  setTimeout(function() { 
		var a = jQuery(":focus");
		alert(unescape(m)); 
		 jQuery(a).focus();
	}, 1);
}
// ---------------------------------------------
var hadfocus='';
var setfocus='';
function afterSv(){
  GreenAll();
  applyHTML5();
  var d = document.getElementById('alert_div');
  if (d != null){
    var i = d.innerHTML;
    var r = /(<([^>]+)>)/gi;
    i = i.replace(r, "");
    if ((i != '') && (i != '\n') && (i != '\n\n') && (i != '<br />')){
      var thissound=document.getElementById('soundfail');
      if (thissound != null){
        thissound.Play();
      }
    } else {
      var thissound=document.getElementById('soundok');
      if (thissound != null){
        thissound.Play();
      }
    }
  }
}

var tables = [];
function GreenAll(){
  for(var e = 0; e < tables.length; e++){
     tables[e].table=document.getElementById(tables[e].tid); // necessary after ajax call
     if (tables[e].table != null){
       tables[e].parseCell();
       tables[e].applyGreenBar();
     }
     tables[e].makeResizable();
     tables[e].prepColumns();
     tables[e].bind();
     //tables[e].restoreFocus();
  }
}
// -----------------------------------------------------------------------------------
// AutoTab support
// If an entry field has data-nt-autotab=1", then when the maxlength is reached focus
// automatically moves to the next field.
// -----------------------------------------------------------------------------------
jQuery.fn.focusNextInputField = function() { // this function from http://jqueryminute.com/, thanks to jdSharp.
    return this.each(function() {
        var fields = $(this).parents('form:eq(0),body').find('button,input,textarea,select').not('[readonly]');
        var index = fields.index( this );
        if ( index > -1 && ( index + 1 ) < fields.length ) {
            fields.eq( index + 1 ).focus();
        }
        return false;
    });
};

jQuery(document).ready( function(){
	jQuery("body").delegate("[data-nt-autotab=1]","keyup",function(e) {
		if ($(this).val().length == $(this).attr("maxlength")){
			if((e.which >= 32) && (e.which <= 122)){
				jQuery(':focus').focusNextInputField();
			}		
		}	
	}); 
});

// -----------------------------------------------------------------------------------
// ntdialog object is a stack to manage popup dialogs
// a single var called ntd is created for each page at the end of the declaration.
//
// -----------------------------------------------------------------------------------
function ntdialog(){
this.level=-1;
this.luf=new Array();
this.sf=new Array();
this.dlg=new Array();
this.pmt=new Array();
this.open=new Array();
this.action=new Array();
this.calledfrom=new Array();
this.grandparent=new Array();
this.row=new Array();
this.other=new Array();
this.autosave=new Array();
this.oncomplete=new Array();
this.viewstate=new Array();
this.equate=new Array();
this.saved=new Array();
}
// ----
ntdialog.prototype.push = function (dlg,luf,pmt,run,action,sf,calledfrom,row,other,autosave,oncomplete,grandparent,viewstate,equate){
 this.level += 1;
 this.dlg[this.level] = dlg;
 this.luf[this.level] = luf;//lookupfield
 this.sf[this.level] = sf;//sortfield
 this.pmt[this.level] = pmt;//title
 this.row[this.level] = row;//row id
 this.other[this.level] = other;//other parameters
 this.autosave[this.level] = autosave;
 this.saved[this.level] = 0;
 this.open[this.level] = 0;
 if (action == null){
   this.action[this.level] = 0;
 } else {
   this.action[this.level] = action;
 }
 if (oncomplete){
   this.oncomplete[this.level] = oncomplete;
 } else {
   this.oncomplete[this.level] = '';
 }

 if (viewstate){
   this.viewstate[this.level] = viewstate;
 } else {
   this.viewstate[this.level] = '';
 }

 if (equate){
   this.equate[this.level] = equate;
 } else {
   this.equate[this.level] = '';
 }

 this.calledfrom[this.level] = calledfrom;
 this.grandparent[this.level] = grandparent;
 if (run == 1){
   this.run();
 }
}
// ----
ntdialog.prototype.current = function (){
  return this.dlg[this.level];
}
// ----
ntdialog.prototype.run = function (){
	var lvl = this.level;
	var _this = this;
	var _id = '#popup_'+this.dlg[this.level].toLowerCase()+'_div';
	if (this.level>-1){
		if (this.autosave[this.level] != 1){
			SetSessionValue('popup_'+this.dlg[this.level],1);
			jQuery(_id).dialog( "option", "title", this.pmt[this.level] ).dialog('open');
		}
		switch (this.action[this.level]){
		case 1: //insert
			jQuery.get(this.dlg[this.level],'pressedButton=insert_btn&_popup_=1&_bidv_='+this.row[this.level] + '&' + this.other[this.level],function(data){xmlProcess(data);firstFocus(_id);});
			break;
		case 2: //change
			if (this.autosave[this.level] != 1){
				jQuery.get(this.dlg[this.level],'pressedButton=change_btn&_popup_=1&_bidv_='+this.row[this.level] + '&' + this.other[this.level],function(data){xmlProcess(data);firstFocus(_id);});
			} else {
				jQuery.get(this.dlg[this.level],'pressedButton=change_btn&_popup_=1&_bidv_='+this.row[this.level] + '&_auto_='+this.calledfrom[this.level]+'&' + this.other[this.level],function(data){xmlProcess(data);firstFocus(_id);});
			}
			break;
//   case 3: //delete
//    jQuery.get(this.dlg[this.level],'pressedButton=delete_btn&_popup_=1&_bidv_='+this.row[this.level],function(data){xmlProcess(data);firstFocus(_id);});
//    break;
		case 4: // copy
			jQuery.get(this.dlg[this.level],'pressedButton=copy_btn&_popup_=1&_bidv_='+this.row[this.level] + '&' + this.other[this.level],function(data){xmlProcess(data);firstFocus(_id);});
			break;
		case 5: // view
			jQuery.get(this.dlg[this.level],'pressedButton=view_btn&_popup_=1&_bidv_='+this.row[this.level] + '&' + this.other[this.level],function(data){xmlProcess(data);firstFocus(_id);});
			break;
		case 6: //lookup
			jQuery.get(this.dlg[this.level],'Lookup_btn=1&Refresh=sort&_popup_=1&LookupField='+this.luf[this.level]+'&_sort='+this.sf[this.level] + '&' + this.other[this.level] + '&'+this.luf[this.level]+'='+jQuery('#'+ntd.getluf()).val(),function(data){xmlProcess(data);firstFocus(_id);});
			break;
//		case 7: //zoom in
//			jQuery.get(this.dlg[this.level],'_zoom_=1&_popup_=1&_date_=' + this.row[this.level],function(data){xmlProcess(data);firstFocus(_id); _this.oncomplete[lvl]();});
//			break;
		default:
				jQuery.get(this.dlg[this.level],'_popup_=1&_bidv_='+this.row[this.level] + '&' + this.other[this.level],function(data){xmlProcess(data);firstFocus(_id);});	
		}
		if (this.autosave[this.level] != 1){
			this.open[this.level]=1;
		}
	}
}
// ----
ntdialog.prototype.pop = function (dlg){
	if (this.level > -1){
		SetSessionValue('popup_'+this.dlg[this.level],0)
		if (dlg != undefined){
			jQuery(dlg).html('');
		} else {
			jQuery('#'+ntd.getdlg().toLowerCase()+'_div').html('');
		}
		this.open[this.level]=0;
		var svd = (this.saved[this.level]) ? "&refresh=saved" : ""
		this.level -= 1;
		if (this.level > -1){
			if (this.open[this.level] == 0){
				this.run();
			} else {
				jQuery.get(this.dlg[this.level],'_focus_=1&_popup_=1&_bidv=' + this.row[this.level+1]+'&viewstate='+this.viewstate[this.level+1]+'&_EIPClm='+this.equate[this.level+1]+'&_action_=' + this.action[this.level+1]+'&_from_='+ this.dlg[this.level+1]+'&_calledfrom_=' + this.calledfrom[this.level+1] + svd + '&' + this.other[this.level+1],function(data){xmlProcess(data);});
			} 
		}	else {
			jQuery.get(this.calledfrom[0],'_focus_=1&_popup_=0&_bidv=' + this.row[this.level+1]+'&viewstate='+this.viewstate[this.level+1]+'&_EIPClm='+this.equate[this.level+1]+'&_action_=' + this.action[this.level+1]+'&_from_='+ this.dlg[this.level+1]+'&_calledfrom_=' + this.calledfrom[this.level+1] + svd + '&' + this.other[this.level+1],function(data){xmlProcess(data);});
		}	
	}
}
// ----
ntdialog.prototype.getluf = function (){
  if (this.level > -1){
    return this.luf[this.level];
  } else {
    return '';
  }
}
// ----
ntdialog.prototype.getdlg = function (){
  if (this.level > -1){
    return this.dlg[this.level];
  } else {
    return '';
  }
}
// ----
ntdialog.prototype.close = function (){
 jQuery('#popup_'+ntd.getdlg().toLowerCase()+'_div').dialog('close'); // close does a ntd.pop
}
// ----
ntdialog.prototype.cancel = function (){
	var id = this.dlg[this.level];
	if (this.calledfrom[this.level] != null){
		id = this.calledfrom[this.level];
	}
	var options = { 
		url: id,
		dataType: 'xml',
    success:    function(data) { 
        xmlProcess(data); 
			},
		url: this.dlg[this.level] + '_xxx_cancelled',
		data: {	pressedButton: 'cancel_btn', _popup_: 1,_xRequestedWithXMLHttpRequest_: true, _grandparent_: this.grandparent[this.level]}
	}; 
  jQuery("#"+this.dlg[this.level]+"_frm").ajaxSubmit(options);
  jQuery('#popup_'+ntd.getdlg().toLowerCase()+'_div').dialog('close'); // close does a ntd.pop
}
// ----
ntdialog.prototype.save = function (event){
	var id = this.dlg[this.level];
	if (this.calledfrom[this.level] != null){
		id = this.calledfrom[this.level];
	}
	removePlaceHolder();
	var options = { 
		url: id,
		dataType: 'xml',
    success:    function(data) { 
        xmlProcess(data); 
			},
		url: this.dlg[this.level] + '_xxx_validate',
		data: {	pressedButton: 'save_btn', _popup_: 1,_xRequestedWithXMLHttpRequest_: true, _grandparent_: this.grandparent[this.level], _buttontext_: $(event.target).text()}
	}; 
	jQuery("#"+this.dlg[this.level]+"_frm").ajaxSubmit(options);
	// use the line below instead when debugging, as it's visible in firebug.
	//jQuery.post(id,'pressedButton=save_btn&_popup_=1&'+ jQuery("#"+this.dlg[this.level]+"_frm").serialize(),function(data){xmlProcess(data);});
}
// ----
ntdialog.prototype.deleteb = function (id,br,ff,par){
 jQuery.post(br,'pressedButton=deleteb_btn&_popup_=1&FromForm='+ ff + '&_bidv_=' + id + '&_parentProc=' + par,function(data){xmlProcess(data);});
}
// ----
ntdialog.prototype.deletef = function (){
 var id = this.dlg[this.level];
 if (this.calledfrom[this.level] != null){
  id = this.calledfrom[this.level];//.id;
 }
 jQuery.post(id,'pressedButton=deletef_btn&_popup_=1&'+ jQuery("#"+this.dlg[this.level]+"_frm").serialize(),function(data){xmlProcess(data);});
 jQuery('#popup_'+ntd.getdlg().toLowerCase()+'_div').dialog('close'); // close does a ntd.pop
}
// ----
var ntd = new ntdialog();
// ------------------------------------------------------------------------------------
// browseTable object handles all browse table related functionality.
function browseTable(id,frm,phn,rows,sp,gb,mo,hs,par,ufu,uft,dc,dcm,phf,sufu,suft,fpr,pup,rez,mw,mh,addsec){
var _this = this;
this.id = id;
this.tid = id + '_tbl';
this.table=document.getElementById(this.tid);
this.bgOver='#DDDDDD';
this.bgHigh='#CCCCCC';
this.bgOne='#FFFFFF';
this.bgTwo='#F8F8F8';
this.rowSelected=-1;
this.rowsHigh=rows;
this.rc=-1;
this.rs=0;
this.sproc=sp;
this.par = par;
this.greenBar=gb; // greenbar table.
this.mouseOver=mo; // mouse over
this.highlightSelected=hs; // highlight selected field
this.form=document.getElementById(frm);
this.frm=frm;
this.phn=phn;
this.ufu=ufu;
this.uft=uft;
this.selectufu=sufu;
this.selectuft=suft;
this.ct=0;
this.vs=0;
this.eipclm=0;
this.dc=dc;
this.dcm=dcm;
this.quiet = 0;
tables[tables.length] = this;
this.parseCell();
this.value = phf;
this.fpr=fpr;
this.pup=pup;
this.expand = 'circle-plus'
this.contract = 'circle-minus'
this.resizable = rez;
this.minwidth = mw;
this.minheight = mh;
this.col=0;
this.addsec=addsec;
this.prepColumns();
this.makeResizable();
this.bind();
this.pcv();
}

browseTable.prototype.prepColumns = function(){
  var cmw=0;
  var iw=0;
  var _this=this;
//  jQuery('#'+_this.id.toLowerCase()+'_table_div').find('th :last').each(function(i){
//		cmw = parseInt($(this).css('min-width'));
//		iw = $(this).find('[data-role="sh"]').width();
//		if (cmw < (iw - -32)){
//			$(this).css('min-width',iw + 32);	
//		}
//  })
  var _this=this;
  if ((this.addsec != '') && (this.addsec != undefined)){
    var k = $('#'+_this.id.toLowerCase()+'_table_div').find('th:last').find('[data-key]').attr('data-key');
    if (k == undefined){
			jQuery('#'+_this.id.toLowerCase()+'_table_div').find('th:last').append('<div class="nt-right" data-key="true"><a href="#" id="' + _this.id.toLowerCase() + '-browse-access" class="nt-browse-titlebar-access"><span class="ui-icon ui-icon-key"></span></a></div>');
			//Secwin Button
			$('#' + _this.id.toLowerCase() + '-browse-access').hover(function(){
				$(this).addClass('ui-state-hover');
			}, function(){
				$(this).removeClass('ui-state-hover');
			}).click(function(){
				ntd.push('secwinwebuseraccess','','header',1,2,null,'','','_screen_=' + _this.addsec);	
				return false;
			});
		}	
	}	
}

// bind events to table
browseTable.prototype.bind = function(){
  var _this = this;
  jQuery('#' + _this.tid + ' tbody tr')
    .unbind('mouseover.bt mouseout.bt click.bt')
    .bind('mouseover.bt',function(e){_this.omv(this,e);})
    .bind('mouseout.bt',function(e){_this.omt(this,e);})
    .bind('click.bt',function(e){_this.cr(this,e);});

  jQuery('#' + _this.tid + ' input')
    .unbind('keydown.bt focus.bt')
    .bind('keydown.bt',function(e){_this.keydown(this,e);})
    .bind('focus.bt',function(e){_this.gotfocus(this,e);});

  jQuery('#Locator1' + _this.id + ',' + '#Locator2' + _this.id)
		.bind('keypress',function(e){return _this.keyPressLoc(this,e);})   
		.bind('keydown',function(e){return _this.keyDownPaging(this,e);});
}

// make browse resizeable
browseTable.prototype.makeResizable = function(){
  if(this.resizable == 1){
    var _this = this;
    jQuery('#' + _this.id.toLowerCase() + '_table_resize_div')
    .resizable({minwidth: _this.minwidth,
                minheight: _this.minheight,
                stop: function(event,ui){_this.resized();}});
  }
}
// browse has been resized
browseTable.prototype.resized = function(){
  var id = '#'+this.id.toLowerCase()+'_table_div';
  var parms = '_resized_=1&_width_=' + jQuery(id).width() + '&_height_=' + jQuery(id).height();
  jQuery.get(this.sproc,parms,function(data){xmlProcess(data);});
}

browseTable.prototype.restoreFocus = function(){
  if (this.col != 0){
     jQuery('#' +this.tid + ' tbody tr:first').children('td:eq('+this.col+')').find(':input:first').focus();
  }
}

browseTable.prototype.gotfocus = function(inp,e){
  this.col = jQuery(inp).closest('td').prevAll().length;
}

// handle paging keys in EIP and locator fields
browseTable.prototype.keyDownPaging = function(inp,e){
  if ((e.which == 191) && (e.shiftKey == true)){
    e.which = ntLookupKey;
  }
  switch(e.which){
    case jQuery.ui.keyCode.PAGE_UP: {
      this.previous(inp.id);
      return false;
    }
    case jQuery.ui.keyCode.PAGE_DOWN: {
      this.next(inp.id);
      return false;
    }
    case jQuery.ui.keyCode.HOME: {
      if (e.ctrlKey==true){
        this.first(inp.id);
        return false;
      }
    }
    case jQuery.ui.keyCode.END: {
      if (e.ctrlKey==true){
        this.last(inp.id);
        return false;
      }
    }
    //case 191:  // ?
    case ntLookupKey: {// F2 by default
      jQuery("#"+inp.id+".hasDatepicker").each(
        function(i,v){
          e.preventDefault();
          jQuery(inp).datepicker("show");
          return false;
        }
      );
      jQuery("#"+inp.id).next(':button').each(
				function(i,v){
					jQuery(this).click();
					return false;
				}	
      )
    }  
		return true;
  }
}

// Handle enter key in locator fields
browseTable.prototype.keyPressLoc = function(inp,e){
  switch(e.which){
		case 13:{
			jQuery(inp).change();
			return false;
		}
		return true;
  }
}
// bind up and down arrow keys in EIP in browse
browseTable.prototype.keydown = function(inp,e){
  switch(e.which){
    case jQuery.ui.keyCode.DOWN: {
      this.col = jQuery(inp).closest('td').prevAll().length;
      jQuery(inp).closest('tr').nextAll(':eq(0)').children('td:eq('+this.col+')').find(':input:first').focus();
      e.which = 0;
      return false;
    }
    case jQuery.ui.keyCode.UP: {
      this.col = jQuery(inp).closest('td').prevAll().length;
      jQuery(inp).closest('tr').prevAll(':eq(0)').children('td:eq('+this.col+')').find(':input:first').focus();
      e.which = 0;
      return false;
    }
  }
  return this.keyDownPaging(inp,e);
}

// pre-contract vertically
browseTable.prototype.pcv = function(co,ex,lx){
 var _this=this;
	jQuery('[data-nt-ctd="true"]').each(function(i,e){
			_this.cv(e);
	});	
}	

// contract vertically
browseTable.prototype.cv = function(e,co,ex,lx){
  if ((ex == null) || (ex == '')){
    var exp = 'ui-icon-' + this.expand;
  } else {
    var exp = 'ui-icon-' + ex;
  }

  if ((co == null) || (co == '')) {
    var con = 'ui-icon-' + this.contract;
  } else {
    var con = 'ui-icon-' + co;
  }

  if (lx == null){
    var l = this.rowsHigh-1;
  } else {
    var l = lx;
  }
  if (jQuery(e).closest('tr').nextAll(':lt('+l+')').is(':visible')){
    jQuery(e).removeClass(con).addClass(exp).closest('tr').children('td').each(function(){
      jQuery(this).attr('rowspanwas',jQuery(this).attr('rowspan'));
      jQuery(this).attr('rowspan',1)
    });
    jQuery(e).closest('tr').nextAll(':lt('+l+')').hide();
  } else {
    jQuery(e).removeClass(exp).addClass(con).closest('tr').children('td').each(function(){
      jQuery(this).attr('rowspan',jQuery(this).attr('rowspanwas'));
    });
    jQuery(e).closest('tr').nextAll(':lt('+l+')').show();
  }
}

// send async request to server
//browseTable.prototype.sv = function (p0,p1,p2,p3,p4){
browseTable.prototype.sv = function (p0){
  var parms='';
  var _this=this;
  for(var d = 1; d < arguments.length; d++){
    parms += arguments[d] + '&';
  }
  parms += '_ParentProc=' + this.par + '&_popup_=' + this.pup + '&_rnd_=' + Math.random();
  jQuery.get(p0,parms,function(data){xmlProcess(data);});
}

// recursive function to find the first checkbox which is "inside" c.
function getCheckbox(c){
 if (c.type == 'checkbox'){
  return c;
 }
 if (c.firstChild != null){
  a = getCheckbox(c.firstChild);
  if (a != null){
  return a;
  }
 }
 while (c.nextSibling != null){
  a = getCheckbox(c.nextSibling);
  if (a != null){
   return a;
  }
 }
 return null;
}

// checkbox on top of column to set checkbox in whole column.
browseTable.prototype.ec = function(hcb){
 for(var i=0;i<this.table.tHead.rows[0].cells.length;i++){
  if (this.table.tHead.rows[0].cells[i] == hcb.parentNode){
   var c = i;
  }
 }
 this.quiet = 1;
 for(i=0;i<this.table.tBodies[0].rows.length;i++){
  var o = this.table.tBodies[0].rows[i].cells[c].firstChild;
  cb = getCheckbox(o);
  if (cb != null){
    cb.checked = hcb.checked;
    cb.onclick();
  }
 }
 this.quiet = 0;
}

// called to trigger eip
browseTable.prototype.eip = function(t,c,v){
 this.sv(this.sproc,'_EIPClm='+c,'ViewState='+v,'Value='+FieldValue(t),'_Quiet='+this.quiet);
}

browseTable.prototype.parseCell = function (){
  var tid = '#' + this.tid;
  var a = jQuery(tid).attr("data-nt-rows");
  if (a != undefined){
    this.rowsHigh = a;
    jQuery(tid).removeAttr("data-nt-rows");
  }
  a = jQuery(tid).attr("data-nt-value");
  if (a != undefined){
    this.value = a;
    jQuery(tid).removeAttr("data-nt-value");
    if (this.highlightSelected ==1){
			this.rowSelected = jQuery(tid).find("[data-nt-id="+this.value+"]").filter(":first").index();  
    }
  }
}

// sets the color of a multi-row block. b is the block number
browseTable.prototype.colorBlock = function (b,what){
	var col=this.bgOne;
	var i = parseInt(b*this.rowsHigh);
	if (i > this.table.tBodies[0].rows.length){
		return 1;
	}
	if (this.mouseOver==1 && what==1){
		col = this.bgOver;
  } else if(this.highlightSelected==1 && this.rowSelected==i){
		col=this.bgHigh;
	} else if (this.greenBar==1){
		col=(b%2==0) ?  this.bgOne :  this.bgTwo;
	}
	for(var j=0;j<this.rowsHigh;j++){
		if (i+j < this.table.tBodies[0].rows.length && i+j >= 0){
			this.table.tBodies[0].rows[i+j].bgColor= col
		}
	}
	return 0;
}
// greenbars the table. And highlights selected row.
browseTable.prototype.applyGreenBar = function (){
	if (this.table == null){
		return;
	}
	if (this.table.tBodies[0] == null){
		return;
	}
	if ((this.greenBar == 0) && (this.highlightSelected == 0) && (this.mouseOver == 0))
	{
		return;
	}
	var b = 0;
	while(this.colorBlock(b) == 0){
		b++;
	}
}
// draws a whole block, based on a row index
browseTable.prototype.colorRow = function (r,what){
		var x=parseInt(r / this.rowsHigh);
		this.colorBlock(x,what);
}
// onmouseover
browseTable.prototype.omv = function (r,e){
	if (jQuery(e.currentTarget).parent().is("tbody")){
	  this.colorRow(r.sectionRowIndex,1);
	}	
}
//onmouseout
browseTable.prototype.omt = function (r,e){
  if (jQuery(e.currentTarget).parent().is("tbody")){
		var x=parseInt(r.sectionRowIndex / this.rowsHigh);
	  this.colorRow(r.sectionRowIndex);
	}
}	
// clickrow
browseTable.prototype.cr = function (r,e){
 // this.rowSelected holds the index of the first row in the selcted block. 
	var i = this.rowSelected;
	this.rowSelected = parseInt(r.sectionRowIndex/this.rowsHigh) * this.rowsHigh;
	this.colorRow(i);
	this.colorRow(r.sectionRowIndex);
	var phf = jQuery(r).attr('data-nt-id');
	this.sv(this.sproc,'_Clicked=1','_bidv_='+phf);
	if (this.par != '' && this.par != this.id){
		var lurl = this.par+'_'+this.id+'_value';
		this.sv(lurl.toLowerCase(),'event=1','_bidv_='+phf,'_Silent=0');
	}
	this.value = phf;
	return;
}

// finds the element, and sets the value
browseTable.prototype.setvalue = function (phf){
  this.form=document.getElementById(this.frm);
  if ((phf != null) && (phf != '')){
    this.value = phf;
  }
  if (this.phn != null){
    var e = document.getElementById(this.phn.replace(/:/g,"__"));
  }
  if ((e == null) && (this.phn != '')){
    var rid = document.createElement('INPUT');
    rid.type = 'hidden';
    rid.name = '_bidv_'; //this.phn.replace(/:/g,"__");
    rid.value = this.value;
    try{
      this.form.appendChild(rid);
    } catch(e){}
  } else {
    if (e != null){
      e.value = this.value;
    }
  }
}

// sets greenbaring,selected and mouse over colors for the browse.
browseTable.prototype.setGreenBar = function (h,o,t,v){
  if ((h != '') && (h != null)){
    this.bgHigh= h;
  }
  if ((o != '') && (o != null)){
    this.bgOne=o;
  }
  if ((t != '') && (t != null)){
    this.bgTwo=t;
  }
  if ((v != '') && (v != null)){
    this.bgOver=v;
  }
}

browseTable.prototype.ufut = function (){
  this.form=document.getElementById(this.frm);
  this.form.action = this.ufu;
  this.form.target = this.uft;
}

browseTable.prototype.alertParent = function(){
  if (this.par != '' && this.par != this.id){
    var lurl = this.par+'_'+this.id+'_value';
    this.sv(lurl.toLowerCase(),'event=1');
  }
}

browseTable.prototype.first = function (f){
 this.sv(this.sproc,'Refresh=first&focus=' + f)
 this.alertParent();
}

browseTable.prototype.previous = function (f){
 this.sv(this.sproc,'Refresh=previous&focus=' + f)
 this.alertParent();
}

browseTable.prototype.next = function (f){
 this.sv(this.sproc,'Refresh=next&focus=' + f)
 this.alertParent();
}

browseTable.prototype.last = function (f){
 this.sv(this.sproc,'Refresh=last&focus=' + f)
 this.alertParent();
}

browseTable.prototype.cl = function (){
 this.sv(this.sproc,'Refresh=clearlocate')
 this.alertParent();
}

browseTable.prototype.locate = function (id,v){
 this.sv(this.sproc,'Refresh=locate',id+'='+encodeURI(v));
 this.alertParent();
}

browseTable.prototype.sort = function (s){
 this.sv(this.sproc,'Refresh=sort',this.id+'_Sort='+s);
 this.alertParent();
}

browseTable.prototype.edit = function (phf,popup,header,action,button){
	this.setvalue(phf); 
	if (popup){
		ntd.push(this.ufu,'',header,1,action,null,this.id,this.value,'_ParentProc=' + this.par); 
	} else {
		this.ufut();
		this.dsb(button);
	}	

}

browseTable.prototype.insert = function (phf,popup,header){
  this.edit(phf,popup,header,1,'insert_btn');
}

browseTable.prototype.change = function (phf,popup,header){
  this.edit(phf,popup,header,2,'change_btn');
}

browseTable.prototype.copy = function (phf,popup,header){
  this.edit(phf,popup,header,4,'copy_btn');
}

browseTable.prototype.view = function (phf,popup,header){
  this.edit(phf,popup,header,5,'view_btn');
}


browseTable.prototype.deleteb = function (phf,url){
 this.form=document.getElementById(this.frm);
 if(this.dc==1){         
  if(confirm(htmlDecode(this.dcm))==false){
   return false;
  }
 }
 this.setvalue(phf);
 this.form.action = url+'?FromForm='+this.fpr;
 this.form.target = '_self';
 this.dsb('deleteb_btn');
}

browseTable.prototype.deletep = function (id,ff){
	if (id == ''){
		id = this.value;
	}
	if(this.dc==1){
		if(confirm(htmlDecode(this.dcm))==false){
			return false;
		}
	}
	ntd.deleteb(id,this.id,ff,this.par);
}

browseTable.prototype.select = function (phf){
 this.setvalue(phf);
 this.form.action = this.selectufu;
 this.form.target = this.selectuft;
 this.dsb('select_btn');
}

browseTable.prototype.pselect = function (phf){
 this.setvalue(phf);
 jQuery('#'+ntd.getluf()).val(this.value);
 jQuery('#'+ntd.getluf()).change();
 jQuery('#popup_'+ntd.getdlg().toLowerCase()+'_div').dialog('close'); // close does a ntd.pop
}

browseTable.prototype.pcancel = function (){
  jQuery('#popup_'+ntd.getdlg().toLowerCase()+'_div').dialog('close'); // close does a ntd.pop
}

browseTable.prototype.lookup = function (lurl,ltar){
 this.form.action = lurl;
 this.form.target = ltar;
 this.dsb('lookup_btn');
}

// Lookup Date
browseTable.prototype.sd = function (id,p,ec,vs){
 this.ct = document.getElementById(id);
 switch (p){
 case "@D6":
 case "@D06":
  var c = new calendar6(this.ct);
  break;
 case "@D2":
 case "@D02":
  var c = new calendar2(this.ct);
  break;
 }
 this.eipclm = ec;
 this.vs = vs;
 c.popup();
}

// Reset After Date
browseTable.prototype.rad = function(){
 this.eip(this.ct,this.eipclm,this.vs);
}

// button pressed
browseTable.prototype.dsb = function (n){
 this.form=document.getElementById(this.frm);
 if (this.form.target == "" || this.form.target == "_self"){
         jQuery(':button').attr('disabled', 'disabled');
 }
 var pb = document.createElement('INPUT');
 pb.type = 'hidden';
 pb.name = 'pressedButton';
 pb.value = n;
 this.form.appendChild(pb);
 removePlaceHolder();
 this.form.submit();
}

browseTable.prototype.removeOtherBrowses = function (){
 var f=document.getElementById(fn);
 var dv=document.getElementById(dn);
 var a;
 var b;
 if (dv != null){
  var divs = dv.getElementsByTagName('DIV');
  for(var e = divs.length-1; e>=0 ; e--){
   if ((divs[e].id != dn) && (divs[e].id != '')){
    removeElement(fn,divs[e].id);
   }
  }
  if (f != null){
   for(var e = f.elements.length-1; e>=0 ; e--) {
    a = f.elements[e].parentNode.id;
    b = dv.id
    if (a==b){
     try{
      dv.removeChild(f.elements[e]);
     } catch (e) {
     }
    }
   }
  }
 }
}

function dsb(event,f,b,n,prid,prv){
 var i=0;
 if (n=='deleteb_btn'){
  if(confirm('Are you sure you want to delete this record?')==false){
   return false;
  }
 }
 // dont send files if form is cancelled.
 if (n=='cancel_btn'){
                jQuery(':file').remove();
 }
 // set all buttons disabled, if target of button is same frame.
 if (f.target == "" || f.target == "_self"){
         jQuery(':button').attr('disabled', 'disabled');
 }
 for (var e=0 ; e < f.elements.length; e++) {
   if (f.elements[e].name == prid){
    f.elements[e].value = prv;
    i = 1;
   }
 }
 var bid = document.createElement('INPUT');
 bid.type = 'hidden';
 bid.name = '_buttontext_';
 bid.value = $(event.target).closest("button").val();
 f.appendChild(bid);

 jQuery("#_webkit_").val(Math.random());
 if ((i==0) && (prid != '')){
  var rid = document.createElement('INPUT');
  rid.type = 'hidden';
  rid.name = prid;
  rid.value = prv;
  f.appendChild(rid);
 }
 var pb = document.createElement('INPUT');
 pb.type = 'hidden';
 pb.name = 'pressedButton';
 pb.value = n;
 f.appendChild(pb);
 osf(f);
 removePlaceHolder();
 f.submit();
}

function osf(f){
	if(f.target=='' || f.target=='_self' || f.target=='_top') {
		for (var e=0 ; e < f.elements.length; e++) {
			if(f.elements[e].type=='button'){
				f.elements[e].disabled = true;
			}
		}
	}
}

function ml(ta,ml,e){
  var k;
  if(window.event){ // IE
    k = e.keyCode
  } else if(e.which){ // Netscape/Firefox/Opera/Safari
    k = e.which
  };
  if (k==8 || k==null || k > 60000){
    return true;
  }
  return (ta.value.length <= ml);
}

function firstFocus(id){
  var e;
  var t = 4000000000;
	jQuery(id + ' :input').not('[readonly],[disabled],[type="hidden"]').each(function(){
		tx = $(this).offset().top
	  if (tx < t && tx != 0){
	    e = this;
	    t = tx;
	  }
	})
	$(e).focus();
}

function nextFocus(f,pname,skipone){
  var i = 0;
  var j = 0;
  if (skipone==2){ // pname is specified control to get focus
    for (var e=0 ; e < f.elements.length; e++) {
      if(f.elements[e].name==pname){
  try{
    f.elements[e].focus();
  } catch (e) {
  }
  break;
      }
    }
  } else {
    for (var e=0 ; e < f.elements.length; e++) {
      if (i==1){
  if ((f.elements[e].type == "text") || (f.elements[e].type == "textarea") || (f.elements[e].type == "checkbox") || (f.elements[e].type == "radio") || (f.elements[e].type == "select-one")){
    //|| (f.elements[e].type == "button")
    if(f.elements[e].readOnly != true){
      if((skipone==1) && (j==0)){
        j = 1;
      } else {
        try{
    f.elements[e].focus();
        } catch (e) {
        }
        break;
      }
    }
  }
      }
      else{
  if(pname==''){
    if(f.elements[e].readOnly != true){
      try{
        f.elements[e].focus();
      } catch (e) {
      }
      break;
    }
  } else {
    if(f.elements[e].name==pname){
      i = 1;
    }
  }
      }
    }
  }
}


function removeElement(fn,dn){
 var f=document.getElementById(fn);
 var dv=document.getElementById(dn);
 var a;
 var b;
 if (dv != null){
  var divs = dv.getElementsByTagName('DIV');
  for(var e = divs.length-1; e>=0 ; e--){
   if ((divs[e].id != dn) && (divs[e].id != '')){
    removeElement(fn,divs[e].id);
   }
  }
  if (f != null){
   for(var e = f.elements.length-1; e>=0 ; e--) {
    a = f.elements[e].parentNode.id;
    b = dv.id
    if (a==b){
     try{
      dv.removeChild(f.elements[e]);
     } catch (e) {
     }
    }
   }
  }
 }
}

function FieldValue(f,e){
  var ans ='';
  var typ = f.type;
  var i = 0;
  var j = 0;
  if (typ == undefined){
    typ = f[0].type;
  }
  switch (typ){
  case "radio":
    j = f.length;
    for(i = 0; i < j; i++) {
      if(f[i].checked) {
  ans = f[i].value;
  break;
      }
    }
    break;
  case "checkbox":
    if (f.checked){
      ans = f.value;
    }
    break;
  case "select-multiple":
    j = f.length;
    for(i = 0; i < j; i++) {
      if(f.options[i].selected) {
        ans = ans + ';|;' + f.options[i].value;
    }
    }
    break;
  default: ans = f.value;
  }
  // if called as a post, do not encode & and %. If called from EIP then do.
  if ((e == 0) || (e == undefined)){
                ans = ans.replace(/%/,"%25");
                ans = ans.replace(/&/,"%26");
                ans = ans.replace(/#/,"%23");
        }
  return ans
}

function SetSessionValue(name,value){
  jQuery.get('SetSessionValue',name+'='+value,function(data){xmlProcess(data);});
}

function GetTab(name){
  jQuery.get(name,'',function(data){xmlProcess(data);});
}

function xmlProcess(data){
  if (typeof(data) == 'string'){
    jQuery('html').trigger("ajaxComplete");
    return;
  }
  jQuery('response',data).each(function(i){
    var t = jQuery("response",data).get(i); // returns Element object
    var e = jQuery(t).attr("type");
    if (window.ActiveXObject) {  //for IE 
        var s = t.xml;           // IE 9 doesn't get this 
        if (s == undefined){
					var s = (new XMLSerializer()).serializeToString(t); // but IE9 can do this, which IE7/8 can't
        }
    } else { // code for Mozilla, Firefox, Opera, etc.
        var s = (new XMLSerializer()).serializeToString(t);
    }
    if (s){
			s = s.substring(s.indexOf('>')+1,s.lastIndexOf('<'));
			if (e=='element'){
				d = jQuery(t).attr("id");
				jQuery("#"+d).replaceWith(s);
			} else if (e=='script'){
				s = s.replace(/&quot;/g,'"');
				s = s.replace(/&amp;/g,"&");
				s = s.replace(/&lt;/g,"<");
				s = s.replace(/&gt;/g,">");
				try{
				eval(s);
				} catch (e){
					try{
					} catch (e){}	
				}
			}
		}	
  });
  afterSv();
	gradient();
	resetCountDown();
}

// SetServer
function sv(id,name,ev,val,par,sil){
 hadfocus = id;
  if(par==undefined){
  jQuery.get(name,{event: ev,value: val, _rnd_: Math.random()},function(data){xmlProcess(data);});
 }else{
  var parms='';
  for(var d = 2; d < arguments.length; d++){
    parms += arguments[d] + '&';
  }
  parms += '_rnd_=' + Math.random();
  jQuery.get(name,parms,function(data){xmlProcess(data);});
 }
}

//Set timer
function SetTimer(name,t,par,sil){
 if(par==undefined)  {par='fred=1'};
 if(sil==undefined)  {sil='fred=2'};
 //sv('',name,'','',par,sil);
 //setTimeout('SetTimer(\'' + name +'\','+t+',\''+par +'\',\'' + sil+'\')',t);
 setTimeout("sv('','"+name+"','','','"+par+"','"+sil+"');",t);
};

// SelectDate and ResetAfterDate called by Date Lookup button
var cr1;
var cs;
var ct;
var cb1;
var cb2;
// SelectDate
function sd(f,e,p,r,b1,b2){
 ct = document.forms[f].elements[e];
 switch (p){
 case "@D6":
 case "@D06":
  var c = new calendar6(ct);
  break;
 case "@D2":
 case "@D02":
  var c = new calendar2(ct);
  break;
 }
 c.popup();
 if (arguments.length == 4){
  cr1 = r;
  cs = 1;
 }
 if (arguments.length == 6){
  cr1 = r;
  cs = 2;
  cb1 = b1;
  cb2 = b2;
 }
}
// ResetAfterDate
function rad(){
 if (cs==1){
  sv('',cr1,1,ct.value);
  cs = 0;
 }
 if (cs==2){
  sv('',cr1,cb1,cb2,'Value='+ct.value);
  cs = 0;
 }
}

// jQuery Default Settings
jQuery.datepicker.setDefaults({
   closeText: 'Cancel',
   dateFormat: 'm/dd/yy',
   showButtonPanel: true,
   showOn: 'button',
   buttonImageOnly: true,
   buttonImage: 'styles/images/calendar.gif',
   buttonText: 'Calendar',
   constrainInput: false
});


// html5 helper-functions for browsers that don't yet support html5.
// Modernizr is used as a detector so if browser has native support these functions do nothing.
// ---------------------------------------------
function applyPlaceHolderElement(e){
 var t = e.attr('placeholder');
 var f = e.parents('form:first');
 if (e.val() === ''){
  e.val(t);
  e.css('color', '#888');
 }
 e.bind('focus.placeholder', function(event) {
  if (e.val() === t){
   e.val('');
  }
  e.css('color', '');
 });
 e.bind('blur.placeholder', function(event) {
  if (e.val() === ''){
         e.val(t);
   e.css('color', '#888');
  }
 });
 f.bind("submit.placeholder", function(event) {
  if (e.val() === t){
   e.val("");
  }
 });
};

// ---------------------------------------------
// Jan 2010 - Webkit support placeholder on a <input> but none yet on <textarea>
function applyPlaceHolder(){
  jQuery('[placeholder]').each(function(i) {
    var e = jQuery(this);
    if (!Modernizr.input.placeholder && e.type === 'INPUT'){
    // do nothing
    } else {
			if (jQuery(e).is(":focus")) {
				// do nothing
			} else {
				applyPlaceHolderElement(e);
			}	
    };
  });
};
// ---------------------------------------------
function removePlaceHolder(){

 jQuery('[placeholder]').each(function(i) {
  var e = jQuery(this);
  if (e.val() === e.attr('placeholder')){
   e.val("");
  }
 });
};
// ---------------------------------------------
function applyHTML5(){
 applyPlaceHolder();
}
// ---------------------------------------------
function bubbleStyle(div,attr,col){
	if ((attr=='background-color') && (col != 'transparent')){
		jQuery("#"+div).parent().css('background-color',col);
		jQuery("#"+div).css('background-color','transparent');
		if (Modernizr.cssgradients ==  false){
			if (window.ActiveXObject) {  //for IE
				var ua = navigator.userAgent;
				var re  = new RegExp("MSIE ([0-8]{1,}[\.0-8]{0,})");
				if (re.exec(ua) != null){
					jQuery("#"+div).parent().each(function(){
						this.style.filter = '"filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='+col+', endColorstr=#FFFFFFFF)"';
					});	
				}	
			}
		} else {
			jQuery("#"+div).parent().css('background','-webkit-gradient(linear, 0 0, 0 100%, from('+col+'), to(#FFFFFF))');
			if (jQuery("#"+div).parent().css('background') == ''){
				jQuery("#"+div).parent().css('background','-moz-linear-gradient(center bottom, #FFFFFF 0%, '+col+' 75%)');
			}
		}	
	}
}

function gradient(){
	jQuery('.nt-grad').each(function(){
		var col = jQuery(this).css('background-color');
		if (col != 'transparent'){
			if (Modernizr.cssgradients ==  false){
				if (window.ActiveXObject) {  //for IE
					var ua = navigator.userAgent;
					var re  = new RegExp("MSIE ([0-8]{1,}[\.0-8]{0,})");
					if (re.exec(ua) != null){
						this.style.filter = '"filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='+col+', endColorstr=#FFFFFFFF)"';
					}	
				}
			} else {
				jQuery(this).css('background','-webkit-gradient(linear, 0 0, 0 100%, from('+col+'), to(#FFFFFF))');
				if (jQuery(this).css('background') == ''){
					jQuery(this).css('background','-moz-linear-gradient(center bottom, #FFFFFF 0%, '+col+' 75%)');
				}
			}	
		}
	});
}

// ---------------------------------------------
jQuery(document).ready( function(){jQuery('.rounded').corners();});


// ---------------------------------------------
// run html5 support scripts when page opens
jQuery(document).bind('ready', function(event) {
  applyHTML5();
  gradient();
});
// ---------------------------------------------
// IE checkbox / radio fix.
// http://norman.walsh.name/2009/03/24/jQueryIE
$(function () {
	if ($.browser.msie) {
		jQuery('input:radio, input:checkbox').bind('click.iefix',function () {
			this.blur();
			this.focus();
		});
	}
});

// ---------------------------------------------
// adds :focus selector to jQuery - can be removed with jQuery 1.6.
//jQuery.expr[':'].focus = function( elem ) {
//  return elem === document.activeElement && ( elem.type || elem.href );
//};

// ---------------------------------------------
//	Extension of jQueryUi dialog to add a call to SetAccess
// ---------------------------------------------
(function(jQuery){
	var _init = jQuery.ui.dialog.prototype._init;
	
	//Custom Dialog Init
	jQuery.ui.dialog.prototype._init = function() {
		_init.apply(this, arguments);
		var _this=this;
    if ((this.options.addsec != '') && (this.options.addsec != undefined)){
			tb = this.uiDialogTitlebar;
			tb.append('<a href="#" id="dialog-access" class="dialog-access ui-dialog-titlebar-access ui-corner-all"><span class="ui-icon ui-icon-key"></span></a>');
			//Secwin Button
			jQuery('.dialog-access', tb).hover(function(){
				jQuery(this).addClass('ui-state-hover');
			}, function(){
				jQuery(this).removeClass('ui-state-hover');
			}).click(function(){
				ntd.push('secwinwebuseraccess','','header',1,2,null,'','','_screen_=' + _this.options.addsec);	
				return false;
			});
		}	
	};

})(jQuery); 


function swpf(id,addsec){
  jQuery('#form-access-'+id).prepend('<a href="#" id="a-form-access-'+id+'" class="nt-form-page-access ui-widget-header ui-corner-all"><span class="ui-icon ui-icon-key"></span></a>');
	jQuery('#a-form-access-'+id).hover(function(){
		jQuery(this).addClass('ui-state-hover');
	}, function(){
		jQuery(this).removeClass('ui-state-hover');
	}).click(function(){
		ntd.push('secwinwebuseraccess','','header',1,2,null,'','','_screen_=' + addsec);	
		return false;
	});
}	

// function courtesy Rob Reid, of http://www.strictly-software.com/htmlencode
function htmlDecode(s){
		var c,m,d = s;		
		// look for numerical entities &#34;
		arr=d.match(/&#[0-9]{1,5};/g);		
		// if no matches found in string then skip
		if(arr!=null){
			for(var x=0;x<arr.length;x++){
				m = arr[x];
				c = m.substring(2,m.length-1); //get numeric part which is refernce to unicode character
				// if its a valid number we can decode
				if(c >= -32768 && c <= 65535){
					// decode every single match within string
					d = d.replace(m, String.fromCharCode(c));
				}else{
					d = d.replace(m, ""); //invalid so replace with nada
				}
			}			
		}
		return d;
}