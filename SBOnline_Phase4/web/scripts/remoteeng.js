// Override Defaults
// jQuery Default Settings
jQuery.datepicker.setDefaults({
   closeText: 'Cancel',
   dateFormat: 'm/dd/yy',
   showButtonPanel: true,
   showOn: 'button',
   buttonImageOnly: true,
   buttonImage: 'styles/images/calendarLarge.gif',
   buttonText: 'Pick Date',
   constrainInput: false
});