

   MEMBER('WebServer_Phase4.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSE039.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSE011.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE012.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE013.INC'),ONCE        !Req'd for module callout resolution
                     END


ViewCosts            PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
tmp:ARCCost1         REAL                                  !
tmp:ARCCost2         REAL                                  !
tmp:ARCCost3         REAL                                  !
tmp:ARCCost4         REAL                                  !
tmp:ARCCost5         REAL                                  !
tmp:ARCCost6         REAL                                  !
tmp:ARCCost7         REAL                                  !
tmp:ARCCost8         REAL                                  !
tmp:RRCCost0         REAL                                  !
tmp:RRCCost1         REAL                                  !
tmp:RRCCost2         REAL                                  !
tmp:RRCCost3         REAL                                  !
tmp:RRCCost4         REAL                                  !
tmp:RRCCost5         REAL                                  !
tmp:RRCCost6         REAL                                  !
tmp:RRCCost7         REAL                                  !
tmp:RRCCost8         REAL                                  !
tmp:ARCIgnoreDefaultCharges BYTE                           !
tmp:RRCIgnoreDefaultCharges BYTE                           !
tmp:ARCViewCostType  STRING(30)                            !
tmp:AdjustmentCost1  REAL                                  !
tmp:AdjustmentCost2  REAL                                  !
tmp:AdjustmentCost3  REAL                                  !
tmp:AdjustmentCost4  REAL                                  !
tmp:AdjustmentCost5  REAL                                  !
tmp:AdjustmentCost6  REAL                                  !
tmp:RRCViewCostType  STRING(30)                            !
tmp:ARCIgnoreReason  STRING(255)                           !
tmp:RRCIgnoreReason  STRING(255)                           !
tmp:originalInvoice  STRING(30)                            !
tmp:ARCInvoiceNumber STRING(60)                            !
FilesOpened     Long
JOBSE2::State  USHORT
SUBCHRGE::State  USHORT
TRACHRGE::State  USHORT
STDCHRGE::State  USHORT
USERS::State  USHORT
JOBPAYMT::State  USHORT
JOBSE::State  USHORT
INVOICE::State  USHORT
JOBSINV::State  USHORT
TRADEACC::State  USHORT
TRDPARTY::State  USHORT
tmp:ARCViewCostType:IsInvalid  Long
__line1:IsInvalid  Long
tmp:ARCIgnoreDefaultCharges:IsInvalid  Long
tmp:ARCIgnoreReason:IsInvalid  Long
buttonAcceptARCReason:IsInvalid  Long
buttonCancelARCReason:IsInvalid  Long
tmp:ARCCost1:IsInvalid  Long
tmp:AdjustmentCost1:IsInvalid  Long
tmp:ARCCost2:IsInvalid  Long
tmp:AdjustmentCost2:IsInvalid  Long
tmp:ARCCost3:IsInvalid  Long
tmp:AdjustmentCost3:IsInvalid  Long
__line2:IsInvalid  Long
tmp:ARCCost4:IsInvalid  Long
tmp:AdjustmentCost4:IsInvalid  Long
tmp:ARCCost5:IsInvalid  Long
tmp:AdjustmentCost5:IsInvalid  Long
tmp:ARCCost6:IsInvalid  Long
tmp:AdjustmentCost6:IsInvalid  Long
tmp:ARCCost7:IsInvalid  Long
tmp:ARCCost8:IsInvalid  Long
jobe:ARC3rdPartyInvoiceNumber:IsInvalid  Long
tmp:ARCInvoiceNumber:IsInvalid  Long
tmp:RRCViewCostType:IsInvalid  Long
__line3:IsInvalid  Long
tmp:RRCIgnoreDefaultCharges:IsInvalid  Long
tmp:RRCIgnoreReason:IsInvalid  Long
buttonAcceptRRCReason:IsInvalid  Long
buttonCancelRRCReason:IsInvalid  Long
tmp:RRCCost0:IsInvalid  Long
tmp:RRCCost1:IsInvalid  Long
tmp:originalInvoice:IsInvalid  Long
tmp:RRCCost2:IsInvalid  Long
tmp:RRCCost3:IsInvalid  Long
BrowseJobCredits:IsInvalid  Long
__line4:IsInvalid  Long
tmp:RRCCost4:IsInvalid  Long
tmp:RRCCost5:IsInvalid  Long
tmp:RRCCost6:IsInvalid  Long
tmp:RRCCost7:IsInvalid  Long
tmp:RRCCost8:IsInvalid  Long
jobe:ExcReplcamentCharge:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
local       class
AddToTempQueue        Procedure(String fField,String fSaveField,String fReason,Byte fSaveCost,String fCost)
            end
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('ViewCosts')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'ViewCosts_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('ViewCosts','')
    p_web.DivHeader('ViewCosts',p_web.combine(p_web.site.style.formdiv,'fdiv'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('ViewCosts') = 0
        p_web.AddPreCall('ViewCosts')
        p_web.DivHeader('popup_ViewCosts','nt-hidden')
        p_web.DivHeader('ViewCosts',p_web.combine(p_web.site.style.formdiv,'fdiv'))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_ViewCosts_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_ViewCosts_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferViewCosts',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_ViewCosts',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferViewCosts',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_ViewCosts',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferViewCosts',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_ViewCosts',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_ViewCosts',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferViewCosts',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_ViewCosts',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferViewCosts',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_ViewCosts',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_ViewCosts',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('ViewCosts')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
DeleteSessionValues ROUTINE
    p_web.DeleteSessionValue('ViewCostsReturnURL')
addToAudit    routine
    data
locNotes        String(255)
    code
    clear(tmpaud:Record)
    tmpaud:sessionID = p_web.SessionID
    set(tmpaud:keySessionID,tmpaud:keySessionID)
    loop
        next(tempAuditQueue)
        if (error())
            break
        end ! if (error())
        if (tmpaud:sessionID <> p_web.sessionID)
            break
        end ! if (tmpaud:sessionID <> p_web.sessionID)
        locNotes = 'REASON: ' & clip(tmpaud:Reason)
        if (tmpaud:SaveCost)
            locNotes = clip(locNotes) & '<13,10>PREVIOUS CHARGE: ' & format(tmpaud:Cost,@n14.2)
        end !if (tmpaud:SaveCost)
        p_web.SSV('AddToAudit:Type','JOB')
        p_web.SSV('AddToAudit:Action',tmpaud:Field)
        p_web.SSV('AddToAudit:Notes',locNotes)
        addToAudit(p_web)
    end

    !Empty Audit Queue
    clear(tmpaud:Record)
    tmpaud:sessionID = p_web.SessionID
    set(tmpaud:keySessionID,tmpaud:keySessionID)
    loop
        next(tempAuditQueue)
        if (error())
            break
        end ! if (error())
        if (tmpaud:sessionID <> p_web.sessionID)
            break
        end ! if (tmpaud:sessionID <> p_web.sessionID)
        delete(tempAuditQueue)
    end



DefaultLabourCost   ROUTINE
    DefaultLabourCost(p_web)


displayARCCostFields      routine
    case (p_web.GSV('tmp:ARCViewCostType'))
    of 'Chargeable'
        p_web.SSV('Prompt:Cost1','')
        IF (p_web.GSV('LoanAttachedToJob') = 1)
            p_web.SSV('Prompt:Cost1','Lost Loan Charge')
        END
        p_web.SSV('Prompt:Cost2','Labour Cost')
        p_web.SSV('Prompt:Cost3','Parts Cost')
        p_web.SSV('Prompt:Cost4','Sub Total')
        p_web.SSV('Prompt:Cost5','V.A.T.')
        p_web.SSV('Prompt:Cost6','Total Cost')
        p_web.SSV('Prompt:Cost7','Paid')
        p_web.SSV('Prompt:Cost8','Outstanding')

        p_web.SSV('tmp:ARCCost1',p_web.GSV('job:Courier_Cost'))
        p_web.SSV('tmp:ARCCost2',p_web.GSV('job:Labour_Cost'))
        p_web.SSV('tmp:ARCCost3',p_web.GSV('job:Parts_Cost'))
        p_web.SSV('tmp:ARCCost4',p_web.GSV('job:Sub_Total'))
        p_web.SSV('tmp:ARCCost5',p_web.GSV('tmp:ARCCVAT'))
        p_web.SSV('tmp:ARCCost6',p_web.GSV('tmp:ARCCTotal'))
        p_web.SSV('tmp:ARCCost7',p_web.GSV('tmp:ARCCPaid'))
        p_web.SSV('tmp:ARCCost8',p_web.GSV('tmp:ARCOutstanding'))

        p_web.SSV('tmp:ARCIgnoreDefaultCharges',p_web.GSV('job:Ignore_Chargeable_Charges'))


    of 'Warranty'
        p_web.SSV('Prompt:Cost1','Exchange Fee')
        p_web.SSV('Prompt:Cost2','Labour Cost')
        p_web.SSV('Prompt:Cost3','Parts Cost')
        p_web.SSV('Prompt:Cost4','Sub Total')
        p_web.SSV('Prompt:Cost5','V.A.T.')
        p_web.SSV('Prompt:Cost6','Total Cost')
        p_web.SSV('Prompt:Cost7','')
        p_web.SSV('Prompt:Cost8','')

        p_web.SSV('tmp:ARCCost1',p_web.GSV('job:Courier_Cost_Warranty'))
        p_web.SSV('tmp:ARCCost2',p_web.GSV('job:Labour_Cost_Warranty'))
        p_web.SSV('tmp:ARCCost3',p_web.GSV('job:Parts_Cost_Warranty'))
        p_web.SSV('tmp:ARCCost4',p_web.GSV('job:Sub_Total_Warranty'))
        p_web.SSV('tmp:ARCCost5',p_web.GSV('tmp:ARCWVAT'))
        p_web.SSV('tmp:ARCCost6',p_web.GSV('tmp:ARCWTotal'))
        p_web.SSV('tmp:ARCCost7',0)
        p_web.SSV('tmp:ARCCost8',0)

        p_web.SSV('tmp:ARCIgnoreDefaultCharges',p_web.GSV('job:Ignore_Warranty_Charges'))

    of 'Claim'
        p_web.SSV('Prompt:Cost1','Exchange Fee')
        p_web.SSV('Prompt:Cost2','Labour Cost')
        p_web.SSV('Prompt:Cost3','Parts Cost')
        p_web.SSV('Prompt:Cost4','Sub Total')
        p_web.SSV('Prompt:Cost5','V.A.T.')
        p_web.SSV('Prompt:Cost6','Total Cost')
        p_web.SSV('Prompt:Cost7','')
        p_web.SSV('Prompt:Cost8','')

        p_web.SSV('tmp:ARCCost1',p_web.GSV('job:Courier_Cost_Warranty'))
        p_web.SSV('tmp:ARCCost2',p_web.GSV('jobe:ClaimValue'))
        p_web.SSV('tmp:ARCCost3',p_web.GSV('jobe:ClaimPartsCost'))
        p_web.SSV('tmp:ARCCost4',p_web.GSV('tmp:ARCClaimSubTotal'))
        p_web.SSV('tmp:ARCCost5',p_web.GSV('tmp:ARCClaimVAT'))
        p_web.SSV('tmp:ARCCost6',p_web.GSV('tmp:ARCClaimTotal'))
        p_web.SSV('tmp:ARCCost7',0)
        p_web.SSV('tmp:ARCCost8',0)

        p_web.SSV('tmp:ARCIgnoreDefaultCharges',p_web.GSV('jobe:IgnoreClaimCosts'))

    of '3rd Party'
        p_web.SSV('Prompt:Cost1','Cost')
        p_web.SSV('Prompt:Cost2','')
        p_web.SSV('Prompt:Cost3','')
        p_web.SSV('Prompt:Cost4','')
        p_web.SSV('Prompt:Cost5','V.A.T.')
        p_web.SSV('Prompt:Cost6','Total')
        p_web.SSV('Prompt:Cost7','')
        p_web.SSV('Prompt:Cost8','Markup')

        p_web.SSV('tmp:ARCCost1',p_web.GSV('jobe:ARC3rdPartyCost'))
        p_web.SSV('tmp:ARCCost2',0)
        p_web.SSV('tmp:ARCCost3',0)
        p_web.SSV('tmp:ARCCost4',0)
        p_web.SSV('tmp:ARCCost5',p_web.GSV('tmp:ARC3rdPartyVAT'))
        p_web.SSV('tmp:ARCCost6',p_web.GSV('tmp:ARC3rdPartyTotal'))
        p_web.SSV('tmp:ARCCost7',0)
        p_web.SSV('tmp:ARCCost8',p_web.GSV('tmp:ARC3rdPartyMarkup'))

        p_web.SSV('tmp:ARCIgnoreDefaultCharges',p_web.GSV('jobe:Ignore3rdPartyCosts'))

    of 'Estimate'
        p_web.SSV('Prompt:Cost1','')
        IF (p_web.GSV('LoanAttachedToJob') = 1)
            p_web.SSV('Prompt:Cost1','Lost Loan Charge')
        END
        p_web.SSV('Prompt:Cost2','Labour Cost')
        p_web.SSV('Prompt:Cost3','Parts Cost')
        p_web.SSV('Prompt:Cost4','Sub Total')
        p_web.SSV('Prompt:Cost5','V.A.T.')
        p_web.SSV('Prompt:Cost6','Total Cost')
        p_web.SSV('Prompt:Cost7','')
        p_web.SSV('Prompt:Cost8','')

        p_web.SSV('tmp:ARCCost1',p_web.GSV('job:Courier_Cost_Estimate'))
        p_web.SSV('tmp:ARCCost2',p_web.GSV('job:Labour_Cost_Estimate'))
        p_web.SSV('tmp:ARCCost3',p_web.GSV('job:Parts_Cost_Estimate'))
        p_web.SSV('tmp:ARCCost4',p_web.GSV('job:Sub_Total_Estimate'))
        p_web.SSV('tmp:ARCCost5',p_web.GSV('tmp:ARCEVAT'))
        p_web.SSV('tmp:ARCCost6',p_web.GSV('tmp:ARCETotal'))
        p_web.SSV('tmp:ARCCost7',0)
        p_web.SSV('tmp:ARCCost8',0)

        p_web.SSV('tmp:ARCIgnoreDefaultCharges',p_web.GSV('job:Ignore_Estimate_Charges'))

    of 'Chargeable - Invoiced'
        p_web.SSV('Prompt:Cost1','')
        IF (p_web.GSV('LoanAttachedToJob') = 1)
            p_web.SSV('Prompt:Cost1','Lost Loan Charge')
        END
        p_web.SSV('Prompt:Cost2','Labour Cost')
        p_web.SSV('Prompt:Cost3','Parts Cost')
        p_web.SSV('Prompt:Cost4','Sub Total')
        p_web.SSV('Prompt:Cost5','V.A.T.')
        p_web.SSV('Prompt:Cost6','Total Cost')
        p_web.SSV('Prompt:Cost7','Paid')
        p_web.SSV('Prompt:Cost8','Outstanding')

        p_web.SSV('tmp:ARCCost1',p_web.GSV('job:Invoice_Courier_Cost'))
        p_web.SSV('tmp:ARCCost2',p_web.GSV('job:Invoice_Labour_Cost'))
        p_web.SSV('tmp:ARCCost3',p_web.GSV('job:Invoice_Parts_Cost'))
        p_web.SSV('tmp:ARCCost4',p_web.GSV('job:Invoice_Sub_Total'))
        p_web.SSV('tmp:ARCCost5',p_web.GSV('tmp:ARCCIVAT'))
        p_web.SSV('tmp:ARCCost6',p_web.GSV('tmp:ARCCITotal'))
        p_web.SSV('tmp:ARCCost7',p_web.GSV('tmp:ARCCIPaid'))
        p_web.SSV('tmp:ARCCost8',p_web.GSV('tmp:ARCIOutstanding'))

        p_web.SSV('tmp:ARCIgnoreDefaultCharges',p_web.GSV('job:Ignore_Chargeable_Charges'))

    of 'Warranty - Invoiced'
        p_web.SSV('Prompt:Cost1','Exchange Fee')
        p_web.SSV('Prompt:Cost2','Labour Cost')
        p_web.SSV('Prompt:Cost3','Parts Cost')
        p_web.SSV('Prompt:Cost4','Sub Total')
        p_web.SSV('Prompt:Cost5','V.A.T.')
        p_web.SSV('Prompt:Cost6','Total Cost')
        p_web.SSV('Prompt:Cost7','')
        p_web.SSV('Prompt:Cost8','')

        p_web.SSV('tmp:ARCCost1',p_web.GSV('job:WInvoice_Courier_Cost'))
        p_web.SSV('tmp:ARCCost2',p_web.GSV('job:WInvoice_Labour_Cost'))
        p_web.SSV('tmp:ARCCost3',p_web.GSV('job:WInvoice_Parts_Cost'))
        p_web.SSV('tmp:ARCCost4',p_web.GSV('job:WInvoice_Sub_Total'))
        p_web.SSV('tmp:ARCCost5',p_web.GSV('tmp:ARCWIVAT'))
        p_web.SSV('tmp:ARCCost6',p_web.GSV('tmp:ARCWITotal'))
        p_web.SSV('tmp:ARCCost7',0)
        p_web.SSV('tmp:ARCCost8',0)

        p_web.SSV('tmp:ARCIgnoreDefaultCharges',p_web.GSV('job:Ignore_Warranty_Charges'))

    of 'Claim - Invoiced'
        p_web.SSV('Prompt:Cost1','Exchange Fee')
        p_web.SSV('Prompt:Cost2','Labour Cost')
        p_web.SSV('Prompt:Cost3','Parts Cost')
        p_web.SSV('Prompt:Cost4','Sub Total')
        p_web.SSV('Prompt:Cost5','V.A.T.')
        p_web.SSV('Prompt:Cost6','Total Cost')
        p_web.SSV('Prompt:Cost7','')
        p_web.SSV('Prompt:Cost8','')

        p_web.SSV('tmp:ARCCost1',p_web.GSV('job:WInvoice_Courier_Cost'))
        p_web.SSV('tmp:ARCCost2',p_web.GSV('jobe:InvoiceClaimValue'))
        p_web.SSV('tmp:ARCCost3',p_web.GSV('jobe:InvClaimPartsCost'))
        p_web.SSV('tmp:ARCCost4',p_web.GSV('tmp:ARCClaimISubTotal'))
        p_web.SSV('tmp:ARCCost5',p_web.GSV('tmp:ARCClaimIVAT'))
        p_web.SSV('tmp:ARCCost6',p_web.GSV('tmp:ARCClaimITotal'))
        p_web.SSV('tmp:ARCCost7',0)
        p_web.SSV('tmp:ARCCost8',0)

        p_web.SSV('tmp:AdjustmentCost1',p_web.GSV('jobe:ExchangeAdjustment'))
        p_web.SSV('tmp:AdjustmentCost2',p_web.GSV('jobe:LabourAdjustment'))
        p_web.SSV('tmp:AdjustmentCost3',p_web.GSV('jobe:PartsAdjustment'))
        p_web.SSV('tmp:AdjustmentCost4',p_web.GSV('tmp:AdjustmentSubTotal'))
        p_web.SSV('tmp:AdjustmentCost5',p_web.GSV('tmp:AdjustmentVAT'))
        p_web.SSV('tmp:AdjustmentCost6',p_web.GSV('tmp:AdjustmentTotal'))

        p_web.SSV('tmp:ARCIgnoreDefaultCharges',p_web.GSV('jobe:IgnoreClaimCosts'))

    of 'Manufacturer Payment'
        p_web.SSV('Prompt:Cost1','Labour Paid')
        p_web.SSV('Prompt:Cost2','Parts Paid')
        p_web.SSV('Prompt:Cost3','Other Costs')
        p_web.SSV('Prompt:Cost4','Sub Total')
        p_web.SSV('Prompt:Cost5','V.A.T.')
        p_web.SSV('Prompt:Cost6','Total Paid')
        p_web.SSV('Prompt:Cost7','')
        p_web.SSV('Prompt:Cost8','')

        p_web.SSV('tmp:ARCCost1',p_web.GSV('jobe2:WLabourPaid'))
        p_web.SSV('tmp:ARCCost2',p_web.GSV('jobe2:WPartsPaid'))
        p_web.SSV('tmp:ARCCost3',p_web.GSV('jobe2:WOtherCosts'))
        p_web.SSV('tmp:ARCCost4',p_web.GSV('jobe2:WSubTotal'))
        p_web.SSV('tmp:ARCCost5',p_web.GSV('jobe2:WVAT'))
        p_web.SSV('tmp:ARCCost6',p_web.GSV('jobe2:WTotal'))
        p_web.SSV('tmp:ARCCost7',0)
        p_web.SSV('tmp:ARCCost8',0)
    end ! case (p_web.GSV('tmp:ARCViewCostType'))


displayRRCCostFields      routine
    case (p_web.GSV('tmp:RRCViewCostType'))
    of 'Chargeable'

        p_web.SSV('Prompt:RCost1','')
        IF (p_web.GSV('LoanAttachedToJob') = 1)
            p_web.SSV('Prompt:RCost1','Lost Loan Charge')
        END

        p_web.SSV('Prompt:RCost2','Labour Cost')
        p_web.SSV('Prompt:RCost3','Parts Cost')
        p_web.SSV('Prompt:RCost4','Sub Total')
        p_web.SSV('Prompt:RCost5','V.A.T.')
        p_web.SSV('Prompt:RCost6','Total Cost')
        p_web.SSV('Prompt:RCost7','Paid')
        p_web.SSV('Prompt:RCost8','Outstanding')

        p_web.SSV('tmp:RRCCost0',p_web.GSV('jobe2:JobDiscountAmnt'))
        p_web.SSV('tmp:RRCCost1',p_web.GSV('job:Courier_Cost'))
        p_web.SSV('tmp:RRCCost2',p_web.GSV('jobe:RRCCLabourCost'))
        p_web.SSV('tmp:RRCCost3',p_web.GSV('jobe:RRCCPartsCost'))
        p_web.SSV('tmp:RRCCost4',p_web.GSV('jobe:RRCCSubTotal'))
        p_web.SSV('tmp:RRCCost5',p_web.GSV('tmp:RRCCVAT'))
        p_web.SSV('tmp:RRCCost6',p_web.GSV('tmp:RRCCTotal'))
        p_web.SSV('tmp:RRCCost7',p_web.GSV('tmp:RRCCPaid'))
        p_web.SSV('tmp:RRCCost8',p_web.GSV('tmp:RRCOutstanding'))


        p_web.SSV('tmp:RRCIgnoreDefaultCharges',p_web.GSV('jobe:IgnoreRRCChaCosts'))

        if (p_web.GSV('jobe2:JobDiscountAmnt') > 0 or p_web.GSV('tmp:RRCIgnoreDefaultCharges') <> 0)
            p_web.SSV('Prompt:RCost0','Discount')
        else
            p_web.SSV('Prompt:RCost0','')
            p_web.SSV('Comment:RCost0','')
        end

        if (p_web.GSV('tmp:RRCIgnoreDefaultCharges') = 1)
            p_web.SSV('ReadOnly:RRCCost2',0)
        else
            p_web.SSV('ReadOnly:RRCCost2',1)
        end

    of 'Warranty'
        p_web.SSV('Prompt:RCost0','')
        p_web.SSV('Comment:RCost0','')
        p_web.SSV('Prompt:RCost1','')
        p_web.SSV('Prompt:RCost2','Labour Cost')
        p_web.SSV('Prompt:RCost3','Parts Cost')
        p_web.SSV('Prompt:RCost4','Sub Total')
        p_web.SSV('Prompt:RCost5','V.A.T.')
        p_web.SSV('Prompt:RCost6','Total Cost')
        p_web.SSV('Prompt:RCost7','')
        p_web.SSV('Prompt:RCost8','')

        p_web.SSV('tmp:RRCCost1',p_web.GSV('job:Courier_Cost_Warranty'))
        p_web.SSV('tmp:RRCCost2',p_web.GSV('jobe:RRCWLabourCost'))
        p_web.SSV('tmp:RRCCost3',p_web.GSV('jobe:RRCWPartsCost'))
        p_web.SSV('tmp:RRCCost4',p_web.GSV('jobe:RRCWSubTotal'))
        p_web.SSV('tmp:RRCCost5',p_web.GSV('tmp:RRCWVAT'))
        p_web.SSV('tmp:RRCCost6',p_web.GSV('tmp:RRCWTotal'))
        p_web.SSV('tmp:RRCCost7',0)
        p_web.SSV('tmp:RRCCost8',0)

        p_web.SSV('tmp:RRCIgnoreDefaultCharges',p_web.GSV('jobe:IgnoreRRCWarCosts'))
        if (p_web.GSV('tmp:RRCIgnoreDefaultCharges') = 1)
            p_web.SSV('ReadOnly:RRCCost2',0)
        else
            p_web.SSV('ReadOnly:RRCCost2',1)
        end


    of 'Handling'
        p_web.SSV('Prompt:RCost0','')
        p_web.SSV('Comment:RCost0','')
        p_web.SSV('Prompt:RCost1','Handling Fee')
        p_web.SSV('Prompt:RCost2','')
        p_web.SSV('Prompt:RCost3','')
        p_web.SSV('Prompt:RCost4','')
        p_web.SSV('Prompt:RCost5','')
        p_web.SSV('Prompt:RCost6','')
        p_web.SSV('Prompt:RCost7','')
        p_web.SSV('Prompt:RCost8','')

        p_web.SSV('tmp:RRCCost1',p_web.GSV('tmp:HandlingFee'))
        p_web.SSV('tmp:RRCCost2',0)
        p_web.SSV('tmp:RRCCost3',0)
        p_web.SSV('tmp:RRCCost4',0)
        p_web.SSV('tmp:RRCCost5',0)
        p_web.SSV('tmp:RRCCost6',0)
        p_web.SSV('tmp:RRCCost7',0)
        p_web.SSV('tmp:RRCCost8',0)



    of 'Exchange'
        p_web.SSV('Prompt:RCost0','')
        p_web.SSV('Comment:RCost0','')
        p_web.SSV('Prompt:RCost1','Exchange Fee')
        p_web.SSV('Prompt:RCost2','')
        p_web.SSV('Prompt:RCost3','')
        p_web.SSV('Prompt:RCost4','')
        p_web.SSV('Prompt:RCost5','')
        p_web.SSV('Prompt:RCost6','')
        p_web.SSV('Prompt:RCost7','')
        p_web.SSV('Prompt:RCost8','')

        p_web.SSV('tmp:RRCCost1',p_web.GSV('tmp:ExchangeFee'))
        p_web.SSV('tmp:RRCCost2',0)
        p_web.SSV('tmp:RRCCost3',0)
        p_web.SSV('tmp:RRCCost4',0)
        p_web.SSV('tmp:RRCCost5',0)
        p_web.SSV('tmp:RRCCost6',0)
        p_web.SSV('tmp:RRCCost7',0)
        p_web.SSV('tmp:RRCCost8',0)

    of 'Estimate'
        p_web.SSV('Prompt:RCost0','')
        p_web.SSV('Comment:RCost0','')
        p_web.SSV('Prompt:RCost1','Lost Loan Charge')
        p_web.SSV('Prompt:RCost2','Labour Cost')
        p_web.SSV('Prompt:RCost3','Parts Cost')
        p_web.SSV('Prompt:RCost4','Sub Total')
        p_web.SSV('Prompt:RCost5','V.A.T.')
        p_web.SSV('Prompt:RCost6','Total Cost')
        p_web.SSV('Prompt:RCost7','')
        p_web.SSV('Prompt:RCost8','')

        p_web.SSV('tmp:RRCCost1',p_web.GSV('job:Courier_Cost_Estimate'))
        p_web.SSV('tmp:RRCCost2',p_web.GSV('jobe:RRCELabourCost'))
        p_web.SSV('tmp:RRCCost3',p_web.GSV('jobe:RRCEPartsCost'))
        p_web.SSV('tmp:RRCCost4',p_web.GSV('jobe:RRCESubTotal'))
        p_web.SSV('tmp:RRCCost5',p_web.GSV('tmp:RRCEVAT'))
        p_web.SSV('tmp:RRCCost6',p_web.GSV('tmp:RRCETotal'))
        p_web.SSV('tmp:RRCCost7',0)
        p_web.SSV('tmp:RRCCost8',0)

        p_web.SSV('tmp:RRCIgnoreDefaultCharges',p_web.GSV('jobe:IgnoreRRCEstCosts'))
        if (p_web.GSV('tmp:RRCIgnoreDefaultCharges') = 1)
            p_web.SSV('ReadOnly:RRCCost2',0)
        else
            p_web.SSV('ReadOnly:RRCCost2',1)
        end

    of 'Chargeable - Invoiced'

        if (p_web.GSV('jobe2:InvDiscountAmnt') > 0  or p_web.GSV('tmp:RRCIgnoreDefaultCharges') <> 0)
            p_web.SSV('Prompt:RCost0','Discount')
        else
            p_web.SSV('Prompt:RCost0','')
            p_web.SSV('Comment:RCost0','')
        end

        p_web.SSV('Prompt:RCost1','')
        IF (p_web.GSV('LoanAttachedToJob') = 1)
            p_web.SSV('Prompt:RCost1','Lost Loan Charge')
        END
        p_web.SSV('Prompt:RCost2','Labour Cost')
        p_web.SSV('Prompt:RCost3','Parts Cost')
        p_web.SSV('Prompt:RCost4','Sub Total')
        p_web.SSV('Prompt:RCost5','V.A.T.')
        p_web.SSV('Prompt:RCost6','Total Cost')
        p_web.SSV('Prompt:RCost7','Paid')
        p_web.SSV('Prompt:RCost8','Outstanding')

        p_web.SSV('tmp:RRCCOst0',p_web.GSV('jobe2:InvDiscountAmnt'))
        p_web.SSV('tmp:RRCCost1',p_web.GSV('job:Invoice_Courier_Cost'))
        p_web.SSV('tmp:RRCCost2',p_web.GSV('jobe:InvRRCCLabourCost'))
        p_web.SSV('tmp:RRCCost3',p_web.GSV('jobe:InvRRCCPartsCost'))
        p_web.SSV('tmp:RRCCost4',p_web.GSV('jobe:InvRRCCSubTotal'))
        p_web.SSV('tmp:RRCCost5',p_web.GSV('tmp:RRCCIVAT'))
        p_web.SSV('tmp:RRCCost6',p_web.GSV('tmp:RRCCITotal'))
        p_web.SSV('tmp:RRCCost7',p_web.GSV('tmp:RRCCIPaid'))
        p_web.SSV('tmp:RRCCost8',p_web.GSV('tmp:RRCIOutstanding'))

        p_web.SSV('tmp:RRCIgnoreDefaultCharges',p_web.GSV('jobe:IgnoreRRCChaCosts'))

    of 'Warranty - Invoiced'
        p_web.SSV('Prompt:RCost0','')
        p_web.SSV('Comment:RCost0','')
        p_web.SSV('Prompt:RCost1','Lost Loan Charge')
        p_web.SSV('Prompt:RCost2','Labour Cost')
        p_web.SSV('Prompt:RCost3','Parts Cost')
        p_web.SSV('Prompt:RCost4','Sub Total')
        p_web.SSV('Prompt:RCost5','V.A.T.')
        p_web.SSV('Prompt:RCost6','Total Cost')
        p_web.SSV('Prompt:RCost7','')
        p_web.SSV('Prompt:RCost8','')

        p_web.SSV('tmp:RRCCost1',p_web.GSV('job:WInvoice_Courier_Cost'))
        p_web.SSV('tmp:RRCCost2',p_web.GSV('jobe:InvRRCWLabourCost'))
        p_web.SSV('tmp:RRCCost3',p_web.GSV('jobe:InvRRCWPartsCost'))
        p_web.SSV('tmp:RRCCost4',p_web.GSV('jobe:InvRRCWSubTotal'))
        p_web.SSV('tmp:RRCCost5',p_web.GSV('tmp:RRCWIVAT'))
        p_web.SSV('tmp:RRCCost6',p_web.GSV('tmp:RRCWITotal'))
        p_web.SSV('tmp:RRCCost7',0)
        p_web.SSV('tmp:RRCCost8',0)


        p_web.SSV('tmp:RRCIgnoreDefaultCharges',p_web.GSV('jobe:IgnoreRRCWarCosts'))

    of 'Credits'
        p_web.SSV('Prompt:RCost0','')
        p_web.SSV('Comment:RCost0','')
        p_web.SSV('tmp:CreditTotal',p_web.GSV('tmp:RRCCITotal'))
        Access:JOBSINV.Clearkey(jov:typeRecordKey)
        jov:refNumber    = p_web.GSV('job:Ref_Number')
        jov:type    = 'C'
        set(jov:typeRecordKey,jov:typeRecordKey)
        loop
            if (Access:JOBSINV.Next())
                Break
            end ! if (Access:JOBSINV.Next())
            if (jov:refNumber    <> p_web.GSV('job:Ref_Number'))
                Break
            end ! if (jov:refNumber    <> p_web.GSV('job:Ref_Number'))
            if (jov:type    <> 'C')
                Break
            end ! if (jov:type    <> 'C')
            p_web.SSV('tmp:RRCCITotal',p_web.GSV('tmp:RRCCITotal') - jov:creditAmount)
            p_web.SSV('tmp:RRCIOutstanding',p_web.GSV('tmp:RRCIOutstanding') - jov:creditAmount)
        end ! loop

        access:INVOICE.clearKey(inv:invoice_Number_Key)
        inv:invoice_Number = p_web.GSV('job:invoice_number')
        if (access:INVOICE.tryfetch(inv:invoice_Number_Key) = level:Benign)
        end !

        access:TRADEACC.clearKey(tra:account_Number_Key)
        tra:account_Number = p_web.GSV('wob:HeadAccountNumber')
        if (access:TRADEACC.tryfetch(tra:account_Number_Key) = level:Benign)
        end

        p_web.SSV('tmp:OriginalInvoice',clip(inv:invoice_number) & '-' & clip(tra:BranchIdentification))


        p_web.SSV('Prompt:RCost1','Original Total')
        p_web.SSV('Prompt:RCost2','Original Invoice')
        p_web.SSV('Prompt:RCost3','')
        p_web.SSV('Prompt:RCost4','')
        p_web.SSV('Prompt:RCost5','')
        p_web.SSV('Prompt:RCost6','')
        p_web.SSV('Prompt:RCost7','')
        p_web.SSV('Prompt:RCost8','Current Total')

        p_web.SSV('tmp:RRCCost1',p_web.GSV('tmp:CreditTotal'))
        p_web.SSV('tmp:RRCCost2',0)
        p_web.SSV('tmp:RRCCost3',0)
        p_web.SSV('tmp:RRCCost4',0)
        p_web.SSV('tmp:RRCCost5',0)
        p_web.SSV('tmp:RRCCost6',0)
        p_web.SSV('tmp:RRCCost7',0)
        p_web.SSV('tmp:RRCCost8',p_web.GSV('tmp:RRCCITotal'))

    end !case (p_web.GSV('tmp:RRCViewCostType'))
displayOtherFields      routine
    p_web.SSV('Hide:ExchangeReplacement',1)

    p_web.SSV('Prompt:RCost1','')
    p_web.SSV('Prompt:ACost1','')


    if (p_web.GSV('tmp:ARCViewCostType') = 'Warranty')
        if (p_web.GSV('job:exchange_unit_number') > 0 and |
            (p_web.GSV('jobe:exchangedAtRRC') = 0 or |
                p_web.GSV('jobe:exchangedAtRRC') = 1 and p_web.GSV('job:repair_Type_Warranty') = 'R.T.M.'))
            p_web.SSV('Prompt:RCost1','Lost Loan Charge')
        else !job:Courier_Cost
            p_web.SSV('Prompt:RCost1','')
        end ! job:Courier_Cost
    end ! if (p_web.GSV('tmp:ARCViewCostType') = 'Warranty')

    LoanAttachedToJob(p_web)
    if (p_web.GSV('LoanAttachedToJob') = 1)
        if (p_web.GSV('jobe:HubRepair') = 0)
            if (p_web.GSV('tmp:RRCViewCostType') = 'Chargeable' Or |
                p_web.GSV('tmp:RRCViewCostType') = 'Chargeable - Invoiced' Or |
                p_web.GSV('tmp:RRCViewCostType') = 'Estimate')

                p_web.SSV('Prompt:RCost1','Lost Loan Charge')
            end
        else ! if (p_web.GSV('jobe:HubRepair') = 0)
            if (p_web.GSV('tmp:ARCViewCostType') = 'Chargeable' Or |
                p_web.GSV('tmp:ARCViewCostType') = 'Chargeable - Invoiced' Or |
                p_web.GSV('tmp:ARCViewCostType') = 'Estimate')

                p_web.SSV('Prompt:ACost1','Lost Loan Charge')
            end

        end ! if (p_web.GSV('jobe:HubRepair') = 0)
    else ! if (p_web.GSV('LoanAttachedToJob') = 1)
        p_web.SSV('Prompt:RCost1','')
        if (p_web.GSV('jobe:Engineer48HourOption') = 1 and p_web.GSV('job:chargeable_job') = 'YES')
            if (p_web.GSV('jobe:WebJob') = 1)
                p_web.SSV('Hide:ExchangeReplacement',0)

                if (securityCheckFailed(p_web.GSV('BookingUserPassword'),'EXCHANGE REPLACEMENT CHARGE'))
                else ! if (securityCheckFailed(p_web.GSV('BookingUserPassword','EXCHANGE REPLACEMENT CHARGE')
                end ! if (securityCheckFailed(p_web.GSV('BookingUserPassword','EXCHANGE REPLACEMENT CHARGE'))

                if (p_web.GSV('jobe:ExcReplcamentCharge') = 1)
                    p_web.SSV('Prompt:RCost1','Exchange Replacement')

                    p_web.SSV('Prompt:ACost1','Exchange Replacement')

                end ! if (p_web.GSV('jobe:ExcReplcamentCharge') = 1)
            end ! if (p_web.GSV('jobe:WebJob') = 1)
        end !if (p_web.GSV('jobe:Engineer48HourOption') = 1 and p_web.GSV('job:chargeable_job') = 'YES')
    end ! if (p_web.GSV('LoanAttachedToJob') = 1)
pricingRoutine      Routine
data
locARCPaid    Real()
locRRCPaid    Real()
locTotalPaid    Real()
code
    jobPricingRoutine(p_web)
    if (p_web.GSV('job:warranty_job') = 'YES')
        if (p_web.GSV('job:invoice_number_warranty') > 0)
            p_web.SSV('tmp:HandlingFee',p_web.GSV('jobe:invoiceHandlingFee'))
            p_web.SSV('tmp:ExchangeFee',p_web.GSV('jobe:invoiceExchangeRate'))

            Access:INVOICE.Clearkey(inv:invoice_number_Key)
            inv:invoice_number    = p_web.GSV('job:invoice_Number_warranty')
            if (Access:INVOICE.TryFetch(inv:invoice_number_Key) = Level:Benign)
                ! Found
                p_web.SSV('tmp:ARCInvoiceNumber',inv:invoice_number)

                p_web.SSV('tmp:ARCWIVat',p_web.GSV('job:WInvoice_Courier_Cost') * (inv:Vat_Rate_Labour/100) + |
                                p_web.GSV('job:WInvoice_Parts_Cost') * (inv:Vat_Rate_Parts/100) + |
                                p_web.GSV('job:WInvoice_Labour_Cost') * (inv:Vat_Rate_Labour/100))
                p_web.SSV('tmp:ARCWITotal',p_web.GSV('tmp:ARCWIVat') + p_web.GSV('job:WInvoice_Sub_Total'))


                p_web.SSV('tmp:ARCClaimIVAT',p_web.GSV('job:WInvoice_Courier_Cost') * (inv:Vat_Rate_Labour/100) + |
                                p_web.GSV('jobe:InvClaimPartsCost') * (inv:Vat_Rate_Parts/100) + |
                                p_web.GSV('jobe:InvoiceClaimValue') * (inv:Vat_Rate_Labour/100))
                p_web.SSV('tmp:ARCClaimISubTotal',p_web.GSV('job:WInvoice_Courier_Cost') + p_web.GSV('jobe:InvClaimPartsCost') + |
                                    p_web.GSV('jobe:InvoiceClaimValue'))
                p_web.SSV('tmp:ARCClaimITotal',p_web.GSV('tmp:ARCClaimIVat') + p_web.GSV('tmp:ARCClaimISubTotal'))

                p_web.SSV('tmp:AdjustmentSubTotal',p_web.GSV('jobe:LabourAdjustment') + p_web.GSV('jobe:ExchangeAdjustment') + |
                                        p_web.GSV('jobe:PartsAdjustment'))
                p_web.SSV('tmp:AdjustmentVAT',p_web.GSV('jobe:PartsAdjustment') * (inv:Vat_Rate_Parts/100) + |
                                    p_web.GSV('jobe:LabourAdjustment') * (inv:Vat_Rate_Labour/100) +|
                                    p_web.GSV('jobe:ExchangeAdjustment') * (inv:Vat_Rate_Labour/100))
                p_web.SSV('tmp:AdjustmentTotal',p_web.GSV('tmp:AdjustmentVAT') + p_web.GSV('tmp:AdjustmentSubTotal'))

            else ! if (Access:INVOICE.TryFetch(inv:invoice_number_Key) = Level:Benign)
                ! Error
            end ! if (Access:INVOICE.TryFetch(inv:invoice_number_Key) = Level:Benign)
        else ! if (p_web.GSV('job:invoice_number_warranty') > 0)
            p_web.SSV('tmp:HandlingFee',p_web.GSV('jobe:HandlingFee'))
            p_web.SSV('tmp:ExchangeFee',p_web.GSV('jobe:ExchangeRate'))
        end ! if (p_web.GSV('job:invoice_number_warranty') > 0)
    end ! if (p_web.GSV('job:warranty_job') = 'YES')

    if (p_web.GSV('job:Chargeable_Job') = 'YES')
        if (p_web.GSV('job:Invoice_Number') > 0)
            p_web.SSV('tmp:HandlingFee',p_web.GSV('jobe:InvoiceHandlingFee'))
            p_web.SSV('tmp:ExchangeFee',p_web.GSV('jobe:InvoiceExchangeRate'))

            Access:INVOICE.Clearkey(inv:invoice_number_Key)
            inv:invoice_number    = p_web.GSV('job:invoice_number')
            if (Access:INVOICE.TryFetch(inv:invoice_number_Key) = Level:Benign)
                ! Found

                Access:TRADEACC.Clearkey(tra:account_number_key)
                tra:account_number    = p_web.GSV('Default:ARCLocation')
                if (Access:TRADEACC.TryFetch(tra:account_number_key) = Level:Benign)
                    ! Found
                    p_web.SSV('tmp:ARCInvoiceNumber',Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification) & ' (' &CLIP(Format(inv:Date_Created,@d6))& ')')
                else ! if (Access:TRADEACC.TryFetch(tra:account_number_key) = Level:Benign)
                    ! Error
                end ! if (Access:TRADEACC.TryFetch(tra:account_number_key) = Level:Benign)

                if (inv:exportedRRCOracle)
                    p_web.SSV('jobe:InvRRCCSubTotal',p_web.GSV('job:Invoice_Courier_Cost') + |
                                            p_web.GSV('jobe:InvRRCCLabourCost') + |
                                            p_web.GSV('jobe:InvRRCCPartsCost'))

                    p_web.SSV('tmp:RRCCIVat',p_web.GSV('job:Invoice_Courier_Cost') * (inv:Vat_Rate_Labour/100) + |
                                    p_web.GSV('jobe:InvRRCCLabourCost') * (inv:Vat_Rate_Labour/100) + |
                                    p_web.GSV('jobe:InvRRCCPartsCost') * (inv:Vat_Rate_Parts/100))
                    p_web.SSV('tmp:RRCCITotal',p_web.GSV('tmp:RRCCIVat') + p_web.GSV('jobe:InvRRCCSubTotal'))

                    Access:TRADEACC.Clearkey(tra:account_number_key)
                    tra:account_number    = p_web.GSV('wob:HeadAccountNumber')
                    if (Access:TRADEACC.TryFetch(tra:account_number_key) = Level:Benign)
                        ! Found
                        p_web.SSV('tmp:RRCInvoiceNumber',Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification) & ' (' &CLIP(Format(inv:RRCInvoiceDate,@d6))& ')')
                        if (p_web.GSV('job:exchange_unit_Number') > 0)
                            p_web.SSV('tmp:RRCInvoiceNumber',p_web.GSV('tmp:RRCInvoiceNumber') & ' Exchange')
                        end ! if (p_web.GSV('job:exchange_unit_Number') > 0)
                    else ! if (Access:TRADEACC.TryFetch(tra:account_number_key) = Level:Benign)
                        ! Error
                    end ! if (Access:TRADEACC.TryFetch(tra:account_number_key) = Level:Benign)
                end ! if (inv:exportedRRCOracle)

                p_web.SSV('tmp:ARCCIVat',p_web.GSV('job:Invoice_Courier_Cost') * (inv:Vat_Rate_Labour/100) + |
                                p_web.GSV('job:Invoice_Parts_Cost') * (inv:Vat_Rate_Parts/100) + |
                                p_web.GSV('job:Invoice_Labour_Cost') * (inv:Vat_Rate_Labour/100))
                p_web.SSV('job:Invoice_Sub_Total',p_web.GSV('job:Invoice_Courier_Cost') + |
                                        p_web.GSV('job:Invoice_Labour_Cost') + |
                                        p_web.GSV('job:Invoice_Parts_Cost'))
                p_web.SSV('tmp:ARCCITotal',p_web.GSV('tmp:ARCCIVat') + p_web.GSV('job:invoice_Sub_Total'))

            else ! if (Access:INVOICE.TryFetch(inv:invoice_number_Key) = Level:Benign)
                ! Error
            end ! if (Access:INVOICE.TryFetch(inv:invoice_number_Key) = Level:Benign)
        else ! if (p_web.GSV('job:Invoice_Number') > 0)
            p_web.SSV('tmp:HandlingFee',p_web.GSV('jobe:HandlingFee'))
            p_web.SSV('tmp:ExchangeFee',p_web.GSV('jobe:ExchangeRate'))
        end ! if (p_web.GSV('job:Invoice_Number') > 0)
    end ! if (p_web.GSV('job:Chargeable_Job') = 'YES')


    ! Paid ?

    Access:JOBPAYMT.Clearkey(jpt:all_Date_Key)
    jpt:ref_Number    = p_web.GSV('job:Ref_Number')
    set(jpt:all_Date_Key,jpt:all_Date_Key)
    loop
        if (Access:JOBPAYMT.Next())
            Break
        end ! if (Access:JOBPAYMT.Next())
        if (jpt:ref_Number    <> p_web.GSV('job:Ref_Number'))
            Break
        end ! if (jpt:ref_Number    <> p_web.GSV('job:Ref_Number'))

        Access:USERS.Clearkey(use:user_code_Key)
        use:user_code    = jpt:user_code
        if (Access:USERS.TryFetch(use:user_code_Key) = Level:Benign)
            ! Found
            if (use:location = p_web.GSV('ARC:SiteLocation'))
                locARCPaid += jpt:Amount
            else
                locRRCPaid += jpt:Amount
            end ! if (use:location = p_web.GSV('ARC:SiteLocation'))
        else ! if (Access:USERS.TryFetch(use:user_code_Key) = Level:Benign)
            ! Error
        end ! if (Access:USERS.TryFetch(use:user_code_Key) = Level:Benign)
        locTotalPaid += jpt:amount
    end ! loop

    p_web.SSV('tmp:RRCCPaid',locRRCPaid)
    p_web.SSV('tmp:RRCCIPaid',locRRCPaid)
    p_web.SSV('tmp:ARCCPaid',locARCPaid)
    p_web.SSV('tmp:ARCCIPaid',locARCPaid)


    Access:TRDPARTY.Clearkey(trd:company_Name_Key)
    trd:company_Name    = p_web.GSV('job:Third_Party_Site')
    if (Access:TRDPARTY.TryFetch(trd:company_Name_Key) = Level:Benign)
        ! Found
        p_web.SSV('tmp:ARC3rdPartyVAT',(p_web.GSV('jobe:ARC3rdPartyCost') * trd:VatRate/100))
        p_web.SSV('tmp:ARC3rdPartyTotal',(p_web.GSV('jobe:ARC3rdPartyCost') * trd:VatRate/100))
        p_web.SSV('tmp:ARC3rdPartyMarkup',p_web.GSV('jobe:ARC3rdPartyCost') + p_web.GSV('tmp:ARC3rdPartyTotal'))

    else ! if (Access:TRDPARTY.TryFetch(trd:company_Name_Ke) = Level:Benign)
        ! Error
    end ! if (Access:TRDPARTY.TryFetch(trd:company_Name_Ke) = Level:Benign)


    p_web.SSV('tmp:ARCCVat',(p_web.GSV('job:Courier_Cost') * (GetVATRate(p_web.GSV('job:Account_Number'),'L') /100)) + |
                               (p_web.GSV('job:Parts_Cost') * (GetVATRate(p_web.GSV('job:Account_Number'),'P') /100)) +  |
                                (p_web.GSV('job:Labour_Cost') * (GetVATRate(p_web.GSV('job:Account_Number'),'L')/100)))

    p_web.SSV('tmp:ARCCTotal',p_web.GSV('tmp:ARCCVat') + p_web.GSV('job:sub_total'))

    p_web.SSV('tmp:RRCCVat',(p_web.GSV('job:Courier_Cost') * (GetVATRate(p_web.GSV('job:Account_Number'),'L') /100)) + |
                           (p_web.GSV('jobe:RRCCPartsCost') * (GetVATRate(p_web.GSV('job:Account_Number'),'P') /100)) +  |
                            (p_web.GSV('jobe:RRCCLabourCost') * (GetVATRate(p_web.GSV('job:Account_Number'),'L')/100)))

    p_web.SSV('tmp:RRCCTotal',p_web.GSV('tmp:RRCCVat') + p_web.GSV('jobe:RRCCSubTotal'))

    p_web.SSV('tmp:ARCWVAT',(p_web.GSV('job:Parts_Cost_Warranty') * (GetVATRate(p_web.GSV('job:Account_Number'),'P') /100)) +  |
                    (p_web.GSV('job:Labour_Cost_Warranty') * (GetVATRate(p_web.GSV('job:Account_Number'),'L')/100)) + |
                    (p_web.GSV('job:Courier_Cost_Warranty') * (GetVATRate(p_web.GSV('job:Account_Number'),'L') /100)))
    p_web.SSV('tmp:ARCWTotal',p_web.GSV('tmp:ARCWVat') + p_web.GSV('job:sub_total_warranty'))

    p_web.SSV('tmp:RRCWVAT',(p_web.GSV('jobe:RRCWPartsCost') * (GetVATRate(p_web.GSV('job:Account_Number'),'P') /100)) +  |
                    (p_web.GSV('jobe:RRCWLabourCost') * (GetVATRate(p_web.GSV('job:Account_Number'),'L')/100)))
    p_web.SSV('tmp:RRCWTotal',p_web.GSV('tmp:RRCWVAT') + p_web.GSV('jobe:RRCWSubTotal'))

    p_web.SSV('tmp:ARCEVAT',(p_web.GSV('job:Courier_Cost_Estimate') * (GetVATRate(p_web.GSV('job:Account_Number'),'L') /100)) + |
                           (p_web.GSV('job:Parts_Cost_Estimate') * (GetVATRate(p_web.GSV('job:Account_Number'),'P') /100)) +  |
                            (p_web.GSV('job:Labour_Cost_Estimate') * (GetVATRate(p_web.GSV('job:Account_Number'),'L')/100)))

    p_web.SSV('tmp:ARCETotal',p_web.GSV('tmp:ARCEVat') + p_web.GSV('job:Sub_Total_Estimate'))

    p_web.SSV('tmp:RRCEVAT',(p_web.GSV('job:Courier_Cost_Estimate') * (GetVATRate(p_web.GSV('job:Account_Number'),'L') /100)) + |
                           (p_web.GSV('jobe:RRCEPartsCost') * (GetVATRate(p_web.GSV('job:Account_Number'),'P') /100)) +  |
                            (p_web.GSV('jobe:RRCELabourCost') * (GetVATRate(p_web.GSV('job:Account_Number'),'L')/100)))
    p_web.SSV('tmp:RRCETotal',p_web.GSV('tmp:RRCEVAT') + p_web.GSV('jobe:RRCESubTotal'))

    p_web.SSV('jobe:InvRRCWSubTotal',p_web.GSV('jobe:InvRRCWPartsCost') + p_web.GSV('jobe:InvRRCWLabourCost'))

    p_web.SSV('tmp:RRCWIVAT',p_web.GSV('jobe:InvRRCWPartsCost') * (inv:Vat_Rate_Parts/100) + |
                    p_web.GSV('jobe:InvRRCWLabourCost') * (inv:Vat_Rate_Labour/100))
    p_web.SSV('tmp:RRCWITotal',p_web.GSV('tmp:RRCWIVAT') + p_web.GSV('jobe:InvRRCWSubTotal'))


    p_web.SSV('tmp:RRCOutstanding',p_web.GSV('tmp:RRCCTotal') - p_web.GSV('tmp:RRCCPaid'))
    p_web.SSV('tmp:ARCOutstanding',p_web.GSV('tmp:ARCCTotal') - p_web.GSV('tmp:ARCCPaid'))
    p_web.SSV('tmp:RRCIOutstanding',p_web.GSV('tmp:RRCCITotal') - p_web.GSV('tmp:RRCCIPaid'))
    p_web.SSV('tmp:ARCIOutstanding',p_web.GSV('tmp:ARCCITotal') - p_web.GSV('tmp:ARCCIPaid'))
restoreFields    routine
    p_web.SSV('job:Courier_Cost',p_web.GSV('save:Courier_Cost'))
    p_web.SSV('job:Labour_Cost',p_web.GSV('save:Labour_Cost'))
    p_web.SSV('job:Parts_Cot',p_web.GSV('save:Parts_Cost'))
    p_web.SSV('job:Sub_Total',p_web.GSV('save:Sub_Total'))
    p_web.SSV('job:Courier_Cost_Warranty',p_web.GSV('save:Courier_Cost_Warranty'))
    p_web.SSV('job:Labour_Cost_Warranty',p_web.GSV('save:Labour_Cost_Warranty'))
    p_web.SSV('job:Parts_Cost_Warranty',p_web.GSV('save:Parts_Cost_Warranty'))
    p_web.SSV('job:Sub_Total_Warranty',p_web.GSV('save:Sub_Total_Warranty'))
    p_web.SSV('jobe:ClaimValue',p_web.GSV('save:ClaimValue'))
    p_web.SSV('jobe:ClaimPartsCost',p_web.GSV('save:ClaimPartsCost'))
    p_web.SSV('jobe:ARC3rdPartyCost',p_web.GSV('save:ARC3rdPartyCost'))
    p_web.SSV('job:Courier_Cost_Estimate',p_web.GSV('save:Courier_Cost_Estimate'))
    p_web.SSV('job:Labour_Cost_Estimate',p_web.GSV('save:Labour_Cost_Estimate'))
    p_web.SSV('job:Parts_Cost_Estimate',p_web.GSV('save:Parts_Cost_Estimate'))
    p_web.SSV('job:Sub_Total_Estimate',p_web.GSV('save:Sub_Total_Estimate'))
    p_web.SSV('job:Invoice_Courier_Cost',p_web.GSV('save:Invoice_Courier_Cost'))
    p_web.SSV('job:Invoice_Labour_Cost',p_web.GSV('save:Invoice_Labour_Cost'))
    p_web.SSV('job:Invoice_Parts_Cost',p_web.GSV('save:Invoice_Parts_Cost'))
    p_web.SSV('job:Invoice_Sub_Total',p_web.GSV('save:Invoice_Sub_Total'))
    p_web.SSV('job:WInvoice_Courier_Cost',p_web.GSV('save:WInvoice_Courier_Cost'))
    p_web.SSV('job:WInvoice_Labour_Cost',p_web.GSV('save:WInvoice_Labour_Cost'))
    p_web.SSV('job:WInvoice_Parts_Cost',p_web.GSV('save:WInvoice_Parts_Cost'))
    p_web.SSV('job:WInvoice_Sub_Total',p_web.GSV('save:WInvoice_Sub_Total'))
    p_web.SSV('jobe:ExchangAdjustment',p_web.GSV('save:ExchangeAdjustment'))
    p_web.SSV('jobe:LabourAdjustment',p_web.GSV('save:LabourAdjustment'))
    p_web.SSV('jobe:PartsAdjustment',p_web.GSV('save:PartsAdjustment'))
    p_web.SSV('jobe2:WLabourPaid',p_web.GSV('save:WLabourPaid'))
    p_web.SSV('jobe2:WPartsPaid',p_web.GSV('save:WPartsPaid'))
    p_web.SSV('jobe2:WOtherCosts',p_web.GSV('save:WOtherCosts'))
    p_web.SSV('jobe2:WVat',p_web.GSV('save:WVat'))
    p_web.SSV('jobe2:WTotal',p_web.GSV('save:WTotal'))
    p_web.SSV('jobe:IgnoreClaimCosts',p_web.GSV('save:IgnoreClaimCosts'))
    p_web.SSV('job:Ignore_Warranty_Charges',p_web.GSV('save:Ignore_Warranty_Charges'))
    p_web.SSV('job:Ignore_Chargeable_Charges',p_web.GSV('save:Ignore_Chargeable_Charges'))
    p_web.SSV('job:Ignore_Estimate_Charges',p_web.GSV('save:Ignore_Estimate_Charges'))
    p_web.SSV('jobe:RRCClabourCost',p_web.GSV('save:RRCCLabourCost'))
    p_web.SSV('jobe:RRCCPartsCost',p_web.GSV('save:RRCCPartsCost'))
    p_web.SSV('jobe:RRCCSubTotal',p_web.GSV('save:RRCCSubTotal'))
    p_web.SSV('jobe:RRCWLabourCost',p_web.GSV('save:RRCWLabourCost'))
    p_web.SSV('jobe:RRCWPartsCost',p_web.GSV('save:RRCWPartsCost'))
    p_web.SSV('jobe:RRCWSubTotal',p_web.GSV('save:RRCWSubTotal'))
    p_web.SSV('jobe:IgnoreRRCChaCosts',p_web.GSV('save:IgnoreRRCChaCosts'))
    p_web.SSV('jobe:IgnoreRRCWarCosts',p_web.GSV('save:IgnoreRRCWarCosts'))
    p_web.SSV('jobe:RRCELabourCost',p_web.GSV('save:RRCELabourCost'))
    p_web.SSV('jobe:RRCEPartsCost',p_web.GSV('save:RRCEPartsCost'))
    p_web.SSV('jobe:RRCESubTotal',p_web.GSV('save:RRCESubTotal'))
    p_web.SSV('jobe:InvRRCCLabourCost',p_web.GSV('save:InvRRCCLabourCost'))
    p_web.SSV('jobe:InvRRCCPartsCost',p_web.GSV('save:InvRRCCPartsCost'))
    p_web.SSV('jobe:InvRRCCSubTotal',p_web.GSV('save:InvRRCCSubTotal'))
    p_web.SSV('jobe:IgnoreRRCEstCosts',p_web.GSV('save:IgnoreRRCEstCosts'))
    p_web.SSV('jobe:InvRRCWLabourCost',p_web.GSV('save:InvRRCWLabourCost'))
    p_web.SSV('jobe:InvRRCWPartsCost',p_web.GSV('save:InvRRCWPartsCost'))
    p_web.SSV('jobe:InvRRCWSubTotal',p_web.GSV('save:InvRRCWSubTotal'))
    p_web.SSV('jobe2:JobDiscountAmnt',p_web.GSV('save:JobDiscountAmnt'))
    p_web.SSV('jobe2:InvDiscountAmnt',p_web.GSV('save:InvDiscountAmnt'))
saveFields    routine
    p_web.SSV('save:Courier_Cost',p_web.GSV('job:Courier_Cost'))
    p_web.SSV('save:Labour_Cost',p_web.GSV('job:Labour_Cost'))
    p_web.SSV('save:Parts_Cot',p_web.GSV('job:Parts_Cost'))
    p_web.SSV('save:Sub_Total',p_web.GSV('job:Sub_Total'))
    p_web.SSV('save:Courier_Cost_Warranty',p_web.GSV('job:Courier_Cost_Warranty'))
    p_web.SSV('save:Labour_Cost_Warranty',p_web.GSV('job:Labour_Cost_Warranty'))
    p_web.SSV('save:Parts_Cost_Warranty',p_web.GSV('job:Parts_Cost_Warranty'))
    p_web.SSV('save:Sub_Total_Warranty',p_web.GSV('job:Sub_Total_Warranty'))
    p_web.SSV('save:ClaimValue',p_web.GSV('jobe:ClaimValue'))
    p_web.SSV('save:ClaimPartsCost',p_web.GSV('jobe:ClaimPartsCost'))
    p_web.SSV('save:ARC3rdPartyCost',p_web.GSV('jobe:ARC3rdPartyCost'))
    p_web.SSV('save:Courier_Cost_Estimate',p_web.GSV('job:Courier_Cost_Estimate'))
    p_web.SSV('save:Labour_Cost_Estimate',p_web.GSV('job:Labour_Cost_Estimate'))
    p_web.SSV('save:Parts_Cost_Estimate',p_web.GSV('job:Parts_Cost_Estimate'))
    p_web.SSV('save:Sub_Total_Estimate',p_web.GSV('job:Sub_Total_Estimate'))
    p_web.SSV('save:Invoice_Courier_Cost',p_web.GSV('job:Invoice_Courier_Cost'))
    p_web.SSV('save:Invoice_Labour_Cost',p_web.GSV('job:Invoice_Labour_Cost'))
    p_web.SSV('save:Invoice_Parts_Cost',p_web.GSV('job:Invoice_Parts_Cost'))
    p_web.SSV('save:Invoice_Sub_Total',p_web.GSV('job:Invoice_Sub_Total'))
    p_web.SSV('save:WInvoice_Courier_Cost',p_web.GSV('job:WInvoice_Courier_Cost'))
    p_web.SSV('save:WInvoice_Labour_Cost',p_web.GSV('job:WInvoice_Labour_Cost'))
    p_web.SSV('save:WInvoice_Parts_Cost',p_web.GSV('job:WInvoice_Parts_Cost'))
    p_web.SSV('save:WInvoice_Sub_Total',p_web.GSV('job:WInvoice_Sub_Total'))
    p_web.SSV('save:ExchangAdjustment',p_web.GSV('jobe:ExchangeAdjustment'))
    p_web.SSV('save:LabourAdjustment',p_web.GSV('jobe:LabourAdjustment'))
    p_web.SSV('save:PartsAdjustment',p_web.GSV('jobe:PartsAdjustment'))
    p_web.SSV('save:WLabourPaid',p_web.GSV('jobe2:WLabourPaid'))
    p_web.SSV('save:WPartsPaid',p_web.GSV('jobe2:WPartsPaid'))
    p_web.SSV('save:WOtherCosts',p_web.GSV('jobe2:WOtherCosts'))
    p_web.SSV('save:WVat',p_web.GSV('jobe2:WVat'))
    p_web.SSV('save:WTotal',p_web.GSV('jobe2:WTotal'))
    p_web.SSV('save:IgnoreClaimCosts',p_web.GSV('jobe:IgnoreClaimCosts'))
    p_web.SSV('save:Ignore_Warranty_Charges',p_web.GSV('job:Ignore_Warranty_Charges'))
    p_web.SSV('save:Ignore_Chargeable_Charges',p_web.GSV('job:Ignore_Chargeable_Charges'))
    p_web.SSV('save:Ignore_Estimate_Charges',p_web.GSV('job:Ignore_Estimate_Charges'))
    p_web.SSV('save:RRCClabourCost',p_web.GSV('jobe:RRCCLabourCost'))
    p_web.SSV('save:RRCCPartsCost',p_web.GSV('jobe:RRCCPartsCost'))
    p_web.SSV('save:RRCCSubTotal',p_web.GSV('jobe:RRCCSubTotal'))
    p_web.SSV('save:RRCWLabourCost',p_web.GSV('jobe:RRCWLabourCost'))
    p_web.SSV('save:RRCWPartsCost',p_web.GSV('jobe:RRCWPartsCost'))
    p_web.SSV('save:RRCWSubTotal',p_web.GSV('jobe:RRCWSubTotal'))
    p_web.SSV('save:IgnoreRRCChaCosts',p_web.GSV('jobe:IgnoreRRCChaCosts'))
    p_web.SSV('save:IgnoreRRCWarCosts',p_web.GSV('jobe:IgnoreRRCWarCosts'))
    p_web.SSV('save:RRCELabourCost',p_web.GSV('jobe:RRCELabourCost'))
    p_web.SSV('save:RRCEPartsCost',p_web.GSV('jobe:RRCEPartsCost'))
    p_web.SSV('save:RRCESubTotal',p_web.GSV('jobe:RRCESubTotal'))
    p_web.SSV('save:InvRRCCLabourCost',p_web.GSV('jobe:InvRRCCLabourCost'))
    p_web.SSV('save:InvRRCCPartsCost',p_web.GSV('jobe:InvRRCCPartsCost'))
    p_web.SSV('save:InvRRCCSubTotal',p_web.GSV('jobe:InvRRCCSubTotal'))
    p_web.SSV('save:IgnoreRRCEstCosts',p_web.GSV('jobe:IgnoreRRCEstCosts'))
    p_web.SSV('save:InvRRCWLabourCost',p_web.GSV('jobe:InvRRCWLabourCost'))
    p_web.SSV('save:InvRRCWPartsCost',p_web.GSV('jobe:InvRRCWPartsCost'))
    p_web.SSV('save:InvRRCWSubTotal',p_web.GSV('jobe:InvRRCWSubTotal'))
    p_web.SSV('save:JobDiscountAmnt',p_web.GSV('jobe2:JobDiscountAmnt'))
    p_web.SSV('save:InvDiscountAmnt',p_web.GSV('jobe2:InvDiscountAmnt'))

updateARCCost      routine
    case (p_web.GSV('tmp:ARCViewCostType'))
    of 'Chargeable'
        p_web.SSV('job:Labour_Cost',p_web.GSV('tmp:ARCCost2'))
    of 'Warranty'
        p_web.SSV('job:Labour_Cost_Warranty',p_web.GSV('tmp:ARCCost2'))
    of 'Estimate'
        p_web.SSV('job:Labour_Cost_Estimate',p_web.GSV('tmp:ARCCost2'))
    end ! case (p_web.GSV('tmp:RRCViewCostType'))
updateRRCCost      routine
    case (p_web.GSV('tmp:RRCViewCostType'))
    of 'Chargeable'
        p_web.SSV('jobe:RRCCLabourCost',p_web.GSV('tmp:RRCCost2'))
        p_web.SSV('jobe2:JobDiscountAmnt',p_web.GSV('DefaultLabourCost') - p_web.GSV('jobe:RRCCLabourCost'))
        if (p_web.GSV('jobe2:JobDiscountAmnt') < 0)
            p_web.SSV('jobe2:JobDiscountAmnt',0)
        end

    of 'Warranty'
        p_web.SSV('jobe:RRCWLabourCost',p_web.GSV('tmp:RRCCost2'))
    of 'Estimate'
        p_web.SSV('jobe:RRCELabourCost',p_web.GSV('tmp:RRCCost2'))
    end ! case (p_web.GSV('tmp:RRCViewCostType'))

OpenFiles  ROUTINE
  p_web._OpenFile(JOBSE2)
  p_web._OpenFile(SUBCHRGE)
  p_web._OpenFile(TRACHRGE)
  p_web._OpenFile(STDCHRGE)
  p_web._OpenFile(USERS)
  p_web._OpenFile(JOBPAYMT)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(INVOICE)
  p_web._OpenFile(JOBSINV)
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(TRDPARTY)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(JOBSE2)
  p_Web._CloseFile(SUBCHRGE)
  p_Web._CloseFile(TRACHRGE)
  p_Web._CloseFile(STDCHRGE)
  p_Web._CloseFile(USERS)
  p_Web._CloseFile(JOBPAYMT)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(INVOICE)
  p_Web._CloseFile(JOBSINV)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(TRDPARTY)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE
  loc:EipClm = upper(p_web.GetValue('_EIPClm'))
  p_web.DeleteValue('_EIPClm')
  case loc:EipClm
  of ''
    case upper(p_web.GetValue('_calledfrom_'))
    of upper('BrowseJobCredits')
      do Value::BrowseJobCredits
    end
  end

InitForm       Routine
  DATA
LF  &FILE
  CODE
  ! Return URL
  IF (p_web.IfExistsValue('ViewCostsReturnURL'))
      p_web.StoreValue('ViewCostsReturnURL')
  ELSE
      p_web.SSV('ViewCostsReturnURL','ViewJob')
  END
  p_web.SSV('Prompt:RCost0','')
  p_web.SSV('Prompt:RCost1','')
  p_web.SSV('Prompt:RCost2','')
  p_web.SSV('Prompt:RCost3','')
  p_web.SSV('Prompt:RCost4','')
  p_web.SSV('Prompt:RCost5','')
  p_web.SSV('Prompt:RCost6','')
  p_web.SSV('Prompt:RCost7','')
  p_web.SSV('Prompt:RCost8','')
  p_web.SSV('Prompt:ACost1','')
  p_web.SSV('Prompt:ACost2','')
  p_web.SSV('Prompt:ACost3','')
  p_web.SSV('Prompt:ACost4','')
  p_web.SSV('Prompt:ACost5','')
  p_web.SSV('Prompt:ACost6','')
  p_web.SSV('Prompt:ACost7','')
  p_web.SSV('Prompt:ACost8','')
  p_web.SSV('tmp:ARCIgnoreReason','')
  p_web.SSV('tmp:RRCIgnoreReason','')
  p_web.SSV('ValidateIgnoreTickBoxARC',0)
  p_web.SSV('ValidateIgnoreTickBox',0)
  
  !Empty Audit Queue
  clear(tmpaud:Record)
  tmpaud:sessionID = p_web.SessionID
  set(tmpaud:keySessionID,tmpaud:keySessionID)
  loop
      next(tempAuditQueue)
      if (error())
          break
      end ! if (error())
      if (tmpaud:sessionID <> p_web.sessionID)
          break
      end ! if (tmpaud:sessionID <> p_web.sessionID)
      delete(tempAuditQueue)
  end
  
  p_web.SSV('JobPricingRoutine:ForceWarranty',0)
  JobPricingRoutine(p_web)
  
  do pricingRoutine
  
  p_web.SSV('Hide:ARCCosts',1)
  p_web.SSV('Hide:RRCCosts',1)
  
  p_web.SSV('Hide:ARCChargeable',1)
  p_web.SSV('Hide:ARCWarranty',1)
  p_web.SSV('Hide:ARCClaim',1)
  p_web.SSV('Hide:ARC3rdParty',1)
  p_web.SSV('Hide:ARCEstimate',1)
  p_web.SSV('Hide:ARCCInvoice',1)
  p_web.SSV('Hide:ARCWInvoice',1)
  p_web.SSV('Hide:ARCClaimInvoice',1)
  p_web.SSV('Hide:ManufacturerPaid',1)
  p_web.SSV('Hide:RRCChargeable',1)
  p_web.SSV('Hide:RRCWarranty',1)
  p_web.SSV('Hide:RRCEstimate',1)
  p_web.SSV('Hide:RRCHandling',1)
  p_web.SSV('Hide:RRCExchange',1)
  p_web.SSV('Hide:RRCCInvoice',1)
  p_web.SSV('Hide:RRCWInvoice',1)
  p_web.SSV('Hide:Credit',1)
  
  p_web.SSV('ReadOnly:ARCCost1',1)
  p_web.SSV('ReadOnly:ARCCost2',1)
  p_web.SSV('ReadOnly:ARCCost3',1)
  p_web.SSV('ReadOnly:RRCCost1',1)
  p_web.SSV('ReadOnly:RRCCost2',1)
  p_web.SSV('ReadOnly:RRCCost3',1)
  
  
  sentToHub(p_web)
  
  if (p_web.GSV('jobe:WebJob') = 1)
      if (p_web.GSV('job:Chargeable_job') = 'YES')
          if (p_web.GSV('BookingSite') = 'RRC')
              p_web.SSV('Hide:RRCChargeable',0)
              if (p_web.GSV('job:Exchange_Unit_Number') > 0)
                  if (p_web.GSV('jobe:ExchangedAtRRC'))
                      p_web.SSV('Hide:RRCExchange',0)
                  else ! if (p_web.GSV('jobe:ExchangedAtRRC'))
                      p_web.SSV('Hide:RRCHandling',0)
                      p_web.SSV('Hide:ARCChargeable',0)
                  end ! if (p_web.GSV('jobe:ExchangedAtRRC'))
              else ! if (p_web.GSV('job:Exchange_Unit_Number') > 0)
                  sentToHub(p_web)
                  if (p_web.GSV('SentToHub') = 1)
                      p_web.SSV('Hide:ARCChargeable',0)
                      p_web.SSV('Hide:RRCHandling',0)
                  end ! if (p_web.GSV('SentToHub') = 1)
              end ! if (p_web.GSV('job:Exchange_Unit_Number') > 0)
          else ! if (p_web.GSV('BookingSite') = 'RRC')
  
              p_web.SSV('Hide:RRCChargeable',0)
              if (p_web.GSV('job:Exchange_Unit_Number') > 0)
                  if (p_web.GSV('jobe:ExchangedAtRRC'))
                      p_web.SSV('Hide:RRCExchange',0)
                  else ! if (p_web.GSV('jobe:ExchangedAtRRC'))
                      p_web.SSV('Hide:RRCHandling',0)
                  end ! if (p_web.GSV('jobe:ExchangedAtRRC'))
              else ! if (p_web.GSV('job:Exchange_Unit_Number') > 0)
                  if (p_web.GSV('SentToHub') = 1)
                      p_web.SSV('Hide:RRCHandling',0)
                  end ! if (p_web.GSV('SentToHub') = 1)
              end ! if (p_web.GSV('job:Exchange_Unit_Number') > 0)
  
              if (p_web.GSV('SentToHub') = 1)
                  p_web.SSV('Hide:ARCChargeable',0)
              end ! if (p_web.GSV('SentToHub') = 1)
          end ! if (p_web.GSV('BookingSite') = 'RRC')
      end ! if (p_web.GSV('job:Chargeable_job') = 'YES')
  
      if (p_web.GSV('job:Warranty_job') = 'YES')
          if (p_web.GSV('BookingSite') = 'RRC')
              if (p_web.GSV('job:Exchange_Unit_Number') > 0)
                  if (p_web.GSV('jobe:ExchangedAtRRC'))
                      p_web.SSV('Hide:RRCExchange',0)
                  else ! if (p_web.GSV('jobe:ExchangedAtRRC'))
                      p_web.SSV('Hide:RRCHandling',0)
                  end ! if (p_web.GSV('jobe:ExchangedAtRRC'))
              else ! if (p_web.GSV('job:Exchange_Unit_Number') > 0)
                  if (p_web.GSV('SentToHub') = 1)
                      p_web.SSV('Hide:RRCHandling',0)
                  else
                      p_web.SSV('Hide:RRCWarranty',0)
                  end ! if (p_web.GSV('SentToHub') = 1)
              end ! if (p_web.GSV('job:Exchange_Unit_Number') > 0)
          else ! if (p_web.GSV('BookingSite') = 'RRC'           )
              p_web.SSV('Hide:ARCClaim',0)
              if (p_web.GSV('job:Exchange_Unit_Number') > 0)
                  if (p_web.GSV('jobe:ExchangedAtRRC'))
                      p_web.SSV('Hide:RRCExchange',0)
                      p_web.SSV('Hide:ARCWarranty',0)
                  else ! if (p_web.GSV('jobe:ExchangedAtRRC'))
                      p_web.SSV('Hide:RRCHandling',0)
                      p_web.SSV('Hide:ARCWarranty',0)
                  end ! if (p_web.GSV('jobe:ExchangedAtRRC'))
              else ! if (p_web.GSV('job:Exchange_Unit_Number') > 0)
                  if (p_web.GSV('SentToHub') = 1)
                      p_web.SSV('Hide:RRCHandling',0)
                      p_web.SSV('Hide:ARCWarranty',0)
                  else
                      p_web.SSV('Hide:RRCWarranty',0)
                  end ! if (p_web.GSV('SentToHub') = 1)
              end ! if (p_web.GSV('job:Exchange_Unit_Number') > 0)
          end !
      end  !if (p_web.GSV('job:Warranty_job') <> 'YES')
  else ! if (p_web.GSV('jobe:WebJob'))
      if (p_web.GSV('BookingSite') <> 'RRC')
          if (p_web.GSV('job:Chargeable_Job') = 'YES')
              p_web.SSV('Hide:ARCChargeable',0)
          end ! if (p_web.GSV('job:Chargeable_Job') = 'YES')
          if (p_web.GSV('job:Warranty_job') = 'YES')
              p_web.SSV('Hide:ARCWarranty',0)
              p_web.SSV('Hide:ARCClaim',0)
          end ! if (p_web.GSV('job:Warranty_job') = 'YES')
      end ! if (p_web.GSV('BookingSite') <> 'RRC')
  end ! if (p_web.GSV('jobe:WebJob'))
  
  
  Access:INVOICE.Clearkey(inv:invoice_Number_Key)
  inv:Invoice_Number    = p_web.GSV('job:Invoice_Number')
  if (Access:INVOICE.TryFetch(inv:invoice_Number_Key) = Level:Benign)
      ! Found
  else ! if (Access:INVOICE.TryFetch(inv:invoice_Number_Key) = Level:Benign)
      ! Error
  end ! if (Access:INVOICE.TryFetch(inv:invoice_Number_Key) = Level:Benign)
  
  if (p_web.GSV('job:Invoice_Number') > 0)
      if (p_web.GSV('Hide:ARCChargeable') = 0 and inv:ARCInvoiceDate > 0)
          p_web.SSV('Hide:ARCChargeable',1)
          p_web.SSV('Hide:ARCCInvoice',0)
      end ! if (p_web.GSV('Hide:ARCChargeable') = 0)
  
      if (p_web.GSV('Hide:RRCChargeable') = 0 and inv:ExportedRRCOracle)
          p_web.SSV('Hide:RRCChargeable',1)
          p_web.SSV('Hide:RRCCInvoice',0)
      end ! if (p_web.GSV('Hide:RRCChargeable') = 0 and inv:ExportedRRCOracle)
  end ! if (p_web.GSV('job:Invoice_Number') > 0)
  
  if (p_web.GSV('Hide:RRCWarranty') = 0 and p_web.GSV('wob:RRCWInvoiceNumber') > 0)
      p_web.SSV('Hide:RRCWarranty',1)
      p_web.SSV('Hide:RRCWInvoice',0)
  end ! if (p_web.GSV('Hide:RRCWarranty',0))
  
  if (p_web.GSV('job:Invoice_Number_Waranty') > 0)
      if (p_web.GSV('Hide:ARCWarranty') = 0)
          p_web.SSV('Hide:ARCWarranty',1)
          p_web.SSV('Hide:ARCWInvoice',0)
      end ! if (p_web.GSV('Hide:RRCWarranty',0))
  
      if (p_web.GSV('Hide:ARCClaim') = 0)
          p_web.SSV('Hide:ARCClaim',1)
          p_web.SSV('Hide:ARCClaimInvoice',0)
          p_web.SSV('Hide:ManufacturerPaid',0)
      end ! if (p_web.GSV('Hide:ARCClaim',0))
  end ! if (p_web.GSV('job:Invoice_Number_Waranty') > 0)
  
  if (p_web.GSV('Hide:RRCHandling') = 0)
      if ((p_web.GSV('job:Chargeable_Job') = 'YES' And |
          ExcludeHandlingFee('C',p_web.GSV('job:Manufacturer'),p_web.GSV('job:Repair_Type'))) Or |
          (p_web.GSV('job:Warranty_job') = 'YES' And |
          ExcludeHandlingFee('W',p_web.GSV('job:Manufacturer'),p_web.GSV('job:Repair_Type_Warranty'))))
  
          p_web.SSV('Hide:RRCHandling',1)
      end
  end ! if (p_web.GSV('Hide:RRCHandling',0))
  
  if (p_web.GSV('job:Chargeable_Job') = 'YES')
      if (p_web.GSV('job:Estimate') = 'YES')
          if (p_web.GSV('job:Estimate_Accepted') <> 'YES' And |
              p_web.GSV('job:Estimate_Rejected'))
              p_web.SSV('Hide:RRCChargeable',1)
              p_web.SSV('Hide:ARCChargeable',1)
          end
  
          if (p_web.GSV('jobe:WebJob') = 1)
              p_web.SSV('Hide:RRCEstimate',0)
              if (p_web.GSV('SentToHub') = 1)
                  p_web.SSV('Hide:ARCEstimate',0)
              end ! if (p_web.GSV('SentToHub') = 1)
          else ! if (p_web.GSV('jobe:WebJob'))
              p_web.SSV('Hide:ARCEstimate',0)
          end ! if (p_web.GSV('jobe:WebJob'))
  
      end ! if (p_web.GSV('job:Estimate') = 'YES')
  
  end ! if (p_web.GSV('job:Chargeable_Job') = 'YES')
  
  if (p_web.GSV('BookingSite') <> 'RRC')
      Access:JOBTHIRD.Clearkey(jot:RefNumberKey)
      jot:RefNumber    = p_web.GSV('job:ref_Number')
      if (Access:JOBTHIRD.TryFetch(jot:RefNumberKey) = Level:Benign)
          ! Found
          p_web.SSV('Hide:ARC3rdParty',0)
      else ! if (Access:JOBTHIRD.TryFetch(jot:RefNumberKey) = Level:Benign)
          ! Error
      end ! if (Access:JOBTHIRD.TryFetch(jot:RefNumberKey) = Level:Benign)
  end ! if (p_web.GSV('BookingSite') <> 'RRC')
  
  if (p_web.GSV('Hide:RRCHandling') = 0)
      if (p_web.GSV('job:Chargeable_Job') = 'YES')
  
          Access:REPTYDEF.Clearkey(rtd:ChaManRepairTypeKey)
          rtd:Manufacturer    = p_web.GSV('job:Manufacturer')
          rtd:Chargeable    = 'YES'
          rtd:Repair_Type    = p_web.GSV('job:repair_Type')
          if (Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign)
              ! Found
              if (rtd:BER = 4)
                  p_web.SSV('Hide:RRCHandling',1)
              end ! if (rtd:BER = 4)
          else ! if (Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign)
              ! Error
          end ! if (Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign)
      end ! if (p_web.GSV('job:Chargeable_Job') = 'YES')
  
      if (p_web.GSV('job:Warranty_Job') = 'YES')
  
          Access:REPTYDEF.Clearkey(rtd:WarManRepairTypeKey)
          rtd:Manufacturer    = p_web.GSV('job:Manufacturer')
          rtd:warranty    = 'YES'
          rtd:Repair_Type    = p_web.GSV('job:repair_Type_warranty')
          if (Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign)
              ! Found
              if (rtd:BER = 4)
                  p_web.SSV('Hide:RRCHandling',1)
              end ! if (rtd:BER = 4)
          else ! if (Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign)
              ! Error
          end ! if (Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign)
      end ! if (p_web.GSV('job:Chargeable_Job') = 'YES')
  end ! if (p_web.GSV('Hide:RRCHandling') = 0)
  
  if (p_web.GSV('Hide:RRCCInvoice') = 0)
  
      Access:JOBSINV.Clearkey(jov:typeRecordKey)
      jov:refNumber    = p_web.GSV('job:Ref_Number')
      jov:type    = 'C'
      set(jov:typeRecordKey,jov:typeRecordKey)
      loop
          if (Access:JOBSINV.Next())
              Break
          end ! if (Access:JOBSINV.Next())
          if (jov:refNumber    <> p_web.GSV('job:Ref_Number'))
              Break
          end ! if (jov:refNumber    <> p_web.GSV('job:Ref_Number'))
          if (jov:type    <> 'C')
              Break
          end ! if (jov:type    <> 'C')
          p_web.SSV('Hide:Credit',0)
          break
      end ! loop
  end ! if (p_web.GSV('Hide:RRCCInvoice') = 0)
  
  
  if (p_web.GSV('Hide:ARCChargeable') = 0 or |
      p_web.GSV('Hide:ARCWarranty') = 0 or |
      p_web.GSV('Hide:ARCClaim') = 0 or |
      p_web.GSV('Hide:ARC3rdParty') = 0 or |
      p_web.GSV('Hide:ARCEstimate') = 0 or |
      p_web.GSV('Hide:ARCCInvoice') = 0 or |
      p_web.GSV('Hide:ARCWInvoice') = 0 or |
      p_web.GSV('Hide:ARCClaimInvoice') = 0 or |
      p_web.GSV('Hide:ManufacturerPaid') = 0)
      p_web.SSV('Hide:ARCCosts',0)
  end
  
  if (p_web.GSV('Hide:RRCChargeable') = 0 or |
      p_web.GSV('Hide:RRCWarranty') = 0 or |
      p_web.GSV('Hide:RRCEstimate') = 0 or |
      p_web.GSV('Hide:RRCHandling') = 0 or |
      p_web.GSV('Hide:RRCExchange') = 0 or |
      p_web.GSV('Hide:RRCCInvoice') = 0 or |
      p_web.GSV('Hide:RRCWInvoice') = 0 or |
      p_web.GSV('Hide:Credit') = 0)
      p_web.SSV('Hide:RRCCosts',0)
  end
  
  if (p_web.GSV('Hide:RRCChargeable') = 0)
      p_web.SSV('tmp:RRCViewCostType','Chargeable')
  elsif (p_web.GSV('Hide:RRCWarranty') = 0)
      p_web.SSV('tmp:RRCViewCostType','Warranty')
  elsif (p_web.GSV('Hide:RRCEstimate') = 0)
      p_web.SSV('tmp:RRCViewCostType','Estimate')
  elsif (p_web.GSV('Hide:RRCHandling') = 0)
      p_web.SSV('tmp:RRCViewCostType','Handling')
  elsif (p_web.GSV('Hide:RRCExchange') = 0)
      p_web.SSV('tmp:RRCViewCostType','Exchange')
  elsif (p_web.GSV('Hide:RRCCInvoice') = 0)
      p_web.SSV('tmp:RRCViewCostType','Chargeable - Invoiced')
  elsif (p_web.GSV('Hide:RRCWInvoice') = 0)
      p_web.SSV('tmp:RRCViewCostType','Warranty - Invoiced')
  elsif (p_web.GSV('Hide:Credit') = 0)
      p_web.SSV('tmp:RRCViewCostType','Credits')
  else
      p_web.SSV('tmp:RRCVIewCostType','')
  end !if (p_web.GSV('Hide:RRCChargeable') = 0)
  
  if (p_web.GSV('Hide:ARCChargeable') = 0)
      p_web.SSV('tmp:ARCViewCostType','Chargeable')
  elsif (p_web.GSV('Hide:ARCWarranty') = 0)
      p_web.SSV('tmp:ARCViewCostType','Warranty')
  elsif (p_web.GSV('Hide:ARCClaim') = 0)
      p_web.SSV('tmp:ARCViewCostType','Claim')
  elsif (p_web.GSV('Hide:ARC3rdParty') = 0)
      p_web.SSV('tmp:ARCViewCostType','3rd Party')
  elsif (p_web.GSV('Hide:ARCEstimate') = 0)
      p_web.SSV('tmp:ARCViewCostType','Estimate')
  elsif (p_web.GSV('Hide:ARCCInvoice') = 0)
      p_web.SSV('tmp:ARCViewCostType','Chargeble - Invoiced')
  elsif (p_web.GSV('Hide:ARCWInvoice') = 0)
      p_web.SSV('tmp:ARCViewCostType','Warranty - Invoiced')
  elsif (p_web.GSV('Hide:ARCClaimInvoice') = 0)
      p_web.SSV('tmp:ARCViewCostType','Claim - Invoiced')
  elsif (p_web.GSV('Hide:ManufacturerPaid') = 0)
      p_web.SSV('tmp:ARCViewCostType','Manufacturer Payment')
  else
      p_web.SSV('tmp:ARCVIewCostType','')
  end !if (p_web.GSV('Hide:RRCChargeable') = 0)
  
  
  do displayARCCostFields
  do displayRRCCostFields
  do displayOtherFields
  
  do saveFields
  
  do DefaultLabourCost
  
  LoanAttachedToJob(p_web) ! Has a loan unit been, or is, attached?
  p_web.SetValue('ViewCosts_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'ViewCosts'
    end
    p_web.formsettings.proc = 'ViewCosts'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine
  do restoreFields
  Do DeleteSessionValues

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('tmp:ARCCost1')
    p_web.SetPicture('tmp:ARCCost1','@n14.2')
  End
  p_web.SetSessionPicture('tmp:ARCCost1','@n14.2')
  If p_web.IfExistsValue('tmp:AdjustmentCost1')
    p_web.SetPicture('tmp:AdjustmentCost1','@n14.2')
  End
  p_web.SetSessionPicture('tmp:AdjustmentCost1','@n14.2')
  If p_web.IfExistsValue('tmp:ARCCost2')
    p_web.SetPicture('tmp:ARCCost2','@n14.2')
  End
  p_web.SetSessionPicture('tmp:ARCCost2','@n14.2')
  If p_web.IfExistsValue('tmp:AdjustmentCost2')
    p_web.SetPicture('tmp:AdjustmentCost2','@n14.2')
  End
  p_web.SetSessionPicture('tmp:AdjustmentCost2','@n14.2')
  If p_web.IfExistsValue('tmp:ARCCost3')
    p_web.SetPicture('tmp:ARCCost3','@n14.2')
  End
  p_web.SetSessionPicture('tmp:ARCCost3','@n14.2')
  If p_web.IfExistsValue('tmp:AdjustmentCost3')
    p_web.SetPicture('tmp:AdjustmentCost3','@n14.2')
  End
  p_web.SetSessionPicture('tmp:AdjustmentCost3','@n14.2')
  If p_web.IfExistsValue('tmp:ARCCost4')
    p_web.SetPicture('tmp:ARCCost4','@n14.2')
  End
  p_web.SetSessionPicture('tmp:ARCCost4','@n14.2')
  If p_web.IfExistsValue('tmp:AdjustmentCost4')
    p_web.SetPicture('tmp:AdjustmentCost4','@n14.2')
  End
  p_web.SetSessionPicture('tmp:AdjustmentCost4','@n14.2')
  If p_web.IfExistsValue('tmp:ARCCost5')
    p_web.SetPicture('tmp:ARCCost5','@n14.2')
  End
  p_web.SetSessionPicture('tmp:ARCCost5','@n14.2')
  If p_web.IfExistsValue('tmp:AdjustmentCost5')
    p_web.SetPicture('tmp:AdjustmentCost5','@n14.2')
  End
  p_web.SetSessionPicture('tmp:AdjustmentCost5','@n14.2')
  If p_web.IfExistsValue('tmp:ARCCost6')
    p_web.SetPicture('tmp:ARCCost6','@n14.2')
  End
  p_web.SetSessionPicture('tmp:ARCCost6','@n14.2')
  If p_web.IfExistsValue('tmp:AdjustmentCost6')
    p_web.SetPicture('tmp:AdjustmentCost6','@n14.2')
  End
  p_web.SetSessionPicture('tmp:AdjustmentCost6','@n14.2')
  If p_web.IfExistsValue('tmp:ARCCost7')
    p_web.SetPicture('tmp:ARCCost7','@n14.2')
  End
  p_web.SetSessionPicture('tmp:ARCCost7','@n14.2')
  If p_web.IfExistsValue('tmp:ARCCost8')
    p_web.SetPicture('tmp:ARCCost8','@n14.2')
  End
  p_web.SetSessionPicture('tmp:ARCCost8','@n14.2')
  If p_web.IfExistsValue('jobe:ARC3rdPartyInvoiceNumber')
    p_web.SetPicture('jobe:ARC3rdPartyInvoiceNumber','@s30')
  End
  p_web.SetSessionPicture('jobe:ARC3rdPartyInvoiceNumber','@s30')
  If p_web.IfExistsValue('tmp:RRCCost0')
    p_web.SetPicture('tmp:RRCCost0','@n14.2')
  End
  p_web.SetSessionPicture('tmp:RRCCost0','@n14.2')
  If p_web.IfExistsValue('tmp:RRCCost1')
    p_web.SetPicture('tmp:RRCCost1','@n14.2')
  End
  p_web.SetSessionPicture('tmp:RRCCost1','@n14.2')
  If p_web.IfExistsValue('tmp:RRCCost2')
    p_web.SetPicture('tmp:RRCCost2','@n14.2')
  End
  p_web.SetSessionPicture('tmp:RRCCost2','@n14.2')
  If p_web.IfExistsValue('tmp:RRCCost3')
    p_web.SetPicture('tmp:RRCCost3','@n14.2')
  End
  p_web.SetSessionPicture('tmp:RRCCost3','@n14.2')
  If p_web.IfExistsValue('tmp:RRCCost4')
    p_web.SetPicture('tmp:RRCCost4','@n14.2')
  End
  p_web.SetSessionPicture('tmp:RRCCost4','@n14.2')
  If p_web.IfExistsValue('tmp:RRCCost5')
    p_web.SetPicture('tmp:RRCCost5','@n14.2')
  End
  p_web.SetSessionPicture('tmp:RRCCost5','@n14.2')
  If p_web.IfExistsValue('tmp:RRCCost6')
    p_web.SetPicture('tmp:RRCCost6','@n14.2')
  End
  p_web.SetSessionPicture('tmp:RRCCost6','@n14.2')
  If p_web.IfExistsValue('tmp:RRCCost7')
    p_web.SetPicture('tmp:RRCCost7','@n14.2')
  End
  p_web.SetSessionPicture('tmp:RRCCost7','@n14.2')
  If p_web.IfExistsValue('tmp:RRCCost8')
    p_web.SetPicture('tmp:RRCCost8','@n14.2')
  End
  p_web.SetSessionPicture('tmp:RRCCost8','@n14.2')

AfterLookup Routine
  loc:TabNumber = -1
  If p_web.GSV('Hide:ARCCosts') <> 1
    loc:TabNumber += 1
  End
  If p_web.GSV('Hide:RRCCosts') <> 1
    loc:TabNumber += 1
  End
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  if p_web.IfExistsValue('tmp:ARCViewCostType') = 0
    p_web.SetSessionValue('tmp:ARCViewCostType',tmp:ARCViewCostType)
  Else
    tmp:ARCViewCostType = p_web.GetSessionValue('tmp:ARCViewCostType')
  End
  if p_web.IfExistsValue('tmp:ARCIgnoreDefaultCharges') = 0
    p_web.SetSessionValue('tmp:ARCIgnoreDefaultCharges',tmp:ARCIgnoreDefaultCharges)
  Else
    tmp:ARCIgnoreDefaultCharges = p_web.GetSessionValue('tmp:ARCIgnoreDefaultCharges')
  End
  if p_web.IfExistsValue('tmp:ARCIgnoreReason') = 0
    p_web.SetSessionValue('tmp:ARCIgnoreReason',tmp:ARCIgnoreReason)
  Else
    tmp:ARCIgnoreReason = p_web.GetSessionValue('tmp:ARCIgnoreReason')
  End
  if p_web.IfExistsValue('tmp:ARCCost1') = 0
    p_web.SetSessionValue('tmp:ARCCost1',tmp:ARCCost1)
  Else
    tmp:ARCCost1 = p_web.GetSessionValue('tmp:ARCCost1')
  End
  if p_web.IfExistsValue('tmp:AdjustmentCost1') = 0
    p_web.SetSessionValue('tmp:AdjustmentCost1',tmp:AdjustmentCost1)
  Else
    tmp:AdjustmentCost1 = p_web.GetSessionValue('tmp:AdjustmentCost1')
  End
  if p_web.IfExistsValue('tmp:ARCCost2') = 0
    p_web.SetSessionValue('tmp:ARCCost2',tmp:ARCCost2)
  Else
    tmp:ARCCost2 = p_web.GetSessionValue('tmp:ARCCost2')
  End
  if p_web.IfExistsValue('tmp:AdjustmentCost2') = 0
    p_web.SetSessionValue('tmp:AdjustmentCost2',tmp:AdjustmentCost2)
  Else
    tmp:AdjustmentCost2 = p_web.GetSessionValue('tmp:AdjustmentCost2')
  End
  if p_web.IfExistsValue('tmp:ARCCost3') = 0
    p_web.SetSessionValue('tmp:ARCCost3',tmp:ARCCost3)
  Else
    tmp:ARCCost3 = p_web.GetSessionValue('tmp:ARCCost3')
  End
  if p_web.IfExistsValue('tmp:AdjustmentCost3') = 0
    p_web.SetSessionValue('tmp:AdjustmentCost3',tmp:AdjustmentCost3)
  Else
    tmp:AdjustmentCost3 = p_web.GetSessionValue('tmp:AdjustmentCost3')
  End
  if p_web.IfExistsValue('tmp:ARCCost4') = 0
    p_web.SetSessionValue('tmp:ARCCost4',tmp:ARCCost4)
  Else
    tmp:ARCCost4 = p_web.GetSessionValue('tmp:ARCCost4')
  End
  if p_web.IfExistsValue('tmp:AdjustmentCost4') = 0
    p_web.SetSessionValue('tmp:AdjustmentCost4',tmp:AdjustmentCost4)
  Else
    tmp:AdjustmentCost4 = p_web.GetSessionValue('tmp:AdjustmentCost4')
  End
  if p_web.IfExistsValue('tmp:ARCCost5') = 0
    p_web.SetSessionValue('tmp:ARCCost5',tmp:ARCCost5)
  Else
    tmp:ARCCost5 = p_web.GetSessionValue('tmp:ARCCost5')
  End
  if p_web.IfExistsValue('tmp:AdjustmentCost5') = 0
    p_web.SetSessionValue('tmp:AdjustmentCost5',tmp:AdjustmentCost5)
  Else
    tmp:AdjustmentCost5 = p_web.GetSessionValue('tmp:AdjustmentCost5')
  End
  if p_web.IfExistsValue('tmp:ARCCost6') = 0
    p_web.SetSessionValue('tmp:ARCCost6',tmp:ARCCost6)
  Else
    tmp:ARCCost6 = p_web.GetSessionValue('tmp:ARCCost6')
  End
  if p_web.IfExistsValue('tmp:AdjustmentCost6') = 0
    p_web.SetSessionValue('tmp:AdjustmentCost6',tmp:AdjustmentCost6)
  Else
    tmp:AdjustmentCost6 = p_web.GetSessionValue('tmp:AdjustmentCost6')
  End
  if p_web.IfExistsValue('tmp:ARCCost7') = 0
    p_web.SetSessionValue('tmp:ARCCost7',tmp:ARCCost7)
  Else
    tmp:ARCCost7 = p_web.GetSessionValue('tmp:ARCCost7')
  End
  if p_web.IfExistsValue('tmp:ARCCost8') = 0
    p_web.SetSessionValue('tmp:ARCCost8',tmp:ARCCost8)
  Else
    tmp:ARCCost8 = p_web.GetSessionValue('tmp:ARCCost8')
  End
  if p_web.IfExistsValue('jobe:ARC3rdPartyInvoiceNumber') = 0
    p_web.SetSessionValue('jobe:ARC3rdPartyInvoiceNumber',jobe:ARC3rdPartyInvoiceNumber)
  Else
    jobe:ARC3rdPartyInvoiceNumber = p_web.GetSessionValue('jobe:ARC3rdPartyInvoiceNumber')
  End
  if p_web.IfExistsValue('tmp:ARCInvoiceNumber') = 0
    p_web.SetSessionValue('tmp:ARCInvoiceNumber',tmp:ARCInvoiceNumber)
  Else
    tmp:ARCInvoiceNumber = p_web.GetSessionValue('tmp:ARCInvoiceNumber')
  End
  if p_web.IfExistsValue('tmp:RRCViewCostType') = 0
    p_web.SetSessionValue('tmp:RRCViewCostType',tmp:RRCViewCostType)
  Else
    tmp:RRCViewCostType = p_web.GetSessionValue('tmp:RRCViewCostType')
  End
  if p_web.IfExistsValue('tmp:RRCIgnoreDefaultCharges') = 0
    p_web.SetSessionValue('tmp:RRCIgnoreDefaultCharges',tmp:RRCIgnoreDefaultCharges)
  Else
    tmp:RRCIgnoreDefaultCharges = p_web.GetSessionValue('tmp:RRCIgnoreDefaultCharges')
  End
  if p_web.IfExistsValue('tmp:RRCIgnoreReason') = 0
    p_web.SetSessionValue('tmp:RRCIgnoreReason',tmp:RRCIgnoreReason)
  Else
    tmp:RRCIgnoreReason = p_web.GetSessionValue('tmp:RRCIgnoreReason')
  End
  if p_web.IfExistsValue('tmp:RRCCost0') = 0
    p_web.SetSessionValue('tmp:RRCCost0',tmp:RRCCost0)
  Else
    tmp:RRCCost0 = p_web.GetSessionValue('tmp:RRCCost0')
  End
  if p_web.IfExistsValue('tmp:RRCCost1') = 0
    p_web.SetSessionValue('tmp:RRCCost1',tmp:RRCCost1)
  Else
    tmp:RRCCost1 = p_web.GetSessionValue('tmp:RRCCost1')
  End
  if p_web.IfExistsValue('tmp:originalInvoice') = 0
    p_web.SetSessionValue('tmp:originalInvoice',tmp:originalInvoice)
  Else
    tmp:originalInvoice = p_web.GetSessionValue('tmp:originalInvoice')
  End
  if p_web.IfExistsValue('tmp:RRCCost2') = 0
    p_web.SetSessionValue('tmp:RRCCost2',tmp:RRCCost2)
  Else
    tmp:RRCCost2 = p_web.GetSessionValue('tmp:RRCCost2')
  End
  if p_web.IfExistsValue('tmp:RRCCost3') = 0
    p_web.SetSessionValue('tmp:RRCCost3',tmp:RRCCost3)
  Else
    tmp:RRCCost3 = p_web.GetSessionValue('tmp:RRCCost3')
  End
  if p_web.IfExistsValue('tmp:RRCCost4') = 0
    p_web.SetSessionValue('tmp:RRCCost4',tmp:RRCCost4)
  Else
    tmp:RRCCost4 = p_web.GetSessionValue('tmp:RRCCost4')
  End
  if p_web.IfExistsValue('tmp:RRCCost5') = 0
    p_web.SetSessionValue('tmp:RRCCost5',tmp:RRCCost5)
  Else
    tmp:RRCCost5 = p_web.GetSessionValue('tmp:RRCCost5')
  End
  if p_web.IfExistsValue('tmp:RRCCost6') = 0
    p_web.SetSessionValue('tmp:RRCCost6',tmp:RRCCost6)
  Else
    tmp:RRCCost6 = p_web.GetSessionValue('tmp:RRCCost6')
  End
  if p_web.IfExistsValue('tmp:RRCCost7') = 0
    p_web.SetSessionValue('tmp:RRCCost7',tmp:RRCCost7)
  Else
    tmp:RRCCost7 = p_web.GetSessionValue('tmp:RRCCost7')
  End
  if p_web.IfExistsValue('tmp:RRCCost8') = 0
    p_web.SetSessionValue('tmp:RRCCost8',tmp:RRCCost8)
  Else
    tmp:RRCCost8 = p_web.GetSessionValue('tmp:RRCCost8')
  End
  if p_web.IfExistsValue('jobe:ExcReplcamentCharge') = 0
    p_web.SetSessionValue('jobe:ExcReplcamentCharge',jobe:ExcReplcamentCharge)
  Else
    jobe:ExcReplcamentCharge = p_web.GetSessionValue('jobe:ExcReplcamentCharge')
  End

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('tmp:ARCViewCostType')
    tmp:ARCViewCostType = p_web.GetValue('tmp:ARCViewCostType')
    p_web.SetSessionValue('tmp:ARCViewCostType',tmp:ARCViewCostType)
  Else
    tmp:ARCViewCostType = p_web.GetSessionValue('tmp:ARCViewCostType')
  End
  if p_web.IfExistsValue('tmp:ARCIgnoreDefaultCharges')
    tmp:ARCIgnoreDefaultCharges = p_web.GetValue('tmp:ARCIgnoreDefaultCharges')
    p_web.SetSessionValue('tmp:ARCIgnoreDefaultCharges',tmp:ARCIgnoreDefaultCharges)
  Else
    tmp:ARCIgnoreDefaultCharges = p_web.GetSessionValue('tmp:ARCIgnoreDefaultCharges')
  End
  if p_web.IfExistsValue('tmp:ARCIgnoreReason')
    tmp:ARCIgnoreReason = p_web.GetValue('tmp:ARCIgnoreReason')
    p_web.SetSessionValue('tmp:ARCIgnoreReason',tmp:ARCIgnoreReason)
  Else
    tmp:ARCIgnoreReason = p_web.GetSessionValue('tmp:ARCIgnoreReason')
  End
  if p_web.IfExistsValue('tmp:ARCCost1')
    tmp:ARCCost1 = p_web.dformat(clip(p_web.GetValue('tmp:ARCCost1')),'@n14.2')
    p_web.SetSessionValue('tmp:ARCCost1',tmp:ARCCost1)
  Else
    tmp:ARCCost1 = p_web.GetSessionValue('tmp:ARCCost1')
  End
  if p_web.IfExistsValue('tmp:AdjustmentCost1')
    tmp:AdjustmentCost1 = p_web.dformat(clip(p_web.GetValue('tmp:AdjustmentCost1')),'@n14.2')
    p_web.SetSessionValue('tmp:AdjustmentCost1',tmp:AdjustmentCost1)
  Else
    tmp:AdjustmentCost1 = p_web.GetSessionValue('tmp:AdjustmentCost1')
  End
  if p_web.IfExistsValue('tmp:ARCCost2')
    tmp:ARCCost2 = p_web.dformat(clip(p_web.GetValue('tmp:ARCCost2')),'@n14.2')
    p_web.SetSessionValue('tmp:ARCCost2',tmp:ARCCost2)
  Else
    tmp:ARCCost2 = p_web.GetSessionValue('tmp:ARCCost2')
  End
  if p_web.IfExistsValue('tmp:AdjustmentCost2')
    tmp:AdjustmentCost2 = p_web.dformat(clip(p_web.GetValue('tmp:AdjustmentCost2')),'@n14.2')
    p_web.SetSessionValue('tmp:AdjustmentCost2',tmp:AdjustmentCost2)
  Else
    tmp:AdjustmentCost2 = p_web.GetSessionValue('tmp:AdjustmentCost2')
  End
  if p_web.IfExistsValue('tmp:ARCCost3')
    tmp:ARCCost3 = p_web.dformat(clip(p_web.GetValue('tmp:ARCCost3')),'@n14.2')
    p_web.SetSessionValue('tmp:ARCCost3',tmp:ARCCost3)
  Else
    tmp:ARCCost3 = p_web.GetSessionValue('tmp:ARCCost3')
  End
  if p_web.IfExistsValue('tmp:AdjustmentCost3')
    tmp:AdjustmentCost3 = p_web.dformat(clip(p_web.GetValue('tmp:AdjustmentCost3')),'@n14.2')
    p_web.SetSessionValue('tmp:AdjustmentCost3',tmp:AdjustmentCost3)
  Else
    tmp:AdjustmentCost3 = p_web.GetSessionValue('tmp:AdjustmentCost3')
  End
  if p_web.IfExistsValue('tmp:ARCCost4')
    tmp:ARCCost4 = p_web.dformat(clip(p_web.GetValue('tmp:ARCCost4')),'@n14.2')
    p_web.SetSessionValue('tmp:ARCCost4',tmp:ARCCost4)
  Else
    tmp:ARCCost4 = p_web.GetSessionValue('tmp:ARCCost4')
  End
  if p_web.IfExistsValue('tmp:AdjustmentCost4')
    tmp:AdjustmentCost4 = p_web.dformat(clip(p_web.GetValue('tmp:AdjustmentCost4')),'@n14.2')
    p_web.SetSessionValue('tmp:AdjustmentCost4',tmp:AdjustmentCost4)
  Else
    tmp:AdjustmentCost4 = p_web.GetSessionValue('tmp:AdjustmentCost4')
  End
  if p_web.IfExistsValue('tmp:ARCCost5')
    tmp:ARCCost5 = p_web.dformat(clip(p_web.GetValue('tmp:ARCCost5')),'@n14.2')
    p_web.SetSessionValue('tmp:ARCCost5',tmp:ARCCost5)
  Else
    tmp:ARCCost5 = p_web.GetSessionValue('tmp:ARCCost5')
  End
  if p_web.IfExistsValue('tmp:AdjustmentCost5')
    tmp:AdjustmentCost5 = p_web.dformat(clip(p_web.GetValue('tmp:AdjustmentCost5')),'@n14.2')
    p_web.SetSessionValue('tmp:AdjustmentCost5',tmp:AdjustmentCost5)
  Else
    tmp:AdjustmentCost5 = p_web.GetSessionValue('tmp:AdjustmentCost5')
  End
  if p_web.IfExistsValue('tmp:ARCCost6')
    tmp:ARCCost6 = p_web.dformat(clip(p_web.GetValue('tmp:ARCCost6')),'@n14.2')
    p_web.SetSessionValue('tmp:ARCCost6',tmp:ARCCost6)
  Else
    tmp:ARCCost6 = p_web.GetSessionValue('tmp:ARCCost6')
  End
  if p_web.IfExistsValue('tmp:AdjustmentCost6')
    tmp:AdjustmentCost6 = p_web.dformat(clip(p_web.GetValue('tmp:AdjustmentCost6')),'@n14.2')
    p_web.SetSessionValue('tmp:AdjustmentCost6',tmp:AdjustmentCost6)
  Else
    tmp:AdjustmentCost6 = p_web.GetSessionValue('tmp:AdjustmentCost6')
  End
  if p_web.IfExistsValue('tmp:ARCCost7')
    tmp:ARCCost7 = p_web.dformat(clip(p_web.GetValue('tmp:ARCCost7')),'@n14.2')
    p_web.SetSessionValue('tmp:ARCCost7',tmp:ARCCost7)
  Else
    tmp:ARCCost7 = p_web.GetSessionValue('tmp:ARCCost7')
  End
  if p_web.IfExistsValue('tmp:ARCCost8')
    tmp:ARCCost8 = p_web.dformat(clip(p_web.GetValue('tmp:ARCCost8')),'@n14.2')
    p_web.SetSessionValue('tmp:ARCCost8',tmp:ARCCost8)
  Else
    tmp:ARCCost8 = p_web.GetSessionValue('tmp:ARCCost8')
  End
  if p_web.IfExistsValue('jobe:ARC3rdPartyInvoiceNumber')
    jobe:ARC3rdPartyInvoiceNumber = p_web.GetValue('jobe:ARC3rdPartyInvoiceNumber')
    p_web.SetSessionValue('jobe:ARC3rdPartyInvoiceNumber',jobe:ARC3rdPartyInvoiceNumber)
  Else
    jobe:ARC3rdPartyInvoiceNumber = p_web.GetSessionValue('jobe:ARC3rdPartyInvoiceNumber')
  End
  if p_web.IfExistsValue('tmp:ARCInvoiceNumber')
    tmp:ARCInvoiceNumber = p_web.GetValue('tmp:ARCInvoiceNumber')
    p_web.SetSessionValue('tmp:ARCInvoiceNumber',tmp:ARCInvoiceNumber)
  Else
    tmp:ARCInvoiceNumber = p_web.GetSessionValue('tmp:ARCInvoiceNumber')
  End
  if p_web.IfExistsValue('tmp:RRCViewCostType')
    tmp:RRCViewCostType = p_web.GetValue('tmp:RRCViewCostType')
    p_web.SetSessionValue('tmp:RRCViewCostType',tmp:RRCViewCostType)
  Else
    tmp:RRCViewCostType = p_web.GetSessionValue('tmp:RRCViewCostType')
  End
  if p_web.IfExistsValue('tmp:RRCIgnoreDefaultCharges')
    tmp:RRCIgnoreDefaultCharges = p_web.GetValue('tmp:RRCIgnoreDefaultCharges')
    p_web.SetSessionValue('tmp:RRCIgnoreDefaultCharges',tmp:RRCIgnoreDefaultCharges)
  Else
    tmp:RRCIgnoreDefaultCharges = p_web.GetSessionValue('tmp:RRCIgnoreDefaultCharges')
  End
  if p_web.IfExistsValue('tmp:RRCIgnoreReason')
    tmp:RRCIgnoreReason = p_web.GetValue('tmp:RRCIgnoreReason')
    p_web.SetSessionValue('tmp:RRCIgnoreReason',tmp:RRCIgnoreReason)
  Else
    tmp:RRCIgnoreReason = p_web.GetSessionValue('tmp:RRCIgnoreReason')
  End
  if p_web.IfExistsValue('tmp:RRCCost0')
    tmp:RRCCost0 = p_web.dformat(clip(p_web.GetValue('tmp:RRCCost0')),'@n14.2')
    p_web.SetSessionValue('tmp:RRCCost0',tmp:RRCCost0)
  Else
    tmp:RRCCost0 = p_web.GetSessionValue('tmp:RRCCost0')
  End
  if p_web.IfExistsValue('tmp:RRCCost1')
    tmp:RRCCost1 = p_web.dformat(clip(p_web.GetValue('tmp:RRCCost1')),'@n14.2')
    p_web.SetSessionValue('tmp:RRCCost1',tmp:RRCCost1)
  Else
    tmp:RRCCost1 = p_web.GetSessionValue('tmp:RRCCost1')
  End
  if p_web.IfExistsValue('tmp:originalInvoice')
    tmp:originalInvoice = p_web.GetValue('tmp:originalInvoice')
    p_web.SetSessionValue('tmp:originalInvoice',tmp:originalInvoice)
  Else
    tmp:originalInvoice = p_web.GetSessionValue('tmp:originalInvoice')
  End
  if p_web.IfExistsValue('tmp:RRCCost2')
    tmp:RRCCost2 = p_web.dformat(clip(p_web.GetValue('tmp:RRCCost2')),'@n14.2')
    p_web.SetSessionValue('tmp:RRCCost2',tmp:RRCCost2)
  Else
    tmp:RRCCost2 = p_web.GetSessionValue('tmp:RRCCost2')
  End
  if p_web.IfExistsValue('tmp:RRCCost3')
    tmp:RRCCost3 = p_web.dformat(clip(p_web.GetValue('tmp:RRCCost3')),'@n14.2')
    p_web.SetSessionValue('tmp:RRCCost3',tmp:RRCCost3)
  Else
    tmp:RRCCost3 = p_web.GetSessionValue('tmp:RRCCost3')
  End
  if p_web.IfExistsValue('tmp:RRCCost4')
    tmp:RRCCost4 = p_web.dformat(clip(p_web.GetValue('tmp:RRCCost4')),'@n14.2')
    p_web.SetSessionValue('tmp:RRCCost4',tmp:RRCCost4)
  Else
    tmp:RRCCost4 = p_web.GetSessionValue('tmp:RRCCost4')
  End
  if p_web.IfExistsValue('tmp:RRCCost5')
    tmp:RRCCost5 = p_web.dformat(clip(p_web.GetValue('tmp:RRCCost5')),'@n14.2')
    p_web.SetSessionValue('tmp:RRCCost5',tmp:RRCCost5)
  Else
    tmp:RRCCost5 = p_web.GetSessionValue('tmp:RRCCost5')
  End
  if p_web.IfExistsValue('tmp:RRCCost6')
    tmp:RRCCost6 = p_web.dformat(clip(p_web.GetValue('tmp:RRCCost6')),'@n14.2')
    p_web.SetSessionValue('tmp:RRCCost6',tmp:RRCCost6)
  Else
    tmp:RRCCost6 = p_web.GetSessionValue('tmp:RRCCost6')
  End
  if p_web.IfExistsValue('tmp:RRCCost7')
    tmp:RRCCost7 = p_web.dformat(clip(p_web.GetValue('tmp:RRCCost7')),'@n14.2')
    p_web.SetSessionValue('tmp:RRCCost7',tmp:RRCCost7)
  Else
    tmp:RRCCost7 = p_web.GetSessionValue('tmp:RRCCost7')
  End
  if p_web.IfExistsValue('tmp:RRCCost8')
    tmp:RRCCost8 = p_web.dformat(clip(p_web.GetValue('tmp:RRCCost8')),'@n14.2')
    p_web.SetSessionValue('tmp:RRCCost8',tmp:RRCCost8)
  Else
    tmp:RRCCost8 = p_web.GetSessionValue('tmp:RRCCost8')
  End
  if p_web.IfExistsValue('jobe:ExcReplcamentCharge')
    jobe:ExcReplcamentCharge = p_web.GetValue('jobe:ExcReplcamentCharge')
    p_web.SetSessionValue('jobe:ExcReplcamentCharge',jobe:ExcReplcamentCharge)
  Else
    jobe:ExcReplcamentCharge = p_web.GetSessionValue('jobe:ExcReplcamentCharge')
  End

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('ViewCosts_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.GSV('ViewCostsReturnURL')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('ViewCosts_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('ViewCosts_ChainTo')
    loc:formaction = p_web.GetSessionValue('ViewCosts_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = p_web.GSV('ViewCostsReturnURL')

GenerateForm   Routine
  do LoadRelatedRecords
 tmp:ARCViewCostType = p_web.RestoreValue('tmp:ARCViewCostType')
 tmp:ARCIgnoreDefaultCharges = p_web.RestoreValue('tmp:ARCIgnoreDefaultCharges')
 tmp:ARCIgnoreReason = p_web.RestoreValue('tmp:ARCIgnoreReason')
 tmp:ARCCost1 = p_web.RestoreValue('tmp:ARCCost1')
 tmp:AdjustmentCost1 = p_web.RestoreValue('tmp:AdjustmentCost1')
 tmp:ARCCost2 = p_web.RestoreValue('tmp:ARCCost2')
 tmp:AdjustmentCost2 = p_web.RestoreValue('tmp:AdjustmentCost2')
 tmp:ARCCost3 = p_web.RestoreValue('tmp:ARCCost3')
 tmp:AdjustmentCost3 = p_web.RestoreValue('tmp:AdjustmentCost3')
 tmp:ARCCost4 = p_web.RestoreValue('tmp:ARCCost4')
 tmp:AdjustmentCost4 = p_web.RestoreValue('tmp:AdjustmentCost4')
 tmp:ARCCost5 = p_web.RestoreValue('tmp:ARCCost5')
 tmp:AdjustmentCost5 = p_web.RestoreValue('tmp:AdjustmentCost5')
 tmp:ARCCost6 = p_web.RestoreValue('tmp:ARCCost6')
 tmp:AdjustmentCost6 = p_web.RestoreValue('tmp:AdjustmentCost6')
 tmp:ARCCost7 = p_web.RestoreValue('tmp:ARCCost7')
 tmp:ARCCost8 = p_web.RestoreValue('tmp:ARCCost8')
 tmp:ARCInvoiceNumber = p_web.RestoreValue('tmp:ARCInvoiceNumber')
 tmp:RRCViewCostType = p_web.RestoreValue('tmp:RRCViewCostType')
 tmp:RRCIgnoreDefaultCharges = p_web.RestoreValue('tmp:RRCIgnoreDefaultCharges')
 tmp:RRCIgnoreReason = p_web.RestoreValue('tmp:RRCIgnoreReason')
 tmp:RRCCost0 = p_web.RestoreValue('tmp:RRCCost0')
 tmp:RRCCost1 = p_web.RestoreValue('tmp:RRCCost1')
 tmp:originalInvoice = p_web.RestoreValue('tmp:originalInvoice')
 tmp:RRCCost2 = p_web.RestoreValue('tmp:RRCCost2')
 tmp:RRCCost3 = p_web.RestoreValue('tmp:RRCCost3')
 tmp:RRCCost4 = p_web.RestoreValue('tmp:RRCCost4')
 tmp:RRCCost5 = p_web.RestoreValue('tmp:RRCCost5')
 tmp:RRCCost6 = p_web.RestoreValue('tmp:RRCCost6')
 tmp:RRCCost7 = p_web.RestoreValue('tmp:RRCCost7')
 tmp:RRCCost8 = p_web.RestoreValue('tmp:RRCCost8')
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
    loc:viewonly = Choose(p_web.GSV('Job:ViewOnly') = 1,1,0)
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('View Costs') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('View Costs',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_ViewCosts',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      If p_web.GSV('Hide:ARCCosts') <> 1
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_ViewCosts0_div')&'">'&p_web.Translate('ARC Costs')&'</a></li>'& CRLF
      End
      If p_web.GSV('Hide:RRCCosts') <> 1
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_ViewCosts1_div')&'">'&p_web.Translate('RRC Costs')&'</a></li>'& CRLF
      End
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_ViewCosts2_div')&'">'&p_web.Translate()&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="ViewCosts_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('ViewCosts_BrowseJobCredits_embedded_div')&''');'
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('ViewCosts_BrowseJobCredits_embedded_div')&''');'
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.cancel();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CancelButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="ViewCosts_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'ViewCosts_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="ViewCosts_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('ViewCosts_BrowseJobCredits_embedded_div')&''');'
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'ViewCosts_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    ElsIf p_web.GSV('Hide:ARCCosts') <> 1
        p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:ARCViewCostType')
    ElsIf p_web.GSV('Hide:RRCCosts') <> 1
        p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:RRCViewCostType')
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.jobe:ExcReplcamentCharge')
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_ViewCosts')>0,p_web.GSV('showtab_ViewCosts'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_ViewCosts'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_ViewCosts') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_ViewCosts'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_ViewCosts')>0,p_web.GSV('showtab_ViewCosts'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_ViewCosts') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
      If p_web.GSV('Hide:ARCCosts') <> 1
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('ARC Costs') & ''''
      End
      If p_web.GSV('Hide:RRCCosts') <> 1
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('RRC Costs') & ''''
      End
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_ViewCosts_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_ViewCosts')>0,p_web.GSV('showtab_ViewCosts'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"ViewCosts",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_ViewCosts')>0,p_web.GSV('showtab_ViewCosts'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_ViewCosts_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('ViewCosts') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('ViewCosts')
    p_web.SetValue('_CallPopups',2)
    If p_web.GetPreCall('BrowseJobCredits') = 0
      p_web.SetValue('BrowseJobCredits:NoForm',1)
      p_web.SetValue('BrowseJobCredits:FormName',loc:formname)
      p_web.SetValue('BrowseJobCredits:parentIs','Form')
      p_web.SetValue('_parentProc','ViewCosts')
      BrowseJobCredits(p_web)
      p_web.SetValue('_CallPopups',0)
      p_web.DeleteValue('BrowseJobCredits:NoForm')
      p_web.DeleteValue('BrowseJobCredits:FormName')
      p_web.DeleteValue('BrowseJobCredits:parentIs')
      p_web.DeleteValue('_parentProc')
    End
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
  If p_web.GSV('Hide:ARCCosts') <> 1
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('ARC Costs')&'</a></h3>' & CRLF & p_web.DivHeader('tab_ViewCosts0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewCosts0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewCosts0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewCosts0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'ARC Costs')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewCosts0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('ARC Costs')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewCosts0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('ARC Costs')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewCosts0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:ARCViewCostType
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:ARCViewCostType
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:ARCViewCostType
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::__line1
        do Comment::__line1
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:ARCIgnoreDefaultCharges
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:ARCIgnoreDefaultCharges
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:ARCIgnoreDefaultCharges
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:ARCIgnoreReason
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:ARCIgnoreReason
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:ARCIgnoreReason
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::buttonAcceptARCReason
        do Comment::buttonAcceptARCReason
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::buttonCancelARCReason
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::buttonCancelARCReason
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:ARCCost1
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:ARCCost1
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:ARCCost1
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:AdjustmentCost1
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:AdjustmentCost1
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:ARCCost2
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:ARCCost2
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:ARCCost2
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:AdjustmentCost2
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:AdjustmentCost2
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:ARCCost3
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:ARCCost3
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:ARCCost3
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:AdjustmentCost3
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:AdjustmentCost3
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::__line2
        do Comment::__line2
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:ARCCost4
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:ARCCost4
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:ARCCost4
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:AdjustmentCost4
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:AdjustmentCost4
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:ARCCost5
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:ARCCost5
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:ARCCost5
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:AdjustmentCost5
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:AdjustmentCost5
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:ARCCost6
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:ARCCost6
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:ARCCost6
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:AdjustmentCost6
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:AdjustmentCost6
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:ARCCost7
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:ARCCost7
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:ARCCost7
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:ARCCost8
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:ARCCost8
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:ARCCost8
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jobe:ARC3rdPartyInvoiceNumber
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jobe:ARC3rdPartyInvoiceNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::jobe:ARC3rdPartyInvoiceNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:ARCInvoiceNumber
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:ARCInvoiceNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:ARCInvoiceNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
  end
GenerateTab1  Routine
  If p_web.GSV('Hide:RRCCosts') <> 1
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('RRC Costs')&'</a></h3>' & CRLF & p_web.DivHeader('tab_ViewCosts1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewCosts1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewCosts1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewCosts1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'RRC Costs')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewCosts1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('RRC Costs')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewCosts1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('RRC Costs')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewCosts1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:RRCViewCostType
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:RRCViewCostType
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:RRCViewCostType
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::__line3
        do Comment::__line3
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:RRCIgnoreDefaultCharges
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:RRCIgnoreDefaultCharges
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:RRCIgnoreDefaultCharges
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:RRCIgnoreReason
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:RRCIgnoreReason
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:RRCIgnoreReason
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::buttonAcceptRRCReason
        do Comment::buttonAcceptRRCReason
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::buttonCancelRRCReason
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::buttonCancelRRCReason
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:RRCCost0
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:RRCCost0
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:RRCCost0
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:RRCCost1
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:RRCCost1
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:RRCCost1
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:originalInvoice
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:originalInvoice
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:originalInvoice
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:RRCCost2
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:RRCCost2
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:RRCCost2
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:RRCCost3
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:RRCCost3
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:RRCCost3
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::BrowseJobCredits
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::__line4
        do Comment::__line4
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:RRCCost4
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:RRCCost4
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:RRCCost4
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:RRCCost5
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:RRCCost5
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:RRCCost5
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:RRCCost6
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:RRCCost6
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:RRCCost6
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:RRCCost7
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:RRCCost7
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:RRCCost7
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:RRCCost8
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:RRCCost8
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:RRCCost8
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
  end
GenerateTab2  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate()&'</a></h3>' & CRLF & p_web.DivHeader('tab_ViewCosts2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewCosts2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewCosts2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewCosts2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,)
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewCosts2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend)
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewCosts2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain">' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewCosts2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jobe:ExcReplcamentCharge
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jobe:ExcReplcamentCharge
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::jobe:ExcReplcamentCharge
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Prompt::tmp:ARCViewCostType  Routine
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCViewCostType') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('View Cost'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:ARCViewCostType  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:ARCViewCostType = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:ARCViewCostType = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:ARCViewCostType  ! copies value to session value if valid.
  do displayARCCostFields
  do displayOtherFields
  do Value::tmp:ARCViewCostType
  do SendAlert
  do Comment::tmp:ARCViewCostType ! allows comment style to be updated.
  do Prompt::tmp:ARCCost1
  do Value::tmp:ARCCost1  !1
  do Comment::tmp:ARCCost1
  do Prompt::tmp:ARCCost2
  do Value::tmp:ARCCost2  !1
  do Comment::tmp:ARCCost2
  do Prompt::tmp:ARCCost3
  do Value::tmp:ARCCost3  !1
  do Comment::tmp:ARCCost3
  do Prompt::tmp:ARCCost4
  do Value::tmp:ARCCost4  !1
  do Comment::tmp:ARCCost4
  do Prompt::tmp:ARCCost5
  do Value::tmp:ARCCost5  !1
  do Comment::tmp:ARCCost5
  do Prompt::tmp:ARCCost6
  do Value::tmp:ARCCost6  !1
  do Comment::tmp:ARCCost6
  do Prompt::tmp:ARCCost7
  do Value::tmp:ARCCost7  !1
  do Comment::tmp:ARCCost7
  do Prompt::tmp:ARCCost8
  do Value::tmp:ARCCost8  !1
  do Comment::tmp:ARCCost8
  do Value::tmp:AdjustmentCost1  !1
  do Value::tmp:AdjustmentCost2  !1
  do Value::tmp:AdjustmentCost3  !1
  do Value::tmp:AdjustmentCost4  !1
  do Value::tmp:AdjustmentCost5  !1
  do Value::tmp:AdjustmentCost6  !1
  do Prompt::jobe:ExcReplcamentCharge
  do Value::jobe:ExcReplcamentCharge  !1
  do Prompt::jobe:ARC3rdPartyInvoiceNumber
  do Value::jobe:ARC3rdPartyInvoiceNumber  !1
  do Prompt::tmp:ARCIgnoreDefaultCharges
  do Value::tmp:ARCIgnoreDefaultCharges  !1

ValidateValue::tmp:ARCViewCostType  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:ARCViewCostType',tmp:ARCViewCostType).
    End

Value::tmp:ARCViewCostType  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCViewCostType') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,p_web.site.style.formselect,'FormEntry',)
  If loc:retrying
    tmp:ARCViewCostType = p_web.RestoreValue('tmp:ARCViewCostType')
    do ValidateValue::tmp:ARCViewCostType
    If tmp:ARCViewCostType:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- DROPLIST ---
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ARCViewCostType'',''viewcosts_tmp:arcviewcosttype_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:ARCViewCostType')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:ARCViewCostType',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
  loc:fieldclass = ''
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('tmp:ARCViewCostType') = 0
    p_web.SetSessionValue('tmp:ARCViewCostType','Chargeable')
  end
  If p_web.GSV('Hide:ARCChargeable') <> 1
    packet = clip(packet) & p_web.CreateOption('Chargeable','Chargeable',choose('Chargeable' = p_web.getsessionvalue('tmp:ARCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:ARCWarranty') <> 1
    packet = clip(packet) & p_web.CreateOption('Warranty','Warranty',choose('Warranty' = p_web.getsessionvalue('tmp:ARCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:ARCClaim') <> 1
    packet = clip(packet) & p_web.CreateOption('Claim','Claim',choose('Claim' = p_web.getsessionvalue('tmp:ARCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:ARC3rdParty') <> 1
    packet = clip(packet) & p_web.CreateOption('3rd Party','3rd Party',choose('3rd Party' = p_web.getsessionvalue('tmp:ARCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:ARCEstimate') <> 1
    packet = clip(packet) & p_web.CreateOption('Estimate','Estimate',choose('Estimate' = p_web.getsessionvalue('tmp:ARCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:ARCCInvoice') <> 1
    packet = clip(packet) & p_web.CreateOption('Chargeable - Invoiced','Chargeable - Invoiced',choose('Chargeable - Invoiced' = p_web.getsessionvalue('tmp:ARCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:ARCWInvoice') <> 1
    packet = clip(packet) & p_web.CreateOption('Warranty - Invoiced','Warranty - Invoiced',choose('Warranty - Invoiced' = p_web.getsessionvalue('tmp:ARCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:ARCClaimInvoice') <> 1
    packet = clip(packet) & p_web.CreateOption('Claim - Invoiced','Claim - Invoiced',choose('Claim - Invoiced' = p_web.getsessionvalue('tmp:ARCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:ManufacturerPaid') <> 1
    packet = clip(packet) & p_web.CreateOption('Manufacturer Payment','Manufacturer Payment',choose('Manufacturer Payment' = p_web.getsessionvalue('tmp:ARCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:ARCViewCostType  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:ARCViewCostType:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCViewCostType') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::__line1  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::__line1  ! copies value to session value if valid.
  do Comment::__line1 ! allows comment style to be updated.

ValidateValue::__line1  Routine
    If not (1=0)
    End

Value::__line1  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine('nt-width-100')
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('__line1') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & '<hr></hr><13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::__line1  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if __line1:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('__line1') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:ARCIgnoreDefaultCharges  Routine
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCIgnoreDefaultCharges') & '_prompt',Choose(p_web.GSV('tmp:ARCViewCostType') = 'Manufacturer Payment' or p_web.GSV('tmp:ARCViewCostType') = '','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('tmp:ARCViewCostType') = 'Manufacturer Payment' or p_web.GSV('tmp:ARCViewCostType') = '','',p_web.Translate('Ignore Default Charges'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:ARCIgnoreDefaultCharges  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:ARCIgnoreDefaultCharges = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    tmp:ARCIgnoreDefaultCharges = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:ARCIgnoreDefaultCharges  ! copies value to session value if valid.
  if (p_web.GSV('tmp:ARCIgnoreDefaultCharges') = 1)
      p_web.SSV('ValidateIgnoreTickBoxARC',1)
      p_web.SSV('tmp:ARCIgnoreReason','')
  else  ! if (p_web.GSV('tmp:RRCIgnoreDefaultCharges') = 1)
      p_web.SSV('ReadOnly:ARCCost2',1)
      case p_web.GSV('tmp:ARCViewCostType')
      of 'Chargeable'
          p_web.SSV('job:Ignore_Chargeable_Charges','NO')
      of 'Warranty'
          p_web.SSV('job:Ignore_Warranty_Charges','NO')
      of 'Estimate'
          p_web.SSV('job:Ignore_Estimate_Charges','NO')
      end ! case p_web.GSV('tmp:ARCViewCostType')
      do pricingRoutine
  end ! if (p_web.GSV('tmp:RRCIgnoreDefaultCharges') = 1)
  do Value::tmp:ARCIgnoreDefaultCharges
  do SendAlert
  do Comment::tmp:ARCIgnoreDefaultCharges ! allows comment style to be updated.
  do Value::buttonAcceptARCReason  !1
  do Value::buttonCancelARCReason  !1
  do Value::tmp:ARCViewCostType  !1
  do Prompt::tmp:ARCIgnoreReason
  do Value::tmp:ARCIgnoreReason  !1
  do Value::tmp:ARCCost2  !1
  do Value::tmp:ARCCost3  !1
  do Value::tmp:ARCCost4  !1
  do Value::tmp:ARCCost5  !1
  do Value::tmp:ARCCost6  !1
  do Value::tmp:ARCCost7  !1
  do Value::tmp:ARCCost8  !1

ValidateValue::tmp:ARCIgnoreDefaultCharges  Routine
    If not (p_web.GSV('tmp:ARCViewCostType') = 'Manufacturer Payment' or p_web.GSV('tmp:ARCViewCostType') = '')
      if loc:invalid = '' then p_web.SetSessionValue('tmp:ARCIgnoreDefaultCharges',tmp:ARCIgnoreDefaultCharges).
    End

Value::tmp:ARCIgnoreDefaultCharges  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('tmp:ARCViewCostType') = 'Manufacturer Payment' or p_web.GSV('tmp:ARCViewCostType') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCIgnoreDefaultCharges') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If loc:retrying
    tmp:ARCIgnoreDefaultCharges = p_web.RestoreValue('tmp:ARCIgnoreDefaultCharges')
    do ValidateValue::tmp:ARCIgnoreDefaultCharges
    If tmp:ARCIgnoreDefaultCharges:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('tmp:ARCViewCostType') = 'Manufacturer Payment' or p_web.GSV('tmp:ARCViewCostType') = '')
  ! --- CHECKBOX --- tmp:ARCIgnoreDefaultCharges
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:ARCIgnoreDefaultCharges'',''viewcosts_tmp:arcignoredefaultcharges_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:ARCIgnoreDefaultCharges')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('BookingSite') = 'RRC' Or Instring('- Invoiced',p_web.GSV('tmp:ARCViewCostType'),1,1) or p_web.GSV('ValidateIgnoreTickBoxARC') = 1,'disabled','')
  If p_web.GetSessionValue('tmp:ARCIgnoreDefaultCharges') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','tmp:ARCIgnoreDefaultCharges',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:ARCIgnoreDefaultCharges  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:ARCIgnoreDefaultCharges:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('tmp:ARCViewCostType') = 'Manufacturer Payment' or p_web.GSV('tmp:ARCViewCostType') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCIgnoreDefaultCharges') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('tmp:ARCViewCostType') = 'Manufacturer Payment' or p_web.GSV('tmp:ARCViewCostType') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:ARCIgnoreReason  Routine
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCIgnoreReason') & '_prompt',Choose(p_web.GSV('ValidateIgnoreTickBoxARC') <> 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('ValidateIgnoreTickBoxARC') <> 1,'',p_web.Translate('Enter Reason For Ignoring Standard Charges'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:ARCIgnoreReason  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:ARCIgnoreReason = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:ARCIgnoreReason = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:ARCIgnoreReason  ! copies value to session value if valid.
  p_web.SSV('tmp:ARCIgnoreReason',BHStripReplace(p_web.GSV('tmp:ARCIgnoreReason'),'<9>',''))
  do Value::tmp:ARCIgnoreReason
  do SendAlert
  do Comment::tmp:ARCIgnoreReason ! allows comment style to be updated.

ValidateValue::tmp:ARCIgnoreReason  Routine
    If not (p_web.GSV('ValidateIgnoreTickBoxARC') <> 1)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:ARCIgnoreReason',tmp:ARCIgnoreReason).
    End

Value::tmp:ARCIgnoreReason  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('ValidateIgnoreTickBoxARC') <> 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCIgnoreReason') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    tmp:ARCIgnoreReason = p_web.RestoreValue('tmp:ARCIgnoreReason')
    do ValidateValue::tmp:ARCIgnoreReason
    If tmp:ARCIgnoreReason:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('ValidateIgnoreTickBoxARC') <> 1)
  ! --- TEXT --- tmp:ARCIgnoreReason
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ARCIgnoreReason'',''viewcosts_tmp:arcignorereason_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:ARCIgnoreReason')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'readonly','')
  do SendPacket
  p_web.CreateTextArea('tmp:ARCIgnoreReason',p_web.GetSessionValue('tmp:ARCIgnoreReason'),5,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(tmp:ARCIgnoreReason),,,,Net:Send)
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:ARCIgnoreReason  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:ARCIgnoreReason:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('ValidateIgnoreTickBoxARC') <> 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCIgnoreReason') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('ValidateIgnoreTickBoxARC') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonAcceptARCReason  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonAcceptARCReason  ! copies value to session value if valid.
  if (p_web.GSV('tmp:ARCIgnoreReason') <> '')
      p_web.SSV('ValidateIgnoreTickBoxARC',0)
      case p_web.GSV('tmp:ARCViewCostType')
      of 'Chargeable'
          local.AddToTempQueue('IGNORE DEFAULT ARC CHARGEABLE COSTS',|
                          p_web.GSV('job:Ignore_Chargeable_Charges'), |
                          p_web.GSV('tmp:ARCIgnoreReason'),1,|
                          p_web.GSV('tmp:ARCCost2'))
          p_web.SSV(job:Ignore_Chargeable_Charges,'YES')
      of 'Warranty'
          local.AddToTempQueue('IGNORE DEFAULT ARC WARRANTY COSTS',|
                          p_web.GSV('job:Ignore_Warranty_Charges'),|
                          p_web.GSV('tmp:ARCIgnoreReason'),1,|
                          p_web.GSV('tmp:ARCCost2'))
          p_web.SSV('job:Ignore_Warranty_Charges','YES')
      of 'Estimate'
          local.AddToTempQueue('IGNORE DEFAULT RRC ESTIMATE COSTS',|
                          p_web.GSV('job:Ignore_Estimate_Charges'),|
                          p_web.GSV('tmp:ARCIgnoreReason'),1,|
                          p_web.GSV('tmp:ARCCost2'))
          p_web.SSV('job:Ignore_Estimate_Charges','YES')
      end ! case p_web.GSV('tmp:ARCViewCostType')
      p_web.SSV('ReadOnly:ARCCost2',0)
  else ! if (p_web.GSV('tmp:RRCIgnoreReason') <> '')
      p_web.SSV('ReadOnly:ARCCost2',1)
      case p_web.GSV('tmp:ARCViewCostType')
      of 'Chargeable'
          p_web.SSV('job:Ignore_Chargeable_Charges','YES')
      of 'Warranty'
          p_web.SSV('job:Ignore_Warranty_Charges','YES')
      of 'Estimate'
          p_web.SSV('job:Ignore_Estimate_Charges','YES')
      end ! case p_web.GSV('tmp:ARCViewCostType')
      p_web.SSV('tmp:ARCIgnoreDefaultCharges',0)
  end ! if (p_web.GSV('tmp:RRCIgnoreReason') <> '')
  do Value::buttonAcceptARCReason
  do Comment::buttonAcceptARCReason ! allows comment style to be updated.
  do Value::buttonCancelARCReason  !1
  do Prompt::tmp:ARCIgnoreDefaultCharges
  do Value::tmp:ARCIgnoreDefaultCharges  !1
  do Prompt::tmp:ARCIgnoreReason
  do Value::tmp:ARCIgnoreReason  !1
  do Prompt::tmp:ARCCost2
  do Value::tmp:ARCCost2  !1
  do Value::tmp:ARCViewCostType  !1

ValidateValue::buttonAcceptARCReason  Routine
    If not (p_web.GSV('ValidateIgnoreTickBoxARC') <> 1)
    End

Value::buttonAcceptARCReason  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('ValidateIgnoreTickBoxARC') <> 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('buttonAcceptARCReason') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (p_web.GSV('ValidateIgnoreTickBoxARC') <> 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonAcceptARCReason'',''viewcosts_buttonacceptarcreason_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','AcceptARCReason','Accept',p_web.combine(Choose('Accept' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'SmallButton'),loc:formname,,,,loc:javascript,loc:disabled,,,,,,,,,,'nt-left')!h
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonAcceptARCReason  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonAcceptARCReason:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('ValidateIgnoreTickBoxARC') <> 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('buttonAcceptARCReason') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('ValidateIgnoreTickBoxARC') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonCancelARCReason  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonCancelARCReason  ! copies value to session value if valid.
  p_web.SSV('ValidateIgnoreTickBoxARC',0)
  p_web.SSV('ReadOnly:ARCCost2',1)
  case p_web.GSV('tmp:ARCViewCostType')
  of 'Chargeable'
      p_web.SSV('job:Ignore_Chargeable_Charges','NO')
  of 'Warranty'
      p_web.SSV('job:Ignore_Warranty_Charges','NO')
  of 'Estimate'
      p_web.SSV('job:Ignore_Warranty_Charges','NO')
  end ! case p_web.GSV('tmp:ARCViewCostType')
  p_web.SSV('tmp:ARCIgnoreDefaultCharges',0)
  do Value::buttonCancelARCReason
  do Comment::buttonCancelARCReason ! allows comment style to be updated.
  do Value::buttonAcceptARCReason  !1
  do Prompt::tmp:ARCIgnoreDefaultCharges
  do Value::tmp:ARCIgnoreDefaultCharges  !1
  do Prompt::tmp:ARCIgnoreReason
  do Value::tmp:ARCIgnoreReason  !1
  do Prompt::tmp:ARCCost2
  do Value::tmp:ARCCost2  !1
  do Prompt::tmp:ARCViewCostType
  do Value::tmp:ARCViewCostType  !1

ValidateValue::buttonCancelARCReason  Routine
    If not (p_web.GSV('ValidateIgnoreTickBoxARC') <> 1)
    End

Value::buttonCancelARCReason  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('ValidateIgnoreTickBoxARC') <> 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('buttonCancelARCReason') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (p_web.GSV('ValidateIgnoreTickBoxARC') <> 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonCancelARCReason'',''viewcosts_buttoncancelarcreason_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','CancelARCReason','Cancel',p_web.combine(Choose('Cancel' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'SmallButton'),loc:formname,,,,loc:javascript,loc:disabled,,,,,,,,,,'nt-left')!h
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonCancelARCReason  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonCancelARCReason:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('ValidateIgnoreTickBoxARC') <> 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('buttonCancelARCReason') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('ValidateIgnoreTickBoxARC') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:ARCCost1  Routine
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost1') & '_prompt',Choose(p_web.GSV('Prompt:Cost1') = '','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Prompt:Cost1') = '','',p_web.Translate(p_web.GSV('Prompt:Cost1')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:ARCCost1  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:ARCCost1 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    tmp:ARCCost1 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::tmp:ARCCost1  ! copies value to session value if valid.
  do Value::tmp:ARCCost1
  do SendAlert
  do Comment::tmp:ARCCost1 ! allows comment style to be updated.

ValidateValue::tmp:ARCCost1  Routine
    If not (p_web.GSV('Prompt:Cost1') = '')
      if loc:invalid = '' then p_web.SetSessionValue('tmp:ARCCost1',tmp:ARCCost1).
    End

Value::tmp:ARCCost1  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Prompt:Cost1') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost1') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If p_web.GSV('BookingSite') = 'RRC' Or p_web.GSV('ReadOnly:ARCCost1') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    tmp:ARCCost1 = p_web.RestoreValue('tmp:ARCCost1')
    do ValidateValue::tmp:ARCCost1
    If tmp:ARCCost1:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('Prompt:Cost1') = '')
  ! --- STRING --- tmp:ARCCost1
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('BookingSite') = 'RRC' Or p_web.GSV('ReadOnly:ARCCost1') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ARCCost1'',''viewcosts_tmp:arccost1_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:ARCCost1',p_web.GetSessionValue('tmp:ARCCost1'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:ARCCost1  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:ARCCost1:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('Prompt:Cost1') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost1') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Prompt:Cost1') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::tmp:AdjustmentCost1  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:AdjustmentCost1 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    tmp:AdjustmentCost1 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::tmp:AdjustmentCost1  ! copies value to session value if valid.
  do Value::tmp:AdjustmentCost1
  do SendAlert
  do Comment::tmp:AdjustmentCost1 ! allows comment style to be updated.

ValidateValue::tmp:AdjustmentCost1  Routine
    If not (p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced')
      if loc:invalid = '' then p_web.SetSessionValue('tmp:AdjustmentCost1',tmp:AdjustmentCost1).
    End

Value::tmp:AdjustmentCost1  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost1') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    tmp:AdjustmentCost1 = p_web.RestoreValue('tmp:AdjustmentCost1')
    do ValidateValue::tmp:AdjustmentCost1
    If tmp:AdjustmentCost1:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced')
  ! --- STRING --- tmp:AdjustmentCost1
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:AdjustmentCost1'',''viewcosts_tmp:adjustmentcost1_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:AdjustmentCost1',p_web.GetSessionValue('tmp:AdjustmentCost1'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:AdjustmentCost1  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:AdjustmentCost1:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost1') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced'
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:ARCCost2  Routine
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost2') & '_prompt',Choose(p_web.GSV('Prompt:Cost2') = '','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Prompt:Cost2') = '','',p_web.Translate(p_web.GSV('Prompt:Cost2')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:ARCCost2  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:ARCCost2 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    tmp:ARCCost2 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::tmp:ARCCost2  ! copies value to session value if valid.
  do Value::tmp:ARCCost2
  do SendAlert
  do Comment::tmp:ARCCost2 ! allows comment style to be updated.

ValidateValue::tmp:ARCCost2  Routine
    If not (p_web.GSV('Prompt:Cost2') = '')
      if loc:invalid = '' then p_web.SetSessionValue('tmp:ARCCost2',tmp:ARCCost2).
    End

Value::tmp:ARCCost2  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Prompt:Cost2') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost2') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If p_web.GSV('BookingSite') = 'RRC' Or p_web.GSV('ReadOnly:ARCCost2') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    tmp:ARCCost2 = p_web.RestoreValue('tmp:ARCCost2')
    do ValidateValue::tmp:ARCCost2
    If tmp:ARCCost2:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('Prompt:Cost2') = '')
  ! --- STRING --- tmp:ARCCost2
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('BookingSite') = 'RRC' Or p_web.GSV('ReadOnly:ARCCost2') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ARCCost2'',''viewcosts_tmp:arccost2_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:ARCCost2',p_web.GetSessionValue('tmp:ARCCost2'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:ARCCost2  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:ARCCost2:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('Prompt:Cost2') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost2') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Prompt:Cost2') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::tmp:AdjustmentCost2  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:AdjustmentCost2 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    tmp:AdjustmentCost2 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::tmp:AdjustmentCost2  ! copies value to session value if valid.
  do Value::tmp:AdjustmentCost2
  do SendAlert
  do Comment::tmp:AdjustmentCost2 ! allows comment style to be updated.

ValidateValue::tmp:AdjustmentCost2  Routine
    If not (p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced')
      if loc:invalid = '' then p_web.SetSessionValue('tmp:AdjustmentCost2',tmp:AdjustmentCost2).
    End

Value::tmp:AdjustmentCost2  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost2') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    tmp:AdjustmentCost2 = p_web.RestoreValue('tmp:AdjustmentCost2')
    do ValidateValue::tmp:AdjustmentCost2
    If tmp:AdjustmentCost2:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced')
  ! --- STRING --- tmp:AdjustmentCost2
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:AdjustmentCost2'',''viewcosts_tmp:adjustmentcost2_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:AdjustmentCost2',p_web.GetSessionValue('tmp:AdjustmentCost2'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:AdjustmentCost2  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:AdjustmentCost2:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost2') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced'
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:ARCCost3  Routine
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost3') & '_prompt',Choose(p_web.GSV('Prompt:Cost3') = '','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Prompt:Cost3') = '','',p_web.Translate(p_web.GSV('Prompt:Cost3')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:ARCCost3  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:ARCCost3 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    tmp:ARCCost3 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::tmp:ARCCost3  ! copies value to session value if valid.
  do Value::tmp:ARCCost3
  do SendAlert
  do Comment::tmp:ARCCost3 ! allows comment style to be updated.

ValidateValue::tmp:ARCCost3  Routine
    If not (p_web.GSV('Prompt:Cost3') = '')
      if loc:invalid = '' then p_web.SetSessionValue('tmp:ARCCost3',tmp:ARCCost3).
    End

Value::tmp:ARCCost3  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Prompt:Cost3') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost3') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If p_web.GSV('BookingSite') = 'RRC' Or p_web.GSV('ReadOnly:ARCCost3') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    tmp:ARCCost3 = p_web.RestoreValue('tmp:ARCCost3')
    do ValidateValue::tmp:ARCCost3
    If tmp:ARCCost3:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('Prompt:Cost3') = '')
  ! --- STRING --- tmp:ARCCost3
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('BookingSite') = 'RRC' Or p_web.GSV('ReadOnly:ARCCost3') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ARCCost3'',''viewcosts_tmp:arccost3_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:ARCCost3',p_web.GetSessionValue('tmp:ARCCost3'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:ARCCost3  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:ARCCost3:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('Prompt:Cost3') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost3') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Prompt:Cost3') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::tmp:AdjustmentCost3  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:AdjustmentCost3 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    tmp:AdjustmentCost3 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::tmp:AdjustmentCost3  ! copies value to session value if valid.
  do Value::tmp:AdjustmentCost3
  do SendAlert
  do Comment::tmp:AdjustmentCost3 ! allows comment style to be updated.

ValidateValue::tmp:AdjustmentCost3  Routine
    If not (p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced')
      if loc:invalid = '' then p_web.SetSessionValue('tmp:AdjustmentCost3',tmp:AdjustmentCost3).
    End

Value::tmp:AdjustmentCost3  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost3') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    tmp:AdjustmentCost3 = p_web.RestoreValue('tmp:AdjustmentCost3')
    do ValidateValue::tmp:AdjustmentCost3
    If tmp:AdjustmentCost3:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced')
  ! --- STRING --- tmp:AdjustmentCost3
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:AdjustmentCost3'',''viewcosts_tmp:adjustmentcost3_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:AdjustmentCost3',p_web.GetSessionValue('tmp:AdjustmentCost3'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:AdjustmentCost3  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:AdjustmentCost3:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost3') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced'
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::__line2  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::__line2  ! copies value to session value if valid.
  do Comment::__line2 ! allows comment style to be updated.

ValidateValue::__line2  Routine
    If not (1=0)
    End

Value::__line2  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine('nt-width-100')
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('__line2') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & '<hr></hr><13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::__line2  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if __line2:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('__line2') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:ARCCost4  Routine
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost4') & '_prompt',Choose(p_web.GSV('Prompt:Cost4') = '','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Prompt:Cost4') = '','',p_web.Translate(p_web.GSV('Prompt:Cost4')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:ARCCost4  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:ARCCost4 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    tmp:ARCCost4 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::tmp:ARCCost4  ! copies value to session value if valid.
  do Value::tmp:ARCCost4
  do SendAlert
  do Comment::tmp:ARCCost4 ! allows comment style to be updated.

ValidateValue::tmp:ARCCost4  Routine
    If not (p_web.GSV('Prompt:Cost4') = '')
      if loc:invalid = '' then p_web.SetSessionValue('tmp:ARCCost4',tmp:ARCCost4).
    End

Value::tmp:ARCCost4  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Prompt:Cost4') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost4') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  If loc:retrying
    tmp:ARCCost4 = p_web.RestoreValue('tmp:ARCCost4')
    do ValidateValue::tmp:ARCCost4
    If tmp:ARCCost4:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('Prompt:Cost4') = '')
  ! --- STRING --- tmp:ARCCost4
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ARCCost4'',''viewcosts_tmp:arccost4_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:ARCCost4',p_web.GetSessionValue('tmp:ARCCost4'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:ARCCost4  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:ARCCost4:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('Prompt:Cost4') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost4') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Prompt:Cost4') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::tmp:AdjustmentCost4  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:AdjustmentCost4 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    tmp:AdjustmentCost4 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::tmp:AdjustmentCost4  ! copies value to session value if valid.
  do Value::tmp:AdjustmentCost4
  do SendAlert
  do Comment::tmp:AdjustmentCost4 ! allows comment style to be updated.

ValidateValue::tmp:AdjustmentCost4  Routine
    If not (p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced')
      if loc:invalid = '' then p_web.SetSessionValue('tmp:AdjustmentCost4',tmp:AdjustmentCost4).
    End

Value::tmp:AdjustmentCost4  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost4') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    tmp:AdjustmentCost4 = p_web.RestoreValue('tmp:AdjustmentCost4')
    do ValidateValue::tmp:AdjustmentCost4
    If tmp:AdjustmentCost4:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced')
  ! --- STRING --- tmp:AdjustmentCost4
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:AdjustmentCost4'',''viewcosts_tmp:adjustmentcost4_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:AdjustmentCost4',p_web.GetSessionValue('tmp:AdjustmentCost4'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:AdjustmentCost4  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:AdjustmentCost4:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost4') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced'
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:ARCCost5  Routine
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost5') & '_prompt',Choose(p_web.GSV('Prompt:Cost5') = '','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Prompt:Cost5') = '','',p_web.Translate(p_web.GSV('Prompt:Cost5')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:ARCCost5  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:ARCCost5 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    tmp:ARCCost5 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::tmp:ARCCost5  ! copies value to session value if valid.
  do Value::tmp:ARCCost5
  do SendAlert
  do Comment::tmp:ARCCost5 ! allows comment style to be updated.

ValidateValue::tmp:ARCCost5  Routine
    If not (p_web.GSV('Prompt:Cost5') = '')
      if loc:invalid = '' then p_web.SetSessionValue('tmp:ARCCost5',tmp:ARCCost5).
    End

Value::tmp:ARCCost5  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Prompt:Cost5') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost5') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  If loc:retrying
    tmp:ARCCost5 = p_web.RestoreValue('tmp:ARCCost5')
    do ValidateValue::tmp:ARCCost5
    If tmp:ARCCost5:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('Prompt:Cost5') = '')
  ! --- STRING --- tmp:ARCCost5
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ARCCost5'',''viewcosts_tmp:arccost5_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:ARCCost5',p_web.GetSessionValue('tmp:ARCCost5'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:ARCCost5  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:ARCCost5:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('Prompt:Cost5') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost5') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Prompt:Cost5') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::tmp:AdjustmentCost5  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:AdjustmentCost5 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    tmp:AdjustmentCost5 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::tmp:AdjustmentCost5  ! copies value to session value if valid.
  do Value::tmp:AdjustmentCost5
  do SendAlert
  do Comment::tmp:AdjustmentCost5 ! allows comment style to be updated.

ValidateValue::tmp:AdjustmentCost5  Routine
    If not (p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced')
      if loc:invalid = '' then p_web.SetSessionValue('tmp:AdjustmentCost5',tmp:AdjustmentCost5).
    End

Value::tmp:AdjustmentCost5  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost5') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    tmp:AdjustmentCost5 = p_web.RestoreValue('tmp:AdjustmentCost5')
    do ValidateValue::tmp:AdjustmentCost5
    If tmp:AdjustmentCost5:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced')
  ! --- STRING --- tmp:AdjustmentCost5
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:AdjustmentCost5'',''viewcosts_tmp:adjustmentcost5_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:AdjustmentCost5',p_web.GetSessionValue('tmp:AdjustmentCost5'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:AdjustmentCost5  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:AdjustmentCost5:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost5') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced'
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:ARCCost6  Routine
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost6') & '_prompt',Choose(p_web.GSV('Prompt:Cost6') = '','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Prompt:Cost6') = '','',p_web.Translate(p_web.GSV('Prompt:Cost6')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:ARCCost6  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:ARCCost6 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    tmp:ARCCost6 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::tmp:ARCCost6  ! copies value to session value if valid.
  do Value::tmp:ARCCost6
  do SendAlert
  do Comment::tmp:ARCCost6 ! allows comment style to be updated.

ValidateValue::tmp:ARCCost6  Routine
    If not (p_web.GSV('Prompt:Cost6') = '')
      if loc:invalid = '' then p_web.SetSessionValue('tmp:ARCCost6',tmp:ARCCost6).
    End

Value::tmp:ARCCost6  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Prompt:Cost6') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost6') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  If loc:retrying
    tmp:ARCCost6 = p_web.RestoreValue('tmp:ARCCost6')
    do ValidateValue::tmp:ARCCost6
    If tmp:ARCCost6:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('Prompt:Cost6') = '')
  ! --- STRING --- tmp:ARCCost6
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ARCCost6'',''viewcosts_tmp:arccost6_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:ARCCost6',p_web.GetSessionValue('tmp:ARCCost6'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:ARCCost6  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:ARCCost6:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('Prompt:Cost6') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost6') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Prompt:Cost6') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::tmp:AdjustmentCost6  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:AdjustmentCost6 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    tmp:AdjustmentCost6 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::tmp:AdjustmentCost6  ! copies value to session value if valid.
  do Value::tmp:AdjustmentCost6
  do SendAlert
  do Comment::tmp:AdjustmentCost6 ! allows comment style to be updated.

ValidateValue::tmp:AdjustmentCost6  Routine
    If not (p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced')
      if loc:invalid = '' then p_web.SetSessionValue('tmp:AdjustmentCost6',tmp:AdjustmentCost6).
    End

Value::tmp:AdjustmentCost6  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost6') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    tmp:AdjustmentCost6 = p_web.RestoreValue('tmp:AdjustmentCost6')
    do ValidateValue::tmp:AdjustmentCost6
    If tmp:AdjustmentCost6:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced')
  ! --- STRING --- tmp:AdjustmentCost6
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:AdjustmentCost6'',''viewcosts_tmp:adjustmentcost6_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:AdjustmentCost6',p_web.GetSessionValue('tmp:AdjustmentCost6'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:AdjustmentCost6  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:AdjustmentCost6:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost6') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced'
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:ARCCost7  Routine
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost7') & '_prompt',Choose(p_web.GSV('Prompt:Cost7') = '','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Prompt:Cost7') = '','',p_web.Translate(p_web.GSV('Prompt:Cost7')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:ARCCost7  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:ARCCost7 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    tmp:ARCCost7 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::tmp:ARCCost7  ! copies value to session value if valid.
  do Value::tmp:ARCCost7
  do SendAlert
  do Comment::tmp:ARCCost7 ! allows comment style to be updated.

ValidateValue::tmp:ARCCost7  Routine
    If not (p_web.GSV('Prompt:Cost7') = '')
      if loc:invalid = '' then p_web.SetSessionValue('tmp:ARCCost7',tmp:ARCCost7).
    End

Value::tmp:ARCCost7  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Prompt:Cost7') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost7') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  If loc:retrying
    tmp:ARCCost7 = p_web.RestoreValue('tmp:ARCCost7')
    do ValidateValue::tmp:ARCCost7
    If tmp:ARCCost7:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('Prompt:Cost7') = '')
  ! --- STRING --- tmp:ARCCost7
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('BookingSite') = 'RRC','readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ARCCost7'',''viewcosts_tmp:arccost7_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:ARCCost7',p_web.GetSessionValue('tmp:ARCCost7'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:ARCCost7  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:ARCCost7:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('Prompt:Cost7') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost7') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Prompt:Cost7') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:ARCCost8  Routine
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost8') & '_prompt',Choose(p_web.GSV('Prompt:Cost8') = '','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Prompt:Cost8') = '','',p_web.Translate(p_web.GSV('Prompt:Cost8')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:ARCCost8  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:ARCCost8 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    tmp:ARCCost8 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::tmp:ARCCost8  ! copies value to session value if valid.
  do Value::tmp:ARCCost8
  do SendAlert
  do Comment::tmp:ARCCost8 ! allows comment style to be updated.

ValidateValue::tmp:ARCCost8  Routine
    If not (p_web.GSV('Prompt:Cost8') = '')
      if loc:invalid = '' then p_web.SetSessionValue('tmp:ARCCost8',tmp:ARCCost8).
    End

Value::tmp:ARCCost8  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Prompt:Cost8') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost8') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  If loc:retrying
    tmp:ARCCost8 = p_web.RestoreValue('tmp:ARCCost8')
    do ValidateValue::tmp:ARCCost8
    If tmp:ARCCost8:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('Prompt:Cost8') = '')
  ! --- STRING --- tmp:ARCCost8
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('BookingSite') = 'RRC','readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ARCCost8'',''viewcosts_tmp:arccost8_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:ARCCost8',p_web.GetSessionValue('tmp:ARCCost8'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:ARCCost8  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:ARCCost8:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('Prompt:Cost8') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost8') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Prompt:Cost8') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::jobe:ARC3rdPartyInvoiceNumber  Routine
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('jobe:ARC3rdPartyInvoiceNumber') & '_prompt',Choose(p_web.GSV('jobe:ARC3rdPartyInvoiceNumber') = 0 Or p_web.GSV('tmp:ARCViewCostType') <> '3rd Party','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('jobe:ARC3rdPartyInvoiceNumber') = 0 Or p_web.GSV('tmp:ARCViewCostType') <> '3rd Party','',p_web.Translate('Invoice Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jobe:ARC3rdPartyInvoiceNumber  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jobe:ARC3rdPartyInvoiceNumber = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    jobe:ARC3rdPartyInvoiceNumber = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::jobe:ARC3rdPartyInvoiceNumber  ! copies value to session value if valid.
  do Comment::jobe:ARC3rdPartyInvoiceNumber ! allows comment style to be updated.

ValidateValue::jobe:ARC3rdPartyInvoiceNumber  Routine
    If not (p_web.GSV('jobe:ARC3rdPartyInvoiceNumber') = 0 Or p_web.GSV('tmp:ARCViewCostType') <> '3rd Party')
      if loc:invalid = '' then p_web.SetSessionValue('jobe:ARC3rdPartyInvoiceNumber',jobe:ARC3rdPartyInvoiceNumber).
    End

Value::jobe:ARC3rdPartyInvoiceNumber  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('jobe:ARC3rdPartyInvoiceNumber') = 0 Or p_web.GSV('tmp:ARCViewCostType') <> '3rd Party','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('jobe:ARC3rdPartyInvoiceNumber') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (p_web.GSV('jobe:ARC3rdPartyInvoiceNumber') = 0 Or p_web.GSV('tmp:ARCViewCostType') <> '3rd Party')
  ! --- DISPLAY --- jobe:ARC3rdPartyInvoiceNumber
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('jobe:ARC3rdPartyInvoiceNumber'),) & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::jobe:ARC3rdPartyInvoiceNumber  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if jobe:ARC3rdPartyInvoiceNumber:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('jobe:ARC3rdPartyInvoiceNumber') = 0 Or p_web.GSV('tmp:ARCViewCostType') <> '3rd Party','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('jobe:ARC3rdPartyInvoiceNumber') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('jobe:ARC3rdPartyInvoiceNumber') = 0 Or p_web.GSV('tmp:ARCViewCostType') <> '3rd Party'
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:ARCInvoiceNumber  Routine
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCInvoiceNumber') & '_prompt',Choose(~Instring('-Invoiced',p_web.GSV('tmp:ARCViewCostType')),'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(~Instring('-Invoiced',p_web.GSV('tmp:ARCViewCostType')),'',p_web.Translate('Invoice Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:ARCInvoiceNumber  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:ARCInvoiceNumber = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:ARCInvoiceNumber = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:ARCInvoiceNumber  ! copies value to session value if valid.
  do Comment::tmp:ARCInvoiceNumber ! allows comment style to be updated.

ValidateValue::tmp:ARCInvoiceNumber  Routine
    If not (~Instring('-Invoiced',p_web.GSV('tmp:ARCViewCostType')))
      if loc:invalid = '' then p_web.SetSessionValue('tmp:ARCInvoiceNumber',tmp:ARCInvoiceNumber).
    End

Value::tmp:ARCInvoiceNumber  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(~Instring('-Invoiced',p_web.GSV('tmp:ARCViewCostType')),'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCInvoiceNumber') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (~Instring('-Invoiced',p_web.GSV('tmp:ARCViewCostType')))
  ! --- DISPLAY --- tmp:ARCInvoiceNumber
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('tmp:ARCInvoiceNumber'),) & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:ARCInvoiceNumber  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:ARCInvoiceNumber:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(~Instring('-Invoiced',p_web.GSV('tmp:ARCViewCostType')),'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCInvoiceNumber') & '_comment',loc:class,Net:NoSend)
  If ~Instring('-Invoiced',p_web.GSV('tmp:ARCViewCostType'))
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:RRCViewCostType  Routine
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCViewCostType') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('View Cost'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:RRCViewCostType  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:RRCViewCostType = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:RRCViewCostType = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:RRCViewCostType  ! copies value to session value if valid.
  do displayRRCCostFields
  do displayOtherFields
  do Value::tmp:RRCViewCostType
  do SendAlert
  do Comment::tmp:RRCViewCostType ! allows comment style to be updated.
  do Prompt::tmp:RRCCost0
  do Value::tmp:RRCCost0  !1
  do Prompt::tmp:RRCCost1
  do Value::tmp:RRCCost1  !1
  do Comment::tmp:RRCCost1
  do Prompt::tmp:RRCCost2
  do Value::tmp:RRCCost2  !1
  do Comment::tmp:RRCCost2
  do Prompt::tmp:RRCCost3
  do Value::tmp:RRCCost3  !1
  do Comment::tmp:RRCCost3
  do Prompt::tmp:RRCCost4
  do Value::tmp:RRCCost4  !1
  do Comment::tmp:RRCCost4
  do Prompt::tmp:RRCCost5
  do Value::tmp:RRCCost5  !1
  do Comment::tmp:RRCCost5
  do Prompt::tmp:RRCCost6
  do Value::tmp:RRCCost6  !1
  do Comment::tmp:RRCCost6
  do Prompt::tmp:RRCCost7
  do Value::tmp:RRCCost7  !1
  do Comment::tmp:RRCCost7
  do Prompt::tmp:RRCCost8
  do Value::tmp:RRCCost8  !1
  do Comment::tmp:RRCCost8
  do Prompt::jobe:ExcReplcamentCharge
  do Value::jobe:ExcReplcamentCharge  !1
  do Prompt::tmp:RRCIgnoreDefaultCharges
  do Value::tmp:RRCIgnoreDefaultCharges  !1

ValidateValue::tmp:RRCViewCostType  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:RRCViewCostType',tmp:RRCViewCostType).
    End

Value::tmp:RRCViewCostType  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCViewCostType') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,p_web.site.style.formselect,'FormEntry',)
  If loc:retrying
    tmp:RRCViewCostType = p_web.RestoreValue('tmp:RRCViewCostType')
    do ValidateValue::tmp:RRCViewCostType
    If tmp:RRCViewCostType:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- DROPLIST ---
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:RRCViewCostType'',''viewcosts_tmp:rrcviewcosttype_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:RRCViewCostType')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('ValidateIgnoreTickBox') = 1,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:RRCViewCostType',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
  loc:fieldclass = ''
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('tmp:RRCViewCostType') = 0
    p_web.SetSessionValue('tmp:RRCViewCostType','')
  end
    packet = clip(packet) & p_web.CreateOption('','',choose('' = p_web.getsessionvalue('tmp:RRCViewCostType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:RRCChargeable') <> 1
    packet = clip(packet) & p_web.CreateOption('Chargeable','Chargeable',choose('Chargeable' = p_web.getsessionvalue('tmp:RRCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:RRCWarranty') <> 1
    packet = clip(packet) & p_web.CreateOption('Warranty','Warranty',choose('Warranty' = p_web.getsessionvalue('tmp:RRCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:RRCHandling') <> 1
    packet = clip(packet) & p_web.CreateOption('Handling','Handling',choose('Handling' = p_web.getsessionvalue('tmp:RRCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:RRCExchange') <> 1
    packet = clip(packet) & p_web.CreateOption('Exchange','Exchange',choose('Exchange' = p_web.getsessionvalue('tmp:RRCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:RRCEstimate') <> 1
    packet = clip(packet) & p_web.CreateOption('Estimate','Estimate',choose('Estimate' = p_web.getsessionvalue('tmp:RRCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:RRCCInvoice') <> 1
    packet = clip(packet) & p_web.CreateOption('Chargeable - Invoiced','Chargeable - Invoiced',choose('Chargeable - Invoiced' = p_web.getsessionvalue('tmp:RRCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:RRCWInvoice') <> 1
    packet = clip(packet) & p_web.CreateOption('Warranty - Invoiced','Warranty - Invoiced',choose('Warranty - Invoiced' = p_web.getsessionvalue('tmp:RRCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:Credit') <> 1
    packet = clip(packet) & p_web.CreateOption('Credits','Credits',choose('Credits' = p_web.getsessionvalue('tmp:RRCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:RRCViewCostType  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:RRCViewCostType:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCViewCostType') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::__line3  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::__line3  ! copies value to session value if valid.
  do Comment::__line3 ! allows comment style to be updated.

ValidateValue::__line3  Routine
    If not (1=0)
    End

Value::__line3  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine('nt-width-100')
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('__line3') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & '<hr></hr><13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::__line3  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if __line3:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('__line3') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:RRCIgnoreDefaultCharges  Routine
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCIgnoreDefaultCharges') & '_prompt',Choose(p_web.GSV('tmp:RRCViewCostType') = 'Credits' Or p_web.GSV('tmp:RRCViewCostType') = 'Handling' Or p_web.GSV('tmp:RRCViewCostType') = 'Exchange' Or p_web.GSV('tmp:RRCViewCostType') = '','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('tmp:RRCViewCostType') = 'Credits' Or p_web.GSV('tmp:RRCViewCostType') = 'Handling' Or p_web.GSV('tmp:RRCViewCostType') = 'Exchange' Or p_web.GSV('tmp:RRCViewCostType') = '','',p_web.Translate('Ignore Default Charges'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:RRCIgnoreDefaultCharges  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:RRCIgnoreDefaultCharges = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    tmp:RRCIgnoreDefaultCharges = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:RRCIgnoreDefaultCharges  ! copies value to session value if valid.
  if (p_web.GSV('tmp:RRCIgnoreDefaultCharges') = 1)
      p_web.SSV('tmp:RRCIgnoreReason','')
      p_web.SSV('ValidateIgnoreTickBox',1)
  else  ! if (p_web.GSV('tmp:RRCIgnoreDefaultCharges') = 1)
      p_web.SSV('ReadOnly:RRCCost2',1)
      case p_web.GSV('tmp:RRCViewCostType')
      of 'Chargeable'
          p_web.SSV('jobe:IgnoreRRCChaCosts',0)
      of 'Warranty'
          p_web.SSV('jobe:IgnoreRRCWarCosts',0)
      of 'Estimate'
          p_web.SSV('jobe:IgnoreRRCEstCosts',0)
      end ! case p_web.GSV('tmp:ARCViewCostType')
      p_web.SSV('jobe2:JobDiscountAmnt',0)
      do pricingRoutine
      do displayRRCCostFields
  end ! if (p_web.GSV('tmp:RRCIgnoreDefaultCharges') = 1)
  do Value::tmp:RRCIgnoreDefaultCharges
  do SendAlert
  do Comment::tmp:RRCIgnoreDefaultCharges ! allows comment style to be updated.
  do Prompt::tmp:RRCIgnoreReason
  do Value::tmp:RRCIgnoreReason  !1
  do Value::buttonAcceptRRCReason  !1
  do Value::buttonCancelRRCReason  !1
  do Value::tmp:RRCViewCostType  !1
  do Value::tmp:RRCCost2  !1
  do Value::tmp:RRCCost3  !1
  do Value::tmp:RRCCost4  !1
  do Value::tmp:RRCCost5  !1
  do Value::tmp:RRCCost6  !1
  do Value::tmp:RRCCost7  !1
  do Value::tmp:RRCCost8  !1
  do Prompt::tmp:RRCCost0
  do Value::tmp:RRCCost0  !1
  do Comment::tmp:RRCCost0

ValidateValue::tmp:RRCIgnoreDefaultCharges  Routine
    If not (p_web.GSV('tmp:RRCViewCostType') = 'Credits' Or p_web.GSV('tmp:RRCViewCostType') = 'Handling' Or p_web.GSV('tmp:RRCViewCostType') = 'Exchange' Or p_web.GSV('tmp:RRCViewCostType') = '')
      if loc:invalid = '' then p_web.SetSessionValue('tmp:RRCIgnoreDefaultCharges',tmp:RRCIgnoreDefaultCharges).
    End

Value::tmp:RRCIgnoreDefaultCharges  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('tmp:RRCViewCostType') = 'Credits' Or p_web.GSV('tmp:RRCViewCostType') = 'Handling' Or p_web.GSV('tmp:RRCViewCostType') = 'Exchange' Or p_web.GSV('tmp:RRCViewCostType') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCIgnoreDefaultCharges') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If loc:retrying
    tmp:RRCIgnoreDefaultCharges = p_web.RestoreValue('tmp:RRCIgnoreDefaultCharges')
    do ValidateValue::tmp:RRCIgnoreDefaultCharges
    If tmp:RRCIgnoreDefaultCharges:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('tmp:RRCViewCostType') = 'Credits' Or p_web.GSV('tmp:RRCViewCostType') = 'Handling' Or p_web.GSV('tmp:RRCViewCostType') = 'Exchange' Or p_web.GSV('tmp:RRCViewCostType') = '')
  ! --- CHECKBOX --- tmp:RRCIgnoreDefaultCharges
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:RRCIgnoreDefaultCharges'',''viewcosts_tmp:rrcignoredefaultcharges_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon(tmp:RRCIgnoreReason)&''',2);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('ValidateIgnoreTickBox') = 1 Or Instring('- Invoiced',p_web.GSV('tmp:RRCViewCostType'),1,1),'disabled','')
  If p_web.GetSessionValue('tmp:RRCIgnoreDefaultCharges') = true
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','tmp:RRCIgnoreDefaultCharges',clip(true),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:RRCIgnoreDefaultCharges  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:RRCIgnoreDefaultCharges:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('tmp:RRCViewCostType') = 'Credits' Or p_web.GSV('tmp:RRCViewCostType') = 'Handling' Or p_web.GSV('tmp:RRCViewCostType') = 'Exchange' Or p_web.GSV('tmp:RRCViewCostType') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCIgnoreDefaultCharges') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('tmp:RRCViewCostType') = 'Credits' Or p_web.GSV('tmp:RRCViewCostType') = 'Handling' Or p_web.GSV('tmp:RRCViewCostType') = 'Exchange' Or p_web.GSV('tmp:RRCViewCostType') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:RRCIgnoreReason  Routine
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCIgnoreReason') & '_prompt',Choose(p_web.GSV('ValidateIgnoreTickBox') <> 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('ValidateIgnoreTickBox') <> 1,'',p_web.Translate('Enter Reason For Ignoring Standard Charges'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:RRCIgnoreReason  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:RRCIgnoreReason = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:RRCIgnoreReason = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:RRCIgnoreReason  ! copies value to session value if valid.
  p_web.SSV('tmp:RRCIgnoreReason',BHStripReplace(p_web.GSV('tmp:RRCIgnoreReason'),'<9>',''))
  do Value::tmp:RRCIgnoreReason
  do SendAlert
  do Comment::tmp:RRCIgnoreReason ! allows comment style to be updated.

ValidateValue::tmp:RRCIgnoreReason  Routine
    If not (p_web.GSV('ValidateIgnoreTickBox') <> 1)
  If tmp:RRCIgnoreReason = ''
    loc:Invalid = 'tmp:RRCIgnoreReason'
    tmp:RRCIgnoreReason:IsInvalid = true
    loc:alert = p_web.translate('Enter Reason For Ignoring Standard Charges') & ' ' & p_web.site.RequiredText
  End
    tmp:RRCIgnoreReason = Upper(tmp:RRCIgnoreReason)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:RRCIgnoreReason',tmp:RRCIgnoreReason).
    End

Value::tmp:RRCIgnoreReason  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('ValidateIgnoreTickBox') <> 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCIgnoreReason') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,'formrqd')
  If loc:retrying
    tmp:RRCIgnoreReason = p_web.RestoreValue('tmp:RRCIgnoreReason')
    do ValidateValue::tmp:RRCIgnoreReason
    If tmp:RRCIgnoreReason:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('ValidateIgnoreTickBox') <> 1)
  ! --- TEXT --- tmp:RRCIgnoreReason
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:RRCIgnoreReason'',''viewcosts_tmp:rrcignorereason_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:RRCIgnoreReason')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'readonly','')
  do SendPacket
  p_web.CreateTextArea('tmp:RRCIgnoreReason',p_web.GetSessionValue('tmp:RRCIgnoreReason'),5,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(tmp:RRCIgnoreReason),,,,Net:Send)
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:RRCIgnoreReason  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:RRCIgnoreReason:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web._jsok(p_web.site.RequiredText)
  loc:class = Choose(p_web.GSV('ValidateIgnoreTickBox') <> 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCIgnoreReason') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('ValidateIgnoreTickBox') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonAcceptRRCReason  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonAcceptRRCReason  ! copies value to session value if valid.
  if (p_web.GSV('tmp:RRCIgnoreReason') <> '')
      p_web.SSV('ValidateIgnoreTickBox',0)
      case p_web.GSV('tmp:RRCViewCostType')
      of 'Chargeable'
          local.AddToTempQueue('IGNORE DEFAULT RRC CHARGEABLE COSTS',|
                          p_web.GSV('jobe:IgnoreRRCChaCosts'), |
                          p_web.GSV('tmp:RRCIgnoreReason'), |
                          1,|
                          p_web.GSV('tmp:RRCCost2'))
          p_web.SSV('jobe:IgnoreRRCChaCosts',1)
          p_web.SSV('Prompt:RCost0','Discount')
      of 'Warranty'
          local.AddToTempQueue('IGNORE DEFAULT RRC WARRANTY COSTS',|
                          p_web.GSV('jobe:IgnoreRRCWarCosts'),|
                          p_web.GSV('tmp:RRCIgnoreReason'), |
                          1,|
                          p_web.GSV('tmp:RRCCost2'))
          p_web.SSV('jobe:IgnoreRRCWarCosts',1)
      of 'Estimate'
          local.AddToTempQueue('IGNORE DEFAULT RRC ESTIMATE COSTS',|
                          p_web.GSV('jobe:IgnoreRRCEstCosts'),|
                          p_web.GSV('tmp:RRCIgnoreReason'),|
                          1,|
                          p_web.GSV('tmp:RRCCost2'))
          p_web.SSV('jobe:IgnoreRRCEstCosts',1)
      end ! case p_web.GSV('tmp:ARCViewCostType')
      p_web.SSV('ReadOnly:RRCCost2',0)
  else ! if (p_web.GSV('tmp:RRCIgnoreReason') <> '')
      p_web.SSV('ReadOnly:RRCCost2',1)
      case p_web.GSV('tmp:RRCViewCostType')
      of 'Chargeable'
          p_web.SSV('jobe:IgnoreRRCChaCosts',0)
      of 'Warranty'
          p_web.SSV('jobe:IgnoreRRCWarCosts',0)
      of 'Estimate'
          p_web.SSV('jobe:IgnoreRRCEstCosts',0)
      end ! case p_web.GSV('tmp:ARCViewCostType')
      p_web.SSV('tmp:RRCIgnoreDefaultCharges',0)
      p_web.SSV('Prompt:RCost0','')
  end ! if (p_web.GSV('tmp:RRCIgnoreReason') <> '')
  do Value::buttonAcceptRRCReason
  do Comment::buttonAcceptRRCReason ! allows comment style to be updated.
  do Value::buttonCancelRRCReason  !1
  do Value::tmp:RRCIgnoreDefaultCharges  !1
  do Prompt::tmp:RRCIgnoreReason
  do Value::tmp:RRCIgnoreReason  !1
  do Value::tmp:RRCCost2  !1
  do Value::tmp:RRCViewCostType  !1
  do Prompt::tmp:RRCCost0
  do Value::tmp:RRCCost0  !1

ValidateValue::buttonAcceptRRCReason  Routine
    If not (p_web.GSV('ValidateIgnoreTickBox') <> 1)
    End

Value::buttonAcceptRRCReason  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('ValidateIgnoreTickBox') <> 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('buttonAcceptRRCReason') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (p_web.GSV('ValidateIgnoreTickBox') <> 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonAcceptRRCReason'',''viewcosts_buttonacceptrrcreason_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','AcceptRRCReason','Accept',p_web.combine(Choose('Accept' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'SmallButton'),loc:formname,,,,loc:javascript,loc:disabled,,,,,,,,,,'nt-left')!h
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonAcceptRRCReason  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonAcceptRRCReason:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('ValidateIgnoreTickBox') <> 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('buttonAcceptRRCReason') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('ValidateIgnoreTickBox') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonCancelRRCReason  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonCancelRRCReason  ! copies value to session value if valid.
  p_web.SSV('ValidateIgnoreTickBox',0)
  p_web.SSV('ReadOnly:RRCCost2',1)
      case p_web.GSV('tmp:RRCViewCostType')
      of 'Chargeable'
          p_web.SSV('jobe:IgnoreRRCChaCosts',0)
      of 'Warranty'
          p_web.SSV('jobe:IgnoreRRCWarCosts',0)
      of 'Estimate'
          p_web.SSV('jobe:IgnoreRRCEstCosts',0)
      end ! case p_web.GSV('tmp:ARCViewCostType')
  p_web.SSV('tmp:RRCIgnoreDefaultCharges',0)
  do Value::buttonCancelRRCReason
  do Comment::buttonCancelRRCReason ! allows comment style to be updated.
  do Value::buttonAcceptRRCReason  !1
  do Prompt::tmp:RRCIgnoreReason
  do Value::tmp:RRCIgnoreReason  !1
  do Value::tmp:RRCViewCostType  !1
  do Value::tmp:RRCIgnoreDefaultCharges  !1
  do Value::tmp:RRCCost2  !1
  do Prompt::tmp:RRCCost0
  do Value::tmp:RRCCost0  !1

ValidateValue::buttonCancelRRCReason  Routine
    If not (p_web.GSV('ValidateIgnoreTickBox') <> 1)
    End

Value::buttonCancelRRCReason  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('ValidateIgnoreTickBox') <> 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('buttonCancelRRCReason') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (p_web.GSV('ValidateIgnoreTickBox') <> 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonCancelRRCReason'',''viewcosts_buttoncancelrrcreason_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','CancelRRCReason','Cancel',p_web.combine(Choose('Cancel' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'SmallButton'),loc:formname,,,,loc:javascript,loc:disabled,,,,,,,,,,'nt-left')!h
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonCancelRRCReason  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonCancelRRCReason:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('ValidateIgnoreTickBox') <> 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('buttonCancelRRCReason') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('ValidateIgnoreTickBox') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:RRCCost0  Routine
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost0') & '_prompt',Choose(p_web.GSV('Prompt:RCost0') = '','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Prompt:RCost0') = '','',p_web.Translate(p_web.GSV('Prompt:RCost0')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:RRCCost0  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:RRCCost0 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    tmp:RRCCost0 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::tmp:RRCCost0  ! copies value to session value if valid.
  do Value::tmp:RRCCost0
  do SendAlert
  do Comment::tmp:RRCCost0 ! allows comment style to be updated.

ValidateValue::tmp:RRCCost0  Routine
    If not (p_web.GSV('Prompt:RCost0') = '')
      if loc:invalid = '' then p_web.SetSessionValue('tmp:RRCCost0',tmp:RRCCost0).
    End

Value::tmp:RRCCost0  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Prompt:RCost0') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost0') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  If loc:retrying
    tmp:RRCCost0 = p_web.RestoreValue('tmp:RRCCost0')
    do ValidateValue::tmp:RRCCost0
    If tmp:RRCCost0:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('Prompt:RCost0') = '')
  ! --- STRING --- tmp:RRCCost0
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:RRCCost0'',''viewcosts_tmp:rrccost0_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:RRCCost0',p_web.GetSessionValue('tmp:RRCCost0'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:RRCCost0  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:RRCCost0:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('Prompt:RCost0') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost0') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Prompt:RCost0') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:RRCCost1  Routine
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost1') & '_prompt',Choose(p_web.GSV('Prompt:RCost1') = '','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Prompt:RCost1') = '','',p_web.Translate(p_web.GSV('Prompt:RCost1')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:RRCCost1  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:RRCCost1 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    tmp:RRCCost1 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::tmp:RRCCost1  ! copies value to session value if valid.
  do Value::tmp:RRCCost1
  do SendAlert
  do Comment::tmp:RRCCost1 ! allows comment style to be updated.

ValidateValue::tmp:RRCCost1  Routine
    If not (p_web.GSV('Prompt:RCost1') = '')
      if loc:invalid = '' then p_web.SetSessionValue('tmp:RRCCost1',tmp:RRCCost1).
    End

Value::tmp:RRCCost1  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Prompt:RCost1') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost1') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If p_web.GSV('ReadOnly:RRCCost1') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    tmp:RRCCost1 = p_web.RestoreValue('tmp:RRCCost1')
    do ValidateValue::tmp:RRCCost1
    If tmp:RRCCost1:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('Prompt:RCost1') = '')
  ! --- STRING --- tmp:RRCCost1
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:RRCCost1') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:RRCCost1'',''viewcosts_tmp:rrccost1_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:RRCCost1',p_web.GetSessionValue('tmp:RRCCost1'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:RRCCost1  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:RRCCost1:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('Prompt:RCost1') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost1') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Prompt:RCost1') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:originalInvoice  Routine
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:originalInvoice') & '_prompt',Choose(p_web.GSV('tmp:RRCViewCostType') <> 'Credits','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('tmp:RRCViewCostType') <> 'Credits','',p_web.Translate('Original Invoice No'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:originalInvoice  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:originalInvoice = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:originalInvoice = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:originalInvoice  ! copies value to session value if valid.
  do Value::tmp:originalInvoice
  do SendAlert
  do Comment::tmp:originalInvoice ! allows comment style to be updated.

ValidateValue::tmp:originalInvoice  Routine
    If not (p_web.GSV('tmp:RRCViewCostType') <> 'Credits')
      if loc:invalid = '' then p_web.SetSessionValue('tmp:originalInvoice',tmp:originalInvoice).
    End

Value::tmp:originalInvoice  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('tmp:RRCViewCostType') <> 'Credits','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:originalInvoice') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  If loc:retrying
    tmp:originalInvoice = p_web.RestoreValue('tmp:originalInvoice')
    do ValidateValue::tmp:originalInvoice
    If tmp:originalInvoice:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('tmp:RRCViewCostType') <> 'Credits')
  ! --- STRING --- tmp:originalInvoice
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:originalInvoice'',''viewcosts_tmp:originalinvoice_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:originalInvoice',p_web.GetSessionValueFormat('tmp:originalInvoice'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:originalInvoice  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:originalInvoice:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('tmp:RRCViewCostType') <> 'Credits','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:originalInvoice') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('tmp:RRCViewCostType') <> 'Credits'
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:RRCCost2  Routine
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost2') & '_prompt',Choose(p_web.GSV('Prompt:RCost2') = '','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Prompt:RCost2') = '','',p_web.Translate(p_web.GSV('Prompt:RCost2')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:RRCCost2  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:RRCCost2 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    tmp:RRCCost2 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::tmp:RRCCost2  ! copies value to session value if valid.
  if (p_web.GSV('tmp:RRCViewCostType') = 'Chargeable')
      if (format(p_web.GSV('tmp:RRCCost2'),@n_14.2) > format(p_web.GSV('DefaultLabourCost'),@n_14.2))
          p_web.SSV('Comment:RCost2','Error! Cost is higher than Default Cost')
          p_web.SSV('Prompt:RCost0','') ! Blank the discount field.
      else
          p_web.SSV('Comment:RCost2','')
      end
  end ! 'tmp:RRCViewCostType'
  do updateRRCCost
  do pricingRoutine
  do displayRRCCostFields
  do Value::tmp:RRCCost2
  do SendAlert
  do Comment::tmp:RRCCost2 ! allows comment style to be updated.
  do Prompt::tmp:RRCCost2
  do Comment::tmp:RRCCost2
  do Value::tmp:RRCCost3  !1
  do Value::tmp:RRCCost4  !1
  do Value::tmp:RRCCost5  !1
  do Value::tmp:RRCCost6  !1
  do Value::tmp:RRCCost7  !1
  do Value::tmp:RRCCost8  !1
  do Prompt::tmp:RRCCost0
  do Value::tmp:RRCCost0  !1
  do Comment::tmp:RRCCost0

ValidateValue::tmp:RRCCost2  Routine
    If not (p_web.GSV('Prompt:RCost2') = '')
      if loc:invalid = '' then p_web.SetSessionValue('tmp:RRCCost2',tmp:RRCCost2).
    End

Value::tmp:RRCCost2  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Prompt:RCost2') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost2') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If p_web.GSV('ReadOnly:RRCCost2') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    tmp:RRCCost2 = p_web.RestoreValue('tmp:RRCCost2')
    do ValidateValue::tmp:RRCCost2
    If tmp:RRCCost2:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('Prompt:RCost2') = '')
  ! --- STRING --- tmp:RRCCost2
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:RRCCost2') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:RRCCost2'',''viewcosts_tmp:rrccost2_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:RRCCost2')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:RRCCost2',p_web.GetSessionValue('tmp:RRCCost2'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:RRCCost2  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:RRCCost2:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:RCost2'))
  loc:class = Choose(p_web.GSV('Prompt:RCost2') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost2') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Prompt:RCost2') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:RRCCost3  Routine
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost3') & '_prompt',Choose(p_web.GSV('Prompt:RCost3') = '','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Prompt:RCost3') = '','',p_web.Translate(p_web.GSV('Prompt:RCost3')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:RRCCost3  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:RRCCost3 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    tmp:RRCCost3 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::tmp:RRCCost3  ! copies value to session value if valid.
  do Value::tmp:RRCCost3
  do SendAlert
  do Comment::tmp:RRCCost3 ! allows comment style to be updated.

ValidateValue::tmp:RRCCost3  Routine
    If not (p_web.GSV('Prompt:RCost3') = '')
      if loc:invalid = '' then p_web.SetSessionValue('tmp:RRCCost3',tmp:RRCCost3).
    End

Value::tmp:RRCCost3  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Prompt:RCost3') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost3') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If p_web.GSV('ReadOnly:RRCCost3') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    tmp:RRCCost3 = p_web.RestoreValue('tmp:RRCCost3')
    do ValidateValue::tmp:RRCCost3
    If tmp:RRCCost3:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('Prompt:RCost3') = '')
  ! --- STRING --- tmp:RRCCost3
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:RRCCost3') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:RRCCost3'',''viewcosts_tmp:rrccost3_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:RRCCost3',p_web.GetSessionValue('tmp:RRCCost3'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:RRCCost3  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:RRCCost3:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('Prompt:RCost3') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost3') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Prompt:RCost3') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::BrowseJobCredits  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  Elsif true
    p_web.StoreValue('jov:RecordNumber')
  End
  do ValidateValue::BrowseJobCredits  ! copies value to session value if valid.
  do Comment::BrowseJobCredits ! allows comment style to be updated.

ValidateValue::BrowseJobCredits  Routine
    If not (p_web.GSV('tmp:RRCViewCostType') <> 'Credits')
    End

Value::BrowseJobCredits  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(p_web.GSV('tmp:RRCViewCostType') <> 'Credits',1,0))
  ! --- BROWSE ---  BrowseJobCredits --
  p_web.SetValue('BrowseJobCredits:NoForm',1)
  p_web.SetValue('BrowseJobCredits:FormName',loc:formname)
  p_web.SetValue('BrowseJobCredits:parentIs','Form')
  p_web.SetValue('_parentProc','ViewCosts')
  if p_web.RequestAjax = 0
    p_web.SSV('ViewCosts:_popup_',p_web.GetValue('_popup_')) ! stores the current browse popup state
    packet = clip(packet) & '<div id="'&lower('ViewCosts_BrowseJobCredits_embedded_div')&'"><!-- Net:BrowseJobCredits --></div><13,10>'
    do SendPacket
    p_web.DivHeader('ViewCosts_' & lower('BrowseJobCredits') & '_value')
    p_web.DivFooter()
  else
    if p_web.GetValue('_popup_') = 1
      p_web.SSV('ViewCosts:_popup_',1)
    elsif p_web.GSV('ViewCosts:_popup_') = 1
      p_web.SetValue('_popup_',1)
    end
    packet = clip(packet) & '<!-- Net:BrowseJobCredits --><13,10>'
  end
  do SendPacket
Comment::BrowseJobCredits  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if BrowseJobCredits:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('tmp:RRCViewCostType') <> 'Credits','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('BrowseJobCredits') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('tmp:RRCViewCostType') <> 'Credits'
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::__line4  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::__line4  ! copies value to session value if valid.
  do Comment::__line4 ! allows comment style to be updated.

ValidateValue::__line4  Routine
    If not (1=0)
    End

Value::__line4  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine('nt-width-100')
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('__line4') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & '<hr></hr><13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::__line4  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if __line4:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('__line4') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:RRCCost4  Routine
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost4') & '_prompt',Choose(p_web.GSV('Prompt:RCost4') = '','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Prompt:RCost4') = '','',p_web.Translate(p_web.GSV('Prompt:RCost4')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:RRCCost4  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:RRCCost4 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    tmp:RRCCost4 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::tmp:RRCCost4  ! copies value to session value if valid.
  do Value::tmp:RRCCost4
  do SendAlert
  do Comment::tmp:RRCCost4 ! allows comment style to be updated.

ValidateValue::tmp:RRCCost4  Routine
    If not (p_web.GSV('Prompt:RCost4') = '')
      if loc:invalid = '' then p_web.SetSessionValue('tmp:RRCCost4',tmp:RRCCost4).
    End

Value::tmp:RRCCost4  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Prompt:RCost4') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost4') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  If loc:retrying
    tmp:RRCCost4 = p_web.RestoreValue('tmp:RRCCost4')
    do ValidateValue::tmp:RRCCost4
    If tmp:RRCCost4:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('Prompt:RCost4') = '')
  ! --- STRING --- tmp:RRCCost4
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:RRCCost4'',''viewcosts_tmp:rrccost4_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:RRCCost4',p_web.GetSessionValue('tmp:RRCCost4'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:RRCCost4  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:RRCCost4:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('Prompt:RCost4') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost4') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Prompt:RCost4') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:RRCCost5  Routine
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost5') & '_prompt',Choose(p_web.GSV('Prompt:RCost5') = '','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Prompt:RCost5') = '','',p_web.Translate(p_web.GSV('Prompt:RCost5')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:RRCCost5  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:RRCCost5 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    tmp:RRCCost5 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::tmp:RRCCost5  ! copies value to session value if valid.
  do Value::tmp:RRCCost5
  do SendAlert
  do Comment::tmp:RRCCost5 ! allows comment style to be updated.

ValidateValue::tmp:RRCCost5  Routine
    If not (p_web.GSV('Prompt:RCost5') = '')
      if loc:invalid = '' then p_web.SetSessionValue('tmp:RRCCost5',tmp:RRCCost5).
    End

Value::tmp:RRCCost5  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Prompt:RCost5') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost5') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  If loc:retrying
    tmp:RRCCost5 = p_web.RestoreValue('tmp:RRCCost5')
    do ValidateValue::tmp:RRCCost5
    If tmp:RRCCost5:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('Prompt:RCost5') = '')
  ! --- STRING --- tmp:RRCCost5
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:RRCCost5'',''viewcosts_tmp:rrccost5_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:RRCCost5',p_web.GetSessionValue('tmp:RRCCost5'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:RRCCost5  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:RRCCost5:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('Prompt:RCost5') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost5') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Prompt:RCost5') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:RRCCost6  Routine
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost6') & '_prompt',Choose(p_web.GSV('Prompt:RCost6') = '','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Prompt:RCost6') = '','',p_web.Translate(p_web.GSV('Prompt:RCost6')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:RRCCost6  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:RRCCost6 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    tmp:RRCCost6 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::tmp:RRCCost6  ! copies value to session value if valid.
  do Value::tmp:RRCCost6
  do SendAlert
  do Comment::tmp:RRCCost6 ! allows comment style to be updated.

ValidateValue::tmp:RRCCost6  Routine
    If not (p_web.GSV('Prompt:RCost6') = '')
      if loc:invalid = '' then p_web.SetSessionValue('tmp:RRCCost6',tmp:RRCCost6).
    End

Value::tmp:RRCCost6  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Prompt:RCost6') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost6') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  If loc:retrying
    tmp:RRCCost6 = p_web.RestoreValue('tmp:RRCCost6')
    do ValidateValue::tmp:RRCCost6
    If tmp:RRCCost6:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('Prompt:RCost6') = '')
  ! --- STRING --- tmp:RRCCost6
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:RRCCost6'',''viewcosts_tmp:rrccost6_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:RRCCost6',p_web.GetSessionValue('tmp:RRCCost6'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:RRCCost6  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:RRCCost6:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('Prompt:RCost6') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost6') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Prompt:RCost6') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:RRCCost7  Routine
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost7') & '_prompt',Choose(p_web.GSV('Prompt:RCost7') = '','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Prompt:RCost7') = '','',p_web.Translate(p_web.GSV('Prompt:RCost7')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:RRCCost7  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:RRCCost7 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    tmp:RRCCost7 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::tmp:RRCCost7  ! copies value to session value if valid.
  do Value::tmp:RRCCost7
  do SendAlert
  do Comment::tmp:RRCCost7 ! allows comment style to be updated.

ValidateValue::tmp:RRCCost7  Routine
    If not (p_web.GSV('Prompt:RCost7') = '')
      if loc:invalid = '' then p_web.SetSessionValue('tmp:RRCCost7',tmp:RRCCost7).
    End

Value::tmp:RRCCost7  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Prompt:RCost7') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost7') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  If loc:retrying
    tmp:RRCCost7 = p_web.RestoreValue('tmp:RRCCost7')
    do ValidateValue::tmp:RRCCost7
    If tmp:RRCCost7:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('Prompt:RCost7') = '')
  ! --- STRING --- tmp:RRCCost7
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:RRCCost7'',''viewcosts_tmp:rrccost7_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:RRCCost7',p_web.GetSessionValue('tmp:RRCCost7'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:RRCCost7  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:RRCCost7:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('Prompt:RCost7') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost7') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Prompt:RCost7') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:RRCCost8  Routine
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost8') & '_prompt',Choose(p_web.GSV('Prompt:RCost8') = '','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Prompt:RCost8') = '','',p_web.Translate(p_web.GSV('Prompt:RCost8')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:RRCCost8  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:RRCCost8 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    tmp:RRCCost8 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::tmp:RRCCost8  ! copies value to session value if valid.
  do Value::tmp:RRCCost8
  do SendAlert
  do Comment::tmp:RRCCost8 ! allows comment style to be updated.

ValidateValue::tmp:RRCCost8  Routine
    If not (p_web.GSV('Prompt:RCost8') = '')
      if loc:invalid = '' then p_web.SetSessionValue('tmp:RRCCost8',tmp:RRCCost8).
    End

Value::tmp:RRCCost8  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Prompt:RCost8') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost8') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  If loc:retrying
    tmp:RRCCost8 = p_web.RestoreValue('tmp:RRCCost8')
    do ValidateValue::tmp:RRCCost8
    If tmp:RRCCost8:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('Prompt:RCost8') = '')
  ! --- STRING --- tmp:RRCCost8
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:RRCCost8'',''viewcosts_tmp:rrccost8_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:RRCCost8',p_web.GetSessionValue('tmp:RRCCost8'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:RRCCost8  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:RRCCost8:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('Prompt:RCost8') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost8') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Prompt:RCost8') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::jobe:ExcReplcamentCharge  Routine
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('jobe:ExcReplcamentCharge') & '_prompt',Choose(p_web.GSV('Hide:ExchangeReplacement') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:ExchangeReplacement') = 1,'',p_web.Translate('Exchange Replacement'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jobe:ExcReplcamentCharge  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jobe:ExcReplcamentCharge = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    jobe:ExcReplcamentCharge = p_web.GetValue('Value')
  End
  do ValidateValue::jobe:ExcReplcamentCharge  ! copies value to session value if valid.
  do Value::jobe:ExcReplcamentCharge
  do SendAlert
  do Comment::jobe:ExcReplcamentCharge ! allows comment style to be updated.

ValidateValue::jobe:ExcReplcamentCharge  Routine
    If not (p_web.GSV('Hide:ExchangeReplacement') = 1)
      if loc:invalid = '' then p_web.SetSessionValue('jobe:ExcReplcamentCharge',jobe:ExcReplcamentCharge).
    End

Value::jobe:ExcReplcamentCharge  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:ExchangeReplacement') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('jobe:ExcReplcamentCharge') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If loc:retrying
    jobe:ExcReplcamentCharge = p_web.RestoreValue('jobe:ExcReplcamentCharge')
    do ValidateValue::jobe:ExcReplcamentCharge
    If jobe:ExcReplcamentCharge:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:ExchangeReplacement') = 1)
  ! --- CHECKBOX --- jobe:ExcReplcamentCharge
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''jobe:ExcReplcamentCharge'',''viewcosts_jobe:excreplcamentcharge_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('jobe:ExcReplcamentCharge') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','jobe:ExcReplcamentCharge',clip(1),,loc:readonly,,,loc:javascript,,'Exchange Replacement Charge') & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::jobe:ExcReplcamentCharge  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if jobe:ExcReplcamentCharge:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:ExchangeReplacement') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('jobe:ExcReplcamentCharge') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:ExchangeReplacement') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('ViewCosts_nexttab_' & 0)
    tmp:ARCViewCostType = p_web.GSV('tmp:ARCViewCostType')
    do ValidateValue::tmp:ARCViewCostType
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:ARCViewCostType
      !do SendAlert
      do Comment::tmp:ARCViewCostType ! allows comment style to be updated.
      !exit
    End
    tmp:ARCIgnoreDefaultCharges = p_web.GSV('tmp:ARCIgnoreDefaultCharges')
    do ValidateValue::tmp:ARCIgnoreDefaultCharges
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:ARCIgnoreDefaultCharges
      !do SendAlert
      do Comment::tmp:ARCIgnoreDefaultCharges ! allows comment style to be updated.
      !exit
    End
    tmp:ARCIgnoreReason = p_web.GSV('tmp:ARCIgnoreReason')
    do ValidateValue::tmp:ARCIgnoreReason
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:ARCIgnoreReason
      !do SendAlert
      do Comment::tmp:ARCIgnoreReason ! allows comment style to be updated.
      !exit
    End
    tmp:ARCCost1 = p_web.GSV('tmp:ARCCost1')
    do ValidateValue::tmp:ARCCost1
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:ARCCost1
      !do SendAlert
      do Comment::tmp:ARCCost1 ! allows comment style to be updated.
      !exit
    End
    tmp:AdjustmentCost1 = p_web.GSV('tmp:AdjustmentCost1')
    do ValidateValue::tmp:AdjustmentCost1
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:AdjustmentCost1
      !do SendAlert
      do Comment::tmp:AdjustmentCost1 ! allows comment style to be updated.
      !exit
    End
    tmp:ARCCost2 = p_web.GSV('tmp:ARCCost2')
    do ValidateValue::tmp:ARCCost2
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:ARCCost2
      !do SendAlert
      do Comment::tmp:ARCCost2 ! allows comment style to be updated.
      !exit
    End
    tmp:AdjustmentCost2 = p_web.GSV('tmp:AdjustmentCost2')
    do ValidateValue::tmp:AdjustmentCost2
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:AdjustmentCost2
      !do SendAlert
      do Comment::tmp:AdjustmentCost2 ! allows comment style to be updated.
      !exit
    End
    tmp:ARCCost3 = p_web.GSV('tmp:ARCCost3')
    do ValidateValue::tmp:ARCCost3
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:ARCCost3
      !do SendAlert
      do Comment::tmp:ARCCost3 ! allows comment style to be updated.
      !exit
    End
    tmp:AdjustmentCost3 = p_web.GSV('tmp:AdjustmentCost3')
    do ValidateValue::tmp:AdjustmentCost3
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:AdjustmentCost3
      !do SendAlert
      do Comment::tmp:AdjustmentCost3 ! allows comment style to be updated.
      !exit
    End
    tmp:ARCCost4 = p_web.GSV('tmp:ARCCost4')
    do ValidateValue::tmp:ARCCost4
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:ARCCost4
      !do SendAlert
      do Comment::tmp:ARCCost4 ! allows comment style to be updated.
      !exit
    End
    tmp:AdjustmentCost4 = p_web.GSV('tmp:AdjustmentCost4')
    do ValidateValue::tmp:AdjustmentCost4
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:AdjustmentCost4
      !do SendAlert
      do Comment::tmp:AdjustmentCost4 ! allows comment style to be updated.
      !exit
    End
    tmp:ARCCost5 = p_web.GSV('tmp:ARCCost5')
    do ValidateValue::tmp:ARCCost5
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:ARCCost5
      !do SendAlert
      do Comment::tmp:ARCCost5 ! allows comment style to be updated.
      !exit
    End
    tmp:AdjustmentCost5 = p_web.GSV('tmp:AdjustmentCost5')
    do ValidateValue::tmp:AdjustmentCost5
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:AdjustmentCost5
      !do SendAlert
      do Comment::tmp:AdjustmentCost5 ! allows comment style to be updated.
      !exit
    End
    tmp:ARCCost6 = p_web.GSV('tmp:ARCCost6')
    do ValidateValue::tmp:ARCCost6
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:ARCCost6
      !do SendAlert
      do Comment::tmp:ARCCost6 ! allows comment style to be updated.
      !exit
    End
    tmp:AdjustmentCost6 = p_web.GSV('tmp:AdjustmentCost6')
    do ValidateValue::tmp:AdjustmentCost6
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:AdjustmentCost6
      !do SendAlert
      do Comment::tmp:AdjustmentCost6 ! allows comment style to be updated.
      !exit
    End
    tmp:ARCCost7 = p_web.GSV('tmp:ARCCost7')
    do ValidateValue::tmp:ARCCost7
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:ARCCost7
      !do SendAlert
      do Comment::tmp:ARCCost7 ! allows comment style to be updated.
      !exit
    End
    tmp:ARCCost8 = p_web.GSV('tmp:ARCCost8')
    do ValidateValue::tmp:ARCCost8
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:ARCCost8
      !do SendAlert
      do Comment::tmp:ARCCost8 ! allows comment style to be updated.
      !exit
    End
    jobe:ARC3rdPartyInvoiceNumber = p_web.GSV('jobe:ARC3rdPartyInvoiceNumber')
    do ValidateValue::jobe:ARC3rdPartyInvoiceNumber
    If loc:Invalid
      loc:retrying = 1
      do Value::jobe:ARC3rdPartyInvoiceNumber
      !do SendAlert
      do Comment::jobe:ARC3rdPartyInvoiceNumber ! allows comment style to be updated.
      !exit
    End
    tmp:ARCInvoiceNumber = p_web.GSV('tmp:ARCInvoiceNumber')
    do ValidateValue::tmp:ARCInvoiceNumber
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:ARCInvoiceNumber
      !do SendAlert
      do Comment::tmp:ARCInvoiceNumber ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('ViewCosts_nexttab_' & 1)
    tmp:RRCViewCostType = p_web.GSV('tmp:RRCViewCostType')
    do ValidateValue::tmp:RRCViewCostType
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:RRCViewCostType
      !do SendAlert
      do Comment::tmp:RRCViewCostType ! allows comment style to be updated.
      !exit
    End
    tmp:RRCIgnoreDefaultCharges = p_web.GSV('tmp:RRCIgnoreDefaultCharges')
    do ValidateValue::tmp:RRCIgnoreDefaultCharges
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:RRCIgnoreDefaultCharges
      !do SendAlert
      do Comment::tmp:RRCIgnoreDefaultCharges ! allows comment style to be updated.
      !exit
    End
    tmp:RRCIgnoreReason = p_web.GSV('tmp:RRCIgnoreReason')
    do ValidateValue::tmp:RRCIgnoreReason
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:RRCIgnoreReason
      !do SendAlert
      do Comment::tmp:RRCIgnoreReason ! allows comment style to be updated.
      !exit
    End
    tmp:RRCCost0 = p_web.GSV('tmp:RRCCost0')
    do ValidateValue::tmp:RRCCost0
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:RRCCost0
      !do SendAlert
      do Comment::tmp:RRCCost0 ! allows comment style to be updated.
      !exit
    End
    tmp:RRCCost1 = p_web.GSV('tmp:RRCCost1')
    do ValidateValue::tmp:RRCCost1
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:RRCCost1
      !do SendAlert
      do Comment::tmp:RRCCost1 ! allows comment style to be updated.
      !exit
    End
    tmp:originalInvoice = p_web.GSV('tmp:originalInvoice')
    do ValidateValue::tmp:originalInvoice
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:originalInvoice
      !do SendAlert
      do Comment::tmp:originalInvoice ! allows comment style to be updated.
      !exit
    End
    tmp:RRCCost2 = p_web.GSV('tmp:RRCCost2')
    do ValidateValue::tmp:RRCCost2
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:RRCCost2
      !do SendAlert
      do Comment::tmp:RRCCost2 ! allows comment style to be updated.
      !exit
    End
    tmp:RRCCost3 = p_web.GSV('tmp:RRCCost3')
    do ValidateValue::tmp:RRCCost3
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:RRCCost3
      !do SendAlert
      do Comment::tmp:RRCCost3 ! allows comment style to be updated.
      !exit
    End
    tmp:RRCCost4 = p_web.GSV('tmp:RRCCost4')
    do ValidateValue::tmp:RRCCost4
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:RRCCost4
      !do SendAlert
      do Comment::tmp:RRCCost4 ! allows comment style to be updated.
      !exit
    End
    tmp:RRCCost5 = p_web.GSV('tmp:RRCCost5')
    do ValidateValue::tmp:RRCCost5
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:RRCCost5
      !do SendAlert
      do Comment::tmp:RRCCost5 ! allows comment style to be updated.
      !exit
    End
    tmp:RRCCost6 = p_web.GSV('tmp:RRCCost6')
    do ValidateValue::tmp:RRCCost6
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:RRCCost6
      !do SendAlert
      do Comment::tmp:RRCCost6 ! allows comment style to be updated.
      !exit
    End
    tmp:RRCCost7 = p_web.GSV('tmp:RRCCost7')
    do ValidateValue::tmp:RRCCost7
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:RRCCost7
      !do SendAlert
      do Comment::tmp:RRCCost7 ! allows comment style to be updated.
      !exit
    End
    tmp:RRCCost8 = p_web.GSV('tmp:RRCCost8')
    do ValidateValue::tmp:RRCCost8
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:RRCCost8
      !do SendAlert
      do Comment::tmp:RRCCost8 ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('ViewCosts_nexttab_' & 2)
    jobe:ExcReplcamentCharge = p_web.GSV('jobe:ExcReplcamentCharge')
    do ValidateValue::jobe:ExcReplcamentCharge
    If loc:Invalid
      loc:retrying = 1
      do Value::jobe:ExcReplcamentCharge
      !do SendAlert
      do Comment::jobe:ExcReplcamentCharge ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_ViewCosts_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('ViewCosts_tab_' & 0)
    do GenerateTab0
  of lower('ViewCosts_tmp:ARCViewCostType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ARCViewCostType
      of event:timer
        do Value::tmp:ARCViewCostType
        do Comment::tmp:ARCViewCostType
      else
        do Value::tmp:ARCViewCostType
      end
  of lower('ViewCosts_tmp:ARCIgnoreDefaultCharges_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ARCIgnoreDefaultCharges
      of event:timer
        do Value::tmp:ARCIgnoreDefaultCharges
        do Comment::tmp:ARCIgnoreDefaultCharges
      else
        do Value::tmp:ARCIgnoreDefaultCharges
      end
  of lower('ViewCosts_tmp:ARCIgnoreReason_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ARCIgnoreReason
      of event:timer
        do Value::tmp:ARCIgnoreReason
        do Comment::tmp:ARCIgnoreReason
      else
        do Value::tmp:ARCIgnoreReason
      end
  of lower('ViewCosts_buttonAcceptARCReason_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonAcceptARCReason
      of event:timer
        do Value::buttonAcceptARCReason
        do Comment::buttonAcceptARCReason
      else
        do Value::buttonAcceptARCReason
      end
  of lower('ViewCosts_buttonCancelARCReason_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonCancelARCReason
      of event:timer
        do Value::buttonCancelARCReason
        do Comment::buttonCancelARCReason
      else
        do Value::buttonCancelARCReason
      end
  of lower('ViewCosts_tmp:ARCCost1_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ARCCost1
      of event:timer
        do Value::tmp:ARCCost1
        do Comment::tmp:ARCCost1
      else
        do Value::tmp:ARCCost1
      end
  of lower('ViewCosts_tmp:AdjustmentCost1_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:AdjustmentCost1
      of event:timer
        do Value::tmp:AdjustmentCost1
        do Comment::tmp:AdjustmentCost1
      else
        do Value::tmp:AdjustmentCost1
      end
  of lower('ViewCosts_tmp:ARCCost2_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ARCCost2
      of event:timer
        do Value::tmp:ARCCost2
        do Comment::tmp:ARCCost2
      else
        do Value::tmp:ARCCost2
      end
  of lower('ViewCosts_tmp:AdjustmentCost2_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:AdjustmentCost2
      of event:timer
        do Value::tmp:AdjustmentCost2
        do Comment::tmp:AdjustmentCost2
      else
        do Value::tmp:AdjustmentCost2
      end
  of lower('ViewCosts_tmp:ARCCost3_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ARCCost3
      of event:timer
        do Value::tmp:ARCCost3
        do Comment::tmp:ARCCost3
      else
        do Value::tmp:ARCCost3
      end
  of lower('ViewCosts_tmp:AdjustmentCost3_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:AdjustmentCost3
      of event:timer
        do Value::tmp:AdjustmentCost3
        do Comment::tmp:AdjustmentCost3
      else
        do Value::tmp:AdjustmentCost3
      end
  of lower('ViewCosts_tmp:ARCCost4_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ARCCost4
      of event:timer
        do Value::tmp:ARCCost4
        do Comment::tmp:ARCCost4
      else
        do Value::tmp:ARCCost4
      end
  of lower('ViewCosts_tmp:AdjustmentCost4_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:AdjustmentCost4
      of event:timer
        do Value::tmp:AdjustmentCost4
        do Comment::tmp:AdjustmentCost4
      else
        do Value::tmp:AdjustmentCost4
      end
  of lower('ViewCosts_tmp:ARCCost5_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ARCCost5
      of event:timer
        do Value::tmp:ARCCost5
        do Comment::tmp:ARCCost5
      else
        do Value::tmp:ARCCost5
      end
  of lower('ViewCosts_tmp:AdjustmentCost5_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:AdjustmentCost5
      of event:timer
        do Value::tmp:AdjustmentCost5
        do Comment::tmp:AdjustmentCost5
      else
        do Value::tmp:AdjustmentCost5
      end
  of lower('ViewCosts_tmp:ARCCost6_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ARCCost6
      of event:timer
        do Value::tmp:ARCCost6
        do Comment::tmp:ARCCost6
      else
        do Value::tmp:ARCCost6
      end
  of lower('ViewCosts_tmp:AdjustmentCost6_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:AdjustmentCost6
      of event:timer
        do Value::tmp:AdjustmentCost6
        do Comment::tmp:AdjustmentCost6
      else
        do Value::tmp:AdjustmentCost6
      end
  of lower('ViewCosts_tmp:ARCCost7_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ARCCost7
      of event:timer
        do Value::tmp:ARCCost7
        do Comment::tmp:ARCCost7
      else
        do Value::tmp:ARCCost7
      end
  of lower('ViewCosts_tmp:ARCCost8_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ARCCost8
      of event:timer
        do Value::tmp:ARCCost8
        do Comment::tmp:ARCCost8
      else
        do Value::tmp:ARCCost8
      end
  of lower('ViewCosts_tab_' & 1)
    do GenerateTab1
  of lower('ViewCosts_tmp:RRCViewCostType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:RRCViewCostType
      of event:timer
        do Value::tmp:RRCViewCostType
        do Comment::tmp:RRCViewCostType
      else
        do Value::tmp:RRCViewCostType
      end
  of lower('ViewCosts_tmp:RRCIgnoreDefaultCharges_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:RRCIgnoreDefaultCharges
      of event:timer
        do Value::tmp:RRCIgnoreDefaultCharges
        do Comment::tmp:RRCIgnoreDefaultCharges
      else
        do Value::tmp:RRCIgnoreDefaultCharges
      end
  of lower('ViewCosts_tmp:RRCIgnoreReason_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:RRCIgnoreReason
      of event:timer
        do Value::tmp:RRCIgnoreReason
        do Comment::tmp:RRCIgnoreReason
      else
        do Value::tmp:RRCIgnoreReason
      end
  of lower('ViewCosts_buttonAcceptRRCReason_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonAcceptRRCReason
      of event:timer
        do Value::buttonAcceptRRCReason
        do Comment::buttonAcceptRRCReason
      else
        do Value::buttonAcceptRRCReason
      end
  of lower('ViewCosts_buttonCancelRRCReason_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonCancelRRCReason
      of event:timer
        do Value::buttonCancelRRCReason
        do Comment::buttonCancelRRCReason
      else
        do Value::buttonCancelRRCReason
      end
  of lower('ViewCosts_tmp:RRCCost0_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:RRCCost0
      of event:timer
        do Value::tmp:RRCCost0
        do Comment::tmp:RRCCost0
      else
        do Value::tmp:RRCCost0
      end
  of lower('ViewCosts_tmp:RRCCost1_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:RRCCost1
      of event:timer
        do Value::tmp:RRCCost1
        do Comment::tmp:RRCCost1
      else
        do Value::tmp:RRCCost1
      end
  of lower('ViewCosts_tmp:originalInvoice_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:originalInvoice
      of event:timer
        do Value::tmp:originalInvoice
        do Comment::tmp:originalInvoice
      else
        do Value::tmp:originalInvoice
      end
  of lower('ViewCosts_tmp:RRCCost2_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:RRCCost2
      of event:timer
        do Value::tmp:RRCCost2
        do Comment::tmp:RRCCost2
      else
        do Value::tmp:RRCCost2
      end
  of lower('ViewCosts_tmp:RRCCost3_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:RRCCost3
      of event:timer
        do Value::tmp:RRCCost3
        do Comment::tmp:RRCCost3
      else
        do Value::tmp:RRCCost3
      end
  of lower('ViewCosts_tmp:RRCCost4_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:RRCCost4
      of event:timer
        do Value::tmp:RRCCost4
        do Comment::tmp:RRCCost4
      else
        do Value::tmp:RRCCost4
      end
  of lower('ViewCosts_tmp:RRCCost5_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:RRCCost5
      of event:timer
        do Value::tmp:RRCCost5
        do Comment::tmp:RRCCost5
      else
        do Value::tmp:RRCCost5
      end
  of lower('ViewCosts_tmp:RRCCost6_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:RRCCost6
      of event:timer
        do Value::tmp:RRCCost6
        do Comment::tmp:RRCCost6
      else
        do Value::tmp:RRCCost6
      end
  of lower('ViewCosts_tmp:RRCCost7_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:RRCCost7
      of event:timer
        do Value::tmp:RRCCost7
        do Comment::tmp:RRCCost7
      else
        do Value::tmp:RRCCost7
      end
  of lower('ViewCosts_tmp:RRCCost8_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:RRCCost8
      of event:timer
        do Value::tmp:RRCCost8
        do Comment::tmp:RRCCost8
      else
        do Value::tmp:RRCCost8
      end
  of lower('ViewCosts_tab_' & 2)
    do GenerateTab2
  of lower('ViewCosts_jobe:ExcReplcamentCharge_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe:ExcReplcamentCharge
      of event:timer
        do Value::jobe:ExcReplcamentCharge
        do Comment::jobe:ExcReplcamentCharge
      else
        do Value::jobe:ExcReplcamentCharge
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('ViewCosts_form:ready_',1)

  p_web.SetSessionValue('ViewCosts_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_ViewCosts',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('ViewCosts_form:ready_',1)
  p_web.SetSessionValue('ViewCosts_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_ViewCosts',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('ViewCosts_form:ready_',1)
  p_web.SetSessionValue('ViewCosts_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('ViewCosts:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('ViewCosts_form:ready_',1)
  p_web.SetSessionValue('ViewCosts_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('ViewCosts:Primed',0)
  p_web.setsessionvalue('showtab_ViewCosts',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
  If p_web.GSV('Hide:ARCCosts') <> 1
      If not (1=0)
          If p_web.IfExistsValue('tmp:ARCViewCostType')
            tmp:ARCViewCostType = p_web.GetValue('tmp:ARCViewCostType')
          End
      End
      If not (p_web.GSV('tmp:ARCViewCostType') = 'Manufacturer Payment' or p_web.GSV('tmp:ARCViewCostType') = '')
        If not (p_web.GSV('BookingSite') = 'RRC' Or Instring('- Invoiced',p_web.GSV('tmp:ARCViewCostType'),1,1) or p_web.GSV('ValidateIgnoreTickBoxARC') = 1)
          If p_web.IfExistsValue('tmp:ARCIgnoreDefaultCharges') = 0
            p_web.SetValue('tmp:ARCIgnoreDefaultCharges',0)
            tmp:ARCIgnoreDefaultCharges = 0
          Else
            tmp:ARCIgnoreDefaultCharges = p_web.GetValue('tmp:ARCIgnoreDefaultCharges')
          End
        End
      End
      If not (p_web.GSV('ValidateIgnoreTickBoxARC') <> 1)
          If p_web.IfExistsValue('tmp:ARCIgnoreReason')
            tmp:ARCIgnoreReason = p_web.GetValue('tmp:ARCIgnoreReason')
          End
      End
      If not (p_web.GSV('Prompt:Cost1') = '')
        If not (p_web.GSV('BookingSite') = 'RRC' Or p_web.GSV('ReadOnly:ARCCost1') = 1)
          If p_web.IfExistsValue('tmp:ARCCost1')
            tmp:ARCCost1 = p_web.GetValue('tmp:ARCCost1')
          End
        End
      End
      If not (p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced')
          If p_web.IfExistsValue('tmp:AdjustmentCost1')
            tmp:AdjustmentCost1 = p_web.GetValue('tmp:AdjustmentCost1')
          End
      End
      If not (p_web.GSV('Prompt:Cost2') = '')
        If not (p_web.GSV('BookingSite') = 'RRC' Or p_web.GSV('ReadOnly:ARCCost2') = 1)
          If p_web.IfExistsValue('tmp:ARCCost2')
            tmp:ARCCost2 = p_web.GetValue('tmp:ARCCost2')
          End
        End
      End
      If not (p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced')
          If p_web.IfExistsValue('tmp:AdjustmentCost2')
            tmp:AdjustmentCost2 = p_web.GetValue('tmp:AdjustmentCost2')
          End
      End
      If not (p_web.GSV('Prompt:Cost3') = '')
        If not (p_web.GSV('BookingSite') = 'RRC' Or p_web.GSV('ReadOnly:ARCCost3') = 1)
          If p_web.IfExistsValue('tmp:ARCCost3')
            tmp:ARCCost3 = p_web.GetValue('tmp:ARCCost3')
          End
        End
      End
      If not (p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced')
          If p_web.IfExistsValue('tmp:AdjustmentCost3')
            tmp:AdjustmentCost3 = p_web.GetValue('tmp:AdjustmentCost3')
          End
      End
      If not (p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced')
          If p_web.IfExistsValue('tmp:AdjustmentCost4')
            tmp:AdjustmentCost4 = p_web.GetValue('tmp:AdjustmentCost4')
          End
      End
      If not (p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced')
          If p_web.IfExistsValue('tmp:AdjustmentCost5')
            tmp:AdjustmentCost5 = p_web.GetValue('tmp:AdjustmentCost5')
          End
      End
      If not (p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced')
          If p_web.IfExistsValue('tmp:AdjustmentCost6')
            tmp:AdjustmentCost6 = p_web.GetValue('tmp:AdjustmentCost6')
          End
      End
  End
  If p_web.GSV('Hide:RRCCosts') <> 1
      If not (1=0)
        If not (p_web.GSV('ValidateIgnoreTickBox') = 1)
          If p_web.IfExistsValue('tmp:RRCViewCostType')
            tmp:RRCViewCostType = p_web.GetValue('tmp:RRCViewCostType')
          End
        End
      End
      If not (p_web.GSV('tmp:RRCViewCostType') = 'Credits' Or p_web.GSV('tmp:RRCViewCostType') = 'Handling' Or p_web.GSV('tmp:RRCViewCostType') = 'Exchange' Or p_web.GSV('tmp:RRCViewCostType') = '')
        If not (p_web.GSV('ValidateIgnoreTickBox') = 1 Or Instring('- Invoiced',p_web.GSV('tmp:RRCViewCostType'),1,1))
          If p_web.IfExistsValue('tmp:RRCIgnoreDefaultCharges') = 0
            p_web.SetValue('tmp:RRCIgnoreDefaultCharges',false)
            tmp:RRCIgnoreDefaultCharges = false
          Else
            tmp:RRCIgnoreDefaultCharges = p_web.GetValue('tmp:RRCIgnoreDefaultCharges')
          End
        End
      End
      If not (p_web.GSV('ValidateIgnoreTickBox') <> 1)
          If p_web.IfExistsValue('tmp:RRCIgnoreReason')
            tmp:RRCIgnoreReason = p_web.GetValue('tmp:RRCIgnoreReason')
          End
      End
      If not (p_web.GSV('Prompt:RCost1') = '')
        If not (p_web.GSV('ReadOnly:RRCCost1') = 1)
          If p_web.IfExistsValue('tmp:RRCCost1')
            tmp:RRCCost1 = p_web.GetValue('tmp:RRCCost1')
          End
        End
      End
      If not (p_web.GSV('Prompt:RCost2') = '')
        If not (p_web.GSV('ReadOnly:RRCCost2') = 1)
          If p_web.IfExistsValue('tmp:RRCCost2')
            tmp:RRCCost2 = p_web.GetValue('tmp:RRCCost2')
          End
        End
      End
      If not (p_web.GSV('Prompt:RCost3') = '')
        If not (p_web.GSV('ReadOnly:RRCCost3') = 1)
          If p_web.IfExistsValue('tmp:RRCCost3')
            tmp:RRCCost3 = p_web.GetValue('tmp:RRCCost3')
          End
        End
      End
  End
      If not (p_web.GSV('Hide:ExchangeReplacement') = 1)
          If p_web.IfExistsValue('jobe:ExcReplcamentCharge') = 0
            p_web.SetValue('jobe:ExcReplcamentCharge',0)
            jobe:ExcReplcamentCharge = 0
          Else
            jobe:ExcReplcamentCharge = p_web.GetValue('jobe:ExcReplcamentCharge')
          End
      End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  if (p_web.GSV('tmp:RRCViewCostType') = 'Chargeable')
      if (format(p_web.GSV('tmp:RRCCost2'),@n_14.2) > format(p_web.GSV('DefaultLabourCost'),@n_14.2))
          loc:Alert = 'Error! Labour Cost is higher than Default Cost'
          loc:Invalid = 'tmp:RRCCost2'
          exit
      end
  end ! 'tmp:RRCViewCostType'
  do addToAudit
  Do DeleteSessionValues
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('ViewCosts_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('ViewCosts_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
  If p_web.GSV('Hide:ARCCosts') <> 1
    loc:InvalidTab += 1
    do ValidateValue::tmp:ARCViewCostType
    If loc:Invalid then exit.
    do ValidateValue::__line1
    If loc:Invalid then exit.
    do ValidateValue::tmp:ARCIgnoreDefaultCharges
    If loc:Invalid then exit.
    do ValidateValue::tmp:ARCIgnoreReason
    If loc:Invalid then exit.
    do ValidateValue::buttonAcceptARCReason
    If loc:Invalid then exit.
    do ValidateValue::buttonCancelARCReason
    If loc:Invalid then exit.
    do ValidateValue::tmp:ARCCost1
    If loc:Invalid then exit.
    do ValidateValue::tmp:AdjustmentCost1
    If loc:Invalid then exit.
    do ValidateValue::tmp:ARCCost2
    If loc:Invalid then exit.
    do ValidateValue::tmp:AdjustmentCost2
    If loc:Invalid then exit.
    do ValidateValue::tmp:ARCCost3
    If loc:Invalid then exit.
    do ValidateValue::tmp:AdjustmentCost3
    If loc:Invalid then exit.
    do ValidateValue::__line2
    If loc:Invalid then exit.
    do ValidateValue::tmp:ARCCost4
    If loc:Invalid then exit.
    do ValidateValue::tmp:AdjustmentCost4
    If loc:Invalid then exit.
    do ValidateValue::tmp:ARCCost5
    If loc:Invalid then exit.
    do ValidateValue::tmp:AdjustmentCost5
    If loc:Invalid then exit.
    do ValidateValue::tmp:ARCCost6
    If loc:Invalid then exit.
    do ValidateValue::tmp:AdjustmentCost6
    If loc:Invalid then exit.
    do ValidateValue::tmp:ARCCost7
    If loc:Invalid then exit.
    do ValidateValue::tmp:ARCCost8
    If loc:Invalid then exit.
    do ValidateValue::jobe:ARC3rdPartyInvoiceNumber
    If loc:Invalid then exit.
    do ValidateValue::tmp:ARCInvoiceNumber
    If loc:Invalid then exit.
  End
  ! tab = 2
  If p_web.GSV('Hide:RRCCosts') <> 1
    loc:InvalidTab += 1
    do ValidateValue::tmp:RRCViewCostType
    If loc:Invalid then exit.
    do ValidateValue::__line3
    If loc:Invalid then exit.
    do ValidateValue::tmp:RRCIgnoreDefaultCharges
    If loc:Invalid then exit.
    do ValidateValue::tmp:RRCIgnoreReason
    If loc:Invalid then exit.
    do ValidateValue::buttonAcceptRRCReason
    If loc:Invalid then exit.
    do ValidateValue::buttonCancelRRCReason
    If loc:Invalid then exit.
    do ValidateValue::tmp:RRCCost0
    If loc:Invalid then exit.
    do ValidateValue::tmp:RRCCost1
    If loc:Invalid then exit.
    do ValidateValue::tmp:originalInvoice
    If loc:Invalid then exit.
    do ValidateValue::tmp:RRCCost2
    If loc:Invalid then exit.
    do ValidateValue::tmp:RRCCost3
    If loc:Invalid then exit.
    do ValidateValue::BrowseJobCredits
    If loc:Invalid then exit.
    do ValidateValue::__line4
    If loc:Invalid then exit.
    do ValidateValue::tmp:RRCCost4
    If loc:Invalid then exit.
    do ValidateValue::tmp:RRCCost5
    If loc:Invalid then exit.
    do ValidateValue::tmp:RRCCost6
    If loc:Invalid then exit.
    do ValidateValue::tmp:RRCCost7
    If loc:Invalid then exit.
    do ValidateValue::tmp:RRCCost8
    If loc:Invalid then exit.
  End
  ! tab = 3
    loc:InvalidTab += 1
    do ValidateValue::jobe:ExcReplcamentCharge
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('ViewCosts:Primed',0)
  p_web.StoreValue('tmp:ARCViewCostType')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:ARCIgnoreDefaultCharges')
  p_web.StoreValue('tmp:ARCIgnoreReason')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:ARCCost1')
  p_web.StoreValue('tmp:AdjustmentCost1')
  p_web.StoreValue('tmp:ARCCost2')
  p_web.StoreValue('tmp:AdjustmentCost2')
  p_web.StoreValue('tmp:ARCCost3')
  p_web.StoreValue('tmp:AdjustmentCost3')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:ARCCost4')
  p_web.StoreValue('tmp:AdjustmentCost4')
  p_web.StoreValue('tmp:ARCCost5')
  p_web.StoreValue('tmp:AdjustmentCost5')
  p_web.StoreValue('tmp:ARCCost6')
  p_web.StoreValue('tmp:AdjustmentCost6')
  p_web.StoreValue('tmp:ARCCost7')
  p_web.StoreValue('tmp:ARCCost8')
  p_web.StoreValue('tmp:ARCInvoiceNumber')
  p_web.StoreValue('tmp:RRCViewCostType')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:RRCIgnoreDefaultCharges')
  p_web.StoreValue('tmp:RRCIgnoreReason')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:RRCCost0')
  p_web.StoreValue('tmp:RRCCost1')
  p_web.StoreValue('tmp:originalInvoice')
  p_web.StoreValue('tmp:RRCCost2')
  p_web.StoreValue('tmp:RRCCost3')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:RRCCost4')
  p_web.StoreValue('tmp:RRCCost5')
  p_web.StoreValue('tmp:RRCCost6')
  p_web.StoreValue('tmp:RRCCost7')
  p_web.StoreValue('tmp:RRCCost8')

local.AddToTempQueue        Procedure(String fField,String fSaveField,String fReason,Byte fSaveCost,String fCost)
    Code
        clear(tmpaud:Record)
        tmpaud:sessionID = p_web.SessionID
        tmpaud:field = fField
        get(TempAuditQueue,tmpaud:keyField)
        if (error())
            tmpaud:Reason = fReason
            if (fSaveCost = 1)
                tmpaud:SaveCost = 1
                tmpaud:Cost = fCost
            end !if (fSaveCost = 1)
            add(tempAuditQueue)
        else ! if (error())
            tmpaud:Reason = fReason
            put(tempAuditQueue)
        end ! if (error())
BannerVodacom        PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
FilesOpened     Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  GlobalErrors.SetProcedureName('BannerVodacom')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'BannerVodacom_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('BannerVodacom','')
    p_web.DivHeader('BannerVodacom',p_web.combine(p_web.site.style.formdiv,'fdiv'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('BannerVodacom') = 0
        p_web.AddPreCall('BannerVodacom')
        p_web.DivHeader('popup_BannerVodacom','nt-hidden')
        p_web.DivHeader('BannerVodacom',p_web.combine(p_web.site.style.formdiv,'fdiv'))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_BannerVodacom_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_BannerVodacom_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferBannerVodacom',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_BannerVodacom',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBannerVodacom',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_BannerVodacom',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBannerVodacom',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_BannerVodacom',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_BannerVodacom',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBannerVodacom',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_BannerVodacom',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBannerVodacom',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_BannerVodacom',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_BannerVodacom',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('BannerVodacom')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('BannerVodacom_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'BannerVodacom'
    end
    p_web.formsettings.proc = 'BannerVodacom'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine

AfterLookup Routine
  loc:TabNumber = -1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('BannerVodacom_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferBannerVodacom')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('BannerVodacom_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('BannerVodacom_ChainTo')
    loc:formaction = p_web.GetSessionValue('BannerVodacom_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction

GenerateForm   Routine
  do LoadRelatedRecords
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
    do SendPacket
    Do heading
    do SendPacket
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
    Packet = clip(Packet) & p_web.DivHeader('Tab_BannerVodacom',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="BannerVodacom_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      packet = clip(packet) & '</div><13,10>' ! end id="BannerVodacom_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'BannerVodacom_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="BannerVodacom_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'BannerVodacom_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_BannerVodacom')>0,p_web.GSV('showtab_BannerVodacom'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_BannerVodacom'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_BannerVodacom') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_BannerVodacom'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_BannerVodacom')>0,p_web.GSV('showtab_BannerVodacom'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_BannerVodacom') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_BannerVodacom_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_BannerVodacom')>0,p_web.GSV('showtab_BannerVodacom'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"BannerVodacom",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_BannerVodacom')>0,p_web.GSV('showtab_BannerVodacom'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_BannerVodacom_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('BannerVodacom') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('BannerVodacom')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine


NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_BannerVodacom_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('BannerVodacom_form:ready_',1)

  p_web.SetSessionValue('BannerVodacom_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_BannerVodacom',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('BannerVodacom_form:ready_',1)
  p_web.SetSessionValue('BannerVodacom_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_BannerVodacom',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('BannerVodacom_form:ready_',1)
  p_web.SetSessionValue('BannerVodacom_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('BannerVodacom:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('BannerVodacom_form:ready_',1)
  p_web.SetSessionValue('BannerVodacom_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('BannerVodacom:Primed',0)
  p_web.setsessionvalue('showtab_BannerVodacom',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('BannerVodacom_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('BannerVodacom_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('BannerVodacom:Primed',0)

heading  Routine
  packet = clip(packet) & p_web.AsciiToUTF(|
    '<<table class="bannerHeading"><13,10>'&|
    '    <<tr><13,10>'&|
    '        <<td width="810" align="center"><<img src="/images/header.gif" width="810" height="124" /><</td><13,10>'&|
    '    <</tr><13,10>'&|
    '    <<tr><13,10>'&|
    '        <<td width="810" align="right" class="SmallText"><<!-- Net:s:VersionNumber --><</td><13,10>'&|
    '    <</tr><13,10>'&|
    '<</table><13,10>'&|
    '',net:OnlyIfUTF)
BrowseJobCredits     PROCEDURE  (NetWebServerWorker p_web)
tmp:CreditNumber     STRING(30)                            !
tmp:CreditType       STRING(4)                             !
tmp:Amount           REAL                                  !
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256),dim(Net:MaxKeyFields)
idx           String(Net:HashSize)
sub           Long
            End
tmp:CreditNumber:IsInvalid  Long
tmp:CreditType:IsInvalid  Long
jov:DateCreated:IsInvalid  Long
tmp:Amount:IsInvalid  Long
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:DefaultSelection    String(Net:HashSize)
loc:ActualSelection     String(Net:HashSize)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
!loc:pagename            String(256)
loc:NavButtonPosition    Long
loc:UpdateButtonPosition Long
loc:SelectionMethod      Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorCase         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
loc:lookupdone          Long
loc:FormPopup           Long
ThisView            View(JOBSINV)
                      Project(jov:RecordNumber)
                      Project(jov:DateCreated)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
Loc:popup             Long
loc:poppedup          Long,static,thread
FilesOpened     Long
JOBS::State  USHORT
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return 
  End
  GlobalErrors.SetProcedureName('BrowseJobCredits')
  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseJobCredits:NoForm')
      loc:NoForm = p_web.GetValue('BrowseJobCredits:NoForm')
      loc:FormName = p_web.GetValue('BrowseJobCredits:FormName')
    else
      loc:FormName = 'BrowseJobCredits_frm'
    End
    p_web.SSV('BrowseJobCredits:NoForm',loc:NoForm)
    p_web.SSV('BrowseJobCredits:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseJobCredits:NoForm')
    loc:FormName = p_web.GSV('BrowseJobCredits:FormName')
  end
  case p_web.site.formpopups
  of -1 ; loc:FormPopup = Net:Page
  of 0 ; loc:FormPopup = Net:Page
  of 1 ; loc:FormPopup = Net:Popup
  End
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseJobCredits') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseJobCredits')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if 1 = 2
  elsif p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  elsif p_web.IfExistsValue('_EIPRow_')
    do CallRow
  elsif p_web.GetValue('_CallPopups') > 0
    do CallPopups
  elsif p_web.IfExistsValue('_GetSecwinSettings')
      p_web.SetValue('Secwin_' & 'BrowseJobCredits' & 'AccessGroupsArray','1') 
      p_web.SetValue('Secwin_AccessWindowName','BrowseJobCredits')
      
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallPopups  Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
loc:CallPopups   Long
loc:name         String(255)
  code
  if loc:poppedup = 0
    loc:CallPopups = p_web.GetValue('_CallPopups')
    if loc:CallPopups = 1 !and p_web.GetPreCall('BrowseJobCredits') = 0! for = 1 include popup divs and scripts for this browse
      !p_web.AddPreCall('BrowseJobCredits')
      p_web.DivHeader('popup_BrowseJobCredits','nt-hidden')
      p_web.DivHeader('BrowseJobCredits',p_web.combine(p_web.site.style.browsediv,))
      p_web.DivFooter()
      p_web.DivFooter()
      loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(400)&', modal: true, position: [''center'',15]'
      packet =  p_web.jQuery('#' & lower('popup_BrowseJobCredits_div'),'dialog',loc:options) & |
      p_web.jQuery('#' & lower('popup_BrowseJobCredits_div'),'removeClass','''nt-hidden''')
    End
    ! for > 0 add in form, EIP-lookups and browse object
    do Popups
    loc:popup = 1
    do ClosingScripts
    do SendPacket
    loc:poppedup = 1
  end

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web.DivHeader('BrowseJobCredits',p_web.combine(p_web.site.style.browsediv,))
  if loc:ParentSilent = 0
    do GenerateBrowse
    if p_web.RequestAjax = 1
      p_web.Script('BrowseProducts.pcv();')
    end
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web.DivFooter()
  do Children
  do Popups
  if loc:poppedup = 0
    do ClosingScripts
  end
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(JOBSINV,jov:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'TMP:CREDITNUMBER') then p_web.SetValue('BrowseJobCredits_sort','1')
    ElsIf (loc:vorder = 'TMP:CREDITTYPE') then p_web.SetValue('BrowseJobCredits_sort','2')
    ElsIf (loc:vorder = 'JOV:DATECREATED') then p_web.SetValue('BrowseJobCredits_sort','3')
    ElsIf (loc:vorder = 'TMP:AMOUNT') then p_web.SetValue('BrowseJobCredits_sort','4')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseJobCredits:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseJobCredits:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseJobCredits:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseJobCredits:LookupField')
    loc:selecting = 0
  End

Popups Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  If loc:popup = 0
  End
SetFormAction  Routine
  loc:formaction = 'BrowseJobCredits'

GotFocusBack   Routine

GenerateBrowse Routine
  data
loc:viewoptions  Long
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  ! Set general Browse options
  loc:NavButtonPosition   = Net:Below
  loc:UpdateButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:FileLoad
  loc:FillBack         = 0
  loc:Sorting          = Net:ServerSort
  if p_web.GetValue('_popup_') = 1
    loc:popup = 1
  end
  ! Set Locator Options
  loc:LocatorPosition  = Net:Below
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  p_web.ClearBrowse('BrowseJobCredits')
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 10
  End
  loc:ActualSelection = ''
  !loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseJobCredits_sort',net:DontEvaluate)
  p_web.SetSessionValue('BrowseJobCredits_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'tmp:CreditNumber','-tmp:CreditNumber')
    Loc:LocateField = 'tmp:CreditNumber'
    Loc:LocatorCase = 0
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'tmp:CreditType','-tmp:CreditType')
    Loc:LocateField = 'tmp:CreditType'
    Loc:LocatorCase = 0
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'jov:DateCreated','-jov:DateCreated')
    Loc:LocateField = 'jov:DateCreated'
    Loc:LocatorCase = 0
  of 4
    loc:vorder = Choose(Loc:SortDirection=1,'tmp:Amount','-tmp:Amount')
    Loc:LocateField = 'tmp:Amount'
    Loc:LocatorCase = 0
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('tmp:CreditNumber')
    loc:SortHeader = p_web.Translate('Credit Number')
    p_web.SetSessionValue('BrowseJobCredits_LocatorPic','@s30')
  Of upper('tmp:CreditType')
    loc:SortHeader = p_web.Translate('Type')
    p_web.SetSessionValue('BrowseJobCredits_LocatorPic','@s4')
  Of upper('jov:DateCreated')
    loc:SortHeader = p_web.Translate('Date Created')
    p_web.SetSessionValue('BrowseJobCredits_LocatorPic','@d6')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
    loc:LocatorType = Net:Date
  Of upper('tmp:Amount')
    loc:SortHeader = p_web.Translate('Amount')
    p_web.SetSessionValue('BrowseJobCredits_LocatorPic','@n-14.2')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
  End
  If loc:selecting = 1
    p_web.GetSettings(p_web.GetSessionValue('Push1'))
    loc:selectaction = p_web.FormSettings.ParentPage !p_web.GetSessionValue('BrowseJobCredits:LookupFrom')
  End!Else
  do SendPacket
  do SetFormAction
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseJobCredits:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseJobCredits:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseJobCredits:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="JOBSINV"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="jov:RecordNumberKey"></input><13,10>'
  end
  case p_web.GetValue('refresh') ; of 'next' orof 'previous' orof 'first' orof 'last'
    p_web.Script('try{{BrowseJobCredits.restoreFocus();} catch(err){{};')
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseJobCredits',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      case loc:locatortype
      of Net:Position
      orof Net:Date
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
      of Net:Contains
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
      of Net:Begins
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
      End
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
      Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
      Case Loc:LocatorType
      of Net:Date
        packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator2BrowseJobCredits','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('BrowseJobCredits_LocatorPic'),,,'onchange="BrowseJobCredits.locate(''Locator2BrowseJobCredits'',this.value);"',,0,,,) & '</div></td>'
      Else
        packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator2BrowseJobCredits',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="BrowseJobCredits.locate(''Locator2BrowseJobCredits'',this.value);" ',,,,,,) & '</td>'
      End
      If loc:LocatorSearchButton or loc:LocatorClearButton
        packet = clip(packet) & '<td><div id="BrowseJobCredits_locate_a" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
        If loc:LocatorSearchButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
        End
        If loc:LocatorClearButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseJobCredits.cl(''BrowseJobCredits'');')
        End
        packet = clip(packet) & '</div><13,10></td><13,10>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
      If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'BrowseJobCredits_locate_a','buttonset',loc:options)
      End
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & p_web.DivHeader('BrowseJobCredits_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'','sortable')&'" id="BrowseJobCredits_tbl">'
  Else
    packet = clip(packet) & p_web.DivHeader('BrowseJobCredits_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'')&'" id="BrowseJobCredits_tbl">'
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'1','BrowseJobCredits',p_web.Translate('Credit Number'),,,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'2','BrowseJobCredits',p_web.Translate('Type'),,,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'3','BrowseJobCredits',p_web.Translate('Date Created'),'Click here to sort by Date Created',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'4','BrowseJobCredits',p_web.Translate('Amount'),,,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:LocateField = 'jov:DateCreated' then Loc:NoBuffer = 1.
  If Loc:NoBuffer = 0
    Buffer(ThisView,10,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  If Instring('jov:recordnumber',lower(loc:vorder),1,1) = 0 !and JOBSINV{prop:SQLDriver} = 1
    loc:vorder = Choose(loc:vorder='','jov:RecordNumber',clip(loc:vorder) & ',' & 'jov:RecordNumber')
  End
  Loc:Selected = Choose(p_web.IfExistsValue('jov:RecordNumber'),p_web.GetValue('jov:RecordNumber'),p_web.GetSessionValue('jov:RecordNumber'))
  ThisView{prop:order} = p_web.CleanFilter(ThisView,clip(loc:vorder))
      loc:FilterWas = 'jov:RefNumber = ' & p_web.GSV('job:Ref_Number')
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseJobCredits',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseJobCredits_Filter')
    p_web.SetSessionValue('BrowseJobCredits_FirstValue','')
    p_web.SetSessionValue('BrowseJobCredits_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,JOBSINV,jov:RecordNumberKey,loc:PageRows,'BrowseJobCredits',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,loc:ViewOptions,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled,Loc:LocatorCase)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or Loc:LocatorType = Net:Date or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position or loc:LocatorType = Net:Date
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If JOBSINV{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(JOBSINV,loc:firstvalue)
              Reset(ThisView,JOBSINV)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If JOBSINV{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(JOBSINV,loc:lastvalue)
            Reset(ThisView,JOBSINV)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      if (jov:Type = 'C')
          tmp:CreditType = 'Cred'
          tmp:CreditNumber = 'CN' & CLIP(jov:InvoiceNumber) & '-' & CLIP(tra:BranchIdentification) & jov:Suffix
          tmp:Amount = jov:CreditAmount * -1
      else ! if (job:Type = 'C')
          tmp:CreditType = 'Inv'
          tmp:CreditNumber = CLIP(jov:InvoiceNumber) & '-' & CLIP(tra:BranchIdentification) & jov:Suffix
          tmp:Amount = jov:NewTOtalCost
      end !if (job:Type = 'C')
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(jov:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td class="'&p_web.combine(p_web.site.style.BrowseEmpty,)&'">'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:NavButtonPosition=Net:Above or (loc:NavButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseJobCredits_nav_a" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseJobCredits.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseJobCredits.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseJobCredits.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseJobCredits.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseJobCredits_nav_a','buttonset',loc:options)
          End
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:UpdateButtonPosition=Net:Above or (loc:UpdateButtonPosition=Net:Both and loc:found > 0))
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseJobCredits',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseJobCredits_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseJobCredits_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          case loc:locatortype
          of Net:Position
          orof Net:Date
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
          of Net:Contains
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
          of Net:Begins
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
          End
          packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
          Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
          Case Loc:LocatorType
          of Net:Date
            packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator1BrowseJobCredits','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('BrowseJobCredits_LocatorPic'),,,'onchange="BrowseJobCredits.locate(''Locator1BrowseJobCredits'',this.value);"',,0,,,) & '</div></td>'
          Else
            packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator1BrowseJobCredits',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="BrowseJobCredits.locate(''Locator1BrowseJobCredits'',this.value);" ',,,,,,) & '</td>'
          End
          If loc:LocatorSearchButton or loc:LocatorClearButton
            packet = clip(packet) & '<td><div id="BrowseJobCredits_locate_b" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
            If loc:LocatorSearchButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
            End
            If loc:LocatorClearButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseJobCredits.cl(''BrowseJobCredits'');')
            End
            packet = clip(packet) & '</div><13,10></td><13,10>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
          If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseJobCredits_locate_b','buttonset',loc:options)
          End
      End
    End
  End
  p_web.SetSessionValue('BrowseJobCredits_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseJobCredits_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:NavButtonPosition=Net:Below or loc:NavButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseJobCredits_nav_b" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseJobCredits.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseJobCredits.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseJobCredits.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseJobCredits.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseJobCredits_nav_b','buttonset',loc:options)
          End
        end
        do SendPacket
  End
  If loc:UpdateButtonPosition=Net:Below or loc:UpdateButtonPosition=Net:Both
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
  data
loc:rowclick  string(1024)
  code
    loc:field = p_web.AddBrowseValue('BrowseJobCredits','JOBSINV',jov:RecordNumberKey) !jov:RecordNumber
    p_web._thisrow = p_web._nocolon('jov:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if Loc:LocatorValue <> '' and loc:ActualSelection = ''
        loc:checked = 'checked'
        do SetSelection
      elsif loc:ActualSelection = '' and jov:RecordNumber = p_web.GetValue('jov:RecordNumber')
         loc:checked = 'checked'
         do SetSelection
      elsif loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseJobCredits:LookupField')) = jov:RecordNumber and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      else
        loc:checked = Choose((jov:RecordNumber = loc:selected) and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      end
      If(loc:SelectionMethod  = Net:Radio)
        loc:RowStyle = 'class="' & p_web.combine(p_web.site.style.browserow,) &'"'
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'data-nt-id="'& p_web.AddBrowseValue('BrowseJobCredits','JOBSINV',jov:RecordNumberKey) &'" onclick="'
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If JOBSINV{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(JOBSINV)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            loc:DefaultSelection = loc:field
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If JOBSINV{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(JOBSINV)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            loc:DefaultSelection = loc:field
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'>'&p_web.CreateInput('radio','_bidv_',clip(loc:field),,loc:checked,'')&'</td>'&CRLF
          If loc:DefaultSelection = ''
            loc:DefaultSelection = loc:field
          End
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          If loc:DefaultSelection = '' or loc:direction < 0
            loc:DefaultSelection = loc:field
          End
        End
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::tmp:CreditNumber
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::tmp:CreditType
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::jov:DateCreated
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::tmp:Amount
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr ' & clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Idx = p_web.AddBrowseValue('BrowseJobCredits','JOBSINV',jov:RecordNumberKey)
  TableQueue.Id[1] = jov:RecordNumber

ClosingScripts  Routine
  data
Rtn_SecwinProcedureName         string(252)
  code
  If p_web.RequestAjax = 0
    do SetFormAction
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var btiBrowseJobCredits;if (btiBrowseJobCredits != 1){{var BrowseJobCredits=new browseTable(''BrowseJobCredits'','''&clip(loc:formname)&|
      ''','''&p_web._jsok('jov:RecordNumber',Net:Parameter)&''',2,'''&clip(loc:divname)&''',1,1,1,'''&|
      clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&|
      p_web.Translate('Are you sure you want to delete this record?')&''','''&clip(loc:ActualSelection)&''','''&clip(loc:selectaction)&''','''&|
      clip(loc:formactiontarget)&''','''','&loc:popup&',0,'''','''',"' & clip(Rtn_SecwinProcedureName) & '");<13,10>'&|
      'BrowseJobCredits.setGreenBar('''&p_web.ColorWeb(p_web.Site.Style.BrowseHighlightColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOneColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseTwoColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOverColor)&''');<13,10>' &|
      'BrowseJobCredits.applyGreenBar();btiBrowseJobCredits=1};<13,10>' &|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseJobCredits')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseJobCredits')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseJobCredits')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseJobCredits')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(JOBSINV)
  p_web._CloseFile(JOBS)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(JOBSINV)
  Bind(jov:Record)
  Clear(jov:Record)
  NetWebSetSessionPics(p_web,JOBSINV)
  p_web._OpenFile(JOBS)
  Bind(job:Record)
  NetWebSetSessionPics(p_web,JOBS)

Children Routine
  if loc:selecting = 0
    If p_web.RequestAjax = 0
      do StartChildren
    Else
      do AjaxChildren
    End
  end

AjaxChildren  Routine
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue('jov:RecordNumber',p_web.GetValue('jov:RecordNumber'))
   p_web.DeleteValue('_Clicked')
   loc:found = 1
   do Children
! ----------------------------------------------------------------------------------------
CallRow  Routine
  data
loc:result  long
  code
  do OpenFilesB
  jov:RecordNumber = p_web.GSV('jov:RecordNumber')
  loc:result = p_web._GetFile(JOBSINV,jov:RecordNumberKey)
  loc:eip = 1
  loc:viewstate = p_web.escape(p_web.Base64Encode(clip(jov:RecordNumber)))
  do BrowseRow
  do SendPacket
  do ClosefilesB

! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(JOBSINV)
  Case upper(p_web.GetValue('_EIPClm'))
  Else
    case p_web.GetValue('_action_')
    of Net:InsertRecord
    orof Net:ChangeRecord
    orof Net:DeleteRecord
    orof Net:CopyRecord
      loc:eip = 0
      do CallBrowse
    end
  End
  do GotFocusBack
  p_web._CloseFile(JOBSINV)
! ----------------------------------------------------------------------------------------
value::tmp:CreditNumber   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseJobCredits_tmp:CreditNumber_'&jov:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(tmp:CreditNumber,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::tmp:CreditType   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseJobCredits_tmp:CreditType_'&jov:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(tmp:CreditType,'@s4')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::jov:DateCreated   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseJobCredits_jov:DateCreated_'&jov:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(jov:DateCreated,'@d6')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::tmp:Amount   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseJobCredits_tmp:Amount_'&jov:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(tmp:Amount,'@n-14.2')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  p_web._OpenFile(JOBS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(JOBS)
     FilesOpened = False
  END
  return
!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
CheckForDuplicate  Routine
SetSelection  Routine
  loc:ActualSelection = loc:field
  p_web.SetSessionValue('jov:RecordNumber',jov:RecordNumber)

MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:ActualSelection = ''
    loc:ActualSelection = loc:DefaultSelection
    p_web.GetBrowseValue(loc:ActualSelection,Net:Web:SessionQueue) ! so children are primed with correct sessionValue
  End

  If loc:ActualSelection <> ''
    TableQueue.Kind = Net:RowTable
    get(TableQueue,TableQueue.Kind)
    if Errorcode() = 0
      TableQueue.Row = sub(TableQueue.Row,1,len(clip(TableQueue.Row))-1) &  ' data-nt-rows="'&loc:RowsHigh&'" data-nt-value="'&clip(loc:ActualSelection)&'"' & '>'
      Put(TableQueue)
    End
  End

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead class="'&p_web.combine(p_web.site.style.BrowseHeader,'')&'"><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot class="'&p_web.combine(p_web.site.style.BrowseFooter,)&'"><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody class="'&p_web.combine(p_web.site.style.BrowseBody,)&'"><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table><13,10>' & p_web.DivFooter(Net:NoSend) ! Table Div
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
BillingConfirmation  PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
tmp:CChargeType      STRING(30)                            !
tmp:CRepairType      STRING(30)                            !
tmp:CConfirmRepairType STRING(30)                          !
tmp:WChargeType      STRING(30)                            !
tmp:WRepairType      STRING(30)                            !
tmp:WConfirmRepairType BYTE                                !
tmp:Password         STRING(30)                            !
FilesOpened     Long
CHARTYPE::State  USHORT
REPTYDEF::State  USHORT
USERS::State  USHORT
tmp:CChargeType:IsInvalid  Long
tmp:CRepairType:IsInvalid  Long
tmp:CConfirmRepairType:IsInvalid  Long
tmp:WChargeType:IsInvalid  Long
tmp:WRepairType:IsInvalid  Long
tmp:WConfirmRepairType:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
tmp:CChargeType_OptionView   View(CHARTYPE)
                          Project(cha:Charge_Type)
                        End
tmp:CRepairType_OptionView   View(REPTYDEF)
                          Project(rtd:Repair_Type)
                        End
tmp:WChargeType_OptionView   View(CHARTYPE)
                          Project(cha:Charge_Type)
                        End
tmp:WRepairType_OptionView   View(REPTYDEF)
                          Project(rtd:Repair_Type)
                        End
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('BillingConfirmation')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'BillingConfirmation_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('BillingConfirmation','')
    p_web.DivHeader('BillingConfirmation',p_web.combine(p_web.site.style.formdiv,))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('BillingConfirmation') = 0
        p_web.AddPreCall('BillingConfirmation')
        p_web.DivHeader('popup_BillingConfirmation','nt-hidden')
        p_web.DivHeader('BillingConfirmation',p_web.combine(p_web.site.style.formdiv,))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_BillingConfirmation_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_BillingConfirmation_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferBillingConfirmation',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_BillingConfirmation',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBillingConfirmation',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_BillingConfirmation',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBillingConfirmation',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_BillingConfirmation',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_BillingConfirmation',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBillingConfirmation',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_BillingConfirmation',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBillingConfirmation',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_BillingConfirmation',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_BillingConfirmation',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('BillingConfirmation')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(CHARTYPE)
  p_web._OpenFile(REPTYDEF)
  p_web._OpenFile(USERS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(CHARTYPE)
  p_Web._CloseFile(REPTYDEF)
  p_Web._CloseFile(USERS)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('BillingConfirmation_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'BillingConfirmation'
    end
    p_web.formsettings.proc = 'BillingConfirmation'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine

AfterLookup Routine
  loc:TabNumber = -1
  If p_web.GSV('job:Chargeable_Job') = 'YES'
    loc:TabNumber += 1
  End
  Case p_Web.GetValue('lookupfield')
  Of 'tmp:CChargeType'
    p_web.setsessionvalue('showtab_BillingConfirmation',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(CHARTYPE)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:CRepairType')
  End
  If p_web.GSV('job:Warranty_job') = 'YES'
    loc:TabNumber += 1
  End
  Case p_Web.GetValue('lookupfield')
  Of 'tmp:WChargeType'
    p_web.setsessionvalue('showtab_BillingConfirmation',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(CHARTYPE)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:WRepairType')
  End
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  if p_web.IfExistsValue('tmp:CChargeType') = 0
    p_web.SetSessionValue('tmp:CChargeType',tmp:CChargeType)
  Else
    tmp:CChargeType = p_web.GetSessionValue('tmp:CChargeType')
  End
  if p_web.IfExistsValue('tmp:CRepairType') = 0
    p_web.SetSessionValue('tmp:CRepairType',tmp:CRepairType)
  Else
    tmp:CRepairType = p_web.GetSessionValue('tmp:CRepairType')
  End
  if p_web.IfExistsValue('tmp:CConfirmRepairType') = 0
    p_web.SetSessionValue('tmp:CConfirmRepairType',tmp:CConfirmRepairType)
  Else
    tmp:CConfirmRepairType = p_web.GetSessionValue('tmp:CConfirmRepairType')
  End
  if p_web.IfExistsValue('tmp:WChargeType') = 0
    p_web.SetSessionValue('tmp:WChargeType',tmp:WChargeType)
  Else
    tmp:WChargeType = p_web.GetSessionValue('tmp:WChargeType')
  End
  if p_web.IfExistsValue('tmp:WRepairType') = 0
    p_web.SetSessionValue('tmp:WRepairType',tmp:WRepairType)
  Else
    tmp:WRepairType = p_web.GetSessionValue('tmp:WRepairType')
  End
  if p_web.IfExistsValue('tmp:WConfirmRepairType') = 0
    p_web.SetSessionValue('tmp:WConfirmRepairType',tmp:WConfirmRepairType)
  Else
    tmp:WConfirmRepairType = p_web.GetSessionValue('tmp:WConfirmRepairType')
  End

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('tmp:CChargeType')
    tmp:CChargeType = p_web.GetValue('tmp:CChargeType')
    p_web.SetSessionValue('tmp:CChargeType',tmp:CChargeType)
  Else
    tmp:CChargeType = p_web.GetSessionValue('tmp:CChargeType')
  End
  if p_web.IfExistsValue('tmp:CRepairType')
    tmp:CRepairType = p_web.GetValue('tmp:CRepairType')
    p_web.SetSessionValue('tmp:CRepairType',tmp:CRepairType)
  Else
    tmp:CRepairType = p_web.GetSessionValue('tmp:CRepairType')
  End
  if p_web.IfExistsValue('tmp:CConfirmRepairType')
    tmp:CConfirmRepairType = p_web.GetValue('tmp:CConfirmRepairType')
    p_web.SetSessionValue('tmp:CConfirmRepairType',tmp:CConfirmRepairType)
  Else
    tmp:CConfirmRepairType = p_web.GetSessionValue('tmp:CConfirmRepairType')
  End
  if p_web.IfExistsValue('tmp:WChargeType')
    tmp:WChargeType = p_web.GetValue('tmp:WChargeType')
    p_web.SetSessionValue('tmp:WChargeType',tmp:WChargeType)
  Else
    tmp:WChargeType = p_web.GetSessionValue('tmp:WChargeType')
  End
  if p_web.IfExistsValue('tmp:WRepairType')
    tmp:WRepairType = p_web.GetValue('tmp:WRepairType')
    p_web.SetSessionValue('tmp:WRepairType',tmp:WRepairType)
  Else
    tmp:WRepairType = p_web.GetSessionValue('tmp:WRepairType')
  End
  if p_web.IfExistsValue('tmp:WConfirmRepairType')
    tmp:WConfirmRepairType = p_web.GetValue('tmp:WConfirmRepairType')
    p_web.SetSessionValue('tmp:WConfirmRepairType',tmp:WConfirmRepairType)
  Else
    tmp:WConfirmRepairType = p_web.GetSessionValue('tmp:WConfirmRepairType')
  End

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('BillingConfirmation_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.GSV('NextURL')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('BillingConfirmation_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('BillingConfirmation_ChainTo')
    loc:formaction = p_web.GetSessionValue('BillingConfirmation_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'ViewJob'

GenerateForm   Routine
  do LoadRelatedRecords
      CalculateBilling(p_web)
  
      p_web.SSV('tmp:CChargeType',p_web.GSV('job:Charge_Type'))
      p_web.SSV('tmp:WChargeType',p_web.GSV('job:Warranty_Charge_Type'))
      p_web.SSV('tmp:CConfirmRepairType',p_web.GSV('jobe:COverwriteRepairType'))
      p_web.SSV('tmp:WConfirmRepairType',p_web.GSV('jobe:WOverwriteRepairType'))
      p_web.SSV('tmp:CRepairType',p_web.GSV('job:Repair_Type'))
      p_web.SSV('tmp:WRepairType',p_web.GSV('job:Repair_Type_Warranty'))
  
      if (securityCheckFailed(p_web.GSV('BookingUserPassword'),'JOBS - AMEND REPAIR TYPES'))
          p_web.SSV('ReadOnly:RepairType',1)
      else!if (securityCheckFailed('JOBS - AMEND REPAIR TYPES',p_web.GSV('BookingPassword'))
          p_web.SSV('ReadOnly:RepairType',0)
      end !if (securityCheckFailed('JOBS - AMEND REPAIR TYPES',p_web.GSV('BookingPassword'))
  
      p_web.SSV('locEngineerSkillLevel',0)
      if (p_web.GSV('job:Engineer') <> '')
          Access:USERS.Clearkey(use:user_Code_Key)
          use:user_Code    = p_web.GSV('job:Engineer')
          if (Access:USERS.TryFetch(use:user_Code_Key) = Level:Benign)
              ! Found
              p_web.SSV('locEngineerSkillLevel',use:SkillLevel)
          else ! if (Access:USERS.TryFetch(use:user_Code_Key) = Level:Benign)
              ! Error
          end ! if (Access:USERS.TryFetch(use:user_Code_Key) = Level:Benign)
      end ! if (p_web.GSV('job:Engineer') <> '')
  
      p_web.storeValue('JobCompleteProcess')
  
      if (p_web.GSV('JobCompleteProcess') = 1)
          if (p_web.GSV('job:Estimate') = 'YES' and p_web.GSV('job:Chargeable_Job') = 'YES')
              if (p_web.GSV('job:Estimate_Accepted') <> 'YES' and |
                  p_web.GSV('job:Estimate_Rejected') <> 'YES' and |
                  p_web.GSV('job:Estimate_Ready') <> 'YES')
  
                  jobPricingRoutine(p_web)
  
                  p_web.SSV('TotalPrice:Type','E')
  
                  totalPrice(p_web)
  
                  if (p_web.GSV('TotalPrice:Total') < p_web.GSV('job:Estimate_If_Over'))
                      p_web.SSV('nextURL','EstimateQuery')
                      p_web.SSV('JobCompleteProcess',0)
                  else ! if (p_web.GSV('TotalRepair:Total') < p_web.GSV('job:Estimate_If_Over'))
                      p_web.SSV('nextURL','ViewJob')
                      p_web.SSV('job:Courier_Cost',p_web.GSV('job:Courier_Cost_Estimate'))
                      p_web.SSV('job:Estimate_Ready','YES')
  
                      p_web.SSV('JobCompleteProcess',0)
  
                      p_web.SSV('Hide:EstimateReady',0)
  
                      getStatus(510,0,'JOB',p_web)
  
                  end ! if (p_web.GSV('TotalRepair:Total') < p_web.GSV('job:Estimate_If_Over'))
              end ! if (p_web.GSV('job:Estimate_Accepted') <> 'YES' and |
          else !
              p_web.SSV('nextURL','ViewJob')
          end ! if (p_web.GSV('job:Estimate') = 'YES' and p_web.GSV('job:Chargeable_Job') = 'YES')
      else !
          p_web.StoreValue('passedURL')
          IF (p_web.GSV('passedURL') <> '')
              p_web.SSV('nextURL',p_web.GSV('passedURL'))
          ELSE
              p_web.SSV('nextURL','ViewCosts')
          END
  
      end ! if (p_web.GSV('Job:CompleteProcess') = 1)
 tmp:CChargeType = p_web.RestoreValue('tmp:CChargeType')
 tmp:CRepairType = p_web.RestoreValue('tmp:CRepairType')
 tmp:CConfirmRepairType = p_web.RestoreValue('tmp:CConfirmRepairType')
 tmp:WChargeType = p_web.RestoreValue('tmp:WChargeType')
 tmp:WRepairType = p_web.RestoreValue('tmp:WRepairType')
 tmp:WConfirmRepairType = p_web.RestoreValue('tmp:WConfirmRepairType')
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Billing Confirmation') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Billing Confirmation',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_BillingConfirmation',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      If p_web.GSV('job:Chargeable_Job') = 'YES'
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_BillingConfirmation0_div')&'">'&p_web.Translate('Chargeable')&'</a></li>'& CRLF
      End
      If p_web.GSV('job:Warranty_job') = 'YES'
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_BillingConfirmation1_div')&'">'&p_web.Translate('Warranty')&'</a></li>'& CRLF
      End
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  do GenerateTab1
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="BillingConfirmation_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.cancel();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CancelButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="BillingConfirmation_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'BillingConfirmation_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="BillingConfirmation_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'BillingConfirmation_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    ElsIf p_web.GSV('job:Chargeable_Job') = 'YES'
        p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:CChargeType')
    ElsIf p_web.GSV('job:Warranty_job') = 'YES'
        p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:WChargeType')
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_BillingConfirmation')>0,p_web.GSV('showtab_BillingConfirmation'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_BillingConfirmation'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_BillingConfirmation') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_BillingConfirmation'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_BillingConfirmation')>0,p_web.GSV('showtab_BillingConfirmation'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_BillingConfirmation') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
      If p_web.GSV('job:Chargeable_Job') = 'YES'
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Chargeable') & ''''
      End
      If p_web.GSV('job:Warranty_job') = 'YES'
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Warranty') & ''''
      End
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_BillingConfirmation_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_BillingConfirmation')>0,p_web.GSV('showtab_BillingConfirmation'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"BillingConfirmation",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_BillingConfirmation')>0,p_web.GSV('showtab_BillingConfirmation'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_BillingConfirmation_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('BillingConfirmation') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('BillingConfirmation')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
  If p_web.GSV('job:Chargeable_Job') = 'YES'
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Chargeable')&'</a></h3>' & CRLF & p_web.DivHeader('tab_BillingConfirmation0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_BillingConfirmation0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_BillingConfirmation0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_BillingConfirmation0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Chargeable')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_BillingConfirmation0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Chargeable')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_BillingConfirmation0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Chargeable')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_BillingConfirmation0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:CChargeType
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:CChargeType
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:CChargeType
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:CRepairType
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:CRepairType
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:CRepairType
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:CConfirmRepairType
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:CConfirmRepairType
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:CConfirmRepairType
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
  end
GenerateTab1  Routine
  If p_web.GSV('job:Warranty_job') = 'YES'
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Warranty')&'</a></h3>' & CRLF & p_web.DivHeader('tab_BillingConfirmation1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_BillingConfirmation1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_BillingConfirmation1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_BillingConfirmation1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Warranty')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_BillingConfirmation1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Warranty')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_BillingConfirmation1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Warranty')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_BillingConfirmation1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:WChargeType
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:WChargeType
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:WChargeType
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:WRepairType
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:WRepairType
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:WRepairType
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:WConfirmRepairType
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:WConfirmRepairType
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:WConfirmRepairType
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
  end


Prompt::tmp:CChargeType  Routine
  packet = clip(packet) & p_web.DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:CChargeType') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Charge Type'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:CChargeType  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:CChargeType = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:CChargeType = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:CChargeType  ! copies value to session value if valid.
  do Value::tmp:CChargeType
  do SendAlert
  do Comment::tmp:CChargeType ! allows comment style to be updated.
  do Value::tmp:CRepairType  !1

ValidateValue::tmp:CChargeType  Routine
    If not (1=0)
    tmp:CChargeType = Upper(tmp:CChargeType)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:CChargeType',tmp:CChargeType).
    End

Value::tmp:CChargeType  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:CChargeType') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,p_web.site.style.formselect,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:retrying
    tmp:CChargeType = p_web.RestoreValue('tmp:CChargeType')
    do ValidateValue::tmp:CChargeType
    If tmp:CChargeType:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- DROPLIST ---
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:CChargeType'',''billingconfirmation_tmp:cchargetype_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:CChargeType')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('Job:ViewOnly') = 1,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:CChargeType',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
  loc:fieldclass = ''
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('tmp:CChargeType') = 0
    p_web.SetSessionValue('tmp:CChargeType','')
  end
    packet = clip(packet) & p_web.CreateOption('-------------------------------------','',choose('' = p_web.getsessionvalue('tmp:CChargeType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(CHARTYPE)
  bind(cha:Record)
  p_web._OpenFile(REPTYDEF)
  bind(rtd:Record)
  p_web._OpenFile(USERS)
  bind(use:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(tmp:CChargeType_OptionView)
  tmp:CChargeType_OptionView{prop:filter} = p_web.CleanFilter(tmp:CChargeType_OptionView,'upper(cha:Warranty ) <> ''YES''')
  tmp:CChargeType_OptionView{prop:order} = p_web.CleanFilter(tmp:CChargeType_OptionView,'UPPER(cha:Charge_Type)')
  Set(tmp:CChargeType_OptionView)
  Loop
    Next(tmp:CChargeType_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('tmp:CChargeType') = 0
      p_web.SetSessionValue('tmp:CChargeType',cha:Charge_Type)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,cha:Charge_Type,choose(cha:Charge_Type = p_web.getsessionvalue('tmp:CChargeType')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(tmp:CChargeType_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(CHARTYPE)
  p_Web._CloseFile(REPTYDEF)
  p_Web._CloseFile(USERS)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:CChargeType  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:CChargeType:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:CChargeType') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:CRepairType  Routine
  packet = clip(packet) & p_web.DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:CRepairType') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Repair Type'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:CRepairType  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:CRepairType = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:CRepairType = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:CRepairType  ! copies value to session value if valid.
  do Value::tmp:CRepairType
  do SendAlert
  do Comment::tmp:CRepairType ! allows comment style to be updated.

ValidateValue::tmp:CRepairType  Routine
    If not (1=0)
    tmp:CRepairType = Upper(tmp:CRepairType)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:CRepairType',tmp:CRepairType).
    End

Value::tmp:CRepairType  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:CRepairType') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,p_web.site.style.formselect,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:retrying
    tmp:CRepairType = p_web.RestoreValue('tmp:CRepairType')
    do ValidateValue::tmp:CRepairType
    If tmp:CRepairType:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- DROPLIST ---
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:CRepairType'',''billingconfirmation_tmp:crepairtype_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('ReadOnly:RepairType') = 1 OR p_web.GSV('Job:ViewOnly') = 1,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:CRepairType',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
  loc:fieldclass = ''
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('tmp:CRepairType') = 0
    p_web.SetSessionValue('tmp:CRepairType','')
  end
    packet = clip(packet) & p_web.CreateOption('-------------------------------------','',choose('' = p_web.getsessionvalue('tmp:CRepairType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(CHARTYPE)
  bind(cha:Record)
  p_web._OpenFile(REPTYDEF)
  bind(rtd:Record)
  p_web._OpenFile(USERS)
  bind(use:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(tmp:CRepairType_OptionView)
  tmp:CRepairType_OptionView{prop:filter} = p_web.CleanFilter(tmp:CRepairType_OptionView,'upper(rtd:manufacturer) = upper(''' & p_web.GSV('job:manufacturer') & ''') and upper(rtd:chargeable) = ''YES'' and rtd:NotAvailable = 0 and rtd:SkillLevel <= ' & p_web.GSV('locEngineerSkillLevel'))
  tmp:CRepairType_OptionView{prop:order} = p_web.CleanFilter(tmp:CRepairType_OptionView,'UPPER(rtd:Repair_Type)')
  Set(tmp:CRepairType_OptionView)
  Loop
    Next(tmp:CRepairType_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('tmp:CRepairType') = 0
      p_web.SetSessionValue('tmp:CRepairType',rtd:Repair_Type)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,rtd:Repair_Type,choose(rtd:Repair_Type = p_web.getsessionvalue('tmp:CRepairType')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(tmp:CRepairType_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(CHARTYPE)
  p_Web._CloseFile(REPTYDEF)
  p_Web._CloseFile(USERS)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:CRepairType  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:CRepairType:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:CRepairType') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:CConfirmRepairType  Routine
  packet = clip(packet) & p_web.DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:CConfirmRepairType') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Confirm Repair Type'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:CConfirmRepairType  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:CConfirmRepairType = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    tmp:CConfirmRepairType = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:CConfirmRepairType  ! copies value to session value if valid.
  do Value::tmp:CConfirmRepairType
  do SendAlert
  do Comment::tmp:CConfirmRepairType ! allows comment style to be updated.

ValidateValue::tmp:CConfirmRepairType  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:CConfirmRepairType',tmp:CConfirmRepairType).
    End

Value::tmp:CConfirmRepairType  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:CConfirmRepairType') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If loc:retrying
    tmp:CConfirmRepairType = p_web.RestoreValue('tmp:CConfirmRepairType')
    do ValidateValue::tmp:CConfirmRepairType
    If tmp:CConfirmRepairType:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- CHECKBOX --- tmp:CConfirmRepairType
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:CConfirmRepairType'',''billingconfirmation_tmp:cconfirmrepairtype_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('Job:ViewOnly') = 1,'disabled','')
  If p_web.GetSessionValue('tmp:CConfirmRepairType') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','tmp:CConfirmRepairType',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:CConfirmRepairType  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:CConfirmRepairType:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:CConfirmRepairType') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:WChargeType  Routine
  packet = clip(packet) & p_web.DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:WChargeType') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Charge Type'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:WChargeType  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:WChargeType = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:WChargeType = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:WChargeType  ! copies value to session value if valid.
  do Value::tmp:WChargeType
  do SendAlert
  do Comment::tmp:WChargeType ! allows comment style to be updated.
  do Value::tmp:WRepairType  !1

ValidateValue::tmp:WChargeType  Routine
    If not (1=0)
    tmp:WChargeType = Upper(tmp:WChargeType)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:WChargeType',tmp:WChargeType).
    End

Value::tmp:WChargeType  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:WChargeType') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,p_web.site.style.formselect,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:retrying
    tmp:WChargeType = p_web.RestoreValue('tmp:WChargeType')
    do ValidateValue::tmp:WChargeType
    If tmp:WChargeType:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- DROPLIST ---
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:WChargeType'',''billingconfirmation_tmp:wchargetype_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:WChargeType')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('Job:ViewOnly') = 1,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:WChargeType',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
  loc:fieldclass = ''
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('tmp:WChargeType') = 0
    p_web.SetSessionValue('tmp:WChargeType','')
  end
    packet = clip(packet) & p_web.CreateOption('-------------------------------------','',choose('' = p_web.getsessionvalue('tmp:WChargeType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(CHARTYPE)
  bind(cha:Record)
  p_web._OpenFile(REPTYDEF)
  bind(rtd:Record)
  p_web._OpenFile(USERS)
  bind(use:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(tmp:WChargeType_OptionView)
  tmp:WChargeType_OptionView{prop:filter} = p_web.CleanFilter(tmp:WChargeType_OptionView,'cha:Warranty = ''YES''')
  tmp:WChargeType_OptionView{prop:order} = p_web.CleanFilter(tmp:WChargeType_OptionView,'UPPER(cha:Charge_Type)')
  Set(tmp:WChargeType_OptionView)
  Loop
    Next(tmp:WChargeType_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('tmp:WChargeType') = 0
      p_web.SetSessionValue('tmp:WChargeType',cha:Charge_Type)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,cha:Charge_Type,choose(cha:Charge_Type = p_web.getsessionvalue('tmp:WChargeType')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(tmp:WChargeType_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(CHARTYPE)
  p_Web._CloseFile(REPTYDEF)
  p_Web._CloseFile(USERS)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:WChargeType  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:WChargeType:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:WChargeType') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:WRepairType  Routine
  packet = clip(packet) & p_web.DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:WRepairType') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Repair Type'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:WRepairType  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:WRepairType = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:WRepairType = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:WRepairType  ! copies value to session value if valid.
  do Value::tmp:WRepairType
  do SendAlert
  do Comment::tmp:WRepairType ! allows comment style to be updated.

ValidateValue::tmp:WRepairType  Routine
    If not (1=0)
    tmp:WRepairType = Upper(tmp:WRepairType)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:WRepairType',tmp:WRepairType).
    End

Value::tmp:WRepairType  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:WRepairType') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,p_web.site.style.formselect,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:retrying
    tmp:WRepairType = p_web.RestoreValue('tmp:WRepairType')
    do ValidateValue::tmp:WRepairType
    If tmp:WRepairType:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- DROPLIST ---
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:WRepairType'',''billingconfirmation_tmp:wrepairtype_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('ReadOnly:RepairType') = 1 OR p_web.GSV('Job:ViewOnly') = 1,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:WRepairType',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
  loc:fieldclass = ''
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('tmp:WRepairType') = 0
    p_web.SetSessionValue('tmp:WRepairType','')
  end
    packet = clip(packet) & p_web.CreateOption('-------------------------------------','',choose('' = p_web.getsessionvalue('tmp:WRepairType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(CHARTYPE)
  bind(cha:Record)
  p_web._OpenFile(REPTYDEF)
  bind(rtd:Record)
  p_web._OpenFile(USERS)
  bind(use:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(tmp:WRepairType_OptionView)
  tmp:WRepairType_OptionView{prop:filter} = p_web.CleanFilter(tmp:WRepairType_OptionView,'upper(rtd:manufacturer) = upper(''' & p_web.GSV('job:manufacturer') & ''') and upper(rtd:warranty) = ''YES'' and rtd:NotAvailable = 0 and rtd:SkillLevel <= ' & p_web.GSV('locEngineerSkillLevel'))
  tmp:WRepairType_OptionView{prop:order} = p_web.CleanFilter(tmp:WRepairType_OptionView,'UPPER(rtd:Repair_Type)')
  Set(tmp:WRepairType_OptionView)
  Loop
    Next(tmp:WRepairType_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('tmp:WRepairType') = 0
      p_web.SetSessionValue('tmp:WRepairType',rtd:Repair_Type)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,rtd:Repair_Type,choose(rtd:Repair_Type = p_web.getsessionvalue('tmp:WRepairType')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(tmp:WRepairType_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(CHARTYPE)
  p_Web._CloseFile(REPTYDEF)
  p_Web._CloseFile(USERS)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:WRepairType  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:WRepairType:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:WRepairType') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:WConfirmRepairType  Routine
  packet = clip(packet) & p_web.DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:WConfirmRepairType') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Confirm Repair Type'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:WConfirmRepairType  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:WConfirmRepairType = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    tmp:WConfirmRepairType = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:WConfirmRepairType  ! copies value to session value if valid.
  do Value::tmp:WConfirmRepairType
  do SendAlert
  do Comment::tmp:WConfirmRepairType ! allows comment style to be updated.

ValidateValue::tmp:WConfirmRepairType  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:WConfirmRepairType',tmp:WConfirmRepairType).
    End

Value::tmp:WConfirmRepairType  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:WConfirmRepairType') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If loc:retrying
    tmp:WConfirmRepairType = p_web.RestoreValue('tmp:WConfirmRepairType')
    do ValidateValue::tmp:WConfirmRepairType
    If tmp:WConfirmRepairType:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- CHECKBOX --- tmp:WConfirmRepairType
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:WConfirmRepairType'',''billingconfirmation_tmp:wconfirmrepairtype_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('Job:ViewOnly') = 1,'disabled','')
  If p_web.GetSessionValue('tmp:WConfirmRepairType') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','tmp:WConfirmRepairType',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:WConfirmRepairType  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:WConfirmRepairType:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:WConfirmRepairType') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('BillingConfirmation_nexttab_' & 0)
    tmp:CChargeType = p_web.GSV('tmp:CChargeType')
    do ValidateValue::tmp:CChargeType
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:CChargeType
      !do SendAlert
      do Comment::tmp:CChargeType ! allows comment style to be updated.
      !exit
    End
    tmp:CRepairType = p_web.GSV('tmp:CRepairType')
    do ValidateValue::tmp:CRepairType
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:CRepairType
      !do SendAlert
      do Comment::tmp:CRepairType ! allows comment style to be updated.
      !exit
    End
    tmp:CConfirmRepairType = p_web.GSV('tmp:CConfirmRepairType')
    do ValidateValue::tmp:CConfirmRepairType
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:CConfirmRepairType
      !do SendAlert
      do Comment::tmp:CConfirmRepairType ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('BillingConfirmation_nexttab_' & 1)
    tmp:WChargeType = p_web.GSV('tmp:WChargeType')
    do ValidateValue::tmp:WChargeType
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:WChargeType
      !do SendAlert
      do Comment::tmp:WChargeType ! allows comment style to be updated.
      !exit
    End
    tmp:WRepairType = p_web.GSV('tmp:WRepairType')
    do ValidateValue::tmp:WRepairType
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:WRepairType
      !do SendAlert
      do Comment::tmp:WRepairType ! allows comment style to be updated.
      !exit
    End
    tmp:WConfirmRepairType = p_web.GSV('tmp:WConfirmRepairType')
    do ValidateValue::tmp:WConfirmRepairType
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:WConfirmRepairType
      !do SendAlert
      do Comment::tmp:WConfirmRepairType ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_BillingConfirmation_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('BillingConfirmation_tab_' & 0)
    do GenerateTab0
  of lower('BillingConfirmation_tmp:CChargeType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:CChargeType
      of event:timer
        do Value::tmp:CChargeType
        do Comment::tmp:CChargeType
      else
        do Value::tmp:CChargeType
      end
  of lower('BillingConfirmation_tmp:CRepairType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:CRepairType
      of event:timer
        do Value::tmp:CRepairType
        do Comment::tmp:CRepairType
      else
        do Value::tmp:CRepairType
      end
  of lower('BillingConfirmation_tmp:CConfirmRepairType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:CConfirmRepairType
      of event:timer
        do Value::tmp:CConfirmRepairType
        do Comment::tmp:CConfirmRepairType
      else
        do Value::tmp:CConfirmRepairType
      end
  of lower('BillingConfirmation_tab_' & 1)
    do GenerateTab1
  of lower('BillingConfirmation_tmp:WChargeType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:WChargeType
      of event:timer
        do Value::tmp:WChargeType
        do Comment::tmp:WChargeType
      else
        do Value::tmp:WChargeType
      end
  of lower('BillingConfirmation_tmp:WRepairType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:WRepairType
      of event:timer
        do Value::tmp:WRepairType
        do Comment::tmp:WRepairType
      else
        do Value::tmp:WRepairType
      end
  of lower('BillingConfirmation_tmp:WConfirmRepairType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:WConfirmRepairType
      of event:timer
        do Value::tmp:WConfirmRepairType
        do Comment::tmp:WConfirmRepairType
      else
        do Value::tmp:WConfirmRepairType
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('BillingConfirmation_form:ready_',1)

  p_web.SetSessionValue('BillingConfirmation_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_BillingConfirmation',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('BillingConfirmation_form:ready_',1)
  p_web.SetSessionValue('BillingConfirmation_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_BillingConfirmation',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('BillingConfirmation_form:ready_',1)
  p_web.SetSessionValue('BillingConfirmation_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('BillingConfirmation:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('BillingConfirmation_form:ready_',1)
  p_web.SetSessionValue('BillingConfirmation_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('BillingConfirmation:Primed',0)
  p_web.setsessionvalue('showtab_BillingConfirmation',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
  If p_web.GSV('job:Chargeable_Job') = 'YES'
      If not (1=0)
        If not (p_web.GSV('Job:ViewOnly') = 1)
          If p_web.IfExistsValue('tmp:CChargeType')
            tmp:CChargeType = p_web.GetValue('tmp:CChargeType')
          End
        End
      End
      If not (1=0)
        If not (p_web.GSV('ReadOnly:RepairType') = 1 OR p_web.GSV('Job:ViewOnly') = 1)
          If p_web.IfExistsValue('tmp:CRepairType')
            tmp:CRepairType = p_web.GetValue('tmp:CRepairType')
          End
        End
      End
      If not (1=0)
        If not (p_web.GSV('Job:ViewOnly') = 1)
          If p_web.IfExistsValue('tmp:CConfirmRepairType') = 0
            p_web.SetValue('tmp:CConfirmRepairType',0)
            tmp:CConfirmRepairType = 0
          Else
            tmp:CConfirmRepairType = p_web.GetValue('tmp:CConfirmRepairType')
          End
        End
      End
  End
  If p_web.GSV('job:Warranty_job') = 'YES'
      If not (1=0)
        If not (p_web.GSV('Job:ViewOnly') = 1)
          If p_web.IfExistsValue('tmp:WChargeType')
            tmp:WChargeType = p_web.GetValue('tmp:WChargeType')
          End
        End
      End
      If not (1=0)
        If not (p_web.GSV('ReadOnly:RepairType') = 1 OR p_web.GSV('Job:ViewOnly') = 1)
          If p_web.IfExistsValue('tmp:WRepairType')
            tmp:WRepairType = p_web.GetValue('tmp:WRepairType')
          End
        End
      End
      If not (1=0)
        If not (p_web.GSV('Job:ViewOnly') = 1)
          If p_web.IfExistsValue('tmp:WConfirmRepairType') = 0
            p_web.SetValue('tmp:WConfirmRepairType',0)
            tmp:WConfirmRepairType = 0
          Else
            tmp:WConfirmRepairType = p_web.GetValue('tmp:WConfirmRepairType')
          End
        End
      End
  End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('BillingConfirmation_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  if (p_web.GSV('JobCompleteProcess') = 1)
      if (p_web.GSV('job:Chargeable_Job') = 'YES')
          if (p_web.GSV('tmp:CConfirmRepairType') = 0)
              loc:alert = 'You must confirm the chargeable repair type.'
              loc:invalid = 'tmp:CConfirmRepairType'
              exit
          end ! if (p_web.GSV('tmp:CConfirmRepairType') = 0)
      end ! if (p_web.GSV('job:Chargeable_Job') = 'YES')
  
      if (p_web.GSV('job:Warranty_Job') = 'YES')
          if (p_web.GSV('tmp:WConfirmRepairType') = 0)
              loc:alert = 'You must confirm the warranty repair type.'
              loc:invalid = 'tmp:WConfirmRepairType'
              exit
          end ! if (p_web.GSV('tmp:CConfirmRepairType') = 0)
      end ! if (p_web.GSV('job:Chargeable_Job') = 'YES')
  end ! if (p_web.GSV('JobCompleteProcess') = 1)
  
  IF (vod.RepairTypesNoParts(p_web.GSV('job:Chargeable_Job'), |
      p_web.GSV('job:Warranty_Job'), |
      p_web.GSV('job:Manufacturer'), |
      p_web.GSV('tmp:CRepairType'), |
      p_web.GSV('tmp:WRepairType')))
      ! #11762 Repair Type does not allow parts.
      ! Check if there are any (Bryan: 24/11/2010)
  
      IF (vod.JobHasSparesAttached(p_web.GSV('job:Ref_Number',), |
          p_web.GSV('job:Estimate'), |
          p_web.GSV('job:Chargeable_Job'), |
          p_web.GSV('job:Warranty_Job')))
          loc:Invalid = 'tmp:ConfirmRepairType'
          loc:Alert = 'The selected Repair Type(s) require that there are no parts attached to the job.'
          EXIT
      END
  END
  
  
  p_web.DeleteSessionValue('BillingConfirmation_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
  If p_web.GSV('job:Chargeable_Job') = 'YES'
    loc:InvalidTab += 1
    do ValidateValue::tmp:CChargeType
    If loc:Invalid then exit.
    do ValidateValue::tmp:CRepairType
    If loc:Invalid then exit.
    do ValidateValue::tmp:CConfirmRepairType
    If loc:Invalid then exit.
  End
  ! tab = 2
  If p_web.GSV('job:Warranty_job') = 'YES'
    loc:InvalidTab += 1
    do ValidateValue::tmp:WChargeType
    If loc:Invalid then exit.
    do ValidateValue::tmp:WRepairType
    If loc:Invalid then exit.
    do ValidateValue::tmp:WConfirmRepairType
    If loc:Invalid then exit.
  End
  ! The following fields are not on the form, but need to be checked anyway.
  if (p_web.GSV('job:Chargeable_Job') = 'YES')
      p_web.SSV('job:Charge_Type',p_web.GSV('tmp:CChargeType'))
      p_web.SSV('job:Repair_Type',p_web.GSV('tmp:CRepairType'))
  
      if (p_web.GSV('job:Estimate') <> 'YES') ! #11659 No need to set to estimate. If it already is. (Bryan: 31/08/2010)
          CASE vod.EstimateRequired(p_web.GSV('job:Charge_Type'),p_web.GSV('job:Account_Number'))
          OF 1 ! Make Esimate
              p_web.SSV('job:Estimate','YES')
          OF 2 ! Don't make esimate
              p_web.SSV('job:Estimate','NO')
          ELSE
          END
      END
  
  end ! if (p_web.GSV('job:Chargeable_Job') = 'YES')
  
  if (p_web.GSV('job:Warranty_job') = 'YES')
      if (p_web.GSV('job:Warranty_Charge_Type') <> p_web.GSV('tmp:WChargeType') Or |
          p_web.GSV('job:Repair_Type_Warranty') <> p_web.GSV('tmp:WRepairType'))
  
          p_web.SSV('job:Warranty_Charge_Type',p_web.GSV('tmp:WChargeType'))
          p_web.SSV('job:Repair_Type_Warranty',p_web.GSV('tmp:WRepairType'))
          p_web.SSV('JobPricingRoutine:ForceWarranty',1)
          JobPricingRoutine(p_web)
      end !
  end ! if (p_web.GSV('job:Warranty_job') = 'YES')
  
  p_web.SSV('jobe:COverwriteRepairType',p_web.GSV('tmp:CConfirmRepairType'))
  p_web.SSV('jobe:WOverwriteRepairType',p_web.GSV('tmp:WConfirmRepairType'))
  
  
  CalculateBilling(p_web)
  
  p_web.DeleteSessionValue('passedURL')
  
  
  
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('BillingConfirmation:Primed',0)
  p_web.StoreValue('tmp:CChargeType')
  p_web.StoreValue('tmp:CRepairType')
  p_web.StoreValue('tmp:CConfirmRepairType')
  p_web.StoreValue('tmp:WChargeType')
  p_web.StoreValue('tmp:WRepairType')
  p_web.StoreValue('tmp:WConfirmRepairType')

JobEstimateQuery     PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locEstimateReadyOption BYTE                                !
FilesOpened     Long
textQuery:IsInvalid  Long
locEstimateReadyOption:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('JobEstimateQuery')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'JobEstimateQuery_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('JobEstimateQuery','')
    p_web.DivHeader('JobEstimateQuery',p_web.combine(p_web.site.style.formdiv,'fdiv'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('JobEstimateQuery') = 0
        p_web.AddPreCall('JobEstimateQuery')
        p_web.DivHeader('popup_JobEstimateQuery','nt-hidden')
        p_web.DivHeader('JobEstimateQuery',p_web.combine(p_web.site.style.formdiv,'fdiv'))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_JobEstimateQuery_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_JobEstimateQuery_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferJobEstimateQuery',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_JobEstimateQuery',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferJobEstimateQuery',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_JobEstimateQuery',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferJobEstimateQuery',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_JobEstimateQuery',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_JobEstimateQuery',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferJobEstimateQuery',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_JobEstimateQuery',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferJobEstimateQuery',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_JobEstimateQuery',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_JobEstimateQuery',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('JobEstimateQuery')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('JobEstimateQuery_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'JobEstimateQuery'
    end
    p_web.formsettings.proc = 'JobEstimateQuery'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  if p_web.IfExistsValue('locEstimateReadyOption') = 0
    p_web.SetSessionValue('locEstimateReadyOption',locEstimateReadyOption)
  Else
    locEstimateReadyOption = p_web.GetSessionValue('locEstimateReadyOption')
  End

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locEstimateReadyOption')
    locEstimateReadyOption = p_web.GetValue('locEstimateReadyOption')
    p_web.SetSessionValue('locEstimateReadyOption',locEstimateReadyOption)
  Else
    locEstimateReadyOption = p_web.GetSessionValue('locEstimateReadyOption')
  End

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('JobEstimateQuery_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'ViewJob'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('JobEstimateQuery_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('JobEstimateQuery_ChainTo')
    loc:formaction = p_web.GetSessionValue('JobEstimateQuery_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'ViewJob'

GenerateForm   Routine
  do LoadRelatedRecords
 locEstimateReadyOption = p_web.RestoreValue('locEstimateReadyOption')
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Estimate Query') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Estimate Query',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_JobEstimateQuery',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_JobEstimateQuery0_div')&'">'&p_web.Translate('General')&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="JobEstimateQuery_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.cancel();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CancelButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="JobEstimateQuery_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'JobEstimateQuery_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="JobEstimateQuery_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'JobEstimateQuery_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_JobEstimateQuery')>0,p_web.GSV('showtab_JobEstimateQuery'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_JobEstimateQuery'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_JobEstimateQuery') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_JobEstimateQuery'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_JobEstimateQuery')>0,p_web.GSV('showtab_JobEstimateQuery'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_JobEstimateQuery') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('General') & ''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_JobEstimateQuery_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_JobEstimateQuery')>0,p_web.GSV('showtab_JobEstimateQuery'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"JobEstimateQuery",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_JobEstimateQuery')>0,p_web.GSV('showtab_JobEstimateQuery'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_JobEstimateQuery_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('JobEstimateQuery') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('JobEstimateQuery')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('General')&'</a></h3>' & CRLF & p_web.DivHeader('tab_JobEstimateQuery0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_JobEstimateQuery0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_JobEstimateQuery0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_JobEstimateQuery0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'General')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_JobEstimateQuery0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('General')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_JobEstimateQuery0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('General')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_JobEstimateQuery0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::textQuery
        do Comment::textQuery
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&100&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locEstimateReadyOption
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locEstimateReadyOption
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locEstimateReadyOption
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Validate::textQuery  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::textQuery  ! copies value to session value if valid.
  do Comment::textQuery ! allows comment style to be updated.

ValidateValue::textQuery  Routine
    If not (1=0)
    End

Value::textQuery  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobEstimateQuery_' & p_web._nocolon('textQuery') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="textQuery" class="'&clip('red bold')&'"'&clip(loc:extra)&'>' & p_web.Translate('The total value of this Estimate is less than the specified Estimate If Over value.<br><br>Do you want to continue and COMPLETE the repair, or create an ESTIMATE anyway?<br><br>',1) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::textQuery  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if textQuery:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('JobEstimateQuery_' & p_web._nocolon('textQuery') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locEstimateReadyOption  Routine
  packet = clip(packet) & p_web.DivHeader('JobEstimateQuery_' & p_web._nocolon('locEstimateReadyOption') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Select Option'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locEstimateReadyOption  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locEstimateReadyOption = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locEstimateReadyOption = p_web.GetValue('Value')
  End
  do ValidateValue::locEstimateReadyOption  ! copies value to session value if valid.
  do Value::locEstimateReadyOption
  do SendAlert
  do Comment::locEstimateReadyOption ! allows comment style to be updated.

ValidateValue::locEstimateReadyOption  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locEstimateReadyOption',locEstimateReadyOption).
    End

Value::locEstimateReadyOption  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobEstimateQuery_' & p_web._nocolon('locEstimateReadyOption') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If loc:retrying
    locEstimateReadyOption = p_web.RestoreValue('locEstimateReadyOption')
    do ValidateValue::locEstimateReadyOption
    If locEstimateReadyOption:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- RADIO --- locEstimateReadyOption
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locEstimateReadyOption') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locEstimateReadyOption'',''jobestimatequery_locestimatereadyoption_value'',1,'''&p_web._jsok(1)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locEstimateReadyOption',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locEstimateReadyOption_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Create Repair') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locEstimateReadyOption') = 2
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locEstimateReadyOption'',''jobestimatequery_locestimatereadyoption_value'',1,'''&p_web._jsok(2)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locEstimateReadyOption',clip(2),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locEstimateReadyOption_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Create Estimate') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locEstimateReadyOption') = 3
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locEstimateReadyOption'',''jobestimatequery_locestimatereadyoption_value'',1,'''&p_web._jsok(3)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locEstimateReadyOption',clip(3),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locEstimateReadyOption_3') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Do Nothing') & '<13,10>'
    packet = clip(packet) & p_web.br
  do SendPacket
  End
  p_web.DivFooter()
Comment::locEstimateReadyOption  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locEstimateReadyOption:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('JobEstimateQuery_' & p_web._nocolon('locEstimateReadyOption') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('JobEstimateQuery_nexttab_' & 0)
    locEstimateReadyOption = p_web.GSV('locEstimateReadyOption')
    do ValidateValue::locEstimateReadyOption
    If loc:Invalid
      loc:retrying = 1
      do Value::locEstimateReadyOption
      !do SendAlert
      do Comment::locEstimateReadyOption ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_JobEstimateQuery_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('JobEstimateQuery_tab_' & 0)
    do GenerateTab0
  of lower('JobEstimateQuery_locEstimateReadyOption_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locEstimateReadyOption
      of event:timer
        do Value::locEstimateReadyOption
        do Comment::locEstimateReadyOption
      else
        do Value::locEstimateReadyOption
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('JobEstimateQuery_form:ready_',1)

  p_web.SetSessionValue('JobEstimateQuery_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_JobEstimateQuery',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('JobEstimateQuery_form:ready_',1)
  p_web.SetSessionValue('JobEstimateQuery_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_JobEstimateQuery',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('JobEstimateQuery_form:ready_',1)
  p_web.SetSessionValue('JobEstimateQuery_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('JobEstimateQuery:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('JobEstimateQuery_form:ready_',1)
  p_web.SetSessionValue('JobEstimateQuery_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('JobEstimateQuery:Primed',0)
  p_web.setsessionvalue('showtab_JobEstimateQuery',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
      If not (1=0)
          If p_web.IfExistsValue('locEstimateReadyOption')
            locEstimateReadyOption = p_web.GetValue('locEstimateReadyOption')
          End
      End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('JobEstimateQuery_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
      if (p_web.GSV('locEstimateReadyOption') = 0)
          loc:alert = 'Please select an option'
          loc:invalid = 'locEstimateReadyOption'
          exit
      end ! if (p_web.GSV('locEstimateReadyOption') = 0)
  p_web.DeleteSessionValue('JobEstimateQuery_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
    do ValidateValue::textQuery
    If loc:Invalid then exit.
    do ValidateValue::locEstimateReadyOption
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('JobEstimateQuery:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('locEstimateReadyOption')

