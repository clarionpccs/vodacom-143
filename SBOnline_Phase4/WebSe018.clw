

   MEMBER('WebServer_Phase4.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSE018.INC'),ONCE        !Local module procedure declarations
                     END


ForceNetwork         PROCEDURE  (func:Type)                ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
  CODE
    If (GETINI('COMPULSORY','Network',,CLIP(PATH())&'\SB2KDEF.INI') = 'B' and func:Type = 'B') Or |
        (GETINI('COMPULSORY','Network',,CLIP(PATH())&'\SB2KDEF.INI') <> 'I' and func:Type = 'C')
        Return 1
    Else !If GETINI('COMPULSORY','Network',,CLIP(PATH())&'\SB2KDEF.INI') = 1
        Return 0
    End !If GETINI('COMPULSORY','Network',,CLIP(PATH())&'\SB2KDEF.INI') = 1
ForceMSN             PROCEDURE  (func:Manufacturer,func:Type) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
DEFAULTS::State  USHORT
FilesOpened     BYTE(0)
  CODE
    Do OpenFiles
    Do SaveFiles
    Access:DEFAULTS.Clearkey(def:RecordNumberKey)
    def:Record_Number = 1
    Set(def:RecordNumberKey,def:RecordNumberKey)
    Loop ! Begin Loop
        If Access:DEFAULTS.Next()
            Break
        End ! If Access:DEFAULTS.Next()
        Break
    End ! Loop

    Return# = 0
    If (def:Force_MSN = 'B' And func:Type = 'B') Or |
        (def:Force_MSN <> 'I' And func:Type = 'C')
        if (MSNRequired(func:Manufacturer))
            Return# = 1
        end !if (MSNRequired(func:Manufacturer) = 0)
    End!If def:Force_MSN = 'B'

    Do RestoreFiles
    Do CloseFiles
    Return Return#
!--------------------------------------
OpenFiles  ROUTINE
  Access:DEFAULTS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:DEFAULTS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:DEFAULTS.Close
     FilesOpened = False
  END
SaveFiles  ROUTINE
  DEFAULTS::State = Access:DEFAULTS.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF DEFAULTS::State <> 0
    Access:DEFAULTS.RestoreFile(DEFAULTS::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
ForceModelNumber     PROCEDURE  (func:Type)                ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
  CODE
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    !Model Number
    If (def:Force_Model_Number = 'B' And func:Type = 'B') Or |
        (def:Force_Model_Number <> 'I' And func:Type = 'C')
        Return Level:Fatal
    End!If def:Force_Model_Number = 'B'
    Return Level:Benign
ForceMobileNumber    PROCEDURE  (func:Type)                ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
DEFAULTS::State  USHORT
FilesOpened     BYTE(0)
  CODE
    Do OpenFiles
    Do SaveFiles
    Access:DEFAULTS.Clearkey(def:RecordNumberKeY)
    def:Record_Number = 1
    Set(def:RecordNumberKey,def:RecordNumberKey)
    Loop ! Begin Loop
        If Access:DEFAULTS.Next()
            Break
        End ! If Access:DEFAULTS.Next()
        Break
    End ! Loop

    Return# = 0
    !Force Mobile Number
    If (def:Force_Mobile_Number = 'B' And func:Type = 'B') Or |
        (def:Force_Mobile_Number <> 'I' And func:Type = 'C')
        Return# = 1
    End!If def:Force_Mobile_Number = 'B'

    Do RestoreFiles
    Do CloseFiles

    Return Return#
!--------------------------------------
OpenFiles  ROUTINE
  Access:DEFAULTS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:DEFAULTS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:DEFAULTS.Close
     FilesOpened = False
  END
SaveFiles  ROUTINE
  DEFAULTS::State = Access:DEFAULTS.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF DEFAULTS::State <> 0
    Access:DEFAULTS.RestoreFile(DEFAULTS::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
ForceLocation        PROCEDURE  (func:TransitType,func:Workshop,func:Type) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
  CODE
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    !Location
    access:trantype.clearkey(trt:transit_type_key)
    trt:transit_type = func:TransitType
    if access:trantype.fetch(trt:transit_type_key) = Level:Benign
        If trt:force_location = 'YES' and func:Workshop = 'YES'
            Return Level:Fatal
        End!If trt:force_location = 'YES' and job:location = ''
    end
    Return Level:Benign
