

   MEMBER('WebServer_Phase4.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSE029.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSE030.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE061.INC'),ONCE        !Req'd for module callout resolution
                     END


CloseWait            PROCEDURE  (NetWebServerWorker p_web)
! Use this procedure to "embed" html in other pages.
! on the web page use <!-- Net:CloseWait -->
!
! In this procedure set the packet string variable, and call the SendPacket routine.
!
! EXAMPLE:
! packet = '<strong>Hello World!</strong>'&CRLF
! do SendPacket
CRLF                    string('<13,10>')
NBSP                    string('&#160;')
packet                  string(NET:MaxBinData)
packetlen               long
timer                   long
loc:options             string(OptionsStringLen) ! options for jQuery calls
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  GlobalErrors.SetProcedureName('CloseWait')
  If p_web.RequestAjax = 1
    GlobalErrors.SetProcedureName()
    Return
  End
    !%SecwinCtrlsDisplay = 0 ; %SecwinAccessGroupsNotCreated = 0
!----------- put your html code here -----------------------------------
  do SendPacket
  Do CloseWait
  do SendPacket
!----------- end of custom code ----------------------------------------
  do SendPacket
  GlobalErrors.SetProcedureName()
  Return

!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet,1,packetlen,NET:NoHeader)
    packet = ''
  end
CloseWait  Routine
  packet = clip(packet) & p_web.AsciiToUTF(|
    '<<script type="text/javascript"><13,10>'&|
    'document.getElementById("waitframe").style.display="none";<13,10>'&|
    '<</script><13,10>'&|
    '',net:OnlyIfUTF)
BannerBlank          PROCEDURE  (NetWebServerWorker p_web)
! Use this procedure to "embed" html in other pages.
! on the web page use <!-- Net:BannerBlank -->
!
! In this procedure set the packet string variable, and call the SendPacket routine.
!
! EXAMPLE:
! packet = '<strong>Hello World!</strong>'&CRLF
! do SendPacket
loc:divname       string(255)
loc:parent        string(255)
CRLF                    string('<13,10>')
NBSP                    string('&#160;')
packet                  string(NET:MaxBinData)
packetlen               long
timer                   long
loc:options             string(OptionsStringLen) ! options for jQuery calls
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  GlobalErrors.SetProcedureName('BannerBlank')
  loc:parent = p_web.GetValue('_ParentProc')
  If loc:parent <> ''
    loc:divname = 'BannerBlank' & '_' & loc:parent
  Else
    loc:divname = 'BannerBlank'
  End
    !%SecwinCtrlsDisplay = 0 ; %SecwinAccessGroupsNotCreated = 0
  p_web.DivHeader(loc:divname,'nt-center-red')
!----------- put your html code here -----------------------------------
  do SendPacket
  Do banner
  do SendPacket
!----------- end of custom code ----------------------------------------
  do SendPacket
  p_web.DivFooter()
    packet = clip(packet) & '<!-- Net:Busy --><13,10>'
    packet = clip(packet) & '<!-- Net:Message --><13,10>'
    do SendPacket
  GlobalErrors.SetProcedureName()
  Return

!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet,1,packetlen,NET:NoHeader)
    packet = ''
  end
banner  Routine
  packet = clip(packet) & p_web.AsciiToUTF(|
    '<<img height="40" width="200" src="/images/topbanner.gif"><13,10>'&|
    '',net:OnlyIfUTF)
PageFooter           PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
FilesOpened     Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  GlobalErrors.SetProcedureName('PageFooter')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'PageFooter_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('PageFooter','')
    p_web.DivHeader('PageFooter',p_web.combine(p_web.site.style.formdiv,'fdiv'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('PageFooter') = 0
        p_web.AddPreCall('PageFooter')
        p_web.DivHeader('popup_PageFooter','nt-hidden')
        p_web.DivHeader('PageFooter',p_web.combine(p_web.site.style.formdiv,'fdiv'))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_PageFooter_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_PageFooter_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferPageFooter',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_PageFooter',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPageFooter',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_PageFooter',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPageFooter',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_PageFooter',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_PageFooter',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPageFooter',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_PageFooter',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPageFooter',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_PageFooter',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_PageFooter',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('PageFooter')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('PageFooter_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'PageFooter'
    end
    p_web.formsettings.proc = 'PageFooter'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine

AfterLookup Routine
  loc:TabNumber = -1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('PageFooter_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferPageFooter')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('PageFooter_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('PageFooter_ChainTo')
    loc:formaction = p_web.GetSessionValue('PageFooter_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction

GenerateForm   Routine
  do LoadRelatedRecords
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
    do SendPacket
    Do heading
    do SendPacket
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
    Packet = clip(Packet) & p_web.DivHeader('Tab_PageFooter',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="PageFooter_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      packet = clip(packet) & '</div><13,10>' ! end id="PageFooter_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'PageFooter_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="PageFooter_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'PageFooter_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_PageFooter')>0,p_web.GSV('showtab_PageFooter'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_PageFooter'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_PageFooter') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_PageFooter'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_PageFooter')>0,p_web.GSV('showtab_PageFooter'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_PageFooter') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_PageFooter_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_PageFooter')>0,p_web.GSV('showtab_PageFooter'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"PageFooter",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_PageFooter')>0,p_web.GSV('showtab_PageFooter'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_PageFooter_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('PageFooter') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('PageFooter')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine


NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_PageFooter_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('PageFooter_form:ready_',1)

  p_web.SetSessionValue('PageFooter_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_PageFooter',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('PageFooter_form:ready_',1)
  p_web.SetSessionValue('PageFooter_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_PageFooter',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('PageFooter_form:ready_',1)
  p_web.SetSessionValue('PageFooter_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('PageFooter:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('PageFooter_form:ready_',1)
  p_web.SetSessionValue('PageFooter_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('PageFooter:Primed',0)
  p_web.setsessionvalue('showtab_PageFooter',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('PageFooter_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('PageFooter_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('PageFooter:Primed',0)

heading  Routine
  packet = clip(packet) & p_web.AsciiToUTF(|
    '<<div id="_VersionText" class="BottomBannerText"><13,10>'&|
    '<<span class="SmallText"><<!-- Net:s:VersionNumber --><</span><13,10>'&|
    '<</div><13,10>'&|
    '',net:OnlyIfUTF)
PrintRoutines        PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locJobNumber         STRING(20)                            !
locWaybillNumber     STRING(30)                            !
FilesOpened     Long
TRADEACC::State  USHORT
WEBJOB::State  USHORT
JOBSE::State  USHORT
JOBS::State  USHORT
WAYBILLJ::State  USHORT
JOBNOTES::State  USHORT
locJobNumber:IsInvalid  Long
buttonPrintJobCard:IsInvalid  Long
buttonPrintEstimate:IsInvalid  Long
buttonPrintDespatchNote:IsInvalid  Long
locWaybillNumber:IsInvalid  Long
buttonPrintWaybill:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('PrintRoutines')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'PrintRoutines_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('PrintRoutines','')
    p_web.DivHeader('PrintRoutines',p_web.combine(p_web.site.style.formdiv,))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('PrintRoutines') = 0
        p_web.AddPreCall('PrintRoutines')
        p_web.DivHeader('popup_PrintRoutines','nt-hidden')
        p_web.DivHeader('PrintRoutines',p_web.combine(p_web.site.style.formdiv,))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_PrintRoutines_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_PrintRoutines_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferPrintRoutines',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_PrintRoutines',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPrintRoutines',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_PrintRoutines',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPrintRoutines',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_PrintRoutines',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_PrintRoutines',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPrintRoutines',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_PrintRoutines',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPrintRoutines',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_PrintRoutines',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_PrintRoutines',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('PrintRoutines')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
HideButtons     routine
    p_web.SSV('Hide:PrintJobCard',1)
    p_web.SSV('Hide:PrintEstimate',1)
    p_web.SSV('Hide:PrintDespatchNote',1)
HideWaybillButtons  ROUTINE
    p_web.SSV('Hide:PrintWaybill',1)
OpenFiles  ROUTINE
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(WEBJOB)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(WAYBILLJ)
  p_web._OpenFile(JOBNOTES)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(WAYBILLJ)
  p_Web._CloseFile(JOBNOTES)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('PrintRoutines_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'PrintRoutines'
    end
    p_web.formsettings.proc = 'PrintRoutines'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  if p_web.IfExistsValue('locJobNumber') = 0
    p_web.SetSessionValue('locJobNumber',locJobNumber)
  Else
    locJobNumber = p_web.GetSessionValue('locJobNumber')
  End
  if p_web.IfExistsValue('locWaybillNumber') = 0
    p_web.SetSessionValue('locWaybillNumber',locWaybillNumber)
  Else
    locWaybillNumber = p_web.GetSessionValue('locWaybillNumber')
  End

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locJobNumber')
    locJobNumber = p_web.GetValue('locJobNumber')
    p_web.SetSessionValue('locJobNumber',locJobNumber)
  Else
    locJobNumber = p_web.GetSessionValue('locJobNumber')
  End
  if p_web.IfExistsValue('locWaybillNumber')
    locWaybillNumber = p_web.GetValue('locWaybillNumber')
    p_web.SetSessionValue('locWaybillNumber',locWaybillNumber)
  Else
    locWaybillNumber = p_web.GetSessionValue('locWaybillNumber')
  End

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('PrintRoutines_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'IndexPage'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('PrintRoutines_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('PrintRoutines_ChainTo')
    loc:formaction = p_web.GetSessionValue('PrintRoutines_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction

GenerateForm   Routine
  do LoadRelatedRecords
  p_web.site.SaveButton.TextValue = 'OK'
  
  if (p_web.GSV('PrintRoutines:SecondTime') = 0)
      p_web.SSV('locJObNumber','')
      p_web.SSV('Comment:JobNumber','Enter Job Number and press [TAB]')
      do HideButtons
      p_web.SSV('PrintRoutines:SecondTime',1)
      p_web.SSV('locWaybillNumber','')
      p_web.SSV('Comment:WaybillNumber','Enter Waybill Number and press [TAB]')
      do HideWaybillButtons
  end ! if (p_web.GSV('PrintRoutines:SecondTime') = 0)
 locJobNumber = p_web.RestoreValue('locJobNumber')
 locWaybillNumber = p_web.RestoreValue('locWaybillNumber')
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Print Routines') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Print Routines',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_PrintRoutines',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_PrintRoutines0_div')&'">'&p_web.Translate('Enter Job Number')&'</a></li>'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_PrintRoutines1_div')&'">'&p_web.Translate()&'</a></li>'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_PrintRoutines2_div')&'">'&p_web.Translate('Select Waybill Number')&'</a></li>'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_PrintRoutines3_div')&'">'&p_web.Translate()&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  do GenerateTab3
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="PrintRoutines_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="PrintRoutines_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'PrintRoutines_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="PrintRoutines_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'PrintRoutines_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locJobNumber')
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_PrintRoutines')>0,p_web.GSV('showtab_PrintRoutines'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_PrintRoutines'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_PrintRoutines') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_PrintRoutines'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_PrintRoutines')>0,p_web.GSV('showtab_PrintRoutines'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_PrintRoutines') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Enter Job Number') & ''''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Select Waybill Number') & ''''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_PrintRoutines_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_PrintRoutines')>0,p_web.GSV('showtab_PrintRoutines'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"PrintRoutines",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_PrintRoutines')>0,p_web.GSV('showtab_PrintRoutines'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_PrintRoutines_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('PrintRoutines') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('PrintRoutines')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Enter Job Number')&'</a></h3>' & CRLF & p_web.DivHeader('tab_PrintRoutines0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_PrintRoutines0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_PrintRoutines0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_PrintRoutines0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Enter Job Number')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_PrintRoutines0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Enter Job Number')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_PrintRoutines0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Enter Job Number')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_PrintRoutines0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locJobNumber
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locJobNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locJobNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab1  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate()&'</a></h3>' & CRLF & p_web.DivHeader('tab_PrintRoutines1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_PrintRoutines1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_PrintRoutines1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_PrintRoutines1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,)
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_PrintRoutines1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend)
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_PrintRoutines1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain">' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_PrintRoutines1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::buttonPrintJobCard
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::buttonPrintJobCard
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::buttonPrintJobCard
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::buttonPrintEstimate
        do Comment::buttonPrintEstimate
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
      If p_web.GSV('PH') > 2
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::buttonPrintDespatchNote
        do Comment::buttonPrintDespatchNote
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab2  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Select Waybill Number')&'</a></h3>' & CRLF & p_web.DivHeader('tab_PrintRoutines2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_PrintRoutines2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_PrintRoutines2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_PrintRoutines2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Select Waybill Number')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_PrintRoutines2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Select Waybill Number')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_PrintRoutines2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Select Waybill Number')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_PrintRoutines2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locWaybillNumber
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locWaybillNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locWaybillNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab3  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate()&'</a></h3>' & CRLF & p_web.DivHeader('tab_PrintRoutines3',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_PrintRoutines3',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_PrintRoutines3',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_PrintRoutines3',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,)
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_PrintRoutines3',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend)
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_PrintRoutines3',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain">' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_PrintRoutines3',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::buttonPrintWaybill
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::buttonPrintWaybill
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::buttonPrintWaybill
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Prompt::locJobNumber  Routine
  packet = clip(packet) & p_web.DivHeader('PrintRoutines_' & p_web._nocolon('locJobNumber') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Job Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locJobNumber  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locJobNumber = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locJobNumber = p_web.GetValue('Value')
  End
  do ValidateValue::locJobNumber  ! copies value to session value if valid.
  Access:JOBS.Clearkey(job:ref_Number_Key)
  job:ref_Number    = p_web.GSV('locJobNumber')
  if (Access:JOBS.TryFetch(job:ref_Number_Key) = Level:Benign)
      ! Found
      p_web.FileToSessionQueue(JOBS)
  
  
      Access:JOBNOTES.Clearkey(jbn:refNumberKey)
      jbn:refNumber    = job:Ref_Number
      if (Access:JOBNOTES.TryFetch(jbn:refNumberKey) = Level:Benign)
          ! Found
          p_web.FileToSessionQueue(JOBNOTES)
      else ! if (Access:JOBNOTES.TryFetch(jbn:refNumberKey) = Level:Benign)
          ! Error
      end ! if (Access:JOBNOTES.TryFetch(jbn:refNumberKey) = Level:Benign)
  
  
      Access:WEBJOB.Clearkey(wob:refNumberKey)
      wob:refNumber    = job:Ref_Number
      if (Access:WEBJOB.TryFetch(wob:refNumberKey) = Level:Benign)
          ! Found
          p_web.FileToSessionQueue(WEBJOB)
      else ! if (Access:WEBJOB.TryFetch(wob:refNumberKey) = Level:Benign)
          ! Error
      end ! if (Access:WEBJOB.TryFetch(wob:refNumberKey) = Level:Benign)
  
      Access:JOBSE.Clearkey(jobe:refNumberKey)
      jobe:refNumber    = job:Ref_Number
      if (Access:JOBSE.TryFetch(jobe:refNumberKey) = Level:Benign)
          ! Found
          p_web.FileToSessionQueue(JOBSE)
      else ! if (Access:JOBSE.TryFetch(jobe:refNumberKey) = Level:Benign)
          ! Error
      end ! if (Access:JOBSE.TryFetch(jobe:refNumberKey) = Level:Benign)
  
      if (job:Estimate = 'YES')
          p_web.SSV('Hide:PrintEstimate',0)
      end ! if (job:Estimate = 'YES')
  
      p_web.SSV('Hide:PrintJObCard',0)
  
      p_web.SSV('Hide:PrintDespatchNote',0)
  
      p_web.SSV('job:Ref_Number',job:Ref_Number)
      p_web.SSV('tmp:JobNumber',job:Ref_Number)
      p_web.SSV('Comment:JobNumber','Job Found')
  else ! if (Access:JOBS.TryFetch(job:ref_Number_Key) = Level:Benign)
      ! Error
      p_web.SSV('Comment:JobNumber','Cannot find the selected Job Number')
      do HideButtons
  end ! if (Access:JOBS.TryFetch(job:ref_Number_Key) = Level:Benign)
  do Value::locJobNumber
  do SendAlert
  do Comment::locJobNumber ! allows comment style to be updated.
  do Value::buttonPrintEstimate  !1
  do Comment::locJobNumber
  do Value::buttonPrintJobCard  !1
  do Value::buttonPrintDespatchNote  !1

ValidateValue::locJobNumber  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locJobNumber',locJobNumber).
    End

Value::locJobNumber  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PrintRoutines_' & p_web._nocolon('locJobNumber') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    locJobNumber = p_web.RestoreValue('locJobNumber')
    do ValidateValue::locJobNumber
    If locJobNumber:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- locJobNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locJobNumber'',''printroutines_locjobnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locJobNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locJobNumber',p_web.GetSessionValueFormat('locJobNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locJobNumber  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locJobNumber:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:JobNumber'))
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PrintRoutines_' & p_web._nocolon('locJobNumber') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::buttonPrintJobCard  Routine
  packet = clip(packet) & p_web.DivHeader('PrintRoutines_' & p_web._nocolon('buttonPrintJobCard') & '_prompt',Choose(p_web.GSV('Hide:PrintJobCard') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:PrintJobCard') = 1,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::buttonPrintJobCard  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonPrintJobCard  ! copies value to session value if valid.
  do Comment::buttonPrintJobCard ! allows comment style to be updated.

ValidateValue::buttonPrintJobCard  Routine
    If not (p_web.GSV('Hide:PrintJobCard') = 1)
    End

Value::buttonPrintJobCard  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:PrintJobCard') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PrintRoutines_' & p_web._nocolon('buttonPrintJobCard') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('Hide:PrintJobCard') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','PrintJobCard','Job Card',p_web.combine(Choose('Job Card' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.WindowOpen(clip('JobCard')&''&'','_blank'),,loc:disabled,'images/printer.png',,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonPrintJobCard  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonPrintJobCard:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:PrintJobCard') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PrintRoutines_' & p_web._nocolon('buttonPrintJobCard') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:PrintJobCard') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonPrintEstimate  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonPrintEstimate  ! copies value to session value if valid.
  do Comment::buttonPrintEstimate ! allows comment style to be updated.

ValidateValue::buttonPrintEstimate  Routine
    If not (p_web.GSV('Hide:PrintEstimate') = 1)
    End

Value::buttonPrintEstimate  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:PrintEstimate') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PrintRoutines_' & p_web._nocolon('buttonPrintEstimate') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('Hide:PrintEstimate') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','PrintEstimate','Estimate',p_web.combine(Choose('Estimate' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.WindowOpen(clip('Estimate')&''&'','_blank'),,loc:disabled,'images/printer.png',,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonPrintEstimate  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonPrintEstimate:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:PrintEstimate') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PrintRoutines_' & p_web._nocolon('buttonPrintEstimate') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:PrintEstimate') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonPrintDespatchNote  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonPrintDespatchNote  ! copies value to session value if valid.
  do Comment::buttonPrintDespatchNote ! allows comment style to be updated.

ValidateValue::buttonPrintDespatchNote  Routine
  If p_web.GSV('PH') > 2
    If not (p_web.GSV('Hide:PrintDespatchNote') = 1)
    End
  End

Value::buttonPrintDespatchNote  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:PrintDespatchNote') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PrintRoutines_' & p_web._nocolon('buttonPrintDespatchNote') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('Hide:PrintDespatchNote') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','PrintDespatch','Despatch Note',p_web.combine(Choose('Despatch Note' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.WindowOpen(clip('DespatchNote')&''&'','_blank'),,loc:disabled,'images/printer.png',,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonPrintDespatchNote  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonPrintDespatchNote:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:PrintDespatchNote') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PrintRoutines_' & p_web._nocolon('buttonPrintDespatchNote') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:PrintDespatchNote') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locWaybillNumber  Routine
  packet = clip(packet) & p_web.DivHeader('PrintRoutines_' & p_web._nocolon('locWaybillNumber') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Waybill Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locWaybillNumber  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locWaybillNumber = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locWaybillNumber = p_web.GetValue('Value')
  End
  do ValidateValue::locWaybillNumber  ! copies value to session value if valid.
  Access:WAYBILLJ.Clearkey(waj:DescWaybillNoKey)
  waj:WaybillNumber = p_web.GSV('locWaybillNumber')
  IF (Access:WAYBILLJ.TryFetch(waj:DescWaybillNoKey))
      p_web.SSV('Comment:WaybillNumber','Cannot find the selected Waybill Number')
      do HideWaybillButtons
  ELSE
  
      Access:WAYBILLS.ClearKey(way:WayBillNumberKey)
      way:WayBillNumber = waj:WayBillNumber
      IF (Access:WAYBILLS.TryFetch(way:WayBillNumberKey))
          p_web.SSV('Comment:WaybillNumber','Cannot find the selected Waybill Number')
          do HideWaybillButtons
  
      ELSE
          ! Get the details from the first job
          Access:JOBS.ClearKey(job:Ref_Number_Key)
          job:Ref_Number = waj:JobNumber
          IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
              p_web.SSV('Comment:WaybillNumber','Cannot find the job attached to the selected Waybill Number')
              do HideWaybillButtons
          END
  
          Access:JOBSE.ClearKey(jobe:RefNumberKey)
          jobe:RefNumber = job:Ref_Number
          IF (Access:JOBSE.TryFetch(jobe:RefNumberKey))
              p_web.SSV('Comment:WaybillNumber','Cannot find the job attached to the selected Waybill Number')
              do HideWaybillButtons
          END
  
          Access:WEBJOB.ClearKey(wob:RefNumberKey)
          wob:RefNumber = job:Ref_Number
          IF (Access:WEBJOB.TryFetch(wob:RefNumberKey))
              p_web.SSV('Comment:WaybillNumber','Cannot find the job attached to the selected Waybill Number')
              do HideWaybillButtons
          END
  
  
          error# = 0
          IF (p_web.GSV('BookingSite') = 'RRC')
              Access:TRADEACC.ClearKey(tra:Account_Number_Key)
              tra:Account_Number = p_web.GSV('BookingAccount')
              IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
              END
  
              IF (way:FromAccount <> p_web.GSV('BookingAccount'))
                  p_web.SSV('Comment:WaybillNumber','The selected waybill was not created at this site.')
                  do HideWaybillButtons
                  error# = 1
              ELSE
                  CASE way:WayBillType
                  OF 1 ! RRC to ARC
                      p_web.SSV('Waybill:ToAccount',p_web.GSV('ARC:AccountNumber'))
                      p_web.SSV('Waybill:ToType','TRA')
                  OF 2 !RRC to Customer
                      p_web.SSV('Waybill:ToAccount','')
                      p_web.SSV('Waybill:ToType','CUS')
                  OF 3 ! RRC Exchange to Customer
                      p_web.SSV('Waybill:ToAccount','')
                      p_web.SSV('Waybill:ToType','CUS')
                  ELSE
                      IF (jobe:Sub_Sub_Account <> '')
                          p_web.SSV('Waybill:ToAccount',jobe:Sub_Sub_Account)
                      ELSE
                          p_web.SSV('Waybill:ToAccount',job:Account_Number)
                      END
                      p_web.SSV('Waybill:ToType','SUB')
                      p_web.SSV('Hide:PrintWaybill',0)
                      p_web.SSV('Comment:WaybillNumber','Waybill Found')
                  END
                  If GETINI('DESPATCH','OVERWRITECOURIER','N',CLIP(PATH()) & '\SB2KDEF.INI') <> 'Y'
                      p_web.SSV('Waybill:Courier',GETINI('DESPATCH','OVERWRITECOURIER','N',CLIP(PATH()) & '\SB2KDEF.INI'))
                  ELSE
                      p_web.SSV('Waybill:Courier',tra:Courier_Outgoing)
                  END
                  p_web.SSV('Waybill:FromType','TRA')
                  p_web.SSV('Waybill:FromAccount',p_web.GSV('BookingAccount'))
              END
          ELSE ! IF (p_web.GSV('BookingSite') = 'RRC')
              IF (way:FromAccount <> p_web.GSV('ARC:AccountNumber'))
                  p_web.SSV('Comment:WaybillNumber','The selected waybill was not created at this site.')
                  do HideWaybillButtons
              ELSE
  
                  Access:TRADEACC.ClearKey(tra:Account_Number_Key)
                  tra:Account_Number = p_web.GSV('ARC:AccountNumber')
                  IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
                  END
  
                  p_web.SSV('Waybill:FromAccount',p_web.GSV('ARC:AccountNumber'))
                  IF (jobe:WebJob = 1)
                      p_web.SSV('Waybill:ToAccount',wob:HeadAccountNumber)
                      p_web.SSV('Waybill:ToType','TRA')
                  ELSE
                      p_web.SSV('Waybill:ToAccount',job:Account_Number)
                      p_web.SSV('Waybill:ToType','SUB')
                  END
                  If GETINI('DESPATCH','OVERWRITECOURIER','N',CLIP(PATH()) & '\SB2KDEF.INI') <> 'Y'
                      p_web.SSV('Waybill:Courier',GETINI('DESPATCH','OVERWRITECOURIER','N',CLIP(PATH()) & '\SB2KDEF.INI'))
                  ELSE
                      p_web.SSV('Waybill:Courier',tra:Courier_Outgoing)
                  END
                  p_web.SSV('Hide:PrintWaybill',0)
                  p_web.SSV('Comment:WaybillNumber','Waybill Found')
              END
          END
  
      end
  
  END
  do Value::locWaybillNumber
  do SendAlert
  do Comment::locWaybillNumber ! allows comment style to be updated.
  do Comment::locWaybillNumber
  do Value::buttonPrintWaybill  !1

ValidateValue::locWaybillNumber  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locWaybillNumber',locWaybillNumber).
    End

Value::locWaybillNumber  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PrintRoutines_' & p_web._nocolon('locWaybillNumber') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    locWaybillNumber = p_web.RestoreValue('locWaybillNumber')
    do ValidateValue::locWaybillNumber
    If locWaybillNumber:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- locWaybillNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locWaybillNumber'',''printroutines_locwaybillnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locWaybillNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locWaybillNumber',p_web.GetSessionValueFormat('locWaybillNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locWaybillNumber  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locWaybillNumber:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:WaybillNumber'))
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PrintRoutines_' & p_web._nocolon('locWaybillNumber') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::buttonPrintWaybill  Routine
  packet = clip(packet) & p_web.DivHeader('PrintRoutines_' & p_web._nocolon('buttonPrintWaybill') & '_prompt',Choose(p_web.GSV('Hide:PrintWaybill') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:PrintWaybill') = 1,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::buttonPrintWaybill  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonPrintWaybill  ! copies value to session value if valid.
  do Comment::buttonPrintWaybill ! allows comment style to be updated.

ValidateValue::buttonPrintWaybill  Routine
    If not (p_web.GSV('Hide:PrintWaybill') = 1)
    End

Value::buttonPrintWaybill  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:PrintWaybill') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PrintRoutines_' & p_web._nocolon('buttonPrintWaybill') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('Hide:PrintWaybill') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','PrintWaybill','Print Waybill',p_web.combine(Choose('Print Waybill' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.WindowOpen(clip('Waybill')&''&'','_blank'),,loc:disabled,'images/printer.png',,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonPrintWaybill  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonPrintWaybill:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:PrintWaybill') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PrintRoutines_' & p_web._nocolon('buttonPrintWaybill') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:PrintWaybill') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('PrintRoutines_nexttab_' & 0)
    locJobNumber = p_web.GSV('locJobNumber')
    do ValidateValue::locJobNumber
    If loc:Invalid
      loc:retrying = 1
      do Value::locJobNumber
      !do SendAlert
      do Comment::locJobNumber ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('PrintRoutines_nexttab_' & 1)
    If loc:Invalid then exit.
  of lower('PrintRoutines_nexttab_' & 2)
    locWaybillNumber = p_web.GSV('locWaybillNumber')
    do ValidateValue::locWaybillNumber
    If loc:Invalid
      loc:retrying = 1
      do Value::locWaybillNumber
      !do SendAlert
      do Comment::locWaybillNumber ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('PrintRoutines_nexttab_' & 3)
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_PrintRoutines_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('PrintRoutines_tab_' & 0)
    do GenerateTab0
  of lower('PrintRoutines_locJobNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locJobNumber
      of event:timer
        do Value::locJobNumber
        do Comment::locJobNumber
      else
        do Value::locJobNumber
      end
  of lower('PrintRoutines_tab_' & 1)
    do GenerateTab1
  of lower('PrintRoutines_tab_' & 2)
    do GenerateTab2
  of lower('PrintRoutines_locWaybillNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locWaybillNumber
      of event:timer
        do Value::locWaybillNumber
        do Comment::locWaybillNumber
      else
        do Value::locWaybillNumber
      end
  of lower('PrintRoutines_tab_' & 3)
    do GenerateTab3
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('PrintRoutines_form:ready_',1)

  p_web.SetSessionValue('PrintRoutines_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_PrintRoutines',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('PrintRoutines_form:ready_',1)
  p_web.SetSessionValue('PrintRoutines_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_PrintRoutines',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('PrintRoutines_form:ready_',1)
  p_web.SetSessionValue('PrintRoutines_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('PrintRoutines:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('PrintRoutines_form:ready_',1)
  p_web.SetSessionValue('PrintRoutines_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('PrintRoutines:Primed',0)
  p_web.setsessionvalue('showtab_PrintRoutines',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
      If not (1=0)
          If p_web.IfExistsValue('locJobNumber')
            locJobNumber = p_web.GetValue('locJobNumber')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('locWaybillNumber')
            locWaybillNumber = p_web.GetValue('locWaybillNumber')
          End
      End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('PrintRoutines_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('PrintRoutines_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
    do ValidateValue::locJobNumber
    If loc:Invalid then exit.
  ! tab = 2
    loc:InvalidTab += 1
    do ValidateValue::buttonPrintJobCard
    If loc:Invalid then exit.
    do ValidateValue::buttonPrintEstimate
    If loc:Invalid then exit.
    do ValidateValue::buttonPrintDespatchNote
    If loc:Invalid then exit.
  ! tab = 4
    loc:InvalidTab += 1
    do ValidateValue::locWaybillNumber
    If loc:Invalid then exit.
  ! tab = 5
    loc:InvalidTab += 1
    do ValidateValue::buttonPrintWaybill
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
      p_web.deleteSessionValue('PrintRoutines:SecondTime')
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('PrintRoutines:Primed',0)
  p_web.StoreValue('locJobNumber')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locWaybillNumber')
  p_web.StoreValue('')

AmendAddress         PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
VariablesGroup       GROUP,PRE(tmp)                        !
CompanyName          STRING(30)                            !TempCompanyName
AddressLine1         STRING(30)                            !TempAddressLine1
AddressLine2         STRING(30)                            !TempAddressLine2
Suburb               STRING(30)                            !TempSuburb
Postcode             STRING(30)                            !TempPostcode
TelephoneNumber      STRING(30)                            !TempTelephoneNumber
FaxNumber            STRING(30)                            !TempFaxNumber
EmailAddress         STRING(255)                           !Email Address
EndUserTelephoneNumber STRING(30)                          !End User Tel No
IDNumber             STRING(30)                            !ID Number
SMSNotification      BYTE(0)                               !SMS Notification
NotificationMobileNumber STRING(30)                        !NotificationMobileNumber
EmailNotification    BYTE(0)                               !EmailNotification
NotificationEmailAddress STRING(255)                       !Notification Email Address
CompanyNameDelivery  STRING(30)                            !tmp:CompanyNameDelivery
AddressLine1Delivery STRING(30)                            !tmp:AddressLine1Delivery
AddressLine2Delivery STRING(30)                            !AddressLine2Delivery
SuburbDelivery       STRING(30)                            !SubrubDelivery
PostcodeDelivery     STRING(30)                            !PostcodeDelivery
TelephoneNumberDelivery STRING(30)                         !TelephoneNumberDelivery
CompanyNameCollection STRING(30)                           !CompanyNameCollection
AddressLine1Collection STRING(30)                          !AddressLine1Collection
AddressLine2Collection STRING(30)                          !AddressLine2Collection
SuburbCollection     STRING(30)                            !SuburbCollection
PostcodeCollection   STRING(30)                            !PostcodeCollection
TelephoneNumberCollection STRING(30)                       !TelephoneNumberCollection
HubCustomer          STRING(30)                            !Hub
HubCollection        STRING(30)                            !Hub
HubDelivery          STRING(30)                            !Hub
                     END                                   !
ReplicateCustomerToDelivery BYTE(0)                        !ReplicateCustomerToDelivery
ReplicateCustomerToCollection BYTE(0)                      !ReplicateCustomerToCollection
ReplicateCollectionToDelivery BYTE(0)                      !ReplicateCollectionToDelivery
ReplicateDeliveryToCollection BYTE(0)                      !ReplicateDeliveryToCollection
FilesOpened     Long
SUBTRACC::State  USHORT
COURIER::State  USHORT
JOBNOTES::State  USHORT
SUBACCAD::State  USHORT
SUBURB::State  USHORT
JOBS::State  USHORT
JOBSE::State  USHORT
WEBJOB::State  USHORT
JOBSE2::State  USHORT
TRANTYPE::State  USHORT
txtEndUserAddress:IsInvalid  Long
txtCollectionAddress:IsInvalid  Long
txtDeliveryAddress:IsInvalid  Long
job:Company_Name:IsInvalid  Long
job:Company_Name_Collection:IsInvalid  Long
job:Company_Name_Delivery:IsInvalid  Long
job:Address_Line1:IsInvalid  Long
job:Address_Line1_Collection:IsInvalid  Long
job:Address_Line1_Delivery:IsInvalid  Long
job:Address_Line2:IsInvalid  Long
job:Address_Line2_Collection:IsInvalid  Long
job:Address_Line2_Delivery:IsInvalid  Long
job:Address_Line3:IsInvalid  Long
job:Address_Line3_Collection:IsInvalid  Long
job:Address_Line3_Delivery:IsInvalid  Long
jobe2:HubCustomer:IsInvalid  Long
jobe2:HubCollection:IsInvalid  Long
jobe2:HubDelivery:IsInvalid  Long
job:Postcode:IsInvalid  Long
job:Postcode_Collection:IsInvalid  Long
job:Postcode_Delivery:IsInvalid  Long
job:Telephone_Number:IsInvalid  Long
job:Telephone_Collection:IsInvalid  Long
job:Telephone_Delivery:IsInvalid  Long
job:Fax_Number:IsInvalid  Long
btnCopyCollectionAddress:IsInvalid  Long
btnCopyDeliveryAddress:IsInvalid  Long
jobe:EndUserEmailAddress:IsInvalid  Long
jobe:EndUserTelNo:IsInvalid  Long
jbn:Collection_Text:IsInvalid  Long
jbn:Delivery_Text:IsInvalid  Long
jobe:VatNumber:IsInvalid  Long
jobe2:IDNumber:IsInvalid  Long
jobe2:SMSNotification:IsInvalid  Long
jobe2:SMSAlertNumber:IsInvalid  Long
btnCollectionText:IsInvalid  Long
btnDeliveryText:IsInvalid  Long
jobe2:EmailNotification:IsInvalid  Long
jobe2:EmailAlertAddress:IsInvalid  Long
jobe2:CourierWaybillNumber:IsInvalid  Long
jobe:Sub_Sub_Account:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('AmendAddress')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'AmendAddress_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('AmendAddress','')
    p_web.DivHeader('AmendAddress',p_web.combine(p_web.site.style.formdiv,))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('AmendAddress') = 0
        p_web.AddPreCall('AmendAddress')
        p_web.DivHeader('popup_AmendAddress','nt-hidden')
        p_web.DivHeader('AmendAddress',p_web.combine(p_web.site.style.formdiv,))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(900)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_AmendAddress_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_AmendAddress_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferAmendAddress',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_AmendAddress',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferAmendAddress',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_AmendAddress',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferAmendAddress',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_AmendAddress',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_AmendAddress',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferAmendAddress',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_AmendAddress',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferAmendAddress',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_AmendAddress',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_AmendAddress',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('AmendAddress')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(SUBTRACC)
  p_web._OpenFile(COURIER)
  p_web._OpenFile(JOBNOTES)
  p_web._OpenFile(SUBACCAD)
  p_web._OpenFile(SUBURB)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(WEBJOB)
  p_web._OpenFile(JOBSE2)
  p_web._OpenFile(TRANTYPE)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(COURIER)
  p_Web._CloseFile(JOBNOTES)
  p_Web._CloseFile(SUBACCAD)
  p_Web._CloseFile(SUBURB)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(JOBSE2)
  p_Web._CloseFile(TRANTYPE)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE
  loc:EipClm = upper(p_web.GetValue('_EIPClm'))
  p_web.DeleteValue('_EIPClm')
  case loc:EipClm
  of upper('btnCopyCollectionAddress')
    do Validate::btnCopyCollectionAddress
  of upper('btnCopyDeliveryAddress')
    do Validate::btnCopyDeliveryAddress
  of upper('btnCollectionText')
    do Validate::btnCollectionText
  of upper('btnDeliveryText')
    do Validate::btnDeliveryText
  of ''
    case upper(p_web.GetValue('_calledfrom_'))
    end
  end

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('AmendAddress_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'AmendAddress'
    end
    p_web.formsettings.proc = 'AmendAddress'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('job:Company_Name')
    p_web.SetPicture('job:Company_Name','@s30')
  End
  p_web.SetSessionPicture('job:Company_Name','@s30')
  If p_web.IfExistsValue('job:Company_Name_Collection')
    p_web.SetPicture('job:Company_Name_Collection','@s30')
  End
  p_web.SetSessionPicture('job:Company_Name_Collection','@s30')
  If p_web.IfExistsValue('job:Company_Name_Delivery')
    p_web.SetPicture('job:Company_Name_Delivery','@s30')
  End
  p_web.SetSessionPicture('job:Company_Name_Delivery','@s30')
  If p_web.IfExistsValue('job:Address_Line1')
    p_web.SetPicture('job:Address_Line1','@s30')
  End
  p_web.SetSessionPicture('job:Address_Line1','@s30')
  If p_web.IfExistsValue('job:Address_Line1_Collection')
    p_web.SetPicture('job:Address_Line1_Collection','@s30')
  End
  p_web.SetSessionPicture('job:Address_Line1_Collection','@s30')
  If p_web.IfExistsValue('job:Address_Line1_Delivery')
    p_web.SetPicture('job:Address_Line1_Delivery','@s30')
  End
  p_web.SetSessionPicture('job:Address_Line1_Delivery','@s30')
  If p_web.IfExistsValue('job:Address_Line2')
    p_web.SetPicture('job:Address_Line2','@s30')
  End
  p_web.SetSessionPicture('job:Address_Line2','@s30')
  If p_web.IfExistsValue('job:Address_Line2_Collection')
    p_web.SetPicture('job:Address_Line2_Collection','@s30')
  End
  p_web.SetSessionPicture('job:Address_Line2_Collection','@s30')
  If p_web.IfExistsValue('job:Address_Line2_Delivery')
    p_web.SetPicture('job:Address_Line2_Delivery','@s30')
  End
  p_web.SetSessionPicture('job:Address_Line2_Delivery','@s30')
  If p_web.IfExistsValue('job:Address_Line3')
    p_web.SetPicture('job:Address_Line3','@s30')
  End
  p_web.SetSessionPicture('job:Address_Line3','@s30')
  If p_web.IfExistsValue('job:Address_Line3_Collection')
    p_web.SetPicture('job:Address_Line3_Collection','@s30')
  End
  p_web.SetSessionPicture('job:Address_Line3_Collection','@s30')
  If p_web.IfExistsValue('job:Address_Line3_Delivery')
    p_web.SetPicture('job:Address_Line3_Delivery','@s30')
  End
  p_web.SetSessionPicture('job:Address_Line3_Delivery','@s30')
  If p_web.IfExistsValue('jobe2:HubCustomer')
    p_web.SetPicture('jobe2:HubCustomer','@s30')
  End
  p_web.SetSessionPicture('jobe2:HubCustomer','@s30')
  If p_web.IfExistsValue('jobe2:HubCollection')
    p_web.SetPicture('jobe2:HubCollection','@s30')
  End
  p_web.SetSessionPicture('jobe2:HubCollection','@s30')
  If p_web.IfExistsValue('jobe2:HubDelivery')
    p_web.SetPicture('jobe2:HubDelivery','@s30')
  End
  p_web.SetSessionPicture('jobe2:HubDelivery','@s30')
  If p_web.IfExistsValue('job:Postcode')
    p_web.SetPicture('job:Postcode','@s10')
  End
  p_web.SetSessionPicture('job:Postcode','@s10')
  If p_web.IfExistsValue('job:Postcode_Collection')
    p_web.SetPicture('job:Postcode_Collection','@s10')
  End
  p_web.SetSessionPicture('job:Postcode_Collection','@s10')
  If p_web.IfExistsValue('job:Postcode_Delivery')
    p_web.SetPicture('job:Postcode_Delivery','@s10')
  End
  p_web.SetSessionPicture('job:Postcode_Delivery','@s10')
  If p_web.IfExistsValue('job:Telephone_Number')
    p_web.SetPicture('job:Telephone_Number','@s15')
  End
  p_web.SetSessionPicture('job:Telephone_Number','@s15')
  If p_web.IfExistsValue('job:Telephone_Collection')
    p_web.SetPicture('job:Telephone_Collection','@s15')
  End
  p_web.SetSessionPicture('job:Telephone_Collection','@s15')
  If p_web.IfExistsValue('job:Telephone_Delivery')
    p_web.SetPicture('job:Telephone_Delivery','@s15')
  End
  p_web.SetSessionPicture('job:Telephone_Delivery','@s15')
  If p_web.IfExistsValue('job:Fax_Number')
    p_web.SetPicture('job:Fax_Number','@s15')
  End
  p_web.SetSessionPicture('job:Fax_Number','@s15')
  If p_web.IfExistsValue('jobe:EndUserEmailAddress')
    p_web.SetPicture('jobe:EndUserEmailAddress','@s255')
  End
  p_web.SetSessionPicture('jobe:EndUserEmailAddress','@s255')
  If p_web.IfExistsValue('jobe:EndUserTelNo')
    p_web.SetPicture('jobe:EndUserTelNo','@s15')
  End
  p_web.SetSessionPicture('jobe:EndUserTelNo','@s15')
  If p_web.IfExistsValue('jobe:VatNumber')
    p_web.SetPicture('jobe:VatNumber','@s30')
  End
  p_web.SetSessionPicture('jobe:VatNumber','@s30')
  If p_web.IfExistsValue('jobe2:IDNumber')
    p_web.SetPicture('jobe2:IDNumber','@s13')
  End
  p_web.SetSessionPicture('jobe2:IDNumber','@s13')
  If p_web.IfExistsValue('jobe2:SMSAlertNumber')
    p_web.SetPicture('jobe2:SMSAlertNumber','@s30')
  End
  p_web.SetSessionPicture('jobe2:SMSAlertNumber','@s30')
  If p_web.IfExistsValue('jobe2:EmailAlertAddress')
    p_web.SetPicture('jobe2:EmailAlertAddress','@s255')
  End
  p_web.SetSessionPicture('jobe2:EmailAlertAddress','@s255')
  If p_web.IfExistsValue('jobe2:CourierWaybillNumber')
    p_web.SetPicture('jobe2:CourierWaybillNumber','@s30')
  End
  p_web.SetSessionPicture('jobe2:CourierWaybillNumber','@s30')
  If p_web.IfExistsValue('jobe:Sub_Sub_Account')
    p_web.SetPicture('jobe:Sub_Sub_Account','@s15')
  End
  p_web.SetSessionPicture('jobe:Sub_Sub_Account','@s15')

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'job:Address_Line3'
    p_web.setsessionvalue('showtab_AmendAddress',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(SUBURB)
        p_web.setsessionvalue('job:Postcode',sur:Postcode)
        p_web.setsessionvalue('jobe2:HubCustomer',sur:Hub)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.job:Address_Line3_Collection')
  Of 'job:Address_Line3_Collection'
    p_web.setsessionvalue('showtab_AmendAddress',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(SUBURB)
        p_web.setsessionvalue('job:Postcode_Collection',sur:postcode)
        p_web.setsessionvalue('jobe2:HubCollection',sur:hub)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.job:Address_Line3_Delivery')
  Of 'job:Address_Line3_Delivery'
    p_web.setsessionvalue('showtab_AmendAddress',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(SUBURB)
        p_web.setsessionvalue('job:Postcode_Delivery',sur:Postcode)
        p_web.setsessionvalue('jobe2:HubDelivery',sur:Hub)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.jobe2:HubCustomer')
  End
  If p_web.GSV('Hide:SubSubAccount') <> 1
    loc:TabNumber += 1
  End
  Case p_Web.GetValue('lookupfield')
  Of 'jobe:Sub_Sub_Account'
    p_web.setsessionvalue('showtab_AmendAddress',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(SUBACCAD)
        p_web.setsessionvalue('job:Postcode_Delivery',sua:Postcode)
        p_web.setsessionvalue('job:Address_Line1_Delivery',sua:addressline1)
        p_web.setsessionvalue('job:Address_Line2_Delivery',sua:addressline2)
        p_web.setsessionvalue('job:Address_Line3_Delivery',sua:addressline3)
        p_web.setsessionvalue('job:Telephone_Delivery',sua:TelephoneNumber)
        p_web.setsessionvalue('job:Company_Name_Delivery',sua:CompanyName)
    End
  End
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  if p_web.IfExistsValue('job:Company_Name') = 0
    p_web.SetSessionValue('job:Company_Name',job:Company_Name)
  Else
    job:Company_Name = p_web.GetSessionValue('job:Company_Name')
  End
  if p_web.IfExistsValue('job:Company_Name_Collection') = 0
    p_web.SetSessionValue('job:Company_Name_Collection',job:Company_Name_Collection)
  Else
    job:Company_Name_Collection = p_web.GetSessionValue('job:Company_Name_Collection')
  End
  if p_web.IfExistsValue('job:Company_Name_Delivery') = 0
    p_web.SetSessionValue('job:Company_Name_Delivery',job:Company_Name_Delivery)
  Else
    job:Company_Name_Delivery = p_web.GetSessionValue('job:Company_Name_Delivery')
  End
  if p_web.IfExistsValue('job:Address_Line1') = 0
    p_web.SetSessionValue('job:Address_Line1',job:Address_Line1)
  Else
    job:Address_Line1 = p_web.GetSessionValue('job:Address_Line1')
  End
  if p_web.IfExistsValue('job:Address_Line1_Collection') = 0
    p_web.SetSessionValue('job:Address_Line1_Collection',job:Address_Line1_Collection)
  Else
    job:Address_Line1_Collection = p_web.GetSessionValue('job:Address_Line1_Collection')
  End
  if p_web.IfExistsValue('job:Address_Line1_Delivery') = 0
    p_web.SetSessionValue('job:Address_Line1_Delivery',job:Address_Line1_Delivery)
  Else
    job:Address_Line1_Delivery = p_web.GetSessionValue('job:Address_Line1_Delivery')
  End
  if p_web.IfExistsValue('job:Address_Line2') = 0
    p_web.SetSessionValue('job:Address_Line2',job:Address_Line2)
  Else
    job:Address_Line2 = p_web.GetSessionValue('job:Address_Line2')
  End
  if p_web.IfExistsValue('job:Address_Line2_Collection') = 0
    p_web.SetSessionValue('job:Address_Line2_Collection',job:Address_Line2_Collection)
  Else
    job:Address_Line2_Collection = p_web.GetSessionValue('job:Address_Line2_Collection')
  End
  if p_web.IfExistsValue('job:Address_Line2_Delivery') = 0
    p_web.SetSessionValue('job:Address_Line2_Delivery',job:Address_Line2_Delivery)
  Else
    job:Address_Line2_Delivery = p_web.GetSessionValue('job:Address_Line2_Delivery')
  End
  if p_web.IfExistsValue('job:Address_Line3') = 0
    p_web.SetSessionValue('job:Address_Line3',job:Address_Line3)
  Else
    job:Address_Line3 = p_web.GetSessionValue('job:Address_Line3')
  End
  if p_web.IfExistsValue('job:Address_Line3_Collection') = 0
    p_web.SetSessionValue('job:Address_Line3_Collection',job:Address_Line3_Collection)
  Else
    job:Address_Line3_Collection = p_web.GetSessionValue('job:Address_Line3_Collection')
  End
  if p_web.IfExistsValue('job:Address_Line3_Delivery') = 0
    p_web.SetSessionValue('job:Address_Line3_Delivery',job:Address_Line3_Delivery)
  Else
    job:Address_Line3_Delivery = p_web.GetSessionValue('job:Address_Line3_Delivery')
  End
  if p_web.IfExistsValue('jobe2:HubCustomer') = 0
    p_web.SetSessionValue('jobe2:HubCustomer',jobe2:HubCustomer)
  Else
    jobe2:HubCustomer = p_web.GetSessionValue('jobe2:HubCustomer')
  End
  if p_web.IfExistsValue('jobe2:HubCollection') = 0
    p_web.SetSessionValue('jobe2:HubCollection',jobe2:HubCollection)
  Else
    jobe2:HubCollection = p_web.GetSessionValue('jobe2:HubCollection')
  End
  if p_web.IfExistsValue('jobe2:HubDelivery') = 0
    p_web.SetSessionValue('jobe2:HubDelivery',jobe2:HubDelivery)
  Else
    jobe2:HubDelivery = p_web.GetSessionValue('jobe2:HubDelivery')
  End
  if p_web.IfExistsValue('job:Postcode') = 0
    p_web.SetSessionValue('job:Postcode',job:Postcode)
  Else
    job:Postcode = p_web.GetSessionValue('job:Postcode')
  End
  if p_web.IfExistsValue('job:Postcode_Collection') = 0
    p_web.SetSessionValue('job:Postcode_Collection',job:Postcode_Collection)
  Else
    job:Postcode_Collection = p_web.GetSessionValue('job:Postcode_Collection')
  End
  if p_web.IfExistsValue('job:Postcode_Delivery') = 0
    p_web.SetSessionValue('job:Postcode_Delivery',job:Postcode_Delivery)
  Else
    job:Postcode_Delivery = p_web.GetSessionValue('job:Postcode_Delivery')
  End
  if p_web.IfExistsValue('job:Telephone_Number') = 0
    p_web.SetSessionValue('job:Telephone_Number',job:Telephone_Number)
  Else
    job:Telephone_Number = p_web.GetSessionValue('job:Telephone_Number')
  End
  if p_web.IfExistsValue('job:Telephone_Collection') = 0
    p_web.SetSessionValue('job:Telephone_Collection',job:Telephone_Collection)
  Else
    job:Telephone_Collection = p_web.GetSessionValue('job:Telephone_Collection')
  End
  if p_web.IfExistsValue('job:Telephone_Delivery') = 0
    p_web.SetSessionValue('job:Telephone_Delivery',job:Telephone_Delivery)
  Else
    job:Telephone_Delivery = p_web.GetSessionValue('job:Telephone_Delivery')
  End
  if p_web.IfExistsValue('job:Fax_Number') = 0
    p_web.SetSessionValue('job:Fax_Number',job:Fax_Number)
  Else
    job:Fax_Number = p_web.GetSessionValue('job:Fax_Number')
  End
  if p_web.IfExistsValue('jobe:EndUserEmailAddress') = 0
    p_web.SetSessionValue('jobe:EndUserEmailAddress',jobe:EndUserEmailAddress)
  Else
    jobe:EndUserEmailAddress = p_web.GetSessionValue('jobe:EndUserEmailAddress')
  End
  if p_web.IfExistsValue('jobe:EndUserTelNo') = 0
    p_web.SetSessionValue('jobe:EndUserTelNo',jobe:EndUserTelNo)
  Else
    jobe:EndUserTelNo = p_web.GetSessionValue('jobe:EndUserTelNo')
  End
  if p_web.IfExistsValue('jbn:Collection_Text') = 0
    p_web.SetSessionValue('jbn:Collection_Text',jbn:Collection_Text)
  Else
    jbn:Collection_Text = p_web.GetSessionValue('jbn:Collection_Text')
  End
  if p_web.IfExistsValue('jbn:Delivery_Text') = 0
    p_web.SetSessionValue('jbn:Delivery_Text',jbn:Delivery_Text)
  Else
    jbn:Delivery_Text = p_web.GetSessionValue('jbn:Delivery_Text')
  End
  if p_web.IfExistsValue('jobe:VatNumber') = 0
    p_web.SetSessionValue('jobe:VatNumber',jobe:VatNumber)
  Else
    jobe:VatNumber = p_web.GetSessionValue('jobe:VatNumber')
  End
  if p_web.IfExistsValue('jobe2:IDNumber') = 0
    p_web.SetSessionValue('jobe2:IDNumber',jobe2:IDNumber)
  Else
    jobe2:IDNumber = p_web.GetSessionValue('jobe2:IDNumber')
  End
  if p_web.IfExistsValue('jobe2:SMSNotification') = 0
    p_web.SetSessionValue('jobe2:SMSNotification',jobe2:SMSNotification)
  Else
    jobe2:SMSNotification = p_web.GetSessionValue('jobe2:SMSNotification')
  End
  if p_web.IfExistsValue('jobe2:SMSAlertNumber') = 0
    p_web.SetSessionValue('jobe2:SMSAlertNumber',jobe2:SMSAlertNumber)
  Else
    jobe2:SMSAlertNumber = p_web.GetSessionValue('jobe2:SMSAlertNumber')
  End
  if p_web.IfExistsValue('jobe2:EmailNotification') = 0
    p_web.SetSessionValue('jobe2:EmailNotification',jobe2:EmailNotification)
  Else
    jobe2:EmailNotification = p_web.GetSessionValue('jobe2:EmailNotification')
  End
  if p_web.IfExistsValue('jobe2:EmailAlertAddress') = 0
    p_web.SetSessionValue('jobe2:EmailAlertAddress',jobe2:EmailAlertAddress)
  Else
    jobe2:EmailAlertAddress = p_web.GetSessionValue('jobe2:EmailAlertAddress')
  End
  if p_web.IfExistsValue('jobe2:CourierWaybillNumber') = 0
    p_web.SetSessionValue('jobe2:CourierWaybillNumber',jobe2:CourierWaybillNumber)
  Else
    jobe2:CourierWaybillNumber = p_web.GetSessionValue('jobe2:CourierWaybillNumber')
  End
  if p_web.IfExistsValue('jobe:Sub_Sub_Account') = 0
    p_web.SetSessionValue('jobe:Sub_Sub_Account',jobe:Sub_Sub_Account)
  Else
    jobe:Sub_Sub_Account = p_web.GetSessionValue('jobe:Sub_Sub_Account')
  End

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('job:Company_Name')
    job:Company_Name = p_web.GetValue('job:Company_Name')
    p_web.SetSessionValue('job:Company_Name',job:Company_Name)
  Else
    job:Company_Name = p_web.GetSessionValue('job:Company_Name')
  End
  if p_web.IfExistsValue('job:Company_Name_Collection')
    job:Company_Name_Collection = p_web.GetValue('job:Company_Name_Collection')
    p_web.SetSessionValue('job:Company_Name_Collection',job:Company_Name_Collection)
  Else
    job:Company_Name_Collection = p_web.GetSessionValue('job:Company_Name_Collection')
  End
  if p_web.IfExistsValue('job:Company_Name_Delivery')
    job:Company_Name_Delivery = p_web.GetValue('job:Company_Name_Delivery')
    p_web.SetSessionValue('job:Company_Name_Delivery',job:Company_Name_Delivery)
  Else
    job:Company_Name_Delivery = p_web.GetSessionValue('job:Company_Name_Delivery')
  End
  if p_web.IfExistsValue('job:Address_Line1')
    job:Address_Line1 = p_web.GetValue('job:Address_Line1')
    p_web.SetSessionValue('job:Address_Line1',job:Address_Line1)
  Else
    job:Address_Line1 = p_web.GetSessionValue('job:Address_Line1')
  End
  if p_web.IfExistsValue('job:Address_Line1_Collection')
    job:Address_Line1_Collection = p_web.GetValue('job:Address_Line1_Collection')
    p_web.SetSessionValue('job:Address_Line1_Collection',job:Address_Line1_Collection)
  Else
    job:Address_Line1_Collection = p_web.GetSessionValue('job:Address_Line1_Collection')
  End
  if p_web.IfExistsValue('job:Address_Line1_Delivery')
    job:Address_Line1_Delivery = p_web.GetValue('job:Address_Line1_Delivery')
    p_web.SetSessionValue('job:Address_Line1_Delivery',job:Address_Line1_Delivery)
  Else
    job:Address_Line1_Delivery = p_web.GetSessionValue('job:Address_Line1_Delivery')
  End
  if p_web.IfExistsValue('job:Address_Line2')
    job:Address_Line2 = p_web.GetValue('job:Address_Line2')
    p_web.SetSessionValue('job:Address_Line2',job:Address_Line2)
  Else
    job:Address_Line2 = p_web.GetSessionValue('job:Address_Line2')
  End
  if p_web.IfExistsValue('job:Address_Line2_Collection')
    job:Address_Line2_Collection = p_web.GetValue('job:Address_Line2_Collection')
    p_web.SetSessionValue('job:Address_Line2_Collection',job:Address_Line2_Collection)
  Else
    job:Address_Line2_Collection = p_web.GetSessionValue('job:Address_Line2_Collection')
  End
  if p_web.IfExistsValue('job:Address_Line2_Delivery')
    job:Address_Line2_Delivery = p_web.GetValue('job:Address_Line2_Delivery')
    p_web.SetSessionValue('job:Address_Line2_Delivery',job:Address_Line2_Delivery)
  Else
    job:Address_Line2_Delivery = p_web.GetSessionValue('job:Address_Line2_Delivery')
  End
  if p_web.IfExistsValue('job:Address_Line3')
    job:Address_Line3 = p_web.GetValue('job:Address_Line3')
    p_web.SetSessionValue('job:Address_Line3',job:Address_Line3)
  Else
    job:Address_Line3 = p_web.GetSessionValue('job:Address_Line3')
  End
  if p_web.IfExistsValue('job:Address_Line3_Collection')
    job:Address_Line3_Collection = p_web.GetValue('job:Address_Line3_Collection')
    p_web.SetSessionValue('job:Address_Line3_Collection',job:Address_Line3_Collection)
  Else
    job:Address_Line3_Collection = p_web.GetSessionValue('job:Address_Line3_Collection')
  End
  if p_web.IfExistsValue('job:Address_Line3_Delivery')
    job:Address_Line3_Delivery = p_web.GetValue('job:Address_Line3_Delivery')
    p_web.SetSessionValue('job:Address_Line3_Delivery',job:Address_Line3_Delivery)
  Else
    job:Address_Line3_Delivery = p_web.GetSessionValue('job:Address_Line3_Delivery')
  End
  if p_web.IfExistsValue('jobe2:HubCustomer')
    jobe2:HubCustomer = p_web.GetValue('jobe2:HubCustomer')
    p_web.SetSessionValue('jobe2:HubCustomer',jobe2:HubCustomer)
  Else
    jobe2:HubCustomer = p_web.GetSessionValue('jobe2:HubCustomer')
  End
  if p_web.IfExistsValue('jobe2:HubCollection')
    jobe2:HubCollection = p_web.GetValue('jobe2:HubCollection')
    p_web.SetSessionValue('jobe2:HubCollection',jobe2:HubCollection)
  Else
    jobe2:HubCollection = p_web.GetSessionValue('jobe2:HubCollection')
  End
  if p_web.IfExistsValue('jobe2:HubDelivery')
    jobe2:HubDelivery = p_web.GetValue('jobe2:HubDelivery')
    p_web.SetSessionValue('jobe2:HubDelivery',jobe2:HubDelivery)
  Else
    jobe2:HubDelivery = p_web.GetSessionValue('jobe2:HubDelivery')
  End
  if p_web.IfExistsValue('job:Postcode')
    job:Postcode = p_web.GetValue('job:Postcode')
    p_web.SetSessionValue('job:Postcode',job:Postcode)
  Else
    job:Postcode = p_web.GetSessionValue('job:Postcode')
  End
  if p_web.IfExistsValue('job:Postcode_Collection')
    job:Postcode_Collection = p_web.GetValue('job:Postcode_Collection')
    p_web.SetSessionValue('job:Postcode_Collection',job:Postcode_Collection)
  Else
    job:Postcode_Collection = p_web.GetSessionValue('job:Postcode_Collection')
  End
  if p_web.IfExistsValue('job:Postcode_Delivery')
    job:Postcode_Delivery = p_web.GetValue('job:Postcode_Delivery')
    p_web.SetSessionValue('job:Postcode_Delivery',job:Postcode_Delivery)
  Else
    job:Postcode_Delivery = p_web.GetSessionValue('job:Postcode_Delivery')
  End
  if p_web.IfExistsValue('job:Telephone_Number')
    job:Telephone_Number = p_web.GetValue('job:Telephone_Number')
    p_web.SetSessionValue('job:Telephone_Number',job:Telephone_Number)
  Else
    job:Telephone_Number = p_web.GetSessionValue('job:Telephone_Number')
  End
  if p_web.IfExistsValue('job:Telephone_Collection')
    job:Telephone_Collection = p_web.GetValue('job:Telephone_Collection')
    p_web.SetSessionValue('job:Telephone_Collection',job:Telephone_Collection)
  Else
    job:Telephone_Collection = p_web.GetSessionValue('job:Telephone_Collection')
  End
  if p_web.IfExistsValue('job:Telephone_Delivery')
    job:Telephone_Delivery = p_web.GetValue('job:Telephone_Delivery')
    p_web.SetSessionValue('job:Telephone_Delivery',job:Telephone_Delivery)
  Else
    job:Telephone_Delivery = p_web.GetSessionValue('job:Telephone_Delivery')
  End
  if p_web.IfExistsValue('job:Fax_Number')
    job:Fax_Number = p_web.GetValue('job:Fax_Number')
    p_web.SetSessionValue('job:Fax_Number',job:Fax_Number)
  Else
    job:Fax_Number = p_web.GetSessionValue('job:Fax_Number')
  End
  if p_web.IfExistsValue('jobe:EndUserEmailAddress')
    jobe:EndUserEmailAddress = p_web.GetValue('jobe:EndUserEmailAddress')
    p_web.SetSessionValue('jobe:EndUserEmailAddress',jobe:EndUserEmailAddress)
  Else
    jobe:EndUserEmailAddress = p_web.GetSessionValue('jobe:EndUserEmailAddress')
  End
  if p_web.IfExistsValue('jobe:EndUserTelNo')
    jobe:EndUserTelNo = p_web.GetValue('jobe:EndUserTelNo')
    p_web.SetSessionValue('jobe:EndUserTelNo',jobe:EndUserTelNo)
  Else
    jobe:EndUserTelNo = p_web.GetSessionValue('jobe:EndUserTelNo')
  End
  if p_web.IfExistsValue('jbn:Collection_Text')
    jbn:Collection_Text = p_web.GetValue('jbn:Collection_Text')
    p_web.SetSessionValue('jbn:Collection_Text',jbn:Collection_Text)
  Else
    jbn:Collection_Text = p_web.GetSessionValue('jbn:Collection_Text')
  End
  if p_web.IfExistsValue('jbn:Delivery_Text')
    jbn:Delivery_Text = p_web.GetValue('jbn:Delivery_Text')
    p_web.SetSessionValue('jbn:Delivery_Text',jbn:Delivery_Text)
  Else
    jbn:Delivery_Text = p_web.GetSessionValue('jbn:Delivery_Text')
  End
  if p_web.IfExistsValue('jobe:VatNumber')
    jobe:VatNumber = p_web.GetValue('jobe:VatNumber')
    p_web.SetSessionValue('jobe:VatNumber',jobe:VatNumber)
  Else
    jobe:VatNumber = p_web.GetSessionValue('jobe:VatNumber')
  End
  if p_web.IfExistsValue('jobe2:IDNumber')
    jobe2:IDNumber = p_web.GetValue('jobe2:IDNumber')
    p_web.SetSessionValue('jobe2:IDNumber',jobe2:IDNumber)
  Else
    jobe2:IDNumber = p_web.GetSessionValue('jobe2:IDNumber')
  End
  if p_web.IfExistsValue('jobe2:SMSNotification')
    jobe2:SMSNotification = p_web.GetValue('jobe2:SMSNotification')
    p_web.SetSessionValue('jobe2:SMSNotification',jobe2:SMSNotification)
  Else
    jobe2:SMSNotification = p_web.GetSessionValue('jobe2:SMSNotification')
  End
  if p_web.IfExistsValue('jobe2:SMSAlertNumber')
    jobe2:SMSAlertNumber = p_web.GetValue('jobe2:SMSAlertNumber')
    p_web.SetSessionValue('jobe2:SMSAlertNumber',jobe2:SMSAlertNumber)
  Else
    jobe2:SMSAlertNumber = p_web.GetSessionValue('jobe2:SMSAlertNumber')
  End
  if p_web.IfExistsValue('jobe2:EmailNotification')
    jobe2:EmailNotification = p_web.GetValue('jobe2:EmailNotification')
    p_web.SetSessionValue('jobe2:EmailNotification',jobe2:EmailNotification)
  Else
    jobe2:EmailNotification = p_web.GetSessionValue('jobe2:EmailNotification')
  End
  if p_web.IfExistsValue('jobe2:EmailAlertAddress')
    jobe2:EmailAlertAddress = p_web.GetValue('jobe2:EmailAlertAddress')
    p_web.SetSessionValue('jobe2:EmailAlertAddress',jobe2:EmailAlertAddress)
  Else
    jobe2:EmailAlertAddress = p_web.GetSessionValue('jobe2:EmailAlertAddress')
  End
  if p_web.IfExistsValue('jobe2:CourierWaybillNumber')
    jobe2:CourierWaybillNumber = p_web.GetValue('jobe2:CourierWaybillNumber')
    p_web.SetSessionValue('jobe2:CourierWaybillNumber',jobe2:CourierWaybillNumber)
  Else
    jobe2:CourierWaybillNumber = p_web.GetSessionValue('jobe2:CourierWaybillNumber')
  End
  if p_web.IfExistsValue('jobe:Sub_Sub_Account')
    jobe:Sub_Sub_Account = p_web.GetValue('jobe:Sub_Sub_Account')
    p_web.SetSessionValue('jobe:Sub_Sub_Account',jobe:Sub_Sub_Account)
  Else
    jobe:Sub_Sub_Account = p_web.GetSessionValue('jobe:Sub_Sub_Account')
  End

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('AmendAddress_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.GSV('FromURL')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('AmendAddress_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('AmendAddress_ChainTo')
    loc:formaction = p_web.GetSessionValue('AmendAddress_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction

GenerateForm   Routine
  do LoadRelatedRecords
  !Prime
      p_web.SSV('Hide:DeliveryAddress',1)
      p_web.SSV('Hide:CollectionAddress',1)
      Access:TRANTYPE.Clearkey(trt:transit_Type_Key)
      trt:transit_Type    = p_web.GSV('job:transit_Type')
      if (Access:TRANTYPE.TryFetch(trt:transit_Type_Key) = Level:Benign)
      ! Found
          if (trt:Delivery_Address = 'YES')
              p_web.SSV('Hide:DeliveryAddress',0)
          end ! if (trt:DeliveryAddress = 'YES')
  
          if (trt:Collection_Address = 'YES')
              p_web.SSV('Hide:CollectionAddress',0)
          end ! if (trt:Collection_Address = 'YES')
  
      else ! if (Access:TRANTYPE.TryFetch(trt:transit_Type_Key) = Level:Benign)
      ! Error
      end ! if (Access:TRANTYPE.TryFetch(trt:transit_Type_Key) = Level:Benign)
  
      p_web.storeValue('FromURL')
  
  
      p_web.SSV('Comment:TelephoneNumber','')
      p_web.SSV('Comment:FaxNumber','')
      p_web.SSV('Comment:SMSAlertNumber','')
      p_web.SSV('Comment:TelephoneNumberDelivery','')
      p_web.SSV('Comment:TelephoneNumberCollection','')
  
      Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
      sub:Account_Number = p_web.GSV('job:Account_Number')
      IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
          IF (sub:UseAlternativeAdd)
              p_web.SSV('Hide:SubSubAccount',0)
          ELSE
              p_web.SSV('Hide:SubSubAccount',1)
          END
          ! Start - Only show VAT Number if the sub account is "overridden" and blank - TrkBs: 5364 (DBH: 26-04-2005)
          If sub:Vat_Number = '' AND sub:OverrideHeadVATNo = True
              p_web.SSV('Hide:VatNumber',0)
          Else ! sub:Vat_Number = '' AND tra:Vat_Number = ''
              p_web.SSV('Hide:VatNumber',1)
          End ! sub:Vat_Number = '' AND tra:Vat_Number = ''
          ! End   - Only show VAT Number if the sub account is "overridden" and blank - TrkBs: 5364 (DBH: 26-04-2005)
  
      END
  
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  loc:FormHeading = p_web.Translate('Amend Addresses',0)
  If loc:FormHeading
    if loc:popup
      packet = clip(packet) & p_web.jQuery('#popup_'&lower('AmendAddress')&'_div','dialog','"option","title",'&p_web.jsString(loc:FormHeading,0),,0)
    else
      packet = clip(packet) & lower('<div id="form-access-AmendAddress"></div>')
      packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formheading,)&'">'&clip(loc:FormHeading)&'</div>'&CRLF
    end
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_AmendAddress',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_AmendAddress0_div')&'">'&p_web.Translate('End User Details')&'</a></li>'& CRLF
      If p_web.GSV('Hide:SubSubAccount') <> 1
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_AmendAddress1_div')&'">'&p_web.Translate('Other Details')&'</a></li>'& CRLF
      End
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  do GenerateTab1
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="AmendAddress_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="AmendAddress_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'AmendAddress_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="AmendAddress_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'AmendAddress_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
    If upper(p_web.getvalue('LookupFile'))='SUBURB'
          If Not (p_web.GSV('Hide:CollectionAddress') = 1)
            p_web.SetValue('SelectField',clip(loc:formname) & '.job:Address_Line3_Collection')
          End
    End
    If upper(p_web.getvalue('LookupFile'))='SUBURB'
          If Not (p_web.GSV('Hide:DeliveryAddress') = 1)
            p_web.SetValue('SelectField',clip(loc:formname) & '.job:Address_Line3_Delivery')
          End
    End
    If upper(p_web.getvalue('LookupFile'))='SUBURB'
          If Not (1=0)
            p_web.SetValue('SelectField',clip(loc:formname) & '.job:Telephone_Number')
          End
    End
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.job:Company_Name')
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_AmendAddress')>0,p_web.GSV('showtab_AmendAddress'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_AmendAddress'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_AmendAddress') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_AmendAddress'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_AmendAddress')>0,p_web.GSV('showtab_AmendAddress'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_AmendAddress') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('End User Details') & ''''
      If p_web.GSV('Hide:SubSubAccount') <> 1
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Other Details') & ''''
      End
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_AmendAddress_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_AmendAddress')>0,p_web.GSV('showtab_AmendAddress'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"AmendAddress",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_AmendAddress')>0,p_web.GSV('showtab_AmendAddress'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_AmendAddress_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('AmendAddress') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    p_web.SetValue('_CallPopups',1)
      If p_web.RequestAjax = 0 and p_web.GetPreCall('LookupSuburbs') = 0 then LookupSuburbs(p_web).
    p_web.SetValue('_CallPopups',1)
      If p_web.RequestAjax = 0 and p_web.GetPreCall('ClearAddressDetails') = 0 then ClearAddressDetails(p_web,Net:Web:Popup).
    p_web.SetValue('_CallPopups',1)
      If p_web.RequestAjax = 0 and p_web.GetPreCall('FormCollectionText') = 0 then FormCollectionText(p_web,Net:Web:Popup).
    p_web.SetValue('_CallPopups',1)
      If p_web.RequestAjax = 0 and p_web.GetPreCall('FormDeliveryText') = 0 then FormDeliveryText(p_web,Net:Web:Popup).
    p_web.SetValue('_CallPopups',1)
      If p_web.RequestAjax = 0 and p_web.GetPreCall('BrowseSubAddresses') = 0 then BrowseSubAddresses(p_web).
    p_web.SetValue('_CallPopups',0)
    do AutoLookups
    p_web.AddPreCall('AmendAddress')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('End User Details')&'</a></h3>' & CRLF & p_web.DivHeader('tab_AmendAddress0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_AmendAddress0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_AmendAddress0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_AmendAddress0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'End User Details')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_AmendAddress0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('End User Details')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_AmendAddress0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('End User Details')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_AmendAddress0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::txtEndUserAddress
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::txtEndUserAddress
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::txtCollectionAddress
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::txtCollectionAddress
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::txtDeliveryAddress
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::txtDeliveryAddress
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Company_Name
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Company_Name
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Company_Name_Collection
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Company_Name_Collection
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Company_Name_Delivery
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Company_Name_Delivery
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Address_Line1
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Address_Line1
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Address_Line1_Collection
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Address_Line1_Collection
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Address_Line1_Delivery
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Address_Line1_Delivery
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Address_Line2
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Address_Line2
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Address_Line2_Collection
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Address_Line2_Collection
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Address_Line2_Delivery
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Address_Line2_Delivery
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Address_Line3
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Address_Line3
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Address_Line3_Collection
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Address_Line3_Collection
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Address_Line3_Delivery
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Address_Line3_Delivery
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jobe2:HubCustomer
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jobe2:HubCustomer
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jobe2:HubCollection
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jobe2:HubCollection
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jobe2:HubDelivery
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jobe2:HubDelivery
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Postcode
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Postcode
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Postcode_Collection
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Postcode_Collection
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Postcode_Delivery
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Postcode_Delivery
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Telephone_Number
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Telephone_Number
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Telephone_Collection
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Telephone_Collection
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Telephone_Delivery
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Telephone_Delivery
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Fax_Number
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Fax_Number
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::btnCopyCollectionAddress
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::btnCopyDeliveryAddress
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jobe:EndUserEmailAddress
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jobe:EndUserEmailAddress
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jobe:EndUserTelNo
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jobe:EndUserTelNo
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td rowspan="'&clip(3)&'" valign="top"'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jbn:Collection_Text
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td rowspan="'&clip(3)&'"'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jbn:Collection_Text
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td rowspan="'&clip(3)&'" valign="top"'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jbn:Delivery_Text
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td rowspan="'&clip(3)&'"'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jbn:Delivery_Text
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      If p_web.GSV('Hide:VatNumber') <> 1
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jobe:VatNumber
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jobe:VatNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jobe2:IDNumber
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jobe2:IDNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jobe2:SMSNotification
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jobe2:SMSNotification
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jobe2:SMSAlertNumber
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jobe2:SMSAlertNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::btnCollectionText
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::btnDeliveryText
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jobe2:EmailNotification
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jobe2:EmailNotification
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jobe2:EmailAlertAddress
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jobe2:EmailAlertAddress
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jobe2:CourierWaybillNumber
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jobe2:CourierWaybillNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab1  Routine
  If p_web.GSV('Hide:SubSubAccount') <> 1
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Other Details')&'</a></h3>' & CRLF & p_web.DivHeader('tab_AmendAddress1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_AmendAddress1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_AmendAddress1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_AmendAddress1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Other Details')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_AmendAddress1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Other Details')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_AmendAddress1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Other Details')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_AmendAddress1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jobe:Sub_Sub_Account
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jobe:Sub_Sub_Account
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
  end


Prompt::txtEndUserAddress  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddress_' & p_web._nocolon('txtEndUserAddress') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,'nt-prompt-small',)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::txtEndUserAddress  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::txtEndUserAddress  ! copies value to session value if valid.

ValidateValue::txtEndUserAddress  Routine
    If not (1=0)
    End

Value::txtEndUserAddress  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddress_' & p_web._nocolon('txtEndUserAddress') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="txtEndUserAddress" class="'&clip('blue bold')&'"'&clip(loc:extra)&'>' & p_web.Translate('End User Address',) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::txtCollectionAddress  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddress_' & p_web._nocolon('txtCollectionAddress') & '_prompt',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,'nt-prompt-small','hidden')),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:CollectionAddress') = 1,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::txtCollectionAddress  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::txtCollectionAddress  ! copies value to session value if valid.

ValidateValue::txtCollectionAddress  Routine
    If not (p_web.GSV('Hide:CollectionAddress') = 1)
    End

Value::txtCollectionAddress  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:CollectionAddress') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddress_' & p_web._nocolon('txtCollectionAddress') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('Hide:CollectionAddress') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="txtCollectionAddress" class="'&clip('blue bold')&'"'&clip(loc:extra)&'>' & p_web.Translate('Collection Address',) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::txtDeliveryAddress  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddress_' & p_web._nocolon('txtDeliveryAddress') & '_prompt',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,'nt-prompt-small','hidden')),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:CollectionAddress') = 1,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::txtDeliveryAddress  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::txtDeliveryAddress  ! copies value to session value if valid.

ValidateValue::txtDeliveryAddress  Routine
    If not (p_web.GSV('Hide:CollectionAddress') = 1)
    End

Value::txtDeliveryAddress  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:CollectionAddress') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddress_' & p_web._nocolon('txtDeliveryAddress') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('Hide:CollectionAddress') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="txtDeliveryAddress" class="'&clip('blue bold')&'"'&clip(loc:extra)&'>' & p_web.Translate('Delivery Address',) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::job:Company_Name  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddress_' & p_web._nocolon('job:Company_Name') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,'nt-prompt-small',)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Company Name'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Company_Name  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Company_Name = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    job:Company_Name = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::job:Company_Name  ! copies value to session value if valid.
  do Value::job:Company_Name
  do SendAlert

ValidateValue::job:Company_Name  Routine
    If not (1=0)
    job:Company_Name = Upper(job:Company_Name)
      if loc:invalid = '' then p_web.SetSessionValue('job:Company_Name',job:Company_Name).
    End

Value::job:Company_Name  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddress_' & p_web._nocolon('job:Company_Name') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    job:Company_Name = p_web.RestoreValue('job:Company_Name')
    do ValidateValue::job:Company_Name
    If job:Company_Name:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- job:Company_Name
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Company_Name'',''amendaddress_job:company_name_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Company_Name',p_web.GetSessionValueFormat('job:Company_Name'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::job:Company_Name_Collection  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddress_' & p_web._nocolon('job:Company_Name_Collection') & '_prompt',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,'nt-prompt-small','hidden')),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:CollectionAddress') = 1,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Company_Name_Collection  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Company_Name_Collection = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    job:Company_Name_Collection = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::job:Company_Name_Collection  ! copies value to session value if valid.
  do Value::job:Company_Name_Collection
  do SendAlert

ValidateValue::job:Company_Name_Collection  Routine
    If not (p_web.GSV('Hide:CollectionAddress') = 1)
    job:Company_Name_Collection = Upper(job:Company_Name_Collection)
      if loc:invalid = '' then p_web.SetSessionValue('job:Company_Name_Collection',job:Company_Name_Collection).
    End

Value::job:Company_Name_Collection  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:CollectionAddress') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddress_' & p_web._nocolon('job:Company_Name_Collection') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.GSV('ReadOnly:CollectionAddress') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    job:Company_Name_Collection = p_web.RestoreValue('job:Company_Name_Collection')
    do ValidateValue::job:Company_Name_Collection
    If job:Company_Name_Collection:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:CollectionAddress') = 1)
  ! --- STRING --- job:Company_Name_Collection
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:CollectionAddress') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Company_Name_Collection'',''amendaddress_job:company_name_collection_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Company_Name_Collection',p_web.GetSessionValueFormat('job:Company_Name_Collection'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::job:Company_Name_Delivery  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddress_' & p_web._nocolon('job:Company_Name_Delivery') & '_prompt',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,'nt-prompt-small','hidden')),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Company_Name_Delivery  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Company_Name_Delivery = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    job:Company_Name_Delivery = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::job:Company_Name_Delivery  ! copies value to session value if valid.
  do Value::job:Company_Name_Delivery
  do SendAlert

ValidateValue::job:Company_Name_Delivery  Routine
    If not (p_web.GSV('Hide:DeliveryAddress') = 1)
    job:Company_Name_Delivery = Upper(job:Company_Name_Delivery)
      if loc:invalid = '' then p_web.SetSessionValue('job:Company_Name_Delivery',job:Company_Name_Delivery).
    End

Value::job:Company_Name_Delivery  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:DeliveryAddress') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddress_' & p_web._nocolon('job:Company_Name_Delivery') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.GSV('ReadOnly:DeliveryAddress') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    job:Company_Name_Delivery = p_web.RestoreValue('job:Company_Name_Delivery')
    do ValidateValue::job:Company_Name_Delivery
    If job:Company_Name_Delivery:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:DeliveryAddress') = 1)
  ! --- STRING --- job:Company_Name_Delivery
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:DeliveryAddress') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Company_Name_Delivery'',''amendaddress_job:company_name_delivery_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Company_Name_Delivery',p_web.GetSessionValueFormat('job:Company_Name_Delivery'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::job:Address_Line1  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line1') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,'nt-prompt-small',)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Address'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Address_Line1  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Address_Line1 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    job:Address_Line1 = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::job:Address_Line1  ! copies value to session value if valid.
  do Value::job:Address_Line1
  do SendAlert

ValidateValue::job:Address_Line1  Routine
    If not (1=0)
    job:Address_Line1 = Upper(job:Address_Line1)
      if loc:invalid = '' then p_web.SetSessionValue('job:Address_Line1',job:Address_Line1).
    End

Value::job:Address_Line1  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line1') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    job:Address_Line1 = p_web.RestoreValue('job:Address_Line1')
    do ValidateValue::job:Address_Line1
    If job:Address_Line1:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- job:Address_Line1
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Address_Line1'',''amendaddress_job:address_line1_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Address_Line1',p_web.GetSessionValueFormat('job:Address_Line1'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::job:Address_Line1_Collection  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line1_Collection') & '_prompt',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,'nt-prompt-small','hidden')),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:CollectionAddress') = 1,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Address_Line1_Collection  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Address_Line1_Collection = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    job:Address_Line1_Collection = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::job:Address_Line1_Collection  ! copies value to session value if valid.
  do Value::job:Address_Line1_Collection
  do SendAlert

ValidateValue::job:Address_Line1_Collection  Routine
    If not (p_web.GSV('Hide:CollectionAddress') = 1)
    job:Address_Line1_Collection = Upper(job:Address_Line1_Collection)
      if loc:invalid = '' then p_web.SetSessionValue('job:Address_Line1_Collection',job:Address_Line1_Collection).
    End

Value::job:Address_Line1_Collection  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:CollectionAddress') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line1_Collection') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.GSV('ReadOnly:CollectionAddress') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    job:Address_Line1_Collection = p_web.RestoreValue('job:Address_Line1_Collection')
    do ValidateValue::job:Address_Line1_Collection
    If job:Address_Line1_Collection:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:CollectionAddress') = 1)
  ! --- STRING --- job:Address_Line1_Collection
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:CollectionAddress') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Address_Line1_Collection'',''amendaddress_job:address_line1_collection_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Address_Line1_Collection',p_web.GetSessionValueFormat('job:Address_Line1_Collection'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::job:Address_Line1_Delivery  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line1_Delivery') & '_prompt',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,'nt-prompt-small','hidden')),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Address_Line1_Delivery  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Address_Line1_Delivery = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    job:Address_Line1_Delivery = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::job:Address_Line1_Delivery  ! copies value to session value if valid.
  do Value::job:Address_Line1_Delivery
  do SendAlert

ValidateValue::job:Address_Line1_Delivery  Routine
    If not (p_web.GSV('Hide:DeliveryAddress') = 1)
    job:Address_Line1_Delivery = Upper(job:Address_Line1_Delivery)
      if loc:invalid = '' then p_web.SetSessionValue('job:Address_Line1_Delivery',job:Address_Line1_Delivery).
    End

Value::job:Address_Line1_Delivery  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:DeliveryAddress') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line1_Delivery') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.GSV('ReadOnly:DeliveryAddress') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    job:Address_Line1_Delivery = p_web.RestoreValue('job:Address_Line1_Delivery')
    do ValidateValue::job:Address_Line1_Delivery
    If job:Address_Line1_Delivery:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:DeliveryAddress') = 1)
  ! --- STRING --- job:Address_Line1_Delivery
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:DeliveryAddress') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Address_Line1_Delivery'',''amendaddress_job:address_line1_delivery_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Address_Line1_Delivery',p_web.GetSessionValueFormat('job:Address_Line1_Delivery'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::job:Address_Line2  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line2') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,'nt-prompt-small',)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Address_Line2  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Address_Line2 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    job:Address_Line2 = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::job:Address_Line2  ! copies value to session value if valid.
  do Value::job:Address_Line2
  do SendAlert

ValidateValue::job:Address_Line2  Routine
    If not (1=0)
    job:Address_Line2 = Upper(job:Address_Line2)
      if loc:invalid = '' then p_web.SetSessionValue('job:Address_Line2',job:Address_Line2).
    End

Value::job:Address_Line2  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line2') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    job:Address_Line2 = p_web.RestoreValue('job:Address_Line2')
    do ValidateValue::job:Address_Line2
    If job:Address_Line2:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- job:Address_Line2
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Address_Line2'',''amendaddress_job:address_line2_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Address_Line2',p_web.GetSessionValueFormat('job:Address_Line2'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::job:Address_Line2_Collection  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line2_Collection') & '_prompt',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,'nt-prompt-small','hidden')),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:CollectionAddress') = 1,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Address_Line2_Collection  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Address_Line2_Collection = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    job:Address_Line2_Collection = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::job:Address_Line2_Collection  ! copies value to session value if valid.
  do Value::job:Address_Line2_Collection
  do SendAlert

ValidateValue::job:Address_Line2_Collection  Routine
    If not (p_web.GSV('Hide:CollectionAddress') = 1)
    job:Address_Line2_Collection = Upper(job:Address_Line2_Collection)
      if loc:invalid = '' then p_web.SetSessionValue('job:Address_Line2_Collection',job:Address_Line2_Collection).
    End

Value::job:Address_Line2_Collection  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:CollectionAddress') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line2_Collection') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.GSV('ReadOnly:CollectionAddress') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    job:Address_Line2_Collection = p_web.RestoreValue('job:Address_Line2_Collection')
    do ValidateValue::job:Address_Line2_Collection
    If job:Address_Line2_Collection:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:CollectionAddress') = 1)
  ! --- STRING --- job:Address_Line2_Collection
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:CollectionAddress') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Address_Line2_Collection'',''amendaddress_job:address_line2_collection_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Address_Line2_Collection',p_web.GetSessionValueFormat('job:Address_Line2_Collection'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::job:Address_Line2_Delivery  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line2_Delivery') & '_prompt',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,'nt-prompt-small','hidden')),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Address_Line2_Delivery  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Address_Line2_Delivery = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    job:Address_Line2_Delivery = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::job:Address_Line2_Delivery  ! copies value to session value if valid.
  do Value::job:Address_Line2_Delivery
  do SendAlert

ValidateValue::job:Address_Line2_Delivery  Routine
    If not (p_web.GSV('Hide:DeliveryAddress') = 1)
    job:Address_Line2_Delivery = Upper(job:Address_Line2_Delivery)
      if loc:invalid = '' then p_web.SetSessionValue('job:Address_Line2_Delivery',job:Address_Line2_Delivery).
    End

Value::job:Address_Line2_Delivery  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:DeliveryAddress') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line2_Delivery') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.GSV('ReadOnly:DeliveryAddress') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    job:Address_Line2_Delivery = p_web.RestoreValue('job:Address_Line2_Delivery')
    do ValidateValue::job:Address_Line2_Delivery
    If job:Address_Line2_Delivery:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:DeliveryAddress') = 1)
  ! --- STRING --- job:Address_Line2_Delivery
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:DeliveryAddress') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Address_Line2_Delivery'',''amendaddress_job:address_line2_delivery_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Address_Line2_Delivery',p_web.GetSessionValueFormat('job:Address_Line2_Delivery'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::job:Address_Line3  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line3') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,'nt-prompt-small',)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Suburb'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Address_Line3  Routine
  ! GetDescription resets the 'NewValue' to the code, if the description is found. Returns 1 if code valid.
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('Value')
    if p_web.GetBrowseValue(p_web.GetValue('Value'),Net:Web:Record)
      p_web.DeleteValue('value')
    else
      loc:ok = p_web.GetDescription(SUBURB,sur:SuburbKey,sur:SuburbKey,sur:Suburb,sur:Suburb,p_web.GetValue('Value')) !7
    end
  Else
    loc:ok = p_web.GetDescription(SUBURB,sur:SuburbKey,sur:SuburbKey,sur:Suburb,sur:Suburb,p_web.GetSessionValue('job:Address_Line3')) !7
  End
  loc:lookupdone = 1
  If p_web.IfExistsValue('NewValue') then p_web.SetValue('Value',p_web.GetValue('NewValue')).
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Address_Line3 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    job:Address_Line3 = p_web.GetValue('Value')
  ElsIf p_web.RequestAjax = 1 and p_web.IfExistsValue('sur:Suburb')
    job:Address_Line3 = p_web.GetValue('sur:Suburb')
  ElsIf p_web.RequestAjax = 1
    job:Address_Line3 = sur:Suburb
  End
  do ValidateValue::job:Address_Line3  ! copies value to session value if valid.
      Access:SUBURB.ClearKey(sur:SuburbKey)
      sur:Suburb = p_web.GSV('job:Address_Line3')
      IF (Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign)
          p_web.SSV('job:Postcode',sur:Postcode)
          p_web.SSV('jobe2:HubCustomer',sur:Hub)
      END
  
  p_Web.SetValue('lookupfield','job:Address_Line3')
  do AfterLookup
  do Value::job:Postcode
  do Value::jobe2:HubCustomer
  do Value::job:Address_Line3
  do SendAlert
  do Value::jobe2:HubCustomer  !1
  do Value::job:Postcode  !1

ValidateValue::job:Address_Line3  Routine
    If not (1=0)
    job:Address_Line3 = Upper(job:Address_Line3)
      if loc:invalid = '' then p_web.SetSessionValue('job:Address_Line3',job:Address_Line3).
    End

Value::job:Address_Line3  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line3') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    job:Address_Line3 = p_web.RestoreValue('job:Address_Line3')
    do ValidateValue::job:Address_Line3
    If job:Address_Line3:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- job:Address_Line3
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Address_Line3'',''amendaddress_job:address_line3_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Address_Line3')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','job:Address_Line3',p_web.GetSessionValue('job:Address_Line3'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s30',loc:javascript,p_web.PicLength('@s30'),,,) & '<13,10>'!4
    if not loc:viewonly and not loc:readonly
      loc:fieldclass = ''
      packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LookupButton,loc:formname,,,'ntd.push(''LookupSuburbs'','''&p_web._nocolon('job:Address_Line3')&''','''&p_web.translate('Select Suburb')&''',1,'&Net:LookupRecord&',''sur:Suburb'',''AmendAddress'')',,,loc:fieldclass)
    End
  do SendPacket
  End
  p_web.DivFooter()

Prompt::job:Address_Line3_Collection  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line3_Collection') & '_prompt',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,'nt-prompt-small','hidden')),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:CollectionAddress') = 1,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Address_Line3_Collection  Routine
  ! GetDescription resets the 'NewValue' to the code, if the description is found. Returns 1 if code valid.
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('Value')
    if p_web.GetBrowseValue(p_web.GetValue('Value'),Net:Web:Record)
      p_web.DeleteValue('value')
    else
      loc:ok = p_web.GetDescription(SUBURB,sur:SuburbKey,sur:SuburbKey,sur:Suburb,sur:Suburb,p_web.GetValue('Value')) !7
    end
  Else
    loc:ok = p_web.GetDescription(SUBURB,sur:SuburbKey,sur:SuburbKey,sur:Suburb,sur:Suburb,p_web.GetSessionValue('job:Address_Line3_Collection')) !7
  End
  loc:lookupdone = 1
  If p_web.IfExistsValue('NewValue') then p_web.SetValue('Value',p_web.GetValue('NewValue')).
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Address_Line3_Collection = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    job:Address_Line3_Collection = p_web.GetValue('Value')
  ElsIf p_web.RequestAjax = 1 and p_web.IfExistsValue('sur:Suburb')
    job:Address_Line3_Collection = p_web.GetValue('sur:Suburb')
  ElsIf p_web.RequestAjax = 1
    job:Address_Line3_Collection = sur:Suburb
  End
  do ValidateValue::job:Address_Line3_Collection  ! copies value to session value if valid.
      Access:SUBURB.ClearKey(sur:SuburbKey)
      sur:Suburb = p_web.GSV('job:Address_Line3_Collection')
      IF (Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign)
          p_web.SSV('job:Postcode_Collection',sur:postcode)
          p_web.SSV('jobe2:HubCollection',sur:hub)
      END
  p_Web.SetValue('lookupfield','job:Address_Line3_Collection')
  do AfterLookup
  do Value::job:Postcode_Collection
  do Value::jobe2:HubCollection
  do Value::job:Address_Line3_Collection
  do SendAlert

ValidateValue::job:Address_Line3_Collection  Routine
    If not (p_web.GSV('Hide:CollectionAddress') = 1)
    job:Address_Line3_Collection = Upper(job:Address_Line3_Collection)
      if loc:invalid = '' then p_web.SetSessionValue('job:Address_Line3_Collection',job:Address_Line3_Collection).
    End

Value::job:Address_Line3_Collection  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:CollectionAddress') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line3_Collection') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.GSV('ReadOnly:CollectionAddress') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    job:Address_Line3_Collection = p_web.RestoreValue('job:Address_Line3_Collection')
    do ValidateValue::job:Address_Line3_Collection
    If job:Address_Line3_Collection:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:CollectionAddress') = 1)
  ! --- STRING --- job:Address_Line3_Collection
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:CollectionAddress') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Address_Line3_Collection'',''amendaddress_job:address_line3_collection_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Address_Line3_Collection')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','job:Address_Line3_Collection',p_web.GetSessionValue('job:Address_Line3_Collection'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s30',loc:javascript,p_web.PicLength('@s30'),,,) & '<13,10>'!4
    if not loc:viewonly and not loc:readonly
      loc:fieldclass = ''
      packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LookupButton,loc:formname,,,'ntd.push(''LookupSuburbs'','''&p_web._nocolon('job:Address_Line3_Collection')&''','''&p_web.translate('Select Suburb')&''',1,'&Net:LookupRecord&',''sur:Suburb'',''AmendAddress'')',,,loc:fieldclass)
    End
  do SendPacket
  End
  p_web.DivFooter()

Prompt::job:Address_Line3_Delivery  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line3_Delivery') & '_prompt',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,'nt-prompt-small','hidden')),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Address_Line3_Delivery  Routine
  ! GetDescription resets the 'NewValue' to the code, if the description is found. Returns 1 if code valid.
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('Value')
    if p_web.GetBrowseValue(p_web.GetValue('Value'),Net:Web:Record)
      p_web.DeleteValue('value')
    else
      loc:ok = p_web.GetDescription(SUBURB,sur:SuburbKey,sur:SuburbKey,sur:Suburb,sur:Suburb,p_web.GetValue('Value')) !7
    end
  Else
    loc:ok = p_web.GetDescription(SUBURB,sur:SuburbKey,sur:SuburbKey,sur:Suburb,sur:Suburb,p_web.GetSessionValue('job:Address_Line3_Delivery')) !7
  End
  loc:lookupdone = 1
  If p_web.IfExistsValue('NewValue') then p_web.SetValue('Value',p_web.GetValue('NewValue')).
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Address_Line3_Delivery = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    job:Address_Line3_Delivery = p_web.GetValue('Value')
  ElsIf p_web.RequestAjax = 1 and p_web.IfExistsValue('sur:Suburb')
    job:Address_Line3_Delivery = p_web.GetValue('sur:Suburb')
  ElsIf p_web.RequestAjax = 1
    job:Address_Line3_Delivery = sur:Suburb
  End
  do ValidateValue::job:Address_Line3_Delivery  ! copies value to session value if valid.
      Access:SUBURB.ClearKey(sur:SuburbKey)
      sur:Suburb = p_web.GSV('job:Address_Line3_Delivery')
      IF (Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign)
          p_web.SSV('job:Postcode_Delivery',sur:postcode)
          p_web.SSV('jobe2:HubDelivery',sur:hub)
      END
  p_Web.SetValue('lookupfield','job:Address_Line3_Delivery')
  do AfterLookup
  do Value::job:Postcode_Delivery
  do Value::jobe2:HubDelivery
  do Value::job:Address_Line3_Delivery
  do SendAlert

ValidateValue::job:Address_Line3_Delivery  Routine
    If not (p_web.GSV('Hide:DeliveryAddress') = 1)
    job:Address_Line3_Delivery = Upper(job:Address_Line3_Delivery)
      if loc:invalid = '' then p_web.SetSessionValue('job:Address_Line3_Delivery',job:Address_Line3_Delivery).
    End

Value::job:Address_Line3_Delivery  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:DeliveryAddress') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line3_Delivery') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.GSV('ReadOnly:DeliveryAddress') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    job:Address_Line3_Delivery = p_web.RestoreValue('job:Address_Line3_Delivery')
    do ValidateValue::job:Address_Line3_Delivery
    If job:Address_Line3_Delivery:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:DeliveryAddress') = 1)
  ! --- STRING --- job:Address_Line3_Delivery
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:DeliveryAddress') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Address_Line3_Delivery'',''amendaddress_job:address_line3_delivery_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Address_Line3_Delivery')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','job:Address_Line3_Delivery',p_web.GetSessionValue('job:Address_Line3_Delivery'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s30',loc:javascript,p_web.PicLength('@s30'),,,) & '<13,10>'!4
    if not loc:viewonly and not loc:readonly
      loc:fieldclass = ''
      packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LookupButton,loc:formname,,,'ntd.push(''LookupSuburbs'','''&p_web._nocolon('job:Address_Line3_Delivery')&''','''&p_web.translate('Select Suburb')&''',1,'&Net:LookupRecord&',''sur:Suburb'',''AmendAddress'')',,,loc:fieldclass)
    End
  do SendPacket
  End
  p_web.DivFooter()

Prompt::jobe2:HubCustomer  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddress_' & p_web._nocolon('jobe2:HubCustomer') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,'nt-prompt-small',)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Hub'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jobe2:HubCustomer  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jobe2:HubCustomer = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    jobe2:HubCustomer = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::jobe2:HubCustomer  ! copies value to session value if valid.
  do Value::jobe2:HubCustomer
  do SendAlert

ValidateValue::jobe2:HubCustomer  Routine
    If not (1=0)
    jobe2:HubCustomer = Upper(jobe2:HubCustomer)
      if loc:invalid = '' then p_web.SetSessionValue('jobe2:HubCustomer',jobe2:HubCustomer).
    End

Value::jobe2:HubCustomer  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddress_' & p_web._nocolon('jobe2:HubCustomer') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  If loc:retrying
    jobe2:HubCustomer = p_web.RestoreValue('jobe2:HubCustomer')
    do ValidateValue::jobe2:HubCustomer
    If jobe2:HubCustomer:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- jobe2:HubCustomer
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe2:HubCustomer'',''amendaddress_jobe2:hubcustomer_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jobe2:HubCustomer',p_web.GetSessionValueFormat('jobe2:HubCustomer'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'Hub',,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::jobe2:HubCollection  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddress_' & p_web._nocolon('jobe2:HubCollection') & '_prompt',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,'nt-prompt-small','hidden')),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:CollectionAddress') = 1,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jobe2:HubCollection  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jobe2:HubCollection = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    jobe2:HubCollection = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::jobe2:HubCollection  ! copies value to session value if valid.
  do Value::jobe2:HubCollection
  do SendAlert

ValidateValue::jobe2:HubCollection  Routine
    If not (p_web.GSV('Hide:CollectionAddress') = 1)
    jobe2:HubCollection = Upper(jobe2:HubCollection)
      if loc:invalid = '' then p_web.SetSessionValue('jobe2:HubCollection',jobe2:HubCollection).
    End

Value::jobe2:HubCollection  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:CollectionAddress') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddress_' & p_web._nocolon('jobe2:HubCollection') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  If loc:retrying
    jobe2:HubCollection = p_web.RestoreValue('jobe2:HubCollection')
    do ValidateValue::jobe2:HubCollection
    If jobe2:HubCollection:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:CollectionAddress') = 1)
  ! --- STRING --- jobe2:HubCollection
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe2:HubCollection'',''amendaddress_jobe2:hubcollection_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jobe2:HubCollection',p_web.GetSessionValueFormat('jobe2:HubCollection'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'Hub',,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::jobe2:HubDelivery  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddress_' & p_web._nocolon('jobe2:HubDelivery') & '_prompt',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,'nt-prompt-small','hidden')),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jobe2:HubDelivery  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jobe2:HubDelivery = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    jobe2:HubDelivery = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::jobe2:HubDelivery  ! copies value to session value if valid.
  do Value::jobe2:HubDelivery
  do SendAlert

ValidateValue::jobe2:HubDelivery  Routine
    If not (p_web.GSV('Hide:DeliveryAddress') = 1)
    jobe2:HubDelivery = Upper(jobe2:HubDelivery)
      if loc:invalid = '' then p_web.SetSessionValue('jobe2:HubDelivery',jobe2:HubDelivery).
    End

Value::jobe2:HubDelivery  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:DeliveryAddress') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddress_' & p_web._nocolon('jobe2:HubDelivery') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  If loc:retrying
    jobe2:HubDelivery = p_web.RestoreValue('jobe2:HubDelivery')
    do ValidateValue::jobe2:HubDelivery
    If jobe2:HubDelivery:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:DeliveryAddress') = 1)
  ! --- STRING --- jobe2:HubDelivery
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe2:HubDelivery'',''amendaddress_jobe2:hubdelivery_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jobe2:HubDelivery',p_web.GetSessionValueFormat('jobe2:HubDelivery'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'Hub',,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::job:Postcode  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddress_' & p_web._nocolon('job:Postcode') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,'nt-prompt-small',)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Postcode'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Postcode  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Postcode = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s10
    job:Postcode = p_web.Dformat(p_web.GetValue('Value'),'@s10')
  End
  do ValidateValue::job:Postcode  ! copies value to session value if valid.
  do Value::job:Postcode
  do SendAlert

ValidateValue::job:Postcode  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('job:Postcode',job:Postcode).
    End

Value::job:Postcode  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddress_' & p_web._nocolon('job:Postcode') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  If loc:retrying
    job:Postcode = p_web.RestoreValue('job:Postcode')
    do ValidateValue::job:Postcode
    If job:Postcode:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- job:Postcode
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Postcode'',''amendaddress_job:postcode_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Postcode',p_web.GetSessionValueFormat('job:Postcode'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s10'),,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::job:Postcode_Collection  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddress_' & p_web._nocolon('job:Postcode_Collection') & '_prompt',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,'nt-prompt-small','hidden')),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:CollectionAddress') = 1,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Postcode_Collection  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Postcode_Collection = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s10
    job:Postcode_Collection = p_web.Dformat(p_web.GetValue('Value'),'@s10')
  End
  do ValidateValue::job:Postcode_Collection  ! copies value to session value if valid.
  do Value::job:Postcode_Collection
  do SendAlert

ValidateValue::job:Postcode_Collection  Routine
    If not (p_web.GSV('Hide:CollectionAddress') = 1)
      if loc:invalid = '' then p_web.SetSessionValue('job:Postcode_Collection',job:Postcode_Collection).
    End

Value::job:Postcode_Collection  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:CollectionAddress') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddress_' & p_web._nocolon('job:Postcode_Collection') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  If loc:retrying
    job:Postcode_Collection = p_web.RestoreValue('job:Postcode_Collection')
    do ValidateValue::job:Postcode_Collection
    If job:Postcode_Collection:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:CollectionAddress') = 1)
  ! --- STRING --- job:Postcode_Collection
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Postcode_Collection'',''amendaddress_job:postcode_collection_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Postcode_Collection',p_web.GetSessionValueFormat('job:Postcode_Collection'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s10'),,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::job:Postcode_Delivery  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddress_' & p_web._nocolon('job:Postcode_Delivery') & '_prompt',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,'nt-prompt-small','hidden')),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Postcode_Delivery  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Postcode_Delivery = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s10
    job:Postcode_Delivery = p_web.Dformat(p_web.GetValue('Value'),'@s10')
  End
  do ValidateValue::job:Postcode_Delivery  ! copies value to session value if valid.
  do Value::job:Postcode_Delivery
  do SendAlert

ValidateValue::job:Postcode_Delivery  Routine
    If not (p_web.GSV('Hide:DeliveryAddress') = 1)
      if loc:invalid = '' then p_web.SetSessionValue('job:Postcode_Delivery',job:Postcode_Delivery).
    End

Value::job:Postcode_Delivery  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:DeliveryAddress') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddress_' & p_web._nocolon('job:Postcode_Delivery') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  If loc:retrying
    job:Postcode_Delivery = p_web.RestoreValue('job:Postcode_Delivery')
    do ValidateValue::job:Postcode_Delivery
    If job:Postcode_Delivery:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:DeliveryAddress') = 1)
  ! --- STRING --- job:Postcode_Delivery
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Postcode_Delivery'',''amendaddress_job:postcode_delivery_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Postcode_Delivery',p_web.GetSessionValueFormat('job:Postcode_Delivery'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s10'),,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::job:Telephone_Number  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddress_' & p_web._nocolon('job:Telephone_Number') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,'nt-prompt-small',)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Telephone Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Telephone_Number  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Telephone_Number = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s15
    job:Telephone_Number = p_web.Dformat(p_web.GetValue('Value'),'@s15')
  End
  do ValidateValue::job:Telephone_Number  ! copies value to session value if valid.
      If PassMobileNumberFormat(p_web.GSV('job:Telephone_Number')) = 0
          p_web.SSV('job:Telephone_Number','')
          p_web.SSV('Comment:TelephoneNumber','Number Format/Length Incorrect.')
      Else ! If PassMobileNumberFormat(p_web.GSV('job:Mobile_Number')) = 0
          p_web.SSV('Comment:TelephoneNumber','')
      End ! If PassMobileNumberFormat(p_web.GSV('job:Mobile_Number')) = 0
  do Value::job:Telephone_Number
  do SendAlert

ValidateValue::job:Telephone_Number  Routine
    If not (1=0)
    job:Telephone_Number = Upper(job:Telephone_Number)
      if loc:invalid = '' then p_web.SetSessionValue('job:Telephone_Number',job:Telephone_Number).
    End

Value::job:Telephone_Number  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddress_' & p_web._nocolon('job:Telephone_Number') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    job:Telephone_Number = p_web.RestoreValue('job:Telephone_Number')
    do ValidateValue::job:Telephone_Number
    If job:Telephone_Number:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- job:Telephone_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Telephone_Number'',''amendaddress_job:telephone_number_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Telephone_Number')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Telephone_Number',p_web.GetSessionValueFormat('job:Telephone_Number'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s15'),,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::job:Telephone_Collection  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddress_' & p_web._nocolon('job:Telephone_Collection') & '_prompt',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,'nt-prompt-small','hidden')),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:CollectionAddress') = 1,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Telephone_Collection  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Telephone_Collection = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s15
    job:Telephone_Collection = p_web.Dformat(p_web.GetValue('Value'),'@s15')
  End
  do ValidateValue::job:Telephone_Collection  ! copies value to session value if valid.
      If PassMobileNumberFormat(p_web.GSV('job:Telephone_Collection')) = 0
          p_web.SSV('job:Telephone_Collection','')
          p_web.SSV('Comment:TelephoneNumberCollection','Number Format/Length Incorrect.')
      Else ! If PassMobileNumberFormat(p_web.GSV('job:Mobile_Number')) = 0
          p_web.SSV('Comment:TelephoneNumberCollection','')
      End ! If PassMobileNumberFormat(p_web.GSV('job:Mobile_Number')) = 0
  do Value::job:Telephone_Collection
  do SendAlert

ValidateValue::job:Telephone_Collection  Routine
    If not (p_web.GSV('Hide:CollectionAddress') = 1)
    job:Telephone_Collection = Upper(job:Telephone_Collection)
      if loc:invalid = '' then p_web.SetSessionValue('job:Telephone_Collection',job:Telephone_Collection).
    End

Value::job:Telephone_Collection  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:CollectionAddress') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddress_' & p_web._nocolon('job:Telephone_Collection') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.GSV('ReadOnly:CollectionAddress') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    job:Telephone_Collection = p_web.RestoreValue('job:Telephone_Collection')
    do ValidateValue::job:Telephone_Collection
    If job:Telephone_Collection:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:CollectionAddress') = 1)
  ! --- STRING --- job:Telephone_Collection
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:CollectionAddress') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Telephone_Collection'',''amendaddress_job:telephone_collection_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Telephone_Collection',p_web.GetSessionValueFormat('job:Telephone_Collection'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s15'),,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::job:Telephone_Delivery  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddress_' & p_web._nocolon('job:Telephone_Delivery') & '_prompt',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,'nt-prompt-small','hidden')),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Telephone_Delivery  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Telephone_Delivery = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s15
    job:Telephone_Delivery = p_web.Dformat(p_web.GetValue('Value'),'@s15')
  End
  do ValidateValue::job:Telephone_Delivery  ! copies value to session value if valid.
      If PassMobileNumberFormat(p_web.GSV('job:Telephone_Delivery')) = 0
          p_web.SSV('job:Telephone_Delivery','')
          p_web.SSV('Comment:TelephoneNumberDelivery','Number Format/Length Incorrect.')
      Else ! If PassMobileNumberFormat(p_web.GSV('job:Mobile_Number')) = 0
          p_web.SSV('Comment:TelephoneNumberDelivery','')
      End ! If PassMobileNumberFormat(p_web.GSV('job:Mobile_Number')) = 0
  do Value::job:Telephone_Delivery
  do SendAlert

ValidateValue::job:Telephone_Delivery  Routine
    If not (p_web.GSV('Hide:DeliveryAddress') = 1)
    job:Telephone_Delivery = Upper(job:Telephone_Delivery)
      if loc:invalid = '' then p_web.SetSessionValue('job:Telephone_Delivery',job:Telephone_Delivery).
    End

Value::job:Telephone_Delivery  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:DeliveryAddress') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddress_' & p_web._nocolon('job:Telephone_Delivery') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.GSV('ReadOnly:DeliveryAddress') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    job:Telephone_Delivery = p_web.RestoreValue('job:Telephone_Delivery')
    do ValidateValue::job:Telephone_Delivery
    If job:Telephone_Delivery:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:DeliveryAddress') = 1)
  ! --- STRING --- job:Telephone_Delivery
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:DeliveryAddress') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Telephone_Delivery'',''amendaddress_job:telephone_delivery_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Telephone_Delivery',p_web.GetSessionValueFormat('job:Telephone_Delivery'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s15'),,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::job:Fax_Number  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddress_' & p_web._nocolon('job:Fax_Number') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,'nt-prompt-small',)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Fax Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Fax_Number  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Fax_Number = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s15
    job:Fax_Number = p_web.Dformat(p_web.GetValue('Value'),'@s15')
  End
  do ValidateValue::job:Fax_Number  ! copies value to session value if valid.
      If PassMobileNumberFormat(p_web.GSV('job:Fax_Number')) = 0
          p_web.SSV('job:Fax_Number','')
          p_web.SSV('Comment:FaxNumber','Number Format/Length Incorrect.')
      Else ! If PassMobileNumberFormat(p_web.GSV('job:Mobile_Number')) = 0
          p_web.SSV('Comment:FaxNumber','')
      End ! If PassMobileNumberFormat(p_web.GSV('job:Mobile_Number')) = 0
  do Value::job:Fax_Number
  do SendAlert

ValidateValue::job:Fax_Number  Routine
    If not (1=0)
    job:Fax_Number = Upper(job:Fax_Number)
      if loc:invalid = '' then p_web.SetSessionValue('job:Fax_Number',job:Fax_Number).
    End

Value::job:Fax_Number  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddress_' & p_web._nocolon('job:Fax_Number') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    job:Fax_Number = p_web.RestoreValue('job:Fax_Number')
    do ValidateValue::job:Fax_Number
    If job:Fax_Number:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- job:Fax_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Fax_Number'',''amendaddress_job:fax_number_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Fax_Number')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Fax_Number',p_web.GetSessionValueFormat('job:Fax_Number'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s15'),,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Validate::btnCopyCollectionAddress  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::btnCopyCollectionAddress  ! copies value to session value if valid.
  !Refresh
      Do Value::job:Postcode_Collection
      Do Value::job:Company_Name_Collection
      Do Value::job:Address_Line1_Collection
      Do Value::job:Address_Line2_Collection
      Do Value::job:Address_Line3_Collection
      Do Value::job:Telephone_Collection
      Do Value::jobe2:HubCollection
  do Value::btnCopyCollectionAddress

ValidateValue::btnCopyCollectionAddress  Routine
    If not (p_web.GSV('Hide:CollectionAddress') = 1)
    End

Value::btnCopyCollectionAddress  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:CollectionAddress') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddress_' & p_web._nocolon('btnCopyCollectionAddress') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('Hide:CollectionAddress') = 1)
  ! --- DISPLAY --- 
  loc:disabled = Choose(p_web.GSV('ReadOnly:CollectionAddress') = 1,1,0)
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnCopyCollectionAddress'',''amendaddress_btncopycollectionaddress_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','btnCopyCollectionAddress','Copy/Clear Address',p_web.combine(Choose('Copy/Clear Address' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'SmallButtonFixed'),loc:formname,,,p_web.OpenDialog('ClearAddressDetails',p_web._jsok('Copy/Clear Address'),0,'''' & lower('AmendAddress') & '''','','',p_web._jsok('btnCopyCollectionAddress'),p_web._jsok('AddType=C')),,loc:disabled,,,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()

Validate::btnCopyDeliveryAddress  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::btnCopyDeliveryAddress  ! copies value to session value if valid.
  !Refresh
      Do Value::job:Postcode_Delivery
      Do Value::job:Company_Name_Delivery
      Do Value::job:Address_Line1_Delivery
      Do Value::job:Address_Line2_Delivery
      Do Value::job:Address_Line3_Delivery
      Do Value::job:Telephone_Delivery
      Do Value::jobe2:HubDelivery    

ValidateValue::btnCopyDeliveryAddress  Routine
    If not (p_web.GSV('Hide:DeliveryAddress') = 1)
    End

Value::btnCopyDeliveryAddress  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:DeliveryAddress') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddress_' & p_web._nocolon('btnCopyDeliveryAddress') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('Hide:DeliveryAddress') = 1)
  ! --- DISPLAY --- 
  loc:disabled = Choose(p_web.GSV('ReadOnly:DeliveryAddress') = 1,1,0)
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','btnCopyDeliveryAddress','Copy/Clear Address',p_web.combine(Choose('Copy/Clear Address' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'SmallButtonFixed'),loc:formname,,,p_web.OpenDialog('ClearAddressDetails',p_web._jsok('Copy/Clear Address'),0,'''' & lower('AmendAddress') & '''','','',p_web._jsok('btnCopyDeliveryAddress'),p_web._jsok('AddType=D')),,loc:disabled,,,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()

Prompt::jobe:EndUserEmailAddress  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddress_' & p_web._nocolon('jobe:EndUserEmailAddress') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,'nt-prompt-small',)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Email Address'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jobe:EndUserEmailAddress  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jobe:EndUserEmailAddress = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s255
    jobe:EndUserEmailAddress = p_web.Dformat(p_web.GetValue('Value'),'@s255')
  End
  do ValidateValue::jobe:EndUserEmailAddress  ! copies value to session value if valid.
  do Value::jobe:EndUserEmailAddress
  do SendAlert

ValidateValue::jobe:EndUserEmailAddress  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('jobe:EndUserEmailAddress',jobe:EndUserEmailAddress).
    End

Value::jobe:EndUserEmailAddress  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddress_' & p_web._nocolon('jobe:EndUserEmailAddress') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    jobe:EndUserEmailAddress = p_web.RestoreValue('jobe:EndUserEmailAddress')
    do ValidateValue::jobe:EndUserEmailAddress
    If jobe:EndUserEmailAddress:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- jobe:EndUserEmailAddress
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe:EndUserEmailAddress'',''amendaddress_jobe:enduseremailaddress_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jobe:EndUserEmailAddress',p_web.GetSessionValueFormat('jobe:EndUserEmailAddress'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s255'),'Email Address',,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::jobe:EndUserTelNo  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddress_' & p_web._nocolon('jobe:EndUserTelNo') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,'nt-prompt-small',)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('End User Tel No'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jobe:EndUserTelNo  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jobe:EndUserTelNo = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s15
    jobe:EndUserTelNo = p_web.Dformat(p_web.GetValue('Value'),'@s15')
  End
  do ValidateValue::jobe:EndUserTelNo  ! copies value to session value if valid.
  do Value::jobe:EndUserTelNo
  do SendAlert

ValidateValue::jobe:EndUserTelNo  Routine
    If not (1=0)
    jobe:EndUserTelNo = Upper(jobe:EndUserTelNo)
      if loc:invalid = '' then p_web.SetSessionValue('jobe:EndUserTelNo',jobe:EndUserTelNo).
    End

Value::jobe:EndUserTelNo  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddress_' & p_web._nocolon('jobe:EndUserTelNo') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    jobe:EndUserTelNo = p_web.RestoreValue('jobe:EndUserTelNo')
    do ValidateValue::jobe:EndUserTelNo
    If jobe:EndUserTelNo:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- jobe:EndUserTelNo
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe:EndUserTelNo'',''amendaddress_jobe:endusertelno_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jobe:EndUserTelNo',p_web.GetSessionValueFormat('jobe:EndUserTelNo'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s15'),,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::jbn:Collection_Text  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddress_' & p_web._nocolon('jbn:Collection_Text') & '_prompt',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,'nt-prompt-small','hidden')),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:CollectionAddress') = 1,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jbn:Collection_Text  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jbn:Collection_Text = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s255
    jbn:Collection_Text = p_web.Dformat(p_web.GetValue('Value'),'@s255')
  End
  do ValidateValue::jbn:Collection_Text  ! copies value to session value if valid.
  do Value::jbn:Collection_Text
  do SendAlert

ValidateValue::jbn:Collection_Text  Routine
    If not (p_web.GSV('Hide:CollectionAddress') = 1)
    jbn:Collection_Text = Upper(jbn:Collection_Text)
      if loc:invalid = '' then p_web.SetSessionValue('jbn:Collection_Text',jbn:Collection_Text).
    End

Value::jbn:Collection_Text  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:CollectionAddress') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddress_' & p_web._nocolon('jbn:Collection_Text') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  If loc:retrying
    jbn:Collection_Text = p_web.RestoreValue('jbn:Collection_Text')
    do ValidateValue::jbn:Collection_Text
    If jbn:Collection_Text:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:CollectionAddress') = 1)
  ! --- TEXT --- jbn:Collection_Text
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jbn:Collection_Text'',''amendaddress_jbn:collection_text_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'readonly'
  do SendPacket
  p_web.CreateTextArea('jbn:Collection_Text',p_web.GetSessionValue('jbn:Collection_Text'),4,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(jbn:Collection_Text),,,,Net:Send)
  do SendPacket
  End
  p_web.DivFooter()

Prompt::jbn:Delivery_Text  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddress_' & p_web._nocolon('jbn:Delivery_Text') & '_prompt',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,'nt-prompt-small','hidden')),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jbn:Delivery_Text  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jbn:Delivery_Text = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s255
    jbn:Delivery_Text = p_web.Dformat(p_web.GetValue('Value'),'@s255')
  End
  do ValidateValue::jbn:Delivery_Text  ! copies value to session value if valid.
  do Value::jbn:Delivery_Text
  do SendAlert

ValidateValue::jbn:Delivery_Text  Routine
    If not (p_web.GSV('Hide:DeliveryAddress') = 1)
    jbn:Delivery_Text = Upper(jbn:Delivery_Text)
      if loc:invalid = '' then p_web.SetSessionValue('jbn:Delivery_Text',jbn:Delivery_Text).
    End

Value::jbn:Delivery_Text  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:DeliveryAddress') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddress_' & p_web._nocolon('jbn:Delivery_Text') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  If loc:retrying
    jbn:Delivery_Text = p_web.RestoreValue('jbn:Delivery_Text')
    do ValidateValue::jbn:Delivery_Text
    If jbn:Delivery_Text:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:DeliveryAddress') = 1)
  ! --- TEXT --- jbn:Delivery_Text
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jbn:Delivery_Text'',''amendaddress_jbn:delivery_text_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'readonly'
  do SendPacket
  p_web.CreateTextArea('jbn:Delivery_Text',p_web.GetSessionValue('jbn:Delivery_Text'),4,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(jbn:Delivery_Text),,,,Net:Send)
  do SendPacket
  End
  p_web.DivFooter()

Prompt::jobe:VatNumber  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddress_' & p_web._nocolon('jobe:VatNumber') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,'nt-prompt-small',)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Vat Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jobe:VatNumber  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jobe:VatNumber = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    jobe:VatNumber = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::jobe:VatNumber  ! copies value to session value if valid.
  do Value::jobe:VatNumber
  do SendAlert

ValidateValue::jobe:VatNumber  Routine
  If p_web.GSV('Hide:VatNumber') <> 1
    If not (1=0)
    jobe:VatNumber = Upper(jobe:VatNumber)
      if loc:invalid = '' then p_web.SetSessionValue('jobe:VatNumber',jobe:VatNumber).
    End
  End

Value::jobe:VatNumber  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddress_' & p_web._nocolon('jobe:VatNumber') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    jobe:VatNumber = p_web.RestoreValue('jobe:VatNumber')
    do ValidateValue::jobe:VatNumber
    If jobe:VatNumber:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- jobe:VatNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe:VatNumber'',''amendaddress_jobe:vatnumber_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jobe:VatNumber',p_web.GetSessionValueFormat('jobe:VatNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'Vat Number',,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::jobe2:IDNumber  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddress_' & p_web._nocolon('jobe2:IDNumber') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,'nt-prompt-small',)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('ID Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jobe2:IDNumber  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jobe2:IDNumber = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s13
    jobe2:IDNumber = p_web.Dformat(p_web.GetValue('Value'),'@s13')
  End
  do ValidateValue::jobe2:IDNumber  ! copies value to session value if valid.
  do Value::jobe2:IDNumber
  do SendAlert

ValidateValue::jobe2:IDNumber  Routine
    If not (1=0)
    jobe2:IDNumber = Upper(jobe2:IDNumber)
      if loc:invalid = '' then p_web.SetSessionValue('jobe2:IDNumber',jobe2:IDNumber).
    End

Value::jobe2:IDNumber  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddress_' & p_web._nocolon('jobe2:IDNumber') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    jobe2:IDNumber = p_web.RestoreValue('jobe2:IDNumber')
    do ValidateValue::jobe2:IDNumber
    If jobe2:IDNumber:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- jobe2:IDNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe2:IDNumber'',''amendaddress_jobe2:idnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('jobe2:IDNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jobe2:IDNumber',p_web.GetSessionValueFormat('jobe2:IDNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s13'),'I.D. Number',,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::jobe2:SMSNotification  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddress_' & p_web._nocolon('jobe2:SMSNotification') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,'nt-prompt-small',)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('SMS Notification'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jobe2:SMSNotification  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jobe2:SMSNotification = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    jobe2:SMSNotification = p_web.GetValue('Value')
  End
  do ValidateValue::jobe2:SMSNotification  ! copies value to session value if valid.
  do Value::jobe2:SMSNotification
  do SendAlert
  do Prompt::jobe2:SMSAlertNumber
  do Value::jobe2:SMSAlertNumber  !1

ValidateValue::jobe2:SMSNotification  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('jobe2:SMSNotification',jobe2:SMSNotification).
    End

Value::jobe2:SMSNotification  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddress_' & p_web._nocolon('jobe2:SMSNotification') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If loc:retrying
    jobe2:SMSNotification = p_web.RestoreValue('jobe2:SMSNotification')
    do ValidateValue::jobe2:SMSNotification
    If jobe2:SMSNotification:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- CHECKBOX --- jobe2:SMSNotification
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''jobe2:SMSNotification'',''amendaddress_jobe2:smsnotification_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('jobe2:SMSNotification')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('jobe2:SMSNotification') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','jobe2:SMSNotification',clip(1),,loc:readonly,,,loc:javascript,,'SMS Notification') & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::jobe2:SMSAlertNumber  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddress_' & p_web._nocolon('jobe2:SMSAlertNumber') & '_prompt',Choose(p_web.GSV('jobe2:SMSNotification') = 0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,'nt-prompt-small',)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('jobe2:SMSNotification') = 0,'',p_web.Translate('Mobile No'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jobe2:SMSAlertNumber  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jobe2:SMSAlertNumber = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    jobe2:SMSAlertNumber = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::jobe2:SMSAlertNumber  ! copies value to session value if valid.
      If PassMobileNumberFormat(p_web.GSV('jobe2:SMSAlertNumber')) = 0
          p_web.SSV('jobe2:SMSAlertNumber','')
          p_web.SSV('Comment:SMSAlertNumber','Number Format/Length Incorrect.')
      Else ! If PassMobileNumberFormat(p_web.GSV('job:Mobile_Number')) = 0
          p_web.SSV('Comment:SMSAlertNumber','')
      End ! If PassMobileNumberFormat(p_web.GSV('job:Mobile_Number')) = 0
  do Value::jobe2:SMSAlertNumber
  do SendAlert

ValidateValue::jobe2:SMSAlertNumber  Routine
    If not (p_web.GSV('jobe2:SMSNotification') = 0)
    jobe2:SMSAlertNumber = Upper(jobe2:SMSAlertNumber)
      if loc:invalid = '' then p_web.SetSessionValue('jobe2:SMSAlertNumber',jobe2:SMSAlertNumber).
    End

Value::jobe2:SMSAlertNumber  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('jobe2:SMSNotification') = 0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddress_' & p_web._nocolon('jobe2:SMSAlertNumber') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    jobe2:SMSAlertNumber = p_web.RestoreValue('jobe2:SMSAlertNumber')
    do ValidateValue::jobe2:SMSAlertNumber
    If jobe2:SMSAlertNumber:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('jobe2:SMSNotification') = 0)
  ! --- STRING --- jobe2:SMSAlertNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe2:SMSAlertNumber'',''amendaddress_jobe2:smsalertnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('jobe2:SMSAlertNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jobe2:SMSAlertNumber',p_web.GetSessionValueFormat('jobe2:SMSAlertNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'SMS Alert Mobile Number',,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Validate::btnCollectionText  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::btnCollectionText  ! copies value to session value if valid.
  do Value::jbn:Collection_Text  !1

ValidateValue::btnCollectionText  Routine
    If not (p_web.GSV('Hide:CollectionAddress') = 1)
    End

Value::btnCollectionText  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:CollectionAddress') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddress_' & p_web._nocolon('btnCollectionText') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('Hide:CollectionAddress') = 1)
  ! --- DISPLAY --- 
  loc:disabled = Choose(p_web.GSV('ReadOnly:CollectionAddress') = 1,1,0)
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnCollectionText'',''amendaddress_btncollectiontext_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','btnCollectionText','Collection Text',p_web.combine(Choose('Collection Text' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'SmallButtonFixed'),loc:formname,,,p_web.OpenDialog('FormCollectionText',p_web._jsok('Collection Text'),0,'''' & lower('AmendAddress') & '''','','',p_web._jsok('btnCollectionText'),p_web._jsok('')),,loc:disabled,,,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()

Validate::btnDeliveryText  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::btnDeliveryText  ! copies value to session value if valid.
  do Value::jbn:Delivery_Text  !1

ValidateValue::btnDeliveryText  Routine
    If not (p_web.GSV('Hide:DeliveryAddress') = 1)
    End

Value::btnDeliveryText  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:DeliveryAddress') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddress_' & p_web._nocolon('btnDeliveryText') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('Hide:DeliveryAddress') = 1)
  ! --- DISPLAY --- 
  loc:disabled = Choose(p_web.GSV('ReadOnly:DeliveryAddress') = 1,1,0)
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnDeliveryText'',''amendaddress_btndeliverytext_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','btnDeliveryText','Delivery Text',p_web.combine(Choose('Delivery Text' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'SmallButtonFixed'),loc:formname,,,p_web.OpenDialog('FormDeliveryText',p_web._jsok('DeliveryText'),0,'''' & lower('AmendAddress') & '''','','',p_web._jsok('btnDeliveryText'),p_web._jsok('')),,loc:disabled,,,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()

Prompt::jobe2:EmailNotification  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddress_' & p_web._nocolon('jobe2:EmailNotification') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,'nt-prompt-small',)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Email Notification'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jobe2:EmailNotification  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jobe2:EmailNotification = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    jobe2:EmailNotification = p_web.GetValue('Value')
  End
  do ValidateValue::jobe2:EmailNotification  ! copies value to session value if valid.
  do Value::jobe2:EmailNotification
  do SendAlert
  do Prompt::jobe2:EmailAlertAddress
  do Value::jobe2:EmailAlertAddress  !1

ValidateValue::jobe2:EmailNotification  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('jobe2:EmailNotification',jobe2:EmailNotification).
    End

Value::jobe2:EmailNotification  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddress_' & p_web._nocolon('jobe2:EmailNotification') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If loc:retrying
    jobe2:EmailNotification = p_web.RestoreValue('jobe2:EmailNotification')
    do ValidateValue::jobe2:EmailNotification
    If jobe2:EmailNotification:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- CHECKBOX --- jobe2:EmailNotification
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''jobe2:EmailNotification'',''amendaddress_jobe2:emailnotification_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('jobe2:EmailNotification')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('jobe2:EmailNotification') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','jobe2:EmailNotification',clip(1),,loc:readonly,,,loc:javascript,,'Email Notification') & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::jobe2:EmailAlertAddress  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddress_' & p_web._nocolon('jobe2:EmailAlertAddress') & '_prompt',Choose(p_web.GSV('jobe2:EmailNotification') = 0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,'nt-prompt-small',)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('jobe2:EmailNotification') = 0,'',p_web.Translate('Email Address'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jobe2:EmailAlertAddress  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jobe2:EmailAlertAddress = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s255
    jobe2:EmailAlertAddress = p_web.Dformat(p_web.GetValue('Value'),'@s255')
  End
  do ValidateValue::jobe2:EmailAlertAddress  ! copies value to session value if valid.
  do Value::jobe2:EmailAlertAddress
  do SendAlert

ValidateValue::jobe2:EmailAlertAddress  Routine
    If not (p_web.GSV('jobe2:EmailNotification') = 0)
      if loc:invalid = '' then p_web.SetSessionValue('jobe2:EmailAlertAddress',jobe2:EmailAlertAddress).
    End

Value::jobe2:EmailAlertAddress  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('jobe2:EmailNotification') = 0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddress_' & p_web._nocolon('jobe2:EmailAlertAddress') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    jobe2:EmailAlertAddress = p_web.RestoreValue('jobe2:EmailAlertAddress')
    do ValidateValue::jobe2:EmailAlertAddress
    If jobe2:EmailAlertAddress:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('jobe2:EmailNotification') = 0)
  ! --- STRING --- jobe2:EmailAlertAddress
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe2:EmailAlertAddress'',''amendaddress_jobe2:emailalertaddress_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jobe2:EmailAlertAddress',p_web.GetSessionValueFormat('jobe2:EmailAlertAddress'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s255'),'Email Alert Address',,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::jobe2:CourierWaybillNumber  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddress_' & p_web._nocolon('jobe2:CourierWaybillNumber') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,'nt-prompt-small',)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Courier Waybill Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jobe2:CourierWaybillNumber  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jobe2:CourierWaybillNumber = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    jobe2:CourierWaybillNumber = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::jobe2:CourierWaybillNumber  ! copies value to session value if valid.
  do Value::jobe2:CourierWaybillNumber
  do SendAlert

ValidateValue::jobe2:CourierWaybillNumber  Routine
    If not (1=0)
    jobe2:CourierWaybillNumber = Upper(jobe2:CourierWaybillNumber)
      if loc:invalid = '' then p_web.SetSessionValue('jobe2:CourierWaybillNumber',jobe2:CourierWaybillNumber).
    End

Value::jobe2:CourierWaybillNumber  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddress_' & p_web._nocolon('jobe2:CourierWaybillNumber') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    jobe2:CourierWaybillNumber = p_web.RestoreValue('jobe2:CourierWaybillNumber')
    do ValidateValue::jobe2:CourierWaybillNumber
    If jobe2:CourierWaybillNumber:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- jobe2:CourierWaybillNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe2:CourierWaybillNumber'',''amendaddress_jobe2:courierwaybillnumber_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jobe2:CourierWaybillNumber',p_web.GetSessionValueFormat('jobe2:CourierWaybillNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'Courier Waybill Number',,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::jobe:Sub_Sub_Account  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddress_' & p_web._nocolon('jobe:Sub_Sub_Account') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,'nt-prompt-small',)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Account Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jobe:Sub_Sub_Account  Routine
  ! GetDescription resets the 'NewValue' to the code, if the description is found. Returns 1 if code valid.
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('Value')
    if p_web.GetBrowseValue(p_web.GetValue('Value'),Net:Web:Record)
      p_web.DeleteValue('value')
    else
      loc:ok = p_web.GetDescription(SUBACCAD,sua:AccountNumberOnlyKey,sua:AccountNumberOnlyKey,sua:AccountNumber,sua:AccountNumber,p_web.GetValue('Value')) !7
    end
  Else
    loc:ok = p_web.GetDescription(SUBACCAD,sua:AccountNumberOnlyKey,sua:AccountNumberOnlyKey,sua:AccountNumber,sua:AccountNumber,p_web.GetSessionValue('jobe:Sub_Sub_Account')) !7
  End
  loc:lookupdone = 1
  If p_web.IfExistsValue('NewValue') then p_web.SetValue('Value',p_web.GetValue('NewValue')).
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jobe:Sub_Sub_Account = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    jobe:Sub_Sub_Account = p_web.GetValue('Value')
  ElsIf p_web.RequestAjax = 1 and p_web.IfExistsValue('sua:AccountNumber')
    jobe:Sub_Sub_Account = p_web.GetValue('sua:AccountNumber')
  ElsIf p_web.RequestAjax = 1
    jobe:Sub_Sub_Account = sua:AccountNumber
  End
  do ValidateValue::jobe:Sub_Sub_Account  ! copies value to session value if valid.
  p_Web.SetValue('lookupfield','jobe:Sub_Sub_Account')
  do AfterLookup
  do Value::job:Postcode_Delivery
  do Value::job:Address_Line1_Delivery
  do Value::job:Address_Line2_Delivery
  do Value::job:Address_Line3_Delivery
  do Value::job:Telephone_Delivery
  do Value::job:Company_Name_Delivery
  do Value::jobe:Sub_Sub_Account
  do SendAlert

ValidateValue::jobe:Sub_Sub_Account  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('jobe:Sub_Sub_Account',jobe:Sub_Sub_Account).
    End

Value::jobe:Sub_Sub_Account  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddress_' & p_web._nocolon('jobe:Sub_Sub_Account') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    jobe:Sub_Sub_Account = p_web.RestoreValue('jobe:Sub_Sub_Account')
    do ValidateValue::jobe:Sub_Sub_Account
    If jobe:Sub_Sub_Account:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- jobe:Sub_Sub_Account
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe:Sub_Sub_Account'',''amendaddress_jobe:sub_sub_account_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('jobe:Sub_Sub_Account')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = 'readonly'
    packet = clip(packet) & p_web.CreateInput('text','jobe:Sub_Sub_Account',p_web.GetSessionValue('jobe:Sub_Sub_Account'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s15',loc:javascript,p_web.PicLength('@s15'),,,) & '<13,10>'!4
    if not loc:viewonly and not loc:readonly
      loc:fieldclass = ''
      packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LookupButton,loc:formname,,,'ntd.push(''BrowseSubAddresses'','''&p_web._nocolon('jobe:Sub_Sub_Account')&''','''&p_web.translate('Select Account Number')&''',1,'&Net:LookupRecord&',''sua:CompanyName'',''AmendAddress'')',,,loc:fieldclass)
    End
  do SendPacket
  End
  p_web.DivFooter()

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('AmendAddress_nexttab_' & 0)
    job:Company_Name = p_web.GSV('job:Company_Name')
    do ValidateValue::job:Company_Name
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Company_Name
      !do SendAlert
      !exit
    End
    job:Company_Name_Collection = p_web.GSV('job:Company_Name_Collection')
    do ValidateValue::job:Company_Name_Collection
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Company_Name_Collection
      !do SendAlert
      !exit
    End
    job:Company_Name_Delivery = p_web.GSV('job:Company_Name_Delivery')
    do ValidateValue::job:Company_Name_Delivery
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Company_Name_Delivery
      !do SendAlert
      !exit
    End
    job:Address_Line1 = p_web.GSV('job:Address_Line1')
    do ValidateValue::job:Address_Line1
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Address_Line1
      !do SendAlert
      !exit
    End
    job:Address_Line1_Collection = p_web.GSV('job:Address_Line1_Collection')
    do ValidateValue::job:Address_Line1_Collection
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Address_Line1_Collection
      !do SendAlert
      !exit
    End
    job:Address_Line1_Delivery = p_web.GSV('job:Address_Line1_Delivery')
    do ValidateValue::job:Address_Line1_Delivery
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Address_Line1_Delivery
      !do SendAlert
      !exit
    End
    job:Address_Line2 = p_web.GSV('job:Address_Line2')
    do ValidateValue::job:Address_Line2
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Address_Line2
      !do SendAlert
      !exit
    End
    job:Address_Line2_Collection = p_web.GSV('job:Address_Line2_Collection')
    do ValidateValue::job:Address_Line2_Collection
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Address_Line2_Collection
      !do SendAlert
      !exit
    End
    job:Address_Line2_Delivery = p_web.GSV('job:Address_Line2_Delivery')
    do ValidateValue::job:Address_Line2_Delivery
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Address_Line2_Delivery
      !do SendAlert
      !exit
    End
    job:Address_Line3 = p_web.GSV('job:Address_Line3')
    do ValidateValue::job:Address_Line3
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Address_Line3
      !do SendAlert
      !exit
    End
    job:Address_Line3_Collection = p_web.GSV('job:Address_Line3_Collection')
    do ValidateValue::job:Address_Line3_Collection
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Address_Line3_Collection
      !do SendAlert
      !exit
    End
    job:Address_Line3_Delivery = p_web.GSV('job:Address_Line3_Delivery')
    do ValidateValue::job:Address_Line3_Delivery
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Address_Line3_Delivery
      !do SendAlert
      !exit
    End
    jobe2:HubCustomer = p_web.GSV('jobe2:HubCustomer')
    do ValidateValue::jobe2:HubCustomer
    If loc:Invalid
      loc:retrying = 1
      do Value::jobe2:HubCustomer
      !do SendAlert
      !exit
    End
    jobe2:HubCollection = p_web.GSV('jobe2:HubCollection')
    do ValidateValue::jobe2:HubCollection
    If loc:Invalid
      loc:retrying = 1
      do Value::jobe2:HubCollection
      !do SendAlert
      !exit
    End
    jobe2:HubDelivery = p_web.GSV('jobe2:HubDelivery')
    do ValidateValue::jobe2:HubDelivery
    If loc:Invalid
      loc:retrying = 1
      do Value::jobe2:HubDelivery
      !do SendAlert
      !exit
    End
    job:Postcode = p_web.GSV('job:Postcode')
    do ValidateValue::job:Postcode
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Postcode
      !do SendAlert
      !exit
    End
    job:Postcode_Collection = p_web.GSV('job:Postcode_Collection')
    do ValidateValue::job:Postcode_Collection
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Postcode_Collection
      !do SendAlert
      !exit
    End
    job:Postcode_Delivery = p_web.GSV('job:Postcode_Delivery')
    do ValidateValue::job:Postcode_Delivery
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Postcode_Delivery
      !do SendAlert
      !exit
    End
    job:Telephone_Number = p_web.GSV('job:Telephone_Number')
    do ValidateValue::job:Telephone_Number
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Telephone_Number
      !do SendAlert
      !exit
    End
    job:Telephone_Collection = p_web.GSV('job:Telephone_Collection')
    do ValidateValue::job:Telephone_Collection
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Telephone_Collection
      !do SendAlert
      !exit
    End
    job:Telephone_Delivery = p_web.GSV('job:Telephone_Delivery')
    do ValidateValue::job:Telephone_Delivery
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Telephone_Delivery
      !do SendAlert
      !exit
    End
    job:Fax_Number = p_web.GSV('job:Fax_Number')
    do ValidateValue::job:Fax_Number
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Fax_Number
      !do SendAlert
      !exit
    End
    jobe:EndUserEmailAddress = p_web.GSV('jobe:EndUserEmailAddress')
    do ValidateValue::jobe:EndUserEmailAddress
    If loc:Invalid
      loc:retrying = 1
      do Value::jobe:EndUserEmailAddress
      !do SendAlert
      !exit
    End
    jobe:EndUserTelNo = p_web.GSV('jobe:EndUserTelNo')
    do ValidateValue::jobe:EndUserTelNo
    If loc:Invalid
      loc:retrying = 1
      do Value::jobe:EndUserTelNo
      !do SendAlert
      !exit
    End
    jbn:Collection_Text = p_web.GSV('jbn:Collection_Text')
    do ValidateValue::jbn:Collection_Text
    If loc:Invalid
      loc:retrying = 1
      do Value::jbn:Collection_Text
      !do SendAlert
      !exit
    End
    jbn:Delivery_Text = p_web.GSV('jbn:Delivery_Text')
    do ValidateValue::jbn:Delivery_Text
    If loc:Invalid
      loc:retrying = 1
      do Value::jbn:Delivery_Text
      !do SendAlert
      !exit
    End
    jobe:VatNumber = p_web.GSV('jobe:VatNumber')
    do ValidateValue::jobe:VatNumber
    If loc:Invalid
      loc:retrying = 1
      do Value::jobe:VatNumber
      !do SendAlert
      !exit
    End
    jobe2:IDNumber = p_web.GSV('jobe2:IDNumber')
    do ValidateValue::jobe2:IDNumber
    If loc:Invalid
      loc:retrying = 1
      do Value::jobe2:IDNumber
      !do SendAlert
      !exit
    End
    jobe2:SMSNotification = p_web.GSV('jobe2:SMSNotification')
    do ValidateValue::jobe2:SMSNotification
    If loc:Invalid
      loc:retrying = 1
      do Value::jobe2:SMSNotification
      !do SendAlert
      !exit
    End
    jobe2:SMSAlertNumber = p_web.GSV('jobe2:SMSAlertNumber')
    do ValidateValue::jobe2:SMSAlertNumber
    If loc:Invalid
      loc:retrying = 1
      do Value::jobe2:SMSAlertNumber
      !do SendAlert
      !exit
    End
    jobe2:EmailNotification = p_web.GSV('jobe2:EmailNotification')
    do ValidateValue::jobe2:EmailNotification
    If loc:Invalid
      loc:retrying = 1
      do Value::jobe2:EmailNotification
      !do SendAlert
      !exit
    End
    jobe2:EmailAlertAddress = p_web.GSV('jobe2:EmailAlertAddress')
    do ValidateValue::jobe2:EmailAlertAddress
    If loc:Invalid
      loc:retrying = 1
      do Value::jobe2:EmailAlertAddress
      !do SendAlert
      !exit
    End
    jobe2:CourierWaybillNumber = p_web.GSV('jobe2:CourierWaybillNumber')
    do ValidateValue::jobe2:CourierWaybillNumber
    If loc:Invalid
      loc:retrying = 1
      do Value::jobe2:CourierWaybillNumber
      !do SendAlert
      !exit
    End
    If loc:Invalid then exit.
  of lower('AmendAddress_nexttab_' & 1)
    jobe:Sub_Sub_Account = p_web.GSV('jobe:Sub_Sub_Account')
    do ValidateValue::jobe:Sub_Sub_Account
    If loc:Invalid
      loc:retrying = 1
      do Value::jobe:Sub_Sub_Account
      !do SendAlert
      !exit
    End
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_AmendAddress_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('AmendAddress_tab_' & 0)
    do GenerateTab0
  of lower('AmendAddress_job:Company_Name_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Company_Name
      of event:timer
        do Value::job:Company_Name
      else
        do Value::job:Company_Name
      end
  of lower('AmendAddress_job:Company_Name_Collection_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Company_Name_Collection
      of event:timer
        do Value::job:Company_Name_Collection
      else
        do Value::job:Company_Name_Collection
      end
  of lower('AmendAddress_job:Company_Name_Delivery_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Company_Name_Delivery
      of event:timer
        do Value::job:Company_Name_Delivery
      else
        do Value::job:Company_Name_Delivery
      end
  of lower('AmendAddress_job:Address_Line1_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Address_Line1
      of event:timer
        do Value::job:Address_Line1
      else
        do Value::job:Address_Line1
      end
  of lower('AmendAddress_job:Address_Line1_Collection_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Address_Line1_Collection
      of event:timer
        do Value::job:Address_Line1_Collection
      else
        do Value::job:Address_Line1_Collection
      end
  of lower('AmendAddress_job:Address_Line1_Delivery_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Address_Line1_Delivery
      of event:timer
        do Value::job:Address_Line1_Delivery
      else
        do Value::job:Address_Line1_Delivery
      end
  of lower('AmendAddress_job:Address_Line2_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Address_Line2
      of event:timer
        do Value::job:Address_Line2
      else
        do Value::job:Address_Line2
      end
  of lower('AmendAddress_job:Address_Line2_Collection_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Address_Line2_Collection
      of event:timer
        do Value::job:Address_Line2_Collection
      else
        do Value::job:Address_Line2_Collection
      end
  of lower('AmendAddress_job:Address_Line2_Delivery_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Address_Line2_Delivery
      of event:timer
        do Value::job:Address_Line2_Delivery
      else
        do Value::job:Address_Line2_Delivery
      end
  of lower('AmendAddress_job:Address_Line3_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Address_Line3
      of event:timer
        do Value::job:Address_Line3
      else
        do Value::job:Address_Line3
      end
  of lower('AmendAddress_job:Address_Line3_Collection_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Address_Line3_Collection
      of event:timer
        do Value::job:Address_Line3_Collection
      else
        do Value::job:Address_Line3_Collection
      end
  of lower('AmendAddress_job:Address_Line3_Delivery_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Address_Line3_Delivery
      of event:timer
        do Value::job:Address_Line3_Delivery
      else
        do Value::job:Address_Line3_Delivery
      end
  of lower('AmendAddress_jobe2:HubCustomer_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:HubCustomer
      of event:timer
        do Value::jobe2:HubCustomer
      else
        do Value::jobe2:HubCustomer
      end
  of lower('AmendAddress_jobe2:HubCollection_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:HubCollection
      of event:timer
        do Value::jobe2:HubCollection
      else
        do Value::jobe2:HubCollection
      end
  of lower('AmendAddress_jobe2:HubDelivery_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:HubDelivery
      of event:timer
        do Value::jobe2:HubDelivery
      else
        do Value::jobe2:HubDelivery
      end
  of lower('AmendAddress_job:Postcode_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Postcode
      of event:timer
        do Value::job:Postcode
      else
        do Value::job:Postcode
      end
  of lower('AmendAddress_job:Postcode_Collection_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Postcode_Collection
      of event:timer
        do Value::job:Postcode_Collection
      else
        do Value::job:Postcode_Collection
      end
  of lower('AmendAddress_job:Postcode_Delivery_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Postcode_Delivery
      of event:timer
        do Value::job:Postcode_Delivery
      else
        do Value::job:Postcode_Delivery
      end
  of lower('AmendAddress_job:Telephone_Number_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Telephone_Number
      of event:timer
        do Value::job:Telephone_Number
      else
        do Value::job:Telephone_Number
      end
  of lower('AmendAddress_job:Telephone_Collection_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Telephone_Collection
      of event:timer
        do Value::job:Telephone_Collection
      else
        do Value::job:Telephone_Collection
      end
  of lower('AmendAddress_job:Telephone_Delivery_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Telephone_Delivery
      of event:timer
        do Value::job:Telephone_Delivery
      else
        do Value::job:Telephone_Delivery
      end
  of lower('AmendAddress_job:Fax_Number_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Fax_Number
      of event:timer
        do Value::job:Fax_Number
      else
        do Value::job:Fax_Number
      end
  of lower('AmendAddress_btnCopyCollectionAddress_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnCopyCollectionAddress
      of event:timer
        do Value::btnCopyCollectionAddress
      else
        do Value::btnCopyCollectionAddress
      end
  of lower('AmendAddress_jobe:EndUserEmailAddress_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe:EndUserEmailAddress
      of event:timer
        do Value::jobe:EndUserEmailAddress
      else
        do Value::jobe:EndUserEmailAddress
      end
  of lower('AmendAddress_jobe:EndUserTelNo_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe:EndUserTelNo
      of event:timer
        do Value::jobe:EndUserTelNo
      else
        do Value::jobe:EndUserTelNo
      end
  of lower('AmendAddress_jbn:Collection_Text_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jbn:Collection_Text
      of event:timer
        do Value::jbn:Collection_Text
      else
        do Value::jbn:Collection_Text
      end
  of lower('AmendAddress_jbn:Delivery_Text_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jbn:Delivery_Text
      of event:timer
        do Value::jbn:Delivery_Text
      else
        do Value::jbn:Delivery_Text
      end
  of lower('AmendAddress_jobe:VatNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe:VatNumber
      of event:timer
        do Value::jobe:VatNumber
      else
        do Value::jobe:VatNumber
      end
  of lower('AmendAddress_jobe2:IDNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:IDNumber
      of event:timer
        do Value::jobe2:IDNumber
      else
        do Value::jobe2:IDNumber
      end
  of lower('AmendAddress_jobe2:SMSNotification_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:SMSNotification
      of event:timer
        do Value::jobe2:SMSNotification
      else
        do Value::jobe2:SMSNotification
      end
  of lower('AmendAddress_jobe2:SMSAlertNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:SMSAlertNumber
      of event:timer
        do Value::jobe2:SMSAlertNumber
      else
        do Value::jobe2:SMSAlertNumber
      end
  of lower('AmendAddress_btnCollectionText_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnCollectionText
      of event:timer
        do Value::btnCollectionText
      else
        do Value::btnCollectionText
      end
  of lower('AmendAddress_btnDeliveryText_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnDeliveryText
      of event:timer
        do Value::btnDeliveryText
      else
        do Value::btnDeliveryText
      end
  of lower('AmendAddress_jobe2:EmailNotification_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:EmailNotification
      of event:timer
        do Value::jobe2:EmailNotification
      else
        do Value::jobe2:EmailNotification
      end
  of lower('AmendAddress_jobe2:EmailAlertAddress_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:EmailAlertAddress
      of event:timer
        do Value::jobe2:EmailAlertAddress
      else
        do Value::jobe2:EmailAlertAddress
      end
  of lower('AmendAddress_jobe2:CourierWaybillNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:CourierWaybillNumber
      of event:timer
        do Value::jobe2:CourierWaybillNumber
      else
        do Value::jobe2:CourierWaybillNumber
      end
  of lower('AmendAddress_tab_' & 1)
    do GenerateTab1
  of lower('AmendAddress_jobe:Sub_Sub_Account_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe:Sub_Sub_Account
      of event:timer
        do Value::jobe:Sub_Sub_Account
      else
        do Value::jobe:Sub_Sub_Account
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('AmendAddress_form:ready_',1)

  p_web.SetSessionValue('AmendAddress_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_AmendAddress',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('AmendAddress_form:ready_',1)
  p_web.SetSessionValue('AmendAddress_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_AmendAddress',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('AmendAddress_form:ready_',1)
  p_web.SetSessionValue('AmendAddress_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('AmendAddress:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('AmendAddress_form:ready_',1)
  p_web.SetSessionValue('AmendAddress_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('AmendAddress:Primed',0)
  p_web.setsessionvalue('showtab_AmendAddress',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  ! GetDescription resets the 'NewValue' to the code, if the description is found. Returns 1 if code valid.
  loc:ok = p_web.GetDescription(SUBURB,sur:SuburbKey,sur:SuburbKey,sur:Suburb,,p_web.GetSessionValue('job:Address_Line3')) !2
  if loc:ok then p_web.FileToSessionQueue(SUBURB).
  ! GetDescription resets the 'NewValue' to the code, if the description is found. Returns 1 if code valid.
  loc:ok = p_web.GetDescription(SUBURB,sur:SuburbKey,sur:SuburbKey,sur:Suburb,,p_web.GetSessionValue('job:Address_Line3_Collection')) !2
  if loc:ok then p_web.FileToSessionQueue(SUBURB).
  ! GetDescription resets the 'NewValue' to the code, if the description is found. Returns 1 if code valid.
  loc:ok = p_web.GetDescription(SUBURB,sur:SuburbKey,sur:SuburbKey,sur:Suburb,,p_web.GetSessionValue('job:Address_Line3_Delivery')) !2
  if loc:ok then p_web.FileToSessionQueue(SUBURB).
  ! GetDescription resets the 'NewValue' to the code, if the description is found. Returns 1 if code valid.
  loc:ok = p_web.GetDescription(SUBACCAD,sua:AccountNumberOnlyKey,sua:AccountNumberOnlyKey,sua:AccountNumber,,p_web.GetSessionValue('jobe:Sub_Sub_Account')) !2
  if loc:ok then p_web.FileToSessionQueue(SUBACCAD).
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
      If not (1=0)
          If p_web.IfExistsValue('job:Company_Name')
            job:Company_Name = p_web.GetValue('job:Company_Name')
          End
      End
      If not (p_web.GSV('Hide:CollectionAddress') = 1)
        If not (p_web.GSV('ReadOnly:CollectionAddress') = 1)
          If p_web.IfExistsValue('job:Company_Name_Collection')
            job:Company_Name_Collection = p_web.GetValue('job:Company_Name_Collection')
          End
        End
      End
      If not (p_web.GSV('Hide:DeliveryAddress') = 1)
        If not (p_web.GSV('ReadOnly:DeliveryAddress') = 1)
          If p_web.IfExistsValue('job:Company_Name_Delivery')
            job:Company_Name_Delivery = p_web.GetValue('job:Company_Name_Delivery')
          End
        End
      End
      If not (1=0)
          If p_web.IfExistsValue('job:Address_Line1')
            job:Address_Line1 = p_web.GetValue('job:Address_Line1')
          End
      End
      If not (p_web.GSV('Hide:CollectionAddress') = 1)
        If not (p_web.GSV('ReadOnly:CollectionAddress') = 1)
          If p_web.IfExistsValue('job:Address_Line1_Collection')
            job:Address_Line1_Collection = p_web.GetValue('job:Address_Line1_Collection')
          End
        End
      End
      If not (p_web.GSV('Hide:DeliveryAddress') = 1)
        If not (p_web.GSV('ReadOnly:DeliveryAddress') = 1)
          If p_web.IfExistsValue('job:Address_Line1_Delivery')
            job:Address_Line1_Delivery = p_web.GetValue('job:Address_Line1_Delivery')
          End
        End
      End
      If not (1=0)
          If p_web.IfExistsValue('job:Address_Line2')
            job:Address_Line2 = p_web.GetValue('job:Address_Line2')
          End
      End
      If not (p_web.GSV('Hide:CollectionAddress') = 1)
        If not (p_web.GSV('ReadOnly:CollectionAddress') = 1)
          If p_web.IfExistsValue('job:Address_Line2_Collection')
            job:Address_Line2_Collection = p_web.GetValue('job:Address_Line2_Collection')
          End
        End
      End
      If not (p_web.GSV('Hide:DeliveryAddress') = 1)
        If not (p_web.GSV('ReadOnly:DeliveryAddress') = 1)
          If p_web.IfExistsValue('job:Address_Line2_Delivery')
            job:Address_Line2_Delivery = p_web.GetValue('job:Address_Line2_Delivery')
          End
        End
      End
      If not (1=0)
          If p_web.IfExistsValue('job:Address_Line3')
            job:Address_Line3 = p_web.GetValue('job:Address_Line3')
          End
      End
      If not (p_web.GSV('Hide:CollectionAddress') = 1)
        If not (p_web.GSV('ReadOnly:CollectionAddress') = 1)
          If p_web.IfExistsValue('job:Address_Line3_Collection')
            job:Address_Line3_Collection = p_web.GetValue('job:Address_Line3_Collection')
          End
        End
      End
      If not (p_web.GSV('Hide:DeliveryAddress') = 1)
        If not (p_web.GSV('ReadOnly:DeliveryAddress') = 1)
          If p_web.IfExistsValue('job:Address_Line3_Delivery')
            job:Address_Line3_Delivery = p_web.GetValue('job:Address_Line3_Delivery')
          End
        End
      End
      If not (1=0)
          If p_web.IfExistsValue('job:Telephone_Number')
            job:Telephone_Number = p_web.GetValue('job:Telephone_Number')
          End
      End
      If not (p_web.GSV('Hide:CollectionAddress') = 1)
        If not (p_web.GSV('ReadOnly:CollectionAddress') = 1)
          If p_web.IfExistsValue('job:Telephone_Collection')
            job:Telephone_Collection = p_web.GetValue('job:Telephone_Collection')
          End
        End
      End
      If not (p_web.GSV('Hide:DeliveryAddress') = 1)
        If not (p_web.GSV('ReadOnly:DeliveryAddress') = 1)
          If p_web.IfExistsValue('job:Telephone_Delivery')
            job:Telephone_Delivery = p_web.GetValue('job:Telephone_Delivery')
          End
        End
      End
      If not (1=0)
          If p_web.IfExistsValue('job:Fax_Number')
            job:Fax_Number = p_web.GetValue('job:Fax_Number')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('jobe:EndUserEmailAddress')
            jobe:EndUserEmailAddress = p_web.GetValue('jobe:EndUserEmailAddress')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('jobe:EndUserTelNo')
            jobe:EndUserTelNo = p_web.GetValue('jobe:EndUserTelNo')
          End
      End
    If (p_web.GSV('Hide:VatNumber') <> 1)
      If not (1=0)
          If p_web.IfExistsValue('jobe:VatNumber')
            jobe:VatNumber = p_web.GetValue('jobe:VatNumber')
          End
      End
    End
      If not (1=0)
          If p_web.IfExistsValue('jobe2:IDNumber')
            jobe2:IDNumber = p_web.GetValue('jobe2:IDNumber')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('jobe2:SMSNotification') = 0
            p_web.SetValue('jobe2:SMSNotification',0)
            jobe2:SMSNotification = 0
          Else
            jobe2:SMSNotification = p_web.GetValue('jobe2:SMSNotification')
          End
      End
      If not (p_web.GSV('jobe2:SMSNotification') = 0)
          If p_web.IfExistsValue('jobe2:SMSAlertNumber')
            jobe2:SMSAlertNumber = p_web.GetValue('jobe2:SMSAlertNumber')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('jobe2:EmailNotification') = 0
            p_web.SetValue('jobe2:EmailNotification',0)
            jobe2:EmailNotification = 0
          Else
            jobe2:EmailNotification = p_web.GetValue('jobe2:EmailNotification')
          End
      End
      If not (p_web.GSV('jobe2:EmailNotification') = 0)
          If p_web.IfExistsValue('jobe2:EmailAlertAddress')
            jobe2:EmailAlertAddress = p_web.GetValue('jobe2:EmailAlertAddress')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('jobe2:CourierWaybillNumber')
            jobe2:CourierWaybillNumber = p_web.GetValue('jobe2:CourierWaybillNumber')
          End
      End
  If p_web.GSV('Hide:SubSubAccount') <> 1
      If not (1=0)
          If p_web.IfExistsValue('jobe:Sub_Sub_Account')
            jobe:Sub_Sub_Account = p_web.GetValue('jobe:Sub_Sub_Account')
          End
      End
  End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('AmendAddress_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('AmendAddress_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
    do ValidateValue::txtEndUserAddress
    If loc:Invalid then exit.
    do ValidateValue::txtCollectionAddress
    If loc:Invalid then exit.
    do ValidateValue::txtDeliveryAddress
    If loc:Invalid then exit.
    do ValidateValue::job:Company_Name
    If loc:Invalid then exit.
    do ValidateValue::job:Company_Name_Collection
    If loc:Invalid then exit.
    do ValidateValue::job:Company_Name_Delivery
    If loc:Invalid then exit.
    do ValidateValue::job:Address_Line1
    If loc:Invalid then exit.
    do ValidateValue::job:Address_Line1_Collection
    If loc:Invalid then exit.
    do ValidateValue::job:Address_Line1_Delivery
    If loc:Invalid then exit.
    do ValidateValue::job:Address_Line2
    If loc:Invalid then exit.
    do ValidateValue::job:Address_Line2_Collection
    If loc:Invalid then exit.
    do ValidateValue::job:Address_Line2_Delivery
    If loc:Invalid then exit.
    do ValidateValue::job:Address_Line3
    If loc:Invalid then exit.
    do ValidateValue::job:Address_Line3_Collection
    If loc:Invalid then exit.
    do ValidateValue::job:Address_Line3_Delivery
    If loc:Invalid then exit.
    do ValidateValue::jobe2:HubCustomer
    If loc:Invalid then exit.
    do ValidateValue::jobe2:HubCollection
    If loc:Invalid then exit.
    do ValidateValue::jobe2:HubDelivery
    If loc:Invalid then exit.
    do ValidateValue::job:Postcode
    If loc:Invalid then exit.
    do ValidateValue::job:Postcode_Collection
    If loc:Invalid then exit.
    do ValidateValue::job:Postcode_Delivery
    If loc:Invalid then exit.
    do ValidateValue::job:Telephone_Number
    If loc:Invalid then exit.
    do ValidateValue::job:Telephone_Collection
    If loc:Invalid then exit.
    do ValidateValue::job:Telephone_Delivery
    If loc:Invalid then exit.
    do ValidateValue::job:Fax_Number
    If loc:Invalid then exit.
    do ValidateValue::btnCopyCollectionAddress
    If loc:Invalid then exit.
    do ValidateValue::btnCopyDeliveryAddress
    If loc:Invalid then exit.
    do ValidateValue::jobe:EndUserEmailAddress
    If loc:Invalid then exit.
    do ValidateValue::jobe:EndUserTelNo
    If loc:Invalid then exit.
    do ValidateValue::jbn:Collection_Text
    If loc:Invalid then exit.
    do ValidateValue::jbn:Delivery_Text
    If loc:Invalid then exit.
    do ValidateValue::jobe:VatNumber
    If loc:Invalid then exit.
    do ValidateValue::jobe2:IDNumber
    If loc:Invalid then exit.
    do ValidateValue::jobe2:SMSNotification
    If loc:Invalid then exit.
    do ValidateValue::jobe2:SMSAlertNumber
    If loc:Invalid then exit.
    do ValidateValue::btnCollectionText
    If loc:Invalid then exit.
    do ValidateValue::btnDeliveryText
    If loc:Invalid then exit.
    do ValidateValue::jobe2:EmailNotification
    If loc:Invalid then exit.
    do ValidateValue::jobe2:EmailAlertAddress
    If loc:Invalid then exit.
    do ValidateValue::jobe2:CourierWaybillNumber
    If loc:Invalid then exit.
  ! tab = 4
  If p_web.GSV('Hide:SubSubAccount') <> 1
    loc:InvalidTab += 1
    do ValidateValue::jobe:Sub_Sub_Account
    If loc:Invalid then exit.
  End
  ! The following fields are not on the form, but need to be checked anyway.
  !Validation
  !p_web.SetSessionValue('job:Company_Name',p_web.GetSessionValue('tmp:CompanyName'))
  !p_web.SetSessionValue('job:Address_Line1',p_web.GetSessionValue('tmp:AddressLine1'))
  !p_web.SetSessionValue('job:Address_Line2',p_web.GetSessionValue('tmp:AddressLine2'))
  !p_web.SetSessionValue('job:Address_Line3',p_web.GetSessionValue('tmp:Suburb'))
  !p_web.SetSessionValue('jobe2:HubCustomer',p_web.GetSessionValue('tmp:HubCustomer'))
  !p_web.SetSessionValue('job:Telephone_Number',p_web.GetSessionValue('tmp:TelephoneNumber'))
  !p_web.SetSessionValue('job:Fax_Number',p_web.GetSessionValue('tmp:FaxNumber'))
  !p_web.SetSessionValue('jobe:EndUserEmailAddress',p_web.GetSessionValue('tmp:EmailAddress'))
  !p_web.SetSessionValue('jobe:EndUserTelNo',p_web.GetSessionValue('tmp:EndUserTelephoneNumber'))
  !p_web.SetSessionValue('jobe2:IDNumber',p_web.GetSessionValue('tmp:IDNumber'))
  !p_web.SetSessionValue('jobe2:SMSNotification',p_web.GetSessionValue('tmp:SMSNotification'))
  !p_web.SetSessionValue('jobe2:SMSAlertNumber',p_web.GetSessionValue('tmp:NotificationMobileNumber'))
  !p_web.SetSessionValue('jobe2:EmailNotification',p_web.GetSessionValue('tmp:EmailNotification'))
  !p_web.SetSessionValue('jobe2:EmailAlertAddress',p_web.GetSessionValue('tmp:NotificationEmailAddress'))
  !
  !p_web.SetSessionValue('job:Company_Name_Delivery',p_web.GetSessionValue('tmp:CompanyNameDelivery'))
  !p_web.SetSessionValue('job:Company_Name_Collection',p_web.GetSessionValue('tmp:CompanyNameCollection'))
  !p_web.SetSessionValue('job:Address_Line1_Delivery',p_web.GetSessionValue('tmp:AddressLine1Delivery'))
  !p_web.SetSessionValue('job:Address_Line1_Collection',p_web.GetSessionValue('tmp:AddressLine1Collection'))
  !p_web.SetSessionValue('job:Address_Line3_Delivery',p_web.GetSessionValue('tmp:SuburbDelivery'))
  !p_web.SetSessionValue('job:Address_Line3_Collection',p_web.GetSessionValue('tmp:SuburbCollection'))
  !p_web.SetSessionValue('jobe2:HubDelivery',p_web.GetSessionValue('tmp:HubDelivery'))
  !p_web.SetSessionValue('jobe2:HubCollection',p_web.GetSessionValue('tmp:HubCollection'))
  !p_web.SetSessionValue('job:Telephone_Delivery',p_web.GetSessionValue('tmp:TelephoneNumberDelivery'))
  !p_web.SetSessionValue('job:Telephone_Collection',p_web.GetSessionValue('tmp:TelephoneNumberCollection'))
  !
  !
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('AmendAddress:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('job:Company_Name')
  p_web.StoreValue('job:Company_Name_Collection')
  p_web.StoreValue('job:Company_Name_Delivery')
  p_web.StoreValue('job:Address_Line1')
  p_web.StoreValue('job:Address_Line1_Collection')
  p_web.StoreValue('job:Address_Line1_Delivery')
  p_web.StoreValue('job:Address_Line2')
  p_web.StoreValue('job:Address_Line2_Collection')
  p_web.StoreValue('job:Address_Line2_Delivery')
  p_web.StoreValue('job:Address_Line3')
  p_web.StoreValue('job:Address_Line3_Collection')
  p_web.StoreValue('job:Address_Line3_Delivery')
  p_web.StoreValue('jobe2:HubCustomer')
  p_web.StoreValue('jobe2:HubCollection')
  p_web.StoreValue('jobe2:HubDelivery')
  p_web.StoreValue('job:Postcode')
  p_web.StoreValue('job:Postcode_Collection')
  p_web.StoreValue('job:Postcode_Delivery')
  p_web.StoreValue('job:Telephone_Number')
  p_web.StoreValue('job:Telephone_Collection')
  p_web.StoreValue('job:Telephone_Delivery')
  p_web.StoreValue('job:Fax_Number')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('jbn:Collection_Text')
  p_web.StoreValue('jbn:Delivery_Text')
  p_web.StoreValue('jobe2:IDNumber')
  p_web.StoreValue('jobe2:SMSNotification')
  p_web.StoreValue('jobe2:SMSAlertNumber')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('jobe2:EmailNotification')
  p_web.StoreValue('jobe2:EmailAlertAddress')
  p_web.StoreValue('jobe2:CourierWaybillNumber')

