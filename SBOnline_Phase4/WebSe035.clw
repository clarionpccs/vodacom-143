

   MEMBER('WebServer_Phase4.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSE035.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSE008.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE009.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE034.INC'),ONCE        !Req'd for module callout resolution
                     END


FormAddToBatch       PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locCompanyDetails    STRING(60)                            !
locHeadAccountNumber STRING(30)                            !
locExistingBatchNumber LONG                                !
locBatchStatusText   STRING(100)                           !
locBatchAccountNumber STRING(30)                           !
locBatchCourier      STRING(30)                            !
locNumberInBatch     LONG                                  !
locBatchAccountName  STRING(30)                            !
locWaybillNumber     STRING(30)                            !
FilesOpened     Long
MULDESPJ::State  USHORT
MULDESP::State  USHORT
TRADEACC::State  USHORT
SUBTRACC::State  USHORT
JOBS::State  USHORT
locCompanyDetails:IsInvalid  Long
job:Ref_Number:IsInvalid  Long
job:Charge_Type:IsInvalid  Long
job:Warranty_Charge_Type:IsInvalid  Long
locBatchStatusText:IsInvalid  Long
locBatchAccountNumber:IsInvalid  Long
locBatchAccountName:IsInvalid  Long
locBatchCourier:IsInvalid  Long
locNumberInBatch:IsInvalid  Long
buttonAddToExistingBatch:IsInvalid  Long
textCreateNewBatchMessage:IsInvalid  Long
textEnterConsingmentNumber:IsInvalid  Long
locWaybillNumber:IsInvalid  Long
buttonCreateNewBatch:IsInvalid  Long
buttonForceSingleDespatch:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormAddToBatch')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'FormAddToBatch_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('FormAddToBatch','')
    p_web.DivHeader('FormAddToBatch',p_web.combine(p_web.site.style.formdiv,'fdiv'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('FormAddToBatch') = 0
        p_web.AddPreCall('FormAddToBatch')
        p_web.DivHeader('popup_FormAddToBatch','nt-hidden')
        p_web.DivHeader('FormAddToBatch',p_web.combine(p_web.site.style.formdiv,'fdiv'))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_FormAddToBatch_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_FormAddToBatch_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormAddToBatch',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_FormAddToBatch',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormAddToBatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_FormAddToBatch',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormAddToBatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormAddToBatch',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_FormAddToBatch',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormAddToBatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_FormAddToBatch',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormAddToBatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormAddToBatch',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormAddToBatch',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('FormAddToBatch')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
DeleteSessionValues ROUTINE
    p_web.DeleteSessionValue('locCompanyDetails')
    p_web.DeleteSessionValue('locHeadAccountNumber')
    p_web.DeleteSessionValue('locExistingBatchNumber')
    p_web.DeleteSessionValue('locBatchStatusText')
    p_web.DeleteSessionValue('locBatchAccountNumber')
    p_web.DeleteSessionValue('locBatchCourier')
    p_web.DeleteSessionValue('locNumberInBatch')
    p_web.DeleteSessionValue('locBatchAccountName')
    p_web.DeleteSessionValue('Hide:CreateNewBatchButton')
    p_web.DeleteSessionvalue('Hide:ConsignmentNumber')
    p_web.DeleteSessionValue('locWaybillNumber')

OpenFiles  ROUTINE
  p_web._OpenFile(MULDESPJ)
  p_web._OpenFile(MULDESP)
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(SUBTRACC)
  p_web._OpenFile(JOBS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(MULDESPJ)
  p_Web._CloseFile(MULDESP)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(JOBS)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  ! Start
  p_web.SSV('Hide:AddToBatch',0)
  p_web.SSV('Hide:CreateNewBatchButton',0)
  p_web.SSV('Hide:ConsignmentNumber',1)
  p_web.SSV('locWaybillNumber','')
  
  Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
  sub:Account_Number = p_web.GSV('job:Account_Number')
  IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
      p_web.SSV('locCompanyDetails',p_web.GSV('job:Account_Number') & ' - ' & Clip(sub:Company_Name))
  END
  
  locHeadAccountNumber = p_web.GSV('job:Account_Number')
  
  IF (sub:Generic_Account)
      IF (p_web.GSV('BookingSite') = 'RRC' OR |
          (p_web.GSV('BookingSite') = 'ARC' AND p_web.GSV('jobe:WebJob') <> 1))
      ELSE
          locHeadAccountNumber = p_web.GSV('wob:HeadAccountNumber')
      END
  
  ELSE
      IF (p_web.GSV('BookingSite') = 'ARC' AND p_web.GSV('jobe:WebJob') = 1)
          locHeadAccountNumber = p_web.GSV('wob:HeadAccountNumber')
      END
  END
  
  ! Is there an existing batch?
  VIRTUAL# = 0
  locExistingBatchNumber = 0
  IF (p_web.GSV('BookingSite') = 'ARC')
      Access:MULDESP.ClearKey(muld:HeadAccountKey)
      muld:HeadAccountNumber = p_web.GSV('BookingAccount')
      muld:AccountNumber = locHeadAccountNumber
      If GETINI('DESPATCH','GroupVirtualBatches',,CLIP(PATH())&'\SB2KDEF.INI') = 1
          IF (NOT sub:Generic_Account)
              IF (vod.RemoteAccount(p_web.GSV('job:Account_Number')))
                  muld:AccountNumber = tra:Account_Number
                  VIRTUAL# = 1
              END
          END
  
      END
  
  ELSE
      IF (p_web.GSV('jobe:Sub_Sub_Account') = '')
          Access:MULDESP.ClearKey(muld:AccountNumberKey)
          muld:HeadAccountNumber = p_web.GSV('BookingAccount')
          muld:AccountNumber = locHeadAccountNumber
      ELSE
          locHeadAccountNumber = p_web.GSV('jobe:Sub_Sub_Account')
          Access:MULDESP.ClearKey(muld:AccountNumberKey)
          muld:HeadAccountNumber = p_web.GSV('BookingAccount')
          muld:AccountNumber = locHeadAccountNumber
      END
  
  END
  SET(muld:HeadAccountKey,muld:HeadAccountKey)
  LOOP UNTIL Access:MULDESP.Next()
      IF (muld:HeadAccountNumber <> p_web.GSV('BookingAccount'))
          BREAK
      END
      IF (VIRTUAL# = 1)
          IF (muld:AccountNumber <> tra:Account_Number)
              BREAK
          END
          IF (muld:Courier = tra:Courier_Outgoing)
              locExistingBatchNumber = muld:RecordNumber
          END
      ELSE
          IF (muld:AccountNumber <> locHeadAccountNumber)
              BREAK
          END
          IF (p_web.GSV('BookingSite') = 'ARC')
              CASE p_web.GSV('job:Despatch_Type')
              OF 'JOB'
                  IF (p_web.GSV('job:Courier') = muld:Courier)
                      locExistingBatchNumber = muld:RecordNumber
                      BREAK
                  END
  
              OF 'EXC'
                  IF (p_web.GSV('job:Exchange_Courier') = muld:Courier)
                      locExistingBatchNumber = muld:RecordNumber
                      BREAK
                  END
              OF 'LOA'
                  IF (p_web.GSV('job:Loan_Courier') = muld:Courier)
                      locExistingBatchNumber = muld:RecordNumber
                      BREAK
                  END
              END
  
          ELSE
              CASE p_web.GSV('jobe:DespatchType')
              OF 'JOB'
                  IF (p_web.GSV('job:Courier') = muld:Courier)
                      locExistingBatchNumber = muld:RecordNumber
                      BREAK
                  END
  
              OF 'EXC'
                  IF (p_web.GSV('job:Exchange_Courier') = muld:Courier)
                      locExistingBatchNumber = muld:RecordNumber
                      BREAK
                  END
              OF 'LOA'
                  IF (p_web.GSV('job:Loan_Courier') = muld:Courier)
                      locExistingBatchNumber = muld:RecordNumber
                      BREAK
                  END
              END
          END
  
      END
  END
  
  IF (locExistingBatchNumber = 0)
      p_web.SSV('locBatchStatusText','No Batch Found')
      p_web.SSV('Hide:AddToBatch',1)
  ELSE
      ! Found an existing batch, fill in the details
      Access:MULDESP.ClearKey(muld:RecordNumberKey)
      muld:RecordNumber = locExistingBatchNumber
      IF (Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign)
          p_web.SSV('locBatchAccountNumber',muld:AccountNumber)
          p_web.SSV('locBatchCourier',muld:Courier)
  
          locNumberInBatch = 0
          Access:MULDESPJ.ClearKey(mulj:JobNumberKey)
          mulj:RefNumber = muld:RecordNumber
          SET(mulj:JobNumberKey,mulj:JobNumberKey)
          LOOP UNTIL Access:MULDESPJ.Next()
              IF (mulj:RefNumber <> muld:RecordNumber)
                  BREAK
              END
              locNumberInBatch += 1
          END
          p_web.SSV('locNumberInBatch',locNumberInBatch)
  
          ! Get The Account Name
          IF (muld:BatchType = 'TRA')
              Access:TRADEACC.ClearKey(tra:Account_Number_Key)
              tra:Account_Number = muld:AccountNumber
              IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
                  p_web.SSV('locBatchAccountName',tra:Company_Name)
              END
          ELSE
              Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
              sub:Account_Number = muld:AccountNumber
              IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
                  p_web.SSV('locBatchAccountName',sub:Company_Name)
              END
  
          END
  
      END
  
      p_web.SSV('locBatchStatusText','Existing Batch Found')
      p_web.SSV('Hide:AddToBatch',0)
  END
  p_web.SSV('locExistingBatchNumber',locExistingBatchNumber)
  
  ! Show Consignment Number?
  IF (p_web.GSV('cou:PrintWaybill') = 1)
  ELSE
      IF (p_web.GSV('cou:AutoConsignmentNo') = 1)
  
      ELSE
          p_web.SSV('Hide:CreateNewBatchButton',1)
          p_web.SSV('Hide:ConsignmentNumber',0)
      END
  END
  
  p_web.SetValue('FormAddToBatch_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'FormAddToBatch'
    end
    p_web.formsettings.proc = 'FormAddToBatch'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine
  DO DeleteSessionValues

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('job:Ref_Number')
    p_web.SetPicture('job:Ref_Number','@s8')
  End
  p_web.SetSessionPicture('job:Ref_Number','@s8')
  If p_web.IfExistsValue('job:Charge_Type')
    p_web.SetPicture('job:Charge_Type','@s30')
  End
  p_web.SetSessionPicture('job:Charge_Type','@s30')
  If p_web.IfExistsValue('job:Warranty_Charge_Type')
    p_web.SetPicture('job:Warranty_Charge_Type','@s30')
  End
  p_web.SetSessionPicture('job:Warranty_Charge_Type','@s30')

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  If p_web.GSV('locExistingBatchNumber') > 0
    loc:TabNumber += 1
  End
  If p_web.GSV('locExistingBatchNumber') = 0
    loc:TabNumber += 1
  End
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  if p_web.IfExistsValue('locCompanyDetails') = 0
    p_web.SetSessionValue('locCompanyDetails',locCompanyDetails)
  Else
    locCompanyDetails = p_web.GetSessionValue('locCompanyDetails')
  End
  if p_web.IfExistsValue('job:Ref_Number') = 0
    p_web.SetSessionValue('job:Ref_Number',job:Ref_Number)
  Else
    job:Ref_Number = p_web.GetSessionValue('job:Ref_Number')
  End
  if p_web.IfExistsValue('job:Charge_Type') = 0
    p_web.SetSessionValue('job:Charge_Type',job:Charge_Type)
  Else
    job:Charge_Type = p_web.GetSessionValue('job:Charge_Type')
  End
  if p_web.IfExistsValue('job:Warranty_Charge_Type') = 0
    p_web.SetSessionValue('job:Warranty_Charge_Type',job:Warranty_Charge_Type)
  Else
    job:Warranty_Charge_Type = p_web.GetSessionValue('job:Warranty_Charge_Type')
  End
  if p_web.IfExistsValue('locBatchStatusText') = 0
    p_web.SetSessionValue('locBatchStatusText',locBatchStatusText)
  Else
    locBatchStatusText = p_web.GetSessionValue('locBatchStatusText')
  End
  if p_web.IfExistsValue('locBatchAccountNumber') = 0
    p_web.SetSessionValue('locBatchAccountNumber',locBatchAccountNumber)
  Else
    locBatchAccountNumber = p_web.GetSessionValue('locBatchAccountNumber')
  End
  if p_web.IfExistsValue('locBatchAccountName') = 0
    p_web.SetSessionValue('locBatchAccountName',locBatchAccountName)
  Else
    locBatchAccountName = p_web.GetSessionValue('locBatchAccountName')
  End
  if p_web.IfExistsValue('locBatchCourier') = 0
    p_web.SetSessionValue('locBatchCourier',locBatchCourier)
  Else
    locBatchCourier = p_web.GetSessionValue('locBatchCourier')
  End
  if p_web.IfExistsValue('locNumberInBatch') = 0
    p_web.SetSessionValue('locNumberInBatch',locNumberInBatch)
  Else
    locNumberInBatch = p_web.GetSessionValue('locNumberInBatch')
  End
  if p_web.IfExistsValue('locWaybillNumber') = 0
    p_web.SetSessionValue('locWaybillNumber',locWaybillNumber)
  Else
    locWaybillNumber = p_web.GetSessionValue('locWaybillNumber')
  End

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locCompanyDetails')
    locCompanyDetails = p_web.GetValue('locCompanyDetails')
    p_web.SetSessionValue('locCompanyDetails',locCompanyDetails)
  Else
    locCompanyDetails = p_web.GetSessionValue('locCompanyDetails')
  End
  if p_web.IfExistsValue('job:Ref_Number')
    job:Ref_Number = p_web.GetValue('job:Ref_Number')
    p_web.SetSessionValue('job:Ref_Number',job:Ref_Number)
  Else
    job:Ref_Number = p_web.GetSessionValue('job:Ref_Number')
  End
  if p_web.IfExistsValue('job:Charge_Type')
    job:Charge_Type = p_web.GetValue('job:Charge_Type')
    p_web.SetSessionValue('job:Charge_Type',job:Charge_Type)
  Else
    job:Charge_Type = p_web.GetSessionValue('job:Charge_Type')
  End
  if p_web.IfExistsValue('job:Warranty_Charge_Type')
    job:Warranty_Charge_Type = p_web.GetValue('job:Warranty_Charge_Type')
    p_web.SetSessionValue('job:Warranty_Charge_Type',job:Warranty_Charge_Type)
  Else
    job:Warranty_Charge_Type = p_web.GetSessionValue('job:Warranty_Charge_Type')
  End
  if p_web.IfExistsValue('locBatchStatusText')
    locBatchStatusText = p_web.GetValue('locBatchStatusText')
    p_web.SetSessionValue('locBatchStatusText',locBatchStatusText)
  Else
    locBatchStatusText = p_web.GetSessionValue('locBatchStatusText')
  End
  if p_web.IfExistsValue('locBatchAccountNumber')
    locBatchAccountNumber = p_web.GetValue('locBatchAccountNumber')
    p_web.SetSessionValue('locBatchAccountNumber',locBatchAccountNumber)
  Else
    locBatchAccountNumber = p_web.GetSessionValue('locBatchAccountNumber')
  End
  if p_web.IfExistsValue('locBatchAccountName')
    locBatchAccountName = p_web.GetValue('locBatchAccountName')
    p_web.SetSessionValue('locBatchAccountName',locBatchAccountName)
  Else
    locBatchAccountName = p_web.GetSessionValue('locBatchAccountName')
  End
  if p_web.IfExistsValue('locBatchCourier')
    locBatchCourier = p_web.GetValue('locBatchCourier')
    p_web.SetSessionValue('locBatchCourier',locBatchCourier)
  Else
    locBatchCourier = p_web.GetSessionValue('locBatchCourier')
  End
  if p_web.IfExistsValue('locNumberInBatch')
    locNumberInBatch = p_web.GetValue('locNumberInBatch')
    p_web.SetSessionValue('locNumberInBatch',locNumberInBatch)
  Else
    locNumberInBatch = p_web.GetSessionValue('locNumberInBatch')
  End
  if p_web.IfExistsValue('locWaybillNumber')
    locWaybillNumber = p_web.GetValue('locWaybillNumber')
    p_web.SetSessionValue('locWaybillNumber',locWaybillNumber)
  Else
    locWaybillNumber = p_web.GetSessionValue('locWaybillNumber')
  End

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('FormAddToBatch_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'MultipleBatchDespatch'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormAddToBatch_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormAddToBatch_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormAddToBatch_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'MultipleBatchDespatch'

GenerateForm   Routine
  do LoadRelatedRecords
 locCompanyDetails = p_web.RestoreValue('locCompanyDetails')
 locBatchStatusText = p_web.RestoreValue('locBatchStatusText')
 locBatchAccountNumber = p_web.RestoreValue('locBatchAccountNumber')
 locBatchAccountName = p_web.RestoreValue('locBatchAccountName')
 locBatchCourier = p_web.RestoreValue('locBatchCourier')
 locNumberInBatch = p_web.RestoreValue('locNumberInBatch')
 locWaybillNumber = p_web.RestoreValue('locWaybillNumber')
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Add To Batch') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Add To Batch',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_FormAddToBatch',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_FormAddToBatch0_div')&'">'&p_web.Translate('Current Job Details')&'</a></li>'& CRLF
      If p_web.GSV('locExistingBatchNumber') > 0
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_FormAddToBatch1_div')&'">'&p_web.Translate('Existing Batch Details')&'</a></li>'& CRLF
      End
      If p_web.GSV('locExistingBatchNumber') = 0
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_FormAddToBatch2_div')&'">'&p_web.Translate('Create New Batch')&'</a></li>'& CRLF
      End
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_FormAddToBatch3_div')&'">'&p_web.Translate('Force Single Despatch')&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  do GenerateTab3
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="FormAddToBatch_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.cancel();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CancelButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="FormAddToBatch_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'FormAddToBatch_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="FormAddToBatch_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'FormAddToBatch_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_FormAddToBatch')>0,p_web.GSV('showtab_FormAddToBatch'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_FormAddToBatch'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_FormAddToBatch') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_FormAddToBatch'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_FormAddToBatch')>0,p_web.GSV('showtab_FormAddToBatch'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_FormAddToBatch') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Current Job Details') & ''''
      If p_web.GSV('locExistingBatchNumber') > 0
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Existing Batch Details') & ''''
      End
      If p_web.GSV('locExistingBatchNumber') = 0
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Create New Batch') & ''''
      End
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Force Single Despatch') & ''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_FormAddToBatch_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_FormAddToBatch')>0,p_web.GSV('showtab_FormAddToBatch'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"FormAddToBatch",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_FormAddToBatch')>0,p_web.GSV('showtab_FormAddToBatch'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_FormAddToBatch_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('FormAddToBatch') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('FormAddToBatch')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Current Job Details')&'</a></h3>' & CRLF & p_web.DivHeader('tab_FormAddToBatch0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_FormAddToBatch0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_FormAddToBatch0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_FormAddToBatch0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Current Job Details')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_FormAddToBatch0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Current Job Details')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_FormAddToBatch0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Current Job Details')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_FormAddToBatch0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&150&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locCompanyDetails
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&150&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Ref_Number
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Ref_Number
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      If p_web.GSV('job:Chargeable_Job') = 'YES'
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&150&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Charge_Type
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Charge_Type
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
      If p_web.GSV('job:warranty_job') = 'YES'
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&150&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Warranty_Charge_Type
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Warranty_Charge_Type
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab1  Routine
  If p_web.GSV('locExistingBatchNumber') > 0
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Existing Batch Details')&'</a></h3>' & CRLF & p_web.DivHeader('tab_FormAddToBatch1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_FormAddToBatch1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_FormAddToBatch1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_FormAddToBatch1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Existing Batch Details')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_FormAddToBatch1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Existing Batch Details')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_FormAddToBatch1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Existing Batch Details')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_FormAddToBatch1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&150&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locBatchStatusText
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      If p_web.GSV('locBatchAccountNumber') <> ''
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&150&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locBatchAccountNumber
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locBatchAccountNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
      If p_web.GSV('locBatchAccountName') <> ''
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&150&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locBatchAccountName
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locBatchAccountName
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
      If p_web.GSV('locBatchCourier') <> ''
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&150&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locBatchCourier
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locBatchCourier
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
      If p_web.GSV('locNumberInBatch') <> ''
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&150&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locNumberInBatch
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locNumberInBatch
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&150&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::buttonAddToExistingBatch
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&150&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::textCreateNewBatchMessage
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
  end
GenerateTab2  Routine
  If p_web.GSV('locExistingBatchNumber') = 0
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Create New Batch')&'</a></h3>' & CRLF & p_web.DivHeader('tab_FormAddToBatch2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_FormAddToBatch2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_FormAddToBatch2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_FormAddToBatch2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Create New Batch')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_FormAddToBatch2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Create New Batch')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_FormAddToBatch2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Create New Batch')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_FormAddToBatch2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
      If p_web.GSV('Hide:ConsignmentNumber') <> 1
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&150&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::textEnterConsingmentNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
      If p_web.GSV('Hide:ConsignmentNumber') <> 1
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&150&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locWaybillNumber
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locWaybillNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&150&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::buttonCreateNewBatch
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
  end
GenerateTab3  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Force Single Despatch')&'</a></h3>' & CRLF & p_web.DivHeader('tab_FormAddToBatch3',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_FormAddToBatch3',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_FormAddToBatch3',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_FormAddToBatch3',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Force Single Despatch')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_FormAddToBatch3',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Force Single Despatch')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_FormAddToBatch3',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Force Single Despatch')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_FormAddToBatch3',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&150&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::buttonForceSingleDespatch
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Validate::locCompanyDetails  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locCompanyDetails = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locCompanyDetails = p_web.GetValue('Value')
  End
  do ValidateValue::locCompanyDetails  ! copies value to session value if valid.

ValidateValue::locCompanyDetails  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locCompanyDetails',locCompanyDetails).
    End

Value::locCompanyDetails  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormAddToBatch_' & p_web._nocolon('locCompanyDetails') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- locCompanyDetails
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('blue bold large')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locCompanyDetails'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::job:Ref_Number  Routine
  packet = clip(packet) & p_web.DivHeader('FormAddToBatch_' & p_web._nocolon('job:Ref_Number') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Job Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Ref_Number  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Ref_Number = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s8
    job:Ref_Number = p_web.Dformat(p_web.GetValue('Value'),'@s8')
  End
  do ValidateValue::job:Ref_Number  ! copies value to session value if valid.

ValidateValue::job:Ref_Number  Routine
    If not (1=0)
  ! Automatic Dictionary Validation
  If p_web.GetSessionValue('FormAddToBatch:Primed') = 1 ! Autonumbered fields are not validated unless the record has been pre-primed
    If job:Ref_Number = 0
      loc:Invalid = 'job:Ref_Number'
      loc:Alert = p_web.translate('Job Number') & ' ' & p_web.site.NotZeroText
      job:Ref_Number:IsInvalid = true
    End
  End
      if loc:invalid = '' then p_web.SetSessionValue('job:Ref_Number',job:Ref_Number).
    End

Value::job:Ref_Number  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormAddToBatch_' & p_web._nocolon('job:Ref_Number') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- job:Ref_Number
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Ref_Number'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::job:Charge_Type  Routine
  packet = clip(packet) & p_web.DivHeader('FormAddToBatch_' & p_web._nocolon('job:Charge_Type') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Chargeable Charge Type'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Charge_Type  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Charge_Type = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    job:Charge_Type = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::job:Charge_Type  ! copies value to session value if valid.

ValidateValue::job:Charge_Type  Routine
  If p_web.GSV('job:Chargeable_Job') = 'YES'
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('job:Charge_Type',job:Charge_Type).
    End
  End

Value::job:Charge_Type  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormAddToBatch_' & p_web._nocolon('job:Charge_Type') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- job:Charge_Type
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Charge_Type'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::job:Warranty_Charge_Type  Routine
  packet = clip(packet) & p_web.DivHeader('FormAddToBatch_' & p_web._nocolon('job:Warranty_Charge_Type') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Warranty Charge Type'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Warranty_Charge_Type  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Warranty_Charge_Type = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    job:Warranty_Charge_Type = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::job:Warranty_Charge_Type  ! copies value to session value if valid.

ValidateValue::job:Warranty_Charge_Type  Routine
  If p_web.GSV('job:warranty_job') = 'YES'
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('job:Warranty_Charge_Type',job:Warranty_Charge_Type).
    End
  End

Value::job:Warranty_Charge_Type  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormAddToBatch_' & p_web._nocolon('job:Warranty_Charge_Type') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- job:Warranty_Charge_Type
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Warranty_Charge_Type'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Validate::locBatchStatusText  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locBatchStatusText = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locBatchStatusText = p_web.GetValue('Value')
  End
  do ValidateValue::locBatchStatusText  ! copies value to session value if valid.

ValidateValue::locBatchStatusText  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locBatchStatusText',locBatchStatusText).
    End

Value::locBatchStatusText  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormAddToBatch_' & p_web._nocolon('locBatchStatusText') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- locBatchStatusText
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('blue bold large')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locBatchStatusText'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::locBatchAccountNumber  Routine
  packet = clip(packet) & p_web.DivHeader('FormAddToBatch_' & p_web._nocolon('locBatchAccountNumber') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Account Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locBatchAccountNumber  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locBatchAccountNumber = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locBatchAccountNumber = p_web.GetValue('Value')
  End
  do ValidateValue::locBatchAccountNumber  ! copies value to session value if valid.

ValidateValue::locBatchAccountNumber  Routine
  If p_web.GSV('locBatchAccountNumber') <> ''
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locBatchAccountNumber',locBatchAccountNumber).
    End
  End

Value::locBatchAccountNumber  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormAddToBatch_' & p_web._nocolon('locBatchAccountNumber') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- locBatchAccountNumber
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locBatchAccountNumber'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::locBatchAccountName  Routine
  packet = clip(packet) & p_web.DivHeader('FormAddToBatch_' & p_web._nocolon('locBatchAccountName') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Account Name'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locBatchAccountName  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locBatchAccountName = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locBatchAccountName = p_web.GetValue('Value')
  End
  do ValidateValue::locBatchAccountName  ! copies value to session value if valid.

ValidateValue::locBatchAccountName  Routine
  If p_web.GSV('locBatchAccountName') <> ''
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locBatchAccountName',locBatchAccountName).
    End
  End

Value::locBatchAccountName  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormAddToBatch_' & p_web._nocolon('locBatchAccountName') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- locBatchAccountName
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locBatchAccountName'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::locBatchCourier  Routine
  packet = clip(packet) & p_web.DivHeader('FormAddToBatch_' & p_web._nocolon('locBatchCourier') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Courier'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locBatchCourier  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locBatchCourier = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locBatchCourier = p_web.GetValue('Value')
  End
  do ValidateValue::locBatchCourier  ! copies value to session value if valid.

ValidateValue::locBatchCourier  Routine
  If p_web.GSV('locBatchCourier') <> ''
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locBatchCourier',locBatchCourier).
    End
  End

Value::locBatchCourier  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormAddToBatch_' & p_web._nocolon('locBatchCourier') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- locBatchCourier
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locBatchCourier'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::locNumberInBatch  Routine
  packet = clip(packet) & p_web.DivHeader('FormAddToBatch_' & p_web._nocolon('locNumberInBatch') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Units In Batch'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locNumberInBatch  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locNumberInBatch = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locNumberInBatch = p_web.GetValue('Value')
  End
  do ValidateValue::locNumberInBatch  ! copies value to session value if valid.

ValidateValue::locNumberInBatch  Routine
  If p_web.GSV('locNumberInBatch') <> ''
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locNumberInBatch',locNumberInBatch).
    End
  End

Value::locNumberInBatch  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormAddToBatch_' & p_web._nocolon('locNumberInBatch') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- locNumberInBatch
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locNumberInBatch'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Validate::buttonAddToExistingBatch  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonAddToExistingBatch  ! copies value to session value if valid.

ValidateValue::buttonAddToExistingBatch  Routine
    If not (1=0)
    End

Value::buttonAddToExistingBatch  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormAddToBatch_' & p_web._nocolon('buttonAddToExistingBatch') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','AddToBatch','Add To Batch',p_web.combine(Choose('Add To Batch' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'button-entryfield'),loc:formname,,,p_web.WindowOpen(clip('MultipleBatchDespatch?Action=AddToBatch&BatchNumber=' & p_web.GSV('locExistingBatchNumber'))&''&'','_self'),,loc:disabled,'images/packinsert.png',,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()

Validate::textCreateNewBatchMessage  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::textCreateNewBatchMessage  ! copies value to session value if valid.

ValidateValue::textCreateNewBatchMessage  Routine
    If not (1=0)
    End

Value::textCreateNewBatchMessage  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormAddToBatch_' & p_web._nocolon('textCreateNewBatchMessage') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="textCreateNewBatchMessage" class="'&clip('blue bold')&'"'&clip(loc:extra)&'>' & p_web.Translate(p_web.br & '(Note: If you wish to create a New Batch, you must despatch/delete the current batch first.)',1) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Validate::textEnterConsingmentNumber  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::textEnterConsingmentNumber  ! copies value to session value if valid.

ValidateValue::textEnterConsingmentNumber  Routine
  If p_web.GSV('Hide:ConsignmentNumber') <> 1
    If not (1=0)
    End
  End

Value::textEnterConsingmentNumber  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormAddToBatch_' & p_web._nocolon('textEnterConsingmentNumber') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="textEnterConsingmentNumber" class="'&clip('red bold')&'"'&clip(loc:extra)&'>' & p_web.Translate('Enter A Consignment Number, then press [TAB]',) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::locWaybillNumber  Routine
  packet = clip(packet) & p_web.DivHeader('FormAddToBatch_' & p_web._nocolon('locWaybillNumber') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Consignment Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locWaybillNumber  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locWaybillNumber = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locWaybillNumber = p_web.GetValue('Value')
  End
  do ValidateValue::locWaybillNumber  ! copies value to session value if valid.
  IF (p_web.GSV('locWaybillNumber') <> '')
      p_web.SSV('Hide:CreateNewBatchButton',0)
  ELSE
      p_web.SSV('Hide:CreateNewBatchButton',1)
  END
  do Value::locWaybillNumber
  do SendAlert
  do Value::buttonCreateNewBatch  !1

ValidateValue::locWaybillNumber  Routine
  If p_web.GSV('Hide:ConsignmentNumber') <> 1
    If not (1=0)
  If locWaybillNumber = ''
    loc:Invalid = 'locWaybillNumber'
    locWaybillNumber:IsInvalid = true
    loc:alert = p_web.translate('Consignment Number') & ' ' & p_web.site.RequiredText
  End
    locWaybillNumber = Upper(locWaybillNumber)
      if loc:invalid = '' then p_web.SetSessionValue('locWaybillNumber',locWaybillNumber).
    End
  End

Value::locWaybillNumber  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormAddToBatch_' & p_web._nocolon('locWaybillNumber') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,'formrqd')
  If loc:retrying
    locWaybillNumber = p_web.RestoreValue('locWaybillNumber')
    do ValidateValue::locWaybillNumber
    If locWaybillNumber:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- locWaybillNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locWaybillNumber'',''formaddtobatch_locwaybillnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locWaybillNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locWaybillNumber',p_web.GetSessionValueFormat('locWaybillNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Validate::buttonCreateNewBatch  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonCreateNewBatch  ! copies value to session value if valid.

ValidateValue::buttonCreateNewBatch  Routine
    If not (p_web.GSV('Hide:CreateNewBatchButton') = 1)
    End

Value::buttonCreateNewBatch  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:CreateNewBatchButton') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormAddToBatch_' & p_web._nocolon('buttonCreateNewBatch') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (p_web.GSV('Hide:CreateNewBatchButton') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','CreateNewBatch','Create New Batch',p_web.combine(Choose('Create New Batch' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'button-entryfield'),loc:formname,,,p_web.WindowOpen(clip('CreateNewBatch')&''&'','_self'),,loc:disabled,'images/new.png',,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()

Validate::buttonForceSingleDespatch  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonForceSingleDespatch  ! copies value to session value if valid.

ValidateValue::buttonForceSingleDespatch  Routine
    If not (1=0)
    End

Value::buttonForceSingleDespatch  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormAddToBatch_' & p_web._nocolon('buttonForceSingleDespatch') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','ForceSingleDespatch','Individual Despatch',p_web.combine(Choose('Individual Despatch' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'button-entryfield'),loc:formname,,,p_web.WindowOpen(clip('IndividualDespatch?PassedJobNumber=' & p_web.GSV('locJobNumber') & '&PassedIMEINumber=' & p_web.GSV('locIMEINumber'))&''&'','_self'),,loc:disabled,'images/package.png',,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('FormAddToBatch_nexttab_' & 0)
    locCompanyDetails = p_web.GSV('locCompanyDetails')
    do ValidateValue::locCompanyDetails
    If loc:Invalid
      loc:retrying = 1
      do Value::locCompanyDetails
      !do SendAlert
      !exit
    End
    job:Ref_Number = p_web.GSV('job:Ref_Number')
    do ValidateValue::job:Ref_Number
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Ref_Number
      !do SendAlert
      !exit
    End
    job:Charge_Type = p_web.GSV('job:Charge_Type')
    do ValidateValue::job:Charge_Type
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Charge_Type
      !do SendAlert
      !exit
    End
    job:Warranty_Charge_Type = p_web.GSV('job:Warranty_Charge_Type')
    do ValidateValue::job:Warranty_Charge_Type
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Warranty_Charge_Type
      !do SendAlert
      !exit
    End
    If loc:Invalid then exit.
  of lower('FormAddToBatch_nexttab_' & 1)
    locBatchStatusText = p_web.GSV('locBatchStatusText')
    do ValidateValue::locBatchStatusText
    If loc:Invalid
      loc:retrying = 1
      do Value::locBatchStatusText
      !do SendAlert
      !exit
    End
    locBatchAccountNumber = p_web.GSV('locBatchAccountNumber')
    do ValidateValue::locBatchAccountNumber
    If loc:Invalid
      loc:retrying = 1
      do Value::locBatchAccountNumber
      !do SendAlert
      !exit
    End
    locBatchAccountName = p_web.GSV('locBatchAccountName')
    do ValidateValue::locBatchAccountName
    If loc:Invalid
      loc:retrying = 1
      do Value::locBatchAccountName
      !do SendAlert
      !exit
    End
    locBatchCourier = p_web.GSV('locBatchCourier')
    do ValidateValue::locBatchCourier
    If loc:Invalid
      loc:retrying = 1
      do Value::locBatchCourier
      !do SendAlert
      !exit
    End
    locNumberInBatch = p_web.GSV('locNumberInBatch')
    do ValidateValue::locNumberInBatch
    If loc:Invalid
      loc:retrying = 1
      do Value::locNumberInBatch
      !do SendAlert
      !exit
    End
    If loc:Invalid then exit.
  of lower('FormAddToBatch_nexttab_' & 2)
    locWaybillNumber = p_web.GSV('locWaybillNumber')
    do ValidateValue::locWaybillNumber
    If loc:Invalid
      loc:retrying = 1
      do Value::locWaybillNumber
      !do SendAlert
      !exit
    End
    If loc:Invalid then exit.
  of lower('FormAddToBatch_nexttab_' & 3)
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_FormAddToBatch_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('FormAddToBatch_tab_' & 0)
    do GenerateTab0
  of lower('FormAddToBatch_tab_' & 1)
    do GenerateTab1
  of lower('FormAddToBatch_tab_' & 2)
    do GenerateTab2
  of lower('FormAddToBatch_locWaybillNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locWaybillNumber
      of event:timer
        do Value::locWaybillNumber
      else
        do Value::locWaybillNumber
      end
  of lower('FormAddToBatch_tab_' & 3)
    do GenerateTab3
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('FormAddToBatch_form:ready_',1)

  p_web.SetSessionValue('FormAddToBatch_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_FormAddToBatch',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('FormAddToBatch_form:ready_',1)
  p_web.SetSessionValue('FormAddToBatch_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormAddToBatch',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('FormAddToBatch_form:ready_',1)
  p_web.SetSessionValue('FormAddToBatch_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('FormAddToBatch:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('FormAddToBatch_form:ready_',1)
  p_web.SetSessionValue('FormAddToBatch_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('FormAddToBatch:Primed',0)
  p_web.setsessionvalue('showtab_FormAddToBatch',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
  If p_web.GSV('locExistingBatchNumber') > 0
  End
  If p_web.GSV('locExistingBatchNumber') = 0
    If (p_web.GSV('Hide:ConsignmentNumber') <> 1)
      If not (1=0)
          If p_web.IfExistsValue('locWaybillNumber')
            locWaybillNumber = p_web.GetValue('locWaybillNumber')
          End
      End
    End
  End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormAddToBatch_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormAddToBatch_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
    loc:InvalidTab += 1
    do ValidateValue::locCompanyDetails
    If loc:Invalid then exit.
    do ValidateValue::job:Ref_Number
    If loc:Invalid then exit.
    do ValidateValue::job:Charge_Type
    If loc:Invalid then exit.
    do ValidateValue::job:Warranty_Charge_Type
    If loc:Invalid then exit.
  ! tab = 3
  If p_web.GSV('locExistingBatchNumber') > 0
    loc:InvalidTab += 1
    do ValidateValue::locBatchStatusText
    If loc:Invalid then exit.
    do ValidateValue::locBatchAccountNumber
    If loc:Invalid then exit.
    do ValidateValue::locBatchAccountName
    If loc:Invalid then exit.
    do ValidateValue::locBatchCourier
    If loc:Invalid then exit.
    do ValidateValue::locNumberInBatch
    If loc:Invalid then exit.
    do ValidateValue::buttonAddToExistingBatch
    If loc:Invalid then exit.
    do ValidateValue::textCreateNewBatchMessage
    If loc:Invalid then exit.
  End
  ! tab = 5
  If p_web.GSV('locExistingBatchNumber') = 0
    loc:InvalidTab += 1
    do ValidateValue::textEnterConsingmentNumber
    If loc:Invalid then exit.
    do ValidateValue::locWaybillNumber
    If loc:Invalid then exit.
    do ValidateValue::buttonCreateNewBatch
    If loc:Invalid then exit.
  End
  ! tab = 4
    loc:InvalidTab += 1
    do ValidateValue::buttonForceSingleDespatch
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('FormAddToBatch:Primed',0)
  p_web.StoreValue('locCompanyDetails')
  p_web.StoreValue('job:Ref_Number')
  p_web.StoreValue('job:Charge_Type')
  p_web.StoreValue('job:Warranty_Charge_Type')
  p_web.StoreValue('locBatchStatusText')
  p_web.StoreValue('locBatchAccountNumber')
  p_web.StoreValue('locBatchAccountName')
  p_web.StoreValue('locBatchCourier')
  p_web.StoreValue('locNumberInBatch')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locWaybillNumber')
  p_web.StoreValue('')
  p_web.StoreValue('')

AddToBatch           PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locExistingBatchNumber LONG                                !
FilesOpened     Long
JOBS::State  USHORT
MULDESPJ::State  USHORT
MULDESP::State  USHORT
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('AddToBatch')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'AddToBatch_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('AddToBatch','')
    p_web.DivHeader('AddToBatch',p_web.combine(p_web.site.style.formdiv,'fdiv'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('AddToBatch') = 0
        p_web.AddPreCall('AddToBatch')
        p_web.DivHeader('popup_AddToBatch','nt-hidden')
        p_web.DivHeader('AddToBatch',p_web.combine(p_web.site.style.formdiv,'fdiv'))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_AddToBatch_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_AddToBatch_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferAddToBatch',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_AddToBatch',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferAddToBatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_AddToBatch',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferAddToBatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_AddToBatch',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_AddToBatch',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferAddToBatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_AddToBatch',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferAddToBatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_AddToBatch',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_AddToBatch',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('AddToBatch')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(JOBS)
  p_web._OpenFile(MULDESPJ)
  p_web._OpenFile(MULDESP)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(MULDESPJ)
  p_Web._CloseFile(MULDESP)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  ! Start
  IF (p_web.GSV('locExistingBatchNumber') > 0)
      Access:MULDESP.ClearKey(muld:RecordNumberKey)
      muld:RecordNumber = p_web.GSV('locExistingBatchNumber')
      IF (Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign)
          IF (Access:MULDESP.PrimeRecord() = Level:Benign)
              mulj:RefNumber = muld:RecordNumber
              mulj:JobNumber = p_web.GSV('job:Ref_Number')
              mulj:IMEINumber = p_web.GSV('job:ESN')
              mulj:MSN = p_web.GSV('job:MSN')
              mulj:AccountNumber = p_web.GSV('job:Account_Number')
              mulj:SecurityPackNumber = p_web.GSV('locSecurityPackNumber')
              IF (p_web.GSV('BookingSite') = 'RRC')
                  CASE p_web.GSV('jobe:DespatchType')
                  OF 'JOB'
                      mulj:Courier = p_web.GSV('job:Courier')
                  OF 'EXC'
                      mulj:Courier = p_web.GSV('job:Exchange_Courier')
                  OF 'LOA'
                      mulj:Courier = p_web.GSV('job:Loan_Courier')
                  END
  
              ELSE
                  CASE p_web.GSV('job:Despatch_Type')
                  OF 'JOB'
                      mulj:Courier = p_web.GSV('job:Courier')
                  OF 'EXC'
                      mulj:Courier = p_web.GSV('job:Exchange_Courier')
                  OF 'LOA'
                      mulj:Courier = p_web.GSV('job:Loan_Courier')
                  END
  
              END
              IF (muld:BatchType = 'TRA')
                  mulj:AccountNumber = p_web.GSV('job:Account_Number')
                  mulj:Courier = muld:Courier
              END
              IF (Access:MULDESPJ.TryInsert() = Level:Benign)
  
              ELSE
                  Access:MULDESPJ.CancelAutoInc()
              END
          END
  
      END
  
  END
  p_web.SetValue('AddToBatch_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'AddToBatch'
    end
    p_web.formsettings.proc = 'AddToBatch'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('AddToBatch_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferAddToBatch')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('AddToBatch_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('AddToBatch_ChainTo')
    loc:formaction = p_web.GetSessionValue('AddToBatch_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction

GenerateForm   Routine
  do LoadRelatedRecords
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
    Packet = clip(Packet) & p_web.DivHeader('Tab_AddToBatch',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_AddToBatch0_div')&'">'&p_web.Translate('Add To Batch')&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="AddToBatch_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.cancel();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CancelButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="AddToBatch_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'AddToBatch_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="AddToBatch_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'AddToBatch_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_AddToBatch')>0,p_web.GSV('showtab_AddToBatch'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_AddToBatch'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_AddToBatch') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_AddToBatch'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_AddToBatch')>0,p_web.GSV('showtab_AddToBatch'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_AddToBatch') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Add To Batch') & ''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_AddToBatch_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_AddToBatch')>0,p_web.GSV('showtab_AddToBatch'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"AddToBatch",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_AddToBatch')>0,p_web.GSV('showtab_AddToBatch'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_AddToBatch_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('AddToBatch') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('AddToBatch')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Add To Batch')&'</a></h3>' & CRLF & p_web.DivHeader('tab_AddToBatch0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_AddToBatch0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_AddToBatch0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_AddToBatch0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Add To Batch')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_AddToBatch0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Add To Batch')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_AddToBatch0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Add To Batch')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_AddToBatch0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('AddToBatch_nexttab_' & 0)
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_AddToBatch_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('AddToBatch_tab_' & 0)
    do GenerateTab0
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('AddToBatch_form:ready_',1)

  p_web.SetSessionValue('AddToBatch_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_AddToBatch',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('AddToBatch_form:ready_',1)
  p_web.SetSessionValue('AddToBatch_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_AddToBatch',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('AddToBatch_form:ready_',1)
  p_web.SetSessionValue('AddToBatch_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('AddToBatch:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('AddToBatch_form:ready_',1)
  p_web.SetSessionValue('AddToBatch_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('AddToBatch:Primed',0)
  p_web.setsessionvalue('showtab_AddToBatch',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('AddToBatch_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('AddToBatch_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('AddToBatch:Primed',0)

FinishBatch          PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locWaybillNumber     STRING(30)                            !
FilesOpened     Long
MULDESPJ_ALIAS::State  USHORT
MULDESP_ALIAS::State  USHORT
WAYBILLS::State  USHORT
WAYBILLJ::State  USHORT
LOAN::State  USHORT
EXCHANGE::State  USHORT
SUBTRACC::State  USHORT
TRADEACC::State  USHORT
COURIER::State  USHORT
JOBSE::State  USHORT
WEBJOB::State  USHORT
JOBS::State  USHORT
MULDESPJ::State  USHORT
MULDESP::State  USHORT
textBatchFinished:IsInvalid  Long
locWaybillNumber:IsInvalid  Long
buttonFinishBatch:IsInvalid  Long
buttonPrintWaybill:IsInvalid  Long
buttonPrintDespatchNote:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
    MAP
ShowAlert   PROCEDURE(STRING fAlert)
    END
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FinishBatch')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'FinishBatch_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('FinishBatch','')
    p_web.DivHeader('FinishBatch',p_web.combine(p_web.site.style.formdiv,'fdiv'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('FinishBatch') = 0
        p_web.AddPreCall('FinishBatch')
        p_web.DivHeader('popup_FinishBatch','nt-hidden')
        p_web.DivHeader('FinishBatch',p_web.combine(p_web.site.style.formdiv,'fdiv'))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_FinishBatch_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_FinishBatch_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFinishBatch',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_FinishBatch',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFinishBatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_FinishBatch',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFinishBatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FinishBatch',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_FinishBatch',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFinishBatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_FinishBatch',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFinishBatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FinishBatch',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FinishBatch',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('FinishBatch')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
Despatch            ROUTINE
    PUTINI('MULTIPLEDESPATCH',p_web.GSV('muld:RecordNumber'),'InUse',CLIP(PATH()) & '\DESPATCH.LOG')
    p_web.SSV('Hide:DespatchNoteButton',1)
    IF (muld:BatchType = 'TRA')
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = muld:AccountNumber
        IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
            IF (tra:Print_Despatch_Despatch = 'YES')
                IF (tra:Summary_Despatch_Notes = 'YES')
                    ! Don't think this is used.
                    !p_web.SSV('Hide:DespatchNoteButton',0)
                END
            END
        END

    ELSE ! IF (muld:BatchType = 'TRA')
        Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
        sub:Account_Number = muld:AccountNumber
        IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
            Access:TRADEACC.ClearKey(tra:Account_Number_Key)
            tra:Account_Number = sub:Main_Account_Number
            IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
                IF (tra:Use_Sub_Accounts = 'YES')
                    IF (sub:Print_Despatch_Despatch = 'YES')
                        IF (sub:Summary_Despatch_Notes = 'YES')
                            !p_web.SSV('Hide:DespatchNoteButton',0)
                        END
                    END

                ELSE
                    IF (tra:Print_Despatch_Despatch = 'YES')
                        IF (tra:Summary_Despatch_Notes = 'YES')
                            !p_web.SSV('Hide:DespatchNoteButton',0)
                        END
                    END
                END
            END
        END
    END ! IF (muld:BatchType = 'TRA')

    Access:MULDESP.ClearKey(muld:RecordNumberKey)
    muld:RecordNumber = p_web.GSV('muld:RecordNumber')
    IF (Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign)
        p_web.SetValue('AllOK',1)
        Access:MULDESPJ.ClearKey(mulj:JobNumberKey)
        mulj:RefNumber = muld:RecordNumber
        SET(mulj:JobNumberKey,mulj:JobNumberKey)
        LOOP UNTIL Access:MULDESPJ.Next()
            IF (mulj:RefNumber <> muld:RecordNumber)
                BREAK
            END

            Access:JOBS.ClearKey(job:Ref_Number_Key)
            job:Ref_Number = mulj:JobNumber
            IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
                CYCLE
            END


            Access:JOBSE.ClearKey(jobe:RefNumberKey)
            jobe:RefNumber = job:Ref_Number
            IF (Access:JOBSE.TryFetch(jobe:RefNumberKey))
                CYCLE
            END

            Access:WEBJOB.ClearKey(wob:RefNumberKey)
            wob:RefNumber = job:Ref_Number
            IF (Access:WEBJOB.TryFetch(wob:RefNumberKey))
                CYCLE
            END

            p_web.FileToSessionQueue(JOBS)
            p_web.FileToSessionQueue(JOBSE)
            p_web.FileToSessionQueue(WEBJOB)

            ! Add to waybill job list
            ! SB does this regards of waybill. So I will to
            IF (Access:WAYBILLJ.PrimeRecord() = Level:Benign)
                waj:WayBillNumber = p_web.GSV('locWaybillNumber')
                waj:JobNumber = job:Ref_Number
                IF (p_web.GSV('BookingSite') = 'RRC')
                    CASE (jobe:DespatchType)
                    OF 'JOB'
                        waj:IMEINumber = job:ESN
                    OF 'EXC'
                        Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
                        xch:Ref_Number = job:Exchange_Unit_Number
                        IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)

                        END
                        waj:IMEINumber = xch:ESN
                    OF 'LOA'
                        Access:LOAN.ClearKey(loa:Ref_Number_Key)
                        loa:Ref_Number = job:Loan_Unit_Number
                        IF (Access:LOAN.TryFetch(loa:Ref_Number_Key))
                        END

                        waj:IMEINumber = loa:ESN
                    END
                    waj:JobType = jobe:DespatchType
                    p_web.SSV('DespatchType',jobe:DespatchType)
                ELSE
                    CASE (job:Despatch_Type)
                    OF 'JOB'
                        waj:IMEINumber = job:ESN
                    OF 'EXC'
                        Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
                        xch:Ref_Number = job:Exchange_Unit_Number
                        IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)

                        END
                        waj:IMEINumber = xch:ESN
                    OF 'LOA'
                        Access:LOAN.ClearKey(loa:Ref_Number_Key)
                        loa:Ref_Number = job:Loan_Unit_Number
                        IF (Access:LOAN.TryFetch(loa:Ref_Number_Key))
                        END

                        waj:IMEINumber = loa:ESN
                    END
                    waj:JobType = job:Despatch_Type
                    p_web.SSV('DespatchType',job:Despatch_Type)
                END
                waj:OrderNumber = job:Order_Number
                waj:SecurityPackNumber = mulj:SecurityPackNumber
                IF (Access:WAYBILLJ.TryInsert())
                    Access:WAYBILLJ.CancelAutoInc()
                    p_web.SetValue('AllOK',0)
                    CYCLE

                END

                CASE p_web.GSV('DespatchType')
                OF 'JOB'
                    Despatch:Job(p_web)
                OF 'EXC'
                    Despatch:Exc(p_web)
                OF 'LOA'
                    Despatch:Loa(p_web)
                END

                Access:JOBS.ClearKey(job:Ref_Number_Key)
                job:Ref_Number = p_web.GSV('job:Ref_Number')
                IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                    p_web.SessionQueueToFile(JOBS)
                    Access:JOBS.TryUpdate()
                END


                Access:JOBSE.ClearKey(jobe:RefNumberKey)
                jobe:RefNumber = p_web.GSV('jobe:RefNumber')
                IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
                    p_web.SessionQueueToFile(JOBSE)
                    Access:JOBSE.TryUpdate()
                END

                Access:WEBJOB.ClearKey(wob:RefNumberKey)
                wob:RefNumber = p_web.GSV('wob:RefNumber')
                IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
                    p_web.SessionQueueToFile(WEBJOB)
                    Access:WEBJOB.TryUpdate()
                END
            END
        END
        IF (p_web.GetValue('AllOK') = 1)
            Access:MULDESP.ClearKey(muld:RecordNumberKey)
            muld:RecordNumber = p_web.GSV('muld:RecordNumber')
            IF (Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign)
                IF (Relate:MULDESP.Delete(0) = Level:Benign)
                    PUTINI('MULTIPLEDESPATCH',p_web.GSV('muld:RecordNumber'),'Free',CLIP(PATH()) & '\DESPATCH.LOG')
                END
            END
        END
    END



DeleteSessionVariables      ROUTINE
    PUTINI('MULTIPLEDESPATCH',p_web.GSV('muld:RecordNumber'),'Free',CLIP(PATH()) & '\DESPATCH.LOG')
    p_web.DeleteSessionValue('IgnoreInUse')
    p_web.DeleteSessionValue('locWaybillNumber')
    p_web.DeleteSessionValue('Hide:PrintWaybillButton')
    p_web.DeleteSessionValue('Hide:DespatchNoteButton')
OpenFiles  ROUTINE
  p_web._OpenFile(MULDESPJ_ALIAS)
  p_web._OpenFile(MULDESP_ALIAS)
  p_web._OpenFile(WAYBILLS)
  p_web._OpenFile(WAYBILLJ)
  p_web._OpenFile(LOAN)
  p_web._OpenFile(EXCHANGE)
  p_web._OpenFile(SUBTRACC)
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(COURIER)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(WEBJOB)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(MULDESPJ)
  p_web._OpenFile(MULDESP)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(MULDESPJ_ALIAS)
  p_Web._CloseFile(MULDESP_ALIAS)
  p_Web._CloseFile(WAYBILLS)
  p_Web._CloseFile(WAYBILLJ)
  p_Web._CloseFile(LOAN)
  p_Web._CloseFile(EXCHANGE)
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(COURIER)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(MULDESPJ)
  p_Web._CloseFile(MULDESP)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FinishBatch_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'FinishBatch'
    end
    p_web.formsettings.proc = 'FinishBatch'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine
  DO DeleteSessionVariables

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  If p_web.GSV('locWaybillNumber') = ''
    loc:TabNumber += 1
  End
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  if p_web.IfExistsValue('locWaybillNumber') = 0
    p_web.SetSessionValue('locWaybillNumber',locWaybillNumber)
  Else
    locWaybillNumber = p_web.GetSessionValue('locWaybillNumber')
  End

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locWaybillNumber')
    locWaybillNumber = p_web.GetValue('locWaybillNumber')
    p_web.SetSessionValue('locWaybillNumber',locWaybillNumber)
  Else
    locWaybillNumber = p_web.GetSessionValue('locWaybillNumber')
  End

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('FinishBatch_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'MultipleBatchDespatch'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FinishBatch_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FinishBatch_ChainTo')
    loc:formaction = p_web.GetSessionValue('FinishBatch_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'MultipleBatchDespatch'

GenerateForm   Routine
  do LoadRelatedRecords
  ! Batch already in use?
  IF (p_web.IfExistsValue('muld:RecordNumber'))
      p_web.StoreValue('muld:RecordNumber')
  END
  
  If GETINI('MULTIPLEDESPATCH',p_web.GSV('muld:RecordNumber'),,CLIP(PATH())&'\DESPATCH.LOG') = 'InUse'
      IF (p_web.IfExistsValue('IgnoreInUse'))
          p_web.StoreValue('IgnoreInUse')
      END
  
      IF (p_web.GSV('IgnoreInUse') = 1)
      ELSE
          p_web.SSV('Message:Text','The selected batch appears to be in use by another station. If you continue despatching, corruption may occur.'&|
              '\n\nIf you are SURE this batch is not in use, click ''OK''. Otherwise click ''Cancel'' and try again.')
          p_web.SSV('Message:PassURL','FinishBatch?IgnoreInUse=1')
          p_web.SSV('Message:FailURL','MultipleBatchDespatch')
          MessageQuestion(p_web)
          EXIT
      END
  
  End !If Sub(Format(Clock(),@t01),4,2) = GETINI('MULTIPLEDESPATCH',muld:BatchNUmber,,CLIP(PATH())&'\DESPATCH.LOG')
  ! Despatch now, or cons number required?
  p_web.SSV('Hide:PrintWaybillButton',1)
  locWaybillNumber = ''
  p_web.SSV('locWaybillNumber',locWaybillNumber)
  Access:MULDESP.ClearKey(muld:RecordNumberKey)
  muld:RecordNumber = p_web.GSV('muld:RecordNumber')
  IF (Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign)
  
      Access:COURIER.ClearKey(cou:Courier_Key)
      cou:Courier = muld:Courier
      IF (Access:COURIER.TryFetch(cou:Courier_Key))
      END
  
      IF (cou:PrintWaybill)
          locWaybillNumber = NextWaybillNumber()
  
          Access:WAYBILLS.ClearKey(way:WayBillNumberKey)
          way:WayBillNumber = locWaybillNumber
          IF (Access:WAYBILLS.TryFetch(way:WayBillNumberKey) = Level:Benign)
              way:WayBillType = 1 ! Created From RRC
              way:AccountNumber = muld:AccountNumber
              way:FromAccount = p_web.GSV('BookingAccount')
              IF (p_web.GSV('BookingSite') = 'RRC')
                  way:ToAccount = muld:AccountNumber
                  way:WaybillID = 10 ! RRC TO Customer (Multi)
                  ! Allow for mult desp back to PUP
                  Access:MULDESPJ.ClearKey(mulj:JobNumberKey)
                  mulj:RefNumber = muld:RecordNumber
                  SET(mulj:JobNumberKey,mulj:JobNumberKey)
                  LOOP UNTIL Access:MULDESPJ.Next()
                      IF (mulj:RefNumber <> muld:RecordNumber)
                          BREAK
                      END
  
                      Access:JOBS.ClearKey(job:Ref_Number_Key)
                      job:Ref_Number = mulj:JobNumber
                      IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
                          CYCLE
                      END
  
                      IF (job:who_booked = 'WEB')
                          way:WayBillType = 9
                          way:WaybillID = 22
                      END
                      BREAK
                  END
  
              ELSE ! IF (p_web.GSV('BookingSite') = 'RRC')
                  way:ToAccount = muld:AccountNumber
                  ! Send To Customer or RRC?
                  Access:MULDESPJ.ClearKey(mulj:JobNumberKey)
                  mulj:RefNumber = muld:RecordNumber
                  SET(mulj:JobNumberKey,mulj:JobNumberKey)
                  LOOP UNTIL Access:MULDESPJ.Next()
                      IF (mulj:RefNumber <> muld:RecordNumber)
                          BREAK
                      END
  
                      Access:JOBS.ClearKey(job:Ref_Number_Key)
                      job:Ref_Number = mulj:JobNumber
                      IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
                          CYCLE
                      END
                      Access:JOBSE.ClearKey(jobe:RefNumberKey)
                      jobe:RefNumber = job:Ref_Number
                      IF (Access:JOBSE.TryFetch(jobe:RefNumberKey))
                          CYCLE
                      END
                      Access:WEBJOB.ClearKey(wob:RefNumberKey)
                      wob:RefNumber = job:Ref_Number
                      IF (Access:WEBJOB.TryFetch(wob:RefNumberKey))
                          CYCLE
                      END
  
                      IF (jobe:WebJob)
                          way:WaybillID = 12 ! ARC TO RRC (Multi)
                          way:AccountNumber = wob:HeadAccountNumber
                          way:ToAccount = wob:HeadAccountNumber
                      ELSE
                          way:WaybillID = 11 ! ARC TO Customer
                          way:WayBillType = 2 ! Don't show in confirmation
                      END
  
                     BREAK
                  END
              END ! IF (p_web.GSV('BookingSite') = 'RRC')
              Access:WAYBILLS.TryUpdate()
              p_web.SSV('Hide:PrintWaybillButton',0)
              ! Waybill Variables
              ! locWaybillNumber
              p_web.SSV('Waybill:Courier',muld:Courier)
              ! Waybill:Courier
              IF (p_web.GSV('BookingSite') = 'RRC')
                  p_web.SSV('Waybill:FromType','TRA')
              ELSE
                  p_web.SSV('Waybill:FromType','DEF')
              END
              ! Waybill:FromType
              p_web.SSV('Waybill:FromAccount',p_web.GSV('BookingAccount'))
              ! Waybill:FromAccount
              p_web.SSV('Waybill:ToType',muld:BatchType)
              ! Waybill:ToType
              p_web.SSV('Waybill:ToAccount',muld:AccountNumber)
              ! Waybill:ToAccount
          END
  
          p_web.SSV('locWaybillNumber',locWaybillNumber)
          DO Despatch
      ELSE
          IF (cou:AutoConsignmentNo)
              cou:LastConsignmentNo += 1
              Access:COURIER.TryUpdate()
              locWaybillNumber = cou:LastConsignmentNo
              p_web.SSV('locWaybillNumber',locWaybillNumber)
              DO Despatch
          ELSE
              locWaybillNumber = ''
          END
      END
  END
  
  
  
  
 locWaybillNumber = p_web.RestoreValue('locWaybillNumber')
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Multiple Despatch') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Multiple Despatch',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_FinishBatch',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_FinishBatch0_div')&'">'&p_web.Translate('Finish Batch')&'</a></li>'& CRLF
      If p_web.GSV('locWaybillNumber') = ''
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_FinishBatch1_div')&'">'&p_web.Translate('Insert Consignment Number')&'</a></li>'& CRLF
      End
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_FinishBatch2_div')&'">'&p_web.Translate('Paperwork')&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="FinishBatch_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="FinishBatch_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'FinishBatch_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="FinishBatch_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'FinishBatch_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_FinishBatch')>0,p_web.GSV('showtab_FinishBatch'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_FinishBatch'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_FinishBatch') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_FinishBatch'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_FinishBatch')>0,p_web.GSV('showtab_FinishBatch'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_FinishBatch') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Finish Batch') & ''''
      If p_web.GSV('locWaybillNumber') = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Insert Consignment Number') & ''''
      End
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Paperwork') & ''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_FinishBatch_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_FinishBatch')>0,p_web.GSV('showtab_FinishBatch'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"FinishBatch",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_FinishBatch')>0,p_web.GSV('showtab_FinishBatch'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_FinishBatch_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('FinishBatch') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('FinishBatch')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Finish Batch')&'</a></h3>' & CRLF & p_web.DivHeader('tab_FinishBatch0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_FinishBatch0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_FinishBatch0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_FinishBatch0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Finish Batch')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_FinishBatch0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Finish Batch')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_FinishBatch0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Finish Batch')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_FinishBatch0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
      If p_web.GSV('locWaybillNumber') <> ''
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::textBatchFinished
        do Comment::textBatchFinished
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab1  Routine
  If p_web.GSV('locWaybillNumber') = ''
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Insert Consignment Number')&'</a></h3>' & CRLF & p_web.DivHeader('tab_FinishBatch1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_FinishBatch1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_FinishBatch1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_FinishBatch1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Insert Consignment Number')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_FinishBatch1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Insert Consignment Number')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_FinishBatch1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Insert Consignment Number')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_FinishBatch1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locWaybillNumber
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locWaybillNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locWaybillNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::buttonFinishBatch
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::buttonFinishBatch
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
  end
GenerateTab2  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Paperwork')&'</a></h3>' & CRLF & p_web.DivHeader('tab_FinishBatch2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_FinishBatch2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_FinishBatch2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_FinishBatch2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Paperwork')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_FinishBatch2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Paperwork')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_FinishBatch2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Paperwork')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_FinishBatch2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::buttonPrintWaybill
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::buttonPrintWaybill
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::buttonPrintDespatchNote
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::buttonPrintDespatchNote
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Validate::textBatchFinished  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::textBatchFinished  ! copies value to session value if valid.
  do Comment::textBatchFinished ! allows comment style to be updated.

ValidateValue::textBatchFinished  Routine
  If p_web.GSV('locWaybillNumber') <> ''
    If not (1=0)
    End
  End

Value::textBatchFinished  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FinishBatch_' & p_web._nocolon('textBatchFinished') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="textBatchFinished" class="'&clip('green bold')&'"'&clip(loc:extra)&'>' & p_web.Translate('Batch Despatched',) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::textBatchFinished  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if textBatchFinished:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FinishBatch_' & p_web._nocolon('textBatchFinished') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locWaybillNumber  Routine
  packet = clip(packet) & p_web.DivHeader('FinishBatch_' & p_web._nocolon('locWaybillNumber') & '_prompt',Choose(p_web.GSV('locWaybillNumber') <> '','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('locWaybillNumber') <> '','',p_web.Translate('Consignment Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locWaybillNumber  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locWaybillNumber = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locWaybillNumber = p_web.GetValue('Value')
  End
  do ValidateValue::locWaybillNumber  ! copies value to session value if valid.
  do Value::locWaybillNumber
  do SendAlert
  do Comment::locWaybillNumber ! allows comment style to be updated.

ValidateValue::locWaybillNumber  Routine
    If not (p_web.GSV('locWaybillNumber') <> '')
    locWaybillNumber = Upper(locWaybillNumber)
      if loc:invalid = '' then p_web.SetSessionValue('locWaybillNumber',locWaybillNumber).
    End

Value::locWaybillNumber  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('locWaybillNumber') <> '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FinishBatch_' & p_web._nocolon('locWaybillNumber') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    locWaybillNumber = p_web.RestoreValue('locWaybillNumber')
    do ValidateValue::locWaybillNumber
    If locWaybillNumber:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('locWaybillNumber') <> '')
  ! --- STRING --- locWaybillNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locWaybillNumber'',''finishbatch_locwaybillnumber_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locWaybillNumber',p_web.GetSessionValueFormat('locWaybillNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locWaybillNumber  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locWaybillNumber:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('locWaybillNumber') <> '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FinishBatch_' & p_web._nocolon('locWaybillNumber') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('locWaybillNumber') <> ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonFinishBatch  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonFinishBatch  ! copies value to session value if valid.
  IF (p_web.GSV('locWaybillNumber') <> '')
      DO Despatch
  END
  do Value::buttonFinishBatch
  do Comment::buttonFinishBatch ! allows comment style to be updated.
  do Value::buttonPrintDespatchNote  !1

ValidateValue::buttonFinishBatch  Routine
    If not (1=0)
    End

Value::buttonFinishBatch  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FinishBatch_' & p_web._nocolon('buttonFinishBatch') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonFinishBatch'',''finishbatch_buttonfinishbatch_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','FinishBatch','Finish Batch',p_web.combine(Choose('Finish Batch' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'MainButtonIcon'),loc:formname,,,,loc:javascript,loc:disabled,'images/package.png',,,,,,,,,'nt-left')!h
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonFinishBatch  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonFinishBatch:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FinishBatch_' & p_web._nocolon('buttonFinishBatch') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonPrintWaybill  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonPrintWaybill  ! copies value to session value if valid.
  do Comment::buttonPrintWaybill ! allows comment style to be updated.

ValidateValue::buttonPrintWaybill  Routine
    If not (p_web.GSV('Hide:PrintWaybillButton') = 1)
    End

Value::buttonPrintWaybill  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:PrintWaybillButton') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FinishBatch_' & p_web._nocolon('buttonPrintWaybill') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (p_web.GSV('Hide:PrintWaybillButton') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','PrintWaybill','Waybill',p_web.combine(Choose('Waybill' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.WindowOpen(clip('Waybill')&''&'','_blank'),,loc:disabled,'images/printer.png',,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonPrintWaybill  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonPrintWaybill:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:PrintWaybillButton') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FinishBatch_' & p_web._nocolon('buttonPrintWaybill') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:PrintWaybillButton') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonPrintDespatchNote  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonPrintDespatchNote  ! copies value to session value if valid.
  do Comment::buttonPrintDespatchNote ! allows comment style to be updated.

ValidateValue::buttonPrintDespatchNote  Routine
    If not (p_web.GSV('Hide:DespatchNoteButton') = 1)
    End

Value::buttonPrintDespatchNote  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:DespatchNoteButton') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FinishBatch_' & p_web._nocolon('buttonPrintDespatchNote') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (p_web.GSV('Hide:DespatchNoteButton') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','DespachNote','Despatch Note',p_web.combine(Choose('Despatch Note' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.WindowOpen(clip('MultipleDespatchNote')&''&'','_blank'),,loc:disabled,'images/printer.png',,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonPrintDespatchNote  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonPrintDespatchNote:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:DespatchNoteButton') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FinishBatch_' & p_web._nocolon('buttonPrintDespatchNote') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:DespatchNoteButton') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('FinishBatch_nexttab_' & 0)
    If loc:Invalid then exit.
  of lower('FinishBatch_nexttab_' & 1)
    locWaybillNumber = p_web.GSV('locWaybillNumber')
    do ValidateValue::locWaybillNumber
    If loc:Invalid
      loc:retrying = 1
      do Value::locWaybillNumber
      !do SendAlert
      do Comment::locWaybillNumber ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('FinishBatch_nexttab_' & 2)
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_FinishBatch_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('FinishBatch_tab_' & 0)
    do GenerateTab0
  of lower('FinishBatch_tab_' & 1)
    do GenerateTab1
  of lower('FinishBatch_locWaybillNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locWaybillNumber
      of event:timer
        do Value::locWaybillNumber
        do Comment::locWaybillNumber
      else
        do Value::locWaybillNumber
      end
  of lower('FinishBatch_buttonFinishBatch_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonFinishBatch
      of event:timer
        do Value::buttonFinishBatch
        do Comment::buttonFinishBatch
      else
        do Value::buttonFinishBatch
      end
  of lower('FinishBatch_tab_' & 2)
    do GenerateTab2
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('FinishBatch_form:ready_',1)

  p_web.SetSessionValue('FinishBatch_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_FinishBatch',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('FinishBatch_form:ready_',1)
  p_web.SetSessionValue('FinishBatch_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FinishBatch',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('FinishBatch_form:ready_',1)
  p_web.SetSessionValue('FinishBatch_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('FinishBatch:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('FinishBatch_form:ready_',1)
  p_web.SetSessionValue('FinishBatch_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('FinishBatch:Primed',0)
  p_web.setsessionvalue('showtab_FinishBatch',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
  If p_web.GSV('locWaybillNumber') = ''
      If not (p_web.GSV('locWaybillNumber') <> '')
          If p_web.IfExistsValue('locWaybillNumber')
            locWaybillNumber = p_web.GetValue('locWaybillNumber')
          End
      End
  End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FinishBatch_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  DO DeleteSessionVariables
  p_web.DeleteSessionValue('FinishBatch_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
    do ValidateValue::textBatchFinished
    If loc:Invalid then exit.
  ! tab = 2
  If p_web.GSV('locWaybillNumber') = ''
    loc:InvalidTab += 1
    do ValidateValue::locWaybillNumber
    If loc:Invalid then exit.
    do ValidateValue::buttonFinishBatch
    If loc:Invalid then exit.
  End
  ! tab = 3
    loc:InvalidTab += 1
    do ValidateValue::buttonPrintWaybill
    If loc:Invalid then exit.
    do ValidateValue::buttonPrintDespatchNote
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('FinishBatch:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('locWaybillNumber')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')

alertmessage  Routine
  packet = clip(packet) & p_web.AsciiToUTF(|
    '<<SCRIPT LANGUAGE="javascript"><13,10>'&|
    '<<!--<13,10>'&|
    'function CONFIRM(){{if (!confirm<13,10>'&|
    '("Change this to your message"))<13,10>'&|
    'window.location = "MultipleBatchDespatch";return " "}<13,10>'&|
    'document.writeln(CONFIRM())<13,10>'&|
    '<<!-- END --><13,10>'&|
    '<</SCRIPT><13,10>'&|
    '',net:OnlyIfUTF)
ShowAlert           Procedure(String fAlert)
Code
    packet = clip(packet) & '<script language="JavaScript" type="text/javascript">alert("' & clip(fAlert) & '")</script>'
    do sendPacket
FormJobsInBatch      PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
FilesOpened     Long
MULDESP::State  USHORT
MULDESPJ::State  USHORT
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormJobsInBatch')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'FormJobsInBatch_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('FormJobsInBatch','')
    p_web.DivHeader('FormJobsInBatch',p_web.combine(p_web.site.style.formdiv,'fdiv'))
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('FormJobsInBatch') = 0
        p_web.AddPreCall('FormJobsInBatch')
        p_web.DivHeader('popup_FormJobsInBatch','nt-hidden')
        p_web.DivHeader('FormJobsInBatch',p_web.combine(p_web.site.style.formdiv,'fdiv'))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_FormJobsInBatch_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_FormJobsInBatch_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormJobsInBatch',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:StagePost
    do RestoreMem
    do PostInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_FormJobsInBatch',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormJobsInBatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:StagePost
    do RestoreMem
    do PostCopy
  of Net:CopyRecord + NET:WEB:Populate
    If p_web.IfExistsValue('mulj:RecordNumber') = 0 then p_web.SetValue('mulj:RecordNumber',p_web.GSV('mulj:RecordNumber')).
    do PreCopy
    p_web.setsessionvalue('showtab_FormJobsInBatch',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormJobsInBatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormJobsInBatch',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do PostInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do PostUpdate
    End
  of Net:ChangeRecord + NET:WEB:Populate
    If p_web.IfExistsValue('mulj:RecordNumber') = 0 then p_web.SetValue('mulj:RecordNumber',p_web.GSV('mulj:RecordNumber')).
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_FormJobsInBatch',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormJobsInBatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:DeleteRecord + NET:WEB:StagePost
    do RestoreMem
    do PostDelete
  of Net:ViewRecord + NET:WEB:Populate
    If p_web.IfExistsValue('mulj:RecordNumber') = 0 then p_web.SetValue('mulj:RecordNumber',p_web.GSV('mulj:RecordNumber')).
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_FormJobsInBatch',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormJobsInBatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormJobsInBatch',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormJobsInBatch',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('FormJobsInBatch')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(MULDESP)
  p_web._OpenFile(MULDESPJ)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(MULDESP)
  p_Web._CloseFile(MULDESPJ)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FormJobsInBatch_form:inited_',1)
  p_web.formsettings.file = 'MULDESPJ'
  p_web.formsettings.key = 'mulj:RecordNumberKey'
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = 'MULDESPJ'
    p_web.formsettings.key = 'mulj:RecordNumberKey'
      clear(p_web.formsettings.FieldName)
    p_web.formsettings.recordid[1] = mulj:RecordNumber
    p_web.formsettings.FieldName[1] = 'mulj:RecordNumber'
    do SetAction
    if p_web.GetSessionValue('FormJobsInBatch:Primed') = 1
      p_web.formsettings.action = Net:ChangeRecord
    Else
      p_web.formsettings.action = Loc:Act
    End
    p_web.formsettings.OriginalAction = Loc:Act
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'FormJobsInBatch'
    end
    p_web.formsettings.proc = 'FormJobsInBatch'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine
  IF p_web.GetSessionValue('FormJobsInBatch:Primed') = 1
    p_web._deleteFile(MULDESPJ)
    p_web.SetSessionValue('FormJobsInBatch:Primed',0)
  End

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine
  p_web.SetValue('UpdateFile','MULDESPJ')
  p_web.SetValue('UpdateKey','mulj:RecordNumberKey')

AfterLookup Routine
  loc:TabNumber = -1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=File

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('FormJobsInBatch_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferFormJobsInBatch')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormJobsInBatch_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormJobsInBatch_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormJobsInBatch_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction

GenerateForm   Routine
  do LoadRelatedRecords
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  loc:FormHeading = p_web.Translate('Update MULDESPJ',0)
  If loc:FormHeading
    if loc:popup
      packet = clip(packet) & p_web.jQuery('#popup_'&lower('FormJobsInBatch')&'_div','dialog','"option","title",'&p_web.jsString(loc:FormHeading,0),,0)
    else
      packet = clip(packet) & lower('<div id="form-access-FormJobsInBatch"></div>')
      packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formheading,)&'">'&clip(loc:FormHeading)&'</div>'&CRLF
    end
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_FormJobsInBatch',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="FormJobsInBatch_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.cancel();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CancelButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="FormJobsInBatch_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'FormJobsInBatch_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="FormJobsInBatch_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'FormJobsInBatch_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_FormJobsInBatch')>0,p_web.GSV('showtab_FormJobsInBatch'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_FormJobsInBatch'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_FormJobsInBatch') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_FormJobsInBatch'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_FormJobsInBatch')>0,p_web.GSV('showtab_FormJobsInBatch'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_FormJobsInBatch') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_FormJobsInBatch_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_FormJobsInBatch')>0,p_web.GSV('showtab_FormJobsInBatch'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"FormJobsInBatch",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_FormJobsInBatch')>0,p_web.GSV('showtab_FormJobsInBatch'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_FormJobsInBatch_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('FormJobsInBatch') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('FormJobsInBatch')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine


NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_FormJobsInBatch_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('FormJobsInBatch_form:ready_',1)

  p_web.SetSessionValue('FormJobsInBatch_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_FormJobsInBatch',0)
  mulj:Current = 0
  p_web.SetSessionValue('mulj:Current',mulj:Current)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('FormJobsInBatch_form:ready_',1)
  p_web.SetSessionValue('FormJobsInBatch_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormJobsInBatch',0)
  p_web._PreCopyRecord(MULDESPJ,mulj:RecordNumberKey)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('FormJobsInBatch_form:ready_',1)
  p_web.SetSessionValue('FormJobsInBatch_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('FormJobsInBatch:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('FormJobsInBatch_form:ready_',1)
  p_web.SetSessionValue('FormJobsInBatch_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('FormJobsInBatch:Primed',0)
  p_web.setsessionvalue('showtab_FormJobsInBatch',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormJobsInBatch_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormJobsInBatch_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostInsert      Routine
PostCopy        Routine
  p_web.SetSessionValue('FormJobsInBatch:Primed',0)
PostUpdate      Routine
  p_web.SetSessionValue('FormJobsInBatch:Primed',0)


PostDelete      Routine
  CountBatch# = 0
  Access:MULDESPJ.ClearKey(mulj:JobNumberKey)
  mulj:RefNumber = p_web.GSV('muld:RecordNumber')
  Set(mulj:JobNumberKey,mulj:JobNumberKey)
  Loop
      If Access:MULDESPJ.NEXT()
          Break
      End !If
      If mulj:RefNumber <> p_web.GSV('muld:RecordNumber')      |
          Then Break.  ! End If
      CountBatch# += 1
  End !Loop
  
  Access:MULDESP.ClearKey(muld:RecordNumberKey)
  muld:RecordNumber = p_web.GSV('muld:RecordNumber')
  IF (Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign)
  
      muld:BatchTotal = CountBatch#
      Access:MULDESP.TryUpdate()
  END
  
BrowseJobsInBatch    PROCEDURE  (NetWebServerWorker p_web)
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256),dim(Net:MaxKeyFields)
idx           String(Net:HashSize)
sub           Long
            End
mulj:JobNumber:IsInvalid  Long
Delete:IsInvalid  Long
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:DefaultSelection    String(Net:HashSize)
loc:ActualSelection     String(Net:HashSize)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
!loc:pagename            String(256)
loc:NavButtonPosition    Long
loc:UpdateButtonPosition Long
loc:SelectionMethod      Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorCase         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
loc:lookupdone          Long
loc:FormPopup           Long
ThisView            View(MULDESPJ)
                      Project(mulj:RecordNumber)
                      Project(mulj:JobNumber)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
Loc:popup             Long
loc:poppedup          Long,static,thread
FilesOpened     Long
MULDESP::State  USHORT
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return 
  End
  GlobalErrors.SetProcedureName('BrowseJobsInBatch')
  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseJobsInBatch:NoForm')
      loc:NoForm = p_web.GetValue('BrowseJobsInBatch:NoForm')
      loc:FormName = p_web.GetValue('BrowseJobsInBatch:FormName')
    else
      loc:FormName = 'BrowseJobsInBatch_frm'
    End
    p_web.SSV('BrowseJobsInBatch:NoForm',loc:NoForm)
    p_web.SSV('BrowseJobsInBatch:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseJobsInBatch:NoForm')
    loc:FormName = p_web.GSV('BrowseJobsInBatch:FormName')
  end
  case p_web.site.formpopups
  of -1 ; loc:FormPopup = Net:Page
  of 0 ; loc:FormPopup = Net:Page
  of 1 ; loc:FormPopup = Net:Popup
  End
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseJobsInBatch') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseJobsInBatch')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if 1 = 2
  elsif p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  elsif p_web.IfExistsValue('_EIPRow_')
    do CallRow
  elsif p_web.GetValue('_CallPopups') > 0
    do CallPopups
  elsif p_web.IfExistsValue('_GetSecwinSettings')
      p_web.SetValue('Secwin_' & 'BrowseJobsInBatch' & 'AccessGroupsArray','1') 
      p_web.SetValue('Secwin_AccessWindowName','BrowseJobsInBatch')
      
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallPopups  Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
loc:CallPopups   Long
loc:name         String(255)
  code
  if loc:poppedup = 0
    loc:CallPopups = p_web.GetValue('_CallPopups')
    if loc:CallPopups = 1 !and p_web.GetPreCall('BrowseJobsInBatch') = 0! for = 1 include popup divs and scripts for this browse
      !p_web.AddPreCall('BrowseJobsInBatch')
      p_web.DivHeader('popup_BrowseJobsInBatch','nt-hidden')
      p_web.DivHeader('BrowseJobsInBatch',p_web.combine(p_web.site.style.browsediv,'fdiv'))
      p_web.DivFooter()
      p_web.DivFooter()
      loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(400)&', modal: true, position: [''center'',15]'
      packet =  p_web.jQuery('#' & lower('popup_BrowseJobsInBatch_div'),'dialog',loc:options) & |
      p_web.jQuery('#' & lower('popup_BrowseJobsInBatch_div'),'removeClass','''nt-hidden''')
    End
    ! for > 0 add in form, EIP-lookups and browse object
    do Popups
    loc:popup = 1
    do ClosingScripts
    do SendPacket
    loc:poppedup = 1
  end

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web.DivHeader('BrowseJobsInBatch',p_web.combine(p_web.site.style.browsediv,'fdiv'))
  if loc:ParentSilent = 0
    do GenerateBrowse
    if p_web.RequestAjax = 1
      p_web.Script('BrowseProducts.pcv();')
    end
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web.DivFooter()
  do Children
  do Popups
  if loc:poppedup = 0
    do ClosingScripts
  end
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(MULDESPJ,mulj:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'MULJ:JOBNUMBER') then p_web.SetValue('BrowseJobsInBatch_sort','1')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseJobsInBatch:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseJobsInBatch:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseJobsInBatch:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseJobsInBatch:LookupField')
    loc:selecting = 0
  End

Popups Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  If loc:popup = 0
  End
SetFormAction  Routine
  loc:formaction = 'FormJobsInBatch'
  loc:formactiontarget = '_self'

GotFocusBack   Routine

GenerateBrowse Routine
  data
loc:viewoptions  Long
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  ! Set general Browse options
  loc:NavButtonPosition   = Net:Below
  loc:UpdateButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:FileLoad
  loc:FillBack         = 0
  loc:Sorting          = Net:ServerSort
  if p_web.GetValue('_popup_') = 1
    loc:popup = 1
  end
  ! Set Locator Options
  loc:LocatorPosition  = Net:Below
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  p_web.ClearBrowse('BrowseJobsInBatch')
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  IF (p_web.IfExistsValue('locPassedBatchNumber'))
      p_web.StoreValue('locPassedBatchNumber')
  END
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 10
  End
  loc:ActualSelection = ''
  !loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseJobsInBatch_sort',net:DontEvaluate)
  p_web.SetSessionValue('BrowseJobsInBatch_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'mulj:JobNumber','-mulj:JobNumber')
    Loc:LocateField = 'mulj:JobNumber'
    Loc:LocatorCase = 0
  of 2
    Loc:LocateField = ''
    Loc:LocatorCase = 0
  end
  if loc:vorder = ''
    loc:vorder = '+mulj:RefNumber,+mulj:JobNumber'
  end
  If False ! add range fields to sort order
  Else
    If Instring('MULJ:REFNUMBER',upper(loc:vOrder),1,1) = 0
      loc:vOrder = 'mulj:RefNumber,' & loc:vorder
    End
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('mulj:JobNumber')
    loc:SortHeader = p_web.Translate('Job Number')
    p_web.SetSessionValue('BrowseJobsInBatch_LocatorPic','@s8')
  End
  If loc:selecting = 1
    p_web.GetSettings(p_web.GetSessionValue('Push1'))
    loc:selectaction = p_web.FormSettings.ParentPage !p_web.GetSessionValue('BrowseJobsInBatch:LookupFrom')
  End!Else
  loc:CloseAction = 'MultipleBatchDespatch'
  do SendPacket
  do SetFormAction
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseJobsInBatch:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseJobsInBatch:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseJobsInBatch:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="MULDESPJ"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="mulj:RecordNumberKey"></input><13,10>'
  end
  If p_web.Translate('Job In Batch Number: ' & p_web.GSV('muld:RecordNumber')) <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.browseSubHeading,)&'">'&p_web.Translate('Job In Batch Number: ' & p_web.GSV('muld:RecordNumber'),0)&'</div>'&CRLF
  End
  case p_web.GetValue('refresh') ; of 'next' orof 'previous' orof 'first' orof 'last'
    p_web.Script('try{{BrowseJobsInBatch.restoreFocus();} catch(err){{};')
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseJobsInBatch',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      case loc:locatortype
      of Net:Position
      orof Net:Date
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
      of Net:Contains
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
      of Net:Begins
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
      End
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,'Locator')&'"><tr>' &|
      Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
      Case Loc:LocatorType
      of Net:Date
        packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator2BrowseJobsInBatch','',p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,, p_web.GSV('BrowseJobsInBatch_LocatorPic'),,,'onchange="BrowseJobsInBatch.locate(''Locator2BrowseJobsInBatch'',this.value);"',,0,,,) & '</div></td>'
      Else
        packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator2BrowseJobsInBatch',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,'size="30" onchange="BrowseJobsInBatch.locate(''Locator2BrowseJobsInBatch'',this.value);" ',,,,,,) & '</td>'
      End
      If loc:LocatorSearchButton or loc:LocatorClearButton
        packet = clip(packet) & '<td><div id="BrowseJobsInBatch_locate_a" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
        If loc:LocatorSearchButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
        End
        If loc:LocatorClearButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseJobsInBatch.cl(''BrowseJobsInBatch'');')
        End
        packet = clip(packet) & '</div><13,10></td><13,10>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
      If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'BrowseJobsInBatch_locate_a','buttonset',loc:options)
      End
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & p_web.DivHeader('BrowseJobsInBatch_table',p_web.Combine(p_web.site.style.BrowseTableDiv,'BrowseLookup'),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,,'sortable')&'" id="BrowseJobsInBatch_tbl">'
  Else
    packet = clip(packet) & p_web.DivHeader('BrowseJobsInBatch_table',p_web.Combine(p_web.site.style.BrowseTableDiv,'BrowseLookup'),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,)&'" id="BrowseJobsInBatch_tbl">'
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'1','BrowseJobsInBatch',p_web.Translate('Job Number'),'Click here to sort by Job Number',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
      If loc:Selecting = 0
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'2','BrowseJobsInBatch',p_web.Translate(),,,,,,,0,loc:Sorting,'Button')
          do AddPacket
          loc:columns += 1
      End ! Selecting
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,10,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  If Instring('mulj:recordnumber',lower(loc:vorder),1,1) = 0 !and MULDESPJ{prop:SQLDriver} = 1
    loc:vorder = Choose(loc:vorder='','mulj:RecordNumber',clip(loc:vorder) & ',' & 'mulj:RecordNumber')
  End
  Loc:Selected = Choose(p_web.IfExistsValue('mulj:RecordNumber'),p_web.GetValue('mulj:RecordNumber'),p_web.GetSessionValue('mulj:RecordNumber'))
  ThisView{prop:order} = p_web.CleanFilter(ThisView,clip(loc:vorder))
    muld:RecordNumber = p_web.RestoreValue('muld:RecordNumber')
    loc:FilterWas = 'mulj:RefNumber = ' & muld:RecordNumber
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseJobsInBatch',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseJobsInBatch_Filter')
    p_web.SetSessionValue('BrowseJobsInBatch_FirstValue','')
    p_web.SetSessionValue('BrowseJobsInBatch_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,MULDESPJ,mulj:RecordNumberKey,loc:PageRows,'BrowseJobsInBatch',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,loc:ViewOptions,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled,Loc:LocatorCase)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or Loc:LocatorType = Net:Date or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position or loc:LocatorType = Net:Date
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If MULDESPJ{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(MULDESPJ,loc:firstvalue)
              Reset(ThisView,MULDESPJ)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If MULDESPJ{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(MULDESPJ,loc:lastvalue)
            Reset(ThisView,MULDESPJ)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(mulj:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td class="'&p_web.combine(p_web.site.style.BrowseEmpty,)&'">'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:NavButtonPosition=Net:Above or (loc:NavButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseJobsInBatch_nav_a" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseJobsInBatch.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseJobsInBatch.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseJobsInBatch.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseJobsInBatch.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseJobsInBatch_nav_a','buttonset',loc:options)
          End
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:UpdateButtonPosition=Net:Above or (loc:UpdateButtonPosition=Net:Both and loc:found > 0))
    packet = clip(packet) & '<div id="BrowseJobsInBatch_update_a" class="'&p_web.combine(p_web.site.style.BrowseUpdateButtonSet,)&'">'
    If loc:found
          TableQueue.Kind = Net:BeforeTable
          do AddPacket
    End
    packet = clip(packet) & '</div><13,10>'
    If p_web.site.UseUpdateButtonSet
      loc:options = ''
      packet = clip(packet) & p_web.jQuery('#' & 'BrowseJobsInBatch_update_a','buttonset',loc:options)
    End ! If p_web.site.UseUpdateButtonSet
      TableQueue.Kind = Net:BeforeTable
      do AddPacket
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseJobsInBatch',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseJobsInBatch_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseJobsInBatch_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          case loc:locatortype
          of Net:Position
          orof Net:Date
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
          of Net:Contains
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
          of Net:Begins
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
          End
          packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,'Locator')&'"><tr>' &|
          Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
          Case Loc:LocatorType
          of Net:Date
            packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator1BrowseJobsInBatch','',p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,, p_web.GSV('BrowseJobsInBatch_LocatorPic'),,,'onchange="BrowseJobsInBatch.locate(''Locator1BrowseJobsInBatch'',this.value);"',,0,,,) & '</div></td>'
          Else
            packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator1BrowseJobsInBatch',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,'size="30" onchange="BrowseJobsInBatch.locate(''Locator1BrowseJobsInBatch'',this.value);" ',,,,,,) & '</td>'
          End
          If loc:LocatorSearchButton or loc:LocatorClearButton
            packet = clip(packet) & '<td><div id="BrowseJobsInBatch_locate_b" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
            If loc:LocatorSearchButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
            End
            If loc:LocatorClearButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseJobsInBatch.cl(''BrowseJobsInBatch'');')
            End
            packet = clip(packet) & '</div><13,10></td><13,10>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
          If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseJobsInBatch_locate_b','buttonset',loc:options)
          End
      End
    End
  End
  p_web.SetSessionValue('BrowseJobsInBatch_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseJobsInBatch_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:NavButtonPosition=Net:Below or loc:NavButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseJobsInBatch_nav_b" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseJobsInBatch.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseJobsInBatch.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseJobsInBatch.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseJobsInBatch.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseJobsInBatch_nav_b','buttonset',loc:options)
          End
        end
        do SendPacket
  End
  If loc:UpdateButtonPosition=Net:Below or loc:UpdateButtonPosition=Net:Both
  packet = clip(packet) & '<div id="BrowseJobsInBatch_update_b" class="'&p_web.combine(p_web.site.style.BrowseUpdateButtonSet,)&'">'
  If loc:found
        do SendPacket
  End
  packet = clip(packet) & '</div><13,10>'
  If p_web.site.UseUpdateButtonSet
    loc:options = ''
    packet = clip(packet) & p_web.jQuery('#' & 'BrowseJobsInBatch_update_b','buttonset',loc:options)
  End ! If p_web.site.UseUpdateButtonSet
    do SendPacket
  End
  If loc:selecting = 0 and loc:parent = ''
      if loc:popup
        packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:BrowseCloseButton,'BrowseJobsInBatch',,,loc:popup)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCloseButton,loc:Formname,loc:CloseAction)
      end
  End
    do SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
  data
loc:rowclick  string(1024)
  code
    loc:field = p_web.AddBrowseValue('BrowseJobsInBatch','MULDESPJ',mulj:RecordNumberKey) !mulj:RecordNumber
    p_web._thisrow = p_web._nocolon('mulj:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if Loc:LocatorValue <> '' and loc:ActualSelection = ''
        loc:checked = 'checked'
        do SetSelection
      elsif loc:ActualSelection = '' and mulj:RecordNumber = p_web.GetValue('mulj:RecordNumber')
         loc:checked = 'checked'
         do SetSelection
      elsif loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseJobsInBatch:LookupField')) = mulj:RecordNumber and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      else
        loc:checked = Choose((mulj:RecordNumber = loc:selected) and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      end
      If(loc:SelectionMethod  = Net:Radio)
        loc:RowStyle = 'class="' & p_web.combine(p_web.site.style.browserow,) &'"'
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'data-nt-id="'& p_web.AddBrowseValue('BrowseJobsInBatch','MULDESPJ',mulj:RecordNumberKey) &'" onclick="'
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If MULDESPJ{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(MULDESPJ)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            loc:DefaultSelection = loc:field
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If MULDESPJ{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(MULDESPJ)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            loc:DefaultSelection = loc:field
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'>'&p_web.CreateInput('radio','_bidv_',clip(loc:field),,loc:checked,'')&'</td>'&CRLF
          If loc:DefaultSelection = ''
            loc:DefaultSelection = loc:field
          End
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          If loc:DefaultSelection = '' or loc:direction < 0
            loc:DefaultSelection = loc:field
          End
        End
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::mulj:JobNumber
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        If Loc:Selecting = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::Delete
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        End     !Selecting
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr ' & clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Idx = p_web.AddBrowseValue('BrowseJobsInBatch','MULDESPJ',mulj:RecordNumberKey)
  TableQueue.Id[1] = mulj:RecordNumber

ClosingScripts  Routine
  data
Rtn_SecwinProcedureName         string(252)
  code
  If p_web.RequestAjax = 0
    do SetFormAction
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var btiBrowseJobsInBatch;if (btiBrowseJobsInBatch != 1){{var BrowseJobsInBatch=new browseTable(''BrowseJobsInBatch'','''&clip(loc:formname)&|
      ''','''&p_web._jsok('mulj:RecordNumber',Net:Parameter)&''',2,'''&clip(loc:divname)&''',1,1,1,'''&|
      clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&|
      p_web.Translate('Are you sure you want to delete this record?')&''','''&clip(loc:ActualSelection)&''','''&clip(loc:selectaction)&''','''&|
      clip(loc:formactiontarget)&''',''FormJobsInBatch'','&loc:popup&',0,'''','''',"' & clip(Rtn_SecwinProcedureName) & '");<13,10>'&|
      'BrowseJobsInBatch.setGreenBar('''&p_web.ColorWeb(p_web.Site.Style.BrowseHighlightColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOneColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseTwoColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOverColor)&''');<13,10>' &|
      'BrowseJobsInBatch.applyGreenBar();btiBrowseJobsInBatch=1};<13,10>' &|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseJobsInBatch')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseJobsInBatch')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseJobsInBatch')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseJobsInBatch')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(MULDESPJ)
  p_web._CloseFile(MULDESP)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(MULDESPJ)
  Bind(mulj:Record)
  Clear(mulj:Record)
  NetWebSetSessionPics(p_web,MULDESPJ)
  p_web._OpenFile(MULDESP)
  Bind(muld:Record)
  NetWebSetSessionPics(p_web,MULDESP)

Children Routine
  if loc:selecting = 0
    If p_web.RequestAjax = 0
      do StartChildren
    Else
      do AjaxChildren
    End
  end

AjaxChildren  Routine
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue('mulj:RecordNumber',p_web.GetValue('mulj:RecordNumber'))
   p_web.DeleteValue('_Clicked')
   loc:found = 1
   do Children
! ----------------------------------------------------------------------------------------
CallRow  Routine
  data
loc:result  long
  code
  do OpenFilesB
  mulj:RecordNumber = p_web.GSV('mulj:RecordNumber')
  loc:result = p_web._GetFile(MULDESPJ,mulj:RecordNumberKey)
  loc:eip = 1
  loc:viewstate = p_web.escape(p_web.Base64Encode(clip(mulj:RecordNumber)))
  do BrowseRow
  do SendPacket
  do ClosefilesB

! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(MULDESPJ)
  Case upper(p_web.GetValue('_EIPClm'))
  Else
    case p_web.GetValue('_action_')
    of Net:InsertRecord
    orof Net:ChangeRecord
    orof Net:DeleteRecord
    orof Net:CopyRecord
      loc:eip = 0
      do CallBrowse
    end
  End
  do GotFocusBack
  p_web._CloseFile(MULDESPJ)
! ----------------------------------------------------------------------------------------
value::mulj:JobNumber   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseJobsInBatch_mulj:JobNumber_'&mulj:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(mulj:JobNumber,'@s8')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::Delete   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseJobsInBatch_Delete_'&mulj:RecordNumber,,net:crc,,loc:extra)
          If loc:viewonly = 0
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallDeleteButton,'BrowseJobsInBatch',p_web.AddBrowseValue('BrowseJobsInBatch','MULDESPJ',mulj:RecordNumberKey),,loc:FormPopup,'FormJobsInBatch') & '<13,10>'
          End
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  p_web._OpenFile(MULDESP)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(MULDESP)
     FilesOpened = False
  END
  return
!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
CheckForDuplicate  Routine
SetSelection  Routine
  loc:ActualSelection = loc:field
  p_web.SetSessionValue('mulj:RecordNumber',mulj:RecordNumber)

MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:ActualSelection = ''
    loc:ActualSelection = loc:DefaultSelection
    p_web.GetBrowseValue(loc:ActualSelection,Net:Web:SessionQueue) ! so children are primed with correct sessionValue
  End

  If loc:ActualSelection <> ''
    TableQueue.Kind = Net:RowTable
    get(TableQueue,TableQueue.Kind)
    if Errorcode() = 0
      TableQueue.Row = sub(TableQueue.Row,1,len(clip(TableQueue.Row))-1) &  ' data-nt-rows="'&loc:RowsHigh&'" data-nt-value="'&clip(loc:ActualSelection)&'"' & '>'
      Put(TableQueue)
    End
  End

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead class="'&p_web.combine(p_web.site.style.BrowseHeader,'')&'"><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot class="'&p_web.combine(p_web.site.style.BrowseFooter,'BrowseFooter')&'"><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody class="'&p_web.combine(p_web.site.style.BrowseBody,)&'"><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table><13,10>' & p_web.DivFooter(Net:NoSend) ! Table Div
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
