

   MEMBER('WebServer_Phase4.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSE026.INC'),ONCE        !Local module procedure declarations
                     END


PushPack             PROCEDURE  (NetWebServerWorker p_web, string p_packet) ! Declare Procedure
packetlen   long
  CODE
  packetlen = len(clip(p_packet))
  if packetlen > 0
    p_web.ParseHTML(p_packet, 1, packetlen, NET:NoHeader)
    p_packet = ''
    packetlen = 0
  end
RefreshPage          PROCEDURE  (NetWebServerWorker p_web)
loc:x          Long
packet         String(NET:MaxBinData)
packetlen      Long
CRLF           String('<13,10>')
NBSP           String('&#160;')
loc:options    String(OptionsStringLen)  ! options for jQuery calls
Loc:User            long
ThisSecwinAccess    string(252)

  CODE
  GlobalErrors.SetProcedureName('RefreshPage')
  p_web.SetValue('_parentPage','RefreshPage')
  p_web.publicpage = 1
  if p_web.sessionId = 0 then p_web.NewSession().
  do Header
  packet = clip(packet) & p_web.BodyOnLoad(p_web.Combine(p_web.site.bodyclass,'PageBody'),,p_web.Combine(p_web.site.bodydivclass,'PageBodyDiv'))
  do Footer
  packet = clip(packet) & p_web.Popup()
  do SendPacket
  GlobalErrors.SetProcedureName()
  Return

SendPacket  Routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet,1,packetlen,Net:NoHeader)
    packet = ''
  end
Header Routine
  packet = p_web.w3Header(p_web.Combine(p_web.site.HtmlClass,))
  packet = clip(packet) & '<head>'&|
      '<title>'&p_web.Translate(p_web.site.PageTitle)&'</title>'&|
      '<meta http-equiv="Content-Type" content="text/html; charset='&clip(p_web.site.HtmlCharset)&'" /><13,10>' &|
      clip(p_web.MetaHeaders)
  packet =  clip(packet) & p_web.IncludeStyles()
  packet =  clip(packet) & p_web.IncludeScripts()
  packet = clip(packet) & '</head><13,10>'
  p_web.ParseHTML(packet,1,0,Net:SendHeader+Net:DontCache)
  packet = ''
Footer Routine
  packet = clip(packet) & '<!-- Net:SelectField --><13,10>' &|
                         '</div></body><13,10></html><13,10>'
RunDOS               PROCEDURE  (string aCmd, byte aWait)  ! Declare Procedure
! dwCreationFlag values
NORMAL_PRIORITY_CLASS             equate(00000020h)
CREATE_NO_WINDOW                  equate(08000000h)

STILL_ACTIVE          equate(259)

ProcessExitCode       ulong
CommandLine           cstring(1024)

StartUpInfo           group
Cb                      ulong
lpReserved              ulong
lpDesktop               ulong
lpTitle                 ulong
dwX                     ulong
dwY                     ulong
dwXSize                 ulong
dwYSize                 ulong
dwXCountChars           ulong
dwYCountChars           ulong
dwFillAttribute         ulong
dwFlags                 ulong
wShowWindow             signed
cbReserved2             signed
lpReserved2             ulong
hStdInput               unsigned
hStdOutput              unsigned
hStdError               unsigned
                      end

ProcessInformation    group
hProcess                unsigned
hThread                 unsigned
dwProcessId             ulong
dwThreadId              ulong
                      end
  CODE
! Run DOS Program

    CommandLine = clip(aCmd)
    StartUpInfo.Cb = size(StartUpInfo)

    ReturnCode# = CreateProcess(0,                                             |
                                address(CommandLine),                          |
                                0,                                             |
                                0,                                             |
                                0,                                             |
                                BOR(NORMAL_PRIORITY_CLASS, CREATE_NO_WINDOW),  |
                                0,                                             |
                                0,                                             |
                                address(StartUpInfo),                          |
                                address(ProcessInformation))

    if ReturnCode# = 0  then
        ! Error Return
        return false
    end

    timeout# = GETINI('MQ','TimeOut',,Clip(Path()) & '\SB2KDEF.INI')
    if (timeout# = 0)
        timeout# = 20
    end ! if (timeout# = 0)
    timeout# = clock() + (timeout# * 100)

    if aWait then
        setcursor(cursor:wait)
        loop
            Sleep(100)
            ! check for when process is finished
            ReturnCode# = GetExitCodeProcess(ProcessInformation.hProcess, address(ProcessExitCode))
        while ProcessExitCode = STILL_ACTIVE and clock() < timeout#
        setcursor
    end

    return(true)
ForcePostcode        PROCEDURE  (func:Type)                ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
  CODE
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    !Postcode
    If (def:ForcePostcode = 'B' and func:Type = 'B') Or |
        (def:ForcePostcode <> 'I' and func:Type = 'C')
        Return Level:Fatal
    End!If def:ForcePostcode = 'B''
    Return Level:Benign
CustomerNameRequired PROCEDURE  (func:AccountNumber)       ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
TRADEACC::State  USHORT
SUBTRACC::State  USHORT
FilesOpened     BYTE(0)
  CODE
    Do OpenFiles
    Do SaveFiles
    Return# = 0
    !Check the trade account details to see if a customer name is required
    Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
    sub:Account_Number = func:AccountNumber
    If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = sub:Main_Account_Number
        If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
            If tra:Invoice_Sub_Accounts = 'YES'
                If sub:ForceEndUserName
                    Return# = 1
                End !If sub:ForceEndUserName
            Else !If tra:Invoice_Sub_Accounts = 'YES'
                If tra:Use_Contact_Name = 'YES'
                    Return# = 1
                End!If tra:Use_Contact_Name = 'YES'

            End !If tra:Invoice_Sub_Accounts = 'YES'
        End!If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
    End!If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign

    Do RestoreFiles
    Do CloseFiles

    Return Return#

!--------------------------------------
OpenFiles  ROUTINE
  Access:TRADEACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBTRACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBTRACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:TRADEACC.Close
     Access:SUBTRACC.Close
     FilesOpened = False
  END
SaveFiles  ROUTINE
  TRADEACC::State = Access:TRADEACC.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  SUBTRACC::State = Access:SUBTRACC.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF TRADEACC::State <> 0
    Access:TRADEACC.RestoreFile(TRADEACC::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF SUBTRACC::State <> 0
    Access:SUBTRACC.RestoreFile(SUBTRACC::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
