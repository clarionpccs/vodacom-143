

   MEMBER('WebServer_Phase4.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSE081.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSE087.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE088.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE089.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE090.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE091.INC'),ONCE        !Req'd for module callout resolution
                     END


frmPendingWebOrder   PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
FilesOpened     Long
TagFile::State  USHORT
brwPendingWebOrder:IsInvalid  Long
btnMakeTaggedOrders:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('frmPendingWebOrder')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'frmPendingWebOrder_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('frmPendingWebOrder','')
    p_web.DivHeader('frmPendingWebOrder',p_web.combine(p_web.site.style.formdiv,))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('frmPendingWebOrder') = 0
        p_web.AddPreCall('frmPendingWebOrder')
        p_web.DivHeader('popup_frmPendingWebOrder','nt-hidden')
        p_web.DivHeader('frmPendingWebOrder',p_web.combine(p_web.site.style.formdiv,))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_frmPendingWebOrder_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_frmPendingWebOrder_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferfrmPendingWebOrder',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_frmPendingWebOrder',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmPendingWebOrder',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_frmPendingWebOrder',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmPendingWebOrder',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_frmPendingWebOrder',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_frmPendingWebOrder',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmPendingWebOrder',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_frmPendingWebOrder',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmPendingWebOrder',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_frmPendingWebOrder',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_frmPendingWebOrder',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('frmPendingWebOrder')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(TagFile)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(TagFile)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE
  loc:EipClm = upper(p_web.GetValue('_EIPClm'))
  p_web.DeleteValue('_EIPClm')
  case loc:EipClm
  of ''
    case upper(p_web.GetValue('_calledfrom_'))
    of upper('brwPendingWebOrder')
      do Value::brwPendingWebOrder
    end
  end

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('frmPendingWebOrder_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'frmPendingWebOrder'
    end
    p_web.formsettings.proc = 'frmPendingWebOrder'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('frmPendingWebOrder_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'FormBrowseStock'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('frmPendingWebOrder_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('frmPendingWebOrder_ChainTo')
    loc:formaction = p_web.GetSessionValue('frmPendingWebOrder_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction

GenerateForm   Routine
  do LoadRelatedRecords
      p_web.site.SaveButton.TextValue = 'Close'
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Browse Pending Web Orders') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Browse Pending Web Orders',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_frmPendingWebOrder',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_frmPendingWebOrder0_div')&'">'&p_web.Translate()&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="frmPendingWebOrder_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('frmPendingWebOrder_brwPendingWebOrder_embedded_div')&''');'
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="frmPendingWebOrder_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'frmPendingWebOrder_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="frmPendingWebOrder_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('frmPendingWebOrder_brwPendingWebOrder_embedded_div')&''');'
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'frmPendingWebOrder_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_frmPendingWebOrder')>0,p_web.GSV('showtab_frmPendingWebOrder'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_frmPendingWebOrder'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_frmPendingWebOrder') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_frmPendingWebOrder'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_frmPendingWebOrder')>0,p_web.GSV('showtab_frmPendingWebOrder'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_frmPendingWebOrder') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_frmPendingWebOrder_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_frmPendingWebOrder')>0,p_web.GSV('showtab_frmPendingWebOrder'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"frmPendingWebOrder",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_frmPendingWebOrder')>0,p_web.GSV('showtab_frmPendingWebOrder'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_frmPendingWebOrder_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('frmPendingWebOrder') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('frmPendingWebOrder')
    p_web.SetValue('_CallPopups',2)
    If p_web.GetPreCall('brwPendingWebOrder') = 0
      p_web.SetValue('brwPendingWebOrder:NoForm',1)
      p_web.SetValue('brwPendingWebOrder:FormName',loc:formname)
      p_web.SetValue('brwPendingWebOrder:parentIs','Form')
      p_web.SetValue('_parentProc','frmPendingWebOrder')
      brwPendingWebOrder(p_web)
      p_web.SetValue('_CallPopups',0)
      p_web.DeleteValue('brwPendingWebOrder:NoForm')
      p_web.DeleteValue('brwPendingWebOrder:FormName')
      p_web.DeleteValue('brwPendingWebOrder:parentIs')
      p_web.DeleteValue('_parentProc')
    End
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate()&'</a></h3>' & CRLF & p_web.DivHeader('tab_frmPendingWebOrder0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_frmPendingWebOrder0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_frmPendingWebOrder0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_frmPendingWebOrder0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,)
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_frmPendingWebOrder0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend)
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_frmPendingWebOrder0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain">' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_frmPendingWebOrder0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::brwPendingWebOrder
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::btnMakeTaggedOrders
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::btnMakeTaggedOrders
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Validate::brwPendingWebOrder  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  Elsif true
    p_web.StoreValue('orw:RecordNumber')
  End
  do ValidateValue::brwPendingWebOrder  ! copies value to session value if valid.
  do Comment::brwPendingWebOrder ! allows comment style to be updated.
  do Value::btnMakeTaggedOrders  !1

ValidateValue::brwPendingWebOrder  Routine
    If not (1=0)
    End

Value::brwPendingWebOrder  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(1=0,1,0))
  ! --- BROWSE ---  brwPendingWebOrder --
  p_web.SetValue('brwPendingWebOrder:NoForm',1)
  p_web.SetValue('brwPendingWebOrder:FormName',loc:formname)
  p_web.SetValue('brwPendingWebOrder:parentIs','Form')
  p_web.SetValue('_parentProc','frmPendingWebOrder')
  if p_web.RequestAjax = 0
    p_web.SSV('frmPendingWebOrder:_popup_',p_web.GetValue('_popup_')) ! stores the current browse popup state
    packet = clip(packet) & '<div id="'&lower('frmPendingWebOrder_brwPendingWebOrder_embedded_div')&'"><!-- Net:brwPendingWebOrder --></div><13,10>'
    do SendPacket
    p_web.DivHeader('frmPendingWebOrder_' & lower('brwPendingWebOrder') & '_value')
    p_web.DivFooter()
  else
    if p_web.GetValue('_popup_') = 1
      p_web.SSV('frmPendingWebOrder:_popup_',1)
    elsif p_web.GSV('frmPendingWebOrder:_popup_') = 1
      p_web.SetValue('_popup_',1)
    end
    packet = clip(packet) & '<!-- Net:brwPendingWebOrder --><13,10>'
  end
  do SendPacket
Comment::brwPendingWebOrder  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if brwPendingWebOrder:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('frmPendingWebOrder_' & p_web._nocolon('brwPendingWebOrder') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::btnMakeTaggedOrders  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::btnMakeTaggedOrders  ! copies value to session value if valid.
  do Comment::btnMakeTaggedOrders ! allows comment style to be updated.

ValidateValue::btnMakeTaggedOrders  Routine
    If not (1=0)
    End

Value::btnMakeTaggedOrders  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('frmPendingWebOrder_' & p_web._nocolon('btnMakeTaggedOrders') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','btnMakeTaggedOrders','Make Tagged Orders',p_web.combine(Choose('Make Tagged Orders' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.WindowOpen('frmMakeTaggedOrders'&'','_self'),,loc:disabled,,,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::btnMakeTaggedOrders  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if btnMakeTaggedOrders:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('frmPendingWebOrder_' & p_web._nocolon('btnMakeTaggedOrders') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('frmPendingWebOrder_nexttab_' & 0)
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_frmPendingWebOrder_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('frmPendingWebOrder_tab_' & 0)
    do GenerateTab0
  of lower('frmPendingWebOrder_brwPendingWebOrder_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::brwPendingWebOrder
      of event:timer
        do Value::brwPendingWebOrder
        do Comment::brwPendingWebOrder
      else
        do Value::brwPendingWebOrder
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('frmPendingWebOrder_form:ready_',1)

  p_web.SetSessionValue('frmPendingWebOrder_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_frmPendingWebOrder',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('frmPendingWebOrder_form:ready_',1)
  p_web.SetSessionValue('frmPendingWebOrder_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_frmPendingWebOrder',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('frmPendingWebOrder_form:ready_',1)
  p_web.SetSessionValue('frmPendingWebOrder_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('frmPendingWebOrder:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('frmPendingWebOrder_form:ready_',1)
  p_web.SetSessionValue('frmPendingWebOrder_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('frmPendingWebOrder:Primed',0)
  p_web.setsessionvalue('showtab_frmPendingWebOrder',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('frmPendingWebOrder_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('frmPendingWebOrder_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
    do ValidateValue::brwPendingWebOrder
    If loc:Invalid then exit.
    do ValidateValue::btnMakeTaggedOrders
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('frmPendingWebOrder:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('')

frmStockReceive      PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locInvoiceNumber     STRING(30)                            !
FilesOpened     Long
STOCKRECEIVETMP::State  USHORT
SUBTRACC::State  USHORT
TRADEACC::State  USHORT
RETSALES::State  USHORT
locInvoiceNumber:IsInvalid  Long
btnRefresh:IsInvalid  Long
brwStockReceive:IsInvalid  Long
btnProcessTaggedItems:IsInvalid  Long
btnProcessExchangeItems:IsInvalid  Long
btnProcessLoanItems:IsInvalid  Long
btnPrintGRN:IsInvalid  Long
btnReprintGRN:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('frmStockReceive')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'frmStockReceive_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('frmStockReceive','')
    p_web.DivHeader('frmStockReceive',p_web.combine(p_web.site.style.formdiv,))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('frmStockReceive') = 0
        p_web.AddPreCall('frmStockReceive')
        p_web.DivHeader('popup_frmStockReceive','nt-hidden')
        p_web.DivHeader('frmStockReceive',p_web.combine(p_web.site.style.formdiv,))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_frmStockReceive_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_frmStockReceive_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferfrmStockReceive',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_frmStockReceive',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmStockReceive',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_frmStockReceive',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmStockReceive',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_frmStockReceive',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_frmStockReceive',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmStockReceive',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_frmStockReceive',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmStockReceive',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_frmStockReceive',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_frmStockReceive',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('frmStockReceive')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
ClearList           ROUTINE
    ClearStockReceiveList(p_web)
BuildList           ROUTINE
    BuildStockReceiveList(p_web,p_web.GSV('ret:Ref_Number'),p_web.GSV('ret:ExchangeOrder'),p_web.GSV('ret:LoanOrder'))
OpenFiles  ROUTINE
  p_web._OpenFile(STOCKRECEIVETMP)
  p_web._OpenFile(SUBTRACC)
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(RETSALES)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(STOCKRECEIVETMP)
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(RETSALES)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE
  loc:EipClm = upper(p_web.GetValue('_EIPClm'))
  p_web.DeleteValue('_EIPClm')
  case loc:EipClm
  of ''
    case upper(p_web.GetValue('_calledfrom_'))
    of upper('brwStockReceive')
      do Value::brwStockReceive
    end
  end

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('frmStockReceive_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'frmStockReceive'
    end
    p_web.formsettings.proc = 'frmStockReceive'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  if p_web.IfExistsValue('locInvoiceNumber') = 0
    p_web.SetSessionValue('locInvoiceNumber',locInvoiceNumber)
  Else
    locInvoiceNumber = p_web.GetSessionValue('locInvoiceNumber')
  End

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locInvoiceNumber')
    locInvoiceNumber = p_web.GetValue('locInvoiceNumber')
    p_web.SetSessionValue('locInvoiceNumber',locInvoiceNumber)
  Else
    locInvoiceNumber = p_web.GetSessionValue('locInvoiceNumber')
  End

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('frmStockReceive_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'FormBrowseStock'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('frmStockReceive_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('frmStockReceive_ChainTo')
    loc:formaction = p_web.GetSessionValue('frmStockReceive_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction

GenerateForm   Routine
  do LoadRelatedRecords
      p_web.site.SaveButton.TextValue = 'Close'
 locInvoiceNumber = p_web.RestoreValue('locInvoiceNumber')
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Stock Receive Procedure') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Stock Receive Procedure',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_frmStockReceive',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_frmStockReceive0_div')&'">'&p_web.Translate('Enter Invoice Number')&'</a></li>'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_frmStockReceive1_div')&'">'&p_web.Translate('Stock On Invoice')&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  do GenerateTab1
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="frmStockReceive_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('frmStockReceive_brwStockReceive_embedded_div')&''');'
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="frmStockReceive_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'frmStockReceive_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="frmStockReceive_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('frmStockReceive_brwStockReceive_embedded_div')&''');'
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'frmStockReceive_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locInvoiceNumber')
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_frmStockReceive')>0,p_web.GSV('showtab_frmStockReceive'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_frmStockReceive'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_frmStockReceive') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_frmStockReceive'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_frmStockReceive')>0,p_web.GSV('showtab_frmStockReceive'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_frmStockReceive') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Enter Invoice Number') & ''''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Stock On Invoice') & ''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_frmStockReceive_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_frmStockReceive')>0,p_web.GSV('showtab_frmStockReceive'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"frmStockReceive",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_frmStockReceive')>0,p_web.GSV('showtab_frmStockReceive'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_frmStockReceive_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('frmStockReceive') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('frmStockReceive')
    p_web.SetValue('_CallPopups',2)
    If p_web.GetPreCall('brwStockReceive') = 0
      p_web.SetValue('brwStockReceive:NoForm',1)
      p_web.SetValue('brwStockReceive:FormName',loc:formname)
      p_web.SetValue('brwStockReceive:parentIs','Form')
      p_web.SetValue('_parentProc','frmStockReceive')
      brwStockReceive(p_web)
      p_web.SetValue('_CallPopups',0)
      p_web.DeleteValue('brwStockReceive:NoForm')
      p_web.DeleteValue('brwStockReceive:FormName')
      p_web.DeleteValue('brwStockReceive:parentIs')
      p_web.DeleteValue('_parentProc')
    End
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Enter Invoice Number')&'</a></h3>' & CRLF & p_web.DivHeader('tab_frmStockReceive0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_frmStockReceive0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_frmStockReceive0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_frmStockReceive0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Enter Invoice Number')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_frmStockReceive0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Enter Invoice Number')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_frmStockReceive0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Enter Invoice Number')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_frmStockReceive0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locInvoiceNumber
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locInvoiceNumber
        do Comment::locInvoiceNumber
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::btnRefresh
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::btnRefresh
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab1  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Stock On Invoice')&'</a></h3>' & CRLF & p_web.DivHeader('tab_frmStockReceive1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_frmStockReceive1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_frmStockReceive1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_frmStockReceive1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Stock On Invoice')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_frmStockReceive1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Stock On Invoice')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_frmStockReceive1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Stock On Invoice')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_frmStockReceive1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::brwStockReceive
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::btnProcessTaggedItems
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::btnProcessExchangeItems
        do Comment::btnProcessExchangeItems
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::btnProcessLoanItems
        do Comment::btnProcessLoanItems
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::btnPrintGRN
        do Comment::btnPrintGRN
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::btnReprintGRN
        do Comment::btnReprintGRN
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Prompt::locInvoiceNumber  Routine
  packet = clip(packet) & p_web.DivHeader('frmStockReceive_' & p_web._nocolon('locInvoiceNumber') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Invoice Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locInvoiceNumber  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locInvoiceNumber = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locInvoiceNumber = p_web.GetValue('Value')
  End
  do ValidateValue::locInvoiceNumber  ! copies value to session value if valid.
  DO ClearList
  p_web.SSV('Comment:InvoiceNumber','')
  Access:RETSALES.ClearKey(ret:Invoice_Number_Key)
  ret:Invoice_Number = p_web.GSV('locInvoiceNumber')
  If Access:RETSALES.TryFetch(ret:Invoice_Number_Key) = Level:Benign
      !Found
      !Does the account number of the sale match either the account number
      !of the current user, or it's Stores Account?
  
      IF (ret:Account_Number <> p_web.GSV('BookingStoresAccount'))
          p_web.SSV('Comment:InvoiceNumber','Invoice Is Not For Your Site')
      ELSE
          p_web.FileToSessionQueue(RETSALES)
          DO BuildList
      END
  Else!If Access:RETSALES.TryFetch(ret:Invoice_Number_Key) = Level:Benign
      !Error
      p_web.SSV('Comment:InvoiceNumber','Invalid Invoice Number')
  End!If Access:RETSALES.TryFetch(ret:Invoice_Number_Key) = Level:Benign
  do Value::locInvoiceNumber
  do SendAlert
  do Comment::locInvoiceNumber ! allows comment style to be updated.
  do Value::brwStockReceive  !1
  do Value::btnPrintGRN  !1
  do Value::btnProcessExchangeItems  !1
  do Value::btnProcessTaggedItems  !1
  do Value::btnProcessLoanItems  !1

ValidateValue::locInvoiceNumber  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locInvoiceNumber',locInvoiceNumber).
    End

Value::locInvoiceNumber  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('frmStockReceive_' & p_web._nocolon('locInvoiceNumber') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    locInvoiceNumber = p_web.RestoreValue('locInvoiceNumber')
    do ValidateValue::locInvoiceNumber
    If locInvoiceNumber:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- locInvoiceNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locInvoiceNumber'',''frmstockreceive_locinvoicenumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locInvoiceNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locInvoiceNumber',p_web.GetSessionValueFormat('locInvoiceNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locInvoiceNumber  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locInvoiceNumber:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:InvoiceNumber'))
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('frmStockReceive_' & p_web._nocolon('locInvoiceNumber') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::btnRefresh  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::btnRefresh  ! copies value to session value if valid.
  DO ClearList
  p_web.SSV('Comment:InvoiceNumber','')
  Access:RETSALES.ClearKey(ret:Invoice_Number_Key)
  ret:Invoice_Number = p_web.GSV('locInvoiceNumber')
  If Access:RETSALES.TryFetch(ret:Invoice_Number_Key) = Level:Benign
      !Found
      !Does the account number of the sale match either the account number
      !of the current user, or it's Stores Account?
  
      IF (ret:Account_Number <> p_web.GSV('BookingStoresAccount'))
          p_web.SSV('Comment:InvoiceNumber','Invoice Is Not For Your Site')
      ELSE
          p_web.FileToSessionQueue(RETSALES)
          DO BuildList
      END
  Else!If Access:RETSALES.TryFetch(ret:Invoice_Number_Key) = Level:Benign
      !Error
      p_web.SSV('Comment:InvoiceNumber','Invalid Invoice Number')
  End!If Access:RETSALES.TryFetch(ret:Invoice_Number_Key) = Level:Benign
  do Value::btnRefresh
  do Comment::btnRefresh ! allows comment style to be updated.
  do Value::brwStockReceive  !1
  do Value::btnPrintGRN  !1
  do Value::btnProcessExchangeItems  !1
  do Value::btnProcessTaggedItems  !1
  do Value::btnProcessLoanItems  !1

ValidateValue::btnRefresh  Routine
    If not (1=0)
    End

Value::btnRefresh  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('frmStockReceive_' & p_web._nocolon('btnRefresh') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnRefresh'',''frmstockreceive_btnrefresh_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','btnRefresh','Refresh',p_web.combine(Choose('Refresh' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'SmallButton'),loc:formname,,,,loc:javascript,loc:disabled,,,,,,0,,,,'nt-left')!h
  do SendPacket
  End
  p_web.DivFooter()
Comment::btnRefresh  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if btnRefresh:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('frmStockReceive_' & p_web._nocolon('btnRefresh') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::brwStockReceive  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  Elsif true
    p_web.StoreValue('stotmp:RecordNumber')
  End
  do ValidateValue::brwStockReceive  ! copies value to session value if valid.
  do Comment::brwStockReceive ! allows comment style to be updated.

ValidateValue::brwStockReceive  Routine
    If not (1=0)
    End

Value::brwStockReceive  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(1=0,1,0))
  ! --- BROWSE ---  brwStockReceive --
  p_web.SetValue('brwStockReceive:NoForm',1)
  p_web.SetValue('brwStockReceive:FormName',loc:formname)
  p_web.SetValue('brwStockReceive:parentIs','Form')
  p_web.SetValue('_parentProc','frmStockReceive')
  if p_web.RequestAjax = 0
    p_web.SSV('frmStockReceive:_popup_',p_web.GetValue('_popup_')) ! stores the current browse popup state
    packet = clip(packet) & '<div id="'&lower('frmStockReceive_brwStockReceive_embedded_div')&'"><!-- Net:brwStockReceive --></div><13,10>'
    do SendPacket
    p_web.DivHeader('frmStockReceive_' & lower('brwStockReceive') & '_value')
    p_web.DivFooter()
  else
    if p_web.GetValue('_popup_') = 1
      p_web.SSV('frmStockReceive:_popup_',1)
    elsif p_web.GSV('frmStockReceive:_popup_') = 1
      p_web.SetValue('_popup_',1)
    end
    packet = clip(packet) & '<!-- Net:brwStockReceive --><13,10>'
  end
  do SendPacket
Comment::brwStockReceive  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if brwStockReceive:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('frmStockReceive_' & p_web._nocolon('brwStockReceive') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::btnProcessTaggedItems  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::btnProcessTaggedItems  ! copies value to session value if valid.

ValidateValue::btnProcessTaggedItems  Routine
    If not (p_web.GSV('locINvoiceNumber') = '')
    End

Value::btnProcessTaggedItems  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('locINvoiceNumber') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('frmStockReceive_' & p_web._nocolon('btnProcessTaggedItems') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('locINvoiceNumber') = '')
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','btnProcessTaggedItems','Process Tagged Stock Items',p_web.combine(Choose('Process Tagged Stock Items' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.WindowOpen('frmStockReceiveProcess'&'','_self'),,loc:disabled,,,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::btnProcessTaggedItems  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if btnProcessTaggedItems:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('locINvoiceNumber') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('frmStockReceive_' & p_web._nocolon('btnProcessTaggedItems') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('locINvoiceNumber') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::btnProcessExchangeItems  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::btnProcessExchangeItems  ! copies value to session value if valid.
  do Comment::btnProcessExchangeItems ! allows comment style to be updated.

ValidateValue::btnProcessExchangeItems  Routine
    If not (p_web.GSV('locINvoiceNumber') = '')
    End

Value::btnProcessExchangeItems  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('locINvoiceNumber') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('frmStockReceive_' & p_web._nocolon('btnProcessExchangeItems') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('locINvoiceNumber') = '')
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','btnProcessExchangeItems','Process Tagged Exchange Items',p_web.combine(Choose('Process Tagged Exchange Items' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.WindowOpen('frmExchangeReceiveProcess&' & p_web._jsok('fLoan=0') &''&'','_self'),,loc:disabled,,,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::btnProcessExchangeItems  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if btnProcessExchangeItems:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('locINvoiceNumber') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('frmStockReceive_' & p_web._nocolon('btnProcessExchangeItems') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('locINvoiceNumber') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::btnProcessLoanItems  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::btnProcessLoanItems  ! copies value to session value if valid.
  do Comment::btnProcessLoanItems ! allows comment style to be updated.

ValidateValue::btnProcessLoanItems  Routine
    If not (p_web.GSV('locINvoiceNumber') = '')
    End

Value::btnProcessLoanItems  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('locINvoiceNumber') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('frmStockReceive_' & p_web._nocolon('btnProcessLoanItems') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('locINvoiceNumber') = '')
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','btnProcessLoanItems','Process Loan Items',p_web.combine(Choose('Process Loan Items' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.WindowOpen('frmExchangeReceiveProcess&' & p_web._jsok('fLoan=1') &''&'','_self'),,loc:disabled,,,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::btnProcessLoanItems  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if btnProcessLoanItems:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('locINvoiceNumber') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('frmStockReceive_' & p_web._nocolon('btnProcessLoanItems') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('locINvoiceNumber') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::btnPrintGRN  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::btnPrintGRN  ! copies value to session value if valid.
  do Comment::btnPrintGRN ! allows comment style to be updated.

ValidateValue::btnPrintGRN  Routine
    If not (p_web.GSV('locINvoiceNumber') = '')
    End

Value::btnPrintGRN  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('locINvoiceNumber') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('frmStockReceive_' & p_web._nocolon('btnPrintGRN') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('locINvoiceNumber') = '')
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','btnPrintGRN','Print GRN',p_web.combine(Choose('Print GRN' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.WindowOpen('frmPrintGRN'&'','_self'),,loc:disabled,,,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::btnPrintGRN  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if btnPrintGRN:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('locINvoiceNumber') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('frmStockReceive_' & p_web._nocolon('btnPrintGRN') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('locINvoiceNumber') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::btnReprintGRN  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::btnReprintGRN  ! copies value to session value if valid.
  do Comment::btnReprintGRN ! allows comment style to be updated.

ValidateValue::btnReprintGRN  Routine
    If not (1=0)
    End

Value::btnReprintGRN  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('frmStockReceive_' & p_web._nocolon('btnReprintGRN') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','btnReprintGRN','Reprint GRN',p_web.combine(Choose('Reprint GRN' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.WindowOpen('frmPrintAllGRN'&'','_self'),,loc:disabled,,,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::btnReprintGRN  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if btnReprintGRN:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('frmStockReceive_' & p_web._nocolon('btnReprintGRN') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('frmStockReceive_nexttab_' & 0)
    locInvoiceNumber = p_web.GSV('locInvoiceNumber')
    do ValidateValue::locInvoiceNumber
    If loc:Invalid
      loc:retrying = 1
      do Value::locInvoiceNumber
      !do SendAlert
      do Comment::locInvoiceNumber ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('frmStockReceive_nexttab_' & 1)
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_frmStockReceive_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('frmStockReceive_tab_' & 0)
    do GenerateTab0
  of lower('frmStockReceive_locInvoiceNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locInvoiceNumber
      of event:timer
        do Value::locInvoiceNumber
        do Comment::locInvoiceNumber
      else
        do Value::locInvoiceNumber
      end
  of lower('frmStockReceive_btnRefresh_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnRefresh
      of event:timer
        do Value::btnRefresh
        do Comment::btnRefresh
      else
        do Value::btnRefresh
      end
  of lower('frmStockReceive_tab_' & 1)
    do GenerateTab1
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('frmStockReceive_form:ready_',1)

  p_web.SetSessionValue('frmStockReceive_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_frmStockReceive',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('frmStockReceive_form:ready_',1)
  p_web.SetSessionValue('frmStockReceive_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_frmStockReceive',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('frmStockReceive_form:ready_',1)
  p_web.SetSessionValue('frmStockReceive_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('frmStockReceive:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('frmStockReceive_form:ready_',1)
  p_web.SetSessionValue('frmStockReceive_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('frmStockReceive:Primed',0)
  p_web.setsessionvalue('showtab_frmStockReceive',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
      If not (1=0)
          If p_web.IfExistsValue('locInvoiceNumber')
            locInvoiceNumber = p_web.GetValue('locInvoiceNumber')
          End
      End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('frmStockReceive_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('frmStockReceive_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
    do ValidateValue::locInvoiceNumber
    If loc:Invalid then exit.
    do ValidateValue::btnRefresh
    If loc:Invalid then exit.
  ! tab = 2
    loc:InvalidTab += 1
    do ValidateValue::brwStockReceive
    If loc:Invalid then exit.
    do ValidateValue::btnProcessTaggedItems
    If loc:Invalid then exit.
    do ValidateValue::btnProcessExchangeItems
    If loc:Invalid then exit.
    do ValidateValue::btnProcessLoanItems
    If loc:Invalid then exit.
    do ValidateValue::btnPrintGRN
    If loc:Invalid then exit.
    do ValidateValue::btnReprintGRN
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('frmStockReceive:Primed',0)
  p_web.StoreValue('locInvoiceNumber')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')

PageProcess          PROCEDURE  (NetWebServerWorker p_web)
loc:x          Long
packet         String(NET:MaxBinData)
packetlen      Long
CRLF           String('<13,10>')
NBSP           String('&#160;')
loc:options    String(OptionsStringLen)  ! options for jQuery calls
Loc:User            long
ThisSecwinAccess    string(252)

  CODE
  GlobalErrors.SetProcedureName('PageProcess')
  p_web.SetValue('_parentPage','PageProcess')
  p_web.publicpage = 1
  if p_web.sessionId = 0 then p_web.NewSession().
        p_web.StoreValue('ProcessType')
        p_web.StoreValue('ReturnURL')
        p_web.StoreValue('RedirectURL')
  do Header
  packet = clip(packet) & p_web.BodyOnLoad(p_web.Combine(p_web.site.bodyclass,),,p_web.Combine(p_web.site.bodydivclass,))
    do SendPacket
    Do heading
    do SendPacket
    Do ShowWait
    do SendPacket
    Do CloseWait
    do SendPacket
  do Footer
  packet = clip(packet) & p_web.Popup()
  do SendPacket
  GlobalErrors.SetProcedureName()
  Return

BuildOutstandingOrders      ROUTINE
DATA
locAccountNumber    STRING(30)
bt  LONG()
locRecordcount      LONG()
locTotalRecords LONG()
kReturnURL  EQUATE('FormBrowseStock')
CODE
    p_web.SSV('locOrderType','S')
    p_web.SSV('RedirectURL','FormBrowseOutstandingParts')

    STREAM(SBO_OutParts)
    STREAM(ORDHEAD)
    STREAM(ORDITEMS)
    STREAM(RETSTOCK)
    STREAM(RETSALES)
    STREAM(EXCHORDR)
    STREAM(LOAORDR)

    bt=clock()

    UpdateProgress(p_web,0,kReturnURL)

    locTotalRecords = RECORDS(SBO_OutParts)

    ! Clear Existing Record
    Access:SBO_OutParts.ClearKey(sout:PartNumberKey)
    sout:SessionID = p_web.SessionID
    SET(sout:PartNumberKey,sout:PartNumberKey)
    LOOP UNTIL Access:SBO_OutParts.Next()
        IF (sout:SessionID <> p_web.SessionID)
            BREAK
        END

        locRecordcount += 1


        if (clock() - bt) > 200
            bt=clock()

            UpdateProgress(p_web,INT((locRecordcount / locTotalRecords) * 100),kReturnURL,'Preparing...')

        end

        Delete(SBO_OutParts)
    END

    locRecordcount = 0

    IF (p_web.GSV('BookingStoresAccount') = '')
        locAccountNumber = p_web.GSV('BookingAccount')
    ELSE
        locAccountNumber = p_web.GSV('BookingStoresAccount')
    END


    ! Loop Through Web Orders For This Account
    Access:ORDHEAD.Clearkey(orh:AccountDateKey)
    orh:Account_No = locAccountNumber
    Set(orh:AccountDateKey,orh:AccountDateKey)
    Loop
        If Access:ORDHEAD.Next()
            Break
        End ! If Access:ORDHEAD.Next()
        If orh:Account_No <> locAccountNumber
            Break
        End ! If orh:Account_No <> locAccountNumber

        locRecordcount += 1


        if (clock() - bt) > 200
            bt=clock()

            UpdateProgress(p_web,INT((locRecordcount / RECORDS(ORDHEAD)) * 100),kReturnURL,'Checking Part Orders')

        end


    ! Count unprocessed orders first (DBH: 06/12/2007)
        If orh:Procesed = 0
        ! Add up items on the unprocessed order (DBH: 06/12/2007)
            Access:ORDITEMS.Clearkey(ori:KeyOrdHNo)
            ori:OrdHNo = orh:Order_No
            Set(ori:KeyOrdHNo,ori:KeyOrdHNo)
            Loop
                If Access:ORDITEMS.Next()
                    Break
                End ! If Access:ORDITEMS.Next()
                If ori:OrdHNo <> orh:Order_No
                    Break
                End ! If ori:OrdHNo <> orh:Order_No
                If ori:Qty = 0
                    Cycle
                End ! If ori:Qty = 0

                Access:SBO_OutParts.ClearKey(sout:PartDescDateRaisedKey)
                sout:SessionID = p_web.SessionID
                sout:LineType = 'S'
                sout:PartNumber = ori:PartNo
                sout:Description = ori:partdiscription
                sout:DateRaised = orh:TheDate
                IF (Access:SBO_OutParts.TryFetch(sout:PartDescDateRaisedKey))
                    IF (Access:SBO_OutParts.PrimeRecord() = Level:Benign)
                        sout:SessionID = p_web.SessionID
                        sout:PartNumber = ori:PartNo
                        sout:Description = ori:partdiscription
                        sout:DateRaised = orh:TheDate
                        sout:Quantity = ori:Qty
                        sout:DateProcessed = 0
                        sout:LineType = 'S'
                        IF (Access:SBO_OutParts.TryInsert())
                        END

                    END

                ELSE
                    sout:Quantity += ori:Qty
                    Access:SBO_OutParts.TryUpdate()
                END
            End ! Loop

        Else ! If orh:Procesed = 0
        ! Find the retail sale associated with this web order (DBH: 06/12/2007)
            Access:RETSALES.Clearkey(ret:Ref_Number_Key)
            ret:Ref_Number = orh:SalesNumber
            If Access:RETSALES.TryFetch(ret:Ref_Number_Key) = Level:Benign
            ! Double check the the sale has the corresponding web order number (DBH: 06/12/2007)
                If ret:WebOrderNumber = 0
                    Cycle
                End ! If ret:WebOrderNumber = 0

            ! Count the items on the sale (DBH: 06/12/2007)
                Access:RETSTOCK.Clearkey(res:Part_Number_Key)
                res:Ref_Number = ret:Ref_Number
                Set(res:Part_Number_Key,res:Part_Number_Key)
                Loop
                    If Access:RETSTOCK.Next()
                        Break
                    End ! If Access:RETSTOCK.Next()
                    If res:Ref_Number <> ret:Ref_Number
                        Break
                    End ! If res:Ref_Number <> ret:Ref_Number
                ! Item should be on another sale, so don't count here. (DBH: 06/12/2007)
                    If res:Despatched = 'OLD'
                        Cycle
                    End ! If res:Despatched = 'OLD'

                    If res:Despatched = 'CAN'
                        CYCLE
                    END

                ! Count the part if it hasn't arrived yet (DBH: 06/12/2007)
                    Access:SBO_OutParts.ClearKey(sout:PartDescDateRaisedProcessedKey)
                    sout:SessionID = p_web.SessionID
                    sout:LineType = 'S'
                    sout:PartNumber = res:Part_Number
                    sout:Description = res:Description
                    sout:DateRaised = orh:TheDate
                    sout:DateProcessed = orh:Pro_Date
                    IF (Access:SBO_OutParts.TryFetch(sout:PartDescDateRaisedProcessedKey))
                        IF (res:Received = 0)
                            IF (Access:SBO_OutParts.PrimeRecord() = Level:Benign)
                                sout:SessionID = p_web.SessionID
                                sout:PartNumber = res:Part_Number
                                sout:Description = res:Description
                                sout:DateRaised = orh:TheDate
                                sout:DateProcessed = orh:Pro_date

                                sout:Quantity = res:Quantity


                                sout:LineType = 'S'
                                IF (Access:SBO_OutParts.TryInsert())
                                END

                            END
                        END

                    ELSE
                        IF (res:Received = 0)
                            sout:Quantity += res:Quantity
                            Access:SBO_OutParts.TryUpdate()
                        END

                    END

                End ! Loop
            End ! If Access:RETSALES.TryFetch(ret:Ref_Number_Key) = Level:Benign
        End ! If orh:Procesed = 0
    End ! Loop

    locRecordcount = 0

! Load exchange queue
    Access:EXCHORDR.ClearKey(exo:Ref_Number_Key)
    exo:Ref_Number = 1
    set(exo:Ref_Number_Key,exo:Ref_Number_Key)
    loop until Access:EXCHORDR.Next()

        locRecordcount += 1

        if (clock() - bt) > 200
            bt=clock()

            UpdateProgress(p_web,INT((locRecordcount / RECORDS(EXCHORDR)) * 100),kReturnURL,'Checking Exchange Orders')

        end

        if exo:Qty_Received < exo:Qty_Required              ! This check is just for safety, when exchange orders are completed the EXCHORDR record is deleted
            IF (exo:Location <> p_web.GSV('BookingSiteLocation'))
                CYCLE
            END



            IF (Access:SBO_OutParts.PrimeRecord() = Level:Benign)
                sout:SessionID = p_web.SessionID
                sout:PartNumber = exo:Model_Number
                sout:Description = exo:Manufacturer
                sout:Quantity = exo:Qty_Required - exo:qty_received
                sout:DateRaised = exo:DateCreated
                sout:LineType = 'E'
                IF (Access:SBO_OutParts.TryInsert() = Level:Benign)
                END

            END
        end
    end

    locRecordcount = 0

! Load loan queue
    Access:LOAORDR.ClearKey(lor:Ref_Number_Key)
    lor:Ref_Number = 1
    set(lor:Ref_Number_Key,lor:Ref_Number_Key)
    loop until Access:LOAORDR.Next()

        locRecordcount += 1

        if (clock() - bt) > 200
            bt=clock()

            UpdateProgress(p_web,INT((locRecordcount / RECORDS(LOAORDR)) * 100),kReturnURL,'Checking Loan Orders')

        end

        if lor:Qty_Received < lor:Qty_Required
            IF (exo:Location <> p_web.GSV('BookingSiteLocation'))
                CYCLE
            END

            IF (Access:SBO_OutParts.PrimeRecord() = Level:Benign)
                sout:SessionID = p_web.SessionID
                sout:PartNumber = lor:Model_Number
                sout:Description = lor:Manufacturer
                sout:Quantity = lor:Qty_Required - lor:Qty_Received
                sout:DateRaised = lor:DateCreated
                sout:LineType = 'L'
                IF (Access:SBO_OutParts.TryInsert() = Level:Benign)
                END

            END

        end
    end

    FLUSH(SBO_OutParts)
    FLUSH(ORDHEAD)
    FLUSH(ORDITEMS)
    FLUSH(RETSTOCK)
    FLUSH(RETSALES)
    FLUSH(EXCHORDR)
    FLUSH(LOAORDR)


BuildPartsList      ROUTINE
DATA
locRecordCount      LONG()
kReturnURL  EQUATE('FormBrowseStock')
locTotalRecords     LONG()
bt  LONG()
CODE

    p_web.SSV('RedirectURL','FormBrowseOldPartNumber')
    IF (p_web.GSV('locOldPartNumber') = '')
        EXIT
    END

    bt = CLOCK()
    UpdateProgress(p_web,0,kReturnURL)
    locTotalRecords = RECORDS(SBO_OutParts)

    ! Clear Existing Record
    Access:SBO_OutParts.ClearKey(sout:PartNumberKey)
    sout:SessionID = p_web.SessionID
    SET(sout:PartNumberKey,sout:PartNumberKey)
    LOOP UNTIL Access:SBO_OutParts.Next()
        IF (sout:SessionID <> p_web.SessionID)
            BREAK
        END

        locRecordcount += 1


        if (clock() - bt) > 200
            bt=clock()

            UpdateProgress(p_web,INT((locRecordcount /locTotalRecords ) * 100),kReturnURL,'Clearing...')

        end

        Delete(SBO_OutParts)
    END

    locRecordcount = 0


    Access:STOPARTS.Clearkey(spt:LocationOldPartKey)
    spt:Location = p_web.GSV('Default:SiteLocation')
    spt:OldPartNumber = p_web.GSV('locOldPartNumber')
    Set(spt:LocationOldPartKey,spt:LocationOldPartKey)
    Loop ! Begin Loop
        If Access:STOPARTS.Next()
            Break
        End ! If Access:STOPARTS.Next()

        locRecordcount += 1


        if (clock() - bt) > 200
            bt=clock()
            UpdateProgress(p_web,INT(locRecordCount / RECORDS(STOPARTS) * 100),kReturnURL,'Searching For Parts')
        END


        If spt:Location <> p_web.GSV('Default:SiteLocation')
            Break
        End ! If spt:Location <> f:Location
        If spt:OldPartNumber <> p_web.GSV('locOldPartNumber')
            Break
        End ! If spt:OldPartNumber <> f:PartNumber
        Access:STOCK.ClearKey(sto:Ref_Number_Key)
        sto:Ref_Number = spt:STOCKRefNumber
        If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
        !Found
        Else ! If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
        !Error
            Cycle
        End ! If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign

        IF (Access:SBO_OutParts.PrimeRecord() = Level:Benign)
            sout:SessionID = p_web.SessionID
            sout:LineType = 'S'
            sout:PartNumber = sto:Part_Number
            sout:Description = sto:Description
            sout:Quantity = sto:Ref_Number ! Use to get part number later
            IF (Access:SBO_OutParts.TryInsert())
                Access:SBO_OutParts.CancelAutoInc()
            END
        END

    End ! Loop
    Access:STOCK.ClearKey(sto:Location_Key)
    sto:Part_Number = p_web.GSV('locOldPartNumber')
    sto:Location = p_web.GSV('Default:SiteLocation')
    If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
    !Found
        IF (Access:SBO_OutParts.PrimeRecord() = Level:Benign)
            sout:SessionID = p_web.SessionID
            sout:LineType = 'S'
            sout:PartNumber = sto:Part_Number
            sout:Description = sto:Description
            sout:Quantity = sto:Ref_Number ! Use to get part number later
            IF (Access:SBO_OutParts.TryInsert())
                Access:SBO_OutParts.CancelAutoInc()
            END
        END
    Else ! If Access:STOCK.TryFetch(sto:Part_Number_Key) = Level:Benign
    !Error
    End ! If Access:STOCK.TryFetch(sto:Part_Number_Key) = Level:Benign

BuildExchangeAllocationQueue           Routine
Data
local:WrongSite     Byte(0)
local:FoundHistory  Byte(0)
locRecordCount      LONG()
locTotalRecords     LONG()
bt LONG()

Code

    bt=clock()

    UpdateProgress(p_web,0,p_web.GSV('RedirectURL'))

    ClearSBOGenericFile(p_web)

    Access:STATUS.ClearKey(sts:Ref_Number_Only_Key)
    sts:Ref_Number = 350
    If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key) = Level:Benign
        !Found
    Else!If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End!If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key) = Level:Benign

    locTotalRecords = RECORDS(JOBS)

    Access:JOBS.ClearKey(job:ExcStatusKey)
    job:Exchange_Status = sts:Status
    Set(job:ExcStatusKey,job:ExcStatusKey)
    Loop
        If Access:JOBS.NEXT()
            Break
        End !If
        If job:Exchange_Status <> sts:Status      |
            Then Break.  ! End If

        locRecordcount += 1
        if (clock() - bt) > 200
            bt=clock()

            UpdateProgress(p_web,INT((locRecordcount /locTotalRecords ) * 100),p_web.GSV('RedirectURL'),'Working..')

        end

        !Which engineer changed the status. Is that engineer at this site? - 4000 (DBH: 04-03-2004)
        local:WrongSite = False
        local:FoundHistory = False

        Access:AUDSTATS.ClearKey(aus:DateChangedKey)
        aus:RefNumber   = job:Ref_Number
        aus:Type        = 'EXC'
        aus:DateChanged = Today()
        Set(aus:DateChangedKey,aus:DateChangedKey)
        Loop
            If Access:AUDSTATS.PREVIOUS()
                Break
            End !If
            If aus:RefNumber   <> job:Ref_Number      |
                Or aus:Type        <> 'EXC'      |
                Then Break.  ! End If
            If aus:NewStatus = sts:Status
                Access:USERS.Clearkey(use:User_Code_Key)
                use:User_Code   = aus:UserCode
                If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                    !Found
                    If use:Location <> p_web.GSV('Default:SiteLocation')
                        local:WrongSite = True
                    End !If use:Location <> p_web.GSV('Default:SiteLocation')
                Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                    !Error
                End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                ! Inserting (DBH 13/04/2007) # 8912 - Found correct status history
                local:FoundHistory = True
                ! End (DBH 13/04/2007) #8912
                Break
            End !If aus:NewStatus = tmp:Status
        End !Loop


        If local:WrongSite = True
            Cycle
        End !If WrongSite# = True

        ! Inserting (DBH 13/04/2007) # 8912 - Add double check in case there is no status history, or even audit trail to determine location
        If local:FoundHistory = False
            Access:AUDIT.Clearkey(aud:TypeActionKey)
            aud:Ref_Number = job:Ref_Number
            aud:Type = 'EXC'
            aud:Action = '48 HOUR EXCHANGE ORDER CREATED'
            aud:Date = Today()
            Set(aud:TypeActionKey,aud:TypeActionKey)
            Loop ! Begin Loop
                If Access:AUDIT.Next()
                    Break
                End ! If Access:AUDIT.Next()
                If aud:Ref_Number <> job:Ref_Number
                    Break
                End ! If aud:Ref_Number <> job:Ref_Number
                If aud:Type <> 'EXC'
                    Break
                End ! If aud:Type <> 'EXC'
                If aud:Action <> '48 HOUR EXCHANGE ORDER CREATED'
                    Break
                End ! If aud:Action <> '48 HOUR EXCHANGE ORDER CREATED'
                If aud:Date > Today()
                    Break
                End ! If aud:Date > Today()
                Access:USERS.ClearKey(use:User_Code_Key)
                use:User_Code = aud:User
                If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
                    !Found
                    If use:Location <> p_web.GSV('Default:SiteLocation')
                        local:WrongSite = True
                    End ! If use:Location <> p_web.GSV('Default:SiteLocation')
                Else ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
                    !Error
                End ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
                local:FoundHistory = True
                Break
            End ! Loop

            If local:WrongSite = True
                Cycle
            End ! If local:WrongSite = True
        End ! If local:FoundStatusHistory = False

        If local:FoundHistory = False
            ! Can't find status or audit history, then just compare the booking location and assume the RRC ordered the exchange (DBH: 13/04/2007)
            Access:TRADEACC.ClearKey(tra:Account_Number_Key)
            tra:Account_Number = p_web.GSV('BookingAccount')
            If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                !Found
                If tra:SiteLocation <> p_web.GSV('Default:SiteLocation')
                    local:WrongSite = True
                End ! If tra:SiteLocation <> p_web.GSV('Default:SiteLocation')
            Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                !Error
            End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
        End ! If local:FoundHistory = False
        If local:WrongSite = True
            Cycle
        End ! If local:WrongSite = True
        ! End (DBH 13/04/2007) #8912




        IF (Access:SBO_GenericFile.PrimeRecord() = Level:Benign)
            sbogen:SessionID = p_web.SessionID
            sbogen:Long1 = job:Ref_Number
            sbogen:String1 = job:Manufacturer
            sbogen:String2 = job:Model_Number
            ! Use this to indicate if it's 48 Hour or not - TrkBs: 6841 (DBH: 09-12-2005)
            sbogen:Long2 = 0
            Access:EXCHORDR.ClearKey(exo:By_Location_Key)
            exo:Location     = p_web.GSV('Default:SiteLocation')
            exo:Manufacturer = job:Manufacturer
            exo:Model_Number = job:Model_Number
            If Access:EXCHORDR.TryFetch(exo:By_Location_Key) = Level:Benign
                !Found
                ! Use this field to indicate the "On Order" - TrkBs: 6841 (DBH: 09-12-2005)
                sbogen:Byte2 = 1
            Else!If Access:EXCHORDR.TryFetch(exo:By_Location_Key) = Level:Benign
                !Error
                sbogen:Byte2 = 0
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End!If Access:EXCHORDR.TryFetch(exo:By_Location_Key) = Level:Benign
            ! Use this field to indicate the "48 Hour" - TrkBs: 6841 (DBH: 09-12-2005)
            sbogen:Byte1 = 0

            If Access:SBO_GenericFile.TryInsert() = Level:Benign
                ! Insert Successful
            Else ! If Access:RAPENGLS.TryInsert() = Level:Benign
                ! Insert Failed
                Access:SBO_GenericFile.CancelAutoInc()
            End ! If Access:RAPENGLS.TryInsert() = Level:Benign
        End !If Access:RAPENGLS.PrimeRecord() = Level:Benign
    End !Loop

    Access:STATUS.ClearKey(sts:Ref_Number_Only_Key)
    sts:Ref_Number = 360
    If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key) = Level:Benign
        !Found

    Else!If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End!If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key) = Level:Benign


    Access:JOBS.ClearKey(job:ExcStatusKey)
    job:Exchange_Status = sts:Status
    Set(job:ExcStatusKey,job:ExcStatusKey)
    Loop
        If Access:JOBS.NEXT()
            Break
        End !If
        If job:Exchange_Status <> sts:Status      |
            Then Break.  ! End If

        locRecordcount += 1
        if (clock() - bt) > 200
            bt=clock()

            UpdateProgress(p_web,INT((locRecordcount /locTotalRecords ) * 100),p_web.GSV('RedirectURL'),'Working..')

        end


        !Which engineer changed the status. Is that engineer at this site? - 4000 (DBH: 04-03-2004)
        local:FoundHistory = False
        local:WrongSite = False

        Access:AUDSTATS.ClearKey(aus:DateChangedKey)
        aus:RefNumber   = job:Ref_Number
        aus:Type        = 'EXC'
        aus:DateChanged = Today()
        Set(aus:DateChangedKey,aus:DateChangedKey)
        Loop
            If Access:AUDSTATS.PREVIOUS()
                Break
            End !If
            If aus:RefNumber   <> job:Ref_Number      |
                Or aus:Type        <> 'EXC'      |
                Then Break.  ! End If
            If aus:NewStatus = sts:Status
                Access:USERS.Clearkey(use:User_Code_Key)
                use:User_Code   = aus:UserCode
                If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                    !Found
                    If use:Location <> p_web.GSV('Default:SiteLocation')
                        local:WrongSite = True
                    End !If use:Location <> p_web.GSV('Default:SiteLocation')
                Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                    !Error
                End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                local:FoundHistory = True
                Break
            End !If aus:NewStatus = tmp:Status
        End !Loop

        If local:WrongSite = True
            Cycle
        End !If WrongSite# = True

        ! Inserting (DBH 13/04/2007) # 8912 - Add double check in case there is no status history, or even audit trail to determine location
        If local:FoundHistory = False

            Access:AUDIT.Clearkey(aud:TypeActionKey)
            aud:Ref_Number = job:Ref_Number
            aud:Type = 'EXC'
            aud:Action = '48 HOUR EXCHANGE ORDER CREATED'
            aud:Date = Today()
            Set(aud:TypeActionKey,aud:TypeActionKey)
            Loop ! Begin Loop
                If Access:AUDIT.Next()
                    Break
                End ! If Access:AUDIT.Next()
                If aud:Ref_Number <> job:Ref_Number
                    Break
                End ! If aud:Ref_Number <> job:Ref_Number
                If aud:Type <> 'EXC'
                    Break
                End ! If aud:Type <> 'EXC'
                If aud:Action <> '48 HOUR EXCHANGE ORDER CREATED'
                    Break
                End ! If aud:Action <> '48 HOUR EXCHANGE ORDER CREATED'
                If aud:Date > Today()
                    Break
                End ! If aud:Date > Today()
                Access:USERS.ClearKey(use:User_Code_Key)
                use:User_Code = aud:User
                If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
                    !Found
                    If use:Location <> p_web.GSV('Default:SiteLocation')
                        local:WrongSite = True
                    End ! If use:Location <> p_web.GSV('Default:SiteLocation')
                Else ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
                    !Error
                End ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
                local:FoundHistory = True
                Break
            End ! Loop

            If local:WrongSite = True
                Cycle
            End ! If local:WrongSite = True
        End ! If local:FoundStatusHistory = False

        If local:FoundHistory = False
            ! Can't find status or audit history, then just compare the booking location and assume the RRC ordered the exchange (DBH: 13/04/2007)
            Access:TRADEACC.ClearKey(tra:Account_Number_Key)
            tra:Account_Number = p_web.GSV('BookingAccount')
            If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                !Found
                If tra:SiteLocation <> p_web.GSV('Default:SiteLocation')
                    local:WrongSite = True
                End ! If tra:SiteLocation <> p_web.GSV('Default:SiteLocation')
            Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                !Error
            End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
        End ! If local:FoundHistory = False
        If local:WrongSite = True
            Cycle
        End ! If local:WrongSite = True
        ! End (DBH 13/04/2007) #8912

        If Access:SBO_GenericFile.PrimeRecord() = Level:Benign
            sbogen:SessionID = p_web.SessionID
            sbogen:Long1 = job:Ref_Number
            sbogen:String1 = job:Manufacturer
            sbogen:String2 = job:Model_Number
            sbogen:Byte2 = 0
            ! Use this to indicate if it's 48 Hour or not - TrkBs: 6841 (DBH: 09-12-2005)
            sbogen:Long2 = 1
            !Has the exchange order been processed? -  (DBH: 30-10-2003)
            Access:EXCHOR48.ClearKey(ex4:LocationJobKey)
            ex4:Received  = 1
            ex4:Returning = 0
            ex4:Location  = p_web.GSV('Default:SiteLocation')
            ex4:JobNumber = sbogen:Long1
            If Access:EXCHOR48.TryFetch(ex4:LocationJobKey) = Level:Benign
                sbogen:Byte1 = 2
            Else !If Access:EXCHOR48.TryFetch(ex4:LocationJobKey) = Level:Benign
                sbogen:Byte1 = 1
            End !If Access:EXCHOR48.TryFetch(ex4:LocationJobKey) = Level:Benign
            If Access:SBO_GenericFile.TryInsert() = Level:Benign
                ! Insert Successful
            Else ! If Access:RAPENGLS.TryInsert() = Level:Benign
                ! Insert Failed
                Access:SBO_GenericFile.CancelAutoInc()
            End ! If Access:RAPENGLS.TryInsert() = Level:Benign
        End !If Access:RAPENGLS.PrimeRecord() = Level:Benign

    End !Loop

SendPacket  Routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet,1,packetlen,Net:NoHeader)
    packet = ''
  end
Header Routine
  packet = p_web.w3Header(p_web.Combine(p_web.site.HtmlClass,))
  packet = clip(packet) & '<head>'&|
      '<title>'&p_web.Translate(p_web.site.PageTitle)&'</title>'&|
      '<meta http-equiv="Content-Type" content="text/html; charset='&clip(p_web.site.HtmlCharset)&'" /><13,10>' &|
      clip(p_web.MetaHeaders)
    Do declareProgressBar
    Do redirect
  packet =  clip(packet) & p_web.IncludeStyles()
  packet =  clip(packet) & p_web.IncludeScripts()
  packet = clip(packet) & '</head><13,10>'
  p_web.ParseHTML(packet,1,0,Net:SendHeader+Net:DontCache)
  packet = ''
Footer Routine
  packet = clip(packet) & '<!-- Net:SelectField --><13,10>' &|
                         '</div></body><13,10></html><13,10>'
declareProgressBar  Routine
  packet = clip(packet) & p_web.AsciiToUTF(|
    '<<!-- Net:DeclareProgressBar --><13,10>'&|
    '',net:OnlyIfUTF)
heading  Routine
  packet = clip(packet) & p_web.AsciiToUTF(|
    '<<!-- Net:BannerBlank --><13,10>'&|
    '',net:OnlyIfUTF)
redirect  Routine
    packet = clip(packet) & p_web.AsciiToUTF(|
        '<<META HTTP-EQUIV="refresh" CONTENT="1;URL=' & p_web.GSV('RedirectURL') & '"><13,10>'&|
        '',net:OnlyIfUTF)
  packet = clip(packet) & p_web.AsciiToUTF(|
    '<13,10>'&|
    '',net:OnlyIfUTF)
ShowWait  Routine
  packet = clip(packet) & p_web.AsciiToUTF(|
    '<13,10>'&|
    '',net:OnlyIfUTF)
  do sendpacket
  
  CASE p_web.GSV('ProcessType')
  OF 'ExchangeAllocation'
      DO BuildExchangeAllocationQueue
  OF 'OutstandingOrders'
  DO BuildOutstandingOrders
  OF 'OldPartNumber'
      DO BuildPartsList
  END ! Case
CloseWait  Routine
    packet = clip(packet) & p_web.AsciiToUTF(|
        '<<!-- Net:CloseWait --><13,10>'&|
        '<<div id="waitframe" class="nt-process"><13,10>'&|
        '<<p class="nt-process-text">Process Completed<<BR /><<br/><13,10>'&|
        'If you are not re-directed then click <<a href="' & p_web.GSV('ReturnURL') & '">here<</a> to view the results.<13,10>'&|
        '<</p><13,10>'&|
        '<<BR/><<BR/><13,10>'&|
        '<<BR/><<BR/><13,10>'&|
        '<<BR/><<BR/><13,10>'&|
        '<</div><13,10>'&|
        '',net:OnlyIfUTF)
  packet = clip(packet) & p_web.AsciiToUTF(|
    '<13,10>'&|
    '',net:OnlyIfUTF)
