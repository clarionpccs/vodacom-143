

   MEMBER('WebServer_Phase4.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSE078.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSE013.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE018.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE021.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE036.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE082.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE099.INC'),ONCE        !Req'd for module callout resolution
                     END


BrowseModelStock     PROCEDURE  (NetWebServerWorker p_web)
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256),dim(Net:MaxKeyFields)
idx           String(Net:HashSize)
sub           Long
            End
Select:IsInvalid  Long
stm:Description:IsInvalid  Long
stm:Part_Number:IsInvalid  Long
sto:Purchase_Cost:IsInvalid  Long
sto:Sale_Cost:IsInvalid  Long
sto:Quantity_Stock:IsInvalid  Long
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:DefaultSelection    String(Net:HashSize)
loc:ActualSelection     String(Net:HashSize)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
!loc:pagename            String(256)
loc:NavButtonPosition    Long
loc:UpdateButtonPosition Long
loc:SelectionMethod      Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorCase         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
loc:lookupdone          Long
loc:FormPopup           Long
ThisView            View(STOMODEL)
                      Project(stm:RecordNumber)
                      Project(stm:Description)
                      Project(stm:Part_Number)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
Loc:popup             Long
loc:poppedup          Long,static,thread
FilesOpened     Long
STOCK::State  USHORT
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return 
  End
  GlobalErrors.SetProcedureName('BrowseModelStock')
  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseModelStock:NoForm')
      loc:NoForm = p_web.GetValue('BrowseModelStock:NoForm')
      loc:FormName = p_web.GetValue('BrowseModelStock:FormName')
    else
      loc:FormName = 'BrowseModelStock_frm'
    End
    p_web.SSV('BrowseModelStock:NoForm',loc:NoForm)
    p_web.SSV('BrowseModelStock:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseModelStock:NoForm')
    loc:FormName = p_web.GSV('BrowseModelStock:FormName')
  end
  case p_web.site.formpopups
  of -1 ; loc:FormPopup = Net:Page
  of 0 ; loc:FormPopup = Net:Page
  of 1 ; loc:FormPopup = Net:Popup
  End
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseModelStock') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseModelStock')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if 1 = 2
  elsif p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  elsif p_web.IfExistsValue('_EIPRow_')
    do CallRow
  elsif p_web.GetValue('_CallPopups') > 0
    do CallPopups
  elsif p_web.IfExistsValue('_GetSecwinSettings')
      p_web.SetValue('Secwin_' & 'BrowseModelStock' & 'AccessGroupsArray','1') 
      p_web.SetValue('Secwin_AccessWindowName','BrowseModelStock')
      
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallPopups  Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
loc:CallPopups   Long
loc:name         String(255)
  code
  if loc:poppedup = 0
    loc:CallPopups = p_web.GetValue('_CallPopups')
    if loc:CallPopups = 1 !and p_web.GetPreCall('BrowseModelStock') = 0! for = 1 include popup divs and scripts for this browse
      !p_web.AddPreCall('BrowseModelStock')
      p_web.DivHeader('popup_BrowseModelStock','nt-hidden')
      p_web.DivHeader('BrowseModelStock',p_web.combine(p_web.site.style.browsediv,))
      p_web.DivFooter()
      p_web.DivFooter()
      loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(400)&', modal: true, position: [''center'',15]'
      packet =  p_web.jQuery('#' & lower('popup_BrowseModelStock_div'),'dialog',loc:options) & |
      p_web.jQuery('#' & lower('popup_BrowseModelStock_div'),'removeClass','''nt-hidden''')
    End
    ! for > 0 add in form, EIP-lookups and browse object
    do Popups
    loc:popup = 1
    do ClosingScripts
    do SendPacket
    loc:poppedup = 1
  end

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web.DivHeader('BrowseModelStock',p_web.combine(p_web.site.style.browsediv,))
  if loc:ParentSilent = 0
    do GenerateBrowse
    if p_web.RequestAjax = 1
      p_web.Script('BrowseProducts.pcv();')
    end
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web.DivFooter()
  do Children
  do Popups
  if loc:poppedup = 0
    do ClosingScripts
  end
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(STOMODEL,stm:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'STM:DESCRIPTION') then p_web.SetValue('BrowseModelStock_sort','2')
    ElsIf (loc:vorder = 'STM:PART_NUMBER') then p_web.SetValue('BrowseModelStock_sort','7')
    ElsIf (loc:vorder = 'STO:PURCHASE_COST') then p_web.SetValue('BrowseModelStock_sort','5')
    ElsIf (loc:vorder = 'STO:SALE_COST') then p_web.SetValue('BrowseModelStock_sort','6')
    ElsIf (loc:vorder = 'STO:QUANTITY_STOCK') then p_web.SetValue('BrowseModelStock_sort','3')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseModelStock:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseModelStock:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseModelStock:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseModelStock:LookupField')
    loc:selecting = 0
  End

Popups Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  If loc:popup = 0
  End
SetFormAction  Routine
  loc:formaction = 'BrowseModelStock'

GotFocusBack   Routine

GenerateBrowse Routine
  data
loc:viewoptions  Long
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  ! Set general Browse options
  loc:NavButtonPosition   = Net:Below
  loc:UpdateButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:PageLoad
  loc:Sorting          = Net:ServerSort
  if p_web.GetValue('_popup_') = 1
    loc:popup = 1
  end
  ! Set Locator Options
  loc:LocatorPosition  = Net:Above
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  p_web.ClearBrowse('BrowseModelStock')
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 15
  End
  loc:ActualSelection = ''
  !loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseModelStock_sort',net:DontEvaluate)
  If loc:vordernumber = 0
    loc:vordernumber = 2
  End
  p_web.SetSessionValue('BrowseModelStock_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 4
    Loc:LocateField = ''
    Loc:LocatorCase = 0
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(stm:Description)','-UPPER(stm:Description)')
    Loc:LocateField = 'stm:Description'
    Loc:LocatorCase = 0
  of 7
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(stm:Part_Number)','-UPPER(stm:Part_Number)')
    Loc:LocateField = 'stm:Part_Number'
    Loc:LocatorCase = 0
  of 5
    loc:vorder = Choose(Loc:SortDirection=1,'sto:Purchase_Cost','-sto:Purchase_Cost')
    Loc:LocateField = 'sto:Purchase_Cost'
    Loc:LocatorCase = 0
  of 6
    loc:vorder = Choose(Loc:SortDirection=1,'sto:Sale_Cost','-sto:Sale_Cost')
    Loc:LocateField = 'sto:Sale_Cost'
    Loc:LocatorCase = 0
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'sto:Quantity_Stock','-sto:Quantity_Stock')
    Loc:LocateField = 'sto:Quantity_Stock'
    Loc:LocatorCase = 0
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('stm:Description')
    loc:SortHeader = p_web.Translate('Description')
    p_web.SetSessionValue('BrowseModelStock_LocatorPic','@s30')
  Of upper('stm:Part_Number')
    loc:SortHeader = p_web.Translate('Part Number')
    p_web.SetSessionValue('BrowseModelStock_LocatorPic','@s30')
  Of upper('sto:Purchase_Cost')
    loc:SortHeader = p_web.Translate('In Warranty')
    p_web.SetSessionValue('BrowseModelStock_LocatorPic','@n14.2')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
  Of upper('sto:Sale_Cost')
    loc:SortHeader = p_web.Translate('Out Warranty')
    p_web.SetSessionValue('BrowseModelStock_LocatorPic','@n14.2')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
  Of upper('sto:Quantity_Stock')
    loc:SortHeader = p_web.Translate('Quantity In Stock')
    p_web.SetSessionValue('BrowseModelStock_LocatorPic','@N8')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
  End
  If loc:selecting = 1
    p_web.GetSettings(p_web.GetSessionValue('Push1'))
    loc:selectaction = p_web.FormSettings.ParentPage !p_web.GetSessionValue('BrowseModelStock:LookupFrom')
  End!Else
  do SendPacket
  do SetFormAction
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseModelStock:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseModelStock:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseModelStock:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="STOMODEL"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="stm:RecordNumberKey"></input><13,10>'
  end
  If p_web.Translate('Browse Stock') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.browseSubHeading,)&'">'&p_web.Translate('Browse Stock',0)&'</div>'&CRLF
  End
  case p_web.GetValue('refresh') ; of 'next' orof 'previous' orof 'first' orof 'last'
    p_web.Script('try{{BrowseModelStock.restoreFocus();} catch(err){{};')
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseModelStock',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      case loc:locatortype
      of Net:Position
      orof Net:Date
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
      of Net:Contains
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
      of Net:Begins
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
      End
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
      Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
      Case Loc:LocatorType
      of Net:Date
        packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator2BrowseModelStock','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('BrowseModelStock_LocatorPic'),,,'onchange="BrowseModelStock.locate(''Locator2BrowseModelStock'',this.value);"',,0,,,) & '</div></td>'
      Else
        packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator2BrowseModelStock',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="BrowseModelStock.locate(''Locator2BrowseModelStock'',this.value);" ',,,,,,) & '</td>'
      End
      If loc:LocatorSearchButton or loc:LocatorClearButton
        packet = clip(packet) & '<td><div id="BrowseModelStock_locate_a" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
        If loc:LocatorSearchButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
        End
        If loc:LocatorClearButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseModelStock.cl(''BrowseModelStock'');')
        End
        packet = clip(packet) & '</div><13,10></td><13,10>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
      If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'BrowseModelStock_locate_a','buttonset',loc:options)
      End
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & p_web.DivHeader('BrowseModelStock_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'','sortable')&'" id="BrowseModelStock_tbl">'
  Else
    packet = clip(packet) & p_web.DivHeader('BrowseModelStock_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'')&'" id="BrowseModelStock_tbl">'
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
      If loc:Selecting = 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'4','BrowseModelStock',p_web.Translate('Pick'),,,,,,,0,loc:Sorting,'Button')
          do AddPacket
          loc:columns += 1
      End ! Selecting
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'2','BrowseModelStock',p_web.Translate('Description'),'Click here to sort by Description',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'7','BrowseModelStock',p_web.Translate('Part Number'),'Click here to sort by Part Number',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'5','BrowseModelStock',p_web.Translate('In Warranty'),'Click here to sort by In Warranty Cost',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'6','BrowseModelStock',p_web.Translate('Out Warranty'),'Click here to sort by Out Warranty Cost',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'3','BrowseModelStock',p_web.Translate('Quantity In Stock'),'Click here to sort by Quantity In Stock',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,15,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  If Instring('stm:recordnumber',lower(loc:vorder),1,1) = 0 !and STOMODEL{prop:SQLDriver} = 1
    loc:vorder = Choose(loc:vorder='','stm:RecordNumber',clip(loc:vorder) & ',' & 'stm:RecordNumber')
  End
  Loc:Selected = Choose(p_web.IfExistsValue('stm:RecordNumber'),p_web.GetValue('stm:RecordNumber'),p_web.GetSessionValue('stm:RecordNumber'))
  ThisView{prop:order} = p_web.CleanFilter(ThisView,clip(loc:vorder))
      loc:FilterWas = 'Upper(stm:Model_Number) = Upper(''' & p_web.GSV('job:Model_Number') & ''') AND Upper(stm:Location)  = Upper(''' & p_web.GSV('BookingSiteLocation') & ''')'
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseModelStock',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseModelStock_Filter')
    p_web.SetSessionValue('BrowseModelStock_FirstValue','')
    p_web.SetSessionValue('BrowseModelStock_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,STOMODEL,stm:RecordNumberKey,loc:PageRows,'BrowseModelStock',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,loc:ViewOptions,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled,Loc:LocatorCase)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or Loc:LocatorType = Net:Date or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position or loc:LocatorType = Net:Date
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If STOMODEL{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(STOMODEL,loc:firstvalue)
              Reset(ThisView,STOMODEL)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If STOMODEL{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(STOMODEL,loc:lastvalue)
            Reset(ThisView,STOMODEL)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      if (stm:Part_Number = 'EXCH')
          cycle
      end !if (stm:Part_Number = 'EXCH')
      
      Access:STOCK.ClearKey(sto:Ref_Number_Key)
      sto:Ref_Number = stm:Ref_Number
      If (Access:STOCK.TryFetch(sto:Ref_Number_Key))
          cycle
      end
      
      if (p_web.GSV('BookingRestrictParts') = 1 and |
          p_web.GSV('BookingRestrictChargeable') = 1 and |
          sto:SkillLevel > p_web.GSV('BookingSkillLevel'))
          cycle
      end ! if (p_web.GSV('BookingRestrictParts') = 1
      
      if (p_web.GSV('BookingSkillLevel') > 0)
          case p_web.GSV('BookingSkillLevel')
          of 1
              if (~sto:E1)
                  cycle
              end ! if (~sto:E1)
          of 2
              if ~(sto:E1 or sto:E2)
                  cycle
              end!if ~(sto:E1 or sto:E2)
          of 3
              if ~(sto:E1 or sto:E2 or sto:E3)
                  cycle
              end ! if ~(sto:E1 or sto:E2 or sto:E3)
          end ! case
      
      end ! if (p_web.GSV('BookingSkillLevel') > 0)
      
      
      if (sto:Suspend)
          cycle
      end ! if (stm:Part_Number = 'EXCH')
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(stm:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td class="'&p_web.combine(p_web.site.style.BrowseEmpty,)&'">'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:NavButtonPosition=Net:Above or (loc:NavButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseModelStock_nav_a" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseModelStock.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseModelStock.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseModelStock.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseModelStock.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseModelStock_nav_a','buttonset',loc:options)
          End
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:UpdateButtonPosition=Net:Above or (loc:UpdateButtonPosition=Net:Both and loc:found > 0))
    If loc:selecting = 1 !and loc:parent = ''
      if loc:popup
        packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:BrowseCancelButton,'BrowseModelStock',,,loc:popup)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
      end
      TableQueue.Kind = Net:BeforeTable
      do AddPacket
    End
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseModelStock',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseModelStock_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseModelStock_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          case loc:locatortype
          of Net:Position
          orof Net:Date
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
          of Net:Contains
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
          of Net:Begins
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
          End
          packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
          Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
          Case Loc:LocatorType
          of Net:Date
            packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator1BrowseModelStock','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('BrowseModelStock_LocatorPic'),,,'onchange="BrowseModelStock.locate(''Locator1BrowseModelStock'',this.value);"',,0,,,) & '</div></td>'
          Else
            packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator1BrowseModelStock',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="BrowseModelStock.locate(''Locator1BrowseModelStock'',this.value);" ',,,,,,) & '</td>'
          End
          If loc:LocatorSearchButton or loc:LocatorClearButton
            packet = clip(packet) & '<td><div id="BrowseModelStock_locate_b" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
            If loc:LocatorSearchButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
            End
            If loc:LocatorClearButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseModelStock.cl(''BrowseModelStock'');')
            End
            packet = clip(packet) & '</div><13,10></td><13,10>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
          If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseModelStock_locate_b','buttonset',loc:options)
          End
      End
    End
  End
  p_web.SetSessionValue('BrowseModelStock_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseModelStock_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:NavButtonPosition=Net:Below or loc:NavButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseModelStock_nav_b" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseModelStock.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseModelStock.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseModelStock.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseModelStock.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseModelStock_nav_b','buttonset',loc:options)
          End
        end
        do SendPacket
  End
  If loc:UpdateButtonPosition=Net:Below or loc:UpdateButtonPosition=Net:Both
  If loc:selecting = 1 !and loc:parent = ''
    if loc:popup
      packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:BrowseCancelButton,'BrowseModelStock',,,loc:popup)
    else
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
    end
    do SendPacket
  End
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
  data
loc:rowclick  string(1024)
  code
    loc:field = p_web.AddBrowseValue('BrowseModelStock','STOMODEL',stm:RecordNumberKey) !stm:RecordNumber
    p_web._thisrow = p_web._nocolon('stm:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if Loc:LocatorValue <> '' and loc:ActualSelection = ''
        loc:checked = 'checked'
        do SetSelection
      elsif loc:ActualSelection = '' and stm:RecordNumber = p_web.GetValue('stm:RecordNumber')
         loc:checked = 'checked'
         do SetSelection
      elsif loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseModelStock:LookupField')) = stm:RecordNumber and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      else
        loc:checked = Choose((stm:RecordNumber = loc:selected) and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      end
      If(loc:SelectionMethod  = Net:Radio)
        loc:RowStyle = 'class="' & p_web.combine(p_web.site.style.browserow,) &'"'
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'data-nt-id="'& p_web.AddBrowseValue('BrowseModelStock','STOMODEL',stm:RecordNumberKey) &'" onclick="'
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If STOMODEL{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(STOMODEL)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            loc:DefaultSelection = loc:field
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If STOMODEL{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(STOMODEL)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            loc:DefaultSelection = loc:field
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'>'&p_web.CreateInput('radio','_bidv_',clip(loc:field),,loc:checked,'')&'</td>'&CRLF
          If loc:DefaultSelection = ''
            loc:DefaultSelection = loc:field
          End
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          If loc:DefaultSelection = '' or loc:direction < 0
            loc:DefaultSelection = loc:field
          End
        End
    end ! loc:eip = 0
        If Loc:Selecting = 1
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::Select
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        End     !Selecting
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::stm:Description
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::stm:Part_Number
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::sto:Purchase_Cost
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::sto:Sale_Cost
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::sto:Quantity_Stock
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr ' & clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Idx = p_web.AddBrowseValue('BrowseModelStock','STOMODEL',stm:RecordNumberKey)
  TableQueue.Id[1] = stm:RecordNumber

ClosingScripts  Routine
  data
Rtn_SecwinProcedureName         string(252)
  code
  If p_web.RequestAjax = 0
    do SetFormAction
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var btiBrowseModelStock;if (btiBrowseModelStock != 1){{var BrowseModelStock=new browseTable(''BrowseModelStock'','''&clip(loc:formname)&|
      ''','''&p_web._jsok('stm:RecordNumber',Net:Parameter)&''',2,'''&clip(loc:divname)&''',1,1,1,'''&|
      clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&|
      p_web.Translate('Are you sure you want to delete this record?')&''','''&clip(loc:ActualSelection)&''','''&clip(loc:selectaction)&''','''&|
      clip(loc:formactiontarget)&''','''','&loc:popup&',0,'''','''',"' & clip(Rtn_SecwinProcedureName) & '");<13,10>'&|
      'BrowseModelStock.setGreenBar('''&p_web.ColorWeb(p_web.Site.Style.BrowseHighlightColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOneColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseTwoColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOverColor)&''');<13,10>' &|
      'BrowseModelStock.applyGreenBar();btiBrowseModelStock=1};<13,10>' &|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseModelStock')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseModelStock')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseModelStock')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseModelStock')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(STOMODEL)
  p_web._CloseFile(STOCK)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(STOMODEL)
  Bind(stm:Record)
  Clear(stm:Record)
  NetWebSetSessionPics(p_web,STOMODEL)
  p_web._OpenFile(STOCK)
  Bind(sto:Record)
  NetWebSetSessionPics(p_web,STOCK)

Children Routine
  if loc:selecting = 0
    If p_web.RequestAjax = 0
      do StartChildren
    Else
      do AjaxChildren
    End
  end

AjaxChildren  Routine
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue('stm:RecordNumber',p_web.GetValue('stm:RecordNumber'))
   p_web.DeleteValue('_Clicked')
   loc:found = 1
   do Children
! ----------------------------------------------------------------------------------------
CallRow  Routine
  data
loc:result  long
  code
  do OpenFilesB
  stm:RecordNumber = p_web.GSV('stm:RecordNumber')
  loc:result = p_web._GetFile(STOMODEL,stm:RecordNumberKey)
  loc:eip = 1
  loc:viewstate = p_web.escape(p_web.Base64Encode(clip(stm:RecordNumber)))
  do BrowseRow
  do SendPacket
  do ClosefilesB

! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(STOMODEL)
  Case upper(p_web.GetValue('_EIPClm'))
  Else
    case p_web.GetValue('_action_')
    of Net:InsertRecord
    orof Net:ChangeRecord
    orof Net:DeleteRecord
    orof Net:CopyRecord
      loc:eip = 0
      do CallBrowse
    end
  End
  do GotFocusBack
  p_web._CloseFile(STOMODEL)
! ----------------------------------------------------------------------------------------
value::Select   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    p_web.site.SmallSelectButton.TextValue = p_web.Translate('>>')
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseModelStock_Select_'&stm:RecordNumber,,net:crc,,loc:extra)
        packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'BrowseModelStock',p_web.AddBrowseValue('BrowseModelStock','STOMODEL',stm:RecordNumberKey),,loc:popup)
    End
    p_web.site.SmallSelectButton = p_web.RequestData.WebServer._SitesQueue.Defaults.SmallSelectButton  !bruce
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::stm:Description   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseModelStock_stm:Description_'&stm:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(stm:Description,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::stm:Part_Number   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseModelStock_stm:Part_Number_'&stm:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(stm:Part_Number,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sto:Purchase_Cost   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseModelStock_sto:Purchase_Cost_'&stm:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(sto:Purchase_Cost,'@n14.2')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sto:Sale_Cost   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseModelStock_sto:Sale_Cost_'&stm:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(sto:Sale_Cost,'@n14.2')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sto:Quantity_Stock   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseModelStock_sto:Quantity_Stock_'&stm:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(sto:Quantity_Stock,'@N8')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  p_web._OpenFile(STOCK)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(STOCK)
     FilesOpened = False
  END
  return
!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
CheckForDuplicate  Routine
  If loc:invalid <> '' then exit. ! no need to check, record is already invalid
  If Duplicate(stm:Model_Number_Key)
    loc:Invalid = 'stm:Ref_Number'
    loc:Alert = clip(p_web.site.DuplicateText) & ' Model_Number_Key --> stm:Ref_Number, stm:Manufacturer, stm:Model_Number'
  End
SetSelection  Routine
  loc:ActualSelection = loc:field
  p_web.SetSessionValue('stm:RecordNumber',stm:RecordNumber)

MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:ActualSelection = ''
    loc:ActualSelection = loc:DefaultSelection
    p_web.GetBrowseValue(loc:ActualSelection,Net:Web:SessionQueue) ! so children are primed with correct sessionValue
  End

  If loc:ActualSelection <> ''
    TableQueue.Kind = Net:RowTable
    get(TableQueue,TableQueue.Kind)
    if Errorcode() = 0
      TableQueue.Row = sub(TableQueue.Row,1,len(clip(TableQueue.Row))-1) &  ' data-nt-rows="'&loc:RowsHigh&'" data-nt-value="'&clip(loc:ActualSelection)&'"' & '>'
      Put(TableQueue)
    End
  End

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead class="'&p_web.combine(p_web.site.style.BrowseHeader,'')&'"><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot class="'&p_web.combine(p_web.site.style.BrowseFooter,)&'"><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody class="'&p_web.combine(p_web.site.style.BrowseBody,)&'"><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table><13,10>' & p_web.DivFooter(Net:NoSend) ! Table Div
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
BrowseWarrantyParts  PROCEDURE  (NetWebServerWorker p_web)
tmp:WARPARTStatus    STRING(20)                            !
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256),dim(Net:MaxKeyFields)
idx           String(Net:HashSize)
sub           Long
            End
wpr:Part_Number:IsInvalid  Long
wpr:Description:IsInvalid  Long
wpr:Quantity:IsInvalid  Long
tmp:WARPARTStatus:IsInvalid  Long
Edit:IsInvalid  Long
MyDelete:IsInvalid  Long
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:DefaultSelection    String(Net:HashSize)
loc:ActualSelection     String(Net:HashSize)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
!loc:pagename            String(256)
loc:NavButtonPosition    Long
loc:UpdateButtonPosition Long
loc:SelectionMethod      Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorCase         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
loc:lookupdone          Long
loc:FormPopup           Long
ThisView            View(WARPARTS)
                      Project(wpr:Record_Number)
                      Project(wpr:Part_Number)
                      Project(wpr:Description)
                      Project(wpr:Quantity)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
Loc:popup             Long
loc:poppedup          Long,static,thread
FilesOpened     Long
STOCK::State  USHORT
USERS::State  USHORT
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return 
  End
  GlobalErrors.SetProcedureName('BrowseWarrantyParts')
  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseWarrantyParts:NoForm')
      loc:NoForm = p_web.GetValue('BrowseWarrantyParts:NoForm')
      loc:FormName = p_web.GetValue('BrowseWarrantyParts:FormName')
    else
      loc:FormName = 'BrowseWarrantyParts_frm'
    End
    p_web.SSV('BrowseWarrantyParts:NoForm',loc:NoForm)
    p_web.SSV('BrowseWarrantyParts:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseWarrantyParts:NoForm')
    loc:FormName = p_web.GSV('BrowseWarrantyParts:FormName')
  end
  case p_web.site.formpopups
  of -1 ; loc:FormPopup = Net:Page
  of 0 ; loc:FormPopup = Net:Page
  of 1 ; loc:FormPopup = Net:Popup
  End
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseWarrantyParts') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseWarrantyParts')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if 1 = 2
  elsif p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  elsif p_web.IfExistsValue('_EIPRow_')
    do CallRow
  elsif p_web.GetValue('_CallPopups') > 0
    do CallPopups
  elsif p_web.IfExistsValue('_GetSecwinSettings')
      p_web.SetValue('Secwin_' & 'BrowseWarrantyParts' & 'AccessGroupsArray','1') 
      p_web.SetValue('Secwin_AccessWindowName','BrowseWarrantyParts')
      
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallPopups  Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
loc:CallPopups   Long
loc:name         String(255)
  code
  if loc:poppedup = 0
    loc:CallPopups = p_web.GetValue('_CallPopups')
    if loc:CallPopups = 1 !and p_web.GetPreCall('BrowseWarrantyParts') = 0! for = 1 include popup divs and scripts for this browse
      !p_web.AddPreCall('BrowseWarrantyParts')
      p_web.DivHeader('popup_BrowseWarrantyParts','nt-hidden')
      p_web.DivHeader('BrowseWarrantyParts',p_web.combine(p_web.site.style.browsediv,))
      p_web.DivFooter()
      p_web.DivFooter()
      loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(400)&', modal: true, position: [''center'',15]'
      packet =  p_web.jQuery('#' & lower('popup_BrowseWarrantyParts_div'),'dialog',loc:options) & |
      p_web.jQuery('#' & lower('popup_BrowseWarrantyParts_div'),'removeClass','''nt-hidden''')
    End
    ! for > 0 add in form, EIP-lookups and browse object
    do Popups
    loc:popup = 1
    do ClosingScripts
    do SendPacket
    loc:poppedup = 1
  end

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web.DivHeader('BrowseWarrantyParts',p_web.combine(p_web.site.style.browsediv,))
  if loc:ParentSilent = 0
    do GenerateBrowse
    if p_web.RequestAjax = 1
      p_web.Script('BrowseProducts.pcv();')
    end
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web.DivFooter()
  do Children
  do Popups
  if loc:poppedup = 0
    do ClosingScripts
  end
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(WARPARTS,wpr:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'WPR:PART_NUMBER') then p_web.SetValue('BrowseWarrantyParts_sort','1')
    ElsIf (loc:vorder = 'WPR:DESCRIPTION') then p_web.SetValue('BrowseWarrantyParts_sort','2')
    ElsIf (loc:vorder = 'WPR:QUANTITY') then p_web.SetValue('BrowseWarrantyParts_sort','3')
    ElsIf (loc:vorder = 'TMP:WARPARTSTATUS') then p_web.SetValue('BrowseWarrantyParts_sort','4')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseWarrantyParts:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseWarrantyParts:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseWarrantyParts:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseWarrantyParts:LookupField')
    loc:selecting = 0
  End

Popups Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  If loc:popup = 0
  End
SetFormAction  Routine
  loc:formaction = 'FormWarrantyParts'
  loc:formactiontarget = '_self'

GotFocusBack   Routine

GenerateBrowse Routine
  data
loc:viewoptions  Long
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  ! Set general Browse options
  loc:NavButtonPosition   = Net:Below
  loc:UpdateButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:FileLoad
  loc:FillBack         = 0
  loc:Sorting          = Net:NoSort
  if p_web.GetValue('_popup_') = 1
    loc:popup = 1
  end
  ! Set Locator Options
  loc:LocatorPosition  = Net:Below
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  p_web.ClearBrowse('BrowseWarrantyParts')
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  p_web.SSV('Hide:InsertButton',0)
  p_web.SSV('Hide:ChangeButton',0)
  p_web.SSV('Hide:DeleteButton',0)
  
  if (p_web.GSV('job:Date_Completed') > 0)
      p_web.SSV('Hide:InsertButton',1)
      p_web.SSV('Hide:DeleteButton',1)
  end ! if (p_web.GSV('job:Date_Completed') > 0)
  
  if (p_web.GSV('job:Invoice_Number_Warranty') > 0)
      p_web.SSV('Hide:InsertButton',1)
      p_web.SSV('Hide:DeleteButton',1)
  end ! if (p_web.GSV('job:Invoice_Number_Warranty') > 0)
  
  if (p_web.GSV('Hide:InsertButton') = 0)
      IF (p_web.GSV('locEngineeringOption') = 'Not Set')
          p_web.SSV('Hide:InsertButton',1)
      ELSE
  
          Access:USERS.Clearkey(use:user_Code_Key)
          use:user_code    = p_web.GSV('job:Engineer')
          if (Access:USERS.TryFetch(use:user_Code_Key) = Level:Benign)
              ! Found
              ! The Site That Attached The Part Must Remove The Part
              if (p_web.GSV('BookingSite') = 'RRC')
                  if (use:location = p_web.GSV('ARC:SiteLocation'))
                      p_web.SSV('Hide:InsertButton',1)
                  end ! if (use:location = p_web.GSV('ARC:SiteLocation'))
              else ! if (p_web.GSV('BookingSite') = 'RRC')
                  if (use:location <> p_web.GSV('ARC:SiteLocation'))
                      p_web.SSV('Hide:InsertButton',1)
                  end ! if (use:location <> p_web.GSV('ARC:SiteLocation'))
              end ! if (p_web.GSV('BookingSite') = 'RRC')
  
  
              Access:LOCATION.Clearkey(loc:location_Key)
              loc:location    = use:Location
              if (Access:LOCATION.TryFetch(loc:location_Key) = Level:Benign)
                  ! Found
                  if (loc:Active = 0)
                      ! can add parts from an inactive location
                      p_web.SSV('Hide:InsertButton',1)
                  end !if (loc:Active = 0)
              else ! if (Access:LOCATION.TryFetch(loc:location_Key) = Level:Benign)
                  ! Error
              end ! if (Access:LOCATION.TryFetch(loc:location_Key) = Level:Benign)
          else ! if (Access:USERS.TryFetch(use:user_Code_Key) = Level:Benign)
              ! Error
              p_web.SSV('Hide:InsertButton',1)
          end ! if (Access:USERS.TryFetch(use:user_Code_Key) = Level:Benign)
      END
  
  end ! if (error# = 0)
  
  ! DBH #10544 - Liquid Damage. Don't amend parts
  IF (p_web.GSV('jobe:Booking48HourOption') = 4) OR (p_web.GSV('Job:ViewOnly') = 1)
      p_web.SSV('Hide:InsertButton',1)
      p_web.SSV('Hide:ChangeBUtton',1)
      p_web.SSV('Hide:DeleteButton',1)
  END ! IF (p_web.GSV('jobe:Booking48HourOption') = 4)
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 10
  End
  loc:ActualSelection = ''
  !loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseWarrantyParts_sort',net:DontEvaluate)
  p_web.SetSessionValue('BrowseWarrantyParts_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(wpr:Part_Number)','-UPPER(wpr:Part_Number)')
    Loc:LocateField = 'wpr:Part_Number'
    Loc:LocatorCase = 0
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(wpr:Description)','-UPPER(wpr:Description)')
    Loc:LocateField = 'wpr:Description'
    Loc:LocatorCase = 0
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'wpr:Quantity','-wpr:Quantity')
    Loc:LocateField = 'wpr:Quantity'
    Loc:LocatorCase = 0
  of 4
    loc:vorder = Choose(Loc:SortDirection=1,'tmp:WARPARTStatus','-tmp:WARPARTStatus')
    Loc:LocateField = 'tmp:WARPARTStatus'
    Loc:LocatorCase = 0
  of 5
    Loc:LocateField = ''
    Loc:LocatorCase = 0
  of 7
    Loc:LocateField = ''
    Loc:LocatorCase = 0
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('wpr:Part_Number')
    loc:SortHeader = p_web.Translate('Part Number')
    p_web.SetSessionValue('BrowseWarrantyParts_LocatorPic','@s30')
  Of upper('wpr:Description')
    loc:SortHeader = p_web.Translate('Description')
    p_web.SetSessionValue('BrowseWarrantyParts_LocatorPic','@s30')
  Of upper('wpr:Quantity')
    loc:SortHeader = p_web.Translate('Quantity')
    p_web.SetSessionValue('BrowseWarrantyParts_LocatorPic','@s8')
  Of upper('tmp:WARPARTStatus')
    loc:SortHeader = p_web.Translate('Status')
    p_web.SetSessionValue('BrowseWarrantyParts_LocatorPic','@s20')
  End
  If loc:selecting = 1
    p_web.GetSettings(p_web.GetSessionValue('Push1'))
    loc:selectaction = p_web.FormSettings.ParentPage !p_web.GetSessionValue('BrowseWarrantyParts:LookupFrom')
  End!Else
  do SendPacket
  do SetFormAction
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseWarrantyParts:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseWarrantyParts:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseWarrantyParts:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="WARPARTS"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="wpr:RecordNumberKey"></input><13,10>'
  end
  if p_web.Translate('Warranty Parts') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.browseHeading,)&'">'&p_web.Translate('Warranty Parts',0)&'</div>'&CRLF
  end
  case p_web.GetValue('refresh') ; of 'next' orof 'previous' orof 'first' orof 'last'
    p_web.Script('try{{BrowseWarrantyParts.restoreFocus();} catch(err){{};')
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseWarrantyParts',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      case loc:locatortype
      of Net:Position
      orof Net:Date
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
      of Net:Contains
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
      of Net:Begins
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
      End
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
      Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
      Case Loc:LocatorType
      of Net:Date
        packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator2BrowseWarrantyParts','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('BrowseWarrantyParts_LocatorPic'),,,'onchange="BrowseWarrantyParts.locate(''Locator2BrowseWarrantyParts'',this.value);"',,0,,,) & '</div></td>'
      Else
        packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator2BrowseWarrantyParts',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="BrowseWarrantyParts.locate(''Locator2BrowseWarrantyParts'',this.value);" ',,,,,,) & '</td>'
      End
      If loc:LocatorSearchButton or loc:LocatorClearButton
        packet = clip(packet) & '<td><div id="BrowseWarrantyParts_locate_a" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
        If loc:LocatorSearchButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
        End
        If loc:LocatorClearButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseWarrantyParts.cl(''BrowseWarrantyParts'');')
        End
        packet = clip(packet) & '</div><13,10></td><13,10>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
      If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'BrowseWarrantyParts_locate_a','buttonset',loc:options)
      End
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & p_web.DivHeader('BrowseWarrantyParts_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'','sortable')&'" id="BrowseWarrantyParts_tbl">'
  Else
    packet = clip(packet) & p_web.DivHeader('BrowseWarrantyParts_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'')&'" id="BrowseWarrantyParts_tbl">'
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'1','BrowseWarrantyParts',p_web.Translate('Part Number'),,,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'2','BrowseWarrantyParts',p_web.Translate('Description'),,,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'3','BrowseWarrantyParts',p_web.Translate('Quantity'),,,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'4','BrowseWarrantyParts',p_web.Translate('Status'),,,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
  If (p_web.GSV('Hide:ChangeButton') <> 1 AND wpr:Part_Number <> 'EXCH') AND  true ! [A]
      If loc:Selecting = 0
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'5','BrowseWarrantyParts',p_web.Translate(),,,,,,,0,loc:Sorting,'Button')
          do AddPacket
          loc:columns += 1
      End ! Selecting
  End ! Field condition [A]
  If (p_web.GSV('Hide:DeleteButton') = 0) AND  true ! [A]
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'7','BrowseWarrantyParts',p_web.Translate(),,,,,,,0,loc:Sorting,'Button')
          do AddPacket
          loc:columns += 1
  End ! Field condition [A]
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,10,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  If Instring('wpr:record_number',lower(loc:vorder),1,1) = 0 !and WARPARTS{prop:SQLDriver} = 1
    loc:vorder = Choose(loc:vorder='','wpr:Record_Number',clip(loc:vorder) & ',' & 'wpr:Record_Number')
  End
  Loc:Selected = Choose(p_web.IfExistsValue('wpr:Record_Number'),p_web.GetValue('wpr:Record_Number'),p_web.GetSessionValue('wpr:Record_Number'))
  ThisView{prop:order} = p_web.CleanFilter(ThisView,clip(loc:vorder))
      loc:FilterWas = 'wpr:Ref_Number = ' & p_web.GetSessionValue('job:Ref_Number')
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseWarrantyParts',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseWarrantyParts_Filter')
    p_web.SetSessionValue('BrowseWarrantyParts_FirstValue','')
    p_web.SetSessionValue('BrowseWarrantyParts_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,WARPARTS,wpr:RecordNumberKey,loc:PageRows,'BrowseWarrantyParts',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,loc:ViewOptions,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled,Loc:LocatorCase)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or Loc:LocatorType = Net:Date or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position or loc:LocatorType = Net:Date
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If WARPARTS{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(WARPARTS,loc:firstvalue)
              Reset(ThisView,WARPARTS)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If WARPARTS{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(WARPARTS,loc:lastvalue)
            Reset(ThisView,WARPARTS)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      tmp:WARPARTStatus = getPartStatus('W')!Work out the colour
      
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(wpr:Record_Number)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td class="'&p_web.combine(p_web.site.style.BrowseEmpty,)&'">'&p_web.Translate('No Warranty Parts')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:NavButtonPosition=Net:Above or (loc:NavButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseWarrantyParts_nav_a" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseWarrantyParts.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseWarrantyParts.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseWarrantyParts.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseWarrantyParts.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseWarrantyParts_nav_a','buttonset',loc:options)
          End
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:UpdateButtonPosition=Net:Above or (loc:UpdateButtonPosition=Net:Both and loc:found > 0))
    packet = clip(packet) & '<div id="BrowseWarrantyParts_update_a" class="'&p_web.combine(p_web.site.style.BrowseUpdateButtonSet,)&'">'
    If loc:selecting = 0 or loc:popup
      if p_web.GSV('Hide:InsertButton') = 0 and loc:viewOnly = 0
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:InsertButton,'BrowseWarrantyParts',,,loc:FormPopup,'FormWarrantyParts')
          TableQueue.Kind = Net:BeforeTable
          do AddPacket
      End
    End
    If loc:found
          TableQueue.Kind = Net:BeforeTable
          do AddPacket
    End
    packet = clip(packet) & '</div><13,10>'
    If p_web.site.UseUpdateButtonSet
      loc:options = ''
      packet = clip(packet) & p_web.jQuery('#' & 'BrowseWarrantyParts_update_a','buttonset',loc:options)
    End ! If p_web.site.UseUpdateButtonSet
      TableQueue.Kind = Net:BeforeTable
      do AddPacket
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseWarrantyParts',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseWarrantyParts_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseWarrantyParts_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          case loc:locatortype
          of Net:Position
          orof Net:Date
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
          of Net:Contains
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
          of Net:Begins
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
          End
          packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
          Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
          Case Loc:LocatorType
          of Net:Date
            packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator1BrowseWarrantyParts','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('BrowseWarrantyParts_LocatorPic'),,,'onchange="BrowseWarrantyParts.locate(''Locator1BrowseWarrantyParts'',this.value);"',,0,,,) & '</div></td>'
          Else
            packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator1BrowseWarrantyParts',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="BrowseWarrantyParts.locate(''Locator1BrowseWarrantyParts'',this.value);" ',,,,,,) & '</td>'
          End
          If loc:LocatorSearchButton or loc:LocatorClearButton
            packet = clip(packet) & '<td><div id="BrowseWarrantyParts_locate_b" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
            If loc:LocatorSearchButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
            End
            If loc:LocatorClearButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseWarrantyParts.cl(''BrowseWarrantyParts'');')
            End
            packet = clip(packet) & '</div><13,10></td><13,10>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
          If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseWarrantyParts_locate_b','buttonset',loc:options)
          End
      End
    End
  End
  p_web.SetSessionValue('BrowseWarrantyParts_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseWarrantyParts_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:NavButtonPosition=Net:Below or loc:NavButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseWarrantyParts_nav_b" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseWarrantyParts.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseWarrantyParts.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseWarrantyParts.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseWarrantyParts.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseWarrantyParts_nav_b','buttonset',loc:options)
          End
        end
        do SendPacket
  End
  If loc:UpdateButtonPosition=Net:Below or loc:UpdateButtonPosition=Net:Both
  packet = clip(packet) & '<div id="BrowseWarrantyParts_update_b" class="'&p_web.combine(p_web.site.style.BrowseUpdateButtonSet,)&'">'
  If loc:selecting = 0 or loc:popup
    if p_web.GSV('Hide:InsertButton') = 0 and loc:viewOnly = 0
          packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:InsertButton,'BrowseWarrantyParts',,,loc:FormPopup,'FormWarrantyParts')
        do SendPacket
    End
  End
  If loc:found
        do SendPacket
  End
  packet = clip(packet) & '</div><13,10>'
  If p_web.site.UseUpdateButtonSet
    loc:options = ''
    packet = clip(packet) & p_web.jQuery('#' & 'BrowseWarrantyParts_update_b','buttonset',loc:options)
  End ! If p_web.site.UseUpdateButtonSet
    do SendPacket
  End
      IF (p_web.GSV('Hide:InsertButton') = 0)
          packet = clip(packet) & p_web.br
          Packet = clip(Packet) & |
              p_web.CreateButton('button','Adjustment','Adjustment','MainButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormWarrantyParts?&Insert_btn=Insert&adjustment=1')) & ''','''&clip('_self')&''')',,0,,,,,)
          packet = clip(packet) & p_web.br
          Do SendPacket
      END
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
  data
loc:rowclick  string(1024)
  code
    loc:field = p_web.AddBrowseValue('BrowseWarrantyParts','WARPARTS',wpr:RecordNumberKey) !wpr:Record_Number
    p_web._thisrow = p_web._nocolon('wpr:Record_Number')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if Loc:LocatorValue <> '' and loc:ActualSelection = ''
        loc:checked = 'checked'
        do SetSelection
      elsif loc:ActualSelection = '' and wpr:Record_Number = p_web.GetValue('wpr:Record_Number')
         loc:checked = 'checked'
         do SetSelection
      elsif loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseWarrantyParts:LookupField')) = wpr:Record_Number and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      else
        loc:checked = Choose((wpr:Record_Number = loc:selected) and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      end
      If(loc:SelectionMethod  = Net:Radio)
        loc:RowStyle = 'class="' & p_web.combine(p_web.site.style.browserow,) &'"'
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'data-nt-id="'& p_web.AddBrowseValue('BrowseWarrantyParts','WARPARTS',wpr:RecordNumberKey) &'" onclick="'
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If WARPARTS{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(WARPARTS)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            loc:DefaultSelection = loc:field
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If WARPARTS{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(WARPARTS)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            loc:DefaultSelection = loc:field
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'>'&p_web.CreateInput('radio','_bidv_',clip(loc:field),,loc:checked,'')&'</td>'&CRLF
          If loc:DefaultSelection = ''
            loc:DefaultSelection = loc:field
          End
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          If loc:DefaultSelection = '' or loc:direction < 0
            loc:DefaultSelection = loc:field
          End
        End
    end ! loc:eip = 0
      !Show hide Buttons
      error# = 0
      if (wpr:Status = 'RET' or wpr:Status = 'RTS')
          if (securityCheckFailed(p_web.GSV('BookingUserPassword'),'JOB - DELETE PART AW RETURN'))
              p_web.SSV('Hide:DeleteButton',1)
          end ! if (securityCheckFailed(p_web.GSV('BookingUserPassword'),'JOB - DELETE PART AW RETURN'))
      end ! if (wpr:Status = 'RET' or wpr:Status = 'RTS')
      
      if (p_web.GSV('Hide:DeleteButton') = 0)
          Access:USERS.Clearkey(use:user_Code_Key)
          use:user_code    = p_web.GSV('job:Engineer')
          if (Access:USERS.TryFetch(use:user_Code_Key) = Level:Benign)
              ! Found
              ! The Site That Attached The Part Must Remove The Part
              if (p_web.GSV('BookingSite') = 'RRC')
                  if (use:location = p_web.GSV('ARC:SiteLocation'))
                      p_web.SSV('Hide:DeleteButton',1)
                  end ! if (use:location = p_web.GSV('ARC:SiteLocation'))
              else ! if (p_web.GSV('BookingSite') = 'RRC')
                  if (use:location <> p_web.GSV('ARC:SiteLocation'))
                      p_web.SSV('Hide:DeleteButton',1)
                  end ! if (use:location <> p_web.GSV('ARC:SiteLocation'))
              end ! if (p_web.GSV('BookingSite') = 'RRC')
          else ! if (Access:USERS.TryFetch(use:user_Code_Key) = Level:Benign)
              ! Error
              p_web.SSV('Hide:DeleteButton',1)
          end ! if (Access:USERS.TryFetch(use:user_Code_Key) = Level:Benign)
      end ! if (error# = 0)
      
      !if (p_web.GSV('Hide:DeleteButton') = 0)
      !    if (wpr:Part_Number = 'EXCH')
      !        ! Can't delete exchange button
      !        p_web.SSV('Hide:DeleteButton',1)
      !    end ! if (wpr:Part_Number = 'EXCH')
      !end ! if (error# = 0)
      
      if (p_web.GSV('Hide:DeleteButton') = 0)
          ! Job Complete, don't let delete.
          if (p_web.GSV('job:Date_Completed') > 0)
              p_web.SSV('Hide:DeleteButton',1)
          end ! if (p_web.GSV('job:Date_Completed') > 0)
      
          if (p_web.GSV('job:Invoice_Number_Warranty') > 0)
              p_web.SSV('Hide:DeleteButton',1)
          end ! if (p_web.GSV('job:Invoice_Number_Warranty') > 0)
      end ! if (error# = 0)
      
          If Loc:Eip = 0
              packet = clip(packet) & '<td width="'&clip(100)&'"><13,10>'
          end ! loc:eip = 0
          do value::wpr:Part_Number
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td width="'&clip(200)&'"><13,10>'
          end ! loc:eip = 0
          do value::wpr:Description
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
            if false
            elsif wpr:Correction = 1
              packet = clip(packet) & '<td class="'&p_web.combine('CenterJustify',)&'"><13,10>'
            else
              packet = clip(packet) & '<td class="'&p_web.combine('CenterJustify')&'"><13,10>'
            end  !False
          end ! loc:eip = 0
          do value::wpr:Quantity
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
            if false
            elsif tmp:WARPARTStatus = 'Requested'
              packet = clip(packet) & '<td class="'&p_web.combine('GreenRegular')&'"><13,10>'
            elsif tmp:WARPARTStatus = 'Picked'
              packet = clip(packet) & '<td class="'&p_web.combine('BlueRegular')&'"><13,10>'
            elsif tmp:WARPARTStatus = 'On Order'
              packet = clip(packet) & '<td class="'&p_web.combine('PurpleRegular')&'"><13,10>'
            elsif tmp:WARPARTStatus = 'Awaiting Picking'
              packet = clip(packet) & '<td class="'&p_web.combine('PinkRegular')&'"><13,10>'
            elsif tmp:WARPARTStatus = 'Awaiting Return'
              packet = clip(packet) & '<td class="'&p_web.combine('OrangeRegular')&'"><13,10>'
            else
              packet = clip(packet) & '<td><13,10>'
            end  !False
          end ! loc:eip = 0
          do value::tmp:WARPARTStatus
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      If (p_web.GSV('Hide:ChangeButton') <> 1 AND wpr:Part_Number <> 'EXCH') AND  true
        If Loc:Selecting = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::Edit
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        End     !Selecting
      End ! Field Condition
      If (p_web.GSV('Hide:DeleteButton') = 0) AND  true
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::MyDelete
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      End ! Field Condition
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr ' & clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Idx = p_web.AddBrowseValue('BrowseWarrantyParts','WARPARTS',wpr:RecordNumberKey)
  TableQueue.Id[1] = wpr:Record_Number

ClosingScripts  Routine
  data
Rtn_SecwinProcedureName         string(252)
  code
  If p_web.RequestAjax = 0
    do SetFormAction
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var btiBrowseWarrantyParts;if (btiBrowseWarrantyParts != 1){{var BrowseWarrantyParts=new browseTable(''BrowseWarrantyParts'','''&clip(loc:formname)&|
      ''','''&p_web._jsok('wpr:Record_Number',Net:Parameter)&''',2,'''&clip(loc:divname)&''',1,1,1,'''&|
      clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&|
      p_web.Translate('Are you sure you want to delete this record?')&''','''&clip(loc:ActualSelection)&''','''&clip(loc:selectaction)&''','''&|
      clip(loc:formactiontarget)&''',''FormWarrantyParts'','&loc:popup&',0,'''','''',"' & clip(Rtn_SecwinProcedureName) & '");<13,10>'&|
      'BrowseWarrantyParts.setGreenBar('''&p_web.ColorWeb(p_web.Site.Style.BrowseHighlightColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOneColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseTwoColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOverColor)&''');<13,10>' &|
      'BrowseWarrantyParts.applyGreenBar();btiBrowseWarrantyParts=1};<13,10>' &|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseWarrantyParts')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseWarrantyParts')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseWarrantyParts')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseWarrantyParts')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(WARPARTS)
  p_web._CloseFile(STOCK)
  p_web._CloseFile(USERS)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(WARPARTS)
  Bind(wpr:Record)
  Clear(wpr:Record)
  NetWebSetSessionPics(p_web,WARPARTS)
  p_web._OpenFile(STOCK)
  Bind(sto:Record)
  NetWebSetSessionPics(p_web,STOCK)
  p_web._OpenFile(USERS)
  Bind(use:Record)
  NetWebSetSessionPics(p_web,USERS)

Children Routine
  if loc:selecting = 0
    If p_web.RequestAjax = 0
      do StartChildren
    Else
      do AjaxChildren
    End
  end

AjaxChildren  Routine
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue('wpr:Record_Number',p_web.GetValue('wpr:Record_Number'))
   p_web.DeleteValue('_Clicked')
   loc:found = 1
   do Children
! ----------------------------------------------------------------------------------------
CallRow  Routine
  data
loc:result  long
  code
  do OpenFilesB
  wpr:Record_Number = p_web.GSV('wpr:Record_Number')
  loc:result = p_web._GetFile(WARPARTS,wpr:RecordNumberKey)
  loc:eip = 1
  loc:viewstate = p_web.escape(p_web.Base64Encode(clip(wpr:Record_Number)))
  do BrowseRow
  do SendPacket
  do ClosefilesB

! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(WARPARTS)
  Case upper(p_web.GetValue('_EIPClm'))
  of upper('MyDelete')
    do Validate::MyDelete
  Else
    case p_web.GetValue('_action_')
    of Net:InsertRecord
    orof Net:ChangeRecord
    orof Net:DeleteRecord
    orof Net:CopyRecord
      loc:eip = 0
      do CallBrowse
    end
  End
  do GotFocusBack
  p_web._CloseFile(WARPARTS)
! ----------------------------------------------------------------------------------------
value::wpr:Part_Number   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseWarrantyParts_wpr:Part_Number_'&wpr:Record_Number,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(wpr:Part_Number,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::wpr:Description   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseWarrantyParts_wpr:Description_'&wpr:Record_Number,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(wpr:Description,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::wpr:Quantity   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    elsif wpr:Correction = 1 ! alternative condition for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseWarrantyParts_wpr:Quantity_'&wpr:Record_Number,p_web.combine('CenterJustify',),net:crc,,loc:extra)
              packet = clip(packet) &  p_web.CreateHyperLink(Left(Format('COR','@s8')),,,,loc:javascript,,,0)
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseWarrantyParts_wpr:Quantity_'&wpr:Record_Number,'CenterJustify',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(wpr:Quantity,'@s8')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::tmp:WARPARTStatus   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    elsif tmp:WARPARTStatus = 'Requested' ! alternative condition for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseWarrantyParts_tmp:WARPARTStatus_'&wpr:Record_Number,,net:crc,,loc:extra)
              packet = clip(packet) &  p_web.CreateHyperLink(Left(Format(tmp:WARPARTStatus,'@s20')),,,,loc:javascript,,,0)
    elsif tmp:WARPARTStatus = 'Picked' ! alternative condition for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseWarrantyParts_tmp:WARPARTStatus_'&wpr:Record_Number,,net:crc,,loc:extra)
              packet = clip(packet) &  p_web.CreateHyperLink(Left(Format(tmp:WARPARTStatus,'@s20')),,,,loc:javascript,,,0)
    elsif tmp:WARPARTStatus = 'On Order' ! alternative condition for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseWarrantyParts_tmp:WARPARTStatus_'&wpr:Record_Number,,net:crc,,loc:extra)
              packet = clip(packet) &  p_web.CreateHyperLink(Left(Format(tmp:WARPARTStatus,'@s20')),,,,loc:javascript,,,0)
    elsif tmp:WARPARTStatus = 'Awaiting Picking' ! alternative condition for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseWarrantyParts_tmp:WARPARTStatus_'&wpr:Record_Number,,net:crc,,loc:extra)
              packet = clip(packet) &  p_web.CreateHyperLink(Left(Format(tmp:WARPARTStatus,'@s20')),,,,loc:javascript,,,0)
    elsif tmp:WARPARTStatus = 'Awaiting Return' ! alternative condition for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseWarrantyParts_tmp:WARPARTStatus_'&wpr:Record_Number,,net:crc,,loc:extra)
              packet = clip(packet) &  p_web.CreateHyperLink(Left(Format(tmp:WARPARTStatus,'@s20')),,,,loc:javascript,,,0)
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseWarrantyParts_tmp:WARPARTStatus_'&wpr:Record_Number,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(tmp:WARPARTStatus,'@s20')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::Edit   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
  If (p_web.GSV('Hide:ChangeButton') <> 1 AND wpr:Part_Number <> 'EXCH')
    p_web.site.SmallChangeButton.TextValue = p_web.Translate('Edit')
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseWarrantyParts_Edit_'&wpr:Record_Number,,net:crc,,loc:extra)
          If loc:viewonly = 0
             packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallChangeButton,'BrowseWarrantyParts',p_web.AddBrowseValue('BrowseWarrantyParts','WARPARTS',wpr:RecordNumberKey),,loc:FormPopup,'FormWarrantyParts') & '<13,10>'
          End
    End
    p_web.site.SmallChangeButton = p_web.RequestData.WebServer._SitesQueue.Defaults.SmallChangeButton  !bruce
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
  End
! ----------------------------------------------------------------------------------------
Validate::MyDelete  Routine
  data
loc:was                    Any
loc:result                 Long
loc:ok                     Long
loc:lookupdone             Long
  code
  do OpenFilesB
  loc:ViewState = p_web.GetValue('ViewState')
  wpr:Record_Number = p_web.Base64Decode(p_web._Unescape(loc:ViewState,Net:NoPlus))
  loc:result = p_web._GetFile(WARPARTS,wpr:RecordNumberKey)
  p_web.FileToSessionQueue(WARPARTS)
  do ClosefilesB
! ----------------------------------------------------------------------------------------
value::MyDelete   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
  If (p_web.GSV('Hide:DeleteButton') = 0)
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseWarrantyParts_MyDelete_'&wpr:Record_Number,,net:crc,,loc:extra)
      loc:disabled = ''
      packet = clip(packet) & p_web.CreateButton('button','Delete','Delete',p_web.combine(Choose('Delete' <> '',p_web.site.style.BrowseOtherButtonWithText,p_web.site.style.BrowseOtherButtonWithOutText),'SmallButtonIcon'),loc:formname,,,p_web.WindowOpen('FormDeletePart&_bidv_=' & p_web.AddBrowseValue('BrowseWarrantyParts','WARPARTS',wpr:RecordNumberKey) &'&' & p_web._jsok('FromURL=ViewJob&DelType=WAR&ReturnURL=ViewJob') &''&'','_self'),,loc:disabled,,,,,,0,,,,'nt-left') !e
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
  End
OpenFiles  ROUTINE
  p_web._OpenFile(STOCK)
  p_web._OpenFile(USERS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(STOCK)
  p_Web._CloseFile(USERS)
     FilesOpened = False
  END
  return
!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
CheckForDuplicate  Routine
SetSelection  Routine
  loc:ActualSelection = loc:field
  p_web.SetSessionValue('wpr:Record_Number',wpr:Record_Number)

MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:ActualSelection = ''
    loc:ActualSelection = loc:DefaultSelection
    p_web.GetBrowseValue(loc:ActualSelection,Net:Web:SessionQueue) ! so children are primed with correct sessionValue
  End

  If loc:ActualSelection <> ''
    TableQueue.Kind = Net:RowTable
    get(TableQueue,TableQueue.Kind)
    if Errorcode() = 0
      TableQueue.Row = sub(TableQueue.Row,1,len(clip(TableQueue.Row))-1) &  ' data-nt-rows="'&loc:RowsHigh&'" data-nt-value="'&clip(loc:ActualSelection)&'"' & '>'
      Put(TableQueue)
    End
  End

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead class="'&p_web.combine(p_web.site.style.BrowseHeader,'')&'"><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot class="'&p_web.combine(p_web.site.style.BrowseFooter,)&'"><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody class="'&p_web.combine(p_web.site.style.BrowseBody,)&'"><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table><13,10>' & p_web.DivFooter(Net:NoSend) ! Table Div
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
FormDeletePart       PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locPartNumber        STRING(30)                            !
locDescription       STRING(30)                            !
locAlertMessage      STRING(255)                           !
locErrorMessage      STRING(255)                           !
locScrapRestock      BYTE                                  !
tmp:delete           BYTE                                  !
tmp:Scrap            BYTE                                  !
FilesOpened     Long
STOCKALL::State  USHORT
ESTPARTS::State  USHORT
STOFAULT::State  USHORT
STOCK::State  USHORT
PARTS::State  USHORT
JOBS::State  USHORT
WARPARTS::State  USHORT
locPartNumber:IsInvalid  Long
locDescription:IsInvalid  Long
locAlertMessage:IsInvalid  Long
locErrorMessage:IsInvalid  Long
locScrapRestock:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
                    Map
AddSolder               Procedure(String fType)
ShowAlert               Procedure(String fAlert)
RemoveWarrantyPartStockHistory      Procedure(String fInformation,STRING fType)
                    end
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormDeletePart')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'FormDeletePart_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('FormDeletePart','')
    p_web.DivHeader('FormDeletePart',p_web.combine(p_web.site.style.formdiv,))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('FormDeletePart') = 0
        p_web.AddPreCall('FormDeletePart')
        p_web.DivHeader('popup_FormDeletePart','nt-hidden')
        p_web.DivHeader('FormDeletePart',p_web.combine(p_web.site.style.formdiv,))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_FormDeletePart_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_FormDeletePart_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormDeletePart',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_FormDeletePart',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormDeletePart',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_FormDeletePart',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormDeletePart',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormDeletePart',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_FormDeletePart',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormDeletePart',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_FormDeletePart',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormDeletePart',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormDeletePart',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormDeletePart',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('FormDeletePart')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(STOCKALL)
  p_web._OpenFile(ESTPARTS)
  p_web._OpenFile(STOFAULT)
  p_web._OpenFile(STOCK)
  p_web._OpenFile(PARTS)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(WARPARTS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(STOCKALL)
  p_Web._CloseFile(ESTPARTS)
  p_Web._CloseFile(STOFAULT)
  p_Web._CloseFile(STOCK)
  p_Web._CloseFile(PARTS)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(WARPARTS)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FormDeletePart_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'FormDeletePart'
    end
    p_web.formsettings.proc = 'FormDeletePart'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  if p_web.IfExistsValue('locPartNumber') = 0
    p_web.SetSessionValue('locPartNumber',locPartNumber)
  Else
    locPartNumber = p_web.GetSessionValue('locPartNumber')
  End
  if p_web.IfExistsValue('locDescription') = 0
    p_web.SetSessionValue('locDescription',locDescription)
  Else
    locDescription = p_web.GetSessionValue('locDescription')
  End
  if p_web.IfExistsValue('locAlertMessage') = 0
    p_web.SetSessionValue('locAlertMessage',locAlertMessage)
  Else
    locAlertMessage = p_web.GetSessionValue('locAlertMessage')
  End
  if p_web.IfExistsValue('locErrorMessage') = 0
    p_web.SetSessionValue('locErrorMessage',locErrorMessage)
  Else
    locErrorMessage = p_web.GetSessionValue('locErrorMessage')
  End
  if p_web.IfExistsValue('locScrapRestock') = 0
    p_web.SetSessionValue('locScrapRestock',locScrapRestock)
  Else
    locScrapRestock = p_web.GetSessionValue('locScrapRestock')
  End

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locPartNumber')
    locPartNumber = p_web.GetValue('locPartNumber')
    p_web.SetSessionValue('locPartNumber',locPartNumber)
  Else
    locPartNumber = p_web.GetSessionValue('locPartNumber')
  End
  if p_web.IfExistsValue('locDescription')
    locDescription = p_web.GetValue('locDescription')
    p_web.SetSessionValue('locDescription',locDescription)
  Else
    locDescription = p_web.GetSessionValue('locDescription')
  End
  if p_web.IfExistsValue('locAlertMessage')
    locAlertMessage = p_web.GetValue('locAlertMessage')
    p_web.SetSessionValue('locAlertMessage',locAlertMessage)
  Else
    locAlertMessage = p_web.GetSessionValue('locAlertMessage')
  End
  if p_web.IfExistsValue('locErrorMessage')
    locErrorMessage = p_web.GetValue('locErrorMessage')
    p_web.SetSessionValue('locErrorMessage',locErrorMessage)
  Else
    locErrorMessage = p_web.GetSessionValue('locErrorMessage')
  End
  if p_web.IfExistsValue('locScrapRestock')
    locScrapRestock = p_web.GetValue('locScrapRestock')
    p_web.SetSessionValue('locScrapRestock',locScrapRestock)
  Else
    locScrapRestock = p_web.GetSessionValue('locScrapRestock')
  End

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('FormDeletePart_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.GSV('ReturnURL')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormDeletePart_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormDeletePart_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormDeletePart_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = p_web.GSV('ReturnURL')

GenerateForm   Routine
  do LoadRelatedRecords
  p_web.site.SaveButton.TextValue = 'Delete'
  p_web.site.SaveButton.Image = 'images/pdelete.png'
  
  p_web.SSV('DeletePart:ViewOnly',0)
  p_web.SSV('Hide:ScrapRestock',0)
  p_web.SSV('locAlertMessage','Click "Delete" to confirm deletion.')
  p_web.SSV('locErrorMessage','')
  p_web.SSV('Hide:ScrapRestock',1)
  p_web.SSV('locScrapRestock',0)
  
  IF (p_web.IfExistsValue('ReturnURL'))
      p_web.StoreValue('ReturnURL')
  END
  IF (p_web.IfExistsValue('FromURL'))
      p_web.StoreValue('FromURL')
  END
  
  If p_web.IfExistsValue('DelType')
      p_web.StoreValue('DelType')
  End ! If p_web.IfExistsValue('a')
  
  IF (p_web.IfExistsSessionValue('stl:RecordNumber') AND p_web.GSV('FromURL') = 'StockAllocation')
  
      Access:STOCKALL.Clearkey(stl:RecordNumberKey)
      stl:RecordNumber    = p_web.GSV('stl:RecordNumber')
      Access:STOCKALL.Tryfetch(stl:RecordNumberKey)
  
  !    Access:STOCKALL.ClearKey(stl:RecordNumberKey)
  !    stl:RecordNumber = p_web.GSV('AllocRecNo')
  !    IF (Access:STOCKALl.TryFetch(stl:RecordNumberKey))
  !    END
  
      p_web.SSV('DelType',stl:PartType)
      CASE p_web.GSV('DelType')
      OF 'WAR'
          p_web.SSV('wpr:Record_Number',stl:PartRecordNumber)
      OF 'EST'
          p_web.SSV('est:Record_Number',stl:PartRecordNumber)
      OF 'CHA'
          p_web.SSV('par:Record_Number',stl:PartRecordNumber)
      END
      Access:JOBS.ClearKey(job:Ref_Number_Key)
      job:Ref_Number = stl:JobNumber
      IF (Access:JOBS.TryFetch(job:Ref_Number_Key)= Level:Benign)
          p_web.FileToSessionQueue(JOBS)
      END
  
  
  ELSE
  
      If p_web.IfExistsValue('wpr:Record_Number')
          p_web.StoreValue('wpr:Record_Number')
      End ! If p_web.IfExistsValue('a')
  
      If p_web.IfExistsValue('par:Record_Number')
          p_web.StoreValue('par:Record_Number')
      End ! If p_web.IfExistsValue('a')
  
  END
  
  
  Case p_web.GSV('DelType')
  OF 'WAR'
      Access:WARPARTS.Clearkey(wpr:RecordNumberKey)
      wpr:Record_Number = p_web.GSV('wpr:Record_Number')
      If (Access:WARPARTS.TryFetch(wpr:RecordNumberKey) = Level:Benign)
  
      End ! End
      p_web.SSV('locPartNumber',wpr:Part_Number)
      p_web.SSV('locDescription',wpr:Description)
  
      IF (wpr:Part_Number = 'EXCH')
          IF (p_web.GSV('job:Exchange_Unit_Number') > 0) ! #11937 Allow to delete an "EXCH" entry if there is no exchange unit attached. (Bryan: 19/01/2011)
              p_web.SSV('locErrorMessage','Click the "Exchange Unit" button on the Amend Job to remove an exchange unit')
          END !IF (p_web.GSV('job:Exchange_Unit_Number') > 0)
      ELSE
          Access:STOCK.ClearKey(sto:Ref_Number_Key)
          sto:Ref_Number = wpr:Part_Ref_Number
          IF (Access:STOCK.TryFetch(sto:Ref_Number_Key))
          END
  
          IF (p_web.GSV('BookingSite') = 'RRC')
              IF (sto:Location = p_web.GSV('ARC:SiteLocation'))
                  p_web.SSV('locErrorMessage','Cannot delete! This part was added at the ARC')
              ELSE
                  IF (p_web.GSV('job:Invoice_Number_Warranty') <> 0)
                      p_web.SSV('locErrorMessage','Cannot delete! The selected job has been invoiced.')
                  END
              END
          ELSE
              IF (sto:Location <> p_web.GSV('ARC:SiteLocation'))
                  p_web.SSV('locErrorMessage','Cannot Delete! This part was added at the RRC')
              ELSE
                  IF (p_web.GSV('job:Invoice_Number_Warranty') <> 0)
                      p_web.SSV('locErrorMessage','Cannot delete! The selected job has been invoiced.')
                  END
              END
          END
  
          IF (wpr:WebOrder)
              p_web.SSV('locAlertMessage','Warning! A Retail Order has been raised for this part.')
          ELSE
              IF (wpr:Adjustment = 'YES')
              ELSE
                  If (sto:Ref_Number > 0)
                      IF (sto:Sundry_Item = 'YES')
                      ELSE
                          IF (RapidLocation(sto:Location) AND p_web.GSV('FromURL') <> 'StockAllocation')
                              IF (sto:AttachBySolder)
                                  p_web.SSV('locAlertMessage','The selected part is "Attached By Solder" and therefore will be scrapped.')
                              ELSE ! IF (sto:AttachBySolder)
                                  IF (wpr:PartAllocated = 0)
                                      p_web.SSV('locAlertMessage','Warning! This part has not yet been allocated.')
                                  ELSE
                                      p_web.SSV('locAlertMessage','This part will be marked for return by allocations.')
                                  END
                              END ! IF (sto:AttachBySolder)
                          ELSE ! IF (RapidLocation(sto:Location))
                              p_web.SSV('locAlertMessage','Select "SCRAP" or "RESTOCK".')
                              p_web.SSV('Hide:ScrapRestock',0)
                          END ! IF (RapidLocation(sto:Location))
  
                      END
  
                  END
  
  
              END
          END
      END
  
  
  OF 'CHA'
      error# = 0
      Access:PARTS.ClearKey(par:recordnumberkey)
      par:Record_Number = p_web.GSV('par:Record_Number')
      IF (Access:PARTS.TryFetch(par:recordnumberkey) = Level:Benign)
      END
      p_web.SSV('locPartNumber',par:Part_Number)
      p_web.SSV('locDescription',par:Description)
  
      IF (par:Part_Number = 'EXCH')
          IF (p_web.GSV('job:Exchange_Unit_Number') > 0) ! #11937 Allow to delete an "EXCH" entry if there is no exchange unit attached. (Bryan: 19/01/2011)
              p_web.SSV('locErrorMessage','Click the "Exchange Unit" button on the Amend Job screen to remove an exchange unit')
              error# = 1
          END
  
      ELSE
          if (par:Status = 'RTS')
              if (securityCheckFailed(p_web.GSV('BookingUserPassword'),'JOB - DELETE PART AW RETURN'))
                  p_web.SSV('locErrorMessage','You do not have access to delete this part')
                  error# = 1
              end ! if
          end ! if
          if (error# = 0)
              Access:STOCK.Clearkey(sto:Ref_Number_Key)
              sto:Ref_Number = par:Part_Ref_Number
              if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
              end ! if
  
              IsJobInvoiced(p_web)
              IF (p_web.GSV('IsJobInvoiced') = 1)
                  p_web.SSV('locErrorMessage','Cannot delete! The selected job has been invoiced.')
                  ERROR# = 1
              ELSE
                  IF (p_web.GSV('BookingSite') = 'RRC')
                      IF (sto:Location = p_web.GSV('ARC:SiteLocation'))
                          p_web.SSV('locErrorMessage','Cannot delete! This part was added at the ARC')
                          ERROR# = 1
                      END
                  ELSE
                      IF (sto:Location <> p_web.GSV('ARC:SiteLocation'))
                          p_web.SSV('locErrorMessage','Cannot Delete! This part was added at the RRC')
                          ERROR#= 1
                      END
                  END
              END
          end ! if
          IF (ERROR# = 0)
              IF (par:WebOrder)
                  p_web.SSV('locAlertMessage','Warning! A Retail Order has been raised for this part.')
              ELSE
                  IF (par:Adjustment = 'YES')
                  ELSE
                      If (sto:Ref_Number > 0)
                          IF (sto:Sundry_Item = 'YES')
                          ELSE
                              IF (RapidLocation(sto:Location) AND p_web.GSV('FromURL') <> 'StockAllocation')
                                  IF (sto:AttachBySolder)
                                      p_web.SSV('locAlertMessage','The selected part is "Attached By Solder" and therefore will be scrapped.')
                                  ELSE ! IF (sto:AttachBySolder)
                                      IF (par:PartAllocated = 0)
                                          p_web.SSV('locAlertMessage','Warning! This part has not yet been allocated.')
                                      ELSE
                                          p_web.SSV('locAlertMessage','This part will be marked for return by allocations.')
                                      END
                                  END ! IF (sto:AttachBySolder)
                              ELSE ! IF (RapidLocation(sto:Location))
                                  p_web.SSV('locAlertMessage','Select "SCRAP" or "RESTOCK".')
                                  p_web.SSV('Hide:ScrapRestock',0)
                              END ! IF (RapidLocation(sto:Location))
  
                          END
  
                      END
  
  
                  END
              END
          END ! if
  
  
      END ! IF
  OF 'EST'
      error# = 0
      Access:ESTPARTS.ClearKey(epr:record_number_key)
      epr:Record_Number = p_web.GSV('epr:Record_Number')
      IF (Access:ESTPARTS.TryFetch(epr:record_number_key) = Level:Benign)
      END
      p_web.SSV('locPartNumber',epr:Part_Number)
      p_web.SSV('locDescription',epr:Description)
  
      if (epr:Status = 'RTS')
          if (securityCheckFailed(p_web.GSV('BookingUserPassword'),'JOB - DELETE PART AW RETURN'))
              p_web.SSV('locErrorMessage','You do not have access to delete this part')
              error# = 1
          end ! if
      end ! if
      if (error# = 0)
          Access:STOCK.Clearkey(sto:Ref_Number_Key)
          sto:Ref_Number = epr:Part_Ref_Number
          if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
          end ! if
  
          if (p_web.GSV('job:Estimate_Accepted') = 'YES')
              p_web.SSV('locErrorMessage','Cannot delete! The estimate has been accepted.')
              error# = 1
          else
              if (p_web.GSV('job:Estimate_Rejected') = 'YES')
                  p_web.SSV('locErrorMessage','Cannot delete! The estimate has been rejected.')
                  error# = 1
              end
          end
      end ! if
      IF (ERROR# = 0)
              IF (epr:Adjustment = 'YES' or epr:UsedOnRepair = 0)
                  IF (sto:AttachBySolder)
                      p_web.SSV('locAlertMessage','The selected part is "Attached By Solder" and therefore will be scrapped.')
                  END
              ELSE
                  If (sto:Ref_Number > 0)
                      IF (sto:Sundry_Item = 'YES')
                      ELSE
                          IF (RapidLocation(sto:Location) AND p_web.GSV('FromURL') <> 'StockAllocation')
                              IF (sto:AttachBySolder)
                                  p_web.SSV('locAlertMessage','The selected part is "Attached By Solder" and therefore will be scrapped.')
                              ELSE ! IF (sto:AttachBySolder)
                                  IF (epr:PartAllocated = 0)
                                      p_web.SSV('locAlertMessage','Warning! This part has not yet been allocated.')
                                  ELSE
                                      p_web.SSV('locAlertMessage','This part will be marked for return by allocations.')
                                  END
                              END ! IF (sto:AttachBySolder)
                          ELSE ! IF (RapidLocation(sto:Location))
                              p_web.SSV('locAlertMessage','Select "SCRAP" or "RESTOCK".')
                              p_web.SSV('Hide:ScrapRestock',0)
                          END ! IF (RapidLocation(sto:Location))
                      END
                  END
              END
      END ! if
  
  ELSE
  
  END
  
  IF (p_web.GSV('locErrorMessage') <> '')
      p_web.SSV('locAlertMessage','')
      ShowALert(p_web.GSV('locErrorMessage'))
      p_web.SSV('DeletePart:ViewOnly',1)
  END
 locPartNumber = p_web.RestoreValue('locPartNumber')
 locDescription = p_web.RestoreValue('locDescription')
 locAlertMessage = p_web.RestoreValue('locAlertMessage')
 locErrorMessage = p_web.RestoreValue('locErrorMessage')
 locScrapRestock = p_web.RestoreValue('locScrapRestock')
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
    loc:viewonly = Choose(p_web.GSV('DeletePart:ViewOnly') = 1,1,0)
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Delete Part') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Delete Part',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_FormDeletePart',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_FormDeletePart0_div')&'">'&p_web.Translate('Part Details')&'</a></li>'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_FormDeletePart1_div')&'">'&p_web.Translate()&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  do GenerateTab1
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="FormDeletePart_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.cancel();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CancelButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="FormDeletePart_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'FormDeletePart_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="FormDeletePart_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'FormDeletePart_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_FormDeletePart')>0,p_web.GSV('showtab_FormDeletePart'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_FormDeletePart'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_FormDeletePart') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_FormDeletePart'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_FormDeletePart')>0,p_web.GSV('showtab_FormDeletePart'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_FormDeletePart') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Part Details') & ''''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_FormDeletePart_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_FormDeletePart')>0,p_web.GSV('showtab_FormDeletePart'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"FormDeletePart",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_FormDeletePart')>0,p_web.GSV('showtab_FormDeletePart'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_FormDeletePart_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('FormDeletePart') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('FormDeletePart')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Part Details')&'</a></h3>' & CRLF & p_web.DivHeader('tab_FormDeletePart0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_FormDeletePart0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_FormDeletePart0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_FormDeletePart0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Part Details')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_FormDeletePart0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Part Details')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_FormDeletePart0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Part Details')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_FormDeletePart0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locPartNumber
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locPartNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locPartNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locDescription
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locDescription
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locDescription
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab1  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate()&'</a></h3>' & CRLF & p_web.DivHeader('tab_FormDeletePart1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_FormDeletePart1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_FormDeletePart1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_FormDeletePart1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,)
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_FormDeletePart1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend)
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_FormDeletePart1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain">' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_FormDeletePart1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::locAlertMessage
        do Comment::locAlertMessage
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::locErrorMessage
        do Comment::locErrorMessage
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locScrapRestock
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locScrapRestock
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locScrapRestock
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Prompt::locPartNumber  Routine
  packet = clip(packet) & p_web.DivHeader('FormDeletePart_' & p_web._nocolon('locPartNumber') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Part Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locPartNumber  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locPartNumber = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locPartNumber = p_web.GetValue('Value')
  End
  do ValidateValue::locPartNumber  ! copies value to session value if valid.
  do Value::locPartNumber
  do SendAlert
  do Comment::locPartNumber ! allows comment style to be updated.

ValidateValue::locPartNumber  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locPartNumber',locPartNumber).
    End

Value::locPartNumber  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormDeletePart_' & p_web._nocolon('locPartNumber') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  If loc:retrying
    locPartNumber = p_web.RestoreValue('locPartNumber')
    do ValidateValue::locPartNumber
    If locPartNumber:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- locPartNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locPartNumber'',''formdeletepart_locpartnumber_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locPartNumber',p_web.GetSessionValueFormat('locPartNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locPartNumber  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locPartNumber:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormDeletePart_' & p_web._nocolon('locPartNumber') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locDescription  Routine
  packet = clip(packet) & p_web.DivHeader('FormDeletePart_' & p_web._nocolon('locDescription') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Description'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locDescription  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locDescription = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locDescription = p_web.GetValue('Value')
  End
  do ValidateValue::locDescription  ! copies value to session value if valid.
  do Value::locDescription
  do SendAlert
  do Comment::locDescription ! allows comment style to be updated.

ValidateValue::locDescription  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locDescription',locDescription).
    End

Value::locDescription  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormDeletePart_' & p_web._nocolon('locDescription') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  If loc:retrying
    locDescription = p_web.RestoreValue('locDescription')
    do ValidateValue::locDescription
    If locDescription:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- locDescription
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locDescription'',''formdeletepart_locdescription_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locDescription',p_web.GetSessionValueFormat('locDescription'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locDescription  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locDescription:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormDeletePart_' & p_web._nocolon('locDescription') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::locAlertMessage  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locAlertMessage = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locAlertMessage = p_web.GetValue('Value')
  End
  do ValidateValue::locAlertMessage  ! copies value to session value if valid.
  do Comment::locAlertMessage ! allows comment style to be updated.

ValidateValue::locAlertMessage  Routine
    If not (p_web.GSV('locAlertMessage') = '')
      if loc:invalid = '' then p_web.SetSessionValue('locAlertMessage',locAlertMessage).
    End

Value::locAlertMessage  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('locAlertMessage') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormDeletePart_' & p_web._nocolon('locAlertMessage') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('locAlertMessage') = '')
  ! --- DISPLAY --- locAlertMessage
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locAlertMessage'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locAlertMessage  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locAlertMessage:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('locAlertMessage') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormDeletePart_' & p_web._nocolon('locAlertMessage') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('locAlertMessage') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::locErrorMessage  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locErrorMessage = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locErrorMessage = p_web.GetValue('Value')
  End
  do ValidateValue::locErrorMessage  ! copies value to session value if valid.
  do Comment::locErrorMessage ! allows comment style to be updated.

ValidateValue::locErrorMessage  Routine
    If not (p_web.GSV('locErrorMessage') = '')
      if loc:invalid = '' then p_web.SetSessionValue('locErrorMessage',locErrorMessage).
    End

Value::locErrorMessage  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('locErrorMessage') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormDeletePart_' & p_web._nocolon('locErrorMessage') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('locErrorMessage') = '')
  ! --- DISPLAY --- locErrorMessage
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('red bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locErrorMessage'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locErrorMessage  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locErrorMessage:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('locErrorMessage') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormDeletePart_' & p_web._nocolon('locErrorMessage') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('locErrorMessage') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locScrapRestock  Routine
  packet = clip(packet) & p_web.DivHeader('FormDeletePart_' & p_web._nocolon('locScrapRestock') & '_prompt',Choose(p_web.GSV('Hide:ScrapRestock') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:ScrapRestock') = 1,'',p_web.Translate('Delete Option'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locScrapRestock  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locScrapRestock = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locScrapRestock = p_web.GetValue('Value')
  End
  do ValidateValue::locScrapRestock  ! copies value to session value if valid.
  do Value::locScrapRestock
  do SendAlert
  do Comment::locScrapRestock ! allows comment style to be updated.

ValidateValue::locScrapRestock  Routine
    If not (p_web.GSV('Hide:ScrapRestock') = 1)
      if loc:invalid = '' then p_web.SetSessionValue('locScrapRestock',locScrapRestock).
    End

Value::locScrapRestock  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:ScrapRestock') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormDeletePart_' & p_web._nocolon('locScrapRestock') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If loc:retrying
    locScrapRestock = p_web.RestoreValue('locScrapRestock')
    do ValidateValue::locScrapRestock
    If locScrapRestock:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:ScrapRestock') = 1)
  ! --- RADIO --- locScrapRestock
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locScrapRestock') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locScrapRestock'',''formdeletepart_locscraprestock_value'',1,'''&p_web._jsok(1)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locScrapRestock',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locScrapRestock_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Scrap') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locScrapRestock') = 2
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locScrapRestock'',''formdeletepart_locscraprestock_value'',1,'''&p_web._jsok(2)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locScrapRestock',clip(2),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locScrapRestock_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Restock') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locScrapRestock  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locScrapRestock:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:ScrapRestock') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormDeletePart_' & p_web._nocolon('locScrapRestock') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:ScrapRestock') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('FormDeletePart_nexttab_' & 0)
    locPartNumber = p_web.GSV('locPartNumber')
    do ValidateValue::locPartNumber
    If loc:Invalid
      loc:retrying = 1
      do Value::locPartNumber
      !do SendAlert
      do Comment::locPartNumber ! allows comment style to be updated.
      !exit
    End
    locDescription = p_web.GSV('locDescription')
    do ValidateValue::locDescription
    If loc:Invalid
      loc:retrying = 1
      do Value::locDescription
      !do SendAlert
      do Comment::locDescription ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('FormDeletePart_nexttab_' & 1)
    locAlertMessage = p_web.GSV('locAlertMessage')
    do ValidateValue::locAlertMessage
    If loc:Invalid
      loc:retrying = 1
      do Value::locAlertMessage
      !do SendAlert
      do Comment::locAlertMessage ! allows comment style to be updated.
      !exit
    End
    locErrorMessage = p_web.GSV('locErrorMessage')
    do ValidateValue::locErrorMessage
    If loc:Invalid
      loc:retrying = 1
      do Value::locErrorMessage
      !do SendAlert
      do Comment::locErrorMessage ! allows comment style to be updated.
      !exit
    End
    locScrapRestock = p_web.GSV('locScrapRestock')
    do ValidateValue::locScrapRestock
    If loc:Invalid
      loc:retrying = 1
      do Value::locScrapRestock
      !do SendAlert
      do Comment::locScrapRestock ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_FormDeletePart_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('FormDeletePart_tab_' & 0)
    do GenerateTab0
  of lower('FormDeletePart_locPartNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locPartNumber
      of event:timer
        do Value::locPartNumber
        do Comment::locPartNumber
      else
        do Value::locPartNumber
      end
  of lower('FormDeletePart_locDescription_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locDescription
      of event:timer
        do Value::locDescription
        do Comment::locDescription
      else
        do Value::locDescription
      end
  of lower('FormDeletePart_tab_' & 1)
    do GenerateTab1
  of lower('FormDeletePart_locScrapRestock_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locScrapRestock
      of event:timer
        do Value::locScrapRestock
        do Comment::locScrapRestock
      else
        do Value::locScrapRestock
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('FormDeletePart_form:ready_',1)

  p_web.SetSessionValue('FormDeletePart_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_FormDeletePart',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('FormDeletePart_form:ready_',1)
  p_web.SetSessionValue('FormDeletePart_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormDeletePart',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('FormDeletePart_form:ready_',1)
  p_web.SetSessionValue('FormDeletePart_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('FormDeletePart:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('FormDeletePart_form:ready_',1)
  p_web.SetSessionValue('FormDeletePart_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('FormDeletePart:Primed',0)
  p_web.setsessionvalue('showtab_FormDeletePart',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
      If not (p_web.GSV('Hide:ScrapRestock') = 1)
          If p_web.IfExistsValue('locScrapRestock')
            locScrapRestock = p_web.GetValue('locScrapRestock')
          End
      End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormDeletePart_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  BHAddToDebugLog('DeletePart: ' & p_web.GSV('Hide:ScrapRestock') & ' - ' & p_web.GSV('locScrapRestock'))
  
  if (p_web.GSV('Hide:ScrapRestock') <> 1)
      if (p_web.GSV('locScrapRestock') = 0)
          loc:Invalid = 'locPartNumber'
          loc:Alert = 'You must select "Scrap" or "Restock"'
          exit
      ENd
  End
  p_web.DeleteSessionValue('FormDeletePart_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
    loc:InvalidTab += 1
    do ValidateValue::locPartNumber
    If loc:Invalid then exit.
    do ValidateValue::locDescription
    If loc:Invalid then exit.
  ! tab = 3
    loc:InvalidTab += 1
    do ValidateValue::locAlertMessage
    If loc:Invalid then exit.
    do ValidateValue::locErrorMessage
    If loc:Invalid then exit.
    do ValidateValue::locScrapRestock
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
  ! Delete Part
  tmp:Delete = 0
  tmp:Scrap = 0
  Case p_web.GSV('DelType')
  of 'WAR'
      Access:WARPARTS.Clearkey(wpr:RecordNumberKey)
      wpr:Record_Number = p_web.GSV('wpr:Record_Number')
      If (Access:WARPARTS.TryFetch(wpr:RecordNumberKey))
  
      End
  
      Access:STOCK.Clearkey(sto:Ref_Number_Key)
      sto:Ref_Number = wpr:Part_Ref_Number
      If (Access:STOCK.TryFetch(sto:Ref_Number_Key))
      End
  
      if (wpr:WebOrder)
          RemoveFromStockAllocation(wpr:Record_Number,'WAR')
      else
          IF (wpr:Adjustment = 'YES')
              tmp:delete = 1
          ELSE
              IF (sto:Sundry_Item = 'YES')
                  tmp:delete = 1
              ELSE
  
                  IF (RapidLocation(sto:Location) AND p_web.GSV('FromURL') <> 'StockAllocation')
                      if (sto:AttachBySolder)
                          tmp:Delete = 1
                          tmp:Scrap = 1
                          AddSolder('WAR')
                      else ! if (sto:AttachBySolder)
                          if (wpr:PartAllocated = 0)
  
                              sto:Quantity_STock += wpr:Quantity
                              If (Access:STOCK.TryUpdate() = Level:Benign)
                                  tmp:Delete = 1
                                  RemoveWarrantyPartStockHistory('UNALLOCATED PART REMOVED','WAR')
                                  if (sto:Suspend)
                                      ! Not has stock. Unsuspend part
                                      sto:Suspend = 0
                                      If (Access:STOCK.TryUpdate() = Level:Benign)
                                          if (AddToStockHistory(sto:Ref_Number, |
                                              'ADD', |
                                              '', |
                                              0, |
                                              0, |
                                              0, |
                                              wpr:Purchase_Cost, |
                                              wpr:Sale_Cost, |
                                              wpr:Retail_Cost, |
                                              'PART UNSUSPENDED', |
                                              '', |
                                              p_web.GSV('BookingUserCode'),|
                                              sto:Quantity_Stock))
                                          end
  
                                      end
                                  end
                              End
                          else ! if (wpr:PartAllocated = 0)
                              wpr:PartAllocated = 0
                              wpr:Status = 'RET'
                              Access:WARPARTS.TryUpdate()
                              p_web.FileToSessionQueue(WARPARTS)
                                AddToStockAllocation(wpr:Record_Number,'WAR',wpr:Quantity,'RET',p_web.GSV('job:Engineer'),p_web)
                          end
                      end
                  else ! If (RapidLocation(sto:Location))
                      Case p_web.GSV('locScrapRestock')
                      of 1 ! Scrap
                          tmp:Delete = 1
                          tmp:Scrap = 1
                      of 2 ! Restock
                          sto:Quantity_Stock += wpr:Quantity
                          If (Access:STOCK.TryUpdate() = Level:Benign)
                              tmp:Delete = 1
                              RemoveWarrantyPartStockHistory('','WAR')
                              if (sto:Suspend)
                                  ! Not has stock. Unsuspend part
                                  sto:Suspend = 0
                                  If (Access:STOCK.TryUpdate() = Level:Benign)
                                      if (AddToStockHistory(sto:Ref_Number, |
                                          'ADD', |
                                          '', |
                                          0, |
                                          0, |
                                          0, |
                                          wpr:Purchase_Cost, |
                                          wpr:Sale_Cost, |
                                          wpr:Retail_Cost, |
                                          'PART UNSUSPENDED', |
                                          '', |
                                          p_web.GSV('BookingUserCode'),|
                                          sto:Quantity_Stock))
                                      end
  
                                  end
                              end
  
                          End
  
                      end
  
                  end ! If (RapidLocation(sto:Location))
              END
          END
  
      end
      if (tmp:Scrap)
          RemoveFromSTockAllocation(wpr:Record_Number,'WAR')
          p_web.SSV('AddToAudit:Type','JOB')
          p_web.SSV('AddToAudit:Action','SCRAP WARRANTY PART: ' & Clip(wpr:Part_Number))
          p_web.SSV('AddToAudit:Notes','DESCRIPTION: ' & Clip(wpr:Description))
          if (sto:AttachBySolder)
              p_web.SSV('AddToAudit:Notes',p_web.GSV('AddToAudit:Notes') & '<13,10,13,10>PART ATTACHED BY SOLDER')
          End
          p_web.SSV('AddToAudit:Notes',p_web.GSV('AddToAudit:Notes') & 'LOCATION: ' & clip(sto:Location) & |
              '<13,10>QTY: ' & wpr:Quantity)
          AddToAudit(p_web)
      else
  
      end
      IF (tmp:delete = 1)
          RemoveFromSTockAllocation(wpr:Record_Number,'WAR')
          p_web.SSV('AddToAudit:Type','JOB')
          p_web.SSV('AddToAudit:Action','WARRANTY PART DELETED: ' & Clip(wpr:Part_Number))
          p_web.SSV('AddToAudit:Notes','DESCRIPTION: ' & Clip(wpr:Description))
          if (sto:AttachBySolder)
              p_web.SSV('AddToAudit:Notes',p_web.GSV('AddToAudit:Notes') & '<13,10,13,10>PART ATTACHED BY SOLDER')
          End
          AddToAudit(p_web)
          Relate:WARPARTS.Delete(0)
      END
  
  OF 'CHA'
      Access:PARTS.Clearkey(par:RecordNumberKey)
      par:Record_Number = p_web.GSV('par:Record_Number')
      If (Access:PARTS.TryFetch(par:RecordNumberKey))
  
      End
  
      Access:STOCK.Clearkey(sto:Ref_Number_Key)
      sto:Ref_Number = par:Part_Ref_Number
      If (Access:STOCK.TryFetch(sto:Ref_Number_Key))
      End
      if (par:WebOrder)
          RemoveFromStockAllocation(par:Record_Number,'CHA')
      else
          if (par:Adjustment = 'YES')
              tmp:delete = 1
          ELSE
              IF (sto:Sundry_Item = 'YES')
                  tmp:delete = 1
              ELSE
                  IF (RapidLocation(sto:Location) AND p_web.GSV('FromURL') <> 'StockAllocation')
                      if (sto:AttachBySolder)
                          tmp:Delete = 1
                          tmp:Scrap = 1
                          AddSolder('CHA')
                      else ! if (sto:AttachBySolder)
                          if (par:PartAllocated = 0)
                              sto:Quantity_STock += par:Quantity
                              If (Access:STOCK.TryUpdate() = Level:Benign)
                                  tmp:Delete = 1
                                  RemoveWarrantyPartStockHistory('UNALLOCATED PART REMOVED','CHA')
                                  if (sto:Suspend)
                                      ! Not has stock. Unsuspend part
                                      sto:Suspend = 0
                                      If (Access:STOCK.TryUpdate() = Level:Benign)
                                          if (AddToStockHistory(sto:Ref_Number, |
                                              'ADD', |
                                              '', |
                                              0, |
                                              0, |
                                              0, |
                                              par:Purchase_Cost, |
                                              par:Sale_Cost, |
                                              par:Retail_Cost, |
                                              'PART UNSUSPENDED', |
                                              '', |
                                              p_web.GSV('BookingUserCode'),|
                                              sto:Quantity_Stock))
                                          end
  
                                      end
                                  end
                              End
                          else ! if (wpr:PartAllocated = 0)
                              par:PartAllocated = 0
                              par:Status = 'RET'
                              Access:PARTS.TryUpdate()
                              p_web.FileToSessionQueue(PARTS)
                              AddToStockAllocation(par:Record_Number,'CHA',par:Quantity,'RET',p_web.GSV('job:Engineer'),p_web)
                          end
                      end
                  else ! If (RapidLocation(sto:Location))
                      Case p_web.GSV('locScrapRestock')
                      of 1 ! Scrap
                          tmp:Delete = 1
                          tmp:Scrap = 1
                      of 2 ! Restock
                          sto:Quantity_Stock += par:Quantity
                          If (Access:STOCK.TryUpdate() = Level:Benign)
                              tmp:Delete = 1
                              RemoveWarrantyPartStockHistory('','CHA')
                              if (sto:Suspend)
                                  ! Not has stock. Unsuspend part
                                  sto:Suspend = 0
                                  If (Access:STOCK.TryUpdate() = Level:Benign)
                                      if (AddToStockHistory(sto:Ref_Number, |
                                          'ADD', |
                                          '', |
                                          0, |
                                          0, |
                                          0, |
                                          par:Purchase_Cost, |
                                          par:Sale_Cost, |
                                          par:Retail_Cost, |
                                          'PART UNSUSPENDED', |
                                          '', |
                                          p_web.GSV('BookingUserCode'),|
                                          sto:Quantity_Stock))
                                      end
  
                                  end
                              end
                          End
                      end
                  end ! If (RapidLocation(sto:Location))
              END
          END
      end
      if (tmp:Scrap)
          RemoveFromSTockAllocation(par:Record_Number,'CHA')
          p_web.SSV('AddToAudit:Type','JOB')
          p_web.SSV('AddToAudit:Action','SCRAP CHARGEABLE PART: ' & Clip(par:Part_Number))
          p_web.SSV('AddToAudit:Notes','DESCRIPTION: ' & Clip(par:Description))
          if (sto:AttachBySolder)
              p_web.SSV('AddToAudit:Notes',p_web.GSV('AddToAudit:Notes') & '<13,10,13,10>PART ATTACHED BY SOLDER')
          End
          p_web.SSV('AddToAudit:Notes',p_web.GSV('AddToAudit:Notes') & 'LOCATION: ' & clip(sto:Location) & |
              '<13,10>QTY: ' & par:Quantity)
          AddToAudit(p_web)
      else
  
      end
      IF (tmp:delete = 1)
          RemoveFromSTockAllocation(par:Record_Number,'CHA')
          p_web.SSV('AddToAudit:Type','JOB')
          p_web.SSV('AddToAudit:Action','CHARGEABLE PART DELETED: ' & Clip(par:Part_Number))
          p_web.SSV('AddToAudit:Notes','DESCRIPTION: ' & Clip(par:Description))
          if (sto:AttachBySolder)
              p_web.SSV('AddToAudit:Notes',p_web.GSV('AddToAudit:Notes') & '<13,10,13,10>PART ATTACHED BY SOLDER')
          End
          AddToAudit(p_web)
          Relate:PARTS.Delete(0)
      END
  
  OF 'EST'
      Access:ESTPARTS.Clearkey(epr:Record_Number_Key)
      epr:Record_Number = p_web.GSV('epr:Record_Number')
      If (Access:ESTPARTS.TryFetch(epr:Record_Number_Key))
  
      End
  
      Access:STOCK.Clearkey(sto:Ref_Number_Key)
      sto:Ref_Number = epr:Part_Ref_Number
      If (Access:STOCK.TryFetch(sto:Ref_Number_Key))
      End
  
      if (epr:Adjustment = 'YES')
          tmp:delete = 1
          if (sto:AttachBySolder)
              tmp:Delete = 1
              tmp:Scrap = 1
              AddSolder('EST')
          end ! if (sto:AttachBySolder)
  
      ELSE
          IF (sto:Sundry_Item = 'YES')
              tmp:delete = 1
          ELSE
              IF (RapidLocation(sto:Location) AND p_web.GSV('FromURL') <> 'StockAllocation')
                  if (sto:AttachBySolder)
                      tmp:Delete = 1
                      tmp:Scrap = 1
                      AddSolder('EST')
                  else ! if (sto:AttachBySolder)
                      if (epr:PartAllocated = 0)
                          sto:Quantity_STock += epr:Quantity
                          If (Access:STOCK.TryUpdate() = Level:Benign)
                              tmp:Delete = 1
                              RemoveWarrantyPartStockHistory('UNALLOCATED PART REMOVED','EST')
                              if (sto:Suspend)
                                  ! Not has stock. Unsuspend part
                                  sto:Suspend = 0
                                  If (Access:STOCK.TryUpdate() = Level:Benign)
                                      if (AddToStockHistory(sto:Ref_Number, |
                                          'ADD', |
                                          '', |
                                          0, |
                                          0, |
                                          0, |
                                          epr:Purchase_Cost, |
                                          epr:Sale_Cost, |
                                          epr:Retail_Cost, |
                                          'PART UNSUSPENDED', |
                                          '', |
                                          p_web.GSV('BookingUserCode'),|
                                          sto:Quantity_Stock))
                                      end
  
                                  end
                              end
                          End
                      else ! if (wpr:PartAllocated = 0)
                          epr:PartAllocated = 0
                          epr:Status = 'RET'
                          Access:ESTPARTS.TryUpdate()
                          p_web.FileToSessionQueue(ESTPARTS)
                            AddToStockAllocation(epr:Record_Number,'EST',epr:Quantity,'RET',p_web.GSV('job:Engineer'),p_web)
                      end
                  end
              else ! If (RapidLocation(sto:Location))
                  Case p_web.GSV('locScrapRestock')
                  of 1 ! Scrap
                      tmp:Delete = 1
                      tmp:Scrap = 1
                  of 2 ! Restock
                      sto:Quantity_Stock += epr:Quantity
                      If (Access:STOCK.TryUpdate() = Level:Benign)
                          tmp:Delete = 1
                          RemoveWarrantyPartStockHistory('','EST')
                          if (sto:Suspend)
                              ! Not has stock. Unsuspend part
                              sto:Suspend = 0
                              If (Access:STOCK.TryUpdate() = Level:Benign)
                                  if (AddToStockHistory(sto:Ref_Number, |
                                      'ADD', |
                                      '', |
                                      0, |
                                      0, |
                                      0, |
                                      epr:Purchase_Cost, |
                                      epr:Sale_Cost, |
                                      epr:Retail_Cost, |
                                      'PART UNSUSPENDED', |
                                      '', |
                                      p_web.GSV('BookingUserCode'),|
                                      sto:Quantity_Stock))
                                  end
  
                              end
                          end
  
                      End
  
                  end
  
              end ! If (RapidLocation(sto:Location))
          END
  
      END
  
      if (tmp:Scrap)
          if (epr:UsedOnRepair)
              RemoveFromSTockAllocation(epr:Record_Number,'EST')
          end
          p_web.SSV('AddToAudit:Type','JOB')
          p_web.SSV('AddToAudit:Action','SCRAP ESTIMATE PART: ' & Clip(epr:Part_Number))
          p_web.SSV('AddToAudit:Notes','DESCRIPTION: ' & Clip(epr:Description))
          if (sto:AttachBySolder)
              p_web.SSV('AddToAudit:Notes',p_web.GSV('AddToAudit:Notes') & '<13,10,13,10>PART ATTACHED BY SOLDER')
          End
          p_web.SSV('AddToAudit:Notes',p_web.GSV('AddToAudit:Notes') & 'LOCATION: ' & clip(sto:Location) & |
              '<13,10>QTY: ' & epr:Quantity)
          AddToAudit(p_web)
      else
  
      end
      IF (tmp:delete = 1)
          RemoveFromSTockAllocation(epr:Record_Number,'EST')
          p_web.SSV('AddToAudit:Type','JOB')
          p_web.SSV('AddToAudit:Action','ESTIMATE PART DELETED: ' & Clip(epr:Part_Number))
          p_web.SSV('AddToAudit:Notes','DESCRIPTION: ' & Clip(epr:Description))
          if (sto:AttachBySolder)
              p_web.SSV('AddToAudit:Notes',p_web.GSV('AddToAudit:Notes') & '<13,10,13,10>PART ATTACHED BY SOLDER')
          End
          AddToAudit(p_web)
          Relate:ESTPARTS.Delete(0)
      END
  
  
  end
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('FormDeletePart:Primed',0)
  p_web.StoreValue('locPartNumber')
  p_web.StoreValue('locDescription')
  p_web.StoreValue('locAlertMessage')
  p_web.StoreValue('locErrorMessage')
  p_web.StoreValue('locScrapRestock')

AddSolder           Procedure(String fType)
    Code

        Case fType
        of 'WAR'
            If Access:STOFAULT.PrimeRecord() = Level:Benign
                stf:PartNumber  = wpr:Part_Number
                stf:Description = wpr:Description
                stf:Quantity    = wpr:Quantity
                stf:PurchaseCost    = wpr:Purchase_Cost
                stf:ModelNumber = p_web.GSV('job:Model_Number')
                stf:IMEI        = p_web.GSV('job:ESN')
                stf:Engineer    = p_web.GSV('job:Engineer')
                stf:StoreUserCode   = p_web.GSV('BookingUserCode')
                stf:PartType        = 'SOL'
                If Access:STOFAULT.TryInsert() = Level:Benign
                    !Insert Successful

                Else !If Access:STOFAULT.TryInsert() = Level:Benign
                    !Insert Failed
                    Access:STOFAULT.CancelAutoInc()
                End !If Access:STOFAULT.TryInsert() = Level:Benign
            End !If Access:STOFAULT.PrimeRecord() = Level:Benign

        of 'CHA'
            If Access:STOFAULT.PrimeRecord() = Level:Benign
                stf:PartNumber  = par:Part_Number
                stf:Description = par:Description
                stf:Quantity    = par:Quantity
                stf:PurchaseCost    = par:Purchase_Cost
                stf:ModelNumber = p_web.GSV('job:Model_Number')
                stf:IMEI        = p_web.GSV('job:ESN')
                stf:Engineer    = p_web.GSV('job:Engineer')
                stf:StoreUserCode   = p_web.GSV('BookingUserCode')
                stf:PartType        = 'SOL'
                If Access:STOFAULT.TryInsert() = Level:Benign
                    !Insert Successful

                Else !If Access:STOFAULT.TryInsert() = Level:Benign
                    !Insert Failed
                    Access:STOFAULT.CancelAutoInc()
                End !If Access:STOFAULT.TryInsert() = Level:Benign
            End !If Access:STOFAULT.PrimeRecord() = Level:Benign

        of 'EST'
            If Access:STOFAULT.PrimeRecord() = Level:Benign
                stf:PartNumber  = epr:Part_Number
                stf:Description = epr:Description
                stf:Quantity    = epr:Quantity
                stf:PurchaseCost    = epr:Purchase_Cost
                stf:ModelNumber = p_web.GSV('job:Model_Number')
                stf:IMEI        = p_web.GSV('job:ESN')
                stf:Engineer    = p_web.GSV('job:Engineer')
                stf:StoreUserCode   = p_web.GSV('BookingUserCode')
                stf:PartType        = 'SOL'
                If Access:STOFAULT.TryInsert() = Level:Benign
                    !Insert Successful

                Else !If Access:STOFAULT.TryInsert() = Level:Benign
                    !Insert Failed
                    Access:STOFAULT.CancelAutoInc()
                End !If Access:STOFAULT.TryInsert() = Level:Benign
            End !If Access:STOFAULT.PrimeRecord() = Level:Benign

        end

RemoveWarrantyPartStockHistory      Procedure(String fInformation,STRING fType)
Code

    CASE fType
    OF 'WAR'
        If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
            'ADD', | ! Transaction_Type
            wpr:Despatch_Note_Number, | ! Depatch_Note_Number
            p_web.GSV('job:Ref_Number'), | ! Job_Number
            0, | ! Sales_Number
            wpr:Quantity, | ! Quantity
            wpr:Purchase_Cost, | ! Purchase_Cost
            wpr:Sale_Cost, | ! Sale_Cost
            wpr:Retail_Cost, | ! Retail_Cost
            fInformation, | ! Notes
            'WARRANTY PART REMOVED FROM JOB', |
            p_web.GSV('BookingUserCode'),|
            sto:Quantity_Stock) ! Information

        Else ! AddToStockHistory
            ! Error
        End ! AddToStockHistory
    OF 'CHA'
        If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
            'ADD', | ! Transaction_Type
            par:Despatch_Note_Number, | ! Depatch_Note_Number
            p_web.GSV('job:Ref_Number'), | ! Job_Number
            0, | ! Sales_Number
            par:Quantity, | ! Quantity
            par:Purchase_Cost, | ! Purchase_Cost
            par:Sale_Cost, | ! Sale_Cost
            par:Retail_Cost, | ! Retail_Cost
            fInformation, | ! Notes
            'WARRANTY PART REMOVED FROM JOB', |
            p_web.GSV('BookingUserCode'),|
            sto:Quantity_Stock) ! Information

        Else ! AddToStockHistory
            ! Error
        End ! AddToStockHistory
    END ! Case

ShowAlert     Procedure(String fAlert)
Code
    packet = clip(packet) & '<script language="JavaScript" type="text/javascript">alert("' & clip(fAlert) & '")</script>'
    do sendPacket
