

   MEMBER('WebServer_Phase4.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSE070.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSE005.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE006.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE025.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE069.INC'),ONCE        !Req'd for module callout resolution
                     END


JobAccessories       PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
tmp:FoundAccessory   STRING(1000)                          !
tmp:TheAccessory     STRING(1000)                          !
tmp:ShowAccessory    STRING(1000)                          !
tmp:TheJobAccessory  STRING(1000)                          !
FilesOpened     Long
ACCESSOR::State  USHORT
JOBSE::State  USHORT
textAccessoryMessage:IsInvalid  Long
tmp:ShowAccessory:IsInvalid  Long
tmp:TheAccessory:IsInvalid  Long
buttonAddAccessories:IsInvalid  Long
buttonRemoveAccessory:IsInvalid  Long
jobe:AccessoryNotes:IsInvalid  Long
BrowseAccessoryNumber:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('JobAccessories')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'JobAccessories_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('JobAccessories','')
    p_web.DivHeader('JobAccessories',p_web.combine(p_web.site.style.formdiv,))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('JobAccessories') = 0
        p_web.AddPreCall('JobAccessories')
        p_web.DivHeader('popup_JobAccessories','nt-hidden')
        p_web.DivHeader('JobAccessories',p_web.combine(p_web.site.style.formdiv,))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_JobAccessories_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_JobAccessories_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferJobAccessories',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_JobAccessories',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferJobAccessories',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_JobAccessories',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferJobAccessories',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_JobAccessories',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_JobAccessories',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferJobAccessories',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_JobAccessories',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferJobAccessories',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_JobAccessories',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_JobAccessories',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('JobAccessories')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(ACCESSOR)
  p_web._OpenFile(JOBSE)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(ACCESSOR)
  p_Web._CloseFile(JOBSE)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE
  loc:EipClm = upper(p_web.GetValue('_EIPClm'))
  p_web.DeleteValue('_EIPClm')
  case loc:EipClm
  of ''
    case upper(p_web.GetValue('_calledfrom_'))
    of upper('BrowseAccessoryNumber')
      do Value::BrowseAccessoryNumber
    end
  end

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('JobAccessories_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'JobAccessories'
    end
    p_web.formsettings.proc = 'JobAccessories'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  if p_web.IfExistsValue('tmp:ShowAccessory') = 0
    p_web.SetSessionValue('tmp:ShowAccessory',tmp:ShowAccessory)
  Else
    tmp:ShowAccessory = p_web.GetSessionValue('tmp:ShowAccessory')
  End
  if p_web.IfExistsValue('tmp:TheAccessory') = 0
    p_web.SetSessionValue('tmp:TheAccessory',tmp:TheAccessory)
  Else
    tmp:TheAccessory = p_web.GetSessionValue('tmp:TheAccessory')
  End
  if p_web.IfExistsValue('jobe:AccessoryNotes') = 0
    p_web.SetSessionValue('jobe:AccessoryNotes',jobe:AccessoryNotes)
  Else
    jobe:AccessoryNotes = p_web.GetSessionValue('jobe:AccessoryNotes')
  End

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('tmp:ShowAccessory')
    tmp:ShowAccessory = p_web.GetValue('tmp:ShowAccessory')
    p_web.SetSessionValue('tmp:ShowAccessory',tmp:ShowAccessory)
  Else
    tmp:ShowAccessory = p_web.GetSessionValue('tmp:ShowAccessory')
  End
  if p_web.IfExistsValue('tmp:TheAccessory')
    tmp:TheAccessory = p_web.GetValue('tmp:TheAccessory')
    p_web.SetSessionValue('tmp:TheAccessory',tmp:TheAccessory)
  Else
    tmp:TheAccessory = p_web.GetSessionValue('tmp:TheAccessory')
  End
  if p_web.IfExistsValue('jobe:AccessoryNotes')
    jobe:AccessoryNotes = p_web.GetValue('jobe:AccessoryNotes')
    p_web.SetSessionValue('jobe:AccessoryNotes',jobe:AccessoryNotes)
  Else
    jobe:AccessoryNotes = p_web.GetSessionValue('jobe:AccessoryNotes')
  End

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('JobAccessories_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'ViewJob'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('JobAccessories_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('JobAccessories_ChainTo')
    loc:formaction = p_web.GetSessionValue('JobAccessories_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'ViewJob'

GenerateForm   Routine
  do LoadRelatedRecords
 tmp:ShowAccessory = p_web.RestoreValue('tmp:ShowAccessory')
 tmp:TheAccessory = p_web.RestoreValue('tmp:TheAccessory')
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Job Accessories') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Job Accessories',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_JobAccessories',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_JobAccessories0_div')&'">'&p_web.Translate()&'</a></li>'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_JobAccessories1_div')&'">'&p_web.Translate('Accessory Numbers')&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  do GenerateTab1
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="JobAccessories_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('JobAccessories_BrowseAccessoryNumber_embedded_div')&''');'
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="JobAccessories_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'JobAccessories_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="JobAccessories_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('JobAccessories_BrowseAccessoryNumber_embedded_div')&''');'
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'JobAccessories_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:ShowAccessory')
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_JobAccessories')>0,p_web.GSV('showtab_JobAccessories'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_JobAccessories'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_JobAccessories') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_JobAccessories'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_JobAccessories')>0,p_web.GSV('showtab_JobAccessories'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_JobAccessories') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Accessory Numbers') & ''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_JobAccessories_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_JobAccessories')>0,p_web.GSV('showtab_JobAccessories'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"JobAccessories",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_JobAccessories')>0,p_web.GSV('showtab_JobAccessories'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_JobAccessories_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('JobAccessories') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('JobAccessories')
    p_web.SetValue('_CallPopups',2)
    If p_web.GetPreCall('BrowseAccessoryNumber') = 0
      p_web.SetValue('BrowseAccessoryNumber:NoForm',1)
      p_web.SetValue('BrowseAccessoryNumber:FormName',loc:formname)
      p_web.SetValue('BrowseAccessoryNumber:parentIs','Form')
      p_web.SetValue('_parentProc','JobAccessories')
      BrowseAccessoryNumber(p_web)
      p_web.SetValue('_CallPopups',0)
      p_web.DeleteValue('BrowseAccessoryNumber:NoForm')
      p_web.DeleteValue('BrowseAccessoryNumber:FormName')
      p_web.DeleteValue('BrowseAccessoryNumber:parentIs')
      p_web.DeleteValue('_parentProc')
    End
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate()&'</a></h3>' & CRLF & p_web.DivHeader('tab_JobAccessories0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_JobAccessories0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_JobAccessories0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_JobAccessories0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,)
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_JobAccessories0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend)
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_JobAccessories0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain">' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_JobAccessories0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&50&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&200&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::textAccessoryMessage
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&50&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:ShowAccessory
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&200&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:ShowAccessory
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&50&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:TheAccessory
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&200&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:TheAccessory
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&50&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::buttonAddAccessories
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&200&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::buttonAddAccessories
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&50&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::buttonRemoveAccessory
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&200&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::buttonRemoveAccessory
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&50&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jobe:AccessoryNotes
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&200&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jobe:AccessoryNotes
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab1  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Accessory Numbers')&'</a></h3>' & CRLF & p_web.DivHeader('tab_JobAccessories1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_JobAccessories1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_JobAccessories1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_JobAccessories1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Accessory Numbers')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_JobAccessories1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Accessory Numbers')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_JobAccessories1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Accessory Numbers')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_JobAccessories1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&50&'"'
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 2.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::BrowseAccessoryNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Validate::textAccessoryMessage  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::textAccessoryMessage  ! copies value to session value if valid.

ValidateValue::textAccessoryMessage  Routine
    If not (1=0)
    End

Value::textAccessoryMessage  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobAccessories_' & p_web._nocolon('textAccessoryMessage') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="textAccessoryMessage" class="'&clip('green bold')&'"'&clip(loc:extra)&'>' & p_web.Translate('Hold "CTRL" or "SHIFT" to select more than one accessory',) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::tmp:ShowAccessory  Routine
  packet = clip(packet) & p_web.DivHeader('JobAccessories_' & p_web._nocolon('tmp:ShowAccessory') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:ShowAccessory  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:ShowAccessory = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:ShowAccessory = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:ShowAccessory  ! copies value to session value if valid.
  do Value::tmp:TheAccessory  !1

ValidateValue::tmp:ShowAccessory  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:ShowAccessory',tmp:ShowAccessory).
    End

Value::tmp:ShowAccessory  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobAccessories_' & p_web._nocolon('tmp:ShowAccessory') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,p_web.site.style.formselect,,)
  If loc:retrying
    tmp:ShowAccessory = p_web.RestoreValue('tmp:ShowAccessory')
    do ValidateValue::tmp:ShowAccessory
    If tmp:ShowAccessory:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- DROPLIST ---
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ShowAccessory'',''jobaccessories_tmp:showaccessory_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:ShowAccessory')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:ShowAccessory',loc:fieldclass,loc:readonly,20,180,1,loc:javascript,)
  loc:fieldclass = ''
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  loc:rowstyle = choose(sub('GreyRegular',1,1)=' ',clip(loc:rowstyle) & 'GreyRegular','GreyRegular')
  if p_web.IfExistsSessionValue('tmp:ShowAccessory') = 0
    p_web.SetSessionValue('tmp:ShowAccessory','')
  end
    packet = clip(packet) & p_web.CreateOption('--- Available Accessories ---','',choose('' = p_web.getsessionvalue('tmp:ShowAccessory')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  Access:ACCESSOR.Clearkey(acr:Accesory_Key)
  acr:Model_NUmber = p_web.GSV('job:Model_Number')
  Set(acr:Accesory_Key,acr:Accesory_Key)
  Loop
      If Access:ACCESSOR.Next()
          Break
      End ! If Access:ACCESSOR.Next()
      If acr:Model_Number <> p_web.GSV('job:Model_Number')
          Break
      End ! If acr:Model_Number <> p_web.GSV('tmp:ModelNumber')
      If Instring(';' & Clip(acr:Accessory),p_web.GSV('tmp:TheJobAccessory'),1,1)
          Cycle
      End ! If Instring(';' & Clip(acr:Accessory) & ';',p_web.GSV('tmp:TheJobAccessory'),1,1)
  
      loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
      packet = clip(packet) & p_web.CreateOption(Clip(acr:Accessory),acr:Accessory,choose(acr:Accessory = p_web.GSV('tmp:ShowAccessory')),clip(loc:rowstyle),,)&CRLF
      loc:even = Choose(loc:even=1,2,1)
  End ! Loop
  
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web.DivFooter()

Prompt::tmp:TheAccessory  Routine
  packet = clip(packet) & p_web.DivHeader('JobAccessories_' & p_web._nocolon('tmp:TheAccessory') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:TheAccessory  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:TheAccessory = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:TheAccessory = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:TheAccessory  ! copies value to session value if valid.

ValidateValue::tmp:TheAccessory  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:TheAccessory',tmp:TheAccessory).
    End

Value::tmp:TheAccessory  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobAccessories_' & p_web._nocolon('tmp:TheAccessory') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,p_web.site.style.formselect,,)
  If loc:retrying
    tmp:TheAccessory = p_web.RestoreValue('tmp:TheAccessory')
    do ValidateValue::tmp:TheAccessory
    If tmp:TheAccessory:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- DROPLIST ---
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:TheAccessory'',''jobaccessories_tmp:theaccessory_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:TheAccessory',loc:fieldclass,loc:readonly,20,180,0,loc:javascript,)
  loc:fieldclass = ''
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  loc:rowstyle = choose(sub('GreyRegular',1,1)=' ',clip(loc:rowstyle) & 'GreyRegular','GreyRegular')
  if p_web.IfExistsSessionValue('tmp:TheAccessory') = 0
    p_web.SetSessionValue('tmp:TheAccessory','')
  end
    packet = clip(packet) & p_web.CreateOption('--- Accessories On Job ---','',choose('' = p_web.getsessionvalue('tmp:TheAccessory')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
      If p_web.GSV('tmp:TheJobAccessory') <> ''
          Loop x# = 1 To 1000
              If Sub(p_web.GSV('tmp:TheJobAccessory'),x#,2) = '|;'
                  Start# = x# + 2
                  Cycle
              End ! If Sub(p_web.GSV('tmp:TheJobAccessory'),x#,2) = '|;'
  
              If Start# > 0
                 If Sub(p_web.GSV('tmp:TheJobAccessory'),x#,2) = ';|'
                     tmp:FoundAccessory = Sub(p_web.GSV('tmp:TheJobAccessory'),Start#,x# - Start#)
  
                     If tmp:FoundAccessory <> ''
                         loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
                         packet = clip(packet) & p_web.CreateOption(tmp:FoundAccessory,tmp:FoundAccessory,choose(tmp:FoundAccessory = p_web.GSV('tmp:TheAccessory')),clip(loc:rowstyle),,)&CRLF
                         loc:even = Choose(loc:even=1,2,1)
                     End ! If tmp:FoundAccessory <> ''
                     Start# = 0
                 End ! If Sub(p_web.GSV('tmp:TheJobAccessory'),x#,2) = ';|'
              End ! If Start# > 0
          End ! Loop x# = 1 To 1000
  
          If Start# > 0
             tmp:FoundAccessory = Clip(Sub(p_web.GSV('tmp:TheJobAccessory'),Start#,30))
             If tmp:FoundAccessory <> ''
                 loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
                 packet = clip(packet) & p_web.CreateOption(tmp:FoundAccessory,tmp:FoundAccessory,choose(tmp:FoundAccessory = p_web.GSV('tmp:TheAccessory')),clip(loc:rowstyle),,)&CRLF
                 loc:even = Choose(loc:even=1,2,1)
             End ! If tmp:FoundAccessory <> ''
          End ! If Start# > 0
      End ! If p_web.GSV('tmp:TheJobAccessory') <> ''
  
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web.DivFooter()

Prompt::buttonAddAccessories  Routine
  packet = clip(packet) & p_web.DivHeader('JobAccessories_' & p_web._nocolon('buttonAddAccessories') & '_prompt',Choose(p_web.GSV('Job:ViewOnly') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Job:ViewOnly') = 1,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::buttonAddAccessories  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonAddAccessories  ! copies value to session value if valid.
  p_web.SSV('tmp:TheJobAccessory',p_web.GSV('tmp:TheJobAccessory') & p_web.GSV('tmp:ShowAccessory'))
  p_web.SSV('tmp:ShowAccessory','')
  do Value::buttonAddAccessories
  do Value::tmp:ShowAccessory  !1
  do Value::tmp:TheAccessory  !1

ValidateValue::buttonAddAccessories  Routine
    If not (p_web.GSV('Job:ViewOnly') = 1)
    End

Value::buttonAddAccessories  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('Job:ViewOnly') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobAccessories_' & p_web._nocolon('buttonAddAccessories') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('Job:ViewOnly') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonAddAccessories'',''jobaccessories_buttonaddaccessories_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','AddAccessories','Add Accessories',p_web.combine(Choose('Add Accessories' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'button-entryfield'),loc:formname,,,,loc:javascript,loc:disabled,,,,,,,,,,'nt-left')!h
  do SendPacket
  End
  p_web.DivFooter()

Prompt::buttonRemoveAccessory  Routine
  packet = clip(packet) & p_web.DivHeader('JobAccessories_' & p_web._nocolon('buttonRemoveAccessory') & '_prompt',Choose(p_web.GSV('Job:ViewOnly') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Job:ViewOnly') = 1,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::buttonRemoveAccessory  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonRemoveAccessory  ! copies value to session value if valid.
  tmp:TheJobAccessory = p_web.GSV('tmp:TheJobAccessory') & ';'
  tmp:TheAccessory = '|;' & Clip(p_web.GSV('tmp:TheAccessory')) & ';'
  Loop x# = 1 To 1000
      pos# = Instring(Clip(tmp:TheAccessory),tmp:TheJobAccessory,1,1)
      If pos# > 0
          tmp:TheJobAccessory = Sub(tmp:TheJobAccessory,1,pos# - 1) & Sub(tmp:TheJobAccessory,pos# + Len(Clip(tmp:TheAccessory)),1000)
          Break
      End ! If pos# > 0#
  End ! Loop x# = 1 To 1000
  p_web.SetSessionValue('tmp:TheJobAccessory',Sub(tmp:TheJobAccessory,1,Len(Clip(tmp:TheJobAccessory)) - 1))
  p_web.SetSessionValue('tmp:TheAccessory','')
  
  do Value::buttonRemoveAccessory
  do Value::tmp:ShowAccessory  !1
  do Value::tmp:TheAccessory  !1

ValidateValue::buttonRemoveAccessory  Routine
    If not (p_web.GSV('Job:ViewOnly') = 1)
    End

Value::buttonRemoveAccessory  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('Job:ViewOnly') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobAccessories_' & p_web._nocolon('buttonRemoveAccessory') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('Job:ViewOnly') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonRemoveAccessory'',''jobaccessories_buttonremoveaccessory_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','RemoveAccessory','Remove Accessory',p_web.combine(Choose('Remove Accessory' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'button-entryfield'),loc:formname,,,,loc:javascript,loc:disabled,,,,,,,,,,'nt-left')!h
  do SendPacket
  End
  p_web.DivFooter()

Prompt::jobe:AccessoryNotes  Routine
  packet = clip(packet) & p_web.DivHeader('JobAccessories_' & p_web._nocolon('jobe:AccessoryNotes') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Accessory Notes'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jobe:AccessoryNotes  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jobe:AccessoryNotes = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s255
    jobe:AccessoryNotes = p_web.Dformat(p_web.GetValue('Value'),'@s255')
  End
  do ValidateValue::jobe:AccessoryNotes  ! copies value to session value if valid.
  do Value::jobe:AccessoryNotes
  do SendAlert

ValidateValue::jobe:AccessoryNotes  Routine
    If not (1=0)
    jobe:AccessoryNotes = Upper(jobe:AccessoryNotes)
      if loc:invalid = '' then p_web.SetSessionValue('jobe:AccessoryNotes',jobe:AccessoryNotes).
    End

Value::jobe:AccessoryNotes  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobAccessories_' & p_web._nocolon('jobe:AccessoryNotes') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.GSV('Job:ViewOnly') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    jobe:AccessoryNotes = p_web.RestoreValue('jobe:AccessoryNotes')
    do ValidateValue::jobe:AccessoryNotes
    If jobe:AccessoryNotes:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- TEXT --- jobe:AccessoryNotes
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe:AccessoryNotes'',''jobaccessories_jobe:accessorynotes_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('Job:ViewOnly') = 1,'readonly','')
  do SendPacket
  p_web.CreateTextArea('jobe:AccessoryNotes',p_web.GetSessionValue('jobe:AccessoryNotes'),5,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(jobe:AccessoryNotes),'Accessory Notes',,,Net:Send)
  do SendPacket
  End
  p_web.DivFooter()

Validate::BrowseAccessoryNumber  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  Elsif true
    p_web.StoreValue('joa:RecordNumber')
  End
  do ValidateValue::BrowseAccessoryNumber  ! copies value to session value if valid.

ValidateValue::BrowseAccessoryNumber  Routine
    If not (1=0)
    End

Value::BrowseAccessoryNumber  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(1=0,1,0))
  ! --- BROWSE ---  BrowseAccessoryNumber --
  p_web.SetValue('BrowseAccessoryNumber:NoForm',1)
  p_web.SetValue('BrowseAccessoryNumber:FormName',loc:formname)
  p_web.SetValue('BrowseAccessoryNumber:parentIs','Form')
  p_web.SetValue('_parentProc','JobAccessories')
  if p_web.RequestAjax = 0
    p_web.SSV('JobAccessories:_popup_',p_web.GetValue('_popup_')) ! stores the current browse popup state
    packet = clip(packet) & '<div id="'&lower('JobAccessories_BrowseAccessoryNumber_embedded_div')&'"><!-- Net:BrowseAccessoryNumber --></div><13,10>'
    do SendPacket
    p_web.DivHeader('JobAccessories_' & lower('BrowseAccessoryNumber') & '_value')
    p_web.DivFooter()
  else
    if p_web.GetValue('_popup_') = 1
      p_web.SSV('JobAccessories:_popup_',1)
    elsif p_web.GSV('JobAccessories:_popup_') = 1
      p_web.SetValue('_popup_',1)
    end
    packet = clip(packet) & '<!-- Net:BrowseAccessoryNumber --><13,10>'
  end
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('JobAccessories_nexttab_' & 0)
    tmp:ShowAccessory = p_web.GSV('tmp:ShowAccessory')
    do ValidateValue::tmp:ShowAccessory
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:ShowAccessory
      !do SendAlert
      !exit
    End
    tmp:TheAccessory = p_web.GSV('tmp:TheAccessory')
    do ValidateValue::tmp:TheAccessory
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:TheAccessory
      !do SendAlert
      !exit
    End
    jobe:AccessoryNotes = p_web.GSV('jobe:AccessoryNotes')
    do ValidateValue::jobe:AccessoryNotes
    If loc:Invalid
      loc:retrying = 1
      do Value::jobe:AccessoryNotes
      !do SendAlert
      !exit
    End
    If loc:Invalid then exit.
  of lower('JobAccessories_nexttab_' & 1)
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_JobAccessories_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('JobAccessories_tab_' & 0)
    do GenerateTab0
  of lower('JobAccessories_tmp:ShowAccessory_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ShowAccessory
      of event:timer
        do Value::tmp:ShowAccessory
      else
        do Value::tmp:ShowAccessory
      end
  of lower('JobAccessories_tmp:TheAccessory_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:TheAccessory
      of event:timer
        do Value::tmp:TheAccessory
      else
        do Value::tmp:TheAccessory
      end
  of lower('JobAccessories_buttonAddAccessories_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonAddAccessories
      of event:timer
        do Value::buttonAddAccessories
      else
        do Value::buttonAddAccessories
      end
  of lower('JobAccessories_buttonRemoveAccessory_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonRemoveAccessory
      of event:timer
        do Value::buttonRemoveAccessory
      else
        do Value::buttonRemoveAccessory
      end
  of lower('JobAccessories_jobe:AccessoryNotes_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe:AccessoryNotes
      of event:timer
        do Value::jobe:AccessoryNotes
      else
        do Value::jobe:AccessoryNotes
      end
  of lower('JobAccessories_tab_' & 1)
    do GenerateTab1
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('JobAccessories_form:ready_',1)

  p_web.SetSessionValue('JobAccessories_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_JobAccessories',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('JobAccessories_form:ready_',1)
  p_web.SetSessionValue('JobAccessories_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_JobAccessories',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('JobAccessories_form:ready_',1)
  p_web.SetSessionValue('JobAccessories_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('JobAccessories:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('JobAccessories_form:ready_',1)
  p_web.SetSessionValue('JobAccessories_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('JobAccessories:Primed',0)
  p_web.setsessionvalue('showtab_JobAccessories',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
      If not (1=0)
          If p_web.IfExistsValue('tmp:ShowAccessory')
            tmp:ShowAccessory = p_web.GetValue('tmp:ShowAccessory')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('tmp:TheAccessory')
            tmp:TheAccessory = p_web.GetValue('tmp:TheAccessory')
          End
      End
      If not (1=0)
        If not (p_web.GSV('Job:ViewOnly') = 1)
          If p_web.IfExistsValue('jobe:AccessoryNotes')
            jobe:AccessoryNotes = p_web.GetValue('jobe:AccessoryNotes')
          End
        End
      End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('JobAccessories_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('JobAccessories_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
    do ValidateValue::textAccessoryMessage
    If loc:Invalid then exit.
    do ValidateValue::tmp:ShowAccessory
    If loc:Invalid then exit.
    do ValidateValue::tmp:TheAccessory
    If loc:Invalid then exit.
    do ValidateValue::buttonAddAccessories
    If loc:Invalid then exit.
    do ValidateValue::buttonRemoveAccessory
    If loc:Invalid then exit.
    do ValidateValue::jobe:AccessoryNotes
    If loc:Invalid then exit.
  ! tab = 3
    loc:InvalidTab += 1
    do ValidateValue::BrowseAccessoryNumber
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('JobAccessories:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('tmp:ShowAccessory')
  p_web.StoreValue('tmp:TheAccessory')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')

FormEngineeringOption PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locUserPassword      STRING(30)                            !
locEngineeringOption STRING(30)                            !
locExchangeManufacturer STRING(30)                         !
locExchangeModelNumber STRING(30)                          !
locExchangeNotes     STRING(255)                           !
locNewEngineeringOption STRING(20)                         !
locSplitJob          BYTE                                  !
FilesOpened     Long
USERS::State  USHORT
ACCAREAS::State  USHORT
MODELNUM::State  USHORT
JOBS::State  USHORT
MANUFACT::State  USHORT
STOCK::State  USHORT
WARPARTS::State  USHORT
PARTS::State  USHORT
STOMODEL::State  USHORT
AUDIT::State  USHORT
STOCKALL::State  USHORT
JOBSE::State  USHORT
JOBSENG::State  USHORT
REPTYDEF::State  USHORT
WEBJOB::State  USHORT
EXCHOR48::State  USHORT
locUserPassword:IsInvalid  Long
locErrorMessage:IsInvalid  Long
locEngineeringOption:IsInvalid  Long
locNewEngineeringOption:IsInvalid  Long
text:ExchangeOrder:IsInvalid  Long
job:DOP:IsInvalid  Long
locExchangeManufacturer:IsInvalid  Long
locExchangeModelNumber:IsInvalid  Long
locExchangeNotes:IsInvalid  Long
locWarningMessage:IsInvalid  Long
locSplitJob:IsInvalid  Long
button:CreateOrder:IsInvalid  Long
button:PrintOrder:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormEngineeringOption')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'FormEngineeringOption_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('FormEngineeringOption','Change')
    p_web.DivHeader('FormEngineeringOption',p_web.combine(p_web.site.style.formdiv,'fdiv'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('FormEngineeringOption') = 0
        p_web.AddPreCall('FormEngineeringOption')
        p_web.DivHeader('popup_FormEngineeringOption','nt-hidden')
        p_web.DivHeader('FormEngineeringOption',p_web.combine(p_web.site.style.formdiv,'fdiv'))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_FormEngineeringOption_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_FormEngineeringOption_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormEngineeringOption',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_FormEngineeringOption',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormEngineeringOption',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_FormEngineeringOption',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormEngineeringOption',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormEngineeringOption',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_FormEngineeringOption',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormEngineeringOption',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_FormEngineeringOption',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormEngineeringOption',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormEngineeringOption',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormEngineeringOption',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('FormEngineeringOption')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
add:ExchangeOrderPart         Routine
data
local:foundUnit             Byte(0)
code
    !Check to see if a part already exists

    If p_web.GSV('job:Warranty_Job') = 'YES'
        Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
        wpr:Ref_Number  = p_web.GSV('job:Ref_Number')
        wpr:Part_Number = 'EXCH'
        Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
        Loop
            If Access:WARPARTS.NEXT()
               Break
            End !If
            If wpr:Ref_Number  <> p_web.GSV('job:Ref_Number')      |
            Or wpr:Part_Number <> 'EXCH'      |
                Then Break.  ! End If
            local:FoundUnit = True
            If local:FoundUnit
                wpr:Status  = 'ORD'
                Access:WARPARTS.Update()

                Access:STOCKALL.Clearkey(stl:PartRecordTypeKey)
                stl:PartType    = 'WAR'
                stl:PartRecordNumber    = wpr:Record_Number
                if (Access:STOCKALL.TryFetch(stl:PartRecordTypeKey) = Level:Benign)
                    ! Found
                    relate:STOCKALL.delete(0)
                else ! if (Access:STOCKALL.TryFetch(stl:PartRecordTypeKey) = Level:Benign)
                    ! Error
                end ! if (Access:STOCKALL.TryFetch(stl:PartRecordTypeKey) = Level:Benign)
                Break
            End !If local:FoundUnit
        End !Loop

    End !If job:Warranty_Job = 'YES'

    If local:FoundUnit = True
        If p_web.GSV('ob:Chargeable_Job') = 'YES'
            Access:PARTS.ClearKey(par:Part_Number_Key)
            par:Ref_Number  = p_web.GSV('job:Ref_Number')
            par:Part_Number = 'EXCH'
            Set(par:Part_Number_Key,par:Part_Number_Key)
            Loop
                If Access:PARTS.NEXT()
                   Break
                End !If
                If par:Ref_Number  <> p_web.GSV('job:Ref_Number')      |
                Or par:Part_Number <> 'EXCH'      |
                    Then Break.  ! End If
                local:FoundUnit = True
                If local:FoundUnit
                    par:Status  = 'ORD'
                    Access:PARTS.Update()
                    Access:STOCKALL.Clearkey(stl:PartRecordTypeKey)
                    stl:PartType    = 'CHA'
                    stl:PartRecordNumber    = par:Record_Number
                    if (Access:STOCKALL.TryFetch(stl:PartRecordTypeKey) = Level:Benign)
                        ! Found
                        relate:STOCKALL.delete(0)
                    else ! if (Access:STOCKALL.TryFetch(stl:PartRecordTypeKey) = Level:Benign)
                        ! Error
                    end ! if (Access:STOCKALL.TryFetch(stl:PartRecordTypeKey) = Level:Benign)
                    Break
                End !If local:FoundUnit
            End !Loop
        End !If job:Chargeable_Job = 'YES'
    End !If FoundEXCHPart# = 0

    If local:FoundUnit = False
        If p_web.GSV('job:Engineer') = ''
            Access:USERS.Clearkey(use:Password_Key)
            use:Password    = p_web.GSV('BookingUserPassword')
            If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                !Found

            Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                !Error
            End !If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        Else !If job:Engineer = ''
            Access:USERS.Clearkey(use:User_Code_Key)
            use:User_Code   = p_web.GSV('job:Engineer')
            If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign

            End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
        End !If job:Engineer = ''

         !Found
        Access:STOCK.Clearkey(sto:Location_Key)
        sto:Location    = use:Location
        sto:Part_Number = 'EXCH'
        If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
            !Found
            Access:LOCATION.Clearkey(loc:Location_Key)
            loc:Location    = sto:Location
            If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
                !Found
            Else ! If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
                !Error
            End !If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
            If p_web.GSV('job:Warranty_Job') = 'YES'
               get(warparts,0)
               if access:warparts.primerecord() = level:benign
                   wpr:PArt_Ref_Number      = sto:Ref_Number
                   wpr:ref_number            = p_web.GSV('job:ref_number')
                   wpr:adjustment            = 'YES'
                   wpr:part_number           = 'EXCH'
                   wpr:description           = p_web.GSV('job:Manufacturer') & ' EXCHANGE UNIT'
                   wpr:quantity              = 1
                   wpr:warranty_part         = 'NO'
                   wpr:exclude_from_order    = 'YES'
                   wpr:PartAllocated         = 1
                   wpr:Status                = 'ORD'
                   wpr:ExchangeUnit          = True
                   wpr:SecondExchangeUnit    = 0
                   If sto:Assign_Fault_Codes = 'YES'
                       !Try and get the fault codes. This key should get the only record
                       Access:STOMODEL.ClearKey(stm:Ref_Part_Description)
                       stm:Ref_Number  = sto:Ref_Number
                       stm:Part_Number = sto:Part_Number
                       stm:Location    = sto:Location
                       stm:Description = sto:Description
                       If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                           !Found
                           wpr:Fault_Code1  = stm:FaultCode1
                           wpr:Fault_Code2  = stm:FaultCode2
                           wpr:Fault_Code3  = stm:FaultCode3
                           wpr:Fault_Code4  = stm:FaultCode4
                           wpr:Fault_Code5  = stm:FaultCode5
                           wpr:Fault_Code6  = stm:FaultCode6
                           wpr:Fault_Code7  = stm:FaultCode7
                           wpr:Fault_Code8  = stm:FaultCode8
                           wpr:Fault_Code9  = stm:FaultCode9
                           wpr:Fault_Code10 = stm:FaultCode10
                           wpr:Fault_Code11 = stm:FaultCode11
                           wpr:Fault_Code12 = stm:FaultCode12
                       Else!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                       !Error
                       !Assert(0,'<13,10>Fetch Error<13,10>')
                       End!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                   end !If sto:Assign_Fault_Codes = 'YES'
                   if access:warparts.insert()
                       access:warparts.cancelautoinc()
                   end
                End !If Prime

             Else !If job:Warranty_Job = 'YES'
                If p_web.GSV('job:Chargeable_Job') = 'YES'
                    get(parts,0)
                    if access:parts.primerecord() = level:benign
                        !message('At break2')
                        par:PArt_Ref_Number      = sto:Ref_Number
                        par:ref_number            = p_web.GSV('job:ref_number')
                        par:adjustment            = 'YES'
                        par:part_number           = 'EXCH'
                        par:description           = p_web.GSV('job:Manufacturer') & ' EXCHANGE UNIT'
                        par:quantity              = 1
                        par:warranty_part         = 'NO'
                        par:exclude_from_order    = 'YES'
                        par:PartAllocated         = 1
                        par:Status                = 'ORD'
                        par:ExchangeUnit          = True
                        par:SecondExchangeUnit    = 0
                        If sto:Assign_Fault_Codes = 'YES'
                           !Try and get the fault codes. This key should get the only record
                           Access:STOMODEL.ClearKey(stm:Ref_Part_Description)
                           stm:Ref_Number  = sto:Ref_Number
                           stm:Part_Number = sto:Part_Number
                           stm:Location    = sto:Location
                           stm:Description = sto:Description
                           If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                               !Found
                               par:Fault_Code1  = stm:FaultCode1
                               par:Fault_Code2  = stm:FaultCode2
                               par:Fault_Code3  = stm:FaultCode3
                               par:Fault_Code4  = stm:FaultCode4
                               par:Fault_Code5  = stm:FaultCode5
                               par:Fault_Code6  = stm:FaultCode6
                               par:Fault_Code7  = stm:FaultCode7
                               par:Fault_Code8  = stm:FaultCode8
                               par:Fault_Code9  = stm:FaultCode9
                               par:Fault_Code10 = stm:FaultCode10
                               par:Fault_Code11 = stm:FaultCode11
                               par:Fault_Code12 = stm:FaultCode12
                           Else!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                               !Error
                               !Assert(0,'<13,10>Fetch Error<13,10>')
                           End!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                        End !If sto:Assign_Fault_Codes = 'YES'
                        if access:parts.insert()
                            access:parts.cancelautoinc()
                        end
                    End !If access:Prime

                End !If job:Chargeable_Job = 'YES'
             End !If job:Warranty_Job = 'YES'

        Else ! If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
             !Error
        End !If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
    End !If FoundEXCHPart# = 0
create:ExchangeOrder        Routine
    if (Access:EXCHOR48.PrimeRecord() = Level:Benign)
        ex4:Location    = p_web.GSV('Default:SiteLocation')
        ex4:Manufacturer    = p_web.GSV('locExchangeManufacturer')
        ex4:ModelNumber    = p_web.GSV('locExchangeModelNumber')
        ex4:Received    = 0
        ex4:Notes    = p_web.GSV('locExchangeNotes')
        ex4:JobNumber    = p_web.GSV('wob:RefNumber')

        if (Access:EXCHOR48.TryInsert() = Level:Benign)
                    ! Inserted

            p_web.SSV('jobe:Engineer48HourOption',1)
            p_web.SSV('locEngineeringOption','48 Hour Exchange')

            p_web.SSV('AddToAudit:Type','JOB')
            p_web.SSV('AddToAudit:Action','48 HOUR EXCHANGE ORDER CREATED')
            p_web.SSV('AddToAudit:Notes','MANUFACTURER: ' & p_web.GSV('locExchangeManufacturer') & |
                '<13,10>MODEL NUMBER: ' & p_web.GSV('locExchangeModelNumber'))
            AddToAudit(p_web)

            p_web.SSV('AddToAudit:Type','JOB')
            p_web.SSV('AddToAudit:Action','ENGINEERING OPTION SELECTED: 48 HOUR EXCHANGE')
            p_web.SSV('AddToAudit:Notes','')
            AddToAudit(p_web)




            GetStatus(360,0,'EXC',p_web)



            GetStatus(355,0,'JOB',p_web)

! #11939 Don't force split (even though original spec states you should) (Bryan: 01/03/2011)
!            !If warranty job, auto change to spilt chargeable - 3876 (DBH: 29-03-2004)
!            if (p_web.GSV('job:Warranty_Job') = 'YES' and p_web.GSV('job:Chargeable_Job') <> 'YES')
!                p_web.SSV('job:Chargeable_Job','YES')
!                if p_web.GSV('BookingSite') = 'RRC'
!                    p_web.SSV('job:Charge_Type','NON-WARR SERVICE FEE')
!                else ! if p_web.GSV('BookingSite') = 'RRC'
!                    p_web.SSV('job:Charge_Type','NON-WARRANTY')
!                end ! if p_web.GSV('BookingSite') = 'RRC'
!                p_web.SSV('job:Repair_Type','48-HOUR SERVICE FEE')
!            end ! if ( p_web.GSV('job:Warranty_Job') = 'YES' and p_web.GSV('job:Chargeable_Job') <> 'YES')

            CASE (p_web.GSV('locSplitJob'))
            OF 1 ! Chargeable Only
                p_web.SSV('job:Chargeable_Job','YES')
                p_web.SSV('job:Warranty_Job','NO')
            OF 2 ! Warranty Only
                p_web.SSV('job:Chargeable_Job','NO')
                p_web.SSV('job:Warranty_Job','YES')
            OF 3 ! Split
                p_web.SSV('job:Chargeable_Job','YES')
                p_web.SSV('job:Warranty_Job','YES')
            END

            IF (p_web.GSV('job:Chargeable_Job') = 'YES')
                if p_web.GSV('BookingSite') = 'RRC'
                    p_web.SSV('job:Charge_Type','NON-WARR SERVICE FEE')
                else ! if p_web.GSV('BookingSite') = 'RRC'
                    p_web.SSV('job:Charge_Type','NON-WARRANTY')
                end ! if p_web.GSV('BookingSite') = 'RRC'
                p_web.SSV('job:Repair_Type','48-HOUR SERVICE FEE')
            END

            do add:ExchangeOrderPart

            Access:JOBSE.Clearkey(jobe:RefNumberKey)
            jobe:RefNumber    = p_web.GSV('wob:RefNumber')
            if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
                ! Found
                p_web.SessionQueueToFile(JOBSE)
                Access:JOBSE.TryUpdate()
            else ! if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
                ! Error
            end ! if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)

            Access:JOBS.Clearkey(job:Ref_Number_Key)
            job:Ref_Number    = p_web.GSV('wob:RefNumber')
            if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                            ! Found
                p_web.SessionQueueToFile(JOBS)
                Access:JOBS.TryUpdate()
            else ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                            ! Error
            end ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
        else ! if (Access:EXCHOR48.TryInsert() = Level:Benign)
                    ! Error
            Access:EXCHOR48.CancelAutoInc()
        end ! if (Access:EXCHOR48.TryInsert() = Level:Benign)
    end ! if (Access:EXCHOR48.PrimeRecord() = Level:Benign)

    p_web.SSV('exchangeOrderCreated',1)
    p_web.SSV('ExchangeOrder:RecordNumber',ex4:RecordNumber)
    p_web.SSV('locWarningMessage','Exchange Order Created')

set:HubRepair      routine
    p_web.SSV('jobe:HubRepairDate',Today())
    p_web.SSV('jobe:HubRepairTime',Clock())

    p_web.SSV('GetStatus:StatusNumber',sub(GETINI('RRC','StatusSendToARC',,CLIP(PATH())&'\SB2KDEF.INI'),1,3))
    p_web.SSV('GetStatus:Type','JOB')
    GetStatus(p_web.GSV('GetStatus:StatusNumber'),0,p_web.GSV('GetStatus:Type'),p_web)

    if (p_web.GSV('BookingSite') = 'RRC')
        if (p_web.GSV('job:Exchange_Unit_Number') > 0)
            Access:REPTYDEF.Clearkey(rtd:ManRepairTypeKey)
            rtd:Manufacturer    = p_web.GSV('job:Manufacturer')
            set(rtd:ManRepairTypeKey,rtd:ManRepairTypeKey)
            loop
                if (Access:REPTYDEF.Next())
                    Break
                end ! if (Access:REPTYDEF.Next())
                if (rtd:Manufacturer    <> p_web.GSV('job:Manufacturer'))
                    Break
                end ! if (rtd:Manufacturer    <> p_web.GSV('job:Manufacturer'))
                if (rtd:BER = 11)
                    if (p_web.GSV('job:Chargeable_Job') = 'YES' and p_web.GSV('job:Repair_Type') = '')
                        p_web.SSV('job:Repair_Type',rtd:Repair_Type)
                    end ! if (p_web.GSV('job:Chargeable_Job') = 'YES' and p_web.GSV('job:Repair_Type') = '')
                    if (p_web.GSV('job:Warranty_Job') = 'YES' and p_web.GSV('job:Repair_Type_Warranty') = '')
                        p_web.SSV('job:Repair_Type_Warranty',rtd:Repair_Type)
                    end ! if (p_web.GSV('job:Chargeable_Job') = 'YES' and p_web.GSV('job:Repair_Type') = '')
                    break
                end ! if (rtd:BER = 11)
            end ! loop
        end ! if (p_web.GSV('job:Exchange_Unit_Number') > 0)
    end ! if (p_web.GSV('BookingSite') = 'RRC')


    Access:JOBSENG.Clearkey(joe:UserCodeKey)
    joe:JobNumber    = p_web.GSV('job:Ref_Number')
    joe:UserCode    = p_web.GSV('job:Engineer')
    joe:DateAllocated    = Today()
    set(joe:UserCodeKey,joe:UserCodeKey)
    loop
        if (Access:JOBSENG.Previous())
            Break
        end ! if (Access:JOBSENG.Next())
        if (joe:JobNumber    <> p_web.GSV('job:Ref_Number'))
            Break
        end ! if (joe:JobNumber    <> p_web.GSV('job:Ref_Number'))
        if (joe:UserCode    <> p_web.GSV('job:Engineer'))
            Break
        end ! if (joe:UserCode    <> p_web.GSV('job:Engineer'))
        if (joe:DateAllocated    > Today())
            Break
        end ! if (joe:DateAllocated    <> Today())
        joe:Status = 'HUB'
        joe:StatusDate = Today()
        joe:StatusTime = Clock()
        access:JOBSENG.update()
        break
    end ! loop


    if (p_web.GSV('jobe2:SMSNotification'))
        if (p_web.GSV('job:Who_Booked') = 'WEB')
            AddEmailSMS(p_web.GSV('job:Ref_Number'),p_web.GSV('job:Account_Number'),'2ARC','SMS',p_web.GSV('jobe2:SMSAlertNumber'),'',0,'')
        else ! if (p_web.GSV('job:Who_Booked') = 'WEB')
            AddEmailSMS(p_web.GSV('job:Ref_Number'),p_web.GSV('BookingAccount'),'2ARC','SMS',p_web.GSV('jobe2:SMSAlertNumber'),'',0,'')
        end ! if (p_web.GSV('job:Who_Booked') = 'WEB')
    end ! if (jobe2:SMSNotification)
    if (p_web.GSV('jobe2:EmailNotification'))
        if (p_web.GSV('job:Who_Booked') = 'WEB')
            AddEmailSMS(p_web.GSV('job:Ref_Number'),p_web.GSV('job:Account_Number'),'2ARC','EMAIL','',p_web.GSV('jobe2:EmailAlertNumber'),0,'')
        else ! if (p_web.GSV('job:Who_Booked') = 'WEB')
            AddEmailSMS(p_web.GSV('job:Ref_Number'),p_web.GSV('BookingAccount'),'2ARC','EMAIL','',p_web.GSV('jobe2:EmailAlertNumber'),0,'')
        end ! if (p_web.GSV('job:Who_Booked') = 'WEB')
    end ! if (jobe2:SMSNotification)
Validate:locEngineeringOption           Routine
    p_web.SSV('locErrorMessage','')
    case p_web.GSV('locNewEngineeringOption')
    of 'Standard Repair'
        if (p_web.GSV('BookingSite') = 'RRC')
            Access:MODELNUM.Clearkey(mod:Model_Number_Key)
            mod:Model_Number    = p_web.GSV('job:Model_Number')
            if (Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign)
                ! Found
                if (mod:ExcludedRRCRepair)
                    p_web.SSV('locErrorMessage','Warning! This model should not be repaired by an RRC and should be sent to the Hub.')
                    if (isUnitLiquidDamaged(p_web.GSV('job:Ref_Number'),p_web.GSV('job:ESN')))
                        p_web.SSV('locErrorMessage','Error! Cannot send to ARC. This Unit Is Liquid Damaged.')
                        p_web.SSV('locValidationFailed',1)
                        p_web.SSV('locNewEngineeringOption','')
                        Exit
                    end ! if (isUnitLiquidDamage(p_web.GSV('job:Ref_Number'),p_web.GSV('job:ESN'))

                end ! if (mod:ExcludedRRCRepair)
            else ! if (Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign)
                ! Error
            end ! if (Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign)
        end ! if (p_web.GSV('BookingSite') = 'RRC')
    of '48 Hour Exchange'
        if (is48HourOrderCreated(p_web.GSV('BookingSiteLocation'),p_web.GSV('job:Ref_Number')) = 1)
            p_web.SSV('locErrorMessage','Error! A 48 Hour Exchange has already been requested for this job.')
            p_web.SSV('locValidationFailed',1)
            Exit
        end ! if (is48HourOrderCreated(p_web.GSV('BookingSiteLocation'),p_web.GSV('job:Ref_Number')) = 1)
        if (is48HourOrderProcessed(p_web.GSV('BookingSiteLocation'),p_web.GSV('job:Ref_Number')) = 1)
            p_web.SSV('locErrorMessage','Error! A 48 Hour Exchange Unit has already been requested and despatched for this job.')
            p_web.SSV('locValidationFailed',1)
            Exit
        end ! if (is48HourOrderProcessed(p_web.GSV('BookingSiteLocation'),p_web.GSV('job:Ref_Number')) = 1)
        IF (p_web.GSV('job:Chargeable_Job') <> 'YES')
            ! #11939 Job is warranty only, show the "split job" option. (Bryan: 01/03/2011)
            p_web.SSV('locSplitJob',0)
            p_web.SSV('Hide:SplitJob',0)
        ELSE
            ! #11980 Job is chargeable so no need to show split option. (Bryan: 01/03/2011)
            IF (p_web.GSV('job:Warranty_Job') = 'YES')
                p_web.SSV('locSplitJob',3)
            ELSE
                p_web.SSV('locSplitJob',1)
            END
        END

    of 'ARC Repair'
        if (isUnitLiquidDamaged(p_web.GSV('job:Ref_Number'),p_web.GSV('job:ESN')))
            p_web.SSV('locErrorMessage','Error! Cannot send to ARC. This Unit Is Liquid Damaged.')
            p_web.SSV('locValidationFailed',1)
            p_web.SSV('locNewEngineeringOption','')
            Exit
        end ! if (isUnitLiquidDamage(p_web.GSV('job:Ref_Number'),p_web.GSV('job:ESN'))
    of '7 Day TAT'
        if (isUnitLiquidDamaged(p_web.GSV('job:Ref_Number'),p_web.GSV('job:ESN')))
            p_web.SSV('locErrorMessage','Error! Cannot send to ARC. This Unit Is Liquid Damaged.')
            p_web.SSV('locValidationFailed',1)
            p_web.SSV('locNewEngineeringOption','')
            Exit
        end ! if (isUnitLiquidDamage(p_web.GSV('job:Ref_Number'),p_web.GSV('job:ESN'))
    end ! case p_web.GSV('locEngineeringOption')
Validate:locExchangeModelNumber           Routine
    if (p_web.GSV('locExchangeModelNumber') <> p_web.GSV('job:Model_Number'))
        if (isThisModelAlternative(p_web.GSV('job:Model_Number'),p_web.GSV('locExchangeModelNumber')) = 0)
            p_web.SSV('locWarningMessage','Warning! The selected unit is not an "alternative" Model Number for this job!')
        else ! if (isThisModelAlternative(p_web.GSV('job:Model_Number'),p_web.GSV('locExchangeModelNumber')) = 0)
            p_web.SSV('locWarningMessage','Warning! You have selected a different Model Number for this job!')
        end ! if (isThisModelAlternative(p_web.GSV('job:Model_Number'),p_web.GSV('locExchangeModelNumber')) = 0)
    else ! if (p_web.GSV('locExchangeModelNumber') <> p_web.GSV('job:Model_Number'))
        p_web.SSV('locWarningMessage','')
    end ! if (p_web.GSV('locExchangeModelNumber') <> p_web.GSV('job:Model_Number'))

OpenFiles  ROUTINE
  p_web._OpenFile(USERS)
  p_web._OpenFile(ACCAREAS)
  p_web._OpenFile(MODELNUM)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(MANUFACT)
  p_web._OpenFile(STOCK)
  p_web._OpenFile(WARPARTS)
  p_web._OpenFile(PARTS)
  p_web._OpenFile(STOMODEL)
  p_web._OpenFile(AUDIT)
  p_web._OpenFile(STOCKALL)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(JOBSENG)
  p_web._OpenFile(REPTYDEF)
  p_web._OpenFile(WEBJOB)
  p_web._OpenFile(EXCHOR48)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(USERS)
  p_Web._CloseFile(ACCAREAS)
  p_Web._CloseFile(MODELNUM)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(MANUFACT)
  p_Web._CloseFile(STOCK)
  p_Web._CloseFile(WARPARTS)
  p_Web._CloseFile(PARTS)
  p_Web._CloseFile(STOMODEL)
  p_Web._CloseFile(AUDIT)
  p_Web._CloseFile(STOCKALL)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(JOBSENG)
  p_Web._CloseFile(REPTYDEF)
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(EXCHOR48)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  !Init Form
  if (p_web.GSV('FormEngineeringOption:FirstTime') = 1)
      p_web.SSV('locPasswordValidated',0)
      p_web.SSV('locValidationFailed',0)
      p_web.SSV('locUserPassword','')
      p_web.SSV('locErrorMessage','')
      p_web.SSV('locWarningMessage','')
      p_web.SSV('Comment:UserPassword','Enter User Password and press [TAB]')
      p_web.SSV('filter:Manufacturer','')
      p_web.SSV('locNewEngineeringOption','')
      p_web.SSV('locExchangeManufacturer',p_web.GSV('job:Manufacturer'))
      p_web.SSV('locExchangeModelNumber',p_web.GSV('job:Model_number'))
      p_web.SSV('locExchangeNotes','')
      p_web.SSV('locSplitJob',0)
      p_web.SSV('Hide:SplitJob',1)
  
      p_web.SSV('exchangeOrderCreated',0)
  
      p_web.SSV('FormEngineeringOption:FirstTime',0)
  
      ! Activate the 48 Hour option and show the booking option (DBH: 24-03-2005)
      p_web.SSV('hide:48HourOption',0)
      If AccountActivate48Hour(p_web.GSV('wob:HeadAccountNumber')) = True
          If Allow48Hour(p_web.GSV('job:ESN'),p_web.GSV('job:Model_Number'),p_web.GSV('wob:HeadAccountNumber')) = True
  
          Else ! If Allow48Hour(job:ESN,job:Model_Number) = True
              p_web.SSV('hide:48HourOption',1)
          End ! If Allow48Hour(job:ESN,job:Model_Number) = True
      Else ! AccountActivate48Hour(wob:HeadAccountNumber) = True
          p_web.SSV('hide:48HourOption',1)
      End ! AccountActivate48Hour(wob:HeadAccountNumber) = True
  
  end ! if (p_web.GSV('FormEngineeringOption:FirstTime') = 1)
  p_web.SetValue('FormEngineeringOption_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'FormEngineeringOption'
    end
    p_web.formsettings.proc = 'FormEngineeringOption'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('job:DOP')
    p_web.SetPicture('job:DOP',p_web.site.DatePicture)
  End
  p_web.SetSessionPicture('job:DOP',p_web.site.DatePicture)

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'locExchangeManufacturer'
    p_web.setsessionvalue('showtab_FormEngineeringOption',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(MANUFACT)
      p_web.SSV('filter:ModelNumber','Upper(mod:Manufacturer) = Upper(<39>' & clip(man:Manufacturer) & '<39>)')
      
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.locExchangeModelNumber')
  Of 'locExchangeModelNumber'
    p_web.setsessionvalue('showtab_FormEngineeringOption',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(MODELNUM)
      do Validate:locExchangeModelNumber
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.locExchangeNotes')
  End
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  if p_web.IfExistsValue('locUserPassword') = 0
    p_web.SetSessionValue('locUserPassword',locUserPassword)
  Else
    locUserPassword = p_web.GetSessionValue('locUserPassword')
  End
  if p_web.IfExistsValue('locEngineeringOption') = 0
    p_web.SetSessionValue('locEngineeringOption',locEngineeringOption)
  Else
    locEngineeringOption = p_web.GetSessionValue('locEngineeringOption')
  End
  if p_web.IfExistsValue('locNewEngineeringOption') = 0
    p_web.SetSessionValue('locNewEngineeringOption',locNewEngineeringOption)
  Else
    locNewEngineeringOption = p_web.GetSessionValue('locNewEngineeringOption')
  End
  if p_web.IfExistsValue('job:DOP') = 0
    p_web.SetSessionValue('job:DOP',job:DOP)
  Else
    job:DOP = p_web.GetSessionValue('job:DOP')
  End
  if p_web.IfExistsValue('locExchangeManufacturer') = 0
    p_web.SetSessionValue('locExchangeManufacturer',locExchangeManufacturer)
  Else
    locExchangeManufacturer = p_web.GetSessionValue('locExchangeManufacturer')
  End
  if p_web.IfExistsValue('locExchangeModelNumber') = 0
    p_web.SetSessionValue('locExchangeModelNumber',locExchangeModelNumber)
  Else
    locExchangeModelNumber = p_web.GetSessionValue('locExchangeModelNumber')
  End
  if p_web.IfExistsValue('locExchangeNotes') = 0
    p_web.SetSessionValue('locExchangeNotes',locExchangeNotes)
  Else
    locExchangeNotes = p_web.GetSessionValue('locExchangeNotes')
  End
  if p_web.IfExistsValue('locSplitJob') = 0
    p_web.SetSessionValue('locSplitJob',locSplitJob)
  Else
    locSplitJob = p_web.GetSessionValue('locSplitJob')
  End

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locUserPassword')
    locUserPassword = p_web.GetValue('locUserPassword')
    p_web.SetSessionValue('locUserPassword',locUserPassword)
  Else
    locUserPassword = p_web.GetSessionValue('locUserPassword')
  End
  if p_web.IfExistsValue('locEngineeringOption')
    locEngineeringOption = p_web.GetValue('locEngineeringOption')
    p_web.SetSessionValue('locEngineeringOption',locEngineeringOption)
  Else
    locEngineeringOption = p_web.GetSessionValue('locEngineeringOption')
  End
  if p_web.IfExistsValue('locNewEngineeringOption')
    locNewEngineeringOption = p_web.GetValue('locNewEngineeringOption')
    p_web.SetSessionValue('locNewEngineeringOption',locNewEngineeringOption)
  Else
    locNewEngineeringOption = p_web.GetSessionValue('locNewEngineeringOption')
  End
  if p_web.IfExistsValue('job:DOP')
    job:DOP = p_web.dformat(clip(p_web.GetValue('job:DOP')),p_web.site.DatePicture)
    p_web.SetSessionValue('job:DOP',job:DOP)
  Else
    job:DOP = p_web.GetSessionValue('job:DOP')
  End
  if p_web.IfExistsValue('locExchangeManufacturer')
    locExchangeManufacturer = p_web.GetValue('locExchangeManufacturer')
    p_web.SetSessionValue('locExchangeManufacturer',locExchangeManufacturer)
  Else
    locExchangeManufacturer = p_web.GetSessionValue('locExchangeManufacturer')
  End
  if p_web.IfExistsValue('locExchangeModelNumber')
    locExchangeModelNumber = p_web.GetValue('locExchangeModelNumber')
    p_web.SetSessionValue('locExchangeModelNumber',locExchangeModelNumber)
  Else
    locExchangeModelNumber = p_web.GetSessionValue('locExchangeModelNumber')
  End
  if p_web.IfExistsValue('locExchangeNotes')
    locExchangeNotes = p_web.GetValue('locExchangeNotes')
    p_web.SetSessionValue('locExchangeNotes',locExchangeNotes)
  Else
    locExchangeNotes = p_web.GetSessionValue('locExchangeNotes')
  End
  if p_web.IfExistsValue('locSplitJob')
    locSplitJob = p_web.GetValue('locSplitJob')
    p_web.SetSessionValue('locSplitJob',locSplitJob)
  Else
    locSplitJob = p_web.GetSessionValue('locSplitJob')
  End

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('FormEngineeringOption_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'ViewJob'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormEngineeringOption_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormEngineeringOption_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormEngineeringOption_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'ViewJob'

GenerateForm   Routine
  do LoadRelatedRecords
 locUserPassword = p_web.RestoreValue('locUserPassword')
 locEngineeringOption = p_web.RestoreValue('locEngineeringOption')
 locNewEngineeringOption = p_web.RestoreValue('locNewEngineeringOption')
 locExchangeManufacturer = p_web.RestoreValue('locExchangeManufacturer')
 locExchangeModelNumber = p_web.RestoreValue('locExchangeModelNumber')
 locExchangeNotes = p_web.RestoreValue('locExchangeNotes')
 locSplitJob = p_web.RestoreValue('locSplitJob')
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Change Engineering Option') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Change Engineering Option',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_FormEngineeringOption',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_FormEngineeringOption0_div')&'">'&p_web.Translate('Confirm User')&'</a></li>'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_FormEngineeringOption1_div')&'">'&p_web.Translate('Engineering Option')&'</a></li>'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_FormEngineeringOption2_div')&'">'&p_web.Translate()&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="FormEngineeringOption_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.cancel();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CancelButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="FormEngineeringOption_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'FormEngineeringOption_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="FormEngineeringOption_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'FormEngineeringOption_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
    If upper(p_web.getvalue('LookupFile'))='MANUFACT'
          If Not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
            p_web.SetValue('SelectField',clip(loc:formname) & '.locExchangeModelNumber')
          End
    End
    If upper(p_web.getvalue('LookupFile'))='MODELNUM'
          If Not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
            p_web.SetValue('SelectField',clip(loc:formname) & '.locExchangeNotes')
          End
    End
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locUserPassword')
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_FormEngineeringOption')>0,p_web.GSV('showtab_FormEngineeringOption'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_FormEngineeringOption'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_FormEngineeringOption') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_FormEngineeringOption'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_FormEngineeringOption')>0,p_web.GSV('showtab_FormEngineeringOption'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_FormEngineeringOption') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Confirm User') & ''''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Engineering Option') & ''''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_FormEngineeringOption_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_FormEngineeringOption')>0,p_web.GSV('showtab_FormEngineeringOption'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"FormEngineeringOption",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_FormEngineeringOption')>0,p_web.GSV('showtab_FormEngineeringOption'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_FormEngineeringOption_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('FormEngineeringOption') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('FormEngineeringOption')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Confirm User')&'</a></h3>' & CRLF & p_web.DivHeader('tab_FormEngineeringOption0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEngineeringOption0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEngineeringOption0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEngineeringOption0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Confirm User')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEngineeringOption0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Confirm User')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEngineeringOption0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Confirm User')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEngineeringOption0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locUserPassword
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locUserPassword
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locUserPassword
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab1  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Engineering Option')&'</a></h3>' & CRLF & p_web.DivHeader('tab_FormEngineeringOption1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEngineeringOption1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEngineeringOption1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEngineeringOption1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Engineering Option')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEngineeringOption1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Engineering Option')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEngineeringOption1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Engineering Option')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEngineeringOption1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::locErrorMessage
        do Comment::locErrorMessage
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locEngineeringOption
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locEngineeringOption
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locEngineeringOption
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locNewEngineeringOption
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locNewEngineeringOption
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locNewEngineeringOption
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab2  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate()&'</a></h3>' & CRLF & p_web.DivHeader('tab_FormEngineeringOption2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEngineeringOption2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEngineeringOption2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEngineeringOption2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,)
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEngineeringOption2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend)
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEngineeringOption2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain">' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_FormEngineeringOption2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::text:ExchangeOrder
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::text:ExchangeOrder
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:DOP
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:DOP
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::job:DOP
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locExchangeManufacturer
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locExchangeManufacturer
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locExchangeManufacturer
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locExchangeModelNumber
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locExchangeModelNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locExchangeModelNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locExchangeNotes
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locExchangeNotes
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locExchangeNotes
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locWarningMessage
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locWarningMessage
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locSplitJob
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locSplitJob
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locSplitJob
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::button:CreateOrder
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::button:CreateOrder
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::button:PrintOrder
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::button:PrintOrder
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Prompt::locUserPassword  Routine
  packet = clip(packet) & p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('locUserPassword') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Enter Password'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locUserPassword  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locUserPassword = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locUserPassword = p_web.GetValue('Value')
  End
  do ValidateValue::locUserPassword  ! copies value to session value if valid.
      p_web.SSV('locPasswordValidated',0)
      p_web.SSV('Comment:UserPassword','User Does Not Have Access')
  
      Access:USERS.Clearkey(use:Password_Key)
      use:Password    = p_web.GSV('locUserPassword')
      if (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
          ! Found
          Access:ACCAREAS.Clearkey(acc:Access_Level_Key)
          acc:User_Level  = use:User_Level
          acc:Access_Area = 'AMEND ENGINEERING OPTION'
          If Access:ACCAREAS.Tryfetch(acc:Access_Level_Key) = Level:Benign
              !Found
              p_web.SSV('locPasswordValidated',1)
              p_web.SSV('Comment:UserPassword','')
              p_web.SSV('headingExchangeOrder','Exchange Order')
          Else ! If Access:ACCAREAS.Tryfetch(Access_Level_Key) = Level:Benign
              !Error
          End !If Access:ACCAREAS.Tryfetch(Access_Level_Key) = Level:Benign
      else ! if (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
          ! Error
      end ! if (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
  do Value::locUserPassword
  do SendAlert
  do Comment::locUserPassword ! allows comment style to be updated.
  do Prompt::locNewEngineeringOption
  do Value::locNewEngineeringOption  !1
  do Comment::locNewEngineeringOption
  do Comment::locUserPassword
  do Prompt::locEngineeringOption
  do Value::locEngineeringOption  !1
  do Comment::locEngineeringOption

ValidateValue::locUserPassword  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locUserPassword',locUserPassword).
    End

Value::locUserPassword  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('locUserPassword') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    locUserPassword = p_web.RestoreValue('locUserPassword')
    do ValidateValue::locUserPassword
    If locUserPassword:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- locUserPassword
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locUserPassword'',''formengineeringoption_locuserpassword_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locUserPassword')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('password','locUserPassword',p_web.GetSessionValueFormat('locUserPassword'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locUserPassword  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locUserPassword:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:UserPassword'))
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('locUserPassword') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::locErrorMessage  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::locErrorMessage  ! copies value to session value if valid.
  do Comment::locErrorMessage ! allows comment style to be updated.

ValidateValue::locErrorMessage  Routine
    If not (1=0)
    End

Value::locErrorMessage  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('locErrorMessage') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="locErrorMessage" class="'&clip('red bold large')&'"'&clip(loc:extra)&'>' & p_web.Translate(p_web.GSV('locErrorMessage'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locErrorMessage  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locErrorMessage:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('locErrorMessage') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locEngineeringOption  Routine
  packet = clip(packet) & p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('locEngineeringOption') & '_prompt',Choose(p_web.GSV('locPasswordValidated') = 0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('locPasswordValidated') = 0,'',p_web.Translate('Current Option'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locEngineeringOption  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locEngineeringOption = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locEngineeringOption = p_web.GetValue('Value')
  End
  do ValidateValue::locEngineeringOption  ! copies value to session value if valid.
  do Comment::locEngineeringOption ! allows comment style to be updated.

ValidateValue::locEngineeringOption  Routine
    If not (p_web.GSV('locPasswordValidated') = 0)
      if loc:invalid = '' then p_web.SetSessionValue('locEngineeringOption',locEngineeringOption).
    End

Value::locEngineeringOption  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('locPasswordValidated') = 0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('locEngineeringOption') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (p_web.GSV('locPasswordValidated') = 0)
  ! --- DISPLAY --- locEngineeringOption
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locEngineeringOption'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locEngineeringOption  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locEngineeringOption:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('locPasswordValidated') = 0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('locEngineeringOption') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('locPasswordValidated') = 0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locNewEngineeringOption  Routine
  packet = clip(packet) & p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('locNewEngineeringOption') & '_prompt',Choose(p_web.GSV('locPasswordValidated') = 0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('locPasswordValidated') = 0,'',p_web.Translate('New Engineering Option'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locNewEngineeringOption  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locNewEngineeringOption = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locNewEngineeringOption = p_web.GetValue('Value')
  End
  do ValidateValue::locNewEngineeringOption  ! copies value to session value if valid.
  do validate:locEngineeringOption
  p_web.SSV('locExchangeNotes',BHStripReplace(p_web.GSV('locExchangeNotes'),'<9>',''))
  p_web.SSV('locExchangeNotes',BHStripReplace(p_web.GSV('locExchangeNotes'),'<</DIV>',''))
  
  
  do Value::locNewEngineeringOption
  do SendAlert
  do Comment::locNewEngineeringOption ! allows comment style to be updated.
  do Value::locErrorMessage  !1
  do Prompt::job:DOP
  do Value::job:DOP  !1
  do Comment::job:DOP
  do Prompt::locExchangeManufacturer
  do Value::locExchangeManufacturer  !1
  do Comment::locExchangeManufacturer
  do Prompt::locExchangeModelNumber
  do Value::locExchangeModelNumber  !1
  do Comment::locExchangeModelNumber
  do Prompt::locExchangeNotes
  do Value::locExchangeNotes  !1
  do Comment::locExchangeNotes
  do Value::text:ExchangeOrder  !1
  do Value::button:CreateOrder  !1
  do Prompt::locSplitJob
  do Value::locSplitJob  !1

ValidateValue::locNewEngineeringOption  Routine
    If not (p_web.GSV('locPasswordValidated') = 0)
  If locNewEngineeringOption = ''
    loc:Invalid = 'locNewEngineeringOption'
    locNewEngineeringOption:IsInvalid = true
    loc:alert = p_web.translate('New Engineering Option') & ' ' & p_web.site.RequiredText
  End
      if loc:invalid = '' then p_web.SetSessionValue('locNewEngineeringOption',locNewEngineeringOption).
    End

Value::locNewEngineeringOption  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('locPasswordValidated') = 0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('locNewEngineeringOption') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,p_web.site.style.formselect,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,'formrqd')
  If loc:retrying
    locNewEngineeringOption = p_web.RestoreValue('locNewEngineeringOption')
    do ValidateValue::locNewEngineeringOption
    If locNewEngineeringOption:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('locPasswordValidated') = 0)
  ! --- DROPLIST ---
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locNewEngineeringOption'',''formengineeringoption_locnewengineeringoption_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locNewEngineeringOption')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('exchangeOrderCreated') = 1,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('locNewEngineeringOption',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
  loc:fieldclass = ''
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('locNewEngineeringOption') = 0
    p_web.SetSessionValue('locNewEngineeringOption','')
  end
    packet = clip(packet) & p_web.CreateOption('-------------------------------------','',choose('' = p_web.getsessionvalue('locNewEngineeringOption')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('locEngineeringOption') <> 'Standard Repair'
    packet = clip(packet) & p_web.CreateOption('Standard Repair','Standard Repair',choose('Standard Repair' = p_web.getsessionvalue('locNewEngineeringOption')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('locEngineeringOption') <> '48 Hour Exchange'  and p_web.GSV('hide:48HourOption') <> 1
    packet = clip(packet) & p_web.CreateOption('48 Hour Exchange','48 Hour Exchange',choose('48 Hour Exchange' = p_web.getsessionvalue('locNewEngineeringOption')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('locEngineeringOption') <> 'ARC Repair'
    packet = clip(packet) & p_web.CreateOption('ARC Repair','ARC Repair',choose('ARC Repair' = p_web.getsessionvalue('locNewEngineeringOption')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('locEngineeringOption') <> '7 Day TAT'
    packet = clip(packet) & p_web.CreateOption('7 Day TAT','7 Day TAT',choose('7 Day TAT' = p_web.getsessionvalue('locNewEngineeringOption')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web.DivFooter()
Comment::locNewEngineeringOption  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locNewEngineeringOption:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('locPasswordValidated') = 0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('locNewEngineeringOption') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('locPasswordValidated') = 0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::text:ExchangeOrder  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::text:ExchangeOrder  ! copies value to session value if valid.
  do Comment::text:ExchangeOrder ! allows comment style to be updated.

ValidateValue::text:ExchangeOrder  Routine
    If not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
    End

Value::text:ExchangeOrder  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('text:ExchangeOrder') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="text:ExchangeOrder" class="'&clip('blue bold')&'"'&clip(loc:extra)&'>' & p_web.Translate('Exchange Order',) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::text:ExchangeOrder  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if text:ExchangeOrder:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('text:ExchangeOrder') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange'
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::job:DOP  Routine
  packet = clip(packet) & p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('job:DOP') & '_prompt',Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','',p_web.Translate('Date Of Purchase'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:DOP  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:DOP = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value')
    job:DOP = p_web.dformat(clip(p_web.GetValue('Value')),p_web.site.DatePicture)
  End
  do ValidateValue::job:DOP  ! copies value to session value if valid.
  do Value::job:DOP
  do SendAlert
  do Comment::job:DOP ! allows comment style to be updated.

ValidateValue::job:DOP  Routine
    If not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
      if loc:invalid = '' then p_web.SetSessionValue('job:DOP',job:DOP).
    End

Value::job:DOP  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('job:DOP') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  If loc:retrying
    job:DOP = p_web.RestoreValue('job:DOP')
    do ValidateValue::job:DOP
    If job:DOP:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
  ! --- DATE --- job:DOP
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:DOP'',''formengineeringoption_job:dop_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = '1'
  loc:options = p_web.site.Dateoptions
  ! example of loc:options; loc:options = Choose(loc:options='','',clip(loc:options) & ',') & 'numberOfMonths: 3,showButtonPanel: true' ! see http://jqueryui.com/demos/datepicker/#options
  packet = clip(packet) & p_web.CreateDateInput ('job:DOP',p_web.GetSessionValue('job:DOP'),loc:fieldclass,loc:readonly,,,loc:javascript,loc:options,loc:extra,,15,,,,0)
  do SendPacket
  End
  p_web.DivFooter()
Comment::job:DOP  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if job:DOP:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web._DateFormat()
  loc:class = Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('job:DOP') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange'
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locExchangeManufacturer  Routine
  packet = clip(packet) & p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('locExchangeManufacturer') & '_prompt',Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','',p_web.Translate('Manufacturer'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locExchangeManufacturer  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locExchangeManufacturer = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locExchangeManufacturer = p_web.GetValue('Value')
  ElsIf p_web.RequestAjax = 1 and p_web.IfExistsValue('man:Manufacturer')
    locExchangeManufacturer = p_web.GetValue('man:Manufacturer')
  ElsIf p_web.RequestAjax = 1
    locExchangeManufacturer = man:Manufacturer
  End
  do ValidateValue::locExchangeManufacturer  ! copies value to session value if valid.
  p_Web.SetValue('lookupfield','locExchangeManufacturer')
  do AfterLookup
  do Value::locExchangeManufacturer
  do SendAlert
  do Comment::locExchangeManufacturer

ValidateValue::locExchangeManufacturer  Routine
    If not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
  If locExchangeManufacturer = ''
    loc:Invalid = 'locExchangeManufacturer'
    locExchangeManufacturer:IsInvalid = true
    loc:alert = p_web.translate('Manufacturer') & ' ' & p_web.site.RequiredText
  End
      if loc:invalid = '' then p_web.SetSessionValue('locExchangeManufacturer',locExchangeManufacturer).
    End

Value::locExchangeManufacturer  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('locExchangeManufacturer') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If p_web.GSV('exchangeOrderCreated') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,'formrqd')
  If loc:retrying
    locExchangeManufacturer = p_web.RestoreValue('locExchangeManufacturer')
    do ValidateValue::locExchangeManufacturer
    If locExchangeManufacturer:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
  ! --- STRING --- locExchangeManufacturer
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('exchangeOrderCreated') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locExchangeManufacturer'',''formengineeringoption_locexchangemanufacturer_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locExchangeManufacturer')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = 'readonly'
    packet = clip(packet) & p_web.CreateInput('text','locExchangeManufacturer',p_web.GetSessionValue('locExchangeManufacturer'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'!4
    if not loc:viewonly and not loc:readonly
      loc:fieldclass = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('SelectManufacturers')&'?LookupField=locExchangeManufacturer&Tab=2&ForeignField=man:Manufacturer&_sort=man:Manufacturer&Refresh=sort'),,,,,loc:fieldclass)
    End
  do SendPacket
  End
  p_web.DivFooter()
Comment::locExchangeManufacturer  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locExchangeManufacturer:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web._jsok(p_web.site.RequiredText)
  loc:class = Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('locExchangeManufacturer') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange'
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locExchangeModelNumber  Routine
  packet = clip(packet) & p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('locExchangeModelNumber') & '_prompt',Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','',p_web.Translate('Model Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locExchangeModelNumber  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locExchangeModelNumber = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locExchangeModelNumber = p_web.GetValue('Value')
  ElsIf p_web.RequestAjax = 1 and p_web.IfExistsValue('mod:Model_Number')
    locExchangeModelNumber = p_web.GetValue('mod:Model_Number')
  ElsIf p_web.RequestAjax = 1
    locExchangeModelNumber = mod:Model_Number
  End
  do ValidateValue::locExchangeModelNumber  ! copies value to session value if valid.
  p_Web.SetValue('lookupfield','locExchangeModelNumber')
  do AfterLookup
  do Value::locExchangeModelNumber
  do SendAlert
  do Comment::locExchangeModelNumber
  do Value::locWarningMessage  !1

ValidateValue::locExchangeModelNumber  Routine
    If not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
  If locExchangeModelNumber = ''
    loc:Invalid = 'locExchangeModelNumber'
    locExchangeModelNumber:IsInvalid = true
    loc:alert = p_web.translate('Model Number') & ' ' & p_web.site.RequiredText
  End
      if loc:invalid = '' then p_web.SetSessionValue('locExchangeModelNumber',locExchangeModelNumber).
    End

Value::locExchangeModelNumber  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('locExchangeModelNumber') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If p_web.GSV('exchangeOrderCreated') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,'formrqd')
  If loc:retrying
    locExchangeModelNumber = p_web.RestoreValue('locExchangeModelNumber')
    do ValidateValue::locExchangeModelNumber
    If locExchangeModelNumber:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
  ! --- STRING --- locExchangeModelNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('exchangeOrderCreated') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locExchangeModelNumber'',''formengineeringoption_locexchangemodelnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locExchangeModelNumber')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = 'readonly'
    packet = clip(packet) & p_web.CreateInput('text','locExchangeModelNumber',p_web.GetSessionValue('locExchangeModelNumber'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'!4
    if not loc:viewonly and not loc:readonly
      loc:fieldclass = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('SelectModelNumbers')&'?LookupField=locExchangeModelNumber&Tab=2&ForeignField=mod:Model_Number&_sort=mod:Model_Number&Refresh=sort'),,,,,loc:fieldclass)
    End
  do SendPacket
  End
  p_web.DivFooter()
Comment::locExchangeModelNumber  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locExchangeModelNumber:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web._jsok(p_web.site.RequiredText)
  loc:class = Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('locExchangeModelNumber') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange'
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locExchangeNotes  Routine
  packet = clip(packet) & p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('locExchangeNotes') & '_prompt',Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','',p_web.Translate('Notes'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locExchangeNotes  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locExchangeNotes = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locExchangeNotes = p_web.GetValue('Value')
  End
  do ValidateValue::locExchangeNotes  ! copies value to session value if valid.
  p_web.SSV('locExchangeNotes',BHStripReplace(p_web.GSV('locExchangeNotes'),'<9>',''))
  do Value::locExchangeNotes
  do SendAlert
  do Comment::locExchangeNotes ! allows comment style to be updated.

ValidateValue::locExchangeNotes  Routine
    If not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
    locExchangeNotes = Upper(locExchangeNotes)
      if loc:invalid = '' then p_web.SetSessionValue('locExchangeNotes',locExchangeNotes).
    End

Value::locExchangeNotes  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('locExchangeNotes') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry','TextEntry')
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    locExchangeNotes = p_web.RestoreValue('locExchangeNotes')
    do ValidateValue::locExchangeNotes
    If locExchangeNotes:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
  ! --- TEXT --- locExchangeNotes
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locExchangeNotes'',''formengineeringoption_locexchangenotes_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locExchangeNotes')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'readonly','')
  do SendPacket
  p_web.CreateTextArea('locExchangeNotes',p_web.GetSessionValue('locExchangeNotes'),3,40,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(locExchangeNotes),,,,Net:Send)
  do SendPacket
  End
  p_web.DivFooter()
Comment::locExchangeNotes  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locExchangeNotes:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('locExchangeNotes') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange'
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::locWarningMessage  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::locWarningMessage  ! copies value to session value if valid.
  do Comment::locWarningMessage ! allows comment style to be updated.

ValidateValue::locWarningMessage  Routine
    If not (1=0)
    End

Value::locWarningMessage  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('locWarningMessage') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="locWarningMessage" class="'&clip('green bold')&'"'&clip(loc:extra)&'>' & p_web.Translate(p_web.GSV('locWarningMessage'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locWarningMessage  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locWarningMessage:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('locWarningMessage') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locSplitJob  Routine
  packet = clip(packet) & p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('locSplitJob') & '_prompt',Choose(p_web.GSV('Hide:SplitJob') = 1 OR p_web.GSV('exchangeOrderCreated') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:SplitJob') = 1 OR p_web.GSV('exchangeOrderCreated') = 1,'',p_web.Translate('Select Job Type'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locSplitJob  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locSplitJob = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locSplitJob = p_web.GetValue('Value')
  End
  do ValidateValue::locSplitJob  ! copies value to session value if valid.
  do Value::locSplitJob
  do SendAlert
  do Comment::locSplitJob ! allows comment style to be updated.
  do Value::button:CreateOrder  !1

ValidateValue::locSplitJob  Routine
    If not (p_web.GSV('Hide:SplitJob') = 1 OR p_web.GSV('exchangeOrderCreated') = 1)
      if loc:invalid = '' then p_web.SetSessionValue('locSplitJob',locSplitJob).
    End

Value::locSplitJob  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:SplitJob') = 1 OR p_web.GSV('exchangeOrderCreated') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('locSplitJob') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If loc:retrying
    locSplitJob = p_web.RestoreValue('locSplitJob')
    do ValidateValue::locSplitJob
    If locSplitJob:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:SplitJob') = 1 OR p_web.GSV('exchangeOrderCreated') = 1)
  ! --- RADIO --- locSplitJob
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locSplitJob') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locSplitJob'',''formengineeringoption_locsplitjob_value'',1,'''&p_web._jsok(1)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locSplitJob')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locSplitJob',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locSplitJob_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Chargeable Only') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locSplitJob') = 2
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locSplitJob'',''formengineeringoption_locsplitjob_value'',1,'''&p_web._jsok(2)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locSplitJob')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locSplitJob',clip(2),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locSplitJob_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Warranty Only') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locSplitJob') = 3
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locSplitJob'',''formengineeringoption_locsplitjob_value'',1,'''&p_web._jsok(3)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locSplitJob')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locSplitJob',clip(3),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locSplitJob_3') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Split Warranty/Chargeable') & '<13,10>'
    packet = clip(packet) & p_web.br
  do SendPacket
  End
  p_web.DivFooter()
Comment::locSplitJob  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locSplitJob:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:SplitJob') = 1 OR p_web.GSV('exchangeOrderCreated') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('locSplitJob') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:SplitJob') = 1 OR p_web.GSV('exchangeOrderCreated') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::button:CreateOrder  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::button:CreateOrder  ! copies value to session value if valid.
  if (p_web.GSV('locExchangeManufacturer') <> '' and p_web.GSV('locExchangeModelNumber') <> '')
      do create:ExchangeOrder
  end ! if (p_web.GSV('locExchangeManufacturer') <> '' and |
  do Value::button:CreateOrder
  do Comment::button:CreateOrder ! allows comment style to be updated.
  do Value::button:PrintOrder  !1
  do Value::locExchangeManufacturer  !1
  do Value::locExchangeModelNumber  !1
  do Value::locExchangeNotes  !1
  do Value::locWarningMessage  !1
  do Prompt::locSplitJob
  do Value::locSplitJob  !1

ValidateValue::button:CreateOrder  Routine
    If not (p_web.GSV('exchangeOrderCreated') = 1 Or p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange' OR p_web.GSV('locSplitJob') = 0)
    End

Value::button:CreateOrder  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('exchangeOrderCreated') = 1 Or p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange' OR p_web.GSV('locSplitJob') = 0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('button:CreateOrder') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (p_web.GSV('exchangeOrderCreated') = 1 Or p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange' OR p_web.GSV('locSplitJob') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''button:CreateOrder'',''formengineeringoption_button:createorder_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','CreateExchangeOrder','Create Exch. Order',p_web.combine(Choose('Create Exch. Order' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,,loc:javascript,loc:disabled,'images\star.png',,,,,,,,,'nt-left')!h
  do SendPacket
  End
  p_web.DivFooter()
Comment::button:CreateOrder  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if button:CreateOrder:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('exchangeOrderCreated') = 1 Or p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange' OR p_web.GSV('locSplitJob') = 0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('button:CreateOrder') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('exchangeOrderCreated') = 1 Or p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange' OR p_web.GSV('locSplitJob') = 0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::button:PrintOrder  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::button:PrintOrder  ! copies value to session value if valid.
  do Comment::button:PrintOrder ! allows comment style to be updated.

ValidateValue::button:PrintOrder  Routine
    If not (p_web.GSV('exchangeOrderCreated') = 0)
    End

Value::button:PrintOrder  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('exchangeOrderCreated') = 0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('button:PrintOrder') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (p_web.GSV('exchangeOrderCreated') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','PrintOrder','Print Order',p_web.combine(Choose('Print Order' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.WindowOpen(clip('ExchangeOrder')&''&'','_blank'),,loc:disabled,'images\printer.png',,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::button:PrintOrder  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if button:PrintOrder:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('exchangeOrderCreated') = 0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormEngineeringOption_' & p_web._nocolon('button:PrintOrder') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('exchangeOrderCreated') = 0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('FormEngineeringOption_nexttab_' & 0)
    locUserPassword = p_web.GSV('locUserPassword')
    do ValidateValue::locUserPassword
    If loc:Invalid
      loc:retrying = 1
      do Value::locUserPassword
      !do SendAlert
      do Comment::locUserPassword ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('FormEngineeringOption_nexttab_' & 1)
    locEngineeringOption = p_web.GSV('locEngineeringOption')
    do ValidateValue::locEngineeringOption
    If loc:Invalid
      loc:retrying = 1
      do Value::locEngineeringOption
      !do SendAlert
      do Comment::locEngineeringOption ! allows comment style to be updated.
      !exit
    End
    locNewEngineeringOption = p_web.GSV('locNewEngineeringOption')
    do ValidateValue::locNewEngineeringOption
    If loc:Invalid
      loc:retrying = 1
      do Value::locNewEngineeringOption
      !do SendAlert
      do Comment::locNewEngineeringOption ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('FormEngineeringOption_nexttab_' & 2)
    job:DOP = p_web.GSV('job:DOP')
    do ValidateValue::job:DOP
    If loc:Invalid
      loc:retrying = 1
      do Value::job:DOP
      !do SendAlert
      do Comment::job:DOP ! allows comment style to be updated.
      !exit
    End
    locExchangeManufacturer = p_web.GSV('locExchangeManufacturer')
    do ValidateValue::locExchangeManufacturer
    If loc:Invalid
      loc:retrying = 1
      do Value::locExchangeManufacturer
      !do SendAlert
      do Comment::locExchangeManufacturer ! allows comment style to be updated.
      !exit
    End
    locExchangeModelNumber = p_web.GSV('locExchangeModelNumber')
    do ValidateValue::locExchangeModelNumber
    If loc:Invalid
      loc:retrying = 1
      do Value::locExchangeModelNumber
      !do SendAlert
      do Comment::locExchangeModelNumber ! allows comment style to be updated.
      !exit
    End
    locExchangeNotes = p_web.GSV('locExchangeNotes')
    do ValidateValue::locExchangeNotes
    If loc:Invalid
      loc:retrying = 1
      do Value::locExchangeNotes
      !do SendAlert
      do Comment::locExchangeNotes ! allows comment style to be updated.
      !exit
    End
    locSplitJob = p_web.GSV('locSplitJob')
    do ValidateValue::locSplitJob
    If loc:Invalid
      loc:retrying = 1
      do Value::locSplitJob
      !do SendAlert
      do Comment::locSplitJob ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_FormEngineeringOption_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('FormEngineeringOption_tab_' & 0)
    do GenerateTab0
  of lower('FormEngineeringOption_locUserPassword_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locUserPassword
      of event:timer
        do Value::locUserPassword
        do Comment::locUserPassword
      else
        do Value::locUserPassword
      end
  of lower('FormEngineeringOption_tab_' & 1)
    do GenerateTab1
  of lower('FormEngineeringOption_locNewEngineeringOption_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locNewEngineeringOption
      of event:timer
        do Value::locNewEngineeringOption
        do Comment::locNewEngineeringOption
      else
        do Value::locNewEngineeringOption
      end
  of lower('FormEngineeringOption_tab_' & 2)
    do GenerateTab2
  of lower('FormEngineeringOption_job:DOP_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:DOP
      of event:timer
        do Value::job:DOP
        do Comment::job:DOP
      else
        do Value::job:DOP
      end
  of lower('FormEngineeringOption_locExchangeManufacturer_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locExchangeManufacturer
      of event:timer
        do Value::locExchangeManufacturer
        do Comment::locExchangeManufacturer
      else
        do Value::locExchangeManufacturer
      end
  of lower('FormEngineeringOption_locExchangeModelNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locExchangeModelNumber
      of event:timer
        do Value::locExchangeModelNumber
        do Comment::locExchangeModelNumber
      else
        do Value::locExchangeModelNumber
      end
  of lower('FormEngineeringOption_locExchangeNotes_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locExchangeNotes
      of event:timer
        do Value::locExchangeNotes
        do Comment::locExchangeNotes
      else
        do Value::locExchangeNotes
      end
  of lower('FormEngineeringOption_locSplitJob_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locSplitJob
      of event:timer
        do Value::locSplitJob
        do Comment::locSplitJob
      else
        do Value::locSplitJob
      end
  of lower('FormEngineeringOption_button:CreateOrder_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::button:CreateOrder
      of event:timer
        do Value::button:CreateOrder
        do Comment::button:CreateOrder
      else
        do Value::button:CreateOrder
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('FormEngineeringOption_form:ready_',1)

  p_web.SetSessionValue('FormEngineeringOption_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_FormEngineeringOption',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('FormEngineeringOption_form:ready_',1)
  p_web.SetSessionValue('FormEngineeringOption_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormEngineeringOption',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('FormEngineeringOption_form:ready_',1)
  p_web.SetSessionValue('FormEngineeringOption_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('FormEngineeringOption:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('FormEngineeringOption_form:ready_',1)
  p_web.SetSessionValue('FormEngineeringOption_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('FormEngineeringOption:Primed',0)
  p_web.setsessionvalue('showtab_FormEngineeringOption',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
      If not (1=0)
          If p_web.IfExistsValue('locUserPassword')
            locUserPassword = p_web.GetValue('locUserPassword')
          End
      End
      If not (p_web.GSV('locPasswordValidated') = 0)
        If not (p_web.GSV('exchangeOrderCreated') = 1)
          If p_web.IfExistsValue('locNewEngineeringOption')
            locNewEngineeringOption = p_web.GetValue('locNewEngineeringOption')
          End
        End
      End
      If not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
          If p_web.IfExistsValue('job:DOP')
            job:DOP = p_web.GetValue('job:DOP')
          End
      End
      If not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
        If not (p_web.GSV('exchangeOrderCreated') = 1)
          If p_web.IfExistsValue('locExchangeManufacturer')
            locExchangeManufacturer = p_web.GetValue('locExchangeManufacturer')
          End
        End
      End
      If not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
        If not (p_web.GSV('exchangeOrderCreated') = 1)
          If p_web.IfExistsValue('locExchangeModelNumber')
            locExchangeModelNumber = p_web.GetValue('locExchangeModelNumber')
          End
        End
      End
      If not (p_web.GSV('locNewEngineeringOption') <> '48 Hour Exchange')
          If p_web.IfExistsValue('locExchangeNotes')
            locExchangeNotes = p_web.GetValue('locExchangeNotes')
          End
      End
      If not (p_web.GSV('Hide:SplitJob') = 1 OR p_web.GSV('exchangeOrderCreated') = 1)
          If p_web.IfExistsValue('locSplitJob')
            locSplitJob = p_web.GetValue('locSplitJob')
          End
      End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord
      ! Validate Form
      if (p_web.GSV('locPasswordValidated') = 0)
          loc:Invalid = 'locUserPassword'
          loc:Alert = 'Your password has not yet been validated'
          p_web.SSV('locUserPassword','')
          exit
      end ! if (p_web.GSV('local:PasswordValidated') = 0)
  
      if (p_web.GSV('locValidationFailed') = 1)
          loc:Invalid = 'locUserPassword'
          loc:Alert = p_web.GSV('locErrorMessage')
          exit
      end ! if (p_web.GSV('locValidationFailed') = 1)
  
  
  
      if ((p_web.GSV('locEngineeringOption') <> p_web.GSV('locNewEngineeringOption')) and p_web.GSV('locNewEngineeringOption') <> '')
          p_web.SSV('locEngineeringOption',p_web.GSV('locNewEngineeringOption'))
  
          case p_web.GSV('locNewEngineeringOption')
          of 'ARC Repair'
              if (p_web.GSV('BookingSite') = 'RRC' and p_web.GSV('jobe:HubRepair') = 0)
                  p_web.SSV('jobe:HubRepair',1)
                  do set:HubRepair
              end ! if (p_web.GSV('BookingSite') = 'RRC' and p_web.GSV('jobe:HubRepair') = 0)
                  p_web.SSV('AddToAudit:Type','JOB')
                  p_web.SSV('AddToAudit:Action','ENGINEERING OPTION SELECTED: ARC REPAIR')
                  p_web.SSV('AddToAudit:Notes','')
                  AddToAudit(p_web)
  
                  p_web.SSV('jobe:Engineer48HourOption',2)
  
  
          of '7 Day TAT'
              if (p_web.GSV('BookingSite') = 'RRC' and p_web.GSV('jobe:HubRepair') = 0)
                  p_web.SSV('jobe:HubRepair',1)
                  do set:HubRepair
              end ! if (p_web.GSV('BookingSite') = 'RRC' and p_web.GSV('jobe:HubRepair') = 0)
                  p_web.SSV('AddToAudit:Type','JOB')
                  p_web.SSV('AddToAudit:Action','ENGINEERING OPTION SELECTED: 7 DAY TAT')
                  p_web.SSV('AddToAudit:Notes','')
                  AddToAudit(p_web)
  
                  p_web.SSV('jobe:Engineer48HourOption',3)
  
          of 'Standard Repair'
              p_web.SSV('AddToAudit:Type','JOB')
              p_web.SSV('AddToAudit:Action','ENGINEERING OPTION SELECTED: STANDARD REPAIR')
              p_web.SSV('AddToAudit:Notes','')
              AddToAudit(p_web)
              p_web.SSV('jobe:Engineer48HourOption',4)
          end !case p_web.GSV('locNewEngineeringOption')
  
          Access:JOBSE.Clearkey(jobe:RefNumberKey)
          jobe:RefNumber    = p_web.GSV('wob:RefNumber')
          if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
              p_web.SessionQueueToFile(JOBSE)
              Access:JOBSE.tryUpdate()
          else ! if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
              ! Error
          end ! if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
  
          Access:JOBS.Clearkey(job:Ref_Number_Key)
          job:Ref_Number    = p_web.GSV('wob:RefNumber')
          if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
              ! Found
              p_web.SessionQueueToFile(JOBS)
              Access:JOBS.tryUpdate()
          else ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
              ! Error
          end ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
  
          Access:WEBJOB.Clearkey(wob:RefNumberKey)
          wob:RefNumber = p_web.GSV('wob:RefNumber')
          if (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
              ! Found
              p_web.SessionQueueToFile(WEBJOB)
              Access:WEBJOB.TryUpdate()
          end ! if (Access:WEBJOB.TryFetch(wob:Ref_Number_Key) = Level:Benign)
  
  
          p_web.SSV('FirstTime',1)
  
  !
  !        p_web.SSV('locMessage','Your changes have been saved')
  !        p_web.SSV('loc:Alert','Your changes have been saved')
  
      end ! if (p_web.GSV('locEngineeringOption') <> p_web.GSV('locNewEngineeringOption'))
  
  
  
  

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormEngineeringOption_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormEngineeringOption_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
    loc:InvalidTab += 1
    do ValidateValue::locUserPassword
    If loc:Invalid then exit.
  ! tab = 1
    loc:InvalidTab += 1
    do ValidateValue::locErrorMessage
    If loc:Invalid then exit.
    do ValidateValue::locEngineeringOption
    If loc:Invalid then exit.
    do ValidateValue::locNewEngineeringOption
    If loc:Invalid then exit.
  ! tab = 3
    loc:InvalidTab += 1
    do ValidateValue::text:ExchangeOrder
    If loc:Invalid then exit.
    do ValidateValue::job:DOP
    If loc:Invalid then exit.
    do ValidateValue::locExchangeManufacturer
    If loc:Invalid then exit.
    do ValidateValue::locExchangeModelNumber
    If loc:Invalid then exit.
    do ValidateValue::locExchangeNotes
    If loc:Invalid then exit.
    do ValidateValue::locWarningMessage
    If loc:Invalid then exit.
    do ValidateValue::locSplitJob
    If loc:Invalid then exit.
    do ValidateValue::button:CreateOrder
    If loc:Invalid then exit.
    do ValidateValue::button:PrintOrder
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('FormEngineeringOption:Primed',0)
  p_web.StoreValue('locUserPassword')
  p_web.StoreValue('')
  p_web.StoreValue('locEngineeringOption')
  p_web.StoreValue('locNewEngineeringOption')
  p_web.StoreValue('')
  p_web.StoreValue('job:DOP')
  p_web.StoreValue('locExchangeManufacturer')
  p_web.StoreValue('locExchangeModelNumber')
  p_web.StoreValue('locExchangeNotes')
  p_web.StoreValue('')
  p_web.StoreValue('locSplitJob')
  p_web.StoreValue('')
  p_web.StoreValue('')

BannerNewJobBooking  PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
FilesOpened     Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  GlobalErrors.SetProcedureName('BannerNewJobBooking')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'BannerNewJobBooking_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('BannerNewJobBooking','')
    p_web.DivHeader('BannerNewJobBooking',p_web.combine(p_web.site.style.formdiv,'fdiv'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('BannerNewJobBooking') = 0
        p_web.AddPreCall('BannerNewJobBooking')
        p_web.DivHeader('popup_BannerNewJobBooking','nt-hidden')
        p_web.DivHeader('BannerNewJobBooking',p_web.combine(p_web.site.style.formdiv,'fdiv'))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_BannerNewJobBooking_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_BannerNewJobBooking_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferBannerNewJobBooking',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_BannerNewJobBooking',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBannerNewJobBooking',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_BannerNewJobBooking',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBannerNewJobBooking',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_BannerNewJobBooking',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_BannerNewJobBooking',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBannerNewJobBooking',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_BannerNewJobBooking',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBannerNewJobBooking',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_BannerNewJobBooking',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_BannerNewJobBooking',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('BannerNewJobBooking')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('BannerNewJobBooking_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'BannerNewJobBooking'
    end
    p_web.formsettings.proc = 'BannerNewJobBooking'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine

AfterLookup Routine
  loc:TabNumber = -1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('BannerNewJobBooking_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferBannerNewJobBooking')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('BannerNewJobBooking_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('BannerNewJobBooking_ChainTo')
    loc:formaction = p_web.GetSessionValue('BannerNewJobBooking_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction

GenerateForm   Routine
  do LoadRelatedRecords
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
    do SendPacket
    Do heading
    do SendPacket
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
    Packet = clip(Packet) & p_web.DivHeader('Tab_BannerNewJobBooking',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="BannerNewJobBooking_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      packet = clip(packet) & '</div><13,10>' ! end id="BannerNewJobBooking_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'BannerNewJobBooking_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="BannerNewJobBooking_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'BannerNewJobBooking_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_BannerNewJobBooking')>0,p_web.GSV('showtab_BannerNewJobBooking'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_BannerNewJobBooking'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_BannerNewJobBooking') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_BannerNewJobBooking'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_BannerNewJobBooking')>0,p_web.GSV('showtab_BannerNewJobBooking'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_BannerNewJobBooking') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_BannerNewJobBooking_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_BannerNewJobBooking')>0,p_web.GSV('showtab_BannerNewJobBooking'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"BannerNewJobBooking",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_BannerNewJobBooking')>0,p_web.GSV('showtab_BannerNewJobBooking'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_BannerNewJobBooking_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('BannerNewJobBooking') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('BannerNewJobBooking')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine


NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_BannerNewJobBooking_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('BannerNewJobBooking_form:ready_',1)

  p_web.SetSessionValue('BannerNewJobBooking_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_BannerNewJobBooking',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('BannerNewJobBooking_form:ready_',1)
  p_web.SetSessionValue('BannerNewJobBooking_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_BannerNewJobBooking',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('BannerNewJobBooking_form:ready_',1)
  p_web.SetSessionValue('BannerNewJobBooking_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('BannerNewJobBooking:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('BannerNewJobBooking_form:ready_',1)
  p_web.SetSessionValue('BannerNewJobBooking_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('BannerNewJobBooking:Primed',0)
  p_web.setsessionvalue('showtab_BannerNewJobBooking',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('BannerNewJobBooking_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('BannerNewJobBooking_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('BannerNewJobBooking:Primed',0)

heading  Routine
  packet = clip(packet) & p_web.AsciiToUTF(|
    '<<table class="TopBanner"><13,10>'&|
    '    <<tr><13,10>'&|
    '        <<td width="140" aligh="left"><<img src="/images/bannerleft.gif" width="140" heigh="30"/><</td><13,10>'&|
    '        <<td width="670" align="center" class="BannerText">New Job Booking<</td><13,10>'&|
    '        <<td width="140" aligh="right"><<img src="/images/bannerright.gif" width="140" heigh="30"/><</td><13,10>'&|
    '    <</tr><13,10>'&|
    '    <<tr><13,10>'&|
    '        <<td width="140"><</td><13,10>'&|
    '        <<td width="670"><</td><13,10>'&|
    '        <<td width="140" align="right" class="SmallText"><<!-- Net:s:VersionNumber --><</td><13,10>'&|
    '    <</tr><13,10>'&|
    '<</table><13,10>'&|
    '<13,10>'&|
    '',net:OnlyIfUTF)
