

   MEMBER('WebServer_Phase4.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSE098.INC'),ONCE        !Local module procedure declarations
                     END


CheckIfTagged        PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
ReturnValue          BYTE                                  !
  CODE
    ReturnValue = 0
    Relate:TagFile.Open()
    Access:TagFile.ClearKey(tag:keyTagged)
    tag:sessionID = p_web.SessionID
    SET(tag:keyTagged,tag:keyTagged)
    LOOP UNTIL Access:TagFile.Next()
        IF (tag:sessionID <> p_web.SessionID)
            BREAK
        END
        IF (tag:tagged = 1)
            ReturnValue = 1
            BREAK
        END
    END
    Relate:TagFile.Close()
    RETURN ReturnValue
AveragePurchaseCost  PROCEDURE  (LONG fRefNumber,REAL fQuantity,*STRING fMessage) ! Declare Procedure
locCount             LONG                                  !
locCost              REAL                                  !
locMessage           STRING(1000)                          !
FilesOpened     BYTE(0)
  CODE
    Do OpenFiles

    fMessage = 'IN STOCK = ' & fQuantity
    locCount = 0
    locCost = 0

    Access:STOHIST.Clearkey(shi:Transaction_Type_Key)
    shi:Ref_Number = fRefNumber
    shi:Transaction_Type = 'ADD'
    shi:Date = TODAY()
    SET(shi:Transaction_Type_Key,shi:Transaction_Type_Key)
    LOOP UNTIL Access:STOHIST.NEXT()
        IF (shi:Ref_Number <> fRefNumber OR |
            shi:Transaction_Type <> 'ADD')
            BREAK
        END

        IF (shi:Notes = 'STOCK ADDED FROM ORDER' Or shi:Notes = 'INITIAL STOCK QUANTITY')
            IF (shi:Quantity <= fQuantity - locCount)
                locCount += shi:Quantity
                locCost += (shi:Quantity * shi:Purchase_Cost)
                fMessage = CLIP(fMessage) & '<13,10>' & |
                    (shi:Quantity) & '@' & Format(shi:Purchase_Cost,@n14.2) & ' = ' & Format((shi:Quantity * shi:Purchase_Cost),@n14.2)
            ELSE ! IF (shi:Quantity <= fQuantity - locCount)
                locCost += (fQuantity - locCount) * shi:Purchase_Cost
                fMessage = CLIP(fMessage) & '<13,10>' & |
                    (fQuantity - locCount) & '@' & Format(shi:Purchase_Cost,@n14.2) & ' = ' & Format((fQuantity - locCount) * shi:Purchase_Cost,@n14.2)
                locCount += fQuantity - locCount
            END ! IF (shi:Quantity <= fQuantity - locCount)

            IF (locCount >= fQuantity)
                BREAK
            END
        END
    END

    fMessage = CLIP(fMessage) & '<13,10>' & |
        'AVERAGE: ' & Format(locCost,@n14.2) & |
        ' / ' & locCount & ' = ' & Format(locCost / locCount,@n14.2)


    Do CloseFiles

    RETURN locCost / locCount


!--------------------------------------
OpenFiles  ROUTINE
  Access:STOHIST.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:STOHIST.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:STOHIST.Close
     FilesOpened = False
  END
ClearSBOPartsList    PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
FilesOpened     BYTE(0)
  CODE
    DO OpenFiles
    Access:SBO_OutParts.Clearkey(sout:PartNumberKey)
    sout:SessionID = p_web.SessionID
    SET(sout:PartNumberKey,sout:PartNumberKey)
    LOOP UNTIL Access:SBO_OutParts.Next()
        IF (sout:SessionID <> p_web.SessionID)
            BREAK
        END

        Access:SBO_OutParts.DeleteRecord(0)
    END

    DO CloseFiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:SBO_OutParts.Open                                 ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:SBO_OutParts.UseFile                              ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:SBO_OutParts.Close
     FilesOpened = False
  END
