

   MEMBER('WebServer_Phase4.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSE063.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSE019.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE020.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE064.INC'),ONCE        !Req'd for module callout resolution
                     END


PickEngineersNotes   PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locPickList          STRING(10000)                         !
locFinalList         STRING(10000)                         !
locFinalListField    STRING(10000)                         !
locFoundField        STRING(10000)                         !
FilesOpened     Long
JOBNOTES::State  USHORT
NOTESENG::State  USHORT
title:IsInvalid  Long
locPickList:IsInvalid  Long
locFinalList:IsInvalid  Long
button:AddSelected:IsInvalid  Long
button:RemoveSelected:IsInvalid  Long
gap:IsInvalid  Long
button:RemoveAll:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  GlobalErrors.SetProcedureName('PickEngineersNotes')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'PickEngineersNotes_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('PickEngineersNotes','')
    p_web.DivHeader('PickEngineersNotes',p_web.combine(p_web.site.style.formdiv,'fdiv'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('PickEngineersNotes') = 0
        p_web.AddPreCall('PickEngineersNotes')
        p_web.DivHeader('popup_PickEngineersNotes','nt-hidden')
        p_web.DivHeader('PickEngineersNotes',p_web.combine(p_web.site.style.formdiv,'fdiv'))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_PickEngineersNotes_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_PickEngineersNotes_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferPickEngineersNotes',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_PickEngineersNotes',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPickEngineersNotes',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_PickEngineersNotes',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPickEngineersNotes',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_PickEngineersNotes',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_PickEngineersNotes',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPickEngineersNotes',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_PickEngineersNotes',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPickEngineersNotes',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_PickEngineersNotes',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_PickEngineersNotes',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('PickEngineersNotes')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
clearVariables      ROUTINE
    p_web.DeleteSessionValue('locPickList')
    p_web.DeleteSessionValue('locFinalList')
    p_web.DeleteSessionValue('locFinalListField')
    p_web.DeleteSessionValue('locFoundField')
OpenFiles  ROUTINE
  p_web._OpenFile(JOBNOTES)
  p_web._OpenFile(NOTESENG)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(JOBNOTES)
  p_Web._CloseFile(NOTESENG)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('PickEngineersNotes_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'PickEngineersNotes'
    end
    p_web.formsettings.proc = 'PickEngineersNotes'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine
  do clearVariables

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  if p_web.IfExistsValue('locPickList') = 0
    p_web.SetSessionValue('locPickList',locPickList)
  Else
    locPickList = p_web.GetSessionValue('locPickList')
  End
  if p_web.IfExistsValue('locFinalList') = 0
    p_web.SetSessionValue('locFinalList',locFinalList)
  Else
    locFinalList = p_web.GetSessionValue('locFinalList')
  End

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locPickList')
    locPickList = p_web.GetValue('locPickList')
    p_web.SetSessionValue('locPickList',locPickList)
  Else
    locPickList = p_web.GetSessionValue('locPickList')
  End
  if p_web.IfExistsValue('locFinalList')
    locFinalList = p_web.GetValue('locFinalList')
    p_web.SetSessionValue('locFinalList',locFinalList)
  Else
    locFinalList = p_web.GetSessionValue('locFinalList')
  End

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('PickEngineersNotes_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'ViewJob'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('PickEngineersNotes_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('PickEngineersNotes_ChainTo')
    loc:formaction = p_web.GetSessionValue('PickEngineersNotes_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'ViewJob'

GenerateForm   Routine
  do LoadRelatedRecords
 locPickList = p_web.RestoreValue('locPickList')
 locFinalList = p_web.RestoreValue('locFinalList')
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Engineers Notes') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Engineers Notes',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_PickEngineersNotes',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_PickEngineersNotes0_div')&'">'&p_web.Translate()&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="PickEngineersNotes_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.cancel();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CancelButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="PickEngineersNotes_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'PickEngineersNotes_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="PickEngineersNotes_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'PickEngineersNotes_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locPickList')
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_PickEngineersNotes')>0,p_web.GSV('showtab_PickEngineersNotes'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_PickEngineersNotes'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_PickEngineersNotes') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_PickEngineersNotes'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_PickEngineersNotes')>0,p_web.GSV('showtab_PickEngineersNotes'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_PickEngineersNotes') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_PickEngineersNotes_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_PickEngineersNotes')>0,p_web.GSV('showtab_PickEngineersNotes'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"PickEngineersNotes",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_PickEngineersNotes')>0,p_web.GSV('showtab_PickEngineersNotes'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_PickEngineersNotes_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('PickEngineersNotes') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('PickEngineersNotes')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate()&'</a></h3>' & CRLF & p_web.DivHeader('tab_PickEngineersNotes0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_PickEngineersNotes0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_PickEngineersNotes0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_PickEngineersNotes0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,)
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_PickEngineersNotes0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend)
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_PickEngineersNotes0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain">' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_PickEngineersNotes0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::title
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locPickList
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locPickList
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locFinalList
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locFinalList
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::button:AddSelected
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::button:AddSelected
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::button:RemoveSelected
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::button:RemoveSelected
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::gap
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::button:RemoveAll
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Validate::title  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::title  ! copies value to session value if valid.

ValidateValue::title  Routine
    If not (1=0)
    End

Value::title  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickEngineersNotes_' & p_web._nocolon('title') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="title" class="'&clip('green bold')&'"'&clip(loc:extra)&'>' & p_web.Translate('Hold "CTRL" or "SHIFT" to select more than one entry.',) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::locPickList  Routine
  packet = clip(packet) & p_web.DivHeader('PickEngineersNotes_' & p_web._nocolon('locPickList') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locPickList  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locPickList = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locPickList = p_web.GetValue('Value')
  End
  do ValidateValue::locPickList  ! copies value to session value if valid.
  do Value::locFinalList  !1

ValidateValue::locPickList  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locPickList',locPickList).
    End

Value::locPickList  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickEngineersNotes_' & p_web._nocolon('locPickList') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,p_web.site.style.formselect,'FormEntry',)
  If loc:retrying
    locPickList = p_web.RestoreValue('locPickList')
    do ValidateValue::locPickList
    If locPickList:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- DROPLIST ---
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locPickList'',''pickengineersnotes_locpicklist_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locPickList')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('locPickList',loc:fieldclass,loc:readonly,30,420,1,loc:javascript,)
  loc:fieldclass = ''
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('locPickList') = 0
    p_web.SetSessionValue('locPickList','')
  end
    packet = clip(packet) & p_web.CreateOption('---- Select Engineers Note -----','',choose('' = p_web.getsessionvalue('locPickList')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
      access:noteseng.clearkey(noe:Notes_Key)
      noe:Reference = ''
      SET(noe:Notes_Key,noe:Notes_Key)
      LOOP
          IF (access:noteseng.next())
              BREAK
          END
          If Instring(';' & Clip(noe:notes),p_web.GSV('locFinalListField'),1,1)
              Cycle
          End ! If Instring(';' & Clip(acr:Accessory) & ';',p_web.GSV('locFinalListField'),1,1)
  
          loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
          packet = clip(packet) & p_web.CreateOption(CLIP(noe:Reference) & ' - ' & Clip(noe:Notes),noe:Notes,choose(noe:Notes = p_web.GSV('locPickList')),clip(loc:rowstyle),,)&CRLF
          loc:even = Choose(loc:even=1,2,1)
      END
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web.DivFooter()

Prompt::locFinalList  Routine
  packet = clip(packet) & p_web.DivHeader('PickEngineersNotes_' & p_web._nocolon('locFinalList') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locFinalList  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locFinalList = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locFinalList = p_web.GetValue('Value')
  End
  do ValidateValue::locFinalList  ! copies value to session value if valid.
  do Value::locFinalList
  do SendAlert
  do Value::locPickList  !1

ValidateValue::locFinalList  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locFinalList',locFinalList).
    End

Value::locFinalList  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickEngineersNotes_' & p_web._nocolon('locFinalList') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,p_web.site.style.formselect,'FormEntry',)
  If loc:retrying
    locFinalList = p_web.RestoreValue('locFinalList')
    do ValidateValue::locFinalList
    If locFinalList:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- DROPLIST ---
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locFinalList'',''pickengineersnotes_locfinallist_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locFinalList')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('locFinalList',loc:fieldclass,loc:readonly,30,350,,loc:javascript,)
  loc:fieldclass = ''
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('locFinalList') = 0
    p_web.SetSessionValue('locFinalList','')
  end
    packet = clip(packet) & p_web.CreateOption('--- Selected Engineers Note ----','',choose('' = p_web.getsessionvalue('locFinalList')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
      If p_web.GSV('locFinalListField') <> ''
          Loop x# = 1 To 10000
              If Sub(p_web.GSV('locFinalListField'),x#,2) = '|;'
                  Start# = x# + 2
                  Cycle
              End ! If Sub(p_web.GSV('locFinalListField'),x#,2) = '|;'
  
              If Start# > 0
                  If Sub(p_web.GSV('locFinalListField'),x#,2) = ';|'
                      locFoundField = Sub(p_web.GSV('locFinalListField'),Start#,x# - Start#)
  
                      If locFoundField <> ''
                          loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
                          packet = clip(packet) & p_web.CreateOption(CLIP(locFoundField),locFoundField,choose(locFoundField = p_web.GSV('locFinalList')),clip(loc:rowstyle),,)&CRLF
                          loc:even = Choose(loc:even=1,2,1)
                      End ! If tmp:FoundAccessory <> ''
                      Start# = 0
                  End ! If Sub(p_web.GSV('locFinalListField'),x#,2) = ';|'
              End ! If Start# > 0
          End ! Loop x# = 1 To 1000
  
          If Start# > 0
              locFoundField = Clip(Sub(p_web.GSV('locFinalListField'),Start#,30))
              If locFoundField <> ''
                  loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
                  packet = clip(packet) & p_web.CreateOption(CLIP(locFoundField),locFoundField,choose(locFoundField = p_web.GSV('locFinalList')),clip(loc:rowstyle),,)&CRLF
                  loc:even = Choose(loc:even=1,2,1)
              End ! If tmp:FoundAccessory <> ''
          End ! If Start# > 0
      End ! If p_web.GSV('locFinalListField') <> ''
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web.DivFooter()

Prompt::button:AddSelected  Routine
  packet = clip(packet) & p_web.DivHeader('PickEngineersNotes_' & p_web._nocolon('button:AddSelected') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::button:AddSelected  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::button:AddSelected  ! copies value to session value if valid.
      p_web.SSV('locFinalListField',p_web.GSV('locFinalListField') & p_web.GSV('locPickList'))
      p_web.SSV('locPickList','')
  do Value::button:AddSelected
  do Value::locFinalList  !1
  do Value::locPickList  !1

ValidateValue::button:AddSelected  Routine
    If not (1=0)
    End

Value::button:AddSelected  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickEngineersNotes_' & p_web._nocolon('button:AddSelected') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''button:AddSelected'',''pickengineersnotes_button:addselected_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','AddSelected','Add Selected',p_web.combine(Choose('Add Selected' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'SmallButtonFixed'),loc:formname,,,,loc:javascript,loc:disabled,,,,,,,,,,'nt-left')!h
  do SendPacket
  End
  p_web.DivFooter()

Prompt::button:RemoveSelected  Routine
  packet = clip(packet) & p_web.DivHeader('PickEngineersNotes_' & p_web._nocolon('button:RemoveSelected') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::button:RemoveSelected  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::button:RemoveSelected  ! copies value to session value if valid.
      locFinalListField = p_web.GSV('locFinalListField') & ';'
      locFinalList = '|;' & Clip(p_web.GSV('locFinalList')) & ';'
      Loop x# = 1 To 10000
          pos# = Instring(Clip(locFinalList),locFinalListField,1,1)
          If pos# > 0
              locFinalListField = Sub(locFinalListField,1,pos# - 1) & Sub(locFinalListField,pos# + Len(Clip(locFinalList)),10000)
              Break
          End ! If pos# > 0#
      End ! Loop x# = 1 To 1000
      p_web.SSV('locFinalListField',Sub(locFinalListField,1,Len(Clip(locFinalListField)) - 1))
      p_web.SSV('locFinalList','')
  do Value::button:RemoveSelected
  do Value::locFinalList  !1
  do Value::locPickList  !1

ValidateValue::button:RemoveSelected  Routine
    If not (1=0)
    End

Value::button:RemoveSelected  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickEngineersNotes_' & p_web._nocolon('button:RemoveSelected') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''button:RemoveSelected'',''pickengineersnotes_button:removeselected_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','DeleteSelected','Delete Selected',p_web.combine(Choose('Delete Selected' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'SmallButtonFixed'),loc:formname,,,,loc:javascript,loc:disabled,,,,,,,,,,'nt-left')!h
  do SendPacket
  End
  p_web.DivFooter()

Validate::gap  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::gap  ! copies value to session value if valid.

ValidateValue::gap  Routine
    If not (1)
    End

Value::gap  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickEngineersNotes_' & p_web._nocolon('gap') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Validate::button:RemoveAll  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::button:RemoveAll  ! copies value to session value if valid.
      p_web.SetSessionValue('locFinalListField','')
      p_web.SetSessionValue('locFinalList','')
      p_web.SetSessionValue('locPickList','')
  do Value::button:RemoveAll
  do Value::locFinalList  !1
  do Value::locPickList  !1

ValidateValue::button:RemoveAll  Routine
    If not (1=0)
    End

Value::button:RemoveAll  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickEngineersNotes_' & p_web._nocolon('button:RemoveAll') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''button:RemoveAll'',''pickengineersnotes_button:removeall_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','RemoveAll','Remove All',p_web.combine(Choose('Remove All' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'SmallButtonFixed'),loc:formname,,,,loc:javascript,loc:disabled,,,,,,,,,,'nt-left')!h
  do SendPacket
  End
  p_web.DivFooter()

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('PickEngineersNotes_nexttab_' & 0)
    locPickList = p_web.GSV('locPickList')
    do ValidateValue::locPickList
    If loc:Invalid
      loc:retrying = 1
      do Value::locPickList
      !do SendAlert
      !exit
    End
    locFinalList = p_web.GSV('locFinalList')
    do ValidateValue::locFinalList
    If loc:Invalid
      loc:retrying = 1
      do Value::locFinalList
      !do SendAlert
      !exit
    End
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_PickEngineersNotes_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('PickEngineersNotes_tab_' & 0)
    do GenerateTab0
  of lower('PickEngineersNotes_locPickList_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locPickList
      of event:timer
        do Value::locPickList
      else
        do Value::locPickList
      end
  of lower('PickEngineersNotes_locFinalList_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locFinalList
      of event:timer
        do Value::locFinalList
      else
        do Value::locFinalList
      end
  of lower('PickEngineersNotes_button:AddSelected_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::button:AddSelected
      of event:timer
        do Value::button:AddSelected
      else
        do Value::button:AddSelected
      end
  of lower('PickEngineersNotes_button:RemoveSelected_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::button:RemoveSelected
      of event:timer
        do Value::button:RemoveSelected
      else
        do Value::button:RemoveSelected
      end
  of lower('PickEngineersNotes_button:RemoveAll_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::button:RemoveAll
      of event:timer
        do Value::button:RemoveAll
      else
        do Value::button:RemoveAll
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('PickEngineersNotes_form:ready_',1)

  p_web.SetSessionValue('PickEngineersNotes_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_PickEngineersNotes',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('PickEngineersNotes_form:ready_',1)
  p_web.SetSessionValue('PickEngineersNotes_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_PickEngineersNotes',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('PickEngineersNotes_form:ready_',1)
  p_web.SetSessionValue('PickEngineersNotes_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('PickEngineersNotes:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('PickEngineersNotes_form:ready_',1)
  p_web.SetSessionValue('PickEngineersNotes_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('PickEngineersNotes:Primed',0)
  p_web.setsessionvalue('showtab_PickEngineersNotes',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
      If not (1=0)
          If p_web.IfExistsValue('locPickList')
            locPickList = p_web.GetValue('locPickList')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('locFinalList')
            locFinalList = p_web.GetValue('locFinalList')
          End
      End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('PickEngineersNotes_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
      If Clip(p_web.GSV('locFinalListField')) <> ''
          Loop x# = 1 To 10000
              If Sub(p_web.GSV('locFinalListField'),x#,2) = '|;'
                  Start# = x# + 2
                  Cycle
              End ! If Sub(p_web.GSV('locFinalListField'),x#,2) = '|;'
  
              If Start# > 0
                  If Sub(p_web.GSV('locFinalListField'),x#,2) = ';|'
                      locFoundField = Sub(p_web.GSV('locFinalListField'),Start#,x# - Start#)
  
                      If locFoundField <> ''
                          p_web.SSV('jbn:Engineers_Notes',p_web.GSV('jbn:Engineers_Notes') & '<13,10>' & |
                              CLIP(locFoundField))
                      End ! If locFoundField <> ''
                      Start# = 0
                  End ! If Sub(p_web.GSV('locFinalListField'),x#,2) = ';|'
              End ! If Start# > 0
          End ! Loop x# = 1 To 1000
  
          If Start# > 0
              locFoundField = Clip(Sub(p_web.GSV('locFinalListField'),Start#,30))
              If locFoundField <> ''
                  p_web.SSV('jbn:Engineers_Notes',p_web.GSV('jbn:Engineers_Notes') & '<13,10>' & |
                      CLIP(locFoundField))
              End ! If locFoundField <> ''
          End ! If Start# > 0
      End ! If Clip(p_web.GSV('locFinalListField') <> ''
  p_web.DeleteSessionValue('PickEngineersNotes_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
    do ValidateValue::title
    If loc:Invalid then exit.
    do ValidateValue::locPickList
    If loc:Invalid then exit.
    do ValidateValue::locFinalList
    If loc:Invalid then exit.
    do ValidateValue::button:AddSelected
    If loc:Invalid then exit.
    do ValidateValue::button:RemoveSelected
    If loc:Invalid then exit.
    do ValidateValue::gap
    If loc:Invalid then exit.
    do ValidateValue::button:RemoveAll
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
  do clearVariables
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('PickEngineersNotes:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('locPickList')
  p_web.StoreValue('locFinalList')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')

ProofOfPurchase      PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
tmp:DOP              DATE                                  !Date Of Purchase
tmp:JobType          STRING(1)                             !
tmp:POPType          STRING(20)                            !
tmp:WarrantyRefNo    STRING(30)                            !
tmp:POP              STRING(1)                             !
locPOPTypePassword   STRING(30)                            !
FilesOpened     Long
ACCAREAS::State  USHORT
USERS::State  USHORT
MANUFACT::State  USHORT
tmp:DOP:IsInvalid  Long
tmp:POP:IsInvalid  Long
locPOPTypePassword:IsInvalid  Long
tmp:POPType:IsInvalid  Long
tmp:WarrantyRefNo:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('ProofOfPurchase')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'ProofOfPurchase_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('ProofOfPurchase','')
    p_web.DivHeader('ProofOfPurchase',p_web.combine(p_web.site.style.formdiv,'fdiv'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('ProofOfPurchase') = 0
        p_web.AddPreCall('ProofOfPurchase')
        p_web.DivHeader('popup_ProofOfPurchase','nt-hidden')
        p_web.DivHeader('ProofOfPurchase',p_web.combine(p_web.site.style.formdiv,'fdiv'))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_ProofOfPurchase_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_ProofOfPurchase_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferProofOfPurchase',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_ProofOfPurchase',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferProofOfPurchase',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_ProofOfPurchase',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferProofOfPurchase',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_ProofOfPurchase',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_ProofOfPurchase',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferProofOfPurchase',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_ProofOfPurchase',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferProofOfPurchase',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_ProofOfPurchase',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_ProofOfPurchase',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('ProofOfPurchase')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(ACCAREAS)
  p_web._OpenFile(USERS)
  p_web._OpenFile(MANUFACT)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(ACCAREAS)
  p_Web._CloseFile(USERS)
  p_Web._CloseFile(MANUFACT)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  ! Reset Variables
  p_web.SSV('tmp:DOP','')
  p_web.SSV('tmp:JobType','')
  p_web.SSV('tmp:POPType','')
  p_web.SSV('tmp:WarrantyRefNo','')
  p_web.SSV('Comment:DOP','')
  p_web.SSV('Comment:POPTypePassword',kCommentPOPTypePassword)
  p_web.SSV('ReadOnly:POPType',1)
  p_web.SSV('locPOPTypePassword','')
  p_web.SetValue('ProofOfPurchase_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'ProofOfPurchase'
    end
    p_web.formsettings.proc = 'ProofOfPurchase'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('tmp:DOP')
    p_web.SetPicture('tmp:DOP','@d06b')
  End
  p_web.SetSessionPicture('tmp:DOP','@d06b')

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  if p_web.IfExistsValue('tmp:DOP') = 0
    p_web.SetSessionValue('tmp:DOP',tmp:DOP)
  Else
    tmp:DOP = p_web.GetSessionValue('tmp:DOP')
  End
  if p_web.IfExistsValue('tmp:POP') = 0
    p_web.SetSessionValue('tmp:POP',tmp:POP)
  Else
    tmp:POP = p_web.GetSessionValue('tmp:POP')
  End
  if p_web.IfExistsValue('locPOPTypePassword') = 0
    p_web.SetSessionValue('locPOPTypePassword',locPOPTypePassword)
  Else
    locPOPTypePassword = p_web.GetSessionValue('locPOPTypePassword')
  End
  if p_web.IfExistsValue('tmp:POPType') = 0
    p_web.SetSessionValue('tmp:POPType',tmp:POPType)
  Else
    tmp:POPType = p_web.GetSessionValue('tmp:POPType')
  End
  if p_web.IfExistsValue('tmp:WarrantyRefNo') = 0
    p_web.SetSessionValue('tmp:WarrantyRefNo',tmp:WarrantyRefNo)
  Else
    tmp:WarrantyRefNo = p_web.GetSessionValue('tmp:WarrantyRefNo')
  End

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('tmp:DOP')
    tmp:DOP = p_web.dformat(clip(p_web.GetValue('tmp:DOP')),'@d06b')
    p_web.SetSessionValue('tmp:DOP',tmp:DOP)
  Else
    tmp:DOP = p_web.GetSessionValue('tmp:DOP')
  End
  if p_web.IfExistsValue('tmp:POP')
    tmp:POP = p_web.GetValue('tmp:POP')
    p_web.SetSessionValue('tmp:POP',tmp:POP)
  Else
    tmp:POP = p_web.GetSessionValue('tmp:POP')
  End
  if p_web.IfExistsValue('locPOPTypePassword')
    locPOPTypePassword = p_web.GetValue('locPOPTypePassword')
    p_web.SetSessionValue('locPOPTypePassword',locPOPTypePassword)
  Else
    locPOPTypePassword = p_web.GetSessionValue('locPOPTypePassword')
  End
  if p_web.IfExistsValue('tmp:POPType')
    tmp:POPType = p_web.GetValue('tmp:POPType')
    p_web.SetSessionValue('tmp:POPType',tmp:POPType)
  Else
    tmp:POPType = p_web.GetSessionValue('tmp:POPType')
  End
  if p_web.IfExistsValue('tmp:WarrantyRefNo')
    tmp:WarrantyRefNo = p_web.GetValue('tmp:WarrantyRefNo')
    p_web.SetSessionValue('tmp:WarrantyRefNo',tmp:WarrantyRefNo)
  Else
    tmp:WarrantyRefNo = p_web.GetSessionValue('tmp:WarrantyRefNo')
  End

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('ProofOfPurchase_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'BillingConfirmation'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('ProofOfPurchase_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('ProofOfPurchase_ChainTo')
    loc:formaction = p_web.GetSessionValue('ProofOfPurchase_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'BillingConfirmation'

GenerateForm   Routine
  do LoadRelatedRecords
  p_web.SSV('tmp:WarrantyRefNo',p_web.GSV('jobe2:WarrantyRefNo'))
  
  p_web.SSV('tmp:DOP',p_web.GSV('job:DOP'))
  p_web.SSV('tmp:POP',p_web.GSV('job:POP'))
  p_web.SSV('tmp:POPType',p_web.GSV('jobe:POPType'))
 tmp:DOP = p_web.RestoreValue('tmp:DOP')
 tmp:POP = p_web.RestoreValue('tmp:POP')
 locPOPTypePassword = p_web.RestoreValue('locPOPTypePassword')
 tmp:POPType = p_web.RestoreValue('tmp:POPType')
 tmp:WarrantyRefNo = p_web.RestoreValue('tmp:WarrantyRefNo')
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Proof Of Purchase') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Proof Of Purchase',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_ProofOfPurchase',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_ProofOfPurchase0_div')&'">'&p_web.Translate('Details')&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="ProofOfPurchase_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.cancel();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CancelButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="ProofOfPurchase_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'ProofOfPurchase_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="ProofOfPurchase_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'ProofOfPurchase_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:DOP')
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_ProofOfPurchase')>0,p_web.GSV('showtab_ProofOfPurchase'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_ProofOfPurchase'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_ProofOfPurchase') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_ProofOfPurchase'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_ProofOfPurchase')>0,p_web.GSV('showtab_ProofOfPurchase'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_ProofOfPurchase') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Details') & ''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_ProofOfPurchase_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_ProofOfPurchase')>0,p_web.GSV('showtab_ProofOfPurchase'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"ProofOfPurchase",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_ProofOfPurchase')>0,p_web.GSV('showtab_ProofOfPurchase'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_ProofOfPurchase_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('ProofOfPurchase') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('ProofOfPurchase')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Details')&'</a></h3>' & CRLF & p_web.DivHeader('tab_ProofOfPurchase0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_ProofOfPurchase0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_ProofOfPurchase0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_ProofOfPurchase0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Details')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_ProofOfPurchase0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Details')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_ProofOfPurchase0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Details')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_ProofOfPurchase0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:DOP
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:DOP
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:DOP
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:POP
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:POP
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:POP
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locPOPTypePassword
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locPOPTypePassword
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locPOPTypePassword
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:POPType
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:POPType
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:POPType
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:WarrantyRefNo
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:WarrantyRefNo
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:WarrantyRefNo
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Prompt::tmp:DOP  Routine
  packet = clip(packet) & p_web.DivHeader('ProofOfPurchase_' & p_web._nocolon('tmp:DOP') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Activation / D.O.P.'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:DOP  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:DOP = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@d06b'  !FieldPicture = 
    tmp:DOP = p_web.Dformat(p_web.GetValue('Value'),'@d06b')
  End
  do ValidateValue::tmp:DOP  ! copies value to session value if valid.
  if (p_web.GSV('tmp:DOP') > today())
      p_web.SSV('comment:DOP','Invalid Date')
      p_web.SSV('tmp:DOP','')
  else !
      p_web.SSV('comment:DOP','')
  end ! if (p_web.GSV('tmp:DOP') > today())
  
  if (p_web.GSV('tmp:POP') = '' and p_web.GSV('tmp:DOP') <> '')
  
      Access:MANUFACT.Clearkey(man:Manufacturer_Key)
      man:Manufacturer    = p_web.GSV('job:Manufacturer')
      if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
          ! Found
      else ! if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
          ! Error
      end ! if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
      if (p_web.GSV('tmp:DOP') < (p_web.GSV('job:Date_Booked') - man:Warranty_Period))
          if (p_web.GSV('tmp:DOP') < (p_web.GSV('job:Date_Booked') - 730))
              p_web.SSV('tmp:POP','C')
          else ! if (p_web.GSV('tmp:DOP') < (p_web.GSV('job:Date_Booked') - 730))
              p_web.SSV('tmp:POP','S')
              ! DBH #11344 - Force Chargeable If Man/Model is One Year Warranty Only
              IF (IsOneYearWarranty(p_web.GSV('job:Manufacturer'),p_web.GSV('job:Model_Number')))
                  p_web.SSV('tmp:POP','C')
              END
          end ! if (p_web.GSV('tmp:DOP') < (p_web.GSV('job:Date_Booked') - 730))
      else ! if (p_web.GSV('tmp:DOP') < (p_web.GSV('job:Date_Booked') - man:Warranty_Period)
          p_web.SSV('tmp:POP','F')
      end ! if (p_web.GSV('tmp:DOP') < (p_web.GSV('job:Date_Booked') - man:Warranty_Period)
  end ! if (p_web.GSV('tmp:POP') = '')
  do Value::tmp:DOP
  do SendAlert
  do Comment::tmp:DOP ! allows comment style to be updated.
  do Comment::tmp:DOP
  do Value::tmp:POP  !1
  do Comment::tmp:POP

ValidateValue::tmp:DOP  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:DOP',tmp:DOP).
    End

Value::tmp:DOP  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ProofOfPurchase_' & p_web._nocolon('tmp:DOP') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    tmp:DOP = p_web.RestoreValue('tmp:DOP')
    do ValidateValue::tmp:DOP
    If tmp:DOP:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- tmp:DOP
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:DOP'',''proofofpurchase_tmp:dop_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:DOP')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:DOP',p_web.GetSessionValue('tmp:DOP'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@d06b',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:DOP  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:DOP:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:DOP'))
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ProofOfPurchase_' & p_web._nocolon('tmp:DOP') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:POP  Routine
  packet = clip(packet) & p_web.DivHeader('ProofOfPurchase_' & p_web._nocolon('tmp:POP') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Job Type'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:POP  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:POP = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:POP = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:POP  ! copies value to session value if valid.
  do Value::tmp:POP
  do SendAlert
  do Comment::tmp:POP ! allows comment style to be updated.

ValidateValue::tmp:POP  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:POP',tmp:POP).
    End

Value::tmp:POP  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ProofOfPurchase_' & p_web._nocolon('tmp:POP') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',' noButton')
  If loc:retrying
    tmp:POP = p_web.RestoreValue('tmp:POP')
    do ValidateValue::tmp:POP
    If tmp:POP:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- RADIO --- tmp:POP
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:POP') = 'F'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:POP'',''proofofpurchase_tmp:pop_value'',1,'''&p_web._jsok('F')&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:POP',clip('F'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:POP_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('First Year Warranty') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:POP') = 'S'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:POP'',''proofofpurchase_tmp:pop_value'',1,'''&p_web._jsok('S')&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:POP',clip('S'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:POP_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Second Year Warranty') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:POP') = 'O'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:POP'',''proofofpurchase_tmp:pop_value'',1,'''&p_web._jsok('O')&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:POP',clip('O'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:POP_3') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Out Of Box Failure') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:POP') = 'C'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:POP'',''proofofpurchase_tmp:pop_value'',1,'''&p_web._jsok('C')&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:POP',clip('C'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:POP_4') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Chargeable') & '<13,10>'
    packet = clip(packet) & p_web.br
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:POP  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:POP:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ProofOfPurchase_' & p_web._nocolon('tmp:POP') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locPOPTypePassword  Routine
  packet = clip(packet) & p_web.DivHeader('ProofOfPurchase_' & p_web._nocolon('locPOPTypePassword') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Enter Password'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locPOPTypePassword  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locPOPTypePassword = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locPOPTypePassword = p_web.GetValue('Value')
  End
  do ValidateValue::locPOPTypePassword  ! copies value to session value if valid.
  p_web.SSV('ReadOnly:POPType',1)
  p_web.SSV('Comment:POPTypePassword',kCommentPOPTypePassword)
  Access:USERS.ClearKey(use:password_key)
  use:Password = p_web.GSV('locPOPTypePassword')
  IF (Access:USERS.TryFetch(use:password_key) = Level:Benign)
      Access:ACCAREAS.ClearKey(acc:Access_level_key)
      acc:User_Level = use:User_Level
      acc:Access_Area = 'AMEND POP TYPE'
      IF (Access:ACCAREAS.TryFetch(acc:Access_level_key) = Level:Benign)
          p_web.SSV('ReadOnly:POPType',0)
  
      ELSE
          p_web.SSV('Comment:POPTypePassword','User Does Not Have Access')
      END
  ELSE
      p_web.SSV('Comment:POPTypePassword','Invalid Password')
  END
  do Value::locPOPTypePassword
  do SendAlert
  do Comment::locPOPTypePassword ! allows comment style to be updated.
  do Value::tmp:POPType  !1
  do Comment::locPOPTypePassword

ValidateValue::locPOPTypePassword  Routine
    If not (1=0)
    locPOPTypePassword = Upper(locPOPTypePassword)
      if loc:invalid = '' then p_web.SetSessionValue('locPOPTypePassword',locPOPTypePassword).
    End

Value::locPOPTypePassword  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ProofOfPurchase_' & p_web._nocolon('locPOPTypePassword') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    locPOPTypePassword = p_web.RestoreValue('locPOPTypePassword')
    do ValidateValue::locPOPTypePassword
    If locPOPTypePassword:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- locPOPTypePassword
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locPOPTypePassword'',''proofofpurchase_locpoptypepassword_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locPOPTypePassword')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('password','locPOPTypePassword',p_web.GetSessionValueFormat('locPOPTypePassword'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,'Enter Password And Press [TAB]',,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locPOPTypePassword  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locPOPTypePassword:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:POPTypePassword'))
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ProofOfPurchase_' & p_web._nocolon('locPOPTypePassword') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:POPType  Routine
  packet = clip(packet) & p_web.DivHeader('ProofOfPurchase_' & p_web._nocolon('tmp:POPType') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('POP Type'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:POPType  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:POPType = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:POPType = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:POPType  ! copies value to session value if valid.
  do Value::tmp:POPType
  do SendAlert
  do Comment::tmp:POPType ! allows comment style to be updated.

ValidateValue::tmp:POPType  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:POPType',tmp:POPType).
    End

Value::tmp:POPType  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ProofOfPurchase_' & p_web._nocolon('tmp:POPType') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,p_web.site.style.formselect,'FormEntry',)
  If loc:retrying
    tmp:POPType = p_web.RestoreValue('tmp:POPType')
    do ValidateValue::tmp:POPType
    If tmp:POPType:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- DROPLIST ---
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:POPType'',''proofofpurchase_tmp:poptype_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('ReadOnly:POPType'),'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:POPType',loc:fieldclass,loc:readonly,,,,loc:javascript,)
  loc:fieldclass = ''
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('tmp:POPType') = 0
    p_web.SetSessionValue('tmp:POPType','')
  end
    packet = clip(packet) & p_web.CreateOption('------------------','',choose('' = p_web.getsessionvalue('tmp:POPType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('POP','POP',choose('POP' = p_web.getsessionvalue('tmp:POPType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('BASTION','BASTION',choose('BASTION' = p_web.getsessionvalue('tmp:POPType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('NONE','NONE',choose('NONE' = p_web.getsessionvalue('tmp:POPType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:POPType  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:POPType:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ProofOfPurchase_' & p_web._nocolon('tmp:POPType') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:WarrantyRefNo  Routine
  packet = clip(packet) & p_web.DivHeader('ProofOfPurchase_' & p_web._nocolon('tmp:WarrantyRefNo') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Warranty Ref No'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:WarrantyRefNo  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:WarrantyRefNo = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:WarrantyRefNo = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:WarrantyRefNo  ! copies value to session value if valid.
  do Value::tmp:WarrantyRefNo
  do SendAlert
  do Comment::tmp:WarrantyRefNo ! allows comment style to be updated.

ValidateValue::tmp:WarrantyRefNo  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:WarrantyRefNo',tmp:WarrantyRefNo).
    End

Value::tmp:WarrantyRefNo  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ProofOfPurchase_' & p_web._nocolon('tmp:WarrantyRefNo') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    tmp:WarrantyRefNo = p_web.RestoreValue('tmp:WarrantyRefNo')
    do ValidateValue::tmp:WarrantyRefNo
    If tmp:WarrantyRefNo:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- tmp:WarrantyRefNo
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:WarrantyRefNo'',''proofofpurchase_tmp:warrantyrefno_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:WarrantyRefNo',p_web.GetSessionValueFormat('tmp:WarrantyRefNo'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:WarrantyRefNo  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:WarrantyRefNo:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ProofOfPurchase_' & p_web._nocolon('tmp:WarrantyRefNo') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('ProofOfPurchase_nexttab_' & 0)
    tmp:DOP = p_web.GSV('tmp:DOP')
    do ValidateValue::tmp:DOP
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:DOP
      !do SendAlert
      do Comment::tmp:DOP ! allows comment style to be updated.
      !exit
    End
    tmp:POP = p_web.GSV('tmp:POP')
    do ValidateValue::tmp:POP
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:POP
      !do SendAlert
      do Comment::tmp:POP ! allows comment style to be updated.
      !exit
    End
    locPOPTypePassword = p_web.GSV('locPOPTypePassword')
    do ValidateValue::locPOPTypePassword
    If loc:Invalid
      loc:retrying = 1
      do Value::locPOPTypePassword
      !do SendAlert
      do Comment::locPOPTypePassword ! allows comment style to be updated.
      !exit
    End
    tmp:POPType = p_web.GSV('tmp:POPType')
    do ValidateValue::tmp:POPType
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:POPType
      !do SendAlert
      do Comment::tmp:POPType ! allows comment style to be updated.
      !exit
    End
    tmp:WarrantyRefNo = p_web.GSV('tmp:WarrantyRefNo')
    do ValidateValue::tmp:WarrantyRefNo
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:WarrantyRefNo
      !do SendAlert
      do Comment::tmp:WarrantyRefNo ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_ProofOfPurchase_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('ProofOfPurchase_tab_' & 0)
    do GenerateTab0
  of lower('ProofOfPurchase_tmp:DOP_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:DOP
      of event:timer
        do Value::tmp:DOP
        do Comment::tmp:DOP
      else
        do Value::tmp:DOP
      end
  of lower('ProofOfPurchase_tmp:POP_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:POP
      of event:timer
        do Value::tmp:POP
        do Comment::tmp:POP
      else
        do Value::tmp:POP
      end
  of lower('ProofOfPurchase_locPOPTypePassword_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locPOPTypePassword
      of event:timer
        do Value::locPOPTypePassword
        do Comment::locPOPTypePassword
      else
        do Value::locPOPTypePassword
      end
  of lower('ProofOfPurchase_tmp:POPType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:POPType
      of event:timer
        do Value::tmp:POPType
        do Comment::tmp:POPType
      else
        do Value::tmp:POPType
      end
  of lower('ProofOfPurchase_tmp:WarrantyRefNo_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:WarrantyRefNo
      of event:timer
        do Value::tmp:WarrantyRefNo
        do Comment::tmp:WarrantyRefNo
      else
        do Value::tmp:WarrantyRefNo
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('ProofOfPurchase_form:ready_',1)

  p_web.SetSessionValue('ProofOfPurchase_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_ProofOfPurchase',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('ProofOfPurchase_form:ready_',1)
  p_web.SetSessionValue('ProofOfPurchase_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_ProofOfPurchase',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('ProofOfPurchase_form:ready_',1)
  p_web.SetSessionValue('ProofOfPurchase_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('ProofOfPurchase:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('ProofOfPurchase_form:ready_',1)
  p_web.SetSessionValue('ProofOfPurchase_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('ProofOfPurchase:Primed',0)
  p_web.setsessionvalue('showtab_ProofOfPurchase',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
      If not (1=0)
          If p_web.IfExistsValue('tmp:DOP')
            tmp:DOP = p_web.GetValue('tmp:DOP')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('tmp:POP')
            tmp:POP = p_web.GetValue('tmp:POP')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('locPOPTypePassword')
            locPOPTypePassword = p_web.GetValue('locPOPTypePassword')
          End
      End
      If not (1=0)
        If not (p_web.GSV('ReadOnly:POPType'))
          If p_web.IfExistsValue('tmp:POPType')
            tmp:POPType = p_web.GetValue('tmp:POPType')
          End
        End
      End
      If not (1=0)
          If p_web.IfExistsValue('tmp:WarrantyRefNo')
            tmp:WarrantyRefNo = p_web.GetValue('tmp:WarrantyRefNo')
          End
      End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('ProofOfPurchase_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  ! Validate
  if (p_web.GSV('tmp:POP') = '')
      loc:alert = 'You have not selected an option'
      loc:invalid = 'tmp:POP'
      exit
  end !if (p_web.GSV('tmp:POP') = '')
  
  case (p_web.GSV('tmp:POP'))
  of 'F' orof 'S' orof 'O'
      if (p_web.GSV('tmp:DOP') = '')
          loc:alert = 'Date Of Purchase Required'
          loc:invalid = 'tmp:DOP'
          exit
      end ! if (p_web.GSV('tmp:DOP') = '')
  end ! case (p_web.GSV('tmp:POP'))
  
  
  Access:MANUFACT.Clearkey(man:Manufacturer_Key)
  man:Manufacturer    = p_web.GSV('job:Manufacturer')
  if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
      ! Found
  else ! if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
      ! Error
  end ! if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
  
  if (p_web.GSV('tmp:POP') <> 'C' and man:validateDateCode)
      if (p_web.GSV('tmp:DOP') < (job:date_Booked - man:warranty_period))
          if (p_web.GSV('tmp:DOP') < (job:date_booked - 730))
              ! Out Of 2nd Year
          else ! if (p_web.GSV('tmp:DOP') < (job:date_booked - 730))
              if (p_web.GSV('tmp:POP') <> 'O')
                  p_web.SSV('tmp:POP','S')
              end ! if (p_web.GSV('tmp:POP') <> 'O')
          end ! if (p_web.GSV('tmp:DOP') < (job:date_booked - 730))
      end ! if (p_web.GSV('tmp:DOP') < (job:date_Booked - man:warranty_period))
  end ! if (p_web.GSV('tmp:POP') <> 'C' and man:validateDateCode)
  
  p_web.SSV('job:POP',p_web.GSV('tmp:POP'))
  p_web.SSV('job:DOP',p_web.GSV('tmp:DOP'))
  
  if (p_web.GSV('job:DOP') > 0)
      if (sub(p_web.GSV('job:Current_Status'),1,3) = '130')
          if (p_web.GSV('job:Engineer') = '')
              p_web.SSV('GetStatus:StatusNumber',305)
              p_web.SSV('GetStatus:Type','JOB')
          else ! if (p_web.GSV('job:Engineer') = ''
              p_web.SSV('GetStatus:StatusNumber',sub(p_web.GSV('job:previousStatus'),1,3))
              p_web.SSV('GetStatus:Type','JOB')
          end ! if (p_web.GSV('job:Engineer') = ''
          GetStatus(p_web.GSV('GetStatus:StatusNumber'),0,p_web.GSV('GetStatus:Type'),p_web)
      end ! if (sub(p_web.GSV('job:Current_Status'),1,3) = '130')
  
      case p_web.GSV('job:POP')
      of 'F'
          p_web.SSV('job:Warranty_job','YES')
          p_web.SSV('job:Warranty_Charge_Type','WARRANTY (MFTR)')
      of 'S'
          p_web.SSV('job:Warranty_job','YES')
          p_web.SSV('job:Warranty_Charge_Type','WARRANTY (2ND YR)')
      of 'O'
          p_web.SSV('job:Warranty_job','YES')
          p_web.SSV('job:Warranty_Charge_Type','WARRANTY (OBF)')
      of 'C'
          p_web.SSV('job:Warranty_job','NO')
          p_web.SSV('job:Chargeable_job','YES')
          p_web.SSV('job:Warranty_Charge_Type','')
          p_web.SSV('job:Charge_Type','NON-WARRANTY')
      end ! case p_web.GSV('job:POP')
  end ! if (p_web.GSV('job:DOP') > 0)
  
  p_web.SSV('jobe:POPType',p_web.GSV('tmp:POPType')) ! #11912 Never saved this field. (Bryan: 14/02/2011)
  
  
  
  p_web.DeleteSessionValue('ProofOfPurchase_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
    do ValidateValue::tmp:DOP
    If loc:Invalid then exit.
    do ValidateValue::tmp:POP
    If loc:Invalid then exit.
    do ValidateValue::locPOPTypePassword
    If loc:Invalid then exit.
    do ValidateValue::tmp:POPType
    If loc:Invalid then exit.
    do ValidateValue::tmp:WarrantyRefNo
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('ProofOfPurchase:Primed',0)
  p_web.StoreValue('tmp:DOP')
  p_web.StoreValue('tmp:POP')
  p_web.StoreValue('locPOPTypePassword')
  p_web.StoreValue('tmp:POPType')
  p_web.StoreValue('tmp:WarrantyRefNo')

ViewCosts            PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
tmp:ARCCost1         REAL                                  !
tmp:ARCCost2         REAL                                  !
tmp:ARCCost3         REAL                                  !
tmp:ARCCost4         REAL                                  !
tmp:ARCCost5         REAL                                  !
tmp:ARCCost6         REAL                                  !
tmp:ARCCost7         REAL                                  !
tmp:ARCCost8         REAL                                  !
tmp:RRCCost0         REAL                                  !
tmp:RRCCost1         REAL                                  !
tmp:RRCCost2         REAL                                  !
tmp:RRCCost3         REAL                                  !
tmp:RRCCost4         REAL                                  !
tmp:RRCCost5         REAL                                  !
tmp:RRCCost6         REAL                                  !
tmp:RRCCost7         REAL                                  !
tmp:RRCCost8         REAL                                  !
tmp:ARCIgnoreDefaultCharges BYTE                           !
tmp:RRCIgnoreDefaultCharges BYTE                           !
tmp:ARCViewCostType  STRING(30)                            !
tmp:AdjustmentCost1  REAL                                  !
tmp:AdjustmentCost2  REAL                                  !
tmp:AdjustmentCost3  REAL                                  !
tmp:AdjustmentCost4  REAL                                  !
tmp:AdjustmentCost5  REAL                                  !
tmp:AdjustmentCost6  REAL                                  !
tmp:RRCViewCostType  STRING(30)                            !
tmp:ARCIgnoreReason  STRING(255)                           !
tmp:RRCIgnoreReason  STRING(255)                           !
tmp:originalInvoice  STRING(30)                            !
tmp:ARCInvoiceNumber STRING(60)                            !
FilesOpened     Long
JOBSE2::State  USHORT
SUBCHRGE::State  USHORT
TRACHRGE::State  USHORT
STDCHRGE::State  USHORT
USERS::State  USHORT
JOBPAYMT::State  USHORT
JOBSE::State  USHORT
INVOICE::State  USHORT
JOBSINV::State  USHORT
TRADEACC::State  USHORT
TRDPARTY::State  USHORT
tmp:ARCViewCostType:IsInvalid  Long
__line1:IsInvalid  Long
tmp:ARCIgnoreDefaultCharges:IsInvalid  Long
tmp:ARCIgnoreReason:IsInvalid  Long
buttonAcceptARCReason:IsInvalid  Long
buttonCancelARCReason:IsInvalid  Long
tmp:ARCCost1:IsInvalid  Long
tmp:AdjustmentCost1:IsInvalid  Long
tmp:ARCCost2:IsInvalid  Long
tmp:AdjustmentCost2:IsInvalid  Long
tmp:ARCCost3:IsInvalid  Long
tmp:AdjustmentCost3:IsInvalid  Long
__line2:IsInvalid  Long
tmp:ARCCost4:IsInvalid  Long
tmp:AdjustmentCost4:IsInvalid  Long
tmp:ARCCost5:IsInvalid  Long
tmp:AdjustmentCost5:IsInvalid  Long
tmp:ARCCost6:IsInvalid  Long
tmp:AdjustmentCost6:IsInvalid  Long
tmp:ARCCost7:IsInvalid  Long
tmp:ARCCost8:IsInvalid  Long
jobe:ARC3rdPartyInvoiceNumber:IsInvalid  Long
tmp:ARCInvoiceNumber:IsInvalid  Long
tmp:RRCViewCostType:IsInvalid  Long
__line3:IsInvalid  Long
tmp:RRCIgnoreDefaultCharges:IsInvalid  Long
tmp:RRCIgnoreReason:IsInvalid  Long
buttonAcceptRRCReason:IsInvalid  Long
buttonCancelRRCReason:IsInvalid  Long
tmp:RRCCost0:IsInvalid  Long
tmp:RRCCost1:IsInvalid  Long
tmp:originalInvoice:IsInvalid  Long
tmp:RRCCost2:IsInvalid  Long
tmp:RRCCost3:IsInvalid  Long
BrowseJobCredits:IsInvalid  Long
__line4:IsInvalid  Long
tmp:RRCCost4:IsInvalid  Long
tmp:RRCCost5:IsInvalid  Long
tmp:RRCCost6:IsInvalid  Long
tmp:RRCCost7:IsInvalid  Long
tmp:RRCCost8:IsInvalid  Long
jobe:ExcReplcamentCharge:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
local       class
AddToTempQueue        Procedure(String fField,String fSaveField,String fReason,Byte fSaveCost,String fCost)
            end
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('ViewCosts')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'ViewCosts_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('ViewCosts','')
    p_web.DivHeader('ViewCosts',p_web.combine(p_web.site.style.formdiv,'fdiv'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('ViewCosts') = 0
        p_web.AddPreCall('ViewCosts')
        p_web.DivHeader('popup_ViewCosts','nt-hidden')
        p_web.DivHeader('ViewCosts',p_web.combine(p_web.site.style.formdiv,'fdiv'))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_ViewCosts_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_ViewCosts_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferViewCosts',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_ViewCosts',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferViewCosts',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_ViewCosts',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferViewCosts',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_ViewCosts',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_ViewCosts',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferViewCosts',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_ViewCosts',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferViewCosts',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_ViewCosts',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_ViewCosts',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('ViewCosts')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
DeleteSessionValues ROUTINE
    p_web.DeleteSessionValue('ViewCostsReturnURL')
addToAudit    routine
    data
locNotes        String(255)
    code
    clear(tmpaud:Record)
    tmpaud:sessionID = p_web.SessionID
    set(tmpaud:keySessionID,tmpaud:keySessionID)
    loop
        next(tempAuditQueue)
        if (error())
            break
        end ! if (error())
        if (tmpaud:sessionID <> p_web.sessionID)
            break
        end ! if (tmpaud:sessionID <> p_web.sessionID)
        locNotes = 'REASON: ' & clip(tmpaud:Reason)
        if (tmpaud:SaveCost)
            locNotes = clip(locNotes) & '<13,10>PREVIOUS CHARGE: ' & format(tmpaud:Cost,@n14.2)
        end !if (tmpaud:SaveCost)
        p_web.SSV('AddToAudit:Type','JOB')
        p_web.SSV('AddToAudit:Action',tmpaud:Field)
        p_web.SSV('AddToAudit:Notes',locNotes)
        addToAudit(p_web)
    end

    !Empty Audit Queue
    clear(tmpaud:Record)
    tmpaud:sessionID = p_web.SessionID
    set(tmpaud:keySessionID,tmpaud:keySessionID)
    loop
        next(tempAuditQueue)
        if (error())
            break
        end ! if (error())
        if (tmpaud:sessionID <> p_web.sessionID)
            break
        end ! if (tmpaud:sessionID <> p_web.sessionID)
        delete(tempAuditQueue)
    end



DefaultLabourCost   ROUTINE
    DefaultLabourCost(p_web)


displayARCCostFields      routine
    case (p_web.GSV('tmp:ARCViewCostType'))
    of 'Chargeable'
        p_web.SSV('Prompt:Cost1','')
        IF (p_web.GSV('LoanAttachedToJob') = 1)
            p_web.SSV('Prompt:Cost1','Lost Loan Charge')
        END
        p_web.SSV('Prompt:Cost2','Labour Cost')
        p_web.SSV('Prompt:Cost3','Parts Cost')
        p_web.SSV('Prompt:Cost4','Sub Total')
        p_web.SSV('Prompt:Cost5','V.A.T.')
        p_web.SSV('Prompt:Cost6','Total Cost')
        p_web.SSV('Prompt:Cost7','Paid')
        p_web.SSV('Prompt:Cost8','Outstanding')

        p_web.SSV('tmp:ARCCost1',p_web.GSV('job:Courier_Cost'))
        p_web.SSV('tmp:ARCCost2',p_web.GSV('job:Labour_Cost'))
        p_web.SSV('tmp:ARCCost3',p_web.GSV('job:Parts_Cost'))
        p_web.SSV('tmp:ARCCost4',p_web.GSV('job:Sub_Total'))
        p_web.SSV('tmp:ARCCost5',p_web.GSV('tmp:ARCCVAT'))
        p_web.SSV('tmp:ARCCost6',p_web.GSV('tmp:ARCCTotal'))
        p_web.SSV('tmp:ARCCost7',p_web.GSV('tmp:ARCCPaid'))
        p_web.SSV('tmp:ARCCost8',p_web.GSV('tmp:ARCOutstanding'))

        p_web.SSV('tmp:ARCIgnoreDefaultCharges',p_web.GSV('job:Ignore_Chargeable_Charges'))


    of 'Warranty'
        p_web.SSV('Prompt:Cost1','Exchange Fee')
        p_web.SSV('Prompt:Cost2','Labour Cost')
        p_web.SSV('Prompt:Cost3','Parts Cost')
        p_web.SSV('Prompt:Cost4','Sub Total')
        p_web.SSV('Prompt:Cost5','V.A.T.')
        p_web.SSV('Prompt:Cost6','Total Cost')
        p_web.SSV('Prompt:Cost7','')
        p_web.SSV('Prompt:Cost8','')

        p_web.SSV('tmp:ARCCost1',p_web.GSV('job:Courier_Cost_Warranty'))
        p_web.SSV('tmp:ARCCost2',p_web.GSV('job:Labour_Cost_Warranty'))
        p_web.SSV('tmp:ARCCost3',p_web.GSV('job:Parts_Cost_Warranty'))
        p_web.SSV('tmp:ARCCost4',p_web.GSV('job:Sub_Total_Warranty'))
        p_web.SSV('tmp:ARCCost5',p_web.GSV('tmp:ARCWVAT'))
        p_web.SSV('tmp:ARCCost6',p_web.GSV('tmp:ARCWTotal'))
        p_web.SSV('tmp:ARCCost7',0)
        p_web.SSV('tmp:ARCCost8',0)

        p_web.SSV('tmp:ARCIgnoreDefaultCharges',p_web.GSV('job:Ignore_Warranty_Charges'))

    of 'Claim'
        p_web.SSV('Prompt:Cost1','Exchange Fee')
        p_web.SSV('Prompt:Cost2','Labour Cost')
        p_web.SSV('Prompt:Cost3','Parts Cost')
        p_web.SSV('Prompt:Cost4','Sub Total')
        p_web.SSV('Prompt:Cost5','V.A.T.')
        p_web.SSV('Prompt:Cost6','Total Cost')
        p_web.SSV('Prompt:Cost7','')
        p_web.SSV('Prompt:Cost8','')

        p_web.SSV('tmp:ARCCost1',p_web.GSV('job:Courier_Cost_Warranty'))
        p_web.SSV('tmp:ARCCost2',p_web.GSV('jobe:ClaimValue'))
        p_web.SSV('tmp:ARCCost3',p_web.GSV('jobe:ClaimPartsCost'))
        p_web.SSV('tmp:ARCCost4',p_web.GSV('tmp:ARCClaimSubTotal'))
        p_web.SSV('tmp:ARCCost5',p_web.GSV('tmp:ARCClaimVAT'))
        p_web.SSV('tmp:ARCCost6',p_web.GSV('tmp:ARCClaimTotal'))
        p_web.SSV('tmp:ARCCost7',0)
        p_web.SSV('tmp:ARCCost8',0)

        p_web.SSV('tmp:ARCIgnoreDefaultCharges',p_web.GSV('jobe:IgnoreClaimCosts'))

    of '3rd Party'
        p_web.SSV('Prompt:Cost1','Cost')
        p_web.SSV('Prompt:Cost2','')
        p_web.SSV('Prompt:Cost3','')
        p_web.SSV('Prompt:Cost4','')
        p_web.SSV('Prompt:Cost5','V.A.T.')
        p_web.SSV('Prompt:Cost6','Total')
        p_web.SSV('Prompt:Cost7','')
        p_web.SSV('Prompt:Cost8','Markup')

        p_web.SSV('tmp:ARCCost1',p_web.GSV('jobe:ARC3rdPartyCost'))
        p_web.SSV('tmp:ARCCost2',0)
        p_web.SSV('tmp:ARCCost3',0)
        p_web.SSV('tmp:ARCCost4',0)
        p_web.SSV('tmp:ARCCost5',p_web.GSV('tmp:ARC3rdPartyVAT'))
        p_web.SSV('tmp:ARCCost6',p_web.GSV('tmp:ARC3rdPartyTotal'))
        p_web.SSV('tmp:ARCCost7',0)
        p_web.SSV('tmp:ARCCost8',p_web.GSV('tmp:ARC3rdPartyMarkup'))

        p_web.SSV('tmp:ARCIgnoreDefaultCharges',p_web.GSV('jobe:Ignore3rdPartyCosts'))

    of 'Estimate'
        p_web.SSV('Prompt:Cost1','')
        IF (p_web.GSV('LoanAttachedToJob') = 1)
            p_web.SSV('Prompt:Cost1','Lost Loan Charge')
        END
        p_web.SSV('Prompt:Cost2','Labour Cost')
        p_web.SSV('Prompt:Cost3','Parts Cost')
        p_web.SSV('Prompt:Cost4','Sub Total')
        p_web.SSV('Prompt:Cost5','V.A.T.')
        p_web.SSV('Prompt:Cost6','Total Cost')
        p_web.SSV('Prompt:Cost7','')
        p_web.SSV('Prompt:Cost8','')

        p_web.SSV('tmp:ARCCost1',p_web.GSV('job:Courier_Cost_Estimate'))
        p_web.SSV('tmp:ARCCost2',p_web.GSV('job:Labour_Cost_Estimate'))
        p_web.SSV('tmp:ARCCost3',p_web.GSV('job:Parts_Cost_Estimate'))
        p_web.SSV('tmp:ARCCost4',p_web.GSV('job:Sub_Total_Estimate'))
        p_web.SSV('tmp:ARCCost5',p_web.GSV('tmp:ARCEVAT'))
        p_web.SSV('tmp:ARCCost6',p_web.GSV('tmp:ARCETotal'))
        p_web.SSV('tmp:ARCCost7',0)
        p_web.SSV('tmp:ARCCost8',0)

        p_web.SSV('tmp:ARCIgnoreDefaultCharges',p_web.GSV('job:Ignore_Estimate_Charges'))

    of 'Chargeable - Invoiced'
        p_web.SSV('Prompt:Cost1','')
        IF (p_web.GSV('LoanAttachedToJob') = 1)
            p_web.SSV('Prompt:Cost1','Lost Loan Charge')
        END
        p_web.SSV('Prompt:Cost2','Labour Cost')
        p_web.SSV('Prompt:Cost3','Parts Cost')
        p_web.SSV('Prompt:Cost4','Sub Total')
        p_web.SSV('Prompt:Cost5','V.A.T.')
        p_web.SSV('Prompt:Cost6','Total Cost')
        p_web.SSV('Prompt:Cost7','Paid')
        p_web.SSV('Prompt:Cost8','Outstanding')

        p_web.SSV('tmp:ARCCost1',p_web.GSV('job:Invoice_Courier_Cost'))
        p_web.SSV('tmp:ARCCost2',p_web.GSV('job:Invoice_Labour_Cost'))
        p_web.SSV('tmp:ARCCost3',p_web.GSV('job:Invoice_Parts_Cost'))
        p_web.SSV('tmp:ARCCost4',p_web.GSV('job:Invoice_Sub_Total'))
        p_web.SSV('tmp:ARCCost5',p_web.GSV('tmp:ARCCIVAT'))
        p_web.SSV('tmp:ARCCost6',p_web.GSV('tmp:ARCCITotal'))
        p_web.SSV('tmp:ARCCost7',p_web.GSV('tmp:ARCCIPaid'))
        p_web.SSV('tmp:ARCCost8',p_web.GSV('tmp:ARCIOutstanding'))

        p_web.SSV('tmp:ARCIgnoreDefaultCharges',p_web.GSV('job:Ignore_Chargeable_Charges'))

    of 'Warranty - Invoiced'
        p_web.SSV('Prompt:Cost1','Exchange Fee')
        p_web.SSV('Prompt:Cost2','Labour Cost')
        p_web.SSV('Prompt:Cost3','Parts Cost')
        p_web.SSV('Prompt:Cost4','Sub Total')
        p_web.SSV('Prompt:Cost5','V.A.T.')
        p_web.SSV('Prompt:Cost6','Total Cost')
        p_web.SSV('Prompt:Cost7','')
        p_web.SSV('Prompt:Cost8','')

        p_web.SSV('tmp:ARCCost1',p_web.GSV('job:WInvoice_Courier_Cost'))
        p_web.SSV('tmp:ARCCost2',p_web.GSV('job:WInvoice_Labour_Cost'))
        p_web.SSV('tmp:ARCCost3',p_web.GSV('job:WInvoice_Parts_Cost'))
        p_web.SSV('tmp:ARCCost4',p_web.GSV('job:WInvoice_Sub_Total'))
        p_web.SSV('tmp:ARCCost5',p_web.GSV('tmp:ARCWIVAT'))
        p_web.SSV('tmp:ARCCost6',p_web.GSV('tmp:ARCWITotal'))
        p_web.SSV('tmp:ARCCost7',0)
        p_web.SSV('tmp:ARCCost8',0)

        p_web.SSV('tmp:ARCIgnoreDefaultCharges',p_web.GSV('job:Ignore_Warranty_Charges'))

    of 'Claim - Invoiced'
        p_web.SSV('Prompt:Cost1','Exchange Fee')
        p_web.SSV('Prompt:Cost2','Labour Cost')
        p_web.SSV('Prompt:Cost3','Parts Cost')
        p_web.SSV('Prompt:Cost4','Sub Total')
        p_web.SSV('Prompt:Cost5','V.A.T.')
        p_web.SSV('Prompt:Cost6','Total Cost')
        p_web.SSV('Prompt:Cost7','')
        p_web.SSV('Prompt:Cost8','')

        p_web.SSV('tmp:ARCCost1',p_web.GSV('job:WInvoice_Courier_Cost'))
        p_web.SSV('tmp:ARCCost2',p_web.GSV('jobe:InvoiceClaimValue'))
        p_web.SSV('tmp:ARCCost3',p_web.GSV('jobe:InvClaimPartsCost'))
        p_web.SSV('tmp:ARCCost4',p_web.GSV('tmp:ARCClaimISubTotal'))
        p_web.SSV('tmp:ARCCost5',p_web.GSV('tmp:ARCClaimIVAT'))
        p_web.SSV('tmp:ARCCost6',p_web.GSV('tmp:ARCClaimITotal'))
        p_web.SSV('tmp:ARCCost7',0)
        p_web.SSV('tmp:ARCCost8',0)

        p_web.SSV('tmp:AdjustmentCost1',p_web.GSV('jobe:ExchangeAdjustment'))
        p_web.SSV('tmp:AdjustmentCost2',p_web.GSV('jobe:LabourAdjustment'))
        p_web.SSV('tmp:AdjustmentCost3',p_web.GSV('jobe:PartsAdjustment'))
        p_web.SSV('tmp:AdjustmentCost4',p_web.GSV('tmp:AdjustmentSubTotal'))
        p_web.SSV('tmp:AdjustmentCost5',p_web.GSV('tmp:AdjustmentVAT'))
        p_web.SSV('tmp:AdjustmentCost6',p_web.GSV('tmp:AdjustmentTotal'))

        p_web.SSV('tmp:ARCIgnoreDefaultCharges',p_web.GSV('jobe:IgnoreClaimCosts'))

    of 'Manufacturer Payment'
        p_web.SSV('Prompt:Cost1','Labour Paid')
        p_web.SSV('Prompt:Cost2','Parts Paid')
        p_web.SSV('Prompt:Cost3','Other Costs')
        p_web.SSV('Prompt:Cost4','Sub Total')
        p_web.SSV('Prompt:Cost5','V.A.T.')
        p_web.SSV('Prompt:Cost6','Total Paid')
        p_web.SSV('Prompt:Cost7','')
        p_web.SSV('Prompt:Cost8','')

        p_web.SSV('tmp:ARCCost1',p_web.GSV('jobe2:WLabourPaid'))
        p_web.SSV('tmp:ARCCost2',p_web.GSV('jobe2:WPartsPaid'))
        p_web.SSV('tmp:ARCCost3',p_web.GSV('jobe2:WOtherCosts'))
        p_web.SSV('tmp:ARCCost4',p_web.GSV('jobe2:WSubTotal'))
        p_web.SSV('tmp:ARCCost5',p_web.GSV('jobe2:WVAT'))
        p_web.SSV('tmp:ARCCost6',p_web.GSV('jobe2:WTotal'))
        p_web.SSV('tmp:ARCCost7',0)
        p_web.SSV('tmp:ARCCost8',0)
    end ! case (p_web.GSV('tmp:ARCViewCostType'))


displayRRCCostFields      routine
    case (p_web.GSV('tmp:RRCViewCostType'))
    of 'Chargeable'

        p_web.SSV('Prompt:RCost1','')
        IF (p_web.GSV('LoanAttachedToJob') = 1)
            p_web.SSV('Prompt:RCost1','Lost Loan Charge')
        END

        p_web.SSV('Prompt:RCost2','Labour Cost')
        p_web.SSV('Prompt:RCost3','Parts Cost')
        p_web.SSV('Prompt:RCost4','Sub Total')
        p_web.SSV('Prompt:RCost5','V.A.T.')
        p_web.SSV('Prompt:RCost6','Total Cost')
        p_web.SSV('Prompt:RCost7','Paid')
        p_web.SSV('Prompt:RCost8','Outstanding')

        p_web.SSV('tmp:RRCCost0',p_web.GSV('jobe2:JobDiscountAmnt'))
        p_web.SSV('tmp:RRCCost1',p_web.GSV('job:Courier_Cost'))
        p_web.SSV('tmp:RRCCost2',p_web.GSV('jobe:RRCCLabourCost'))
        p_web.SSV('tmp:RRCCost3',p_web.GSV('jobe:RRCCPartsCost'))
        p_web.SSV('tmp:RRCCost4',p_web.GSV('jobe:RRCCSubTotal'))
        p_web.SSV('tmp:RRCCost5',p_web.GSV('tmp:RRCCVAT'))
        p_web.SSV('tmp:RRCCost6',p_web.GSV('tmp:RRCCTotal'))
        p_web.SSV('tmp:RRCCost7',p_web.GSV('tmp:RRCCPaid'))
        p_web.SSV('tmp:RRCCost8',p_web.GSV('tmp:RRCOutstanding'))


        p_web.SSV('tmp:RRCIgnoreDefaultCharges',p_web.GSV('jobe:IgnoreRRCChaCosts'))

        if (p_web.GSV('jobe2:JobDiscountAmnt') > 0 or p_web.GSV('tmp:RRCIgnoreDefaultCharges') <> 0)
            p_web.SSV('Prompt:RCost0','Discount')
        else
            p_web.SSV('Prompt:RCost0','')
            p_web.SSV('Comment:RCost0','')
        end

        if (p_web.GSV('tmp:RRCIgnoreDefaultCharges') = 1)
            p_web.SSV('ReadOnly:RRCCost2',0)
        else
            p_web.SSV('ReadOnly:RRCCost2',1)
        end

    of 'Warranty'
        p_web.SSV('Prompt:RCost0','')
        p_web.SSV('Comment:RCost0','')
        p_web.SSV('Prompt:RCost1','')
        p_web.SSV('Prompt:RCost2','Labour Cost')
        p_web.SSV('Prompt:RCost3','Parts Cost')
        p_web.SSV('Prompt:RCost4','Sub Total')
        p_web.SSV('Prompt:RCost5','V.A.T.')
        p_web.SSV('Prompt:RCost6','Total Cost')
        p_web.SSV('Prompt:RCost7','')
        p_web.SSV('Prompt:RCost8','')

        p_web.SSV('tmp:RRCCost1',p_web.GSV('job:Courier_Cost_Warranty'))
        p_web.SSV('tmp:RRCCost2',p_web.GSV('jobe:RRCWLabourCost'))
        p_web.SSV('tmp:RRCCost3',p_web.GSV('jobe:RRCWPartsCost'))
        p_web.SSV('tmp:RRCCost4',p_web.GSV('jobe:RRCWSubTotal'))
        p_web.SSV('tmp:RRCCost5',p_web.GSV('tmp:RRCWVAT'))
        p_web.SSV('tmp:RRCCost6',p_web.GSV('tmp:RRCWTotal'))
        p_web.SSV('tmp:RRCCost7',0)
        p_web.SSV('tmp:RRCCost8',0)

        p_web.SSV('tmp:RRCIgnoreDefaultCharges',p_web.GSV('jobe:IgnoreRRCWarCosts'))
        if (p_web.GSV('tmp:RRCIgnoreDefaultCharges') = 1)
            p_web.SSV('ReadOnly:RRCCost2',0)
        else
            p_web.SSV('ReadOnly:RRCCost2',1)
        end


    of 'Handling'
        p_web.SSV('Prompt:RCost0','')
        p_web.SSV('Comment:RCost0','')
        p_web.SSV('Prompt:RCost1','Handling Fee')
        p_web.SSV('Prompt:RCost2','')
        p_web.SSV('Prompt:RCost3','')
        p_web.SSV('Prompt:RCost4','')
        p_web.SSV('Prompt:RCost5','')
        p_web.SSV('Prompt:RCost6','')
        p_web.SSV('Prompt:RCost7','')
        p_web.SSV('Prompt:RCost8','')

        p_web.SSV('tmp:RRCCost1',p_web.GSV('tmp:HandlingFee'))
        p_web.SSV('tmp:RRCCost2',0)
        p_web.SSV('tmp:RRCCost3',0)
        p_web.SSV('tmp:RRCCost4',0)
        p_web.SSV('tmp:RRCCost5',0)
        p_web.SSV('tmp:RRCCost6',0)
        p_web.SSV('tmp:RRCCost7',0)
        p_web.SSV('tmp:RRCCost8',0)



    of 'Exchange'
        p_web.SSV('Prompt:RCost0','')
        p_web.SSV('Comment:RCost0','')
        p_web.SSV('Prompt:RCost1','Exchange Fee')
        p_web.SSV('Prompt:RCost2','')
        p_web.SSV('Prompt:RCost3','')
        p_web.SSV('Prompt:RCost4','')
        p_web.SSV('Prompt:RCost5','')
        p_web.SSV('Prompt:RCost6','')
        p_web.SSV('Prompt:RCost7','')
        p_web.SSV('Prompt:RCost8','')

        p_web.SSV('tmp:RRCCost1',p_web.GSV('tmp:ExchangeFee'))
        p_web.SSV('tmp:RRCCost2',0)
        p_web.SSV('tmp:RRCCost3',0)
        p_web.SSV('tmp:RRCCost4',0)
        p_web.SSV('tmp:RRCCost5',0)
        p_web.SSV('tmp:RRCCost6',0)
        p_web.SSV('tmp:RRCCost7',0)
        p_web.SSV('tmp:RRCCost8',0)

    of 'Estimate'
        p_web.SSV('Prompt:RCost0','')
        p_web.SSV('Comment:RCost0','')
        p_web.SSV('Prompt:RCost1','Lost Loan Charge')
        p_web.SSV('Prompt:RCost2','Labour Cost')
        p_web.SSV('Prompt:RCost3','Parts Cost')
        p_web.SSV('Prompt:RCost4','Sub Total')
        p_web.SSV('Prompt:RCost5','V.A.T.')
        p_web.SSV('Prompt:RCost6','Total Cost')
        p_web.SSV('Prompt:RCost7','')
        p_web.SSV('Prompt:RCost8','')

        p_web.SSV('tmp:RRCCost1',p_web.GSV('job:Courier_Cost_Estimate'))
        p_web.SSV('tmp:RRCCost2',p_web.GSV('jobe:RRCELabourCost'))
        p_web.SSV('tmp:RRCCost3',p_web.GSV('jobe:RRCEPartsCost'))
        p_web.SSV('tmp:RRCCost4',p_web.GSV('jobe:RRCESubTotal'))
        p_web.SSV('tmp:RRCCost5',p_web.GSV('tmp:RRCEVAT'))
        p_web.SSV('tmp:RRCCost6',p_web.GSV('tmp:RRCETotal'))
        p_web.SSV('tmp:RRCCost7',0)
        p_web.SSV('tmp:RRCCost8',0)

        p_web.SSV('tmp:RRCIgnoreDefaultCharges',p_web.GSV('jobe:IgnoreRRCEstCosts'))
        if (p_web.GSV('tmp:RRCIgnoreDefaultCharges') = 1)
            p_web.SSV('ReadOnly:RRCCost2',0)
        else
            p_web.SSV('ReadOnly:RRCCost2',1)
        end

    of 'Chargeable - Invoiced'

        if (p_web.GSV('jobe2:InvDiscountAmnt') > 0  or p_web.GSV('tmp:RRCIgnoreDefaultCharges') <> 0)
            p_web.SSV('Prompt:RCost0','Discount')
        else
            p_web.SSV('Prompt:RCost0','')
            p_web.SSV('Comment:RCost0','')
        end

        p_web.SSV('Prompt:RCost1','')
        IF (p_web.GSV('LoanAttachedToJob') = 1)
            p_web.SSV('Prompt:RCost1','Lost Loan Charge')
        END
        p_web.SSV('Prompt:RCost2','Labour Cost')
        p_web.SSV('Prompt:RCost3','Parts Cost')
        p_web.SSV('Prompt:RCost4','Sub Total')
        p_web.SSV('Prompt:RCost5','V.A.T.')
        p_web.SSV('Prompt:RCost6','Total Cost')
        p_web.SSV('Prompt:RCost7','Paid')
        p_web.SSV('Prompt:RCost8','Outstanding')

        p_web.SSV('tmp:RRCCOst0',p_web.GSV('jobe2:InvDiscountAmnt'))
        p_web.SSV('tmp:RRCCost1',p_web.GSV('job:Invoice_Courier_Cost'))
        p_web.SSV('tmp:RRCCost2',p_web.GSV('jobe:InvRRCCLabourCost'))
        p_web.SSV('tmp:RRCCost3',p_web.GSV('jobe:InvRRCCPartsCost'))
        p_web.SSV('tmp:RRCCost4',p_web.GSV('jobe:InvRRCCSubTotal'))
        p_web.SSV('tmp:RRCCost5',p_web.GSV('tmp:RRCCIVAT'))
        p_web.SSV('tmp:RRCCost6',p_web.GSV('tmp:RRCCITotal'))
        p_web.SSV('tmp:RRCCost7',p_web.GSV('tmp:RRCCIPaid'))
        p_web.SSV('tmp:RRCCost8',p_web.GSV('tmp:RRCIOutstanding'))

        p_web.SSV('tmp:RRCIgnoreDefaultCharges',p_web.GSV('jobe:IgnoreRRCChaCosts'))

    of 'Warranty - Invoiced'
        p_web.SSV('Prompt:RCost0','')
        p_web.SSV('Comment:RCost0','')
        p_web.SSV('Prompt:RCost1','Lost Loan Charge')
        p_web.SSV('Prompt:RCost2','Labour Cost')
        p_web.SSV('Prompt:RCost3','Parts Cost')
        p_web.SSV('Prompt:RCost4','Sub Total')
        p_web.SSV('Prompt:RCost5','V.A.T.')
        p_web.SSV('Prompt:RCost6','Total Cost')
        p_web.SSV('Prompt:RCost7','')
        p_web.SSV('Prompt:RCost8','')

        p_web.SSV('tmp:RRCCost1',p_web.GSV('job:WInvoice_Courier_Cost'))
        p_web.SSV('tmp:RRCCost2',p_web.GSV('jobe:InvRRCWLabourCost'))
        p_web.SSV('tmp:RRCCost3',p_web.GSV('jobe:InvRRCWPartsCost'))
        p_web.SSV('tmp:RRCCost4',p_web.GSV('jobe:InvRRCWSubTotal'))
        p_web.SSV('tmp:RRCCost5',p_web.GSV('tmp:RRCWIVAT'))
        p_web.SSV('tmp:RRCCost6',p_web.GSV('tmp:RRCWITotal'))
        p_web.SSV('tmp:RRCCost7',0)
        p_web.SSV('tmp:RRCCost8',0)


        p_web.SSV('tmp:RRCIgnoreDefaultCharges',p_web.GSV('jobe:IgnoreRRCWarCosts'))

    of 'Credits'
        p_web.SSV('Prompt:RCost0','')
        p_web.SSV('Comment:RCost0','')
        p_web.SSV('tmp:CreditTotal',p_web.GSV('tmp:RRCCITotal'))
        Access:JOBSINV.Clearkey(jov:typeRecordKey)
        jov:refNumber    = p_web.GSV('job:Ref_Number')
        jov:type    = 'C'
        set(jov:typeRecordKey,jov:typeRecordKey)
        loop
            if (Access:JOBSINV.Next())
                Break
            end ! if (Access:JOBSINV.Next())
            if (jov:refNumber    <> p_web.GSV('job:Ref_Number'))
                Break
            end ! if (jov:refNumber    <> p_web.GSV('job:Ref_Number'))
            if (jov:type    <> 'C')
                Break
            end ! if (jov:type    <> 'C')
            p_web.SSV('tmp:RRCCITotal',p_web.GSV('tmp:RRCCITotal') - jov:creditAmount)
            p_web.SSV('tmp:RRCIOutstanding',p_web.GSV('tmp:RRCIOutstanding') - jov:creditAmount)
        end ! loop

        access:INVOICE.clearKey(inv:invoice_Number_Key)
        inv:invoice_Number = p_web.GSV('job:invoice_number')
        if (access:INVOICE.tryfetch(inv:invoice_Number_Key) = level:Benign)
        end !

        access:TRADEACC.clearKey(tra:account_Number_Key)
        tra:account_Number = p_web.GSV('wob:HeadAccountNumber')
        if (access:TRADEACC.tryfetch(tra:account_Number_Key) = level:Benign)
        end

        p_web.SSV('tmp:OriginalInvoice',clip(inv:invoice_number) & '-' & clip(tra:BranchIdentification))


        p_web.SSV('Prompt:RCost1','Original Total')
        p_web.SSV('Prompt:RCost2','Original Invoice')
        p_web.SSV('Prompt:RCost3','')
        p_web.SSV('Prompt:RCost4','')
        p_web.SSV('Prompt:RCost5','')
        p_web.SSV('Prompt:RCost6','')
        p_web.SSV('Prompt:RCost7','')
        p_web.SSV('Prompt:RCost8','Current Total')

        p_web.SSV('tmp:RRCCost1',p_web.GSV('tmp:CreditTotal'))
        p_web.SSV('tmp:RRCCost2',0)
        p_web.SSV('tmp:RRCCost3',0)
        p_web.SSV('tmp:RRCCost4',0)
        p_web.SSV('tmp:RRCCost5',0)
        p_web.SSV('tmp:RRCCost6',0)
        p_web.SSV('tmp:RRCCost7',0)
        p_web.SSV('tmp:RRCCost8',p_web.GSV('tmp:RRCCITotal'))

    end !case (p_web.GSV('tmp:RRCViewCostType'))
displayOtherFields      routine
    p_web.SSV('Hide:ExchangeReplacement',1)

    p_web.SSV('Prompt:RCost1','')
    p_web.SSV('Prompt:ACost1','')


    if (p_web.GSV('tmp:ARCViewCostType') = 'Warranty')
        if (p_web.GSV('job:exchange_unit_number') > 0 and |
            (p_web.GSV('jobe:exchangedAtRRC') = 0 or |
                p_web.GSV('jobe:exchangedAtRRC') = 1 and p_web.GSV('job:repair_Type_Warranty') = 'R.T.M.'))
            p_web.SSV('Prompt:RCost1','Lost Loan Charge')
        else !job:Courier_Cost
            p_web.SSV('Prompt:RCost1','')
        end ! job:Courier_Cost
    end ! if (p_web.GSV('tmp:ARCViewCostType') = 'Warranty')

    LoanAttachedToJob(p_web)
    if (p_web.GSV('LoanAttachedToJob') = 1)
        if (p_web.GSV('jobe:HubRepair') = 0)
            if (p_web.GSV('tmp:RRCViewCostType') = 'Chargeable' Or |
                p_web.GSV('tmp:RRCViewCostType') = 'Chargeable - Invoiced' Or |
                p_web.GSV('tmp:RRCViewCostType') = 'Estimate')

                p_web.SSV('Prompt:RCost1','Lost Loan Charge')
            end
        else ! if (p_web.GSV('jobe:HubRepair') = 0)
            if (p_web.GSV('tmp:ARCViewCostType') = 'Chargeable' Or |
                p_web.GSV('tmp:ARCViewCostType') = 'Chargeable - Invoiced' Or |
                p_web.GSV('tmp:ARCViewCostType') = 'Estimate')

                p_web.SSV('Prompt:ACost1','Lost Loan Charge')
            end

        end ! if (p_web.GSV('jobe:HubRepair') = 0)
    else ! if (p_web.GSV('LoanAttachedToJob') = 1)
        p_web.SSV('Prompt:RCost1','')
        if (p_web.GSV('jobe:Engineer48HourOption') = 1 and p_web.GSV('job:chargeable_job') = 'YES')
            if (p_web.GSV('jobe:WebJob') = 1)
                p_web.SSV('Hide:ExchangeReplacement',0)

                if (securityCheckFailed(p_web.GSV('BookingUserPassword'),'EXCHANGE REPLACEMENT CHARGE'))
                else ! if (securityCheckFailed(p_web.GSV('BookingUserPassword','EXCHANGE REPLACEMENT CHARGE')
                end ! if (securityCheckFailed(p_web.GSV('BookingUserPassword','EXCHANGE REPLACEMENT CHARGE'))

                if (p_web.GSV('jobe:ExcReplcamentCharge') = 1)
                    p_web.SSV('Prompt:RCost1','Exchange Replacement')

                    p_web.SSV('Prompt:ACost1','Exchange Replacement')

                end ! if (p_web.GSV('jobe:ExcReplcamentCharge') = 1)
            end ! if (p_web.GSV('jobe:WebJob') = 1)
        end !if (p_web.GSV('jobe:Engineer48HourOption') = 1 and p_web.GSV('job:chargeable_job') = 'YES')
    end ! if (p_web.GSV('LoanAttachedToJob') = 1)
pricingRoutine      Routine
data
locARCPaid    Real()
locRRCPaid    Real()
locTotalPaid    Real()
code
    jobPricingRoutine(p_web)
    if (p_web.GSV('job:warranty_job') = 'YES')
        if (p_web.GSV('job:invoice_number_warranty') > 0)
            p_web.SSV('tmp:HandlingFee',p_web.GSV('jobe:invoiceHandlingFee'))
            p_web.SSV('tmp:ExchangeFee',p_web.GSV('jobe:invoiceExchangeRate'))

            Access:INVOICE.Clearkey(inv:invoice_number_Key)
            inv:invoice_number    = p_web.GSV('job:invoice_Number_warranty')
            if (Access:INVOICE.TryFetch(inv:invoice_number_Key) = Level:Benign)
                ! Found
                p_web.SSV('tmp:ARCInvoiceNumber',inv:invoice_number)

                p_web.SSV('tmp:ARCWIVat',p_web.GSV('job:WInvoice_Courier_Cost') * (inv:Vat_Rate_Labour/100) + |
                                p_web.GSV('job:WInvoice_Parts_Cost') * (inv:Vat_Rate_Parts/100) + |
                                p_web.GSV('job:WInvoice_Labour_Cost') * (inv:Vat_Rate_Labour/100))
                p_web.SSV('tmp:ARCWITotal',p_web.GSV('tmp:ARCWIVat') + p_web.GSV('job:WInvoice_Sub_Total'))


                p_web.SSV('tmp:ARCClaimIVAT',p_web.GSV('job:WInvoice_Courier_Cost') * (inv:Vat_Rate_Labour/100) + |
                                p_web.GSV('jobe:InvClaimPartsCost') * (inv:Vat_Rate_Parts/100) + |
                                p_web.GSV('jobe:InvoiceClaimValue') * (inv:Vat_Rate_Labour/100))
                p_web.SSV('tmp:ARCClaimISubTotal',p_web.GSV('job:WInvoice_Courier_Cost') + p_web.GSV('jobe:InvClaimPartsCost') + |
                                    p_web.GSV('jobe:InvoiceClaimValue'))
                p_web.SSV('tmp:ARCClaimITotal',p_web.GSV('tmp:ARCClaimIVat') + p_web.GSV('tmp:ARCClaimISubTotal'))

                p_web.SSV('tmp:AdjustmentSubTotal',p_web.GSV('jobe:LabourAdjustment') + p_web.GSV('jobe:ExchangeAdjustment') + |
                                        p_web.GSV('jobe:PartsAdjustment'))
                p_web.SSV('tmp:AdjustmentVAT',p_web.GSV('jobe:PartsAdjustment') * (inv:Vat_Rate_Parts/100) + |
                                    p_web.GSV('jobe:LabourAdjustment') * (inv:Vat_Rate_Labour/100) +|
                                    p_web.GSV('jobe:ExchangeAdjustment') * (inv:Vat_Rate_Labour/100))
                p_web.SSV('tmp:AdjustmentTotal',p_web.GSV('tmp:AdjustmentVAT') + p_web.GSV('tmp:AdjustmentSubTotal'))

            else ! if (Access:INVOICE.TryFetch(inv:invoice_number_Key) = Level:Benign)
                ! Error
            end ! if (Access:INVOICE.TryFetch(inv:invoice_number_Key) = Level:Benign)
        else ! if (p_web.GSV('job:invoice_number_warranty') > 0)
            p_web.SSV('tmp:HandlingFee',p_web.GSV('jobe:HandlingFee'))
            p_web.SSV('tmp:ExchangeFee',p_web.GSV('jobe:ExchangeRate'))
        end ! if (p_web.GSV('job:invoice_number_warranty') > 0)
    end ! if (p_web.GSV('job:warranty_job') = 'YES')

    if (p_web.GSV('job:Chargeable_Job') = 'YES')
        if (p_web.GSV('job:Invoice_Number') > 0)
            p_web.SSV('tmp:HandlingFee',p_web.GSV('jobe:InvoiceHandlingFee'))
            p_web.SSV('tmp:ExchangeFee',p_web.GSV('jobe:InvoiceExchangeRate'))

            Access:INVOICE.Clearkey(inv:invoice_number_Key)
            inv:invoice_number    = p_web.GSV('job:invoice_number')
            if (Access:INVOICE.TryFetch(inv:invoice_number_Key) = Level:Benign)
                ! Found

                Access:TRADEACC.Clearkey(tra:account_number_key)
                tra:account_number    = p_web.GSV('Default:ARCLocation')
                if (Access:TRADEACC.TryFetch(tra:account_number_key) = Level:Benign)
                    ! Found
                    p_web.SSV('tmp:ARCInvoiceNumber',Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification) & ' (' &CLIP(Format(inv:Date_Created,@d6))& ')')
                else ! if (Access:TRADEACC.TryFetch(tra:account_number_key) = Level:Benign)
                    ! Error
                end ! if (Access:TRADEACC.TryFetch(tra:account_number_key) = Level:Benign)

                if (inv:exportedRRCOracle)
                    p_web.SSV('jobe:InvRRCCSubTotal',p_web.GSV('job:Invoice_Courier_Cost') + |
                                            p_web.GSV('jobe:InvRRCCLabourCost') + |
                                            p_web.GSV('jobe:InvRRCCPartsCost'))

                    p_web.SSV('tmp:RRCCIVat',p_web.GSV('job:Invoice_Courier_Cost') * (inv:Vat_Rate_Labour/100) + |
                                    p_web.GSV('jobe:InvRRCCLabourCost') * (inv:Vat_Rate_Labour/100) + |
                                    p_web.GSV('jobe:InvRRCCPartsCost') * (inv:Vat_Rate_Parts/100))
                    p_web.SSV('tmp:RRCCITotal',p_web.GSV('tmp:RRCCIVat') + p_web.GSV('jobe:InvRRCCSubTotal'))

                    Access:TRADEACC.Clearkey(tra:account_number_key)
                    tra:account_number    = p_web.GSV('wob:HeadAccountNumber')
                    if (Access:TRADEACC.TryFetch(tra:account_number_key) = Level:Benign)
                        ! Found
                        p_web.SSV('tmp:RRCInvoiceNumber',Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification) & ' (' &CLIP(Format(inv:RRCInvoiceDate,@d6))& ')')
                        if (p_web.GSV('job:exchange_unit_Number') > 0)
                            p_web.SSV('tmp:RRCInvoiceNumber',p_web.GSV('tmp:RRCInvoiceNumber') & ' Exchange')
                        end ! if (p_web.GSV('job:exchange_unit_Number') > 0)
                    else ! if (Access:TRADEACC.TryFetch(tra:account_number_key) = Level:Benign)
                        ! Error
                    end ! if (Access:TRADEACC.TryFetch(tra:account_number_key) = Level:Benign)
                end ! if (inv:exportedRRCOracle)

                p_web.SSV('tmp:ARCCIVat',p_web.GSV('job:Invoice_Courier_Cost') * (inv:Vat_Rate_Labour/100) + |
                                p_web.GSV('job:Invoice_Parts_Cost') * (inv:Vat_Rate_Parts/100) + |
                                p_web.GSV('job:Invoice_Labour_Cost') * (inv:Vat_Rate_Labour/100))
                p_web.SSV('job:Invoice_Sub_Total',p_web.GSV('job:Invoice_Courier_Cost') + |
                                        p_web.GSV('job:Invoice_Labour_Cost') + |
                                        p_web.GSV('job:Invoice_Parts_Cost'))
                p_web.SSV('tmp:ARCCITotal',p_web.GSV('tmp:ARCCIVat') + p_web.GSV('job:invoice_Sub_Total'))

            else ! if (Access:INVOICE.TryFetch(inv:invoice_number_Key) = Level:Benign)
                ! Error
            end ! if (Access:INVOICE.TryFetch(inv:invoice_number_Key) = Level:Benign)
        else ! if (p_web.GSV('job:Invoice_Number') > 0)
            p_web.SSV('tmp:HandlingFee',p_web.GSV('jobe:HandlingFee'))
            p_web.SSV('tmp:ExchangeFee',p_web.GSV('jobe:ExchangeRate'))
        end ! if (p_web.GSV('job:Invoice_Number') > 0)
    end ! if (p_web.GSV('job:Chargeable_Job') = 'YES')


    ! Paid ?

    Access:JOBPAYMT.Clearkey(jpt:all_Date_Key)
    jpt:ref_Number    = p_web.GSV('job:Ref_Number')
    set(jpt:all_Date_Key,jpt:all_Date_Key)
    loop
        if (Access:JOBPAYMT.Next())
            Break
        end ! if (Access:JOBPAYMT.Next())
        if (jpt:ref_Number    <> p_web.GSV('job:Ref_Number'))
            Break
        end ! if (jpt:ref_Number    <> p_web.GSV('job:Ref_Number'))

        Access:USERS.Clearkey(use:user_code_Key)
        use:user_code    = jpt:user_code
        if (Access:USERS.TryFetch(use:user_code_Key) = Level:Benign)
            ! Found
            if (use:location = p_web.GSV('ARC:SiteLocation'))
                locARCPaid += jpt:Amount
            else
                locRRCPaid += jpt:Amount
            end ! if (use:location = p_web.GSV('ARC:SiteLocation'))
        else ! if (Access:USERS.TryFetch(use:user_code_Key) = Level:Benign)
            ! Error
        end ! if (Access:USERS.TryFetch(use:user_code_Key) = Level:Benign)
        locTotalPaid += jpt:amount
    end ! loop

    p_web.SSV('tmp:RRCCPaid',locRRCPaid)
    p_web.SSV('tmp:RRCCIPaid',locRRCPaid)
    p_web.SSV('tmp:ARCCPaid',locARCPaid)
    p_web.SSV('tmp:ARCCIPaid',locARCPaid)


    Access:TRDPARTY.Clearkey(trd:company_Name_Key)
    trd:company_Name    = p_web.GSV('job:Third_Party_Site')
    if (Access:TRDPARTY.TryFetch(trd:company_Name_Key) = Level:Benign)
        ! Found
        p_web.SSV('tmp:ARC3rdPartyVAT',(p_web.GSV('jobe:ARC3rdPartyCost') * trd:VatRate/100))
        p_web.SSV('tmp:ARC3rdPartyTotal',(p_web.GSV('jobe:ARC3rdPartyCost') * trd:VatRate/100))
        p_web.SSV('tmp:ARC3rdPartyMarkup',p_web.GSV('jobe:ARC3rdPartyCost') + p_web.GSV('tmp:ARC3rdPartyTotal'))

    else ! if (Access:TRDPARTY.TryFetch(trd:company_Name_Ke) = Level:Benign)
        ! Error
    end ! if (Access:TRDPARTY.TryFetch(trd:company_Name_Ke) = Level:Benign)


    p_web.SSV('tmp:ARCCVat',(p_web.GSV('job:Courier_Cost') * (GetVATRate(p_web.GSV('job:Account_Number'),'L') /100)) + |
                               (p_web.GSV('job:Parts_Cost') * (GetVATRate(p_web.GSV('job:Account_Number'),'P') /100)) +  |
                                (p_web.GSV('job:Labour_Cost') * (GetVATRate(p_web.GSV('job:Account_Number'),'L')/100)))

    p_web.SSV('tmp:ARCCTotal',p_web.GSV('tmp:ARCCVat') + p_web.GSV('job:sub_total'))

    p_web.SSV('tmp:RRCCVat',(p_web.GSV('job:Courier_Cost') * (GetVATRate(p_web.GSV('job:Account_Number'),'L') /100)) + |
                           (p_web.GSV('jobe:RRCCPartsCost') * (GetVATRate(p_web.GSV('job:Account_Number'),'P') /100)) +  |
                            (p_web.GSV('jobe:RRCCLabourCost') * (GetVATRate(p_web.GSV('job:Account_Number'),'L')/100)))

    p_web.SSV('tmp:RRCCTotal',p_web.GSV('tmp:RRCCVat') + p_web.GSV('jobe:RRCCSubTotal'))

    p_web.SSV('tmp:ARCWVAT',(p_web.GSV('job:Parts_Cost_Warranty') * (GetVATRate(p_web.GSV('job:Account_Number'),'P') /100)) +  |
                    (p_web.GSV('job:Labour_Cost_Warranty') * (GetVATRate(p_web.GSV('job:Account_Number'),'L')/100)) + |
                    (p_web.GSV('job:Courier_Cost_Warranty') * (GetVATRate(p_web.GSV('job:Account_Number'),'L') /100)))
    p_web.SSV('tmp:ARCWTotal',p_web.GSV('tmp:ARCWVat') + p_web.GSV('job:sub_total_warranty'))

    p_web.SSV('tmp:RRCWVAT',(p_web.GSV('jobe:RRCWPartsCost') * (GetVATRate(p_web.GSV('job:Account_Number'),'P') /100)) +  |
                    (p_web.GSV('jobe:RRCWLabourCost') * (GetVATRate(p_web.GSV('job:Account_Number'),'L')/100)))
    p_web.SSV('tmp:RRCWTotal',p_web.GSV('tmp:RRCWVAT') + p_web.GSV('jobe:RRCWSubTotal'))

    p_web.SSV('tmp:ARCEVAT',(p_web.GSV('job:Courier_Cost_Estimate') * (GetVATRate(p_web.GSV('job:Account_Number'),'L') /100)) + |
                           (p_web.GSV('job:Parts_Cost_Estimate') * (GetVATRate(p_web.GSV('job:Account_Number'),'P') /100)) +  |
                            (p_web.GSV('job:Labour_Cost_Estimate') * (GetVATRate(p_web.GSV('job:Account_Number'),'L')/100)))

    p_web.SSV('tmp:ARCETotal',p_web.GSV('tmp:ARCEVat') + p_web.GSV('job:Sub_Total_Estimate'))

    p_web.SSV('tmp:RRCEVAT',(p_web.GSV('job:Courier_Cost_Estimate') * (GetVATRate(p_web.GSV('job:Account_Number'),'L') /100)) + |
                           (p_web.GSV('jobe:RRCEPartsCost') * (GetVATRate(p_web.GSV('job:Account_Number'),'P') /100)) +  |
                            (p_web.GSV('jobe:RRCELabourCost') * (GetVATRate(p_web.GSV('job:Account_Number'),'L')/100)))
    p_web.SSV('tmp:RRCETotal',p_web.GSV('tmp:RRCEVAT') + p_web.GSV('jobe:RRCESubTotal'))

    p_web.SSV('jobe:InvRRCWSubTotal',p_web.GSV('jobe:InvRRCWPartsCost') + p_web.GSV('jobe:InvRRCWLabourCost'))

    p_web.SSV('tmp:RRCWIVAT',p_web.GSV('jobe:InvRRCWPartsCost') * (inv:Vat_Rate_Parts/100) + |
                    p_web.GSV('jobe:InvRRCWLabourCost') * (inv:Vat_Rate_Labour/100))
    p_web.SSV('tmp:RRCWITotal',p_web.GSV('tmp:RRCWIVAT') + p_web.GSV('jobe:InvRRCWSubTotal'))


    p_web.SSV('tmp:RRCOutstanding',p_web.GSV('tmp:RRCCTotal') - p_web.GSV('tmp:RRCCPaid'))
    p_web.SSV('tmp:ARCOutstanding',p_web.GSV('tmp:ARCCTotal') - p_web.GSV('tmp:ARCCPaid'))
    p_web.SSV('tmp:RRCIOutstanding',p_web.GSV('tmp:RRCCITotal') - p_web.GSV('tmp:RRCCIPaid'))
    p_web.SSV('tmp:ARCIOutstanding',p_web.GSV('tmp:ARCCITotal') - p_web.GSV('tmp:ARCCIPaid'))
restoreFields    routine
    p_web.SSV('job:Courier_Cost',p_web.GSV('save:Courier_Cost'))
    p_web.SSV('job:Labour_Cost',p_web.GSV('save:Labour_Cost'))
    p_web.SSV('job:Parts_Cot',p_web.GSV('save:Parts_Cost'))
    p_web.SSV('job:Sub_Total',p_web.GSV('save:Sub_Total'))
    p_web.SSV('job:Courier_Cost_Warranty',p_web.GSV('save:Courier_Cost_Warranty'))
    p_web.SSV('job:Labour_Cost_Warranty',p_web.GSV('save:Labour_Cost_Warranty'))
    p_web.SSV('job:Parts_Cost_Warranty',p_web.GSV('save:Parts_Cost_Warranty'))
    p_web.SSV('job:Sub_Total_Warranty',p_web.GSV('save:Sub_Total_Warranty'))
    p_web.SSV('jobe:ClaimValue',p_web.GSV('save:ClaimValue'))
    p_web.SSV('jobe:ClaimPartsCost',p_web.GSV('save:ClaimPartsCost'))
    p_web.SSV('jobe:ARC3rdPartyCost',p_web.GSV('save:ARC3rdPartyCost'))
    p_web.SSV('job:Courier_Cost_Estimate',p_web.GSV('save:Courier_Cost_Estimate'))
    p_web.SSV('job:Labour_Cost_Estimate',p_web.GSV('save:Labour_Cost_Estimate'))
    p_web.SSV('job:Parts_Cost_Estimate',p_web.GSV('save:Parts_Cost_Estimate'))
    p_web.SSV('job:Sub_Total_Estimate',p_web.GSV('save:Sub_Total_Estimate'))
    p_web.SSV('job:Invoice_Courier_Cost',p_web.GSV('save:Invoice_Courier_Cost'))
    p_web.SSV('job:Invoice_Labour_Cost',p_web.GSV('save:Invoice_Labour_Cost'))
    p_web.SSV('job:Invoice_Parts_Cost',p_web.GSV('save:Invoice_Parts_Cost'))
    p_web.SSV('job:Invoice_Sub_Total',p_web.GSV('save:Invoice_Sub_Total'))
    p_web.SSV('job:WInvoice_Courier_Cost',p_web.GSV('save:WInvoice_Courier_Cost'))
    p_web.SSV('job:WInvoice_Labour_Cost',p_web.GSV('save:WInvoice_Labour_Cost'))
    p_web.SSV('job:WInvoice_Parts_Cost',p_web.GSV('save:WInvoice_Parts_Cost'))
    p_web.SSV('job:WInvoice_Sub_Total',p_web.GSV('save:WInvoice_Sub_Total'))
    p_web.SSV('jobe:ExchangAdjustment',p_web.GSV('save:ExchangeAdjustment'))
    p_web.SSV('jobe:LabourAdjustment',p_web.GSV('save:LabourAdjustment'))
    p_web.SSV('jobe:PartsAdjustment',p_web.GSV('save:PartsAdjustment'))
    p_web.SSV('jobe2:WLabourPaid',p_web.GSV('save:WLabourPaid'))
    p_web.SSV('jobe2:WPartsPaid',p_web.GSV('save:WPartsPaid'))
    p_web.SSV('jobe2:WOtherCosts',p_web.GSV('save:WOtherCosts'))
    p_web.SSV('jobe2:WVat',p_web.GSV('save:WVat'))
    p_web.SSV('jobe2:WTotal',p_web.GSV('save:WTotal'))
    p_web.SSV('jobe:IgnoreClaimCosts',p_web.GSV('save:IgnoreClaimCosts'))
    p_web.SSV('job:Ignore_Warranty_Charges',p_web.GSV('save:Ignore_Warranty_Charges'))
    p_web.SSV('job:Ignore_Chargeable_Charges',p_web.GSV('save:Ignore_Chargeable_Charges'))
    p_web.SSV('job:Ignore_Estimate_Charges',p_web.GSV('save:Ignore_Estimate_Charges'))
    p_web.SSV('jobe:RRCClabourCost',p_web.GSV('save:RRCCLabourCost'))
    p_web.SSV('jobe:RRCCPartsCost',p_web.GSV('save:RRCCPartsCost'))
    p_web.SSV('jobe:RRCCSubTotal',p_web.GSV('save:RRCCSubTotal'))
    p_web.SSV('jobe:RRCWLabourCost',p_web.GSV('save:RRCWLabourCost'))
    p_web.SSV('jobe:RRCWPartsCost',p_web.GSV('save:RRCWPartsCost'))
    p_web.SSV('jobe:RRCWSubTotal',p_web.GSV('save:RRCWSubTotal'))
    p_web.SSV('jobe:IgnoreRRCChaCosts',p_web.GSV('save:IgnoreRRCChaCosts'))
    p_web.SSV('jobe:IgnoreRRCWarCosts',p_web.GSV('save:IgnoreRRCWarCosts'))
    p_web.SSV('jobe:RRCELabourCost',p_web.GSV('save:RRCELabourCost'))
    p_web.SSV('jobe:RRCEPartsCost',p_web.GSV('save:RRCEPartsCost'))
    p_web.SSV('jobe:RRCESubTotal',p_web.GSV('save:RRCESubTotal'))
    p_web.SSV('jobe:InvRRCCLabourCost',p_web.GSV('save:InvRRCCLabourCost'))
    p_web.SSV('jobe:InvRRCCPartsCost',p_web.GSV('save:InvRRCCPartsCost'))
    p_web.SSV('jobe:InvRRCCSubTotal',p_web.GSV('save:InvRRCCSubTotal'))
    p_web.SSV('jobe:IgnoreRRCEstCosts',p_web.GSV('save:IgnoreRRCEstCosts'))
    p_web.SSV('jobe:InvRRCWLabourCost',p_web.GSV('save:InvRRCWLabourCost'))
    p_web.SSV('jobe:InvRRCWPartsCost',p_web.GSV('save:InvRRCWPartsCost'))
    p_web.SSV('jobe:InvRRCWSubTotal',p_web.GSV('save:InvRRCWSubTotal'))
    p_web.SSV('jobe2:JobDiscountAmnt',p_web.GSV('save:JobDiscountAmnt'))
    p_web.SSV('jobe2:InvDiscountAmnt',p_web.GSV('save:InvDiscountAmnt'))
saveFields    routine
    p_web.SSV('save:Courier_Cost',p_web.GSV('job:Courier_Cost'))
    p_web.SSV('save:Labour_Cost',p_web.GSV('job:Labour_Cost'))
    p_web.SSV('save:Parts_Cot',p_web.GSV('job:Parts_Cost'))
    p_web.SSV('save:Sub_Total',p_web.GSV('job:Sub_Total'))
    p_web.SSV('save:Courier_Cost_Warranty',p_web.GSV('job:Courier_Cost_Warranty'))
    p_web.SSV('save:Labour_Cost_Warranty',p_web.GSV('job:Labour_Cost_Warranty'))
    p_web.SSV('save:Parts_Cost_Warranty',p_web.GSV('job:Parts_Cost_Warranty'))
    p_web.SSV('save:Sub_Total_Warranty',p_web.GSV('job:Sub_Total_Warranty'))
    p_web.SSV('save:ClaimValue',p_web.GSV('jobe:ClaimValue'))
    p_web.SSV('save:ClaimPartsCost',p_web.GSV('jobe:ClaimPartsCost'))
    p_web.SSV('save:ARC3rdPartyCost',p_web.GSV('jobe:ARC3rdPartyCost'))
    p_web.SSV('save:Courier_Cost_Estimate',p_web.GSV('job:Courier_Cost_Estimate'))
    p_web.SSV('save:Labour_Cost_Estimate',p_web.GSV('job:Labour_Cost_Estimate'))
    p_web.SSV('save:Parts_Cost_Estimate',p_web.GSV('job:Parts_Cost_Estimate'))
    p_web.SSV('save:Sub_Total_Estimate',p_web.GSV('job:Sub_Total_Estimate'))
    p_web.SSV('save:Invoice_Courier_Cost',p_web.GSV('job:Invoice_Courier_Cost'))
    p_web.SSV('save:Invoice_Labour_Cost',p_web.GSV('job:Invoice_Labour_Cost'))
    p_web.SSV('save:Invoice_Parts_Cost',p_web.GSV('job:Invoice_Parts_Cost'))
    p_web.SSV('save:Invoice_Sub_Total',p_web.GSV('job:Invoice_Sub_Total'))
    p_web.SSV('save:WInvoice_Courier_Cost',p_web.GSV('job:WInvoice_Courier_Cost'))
    p_web.SSV('save:WInvoice_Labour_Cost',p_web.GSV('job:WInvoice_Labour_Cost'))
    p_web.SSV('save:WInvoice_Parts_Cost',p_web.GSV('job:WInvoice_Parts_Cost'))
    p_web.SSV('save:WInvoice_Sub_Total',p_web.GSV('job:WInvoice_Sub_Total'))
    p_web.SSV('save:ExchangAdjustment',p_web.GSV('jobe:ExchangeAdjustment'))
    p_web.SSV('save:LabourAdjustment',p_web.GSV('jobe:LabourAdjustment'))
    p_web.SSV('save:PartsAdjustment',p_web.GSV('jobe:PartsAdjustment'))
    p_web.SSV('save:WLabourPaid',p_web.GSV('jobe2:WLabourPaid'))
    p_web.SSV('save:WPartsPaid',p_web.GSV('jobe2:WPartsPaid'))
    p_web.SSV('save:WOtherCosts',p_web.GSV('jobe2:WOtherCosts'))
    p_web.SSV('save:WVat',p_web.GSV('jobe2:WVat'))
    p_web.SSV('save:WTotal',p_web.GSV('jobe2:WTotal'))
    p_web.SSV('save:IgnoreClaimCosts',p_web.GSV('jobe:IgnoreClaimCosts'))
    p_web.SSV('save:Ignore_Warranty_Charges',p_web.GSV('job:Ignore_Warranty_Charges'))
    p_web.SSV('save:Ignore_Chargeable_Charges',p_web.GSV('job:Ignore_Chargeable_Charges'))
    p_web.SSV('save:Ignore_Estimate_Charges',p_web.GSV('job:Ignore_Estimate_Charges'))
    p_web.SSV('save:RRCClabourCost',p_web.GSV('jobe:RRCCLabourCost'))
    p_web.SSV('save:RRCCPartsCost',p_web.GSV('jobe:RRCCPartsCost'))
    p_web.SSV('save:RRCCSubTotal',p_web.GSV('jobe:RRCCSubTotal'))
    p_web.SSV('save:RRCWLabourCost',p_web.GSV('jobe:RRCWLabourCost'))
    p_web.SSV('save:RRCWPartsCost',p_web.GSV('jobe:RRCWPartsCost'))
    p_web.SSV('save:RRCWSubTotal',p_web.GSV('jobe:RRCWSubTotal'))
    p_web.SSV('save:IgnoreRRCChaCosts',p_web.GSV('jobe:IgnoreRRCChaCosts'))
    p_web.SSV('save:IgnoreRRCWarCosts',p_web.GSV('jobe:IgnoreRRCWarCosts'))
    p_web.SSV('save:RRCELabourCost',p_web.GSV('jobe:RRCELabourCost'))
    p_web.SSV('save:RRCEPartsCost',p_web.GSV('jobe:RRCEPartsCost'))
    p_web.SSV('save:RRCESubTotal',p_web.GSV('jobe:RRCESubTotal'))
    p_web.SSV('save:InvRRCCLabourCost',p_web.GSV('jobe:InvRRCCLabourCost'))
    p_web.SSV('save:InvRRCCPartsCost',p_web.GSV('jobe:InvRRCCPartsCost'))
    p_web.SSV('save:InvRRCCSubTotal',p_web.GSV('jobe:InvRRCCSubTotal'))
    p_web.SSV('save:IgnoreRRCEstCosts',p_web.GSV('jobe:IgnoreRRCEstCosts'))
    p_web.SSV('save:InvRRCWLabourCost',p_web.GSV('jobe:InvRRCWLabourCost'))
    p_web.SSV('save:InvRRCWPartsCost',p_web.GSV('jobe:InvRRCWPartsCost'))
    p_web.SSV('save:InvRRCWSubTotal',p_web.GSV('jobe:InvRRCWSubTotal'))
    p_web.SSV('save:JobDiscountAmnt',p_web.GSV('jobe2:JobDiscountAmnt'))
    p_web.SSV('save:InvDiscountAmnt',p_web.GSV('jobe2:InvDiscountAmnt'))

updateARCCost      routine
    case (p_web.GSV('tmp:ARCViewCostType'))
    of 'Chargeable'
        p_web.SSV('job:Labour_Cost',p_web.GSV('tmp:ARCCost2'))
    of 'Warranty'
        p_web.SSV('job:Labour_Cost_Warranty',p_web.GSV('tmp:ARCCost2'))
    of 'Estimate'
        p_web.SSV('job:Labour_Cost_Estimate',p_web.GSV('tmp:ARCCost2'))
    end ! case (p_web.GSV('tmp:RRCViewCostType'))
updateRRCCost      routine
    case (p_web.GSV('tmp:RRCViewCostType'))
    of 'Chargeable'
        p_web.SSV('jobe:RRCCLabourCost',p_web.GSV('tmp:RRCCost2'))
        p_web.SSV('jobe2:JobDiscountAmnt',p_web.GSV('DefaultLabourCost') - p_web.GSV('jobe:RRCCLabourCost'))
        if (p_web.GSV('jobe2:JobDiscountAmnt') < 0)
            p_web.SSV('jobe2:JobDiscountAmnt',0)
        end

    of 'Warranty'
        p_web.SSV('jobe:RRCWLabourCost',p_web.GSV('tmp:RRCCost2'))
    of 'Estimate'
        p_web.SSV('jobe:RRCELabourCost',p_web.GSV('tmp:RRCCost2'))
    end ! case (p_web.GSV('tmp:RRCViewCostType'))

OpenFiles  ROUTINE
  p_web._OpenFile(JOBSE2)
  p_web._OpenFile(SUBCHRGE)
  p_web._OpenFile(TRACHRGE)
  p_web._OpenFile(STDCHRGE)
  p_web._OpenFile(USERS)
  p_web._OpenFile(JOBPAYMT)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(INVOICE)
  p_web._OpenFile(JOBSINV)
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(TRDPARTY)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(JOBSE2)
  p_Web._CloseFile(SUBCHRGE)
  p_Web._CloseFile(TRACHRGE)
  p_Web._CloseFile(STDCHRGE)
  p_Web._CloseFile(USERS)
  p_Web._CloseFile(JOBPAYMT)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(INVOICE)
  p_Web._CloseFile(JOBSINV)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(TRDPARTY)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE
  loc:EipClm = upper(p_web.GetValue('_EIPClm'))
  p_web.DeleteValue('_EIPClm')
  case loc:EipClm
  of ''
    case upper(p_web.GetValue('_calledfrom_'))
    of upper('BrowseJobCredits')
      do Value::BrowseJobCredits
    end
  end

InitForm       Routine
  DATA
LF  &FILE
  CODE
  ! Return URL
  IF (p_web.IfExistsValue('ViewCostsReturnURL'))
      p_web.StoreValue('ViewCostsReturnURL')
  ELSE
      p_web.SSV('ViewCostsReturnURL','ViewJob')
  END
  p_web.SSV('Prompt:RCost0','')
  p_web.SSV('Prompt:RCost1','')
  p_web.SSV('Prompt:RCost2','')
  p_web.SSV('Prompt:RCost3','')
  p_web.SSV('Prompt:RCost4','')
  p_web.SSV('Prompt:RCost5','')
  p_web.SSV('Prompt:RCost6','')
  p_web.SSV('Prompt:RCost7','')
  p_web.SSV('Prompt:RCost8','')
  p_web.SSV('Prompt:ACost1','')
  p_web.SSV('Prompt:ACost2','')
  p_web.SSV('Prompt:ACost3','')
  p_web.SSV('Prompt:ACost4','')
  p_web.SSV('Prompt:ACost5','')
  p_web.SSV('Prompt:ACost6','')
  p_web.SSV('Prompt:ACost7','')
  p_web.SSV('Prompt:ACost8','')
  p_web.SSV('tmp:ARCIgnoreReason','')
  p_web.SSV('tmp:RRCIgnoreReason','')
  p_web.SSV('ValidateIgnoreTickBoxARC',0)
  p_web.SSV('ValidateIgnoreTickBox',0)
  
  !Empty Audit Queue
  clear(tmpaud:Record)
  tmpaud:sessionID = p_web.SessionID
  set(tmpaud:keySessionID,tmpaud:keySessionID)
  loop
      next(tempAuditQueue)
      if (error())
          break
      end ! if (error())
      if (tmpaud:sessionID <> p_web.sessionID)
          break
      end ! if (tmpaud:sessionID <> p_web.sessionID)
      delete(tempAuditQueue)
  end
  
  p_web.SSV('JobPricingRoutine:ForceWarranty',0)
  JobPricingRoutine(p_web)
  
  do pricingRoutine
  
  p_web.SSV('Hide:ARCCosts',1)
  p_web.SSV('Hide:RRCCosts',1)
  
  p_web.SSV('Hide:ARCChargeable',1)
  p_web.SSV('Hide:ARCWarranty',1)
  p_web.SSV('Hide:ARCClaim',1)
  p_web.SSV('Hide:ARC3rdParty',1)
  p_web.SSV('Hide:ARCEstimate',1)
  p_web.SSV('Hide:ARCCInvoice',1)
  p_web.SSV('Hide:ARCWInvoice',1)
  p_web.SSV('Hide:ARCClaimInvoice',1)
  p_web.SSV('Hide:ManufacturerPaid',1)
  p_web.SSV('Hide:RRCChargeable',1)
  p_web.SSV('Hide:RRCWarranty',1)
  p_web.SSV('Hide:RRCEstimate',1)
  p_web.SSV('Hide:RRCHandling',1)
  p_web.SSV('Hide:RRCExchange',1)
  p_web.SSV('Hide:RRCCInvoice',1)
  p_web.SSV('Hide:RRCWInvoice',1)
  p_web.SSV('Hide:Credit',1)
  
  p_web.SSV('ReadOnly:ARCCost1',1)
  p_web.SSV('ReadOnly:ARCCost2',1)
  p_web.SSV('ReadOnly:ARCCost3',1)
  p_web.SSV('ReadOnly:RRCCost1',1)
  p_web.SSV('ReadOnly:RRCCost2',1)
  p_web.SSV('ReadOnly:RRCCost3',1)
  
  
  sentToHub(p_web)
  
  if (p_web.GSV('jobe:WebJob') = 1)
      if (p_web.GSV('job:Chargeable_job') = 'YES')
          if (p_web.GSV('BookingSite') = 'RRC')
              p_web.SSV('Hide:RRCChargeable',0)
              if (p_web.GSV('job:Exchange_Unit_Number') > 0)
                  if (p_web.GSV('jobe:ExchangedAtRRC'))
                      p_web.SSV('Hide:RRCExchange',0)
                  else ! if (p_web.GSV('jobe:ExchangedAtRRC'))
                      p_web.SSV('Hide:RRCHandling',0)
                      p_web.SSV('Hide:ARCChargeable',0)
                  end ! if (p_web.GSV('jobe:ExchangedAtRRC'))
              else ! if (p_web.GSV('job:Exchange_Unit_Number') > 0)
                  sentToHub(p_web)
                  if (p_web.GSV('SentToHub') = 1)
                      p_web.SSV('Hide:ARCChargeable',0)
                      p_web.SSV('Hide:RRCHandling',0)
                  end ! if (p_web.GSV('SentToHub') = 1)
              end ! if (p_web.GSV('job:Exchange_Unit_Number') > 0)
          else ! if (p_web.GSV('BookingSite') = 'RRC')
  
              p_web.SSV('Hide:RRCChargeable',0)
              if (p_web.GSV('job:Exchange_Unit_Number') > 0)
                  if (p_web.GSV('jobe:ExchangedAtRRC'))
                      p_web.SSV('Hide:RRCExchange',0)
                  else ! if (p_web.GSV('jobe:ExchangedAtRRC'))
                      p_web.SSV('Hide:RRCHandling',0)
                  end ! if (p_web.GSV('jobe:ExchangedAtRRC'))
              else ! if (p_web.GSV('job:Exchange_Unit_Number') > 0)
                  if (p_web.GSV('SentToHub') = 1)
                      p_web.SSV('Hide:RRCHandling',0)
                  end ! if (p_web.GSV('SentToHub') = 1)
              end ! if (p_web.GSV('job:Exchange_Unit_Number') > 0)
  
              if (p_web.GSV('SentToHub') = 1)
                  p_web.SSV('Hide:ARCChargeable',0)
              end ! if (p_web.GSV('SentToHub') = 1)
          end ! if (p_web.GSV('BookingSite') = 'RRC')
      end ! if (p_web.GSV('job:Chargeable_job') = 'YES')
  
      if (p_web.GSV('job:Warranty_job') = 'YES')
          if (p_web.GSV('BookingSite') = 'RRC')
              if (p_web.GSV('job:Exchange_Unit_Number') > 0)
                  if (p_web.GSV('jobe:ExchangedAtRRC'))
                      p_web.SSV('Hide:RRCExchange',0)
                  else ! if (p_web.GSV('jobe:ExchangedAtRRC'))
                      p_web.SSV('Hide:RRCHandling',0)
                  end ! if (p_web.GSV('jobe:ExchangedAtRRC'))
              else ! if (p_web.GSV('job:Exchange_Unit_Number') > 0)
                  if (p_web.GSV('SentToHub') = 1)
                      p_web.SSV('Hide:RRCHandling',0)
                  else
                      p_web.SSV('Hide:RRCWarranty',0)
                  end ! if (p_web.GSV('SentToHub') = 1)
              end ! if (p_web.GSV('job:Exchange_Unit_Number') > 0)
          else ! if (p_web.GSV('BookingSite') = 'RRC'           )
              p_web.SSV('Hide:ARCClaim',0)
              if (p_web.GSV('job:Exchange_Unit_Number') > 0)
                  if (p_web.GSV('jobe:ExchangedAtRRC'))
                      p_web.SSV('Hide:RRCExchange',0)
                      p_web.SSV('Hide:ARCWarranty',0)
                  else ! if (p_web.GSV('jobe:ExchangedAtRRC'))
                      p_web.SSV('Hide:RRCHandling',0)
                      p_web.SSV('Hide:ARCWarranty',0)
                  end ! if (p_web.GSV('jobe:ExchangedAtRRC'))
              else ! if (p_web.GSV('job:Exchange_Unit_Number') > 0)
                  if (p_web.GSV('SentToHub') = 1)
                      p_web.SSV('Hide:RRCHandling',0)
                      p_web.SSV('Hide:ARCWarranty',0)
                  else
                      p_web.SSV('Hide:RRCWarranty',0)
                  end ! if (p_web.GSV('SentToHub') = 1)
              end ! if (p_web.GSV('job:Exchange_Unit_Number') > 0)
          end !
      end  !if (p_web.GSV('job:Warranty_job') <> 'YES')
  else ! if (p_web.GSV('jobe:WebJob'))
      if (p_web.GSV('BookingSite') <> 'RRC')
          if (p_web.GSV('job:Chargeable_Job') = 'YES')
              p_web.SSV('Hide:ARCChargeable',0)
          end ! if (p_web.GSV('job:Chargeable_Job') = 'YES')
          if (p_web.GSV('job:Warranty_job') = 'YES')
              p_web.SSV('Hide:ARCWarranty',0)
              p_web.SSV('Hide:ARCClaim',0)
          end ! if (p_web.GSV('job:Warranty_job') = 'YES')
      end ! if (p_web.GSV('BookingSite') <> 'RRC')
  end ! if (p_web.GSV('jobe:WebJob'))
  
  
  Access:INVOICE.Clearkey(inv:invoice_Number_Key)
  inv:Invoice_Number    = p_web.GSV('job:Invoice_Number')
  if (Access:INVOICE.TryFetch(inv:invoice_Number_Key) = Level:Benign)
      ! Found
  else ! if (Access:INVOICE.TryFetch(inv:invoice_Number_Key) = Level:Benign)
      ! Error
  end ! if (Access:INVOICE.TryFetch(inv:invoice_Number_Key) = Level:Benign)
  
  if (p_web.GSV('job:Invoice_Number') > 0)
      if (p_web.GSV('Hide:ARCChargeable') = 0 and inv:ARCInvoiceDate > 0)
          p_web.SSV('Hide:ARCChargeable',1)
          p_web.SSV('Hide:ARCCInvoice',0)
      end ! if (p_web.GSV('Hide:ARCChargeable') = 0)
  
      if (p_web.GSV('Hide:RRCChargeable') = 0 and inv:ExportedRRCOracle)
          p_web.SSV('Hide:RRCChargeable',1)
          p_web.SSV('Hide:RRCCInvoice',0)
      end ! if (p_web.GSV('Hide:RRCChargeable') = 0 and inv:ExportedRRCOracle)
  end ! if (p_web.GSV('job:Invoice_Number') > 0)
  
  if (p_web.GSV('Hide:RRCWarranty') = 0 and p_web.GSV('wob:RRCWInvoiceNumber') > 0)
      p_web.SSV('Hide:RRCWarranty',1)
      p_web.SSV('Hide:RRCWInvoice',0)
  end ! if (p_web.GSV('Hide:RRCWarranty',0))
  
  if (p_web.GSV('job:Invoice_Number_Waranty') > 0)
      if (p_web.GSV('Hide:ARCWarranty') = 0)
          p_web.SSV('Hide:ARCWarranty',1)
          p_web.SSV('Hide:ARCWInvoice',0)
      end ! if (p_web.GSV('Hide:RRCWarranty',0))
  
      if (p_web.GSV('Hide:ARCClaim') = 0)
          p_web.SSV('Hide:ARCClaim',1)
          p_web.SSV('Hide:ARCClaimInvoice',0)
          p_web.SSV('Hide:ManufacturerPaid',0)
      end ! if (p_web.GSV('Hide:ARCClaim',0))
  end ! if (p_web.GSV('job:Invoice_Number_Waranty') > 0)
  
  if (p_web.GSV('Hide:RRCHandling') = 0)
      if ((p_web.GSV('job:Chargeable_Job') = 'YES' And |
          ExcludeHandlingFee('C',p_web.GSV('job:Manufacturer'),p_web.GSV('job:Repair_Type'))) Or |
          (p_web.GSV('job:Warranty_job') = 'YES' And |
          ExcludeHandlingFee('W',p_web.GSV('job:Manufacturer'),p_web.GSV('job:Repair_Type_Warranty'))))
  
          p_web.SSV('Hide:RRCHandling',1)
      end
  end ! if (p_web.GSV('Hide:RRCHandling',0))
  
  if (p_web.GSV('job:Chargeable_Job') = 'YES')
      if (p_web.GSV('job:Estimate') = 'YES')
          if (p_web.GSV('job:Estimate_Accepted') <> 'YES' And |
              p_web.GSV('job:Estimate_Rejected'))
              p_web.SSV('Hide:RRCChargeable',1)
              p_web.SSV('Hide:ARCChargeable',1)
          end
  
          if (p_web.GSV('jobe:WebJob') = 1)
              p_web.SSV('Hide:RRCEstimate',0)
              if (p_web.GSV('SentToHub') = 1)
                  p_web.SSV('Hide:ARCEstimate',0)
              end ! if (p_web.GSV('SentToHub') = 1)
          else ! if (p_web.GSV('jobe:WebJob'))
              p_web.SSV('Hide:ARCEstimate',0)
          end ! if (p_web.GSV('jobe:WebJob'))
  
      end ! if (p_web.GSV('job:Estimate') = 'YES')
  
  end ! if (p_web.GSV('job:Chargeable_Job') = 'YES')
  
  if (p_web.GSV('BookingSite') <> 'RRC')
      Access:JOBTHIRD.Clearkey(jot:RefNumberKey)
      jot:RefNumber    = p_web.GSV('job:ref_Number')
      if (Access:JOBTHIRD.TryFetch(jot:RefNumberKey) = Level:Benign)
          ! Found
          p_web.SSV('Hide:ARC3rdParty',0)
      else ! if (Access:JOBTHIRD.TryFetch(jot:RefNumberKey) = Level:Benign)
          ! Error
      end ! if (Access:JOBTHIRD.TryFetch(jot:RefNumberKey) = Level:Benign)
  end ! if (p_web.GSV('BookingSite') <> 'RRC')
  
  if (p_web.GSV('Hide:RRCHandling') = 0)
      if (p_web.GSV('job:Chargeable_Job') = 'YES')
  
          Access:REPTYDEF.Clearkey(rtd:ChaManRepairTypeKey)
          rtd:Manufacturer    = p_web.GSV('job:Manufacturer')
          rtd:Chargeable    = 'YES'
          rtd:Repair_Type    = p_web.GSV('job:repair_Type')
          if (Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign)
              ! Found
              if (rtd:BER = 4)
                  p_web.SSV('Hide:RRCHandling',1)
              end ! if (rtd:BER = 4)
          else ! if (Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign)
              ! Error
          end ! if (Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign)
      end ! if (p_web.GSV('job:Chargeable_Job') = 'YES')
  
      if (p_web.GSV('job:Warranty_Job') = 'YES')
  
          Access:REPTYDEF.Clearkey(rtd:WarManRepairTypeKey)
          rtd:Manufacturer    = p_web.GSV('job:Manufacturer')
          rtd:warranty    = 'YES'
          rtd:Repair_Type    = p_web.GSV('job:repair_Type_warranty')
          if (Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign)
              ! Found
              if (rtd:BER = 4)
                  p_web.SSV('Hide:RRCHandling',1)
              end ! if (rtd:BER = 4)
          else ! if (Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign)
              ! Error
          end ! if (Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign)
      end ! if (p_web.GSV('job:Chargeable_Job') = 'YES')
  end ! if (p_web.GSV('Hide:RRCHandling') = 0)
  
  if (p_web.GSV('Hide:RRCCInvoice') = 0)
  
      Access:JOBSINV.Clearkey(jov:typeRecordKey)
      jov:refNumber    = p_web.GSV('job:Ref_Number')
      jov:type    = 'C'
      set(jov:typeRecordKey,jov:typeRecordKey)
      loop
          if (Access:JOBSINV.Next())
              Break
          end ! if (Access:JOBSINV.Next())
          if (jov:refNumber    <> p_web.GSV('job:Ref_Number'))
              Break
          end ! if (jov:refNumber    <> p_web.GSV('job:Ref_Number'))
          if (jov:type    <> 'C')
              Break
          end ! if (jov:type    <> 'C')
          p_web.SSV('Hide:Credit',0)
          break
      end ! loop
  end ! if (p_web.GSV('Hide:RRCCInvoice') = 0)
  
  
  if (p_web.GSV('Hide:ARCChargeable') = 0 or |
      p_web.GSV('Hide:ARCWarranty') = 0 or |
      p_web.GSV('Hide:ARCClaim') = 0 or |
      p_web.GSV('Hide:ARC3rdParty') = 0 or |
      p_web.GSV('Hide:ARCEstimate') = 0 or |
      p_web.GSV('Hide:ARCCInvoice') = 0 or |
      p_web.GSV('Hide:ARCWInvoice') = 0 or |
      p_web.GSV('Hide:ARCClaimInvoice') = 0 or |
      p_web.GSV('Hide:ManufacturerPaid') = 0)
      p_web.SSV('Hide:ARCCosts',0)
  end
  
  if (p_web.GSV('Hide:RRCChargeable') = 0 or |
      p_web.GSV('Hide:RRCWarranty') = 0 or |
      p_web.GSV('Hide:RRCEstimate') = 0 or |
      p_web.GSV('Hide:RRCHandling') = 0 or |
      p_web.GSV('Hide:RRCExchange') = 0 or |
      p_web.GSV('Hide:RRCCInvoice') = 0 or |
      p_web.GSV('Hide:RRCWInvoice') = 0 or |
      p_web.GSV('Hide:Credit') = 0)
      p_web.SSV('Hide:RRCCosts',0)
  end
  
  if (p_web.GSV('Hide:RRCChargeable') = 0)
      p_web.SSV('tmp:RRCViewCostType','Chargeable')
  elsif (p_web.GSV('Hide:RRCWarranty') = 0)
      p_web.SSV('tmp:RRCViewCostType','Warranty')
  elsif (p_web.GSV('Hide:RRCEstimate') = 0)
      p_web.SSV('tmp:RRCViewCostType','Estimate')
  elsif (p_web.GSV('Hide:RRCHandling') = 0)
      p_web.SSV('tmp:RRCViewCostType','Handling')
  elsif (p_web.GSV('Hide:RRCExchange') = 0)
      p_web.SSV('tmp:RRCViewCostType','Exchange')
  elsif (p_web.GSV('Hide:RRCCInvoice') = 0)
      p_web.SSV('tmp:RRCViewCostType','Chargeable - Invoiced')
  elsif (p_web.GSV('Hide:RRCWInvoice') = 0)
      p_web.SSV('tmp:RRCViewCostType','Warranty - Invoiced')
  elsif (p_web.GSV('Hide:Credit') = 0)
      p_web.SSV('tmp:RRCViewCostType','Credits')
  else
      p_web.SSV('tmp:RRCVIewCostType','')
  end !if (p_web.GSV('Hide:RRCChargeable') = 0)
  
  if (p_web.GSV('Hide:ARCChargeable') = 0)
      p_web.SSV('tmp:ARCViewCostType','Chargeable')
  elsif (p_web.GSV('Hide:ARCWarranty') = 0)
      p_web.SSV('tmp:ARCViewCostType','Warranty')
  elsif (p_web.GSV('Hide:ARCClaim') = 0)
      p_web.SSV('tmp:ARCViewCostType','Claim')
  elsif (p_web.GSV('Hide:ARC3rdParty') = 0)
      p_web.SSV('tmp:ARCViewCostType','3rd Party')
  elsif (p_web.GSV('Hide:ARCEstimate') = 0)
      p_web.SSV('tmp:ARCViewCostType','Estimate')
  elsif (p_web.GSV('Hide:ARCCInvoice') = 0)
      p_web.SSV('tmp:ARCViewCostType','Chargeble - Invoiced')
  elsif (p_web.GSV('Hide:ARCWInvoice') = 0)
      p_web.SSV('tmp:ARCViewCostType','Warranty - Invoiced')
  elsif (p_web.GSV('Hide:ARCClaimInvoice') = 0)
      p_web.SSV('tmp:ARCViewCostType','Claim - Invoiced')
  elsif (p_web.GSV('Hide:ManufacturerPaid') = 0)
      p_web.SSV('tmp:ARCViewCostType','Manufacturer Payment')
  else
      p_web.SSV('tmp:ARCVIewCostType','')
  end !if (p_web.GSV('Hide:RRCChargeable') = 0)
  
  
  do displayARCCostFields
  do displayRRCCostFields
  do displayOtherFields
  
  do saveFields
  
  do DefaultLabourCost
  
  LoanAttachedToJob(p_web) ! Has a loan unit been, or is, attached?
  p_web.SetValue('ViewCosts_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'ViewCosts'
    end
    p_web.formsettings.proc = 'ViewCosts'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine
  do restoreFields
  Do DeleteSessionValues

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('tmp:ARCCost1')
    p_web.SetPicture('tmp:ARCCost1','@n14.2')
  End
  p_web.SetSessionPicture('tmp:ARCCost1','@n14.2')
  If p_web.IfExistsValue('tmp:AdjustmentCost1')
    p_web.SetPicture('tmp:AdjustmentCost1','@n14.2')
  End
  p_web.SetSessionPicture('tmp:AdjustmentCost1','@n14.2')
  If p_web.IfExistsValue('tmp:ARCCost2')
    p_web.SetPicture('tmp:ARCCost2','@n14.2')
  End
  p_web.SetSessionPicture('tmp:ARCCost2','@n14.2')
  If p_web.IfExistsValue('tmp:AdjustmentCost2')
    p_web.SetPicture('tmp:AdjustmentCost2','@n14.2')
  End
  p_web.SetSessionPicture('tmp:AdjustmentCost2','@n14.2')
  If p_web.IfExistsValue('tmp:ARCCost3')
    p_web.SetPicture('tmp:ARCCost3','@n14.2')
  End
  p_web.SetSessionPicture('tmp:ARCCost3','@n14.2')
  If p_web.IfExistsValue('tmp:AdjustmentCost3')
    p_web.SetPicture('tmp:AdjustmentCost3','@n14.2')
  End
  p_web.SetSessionPicture('tmp:AdjustmentCost3','@n14.2')
  If p_web.IfExistsValue('tmp:ARCCost4')
    p_web.SetPicture('tmp:ARCCost4','@n14.2')
  End
  p_web.SetSessionPicture('tmp:ARCCost4','@n14.2')
  If p_web.IfExistsValue('tmp:AdjustmentCost4')
    p_web.SetPicture('tmp:AdjustmentCost4','@n14.2')
  End
  p_web.SetSessionPicture('tmp:AdjustmentCost4','@n14.2')
  If p_web.IfExistsValue('tmp:ARCCost5')
    p_web.SetPicture('tmp:ARCCost5','@n14.2')
  End
  p_web.SetSessionPicture('tmp:ARCCost5','@n14.2')
  If p_web.IfExistsValue('tmp:AdjustmentCost5')
    p_web.SetPicture('tmp:AdjustmentCost5','@n14.2')
  End
  p_web.SetSessionPicture('tmp:AdjustmentCost5','@n14.2')
  If p_web.IfExistsValue('tmp:ARCCost6')
    p_web.SetPicture('tmp:ARCCost6','@n14.2')
  End
  p_web.SetSessionPicture('tmp:ARCCost6','@n14.2')
  If p_web.IfExistsValue('tmp:AdjustmentCost6')
    p_web.SetPicture('tmp:AdjustmentCost6','@n14.2')
  End
  p_web.SetSessionPicture('tmp:AdjustmentCost6','@n14.2')
  If p_web.IfExistsValue('tmp:ARCCost7')
    p_web.SetPicture('tmp:ARCCost7','@n14.2')
  End
  p_web.SetSessionPicture('tmp:ARCCost7','@n14.2')
  If p_web.IfExistsValue('tmp:ARCCost8')
    p_web.SetPicture('tmp:ARCCost8','@n14.2')
  End
  p_web.SetSessionPicture('tmp:ARCCost8','@n14.2')
  If p_web.IfExistsValue('jobe:ARC3rdPartyInvoiceNumber')
    p_web.SetPicture('jobe:ARC3rdPartyInvoiceNumber','@s30')
  End
  p_web.SetSessionPicture('jobe:ARC3rdPartyInvoiceNumber','@s30')
  If p_web.IfExistsValue('tmp:RRCCost0')
    p_web.SetPicture('tmp:RRCCost0','@n14.2')
  End
  p_web.SetSessionPicture('tmp:RRCCost0','@n14.2')
  If p_web.IfExistsValue('tmp:RRCCost1')
    p_web.SetPicture('tmp:RRCCost1','@n14.2')
  End
  p_web.SetSessionPicture('tmp:RRCCost1','@n14.2')
  If p_web.IfExistsValue('tmp:RRCCost2')
    p_web.SetPicture('tmp:RRCCost2','@n14.2')
  End
  p_web.SetSessionPicture('tmp:RRCCost2','@n14.2')
  If p_web.IfExistsValue('tmp:RRCCost3')
    p_web.SetPicture('tmp:RRCCost3','@n14.2')
  End
  p_web.SetSessionPicture('tmp:RRCCost3','@n14.2')
  If p_web.IfExistsValue('tmp:RRCCost4')
    p_web.SetPicture('tmp:RRCCost4','@n14.2')
  End
  p_web.SetSessionPicture('tmp:RRCCost4','@n14.2')
  If p_web.IfExistsValue('tmp:RRCCost5')
    p_web.SetPicture('tmp:RRCCost5','@n14.2')
  End
  p_web.SetSessionPicture('tmp:RRCCost5','@n14.2')
  If p_web.IfExistsValue('tmp:RRCCost6')
    p_web.SetPicture('tmp:RRCCost6','@n14.2')
  End
  p_web.SetSessionPicture('tmp:RRCCost6','@n14.2')
  If p_web.IfExistsValue('tmp:RRCCost7')
    p_web.SetPicture('tmp:RRCCost7','@n14.2')
  End
  p_web.SetSessionPicture('tmp:RRCCost7','@n14.2')
  If p_web.IfExistsValue('tmp:RRCCost8')
    p_web.SetPicture('tmp:RRCCost8','@n14.2')
  End
  p_web.SetSessionPicture('tmp:RRCCost8','@n14.2')

AfterLookup Routine
  loc:TabNumber = -1
  If p_web.GSV('Hide:ARCCosts') <> 1
    loc:TabNumber += 1
  End
  If p_web.GSV('Hide:RRCCosts') <> 1
    loc:TabNumber += 1
  End
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  if p_web.IfExistsValue('tmp:ARCViewCostType') = 0
    p_web.SetSessionValue('tmp:ARCViewCostType',tmp:ARCViewCostType)
  Else
    tmp:ARCViewCostType = p_web.GetSessionValue('tmp:ARCViewCostType')
  End
  if p_web.IfExistsValue('tmp:ARCIgnoreDefaultCharges') = 0
    p_web.SetSessionValue('tmp:ARCIgnoreDefaultCharges',tmp:ARCIgnoreDefaultCharges)
  Else
    tmp:ARCIgnoreDefaultCharges = p_web.GetSessionValue('tmp:ARCIgnoreDefaultCharges')
  End
  if p_web.IfExistsValue('tmp:ARCIgnoreReason') = 0
    p_web.SetSessionValue('tmp:ARCIgnoreReason',tmp:ARCIgnoreReason)
  Else
    tmp:ARCIgnoreReason = p_web.GetSessionValue('tmp:ARCIgnoreReason')
  End
  if p_web.IfExistsValue('tmp:ARCCost1') = 0
    p_web.SetSessionValue('tmp:ARCCost1',tmp:ARCCost1)
  Else
    tmp:ARCCost1 = p_web.GetSessionValue('tmp:ARCCost1')
  End
  if p_web.IfExistsValue('tmp:AdjustmentCost1') = 0
    p_web.SetSessionValue('tmp:AdjustmentCost1',tmp:AdjustmentCost1)
  Else
    tmp:AdjustmentCost1 = p_web.GetSessionValue('tmp:AdjustmentCost1')
  End
  if p_web.IfExistsValue('tmp:ARCCost2') = 0
    p_web.SetSessionValue('tmp:ARCCost2',tmp:ARCCost2)
  Else
    tmp:ARCCost2 = p_web.GetSessionValue('tmp:ARCCost2')
  End
  if p_web.IfExistsValue('tmp:AdjustmentCost2') = 0
    p_web.SetSessionValue('tmp:AdjustmentCost2',tmp:AdjustmentCost2)
  Else
    tmp:AdjustmentCost2 = p_web.GetSessionValue('tmp:AdjustmentCost2')
  End
  if p_web.IfExistsValue('tmp:ARCCost3') = 0
    p_web.SetSessionValue('tmp:ARCCost3',tmp:ARCCost3)
  Else
    tmp:ARCCost3 = p_web.GetSessionValue('tmp:ARCCost3')
  End
  if p_web.IfExistsValue('tmp:AdjustmentCost3') = 0
    p_web.SetSessionValue('tmp:AdjustmentCost3',tmp:AdjustmentCost3)
  Else
    tmp:AdjustmentCost3 = p_web.GetSessionValue('tmp:AdjustmentCost3')
  End
  if p_web.IfExistsValue('tmp:ARCCost4') = 0
    p_web.SetSessionValue('tmp:ARCCost4',tmp:ARCCost4)
  Else
    tmp:ARCCost4 = p_web.GetSessionValue('tmp:ARCCost4')
  End
  if p_web.IfExistsValue('tmp:AdjustmentCost4') = 0
    p_web.SetSessionValue('tmp:AdjustmentCost4',tmp:AdjustmentCost4)
  Else
    tmp:AdjustmentCost4 = p_web.GetSessionValue('tmp:AdjustmentCost4')
  End
  if p_web.IfExistsValue('tmp:ARCCost5') = 0
    p_web.SetSessionValue('tmp:ARCCost5',tmp:ARCCost5)
  Else
    tmp:ARCCost5 = p_web.GetSessionValue('tmp:ARCCost5')
  End
  if p_web.IfExistsValue('tmp:AdjustmentCost5') = 0
    p_web.SetSessionValue('tmp:AdjustmentCost5',tmp:AdjustmentCost5)
  Else
    tmp:AdjustmentCost5 = p_web.GetSessionValue('tmp:AdjustmentCost5')
  End
  if p_web.IfExistsValue('tmp:ARCCost6') = 0
    p_web.SetSessionValue('tmp:ARCCost6',tmp:ARCCost6)
  Else
    tmp:ARCCost6 = p_web.GetSessionValue('tmp:ARCCost6')
  End
  if p_web.IfExistsValue('tmp:AdjustmentCost6') = 0
    p_web.SetSessionValue('tmp:AdjustmentCost6',tmp:AdjustmentCost6)
  Else
    tmp:AdjustmentCost6 = p_web.GetSessionValue('tmp:AdjustmentCost6')
  End
  if p_web.IfExistsValue('tmp:ARCCost7') = 0
    p_web.SetSessionValue('tmp:ARCCost7',tmp:ARCCost7)
  Else
    tmp:ARCCost7 = p_web.GetSessionValue('tmp:ARCCost7')
  End
  if p_web.IfExistsValue('tmp:ARCCost8') = 0
    p_web.SetSessionValue('tmp:ARCCost8',tmp:ARCCost8)
  Else
    tmp:ARCCost8 = p_web.GetSessionValue('tmp:ARCCost8')
  End
  if p_web.IfExistsValue('jobe:ARC3rdPartyInvoiceNumber') = 0
    p_web.SetSessionValue('jobe:ARC3rdPartyInvoiceNumber',jobe:ARC3rdPartyInvoiceNumber)
  Else
    jobe:ARC3rdPartyInvoiceNumber = p_web.GetSessionValue('jobe:ARC3rdPartyInvoiceNumber')
  End
  if p_web.IfExistsValue('tmp:ARCInvoiceNumber') = 0
    p_web.SetSessionValue('tmp:ARCInvoiceNumber',tmp:ARCInvoiceNumber)
  Else
    tmp:ARCInvoiceNumber = p_web.GetSessionValue('tmp:ARCInvoiceNumber')
  End
  if p_web.IfExistsValue('tmp:RRCViewCostType') = 0
    p_web.SetSessionValue('tmp:RRCViewCostType',tmp:RRCViewCostType)
  Else
    tmp:RRCViewCostType = p_web.GetSessionValue('tmp:RRCViewCostType')
  End
  if p_web.IfExistsValue('tmp:RRCIgnoreDefaultCharges') = 0
    p_web.SetSessionValue('tmp:RRCIgnoreDefaultCharges',tmp:RRCIgnoreDefaultCharges)
  Else
    tmp:RRCIgnoreDefaultCharges = p_web.GetSessionValue('tmp:RRCIgnoreDefaultCharges')
  End
  if p_web.IfExistsValue('tmp:RRCIgnoreReason') = 0
    p_web.SetSessionValue('tmp:RRCIgnoreReason',tmp:RRCIgnoreReason)
  Else
    tmp:RRCIgnoreReason = p_web.GetSessionValue('tmp:RRCIgnoreReason')
  End
  if p_web.IfExistsValue('tmp:RRCCost0') = 0
    p_web.SetSessionValue('tmp:RRCCost0',tmp:RRCCost0)
  Else
    tmp:RRCCost0 = p_web.GetSessionValue('tmp:RRCCost0')
  End
  if p_web.IfExistsValue('tmp:RRCCost1') = 0
    p_web.SetSessionValue('tmp:RRCCost1',tmp:RRCCost1)
  Else
    tmp:RRCCost1 = p_web.GetSessionValue('tmp:RRCCost1')
  End
  if p_web.IfExistsValue('tmp:originalInvoice') = 0
    p_web.SetSessionValue('tmp:originalInvoice',tmp:originalInvoice)
  Else
    tmp:originalInvoice = p_web.GetSessionValue('tmp:originalInvoice')
  End
  if p_web.IfExistsValue('tmp:RRCCost2') = 0
    p_web.SetSessionValue('tmp:RRCCost2',tmp:RRCCost2)
  Else
    tmp:RRCCost2 = p_web.GetSessionValue('tmp:RRCCost2')
  End
  if p_web.IfExistsValue('tmp:RRCCost3') = 0
    p_web.SetSessionValue('tmp:RRCCost3',tmp:RRCCost3)
  Else
    tmp:RRCCost3 = p_web.GetSessionValue('tmp:RRCCost3')
  End
  if p_web.IfExistsValue('tmp:RRCCost4') = 0
    p_web.SetSessionValue('tmp:RRCCost4',tmp:RRCCost4)
  Else
    tmp:RRCCost4 = p_web.GetSessionValue('tmp:RRCCost4')
  End
  if p_web.IfExistsValue('tmp:RRCCost5') = 0
    p_web.SetSessionValue('tmp:RRCCost5',tmp:RRCCost5)
  Else
    tmp:RRCCost5 = p_web.GetSessionValue('tmp:RRCCost5')
  End
  if p_web.IfExistsValue('tmp:RRCCost6') = 0
    p_web.SetSessionValue('tmp:RRCCost6',tmp:RRCCost6)
  Else
    tmp:RRCCost6 = p_web.GetSessionValue('tmp:RRCCost6')
  End
  if p_web.IfExistsValue('tmp:RRCCost7') = 0
    p_web.SetSessionValue('tmp:RRCCost7',tmp:RRCCost7)
  Else
    tmp:RRCCost7 = p_web.GetSessionValue('tmp:RRCCost7')
  End
  if p_web.IfExistsValue('tmp:RRCCost8') = 0
    p_web.SetSessionValue('tmp:RRCCost8',tmp:RRCCost8)
  Else
    tmp:RRCCost8 = p_web.GetSessionValue('tmp:RRCCost8')
  End
  if p_web.IfExistsValue('jobe:ExcReplcamentCharge') = 0
    p_web.SetSessionValue('jobe:ExcReplcamentCharge',jobe:ExcReplcamentCharge)
  Else
    jobe:ExcReplcamentCharge = p_web.GetSessionValue('jobe:ExcReplcamentCharge')
  End

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('tmp:ARCViewCostType')
    tmp:ARCViewCostType = p_web.GetValue('tmp:ARCViewCostType')
    p_web.SetSessionValue('tmp:ARCViewCostType',tmp:ARCViewCostType)
  Else
    tmp:ARCViewCostType = p_web.GetSessionValue('tmp:ARCViewCostType')
  End
  if p_web.IfExistsValue('tmp:ARCIgnoreDefaultCharges')
    tmp:ARCIgnoreDefaultCharges = p_web.GetValue('tmp:ARCIgnoreDefaultCharges')
    p_web.SetSessionValue('tmp:ARCIgnoreDefaultCharges',tmp:ARCIgnoreDefaultCharges)
  Else
    tmp:ARCIgnoreDefaultCharges = p_web.GetSessionValue('tmp:ARCIgnoreDefaultCharges')
  End
  if p_web.IfExistsValue('tmp:ARCIgnoreReason')
    tmp:ARCIgnoreReason = p_web.GetValue('tmp:ARCIgnoreReason')
    p_web.SetSessionValue('tmp:ARCIgnoreReason',tmp:ARCIgnoreReason)
  Else
    tmp:ARCIgnoreReason = p_web.GetSessionValue('tmp:ARCIgnoreReason')
  End
  if p_web.IfExistsValue('tmp:ARCCost1')
    tmp:ARCCost1 = p_web.dformat(clip(p_web.GetValue('tmp:ARCCost1')),'@n14.2')
    p_web.SetSessionValue('tmp:ARCCost1',tmp:ARCCost1)
  Else
    tmp:ARCCost1 = p_web.GetSessionValue('tmp:ARCCost1')
  End
  if p_web.IfExistsValue('tmp:AdjustmentCost1')
    tmp:AdjustmentCost1 = p_web.dformat(clip(p_web.GetValue('tmp:AdjustmentCost1')),'@n14.2')
    p_web.SetSessionValue('tmp:AdjustmentCost1',tmp:AdjustmentCost1)
  Else
    tmp:AdjustmentCost1 = p_web.GetSessionValue('tmp:AdjustmentCost1')
  End
  if p_web.IfExistsValue('tmp:ARCCost2')
    tmp:ARCCost2 = p_web.dformat(clip(p_web.GetValue('tmp:ARCCost2')),'@n14.2')
    p_web.SetSessionValue('tmp:ARCCost2',tmp:ARCCost2)
  Else
    tmp:ARCCost2 = p_web.GetSessionValue('tmp:ARCCost2')
  End
  if p_web.IfExistsValue('tmp:AdjustmentCost2')
    tmp:AdjustmentCost2 = p_web.dformat(clip(p_web.GetValue('tmp:AdjustmentCost2')),'@n14.2')
    p_web.SetSessionValue('tmp:AdjustmentCost2',tmp:AdjustmentCost2)
  Else
    tmp:AdjustmentCost2 = p_web.GetSessionValue('tmp:AdjustmentCost2')
  End
  if p_web.IfExistsValue('tmp:ARCCost3')
    tmp:ARCCost3 = p_web.dformat(clip(p_web.GetValue('tmp:ARCCost3')),'@n14.2')
    p_web.SetSessionValue('tmp:ARCCost3',tmp:ARCCost3)
  Else
    tmp:ARCCost3 = p_web.GetSessionValue('tmp:ARCCost3')
  End
  if p_web.IfExistsValue('tmp:AdjustmentCost3')
    tmp:AdjustmentCost3 = p_web.dformat(clip(p_web.GetValue('tmp:AdjustmentCost3')),'@n14.2')
    p_web.SetSessionValue('tmp:AdjustmentCost3',tmp:AdjustmentCost3)
  Else
    tmp:AdjustmentCost3 = p_web.GetSessionValue('tmp:AdjustmentCost3')
  End
  if p_web.IfExistsValue('tmp:ARCCost4')
    tmp:ARCCost4 = p_web.dformat(clip(p_web.GetValue('tmp:ARCCost4')),'@n14.2')
    p_web.SetSessionValue('tmp:ARCCost4',tmp:ARCCost4)
  Else
    tmp:ARCCost4 = p_web.GetSessionValue('tmp:ARCCost4')
  End
  if p_web.IfExistsValue('tmp:AdjustmentCost4')
    tmp:AdjustmentCost4 = p_web.dformat(clip(p_web.GetValue('tmp:AdjustmentCost4')),'@n14.2')
    p_web.SetSessionValue('tmp:AdjustmentCost4',tmp:AdjustmentCost4)
  Else
    tmp:AdjustmentCost4 = p_web.GetSessionValue('tmp:AdjustmentCost4')
  End
  if p_web.IfExistsValue('tmp:ARCCost5')
    tmp:ARCCost5 = p_web.dformat(clip(p_web.GetValue('tmp:ARCCost5')),'@n14.2')
    p_web.SetSessionValue('tmp:ARCCost5',tmp:ARCCost5)
  Else
    tmp:ARCCost5 = p_web.GetSessionValue('tmp:ARCCost5')
  End
  if p_web.IfExistsValue('tmp:AdjustmentCost5')
    tmp:AdjustmentCost5 = p_web.dformat(clip(p_web.GetValue('tmp:AdjustmentCost5')),'@n14.2')
    p_web.SetSessionValue('tmp:AdjustmentCost5',tmp:AdjustmentCost5)
  Else
    tmp:AdjustmentCost5 = p_web.GetSessionValue('tmp:AdjustmentCost5')
  End
  if p_web.IfExistsValue('tmp:ARCCost6')
    tmp:ARCCost6 = p_web.dformat(clip(p_web.GetValue('tmp:ARCCost6')),'@n14.2')
    p_web.SetSessionValue('tmp:ARCCost6',tmp:ARCCost6)
  Else
    tmp:ARCCost6 = p_web.GetSessionValue('tmp:ARCCost6')
  End
  if p_web.IfExistsValue('tmp:AdjustmentCost6')
    tmp:AdjustmentCost6 = p_web.dformat(clip(p_web.GetValue('tmp:AdjustmentCost6')),'@n14.2')
    p_web.SetSessionValue('tmp:AdjustmentCost6',tmp:AdjustmentCost6)
  Else
    tmp:AdjustmentCost6 = p_web.GetSessionValue('tmp:AdjustmentCost6')
  End
  if p_web.IfExistsValue('tmp:ARCCost7')
    tmp:ARCCost7 = p_web.dformat(clip(p_web.GetValue('tmp:ARCCost7')),'@n14.2')
    p_web.SetSessionValue('tmp:ARCCost7',tmp:ARCCost7)
  Else
    tmp:ARCCost7 = p_web.GetSessionValue('tmp:ARCCost7')
  End
  if p_web.IfExistsValue('tmp:ARCCost8')
    tmp:ARCCost8 = p_web.dformat(clip(p_web.GetValue('tmp:ARCCost8')),'@n14.2')
    p_web.SetSessionValue('tmp:ARCCost8',tmp:ARCCost8)
  Else
    tmp:ARCCost8 = p_web.GetSessionValue('tmp:ARCCost8')
  End
  if p_web.IfExistsValue('jobe:ARC3rdPartyInvoiceNumber')
    jobe:ARC3rdPartyInvoiceNumber = p_web.GetValue('jobe:ARC3rdPartyInvoiceNumber')
    p_web.SetSessionValue('jobe:ARC3rdPartyInvoiceNumber',jobe:ARC3rdPartyInvoiceNumber)
  Else
    jobe:ARC3rdPartyInvoiceNumber = p_web.GetSessionValue('jobe:ARC3rdPartyInvoiceNumber')
  End
  if p_web.IfExistsValue('tmp:ARCInvoiceNumber')
    tmp:ARCInvoiceNumber = p_web.GetValue('tmp:ARCInvoiceNumber')
    p_web.SetSessionValue('tmp:ARCInvoiceNumber',tmp:ARCInvoiceNumber)
  Else
    tmp:ARCInvoiceNumber = p_web.GetSessionValue('tmp:ARCInvoiceNumber')
  End
  if p_web.IfExistsValue('tmp:RRCViewCostType')
    tmp:RRCViewCostType = p_web.GetValue('tmp:RRCViewCostType')
    p_web.SetSessionValue('tmp:RRCViewCostType',tmp:RRCViewCostType)
  Else
    tmp:RRCViewCostType = p_web.GetSessionValue('tmp:RRCViewCostType')
  End
  if p_web.IfExistsValue('tmp:RRCIgnoreDefaultCharges')
    tmp:RRCIgnoreDefaultCharges = p_web.GetValue('tmp:RRCIgnoreDefaultCharges')
    p_web.SetSessionValue('tmp:RRCIgnoreDefaultCharges',tmp:RRCIgnoreDefaultCharges)
  Else
    tmp:RRCIgnoreDefaultCharges = p_web.GetSessionValue('tmp:RRCIgnoreDefaultCharges')
  End
  if p_web.IfExistsValue('tmp:RRCIgnoreReason')
    tmp:RRCIgnoreReason = p_web.GetValue('tmp:RRCIgnoreReason')
    p_web.SetSessionValue('tmp:RRCIgnoreReason',tmp:RRCIgnoreReason)
  Else
    tmp:RRCIgnoreReason = p_web.GetSessionValue('tmp:RRCIgnoreReason')
  End
  if p_web.IfExistsValue('tmp:RRCCost0')
    tmp:RRCCost0 = p_web.dformat(clip(p_web.GetValue('tmp:RRCCost0')),'@n14.2')
    p_web.SetSessionValue('tmp:RRCCost0',tmp:RRCCost0)
  Else
    tmp:RRCCost0 = p_web.GetSessionValue('tmp:RRCCost0')
  End
  if p_web.IfExistsValue('tmp:RRCCost1')
    tmp:RRCCost1 = p_web.dformat(clip(p_web.GetValue('tmp:RRCCost1')),'@n14.2')
    p_web.SetSessionValue('tmp:RRCCost1',tmp:RRCCost1)
  Else
    tmp:RRCCost1 = p_web.GetSessionValue('tmp:RRCCost1')
  End
  if p_web.IfExistsValue('tmp:originalInvoice')
    tmp:originalInvoice = p_web.GetValue('tmp:originalInvoice')
    p_web.SetSessionValue('tmp:originalInvoice',tmp:originalInvoice)
  Else
    tmp:originalInvoice = p_web.GetSessionValue('tmp:originalInvoice')
  End
  if p_web.IfExistsValue('tmp:RRCCost2')
    tmp:RRCCost2 = p_web.dformat(clip(p_web.GetValue('tmp:RRCCost2')),'@n14.2')
    p_web.SetSessionValue('tmp:RRCCost2',tmp:RRCCost2)
  Else
    tmp:RRCCost2 = p_web.GetSessionValue('tmp:RRCCost2')
  End
  if p_web.IfExistsValue('tmp:RRCCost3')
    tmp:RRCCost3 = p_web.dformat(clip(p_web.GetValue('tmp:RRCCost3')),'@n14.2')
    p_web.SetSessionValue('tmp:RRCCost3',tmp:RRCCost3)
  Else
    tmp:RRCCost3 = p_web.GetSessionValue('tmp:RRCCost3')
  End
  if p_web.IfExistsValue('tmp:RRCCost4')
    tmp:RRCCost4 = p_web.dformat(clip(p_web.GetValue('tmp:RRCCost4')),'@n14.2')
    p_web.SetSessionValue('tmp:RRCCost4',tmp:RRCCost4)
  Else
    tmp:RRCCost4 = p_web.GetSessionValue('tmp:RRCCost4')
  End
  if p_web.IfExistsValue('tmp:RRCCost5')
    tmp:RRCCost5 = p_web.dformat(clip(p_web.GetValue('tmp:RRCCost5')),'@n14.2')
    p_web.SetSessionValue('tmp:RRCCost5',tmp:RRCCost5)
  Else
    tmp:RRCCost5 = p_web.GetSessionValue('tmp:RRCCost5')
  End
  if p_web.IfExistsValue('tmp:RRCCost6')
    tmp:RRCCost6 = p_web.dformat(clip(p_web.GetValue('tmp:RRCCost6')),'@n14.2')
    p_web.SetSessionValue('tmp:RRCCost6',tmp:RRCCost6)
  Else
    tmp:RRCCost6 = p_web.GetSessionValue('tmp:RRCCost6')
  End
  if p_web.IfExistsValue('tmp:RRCCost7')
    tmp:RRCCost7 = p_web.dformat(clip(p_web.GetValue('tmp:RRCCost7')),'@n14.2')
    p_web.SetSessionValue('tmp:RRCCost7',tmp:RRCCost7)
  Else
    tmp:RRCCost7 = p_web.GetSessionValue('tmp:RRCCost7')
  End
  if p_web.IfExistsValue('tmp:RRCCost8')
    tmp:RRCCost8 = p_web.dformat(clip(p_web.GetValue('tmp:RRCCost8')),'@n14.2')
    p_web.SetSessionValue('tmp:RRCCost8',tmp:RRCCost8)
  Else
    tmp:RRCCost8 = p_web.GetSessionValue('tmp:RRCCost8')
  End
  if p_web.IfExistsValue('jobe:ExcReplcamentCharge')
    jobe:ExcReplcamentCharge = p_web.GetValue('jobe:ExcReplcamentCharge')
    p_web.SetSessionValue('jobe:ExcReplcamentCharge',jobe:ExcReplcamentCharge)
  Else
    jobe:ExcReplcamentCharge = p_web.GetSessionValue('jobe:ExcReplcamentCharge')
  End

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('ViewCosts_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.GSV('ViewCostsReturnURL')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('ViewCosts_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('ViewCosts_ChainTo')
    loc:formaction = p_web.GetSessionValue('ViewCosts_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = p_web.GSV('ViewCostsReturnURL')

GenerateForm   Routine
  do LoadRelatedRecords
 tmp:ARCViewCostType = p_web.RestoreValue('tmp:ARCViewCostType')
 tmp:ARCIgnoreDefaultCharges = p_web.RestoreValue('tmp:ARCIgnoreDefaultCharges')
 tmp:ARCIgnoreReason = p_web.RestoreValue('tmp:ARCIgnoreReason')
 tmp:ARCCost1 = p_web.RestoreValue('tmp:ARCCost1')
 tmp:AdjustmentCost1 = p_web.RestoreValue('tmp:AdjustmentCost1')
 tmp:ARCCost2 = p_web.RestoreValue('tmp:ARCCost2')
 tmp:AdjustmentCost2 = p_web.RestoreValue('tmp:AdjustmentCost2')
 tmp:ARCCost3 = p_web.RestoreValue('tmp:ARCCost3')
 tmp:AdjustmentCost3 = p_web.RestoreValue('tmp:AdjustmentCost3')
 tmp:ARCCost4 = p_web.RestoreValue('tmp:ARCCost4')
 tmp:AdjustmentCost4 = p_web.RestoreValue('tmp:AdjustmentCost4')
 tmp:ARCCost5 = p_web.RestoreValue('tmp:ARCCost5')
 tmp:AdjustmentCost5 = p_web.RestoreValue('tmp:AdjustmentCost5')
 tmp:ARCCost6 = p_web.RestoreValue('tmp:ARCCost6')
 tmp:AdjustmentCost6 = p_web.RestoreValue('tmp:AdjustmentCost6')
 tmp:ARCCost7 = p_web.RestoreValue('tmp:ARCCost7')
 tmp:ARCCost8 = p_web.RestoreValue('tmp:ARCCost8')
 tmp:ARCInvoiceNumber = p_web.RestoreValue('tmp:ARCInvoiceNumber')
 tmp:RRCViewCostType = p_web.RestoreValue('tmp:RRCViewCostType')
 tmp:RRCIgnoreDefaultCharges = p_web.RestoreValue('tmp:RRCIgnoreDefaultCharges')
 tmp:RRCIgnoreReason = p_web.RestoreValue('tmp:RRCIgnoreReason')
 tmp:RRCCost0 = p_web.RestoreValue('tmp:RRCCost0')
 tmp:RRCCost1 = p_web.RestoreValue('tmp:RRCCost1')
 tmp:originalInvoice = p_web.RestoreValue('tmp:originalInvoice')
 tmp:RRCCost2 = p_web.RestoreValue('tmp:RRCCost2')
 tmp:RRCCost3 = p_web.RestoreValue('tmp:RRCCost3')
 tmp:RRCCost4 = p_web.RestoreValue('tmp:RRCCost4')
 tmp:RRCCost5 = p_web.RestoreValue('tmp:RRCCost5')
 tmp:RRCCost6 = p_web.RestoreValue('tmp:RRCCost6')
 tmp:RRCCost7 = p_web.RestoreValue('tmp:RRCCost7')
 tmp:RRCCost8 = p_web.RestoreValue('tmp:RRCCost8')
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
    loc:viewonly = Choose(p_web.GSV('Job:ViewOnly') = 1,1,0)
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('View Costs') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('View Costs',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_ViewCosts',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      If p_web.GSV('Hide:ARCCosts') <> 1
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_ViewCosts0_div')&'">'&p_web.Translate('ARC Costs')&'</a></li>'& CRLF
      End
      If p_web.GSV('Hide:RRCCosts') <> 1
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_ViewCosts1_div')&'">'&p_web.Translate('RRC Costs')&'</a></li>'& CRLF
      End
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_ViewCosts2_div')&'">'&p_web.Translate()&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="ViewCosts_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('ViewCosts_BrowseJobCredits_embedded_div')&''');'
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('ViewCosts_BrowseJobCredits_embedded_div')&''');'
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.cancel();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CancelButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="ViewCosts_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'ViewCosts_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="ViewCosts_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('ViewCosts_BrowseJobCredits_embedded_div')&''');'
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'ViewCosts_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    ElsIf p_web.GSV('Hide:ARCCosts') <> 1
        p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:ARCViewCostType')
    ElsIf p_web.GSV('Hide:RRCCosts') <> 1
        p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:RRCViewCostType')
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.jobe:ExcReplcamentCharge')
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_ViewCosts')>0,p_web.GSV('showtab_ViewCosts'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_ViewCosts'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_ViewCosts') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_ViewCosts'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_ViewCosts')>0,p_web.GSV('showtab_ViewCosts'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_ViewCosts') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
      If p_web.GSV('Hide:ARCCosts') <> 1
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('ARC Costs') & ''''
      End
      If p_web.GSV('Hide:RRCCosts') <> 1
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('RRC Costs') & ''''
      End
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_ViewCosts_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_ViewCosts')>0,p_web.GSV('showtab_ViewCosts'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"ViewCosts",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_ViewCosts')>0,p_web.GSV('showtab_ViewCosts'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_ViewCosts_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('ViewCosts') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('ViewCosts')
    p_web.SetValue('_CallPopups',2)
    If p_web.GetPreCall('BrowseJobCredits') = 0
      p_web.SetValue('BrowseJobCredits:NoForm',1)
      p_web.SetValue('BrowseJobCredits:FormName',loc:formname)
      p_web.SetValue('BrowseJobCredits:parentIs','Form')
      p_web.SetValue('_parentProc','ViewCosts')
      BrowseJobCredits(p_web)
      p_web.SetValue('_CallPopups',0)
      p_web.DeleteValue('BrowseJobCredits:NoForm')
      p_web.DeleteValue('BrowseJobCredits:FormName')
      p_web.DeleteValue('BrowseJobCredits:parentIs')
      p_web.DeleteValue('_parentProc')
    End
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
  If p_web.GSV('Hide:ARCCosts') <> 1
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('ARC Costs')&'</a></h3>' & CRLF & p_web.DivHeader('tab_ViewCosts0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewCosts0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewCosts0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewCosts0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'ARC Costs')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewCosts0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('ARC Costs')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewCosts0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('ARC Costs')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewCosts0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:ARCViewCostType
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:ARCViewCostType
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:ARCViewCostType
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::__line1
        do Comment::__line1
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:ARCIgnoreDefaultCharges
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:ARCIgnoreDefaultCharges
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:ARCIgnoreDefaultCharges
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:ARCIgnoreReason
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:ARCIgnoreReason
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:ARCIgnoreReason
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::buttonAcceptARCReason
        do Comment::buttonAcceptARCReason
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::buttonCancelARCReason
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::buttonCancelARCReason
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:ARCCost1
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:ARCCost1
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:ARCCost1
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:AdjustmentCost1
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:AdjustmentCost1
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:ARCCost2
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:ARCCost2
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:ARCCost2
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:AdjustmentCost2
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:AdjustmentCost2
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:ARCCost3
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:ARCCost3
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:ARCCost3
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:AdjustmentCost3
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:AdjustmentCost3
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::__line2
        do Comment::__line2
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:ARCCost4
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:ARCCost4
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:ARCCost4
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:AdjustmentCost4
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:AdjustmentCost4
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:ARCCost5
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:ARCCost5
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:ARCCost5
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:AdjustmentCost5
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:AdjustmentCost5
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:ARCCost6
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:ARCCost6
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:ARCCost6
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:AdjustmentCost6
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:AdjustmentCost6
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:ARCCost7
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:ARCCost7
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:ARCCost7
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:ARCCost8
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:ARCCost8
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:ARCCost8
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jobe:ARC3rdPartyInvoiceNumber
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jobe:ARC3rdPartyInvoiceNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::jobe:ARC3rdPartyInvoiceNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:ARCInvoiceNumber
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:ARCInvoiceNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:ARCInvoiceNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
  end
GenerateTab1  Routine
  If p_web.GSV('Hide:RRCCosts') <> 1
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('RRC Costs')&'</a></h3>' & CRLF & p_web.DivHeader('tab_ViewCosts1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewCosts1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewCosts1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewCosts1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'RRC Costs')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewCosts1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('RRC Costs')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewCosts1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('RRC Costs')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewCosts1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:RRCViewCostType
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:RRCViewCostType
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:RRCViewCostType
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::__line3
        do Comment::__line3
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:RRCIgnoreDefaultCharges
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:RRCIgnoreDefaultCharges
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:RRCIgnoreDefaultCharges
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:RRCIgnoreReason
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:RRCIgnoreReason
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:RRCIgnoreReason
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::buttonAcceptRRCReason
        do Comment::buttonAcceptRRCReason
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::buttonCancelRRCReason
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::buttonCancelRRCReason
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:RRCCost0
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:RRCCost0
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:RRCCost0
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:RRCCost1
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:RRCCost1
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:RRCCost1
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:originalInvoice
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:originalInvoice
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:originalInvoice
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:RRCCost2
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:RRCCost2
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:RRCCost2
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:RRCCost3
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:RRCCost3
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:RRCCost3
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::BrowseJobCredits
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::__line4
        do Comment::__line4
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:RRCCost4
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:RRCCost4
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:RRCCost4
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:RRCCost5
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:RRCCost5
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:RRCCost5
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:RRCCost6
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:RRCCost6
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:RRCCost6
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:RRCCost7
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:RRCCost7
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:RRCCost7
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:RRCCost8
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:RRCCost8
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:RRCCost8
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
  end
GenerateTab2  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate()&'</a></h3>' & CRLF & p_web.DivHeader('tab_ViewCosts2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewCosts2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewCosts2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewCosts2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,)
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewCosts2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend)
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewCosts2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain">' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewCosts2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jobe:ExcReplcamentCharge
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&180&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jobe:ExcReplcamentCharge
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::jobe:ExcReplcamentCharge
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Prompt::tmp:ARCViewCostType  Routine
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCViewCostType') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('View Cost'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:ARCViewCostType  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:ARCViewCostType = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:ARCViewCostType = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:ARCViewCostType  ! copies value to session value if valid.
  do displayARCCostFields
  do displayOtherFields
  do Value::tmp:ARCViewCostType
  do SendAlert
  do Comment::tmp:ARCViewCostType ! allows comment style to be updated.
  do Prompt::tmp:ARCCost1
  do Value::tmp:ARCCost1  !1
  do Comment::tmp:ARCCost1
  do Prompt::tmp:ARCCost2
  do Value::tmp:ARCCost2  !1
  do Comment::tmp:ARCCost2
  do Prompt::tmp:ARCCost3
  do Value::tmp:ARCCost3  !1
  do Comment::tmp:ARCCost3
  do Prompt::tmp:ARCCost4
  do Value::tmp:ARCCost4  !1
  do Comment::tmp:ARCCost4
  do Prompt::tmp:ARCCost5
  do Value::tmp:ARCCost5  !1
  do Comment::tmp:ARCCost5
  do Prompt::tmp:ARCCost6
  do Value::tmp:ARCCost6  !1
  do Comment::tmp:ARCCost6
  do Prompt::tmp:ARCCost7
  do Value::tmp:ARCCost7  !1
  do Comment::tmp:ARCCost7
  do Prompt::tmp:ARCCost8
  do Value::tmp:ARCCost8  !1
  do Comment::tmp:ARCCost8
  do Value::tmp:AdjustmentCost1  !1
  do Value::tmp:AdjustmentCost2  !1
  do Value::tmp:AdjustmentCost3  !1
  do Value::tmp:AdjustmentCost4  !1
  do Value::tmp:AdjustmentCost5  !1
  do Value::tmp:AdjustmentCost6  !1
  do Prompt::jobe:ExcReplcamentCharge
  do Value::jobe:ExcReplcamentCharge  !1
  do Prompt::jobe:ARC3rdPartyInvoiceNumber
  do Value::jobe:ARC3rdPartyInvoiceNumber  !1
  do Prompt::tmp:ARCIgnoreDefaultCharges
  do Value::tmp:ARCIgnoreDefaultCharges  !1

ValidateValue::tmp:ARCViewCostType  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:ARCViewCostType',tmp:ARCViewCostType).
    End

Value::tmp:ARCViewCostType  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCViewCostType') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,p_web.site.style.formselect,'FormEntry',)
  If loc:retrying
    tmp:ARCViewCostType = p_web.RestoreValue('tmp:ARCViewCostType')
    do ValidateValue::tmp:ARCViewCostType
    If tmp:ARCViewCostType:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- DROPLIST ---
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ARCViewCostType'',''viewcosts_tmp:arcviewcosttype_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:ARCViewCostType')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:ARCViewCostType',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
  loc:fieldclass = ''
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('tmp:ARCViewCostType') = 0
    p_web.SetSessionValue('tmp:ARCViewCostType','Chargeable')
  end
  If p_web.GSV('Hide:ARCChargeable') <> 1
    packet = clip(packet) & p_web.CreateOption('Chargeable','Chargeable',choose('Chargeable' = p_web.getsessionvalue('tmp:ARCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:ARCWarranty') <> 1
    packet = clip(packet) & p_web.CreateOption('Warranty','Warranty',choose('Warranty' = p_web.getsessionvalue('tmp:ARCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:ARCClaim') <> 1
    packet = clip(packet) & p_web.CreateOption('Claim','Claim',choose('Claim' = p_web.getsessionvalue('tmp:ARCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:ARC3rdParty') <> 1
    packet = clip(packet) & p_web.CreateOption('3rd Party','3rd Party',choose('3rd Party' = p_web.getsessionvalue('tmp:ARCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:ARCEstimate') <> 1
    packet = clip(packet) & p_web.CreateOption('Estimate','Estimate',choose('Estimate' = p_web.getsessionvalue('tmp:ARCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:ARCCInvoice') <> 1
    packet = clip(packet) & p_web.CreateOption('Chargeable - Invoiced','Chargeable - Invoiced',choose('Chargeable - Invoiced' = p_web.getsessionvalue('tmp:ARCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:ARCWInvoice') <> 1
    packet = clip(packet) & p_web.CreateOption('Warranty - Invoiced','Warranty - Invoiced',choose('Warranty - Invoiced' = p_web.getsessionvalue('tmp:ARCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:ARCClaimInvoice') <> 1
    packet = clip(packet) & p_web.CreateOption('Claim - Invoiced','Claim - Invoiced',choose('Claim - Invoiced' = p_web.getsessionvalue('tmp:ARCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:ManufacturerPaid') <> 1
    packet = clip(packet) & p_web.CreateOption('Manufacturer Payment','Manufacturer Payment',choose('Manufacturer Payment' = p_web.getsessionvalue('tmp:ARCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:ARCViewCostType  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:ARCViewCostType:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCViewCostType') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::__line1  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::__line1  ! copies value to session value if valid.
  do Comment::__line1 ! allows comment style to be updated.

ValidateValue::__line1  Routine
    If not (1=0)
    End

Value::__line1  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine('nt-width-100')
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('__line1') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & '<hr></hr><13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::__line1  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if __line1:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('__line1') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:ARCIgnoreDefaultCharges  Routine
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCIgnoreDefaultCharges') & '_prompt',Choose(p_web.GSV('tmp:ARCViewCostType') = 'Manufacturer Payment' or p_web.GSV('tmp:ARCViewCostType') = '','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('tmp:ARCViewCostType') = 'Manufacturer Payment' or p_web.GSV('tmp:ARCViewCostType') = '','',p_web.Translate('Ignore Default Charges'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:ARCIgnoreDefaultCharges  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:ARCIgnoreDefaultCharges = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    tmp:ARCIgnoreDefaultCharges = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:ARCIgnoreDefaultCharges  ! copies value to session value if valid.
  if (p_web.GSV('tmp:ARCIgnoreDefaultCharges') = 1)
      p_web.SSV('ValidateIgnoreTickBoxARC',1)
      p_web.SSV('tmp:ARCIgnoreReason','')
  else  ! if (p_web.GSV('tmp:RRCIgnoreDefaultCharges') = 1)
      p_web.SSV('ReadOnly:ARCCost2',1)
      case p_web.GSV('tmp:ARCViewCostType')
      of 'Chargeable'
          p_web.SSV('job:Ignore_Chargeable_Charges','NO')
      of 'Warranty'
          p_web.SSV('job:Ignore_Warranty_Charges','NO')
      of 'Estimate'
          p_web.SSV('job:Ignore_Estimate_Charges','NO')
      end ! case p_web.GSV('tmp:ARCViewCostType')
      do pricingRoutine
  end ! if (p_web.GSV('tmp:RRCIgnoreDefaultCharges') = 1)
  do Value::tmp:ARCIgnoreDefaultCharges
  do SendAlert
  do Comment::tmp:ARCIgnoreDefaultCharges ! allows comment style to be updated.
  do Value::buttonAcceptARCReason  !1
  do Value::buttonCancelARCReason  !1
  do Value::tmp:ARCViewCostType  !1
  do Prompt::tmp:ARCIgnoreReason
  do Value::tmp:ARCIgnoreReason  !1
  do Value::tmp:ARCCost2  !1
  do Value::tmp:ARCCost3  !1
  do Value::tmp:ARCCost4  !1
  do Value::tmp:ARCCost5  !1
  do Value::tmp:ARCCost6  !1
  do Value::tmp:ARCCost7  !1
  do Value::tmp:ARCCost8  !1

ValidateValue::tmp:ARCIgnoreDefaultCharges  Routine
    If not (p_web.GSV('tmp:ARCViewCostType') = 'Manufacturer Payment' or p_web.GSV('tmp:ARCViewCostType') = '')
      if loc:invalid = '' then p_web.SetSessionValue('tmp:ARCIgnoreDefaultCharges',tmp:ARCIgnoreDefaultCharges).
    End

Value::tmp:ARCIgnoreDefaultCharges  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('tmp:ARCViewCostType') = 'Manufacturer Payment' or p_web.GSV('tmp:ARCViewCostType') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCIgnoreDefaultCharges') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If loc:retrying
    tmp:ARCIgnoreDefaultCharges = p_web.RestoreValue('tmp:ARCIgnoreDefaultCharges')
    do ValidateValue::tmp:ARCIgnoreDefaultCharges
    If tmp:ARCIgnoreDefaultCharges:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('tmp:ARCViewCostType') = 'Manufacturer Payment' or p_web.GSV('tmp:ARCViewCostType') = '')
  ! --- CHECKBOX --- tmp:ARCIgnoreDefaultCharges
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:ARCIgnoreDefaultCharges'',''viewcosts_tmp:arcignoredefaultcharges_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:ARCIgnoreDefaultCharges')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('BookingSite') = 'RRC' Or Instring('- Invoiced',p_web.GSV('tmp:ARCViewCostType'),1,1) or p_web.GSV('ValidateIgnoreTickBoxARC') = 1,'disabled','')
  If p_web.GetSessionValue('tmp:ARCIgnoreDefaultCharges') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','tmp:ARCIgnoreDefaultCharges',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:ARCIgnoreDefaultCharges  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:ARCIgnoreDefaultCharges:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('tmp:ARCViewCostType') = 'Manufacturer Payment' or p_web.GSV('tmp:ARCViewCostType') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCIgnoreDefaultCharges') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('tmp:ARCViewCostType') = 'Manufacturer Payment' or p_web.GSV('tmp:ARCViewCostType') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:ARCIgnoreReason  Routine
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCIgnoreReason') & '_prompt',Choose(p_web.GSV('ValidateIgnoreTickBoxARC') <> 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('ValidateIgnoreTickBoxARC') <> 1,'',p_web.Translate('Enter Reason For Ignoring Standard Charges'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:ARCIgnoreReason  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:ARCIgnoreReason = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:ARCIgnoreReason = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:ARCIgnoreReason  ! copies value to session value if valid.
  p_web.SSV('tmp:ARCIgnoreReason',BHStripReplace(p_web.GSV('tmp:ARCIgnoreReason'),'<9>',''))
  do Value::tmp:ARCIgnoreReason
  do SendAlert
  do Comment::tmp:ARCIgnoreReason ! allows comment style to be updated.

ValidateValue::tmp:ARCIgnoreReason  Routine
    If not (p_web.GSV('ValidateIgnoreTickBoxARC') <> 1)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:ARCIgnoreReason',tmp:ARCIgnoreReason).
    End

Value::tmp:ARCIgnoreReason  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('ValidateIgnoreTickBoxARC') <> 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCIgnoreReason') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    tmp:ARCIgnoreReason = p_web.RestoreValue('tmp:ARCIgnoreReason')
    do ValidateValue::tmp:ARCIgnoreReason
    If tmp:ARCIgnoreReason:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('ValidateIgnoreTickBoxARC') <> 1)
  ! --- TEXT --- tmp:ARCIgnoreReason
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ARCIgnoreReason'',''viewcosts_tmp:arcignorereason_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:ARCIgnoreReason')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'readonly','')
  do SendPacket
  p_web.CreateTextArea('tmp:ARCIgnoreReason',p_web.GetSessionValue('tmp:ARCIgnoreReason'),5,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(tmp:ARCIgnoreReason),,,,Net:Send)
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:ARCIgnoreReason  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:ARCIgnoreReason:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('ValidateIgnoreTickBoxARC') <> 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCIgnoreReason') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('ValidateIgnoreTickBoxARC') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonAcceptARCReason  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonAcceptARCReason  ! copies value to session value if valid.
  if (p_web.GSV('tmp:ARCIgnoreReason') <> '')
      p_web.SSV('ValidateIgnoreTickBoxARC',0)
      case p_web.GSV('tmp:ARCViewCostType')
      of 'Chargeable'
          local.AddToTempQueue('IGNORE DEFAULT ARC CHARGEABLE COSTS',|
                          p_web.GSV('job:Ignore_Chargeable_Charges'), |
                          p_web.GSV('tmp:ARCIgnoreReason'),1,|
                          p_web.GSV('tmp:ARCCost2'))
          p_web.SSV(job:Ignore_Chargeable_Charges,'YES')
      of 'Warranty'
          local.AddToTempQueue('IGNORE DEFAULT ARC WARRANTY COSTS',|
                          p_web.GSV('job:Ignore_Warranty_Charges'),|
                          p_web.GSV('tmp:ARCIgnoreReason'),1,|
                          p_web.GSV('tmp:ARCCost2'))
          p_web.SSV('job:Ignore_Warranty_Charges','YES')
      of 'Estimate'
          local.AddToTempQueue('IGNORE DEFAULT RRC ESTIMATE COSTS',|
                          p_web.GSV('job:Ignore_Estimate_Charges'),|
                          p_web.GSV('tmp:ARCIgnoreReason'),1,|
                          p_web.GSV('tmp:ARCCost2'))
          p_web.SSV('job:Ignore_Estimate_Charges','YES')
      end ! case p_web.GSV('tmp:ARCViewCostType')
      p_web.SSV('ReadOnly:ARCCost2',0)
  else ! if (p_web.GSV('tmp:RRCIgnoreReason') <> '')
      p_web.SSV('ReadOnly:ARCCost2',1)
      case p_web.GSV('tmp:ARCViewCostType')
      of 'Chargeable'
          p_web.SSV('job:Ignore_Chargeable_Charges','YES')
      of 'Warranty'
          p_web.SSV('job:Ignore_Warranty_Charges','YES')
      of 'Estimate'
          p_web.SSV('job:Ignore_Estimate_Charges','YES')
      end ! case p_web.GSV('tmp:ARCViewCostType')
      p_web.SSV('tmp:ARCIgnoreDefaultCharges',0)
  end ! if (p_web.GSV('tmp:RRCIgnoreReason') <> '')
  do Value::buttonAcceptARCReason
  do Comment::buttonAcceptARCReason ! allows comment style to be updated.
  do Value::buttonCancelARCReason  !1
  do Prompt::tmp:ARCIgnoreDefaultCharges
  do Value::tmp:ARCIgnoreDefaultCharges  !1
  do Prompt::tmp:ARCIgnoreReason
  do Value::tmp:ARCIgnoreReason  !1
  do Prompt::tmp:ARCCost2
  do Value::tmp:ARCCost2  !1
  do Value::tmp:ARCViewCostType  !1

ValidateValue::buttonAcceptARCReason  Routine
    If not (p_web.GSV('ValidateIgnoreTickBoxARC') <> 1)
    End

Value::buttonAcceptARCReason  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('ValidateIgnoreTickBoxARC') <> 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('buttonAcceptARCReason') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (p_web.GSV('ValidateIgnoreTickBoxARC') <> 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonAcceptARCReason'',''viewcosts_buttonacceptarcreason_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','AcceptARCReason','Accept',p_web.combine(Choose('Accept' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'SmallButton'),loc:formname,,,,loc:javascript,loc:disabled,,,,,,,,,,'nt-left')!h
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonAcceptARCReason  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonAcceptARCReason:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('ValidateIgnoreTickBoxARC') <> 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('buttonAcceptARCReason') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('ValidateIgnoreTickBoxARC') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonCancelARCReason  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonCancelARCReason  ! copies value to session value if valid.
  p_web.SSV('ValidateIgnoreTickBoxARC',0)
  p_web.SSV('ReadOnly:ARCCost2',1)
  case p_web.GSV('tmp:ARCViewCostType')
  of 'Chargeable'
      p_web.SSV('job:Ignore_Chargeable_Charges','NO')
  of 'Warranty'
      p_web.SSV('job:Ignore_Warranty_Charges','NO')
  of 'Estimate'
      p_web.SSV('job:Ignore_Warranty_Charges','NO')
  end ! case p_web.GSV('tmp:ARCViewCostType')
  p_web.SSV('tmp:ARCIgnoreDefaultCharges',0)
  do Value::buttonCancelARCReason
  do Comment::buttonCancelARCReason ! allows comment style to be updated.
  do Value::buttonAcceptARCReason  !1
  do Prompt::tmp:ARCIgnoreDefaultCharges
  do Value::tmp:ARCIgnoreDefaultCharges  !1
  do Prompt::tmp:ARCIgnoreReason
  do Value::tmp:ARCIgnoreReason  !1
  do Prompt::tmp:ARCCost2
  do Value::tmp:ARCCost2  !1
  do Prompt::tmp:ARCViewCostType
  do Value::tmp:ARCViewCostType  !1

ValidateValue::buttonCancelARCReason  Routine
    If not (p_web.GSV('ValidateIgnoreTickBoxARC') <> 1)
    End

Value::buttonCancelARCReason  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('ValidateIgnoreTickBoxARC') <> 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('buttonCancelARCReason') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (p_web.GSV('ValidateIgnoreTickBoxARC') <> 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonCancelARCReason'',''viewcosts_buttoncancelarcreason_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','CancelARCReason','Cancel',p_web.combine(Choose('Cancel' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'SmallButton'),loc:formname,,,,loc:javascript,loc:disabled,,,,,,,,,,'nt-left')!h
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonCancelARCReason  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonCancelARCReason:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('ValidateIgnoreTickBoxARC') <> 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('buttonCancelARCReason') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('ValidateIgnoreTickBoxARC') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:ARCCost1  Routine
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost1') & '_prompt',Choose(p_web.GSV('Prompt:Cost1') = '','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Prompt:Cost1') = '','',p_web.Translate(p_web.GSV('Prompt:Cost1')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:ARCCost1  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:ARCCost1 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    tmp:ARCCost1 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::tmp:ARCCost1  ! copies value to session value if valid.
  do Value::tmp:ARCCost1
  do SendAlert
  do Comment::tmp:ARCCost1 ! allows comment style to be updated.

ValidateValue::tmp:ARCCost1  Routine
    If not (p_web.GSV('Prompt:Cost1') = '')
      if loc:invalid = '' then p_web.SetSessionValue('tmp:ARCCost1',tmp:ARCCost1).
    End

Value::tmp:ARCCost1  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Prompt:Cost1') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost1') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If p_web.GSV('BookingSite') = 'RRC' Or p_web.GSV('ReadOnly:ARCCost1') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    tmp:ARCCost1 = p_web.RestoreValue('tmp:ARCCost1')
    do ValidateValue::tmp:ARCCost1
    If tmp:ARCCost1:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('Prompt:Cost1') = '')
  ! --- STRING --- tmp:ARCCost1
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('BookingSite') = 'RRC' Or p_web.GSV('ReadOnly:ARCCost1') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ARCCost1'',''viewcosts_tmp:arccost1_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:ARCCost1',p_web.GetSessionValue('tmp:ARCCost1'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:ARCCost1  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:ARCCost1:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('Prompt:Cost1') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost1') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Prompt:Cost1') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::tmp:AdjustmentCost1  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:AdjustmentCost1 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    tmp:AdjustmentCost1 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::tmp:AdjustmentCost1  ! copies value to session value if valid.
  do Value::tmp:AdjustmentCost1
  do SendAlert
  do Comment::tmp:AdjustmentCost1 ! allows comment style to be updated.

ValidateValue::tmp:AdjustmentCost1  Routine
    If not (p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced')
      if loc:invalid = '' then p_web.SetSessionValue('tmp:AdjustmentCost1',tmp:AdjustmentCost1).
    End

Value::tmp:AdjustmentCost1  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost1') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    tmp:AdjustmentCost1 = p_web.RestoreValue('tmp:AdjustmentCost1')
    do ValidateValue::tmp:AdjustmentCost1
    If tmp:AdjustmentCost1:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced')
  ! --- STRING --- tmp:AdjustmentCost1
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:AdjustmentCost1'',''viewcosts_tmp:adjustmentcost1_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:AdjustmentCost1',p_web.GetSessionValue('tmp:AdjustmentCost1'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:AdjustmentCost1  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:AdjustmentCost1:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost1') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced'
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:ARCCost2  Routine
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost2') & '_prompt',Choose(p_web.GSV('Prompt:Cost2') = '','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Prompt:Cost2') = '','',p_web.Translate(p_web.GSV('Prompt:Cost2')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:ARCCost2  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:ARCCost2 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    tmp:ARCCost2 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::tmp:ARCCost2  ! copies value to session value if valid.
  do Value::tmp:ARCCost2
  do SendAlert
  do Comment::tmp:ARCCost2 ! allows comment style to be updated.

ValidateValue::tmp:ARCCost2  Routine
    If not (p_web.GSV('Prompt:Cost2') = '')
      if loc:invalid = '' then p_web.SetSessionValue('tmp:ARCCost2',tmp:ARCCost2).
    End

Value::tmp:ARCCost2  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Prompt:Cost2') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost2') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If p_web.GSV('BookingSite') = 'RRC' Or p_web.GSV('ReadOnly:ARCCost2') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    tmp:ARCCost2 = p_web.RestoreValue('tmp:ARCCost2')
    do ValidateValue::tmp:ARCCost2
    If tmp:ARCCost2:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('Prompt:Cost2') = '')
  ! --- STRING --- tmp:ARCCost2
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('BookingSite') = 'RRC' Or p_web.GSV('ReadOnly:ARCCost2') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ARCCost2'',''viewcosts_tmp:arccost2_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:ARCCost2',p_web.GetSessionValue('tmp:ARCCost2'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:ARCCost2  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:ARCCost2:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('Prompt:Cost2') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost2') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Prompt:Cost2') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::tmp:AdjustmentCost2  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:AdjustmentCost2 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    tmp:AdjustmentCost2 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::tmp:AdjustmentCost2  ! copies value to session value if valid.
  do Value::tmp:AdjustmentCost2
  do SendAlert
  do Comment::tmp:AdjustmentCost2 ! allows comment style to be updated.

ValidateValue::tmp:AdjustmentCost2  Routine
    If not (p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced')
      if loc:invalid = '' then p_web.SetSessionValue('tmp:AdjustmentCost2',tmp:AdjustmentCost2).
    End

Value::tmp:AdjustmentCost2  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost2') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    tmp:AdjustmentCost2 = p_web.RestoreValue('tmp:AdjustmentCost2')
    do ValidateValue::tmp:AdjustmentCost2
    If tmp:AdjustmentCost2:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced')
  ! --- STRING --- tmp:AdjustmentCost2
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:AdjustmentCost2'',''viewcosts_tmp:adjustmentcost2_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:AdjustmentCost2',p_web.GetSessionValue('tmp:AdjustmentCost2'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:AdjustmentCost2  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:AdjustmentCost2:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost2') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced'
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:ARCCost3  Routine
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost3') & '_prompt',Choose(p_web.GSV('Prompt:Cost3') = '','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Prompt:Cost3') = '','',p_web.Translate(p_web.GSV('Prompt:Cost3')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:ARCCost3  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:ARCCost3 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    tmp:ARCCost3 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::tmp:ARCCost3  ! copies value to session value if valid.
  do Value::tmp:ARCCost3
  do SendAlert
  do Comment::tmp:ARCCost3 ! allows comment style to be updated.

ValidateValue::tmp:ARCCost3  Routine
    If not (p_web.GSV('Prompt:Cost3') = '')
      if loc:invalid = '' then p_web.SetSessionValue('tmp:ARCCost3',tmp:ARCCost3).
    End

Value::tmp:ARCCost3  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Prompt:Cost3') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost3') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If p_web.GSV('BookingSite') = 'RRC' Or p_web.GSV('ReadOnly:ARCCost3') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    tmp:ARCCost3 = p_web.RestoreValue('tmp:ARCCost3')
    do ValidateValue::tmp:ARCCost3
    If tmp:ARCCost3:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('Prompt:Cost3') = '')
  ! --- STRING --- tmp:ARCCost3
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('BookingSite') = 'RRC' Or p_web.GSV('ReadOnly:ARCCost3') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ARCCost3'',''viewcosts_tmp:arccost3_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:ARCCost3',p_web.GetSessionValue('tmp:ARCCost3'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:ARCCost3  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:ARCCost3:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('Prompt:Cost3') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost3') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Prompt:Cost3') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::tmp:AdjustmentCost3  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:AdjustmentCost3 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    tmp:AdjustmentCost3 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::tmp:AdjustmentCost3  ! copies value to session value if valid.
  do Value::tmp:AdjustmentCost3
  do SendAlert
  do Comment::tmp:AdjustmentCost3 ! allows comment style to be updated.

ValidateValue::tmp:AdjustmentCost3  Routine
    If not (p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced')
      if loc:invalid = '' then p_web.SetSessionValue('tmp:AdjustmentCost3',tmp:AdjustmentCost3).
    End

Value::tmp:AdjustmentCost3  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost3') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    tmp:AdjustmentCost3 = p_web.RestoreValue('tmp:AdjustmentCost3')
    do ValidateValue::tmp:AdjustmentCost3
    If tmp:AdjustmentCost3:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced')
  ! --- STRING --- tmp:AdjustmentCost3
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:AdjustmentCost3'',''viewcosts_tmp:adjustmentcost3_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:AdjustmentCost3',p_web.GetSessionValue('tmp:AdjustmentCost3'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:AdjustmentCost3  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:AdjustmentCost3:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost3') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced'
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::__line2  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::__line2  ! copies value to session value if valid.
  do Comment::__line2 ! allows comment style to be updated.

ValidateValue::__line2  Routine
    If not (1=0)
    End

Value::__line2  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine('nt-width-100')
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('__line2') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & '<hr></hr><13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::__line2  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if __line2:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('__line2') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:ARCCost4  Routine
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost4') & '_prompt',Choose(p_web.GSV('Prompt:Cost4') = '','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Prompt:Cost4') = '','',p_web.Translate(p_web.GSV('Prompt:Cost4')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:ARCCost4  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:ARCCost4 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    tmp:ARCCost4 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::tmp:ARCCost4  ! copies value to session value if valid.
  do Value::tmp:ARCCost4
  do SendAlert
  do Comment::tmp:ARCCost4 ! allows comment style to be updated.

ValidateValue::tmp:ARCCost4  Routine
    If not (p_web.GSV('Prompt:Cost4') = '')
      if loc:invalid = '' then p_web.SetSessionValue('tmp:ARCCost4',tmp:ARCCost4).
    End

Value::tmp:ARCCost4  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Prompt:Cost4') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost4') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  If loc:retrying
    tmp:ARCCost4 = p_web.RestoreValue('tmp:ARCCost4')
    do ValidateValue::tmp:ARCCost4
    If tmp:ARCCost4:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('Prompt:Cost4') = '')
  ! --- STRING --- tmp:ARCCost4
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ARCCost4'',''viewcosts_tmp:arccost4_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:ARCCost4',p_web.GetSessionValue('tmp:ARCCost4'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:ARCCost4  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:ARCCost4:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('Prompt:Cost4') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost4') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Prompt:Cost4') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::tmp:AdjustmentCost4  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:AdjustmentCost4 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    tmp:AdjustmentCost4 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::tmp:AdjustmentCost4  ! copies value to session value if valid.
  do Value::tmp:AdjustmentCost4
  do SendAlert
  do Comment::tmp:AdjustmentCost4 ! allows comment style to be updated.

ValidateValue::tmp:AdjustmentCost4  Routine
    If not (p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced')
      if loc:invalid = '' then p_web.SetSessionValue('tmp:AdjustmentCost4',tmp:AdjustmentCost4).
    End

Value::tmp:AdjustmentCost4  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost4') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    tmp:AdjustmentCost4 = p_web.RestoreValue('tmp:AdjustmentCost4')
    do ValidateValue::tmp:AdjustmentCost4
    If tmp:AdjustmentCost4:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced')
  ! --- STRING --- tmp:AdjustmentCost4
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:AdjustmentCost4'',''viewcosts_tmp:adjustmentcost4_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:AdjustmentCost4',p_web.GetSessionValue('tmp:AdjustmentCost4'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:AdjustmentCost4  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:AdjustmentCost4:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost4') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced'
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:ARCCost5  Routine
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost5') & '_prompt',Choose(p_web.GSV('Prompt:Cost5') = '','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Prompt:Cost5') = '','',p_web.Translate(p_web.GSV('Prompt:Cost5')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:ARCCost5  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:ARCCost5 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    tmp:ARCCost5 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::tmp:ARCCost5  ! copies value to session value if valid.
  do Value::tmp:ARCCost5
  do SendAlert
  do Comment::tmp:ARCCost5 ! allows comment style to be updated.

ValidateValue::tmp:ARCCost5  Routine
    If not (p_web.GSV('Prompt:Cost5') = '')
      if loc:invalid = '' then p_web.SetSessionValue('tmp:ARCCost5',tmp:ARCCost5).
    End

Value::tmp:ARCCost5  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Prompt:Cost5') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost5') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  If loc:retrying
    tmp:ARCCost5 = p_web.RestoreValue('tmp:ARCCost5')
    do ValidateValue::tmp:ARCCost5
    If tmp:ARCCost5:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('Prompt:Cost5') = '')
  ! --- STRING --- tmp:ARCCost5
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ARCCost5'',''viewcosts_tmp:arccost5_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:ARCCost5',p_web.GetSessionValue('tmp:ARCCost5'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:ARCCost5  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:ARCCost5:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('Prompt:Cost5') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost5') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Prompt:Cost5') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::tmp:AdjustmentCost5  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:AdjustmentCost5 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    tmp:AdjustmentCost5 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::tmp:AdjustmentCost5  ! copies value to session value if valid.
  do Value::tmp:AdjustmentCost5
  do SendAlert
  do Comment::tmp:AdjustmentCost5 ! allows comment style to be updated.

ValidateValue::tmp:AdjustmentCost5  Routine
    If not (p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced')
      if loc:invalid = '' then p_web.SetSessionValue('tmp:AdjustmentCost5',tmp:AdjustmentCost5).
    End

Value::tmp:AdjustmentCost5  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost5') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    tmp:AdjustmentCost5 = p_web.RestoreValue('tmp:AdjustmentCost5')
    do ValidateValue::tmp:AdjustmentCost5
    If tmp:AdjustmentCost5:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced')
  ! --- STRING --- tmp:AdjustmentCost5
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:AdjustmentCost5'',''viewcosts_tmp:adjustmentcost5_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:AdjustmentCost5',p_web.GetSessionValue('tmp:AdjustmentCost5'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:AdjustmentCost5  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:AdjustmentCost5:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost5') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced'
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:ARCCost6  Routine
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost6') & '_prompt',Choose(p_web.GSV('Prompt:Cost6') = '','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Prompt:Cost6') = '','',p_web.Translate(p_web.GSV('Prompt:Cost6')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:ARCCost6  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:ARCCost6 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    tmp:ARCCost6 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::tmp:ARCCost6  ! copies value to session value if valid.
  do Value::tmp:ARCCost6
  do SendAlert
  do Comment::tmp:ARCCost6 ! allows comment style to be updated.

ValidateValue::tmp:ARCCost6  Routine
    If not (p_web.GSV('Prompt:Cost6') = '')
      if loc:invalid = '' then p_web.SetSessionValue('tmp:ARCCost6',tmp:ARCCost6).
    End

Value::tmp:ARCCost6  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Prompt:Cost6') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost6') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  If loc:retrying
    tmp:ARCCost6 = p_web.RestoreValue('tmp:ARCCost6')
    do ValidateValue::tmp:ARCCost6
    If tmp:ARCCost6:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('Prompt:Cost6') = '')
  ! --- STRING --- tmp:ARCCost6
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ARCCost6'',''viewcosts_tmp:arccost6_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:ARCCost6',p_web.GetSessionValue('tmp:ARCCost6'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:ARCCost6  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:ARCCost6:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('Prompt:Cost6') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost6') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Prompt:Cost6') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::tmp:AdjustmentCost6  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:AdjustmentCost6 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    tmp:AdjustmentCost6 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::tmp:AdjustmentCost6  ! copies value to session value if valid.
  do Value::tmp:AdjustmentCost6
  do SendAlert
  do Comment::tmp:AdjustmentCost6 ! allows comment style to be updated.

ValidateValue::tmp:AdjustmentCost6  Routine
    If not (p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced')
      if loc:invalid = '' then p_web.SetSessionValue('tmp:AdjustmentCost6',tmp:AdjustmentCost6).
    End

Value::tmp:AdjustmentCost6  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost6') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    tmp:AdjustmentCost6 = p_web.RestoreValue('tmp:AdjustmentCost6')
    do ValidateValue::tmp:AdjustmentCost6
    If tmp:AdjustmentCost6:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced')
  ! --- STRING --- tmp:AdjustmentCost6
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:AdjustmentCost6'',''viewcosts_tmp:adjustmentcost6_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:AdjustmentCost6',p_web.GetSessionValue('tmp:AdjustmentCost6'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:AdjustmentCost6  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:AdjustmentCost6:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost6') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced'
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:ARCCost7  Routine
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost7') & '_prompt',Choose(p_web.GSV('Prompt:Cost7') = '','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Prompt:Cost7') = '','',p_web.Translate(p_web.GSV('Prompt:Cost7')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:ARCCost7  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:ARCCost7 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    tmp:ARCCost7 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::tmp:ARCCost7  ! copies value to session value if valid.
  do Value::tmp:ARCCost7
  do SendAlert
  do Comment::tmp:ARCCost7 ! allows comment style to be updated.

ValidateValue::tmp:ARCCost7  Routine
    If not (p_web.GSV('Prompt:Cost7') = '')
      if loc:invalid = '' then p_web.SetSessionValue('tmp:ARCCost7',tmp:ARCCost7).
    End

Value::tmp:ARCCost7  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Prompt:Cost7') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost7') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  If loc:retrying
    tmp:ARCCost7 = p_web.RestoreValue('tmp:ARCCost7')
    do ValidateValue::tmp:ARCCost7
    If tmp:ARCCost7:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('Prompt:Cost7') = '')
  ! --- STRING --- tmp:ARCCost7
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('BookingSite') = 'RRC','readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ARCCost7'',''viewcosts_tmp:arccost7_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:ARCCost7',p_web.GetSessionValue('tmp:ARCCost7'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:ARCCost7  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:ARCCost7:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('Prompt:Cost7') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost7') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Prompt:Cost7') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:ARCCost8  Routine
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost8') & '_prompt',Choose(p_web.GSV('Prompt:Cost8') = '','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Prompt:Cost8') = '','',p_web.Translate(p_web.GSV('Prompt:Cost8')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:ARCCost8  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:ARCCost8 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    tmp:ARCCost8 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::tmp:ARCCost8  ! copies value to session value if valid.
  do Value::tmp:ARCCost8
  do SendAlert
  do Comment::tmp:ARCCost8 ! allows comment style to be updated.

ValidateValue::tmp:ARCCost8  Routine
    If not (p_web.GSV('Prompt:Cost8') = '')
      if loc:invalid = '' then p_web.SetSessionValue('tmp:ARCCost8',tmp:ARCCost8).
    End

Value::tmp:ARCCost8  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Prompt:Cost8') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost8') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  If loc:retrying
    tmp:ARCCost8 = p_web.RestoreValue('tmp:ARCCost8')
    do ValidateValue::tmp:ARCCost8
    If tmp:ARCCost8:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('Prompt:Cost8') = '')
  ! --- STRING --- tmp:ARCCost8
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('BookingSite') = 'RRC','readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ARCCost8'',''viewcosts_tmp:arccost8_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:ARCCost8',p_web.GetSessionValue('tmp:ARCCost8'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:ARCCost8  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:ARCCost8:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('Prompt:Cost8') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost8') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Prompt:Cost8') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::jobe:ARC3rdPartyInvoiceNumber  Routine
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('jobe:ARC3rdPartyInvoiceNumber') & '_prompt',Choose(p_web.GSV('jobe:ARC3rdPartyInvoiceNumber') = 0 Or p_web.GSV('tmp:ARCViewCostType') <> '3rd Party','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('jobe:ARC3rdPartyInvoiceNumber') = 0 Or p_web.GSV('tmp:ARCViewCostType') <> '3rd Party','',p_web.Translate('Invoice Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jobe:ARC3rdPartyInvoiceNumber  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jobe:ARC3rdPartyInvoiceNumber = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    jobe:ARC3rdPartyInvoiceNumber = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::jobe:ARC3rdPartyInvoiceNumber  ! copies value to session value if valid.
  do Comment::jobe:ARC3rdPartyInvoiceNumber ! allows comment style to be updated.

ValidateValue::jobe:ARC3rdPartyInvoiceNumber  Routine
    If not (p_web.GSV('jobe:ARC3rdPartyInvoiceNumber') = 0 Or p_web.GSV('tmp:ARCViewCostType') <> '3rd Party')
      if loc:invalid = '' then p_web.SetSessionValue('jobe:ARC3rdPartyInvoiceNumber',jobe:ARC3rdPartyInvoiceNumber).
    End

Value::jobe:ARC3rdPartyInvoiceNumber  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('jobe:ARC3rdPartyInvoiceNumber') = 0 Or p_web.GSV('tmp:ARCViewCostType') <> '3rd Party','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('jobe:ARC3rdPartyInvoiceNumber') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (p_web.GSV('jobe:ARC3rdPartyInvoiceNumber') = 0 Or p_web.GSV('tmp:ARCViewCostType') <> '3rd Party')
  ! --- DISPLAY --- jobe:ARC3rdPartyInvoiceNumber
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('jobe:ARC3rdPartyInvoiceNumber'),) & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::jobe:ARC3rdPartyInvoiceNumber  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if jobe:ARC3rdPartyInvoiceNumber:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('jobe:ARC3rdPartyInvoiceNumber') = 0 Or p_web.GSV('tmp:ARCViewCostType') <> '3rd Party','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('jobe:ARC3rdPartyInvoiceNumber') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('jobe:ARC3rdPartyInvoiceNumber') = 0 Or p_web.GSV('tmp:ARCViewCostType') <> '3rd Party'
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:ARCInvoiceNumber  Routine
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCInvoiceNumber') & '_prompt',Choose(~Instring('-Invoiced',p_web.GSV('tmp:ARCViewCostType')),'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(~Instring('-Invoiced',p_web.GSV('tmp:ARCViewCostType')),'',p_web.Translate('Invoice Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:ARCInvoiceNumber  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:ARCInvoiceNumber = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:ARCInvoiceNumber = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:ARCInvoiceNumber  ! copies value to session value if valid.
  do Comment::tmp:ARCInvoiceNumber ! allows comment style to be updated.

ValidateValue::tmp:ARCInvoiceNumber  Routine
    If not (~Instring('-Invoiced',p_web.GSV('tmp:ARCViewCostType')))
      if loc:invalid = '' then p_web.SetSessionValue('tmp:ARCInvoiceNumber',tmp:ARCInvoiceNumber).
    End

Value::tmp:ARCInvoiceNumber  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(~Instring('-Invoiced',p_web.GSV('tmp:ARCViewCostType')),'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCInvoiceNumber') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (~Instring('-Invoiced',p_web.GSV('tmp:ARCViewCostType')))
  ! --- DISPLAY --- tmp:ARCInvoiceNumber
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('tmp:ARCInvoiceNumber'),) & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:ARCInvoiceNumber  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:ARCInvoiceNumber:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(~Instring('-Invoiced',p_web.GSV('tmp:ARCViewCostType')),'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCInvoiceNumber') & '_comment',loc:class,Net:NoSend)
  If ~Instring('-Invoiced',p_web.GSV('tmp:ARCViewCostType'))
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:RRCViewCostType  Routine
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCViewCostType') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('View Cost'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:RRCViewCostType  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:RRCViewCostType = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:RRCViewCostType = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:RRCViewCostType  ! copies value to session value if valid.
  do displayRRCCostFields
  do displayOtherFields
  do Value::tmp:RRCViewCostType
  do SendAlert
  do Comment::tmp:RRCViewCostType ! allows comment style to be updated.
  do Prompt::tmp:RRCCost0
  do Value::tmp:RRCCost0  !1
  do Prompt::tmp:RRCCost1
  do Value::tmp:RRCCost1  !1
  do Comment::tmp:RRCCost1
  do Prompt::tmp:RRCCost2
  do Value::tmp:RRCCost2  !1
  do Comment::tmp:RRCCost2
  do Prompt::tmp:RRCCost3
  do Value::tmp:RRCCost3  !1
  do Comment::tmp:RRCCost3
  do Prompt::tmp:RRCCost4
  do Value::tmp:RRCCost4  !1
  do Comment::tmp:RRCCost4
  do Prompt::tmp:RRCCost5
  do Value::tmp:RRCCost5  !1
  do Comment::tmp:RRCCost5
  do Prompt::tmp:RRCCost6
  do Value::tmp:RRCCost6  !1
  do Comment::tmp:RRCCost6
  do Prompt::tmp:RRCCost7
  do Value::tmp:RRCCost7  !1
  do Comment::tmp:RRCCost7
  do Prompt::tmp:RRCCost8
  do Value::tmp:RRCCost8  !1
  do Comment::tmp:RRCCost8
  do Prompt::jobe:ExcReplcamentCharge
  do Value::jobe:ExcReplcamentCharge  !1
  do Prompt::tmp:RRCIgnoreDefaultCharges
  do Value::tmp:RRCIgnoreDefaultCharges  !1

ValidateValue::tmp:RRCViewCostType  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:RRCViewCostType',tmp:RRCViewCostType).
    End

Value::tmp:RRCViewCostType  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCViewCostType') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,p_web.site.style.formselect,'FormEntry',)
  If loc:retrying
    tmp:RRCViewCostType = p_web.RestoreValue('tmp:RRCViewCostType')
    do ValidateValue::tmp:RRCViewCostType
    If tmp:RRCViewCostType:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- DROPLIST ---
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:RRCViewCostType'',''viewcosts_tmp:rrcviewcosttype_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:RRCViewCostType')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('ValidateIgnoreTickBox') = 1,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:RRCViewCostType',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
  loc:fieldclass = ''
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('tmp:RRCViewCostType') = 0
    p_web.SetSessionValue('tmp:RRCViewCostType','')
  end
    packet = clip(packet) & p_web.CreateOption('','',choose('' = p_web.getsessionvalue('tmp:RRCViewCostType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:RRCChargeable') <> 1
    packet = clip(packet) & p_web.CreateOption('Chargeable','Chargeable',choose('Chargeable' = p_web.getsessionvalue('tmp:RRCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:RRCWarranty') <> 1
    packet = clip(packet) & p_web.CreateOption('Warranty','Warranty',choose('Warranty' = p_web.getsessionvalue('tmp:RRCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:RRCHandling') <> 1
    packet = clip(packet) & p_web.CreateOption('Handling','Handling',choose('Handling' = p_web.getsessionvalue('tmp:RRCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:RRCExchange') <> 1
    packet = clip(packet) & p_web.CreateOption('Exchange','Exchange',choose('Exchange' = p_web.getsessionvalue('tmp:RRCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:RRCEstimate') <> 1
    packet = clip(packet) & p_web.CreateOption('Estimate','Estimate',choose('Estimate' = p_web.getsessionvalue('tmp:RRCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:RRCCInvoice') <> 1
    packet = clip(packet) & p_web.CreateOption('Chargeable - Invoiced','Chargeable - Invoiced',choose('Chargeable - Invoiced' = p_web.getsessionvalue('tmp:RRCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:RRCWInvoice') <> 1
    packet = clip(packet) & p_web.CreateOption('Warranty - Invoiced','Warranty - Invoiced',choose('Warranty - Invoiced' = p_web.getsessionvalue('tmp:RRCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:Credit') <> 1
    packet = clip(packet) & p_web.CreateOption('Credits','Credits',choose('Credits' = p_web.getsessionvalue('tmp:RRCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:RRCViewCostType  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:RRCViewCostType:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCViewCostType') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::__line3  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::__line3  ! copies value to session value if valid.
  do Comment::__line3 ! allows comment style to be updated.

ValidateValue::__line3  Routine
    If not (1=0)
    End

Value::__line3  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine('nt-width-100')
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('__line3') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & '<hr></hr><13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::__line3  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if __line3:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('__line3') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:RRCIgnoreDefaultCharges  Routine
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCIgnoreDefaultCharges') & '_prompt',Choose(p_web.GSV('tmp:RRCViewCostType') = 'Credits' Or p_web.GSV('tmp:RRCViewCostType') = 'Handling' Or p_web.GSV('tmp:RRCViewCostType') = 'Exchange' Or p_web.GSV('tmp:RRCViewCostType') = '','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('tmp:RRCViewCostType') = 'Credits' Or p_web.GSV('tmp:RRCViewCostType') = 'Handling' Or p_web.GSV('tmp:RRCViewCostType') = 'Exchange' Or p_web.GSV('tmp:RRCViewCostType') = '','',p_web.Translate('Ignore Default Charges'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:RRCIgnoreDefaultCharges  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:RRCIgnoreDefaultCharges = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    tmp:RRCIgnoreDefaultCharges = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:RRCIgnoreDefaultCharges  ! copies value to session value if valid.
  if (p_web.GSV('tmp:RRCIgnoreDefaultCharges') = 1)
      p_web.SSV('tmp:RRCIgnoreReason','')
      p_web.SSV('ValidateIgnoreTickBox',1)
  else  ! if (p_web.GSV('tmp:RRCIgnoreDefaultCharges') = 1)
      p_web.SSV('ReadOnly:RRCCost2',1)
      case p_web.GSV('tmp:RRCViewCostType')
      of 'Chargeable'
          p_web.SSV('jobe:IgnoreRRCChaCosts',0)
      of 'Warranty'
          p_web.SSV('jobe:IgnoreRRCWarCosts',0)
      of 'Estimate'
          p_web.SSV('jobe:IgnoreRRCEstCosts',0)
      end ! case p_web.GSV('tmp:ARCViewCostType')
      p_web.SSV('jobe2:JobDiscountAmnt',0)
      do pricingRoutine
      do displayRRCCostFields
  end ! if (p_web.GSV('tmp:RRCIgnoreDefaultCharges') = 1)
  do Value::tmp:RRCIgnoreDefaultCharges
  do SendAlert
  do Comment::tmp:RRCIgnoreDefaultCharges ! allows comment style to be updated.
  do Prompt::tmp:RRCIgnoreReason
  do Value::tmp:RRCIgnoreReason  !1
  do Value::buttonAcceptRRCReason  !1
  do Value::buttonCancelRRCReason  !1
  do Value::tmp:RRCViewCostType  !1
  do Value::tmp:RRCCost2  !1
  do Value::tmp:RRCCost3  !1
  do Value::tmp:RRCCost4  !1
  do Value::tmp:RRCCost5  !1
  do Value::tmp:RRCCost6  !1
  do Value::tmp:RRCCost7  !1
  do Value::tmp:RRCCost8  !1
  do Prompt::tmp:RRCCost0
  do Value::tmp:RRCCost0  !1
  do Comment::tmp:RRCCost0

ValidateValue::tmp:RRCIgnoreDefaultCharges  Routine
    If not (p_web.GSV('tmp:RRCViewCostType') = 'Credits' Or p_web.GSV('tmp:RRCViewCostType') = 'Handling' Or p_web.GSV('tmp:RRCViewCostType') = 'Exchange' Or p_web.GSV('tmp:RRCViewCostType') = '')
      if loc:invalid = '' then p_web.SetSessionValue('tmp:RRCIgnoreDefaultCharges',tmp:RRCIgnoreDefaultCharges).
    End

Value::tmp:RRCIgnoreDefaultCharges  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('tmp:RRCViewCostType') = 'Credits' Or p_web.GSV('tmp:RRCViewCostType') = 'Handling' Or p_web.GSV('tmp:RRCViewCostType') = 'Exchange' Or p_web.GSV('tmp:RRCViewCostType') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCIgnoreDefaultCharges') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If loc:retrying
    tmp:RRCIgnoreDefaultCharges = p_web.RestoreValue('tmp:RRCIgnoreDefaultCharges')
    do ValidateValue::tmp:RRCIgnoreDefaultCharges
    If tmp:RRCIgnoreDefaultCharges:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('tmp:RRCViewCostType') = 'Credits' Or p_web.GSV('tmp:RRCViewCostType') = 'Handling' Or p_web.GSV('tmp:RRCViewCostType') = 'Exchange' Or p_web.GSV('tmp:RRCViewCostType') = '')
  ! --- CHECKBOX --- tmp:RRCIgnoreDefaultCharges
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:RRCIgnoreDefaultCharges'',''viewcosts_tmp:rrcignoredefaultcharges_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon(tmp:RRCIgnoreReason)&''',2);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('ValidateIgnoreTickBox') = 1 Or Instring('- Invoiced',p_web.GSV('tmp:RRCViewCostType'),1,1),'disabled','')
  If p_web.GetSessionValue('tmp:RRCIgnoreDefaultCharges') = true
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','tmp:RRCIgnoreDefaultCharges',clip(true),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:RRCIgnoreDefaultCharges  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:RRCIgnoreDefaultCharges:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('tmp:RRCViewCostType') = 'Credits' Or p_web.GSV('tmp:RRCViewCostType') = 'Handling' Or p_web.GSV('tmp:RRCViewCostType') = 'Exchange' Or p_web.GSV('tmp:RRCViewCostType') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCIgnoreDefaultCharges') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('tmp:RRCViewCostType') = 'Credits' Or p_web.GSV('tmp:RRCViewCostType') = 'Handling' Or p_web.GSV('tmp:RRCViewCostType') = 'Exchange' Or p_web.GSV('tmp:RRCViewCostType') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:RRCIgnoreReason  Routine
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCIgnoreReason') & '_prompt',Choose(p_web.GSV('ValidateIgnoreTickBox') <> 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('ValidateIgnoreTickBox') <> 1,'',p_web.Translate('Enter Reason For Ignoring Standard Charges'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:RRCIgnoreReason  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:RRCIgnoreReason = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:RRCIgnoreReason = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:RRCIgnoreReason  ! copies value to session value if valid.
  p_web.SSV('tmp:RRCIgnoreReason',BHStripReplace(p_web.GSV('tmp:RRCIgnoreReason'),'<9>',''))
  do Value::tmp:RRCIgnoreReason
  do SendAlert
  do Comment::tmp:RRCIgnoreReason ! allows comment style to be updated.

ValidateValue::tmp:RRCIgnoreReason  Routine
    If not (p_web.GSV('ValidateIgnoreTickBox') <> 1)
  If tmp:RRCIgnoreReason = ''
    loc:Invalid = 'tmp:RRCIgnoreReason'
    tmp:RRCIgnoreReason:IsInvalid = true
    loc:alert = p_web.translate('Enter Reason For Ignoring Standard Charges') & ' ' & p_web.site.RequiredText
  End
    tmp:RRCIgnoreReason = Upper(tmp:RRCIgnoreReason)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:RRCIgnoreReason',tmp:RRCIgnoreReason).
    End

Value::tmp:RRCIgnoreReason  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('ValidateIgnoreTickBox') <> 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCIgnoreReason') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,'formrqd')
  If loc:retrying
    tmp:RRCIgnoreReason = p_web.RestoreValue('tmp:RRCIgnoreReason')
    do ValidateValue::tmp:RRCIgnoreReason
    If tmp:RRCIgnoreReason:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('ValidateIgnoreTickBox') <> 1)
  ! --- TEXT --- tmp:RRCIgnoreReason
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:RRCIgnoreReason'',''viewcosts_tmp:rrcignorereason_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:RRCIgnoreReason')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'readonly','')
  do SendPacket
  p_web.CreateTextArea('tmp:RRCIgnoreReason',p_web.GetSessionValue('tmp:RRCIgnoreReason'),5,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(tmp:RRCIgnoreReason),,,,Net:Send)
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:RRCIgnoreReason  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:RRCIgnoreReason:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web._jsok(p_web.site.RequiredText)
  loc:class = Choose(p_web.GSV('ValidateIgnoreTickBox') <> 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCIgnoreReason') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('ValidateIgnoreTickBox') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonAcceptRRCReason  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonAcceptRRCReason  ! copies value to session value if valid.
  if (p_web.GSV('tmp:RRCIgnoreReason') <> '')
      p_web.SSV('ValidateIgnoreTickBox',0)
      case p_web.GSV('tmp:RRCViewCostType')
      of 'Chargeable'
          local.AddToTempQueue('IGNORE DEFAULT RRC CHARGEABLE COSTS',|
                          p_web.GSV('jobe:IgnoreRRCChaCosts'), |
                          p_web.GSV('tmp:RRCIgnoreReason'), |
                          1,|
                          p_web.GSV('tmp:RRCCost2'))
          p_web.SSV('jobe:IgnoreRRCChaCosts',1)
          p_web.SSV('Prompt:RCost0','Discount')
      of 'Warranty'
          local.AddToTempQueue('IGNORE DEFAULT RRC WARRANTY COSTS',|
                          p_web.GSV('jobe:IgnoreRRCWarCosts'),|
                          p_web.GSV('tmp:RRCIgnoreReason'), |
                          1,|
                          p_web.GSV('tmp:RRCCost2'))
          p_web.SSV('jobe:IgnoreRRCWarCosts',1)
      of 'Estimate'
          local.AddToTempQueue('IGNORE DEFAULT RRC ESTIMATE COSTS',|
                          p_web.GSV('jobe:IgnoreRRCEstCosts'),|
                          p_web.GSV('tmp:RRCIgnoreReason'),|
                          1,|
                          p_web.GSV('tmp:RRCCost2'))
          p_web.SSV('jobe:IgnoreRRCEstCosts',1)
      end ! case p_web.GSV('tmp:ARCViewCostType')
      p_web.SSV('ReadOnly:RRCCost2',0)
  else ! if (p_web.GSV('tmp:RRCIgnoreReason') <> '')
      p_web.SSV('ReadOnly:RRCCost2',1)
      case p_web.GSV('tmp:RRCViewCostType')
      of 'Chargeable'
          p_web.SSV('jobe:IgnoreRRCChaCosts',0)
      of 'Warranty'
          p_web.SSV('jobe:IgnoreRRCWarCosts',0)
      of 'Estimate'
          p_web.SSV('jobe:IgnoreRRCEstCosts',0)
      end ! case p_web.GSV('tmp:ARCViewCostType')
      p_web.SSV('tmp:RRCIgnoreDefaultCharges',0)
      p_web.SSV('Prompt:RCost0','')
  end ! if (p_web.GSV('tmp:RRCIgnoreReason') <> '')
  do Value::buttonAcceptRRCReason
  do Comment::buttonAcceptRRCReason ! allows comment style to be updated.
  do Value::buttonCancelRRCReason  !1
  do Value::tmp:RRCIgnoreDefaultCharges  !1
  do Prompt::tmp:RRCIgnoreReason
  do Value::tmp:RRCIgnoreReason  !1
  do Value::tmp:RRCCost2  !1
  do Value::tmp:RRCViewCostType  !1
  do Prompt::tmp:RRCCost0
  do Value::tmp:RRCCost0  !1

ValidateValue::buttonAcceptRRCReason  Routine
    If not (p_web.GSV('ValidateIgnoreTickBox') <> 1)
    End

Value::buttonAcceptRRCReason  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('ValidateIgnoreTickBox') <> 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('buttonAcceptRRCReason') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (p_web.GSV('ValidateIgnoreTickBox') <> 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonAcceptRRCReason'',''viewcosts_buttonacceptrrcreason_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','AcceptRRCReason','Accept',p_web.combine(Choose('Accept' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'SmallButton'),loc:formname,,,,loc:javascript,loc:disabled,,,,,,,,,,'nt-left')!h
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonAcceptRRCReason  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonAcceptRRCReason:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('ValidateIgnoreTickBox') <> 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('buttonAcceptRRCReason') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('ValidateIgnoreTickBox') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonCancelRRCReason  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonCancelRRCReason  ! copies value to session value if valid.
  p_web.SSV('ValidateIgnoreTickBox',0)
  p_web.SSV('ReadOnly:RRCCost2',1)
      case p_web.GSV('tmp:RRCViewCostType')
      of 'Chargeable'
          p_web.SSV('jobe:IgnoreRRCChaCosts',0)
      of 'Warranty'
          p_web.SSV('jobe:IgnoreRRCWarCosts',0)
      of 'Estimate'
          p_web.SSV('jobe:IgnoreRRCEstCosts',0)
      end ! case p_web.GSV('tmp:ARCViewCostType')
  p_web.SSV('tmp:RRCIgnoreDefaultCharges',0)
  do Value::buttonCancelRRCReason
  do Comment::buttonCancelRRCReason ! allows comment style to be updated.
  do Value::buttonAcceptRRCReason  !1
  do Prompt::tmp:RRCIgnoreReason
  do Value::tmp:RRCIgnoreReason  !1
  do Value::tmp:RRCViewCostType  !1
  do Value::tmp:RRCIgnoreDefaultCharges  !1
  do Value::tmp:RRCCost2  !1
  do Prompt::tmp:RRCCost0
  do Value::tmp:RRCCost0  !1

ValidateValue::buttonCancelRRCReason  Routine
    If not (p_web.GSV('ValidateIgnoreTickBox') <> 1)
    End

Value::buttonCancelRRCReason  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('ValidateIgnoreTickBox') <> 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('buttonCancelRRCReason') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (p_web.GSV('ValidateIgnoreTickBox') <> 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonCancelRRCReason'',''viewcosts_buttoncancelrrcreason_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','CancelRRCReason','Cancel',p_web.combine(Choose('Cancel' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'SmallButton'),loc:formname,,,,loc:javascript,loc:disabled,,,,,,,,,,'nt-left')!h
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonCancelRRCReason  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonCancelRRCReason:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('ValidateIgnoreTickBox') <> 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('buttonCancelRRCReason') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('ValidateIgnoreTickBox') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:RRCCost0  Routine
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost0') & '_prompt',Choose(p_web.GSV('Prompt:RCost0') = '','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Prompt:RCost0') = '','',p_web.Translate(p_web.GSV('Prompt:RCost0')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:RRCCost0  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:RRCCost0 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    tmp:RRCCost0 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::tmp:RRCCost0  ! copies value to session value if valid.
  do Value::tmp:RRCCost0
  do SendAlert
  do Comment::tmp:RRCCost0 ! allows comment style to be updated.

ValidateValue::tmp:RRCCost0  Routine
    If not (p_web.GSV('Prompt:RCost0') = '')
      if loc:invalid = '' then p_web.SetSessionValue('tmp:RRCCost0',tmp:RRCCost0).
    End

Value::tmp:RRCCost0  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Prompt:RCost0') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost0') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  If loc:retrying
    tmp:RRCCost0 = p_web.RestoreValue('tmp:RRCCost0')
    do ValidateValue::tmp:RRCCost0
    If tmp:RRCCost0:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('Prompt:RCost0') = '')
  ! --- STRING --- tmp:RRCCost0
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:RRCCost0'',''viewcosts_tmp:rrccost0_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:RRCCost0',p_web.GetSessionValue('tmp:RRCCost0'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:RRCCost0  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:RRCCost0:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('Prompt:RCost0') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost0') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Prompt:RCost0') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:RRCCost1  Routine
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost1') & '_prompt',Choose(p_web.GSV('Prompt:RCost1') = '','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Prompt:RCost1') = '','',p_web.Translate(p_web.GSV('Prompt:RCost1')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:RRCCost1  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:RRCCost1 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    tmp:RRCCost1 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::tmp:RRCCost1  ! copies value to session value if valid.
  do Value::tmp:RRCCost1
  do SendAlert
  do Comment::tmp:RRCCost1 ! allows comment style to be updated.

ValidateValue::tmp:RRCCost1  Routine
    If not (p_web.GSV('Prompt:RCost1') = '')
      if loc:invalid = '' then p_web.SetSessionValue('tmp:RRCCost1',tmp:RRCCost1).
    End

Value::tmp:RRCCost1  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Prompt:RCost1') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost1') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If p_web.GSV('ReadOnly:RRCCost1') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    tmp:RRCCost1 = p_web.RestoreValue('tmp:RRCCost1')
    do ValidateValue::tmp:RRCCost1
    If tmp:RRCCost1:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('Prompt:RCost1') = '')
  ! --- STRING --- tmp:RRCCost1
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:RRCCost1') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:RRCCost1'',''viewcosts_tmp:rrccost1_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:RRCCost1',p_web.GetSessionValue('tmp:RRCCost1'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:RRCCost1  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:RRCCost1:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('Prompt:RCost1') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost1') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Prompt:RCost1') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:originalInvoice  Routine
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:originalInvoice') & '_prompt',Choose(p_web.GSV('tmp:RRCViewCostType') <> 'Credits','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('tmp:RRCViewCostType') <> 'Credits','',p_web.Translate('Original Invoice No'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:originalInvoice  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:originalInvoice = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:originalInvoice = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:originalInvoice  ! copies value to session value if valid.
  do Value::tmp:originalInvoice
  do SendAlert
  do Comment::tmp:originalInvoice ! allows comment style to be updated.

ValidateValue::tmp:originalInvoice  Routine
    If not (p_web.GSV('tmp:RRCViewCostType') <> 'Credits')
      if loc:invalid = '' then p_web.SetSessionValue('tmp:originalInvoice',tmp:originalInvoice).
    End

Value::tmp:originalInvoice  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('tmp:RRCViewCostType') <> 'Credits','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:originalInvoice') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  If loc:retrying
    tmp:originalInvoice = p_web.RestoreValue('tmp:originalInvoice')
    do ValidateValue::tmp:originalInvoice
    If tmp:originalInvoice:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('tmp:RRCViewCostType') <> 'Credits')
  ! --- STRING --- tmp:originalInvoice
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:originalInvoice'',''viewcosts_tmp:originalinvoice_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:originalInvoice',p_web.GetSessionValueFormat('tmp:originalInvoice'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:originalInvoice  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:originalInvoice:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('tmp:RRCViewCostType') <> 'Credits','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:originalInvoice') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('tmp:RRCViewCostType') <> 'Credits'
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:RRCCost2  Routine
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost2') & '_prompt',Choose(p_web.GSV('Prompt:RCost2') = '','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Prompt:RCost2') = '','',p_web.Translate(p_web.GSV('Prompt:RCost2')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:RRCCost2  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:RRCCost2 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    tmp:RRCCost2 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::tmp:RRCCost2  ! copies value to session value if valid.
  if (p_web.GSV('tmp:RRCViewCostType') = 'Chargeable')
      if (format(p_web.GSV('tmp:RRCCost2'),@n_14.2) > format(p_web.GSV('DefaultLabourCost'),@n_14.2))
          p_web.SSV('Comment:RCost2','Error! Cost is higher than Default Cost')
          p_web.SSV('Prompt:RCost0','') ! Blank the discount field.
      else
          p_web.SSV('Comment:RCost2','')
      end
  end ! 'tmp:RRCViewCostType'
  do updateRRCCost
  do pricingRoutine
  do displayRRCCostFields
  do Value::tmp:RRCCost2
  do SendAlert
  do Comment::tmp:RRCCost2 ! allows comment style to be updated.
  do Prompt::tmp:RRCCost2
  do Comment::tmp:RRCCost2
  do Value::tmp:RRCCost3  !1
  do Value::tmp:RRCCost4  !1
  do Value::tmp:RRCCost5  !1
  do Value::tmp:RRCCost6  !1
  do Value::tmp:RRCCost7  !1
  do Value::tmp:RRCCost8  !1
  do Prompt::tmp:RRCCost0
  do Value::tmp:RRCCost0  !1
  do Comment::tmp:RRCCost0

ValidateValue::tmp:RRCCost2  Routine
    If not (p_web.GSV('Prompt:RCost2') = '')
      if loc:invalid = '' then p_web.SetSessionValue('tmp:RRCCost2',tmp:RRCCost2).
    End

Value::tmp:RRCCost2  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Prompt:RCost2') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost2') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If p_web.GSV('ReadOnly:RRCCost2') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    tmp:RRCCost2 = p_web.RestoreValue('tmp:RRCCost2')
    do ValidateValue::tmp:RRCCost2
    If tmp:RRCCost2:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('Prompt:RCost2') = '')
  ! --- STRING --- tmp:RRCCost2
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:RRCCost2') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:RRCCost2'',''viewcosts_tmp:rrccost2_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:RRCCost2')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:RRCCost2',p_web.GetSessionValue('tmp:RRCCost2'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:RRCCost2  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:RRCCost2:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:RCost2'))
  loc:class = Choose(p_web.GSV('Prompt:RCost2') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost2') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Prompt:RCost2') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:RRCCost3  Routine
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost3') & '_prompt',Choose(p_web.GSV('Prompt:RCost3') = '','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Prompt:RCost3') = '','',p_web.Translate(p_web.GSV('Prompt:RCost3')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:RRCCost3  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:RRCCost3 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    tmp:RRCCost3 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::tmp:RRCCost3  ! copies value to session value if valid.
  do Value::tmp:RRCCost3
  do SendAlert
  do Comment::tmp:RRCCost3 ! allows comment style to be updated.

ValidateValue::tmp:RRCCost3  Routine
    If not (p_web.GSV('Prompt:RCost3') = '')
      if loc:invalid = '' then p_web.SetSessionValue('tmp:RRCCost3',tmp:RRCCost3).
    End

Value::tmp:RRCCost3  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Prompt:RCost3') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost3') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If p_web.GSV('ReadOnly:RRCCost3') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    tmp:RRCCost3 = p_web.RestoreValue('tmp:RRCCost3')
    do ValidateValue::tmp:RRCCost3
    If tmp:RRCCost3:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('Prompt:RCost3') = '')
  ! --- STRING --- tmp:RRCCost3
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:RRCCost3') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:RRCCost3'',''viewcosts_tmp:rrccost3_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:RRCCost3',p_web.GetSessionValue('tmp:RRCCost3'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:RRCCost3  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:RRCCost3:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('Prompt:RCost3') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost3') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Prompt:RCost3') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::BrowseJobCredits  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  Elsif true
    p_web.StoreValue('jov:RecordNumber')
  End
  do ValidateValue::BrowseJobCredits  ! copies value to session value if valid.
  do Comment::BrowseJobCredits ! allows comment style to be updated.

ValidateValue::BrowseJobCredits  Routine
    If not (p_web.GSV('tmp:RRCViewCostType') <> 'Credits')
    End

Value::BrowseJobCredits  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(p_web.GSV('tmp:RRCViewCostType') <> 'Credits',1,0))
  ! --- BROWSE ---  BrowseJobCredits --
  p_web.SetValue('BrowseJobCredits:NoForm',1)
  p_web.SetValue('BrowseJobCredits:FormName',loc:formname)
  p_web.SetValue('BrowseJobCredits:parentIs','Form')
  p_web.SetValue('_parentProc','ViewCosts')
  if p_web.RequestAjax = 0
    p_web.SSV('ViewCosts:_popup_',p_web.GetValue('_popup_')) ! stores the current browse popup state
    packet = clip(packet) & '<div id="'&lower('ViewCosts_BrowseJobCredits_embedded_div')&'"><!-- Net:BrowseJobCredits --></div><13,10>'
    do SendPacket
    p_web.DivHeader('ViewCosts_' & lower('BrowseJobCredits') & '_value')
    p_web.DivFooter()
  else
    if p_web.GetValue('_popup_') = 1
      p_web.SSV('ViewCosts:_popup_',1)
    elsif p_web.GSV('ViewCosts:_popup_') = 1
      p_web.SetValue('_popup_',1)
    end
    packet = clip(packet) & '<!-- Net:BrowseJobCredits --><13,10>'
  end
  do SendPacket
Comment::BrowseJobCredits  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if BrowseJobCredits:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('tmp:RRCViewCostType') <> 'Credits','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('BrowseJobCredits') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('tmp:RRCViewCostType') <> 'Credits'
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::__line4  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::__line4  ! copies value to session value if valid.
  do Comment::__line4 ! allows comment style to be updated.

ValidateValue::__line4  Routine
    If not (1=0)
    End

Value::__line4  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine('nt-width-100')
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('__line4') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & '<hr></hr><13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::__line4  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if __line4:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('__line4') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:RRCCost4  Routine
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost4') & '_prompt',Choose(p_web.GSV('Prompt:RCost4') = '','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Prompt:RCost4') = '','',p_web.Translate(p_web.GSV('Prompt:RCost4')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:RRCCost4  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:RRCCost4 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    tmp:RRCCost4 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::tmp:RRCCost4  ! copies value to session value if valid.
  do Value::tmp:RRCCost4
  do SendAlert
  do Comment::tmp:RRCCost4 ! allows comment style to be updated.

ValidateValue::tmp:RRCCost4  Routine
    If not (p_web.GSV('Prompt:RCost4') = '')
      if loc:invalid = '' then p_web.SetSessionValue('tmp:RRCCost4',tmp:RRCCost4).
    End

Value::tmp:RRCCost4  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Prompt:RCost4') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost4') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  If loc:retrying
    tmp:RRCCost4 = p_web.RestoreValue('tmp:RRCCost4')
    do ValidateValue::tmp:RRCCost4
    If tmp:RRCCost4:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('Prompt:RCost4') = '')
  ! --- STRING --- tmp:RRCCost4
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:RRCCost4'',''viewcosts_tmp:rrccost4_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:RRCCost4',p_web.GetSessionValue('tmp:RRCCost4'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:RRCCost4  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:RRCCost4:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('Prompt:RCost4') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost4') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Prompt:RCost4') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:RRCCost5  Routine
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost5') & '_prompt',Choose(p_web.GSV('Prompt:RCost5') = '','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Prompt:RCost5') = '','',p_web.Translate(p_web.GSV('Prompt:RCost5')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:RRCCost5  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:RRCCost5 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    tmp:RRCCost5 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::tmp:RRCCost5  ! copies value to session value if valid.
  do Value::tmp:RRCCost5
  do SendAlert
  do Comment::tmp:RRCCost5 ! allows comment style to be updated.

ValidateValue::tmp:RRCCost5  Routine
    If not (p_web.GSV('Prompt:RCost5') = '')
      if loc:invalid = '' then p_web.SetSessionValue('tmp:RRCCost5',tmp:RRCCost5).
    End

Value::tmp:RRCCost5  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Prompt:RCost5') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost5') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  If loc:retrying
    tmp:RRCCost5 = p_web.RestoreValue('tmp:RRCCost5')
    do ValidateValue::tmp:RRCCost5
    If tmp:RRCCost5:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('Prompt:RCost5') = '')
  ! --- STRING --- tmp:RRCCost5
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:RRCCost5'',''viewcosts_tmp:rrccost5_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:RRCCost5',p_web.GetSessionValue('tmp:RRCCost5'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:RRCCost5  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:RRCCost5:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('Prompt:RCost5') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost5') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Prompt:RCost5') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:RRCCost6  Routine
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost6') & '_prompt',Choose(p_web.GSV('Prompt:RCost6') = '','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Prompt:RCost6') = '','',p_web.Translate(p_web.GSV('Prompt:RCost6')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:RRCCost6  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:RRCCost6 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    tmp:RRCCost6 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::tmp:RRCCost6  ! copies value to session value if valid.
  do Value::tmp:RRCCost6
  do SendAlert
  do Comment::tmp:RRCCost6 ! allows comment style to be updated.

ValidateValue::tmp:RRCCost6  Routine
    If not (p_web.GSV('Prompt:RCost6') = '')
      if loc:invalid = '' then p_web.SetSessionValue('tmp:RRCCost6',tmp:RRCCost6).
    End

Value::tmp:RRCCost6  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Prompt:RCost6') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost6') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  If loc:retrying
    tmp:RRCCost6 = p_web.RestoreValue('tmp:RRCCost6')
    do ValidateValue::tmp:RRCCost6
    If tmp:RRCCost6:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('Prompt:RCost6') = '')
  ! --- STRING --- tmp:RRCCost6
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:RRCCost6'',''viewcosts_tmp:rrccost6_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:RRCCost6',p_web.GetSessionValue('tmp:RRCCost6'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:RRCCost6  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:RRCCost6:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('Prompt:RCost6') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost6') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Prompt:RCost6') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:RRCCost7  Routine
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost7') & '_prompt',Choose(p_web.GSV('Prompt:RCost7') = '','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Prompt:RCost7') = '','',p_web.Translate(p_web.GSV('Prompt:RCost7')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:RRCCost7  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:RRCCost7 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    tmp:RRCCost7 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::tmp:RRCCost7  ! copies value to session value if valid.
  do Value::tmp:RRCCost7
  do SendAlert
  do Comment::tmp:RRCCost7 ! allows comment style to be updated.

ValidateValue::tmp:RRCCost7  Routine
    If not (p_web.GSV('Prompt:RCost7') = '')
      if loc:invalid = '' then p_web.SetSessionValue('tmp:RRCCost7',tmp:RRCCost7).
    End

Value::tmp:RRCCost7  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Prompt:RCost7') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost7') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  If loc:retrying
    tmp:RRCCost7 = p_web.RestoreValue('tmp:RRCCost7')
    do ValidateValue::tmp:RRCCost7
    If tmp:RRCCost7:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('Prompt:RCost7') = '')
  ! --- STRING --- tmp:RRCCost7
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:RRCCost7'',''viewcosts_tmp:rrccost7_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:RRCCost7',p_web.GetSessionValue('tmp:RRCCost7'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:RRCCost7  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:RRCCost7:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('Prompt:RCost7') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost7') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Prompt:RCost7') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:RRCCost8  Routine
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost8') & '_prompt',Choose(p_web.GSV('Prompt:RCost8') = '','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Prompt:RCost8') = '','',p_web.Translate(p_web.GSV('Prompt:RCost8')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:RRCCost8  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:RRCCost8 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    tmp:RRCCost8 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::tmp:RRCCost8  ! copies value to session value if valid.
  do Value::tmp:RRCCost8
  do SendAlert
  do Comment::tmp:RRCCost8 ! allows comment style to be updated.

ValidateValue::tmp:RRCCost8  Routine
    If not (p_web.GSV('Prompt:RCost8') = '')
      if loc:invalid = '' then p_web.SetSessionValue('tmp:RRCCost8',tmp:RRCCost8).
    End

Value::tmp:RRCCost8  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Prompt:RCost8') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost8') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  If loc:retrying
    tmp:RRCCost8 = p_web.RestoreValue('tmp:RRCCost8')
    do ValidateValue::tmp:RRCCost8
    If tmp:RRCCost8:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('Prompt:RCost8') = '')
  ! --- STRING --- tmp:RRCCost8
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:RRCCost8'',''viewcosts_tmp:rrccost8_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:RRCCost8',p_web.GetSessionValue('tmp:RRCCost8'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:RRCCost8  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:RRCCost8:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('Prompt:RCost8') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost8') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Prompt:RCost8') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::jobe:ExcReplcamentCharge  Routine
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('jobe:ExcReplcamentCharge') & '_prompt',Choose(p_web.GSV('Hide:ExchangeReplacement') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:ExchangeReplacement') = 1,'',p_web.Translate('Exchange Replacement'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jobe:ExcReplcamentCharge  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jobe:ExcReplcamentCharge = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    jobe:ExcReplcamentCharge = p_web.GetValue('Value')
  End
  do ValidateValue::jobe:ExcReplcamentCharge  ! copies value to session value if valid.
  do Value::jobe:ExcReplcamentCharge
  do SendAlert
  do Comment::jobe:ExcReplcamentCharge ! allows comment style to be updated.

ValidateValue::jobe:ExcReplcamentCharge  Routine
    If not (p_web.GSV('Hide:ExchangeReplacement') = 1)
      if loc:invalid = '' then p_web.SetSessionValue('jobe:ExcReplcamentCharge',jobe:ExcReplcamentCharge).
    End

Value::jobe:ExcReplcamentCharge  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:ExchangeReplacement') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewCosts_' & p_web._nocolon('jobe:ExcReplcamentCharge') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If loc:retrying
    jobe:ExcReplcamentCharge = p_web.RestoreValue('jobe:ExcReplcamentCharge')
    do ValidateValue::jobe:ExcReplcamentCharge
    If jobe:ExcReplcamentCharge:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:ExchangeReplacement') = 1)
  ! --- CHECKBOX --- jobe:ExcReplcamentCharge
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''jobe:ExcReplcamentCharge'',''viewcosts_jobe:excreplcamentcharge_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('jobe:ExcReplcamentCharge') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','jobe:ExcReplcamentCharge',clip(1),,loc:readonly,,,loc:javascript,,'Exchange Replacement Charge') & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::jobe:ExcReplcamentCharge  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if jobe:ExcReplcamentCharge:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:ExchangeReplacement') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ViewCosts_' & p_web._nocolon('jobe:ExcReplcamentCharge') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:ExchangeReplacement') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('ViewCosts_nexttab_' & 0)
    tmp:ARCViewCostType = p_web.GSV('tmp:ARCViewCostType')
    do ValidateValue::tmp:ARCViewCostType
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:ARCViewCostType
      !do SendAlert
      do Comment::tmp:ARCViewCostType ! allows comment style to be updated.
      !exit
    End
    tmp:ARCIgnoreDefaultCharges = p_web.GSV('tmp:ARCIgnoreDefaultCharges')
    do ValidateValue::tmp:ARCIgnoreDefaultCharges
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:ARCIgnoreDefaultCharges
      !do SendAlert
      do Comment::tmp:ARCIgnoreDefaultCharges ! allows comment style to be updated.
      !exit
    End
    tmp:ARCIgnoreReason = p_web.GSV('tmp:ARCIgnoreReason')
    do ValidateValue::tmp:ARCIgnoreReason
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:ARCIgnoreReason
      !do SendAlert
      do Comment::tmp:ARCIgnoreReason ! allows comment style to be updated.
      !exit
    End
    tmp:ARCCost1 = p_web.GSV('tmp:ARCCost1')
    do ValidateValue::tmp:ARCCost1
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:ARCCost1
      !do SendAlert
      do Comment::tmp:ARCCost1 ! allows comment style to be updated.
      !exit
    End
    tmp:AdjustmentCost1 = p_web.GSV('tmp:AdjustmentCost1')
    do ValidateValue::tmp:AdjustmentCost1
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:AdjustmentCost1
      !do SendAlert
      do Comment::tmp:AdjustmentCost1 ! allows comment style to be updated.
      !exit
    End
    tmp:ARCCost2 = p_web.GSV('tmp:ARCCost2')
    do ValidateValue::tmp:ARCCost2
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:ARCCost2
      !do SendAlert
      do Comment::tmp:ARCCost2 ! allows comment style to be updated.
      !exit
    End
    tmp:AdjustmentCost2 = p_web.GSV('tmp:AdjustmentCost2')
    do ValidateValue::tmp:AdjustmentCost2
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:AdjustmentCost2
      !do SendAlert
      do Comment::tmp:AdjustmentCost2 ! allows comment style to be updated.
      !exit
    End
    tmp:ARCCost3 = p_web.GSV('tmp:ARCCost3')
    do ValidateValue::tmp:ARCCost3
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:ARCCost3
      !do SendAlert
      do Comment::tmp:ARCCost3 ! allows comment style to be updated.
      !exit
    End
    tmp:AdjustmentCost3 = p_web.GSV('tmp:AdjustmentCost3')
    do ValidateValue::tmp:AdjustmentCost3
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:AdjustmentCost3
      !do SendAlert
      do Comment::tmp:AdjustmentCost3 ! allows comment style to be updated.
      !exit
    End
    tmp:ARCCost4 = p_web.GSV('tmp:ARCCost4')
    do ValidateValue::tmp:ARCCost4
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:ARCCost4
      !do SendAlert
      do Comment::tmp:ARCCost4 ! allows comment style to be updated.
      !exit
    End
    tmp:AdjustmentCost4 = p_web.GSV('tmp:AdjustmentCost4')
    do ValidateValue::tmp:AdjustmentCost4
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:AdjustmentCost4
      !do SendAlert
      do Comment::tmp:AdjustmentCost4 ! allows comment style to be updated.
      !exit
    End
    tmp:ARCCost5 = p_web.GSV('tmp:ARCCost5')
    do ValidateValue::tmp:ARCCost5
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:ARCCost5
      !do SendAlert
      do Comment::tmp:ARCCost5 ! allows comment style to be updated.
      !exit
    End
    tmp:AdjustmentCost5 = p_web.GSV('tmp:AdjustmentCost5')
    do ValidateValue::tmp:AdjustmentCost5
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:AdjustmentCost5
      !do SendAlert
      do Comment::tmp:AdjustmentCost5 ! allows comment style to be updated.
      !exit
    End
    tmp:ARCCost6 = p_web.GSV('tmp:ARCCost6')
    do ValidateValue::tmp:ARCCost6
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:ARCCost6
      !do SendAlert
      do Comment::tmp:ARCCost6 ! allows comment style to be updated.
      !exit
    End
    tmp:AdjustmentCost6 = p_web.GSV('tmp:AdjustmentCost6')
    do ValidateValue::tmp:AdjustmentCost6
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:AdjustmentCost6
      !do SendAlert
      do Comment::tmp:AdjustmentCost6 ! allows comment style to be updated.
      !exit
    End
    tmp:ARCCost7 = p_web.GSV('tmp:ARCCost7')
    do ValidateValue::tmp:ARCCost7
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:ARCCost7
      !do SendAlert
      do Comment::tmp:ARCCost7 ! allows comment style to be updated.
      !exit
    End
    tmp:ARCCost8 = p_web.GSV('tmp:ARCCost8')
    do ValidateValue::tmp:ARCCost8
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:ARCCost8
      !do SendAlert
      do Comment::tmp:ARCCost8 ! allows comment style to be updated.
      !exit
    End
    jobe:ARC3rdPartyInvoiceNumber = p_web.GSV('jobe:ARC3rdPartyInvoiceNumber')
    do ValidateValue::jobe:ARC3rdPartyInvoiceNumber
    If loc:Invalid
      loc:retrying = 1
      do Value::jobe:ARC3rdPartyInvoiceNumber
      !do SendAlert
      do Comment::jobe:ARC3rdPartyInvoiceNumber ! allows comment style to be updated.
      !exit
    End
    tmp:ARCInvoiceNumber = p_web.GSV('tmp:ARCInvoiceNumber')
    do ValidateValue::tmp:ARCInvoiceNumber
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:ARCInvoiceNumber
      !do SendAlert
      do Comment::tmp:ARCInvoiceNumber ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('ViewCosts_nexttab_' & 1)
    tmp:RRCViewCostType = p_web.GSV('tmp:RRCViewCostType')
    do ValidateValue::tmp:RRCViewCostType
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:RRCViewCostType
      !do SendAlert
      do Comment::tmp:RRCViewCostType ! allows comment style to be updated.
      !exit
    End
    tmp:RRCIgnoreDefaultCharges = p_web.GSV('tmp:RRCIgnoreDefaultCharges')
    do ValidateValue::tmp:RRCIgnoreDefaultCharges
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:RRCIgnoreDefaultCharges
      !do SendAlert
      do Comment::tmp:RRCIgnoreDefaultCharges ! allows comment style to be updated.
      !exit
    End
    tmp:RRCIgnoreReason = p_web.GSV('tmp:RRCIgnoreReason')
    do ValidateValue::tmp:RRCIgnoreReason
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:RRCIgnoreReason
      !do SendAlert
      do Comment::tmp:RRCIgnoreReason ! allows comment style to be updated.
      !exit
    End
    tmp:RRCCost0 = p_web.GSV('tmp:RRCCost0')
    do ValidateValue::tmp:RRCCost0
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:RRCCost0
      !do SendAlert
      do Comment::tmp:RRCCost0 ! allows comment style to be updated.
      !exit
    End
    tmp:RRCCost1 = p_web.GSV('tmp:RRCCost1')
    do ValidateValue::tmp:RRCCost1
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:RRCCost1
      !do SendAlert
      do Comment::tmp:RRCCost1 ! allows comment style to be updated.
      !exit
    End
    tmp:originalInvoice = p_web.GSV('tmp:originalInvoice')
    do ValidateValue::tmp:originalInvoice
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:originalInvoice
      !do SendAlert
      do Comment::tmp:originalInvoice ! allows comment style to be updated.
      !exit
    End
    tmp:RRCCost2 = p_web.GSV('tmp:RRCCost2')
    do ValidateValue::tmp:RRCCost2
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:RRCCost2
      !do SendAlert
      do Comment::tmp:RRCCost2 ! allows comment style to be updated.
      !exit
    End
    tmp:RRCCost3 = p_web.GSV('tmp:RRCCost3')
    do ValidateValue::tmp:RRCCost3
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:RRCCost3
      !do SendAlert
      do Comment::tmp:RRCCost3 ! allows comment style to be updated.
      !exit
    End
    tmp:RRCCost4 = p_web.GSV('tmp:RRCCost4')
    do ValidateValue::tmp:RRCCost4
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:RRCCost4
      !do SendAlert
      do Comment::tmp:RRCCost4 ! allows comment style to be updated.
      !exit
    End
    tmp:RRCCost5 = p_web.GSV('tmp:RRCCost5')
    do ValidateValue::tmp:RRCCost5
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:RRCCost5
      !do SendAlert
      do Comment::tmp:RRCCost5 ! allows comment style to be updated.
      !exit
    End
    tmp:RRCCost6 = p_web.GSV('tmp:RRCCost6')
    do ValidateValue::tmp:RRCCost6
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:RRCCost6
      !do SendAlert
      do Comment::tmp:RRCCost6 ! allows comment style to be updated.
      !exit
    End
    tmp:RRCCost7 = p_web.GSV('tmp:RRCCost7')
    do ValidateValue::tmp:RRCCost7
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:RRCCost7
      !do SendAlert
      do Comment::tmp:RRCCost7 ! allows comment style to be updated.
      !exit
    End
    tmp:RRCCost8 = p_web.GSV('tmp:RRCCost8')
    do ValidateValue::tmp:RRCCost8
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:RRCCost8
      !do SendAlert
      do Comment::tmp:RRCCost8 ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('ViewCosts_nexttab_' & 2)
    jobe:ExcReplcamentCharge = p_web.GSV('jobe:ExcReplcamentCharge')
    do ValidateValue::jobe:ExcReplcamentCharge
    If loc:Invalid
      loc:retrying = 1
      do Value::jobe:ExcReplcamentCharge
      !do SendAlert
      do Comment::jobe:ExcReplcamentCharge ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_ViewCosts_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('ViewCosts_tab_' & 0)
    do GenerateTab0
  of lower('ViewCosts_tmp:ARCViewCostType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ARCViewCostType
      of event:timer
        do Value::tmp:ARCViewCostType
        do Comment::tmp:ARCViewCostType
      else
        do Value::tmp:ARCViewCostType
      end
  of lower('ViewCosts_tmp:ARCIgnoreDefaultCharges_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ARCIgnoreDefaultCharges
      of event:timer
        do Value::tmp:ARCIgnoreDefaultCharges
        do Comment::tmp:ARCIgnoreDefaultCharges
      else
        do Value::tmp:ARCIgnoreDefaultCharges
      end
  of lower('ViewCosts_tmp:ARCIgnoreReason_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ARCIgnoreReason
      of event:timer
        do Value::tmp:ARCIgnoreReason
        do Comment::tmp:ARCIgnoreReason
      else
        do Value::tmp:ARCIgnoreReason
      end
  of lower('ViewCosts_buttonAcceptARCReason_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonAcceptARCReason
      of event:timer
        do Value::buttonAcceptARCReason
        do Comment::buttonAcceptARCReason
      else
        do Value::buttonAcceptARCReason
      end
  of lower('ViewCosts_buttonCancelARCReason_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonCancelARCReason
      of event:timer
        do Value::buttonCancelARCReason
        do Comment::buttonCancelARCReason
      else
        do Value::buttonCancelARCReason
      end
  of lower('ViewCosts_tmp:ARCCost1_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ARCCost1
      of event:timer
        do Value::tmp:ARCCost1
        do Comment::tmp:ARCCost1
      else
        do Value::tmp:ARCCost1
      end
  of lower('ViewCosts_tmp:AdjustmentCost1_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:AdjustmentCost1
      of event:timer
        do Value::tmp:AdjustmentCost1
        do Comment::tmp:AdjustmentCost1
      else
        do Value::tmp:AdjustmentCost1
      end
  of lower('ViewCosts_tmp:ARCCost2_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ARCCost2
      of event:timer
        do Value::tmp:ARCCost2
        do Comment::tmp:ARCCost2
      else
        do Value::tmp:ARCCost2
      end
  of lower('ViewCosts_tmp:AdjustmentCost2_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:AdjustmentCost2
      of event:timer
        do Value::tmp:AdjustmentCost2
        do Comment::tmp:AdjustmentCost2
      else
        do Value::tmp:AdjustmentCost2
      end
  of lower('ViewCosts_tmp:ARCCost3_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ARCCost3
      of event:timer
        do Value::tmp:ARCCost3
        do Comment::tmp:ARCCost3
      else
        do Value::tmp:ARCCost3
      end
  of lower('ViewCosts_tmp:AdjustmentCost3_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:AdjustmentCost3
      of event:timer
        do Value::tmp:AdjustmentCost3
        do Comment::tmp:AdjustmentCost3
      else
        do Value::tmp:AdjustmentCost3
      end
  of lower('ViewCosts_tmp:ARCCost4_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ARCCost4
      of event:timer
        do Value::tmp:ARCCost4
        do Comment::tmp:ARCCost4
      else
        do Value::tmp:ARCCost4
      end
  of lower('ViewCosts_tmp:AdjustmentCost4_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:AdjustmentCost4
      of event:timer
        do Value::tmp:AdjustmentCost4
        do Comment::tmp:AdjustmentCost4
      else
        do Value::tmp:AdjustmentCost4
      end
  of lower('ViewCosts_tmp:ARCCost5_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ARCCost5
      of event:timer
        do Value::tmp:ARCCost5
        do Comment::tmp:ARCCost5
      else
        do Value::tmp:ARCCost5
      end
  of lower('ViewCosts_tmp:AdjustmentCost5_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:AdjustmentCost5
      of event:timer
        do Value::tmp:AdjustmentCost5
        do Comment::tmp:AdjustmentCost5
      else
        do Value::tmp:AdjustmentCost5
      end
  of lower('ViewCosts_tmp:ARCCost6_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ARCCost6
      of event:timer
        do Value::tmp:ARCCost6
        do Comment::tmp:ARCCost6
      else
        do Value::tmp:ARCCost6
      end
  of lower('ViewCosts_tmp:AdjustmentCost6_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:AdjustmentCost6
      of event:timer
        do Value::tmp:AdjustmentCost6
        do Comment::tmp:AdjustmentCost6
      else
        do Value::tmp:AdjustmentCost6
      end
  of lower('ViewCosts_tmp:ARCCost7_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ARCCost7
      of event:timer
        do Value::tmp:ARCCost7
        do Comment::tmp:ARCCost7
      else
        do Value::tmp:ARCCost7
      end
  of lower('ViewCosts_tmp:ARCCost8_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ARCCost8
      of event:timer
        do Value::tmp:ARCCost8
        do Comment::tmp:ARCCost8
      else
        do Value::tmp:ARCCost8
      end
  of lower('ViewCosts_tab_' & 1)
    do GenerateTab1
  of lower('ViewCosts_tmp:RRCViewCostType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:RRCViewCostType
      of event:timer
        do Value::tmp:RRCViewCostType
        do Comment::tmp:RRCViewCostType
      else
        do Value::tmp:RRCViewCostType
      end
  of lower('ViewCosts_tmp:RRCIgnoreDefaultCharges_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:RRCIgnoreDefaultCharges
      of event:timer
        do Value::tmp:RRCIgnoreDefaultCharges
        do Comment::tmp:RRCIgnoreDefaultCharges
      else
        do Value::tmp:RRCIgnoreDefaultCharges
      end
  of lower('ViewCosts_tmp:RRCIgnoreReason_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:RRCIgnoreReason
      of event:timer
        do Value::tmp:RRCIgnoreReason
        do Comment::tmp:RRCIgnoreReason
      else
        do Value::tmp:RRCIgnoreReason
      end
  of lower('ViewCosts_buttonAcceptRRCReason_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonAcceptRRCReason
      of event:timer
        do Value::buttonAcceptRRCReason
        do Comment::buttonAcceptRRCReason
      else
        do Value::buttonAcceptRRCReason
      end
  of lower('ViewCosts_buttonCancelRRCReason_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonCancelRRCReason
      of event:timer
        do Value::buttonCancelRRCReason
        do Comment::buttonCancelRRCReason
      else
        do Value::buttonCancelRRCReason
      end
  of lower('ViewCosts_tmp:RRCCost0_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:RRCCost0
      of event:timer
        do Value::tmp:RRCCost0
        do Comment::tmp:RRCCost0
      else
        do Value::tmp:RRCCost0
      end
  of lower('ViewCosts_tmp:RRCCost1_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:RRCCost1
      of event:timer
        do Value::tmp:RRCCost1
        do Comment::tmp:RRCCost1
      else
        do Value::tmp:RRCCost1
      end
  of lower('ViewCosts_tmp:originalInvoice_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:originalInvoice
      of event:timer
        do Value::tmp:originalInvoice
        do Comment::tmp:originalInvoice
      else
        do Value::tmp:originalInvoice
      end
  of lower('ViewCosts_tmp:RRCCost2_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:RRCCost2
      of event:timer
        do Value::tmp:RRCCost2
        do Comment::tmp:RRCCost2
      else
        do Value::tmp:RRCCost2
      end
  of lower('ViewCosts_tmp:RRCCost3_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:RRCCost3
      of event:timer
        do Value::tmp:RRCCost3
        do Comment::tmp:RRCCost3
      else
        do Value::tmp:RRCCost3
      end
  of lower('ViewCosts_tmp:RRCCost4_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:RRCCost4
      of event:timer
        do Value::tmp:RRCCost4
        do Comment::tmp:RRCCost4
      else
        do Value::tmp:RRCCost4
      end
  of lower('ViewCosts_tmp:RRCCost5_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:RRCCost5
      of event:timer
        do Value::tmp:RRCCost5
        do Comment::tmp:RRCCost5
      else
        do Value::tmp:RRCCost5
      end
  of lower('ViewCosts_tmp:RRCCost6_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:RRCCost6
      of event:timer
        do Value::tmp:RRCCost6
        do Comment::tmp:RRCCost6
      else
        do Value::tmp:RRCCost6
      end
  of lower('ViewCosts_tmp:RRCCost7_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:RRCCost7
      of event:timer
        do Value::tmp:RRCCost7
        do Comment::tmp:RRCCost7
      else
        do Value::tmp:RRCCost7
      end
  of lower('ViewCosts_tmp:RRCCost8_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:RRCCost8
      of event:timer
        do Value::tmp:RRCCost8
        do Comment::tmp:RRCCost8
      else
        do Value::tmp:RRCCost8
      end
  of lower('ViewCosts_tab_' & 2)
    do GenerateTab2
  of lower('ViewCosts_jobe:ExcReplcamentCharge_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe:ExcReplcamentCharge
      of event:timer
        do Value::jobe:ExcReplcamentCharge
        do Comment::jobe:ExcReplcamentCharge
      else
        do Value::jobe:ExcReplcamentCharge
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('ViewCosts_form:ready_',1)

  p_web.SetSessionValue('ViewCosts_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_ViewCosts',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('ViewCosts_form:ready_',1)
  p_web.SetSessionValue('ViewCosts_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_ViewCosts',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('ViewCosts_form:ready_',1)
  p_web.SetSessionValue('ViewCosts_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('ViewCosts:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('ViewCosts_form:ready_',1)
  p_web.SetSessionValue('ViewCosts_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('ViewCosts:Primed',0)
  p_web.setsessionvalue('showtab_ViewCosts',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
  If p_web.GSV('Hide:ARCCosts') <> 1
      If not (1=0)
          If p_web.IfExistsValue('tmp:ARCViewCostType')
            tmp:ARCViewCostType = p_web.GetValue('tmp:ARCViewCostType')
          End
      End
      If not (p_web.GSV('tmp:ARCViewCostType') = 'Manufacturer Payment' or p_web.GSV('tmp:ARCViewCostType') = '')
        If not (p_web.GSV('BookingSite') = 'RRC' Or Instring('- Invoiced',p_web.GSV('tmp:ARCViewCostType'),1,1) or p_web.GSV('ValidateIgnoreTickBoxARC') = 1)
          If p_web.IfExistsValue('tmp:ARCIgnoreDefaultCharges') = 0
            p_web.SetValue('tmp:ARCIgnoreDefaultCharges',0)
            tmp:ARCIgnoreDefaultCharges = 0
          Else
            tmp:ARCIgnoreDefaultCharges = p_web.GetValue('tmp:ARCIgnoreDefaultCharges')
          End
        End
      End
      If not (p_web.GSV('ValidateIgnoreTickBoxARC') <> 1)
          If p_web.IfExistsValue('tmp:ARCIgnoreReason')
            tmp:ARCIgnoreReason = p_web.GetValue('tmp:ARCIgnoreReason')
          End
      End
      If not (p_web.GSV('Prompt:Cost1') = '')
        If not (p_web.GSV('BookingSite') = 'RRC' Or p_web.GSV('ReadOnly:ARCCost1') = 1)
          If p_web.IfExistsValue('tmp:ARCCost1')
            tmp:ARCCost1 = p_web.GetValue('tmp:ARCCost1')
          End
        End
      End
      If not (p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced')
          If p_web.IfExistsValue('tmp:AdjustmentCost1')
            tmp:AdjustmentCost1 = p_web.GetValue('tmp:AdjustmentCost1')
          End
      End
      If not (p_web.GSV('Prompt:Cost2') = '')
        If not (p_web.GSV('BookingSite') = 'RRC' Or p_web.GSV('ReadOnly:ARCCost2') = 1)
          If p_web.IfExistsValue('tmp:ARCCost2')
            tmp:ARCCost2 = p_web.GetValue('tmp:ARCCost2')
          End
        End
      End
      If not (p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced')
          If p_web.IfExistsValue('tmp:AdjustmentCost2')
            tmp:AdjustmentCost2 = p_web.GetValue('tmp:AdjustmentCost2')
          End
      End
      If not (p_web.GSV('Prompt:Cost3') = '')
        If not (p_web.GSV('BookingSite') = 'RRC' Or p_web.GSV('ReadOnly:ARCCost3') = 1)
          If p_web.IfExistsValue('tmp:ARCCost3')
            tmp:ARCCost3 = p_web.GetValue('tmp:ARCCost3')
          End
        End
      End
      If not (p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced')
          If p_web.IfExistsValue('tmp:AdjustmentCost3')
            tmp:AdjustmentCost3 = p_web.GetValue('tmp:AdjustmentCost3')
          End
      End
      If not (p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced')
          If p_web.IfExistsValue('tmp:AdjustmentCost4')
            tmp:AdjustmentCost4 = p_web.GetValue('tmp:AdjustmentCost4')
          End
      End
      If not (p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced')
          If p_web.IfExistsValue('tmp:AdjustmentCost5')
            tmp:AdjustmentCost5 = p_web.GetValue('tmp:AdjustmentCost5')
          End
      End
      If not (p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced')
          If p_web.IfExistsValue('tmp:AdjustmentCost6')
            tmp:AdjustmentCost6 = p_web.GetValue('tmp:AdjustmentCost6')
          End
      End
  End
  If p_web.GSV('Hide:RRCCosts') <> 1
      If not (1=0)
        If not (p_web.GSV('ValidateIgnoreTickBox') = 1)
          If p_web.IfExistsValue('tmp:RRCViewCostType')
            tmp:RRCViewCostType = p_web.GetValue('tmp:RRCViewCostType')
          End
        End
      End
      If not (p_web.GSV('tmp:RRCViewCostType') = 'Credits' Or p_web.GSV('tmp:RRCViewCostType') = 'Handling' Or p_web.GSV('tmp:RRCViewCostType') = 'Exchange' Or p_web.GSV('tmp:RRCViewCostType') = '')
        If not (p_web.GSV('ValidateIgnoreTickBox') = 1 Or Instring('- Invoiced',p_web.GSV('tmp:RRCViewCostType'),1,1))
          If p_web.IfExistsValue('tmp:RRCIgnoreDefaultCharges') = 0
            p_web.SetValue('tmp:RRCIgnoreDefaultCharges',false)
            tmp:RRCIgnoreDefaultCharges = false
          Else
            tmp:RRCIgnoreDefaultCharges = p_web.GetValue('tmp:RRCIgnoreDefaultCharges')
          End
        End
      End
      If not (p_web.GSV('ValidateIgnoreTickBox') <> 1)
          If p_web.IfExistsValue('tmp:RRCIgnoreReason')
            tmp:RRCIgnoreReason = p_web.GetValue('tmp:RRCIgnoreReason')
          End
      End
      If not (p_web.GSV('Prompt:RCost1') = '')
        If not (p_web.GSV('ReadOnly:RRCCost1') = 1)
          If p_web.IfExistsValue('tmp:RRCCost1')
            tmp:RRCCost1 = p_web.GetValue('tmp:RRCCost1')
          End
        End
      End
      If not (p_web.GSV('Prompt:RCost2') = '')
        If not (p_web.GSV('ReadOnly:RRCCost2') = 1)
          If p_web.IfExistsValue('tmp:RRCCost2')
            tmp:RRCCost2 = p_web.GetValue('tmp:RRCCost2')
          End
        End
      End
      If not (p_web.GSV('Prompt:RCost3') = '')
        If not (p_web.GSV('ReadOnly:RRCCost3') = 1)
          If p_web.IfExistsValue('tmp:RRCCost3')
            tmp:RRCCost3 = p_web.GetValue('tmp:RRCCost3')
          End
        End
      End
  End
      If not (p_web.GSV('Hide:ExchangeReplacement') = 1)
          If p_web.IfExistsValue('jobe:ExcReplcamentCharge') = 0
            p_web.SetValue('jobe:ExcReplcamentCharge',0)
            jobe:ExcReplcamentCharge = 0
          Else
            jobe:ExcReplcamentCharge = p_web.GetValue('jobe:ExcReplcamentCharge')
          End
      End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  if (p_web.GSV('tmp:RRCViewCostType') = 'Chargeable')
      if (format(p_web.GSV('tmp:RRCCost2'),@n_14.2) > format(p_web.GSV('DefaultLabourCost'),@n_14.2))
          loc:Alert = 'Error! Labour Cost is higher than Default Cost'
          loc:Invalid = 'tmp:RRCCost2'
          exit
      end
  end ! 'tmp:RRCViewCostType'
  do addToAudit
  Do DeleteSessionValues
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('ViewCosts_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('ViewCosts_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
  If p_web.GSV('Hide:ARCCosts') <> 1
    loc:InvalidTab += 1
    do ValidateValue::tmp:ARCViewCostType
    If loc:Invalid then exit.
    do ValidateValue::__line1
    If loc:Invalid then exit.
    do ValidateValue::tmp:ARCIgnoreDefaultCharges
    If loc:Invalid then exit.
    do ValidateValue::tmp:ARCIgnoreReason
    If loc:Invalid then exit.
    do ValidateValue::buttonAcceptARCReason
    If loc:Invalid then exit.
    do ValidateValue::buttonCancelARCReason
    If loc:Invalid then exit.
    do ValidateValue::tmp:ARCCost1
    If loc:Invalid then exit.
    do ValidateValue::tmp:AdjustmentCost1
    If loc:Invalid then exit.
    do ValidateValue::tmp:ARCCost2
    If loc:Invalid then exit.
    do ValidateValue::tmp:AdjustmentCost2
    If loc:Invalid then exit.
    do ValidateValue::tmp:ARCCost3
    If loc:Invalid then exit.
    do ValidateValue::tmp:AdjustmentCost3
    If loc:Invalid then exit.
    do ValidateValue::__line2
    If loc:Invalid then exit.
    do ValidateValue::tmp:ARCCost4
    If loc:Invalid then exit.
    do ValidateValue::tmp:AdjustmentCost4
    If loc:Invalid then exit.
    do ValidateValue::tmp:ARCCost5
    If loc:Invalid then exit.
    do ValidateValue::tmp:AdjustmentCost5
    If loc:Invalid then exit.
    do ValidateValue::tmp:ARCCost6
    If loc:Invalid then exit.
    do ValidateValue::tmp:AdjustmentCost6
    If loc:Invalid then exit.
    do ValidateValue::tmp:ARCCost7
    If loc:Invalid then exit.
    do ValidateValue::tmp:ARCCost8
    If loc:Invalid then exit.
    do ValidateValue::jobe:ARC3rdPartyInvoiceNumber
    If loc:Invalid then exit.
    do ValidateValue::tmp:ARCInvoiceNumber
    If loc:Invalid then exit.
  End
  ! tab = 2
  If p_web.GSV('Hide:RRCCosts') <> 1
    loc:InvalidTab += 1
    do ValidateValue::tmp:RRCViewCostType
    If loc:Invalid then exit.
    do ValidateValue::__line3
    If loc:Invalid then exit.
    do ValidateValue::tmp:RRCIgnoreDefaultCharges
    If loc:Invalid then exit.
    do ValidateValue::tmp:RRCIgnoreReason
    If loc:Invalid then exit.
    do ValidateValue::buttonAcceptRRCReason
    If loc:Invalid then exit.
    do ValidateValue::buttonCancelRRCReason
    If loc:Invalid then exit.
    do ValidateValue::tmp:RRCCost0
    If loc:Invalid then exit.
    do ValidateValue::tmp:RRCCost1
    If loc:Invalid then exit.
    do ValidateValue::tmp:originalInvoice
    If loc:Invalid then exit.
    do ValidateValue::tmp:RRCCost2
    If loc:Invalid then exit.
    do ValidateValue::tmp:RRCCost3
    If loc:Invalid then exit.
    do ValidateValue::BrowseJobCredits
    If loc:Invalid then exit.
    do ValidateValue::__line4
    If loc:Invalid then exit.
    do ValidateValue::tmp:RRCCost4
    If loc:Invalid then exit.
    do ValidateValue::tmp:RRCCost5
    If loc:Invalid then exit.
    do ValidateValue::tmp:RRCCost6
    If loc:Invalid then exit.
    do ValidateValue::tmp:RRCCost7
    If loc:Invalid then exit.
    do ValidateValue::tmp:RRCCost8
    If loc:Invalid then exit.
  End
  ! tab = 3
    loc:InvalidTab += 1
    do ValidateValue::jobe:ExcReplcamentCharge
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('ViewCosts:Primed',0)
  p_web.StoreValue('tmp:ARCViewCostType')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:ARCIgnoreDefaultCharges')
  p_web.StoreValue('tmp:ARCIgnoreReason')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:ARCCost1')
  p_web.StoreValue('tmp:AdjustmentCost1')
  p_web.StoreValue('tmp:ARCCost2')
  p_web.StoreValue('tmp:AdjustmentCost2')
  p_web.StoreValue('tmp:ARCCost3')
  p_web.StoreValue('tmp:AdjustmentCost3')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:ARCCost4')
  p_web.StoreValue('tmp:AdjustmentCost4')
  p_web.StoreValue('tmp:ARCCost5')
  p_web.StoreValue('tmp:AdjustmentCost5')
  p_web.StoreValue('tmp:ARCCost6')
  p_web.StoreValue('tmp:AdjustmentCost6')
  p_web.StoreValue('tmp:ARCCost7')
  p_web.StoreValue('tmp:ARCCost8')
  p_web.StoreValue('tmp:ARCInvoiceNumber')
  p_web.StoreValue('tmp:RRCViewCostType')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:RRCIgnoreDefaultCharges')
  p_web.StoreValue('tmp:RRCIgnoreReason')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:RRCCost0')
  p_web.StoreValue('tmp:RRCCost1')
  p_web.StoreValue('tmp:originalInvoice')
  p_web.StoreValue('tmp:RRCCost2')
  p_web.StoreValue('tmp:RRCCost3')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:RRCCost4')
  p_web.StoreValue('tmp:RRCCost5')
  p_web.StoreValue('tmp:RRCCost6')
  p_web.StoreValue('tmp:RRCCost7')
  p_web.StoreValue('tmp:RRCCost8')

local.AddToTempQueue        Procedure(String fField,String fSaveField,String fReason,Byte fSaveCost,String fCost)
    Code
        clear(tmpaud:Record)
        tmpaud:sessionID = p_web.SessionID
        tmpaud:field = fField
        get(TempAuditQueue,tmpaud:keyField)
        if (error())
            tmpaud:Reason = fReason
            if (fSaveCost = 1)
                tmpaud:SaveCost = 1
                tmpaud:Cost = fCost
            end !if (fSaveCost = 1)
            add(tempAuditQueue)
        else ! if (error())
            tmpaud:Reason = fReason
            put(tempAuditQueue)
        end ! if (error())
