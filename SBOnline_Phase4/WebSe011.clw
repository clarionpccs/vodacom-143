

   MEMBER('WebServer_Phase4.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSE011.INC'),ONCE        !Local module procedure declarations
                     END


MSNRequired          PROCEDURE  (func:Manufacturer)        ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
MANUFACT::State  USHORT
FilesOpened     BYTE(0)
  CODE
    Do OpenFiles
    Do SaveFiles
    Return# = 0
    Access:MANUFACT.ClearKey(man:Manufacturer_Key)
    man:Manufacturer = func:Manufacturer
    If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
        If man:Use_MSN = 'YES'
            Return# = 1
        End!If man:Use_MSN = 'YES'
    End!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign

    Do RestoreFiles
    Do CloseFiles

    Return Return#
!--------------------------------------
OpenFiles  ROUTINE
  Access:MANUFACT.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANUFACT.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:MANUFACT.Close
     FilesOpened = False
  END
SaveFiles  ROUTINE
  MANUFACT::State = Access:MANUFACT.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF MANUFACT::State <> 0
    Access:MANUFACT.RestoreFile(MANUFACT::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
RemoveFromStockAllocation PROCEDURE  (func:PartRecordNumber,func:PartType) ! Declare Procedure
save_sto_id          USHORT,AUTO                           !
save_par_id          USHORT,AUTO                           !
save_wpr_id          USHORT,AUTO                           !
save_epr_id          USHORT,AUTO                           !
save_tra_id          USHORT,AUTO                           !
tmp:CurrentAccount   STRING(30)                            !Current Account Number
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
STOCKALL::State  USHORT
FilesOpened     BYTE(0)
  CODE
    !Part Record Number
    !Part Type
    !Quantity

    DO openfiles
    DO Savefiles
    Access:STOCKALL.ClearKey(stl:PartRecordTypeKey)
    stl:PartType         = func:PartType
    stl:PartRecordNumber = func:PartRecordNumber
    If Access:STOCKALL.TryFetch(stl:PartRecordTypeKey) = Level:Benign
        !Found
        Relate:STOCKALL.Delete(0)
    Else !If Access:STOCKALL.TryFetch(stl:PartRecordTypeKey) = Level:Benign
        !Error
    End !If Access:STOCKALL.TryFetch(stl:PartRecordTypeKey) = Level:Benign

    DO restorefiles
    DO closefiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:STOCKALL.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:STOCKALL.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:STOCKALL.Close
     FilesOpened = False
  END
SaveFiles  ROUTINE
  STOCKALL::State = Access:STOCKALL.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF STOCKALL::State <> 0
    Access:STOCKALL.RestoreFile(STOCKALL::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
PendingJob           PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
FilesOpened     BYTE(0)
  CODE
    do OpenFiles

    Access:MANUFACT.ClearKey(man:Manufacturer_Key)
    man:Manufacturer = p_web.GSV('job:Manufacturer')
    If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
        !Found
        If man:EDIFileType    = 'ALCATEL' Or  |
            man:EDIFileType   = 'BOSCH' Or    |
            man:EDIFileType   = 'ERICSSON' Or |
            man:EDIFileType   = 'LG' Or    |
            man:EDIFileType   = 'MAXON' Or    |
            man:EDIFileType   = 'MOTOROLA' Or |
            man:EDIFileType   = 'NEC' Or      |
            man:EDIFileType   = 'NOKIA' Or    |
            man:EDIFileType   = 'SIEMENS' Or  |
            man:EDIFileType   = 'SAMSUNG' Or  |
            man:EDIFileType   = 'SAMSUNG SA' Or  |
            man:EDIFileType   = 'MITSUBISHI' Or   |
            man:EDIFileType   = 'TELITAL' Or  |
            man:EDIFileType   = 'SAGEM' Or    |
            man:EDIFileType   = 'SONY' Or     |
            man:EDIFileType   = 'PHILIPS' Or     |
            man:EDIFileType   = 'PANASONIC'

            Access:TRADEACC.ClearKey(tra:Account_Number_Key)
            tra:Account_Number = p_web.GSV('wob:HeadAccountNumber')
            If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                !Found
            Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                !Error
            End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign

            !Job should never becaome an EDI exception by mistake
            !So no need to double check if it's edi is set to exception

            case p_web.GSV('job:EDI')
            of 'EXC' orof 'REJ'
                p_web.SSV('wob:EDI',p_web.GSV('job:EDI'))
                return
            of 'EXM' orof 'AAJ'
                return
            end ! case p_web.GSV('job:EDI')

            !Do the calculation to check if a job should appear in the Pending Claims Table
            ! Job must be completed, a warranty job, and not already have a batch number. Not be an exception, a bouncer and not be excluded (by Charge TYpe or Repair Type)

            found# = 0
            If p_web.GSV('job:Date_Completed') <> ''
!Stop(2)
                If p_web.GSV('job:Warranty_Job') = 'YES'
!Stop(3)
                    If p_web.GSV('job:EDI_Batch_Number') = 0
!Stop(4)
                        If p_web.GSV('job:EDI') <> 'AAJ'
!Stop(5)
                            If p_web.GSV('job:EDI') <> 'EXC'
!Stop(6)
                                If ~(p_web.GSV('job:Bouncer') <> '' And (p_web.GSV('job:Bouncer_Type') = 'BOT' Or p_web.GSV('job:Bouncer_Type') = 'WAR'))
!Stop(7)
                                    Access:CHARTYPE.ClearKey(cha:Charge_Type_Key)
                                    cha:Charge_Type = p_web.GSV('job:Warranty_Charge_Type')
                                    If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
                                        !Found
                                        If cha:Exclude_EDI <> 'YES'
!Stop(8)
                                            Access:REPTYDEF.ClearKey(rtd:WarManRepairTypeKey)
                                            rtd:Manufacturer = p_web.GSV('job:Manufacturer')
                                            rtd:Warranty = 'YES'
                                            rtd:Repair_Type = p_web.GSV('job:Repair_Type_Warranty')
                                            If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
                                                !Found
                                                If ~rtd:ExcludeFromEDI
!Stop(9)
                                                    If p_web.GSV('wob:EDI') <> 'PAY' And p_web.GSV('wob:EDI') <> 'AAJ' And p_web.GSV('wob:EDI') <> 'APP'
                                                        p_web.SSV('wob:EDI','NO')
                                                    End ! If wob:EDI <> 'PAY' And wob:EDI <> 'AAJ' And wob:EDI <> 'APP'

                                                    !Check if the job is a 48 Hour Scrap/RTM with only one exchange - 4449
                                                    ScrapCheck# = 0
                                                    ! Is this a 48 hour job? (DBH: 05/02/2007)
                                                    If p_web.GSV('jobe:Engineer48HourOption') = 1
                                                        ! Is there only one exchange attached? (DBH: 05/02/2007)
                                                        If p_web.GSV('job:Exchange_Unit_Number') > 0
                                                            If p_web.GSV('jobe:SecondExchangeNumber') = 0
                                                                If p_web.GSV('job:Repair_Type_Warranty') = 'R.T.M.' Or p_web.GSV('job:Repair_Type_Warranty') = 'SCRAP'
                                                                    ScrapCheck# = 1
                                                                    ! Do not claim for this job (DBH: 05/02/2007)
                                                                End ! If job:Repair_Type_Warranty = 'R.T.M.' Or job:Repair_Type_Warranty = 'SCRAP'
                                                            End ! If jobe:SecondExchangeNumber = 0
                                                        End ! If job:Exchange_Unit_Number > 0
                                                    End ! If jobe:Engineer48HourOption = 1

                                                    If ScrapCheck# = 0
!Stop(10)
                                                        If p_web.GSV('job:EDI') <> 'NO'
                                                            p_web.SSV('jobe2:InPendingDate',Today())
                                                        End ! If job:EDI <> 'NO'
                                                        p_web.SSV('job:EDI','NO')
                                                        found# = 1
                                                    End ! If ScrapCheck# = 0
                                                End ! If ~rtd:ExcludeFromEDI
                                            Else ! If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
                                                !Error
                                            End ! If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign

                                        End ! If cha:Exclude_EDI <> 'YES'
                                    Else ! If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
                                        !Error
                                    End ! If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign

                                End ! If (~job:Bouncer <> '' And (job:Bouncer_Type = 'BOT' Or job:Bouncer_Type = 'WAR'))
                            Else ! If job:EDI <> 'EXC'
                                if (p_web.GSV('BookingSite') = 'RRC')
                                    p_web.SSV('wob:EDI','AAJ')
                                    p_web.SSV('job:EDI','AAJ')
                                Else ! If glo:WebJob
                                    p_web.SSV('job:EDI','EXC')
                                End ! If glo:WebJob
                                found# = 1
                            End ! If job:EDI <> 'EXC'
                        Else ! If job:EDI <> 'AAJ'
                        End ! If job:EDI <> 'AAJ'
                    Else ! If job:EDI_Batch_Number = 0
                        ! This is an error check (DBH: 05/02/2007)
                        ! If the job has a batch number then it can only be 3 things. Invoice, Reconciled or exception (DBH: 05/02/2007)
                        If p_web.GSV('job:Invoice_Number_Warranty') <> ''
                            If p_web.GSV('wob:EDI') <> 'PAY' And p_web.GSV('wob:EDI') <> 'AAJ' And p_web.GSV('wob:EDI') <> 'APP'
                                p_web.SSV('wob:EDI','FIN')
                            End ! If wob:EDI <> 'PAY' And wob:EDI <> 'AAJ' And wob:EDI <> 'APP'
                            p_web.SSV('job:EDI','FIN')
                        Else ! If job:Invoice_Number_Warranty <> ''
                            If p_web.GSV('wob:EDI') <> 'PAY' And p_web.GSV('wob:EDI') <> 'AAJ' And p_web.GSV('wob:EDI') <> 'APP'
                                p_web.SSV('wob:EDI','YES')
                            End ! If wob:EDI <> 'PAY' And wob:EDI <> 'AAJ' And wob:EDI <> 'APP'
                            p_web.SSV('job:EDI','YES')
                        End ! If job:Invoice_Number_Warranty <> ''
                        found# = 1
                    End ! If job:EDI_Batch_Number = 0
                End ! If job:Warranty_Job = 'YES'
            End ! If job:Date_Completed <> ''
        End ! man:EDIFileType   = 'PANASONIC'

        ! Make sure the EDI value doesn't change once it's set (DBH: 05/02/2007)

        ! Reconciled (DBH: 05/02/2007)
        If (found# = 0)
            If (p_web.GSV('job:EDI') = 'YES')
                If p_web.GSV('wob:EDI') <> 'PAY' And p_web.GSV('wob:EDI') <> 'AAJ' And p_web.GSV('wob:EDI') = 'APP'
                    p_web.SSV('wob:EDI','YES')
                End ! If wob:EDI <> 'PAY' And wob:EDI <> 'AAJ' And wob:EDI = 'APP'
                p_web.SSV('job:EDI','YES')
                found# = 1
            End ! If job:EDI = 'YES'
        End ! If Clip(tmp:Return) = ''

        ! Invoiced (DBH: 05/02/2007)
        If (found# = 0)
            If p_web.GSV('job:EDI') = 'FIN'
                If p_web.GSV('wob:EDI') <> 'PAY' And p_web.GSV('wob:EDI') <> 'AAJ' And p_web.GSV('wob:EDI') <> 'APP'
                    p_web.SSV('wob:EDI','FIN')
                End ! If wob:EDI <> 'PAY' And wob:EDI <> 'AAJ' And wob:EDI <> 'APP'
                p_web.SSV('job:EDI','FIN')
                found# = 1
            End ! If job:EDI = 'FIN'
        End ! If Clip(tmp:Return) = ''


        ! If all else failes, then this shouldn't be a warranty claim (DBH: 05/02/2007)
        If (found# = 0)
            If p_web.GSV('wob:EDI') <> 'PAY' And p_web.GSV('wob:EDI') <> 'AAJ' And p_web.GSV('wob:EDI') <> 'APP'
                p_web.SSV('wob:EDI','XXX')
            End ! If wob:EDI <> 'PAY' And wob:EDI <> 'AAJ' And wob:EDI <> 'APP'
            p_web.SSV('job:EDI','XXX')
            found# = 1
        End ! If Clip(tmp:Return) = ''

        ! Inserting (DBH 05/02/2007) # 8707 - Write warranty information to new file
        Access:JOBSWARR.ClearKey(jow:RefNumberKey)
        jow:RefNumber = p_web.GSV('job:Ref_Number')
        If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign
            !Found
        Else ! If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign
            !Error
            If Access:JOBSWARR.PrimeRecord() = Level:Benign
                jow:RefNumber = p_web.GSV('job:Ref_Number')
                If Access:JOBSWARR.TryInsert() = Level:Benign
                    !Insert
                Else ! If Access:JOBSWARR.TryInsert() = Level:Benign
                    Access:JOBSWARR.CancelAutoInc()
                End ! If Access:JOBSWARR.TryInsert() = Level:Benign
            End ! If Access.JOBSWARR.PrimeRecord() = Level:Benign
        End ! If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign


        If (p_web.GSV('job:EDI') = 'XXX' Or found#= 0)
            Relate:JOBSWARR.Delete(0)
        Else ! If tmp:Return = 'XXX'
            jow:Status = p_web.GSV('job:EDI')
            jow:BranchID = tra:BranchIdentification
!            If SentToHub(job:Ref_Number)
!                jow:RepairedAt = 'ARC'
!            Else ! If SentToHub(job:Ref_Number)
!                jow:RepairedAt = 'RRC'
!            End ! If SentToHub(job:Ref_Number)
            jow:Manufacturer = p_web.GSV('job:Manufacturer')

            Access:CHARTYPE.ClearKey(cha:Warranty_Key)
            cha:Warranty = 'YES'
            cha:Charge_Type = p_web.GSV('job:Warranty_Charge_Type')
            If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
                !Found
            Else ! If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
                !Error
            End ! If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
            If cha:SecondYearWarranty
                jow:FirstSecondYear = 1
            Else ! If cha:SecondYearWarranty
                jow:FirstSecondYear = 0
            End ! If cha:SecondYearWarranty

            If (p_web.GSV('job:EDI') = 'NO')
                If jow:Submitted = 0
                    jow:Submitted = 1
                End ! If jow:Submitted = 0
            End ! If tmp:Return = 'NO'
            Access:JOBSWARR.Update()
        End ! If tmp:Return = 'XXX'
        ! End (DBH 05/02/2007) #8707

    Else ! If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
        !Error
    End ! If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign

    do CloseFiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:CHARTYPE.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:CHARTYPE.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:REPTYDEF.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:REPTYDEF.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:MANUFACT.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANUFACT.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSWARR.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSWARR.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:CHARTYPE.Close
     Access:REPTYDEF.Close
     Access:MANUFACT.Close
     Access:JOBSWARR.Close
     Access:TRADEACC.Close
     FilesOpened = False
  END
LockRecord           PROCEDURE  (fJobNumber,fSessionValue,fType) ! Declare Procedure
JOBSLOCK::State  USHORT
FilesOpened     BYTE(0)
  CODE
    ! Clear blanks
    Access:JOBSLOCK.Clearkey(lock:JobNumberKey)
    lock:JobNumber = 0
    IF (Access:JOBSLOCK.Tryfetch(lock:JobNumberKey) = LEvel:Benign)
        Access:JOBSLOCK.DeleteRecord(0)
    END ! IF

    ! Ignore if zero job sent
    IF (fJobNumber = 0)
        RETURN
    END
    do openFiles

    access:JOBSLOCK.clearkey(lock:jobnumberkey)
    lock:jobNumber = fjobnumber
    if (access:jobslock.tryfetch(lock:jobnumberkey) = level:benign)
        if (fType = 0) ! Add Lock
            lock:datelocked = today()
            lock:timelocked = clock()
            lock:SessionValue = fsessionvalue
            access:JOBSLOCK.tryUpdate()
        else
            access:jobslock.deleterecord(0)
        end

    else
        if (fType = 0) ! Add Lock
            if (access:jobslock.primerecord() = level:benign)
                lock:jobNumber = fjobnumber
                lock:sessionValue = fsessionvalue
                if (access:jobslock.tryinsert() = level:benign)
                else
                    access:jobslock.cancelautoinc()
                end
            end
        end
    end
    do closefiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:JOBSLOCK.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSLOCK.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:JOBSLOCK.Close
     FilesOpened = False
  END
SaveFiles  ROUTINE
  JOBSLOCK::State = Access:JOBSLOCK.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF JOBSLOCK::State <> 0
    Access:JOBSLOCK.RestoreFile(JOBSLOCK::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
ExcludeHandlingFee   PROCEDURE  (func:Type,func:Manufacturer,func:RepairType) ! Declare Procedure
FilesOpened     BYTE(0)
  CODE
    do OpenFiles

    rtnValue# = 0
    !Return Fatal if the Handling Fee should be excluded
    Case func:Type
        Of 'C'
            Access:REPTYDEF.ClearKey(rtd:ChaManRepairTypeKey)
            rtd:Manufacturer = func:Manufacturer
            rtd:Chargeable   = 'YES'
            rtd:Repair_Type  = func:RepairType
            If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
                !Found
                IF rtd:ExcludeHandlingFee
                    rtnValue# = 1
                End !IF rtd:ExcludeHandlingFee
            Else!If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End!If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
        Of 'W'
            Access:REPTYDEF.ClearKey(rtd:WarManRepairTypeKey)
            rtd:Manufacturer = func:Manufacturer
            rtd:Warranty     = 'YES'
            rtd:Repair_Type  = func:RepairType
            If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
                !Found
                If rtd:ExcludeHandlingFee
                    rtnValue# = 1
                End !If rtd:ExcludeHandlingFee
            Else!If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End!If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
    End !Case func:Type

    do closeFiles
    Return rtnValue#
!--------------------------------------
OpenFiles  ROUTINE
  Access:REPTYDEF.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:REPTYDEF.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:REPTYDEF.Close
     FilesOpened = False
  END
