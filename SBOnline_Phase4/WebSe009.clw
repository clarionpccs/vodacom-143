

   MEMBER('WebServer_Phase4.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSE009.INC'),ONCE        !Local module procedure declarations
                     END


Despatch:Job         PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
FilesOpened     BYTE(0)
  CODE
    DO OpenFiles
    if (p_web.GSV('BookingSite') = 'RRC')
        p_web.SSV('GetStatus:Type','JOB')
        IF (p_web.GSV('job:Who_Booked') = 'WEB')
            p_web.SSV('GetStatus:StatusNumber',Sub(GETINI('RRC','StatusSentToPUP',,Clip(Path()) & '\SB2KDEF.INI'),1,3))

            ! Add To COnsignment History
            IF (Access:JOBSCONS.PrimeRecord() = Level:Benign)
                joc:RefNumber = p_web.GSV('job:Ref_Number')
                joc:TheDate = TODAY()
                joc:TheTime = CLOCK()
                joc:UserCode = p_web.GSV('BookingUserCode')
                joc:DespatchFrom = 'RRC'
                joc:DespatchTo = 'PUP'
                joc:Courier = p_web.GSV('job:Courier')
                joc:ConsignmentNumber = p_web.GSV('locWaybillNumber')
                joc:DespatchType = 'JOB'
                IF (Access:JOBSCONS.TryInsert())
                    Access:JOBSCONS.CancelAutoInc()
                END
            END

            p_web.SSV('LocationChange:Location',Clip(GETINI('RRC','InTransitToPUPLocation',,CLIP(PATH())&'\SB2KDEF.INI')))
        ELSE ! IF (p_web.GSV('job:Who_Booked') = 'WEB')
            IF (p_web.GSV('job:Paid') = 'YES' OR (p_web.GSV('job:Chargeable_Job') = 'YES' AND p_web.GSV('jobe:RRCCSubTotal') = 0))
                p_web.SSV('GetStatus:StatusNumber','910')
            ELSE
                p_web.SSV('GetStatus:StatusNumber','905')
            END

            ! Add To COnsignment History
            IF (Access:JOBSCONS.PrimeRecord() = Level:Benign)
                joc:RefNumber = p_web.GSV('job:Ref_Number')
                joc:TheDate = TODAY()
                joc:TheTime = CLOCK()
                joc:UserCode = p_web.GSV('BookingUserCode')
                joc:DespatchFrom = 'RRC'
                joc:DespatchTo = 'CUSTOMER'
                joc:Courier = p_web.GSV('job:Courier')
                joc:ConsignmentNumber = p_web.GSV('locWaybillNumber')
                joc:DespatchType = 'JOB'
                IF (Access:JOBSCONS.TryInsert())
                    Access:JOBSCONS.CancelAutoInc()
                END
            END
            p_web.SSV('LocationChange:Location',Clip(GETINI('RRC','DespatchToCustomer',,CLIP(PATH())&'\SB2KDEF.INI')))
        END

        GetStatus(p_web.GSV('GetStatus:StatusNumber'),0,p_web.GSV('GetStatus:Type'),p_web)
        LocationChange(p_web)

        IF (p_web.GSV('job:Loan_Unit_Number') <> 0)
            p_web.SSV('GetStatus:StatusNumber',812)
            p_web.SSV('GetStatus:Type','LOA')

            ! Loan Collection Note???
            ! MissingFor Noe
            !!
        END

        ! Update Job

        p_web.SSV('wob:DateJobDespatched',TODAY())
        p_web.SSV('jobe:DespatchType','')
        p_web.SSV('jobe:Despatched','')
        p_web.SSV('wob:ReadyToDespatch',0)

        ! Update Files

        IF (p_web.GSV('jobe:VSACustomer') = 1)
            CID_XML(p_web.GSV('job:Mobile_Number'),p_web.GSV('wob:HeadAccountNumber'),2)
        END

        Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
        sub:Account_Number = p_web.GSV('job:Account_Number')
        IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
            Access:TRADEACC.ClearKey(tra:Account_Number_Key)
            tra:Account_Number = sub:Main_Account_Number
            IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
                IF (tra:Use_Sub_Accounts = 'YES')
                    IF (sub:Print_Despatch_Despatch = 'YES' AND |
                        sub:Despatch_Note_Per_Item = 'YES')
                        p_web.SSV('Hide:PrintDespatchNote',0)
                    END

                ELSE
                    IF (tra:Print_Despatch_Despatch = 'YES' AND |
                        tra:Despatch_Note_Per_Item = 'YES')
                        p_web.SSV('Hide:PrintDespatchNote',0)
                    END
                END
            END
        END

        p_web.SSV('AddToAudit:Action','DESPATCH FROM RRC')
        p_web.SSV('AddToAudit:Notes','COURIER: ' & p_web.GSV('job:Courier') & |
            '<13,10>WAYBILL NO: ' & p_web.GSV('locWaybillNumber'))

    ELSE ! if (p_web.GSV('BookingSite') = 'RRC')
        p_web.SSV('job:Date_Despatched',Today())
        p_web.SSV('job:Despatched','YES')
        p_web.SSV('job:Consignment_Number',p_web.GSV('locWaybillNumber'))
        if (p_web.GSV('jobe:WebJob') = 1)
            p_web.SSV('GetStatus:StatusNumber',Sub(GETINI('RRC','StatusDespatchedToRRC',,CLIP(PATH())&'\SB2KDEF.INI'),1,3))
            p_web.SSV('GetStatus:Type','JOB')
            GetStatus(p_web.GSV('GetStatus:StatusNumber'),0,p_web.GSV('GetStatus:Type'),p_web)
            ! Add To COnsignment History
            IF (Access:JOBSCONS.PrimeRecord() = Level:Benign)
                joc:RefNumber = p_web.GSV('job:Ref_Number')
                joc:TheDate = TODAY()
                joc:TheTime = CLOCK()
                joc:UserCode = p_web.GSV('BookingUserCode')
                joc:DespatchFrom = 'ARC'
                joc:DespatchTo = 'RRC'
                joc:Courier = p_web.GSV('job:Courier')
                joc:ConsignmentNumber = p_web.GSV('locWaybillNumber')
                joc:DespatchType = 'JOB'
                IF (Access:JOBSCONS.TryInsert())
                    Access:JOBSCONS.CancelAutoInc()
                END
            END
        ELSE !if (p_web.GSV('jobe:WebJob') = 1)
            ! Add To COnsignment History
            IF (Access:JOBSCONS.PrimeRecord() = Level:Benign)
                joc:RefNumber = p_web.GSV('job:Ref_Number')
                joc:TheDate = TODAY()
                joc:TheTime = CLOCK()
                joc:UserCode = p_web.GSV('BookingUserCode')
                joc:DespatchFrom = 'ARC'
                joc:DespatchTo = 'RRC'
                joc:Courier = p_web.GSV('job:Courier')
                joc:ConsignmentNumber = p_web.GSV('locWaybillNumber')
                joc:DespatchType = 'JOB'
                IF (Access:JOBSCONS.TryInsert())
                    Access:JOBSCONS.CancelAutoInc()
                END
            END

            paid# = 1
            if (p_web.GSV('job:Chargeable_Job') = 'YES' AND p_web.GSV('job:Paid') <> 'YES')
                paid# = 0
            END
            IF (paid# = 1 AND p_web.GSV('job:Warranty_Job') = 'YES' AND p_web.GSV('job:Paid_Warranty') <> 'YES')
                paid# = 0
            END
            IF (paid# = 1)

                GetStatus(910,0,'JOB',p_web) ! Despatch Paid
            ELSE

                GetStatus(905,0,'JOB',p_web) ! Despatch Unpaid
            END

            if (p_web.GSV('job:Loan_Unit_Number') <> 0)

                GetStatus(812,0,'LOA',p_web)
            END


        end
        p_web.SSV('AddToAudit:Action','JOB DESPATCHED VIA ' & p_web.GSV('job:Courier'))
        p_web.SSV('AddToAudit:Notes','CONSIGNMENT NUMBER: ' & Clip(p_web.GSV('locWaybillNumber')))
    END ! if (p_web.GSV('BookingSite') = 'RRC')

    p_web.SSV('jobe:JobSecurityPackNo',p_web.GSV('locSecurityPackID'))

    ! Add To Audit
    p_web.SSV('AddToAudit:Type','JOB')
    IF (p_web.GSV('locSecurityPackID') <> '')
        p_web.SSV('AddToAudit:Notes',p_web.GSV('AddToAudit:Notes') & |
            '<13,10>SECURITY PACK NO: ' & p_web.GSV('locSecurityPackID'))
    END
    AddToAudit(p_web)

    DO CloseFiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:TRADEACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBTRACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBTRACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSCONS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSCONS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:TRADEACC.Close
     Access:SUBTRACC.Close
     Access:JOBSCONS.Close
     FilesOpened = False
  END
Despatch:Exc         PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
FilesOpened     BYTE(0)
  CODE
    Do OpenFiles
    if (p_web.GSV('BookingSite') = 'RRC')
        IF (p_web.GSV('job:Who_Booked') = 'WEB')
            p_web.SSV('GetStatus:StatusNumber',468)
            ! Add To COnsignment History
            IF (Access:JOBSCONS.PrimeRecord() = Level:Benign)
                joc:RefNumber = p_web.GSV('job:Ref_Number')
                joc:TheDate = TODAY()
                joc:TheTime = CLOCK()
                joc:UserCode = p_web.GSV('BookingUserCode')
                joc:DespatchFrom = 'RRC'
                joc:DespatchTo = 'PUP'
                joc:Courier = p_web.GSV('job:Exchange_Courier')
                joc:ConsignmentNumber = p_web.GSV('locWaybillNumber')
                joc:DespatchType = 'EXC'
                IF (Access:JOBSCONS.TryInsert())
                    Access:JOBSCONS.CancelAutoInc()
                END
            END
        ELSE ! IF (p_web.GSV('job:Who_Booked') = 'WEB')
            p_web.SSV('GetStatus:StatusNumber',901)
            ! Add To COnsignment History
            IF (Access:JOBSCONS.PrimeRecord() = Level:Benign)
                joc:RefNumber = p_web.GSV('job:Ref_Number')
                joc:TheDate = TODAY()
                joc:TheTime = CLOCK()
                joc:UserCode = p_web.GSV('BookingUserCode')
                joc:DespatchFrom = 'RRC'
                joc:DespatchTo = 'CUSTOMER'
                joc:Courier = p_web.GSV('job:Exchange_Courier')
                joc:ConsignmentNumber = p_web.GSV('locWaybillNumber')
                joc:DespatchType = 'EXC'
                IF (Access:JOBSCONS.TryInsert())
                    Access:JOBSCONS.CancelAutoInc()
                END
            END
        END ! IF (p_web.GSV('job:Who_Booked') = 'WEB')
        p_web.SSV('GetStatus:Type','EXC')
        GetStatus(p_web.GSV('GetStatus:StatusNumber'),0,p_web.GSV('GetStatus:Type'),p_web)

        p_web.SSV('wob:ReadyToDespatch',0)


        p_web.SSV('AddToAudit:Action','DESPATCH FROM RRC')
        p_web.SSV('AddToAudit:Notes','UNIT NO: ' & p_web.GSV('job:Exchange_Unit_Number') & |
            '<13,10>COURIER: ' & p_web.GSV('job:Loan_Courier') & |
            '<13,10>WAYBILL NO: ' & p_web.GSV('locWaybillNumber'))


        IF (p_web.GSV('jobe:VSACustomer') = 1)
            CID_XML(p_web.GSV('job:Mobile_Number'),p_web.GSV('wob:HeadAccountNumber'),2)
        END
    ELSE ! if (p_web.GSV('BookingSite') = 'RRC')
        p_web.SSV('job:Exchange_Consignment_Number',p_web.GSV('locWaybillNumber'))
        p_web.SSV('job:Exchange_Despatched',TOday())
        p_web.SSV('job:Despatched','')
        p_web.SSV('job:Exchange_Despatched_User',p_web.GSV('BookingUserCode'))

        if (p_web.GSV('jobe:WebJob') = 1)
            p_web.SSV('wob:ExcWaybillNumber',p_web.GSV('locWaybillNumber'))

            p_web.SSV('GetStatus:StatusNumber',Sub(GETINI('RRC','ExchangeStatusDespatchToRRC',,CLIP(PATH())&'\SB2KDEF.INI'),1,3))

            ! Add To COnsignment History
            IF (Access:JOBSCONS.PrimeRecord() = Level:Benign)
                joc:RefNumber = p_web.GSV('job:Ref_Number')
                joc:TheDate = TODAY()
                joc:TheTime = CLOCK()
                joc:UserCode = p_web.GSV('BookingUserCode')
                joc:DespatchFrom = 'ARC'
                joc:DespatchTo = 'RRC'
                joc:Courier = p_web.GSV('job:Exchange_Courier')
                joc:ConsignmentNumber = p_web.GSV('locWaybillNumber')
                joc:DespatchType = 'EXC'
                IF (Access:JOBSCONS.TryInsert())
                    Access:JOBSCONS.CancelAutoInc()
                END
            END
        ELSE ! if (p_web.GSV('jobe:WebJob') = 1)
            p_web.SSV('GetStatus:StatusNumber',901)
            ! Add To COnsignment History
            IF (Access:JOBSCONS.PrimeRecord() = Level:Benign)
                joc:RefNumber = p_web.GSV('job:Ref_Number')
                joc:TheDate = TODAY()
                joc:TheTime = CLOCK()
                joc:UserCode = p_web.GSV('BookingUserCode')
                joc:DespatchFrom = 'ARC'
                joc:Courier = p_web.GSV('job:Exchange_Courier')
                joc:ConsignmentNumber = p_web.GSV('locWaybillNumber')
                joc:DespatchType = 'EXC'
                IF (Access:JOBSCONS.TryInsert())
                    Access:JOBSCONS.CancelAutoInc()
                END
            END
        END ! if (p_web.GSV('jobe:WebJob') = 1)

        p_web.SSV('AddToAudit:Action','EXCHANGE UNIT DESPATCH VIA ' & p_web.GSV('job:Exchange_Courier'))
        p_web.SSV('AddToAudit:Notes','CONSIGNMENT NOTE NUMBER: ' & p_web.GSV('locWaybillNumber'))

        Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
        xch:Ref_Number = p_web.GSV('job:Exchange_Unit_Number')
        if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
            xch:Available = 'DES'
            xch:StatusChangeDate = TODAY() ! #12127 Record status change. (Bryan: 13/07/2011)
            if (Access:EXCHANGE.TryUpdate() = Level:Benign)

                if (Access:EXCHHIST.PrimeRecord() = Level:Benign)
                    exh:Ref_Number = xch:Ref_Number
                    exh:Date = Today()
                    exh:Time = Clock()
                    exh:User = p_web.GSV('BookingUserCode')
                    exh:Status = 'UNIT DESPATCHED ON JOB: ' & p_web.GSV('job:ref_Number')
                    Access:EXCHHIST.TryInsert()
                END
            END
        END

    END ! if (p_web.GSV('BookingSite') = 'RRC')

    p_web.SSV('jobe:ExcSecurityPackNo',p_web.GSV('locSecurityPackID'))

    p_web.SSV('GetStatus:Type','EXC')
    GetStatus(p_web.GSV('GetStatus:StatusNumber'),0,p_web.GSV('GetStatus:Type'),p_web)

    ! Add To Audit
    p_web.SSV('AddToAudit:Type','EXC')
    IF (p_web.GSV('locSecurityPackID') <> '')
        p_web.SSV('AddToAudit:Notes',p_web.GSV('AddToAudit:Notes') & |
            '<13,10>SECURITY PACK NO: ' & p_web.GSV('locSecurityPackID'))
    END
    AddToAudit(p_web)
    DO CloseFiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:EXCHHIST.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:EXCHHIST.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSCONS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSCONS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:EXCHANGE.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:EXCHANGE.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:EXCHHIST.Close
     Access:JOBSCONS.Close
     Access:EXCHANGE.Close
     FilesOpened = False
  END
MessageQuestion      PROCEDURE  (NetWebServerWorker p_web)
! Use this procedure to "embed" html in other pages.
! on the web page use <!-- Net:MessageQuestion -->
!
! In this procedure set the packet string variable, and call the SendPacket routine.
!
! EXAMPLE:
! packet = '<strong>Hello World!</strong>'&CRLF
! do SendPacket
CRLF                    string('<13,10>')
NBSP                    string('&#160;')
packet                  string(NET:MaxBinData)
packetlen               long
timer                   long
loc:options             string(OptionsStringLen) ! options for jQuery calls
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  GlobalErrors.SetProcedureName('MessageQuestion')
  If p_web.RequestAjax = 1
    GlobalErrors.SetProcedureName()
    Return
  End
    !%SecwinCtrlsDisplay = 0 ; %SecwinAccessGroupsNotCreated = 0
!----------- put your html code here -----------------------------------
    packet = clip(packet) & |
        '<<html>' & |
        '<<script type="text/javascript"><13,10>'&|
        '<<!--<13,10>'&|
        'function confirmation() {{<13,10>'&|
        'var answer = confirm("' & p_web.GSV('Message:Text') & '")<13,10>'&|
        'if (answer){{<13,10>'&|
        'window.location.href = "' & p_web.GSV('Message:PassURL') & '";<13,10>'&|
        '}<13,10>'&|
        'else{{<13,10>'&|
        'window.location.href = "' & p_web.GSV('Message:FailURL') & '";<13,10>'&|
        '}<13,10>'&|
        '}<13,10>'&|
        'document.write(confirmation())<13,10>'&|
        '// --><13,10>'&|
        '<</script>'&|
        '<</html>'
!----------- end of custom code ----------------------------------------
  do SendPacket
  GlobalErrors.SetProcedureName()
  Return

!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet,1,packetlen,NET:NoHeader)
    packet = ''
  end
ClearTagFile         PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
FilesOpened     BYTE(0)
  CODE
    do openFiles

    Access:TAGFILE.Clearkey(tag:keyTagged)
    tag:sessionID    = p_web.sessionID
    set(tag:keyTagged,tag:keyTagged)
    loop
        if (Access:TAGFILE.Next())
            Break
        end ! if (Access:TAGFILE.Next())
        if (tag:sessionID    <> p_web.sessionID)
            Break
        end ! if (tag:sessionID    <> p_web.sessionID)
        DELETE(TAGFILE)
    end ! loop

    do closeFiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:TagFile.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TagFile.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:TagFile.Close
     FilesOpened = False
  END
AccessoryCheck       PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
FilesOpened     BYTE(0)
  CODE
    !Validate Accessories
    ! Variables Passed:
    ! AccessoryCheck:Type
    ! AccessoryCheck:RefNumber

    ! Returned Value
! AccessoryCheck:Return

    do OpenFiles
    Case p_web.GSV('AccessoryCheck:Type')
    of 'LOA'
        error# = 0
        Access:LOANACC.Clearkey(lac:Ref_Number_Key)
        lac:Ref_Number    = p_web.GSV('AccessoryCheck:RefNumber')
        set(lac:Ref_Number_Key,lac:Ref_Number_Key)
        loop
            if (Access:LOANACC.Next())
                Break
            end ! if (Access:LOANACC.Next())
            if (lac:Ref_Number    <> p_web.GSV('AccessoryCheck:RefNumber'))
                Break
            end ! if (lac:Ref_Number    <> p_web.GSV('tmp:LoanUnitNumber'))


            Access:TAGFILE.Clearkey(tag:keyTagged)
            tag:sessionID    = p_web.sessionID
            tag:taggedValue  = lac:accessory
            if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
                ! Found
                if (tag:tagged <> 1)
                    error# = 1
                    break
                end ! if (tag:tagged <> 1)
            else ! if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
                ! Error
                error# = 1
                break
            end ! if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
        end ! loop

        if (error# = 0)
            Access:TAGFILE.Clearkey(tag:keyTagged)
            tag:sessionID    = p_web.sessionID
            set(tag:keyTagged,tag:keyTagged)
            loop
                if (Access:TAGFILE.Next())
                    Break
                end ! if (Access:TAGFILE.Next())
                if (tag:sessionID    <> p_web.sessionID)
                    Break
                end ! if (tag:sessionID    <> p_web.sessionID)
                if (tag:Tagged = 0)
                    cycle
                end ! if (tag:Tagged = 0)
                Access:LOANACC.Clearkey(lac:ref_number_Key)
                lac:ref_number    = p_web.GSV('AccessoryCheck:RefNumber')
                lac:accessory    = tag:taggedValue
                if (Access:LOANACC.TryFetch(lac:ref_number_Key) = Level:Benign)
                    ! Found
                else ! if (Access:LOANACC.TryFetch(lac:ref_number_Key) = Level:Benign)
                    ! Error
                    error# = 2
                    break
                end ! if (Access:LOANACC.TryFetch(lac:ref_number_Key) = Level:Benign)
            end ! loop
        end ! if (error# = 0)
    Else
        error# = 0
        Access:JOBACC.Clearkey(jac:Ref_Number_Key)
        jac:Ref_Number    = p_web.GSV('AccessoryCheck:RefNumber')
        set(jac:Ref_Number_Key,jac:Ref_Number_Key)
        loop
            if (Access:JOBACC.Next())
                Break
            end ! if (Access:LOANACC.Next())
            if (jac:Ref_Number    <> p_web.GSV('AccessoryCheck:RefNumber'))
                Break
            end ! if (lac:Ref_Number    <> p_web.GSV('tmp:LoanUnitNumber'))

            if (p_web.GSV('BookingSite') <> 'RRC')
                if (NOT jac:Attached)
                    Cycle
                End
            End

            Access:TAGFILE.Clearkey(tag:keyTagged)
            tag:sessionID    = p_web.sessionID
            tag:taggedValue  = jac:accessory
            if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
                ! Found
                if (tag:tagged <> 1)
                    error# = 1
                    break
                else
                    if (p_web.GSV('BookingSite') <> 'RRC' AND NOT jac:Attached)
                        error# = 1
                        break
                    End
                end ! if (tag:tagged <> 1)
            else ! if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
                ! Error
                error# = 1
                break
            end ! if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
        end ! loop

        if (error# = 0)
            Access:TAGFILE.Clearkey(tag:keyTagged)
            tag:sessionID    = p_web.sessionID
            set(tag:keyTagged,tag:keyTagged)
            loop
                if (Access:TAGFILE.Next())
                    Break
                end ! if (Access:TAGFILE.Next())
                if (tag:sessionID    <> p_web.sessionID)
                    Break
                end ! if (tag:sessionID    <> p_web.sessionID)
                if (tag:Tagged = 0)
                    cycle
                end ! if (tag:Tagged = 0)
                Access:JOBACC.Clearkey(jac:ref_number_Key)
                jac:ref_number    = p_web.GSV('AccessoryCheck:RefNumber')
                jac:accessory    = tag:taggedValue
                if (Access:JOBACC.TryFetch(jac:ref_number_Key) = Level:Benign)
                    ! Found
                else ! if (Access:LOANACC.TryFetch(lac:ref_number_Key) = Level:Benign)
                    ! Error
                    error# = 2
                    break
                end ! if (Access:LOANACC.TryFetch(lac:ref_number_Key) = Level:Benign)
            end ! loop
        end ! if (error# = 0)
    End

    do CloseFiles

    p_web.SSV('AccessoryCheck:Return',error#)
    ! 1 = Missing
    ! 2 = Mismatch
    ! Else = All Fine
!--------------------------------------
OpenFiles  ROUTINE
  Access:TagFile.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TagFile.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBACC.Open                                       ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBACC.UseFile                                    ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:LOANACC.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:LOANACC.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:TagFile.Close
     Access:JOBACC.Close
     Access:LOANACC.Close
     FilesOpened = False
  END
