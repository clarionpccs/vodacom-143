

   MEMBER('WebServer_Phase4.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSE072.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSE071.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE073.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE099.INC'),ONCE        !Req'd for module callout resolution
                     END


FormBrowseEngineerHistory PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
FilesOpened     Long
BrowseEngineerHistory:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormBrowseEngineerHistory')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'FormBrowseEngineerHistory_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('FormBrowseEngineerHistory','')
    p_web.DivHeader('FormBrowseEngineerHistory',p_web.combine(p_web.site.style.formdiv,'fdiv'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('FormBrowseEngineerHistory') = 0
        p_web.AddPreCall('FormBrowseEngineerHistory')
        p_web.DivHeader('popup_FormBrowseEngineerHistory','nt-hidden')
        p_web.DivHeader('FormBrowseEngineerHistory',p_web.combine(p_web.site.style.formdiv,'fdiv'))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_FormBrowseEngineerHistory_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_FormBrowseEngineerHistory_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormBrowseEngineerHistory',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_FormBrowseEngineerHistory',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormBrowseEngineerHistory',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_FormBrowseEngineerHistory',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormBrowseEngineerHistory',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormBrowseEngineerHistory',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_FormBrowseEngineerHistory',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormBrowseEngineerHistory',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_FormBrowseEngineerHistory',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormBrowseEngineerHistory',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormBrowseEngineerHistory',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormBrowseEngineerHistory',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('FormBrowseEngineerHistory')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE
  loc:EipClm = upper(p_web.GetValue('_EIPClm'))
  p_web.DeleteValue('_EIPClm')
  case loc:EipClm
  of ''
    case upper(p_web.GetValue('_calledfrom_'))
    of upper('BrowseEngineerHistory')
      do Value::BrowseEngineerHistory
    end
  end

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FormBrowseEngineerHistory_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'FormBrowseEngineerHistory'
    end
    p_web.formsettings.proc = 'FormBrowseEngineerHistory'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('FormBrowseEngineerHistory_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'ViewJob'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormBrowseEngineerHistory_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormBrowseEngineerHistory_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormBrowseEngineerHistory_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'ViewJob'

GenerateForm   Routine
  do LoadRelatedRecords
  p_web.site.CancelButton.TextValue = 'Back'
  p_web.site.CancelButton.image = '/images/listback.png'
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  ! "Back Hyperlink" at top of screen
  packet = CLIP(packet) & '<<a href="' & loc:formactioncancel & '" target="_self" >Back<</a>'
  do sendPacket
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Engineer History') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Engineer History',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_FormBrowseEngineerHistory',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_FormBrowseEngineerHistory0_div')&'">'&p_web.Translate()&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="FormBrowseEngineerHistory_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('FormBrowseEngineerHistory_BrowseEngineerHistory_embedded_div')&''');'
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.cancel();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CancelButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="FormBrowseEngineerHistory_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'FormBrowseEngineerHistory_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="FormBrowseEngineerHistory_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'FormBrowseEngineerHistory_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_FormBrowseEngineerHistory')>0,p_web.GSV('showtab_FormBrowseEngineerHistory'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_FormBrowseEngineerHistory'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_FormBrowseEngineerHistory') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_FormBrowseEngineerHistory'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_FormBrowseEngineerHistory')>0,p_web.GSV('showtab_FormBrowseEngineerHistory'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_FormBrowseEngineerHistory') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_FormBrowseEngineerHistory_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_FormBrowseEngineerHistory')>0,p_web.GSV('showtab_FormBrowseEngineerHistory'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"FormBrowseEngineerHistory",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_FormBrowseEngineerHistory')>0,p_web.GSV('showtab_FormBrowseEngineerHistory'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_FormBrowseEngineerHistory_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('FormBrowseEngineerHistory') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('FormBrowseEngineerHistory')
    p_web.SetValue('_CallPopups',2)
    If p_web.GetPreCall('BrowseEngineerHistory') = 0
      p_web.SetValue('BrowseEngineerHistory:NoForm',1)
      p_web.SetValue('BrowseEngineerHistory:FormName',loc:formname)
      p_web.SetValue('BrowseEngineerHistory:parentIs','Form')
      p_web.SetValue('_parentProc','FormBrowseEngineerHistory')
      BrowseEngineerHistory(p_web)
      p_web.SetValue('_CallPopups',0)
      p_web.DeleteValue('BrowseEngineerHistory:NoForm')
      p_web.DeleteValue('BrowseEngineerHistory:FormName')
      p_web.DeleteValue('BrowseEngineerHistory:parentIs')
      p_web.DeleteValue('_parentProc')
    End
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate()&'</a></h3>' & CRLF & p_web.DivHeader('tab_FormBrowseEngineerHistory0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_FormBrowseEngineerHistory0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_FormBrowseEngineerHistory0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_FormBrowseEngineerHistory0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,)
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_FormBrowseEngineerHistory0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend)
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_FormBrowseEngineerHistory0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain">' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_FormBrowseEngineerHistory0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::BrowseEngineerHistory
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Validate::BrowseEngineerHistory  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  Elsif true
    p_web.StoreValue('joe:RecordNumber')
  End
  do ValidateValue::BrowseEngineerHistory  ! copies value to session value if valid.
  do Comment::BrowseEngineerHistory ! allows comment style to be updated.

ValidateValue::BrowseEngineerHistory  Routine
    If not (1=0)
    End

Value::BrowseEngineerHistory  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(1=0,1,0))
  ! --- BROWSE ---  BrowseEngineerHistory --
  p_web.SetValue('BrowseEngineerHistory:NoForm',1)
  p_web.SetValue('BrowseEngineerHistory:FormName',loc:formname)
  p_web.SetValue('BrowseEngineerHistory:parentIs','Form')
  p_web.SetValue('_parentProc','FormBrowseEngineerHistory')
  if p_web.RequestAjax = 0
    p_web.SSV('FormBrowseEngineerHistory:_popup_',p_web.GetValue('_popup_')) ! stores the current browse popup state
    packet = clip(packet) & '<div id="'&lower('FormBrowseEngineerHistory_BrowseEngineerHistory_embedded_div')&'"><!-- Net:BrowseEngineerHistory --></div><13,10>'
    do SendPacket
    p_web.DivHeader('FormBrowseEngineerHistory_' & lower('BrowseEngineerHistory') & '_value')
    p_web.DivFooter()
  else
    if p_web.GetValue('_popup_') = 1
      p_web.SSV('FormBrowseEngineerHistory:_popup_',1)
    elsif p_web.GSV('FormBrowseEngineerHistory:_popup_') = 1
      p_web.SetValue('_popup_',1)
    end
    packet = clip(packet) & '<!-- Net:BrowseEngineerHistory --><13,10>'
  end
  do SendPacket
Comment::BrowseEngineerHistory  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if BrowseEngineerHistory:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormBrowseEngineerHistory_' & p_web._nocolon('BrowseEngineerHistory') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('FormBrowseEngineerHistory_nexttab_' & 0)
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_FormBrowseEngineerHistory_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('FormBrowseEngineerHistory_tab_' & 0)
    do GenerateTab0
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('FormBrowseEngineerHistory_form:ready_',1)

  p_web.SetSessionValue('FormBrowseEngineerHistory_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_FormBrowseEngineerHistory',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('FormBrowseEngineerHistory_form:ready_',1)
  p_web.SetSessionValue('FormBrowseEngineerHistory_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormBrowseEngineerHistory',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('FormBrowseEngineerHistory_form:ready_',1)
  p_web.SetSessionValue('FormBrowseEngineerHistory_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('FormBrowseEngineerHistory:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('FormBrowseEngineerHistory_form:ready_',1)
  p_web.SetSessionValue('FormBrowseEngineerHistory_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('FormBrowseEngineerHistory:Primed',0)
  p_web.setsessionvalue('showtab_FormBrowseEngineerHistory',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormBrowseEngineerHistory_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormBrowseEngineerHistory_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
    loc:InvalidTab += 1
    do ValidateValue::BrowseEngineerHistory
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('FormBrowseEngineerHistory:Primed',0)
  p_web.StoreValue('')

SendSMS              PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
FilesOpened     Long
messageText:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  GlobalErrors.SetProcedureName('SendSMS')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'SendSMS_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('SendSMS','')
    p_web.DivHeader('SendSMS',p_web.combine(p_web.site.style.formdiv,'fdiv'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('SendSMS') = 0
        p_web.AddPreCall('SendSMS')
        p_web.DivHeader('popup_SendSMS','nt-hidden')
        p_web.DivHeader('SendSMS',p_web.combine(p_web.site.style.formdiv,'fdiv'))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_SendSMS_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_SendSMS_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferSendSMS',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_SendSMS',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferSendSMS',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_SendSMS',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferSendSMS',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_SendSMS',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_SendSMS',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferSendSMS',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_SendSMS',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferSendSMS',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_SendSMS',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_SendSMS',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('SendSMS')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('SendSMS_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'SendSMS'
    end
    p_web.formsettings.proc = 'SendSMS'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('SendSMS_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'BrowseContactHistory'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('SendSMS_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('SendSMS_ChainTo')
    loc:formaction = p_web.GetSessionValue('SendSMS_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'BrowseContactHistory'

GenerateForm   Routine
  do LoadRelatedRecords
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Send SMS') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Send SMS',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_SendSMS',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_SendSMS0_div')&'">'&p_web.Translate()&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="SendSMS_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.cancel();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CancelButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="SendSMS_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'SendSMS_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="SendSMS_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'SendSMS_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_SendSMS')>0,p_web.GSV('showtab_SendSMS'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_SendSMS'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_SendSMS') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_SendSMS'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_SendSMS')>0,p_web.GSV('showtab_SendSMS'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_SendSMS') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_SendSMS_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_SendSMS')>0,p_web.GSV('showtab_SendSMS'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"SendSMS",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_SendSMS')>0,p_web.GSV('showtab_SendSMS'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_SendSMS_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('SendSMS') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('SendSMS')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate()&'</a></h3>' & CRLF & p_web.DivHeader('tab_SendSMS0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_SendSMS0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_SendSMS0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_SendSMS0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,)
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_SendSMS0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend)
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_SendSMS0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain">' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_SendSMS0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::messageText
        do Comment::messageText
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Validate::messageText  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::messageText  ! copies value to session value if valid.
  do Comment::messageText ! allows comment style to be updated.

ValidateValue::messageText  Routine
    If not (1=0)
    End

Value::messageText  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('SendSMS_' & p_web._nocolon('messageText') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="messageText" class="'&clip('green bold')&'"'&clip(loc:extra)&'>' & p_web.Translate('Are you sure you want to resend an SMS?',) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::messageText  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if messageText:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('SendSMS_' & p_web._nocolon('messageText') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('SendSMS_nexttab_' & 0)
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_SendSMS_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('SendSMS_tab_' & 0)
    do GenerateTab0
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('SendSMS_form:ready_',1)

  p_web.SetSessionValue('SendSMS_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_SendSMS',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('SendSMS_form:ready_',1)
  p_web.SetSessionValue('SendSMS_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_SendSMS',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('SendSMS_form:ready_',1)
  p_web.SetSessionValue('SendSMS_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('SendSMS:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('SendSMS_form:ready_',1)
  p_web.SetSessionValue('SendSMS_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('SendSMS:Primed',0)
  p_web.setsessionvalue('showtab_SendSMS',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('SendSMS_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  !    AddEmailSMS(p_web.GSV('job:Ref_Number'), |
  !        p_web.GSV('wob:HeadAccountNumber') |
  !    ,'COMP','SMS',p_web.GSV('jobe2:SMSAlertNumber'),'',0,'')
  
      ! Assumes we are in the jobs record
      SendSMSText('J','N','N',p_web)
  p_web.DeleteSessionValue('SendSMS_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
    loc:InvalidTab += 1
    do ValidateValue::messageText
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('SendSMS:Primed',0)
  p_web.StoreValue('')

BrowseContactHistory PROCEDURE  (NetWebServerWorker p_web)
locUserName          STRING(60)                            !
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256),dim(Net:MaxKeyFields)
idx           String(Net:HashSize)
sub           Long
            End
cht:Date:IsInvalid  Long
cht:Time:IsInvalid  Long
locUserName:IsInvalid  Long
cht:Action:IsInvalid  Long
cht:Notes:IsInvalid  Long
Change:IsInvalid  Long
Delete:IsInvalid  Long
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:DefaultSelection    String(Net:HashSize)
loc:ActualSelection     String(Net:HashSize)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
!loc:pagename            String(256)
loc:NavButtonPosition    Long
loc:UpdateButtonPosition Long
loc:SelectionMethod      Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorCase         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
loc:lookupdone          Long
loc:FormPopup           Long
ThisView            View(CONTHIST)
                      Project(cht:Record_Number)
                      Project(cht:Date)
                      Project(cht:Time)
                      Project(cht:Action)
                      Project(cht:Notes)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
Loc:popup             Long
loc:poppedup          Long,static,thread
FilesOpened     Long
USERS::State  USHORT
JOBS::State  USHORT
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return 
  End
  GlobalErrors.SetProcedureName('BrowseContactHistory')
  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseContactHistory:NoForm')
      loc:NoForm = p_web.GetValue('BrowseContactHistory:NoForm')
      loc:FormName = p_web.GetValue('BrowseContactHistory:FormName')
    else
      loc:FormName = 'BrowseContactHistory_frm'
    End
    p_web.SSV('BrowseContactHistory:NoForm',loc:NoForm)
    p_web.SSV('BrowseContactHistory:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseContactHistory:NoForm')
    loc:FormName = p_web.GSV('BrowseContactHistory:FormName')
  end
  case p_web.site.formpopups
  of -1 ; loc:FormPopup = Net:Page
  of 0 ; loc:FormPopup = Net:Page
  of 1 ; loc:FormPopup = Net:Popup
  End
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseContactHistory') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseContactHistory')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if 1 = 2
  elsif p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  elsif p_web.IfExistsValue('_EIPRow_')
    do CallRow
  elsif p_web.GetValue('_CallPopups') > 0
    do CallPopups
  elsif p_web.IfExistsValue('_GetSecwinSettings')
      p_web.SetValue('Secwin_' & 'BrowseContactHistory' & 'AccessGroupsArray','1') 
      p_web.SetValue('Secwin_AccessWindowName','BrowseContactHistory')
      
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallPopups  Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
loc:CallPopups   Long
loc:name         String(255)
  code
  if loc:poppedup = 0
    loc:CallPopups = p_web.GetValue('_CallPopups')
    if loc:CallPopups = 1 !and p_web.GetPreCall('BrowseContactHistory') = 0! for = 1 include popup divs and scripts for this browse
      !p_web.AddPreCall('BrowseContactHistory')
      p_web.DivHeader('popup_BrowseContactHistory','nt-hidden')
      p_web.DivHeader('BrowseContactHistory',p_web.combine(p_web.site.style.browsediv,))
      p_web.DivFooter()
      p_web.DivFooter()
      loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(400)&', modal: true, position: [''center'',15]'
      packet =  p_web.jQuery('#' & lower('popup_BrowseContactHistory_div'),'dialog',loc:options) & |
      p_web.jQuery('#' & lower('popup_BrowseContactHistory_div'),'removeClass','''nt-hidden''')
    End
    ! for > 0 add in form, EIP-lookups and browse object
    do Popups
    loc:popup = 1
    do ClosingScripts
    do SendPacket
    loc:poppedup = 1
  end

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web.DivHeader('BrowseContactHistory',p_web.combine(p_web.site.style.browsediv,))
  if loc:ParentSilent = 0
    do GenerateBrowse
    if p_web.RequestAjax = 1
      p_web.Script('BrowseProducts.pcv();')
    end
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web.DivFooter()
  do Children
  do Popups
  if loc:poppedup = 0
    do ClosingScripts
  end
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(CONTHIST,cht:Record_Number_Key,loc:vorder)
    If False
    ElsIf (loc:vorder = 'CHT:DATE') then p_web.SetValue('BrowseContactHistory_sort','1')
    ElsIf (loc:vorder = 'CHT:TIME') then p_web.SetValue('BrowseContactHistory_sort','2')
    ElsIf (loc:vorder = 'CHT:ACTION') then p_web.SetValue('BrowseContactHistory_sort','4')
    ElsIf (loc:vorder = 'CHT:NOTES') then p_web.SetValue('BrowseContactHistory_sort','5')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseContactHistory:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseContactHistory:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseContactHistory:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseContactHistory:LookupField')
    loc:selecting = 0
  End

Popups Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  If loc:popup = 0
  End
SetFormAction  Routine
  loc:formaction = 'FormContactHistory'
  loc:formactiontarget = '_self'

GotFocusBack   Routine

GenerateBrowse Routine
  data
loc:viewoptions  Long
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  ! Set general Browse options
  loc:NavButtonPosition   = Net:Below
  loc:UpdateButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:viewonly = Choose(p_web.GSV('Job:ViewOnly') = 1,1,0)
  loc:FileLoading      = Net:FileLoad
  loc:FillBack         = 0
  loc:Sorting          = Net:ServerSort
  if p_web.GetValue('_popup_') = 1
    loc:popup = 1
  end
  ! Set Locator Options
  loc:LocatorPosition  = Net:Below
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  p_web.ClearBrowse('BrowseContactHistory')
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 10
  End
  loc:ActualSelection = ''
  !loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseContactHistory_sort',net:DontEvaluate)
  p_web.SetSessionValue('BrowseContactHistory_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'cht:Date','-cht:Date')
    Loc:LocateField = 'cht:Date'
    Loc:LocatorCase = 0
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'cht:Time','-cht:Time')
    Loc:LocateField = 'cht:Time'
    Loc:LocatorCase = 0
  of 4
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(cht:Action)','-UPPER(cht:Action)')
    Loc:LocateField = 'cht:Action'
    Loc:LocatorCase = 0
  of 5
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(cht:Notes)','-UPPER(cht:Notes)')
    Loc:LocateField = 'cht:Notes'
    Loc:LocatorCase = 0
  of 6
    Loc:LocateField = ''
    Loc:LocatorCase = 0
  of 7
    Loc:LocateField = ''
    Loc:LocatorCase = 0
  end
  if loc:vorder = ''
    loc:vorder = '+cht:Ref_Number,-cht:Date,-cht:Time,+UPPER(cht:Action)'
  end
  If False ! add range fields to sort order
  Else
    If Instring('CHT:REF_NUMBER',upper(loc:vOrder),1,1) = 0
      loc:vOrder = 'cht:Ref_Number,' & loc:vorder
    End
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('cht:Date')
    loc:SortHeader = p_web.Translate('Date')
    p_web.SetSessionValue('BrowseContactHistory_LocatorPic','@d6b')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
    loc:LocatorType = Net:Date
  Of upper('cht:Time')
    loc:SortHeader = p_web.Translate('Time')
    p_web.SetSessionValue('BrowseContactHistory_LocatorPic','@t1')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
  Of upper('locUserName')
    loc:SortHeader = p_web.Translate('User Name')
    p_web.SetSessionValue('BrowseContactHistory_LocatorPic','@s60')
  Of upper('cht:Action')
    loc:SortHeader = p_web.Translate('Action')
    p_web.SetSessionValue('BrowseContactHistory_LocatorPic','@s80')
  Of upper('cht:Notes')
    loc:SortHeader = p_web.Translate('Notes')
    p_web.SetSessionValue('BrowseContactHistory_LocatorPic','@s255')
  End
  If loc:selecting = 1
    p_web.GetSettings(p_web.GetSessionValue('Push1'))
    loc:selectaction = p_web.FormSettings.ParentPage !p_web.GetSessionValue('BrowseContactHistory:LookupFrom')
  End!Else
  do SendPacket
  do SetFormAction
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseContactHistory:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseContactHistory:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseContactHistory:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="CONTHIST"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="cht:Record_Number_Key"></input><13,10>'
  end
  case p_web.GetValue('refresh') ; of 'next' orof 'previous' orof 'first' orof 'last'
    p_web.Script('try{{BrowseContactHistory.restoreFocus();} catch(err){{};')
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseContactHistory',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      case loc:locatortype
      of Net:Position
      orof Net:Date
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
      of Net:Contains
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
      of Net:Begins
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
      End
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
      Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
      Case Loc:LocatorType
      of Net:Date
        packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator2BrowseContactHistory','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('BrowseContactHistory_LocatorPic'),,,'onchange="BrowseContactHistory.locate(''Locator2BrowseContactHistory'',this.value);"',,0,,,) & '</div></td>'
      Else
        packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator2BrowseContactHistory',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="BrowseContactHistory.locate(''Locator2BrowseContactHistory'',this.value);" ',,,,,,) & '</td>'
      End
      If loc:LocatorSearchButton or loc:LocatorClearButton
        packet = clip(packet) & '<td><div id="BrowseContactHistory_locate_a" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
        If loc:LocatorSearchButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
        End
        If loc:LocatorClearButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseContactHistory.cl(''BrowseContactHistory'');')
        End
        packet = clip(packet) & '</div><13,10></td><13,10>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
      If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'BrowseContactHistory_locate_a','buttonset',loc:options)
      End
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & p_web.DivHeader('BrowseContactHistory_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'','sortable')&'" id="BrowseContactHistory_tbl">'
  Else
    packet = clip(packet) & p_web.DivHeader('BrowseContactHistory_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'')&'" id="BrowseContactHistory_tbl">'
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'1','BrowseContactHistory',p_web.Translate('Date'),'Click here to sort by Date',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'2','BrowseContactHistory',p_web.Translate('Time'),'Click here to sort by Time',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'3','BrowseContactHistory',p_web.Translate('User Name'),,,,,,,1,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'4','BrowseContactHistory',p_web.Translate('Action'),'Click here to sort by Action',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'5','BrowseContactHistory',p_web.Translate('Notes'),'Click here to sort by Notes',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
      If loc:Selecting = 0
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'6','BrowseContactHistory',p_web.Translate(),,,,,,,0,loc:Sorting,'Button')
          do AddPacket
          loc:columns += 1
      End ! Selecting
      If loc:Selecting = 0
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'7','BrowseContactHistory',p_web.Translate(),,,,,,,0,loc:Sorting,'Button')
          do AddPacket
          loc:columns += 1
      End ! Selecting
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:LocateField = 'cht:Date' then Loc:NoBuffer = 1.
  If Loc:NoBuffer = 0
    Buffer(ThisView,10,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  If Instring('cht:record_number',lower(loc:vorder),1,1) = 0 !and CONTHIST{prop:SQLDriver} = 1
    loc:vorder = Choose(loc:vorder='','cht:Record_Number',clip(loc:vorder) & ',' & 'cht:Record_Number')
  End
  Loc:Selected = Choose(p_web.IfExistsValue('cht:Record_Number'),p_web.GetValue('cht:Record_Number'),p_web.GetSessionValue('cht:Record_Number'))
  ThisView{prop:order} = p_web.CleanFilter(ThisView,clip(loc:vorder))
    job:Ref_Number = p_web.RestoreValue('job:Ref_Number')
    loc:FilterWas = 'cht:Ref_Number = ' & job:Ref_Number
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseContactHistory',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseContactHistory_Filter')
    p_web.SetSessionValue('BrowseContactHistory_FirstValue','')
    p_web.SetSessionValue('BrowseContactHistory_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,CONTHIST,cht:Record_Number_Key,loc:PageRows,'BrowseContactHistory',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,loc:ViewOptions,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled,Loc:LocatorCase)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or Loc:LocatorType = Net:Date or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position or loc:LocatorType = Net:Date
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If CONTHIST{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(CONTHIST,loc:firstvalue)
              Reset(ThisView,CONTHIST)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If CONTHIST{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(CONTHIST,loc:lastvalue)
            Reset(ThisView,CONTHIST)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
          Access:USERS.ClearKey(use:User_Code_Key)
          use:User_Code = cht:User
          IF (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
              locUserName = CLIP(use:Forename) & ' ' & CLIP(use:Surname)
          ELSE
              locUserName = '** Unknown User (' & cht:User & ') **'
          END
      
      
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(cht:Record_Number)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td class="'&p_web.combine(p_web.site.style.BrowseEmpty,)&'">'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:NavButtonPosition=Net:Above or (loc:NavButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseContactHistory_nav_a" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseContactHistory.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseContactHistory.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseContactHistory.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseContactHistory.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseContactHistory_nav_a','buttonset',loc:options)
          End
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:UpdateButtonPosition=Net:Above or (loc:UpdateButtonPosition=Net:Both and loc:found > 0))
    packet = clip(packet) & '<div id="BrowseContactHistory_update_a" class="'&p_web.combine(p_web.site.style.BrowseUpdateButtonSet,)&'">'
    If loc:selecting = 0 or loc:popup
      If loc:viewonly = 0
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:InsertButton,'BrowseContactHistory',,,loc:FormPopup,'FormContactHistory')
          TableQueue.Kind = Net:BeforeTable
          do AddPacket
      End
    End
    If loc:found
          TableQueue.Kind = Net:BeforeTable
          do AddPacket
    End
    packet = clip(packet) & '</div><13,10>'
    If p_web.site.UseUpdateButtonSet
      loc:options = ''
      packet = clip(packet) & p_web.jQuery('#' & 'BrowseContactHistory_update_a','buttonset',loc:options)
    End ! If p_web.site.UseUpdateButtonSet
      TableQueue.Kind = Net:BeforeTable
      do AddPacket
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseContactHistory',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseContactHistory_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseContactHistory_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          case loc:locatortype
          of Net:Position
          orof Net:Date
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
          of Net:Contains
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
          of Net:Begins
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
          End
          packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
          Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
          Case Loc:LocatorType
          of Net:Date
            packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator1BrowseContactHistory','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('BrowseContactHistory_LocatorPic'),,,'onchange="BrowseContactHistory.locate(''Locator1BrowseContactHistory'',this.value);"',,0,,,) & '</div></td>'
          Else
            packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator1BrowseContactHistory',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="BrowseContactHistory.locate(''Locator1BrowseContactHistory'',this.value);" ',,,,,,) & '</td>'
          End
          If loc:LocatorSearchButton or loc:LocatorClearButton
            packet = clip(packet) & '<td><div id="BrowseContactHistory_locate_b" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
            If loc:LocatorSearchButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
            End
            If loc:LocatorClearButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseContactHistory.cl(''BrowseContactHistory'');')
            End
            packet = clip(packet) & '</div><13,10></td><13,10>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
          If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseContactHistory_locate_b','buttonset',loc:options)
          End
      End
    End
  End
  p_web.SetSessionValue('BrowseContactHistory_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseContactHistory_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:NavButtonPosition=Net:Below or loc:NavButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseContactHistory_nav_b" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseContactHistory.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseContactHistory.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseContactHistory.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseContactHistory.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseContactHistory_nav_b','buttonset',loc:options)
          End
        end
        do SendPacket
  End
  If loc:UpdateButtonPosition=Net:Below or loc:UpdateButtonPosition=Net:Both
  packet = clip(packet) & '<div id="BrowseContactHistory_update_b" class="'&p_web.combine(p_web.site.style.BrowseUpdateButtonSet,)&'">'
  If loc:selecting = 0 or loc:popup
    If loc:viewonly = 0
          packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:InsertButton,'BrowseContactHistory',,,loc:FormPopup,'FormContactHistory')
        do SendPacket
    End
  End
  If loc:found
        do SendPacket
  End
  packet = clip(packet) & '</div><13,10>'
  If p_web.site.UseUpdateButtonSet
    loc:options = ''
    packet = clip(packet) & p_web.jQuery('#' & 'BrowseContactHistory_update_b','buttonset',loc:options)
  End ! If p_web.site.UseUpdateButtonSet
    do SendPacket
  End
      IF (p_web.GSV('BookingSite') = 'RRC' OR |
          (p_web.GSV('BookingSite') = 'ARC' AND p_web.GSV('jobe:WebJob') = 0))
          IF (SUB(p_web.GSV('job:Current_Status'),1,3) > 704 AND SUB(p_web.GSV('job:Current_Status'),1,3) < 901)
              IF (p_web.GSV('jobe2:SMSNotification') = 1)
  
                  packet = clip(packet) & p_web.br
                  Packet = clip(Packet) & |
                      p_web.CreateButton('button','ResendSMS','Resend SMS','MainButton',loc:formname,,, |
                      'window.open('''& p_web._MakeURL(clip('SendSMS?returnURL=BrowseContactHistory')) & |
                      ''','''&clip('_self')&''')',,0,,,,,)
                  Do SendPacket
              END
              IF (p_web.GSV('jobe2:EmailNotification') = 1)
  
  
                  Packet = clip(Packet) & |
                      p_web.CreateButton('button','ResendEmail','Resend E-mail','MainButton',loc:formname,,, |
                      'window.open('''& p_web._MakeURL(clip('SendAnEmail?returnURL=BrowseContactHistory')) & |
                      ''','''&clip('_self')&''')',,0,,,,,)
                  packet = clip(packet) & p_web.br
                  Do SendPacket
              END
          END
  
      END
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
  data
loc:rowclick  string(1024)
  code
    loc:field = p_web.AddBrowseValue('BrowseContactHistory','CONTHIST',cht:Record_Number_Key) !cht:Record_Number
    p_web._thisrow = p_web._nocolon('cht:Record_Number')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if Loc:LocatorValue <> '' and loc:ActualSelection = ''
        loc:checked = 'checked'
        do SetSelection
      elsif loc:ActualSelection = '' and cht:Record_Number = p_web.GetValue('cht:Record_Number')
         loc:checked = 'checked'
         do SetSelection
      elsif loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseContactHistory:LookupField')) = cht:Record_Number and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      else
        loc:checked = Choose((cht:Record_Number = loc:selected) and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      end
      If(loc:SelectionMethod  = Net:Radio)
        loc:RowStyle = 'class="' & p_web.combine(p_web.site.style.browserow,) &'"'
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'data-nt-id="'& p_web.AddBrowseValue('BrowseContactHistory','CONTHIST',cht:Record_Number_Key) &'" onclick="'
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If CONTHIST{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(CONTHIST)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            loc:DefaultSelection = loc:field
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If CONTHIST{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(CONTHIST)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            loc:DefaultSelection = loc:field
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'>'&p_web.CreateInput('radio','_bidv_',clip(loc:field),,loc:checked,'')&'</td>'&CRLF
          If loc:DefaultSelection = ''
            loc:DefaultSelection = loc:field
          End
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          If loc:DefaultSelection = '' or loc:direction < 0
            loc:DefaultSelection = loc:field
          End
        End
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('RightJustify')&'"><13,10>'
          end ! loc:eip = 0
          do value::cht:Date
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('RightJustify')&'"><13,10>'
          end ! loc:eip = 0
          do value::cht:Time
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::locUserName
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::cht:Action
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::cht:Notes
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        If Loc:Selecting = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::Change
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        End     !Selecting
        If Loc:Selecting = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::Delete
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        End     !Selecting
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr ' & clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Idx = p_web.AddBrowseValue('BrowseContactHistory','CONTHIST',cht:Record_Number_Key)
  TableQueue.Id[1] = cht:Record_Number

ClosingScripts  Routine
  data
Rtn_SecwinProcedureName         string(252)
  code
  If p_web.RequestAjax = 0
    do SetFormAction
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var btiBrowseContactHistory;if (btiBrowseContactHistory != 1){{var BrowseContactHistory=new browseTable(''BrowseContactHistory'','''&clip(loc:formname)&|
      ''','''&p_web._jsok('cht:Record_Number',Net:Parameter)&''',2,'''&clip(loc:divname)&''',1,1,1,'''&|
      clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&|
      p_web.Translate('Are you sure you want to delete this record?')&''','''&clip(loc:ActualSelection)&''','''&clip(loc:selectaction)&''','''&|
      clip(loc:formactiontarget)&''',''FormContactHistory'','&loc:popup&',0,'''','''',"' & clip(Rtn_SecwinProcedureName) & '");<13,10>'&|
      'BrowseContactHistory.setGreenBar('''&p_web.ColorWeb(p_web.Site.Style.BrowseHighlightColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOneColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseTwoColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOverColor)&''');<13,10>' &|
      'BrowseContactHistory.applyGreenBar();btiBrowseContactHistory=1};<13,10>' &|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseContactHistory')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseContactHistory')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseContactHistory')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseContactHistory')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(CONTHIST)
  p_web._CloseFile(USERS)
  p_web._CloseFile(JOBS)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(CONTHIST)
  Bind(cht:Record)
  Clear(cht:Record)
  NetWebSetSessionPics(p_web,CONTHIST)
  p_web._OpenFile(USERS)
  Bind(use:Record)
  NetWebSetSessionPics(p_web,USERS)
  p_web._OpenFile(JOBS)
  Bind(job:Record)
  NetWebSetSessionPics(p_web,JOBS)

Children Routine
  if loc:selecting = 0
    If p_web.RequestAjax = 0
      do StartChildren
    Else
      do AjaxChildren
    End
  end

AjaxChildren  Routine
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue('cht:Record_Number',p_web.GetValue('cht:Record_Number'))
   p_web.DeleteValue('_Clicked')
   loc:found = 1
   do Children
! ----------------------------------------------------------------------------------------
CallRow  Routine
  data
loc:result  long
  code
  do OpenFilesB
  cht:Record_Number = p_web.GSV('cht:Record_Number')
  loc:result = p_web._GetFile(CONTHIST,cht:Record_Number_Key)
  loc:eip = 1
  loc:viewstate = p_web.escape(p_web.Base64Encode(clip(cht:Record_Number)))
  do BrowseRow
  do SendPacket
  do ClosefilesB

! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(CONTHIST)
  Case upper(p_web.GetValue('_EIPClm'))
  Else
    case p_web.GetValue('_action_')
    of Net:InsertRecord
    orof Net:ChangeRecord
    orof Net:DeleteRecord
    orof Net:CopyRecord
      loc:eip = 0
      do CallBrowse
    end
  End
  do GotFocusBack
  p_web._CloseFile(CONTHIST)
! ----------------------------------------------------------------------------------------
value::cht:Date   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseContactHistory_cht:Date_'&cht:Record_Number,'RightJustify',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(cht:Date,'@d6b')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::cht:Time   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseContactHistory_cht:Time_'&cht:Record_Number,'RightJustify',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(cht:Time,'@t1')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::locUserName   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseContactHistory_locUserName_'&cht:Record_Number,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(locUserName,'@s60')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::cht:Action   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseContactHistory_cht:Action_'&cht:Record_Number,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(cht:Action,'@s80')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::cht:Notes   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseContactHistory_cht:Notes_'&cht:Record_Number,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(cht:Notes,'@s255')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::Change   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseContactHistory_Change_'&cht:Record_Number,,net:crc,,loc:extra)
          If loc:viewonly = 0
             packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallChangeButton,'BrowseContactHistory',p_web.AddBrowseValue('BrowseContactHistory','CONTHIST',cht:Record_Number_Key),,loc:FormPopup,'FormContactHistory') & '<13,10>'
          End
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::Delete   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseContactHistory_Delete_'&cht:Record_Number,,net:crc,,loc:extra)
          If loc:viewonly = 0
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallDeleteButton,'BrowseContactHistory',p_web.AddBrowseValue('BrowseContactHistory','CONTHIST',cht:Record_Number_Key),,loc:FormPopup,'FormContactHistory') & '<13,10>'
          End
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  p_web._OpenFile(USERS)
  p_web._OpenFile(JOBS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(USERS)
  p_Web._CloseFile(JOBS)
     FilesOpened = False
  END
  return
!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
CheckForDuplicate  Routine
SetSelection  Routine
  loc:ActualSelection = loc:field
  p_web.SetSessionValue('cht:Record_Number',cht:Record_Number)

MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:ActualSelection = ''
    loc:ActualSelection = loc:DefaultSelection
    p_web.GetBrowseValue(loc:ActualSelection,Net:Web:SessionQueue) ! so children are primed with correct sessionValue
  End

  If loc:ActualSelection <> ''
    TableQueue.Kind = Net:RowTable
    get(TableQueue,TableQueue.Kind)
    if Errorcode() = 0
      TableQueue.Row = sub(TableQueue.Row,1,len(clip(TableQueue.Row))-1) &  ' data-nt-rows="'&loc:RowsHigh&'" data-nt-value="'&clip(loc:ActualSelection)&'"' & '>'
      Put(TableQueue)
    End
  End

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead class="'&p_web.combine(p_web.site.style.BrowseHeader,'')&'"><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot class="'&p_web.combine(p_web.site.style.BrowseFooter,)&'"><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody class="'&p_web.combine(p_web.site.style.BrowseBody,)&'"><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table><13,10>' & p_web.DivFooter(Net:NoSend) ! Table Div
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
