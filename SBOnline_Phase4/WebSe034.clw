

   MEMBER('WebServer_Phase4.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSE034.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSE007.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE008.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE009.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE035.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE036.INC'),ONCE        !Req'd for module callout resolution
                     END


BouncerOutFaults     PROCEDURE  (NetWebServerWorker p_web)
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256),dim(Net:MaxKeyFields)
idx           String(Net:HashSize)
sub           Long
            End
joo:FaultCode:IsInvalid  Long
joo:Description:IsInvalid  Long
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:DefaultSelection    String(Net:HashSize)
loc:ActualSelection     String(Net:HashSize)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
!loc:pagename            String(256)
loc:NavButtonPosition    Long
loc:UpdateButtonPosition Long
loc:SelectionMethod      Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorCase         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
loc:lookupdone          Long
loc:FormPopup           Long
ThisView            View(JOBOUTFL)
                      Project(joo:RecordNumber)
                      Project(joo:FaultCode)
                      Project(joo:Description)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
Loc:popup             Long
loc:poppedup          Long,static,thread
FilesOpened     Long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  GlobalErrors.SetProcedureName('BouncerOutFaults')
  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BouncerOutFaults:NoForm')
      loc:NoForm = p_web.GetValue('BouncerOutFaults:NoForm')
      loc:FormName = p_web.GetValue('BouncerOutFaults:FormName')
    else
      loc:FormName = 'BouncerOutFaults_frm'
    End
    p_web.SSV('BouncerOutFaults:NoForm',loc:NoForm)
    p_web.SSV('BouncerOutFaults:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BouncerOutFaults:NoForm')
    loc:FormName = p_web.GSV('BouncerOutFaults:FormName')
  end
  case p_web.site.formpopups
  of -1 ; loc:FormPopup = Net:Page
  of 0 ; loc:FormPopup = Net:Page
  of 1 ; loc:FormPopup = Net:Popup
  End
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BouncerOutFaults') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BouncerOutFaults')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if 1 = 2
  elsif p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  elsif p_web.IfExistsValue('_EIPRow_')
    do CallRow
  elsif p_web.GetValue('_CallPopups') > 0
    do CallPopups
  elsif p_web.IfExistsValue('_GetSecwinSettings')
      p_web.SetValue('Secwin_' & 'BouncerOutFaults' & 'AccessGroupsArray','1') 
      p_web.SetValue('Secwin_AccessWindowName','BouncerOutFaults')
      
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallPopups  Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
loc:CallPopups   Long
loc:name         String(255)
  code
  if loc:poppedup = 0
    loc:CallPopups = p_web.GetValue('_CallPopups')
    if loc:CallPopups = 1 !and p_web.GetPreCall('BouncerOutFaults') = 0! for = 1 include popup divs and scripts for this browse
      !p_web.AddPreCall('BouncerOutFaults')
      p_web.DivHeader('popup_BouncerOutFaults','nt-hidden')
      p_web.DivHeader('BouncerOutFaults',p_web.combine(p_web.site.style.browsediv,))
      p_web.DivFooter()
      p_web.DivFooter()
      loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(400)&', modal: true, position: [''center'',15]'
      packet =  p_web.jQuery('#' & lower('popup_BouncerOutFaults_div'),'dialog',loc:options) & |
      p_web.jQuery('#' & lower('popup_BouncerOutFaults_div'),'removeClass','''nt-hidden''')
    End
    ! for > 0 add in form, EIP-lookups and browse object
    do Popups
    loc:popup = 1
    do ClosingScripts
    do SendPacket
    loc:poppedup = 1
  end

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web.DivHeader('BouncerOutFaults',p_web.combine(p_web.site.style.browsediv,))
  if loc:ParentSilent = 0
    do GenerateBrowse
    if p_web.RequestAjax = 1
      p_web.Script('BrowseProducts.pcv();')
    end
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web.DivFooter()
  do Children
  do Popups
  if loc:poppedup = 0
    do ClosingScripts
  end
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(JOBOUTFL,joo:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'JOO:FAULTCODE') then p_web.SetValue('BouncerOutFaults_sort','1')
    ElsIf (loc:vorder = 'JOO:DESCRIPTION') then p_web.SetValue('BouncerOutFaults_sort','2')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BouncerOutFaults:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BouncerOutFaults:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BouncerOutFaults:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BouncerOutFaults:LookupField')
    loc:selecting = 0
  End

Popups Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  If loc:popup = 0
  End
SetFormAction  Routine
  loc:formaction = 'BouncerOutFaults'

GotFocusBack   Routine

GenerateBrowse Routine
  data
loc:viewoptions  Long
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  ! Set general Browse options
  loc:NavButtonPosition   = Net:Below
  loc:UpdateButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:FileLoad
  loc:FillBack         = 0
  loc:Sorting          = Net:NoSort
  if p_web.GetValue('_popup_') = 1
    loc:popup = 1
  end
  ! Set Locator Options
  loc:LocatorPosition  = Net:Below
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  p_web.ClearBrowse('BouncerOutFaults')
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 10
  End
  loc:ActualSelection = ''
  !loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BouncerOutFaults_sort',net:DontEvaluate)
  p_web.SetSessionValue('BouncerOutFaults_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(joo:FaultCode)','-UPPER(joo:FaultCode)')
    Loc:LocateField = 'joo:FaultCode'
    Loc:LocatorCase = 0
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(joo:Description)','-UPPER(joo:Description)')
    Loc:LocateField = 'joo:Description'
    Loc:LocatorCase = 0
  end
  if loc:vorder = ''
    loc:vorder = '+joo:JobNumber,+UPPER(joo:FaultCode)'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('joo:FaultCode')
    loc:SortHeader = p_web.Translate('Fault Code')
    p_web.SetSessionValue('BouncerOutFaults_LocatorPic','@s30')
  Of upper('joo:Description')
    loc:SortHeader = p_web.Translate('Description')
    p_web.SetSessionValue('BouncerOutFaults_LocatorPic','@s255')
  End
  If loc:selecting = 1
    p_web.GetSettings(p_web.GetSessionValue('Push1'))
    loc:selectaction = p_web.FormSettings.ParentPage !p_web.GetSessionValue('BouncerOutFaults:LookupFrom')
  End!Else
  loc:CloseAction = p_web.site.DefaultPage
  do SendPacket
  do SetFormAction
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BouncerOutFaults:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BouncerOutFaults:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BouncerOutFaults:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="JOBOUTFL"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="joo:RecordNumberKey"></input><13,10>'
  end
  If p_web.Translate('Outfaults') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.browseSubHeading,)&'">'&p_web.Translate('Outfaults',0)&'</div>'&CRLF
  End
  case p_web.GetValue('refresh') ; of 'next' orof 'previous' orof 'first' orof 'last'
    p_web.Script('try{{BouncerOutFaults.restoreFocus();} catch(err){{};')
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BouncerOutFaults',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      case loc:locatortype
      of Net:Position
      orof Net:Date
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
      of Net:Contains
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
      of Net:Begins
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
      End
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
      Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
      Case Loc:LocatorType
      of Net:Date
        packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator2BouncerOutFaults','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('BouncerOutFaults_LocatorPic'),,,'onchange="BouncerOutFaults.locate(''Locator2BouncerOutFaults'',this.value);"',,0,,,) & '</div></td>'
      Else
        packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator2BouncerOutFaults',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="BouncerOutFaults.locate(''Locator2BouncerOutFaults'',this.value);" ',,,,,,) & '</td>'
      End
      If loc:LocatorSearchButton or loc:LocatorClearButton
        packet = clip(packet) & '<td><div id="BouncerOutFaults_locate_a" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
        If loc:LocatorSearchButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
        End
        If loc:LocatorClearButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BouncerOutFaults.cl(''BouncerOutFaults'');')
        End
        packet = clip(packet) & '</div><13,10></td><13,10>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
      If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'BouncerOutFaults_locate_a','buttonset',loc:options)
      End
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & p_web.DivHeader('BouncerOutFaults_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'','sortable')&'" id="BouncerOutFaults_tbl">'
  Else
    packet = clip(packet) & p_web.DivHeader('BouncerOutFaults_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'')&'" id="BouncerOutFaults_tbl">'
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'1','BouncerOutFaults',p_web.Translate('Fault Code'),,,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'2','BouncerOutFaults',p_web.Translate('Description'),,,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,10,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  If Instring('joo:recordnumber',lower(loc:vorder),1,1) = 0 !and JOBOUTFL{prop:SQLDriver} = 1
    loc:vorder = Choose(loc:vorder='','joo:RecordNumber',clip(loc:vorder) & ',' & 'joo:RecordNumber')
  End
  Loc:Selected = Choose(p_web.IfExistsValue('joo:RecordNumber'),p_web.GetValue('joo:RecordNumber'),p_web.GetSessionValue('joo:RecordNumber'))
  ThisView{prop:order} = p_web.CleanFilter(ThisView,clip(loc:vorder))
      loc:FilterWas = 'joo:JobNumber = ' & p_web.GetSessionValue('job_ali:Ref_Number')
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BouncerOutFaults',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BouncerOutFaults_Filter')
    p_web.SetSessionValue('BouncerOutFaults_FirstValue','')
    p_web.SetSessionValue('BouncerOutFaults_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,JOBOUTFL,joo:RecordNumberKey,loc:PageRows,'BouncerOutFaults',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,loc:ViewOptions,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled,Loc:LocatorCase)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or Loc:LocatorType = Net:Date or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position or loc:LocatorType = Net:Date
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If JOBOUTFL{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(JOBOUTFL,loc:firstvalue)
              Reset(ThisView,JOBOUTFL)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If JOBOUTFL{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(JOBOUTFL,loc:lastvalue)
            Reset(ThisView,JOBOUTFL)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(joo:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td class="'&p_web.combine(p_web.site.style.BrowseEmpty,)&'">'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:NavButtonPosition=Net:Above or (loc:NavButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BouncerOutFaults_nav_a" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BouncerOutFaults.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BouncerOutFaults.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BouncerOutFaults.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BouncerOutFaults.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BouncerOutFaults_nav_a','buttonset',loc:options)
          End
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:UpdateButtonPosition=Net:Above or (loc:UpdateButtonPosition=Net:Both and loc:found > 0))
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BouncerOutFaults',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BouncerOutFaults_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BouncerOutFaults_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          case loc:locatortype
          of Net:Position
          orof Net:Date
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
          of Net:Contains
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
          of Net:Begins
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
          End
          packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
          Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
          Case Loc:LocatorType
          of Net:Date
            packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator1BouncerOutFaults','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('BouncerOutFaults_LocatorPic'),,,'onchange="BouncerOutFaults.locate(''Locator1BouncerOutFaults'',this.value);"',,0,,,) & '</div></td>'
          Else
            packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator1BouncerOutFaults',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="BouncerOutFaults.locate(''Locator1BouncerOutFaults'',this.value);" ',,,,,,) & '</td>'
          End
          If loc:LocatorSearchButton or loc:LocatorClearButton
            packet = clip(packet) & '<td><div id="BouncerOutFaults_locate_b" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
            If loc:LocatorSearchButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
            End
            If loc:LocatorClearButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BouncerOutFaults.cl(''BouncerOutFaults'');')
            End
            packet = clip(packet) & '</div><13,10></td><13,10>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
          If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BouncerOutFaults_locate_b','buttonset',loc:options)
          End
      End
    End
  End
  p_web.SetSessionValue('BouncerOutFaults_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BouncerOutFaults_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:NavButtonPosition=Net:Below or loc:NavButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BouncerOutFaults_nav_b" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BouncerOutFaults.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BouncerOutFaults.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BouncerOutFaults.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BouncerOutFaults.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BouncerOutFaults_nav_b','buttonset',loc:options)
          End
        end
        do SendPacket
  End
  If loc:UpdateButtonPosition=Net:Below or loc:UpdateButtonPosition=Net:Both
  End
  If loc:selecting = 0 and loc:parent = ''
      if loc:popup
        packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:BrowseCloseButton,'BouncerOutFaults',,,loc:popup)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCloseButton,loc:Formname,loc:CloseAction)
      end
  End
    do SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
  data
loc:rowclick  string(1024)
  code
    loc:field = p_web.AddBrowseValue('BouncerOutFaults','JOBOUTFL',joo:RecordNumberKey) !joo:RecordNumber
    p_web._thisrow = p_web._nocolon('joo:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if Loc:LocatorValue <> '' and loc:ActualSelection = ''
        loc:checked = 'checked'
        do SetSelection
      elsif loc:ActualSelection = '' and joo:RecordNumber = p_web.GetValue('joo:RecordNumber')
         loc:checked = 'checked'
         do SetSelection
      elsif loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BouncerOutFaults:LookupField')) = joo:RecordNumber and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      else
        loc:checked = Choose((joo:RecordNumber = loc:selected) and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      end
      If(loc:SelectionMethod  = Net:Radio)
        loc:RowStyle = 'class="' & p_web.combine(p_web.site.style.browserow,) &'"'
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'data-nt-id="'& p_web.AddBrowseValue('BouncerOutFaults','JOBOUTFL',joo:RecordNumberKey) &'" onclick="'
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If JOBOUTFL{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(JOBOUTFL)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            loc:DefaultSelection = loc:field
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If JOBOUTFL{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(JOBOUTFL)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            loc:DefaultSelection = loc:field
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'>'&p_web.CreateInput('radio','_bidv_',clip(loc:field),,loc:checked,'')&'</td>'&CRLF
          If loc:DefaultSelection = ''
            loc:DefaultSelection = loc:field
          End
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          If loc:DefaultSelection = '' or loc:direction < 0
            loc:DefaultSelection = loc:field
          End
        End
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::joo:FaultCode
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::joo:Description
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr ' & clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Idx = p_web.AddBrowseValue('BouncerOutFaults','JOBOUTFL',joo:RecordNumberKey)
  TableQueue.Id[1] = joo:RecordNumber

ClosingScripts  Routine
  data
Rtn_SecwinProcedureName         string(252)
  code
  If p_web.RequestAjax = 0
    do SetFormAction
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var btiBouncerOutFaults;if (btiBouncerOutFaults != 1){{var BouncerOutFaults=new browseTable(''BouncerOutFaults'','''&clip(loc:formname)&|
      ''','''&p_web._jsok('joo:RecordNumber',Net:Parameter)&''',2,'''&clip(loc:divname)&''',1,1,1,'''&|
      clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&|
      p_web.Translate('Are you sure you want to delete this record?')&''','''&clip(loc:ActualSelection)&''','''&clip(loc:selectaction)&''','''&|
      clip(loc:formactiontarget)&''','''','&loc:popup&',0,'''','''',"' & clip(Rtn_SecwinProcedureName) & '");<13,10>'&|
      'BouncerOutFaults.setGreenBar('''&p_web.ColorWeb(p_web.Site.Style.BrowseHighlightColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOneColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseTwoColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOverColor)&''');<13,10>' &|
      'BouncerOutFaults.applyGreenBar();btiBouncerOutFaults=1};<13,10>' &|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BouncerOutFaults')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BouncerOutFaults')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BouncerOutFaults')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BouncerOutFaults')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(JOBOUTFL)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(JOBOUTFL)
  Bind(joo:Record)
  Clear(joo:Record)
  NetWebSetSessionPics(p_web,JOBOUTFL)

Children Routine
  if loc:selecting = 0
    If p_web.RequestAjax = 0
      do StartChildren
    Else
      do AjaxChildren
    End
  end

AjaxChildren  Routine
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue('joo:RecordNumber',p_web.GetValue('joo:RecordNumber'))
   p_web.DeleteValue('_Clicked')
   loc:found = 1
   do Children
! ----------------------------------------------------------------------------------------
CallRow  Routine
  data
loc:result  long
  code
  do OpenFilesB
  joo:RecordNumber = p_web.GSV('joo:RecordNumber')
  loc:result = p_web._GetFile(JOBOUTFL,joo:RecordNumberKey)
  loc:eip = 1
  loc:viewstate = p_web.escape(p_web.Base64Encode(clip(joo:RecordNumber)))
  do BrowseRow
  do SendPacket
  do ClosefilesB

! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(JOBOUTFL)
  Case upper(p_web.GetValue('_EIPClm'))
  Else
    case p_web.GetValue('_action_')
    of Net:InsertRecord
    orof Net:ChangeRecord
    orof Net:DeleteRecord
    orof Net:CopyRecord
      loc:eip = 0
      do CallBrowse
    end
  End
  do GotFocusBack
  p_web._CloseFile(JOBOUTFL)
! ----------------------------------------------------------------------------------------
value::joo:FaultCode   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BouncerOutFaults_joo:FaultCode_'&joo:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(joo:FaultCode,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::joo:Description   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BouncerOutFaults_joo:Description_'&joo:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(joo:Description,'@s255')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END
  return
!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
CheckForDuplicate  Routine
SetSelection  Routine
  loc:ActualSelection = loc:field
  p_web.SetSessionValue('joo:RecordNumber',joo:RecordNumber)

MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:ActualSelection = ''
    loc:ActualSelection = loc:DefaultSelection
    p_web.GetBrowseValue(loc:ActualSelection,Net:Web:SessionQueue) ! so children are primed with correct sessionValue
  End

  If loc:ActualSelection <> ''
    TableQueue.Kind = Net:RowTable
    get(TableQueue,TableQueue.Kind)
    if Errorcode() = 0
      TableQueue.Row = sub(TableQueue.Row,1,len(clip(TableQueue.Row))-1) &  ' data-nt-rows="'&loc:RowsHigh&'" data-nt-value="'&clip(loc:ActualSelection)&'"' & '>'
      Put(TableQueue)
    End
  End

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead class="'&p_web.combine(p_web.site.style.BrowseHeader,'')&'"><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot class="'&p_web.combine(p_web.site.style.BrowseFooter,)&'"><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody class="'&p_web.combine(p_web.site.style.BrowseBody,)&'"><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table><13,10>' & p_web.DivFooter(Net:NoSend) ! Table Div
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
ViewBouncerJob       PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
tmp:CurrentTradeAccount STRING(30)                         !Current Trade Account
FilesOpened     Long
JOBS_ALIAS::State  USHORT
TRADEACC::State  USHORT
SUBTRACC::State  USHORT
tra:Account_Number:IsInvalid  Long
tra:Company_Name:IsInvalid  Long
tra:Address_Line1:IsInvalid  Long
tra:Address_Line2:IsInvalid  Long
tra:Address_Line3:IsInvalid  Long
tra:Postcode:IsInvalid  Long
tra:Telephone_Number:IsInvalid  Long
tra:Fax_Number:IsInvalid  Long
tra:EmailAddress:IsInvalid  Long
TextDisplay:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  GlobalErrors.SetProcedureName('ViewBouncerJob')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'ViewBouncerJob_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('ViewBouncerJob','')
    p_web.DivHeader('ViewBouncerJob',p_web.combine(p_web.site.style.formdiv,))
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('ViewBouncerJob') = 0
        p_web.AddPreCall('ViewBouncerJob')
        p_web.DivHeader('popup_ViewBouncerJob','nt-hidden')
        p_web.DivHeader('ViewBouncerJob',p_web.combine(p_web.site.style.formdiv,))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_ViewBouncerJob_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_ViewBouncerJob_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferViewBouncerJob',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:StagePost
    do RestoreMem
    do PostInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_ViewBouncerJob',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferViewBouncerJob',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:StagePost
    do RestoreMem
    do PostCopy
  of Net:CopyRecord + NET:WEB:Populate
    If p_web.IfExistsValue('job_ali:Ref_Number') = 0 then p_web.SetValue('job_ali:Ref_Number',p_web.GSV('job_ali:Ref_Number')).
    do PreCopy
    p_web.setsessionvalue('showtab_ViewBouncerJob',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferViewBouncerJob',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_ViewBouncerJob',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do PostInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do PostUpdate
    End
  of Net:ChangeRecord + NET:WEB:Populate
    If p_web.IfExistsValue('job_ali:Ref_Number') = 0 then p_web.SetValue('job_ali:Ref_Number',p_web.GSV('job_ali:Ref_Number')).
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_ViewBouncerJob',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferViewBouncerJob',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:DeleteRecord + NET:WEB:StagePost
    do RestoreMem
    do PostDelete
  of Net:ViewRecord + NET:WEB:Populate
    If p_web.IfExistsValue('job_ali:Ref_Number') = 0 then p_web.SetValue('job_ali:Ref_Number',p_web.GSV('job_ali:Ref_Number')).
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_ViewBouncerJob',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferViewBouncerJob',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_ViewBouncerJob',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_ViewBouncerJob',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('ViewBouncerJob')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(JOBS_ALIAS)
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(SUBTRACC)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(JOBS_ALIAS)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(SUBTRACC)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('ViewBouncerJob_form:inited_',1)
  p_web.formsettings.file = 'JOBS_ALIAS'
  p_web.formsettings.key = 'job_ali:Ref_Number_Key'
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = 'JOBS_ALIAS'
    p_web.formsettings.key = 'job_ali:Ref_Number_Key'
      clear(p_web.formsettings.FieldName)
    p_web.formsettings.recordid[1] = job_ali:Ref_Number
    p_web.formsettings.FieldName[1] = 'job_ali:Ref_Number'
    do SetAction
    if p_web.GetSessionValue('ViewBouncerJob:Primed') = 1
      p_web.formsettings.action = Net:ChangeRecord
    Else
      p_web.formsettings.action = Loc:Act
    End
    p_web.formsettings.OriginalAction = Loc:Act
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'ViewBouncerJob'
    end
    p_web.formsettings.proc = 'ViewBouncerJob'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine
  IF p_web.GetSessionValue('ViewBouncerJob:Primed') = 1
    p_web._deleteFile(JOBS_ALIAS)
    p_web.SetSessionValue('ViewBouncerJob:Primed',0)
  End

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine
  p_web.SetValue('UpdateFile','JOBS_ALIAS')
  p_web.SetValue('UpdateKey','job_ali:Ref_Number_Key')
  If p_web.IfExistsValue('tra:Account_Number')
    p_web.SetPicture('tra:Account_Number','@s15')
  End
  p_web.SetSessionPicture('tra:Account_Number','@s15')
  If p_web.IfExistsValue('tra:Company_Name')
    p_web.SetPicture('tra:Company_Name','@s30')
  End
  p_web.SetSessionPicture('tra:Company_Name','@s30')
  If p_web.IfExistsValue('tra:Address_Line1')
    p_web.SetPicture('tra:Address_Line1','@s30')
  End
  p_web.SetSessionPicture('tra:Address_Line1','@s30')
  If p_web.IfExistsValue('tra:Address_Line2')
    p_web.SetPicture('tra:Address_Line2','@s30')
  End
  p_web.SetSessionPicture('tra:Address_Line2','@s30')
  If p_web.IfExistsValue('tra:Address_Line3')
    p_web.SetPicture('tra:Address_Line3','@s30')
  End
  p_web.SetSessionPicture('tra:Address_Line3','@s30')
  If p_web.IfExistsValue('tra:Postcode')
    p_web.SetPicture('tra:Postcode','@s15')
  End
  p_web.SetSessionPicture('tra:Postcode','@s15')
  If p_web.IfExistsValue('tra:Telephone_Number')
    p_web.SetPicture('tra:Telephone_Number','@s15')
  End
  p_web.SetSessionPicture('tra:Telephone_Number','@s15')
  If p_web.IfExistsValue('tra:Fax_Number')
    p_web.SetPicture('tra:Fax_Number','@s15')
  End
  p_web.SetSessionPicture('tra:Fax_Number','@s15')
  If p_web.IfExistsValue('tra:EmailAddress')
    p_web.SetPicture('tra:EmailAddress','@s255')
  End
  p_web.SetSessionPicture('tra:EmailAddress','@s255')

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  if p_web.IfExistsValue('tra:Account_Number') = 0
    p_web.SetSessionValue('tra:Account_Number',tra:Account_Number)
  Else
    tra:Account_Number = p_web.GetSessionValue('tra:Account_Number')
  End
  if p_web.IfExistsValue('tra:Company_Name') = 0
    p_web.SetSessionValue('tra:Company_Name',tra:Company_Name)
  Else
    tra:Company_Name = p_web.GetSessionValue('tra:Company_Name')
  End
  if p_web.IfExistsValue('tra:Address_Line1') = 0
    p_web.SetSessionValue('tra:Address_Line1',tra:Address_Line1)
  Else
    tra:Address_Line1 = p_web.GetSessionValue('tra:Address_Line1')
  End
  if p_web.IfExistsValue('tra:Address_Line2') = 0
    p_web.SetSessionValue('tra:Address_Line2',tra:Address_Line2)
  Else
    tra:Address_Line2 = p_web.GetSessionValue('tra:Address_Line2')
  End
  if p_web.IfExistsValue('tra:Address_Line3') = 0
    p_web.SetSessionValue('tra:Address_Line3',tra:Address_Line3)
  Else
    tra:Address_Line3 = p_web.GetSessionValue('tra:Address_Line3')
  End
  if p_web.IfExistsValue('tra:Postcode') = 0
    p_web.SetSessionValue('tra:Postcode',tra:Postcode)
  Else
    tra:Postcode = p_web.GetSessionValue('tra:Postcode')
  End
  if p_web.IfExistsValue('tra:Telephone_Number') = 0
    p_web.SetSessionValue('tra:Telephone_Number',tra:Telephone_Number)
  Else
    tra:Telephone_Number = p_web.GetSessionValue('tra:Telephone_Number')
  End
  if p_web.IfExistsValue('tra:Fax_Number') = 0
    p_web.SetSessionValue('tra:Fax_Number',tra:Fax_Number)
  Else
    tra:Fax_Number = p_web.GetSessionValue('tra:Fax_Number')
  End
  if p_web.IfExistsValue('tra:EmailAddress') = 0
    p_web.SetSessionValue('tra:EmailAddress',tra:EmailAddress)
  Else
    tra:EmailAddress = p_web.GetSessionValue('tra:EmailAddress')
  End

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=File
  if p_web.IfExistsValue('tra:Account_Number')
    tra:Account_Number = p_web.GetValue('tra:Account_Number')
    p_web.SetSessionValue('tra:Account_Number',tra:Account_Number)
  Else
    tra:Account_Number = p_web.GetSessionValue('tra:Account_Number')
  End
  if p_web.IfExistsValue('tra:Company_Name')
    tra:Company_Name = p_web.GetValue('tra:Company_Name')
    p_web.SetSessionValue('tra:Company_Name',tra:Company_Name)
  Else
    tra:Company_Name = p_web.GetSessionValue('tra:Company_Name')
  End
  if p_web.IfExistsValue('tra:Address_Line1')
    tra:Address_Line1 = p_web.GetValue('tra:Address_Line1')
    p_web.SetSessionValue('tra:Address_Line1',tra:Address_Line1)
  Else
    tra:Address_Line1 = p_web.GetSessionValue('tra:Address_Line1')
  End
  if p_web.IfExistsValue('tra:Address_Line2')
    tra:Address_Line2 = p_web.GetValue('tra:Address_Line2')
    p_web.SetSessionValue('tra:Address_Line2',tra:Address_Line2)
  Else
    tra:Address_Line2 = p_web.GetSessionValue('tra:Address_Line2')
  End
  if p_web.IfExistsValue('tra:Address_Line3')
    tra:Address_Line3 = p_web.GetValue('tra:Address_Line3')
    p_web.SetSessionValue('tra:Address_Line3',tra:Address_Line3)
  Else
    tra:Address_Line3 = p_web.GetSessionValue('tra:Address_Line3')
  End
  if p_web.IfExistsValue('tra:Postcode')
    tra:Postcode = p_web.GetValue('tra:Postcode')
    p_web.SetSessionValue('tra:Postcode',tra:Postcode)
  Else
    tra:Postcode = p_web.GetSessionValue('tra:Postcode')
  End
  if p_web.IfExistsValue('tra:Telephone_Number')
    tra:Telephone_Number = p_web.GetValue('tra:Telephone_Number')
    p_web.SetSessionValue('tra:Telephone_Number',tra:Telephone_Number)
  Else
    tra:Telephone_Number = p_web.GetSessionValue('tra:Telephone_Number')
  End
  if p_web.IfExistsValue('tra:Fax_Number')
    tra:Fax_Number = p_web.GetValue('tra:Fax_Number')
    p_web.SetSessionValue('tra:Fax_Number',tra:Fax_Number)
  Else
    tra:Fax_Number = p_web.GetSessionValue('tra:Fax_Number')
  End
  if p_web.IfExistsValue('tra:EmailAddress')
    tra:EmailAddress = p_web.GetValue('tra:EmailAddress')
    p_web.SetSessionValue('tra:EmailAddress',tra:EmailAddress)
  Else
    tra:EmailAddress = p_web.GetSessionValue('tra:EmailAddress')
  End

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('ViewBouncerJob_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'NewJobBooking'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('ViewBouncerJob_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('ViewBouncerJob_ChainTo')
    loc:formaction = p_web.GetSessionValue('ViewBouncerJob_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'BrowseIMEIHistory'

GenerateForm   Routine
  do LoadRelatedRecords
  If p_web.GetSessionValue('job:Account_Number') <> ''
      Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
      sub:Account_Number = p_web.GetSessionValue('job:Account_Number')
      If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
          tmp:CurrentTradeAccount = sub:Main_Account_Number
      End ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
  End ! If p_web.GetSessionValue('tmp:AccountNumber') <> ''
  
  
  Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
  sub:Account_Number = p_web.GetSessionValue('job_ali:Account_Number')
  If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
      p_web.FileToSessionQueue(SUBTRACC)
      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
      tra:Account_Number = sub:Main_Account_Number
      If ACcess:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
          p_web.FileToSessionQueue(TRADEACC)
      End ! If ACcess:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
  End ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
  
  If tmp:CurrentTradeAccount = tra:Account_Number
      p_web.SetSessionValue('Local:NoAuthorise',1)
  Else ! If tmp:CurrentTradeAccount = p_web.GetSessionValue('tra:Account_Number')
      p_web.SetSessionValue('Local:NoAuthorise',0)
  End ! If tmp:CurrentTradeAccount = p_web.GetSessionValue('tra:Account_Number')
  
  If SecurityCheckFailed(p_web.GetSessionValue('BookingUserPassword'),'BOUNCER - AUTHORISE BILLING')
      p_web.SetSessionValue('Local:NoAuthorise',0)
  End ! If SecurityCheckFailed('RAPID ENG - CHANGE JOB STATUS')
  
  p_web.site.SaveButton.TextValue = 'Authorise'
  
  p_web.SSV('NewAccountAuthorised',0)
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
    loc:viewonly = Choose(p_web.GetSessionValue('Local:NoAuthorise') = 1,1,0)
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('View Job Details') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('View Job Details',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_ViewBouncerJob',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_ViewBouncerJob0_div')&'">'&p_web.Translate('Job Details')&'</a></li>'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_ViewBouncerJob1_div')&'">'&p_web.Translate()&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  do GenerateTab1
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="ViewBouncerJob_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.cancel();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CancelButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="ViewBouncerJob_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'ViewBouncerJob_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="ViewBouncerJob_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'ViewBouncerJob_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_ViewBouncerJob')>0,p_web.GSV('showtab_ViewBouncerJob'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_ViewBouncerJob'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_ViewBouncerJob') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_ViewBouncerJob'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_ViewBouncerJob')>0,p_web.GSV('showtab_ViewBouncerJob'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_ViewBouncerJob') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Job Details') & ''''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_ViewBouncerJob_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_ViewBouncerJob')>0,p_web.GSV('showtab_ViewBouncerJob'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"ViewBouncerJob",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_ViewBouncerJob')>0,p_web.GSV('showtab_ViewBouncerJob'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_ViewBouncerJob_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('ViewBouncerJob') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('ViewBouncerJob')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Job Details')&'</a></h3>' & CRLF & p_web.DivHeader('tab_ViewBouncerJob0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewBouncerJob0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewBouncerJob0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewBouncerJob0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Job Details')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewBouncerJob0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Job Details')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewBouncerJob0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Job Details')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewBouncerJob0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&150&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tra:Account_Number
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tra:Account_Number
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&150&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tra:Company_Name
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tra:Company_Name
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&150&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tra:Address_Line1
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tra:Address_Line1
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&150&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tra:Address_Line2
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tra:Address_Line2
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&150&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tra:Address_Line3
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tra:Address_Line3
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&150&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tra:Postcode
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tra:Postcode
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&150&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tra:Telephone_Number
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tra:Telephone_Number
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&150&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tra:Fax_Number
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tra:Fax_Number
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&150&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tra:EmailAddress
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tra:EmailAddress
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab1  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate()&'</a></h3>' & CRLF & p_web.DivHeader('tab_ViewBouncerJob1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewBouncerJob1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewBouncerJob1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewBouncerJob1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,)
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewBouncerJob1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend)
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewBouncerJob1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain">' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_ViewBouncerJob1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
      If p_web.GetSessionValue('Local:NoAuthorise') = 0
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 2.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::TextDisplay
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Prompt::tra:Account_Number  Routine
  packet = clip(packet) & p_web.DivHeader('ViewBouncerJob_' & p_web._nocolon('tra:Account_Number') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Account Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tra:Account_Number  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tra:Account_Number = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s15
    tra:Account_Number = p_web.Dformat(p_web.GetValue('Value'),'@s15')
  End
  do ValidateValue::tra:Account_Number  ! copies value to session value if valid.
  do Value::tra:Account_Number
  do SendAlert

ValidateValue::tra:Account_Number  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tra:Account_Number',tra:Account_Number).
    End

Value::tra:Account_Number  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewBouncerJob_' & p_web._nocolon('tra:Account_Number') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  If loc:retrying
    tra:Account_Number = p_web.RestoreValue('tra:Account_Number')
    do ValidateValue::tra:Account_Number
    If tra:Account_Number:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- tra:Account_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tra:Account_Number'',''viewbouncerjob_tra:account_number_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tra:Account_Number',p_web.GetSessionValueFormat('tra:Account_Number'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s15'),,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::tra:Company_Name  Routine
  packet = clip(packet) & p_web.DivHeader('ViewBouncerJob_' & p_web._nocolon('tra:Company_Name') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Company Name'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tra:Company_Name  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tra:Company_Name = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    tra:Company_Name = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::tra:Company_Name  ! copies value to session value if valid.
  do Value::tra:Company_Name
  do SendAlert

ValidateValue::tra:Company_Name  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tra:Company_Name',tra:Company_Name).
    End

Value::tra:Company_Name  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewBouncerJob_' & p_web._nocolon('tra:Company_Name') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  If loc:retrying
    tra:Company_Name = p_web.RestoreValue('tra:Company_Name')
    do ValidateValue::tra:Company_Name
    If tra:Company_Name:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- tra:Company_Name
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tra:Company_Name'',''viewbouncerjob_tra:company_name_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tra:Company_Name',p_web.GetSessionValueFormat('tra:Company_Name'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::tra:Address_Line1  Routine
  packet = clip(packet) & p_web.DivHeader('ViewBouncerJob_' & p_web._nocolon('tra:Address_Line1') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Address'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tra:Address_Line1  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tra:Address_Line1 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    tra:Address_Line1 = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::tra:Address_Line1  ! copies value to session value if valid.
  do Value::tra:Address_Line1
  do SendAlert

ValidateValue::tra:Address_Line1  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tra:Address_Line1',tra:Address_Line1).
    End

Value::tra:Address_Line1  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewBouncerJob_' & p_web._nocolon('tra:Address_Line1') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  If loc:retrying
    tra:Address_Line1 = p_web.RestoreValue('tra:Address_Line1')
    do ValidateValue::tra:Address_Line1
    If tra:Address_Line1:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- tra:Address_Line1
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tra:Address_Line1'',''viewbouncerjob_tra:address_line1_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tra:Address_Line1',p_web.GetSessionValueFormat('tra:Address_Line1'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::tra:Address_Line2  Routine
  packet = clip(packet) & p_web.DivHeader('ViewBouncerJob_' & p_web._nocolon('tra:Address_Line2') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tra:Address_Line2  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tra:Address_Line2 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    tra:Address_Line2 = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::tra:Address_Line2  ! copies value to session value if valid.
  do Value::tra:Address_Line2
  do SendAlert

ValidateValue::tra:Address_Line2  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tra:Address_Line2',tra:Address_Line2).
    End

Value::tra:Address_Line2  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewBouncerJob_' & p_web._nocolon('tra:Address_Line2') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  If loc:retrying
    tra:Address_Line2 = p_web.RestoreValue('tra:Address_Line2')
    do ValidateValue::tra:Address_Line2
    If tra:Address_Line2:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- tra:Address_Line2
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tra:Address_Line2'',''viewbouncerjob_tra:address_line2_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tra:Address_Line2',p_web.GetSessionValueFormat('tra:Address_Line2'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::tra:Address_Line3  Routine
  packet = clip(packet) & p_web.DivHeader('ViewBouncerJob_' & p_web._nocolon('tra:Address_Line3') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Suburb'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tra:Address_Line3  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tra:Address_Line3 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    tra:Address_Line3 = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::tra:Address_Line3  ! copies value to session value if valid.
  do Value::tra:Address_Line3
  do SendAlert

ValidateValue::tra:Address_Line3  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tra:Address_Line3',tra:Address_Line3).
    End

Value::tra:Address_Line3  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewBouncerJob_' & p_web._nocolon('tra:Address_Line3') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  If loc:retrying
    tra:Address_Line3 = p_web.RestoreValue('tra:Address_Line3')
    do ValidateValue::tra:Address_Line3
    If tra:Address_Line3:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- tra:Address_Line3
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tra:Address_Line3'',''viewbouncerjob_tra:address_line3_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tra:Address_Line3',p_web.GetSessionValueFormat('tra:Address_Line3'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::tra:Postcode  Routine
  packet = clip(packet) & p_web.DivHeader('ViewBouncerJob_' & p_web._nocolon('tra:Postcode') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Postcode'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tra:Postcode  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tra:Postcode = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s15
    tra:Postcode = p_web.Dformat(p_web.GetValue('Value'),'@s15')
  End
  do ValidateValue::tra:Postcode  ! copies value to session value if valid.
  do Value::tra:Postcode
  do SendAlert

ValidateValue::tra:Postcode  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tra:Postcode',tra:Postcode).
    End

Value::tra:Postcode  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewBouncerJob_' & p_web._nocolon('tra:Postcode') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  If loc:retrying
    tra:Postcode = p_web.RestoreValue('tra:Postcode')
    do ValidateValue::tra:Postcode
    If tra:Postcode:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- tra:Postcode
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tra:Postcode'',''viewbouncerjob_tra:postcode_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tra:Postcode',p_web.GetSessionValueFormat('tra:Postcode'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s15'),,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::tra:Telephone_Number  Routine
  packet = clip(packet) & p_web.DivHeader('ViewBouncerJob_' & p_web._nocolon('tra:Telephone_Number') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Telephone Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tra:Telephone_Number  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tra:Telephone_Number = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s15
    tra:Telephone_Number = p_web.Dformat(p_web.GetValue('Value'),'@s15')
  End
  do ValidateValue::tra:Telephone_Number  ! copies value to session value if valid.
  do Value::tra:Telephone_Number
  do SendAlert

ValidateValue::tra:Telephone_Number  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tra:Telephone_Number',tra:Telephone_Number).
    End

Value::tra:Telephone_Number  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewBouncerJob_' & p_web._nocolon('tra:Telephone_Number') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  If loc:retrying
    tra:Telephone_Number = p_web.RestoreValue('tra:Telephone_Number')
    do ValidateValue::tra:Telephone_Number
    If tra:Telephone_Number:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- tra:Telephone_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tra:Telephone_Number'',''viewbouncerjob_tra:telephone_number_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tra:Telephone_Number',p_web.GetSessionValueFormat('tra:Telephone_Number'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s15'),,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::tra:Fax_Number  Routine
  packet = clip(packet) & p_web.DivHeader('ViewBouncerJob_' & p_web._nocolon('tra:Fax_Number') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Fax Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tra:Fax_Number  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tra:Fax_Number = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s15
    tra:Fax_Number = p_web.Dformat(p_web.GetValue('Value'),'@s15')
  End
  do ValidateValue::tra:Fax_Number  ! copies value to session value if valid.
  do Value::tra:Fax_Number
  do SendAlert

ValidateValue::tra:Fax_Number  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tra:Fax_Number',tra:Fax_Number).
    End

Value::tra:Fax_Number  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewBouncerJob_' & p_web._nocolon('tra:Fax_Number') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  If loc:retrying
    tra:Fax_Number = p_web.RestoreValue('tra:Fax_Number')
    do ValidateValue::tra:Fax_Number
    If tra:Fax_Number:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- tra:Fax_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tra:Fax_Number'',''viewbouncerjob_tra:fax_number_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tra:Fax_Number',p_web.GetSessionValueFormat('tra:Fax_Number'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s15'),,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::tra:EmailAddress  Routine
  packet = clip(packet) & p_web.DivHeader('ViewBouncerJob_' & p_web._nocolon('tra:EmailAddress') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Email Address'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tra:EmailAddress  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tra:EmailAddress = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s255
    tra:EmailAddress = p_web.Dformat(p_web.GetValue('Value'),'@s255')
  End
  do ValidateValue::tra:EmailAddress  ! copies value to session value if valid.
  do Value::tra:EmailAddress
  do SendAlert

ValidateValue::tra:EmailAddress  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tra:EmailAddress',tra:EmailAddress).
    End

Value::tra:EmailAddress  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewBouncerJob_' & p_web._nocolon('tra:EmailAddress') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  If loc:retrying
    tra:EmailAddress = p_web.RestoreValue('tra:EmailAddress')
    do ValidateValue::tra:EmailAddress
    If tra:EmailAddress:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- tra:EmailAddress
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(60) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tra:EmailAddress'',''viewbouncerjob_tra:emailaddress_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tra:EmailAddress',p_web.GetSessionValueFormat('tra:EmailAddress'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s255'),'Email Address',,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Validate::TextDisplay  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::TextDisplay  ! copies value to session value if valid.

ValidateValue::TextDisplay  Routine
  If p_web.GetSessionValue('Local:NoAuthorise') = 0
    If not (1=0)
    End
  End

Value::TextDisplay  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ViewBouncerJob_' & p_web._nocolon('TextDisplay') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="TextDisplay" class="'&clip('green bold')&'"'&clip(loc:extra)&'>' & p_web.Translate('Select "Authorise" to use the above details on the job',) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('ViewBouncerJob_nexttab_' & 0)
    tra:Account_Number = p_web.GSV('tra:Account_Number')
    do ValidateValue::tra:Account_Number
    If loc:Invalid
      loc:retrying = 1
      do Value::tra:Account_Number
      !do SendAlert
      !exit
    End
    tra:Company_Name = p_web.GSV('tra:Company_Name')
    do ValidateValue::tra:Company_Name
    If loc:Invalid
      loc:retrying = 1
      do Value::tra:Company_Name
      !do SendAlert
      !exit
    End
    tra:Address_Line1 = p_web.GSV('tra:Address_Line1')
    do ValidateValue::tra:Address_Line1
    If loc:Invalid
      loc:retrying = 1
      do Value::tra:Address_Line1
      !do SendAlert
      !exit
    End
    tra:Address_Line2 = p_web.GSV('tra:Address_Line2')
    do ValidateValue::tra:Address_Line2
    If loc:Invalid
      loc:retrying = 1
      do Value::tra:Address_Line2
      !do SendAlert
      !exit
    End
    tra:Address_Line3 = p_web.GSV('tra:Address_Line3')
    do ValidateValue::tra:Address_Line3
    If loc:Invalid
      loc:retrying = 1
      do Value::tra:Address_Line3
      !do SendAlert
      !exit
    End
    tra:Postcode = p_web.GSV('tra:Postcode')
    do ValidateValue::tra:Postcode
    If loc:Invalid
      loc:retrying = 1
      do Value::tra:Postcode
      !do SendAlert
      !exit
    End
    tra:Telephone_Number = p_web.GSV('tra:Telephone_Number')
    do ValidateValue::tra:Telephone_Number
    If loc:Invalid
      loc:retrying = 1
      do Value::tra:Telephone_Number
      !do SendAlert
      !exit
    End
    tra:Fax_Number = p_web.GSV('tra:Fax_Number')
    do ValidateValue::tra:Fax_Number
    If loc:Invalid
      loc:retrying = 1
      do Value::tra:Fax_Number
      !do SendAlert
      !exit
    End
    tra:EmailAddress = p_web.GSV('tra:EmailAddress')
    do ValidateValue::tra:EmailAddress
    If loc:Invalid
      loc:retrying = 1
      do Value::tra:EmailAddress
      !do SendAlert
      !exit
    End
    If loc:Invalid then exit.
  of lower('ViewBouncerJob_nexttab_' & 1)
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_ViewBouncerJob_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('ViewBouncerJob_tab_' & 0)
    do GenerateTab0
  of lower('ViewBouncerJob_tra:Account_Number_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tra:Account_Number
      of event:timer
        do Value::tra:Account_Number
      else
        do Value::tra:Account_Number
      end
  of lower('ViewBouncerJob_tra:Company_Name_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tra:Company_Name
      of event:timer
        do Value::tra:Company_Name
      else
        do Value::tra:Company_Name
      end
  of lower('ViewBouncerJob_tra:Address_Line1_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tra:Address_Line1
      of event:timer
        do Value::tra:Address_Line1
      else
        do Value::tra:Address_Line1
      end
  of lower('ViewBouncerJob_tra:Address_Line2_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tra:Address_Line2
      of event:timer
        do Value::tra:Address_Line2
      else
        do Value::tra:Address_Line2
      end
  of lower('ViewBouncerJob_tra:Address_Line3_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tra:Address_Line3
      of event:timer
        do Value::tra:Address_Line3
      else
        do Value::tra:Address_Line3
      end
  of lower('ViewBouncerJob_tra:Postcode_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tra:Postcode
      of event:timer
        do Value::tra:Postcode
      else
        do Value::tra:Postcode
      end
  of lower('ViewBouncerJob_tra:Telephone_Number_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tra:Telephone_Number
      of event:timer
        do Value::tra:Telephone_Number
      else
        do Value::tra:Telephone_Number
      end
  of lower('ViewBouncerJob_tra:Fax_Number_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tra:Fax_Number
      of event:timer
        do Value::tra:Fax_Number
      else
        do Value::tra:Fax_Number
      end
  of lower('ViewBouncerJob_tra:EmailAddress_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tra:EmailAddress
      of event:timer
        do Value::tra:EmailAddress
      else
        do Value::tra:EmailAddress
      end
  of lower('ViewBouncerJob_tab_' & 1)
    do GenerateTab1
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('ViewBouncerJob_form:ready_',1)

  p_web.SetSessionValue('ViewBouncerJob_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_ViewBouncerJob',0)
  job_ali:date_booked = Today()
  p_web.SetSessionValue('job_ali:date_booked',job_ali:date_booked)
  job_ali:time_booked = Clock()
  p_web.SetSessionValue('job_ali:time_booked',job_ali:time_booked)
  job_ali:Cancelled = 'NO'
  p_web.SetSessionValue('job_ali:Cancelled',job_ali:Cancelled)
  job_ali:Warranty_Job = 'NO'
  p_web.SetSessionValue('job_ali:Warranty_Job',job_ali:Warranty_Job)
  job_ali:Chargeable_Job = 'NO'
  p_web.SetSessionValue('job_ali:Chargeable_Job',job_ali:Chargeable_Job)
  job_ali:Workshop = 'NO'
  p_web.SetSessionValue('job_ali:Workshop',job_ali:Workshop)
  job_ali:Insurance = 'NO'
  p_web.SetSessionValue('job_ali:Insurance',job_ali:Insurance)
  job_ali:Physical_Damage = 'NO'
  p_web.SetSessionValue('job_ali:Physical_Damage',job_ali:Physical_Damage)
  job_ali:Intermittent_Fault = 'NO'
  p_web.SetSessionValue('job_ali:Intermittent_Fault',job_ali:Intermittent_Fault)
  job_ali:Loan_Status = 'NOT ISSUED'
  p_web.SetSessionValue('job_ali:Loan_Status',job_ali:Loan_Status)
  job_ali:Exchange_Status = 'NOT ISSUED'
  p_web.SetSessionValue('job_ali:Exchange_Status',job_ali:Exchange_Status)
  job_ali:POP = 'NO'
  p_web.SetSessionValue('job_ali:POP',job_ali:POP)
  job_ali:In_Repair = 'NO'
  p_web.SetSessionValue('job_ali:In_Repair',job_ali:In_Repair)
  job_ali:On_Test = 'NO'
  p_web.SetSessionValue('job_ali:On_Test',job_ali:On_Test)
  job_ali:Estimate_Ready = 'NO'
  p_web.SetSessionValue('job_ali:Estimate_Ready',job_ali:Estimate_Ready)
  job_ali:QA_Passed = 'NO'
  p_web.SetSessionValue('job_ali:QA_Passed',job_ali:QA_Passed)
  job_ali:QA_Rejected = 'NO'
  p_web.SetSessionValue('job_ali:QA_Rejected',job_ali:QA_Rejected)
  job_ali:QA_Second_Passed = 'NO'
  p_web.SetSessionValue('job_ali:QA_Second_Passed',job_ali:QA_Second_Passed)
  job_ali:Date_Completed = 0
  p_web.SetSessionValue('job_ali:Date_Completed',job_ali:Date_Completed)
  job_ali:Time_Completed = 0
  p_web.SetSessionValue('job_ali:Time_Completed',job_ali:Time_Completed)
  job_ali:Completed = 'NO'
  p_web.SetSessionValue('job_ali:Completed',job_ali:Completed)
  job_ali:Paid = 'NO'
  p_web.SetSessionValue('job_ali:Paid',job_ali:Paid)
  job_ali:Paid_Warranty = 'NO'
  p_web.SetSessionValue('job_ali:Paid_Warranty',job_ali:Paid_Warranty)
  job_ali:Ignore_Chargeable_Charges = 'NO'
  p_web.SetSessionValue('job_ali:Ignore_Chargeable_Charges',job_ali:Ignore_Chargeable_Charges)
  job_ali:Ignore_Warranty_Charges = 'NO'
  p_web.SetSessionValue('job_ali:Ignore_Warranty_Charges',job_ali:Ignore_Warranty_Charges)
  job_ali:Ignore_Estimate_Charges = 'NO'
  p_web.SetSessionValue('job_ali:Ignore_Estimate_Charges',job_ali:Ignore_Estimate_Charges)
  job_ali:Loan_accessory = 'NO'
  p_web.SetSessionValue('job_ali:Loan_accessory',job_ali:Loan_accessory)
  job_ali:Exchange_Authorised = 'NO'
  p_web.SetSessionValue('job_ali:Exchange_Authorised',job_ali:Exchange_Authorised)
  job_ali:Loan_Authorised = 'NO'
  p_web.SetSessionValue('job_ali:Loan_Authorised',job_ali:Loan_Authorised)
  job_ali:Exchange_Accessory = 'NO'
  p_web.SetSessionValue('job_ali:Exchange_Accessory',job_ali:Exchange_Accessory)
  job_ali:Despatched = 'NO'
  p_web.SetSessionValue('job_ali:Despatched',job_ali:Despatched)
  job_ali:Estimate = 'NO'
  p_web.SetSessionValue('job_ali:Estimate',job_ali:Estimate)
  job_ali:Estimate_Accepted = 'NO'
  p_web.SetSessionValue('job_ali:Estimate_Accepted',job_ali:Estimate_Accepted)
  job_ali:Estimate_Rejected = 'NO'
  p_web.SetSessionValue('job_ali:Estimate_Rejected',job_ali:Estimate_Rejected)
  job_ali:Third_Party_Printed = 'NO'
  p_web.SetSessionValue('job_ali:Third_Party_Printed',job_ali:Third_Party_Printed)
  job_ali:Invoice_Exception = 'NO'
  p_web.SetSessionValue('job_ali:Invoice_Exception',job_ali:Invoice_Exception)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('ViewBouncerJob_form:ready_',1)
  p_web.SetSessionValue('ViewBouncerJob_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_ViewBouncerJob',0)
  p_web._PreCopyRecord(JOBS_ALIAS,job_ali:Ref_Number_Key)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('ViewBouncerJob_form:ready_',1)
  p_web.SetSessionValue('ViewBouncerJob_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('ViewBouncerJob:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('ViewBouncerJob_form:ready_',1)
  p_web.SetSessionValue('ViewBouncerJob_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('ViewBouncerJob:Primed',0)
  p_web.setsessionvalue('showtab_ViewBouncerJob',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord
  If p_web.Failed = 0
      p_web.SetSessionValue('job:Account_Number',p_web.GetSessionValue('sub:Account_Number'))
      If p_web.GetSessionValue('sub:Generic_Account') = 1
          p_web.SetSessionValue('FranchiseAccount',2)
      Else ! If p_web.GetSessionValue('sub:GenericAcount') = 1
          p_web.SetSessionValue('FranchiseAccount',1)
      End ! If p_web.GetSessionValue('sub:GenericAcount') = 1
      p_web.SetSessionValue('Hide:PreviousAddress',1)
      p_web.SSV('NewAccountAuthorised',1)
  End ! If p_web.Failed = 0

ValidateDelete  Routine
  p_web.DeleteSessionValue('ViewBouncerJob_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('ViewBouncerJob_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
    do ValidateValue::tra:Account_Number
    If loc:Invalid then exit.
    do ValidateValue::tra:Company_Name
    If loc:Invalid then exit.
    do ValidateValue::tra:Address_Line1
    If loc:Invalid then exit.
    do ValidateValue::tra:Address_Line2
    If loc:Invalid then exit.
    do ValidateValue::tra:Address_Line3
    If loc:Invalid then exit.
    do ValidateValue::tra:Postcode
    If loc:Invalid then exit.
    do ValidateValue::tra:Telephone_Number
    If loc:Invalid then exit.
    do ValidateValue::tra:Fax_Number
    If loc:Invalid then exit.
    do ValidateValue::tra:EmailAddress
    If loc:Invalid then exit.
  ! tab = 2
    loc:InvalidTab += 1
    do ValidateValue::TextDisplay
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
  ! Automatic Dictionary Validation
  If p_web.GetSessionValue('ViewBouncerJob:Primed') = 1 ! Autonumbered fields are not validated unless the record has been pre-primed
    If job_ali:Ref_Number = 0
      loc:Invalid = 'job_ali:Ref_Number'
      loc:Alert = p_web.translate('job_ali:Ref_Number') & ' ' & p_web.site.NotZeroText
    End
  End
  If Loc:Invalid <> '' then exit.
! NET:WEB:StagePOST
PostInsert      Routine
PostCopy        Routine
  p_web.SetSessionValue('ViewBouncerJob:Primed',0)
PostUpdate      Routine
  p_web.SetSessionValue('ViewBouncerJob:Primed',0)
  p_web.StoreValue('tra:Account_Number')
  p_web.StoreValue('tra:Company_Name')
  p_web.StoreValue('tra:Address_Line1')
  p_web.StoreValue('tra:Address_Line2')
  p_web.StoreValue('tra:Address_Line3')
  p_web.StoreValue('tra:Postcode')
  p_web.StoreValue('tra:Telephone_Number')
  p_web.StoreValue('tra:Fax_Number')
  p_web.StoreValue('tra:EmailAddress')
  p_web.StoreValue('')


PostDelete      Routine
MultipleBatchDespatch PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locJobNumber         STRING(30)                            !
locIMEINumber        STRING(30)                            !
locSecurityPackNumber STRING(30)                           !
locErrorMessage      STRING(255)                           !
locAccessoryMessage  STRING(255)                           !
locAccessoryErrorMessage STRING(255)                       !
locAccessoryPassword STRING(30)                            !
locAuditTrail        STRING(255)                           !
FilesOpened     Long
MULDESPJ::State  USHORT
MULDESP::State  USHORT
LOAN::State  USHORT
EXCHAMF::State  USHORT
COURIER::State  USHORT
TRADEACC::State  USHORT
SUBTRACC::State  USHORT
WEBJOB::State  USHORT
JOBSE::State  USHORT
JOBS::State  USHORT
BrowseBatchesInProgress:IsInvalid  Long
locJobNumber:IsInvalid  Long
locIMEINumber:IsInvalid  Long
locSecurityPackNumber:IsInvalid  Long
locErrorMessage:IsInvalid  Long
buttonProcessJob:IsInvalid  Long
TagValidateLoanAccessories:IsInvalid  Long
buttonValidateAccessories:IsInvalid  Long
locAccessoryMessage:IsInvalid  Long
locAccessoryErrorMessage:IsInvalid  Long
buttonConfirmMismatch:IsInvalid  Long
buttonFailAccessory:IsInvalid  Long
locAccessoryPassword:IsInvalid  Long
buttonAddToBatch:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('MultipleBatchDespatch')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'MultipleBatchDespatch_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('MultipleBatchDespatch','')
    p_web.DivHeader('MultipleBatchDespatch',p_web.combine(p_web.site.style.formdiv,'fdiv'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('MultipleBatchDespatch') = 0
        p_web.AddPreCall('MultipleBatchDespatch')
        p_web.DivHeader('popup_MultipleBatchDespatch','nt-hidden')
        p_web.DivHeader('MultipleBatchDespatch',p_web.combine(p_web.site.style.formdiv,'fdiv'))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_MultipleBatchDespatch_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_MultipleBatchDespatch_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferMultipleBatchDespatch',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_MultipleBatchDespatch',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferMultipleBatchDespatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_MultipleBatchDespatch',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferMultipleBatchDespatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_MultipleBatchDespatch',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_MultipleBatchDespatch',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferMultipleBatchDespatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_MultipleBatchDespatch',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferMultipleBatchDespatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_MultipleBatchDespatch',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_MultipleBatchDespatch',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('MultipleBatchDespatch')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
ClearVariables      ROUTINE
    p_web.SSV('locJobNumber','')
    p_web.SSV('locIMEINumber','')
    p_web.SSV('locSecurityPackNumber','')
    p_web.SSV('UnitValidated',0)
    p_web.SSV('Hide:Accessories',1)
    p_web.SSV('locAccessoryMessage','')
    p_web.SSV('locAccessoryErrorMessage','')
    p_web.SSV('AccessoryConfirmationRequired','')
    p_web.SSV('AccessoriesValidated','')
    p_web.SSV('ValidateButtonText','Validate Unit Details')
    p_web.SSV('Hide:AddToBatchButton',1)
    p_web.SSV('Hide:ValidateAccessoriesButton',0)
DeleteSessionValues ROUTINE
    p_web.DeleteSessionValue('locJobNumber')
    p_web.DeleteSessionValue('locIMEINumber')
    p_web.DeleteSessionValue('locSecurityPackNumber')
    p_web.DeleteSessionValue('locErrorMessage')
    p_web.DeleteSessionValue('Hide:Accessories')
    p_web.DeleteSessionValue('AccessoriesValidated')
    p_web.DeleteSessionValue('locAccessoryMessage')
    p_web.DeleteSessionValue('locAccessoryErrorMessage')
    p_web.DeleteSessionValue('locAccessoryPassword')
    p_web.DeleteSessionValue('Hide:AddToBatchButton')
    p_web.DeleteSessionValue('Hide:ValidateAccessoriesButton')
    p_web.DeleteSessionValue('AccessoryConfirmationRequired')
    p_web.DeleteSessionValue('UnitValidated')
ValidateUnitDetails         ROUTINE
DATA
txtJobInUse EQUATE('Error! The selected job is in use.')
txtMissingJob       EQUATE('Error! Cannot find the selected job.')
txtDespatchString   EQUATE('Error! The selected job is not ready for despatch.')
txtOutofRegion    EQUATE('Error! The delivery address is outside your region.')
txtNotPaid  EQUATE('Error! The selected job has not been paid.')
txtNotInvoiced      EQUATE('Error! The selected job has not been invoiced.')
txtNoCourier        EQUATE('Error! The selected unit does not have a courier assigned to it.')
txtLoanNotReturned  EQUATE('Error! The loan unit attached to this job has not been returned.')
txtAlreadyInBatch   EQUATE('Error! The selected job is already on a batch.')
txtWrongIMEI        EQUATE('Error! I.M.E.I. Number does not match the selected job.')
txtWrongExchangeIMEI        EQUATE('Error! I.M.E.I. Number does not match the selected job''s Exchange Unit.')
txtWrongLoanIMEI    EQUATE('Error! I.M.E.I. Number does not match the selected job''s Loan Unit.')
txtIndividualOnly   EQUATE('Error! The selected job can only be despatched individually and not as part of a batch.')
txtAccountOnStop    EQUATE('Error! Cannot despatch. The selected account is "on stop".')
locDespatchType     STRING(3)
CODE
    ClearTagFile(p_web)
    if (p_web.GSV('UnitValidated') = 1)
        Do ClearVariables
        exit
    end

    p_web.SSV('locErrorMessage','')
    IF (JobInUse(p_web.GSV('locJobNumber')))
        p_web.SSV('locErrorMessage',txtJobInUse)

        EXIT
    END

    Access:JOBS.ClearKey(job:Ref_Number_Key)
    job:Ref_Number = p_web.GSV('locJobNumber')
    IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
        p_web.SSV('locErrorMessage',txtMissingJob)
        EXIT
    END

    Access:JOBSE.ClearKey(jobe:RefNumberKey)
    jobe:RefNumber = job:Ref_Number
    IF (Access:JOBSE.TryFetch(jobe:RefNumberKey))
    END

    Access:WEBJOB.ClearKey(wob:RefNumberKey)
    wob:RefNumber = job:Ref_Number
    IF (Access:WEBJOB.TryFetch(wob:RefNumberKey))
    END


    IF (p_web.GSV('BookingSite') = 'RRC')
        locDespatchType = jobe:DespatchType
        IF (Inlist(jobe:DespatchType,'JOB','EXC','LOA') = 0)
            p_web.SSV('locErrorMessage',txtDespatchString)
            EXIT
        END

        Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
        sub:Account_Number = job:Account_Number
        IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
            IF (HubOutOfRegion(p_web.GSV('BookingAccount'),sub:Hub) = 1)
                IF (ReleasedForDespatch(job:Ref_Number) = 0)
                    p_web.SSV('locErrorMessage',txtOutOfRegion)
                    EXIT
                END
            END
        END


    ELSE ! IF (p_web.GSV('BookingSite') = 'RRC')
        locDespatchType = job:Despatch_Type
        IF (InList(job:Despatch_Type,'JOB','EXC','LOA') = 0)
            p_web.SSV('locErrorMessage',txtDespatchString)
            EXIT
        END
        CASE job:Despatch_Type
        OF 'JOB'
            IF (job:Consignment_Number <> '')
                p_web.SSV('locErrorMessage',txtDespatchString)
                EXIT
            END
        OF 'EXC'
            IF (job:Exchange_Consignment_Number <> '')
                p_web.SSV('locErrorMessage',txtDespatchString)
                EXIT
            END
        OF 'LOA'
            IF (job:Loan_Consignment_Number <> '')
                p_web.SSV('locErrorMessage',txtDespatchString)
                EXIT
            END
        END

    END ! IF (p_web.GSV('BookingSite') = 'RRC')

    ! Job Been Paid/Invoiced?
    IF (job:Chargeable_Job = 'YES')
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = p_web.GSV('BookingAccount')
        IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
            IF (tra:Despatch_Paid_Jobs = 'YES' AND NOT JobPaid(job:Ref_Number))
                p_web.SSV('locerrorMessage',txtNotPaid)
                EXIT
            END
            IsJobInvoiced(p_web)
            IF (tra:Despatch_Invoiced_Jobs = 'YES' AND p_web.GSV('IsJobInvoiced') <> 1)
                p_web.SSV('locErrorMessage',txtNotInvoiced)
                EXIT
            END

        END

    END

    ! Have courier attached?
    Access:COURIER.ClearKey(cou:Courier_Key)
    CASE locDespatchType
    OF 'JOB'
        IF (job:Courier = '')
            p_web.SSV('locErrorMessage',txtNoCourier)
            EXIT
        END
        cou:Courier = job:Courier
    OF 'EXC'
        IF (job:Exchange_Courier = '')
            p_web.SSV('locErrorMessage',txtNoCourier)
            EXIT
        END
        cou:Courier = job:Exchange_Courier
    OF 'LOA'
        IF (job:Loan_Courier = '')
            p_web.SSV('locErrorMessage',txtNoCourier)
            EXIT
        END
        cou:Courier = job:Loan_Courier
    END
    IF (Access:COURIER.tryfetch(cou:Courier_Key))
    END


    !Has the job got a loan attached?
    If GETINI('DESPATCH','DoNotDespatchLoan',,CLIP(PATH())&'\SB2KDEF.INI') = 1
        IF (p_web.GSV('BookingSite') = 'RRC' OR (p_web.GSV('BookingSite') = 'ARC' AND jobe:WebJob <> 1))
            ! #11817 Only stop despatch if RRC, or ARC back to customer. (Bryan: 11/05/2011)
            If job:Loan_Unit_Number <> 0 And locDespatchType = 'JOB'
                p_web.SSV('locErrorMessage',txtLoanNotReturned)
                EXIT
            End !If job:Loan_Unit_Number <> 0
        END ! IF (p_web.GSV('BookingSite') = 'RRC' OR (p_web.GSV('BookingSite') = 'ARC' AND jobe:WebJob <> 1))

    End !If GETINI('DESPATCH','DoNotDespatchLoan',,CLIP(PATH())&'\SB2KDEF.INI') = 1

    IF (vod.IsTheJobAlreadyInBatch(job:Ref_Number,p_web.GSV('BookingAccount')))
        p_web.SSV('locErrorMessage',txtAlreadyInBatch)
        EXIT
    END
    p_web.SSV('AccessoryRefNumber',job:Ref_Number)

    ! Check IMEI Number
    CASE locDespatchType
    OF 'JOB'
        IF (p_web.GSV('locIMEINumber') <> job:ESN)
            p_web.SSV('locErrorMessage',txtWrongIMEI)
            EXIT
        END
        p_web.SSV('tmp:LoanModelNumber',job:Model_Number)
    OF 'EXC'
        Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
        xch:Ref_Number = job:Exchange_Unit_Number
        IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key))
            p_web.SSV('locErrorMessage',txtWrongExchangeIMEI)
            EXIT
        ELSE
            IF (xch:ESN <> p_web.GSV('locIMEINumber'))
                p_web.SSV('locErrorMessage',txtWrongExchangeIMEI)
                EXIT
            END

        END

    OF 'LOA'
        Access:LOAN.ClearKey(loa:Ref_Number_Key)
        loa:Ref_Number = job:Loan_Unit_Number
        IF (Access:LOAN.TryFetch(loa:Ref_Number_Key))
            p_web.SSV('locErrorMessage',txtWrongLoanIMEI)
            EXIT
        ELSE
            IF (loa:ESN <> p_web.GSV('locIMEINumber'))
                p_web.SSV('locErrorMessage',txtWrongLoanIMEI)
                EXIT
            END
            p_web.SSV('AccessoryRefNumber',loa:Ref_Number)
        END

    END

    ! Check if it has to be indivdual despatch only.
    Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
    sub:Account_Number = job:Account_Number
    IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = sub:Main_Account_Number
        IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
            If tra:Use_Sub_Accounts = 'YES' And tra:Invoice_Sub_Accounts = 'YES'
                If job:Chargeable_Job = 'YES' And sub:Stop_Account = 'YES'
                    p_web.SSV('locErrorMessage',txtAccountOnStop)
                    EXIT
                End !If job:Chargeable_Job = 'YES' And sub:Stop_Account = 'YES'
                If sub:UseCustDespAdd = 'YES'
                    p_web.SSV('locErrorMessage',txtIndividualOnly)
                    EXIT
                End !If sub:UseCustDespAdd = 'YES'
            Else !If tra:Use_Sub_Accounts = 'YES' And tra:Invoice_Sub_Accounts = 'YES'
                If job:Chargeable_Job = 'YES' And tra:Stop_Account = 'YES'
                    p_web.SSV('locErrorMessage',txtAccountOnStop)
                    EXIT
                End !If job:Chargeable_Job = 'YES' And tra:Stop_Account = 'YES'
                If tra:UseCustDespAdd = 'YES'
                    p_web.SSV('locErrorMessage',txtIndividualOnly)
                    EXIT
                End !If tra:UseCustDespAdd = 'YES'
            End !If tra:Use_Sub_Accounts = 'YES' And tra:Invoice_Sub_Accounts = 'YES'
        END
    END


! A Ok
    p_web.SSV('Hide:Accessories',0)
    p_web.SSV('AccessoriesValidated',0)
    p_web.SSV('DespatchType',locDespatchType)
    p_web.SSV('UnitValidated',1)
    p_web.SSV('ValidateButtonText','Despatch Another Unit')

    p_web.FileToSessionQueue(JOBS)
    p_web.FileToSessionQueue(WEBJOB)
    p_web.FileToSessionQueue(JOBSE)
    p_web.FileToSessionQueue(COURIER)



OpenFiles  ROUTINE
  p_web._OpenFile(MULDESPJ)
  p_web._OpenFile(MULDESP)
  p_web._OpenFile(LOAN)
  p_web._OpenFile(EXCHAMF)
  p_web._OpenFile(COURIER)
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(SUBTRACC)
  p_web._OpenFile(WEBJOB)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(JOBS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(MULDESPJ)
  p_Web._CloseFile(MULDESP)
  p_Web._CloseFile(LOAN)
  p_Web._CloseFile(EXCHAMF)
  p_Web._CloseFile(COURIER)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(JOBS)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE
  loc:EipClm = upper(p_web.GetValue('_EIPClm'))
  p_web.DeleteValue('_EIPClm')
  case loc:EipClm
  of ''
    case upper(p_web.GetValue('_calledfrom_'))
    of upper('BrowseBatchesInProgress')
      do Value::BrowseBatchesInProgress
    of upper('TagValidateLoanAccessories')
      do Value::TagValidateLoanAccessories
    end
  end

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.site.CancelButton.TextValue = 'Close'
  p_web.SetValue('MultipleBatchDespatch_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'MultipleBatchDespatch'
    end
    p_web.formsettings.proc = 'MultipleBatchDespatch'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine
  DO DeleteSessionvalues

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  if p_web.IfExistsValue('locJobNumber') = 0
    p_web.SetSessionValue('locJobNumber',locJobNumber)
  Else
    locJobNumber = p_web.GetSessionValue('locJobNumber')
  End
  if p_web.IfExistsValue('locIMEINumber') = 0
    p_web.SetSessionValue('locIMEINumber',locIMEINumber)
  Else
    locIMEINumber = p_web.GetSessionValue('locIMEINumber')
  End
  if p_web.IfExistsValue('locSecurityPackNumber') = 0
    p_web.SetSessionValue('locSecurityPackNumber',locSecurityPackNumber)
  Else
    locSecurityPackNumber = p_web.GetSessionValue('locSecurityPackNumber')
  End
  if p_web.IfExistsValue('locErrorMessage') = 0
    p_web.SetSessionValue('locErrorMessage',locErrorMessage)
  Else
    locErrorMessage = p_web.GetSessionValue('locErrorMessage')
  End
  if p_web.IfExistsValue('locAccessoryMessage') = 0
    p_web.SetSessionValue('locAccessoryMessage',locAccessoryMessage)
  Else
    locAccessoryMessage = p_web.GetSessionValue('locAccessoryMessage')
  End
  if p_web.IfExistsValue('locAccessoryErrorMessage') = 0
    p_web.SetSessionValue('locAccessoryErrorMessage',locAccessoryErrorMessage)
  Else
    locAccessoryErrorMessage = p_web.GetSessionValue('locAccessoryErrorMessage')
  End
  if p_web.IfExistsValue('locAccessoryPassword') = 0
    p_web.SetSessionValue('locAccessoryPassword',locAccessoryPassword)
  Else
    locAccessoryPassword = p_web.GetSessionValue('locAccessoryPassword')
  End

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locJobNumber')
    locJobNumber = p_web.GetValue('locJobNumber')
    p_web.SetSessionValue('locJobNumber',locJobNumber)
  Else
    locJobNumber = p_web.GetSessionValue('locJobNumber')
  End
  if p_web.IfExistsValue('locIMEINumber')
    locIMEINumber = p_web.GetValue('locIMEINumber')
    p_web.SetSessionValue('locIMEINumber',locIMEINumber)
  Else
    locIMEINumber = p_web.GetSessionValue('locIMEINumber')
  End
  if p_web.IfExistsValue('locSecurityPackNumber')
    locSecurityPackNumber = p_web.GetValue('locSecurityPackNumber')
    p_web.SetSessionValue('locSecurityPackNumber',locSecurityPackNumber)
  Else
    locSecurityPackNumber = p_web.GetSessionValue('locSecurityPackNumber')
  End
  if p_web.IfExistsValue('locErrorMessage')
    locErrorMessage = p_web.GetValue('locErrorMessage')
    p_web.SetSessionValue('locErrorMessage',locErrorMessage)
  Else
    locErrorMessage = p_web.GetSessionValue('locErrorMessage')
  End
  if p_web.IfExistsValue('locAccessoryMessage')
    locAccessoryMessage = p_web.GetValue('locAccessoryMessage')
    p_web.SetSessionValue('locAccessoryMessage',locAccessoryMessage)
  Else
    locAccessoryMessage = p_web.GetSessionValue('locAccessoryMessage')
  End
  if p_web.IfExistsValue('locAccessoryErrorMessage')
    locAccessoryErrorMessage = p_web.GetValue('locAccessoryErrorMessage')
    p_web.SetSessionValue('locAccessoryErrorMessage',locAccessoryErrorMessage)
  Else
    locAccessoryErrorMessage = p_web.GetSessionValue('locAccessoryErrorMessage')
  End
  if p_web.IfExistsValue('locAccessoryPassword')
    locAccessoryPassword = p_web.GetValue('locAccessoryPassword')
    p_web.SetSessionValue('locAccessoryPassword',locAccessoryPassword)
  Else
    locAccessoryPassword = p_web.GetSessionValue('locAccessoryPassword')
  End

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('MultipleBatchDespatch_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferMultipleBatchDespatch')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('MultipleBatchDespatch_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('MultipleBatchDespatch_ChainTo')
    loc:formaction = p_web.GetSessionValue('MultipleBatchDespatch_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'IndexPage'

GenerateForm   Routine
  do LoadRelatedRecords
  ! Add To Batch
  IF p_web.IfExistsValue('Action')
      p_web.StoreValue('Action')
      IF p_web.IfExistsValue('BatchNumber')
          p_web.StoreValue('BatchNumber')
          IF (p_web.GSV('Action') = 'AddToBatch')
              ! Start
              IF (p_web.GSV('BatchNumber') > 0)
                  Access:MULDESP.ClearKey(muld:RecordNumberKey)
                  muld:RecordNumber = p_web.GSV('BatchNumber')
                  IF (Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign)
                      IF (p_web.GSV('job:Ref_Number') > 0)
                          ! Check job isn't already there
                          Access:MULDESPJ.ClearKey(mulj:JobNumberKey)
                          mulj:RefNumber = muld:RecordNumber
                          mulj:JobNumber = p_web.GSV('job:Ref_Number')
                          IF (Access:MULDESPJ.TryFetch(mulj:JobNumberKey))
                              IF (Access:MULDESPJ.PrimeRecord() = Level:Benign)
                                  mulj:RefNumber = muld:RecordNumber
                                  mulj:JobNumber = p_web.GSV('job:Ref_Number')
                                  mulj:IMEINumber = p_web.GSV('job:ESN')
                                  mulj:MSN = p_web.GSV('job:MSN')
                                  mulj:AccountNumber = p_web.GSV('job:Account_Number')
                                  mulj:SecurityPackNumber = p_web.GSV('locSecurityPackNumber')
                                  IF (p_web.GSV('BookingSite') = 'RRC')
                                      CASE p_web.GSV('jobe:DespatchType')
                                      OF 'JOB'
                                          mulj:Courier = p_web.GSV('job:Courier')
                                      OF 'EXC'
                                          mulj:Courier = p_web.GSV('job:Exchange_Courier')
                                      OF 'LOA'
                                          mulj:Courier = p_web.GSV('job:Loan_Courier')
                                      END
  
                                  ELSE
                                      CASE p_web.GSV('job:Despatch_Type')
                                      OF 'JOB'
                                          mulj:Courier = p_web.GSV('job:Courier')
                                      OF 'EXC'
                                          mulj:Courier = p_web.GSV('job:Exchange_Courier')
                                      OF 'LOA'
                                          mulj:Courier = p_web.GSV('job:Loan_Courier')
                                      END
  
                                  END
                                  IF (muld:BatchType = 'TRA')
                                      mulj:AccountNumber = p_web.GSV('job:Account_Number')
                                      mulj:Courier = muld:Courier
                                  END
                                  IF (Access:MULDESPJ.TryInsert() = Level:Benign)
                                      CountBatch# = 0
                                      Access:MULDESPJ.ClearKey(mulj:JobNumberKey)
                                      mulj:RefNumber = muld:RecordNumber
                                      Set(mulj:JobNumberKey,mulj:JobNumberKey)
                                      Loop
                                          If Access:MULDESPJ.NEXT()
                                              Break
                                          End !If
                                          If mulj:RefNumber <> muld:RecordNumber      |
                                              Then Break.  ! End If
                                          CountBatch# += 1
                                      End !Loop
  
                                      muld:BatchTotal = CountBatch#
                                      Access:MULDESP.Update()
                                  ELSE
                                      Access:MULDESPJ.CancelAutoInc()
                                  END
                              END
                          END
                      END
                  END
              END
          END
      END
  END
  
  p_web.DeleteSessionValue('Action')
  p_web.DeleteSessionValue('BatchNumber')
  p_web.DeleteSessionValue('job:Ref_Number')
  
  
  ClearTagFile(p_web)
  DO ClearVariables
 locJobNumber = p_web.RestoreValue('locJobNumber')
 locIMEINumber = p_web.RestoreValue('locIMEINumber')
 locSecurityPackNumber = p_web.RestoreValue('locSecurityPackNumber')
 locErrorMessage = p_web.RestoreValue('locErrorMessage')
 locAccessoryMessage = p_web.RestoreValue('locAccessoryMessage')
 locAccessoryErrorMessage = p_web.RestoreValue('locAccessoryErrorMessage')
 locAccessoryPassword = p_web.RestoreValue('locAccessoryPassword')
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Multiple Batch Despatch') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Multiple Batch Despatch',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_MultipleBatchDespatch',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_MultipleBatchDespatch0_div')&'">'&p_web.Translate('Batches In Progress')&'</a></li>'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_MultipleBatchDespatch1_div')&'">'&p_web.Translate('Input Batch')&'</a></li>'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_MultipleBatchDespatch2_div')&'">'&p_web.Translate()&'</a></li>'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_MultipleBatchDespatch3_div')&'">'&p_web.Translate('Confirm')&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  do GenerateTab3
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="MultipleBatchDespatch_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('MultipleBatchDespatch_BrowseBatchesInProgress_embedded_div')&''');'
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('MultipleBatchDespatch_TagValidateLoanAccessories_embedded_div')&''');'
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.cancel();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CancelButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="MultipleBatchDespatch_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'MultipleBatchDespatch_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="MultipleBatchDespatch_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'MultipleBatchDespatch_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_MultipleBatchDespatch')>0,p_web.GSV('showtab_MultipleBatchDespatch'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_MultipleBatchDespatch'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_MultipleBatchDespatch') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_MultipleBatchDespatch'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_MultipleBatchDespatch')>0,p_web.GSV('showtab_MultipleBatchDespatch'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_MultipleBatchDespatch') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Batches In Progress') & ''''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Input Batch') & ''''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Confirm') & ''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_MultipleBatchDespatch_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_MultipleBatchDespatch')>0,p_web.GSV('showtab_MultipleBatchDespatch'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"MultipleBatchDespatch",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_MultipleBatchDespatch')>0,p_web.GSV('showtab_MultipleBatchDespatch'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_MultipleBatchDespatch_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('MultipleBatchDespatch') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('MultipleBatchDespatch')
    p_web.SetValue('_CallPopups',2)
    If p_web.GetPreCall('BrowseBatchesInProgress') = 0
      p_web.SetValue('BrowseBatchesInProgress:NoForm',1)
      p_web.SetValue('BrowseBatchesInProgress:FormName',loc:formname)
      p_web.SetValue('BrowseBatchesInProgress:parentIs','Form')
      p_web.SetValue('_parentProc','MultipleBatchDespatch')
      BrowseBatchesInProgress(p_web)
      p_web.SetValue('_CallPopups',0)
      p_web.DeleteValue('BrowseBatchesInProgress:NoForm')
      p_web.DeleteValue('BrowseBatchesInProgress:FormName')
      p_web.DeleteValue('BrowseBatchesInProgress:parentIs')
      p_web.DeleteValue('_parentProc')
    End
    p_web.SetValue('_CallPopups',2)
    If p_web.GetPreCall('TagValidateLoanAccessories') = 0
      p_web.SetValue('TagValidateLoanAccessories:NoForm',1)
      p_web.SetValue('TagValidateLoanAccessories:FormName',loc:formname)
      p_web.SetValue('TagValidateLoanAccessories:parentIs','Form')
      p_web.SetValue('_parentProc','MultipleBatchDespatch')
      TagValidateLoanAccessories(p_web)
      p_web.SetValue('_CallPopups',0)
      p_web.DeleteValue('TagValidateLoanAccessories:NoForm')
      p_web.DeleteValue('TagValidateLoanAccessories:FormName')
      p_web.DeleteValue('TagValidateLoanAccessories:parentIs')
      p_web.DeleteValue('_parentProc')
    End
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Batches In Progress')&'</a></h3>' & CRLF & p_web.DivHeader('tab_MultipleBatchDespatch0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_MultipleBatchDespatch0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_MultipleBatchDespatch0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_MultipleBatchDespatch0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Batches In Progress')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_MultipleBatchDespatch0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Batches In Progress')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_MultipleBatchDespatch0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Batches In Progress')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_MultipleBatchDespatch0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::BrowseBatchesInProgress
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab1  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Input Batch')&'</a></h3>' & CRLF & p_web.DivHeader('tab_MultipleBatchDespatch1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_MultipleBatchDespatch1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_MultipleBatchDespatch1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_MultipleBatchDespatch1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Input Batch')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_MultipleBatchDespatch1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Input Batch')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_MultipleBatchDespatch1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Input Batch')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_MultipleBatchDespatch1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locJobNumber
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locJobNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locJobNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locIMEINumber
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locIMEINumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locIMEINumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locSecurityPackNumber
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locSecurityPackNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locSecurityPackNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locErrorMessage
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locErrorMessage
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::buttonProcessJob
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::buttonProcessJob
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab2  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate()&'</a></h3>' & CRLF & p_web.DivHeader('tab_MultipleBatchDespatch2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_MultipleBatchDespatch2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_MultipleBatchDespatch2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_MultipleBatchDespatch2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,)
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_MultipleBatchDespatch2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend)
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_MultipleBatchDespatch2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain">' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_MultipleBatchDespatch2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::TagValidateLoanAccessories
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::buttonValidateAccessories
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::buttonValidateAccessories
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locAccessoryMessage
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locAccessoryMessage
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locAccessoryErrorMessage
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locAccessoryErrorMessage
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::buttonConfirmMismatch
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::buttonConfirmMismatch
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::buttonFailAccessory
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::buttonFailAccessory
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locAccessoryPassword
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locAccessoryPassword
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locAccessoryPassword
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab3  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Confirm')&'</a></h3>' & CRLF & p_web.DivHeader('tab_MultipleBatchDespatch3',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_MultipleBatchDespatch3',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_MultipleBatchDespatch3',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_MultipleBatchDespatch3',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Confirm')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_MultipleBatchDespatch3',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Confirm')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_MultipleBatchDespatch3',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Confirm')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_MultipleBatchDespatch3',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::buttonAddToBatch
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::buttonAddToBatch
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Validate::BrowseBatchesInProgress  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  Elsif true
    p_web.StoreValue('muld:RecordNumber')
  End
  do ValidateValue::BrowseBatchesInProgress  ! copies value to session value if valid.
  do Comment::BrowseBatchesInProgress ! allows comment style to be updated.

ValidateValue::BrowseBatchesInProgress  Routine
    If not (1=0)
    End

Value::BrowseBatchesInProgress  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(1=0,1,0))
  ! --- BROWSE ---  BrowseBatchesInProgress --
  p_web.SetValue('BrowseBatchesInProgress:NoForm',1)
  p_web.SetValue('BrowseBatchesInProgress:FormName',loc:formname)
  p_web.SetValue('BrowseBatchesInProgress:parentIs','Form')
  p_web.SetValue('_parentProc','MultipleBatchDespatch')
  if p_web.RequestAjax = 0
    p_web.SSV('MultipleBatchDespatch:_popup_',p_web.GetValue('_popup_')) ! stores the current browse popup state
    packet = clip(packet) & '<div id="'&lower('MultipleBatchDespatch_BrowseBatchesInProgress_embedded_div')&'"><!-- Net:BrowseBatchesInProgress --></div><13,10>'
    do SendPacket
    p_web.DivHeader('MultipleBatchDespatch_' & lower('BrowseBatchesInProgress') & '_value')
    p_web.DivFooter()
  else
    if p_web.GetValue('_popup_') = 1
      p_web.SSV('MultipleBatchDespatch:_popup_',1)
    elsif p_web.GSV('MultipleBatchDespatch:_popup_') = 1
      p_web.SetValue('_popup_',1)
    end
    packet = clip(packet) & '<!-- Net:BrowseBatchesInProgress --><13,10>'
  end
  do SendPacket
Comment::BrowseBatchesInProgress  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if BrowseBatchesInProgress:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('MultipleBatchDespatch_' & p_web._nocolon('BrowseBatchesInProgress') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locJobNumber  Routine
  packet = clip(packet) & p_web.DivHeader('MultipleBatchDespatch_' & p_web._nocolon('locJobNumber') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Job Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locJobNumber  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locJobNumber = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locJobNumber = p_web.GetValue('Value')
  End
  do ValidateValue::locJobNumber  ! copies value to session value if valid.
  ! Clear error
  p_web.SSV('locErrorMessage','')
  do Value::locJobNumber
  do SendAlert
  do Comment::locJobNumber ! allows comment style to be updated.
  do Value::locErrorMessage  !1

ValidateValue::locJobNumber  Routine
    If not (1=0)
  If locJobNumber = ''
    loc:Invalid = 'locJobNumber'
    locJobNumber:IsInvalid = true
    loc:alert = p_web.translate('Job Number') & ' ' & p_web.site.RequiredText
  End
      if loc:invalid = '' then p_web.SetSessionValue('locJobNumber',locJobNumber).
    End

Value::locJobNumber  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('MultipleBatchDespatch_' & p_web._nocolon('locJobNumber') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,'formrqd')
  If loc:retrying
    locJobNumber = p_web.RestoreValue('locJobNumber')
    do ValidateValue::locJobNumber
    If locJobNumber:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- locJobNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locJobNumber'',''multiplebatchdespatch_locjobnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locJobNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locJobNumber',p_web.GetSessionValueFormat('locJobNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locJobNumber  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locJobNumber:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web._jsok(p_web.site.RequiredText)
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('MultipleBatchDespatch_' & p_web._nocolon('locJobNumber') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locIMEINumber  Routine
  packet = clip(packet) & p_web.DivHeader('MultipleBatchDespatch_' & p_web._nocolon('locIMEINumber') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('I.M.E.I. Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locIMEINumber  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locIMEINumber = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locIMEINumber = p_web.GetValue('Value')
  End
  do ValidateValue::locIMEINumber  ! copies value to session value if valid.
  ! Clear error
  p_web.SSV('locErrorMessage','')
  do Value::locIMEINumber
  do SendAlert
  do Comment::locIMEINumber ! allows comment style to be updated.
  do Value::locErrorMessage  !1

ValidateValue::locIMEINumber  Routine
    If not (1=0)
  If locIMEINumber = ''
    loc:Invalid = 'locIMEINumber'
    locIMEINumber:IsInvalid = true
    loc:alert = p_web.translate('I.M.E.I. Number') & ' ' & p_web.site.RequiredText
  End
      if loc:invalid = '' then p_web.SetSessionValue('locIMEINumber',locIMEINumber).
    End

Value::locIMEINumber  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('MultipleBatchDespatch_' & p_web._nocolon('locIMEINumber') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,'formrqd')
  If loc:retrying
    locIMEINumber = p_web.RestoreValue('locIMEINumber')
    do ValidateValue::locIMEINumber
    If locIMEINumber:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- locIMEINumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locIMEINumber'',''multiplebatchdespatch_locimeinumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locIMEINumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locIMEINumber',p_web.GetSessionValueFormat('locIMEINumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locIMEINumber  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locIMEINumber:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web._jsok(p_web.site.RequiredText)
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('MultipleBatchDespatch_' & p_web._nocolon('locIMEINumber') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locSecurityPackNumber  Routine
  packet = clip(packet) & p_web.DivHeader('MultipleBatchDespatch_' & p_web._nocolon('locSecurityPackNumber') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Security Pack No'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locSecurityPackNumber  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locSecurityPackNumber = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locSecurityPackNumber = p_web.GetValue('Value')
  End
  do ValidateValue::locSecurityPackNumber  ! copies value to session value if valid.
  ! Clear error
  p_web.SSV('locErrorMessage','')
  do Value::locSecurityPackNumber
  do SendAlert
  do Comment::locSecurityPackNumber ! allows comment style to be updated.
  do Value::locErrorMessage  !1

ValidateValue::locSecurityPackNumber  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locSecurityPackNumber',locSecurityPackNumber).
    End

Value::locSecurityPackNumber  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('MultipleBatchDespatch_' & p_web._nocolon('locSecurityPackNumber') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    locSecurityPackNumber = p_web.RestoreValue('locSecurityPackNumber')
    do ValidateValue::locSecurityPackNumber
    If locSecurityPackNumber:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- locSecurityPackNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locSecurityPackNumber'',''multiplebatchdespatch_locsecuritypacknumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locSecurityPackNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locSecurityPackNumber',p_web.GetSessionValueFormat('locSecurityPackNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locSecurityPackNumber  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locSecurityPackNumber:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('MultipleBatchDespatch_' & p_web._nocolon('locSecurityPackNumber') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::locErrorMessage  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locErrorMessage = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locErrorMessage = p_web.GetValue('Value')
  End
  do ValidateValue::locErrorMessage  ! copies value to session value if valid.
  do Comment::locErrorMessage ! allows comment style to be updated.

ValidateValue::locErrorMessage  Routine
    If not (p_web.GSV('locErrorMessage') = '')
      if loc:invalid = '' then p_web.SetSessionValue('locErrorMessage',locErrorMessage).
    End

Value::locErrorMessage  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('locErrorMessage') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('MultipleBatchDespatch_' & p_web._nocolon('locErrorMessage') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (p_web.GSV('locErrorMessage') = '')
  ! --- DISPLAY --- locErrorMessage
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('red bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locErrorMessage'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locErrorMessage  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locErrorMessage:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('locErrorMessage') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('MultipleBatchDespatch_' & p_web._nocolon('locErrorMessage') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('locErrorMessage') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonProcessJob  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonProcessJob  ! copies value to session value if valid.
  DO ValidateUnitDetails
  do Value::buttonProcessJob
  do Comment::buttonProcessJob ! allows comment style to be updated.
  do Value::locErrorMessage  !1
  do Value::locIMEINumber  !1
  do Value::locJobNumber  !1
  do Value::locSecurityPackNumber  !1
  do Value::TagValidateLoanAccessories  !1
  do Value::buttonValidateAccessories  !1
  do Value::buttonConfirmMismatch  !1
  do Value::buttonFailAccessory  !1
  do Value::locAccessoryErrorMessage  !1
  do Value::locAccessoryMessage  !1
  do Prompt::locAccessoryPassword
  do Value::locAccessoryPassword  !1
  do Value::buttonAddToBatch  !1

ValidateValue::buttonProcessJob  Routine
    If not (1=0)
    End

Value::buttonProcessJob  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('MultipleBatchDespatch_' & p_web._nocolon('buttonProcessJob') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonProcessJob'',''multiplebatchdespatch_buttonprocessjob_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','ProcessJob',p_web.GSV('ValidateButtonText'),p_web.combine(Choose(p_web.GSV('ValidateButtonText') <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'button-entryfield'),loc:formname,,,,loc:javascript,loc:disabled,,,,,,,,,,'nt-left')!h
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonProcessJob  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonProcessJob:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('MultipleBatchDespatch_' & p_web._nocolon('buttonProcessJob') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::TagValidateLoanAccessories  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  Elsif true
    p_web.StoreValue('acr:Accessory')
  End
  do ValidateValue::TagValidateLoanAccessories  ! copies value to session value if valid.
  do Comment::TagValidateLoanAccessories ! allows comment style to be updated.

ValidateValue::TagValidateLoanAccessories  Routine
    If not (p_web.GSV('Hide:Accessories') = 1)
    End

Value::TagValidateLoanAccessories  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(p_web.GSV('Hide:Accessories') = 1,1,0))
  ! --- BROWSE ---  TagValidateLoanAccessories --
  p_web.SetValue('TagValidateLoanAccessories:NoForm',1)
  p_web.SetValue('TagValidateLoanAccessories:FormName',loc:formname)
  p_web.SetValue('TagValidateLoanAccessories:parentIs','Form')
  p_web.SetValue('_parentProc','MultipleBatchDespatch')
  if p_web.RequestAjax = 0
    p_web.SSV('MultipleBatchDespatch:_popup_',p_web.GetValue('_popup_')) ! stores the current browse popup state
    packet = clip(packet) & '<div id="'&lower('MultipleBatchDespatch_TagValidateLoanAccessories_embedded_div')&'"><!-- Net:TagValidateLoanAccessories --></div><13,10>'
    do SendPacket
    p_web.DivHeader('MultipleBatchDespatch_' & lower('TagValidateLoanAccessories') & '_value')
    p_web.DivFooter()
  else
    if p_web.GetValue('_popup_') = 1
      p_web.SSV('MultipleBatchDespatch:_popup_',1)
    elsif p_web.GSV('MultipleBatchDespatch:_popup_') = 1
      p_web.SetValue('_popup_',1)
    end
    packet = clip(packet) & '<!-- Net:TagValidateLoanAccessories --><13,10>'
  end
  do SendPacket
Comment::TagValidateLoanAccessories  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if TagValidateLoanAccessories:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:Accessories') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('MultipleBatchDespatch_' & p_web._nocolon('TagValidateLoanAccessories') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:Accessories') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonValidateAccessories  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonValidateAccessories  ! copies value to session value if valid.
  ! Validate Accessories
  p_web.SSV('locAccessoryErrorMessage','')
  p_web.SSV('locAccessoryMessage','')
  p_web.SSV('Hide:ValidateAccessoriesButton',1)
  p_web.SSV('AccessoryConfirmationRequired',0)
  p_web.SSV('AccessoryPasswordRequired',0)
  p_web.SSV('AccessoriesValidated',0)
  p_web.SSV('Hide:AddToBatchButton',1)
  
  ! Validate
  p_web.SSV('AccessoryCheck:Type',p_web.GSV('DespatchType'))
  p_web.SSV('AccessoryCheck:RefNumber',p_web.GSV('AccessoryRefNumber'))
  AccessoryCheck(p_web)
  Case p_web.GSV('AccessoryCheck:Return')
  Of 1 ! Missing
      p_web.SSV('locAccessoryErrorMessage','The selected unit has a missing accessory.')
      p_web.SSV('AccessoryConfirmationRequired',1)
      p_web.SSV('ConfirmMismatchText','Confirm Access. Validation')
  
      if (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'ACCESSORY MISMATCH - ACCEPT'))
          p_web.SSV('AccessoryPasswordRequired',1)
      end
  Of 2 ! Mismatch
      p_web.SSV('locAccessoryErrorMessage','There is a mismatch between the selected unit''s accessories.')
      p_web.SSV('AccessoryConfirmationRequired',1)
      p_web.SSV('ConfirmMismatchText','Confirm Access. Validation')
      if (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'ACCESSORY MISMATCH - ACCEPT'))
          p_web.SSV('AccessoryPasswordRequired',1)
      end
  Else ! A Ok
      p_web.SSV('locAccessoryMessage','Accessory Validated')
      p_web.SSV('AccessoryConfirmationRequired',0)
      p_web.SSV('AccessoriesValidated',1)
      p_web.SSV('Hide:AddToBatchButton',0)
      !DO ShowConsignmentNumber
  End
  do Value::buttonValidateAccessories
  do Comment::buttonValidateAccessories ! allows comment style to be updated.
  do Value::buttonConfirmMismatch  !1
  do Value::buttonFailAccessory  !1
  do Value::locAccessoryErrorMessage  !1
  do Value::locAccessoryMessage  !1
  do Prompt::locAccessoryPassword
  do Value::locAccessoryPassword  !1
  do Value::buttonAddToBatch  !1

ValidateValue::buttonValidateAccessories  Routine
    If not (p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('Hide:ValidateAccessoriesButton') = 1)
    End

Value::buttonValidateAccessories  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('Hide:ValidateAccessoriesButton') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('MultipleBatchDespatch_' & p_web._nocolon('buttonValidateAccessories') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('Hide:ValidateAccessoriesButton') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonValidateAccessories'',''multiplebatchdespatch_buttonvalidateaccessories_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','ValidateAccessories','Validate Accessories',p_web.combine(Choose('Validate Accessories' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'button-entryfield'),loc:formname,,,,loc:javascript,loc:disabled,,,,,,,,,,'nt-left')!h
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonValidateAccessories  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonValidateAccessories:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('Hide:ValidateAccessoriesButton') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('MultipleBatchDespatch_' & p_web._nocolon('buttonValidateAccessories') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('Hide:ValidateAccessoriesButton') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::locAccessoryMessage  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locAccessoryMessage = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locAccessoryMessage = p_web.GetValue('Value')
  End
  do ValidateValue::locAccessoryMessage  ! copies value to session value if valid.
  do Comment::locAccessoryMessage ! allows comment style to be updated.

ValidateValue::locAccessoryMessage  Routine
    If not (p_web.GSV('Hide:Accessories') = 1)
      if loc:invalid = '' then p_web.SetSessionValue('locAccessoryMessage',locAccessoryMessage).
    End

Value::locAccessoryMessage  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:Accessories') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('MultipleBatchDespatch_' & p_web._nocolon('locAccessoryMessage') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (p_web.GSV('Hide:Accessories') = 1)
  ! --- DISPLAY --- locAccessoryMessage
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locAccessoryMessage'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locAccessoryMessage  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locAccessoryMessage:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:Accessories') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('MultipleBatchDespatch_' & p_web._nocolon('locAccessoryMessage') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:Accessories') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::locAccessoryErrorMessage  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locAccessoryErrorMessage = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locAccessoryErrorMessage = p_web.GetValue('Value')
  End
  do ValidateValue::locAccessoryErrorMessage  ! copies value to session value if valid.
  do Comment::locAccessoryErrorMessage ! allows comment style to be updated.

ValidateValue::locAccessoryErrorMessage  Routine
    If not (p_web.GSV('Hide:Accessories') = 1)
      if loc:invalid = '' then p_web.SetSessionValue('locAccessoryErrorMessage',locAccessoryErrorMessage).
    End

Value::locAccessoryErrorMessage  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:Accessories') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('MultipleBatchDespatch_' & p_web._nocolon('locAccessoryErrorMessage') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (p_web.GSV('Hide:Accessories') = 1)
  ! --- DISPLAY --- locAccessoryErrorMessage
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('red bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locAccessoryErrorMessage'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locAccessoryErrorMessage  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locAccessoryErrorMessage:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:Accessories') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('MultipleBatchDespatch_' & p_web._nocolon('locAccessoryErrorMessage') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:Accessories') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonConfirmMismatch  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonConfirmMismatch  ! copies value to session value if valid.
  ! Mismatch Confirmation
  if (p_web.GSV('AccessoryPasswordRequired') = 1)
      if (p_web.GSV('locAccessoryPassword') = '')
          p_web.SSV('locAccessoryErrorMessage','Password Required')
      else
          If (SecurityCheckFailed(p_web.GSV('locAccessoryPassword'),'ACCESSORY MISMATCH - ACCEPT'))
              p_web.SSV('locAccessoryErrorMessage','The selected password does not have access to this option')
          else
              p_web.SSV('locAccessoryMessage','Accessory Validated')
              p_web.SSV('locAccessoryErrorMessage','')
              p_web.SSV('Hide:ValidateAccessoriesButton',1)
              p_web.SSV('AccessoryConfirmationRequired',0)
              p_web.SSV('AccessoriesValidated',1)
              p_web.SSV('AccessoryPasswordRequired',0)
              p_web.SSV('locAccessoryPasswordMessage','')
              p_web.SSV('Hide:AddToBatchButton',0)
          end
      end
  else
      p_web.SSV('locAccessoryMessage','Accessory Validated')
      p_web.SSV('locAccessoryErrorMessage','')
      p_web.SSV('Hide:ValidateAccessoriesButton',1)
      p_web.SSV('AccessoryConfirmationRequired',0)
      p_web.SSV('AccessoriesValidated',1)
      p_web.SSV('AccessoryPasswordRequired',0)
      p_web.SSV('locAccessoryPasswordMessage','')
      p_web.SSV('Hide:AddToBatchButton',0)
      !DO ShowConsignmentNumber
  end
  do Value::buttonConfirmMismatch
  do Comment::buttonConfirmMismatch ! allows comment style to be updated.
  do Value::TagValidateLoanAccessories  !1
  do Value::buttonAddToBatch  !1
  do Value::buttonFailAccessory  !1
  do Value::buttonProcessJob  !1
  do Value::buttonValidateAccessories  !1
  do Value::locAccessoryErrorMessage  !1
  do Value::locAccessoryMessage  !1
  do Value::locAccessoryPassword  !1

ValidateValue::buttonConfirmMismatch  Routine
    If not (p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('AccessoryConfirmationRequired') <> 1)
    End

Value::buttonConfirmMismatch  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('AccessoryConfirmationRequired') <> 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('MultipleBatchDespatch_' & p_web._nocolon('buttonConfirmMismatch') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('AccessoryConfirmationRequired') <> 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonConfirmMismatch'',''multiplebatchdespatch_buttonconfirmmismatch_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','ConfirmAccessoryValidation','Confirm Access. Validation',p_web.combine(Choose('Confirm Access. Validation' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'button-entryfield'),loc:formname,,,,loc:javascript,loc:disabled,'images/tick.png',,,,,,,,,'nt-left')!h
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonConfirmMismatch  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonConfirmMismatch:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('AccessoryConfirmationRequired') <> 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('MultipleBatchDespatch_' & p_web._nocolon('buttonConfirmMismatch') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('AccessoryConfirmationRequired') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonFailAccessory  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonFailAccessory  ! copies value to session value if valid.
  ! Fail Accessory Check
  
  GetStatus(810,0,p_web.GSV('DespatchType'),p_web)
  
  Access:JOBS.ClearKey(job:Ref_Number_Key)
  job:Ref_Number = p_web.GSV('job:Ref_Number')
  IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
      p_web.SessionQueueToFile(JOBS)
      Access:JOBS.TryUpdate()
  END
  
  Access:WEBJOB.ClearKey(wob:RefNumberKey)
  wob:RefNumber = job:Ref_Number
  IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
      p_web.SessionQueueToFile(WEBJOB)
      Access:WEBJOB.TryUpdate()
  END
  
  locAuditTrail = 'ACCESSORY MISMATCH<13,10,13,10>ACCESSORIES BOOKED IN:'
  IF (p_web.GSV('DespatchType') <> 'Loan')
      Access:JOBACC.ClearKey(jac:Ref_Number_Key)
      jac:Ref_Number = job:Ref_Number
      Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
      Loop
          If Access:JOBACC.NEXT()
              Break
          End !If
          If jac:Ref_Number <> job:Ref_Number      |
              Then Break.  ! End If
          locAuditTrail = CLIP(locAuditTrail) & '<13,10>' & Clip(jac:Accessory)
      End !Loop
  ELSE
      Access:LOANACC.ClearKey(lac:Ref_Number_Key)
      lac:Ref_Number = job:Ref_Number
      Set(lac:Ref_Number_Key,lac:Ref_Number_Key)
      Loop
          If Access:LOANACC.NEXT()
              Break
          End !If
          If lac:Ref_Number <> job:Ref_Number      |
              Then Break.  ! End If
          locAuditTrail = CLIP(locAuditTrail) & '<13,10>' & Clip(lac:Accessory)
      End !Loop
  END
  locAuditTrail = CLIP(locAuditTrail) & '<13,10,13,10>ACCESSORIES BOOKED OUT: '
  
  Access:TagFile.ClearKey(tag:keyTagged)
  tag:sessionID = p_web.SessionID
  SET(tag:keyTagged,tag:keyTagged)
  LOOP UNTIL Access:TagFile.Next()
      IF (tag:sessionID <> p_web.SessionID)
          BREAK
      END
      IF (tag:tagged = 1)
          locAuditTrail = CLIP(locAuditTrail) & '<13,10>' & Clip(tag:TaggedValue)
      END
  END
  
  p_web.SSV('AddToAudit:Type',p_web.GSV('DespatchType'))
  p_web.SSV('AddToAudit:Action','FAILED DESPATCH VALIDATION')
  p_web.SSV('AddToAudit:Notes',CLIP(locAuditTrail))
  AddToAudit(p_web)
  
  
  DO ValidateUnitDetails
  
  do Value::buttonFailAccessory
  do Comment::buttonFailAccessory ! allows comment style to be updated.
  do Value::TagValidateLoanAccessories  !1
  do Value::buttonAddToBatch  !1
  do Value::buttonConfirmMismatch  !1
  do Value::buttonProcessJob  !1
  do Value::buttonValidateAccessories  !1
  do Value::locAccessoryErrorMessage  !1
  do Value::locAccessoryMessage  !1
  do Value::locAccessoryPassword  !1
  do Value::locErrorMessage  !1
  do Value::locJobNumber  !1
  do Value::locIMEINumber  !1
  do Value::locSecurityPackNumber  !1

ValidateValue::buttonFailAccessory  Routine
    If not (p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('AccessoryConfirmationRequired') <> 1)
    End

Value::buttonFailAccessory  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('AccessoryConfirmationRequired') <> 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('MultipleBatchDespatch_' & p_web._nocolon('buttonFailAccessory') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('AccessoryConfirmationRequired') <> 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonFailAccessory'',''multiplebatchdespatch_buttonfailaccessory_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','FailAccessory','Fail Accessory Validation',p_web.combine(Choose('Fail Accessory Validation' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'button-entryfield'),loc:formname,,,,loc:javascript,loc:disabled,'images/cross.png',,,,,,,,,'nt-left')!h
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonFailAccessory  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonFailAccessory:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('AccessoryConfirmationRequired') <> 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('MultipleBatchDespatch_' & p_web._nocolon('buttonFailAccessory') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('AccessoryConfirmationRequired') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locAccessoryPassword  Routine
  packet = clip(packet) & p_web.DivHeader('MultipleBatchDespatch_' & p_web._nocolon('locAccessoryPassword') & '_prompt',Choose(p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('AccesssoryPasswordRequired') <> 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('AccesssoryPasswordRequired') <> 1,'',p_web.Translate('Enter Password'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locAccessoryPassword  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locAccessoryPassword = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locAccessoryPassword = p_web.GetValue('Value')
  End
  do ValidateValue::locAccessoryPassword  ! copies value to session value if valid.
  do Value::locAccessoryPassword
  do SendAlert
  do Comment::locAccessoryPassword ! allows comment style to be updated.

ValidateValue::locAccessoryPassword  Routine
    If not (p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('AccesssoryPasswordRequired') <> 1)
    locAccessoryPassword = Upper(locAccessoryPassword)
      if loc:invalid = '' then p_web.SetSessionValue('locAccessoryPassword',locAccessoryPassword).
    End

Value::locAccessoryPassword  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('AccesssoryPasswordRequired') <> 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('MultipleBatchDespatch_' & p_web._nocolon('locAccessoryPassword') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    locAccessoryPassword = p_web.RestoreValue('locAccessoryPassword')
    do ValidateValue::locAccessoryPassword
    If locAccessoryPassword:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('AccesssoryPasswordRequired') <> 1)
  ! --- STRING --- locAccessoryPassword
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locAccessoryPassword'',''multiplebatchdespatch_locaccessorypassword_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('password','locAccessoryPassword',p_web.GetSessionValueFormat('locAccessoryPassword'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locAccessoryPassword  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locAccessoryPassword:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('AccesssoryPasswordRequired') <> 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('MultipleBatchDespatch_' & p_web._nocolon('locAccessoryPassword') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('AccesssoryPasswordRequired') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonAddToBatch  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonAddToBatch  ! copies value to session value if valid.
  do Comment::buttonAddToBatch ! allows comment style to be updated.

ValidateValue::buttonAddToBatch  Routine
    If not (p_web.GSV('Hide:AddToBatchButton') = 1)
    End

Value::buttonAddToBatch  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:AddToBatchButton') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('MultipleBatchDespatch_' & p_web._nocolon('buttonAddToBatch') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (p_web.GSV('Hide:AddToBatchButton') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','AddToBatch','Add Job To Batch',p_web.combine(Choose('Add Job To Batch' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'button-entryfield'),loc:formname,,,p_web.WindowOpen(clip('FormAddToBatch')&''&'','_self'),,loc:disabled,,,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonAddToBatch  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonAddToBatch:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:AddToBatchButton') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('MultipleBatchDespatch_' & p_web._nocolon('buttonAddToBatch') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:AddToBatchButton') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('MultipleBatchDespatch_nexttab_' & 0)
    If loc:Invalid then exit.
  of lower('MultipleBatchDespatch_nexttab_' & 1)
    locJobNumber = p_web.GSV('locJobNumber')
    do ValidateValue::locJobNumber
    If loc:Invalid
      loc:retrying = 1
      do Value::locJobNumber
      !do SendAlert
      do Comment::locJobNumber ! allows comment style to be updated.
      !exit
    End
    locIMEINumber = p_web.GSV('locIMEINumber')
    do ValidateValue::locIMEINumber
    If loc:Invalid
      loc:retrying = 1
      do Value::locIMEINumber
      !do SendAlert
      do Comment::locIMEINumber ! allows comment style to be updated.
      !exit
    End
    locSecurityPackNumber = p_web.GSV('locSecurityPackNumber')
    do ValidateValue::locSecurityPackNumber
    If loc:Invalid
      loc:retrying = 1
      do Value::locSecurityPackNumber
      !do SendAlert
      do Comment::locSecurityPackNumber ! allows comment style to be updated.
      !exit
    End
    locErrorMessage = p_web.GSV('locErrorMessage')
    do ValidateValue::locErrorMessage
    If loc:Invalid
      loc:retrying = 1
      do Value::locErrorMessage
      !do SendAlert
      do Comment::locErrorMessage ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('MultipleBatchDespatch_nexttab_' & 2)
    locAccessoryMessage = p_web.GSV('locAccessoryMessage')
    do ValidateValue::locAccessoryMessage
    If loc:Invalid
      loc:retrying = 1
      do Value::locAccessoryMessage
      !do SendAlert
      do Comment::locAccessoryMessage ! allows comment style to be updated.
      !exit
    End
    locAccessoryErrorMessage = p_web.GSV('locAccessoryErrorMessage')
    do ValidateValue::locAccessoryErrorMessage
    If loc:Invalid
      loc:retrying = 1
      do Value::locAccessoryErrorMessage
      !do SendAlert
      do Comment::locAccessoryErrorMessage ! allows comment style to be updated.
      !exit
    End
    locAccessoryPassword = p_web.GSV('locAccessoryPassword')
    do ValidateValue::locAccessoryPassword
    If loc:Invalid
      loc:retrying = 1
      do Value::locAccessoryPassword
      !do SendAlert
      do Comment::locAccessoryPassword ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('MultipleBatchDespatch_nexttab_' & 3)
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_MultipleBatchDespatch_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('MultipleBatchDespatch_tab_' & 0)
    do GenerateTab0
  of lower('MultipleBatchDespatch_BrowseBatchesInProgress_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::BrowseBatchesInProgress
      of event:timer
        do Value::BrowseBatchesInProgress
        do Comment::BrowseBatchesInProgress
      else
        do Value::BrowseBatchesInProgress
      end
  of lower('MultipleBatchDespatch_tab_' & 1)
    do GenerateTab1
  of lower('MultipleBatchDespatch_locJobNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locJobNumber
      of event:timer
        do Value::locJobNumber
        do Comment::locJobNumber
      else
        do Value::locJobNumber
      end
  of lower('MultipleBatchDespatch_locIMEINumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locIMEINumber
      of event:timer
        do Value::locIMEINumber
        do Comment::locIMEINumber
      else
        do Value::locIMEINumber
      end
  of lower('MultipleBatchDespatch_locSecurityPackNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locSecurityPackNumber
      of event:timer
        do Value::locSecurityPackNumber
        do Comment::locSecurityPackNumber
      else
        do Value::locSecurityPackNumber
      end
  of lower('MultipleBatchDespatch_buttonProcessJob_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonProcessJob
      of event:timer
        do Value::buttonProcessJob
        do Comment::buttonProcessJob
      else
        do Value::buttonProcessJob
      end
  of lower('MultipleBatchDespatch_tab_' & 2)
    do GenerateTab2
  of lower('MultipleBatchDespatch_buttonValidateAccessories_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonValidateAccessories
      of event:timer
        do Value::buttonValidateAccessories
        do Comment::buttonValidateAccessories
      else
        do Value::buttonValidateAccessories
      end
  of lower('MultipleBatchDespatch_buttonConfirmMismatch_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonConfirmMismatch
      of event:timer
        do Value::buttonConfirmMismatch
        do Comment::buttonConfirmMismatch
      else
        do Value::buttonConfirmMismatch
      end
  of lower('MultipleBatchDespatch_buttonFailAccessory_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonFailAccessory
      of event:timer
        do Value::buttonFailAccessory
        do Comment::buttonFailAccessory
      else
        do Value::buttonFailAccessory
      end
  of lower('MultipleBatchDespatch_locAccessoryPassword_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locAccessoryPassword
      of event:timer
        do Value::locAccessoryPassword
        do Comment::locAccessoryPassword
      else
        do Value::locAccessoryPassword
      end
  of lower('MultipleBatchDespatch_tab_' & 3)
    do GenerateTab3
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('MultipleBatchDespatch_form:ready_',1)

  p_web.SetSessionValue('MultipleBatchDespatch_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_MultipleBatchDespatch',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('MultipleBatchDespatch_form:ready_',1)
  p_web.SetSessionValue('MultipleBatchDespatch_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_MultipleBatchDespatch',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('MultipleBatchDespatch_form:ready_',1)
  p_web.SetSessionValue('MultipleBatchDespatch_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('MultipleBatchDespatch:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('MultipleBatchDespatch_form:ready_',1)
  p_web.SetSessionValue('MultipleBatchDespatch_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('MultipleBatchDespatch:Primed',0)
  p_web.setsessionvalue('showtab_MultipleBatchDespatch',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
      If not (1=0)
          If p_web.IfExistsValue('locJobNumber')
            locJobNumber = p_web.GetValue('locJobNumber')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('locIMEINumber')
            locIMEINumber = p_web.GetValue('locIMEINumber')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('locSecurityPackNumber')
            locSecurityPackNumber = p_web.GetValue('locSecurityPackNumber')
          End
      End
      If not (p_web.GSV('Hide:Accessories') = 1 OR p_web.GSV('AccessoriesValidated') = 1 OR p_web.GSV('AccesssoryPasswordRequired') <> 1)
          If p_web.IfExistsValue('locAccessoryPassword')
            locAccessoryPassword = p_web.GetValue('locAccessoryPassword')
          End
      End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('MultipleBatchDespatch_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('MultipleBatchDespatch_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
    do ValidateValue::BrowseBatchesInProgress
    If loc:Invalid then exit.
  ! tab = 2
    loc:InvalidTab += 1
    do ValidateValue::locJobNumber
    If loc:Invalid then exit.
    do ValidateValue::locIMEINumber
    If loc:Invalid then exit.
    do ValidateValue::locSecurityPackNumber
    If loc:Invalid then exit.
    do ValidateValue::locErrorMessage
    If loc:Invalid then exit.
    do ValidateValue::buttonProcessJob
    If loc:Invalid then exit.
  ! tab = 3
    loc:InvalidTab += 1
    do ValidateValue::TagValidateLoanAccessories
    If loc:Invalid then exit.
    do ValidateValue::buttonValidateAccessories
    If loc:Invalid then exit.
    do ValidateValue::locAccessoryMessage
    If loc:Invalid then exit.
    do ValidateValue::locAccessoryErrorMessage
    If loc:Invalid then exit.
    do ValidateValue::buttonConfirmMismatch
    If loc:Invalid then exit.
    do ValidateValue::buttonFailAccessory
    If loc:Invalid then exit.
    do ValidateValue::locAccessoryPassword
    If loc:Invalid then exit.
  ! tab = 4
    loc:InvalidTab += 1
    do ValidateValue::buttonAddToBatch
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('MultipleBatchDespatch:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('locJobNumber')
  p_web.StoreValue('locIMEINumber')
  p_web.StoreValue('locSecurityPackNumber')
  p_web.StoreValue('locErrorMessage')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locAccessoryMessage')
  p_web.StoreValue('locAccessoryErrorMessage')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locAccessoryPassword')
  p_web.StoreValue('')

CreateNewBatch       PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locExistingBatchNumber LONG                                !
locWaybillNumber     STRING(30)                            !
FilesOpened     Long
MULDESP_ALIAS::State  USHORT
JOBSE::State  USHORT
WAYBILLS::State  USHORT
COURIER::State  USHORT
WEBJOB::State  USHORT
JOBS::State  USHORT
SUBTRACC::State  USHORT
TRADEACC::State  USHORT
MULDESPJ::State  USHORT
WAYBILLJ::State  USHORT
MULDESP::State  USHORT
textMessage:IsInvalid  Long
buttonDespatchNote:IsInvalid  Long
buttonCreateInvoice:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('CreateNewBatch')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'CreateNewBatch_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('CreateNewBatch','')
    p_web.DivHeader('CreateNewBatch',p_web.combine(p_web.site.style.formdiv,))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('CreateNewBatch') = 0
        p_web.AddPreCall('CreateNewBatch')
        p_web.DivHeader('popup_CreateNewBatch','nt-hidden')
        p_web.DivHeader('CreateNewBatch',p_web.combine(p_web.site.style.formdiv,))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_CreateNewBatch_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_CreateNewBatch_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferCreateNewBatch',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_CreateNewBatch',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferCreateNewBatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_CreateNewBatch',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferCreateNewBatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_CreateNewBatch',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_CreateNewBatch',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferCreateNewBatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_CreateNewBatch',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferCreateNewBatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_CreateNewBatch',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_CreateNewBatch',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('CreateNewBatch')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(MULDESP_ALIAS)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(WAYBILLS)
  p_web._OpenFile(COURIER)
  p_web._OpenFile(WEBJOB)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(SUBTRACC)
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(MULDESPJ)
  p_web._OpenFile(WAYBILLJ)
  p_web._OpenFile(MULDESP)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(MULDESP_ALIAS)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(WAYBILLS)
  p_Web._CloseFile(COURIER)
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(MULDESPJ)
  p_Web._CloseFile(WAYBILLJ)
  p_Web._CloseFile(MULDESP)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('CreateNewBatch_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'CreateNewBatch'
    end
    p_web.formsettings.proc = 'CreateNewBatch'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('CreateNewBatch_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'MultipleBatchDespatch'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('CreateNewBatch_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('CreateNewBatch_ChainTo')
    loc:formaction = p_web.GetSessionValue('CreateNewBatch_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction

GenerateForm   Routine
  do LoadRelatedRecords
  ! Start
  p_web.SSV('Hide:DespatchNoteButton',1)
  p_web.SSV('Hide:CreateInvoiceButton',1)
  
  Access:JOBS.ClearKey(job:Ref_Number_Key)
  job:Ref_Number = p_web.GSV('job:Ref_Number')
  IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
      Access:MULDESPJ.ClearKey(mulj:JobNumberOnlyKey)
      mulj:JobNumber = job:Ref_Number
      IF (Access:MULDESPJ.TryFetch(mulj:JobNumberOnlyKey))
          ! only carry on if job is not already on batch
          ! this should cope if the user presses refresh on the window
  
          Access:JOBSE.ClearKey(jobe:RefNumberKey)
          jobe:RefNumber = job:Ref_Number
          IF (Access:JOBSE.TryFetch(jobe:RefNumberKey))
          END
  
          Access:WEBJOB.ClearKey(wob:RefNumberKey)
          wob:RefNumber = job:Ref_Number
          IF (Access:WEBJOB.TryFetch(wob:RefNumberKey))
          END
  
  
          Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
          sub:Account_Number = job:Account_Number
          IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
              Access:TRADEACC.ClearKey(tra:Account_Number_Key)
              tra:Account_Number = sub:Main_Account_Number
              IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
                  ! Invoice At Despatch Bit??
                  IF (tra:Invoice_Sub_Accounts = 'YES' AND tra:Use_Sub_Accounts = 'YES')
                  ELSE
                  END
  
              END
  
          END
  
          IF (job:Warranty_Job = 'YES')
              IF (tra:Use_Sub_Accounts = 'YES')
                  IF (sub:Print_Despatch_Despatch = 'YES')
                      IF (sub:Despatch_Note_Per_Item = 'YES')
                          p_web.SSV('Hide:DespatchNoteButton',0)
                      END
                  END
  
              ELSE
                  IF (tra:Print_Despatch_Despatch = 'YES')
                      IF (tra:Despatch_Note_Per_Item = 'YES')
                          p_web.SSV('Hide:DespathNoteButton',0)
                      END
                  END
  
              END
          END
  
          ! Add To Batch
          IF (Access:MULDESP.PrimeRecord() = Level:Benign)
              muld:BatchType = 'SUB'
              IF (jobe:Sub_Sub_Account = '')
                  muld:AccountNumber = job:Account_Number
              ELSE
                  muld:AccountNumber = jobe:Sub_Sub_Account
              END
              IF (p_web.GSV('BookingSite') <> 'RRC' AND jobe:WebJob)
                  IF (sub:Generic_Account AND p_web.GSV('BookingSite') <> 'RRC')
                      muld:AccountNumber = wob:HeadAccountNumber
                      muld:BatchType = 'TRA'
                  END
  
              ELSE
              END
  
              CASE p_web.GSV('DespatchType')
              OF 'JOB'
                  muld:Courier = job:Courier
              OF 'EXC'
                  muld:Courier = job:Exchange_Courier
              OF 'LOA'
                  muld:Courier = job:Loan_Courier
              END
  
              If GETINI('DESPATCH','GroupVirtualBatches',,CLIP(PATH())&'\SB2KDEF.INI') = 1 and p_web.GSV('BookingSite') <> 'RRC'
                  !Is this for a Virtual Site?
                  If ~sub:Generic_Account
                      If (vod.RemoteAccount(job:Account_Number))
                          muld:BatchType = 'TRA'
                          muld:AccountNumber = tra:Account_Number
                          muld:Courier        = tra:Courier_Outgoing
                      End !If VirualAccount(job:Account_Number)
                  End !If ~sub:Generic_Account
              END
              muld:BatchTotal = 1
              !Allocate a Batch Number
              !Count between 1 to 1000, (that should be enough)
              !and if I can't find a Batch with that batch number, then
              !assign that batch number to this batch.
              BatchNumber# = 0
              Loop BatchNumber# = 1 To 1000
                  Access:MULDESP_ALIAS.ClearKey(muld_ali:BatchNumberKey)
                  muld_ali:BatchNumber = BatchNumber#
                  If Access:MULDESP_ALIAS.TryFetch(muld_ali:BatchNumberKey) = Level:Benign
                      !Found
                  Else!If Access:MULDESP_ALIAS.TryFetch(muld_ali:BatchNumberKey) = Level:Benign
                      !Error
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                      muld:BatchNumber    = BatchNumber#
                      !tmp:ReturnedBatchNumber = muld:BatchNumber
                      Break
  
                  End!If Access:MULDESP_ALIAS.TryFetch(muld_ali:BatchNumberKey) = Level:Benign
              End !BatchNumber# = 1 To 1000
  
              muld:HeadAccountNumber = p_web.GSV('BookingAccount')
  
              IF (Access:MULDESP.TryInsert() = Level:Benign)
                  IF (Access:MULDESPJ.PrimeRecord() = Level:Benign)
                      mulj:RefNumber = muld:RecordNumber
                      mulj:JobNumber = job:Ref_Number
                      mulj:IMEINumber = job:ESN
                      mulj:MSN = job:MSN
                      mulj:AccountNumber = muld:AccountNumber
                      mulj:Courier = muld:Courier
                      mulj:Current = 1
                      mulj:SecurityPackNumber = p_web.GSV('locSecurityPackNumber')
                      IF (Access:MULDESPJ.TryInsert() = Level:Benign)
                      END
                  END
              END
              p_web.SSV('job:Ref_Number','') ! Clear to ensure code isn't called again
  
          END
  
      END
  
  END
  
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Multiple Despatch') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Multiple Despatch',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_CreateNewBatch',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_CreateNewBatch0_div')&'">'&p_web.Translate('Job Added To Batch')&'</a></li>'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_CreateNewBatch1_div')&'">'&p_web.Translate('Paperwork')&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  do GenerateTab1
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="CreateNewBatch_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="CreateNewBatch_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'CreateNewBatch_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="CreateNewBatch_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'CreateNewBatch_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_CreateNewBatch')>0,p_web.GSV('showtab_CreateNewBatch'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_CreateNewBatch'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_CreateNewBatch') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_CreateNewBatch'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_CreateNewBatch')>0,p_web.GSV('showtab_CreateNewBatch'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_CreateNewBatch') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Job Added To Batch') & ''''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Paperwork') & ''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_CreateNewBatch_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_CreateNewBatch')>0,p_web.GSV('showtab_CreateNewBatch'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"CreateNewBatch",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_CreateNewBatch')>0,p_web.GSV('showtab_CreateNewBatch'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_CreateNewBatch_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('CreateNewBatch') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('CreateNewBatch')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Job Added To Batch')&'</a></h3>' & CRLF & p_web.DivHeader('tab_CreateNewBatch0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_CreateNewBatch0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_CreateNewBatch0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_CreateNewBatch0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Job Added To Batch')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_CreateNewBatch0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Job Added To Batch')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_CreateNewBatch0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Job Added To Batch')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_CreateNewBatch0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::textMessage
        do Comment::textMessage
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab1  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Paperwork')&'</a></h3>' & CRLF & p_web.DivHeader('tab_CreateNewBatch1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_CreateNewBatch1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_CreateNewBatch1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_CreateNewBatch1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Paperwork')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_CreateNewBatch1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Paperwork')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_CreateNewBatch1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Paperwork')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_CreateNewBatch1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::buttonDespatchNote
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::buttonDespatchNote
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::buttonCreateInvoice
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::buttonCreateInvoice
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Validate::textMessage  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::textMessage  ! copies value to session value if valid.
  do Comment::textMessage ! allows comment style to be updated.

ValidateValue::textMessage  Routine
    If not (1=0)
    End

Value::textMessage  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('CreateNewBatch_' & p_web._nocolon('textMessage') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="textMessage" class="'&clip('green bold')&'"'&clip(loc:extra)&'>' & p_web.Translate('Job Number ' & p_web.GSV('locJobNumber') & ' Added To Batch',) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::textMessage  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if textMessage:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('CreateNewBatch_' & p_web._nocolon('textMessage') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonDespatchNote  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonDespatchNote  ! copies value to session value if valid.
  do Comment::buttonDespatchNote ! allows comment style to be updated.

ValidateValue::buttonDespatchNote  Routine
    If not (p_web.GSV('Hide:DespatchNoteButton') = 1)
    End

Value::buttonDespatchNote  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:DespatchNoteButton') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('CreateNewBatch_' & p_web._nocolon('buttonDespatchNote') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('Hide:DespatchNoteButton') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','DespatchNote','Despatch Note',p_web.combine(Choose('Despatch Note' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.WindowOpen(clip('DespatchNote')&''&'','_blank'),,loc:disabled,'images/printer.png',,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonDespatchNote  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonDespatchNote:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:DespatchNoteButton') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('CreateNewBatch_' & p_web._nocolon('buttonDespatchNote') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:DespatchNoteButton') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonCreateInvoice  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonCreateInvoice  ! copies value to session value if valid.
  do Comment::buttonCreateInvoice ! allows comment style to be updated.

ValidateValue::buttonCreateInvoice  Routine
    If not (p_web.GSV('Hide:CreateInvoiceButton') = 1)
    End

Value::buttonCreateInvoice  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:CreateInvoiceButton') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('CreateNewBatch_' & p_web._nocolon('buttonCreateInvoice') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('Hide:CreateInvoiceButton') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','CreateInvoice','Create Invoice',p_web.combine(Choose('Create Invoice' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.WindowOpen(clip('CreateInvoice')&''&'','_self'),,loc:disabled,'images/moneys.png',,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonCreateInvoice  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonCreateInvoice:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:CreateInvoiceButton') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('CreateNewBatch_' & p_web._nocolon('buttonCreateInvoice') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:CreateInvoiceButton') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('CreateNewBatch_nexttab_' & 0)
    If loc:Invalid then exit.
  of lower('CreateNewBatch_nexttab_' & 1)
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_CreateNewBatch_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('CreateNewBatch_tab_' & 0)
    do GenerateTab0
  of lower('CreateNewBatch_tab_' & 1)
    do GenerateTab1
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('CreateNewBatch_form:ready_',1)

  p_web.SetSessionValue('CreateNewBatch_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_CreateNewBatch',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('CreateNewBatch_form:ready_',1)
  p_web.SetSessionValue('CreateNewBatch_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_CreateNewBatch',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('CreateNewBatch_form:ready_',1)
  p_web.SetSessionValue('CreateNewBatch_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('CreateNewBatch:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('CreateNewBatch_form:ready_',1)
  p_web.SetSessionValue('CreateNewBatch_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('CreateNewBatch:Primed',0)
  p_web.setsessionvalue('showtab_CreateNewBatch',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('CreateNewBatch_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('CreateNewBatch_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
    do ValidateValue::textMessage
    If loc:Invalid then exit.
  ! tab = 2
    loc:InvalidTab += 1
    do ValidateValue::buttonDespatchNote
    If loc:Invalid then exit.
    do ValidateValue::buttonCreateInvoice
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('CreateNewBatch:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')

BannerEngineeringDetails PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
FilesOpened     Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  GlobalErrors.SetProcedureName('BannerEngineeringDetails')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'BannerEngineeringDetails_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('BannerEngineeringDetails','')
    p_web.DivHeader('BannerEngineeringDetails',p_web.combine(p_web.site.style.formdiv,'fdiv'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('BannerEngineeringDetails') = 0
        p_web.AddPreCall('BannerEngineeringDetails')
        p_web.DivHeader('popup_BannerEngineeringDetails','nt-hidden')
        p_web.DivHeader('BannerEngineeringDetails',p_web.combine(p_web.site.style.formdiv,'fdiv'))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_BannerEngineeringDetails_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_BannerEngineeringDetails_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferBannerEngineeringDetails',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_BannerEngineeringDetails',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBannerEngineeringDetails',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_BannerEngineeringDetails',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBannerEngineeringDetails',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_BannerEngineeringDetails',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_BannerEngineeringDetails',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBannerEngineeringDetails',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_BannerEngineeringDetails',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBannerEngineeringDetails',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_BannerEngineeringDetails',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_BannerEngineeringDetails',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('BannerEngineeringDetails')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('BannerEngineeringDetails_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'BannerEngineeringDetails'
    end
    p_web.formsettings.proc = 'BannerEngineeringDetails'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine

AfterLookup Routine
  loc:TabNumber = -1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('BannerEngineeringDetails_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferBannerEngineeringDetails')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('BannerEngineeringDetails_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('BannerEngineeringDetails_ChainTo')
    loc:formaction = p_web.GetSessionValue('BannerEngineeringDetails_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction

GenerateForm   Routine
  do LoadRelatedRecords
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
    do SendPacket
    Do heading
    do SendPacket
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
    Packet = clip(Packet) & p_web.DivHeader('Tab_BannerEngineeringDetails',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="BannerEngineeringDetails_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      packet = clip(packet) & '</div><13,10>' ! end id="BannerEngineeringDetails_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'BannerEngineeringDetails_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="BannerEngineeringDetails_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'BannerEngineeringDetails_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_BannerEngineeringDetails')>0,p_web.GSV('showtab_BannerEngineeringDetails'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_BannerEngineeringDetails'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_BannerEngineeringDetails') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_BannerEngineeringDetails'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_BannerEngineeringDetails')>0,p_web.GSV('showtab_BannerEngineeringDetails'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_BannerEngineeringDetails') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_BannerEngineeringDetails_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_BannerEngineeringDetails')>0,p_web.GSV('showtab_BannerEngineeringDetails'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"BannerEngineeringDetails",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_BannerEngineeringDetails')>0,p_web.GSV('showtab_BannerEngineeringDetails'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_BannerEngineeringDetails_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('BannerEngineeringDetails') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('BannerEngineeringDetails')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine


NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_BannerEngineeringDetails_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('BannerEngineeringDetails_form:ready_',1)

  p_web.SetSessionValue('BannerEngineeringDetails_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_BannerEngineeringDetails',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('BannerEngineeringDetails_form:ready_',1)
  p_web.SetSessionValue('BannerEngineeringDetails_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_BannerEngineeringDetails',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('BannerEngineeringDetails_form:ready_',1)
  p_web.SetSessionValue('BannerEngineeringDetails_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('BannerEngineeringDetails:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('BannerEngineeringDetails_form:ready_',1)
  p_web.SetSessionValue('BannerEngineeringDetails_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('BannerEngineeringDetails:Primed',0)
  p_web.setsessionvalue('showtab_BannerEngineeringDetails',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('BannerEngineeringDetails_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('BannerEngineeringDetails_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('BannerEngineeringDetails:Primed',0)

heading  Routine
  packet = clip(packet) & p_web.AsciiToUTF(|
    '<<table class="FormCentre"><13,10>'&|
    '    <<tr><13,10>'&|
    '        <<td width="140" aligh="left"><<img src="/images/bannerleft.gif" width="140" heigh="30"/><</td><13,10>'&|
    '        <<td width="670" align="center" class="BannerText">Engineering Details<</td><13,10>'&|
    '        <<td width="140" aligh="right"><<img src="/images/bannerright.gif" width="140" heigh="30"/><</td><13,10>'&|
    '    <</tr><13,10>'&|
    '    <<tr><13,10>'&|
    '        <<td width="140"><</td><13,10>'&|
    '        <<td width="670"><</td><13,10>'&|
    '        <<td width="140" align="right" class="SmallText"><<!-- Net:s:VersionNumber --><</td><13,10>'&|
    '    <</tr><13,10>'&|
    '<</table><13,10>'&|
    '<13,10>'&|
    '',net:OnlyIfUTF)
