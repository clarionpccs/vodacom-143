

   MEMBER('WebServer_Phase4.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSE058.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSE056.INC'),ONCE        !Req'd for module callout resolution
                     END


FormStockModels      PROCEDURE  (NetWebServerWorker p_web)
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256),dim(Net:MaxKeyFields)
idx           String(Net:HashSize)
sub           Long
            End
stm:Model_Number:IsInvalid  Long
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:DefaultSelection    String(Net:HashSize)
loc:ActualSelection     String(Net:HashSize)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
!loc:pagename            String(256)
loc:NavButtonPosition    Long
loc:UpdateButtonPosition Long
loc:SelectionMethod      Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorCase         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
loc:lookupdone          Long
loc:FormPopup           Long
ThisView            View(STOMODEL)
                      Project(stm:RecordNumber)
                      Project(stm:Model_Number)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
Loc:popup             Long
loc:poppedup          Long,static,thread
FilesOpened     Long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  GlobalErrors.SetProcedureName('FormStockModels')
  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('FormStockModels:NoForm')
      loc:NoForm = p_web.GetValue('FormStockModels:NoForm')
      loc:FormName = p_web.GetValue('FormStockModels:FormName')
    else
      loc:FormName = 'FormStockModels_frm'
    End
    p_web.SSV('FormStockModels:NoForm',loc:NoForm)
    p_web.SSV('FormStockModels:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('FormStockModels:NoForm')
    loc:FormName = p_web.GSV('FormStockModels:FormName')
  end
  case p_web.site.formpopups
  of -1 ; loc:FormPopup = Net:Page
  of 0 ; loc:FormPopup = Net:Page
  of 1 ; loc:FormPopup = Net:Popup
  End
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('FormStockModels') & '_' & lower(loc:parent)
  else
    loc:divname = lower('FormStockModels')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if 1 = 2
  elsif p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  elsif p_web.IfExistsValue('_EIPRow_')
    do CallRow
  elsif p_web.GetValue('_CallPopups') > 0
    do CallPopups
  elsif p_web.IfExistsValue('_GetSecwinSettings')
      p_web.SetValue('Secwin_' & 'FormStockModels' & 'AccessGroupsArray','1') 
      p_web.SetValue('Secwin_AccessWindowName','FormStockModels')
      
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallPopups  Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
loc:CallPopups   Long
loc:name         String(255)
  code
  if loc:poppedup = 0
    loc:CallPopups = p_web.GetValue('_CallPopups')
    if loc:CallPopups = 1 !and p_web.GetPreCall('FormStockModels') = 0! for = 1 include popup divs and scripts for this browse
      !p_web.AddPreCall('FormStockModels')
      p_web.DivHeader('popup_FormStockModels','nt-hidden')
      p_web.DivHeader('FormStockModels',p_web.combine(p_web.site.style.browsediv,'adiv'))
      p_web.DivFooter()
      p_web.DivFooter()
      loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(400)&', modal: true, position: [''center'',15]'
      packet =  p_web.jQuery('#' & lower('popup_FormStockModels_div'),'dialog',loc:options) & |
      p_web.jQuery('#' & lower('popup_FormStockModels_div'),'removeClass','''nt-hidden''')
    End
    ! for > 0 add in form, EIP-lookups and browse object
    do Popups
    loc:popup = 1
    do ClosingScripts
    do SendPacket
    loc:poppedup = 1
  end

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web.DivHeader('FormStockModels',p_web.combine(p_web.site.style.browsediv,'adiv'))
  if loc:ParentSilent = 0
    do GenerateBrowse
    if p_web.RequestAjax = 1
      p_web.Script('BrowseProducts.pcv();')
    end
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web.DivFooter()
  do Children
  do Popups
  if loc:poppedup = 0
    do ClosingScripts
  end
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(STOMODEL,stm:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'STM:MODEL_NUMBER') then p_web.SetValue('FormStockModels_sort','1')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('FormStockModels:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('FormStockModels:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('FormStockModels:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('FormStockModels:LookupField')
    loc:selecting = 0
  End

Popups Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  If loc:popup = 0
  End
SetFormAction  Routine
  loc:formaction = 'FormStockModels'

GotFocusBack   Routine

GenerateBrowse Routine
  data
loc:viewoptions  Long
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  ! Set general Browse options
  loc:NavButtonPosition   = Net:Below
  loc:UpdateButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:PageLoad
  loc:Sorting          = Net:ServerSort
  if p_web.GetValue('_popup_') = 1
    loc:popup = 1
  end
  ! Set Locator Options
  loc:LocatorPosition  = Net:Below
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  p_web.ClearBrowse('FormStockModels')
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 5
  End
  loc:ActualSelection = ''
  !loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('FormStockModels_sort',net:DontEvaluate)
  p_web.SetSessionValue('FormStockModels_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(stm:Model_Number)','-UPPER(stm:Model_Number)')
    Loc:LocateField = 'stm:Model_Number'
    Loc:LocatorCase = 0
  end
  if loc:vorder = ''
    loc:vorder = '+stm:Ref_Number,+UPPER(stm:Manufacturer),+UPPER(stm:Model_Number)'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('stm:Model_Number')
    loc:SortHeader = p_web.Translate('Model Number')
    p_web.SetSessionValue('FormStockModels_LocatorPic','@s30')
  End
  If loc:selecting = 1
    p_web.GetSettings(p_web.GetSessionValue('Push1'))
    loc:selectaction = p_web.FormSettings.ParentPage !p_web.GetSessionValue('FormStockModels:LookupFrom')
  End!Else
  do SendPacket
  do SetFormAction
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="FormStockModels:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="FormStockModels:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('FormStockModels:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="STOMODEL"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="stm:RecordNumberKey"></input><13,10>'
  end
  case p_web.GetValue('refresh') ; of 'next' orof 'previous' orof 'first' orof 'last'
    p_web.Script('try{{FormStockModels.restoreFocus();} catch(err){{};')
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'FormStockModels',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      case loc:locatortype
      of Net:Position
      orof Net:Date
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
      of Net:Contains
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
      of Net:Begins
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
      End
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,'Locator')&'"><tr>' &|
      Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
      Case Loc:LocatorType
      of Net:Date
        packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator2FormStockModels','',p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,, p_web.GSV('FormStockModels_LocatorPic'),,,'onchange="FormStockModels.locate(''Locator2FormStockModels'',this.value);"',,0,,,) & '</div></td>'
      Else
        packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator2FormStockModels',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,'size="30" onchange="FormStockModels.locate(''Locator2FormStockModels'',this.value);" ',,,,,,) & '</td>'
      End
      If loc:LocatorSearchButton or loc:LocatorClearButton
        packet = clip(packet) & '<td><div id="FormStockModels_locate_a" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
        If loc:LocatorSearchButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
        End
        If loc:LocatorClearButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'FormStockModels.cl(''FormStockModels'');')
        End
        packet = clip(packet) & '</div><13,10></td><13,10>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
      If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'FormStockModels_locate_a','buttonset',loc:options)
      End
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & p_web.DivHeader('FormStockModels_table',p_web.Combine(p_web.site.style.BrowseTableDiv,''),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,,'sortable')&'" id="FormStockModels_tbl">'
  Else
    packet = clip(packet) & p_web.DivHeader('FormStockModels_table',p_web.Combine(p_web.site.style.BrowseTableDiv,''),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,)&'" id="FormStockModels_tbl">'
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'1','FormStockModels',p_web.Translate('Model Number'),'Click here to sort by Model Number',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,5,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  If Instring('stm:recordnumber',lower(loc:vorder),1,1) = 0 !and STOMODEL{prop:SQLDriver} = 1
    loc:vorder = Choose(loc:vorder='','stm:RecordNumber',clip(loc:vorder) & ',' & 'stm:RecordNumber')
  End
  Loc:Selected = Choose(p_web.IfExistsValue('stm:RecordNumber'),p_web.GetValue('stm:RecordNumber'),p_web.GetSessionValue('stm:RecordNumber'))
  ThisView{prop:order} = p_web.CleanFilter(ThisView,clip(loc:vorder))
      loc:FilterWas = 'stm:Ref_Number = ' & p_web.GSV('sto:Ref_Number') & ' AND UPPER(stm:Manufacturer) = UPPER(''' & p_web.GSV('sto:Manufacturer') & ''')'
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'FormStockModels',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('FormStockModels_Filter')
    p_web.SetSessionValue('FormStockModels_FirstValue','')
    p_web.SetSessionValue('FormStockModels_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,STOMODEL,stm:RecordNumberKey,loc:PageRows,'FormStockModels',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,loc:ViewOptions,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled,Loc:LocatorCase)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or Loc:LocatorType = Net:Date or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position or loc:LocatorType = Net:Date
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If STOMODEL{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(STOMODEL,loc:firstvalue)
              Reset(ThisView,STOMODEL)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If STOMODEL{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(STOMODEL,loc:lastvalue)
            Reset(ThisView,STOMODEL)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(stm:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td class="'&p_web.combine(p_web.site.style.BrowseEmpty,)&'">'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:NavButtonPosition=Net:Above or (loc:NavButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="FormStockModels_nav_a" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'FormStockModels.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'FormStockModels.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'FormStockModels.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'FormStockModels.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'FormStockModels_nav_a','buttonset',loc:options)
          End
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:UpdateButtonPosition=Net:Above or (loc:UpdateButtonPosition=Net:Both and loc:found > 0))
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'FormStockModels',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('FormStockModels_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('FormStockModels_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          case loc:locatortype
          of Net:Position
          orof Net:Date
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
          of Net:Contains
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
          of Net:Begins
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
          End
          packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,'Locator')&'"><tr>' &|
          Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
          Case Loc:LocatorType
          of Net:Date
            packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator1FormStockModels','',p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,, p_web.GSV('FormStockModels_LocatorPic'),,,'onchange="FormStockModels.locate(''Locator1FormStockModels'',this.value);"',,0,,,) & '</div></td>'
          Else
            packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator1FormStockModels',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,'size="30" onchange="FormStockModels.locate(''Locator1FormStockModels'',this.value);" ',,,,,,) & '</td>'
          End
          If loc:LocatorSearchButton or loc:LocatorClearButton
            packet = clip(packet) & '<td><div id="FormStockModels_locate_b" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
            If loc:LocatorSearchButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
            End
            If loc:LocatorClearButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'FormStockModels.cl(''FormStockModels'');')
            End
            packet = clip(packet) & '</div><13,10></td><13,10>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
          If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'FormStockModels_locate_b','buttonset',loc:options)
          End
      End
    End
  End
  p_web.SetSessionValue('FormStockModels_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('FormStockModels_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:NavButtonPosition=Net:Below or loc:NavButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="FormStockModels_nav_b" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'FormStockModels.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'FormStockModels.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'FormStockModels.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'FormStockModels.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'FormStockModels_nav_b','buttonset',loc:options)
          End
        end
        do SendPacket
  End
  If loc:UpdateButtonPosition=Net:Below or loc:UpdateButtonPosition=Net:Both
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
  data
loc:rowclick  string(1024)
  code
    loc:field = p_web.AddBrowseValue('FormStockModels','STOMODEL',stm:RecordNumberKey) !stm:RecordNumber
    p_web._thisrow = p_web._nocolon('stm:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if Loc:LocatorValue <> '' and loc:ActualSelection = ''
        loc:checked = 'checked'
        do SetSelection
      elsif loc:ActualSelection = '' and stm:RecordNumber = p_web.GetValue('stm:RecordNumber')
         loc:checked = 'checked'
         do SetSelection
      elsif loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('FormStockModels:LookupField')) = stm:RecordNumber and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      else
        loc:checked = Choose((stm:RecordNumber = loc:selected) and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      end
      If(loc:SelectionMethod  = Net:Radio)
        loc:RowStyle = 'class="' & p_web.combine(p_web.site.style.browserow,) &'"'
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'data-nt-id="'& p_web.AddBrowseValue('FormStockModels','STOMODEL',stm:RecordNumberKey) &'" onclick="'
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If STOMODEL{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(STOMODEL)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            loc:DefaultSelection = loc:field
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If STOMODEL{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(STOMODEL)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            loc:DefaultSelection = loc:field
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'>'&p_web.CreateInput('radio','_bidv_',clip(loc:field),,loc:checked,'')&'</td>'&CRLF
          If loc:DefaultSelection = ''
            loc:DefaultSelection = loc:field
          End
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          If loc:DefaultSelection = '' or loc:direction < 0
            loc:DefaultSelection = loc:field
          End
        End
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::stm:Model_Number
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr ' & clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Idx = p_web.AddBrowseValue('FormStockModels','STOMODEL',stm:RecordNumberKey)
  TableQueue.Id[1] = stm:RecordNumber

ClosingScripts  Routine
  data
Rtn_SecwinProcedureName         string(252)
  code
  If p_web.RequestAjax = 0
    do SetFormAction
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var btiFormStockModels;if (btiFormStockModels != 1){{var FormStockModels=new browseTable(''FormStockModels'','''&clip(loc:formname)&|
      ''','''&p_web._jsok('stm:RecordNumber',Net:Parameter)&''',2,'''&clip(loc:divname)&''',1,1,1,'''&|
      clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&|
      p_web.Translate('Are you sure you want to delete this record?')&''','''&clip(loc:ActualSelection)&''','''&clip(loc:selectaction)&''','''&|
      clip(loc:formactiontarget)&''','''','&loc:popup&',0,'''','''',"' & clip(Rtn_SecwinProcedureName) & '");<13,10>'&|
      'FormStockModels.setGreenBar('''&p_web.ColorWeb(p_web.Site.Style.BrowseHighlightColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOneColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseTwoColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOverColor)&''');<13,10>' &|
      'FormStockModels.applyGreenBar();btiFormStockModels=1};<13,10>' &|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2FormStockModels')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1FormStockModels')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1FormStockModels')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2FormStockModels')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(STOMODEL)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(STOMODEL)
  Bind(stm:Record)
  Clear(stm:Record)
  NetWebSetSessionPics(p_web,STOMODEL)

Children Routine
  if loc:selecting = 0
    If p_web.RequestAjax = 0
      do StartChildren
    Else
      do AjaxChildren
    End
  end

AjaxChildren  Routine
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue('stm:RecordNumber',p_web.GetValue('stm:RecordNumber'))
   p_web.DeleteValue('_Clicked')
   loc:found = 1
   do Children
! ----------------------------------------------------------------------------------------
CallRow  Routine
  data
loc:result  long
  code
  do OpenFilesB
  stm:RecordNumber = p_web.GSV('stm:RecordNumber')
  loc:result = p_web._GetFile(STOMODEL,stm:RecordNumberKey)
  loc:eip = 1
  loc:viewstate = p_web.escape(p_web.Base64Encode(clip(stm:RecordNumber)))
  do BrowseRow
  do SendPacket
  do ClosefilesB

! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(STOMODEL)
  Case upper(p_web.GetValue('_EIPClm'))
  Else
    case p_web.GetValue('_action_')
    of Net:InsertRecord
    orof Net:ChangeRecord
    orof Net:DeleteRecord
    orof Net:CopyRecord
      loc:eip = 0
      do CallBrowse
    end
  End
  do GotFocusBack
  p_web._CloseFile(STOMODEL)
! ----------------------------------------------------------------------------------------
value::stm:Model_Number   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('FormStockModels_stm:Model_Number_'&stm:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(stm:Model_Number,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END
  return
!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
CheckForDuplicate  Routine
  If loc:invalid <> '' then exit. ! no need to check, record is already invalid
  If Duplicate(stm:Model_Number_Key)
    loc:Invalid = 'stm:Ref_Number'
    loc:Alert = clip(p_web.site.DuplicateText) & ' Model_Number_Key --> stm:Ref_Number, stm:Manufacturer, '&clip('Model Number')&''
  End
SetSelection  Routine
  loc:ActualSelection = loc:field
  p_web.SetSessionValue('stm:RecordNumber',stm:RecordNumber)

MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:ActualSelection = ''
    loc:ActualSelection = loc:DefaultSelection
    p_web.GetBrowseValue(loc:ActualSelection,Net:Web:SessionQueue) ! so children are primed with correct sessionValue
  End

  If loc:ActualSelection <> ''
    TableQueue.Kind = Net:RowTable
    get(TableQueue,TableQueue.Kind)
    if Errorcode() = 0
      TableQueue.Row = sub(TableQueue.Row,1,len(clip(TableQueue.Row))-1) &  ' data-nt-rows="'&loc:RowsHigh&'" data-nt-value="'&clip(loc:ActualSelection)&'"' & '>'
      Put(TableQueue)
    End
  End

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead class="'&p_web.combine(p_web.site.style.BrowseHeader,'')&'"><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot class="'&p_web.combine(p_web.site.style.BrowseFooter,'BrowseFooter')&'"><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody class="'&p_web.combine(p_web.site.style.BrowseBody,)&'"><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table><13,10>' & p_web.DivFooter(Net:NoSend) ! Table Div
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
BrowseSuspendedStock PROCEDURE  (NetWebServerWorker p_web)
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256),dim(Net:MaxKeyFields)
idx           String(Net:HashSize)
sub           Long
            End
sto:Part_Number:IsInvalid  Long
sto:Description:IsInvalid  Long
sto:Supplier:IsInvalid  Long
sto:Purchase_Cost:IsInvalid  Long
sto:Sale_Cost:IsInvalid  Long
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:DefaultSelection    String(Net:HashSize)
loc:ActualSelection     String(Net:HashSize)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
!loc:pagename            String(256)
loc:NavButtonPosition    Long
loc:UpdateButtonPosition Long
loc:SelectionMethod      Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorCase         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
loc:lookupdone          Long
loc:FormPopup           Long
ThisView            View(STOCK)
                      Project(sto:Ref_Number)
                      Project(sto:Part_Number)
                      Project(sto:Description)
                      Project(sto:Supplier)
                      Project(sto:Purchase_Cost)
                      Project(sto:Sale_Cost)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
Loc:popup             Long
loc:poppedup          Long,static,thread
FilesOpened     Long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return 
  End
  GlobalErrors.SetProcedureName('BrowseSuspendedStock')
  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseSuspendedStock:NoForm')
      loc:NoForm = p_web.GetValue('BrowseSuspendedStock:NoForm')
      loc:FormName = p_web.GetValue('BrowseSuspendedStock:FormName')
    else
      loc:FormName = 'BrowseSuspendedStock_frm'
    End
    p_web.SSV('BrowseSuspendedStock:NoForm',loc:NoForm)
    p_web.SSV('BrowseSuspendedStock:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseSuspendedStock:NoForm')
    loc:FormName = p_web.GSV('BrowseSuspendedStock:FormName')
  end
  case p_web.site.formpopups
  of -1 ; loc:FormPopup = Net:Page
  of 0 ; loc:FormPopup = Net:Page
  of 1 ; loc:FormPopup = Net:Popup
  End
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseSuspendedStock') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseSuspendedStock')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if 1 = 2
  elsif p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  elsif p_web.IfExistsValue('_EIPRow_')
    do CallRow
  elsif p_web.GetValue('_CallPopups') > 0
    do CallPopups
  elsif p_web.IfExistsValue('_GetSecwinSettings')
      p_web.SetValue('Secwin_' & 'BrowseSuspendedStock' & 'AccessGroupsArray','1') 
      p_web.SetValue('Secwin_AccessWindowName','BrowseSuspendedStock')
      
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallPopups  Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
loc:CallPopups   Long
loc:name         String(255)
  code
  if loc:poppedup = 0
    loc:CallPopups = p_web.GetValue('_CallPopups')
    if loc:CallPopups = 1 !and p_web.GetPreCall('BrowseSuspendedStock') = 0! for = 1 include popup divs and scripts for this browse
      !p_web.AddPreCall('BrowseSuspendedStock')
      p_web.DivHeader('popup_BrowseSuspendedStock','nt-hidden')
      p_web.DivHeader('BrowseSuspendedStock',p_web.combine(p_web.site.style.browsediv,'adiv'))
      p_web.DivFooter()
      p_web.DivFooter()
      loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(400)&', modal: true, position: [''center'',15]'
      packet =  p_web.jQuery('#' & lower('popup_BrowseSuspendedStock_div'),'dialog',loc:options) & |
      p_web.jQuery('#' & lower('popup_BrowseSuspendedStock_div'),'removeClass','''nt-hidden''')
    End
    ! for > 0 add in form, EIP-lookups and browse object
    do Popups
    loc:popup = 1
    do ClosingScripts
    do SendPacket
    loc:poppedup = 1
  end

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web.DivHeader('BrowseSuspendedStock',p_web.combine(p_web.site.style.browsediv,'adiv'))
  if loc:ParentSilent = 0
    do GenerateBrowse
    if p_web.RequestAjax = 1
      p_web.Script('BrowseProducts.pcv();')
    end
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web.DivFooter()
  do Children
  do Popups
  if loc:poppedup = 0
    do ClosingScripts
  end
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(STOCK,sto:Ref_Number_Key,loc:vorder)
    If False
    ElsIf (loc:vorder = 'STO:PART_NUMBER') then p_web.SetValue('BrowseSuspendedStock_sort','1')
    ElsIf (loc:vorder = 'STO:DESCRIPTION') then p_web.SetValue('BrowseSuspendedStock_sort','2')
    ElsIf (loc:vorder = 'STO:SUPPLIER') then p_web.SetValue('BrowseSuspendedStock_sort','3')
    ElsIf (loc:vorder = 'STO:PURCHASE_COST') then p_web.SetValue('BrowseSuspendedStock_sort','4')
    ElsIf (loc:vorder = 'STO:SALE_COST') then p_web.SetValue('BrowseSuspendedStock_sort','5')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseSuspendedStock:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseSuspendedStock:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseSuspendedStock:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseSuspendedStock:LookupField')
    loc:selecting = 0
  End

Popups Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  If loc:popup = 0
  End
SetFormAction  Routine
  loc:formaction = 'FormStockControl'
  loc:formactiontarget = '_self'

GotFocusBack   Routine

GenerateBrowse Routine
  data
loc:viewoptions  Long
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  ! Set general Browse options
  loc:NavButtonPosition   = Net:Below
  loc:UpdateButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:PageLoad
  loc:Sorting          = Net:ServerSort
  if p_web.GetValue('_popup_') = 1
    loc:popup = 1
  end
  ! Set Locator Options
  loc:LocatorPosition  = Net:Above
  loc:LocatorBlank = 1
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  p_web.ClearBrowse('BrowseSuspendedStock')
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 12
  End
  loc:ActualSelection = ''
  !loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseSuspendedStock_sort',net:DontEvaluate)
  If loc:vordernumber = 0
    loc:vordernumber = 1
  End
  p_web.SetSessionValue('BrowseSuspendedStock_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(sto:Part_Number)','-UPPER(sto:Part_Number)')
    Loc:LocateField = 'sto:Part_Number'
    Loc:LocatorCase = 0
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(sto:Description)','-UPPER(sto:Description)')
    Loc:LocateField = 'sto:Description'
    Loc:LocatorCase = 0
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(sto:Supplier)','-UPPER(sto:Supplier)')
    Loc:LocateField = 'sto:Supplier'
    Loc:LocatorCase = 0
  of 4
    loc:vorder = Choose(Loc:SortDirection=1,'sto:Purchase_Cost','-sto:Purchase_Cost')
    Loc:LocateField = 'sto:Purchase_Cost'
    Loc:LocatorCase = 0
  of 5
    loc:vorder = Choose(Loc:SortDirection=1,'sto:Sale_Cost','-sto:Sale_Cost')
    Loc:LocateField = 'sto:Sale_Cost'
    Loc:LocatorCase = 0
  end
  if loc:vorder = ''
    loc:vorder = '+UPPER(sto:Location),+sto:Suspend,+UPPER(sto:Part_Number)'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('sto:Part_Number')
    loc:SortHeader = p_web.Translate('Part Number')
    p_web.SetSessionValue('BrowseSuspendedStock_LocatorPic','@s30')
  Of upper('sto:Description')
    loc:SortHeader = p_web.Translate('Description')
    p_web.SetSessionValue('BrowseSuspendedStock_LocatorPic','@s30')
  Of upper('sto:Supplier')
    loc:SortHeader = p_web.Translate('Supplier')
    p_web.SetSessionValue('BrowseSuspendedStock_LocatorPic','@s30')
  Of upper('sto:Purchase_Cost')
    loc:SortHeader = p_web.Translate('In Warr')
    p_web.SetSessionValue('BrowseSuspendedStock_LocatorPic','@n14.2')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
  Of upper('sto:Sale_Cost')
    loc:SortHeader = p_web.Translate('Out Warr')
    p_web.SetSessionValue('BrowseSuspendedStock_LocatorPic','@n14.2')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
  End
  If loc:selecting = 1
    p_web.GetSettings(p_web.GetSessionValue('Push1'))
    loc:selectaction = p_web.FormSettings.ParentPage !p_web.GetSessionValue('BrowseSuspendedStock:LookupFrom')
  End!Else
  do SendPacket
  do SetFormAction
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseSuspendedStock:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseSuspendedStock:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseSuspendedStock:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="STOCK"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="sto:Ref_Number_Key"></input><13,10>'
  end
  case p_web.GetValue('refresh') ; of 'next' orof 'previous' orof 'first' orof 'last'
    p_web.Script('try{{BrowseSuspendedStock.restoreFocus();} catch(err){{};')
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseSuspendedStock',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      case loc:locatortype
      of Net:Position
      orof Net:Date
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
      of Net:Contains
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
      of Net:Begins
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
      End
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,'Locator')&'"><tr>' &|
      Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
      Case Loc:LocatorType
      of Net:Date
        packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator2BrowseSuspendedStock','',p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,, p_web.GSV('BrowseSuspendedStock_LocatorPic'),,,'onchange="BrowseSuspendedStock.locate(''Locator2BrowseSuspendedStock'',this.value);"',,0,,,) & '</div></td>'
      Else
        packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator2BrowseSuspendedStock',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,'size="30" onchange="BrowseSuspendedStock.locate(''Locator2BrowseSuspendedStock'',this.value);" ',,,,,,) & '</td>'
      End
      If loc:LocatorSearchButton or loc:LocatorClearButton
        packet = clip(packet) & '<td><div id="BrowseSuspendedStock_locate_a" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
        If loc:LocatorSearchButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
        End
        If loc:LocatorClearButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseSuspendedStock.cl(''BrowseSuspendedStock'');')
        End
        packet = clip(packet) & '</div><13,10></td><13,10>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
      If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'BrowseSuspendedStock_locate_a','buttonset',loc:options)
      End
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & p_web.DivHeader('BrowseSuspendedStock_table',p_web.Combine(p_web.site.style.BrowseTableDiv,''),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,,'sortable')&'" id="BrowseSuspendedStock_tbl">'
  Else
    packet = clip(packet) & p_web.DivHeader('BrowseSuspendedStock_table',p_web.Combine(p_web.site.style.BrowseTableDiv,''),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,)&'" id="BrowseSuspendedStock_tbl">'
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'1','BrowseSuspendedStock',p_web.Translate('Part Number'),'Click here to sort by Part Number',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'2','BrowseSuspendedStock',p_web.Translate('Description'),'Click here to sort by Description',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'3','BrowseSuspendedStock',p_web.Translate('Supplier'),'Click here to sort by Supplier',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'4','BrowseSuspendedStock',p_web.Translate('In Warr'),'Click here to sort by Purchase Cost',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'5','BrowseSuspendedStock',p_web.Translate('Out Warr'),'Click here to sort by Sale Cost',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,12,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  If Instring('sto:ref_number',lower(loc:vorder),1,1) = 0 !and STOCK{prop:SQLDriver} = 1
    loc:vorder = Choose(loc:vorder='','sto:Ref_Number',clip(loc:vorder) & ',' & 'sto:Ref_Number')
  End
  Loc:Selected = Choose(p_web.IfExistsValue('sto:Ref_Number'),p_web.GetValue('sto:Ref_Number'),p_web.GetSessionValue('sto:Ref_Number'))
  ThisView{prop:order} = p_web.CleanFilter(ThisView,clip(loc:vorder))
      loc:FilterWas = 'Upper(sto:Location) = Upper(''' & p_web.GSV('Default:SiteLocation') & ''') AND Upper(sto:Suspend) = 1'
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseSuspendedStock',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseSuspendedStock_Filter')
    p_web.SetSessionValue('BrowseSuspendedStock_FirstValue','')
    p_web.SetSessionValue('BrowseSuspendedStock_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,STOCK,sto:Ref_Number_Key,loc:PageRows,'BrowseSuspendedStock',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,loc:ViewOptions,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled,Loc:LocatorCase)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or Loc:LocatorType = Net:Date or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position or loc:LocatorType = Net:Date
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If STOCK{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(STOCK,loc:firstvalue)
              Reset(ThisView,STOCK)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If STOCK{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(STOCK,loc:lastvalue)
            Reset(ThisView,STOCK)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(sto:Ref_Number)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td class="'&p_web.combine(p_web.site.style.BrowseEmpty,)&'">'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:NavButtonPosition=Net:Above or (loc:NavButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseSuspendedStock_nav_a" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,' nt-left nt-margin-right',)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseSuspendedStock.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseSuspendedStock.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseSuspendedStock.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseSuspendedStock.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseSuspendedStock_nav_a','buttonset',loc:options)
          End
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:UpdateButtonPosition=Net:Above or (loc:UpdateButtonPosition=Net:Both and loc:found > 0))
    packet = clip(packet) & '<div id="BrowseSuspendedStock_update_a" class="'&p_web.combine(p_web.site.style.BrowseUpdateButtonSet,)&'">'
    If loc:found
          Loc:IsChange = 0
          If loc:selecting = 0 or loc:popup
            If loc:viewonly = 0
              packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:ChangeButton,'BrowseSuspendedStock',,,loc:FormPopup,'FormStockControl')
              loc:isChange = 1
            End
          End
          TableQueue.Kind = Net:BeforeTable
          do AddPacket
    End
    packet = clip(packet) & '</div><13,10>'
    If p_web.site.UseUpdateButtonSet
      loc:options = ''
      packet = clip(packet) & p_web.jQuery('#' & 'BrowseSuspendedStock_update_a','buttonset',loc:options)
    End ! If p_web.site.UseUpdateButtonSet
      TableQueue.Kind = Net:BeforeTable
      do AddPacket
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseSuspendedStock',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseSuspendedStock_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseSuspendedStock_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          case loc:locatortype
          of Net:Position
          orof Net:Date
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
          of Net:Contains
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
          of Net:Begins
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
          End
          packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,'Locator')&'"><tr>' &|
          Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
          Case Loc:LocatorType
          of Net:Date
            packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator1BrowseSuspendedStock','',p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,, p_web.GSV('BrowseSuspendedStock_LocatorPic'),,,'onchange="BrowseSuspendedStock.locate(''Locator1BrowseSuspendedStock'',this.value);"',,0,,,) & '</div></td>'
          Else
            packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator1BrowseSuspendedStock',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,'size="30" onchange="BrowseSuspendedStock.locate(''Locator1BrowseSuspendedStock'',this.value);" ',,,,,,) & '</td>'
          End
          If loc:LocatorSearchButton or loc:LocatorClearButton
            packet = clip(packet) & '<td><div id="BrowseSuspendedStock_locate_b" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
            If loc:LocatorSearchButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
            End
            If loc:LocatorClearButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseSuspendedStock.cl(''BrowseSuspendedStock'');')
            End
            packet = clip(packet) & '</div><13,10></td><13,10>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
          If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseSuspendedStock_locate_b','buttonset',loc:options)
          End
      End
    End
  End
  p_web.SetSessionValue('BrowseSuspendedStock_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseSuspendedStock_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:NavButtonPosition=Net:Below or loc:NavButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseSuspendedStock_nav_b" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,' nt-left nt-margin-right',)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseSuspendedStock.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseSuspendedStock.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseSuspendedStock.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseSuspendedStock.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseSuspendedStock_nav_b','buttonset',loc:options)
          End
        end
        do SendPacket
  End
  If loc:UpdateButtonPosition=Net:Below or loc:UpdateButtonPosition=Net:Both
  packet = clip(packet) & '<div id="BrowseSuspendedStock_update_b" class="'&p_web.combine(p_web.site.style.BrowseUpdateButtonSet,)&'">'
  If loc:found
        Loc:IsChange = 0
        If loc:selecting = 0 or loc:popup
          If loc:viewonly = 0
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:ChangeButton,'BrowseSuspendedStock',,,loc:FormPopup,'FormStockControl')
            loc:isChange = 1
          End
        End
        do SendPacket
  End
  packet = clip(packet) & '</div><13,10>'
  If p_web.site.UseUpdateButtonSet
    loc:options = ''
    packet = clip(packet) & p_web.jQuery('#' & 'BrowseSuspendedStock_update_b','buttonset',loc:options)
  End ! If p_web.site.UseUpdateButtonSet
    do SendPacket
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
  data
loc:rowclick  string(1024)
  code
    loc:field = p_web.AddBrowseValue('BrowseSuspendedStock','STOCK',sto:Ref_Number_Key) !sto:Ref_Number
    p_web._thisrow = p_web._nocolon('sto:Ref_Number')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if Loc:LocatorValue <> '' and loc:ActualSelection = ''
        loc:checked = 'checked'
        do SetSelection
      elsif loc:ActualSelection = '' and sto:Ref_Number = p_web.GetValue('sto:Ref_Number')
         loc:checked = 'checked'
         do SetSelection
      elsif loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseSuspendedStock:LookupField')) = sto:Ref_Number and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      else
        loc:checked = Choose((sto:Ref_Number = loc:selected) and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      end
      If(loc:SelectionMethod  = Net:Radio)
        loc:RowStyle = 'class="' & p_web.combine(p_web.site.style.browserow,) &'"'
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'data-nt-id="'& p_web.AddBrowseValue('BrowseSuspendedStock','STOCK',sto:Ref_Number_Key) &'" onclick="'
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
      ! Save highlighted record
      If (loc:Checked)
          p_web.SSV('BrowseID',sto:Ref_Number)
      END
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If STOCK{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(STOCK)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            loc:DefaultSelection = loc:field
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If STOCK{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(STOCK)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            loc:DefaultSelection = loc:field
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'>'&p_web.CreateInput('radio','_bidv_',clip(loc:field),,loc:checked,'')&'</td>'&CRLF
          If loc:DefaultSelection = ''
            loc:DefaultSelection = loc:field
          End
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          If loc:DefaultSelection = '' or loc:direction < 0
            loc:DefaultSelection = loc:field
          End
        End
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::sto:Part_Number
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::sto:Description
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::sto:Supplier
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::sto:Purchase_Cost
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::sto:Sale_Cost
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr ' & clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Idx = p_web.AddBrowseValue('BrowseSuspendedStock','STOCK',sto:Ref_Number_Key)
  TableQueue.Id[1] = sto:Ref_Number

ClosingScripts  Routine
  data
Rtn_SecwinProcedureName         string(252)
  code
  If p_web.RequestAjax = 0
    do SetFormAction
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var btiBrowseSuspendedStock;if (btiBrowseSuspendedStock != 1){{var BrowseSuspendedStock=new browseTable(''BrowseSuspendedStock'','''&clip(loc:formname)&|
      ''','''&p_web._jsok('sto:Ref_Number',Net:Parameter)&''',2,'''&clip(loc:divname)&''',1,1,1,'''&|
      clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&|
      p_web.Translate('Are you sure you want to delete this record?')&''','''&clip(loc:ActualSelection)&''','''&clip(loc:selectaction)&''','''&|
      clip(loc:formactiontarget)&''',''FormStockControl'','&loc:popup&',0,'''','''',"' & clip(Rtn_SecwinProcedureName) & '");<13,10>'&|
      'BrowseSuspendedStock.setGreenBar('''&p_web.ColorWeb(p_web.Site.Style.BrowseHighlightColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOneColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseTwoColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOverColor)&''');<13,10>' &|
      'BrowseSuspendedStock.applyGreenBar();btiBrowseSuspendedStock=1};<13,10>' &|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseSuspendedStock')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseSuspendedStock')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseSuspendedStock')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseSuspendedStock')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(STOCK)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(STOCK)
  Bind(sto:Record)
  Clear(sto:Record)
  NetWebSetSessionPics(p_web,STOCK)

Children Routine
  if loc:selecting = 0
    If p_web.RequestAjax = 0
      do StartChildren
    Else
      do AjaxChildren
    End
  end

AjaxChildren  Routine
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue('sto:Ref_Number',p_web.GetValue('sto:Ref_Number'))
   p_web.DeleteValue('_Clicked')
   loc:found = 1
   do Children
! ----------------------------------------------------------------------------------------
CallRow  Routine
  data
loc:result  long
  code
  do OpenFilesB
  sto:Ref_Number = p_web.GSV('sto:Ref_Number')
  loc:result = p_web._GetFile(STOCK,sto:Ref_Number_Key)
  loc:eip = 1
  loc:viewstate = p_web.escape(p_web.Base64Encode(clip(sto:Ref_Number)))
  do BrowseRow
  do SendPacket
  do ClosefilesB

! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(STOCK)
  Case upper(p_web.GetValue('_EIPClm'))
  Else
    case p_web.GetValue('_action_')
    of Net:InsertRecord
    orof Net:ChangeRecord
    orof Net:DeleteRecord
    orof Net:CopyRecord
      loc:eip = 0
      do CallBrowse
    end
  End
  do GotFocusBack
  p_web._CloseFile(STOCK)
! ----------------------------------------------------------------------------------------
value::sto:Part_Number   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseSuspendedStock_sto:Part_Number_'&sto:Ref_Number,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(sto:Part_Number,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sto:Description   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseSuspendedStock_sto:Description_'&sto:Ref_Number,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(sto:Description,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sto:Supplier   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseSuspendedStock_sto:Supplier_'&sto:Ref_Number,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(sto:Supplier,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sto:Purchase_Cost   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseSuspendedStock_sto:Purchase_Cost_'&sto:Ref_Number,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(sto:Purchase_Cost,'@n14.2')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sto:Sale_Cost   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseSuspendedStock_sto:Sale_Cost_'&sto:Ref_Number,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(sto:Sale_Cost,'@n14.2')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END
  return
!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
CheckForDuplicate  Routine
SetSelection  Routine
  loc:ActualSelection = loc:field
  p_web.SetSessionValue('sto:Ref_Number',sto:Ref_Number)

MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:ActualSelection = ''
    loc:ActualSelection = loc:DefaultSelection
    p_web.GetBrowseValue(loc:ActualSelection,Net:Web:SessionQueue) ! so children are primed with correct sessionValue
  End

  If loc:ActualSelection <> ''
    TableQueue.Kind = Net:RowTable
    get(TableQueue,TableQueue.Kind)
    if Errorcode() = 0
      TableQueue.Row = sub(TableQueue.Row,1,len(clip(TableQueue.Row))-1) &  ' data-nt-rows="'&loc:RowsHigh&'" data-nt-value="'&clip(loc:ActualSelection)&'"' & '>'
      Put(TableQueue)
    End
  End

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead class="'&p_web.combine(p_web.site.style.BrowseHeader,'')&'"><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot class="'&p_web.combine(p_web.site.style.BrowseFooter,'BrowseFooter')&'"><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody class="'&p_web.combine(p_web.site.style.BrowseBody,)&'"><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table><13,10>' & p_web.DivFooter(Net:NoSend) ! Table Div
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
BrowseReturnOrderTracking PROCEDURE  (NetWebServerWorker p_web)
locAcceptReject      STRING(30)                            !
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256),dim(Net:MaxKeyFields)
idx           String(Net:HashSize)
sub           Long
            End
rtn:WaybillNumber:IsInvalid  Long
rtn:PartNumber:IsInvalid  Long
rtn:Description:IsInvalid  Long
rtn:DateCreated:IsInvalid  Long
rtn:DateOrdered:IsInvalid  Long
rtn:DateReceived:IsInvalid  Long
locAcceptReject:IsInvalid  Long
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:DefaultSelection    String(Net:HashSize)
loc:ActualSelection     String(Net:HashSize)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
!loc:pagename            String(256)
loc:NavButtonPosition    Long
loc:UpdateButtonPosition Long
loc:SelectionMethod      Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorCase         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
loc:lookupdone          Long
loc:FormPopup           Long
ThisView            View(RTNORDER)
                      Project(rtn:RecordNumber)
                      Project(rtn:WaybillNumber)
                      Project(rtn:PartNumber)
                      Project(rtn:Description)
                      Project(rtn:DateCreated)
                      Project(rtn:DateOrdered)
                      Project(rtn:DateReceived)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
Loc:popup             Long
loc:poppedup          Long,static,thread
FilesOpened     Long
WAYBILLS::State  USHORT
RTNAWAIT::State  USHORT
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return 
  End
  GlobalErrors.SetProcedureName('BrowseReturnOrderTracking')
  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseReturnOrderTracking:NoForm')
      loc:NoForm = p_web.GetValue('BrowseReturnOrderTracking:NoForm')
      loc:FormName = p_web.GetValue('BrowseReturnOrderTracking:FormName')
    else
      loc:FormName = 'BrowseReturnOrderTracking_frm'
    End
    p_web.SSV('BrowseReturnOrderTracking:NoForm',loc:NoForm)
    p_web.SSV('BrowseReturnOrderTracking:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseReturnOrderTracking:NoForm')
    loc:FormName = p_web.GSV('BrowseReturnOrderTracking:FormName')
  end
  case p_web.site.formpopups
  of -1 ; loc:FormPopup = Net:Page
  of 0 ; loc:FormPopup = Net:Page
  of 1 ; loc:FormPopup = Net:Popup
  End
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseReturnOrderTracking') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseReturnOrderTracking')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if 1 = 2
  elsif p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  elsif p_web.IfExistsValue('_EIPRow_')
    do CallRow
  elsif p_web.GetValue('_CallPopups') > 0
    do CallPopups
  elsif p_web.IfExistsValue('_GetSecwinSettings')
      p_web.SetValue('Secwin_' & 'BrowseReturnOrderTracking' & 'AccessGroupsArray','1') 
      p_web.SetValue('Secwin_AccessWindowName','BrowseReturnOrderTracking')
      
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallPopups  Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
loc:CallPopups   Long
loc:name         String(255)
  code
  if loc:poppedup = 0
    loc:CallPopups = p_web.GetValue('_CallPopups')
    if loc:CallPopups = 1 !and p_web.GetPreCall('BrowseReturnOrderTracking') = 0! for = 1 include popup divs and scripts for this browse
      !p_web.AddPreCall('BrowseReturnOrderTracking')
      p_web.DivHeader('popup_BrowseReturnOrderTracking','nt-hidden')
      p_web.DivHeader('BrowseReturnOrderTracking',p_web.combine(p_web.site.style.browsediv,))
      p_web.DivFooter()
      p_web.DivFooter()
      loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(400)&', modal: true, position: [''center'',15]'
      packet =  p_web.jQuery('#' & lower('popup_BrowseReturnOrderTracking_div'),'dialog',loc:options) & |
      p_web.jQuery('#' & lower('popup_BrowseReturnOrderTracking_div'),'removeClass','''nt-hidden''')
    End
    ! for > 0 add in form, EIP-lookups and browse object
    do Popups
    loc:popup = 1
    do ClosingScripts
    do SendPacket
    loc:poppedup = 1
  end

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web.DivHeader('BrowseReturnOrderTracking',p_web.combine(p_web.site.style.browsediv,))
  if loc:ParentSilent = 0
    do GenerateBrowse
    if p_web.RequestAjax = 1
      p_web.Script('BrowseProducts.pcv();')
    end
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web.DivFooter()
  do Children
  do Popups
  if loc:poppedup = 0
    do ClosingScripts
  end
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(RTNORDER,rtn:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'RTN:WAYBILLNUMBER') then p_web.SetValue('BrowseReturnOrderTracking_sort','1')
    ElsIf (loc:vorder = 'RTN:PARTNUMBER') then p_web.SetValue('BrowseReturnOrderTracking_sort','2')
    ElsIf (loc:vorder = 'RTN:DESCRIPTION') then p_web.SetValue('BrowseReturnOrderTracking_sort','3')
    ElsIf (loc:vorder = 'RTN:DATECREATED') then p_web.SetValue('BrowseReturnOrderTracking_sort','4')
    ElsIf (loc:vorder = 'RTN:DATEORDERED') then p_web.SetValue('BrowseReturnOrderTracking_sort','5')
    ElsIf (loc:vorder = 'RTN:DATERECEIVED') then p_web.SetValue('BrowseReturnOrderTracking_sort','6')
    ElsIf (loc:vorder = 'LOCACCEPTREJECT') then p_web.SetValue('BrowseReturnOrderTracking_sort','7')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseReturnOrderTracking:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseReturnOrderTracking:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseReturnOrderTracking:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseReturnOrderTracking:LookupField')
    loc:selecting = 0
  End

Popups Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  If loc:popup = 0
  End
SetFormAction  Routine
  loc:formaction = 'BrowseReturnOrderTracking'

GotFocusBack   Routine

GenerateBrowse Routine
  data
loc:viewoptions  Long
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  ! Set general Browse options
  loc:NavButtonPosition   = Net:Below
  loc:UpdateButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:PageLoad
  loc:Sorting          = Net:ServerSort
  if p_web.GetValue('_popup_') = 1
    loc:popup = 1
  end
  ! Set Locator Options
  loc:LocatorPosition  = Net:Below
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  p_web.ClearBrowse('BrowseReturnOrderTracking')
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 15
  End
  loc:ActualSelection = ''
  !loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseReturnOrderTracking_sort',net:DontEvaluate)
  If loc:vordernumber = 0
    loc:vordernumber = 1
  End
  p_web.SetSessionValue('BrowseReturnOrderTracking_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'rtn:WaybillNumber','-rtn:WaybillNumber')
    Loc:LocateField = 'rtn:WaybillNumber'
    Loc:LocatorCase = 0
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(rtn:PartNumber)','-UPPER(rtn:PartNumber)')
    Loc:LocateField = 'rtn:PartNumber'
    Loc:LocatorCase = 0
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(rtn:Description)','-UPPER(rtn:Description)')
    Loc:LocateField = 'rtn:Description'
    Loc:LocatorCase = 0
  of 4
    loc:vorder = Choose(Loc:SortDirection=1,'rtn:DateCreated','-rtn:DateCreated')
    Loc:LocateField = 'rtn:DateCreated'
    Loc:LocatorCase = 0
  of 5
    loc:vorder = Choose(Loc:SortDirection=1,'rtn:DateOrdered','-rtn:DateOrdered')
    Loc:LocateField = 'rtn:DateOrdered'
    Loc:LocatorCase = 0
  of 6
    loc:vorder = Choose(Loc:SortDirection=1,'rtn:DateReceived','-rtn:DateReceived')
    Loc:LocateField = 'rtn:DateReceived'
    Loc:LocatorCase = 0
  of 7
    loc:vorder = Choose(Loc:SortDirection=1,'locAcceptReject','-locAcceptReject')
    Loc:LocateField = 'locAcceptReject'
    Loc:LocatorCase = 0
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('rtn:WaybillNumber')
    loc:SortHeader = p_web.Translate('Waybill Number')
    p_web.SetSessionValue('BrowseReturnOrderTracking_LocatorPic','@n_10')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
  Of upper('rtn:PartNumber')
    loc:SortHeader = p_web.Translate('Part / Model No')
    p_web.SetSessionValue('BrowseReturnOrderTracking_LocatorPic','@s30')
  Of upper('rtn:Description')
    loc:SortHeader = p_web.Translate('Description / IMEI No')
    p_web.SetSessionValue('BrowseReturnOrderTracking_LocatorPic','@s30')
  Of upper('rtn:DateCreated')
    loc:SortHeader = p_web.Translate('Date Created')
    p_web.SetSessionValue('BrowseReturnOrderTracking_LocatorPic','@d17b')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
    loc:LocatorType = Net:Date
  Of upper('rtn:DateOrdered')
    loc:SortHeader = p_web.Translate('Date Returned')
    p_web.SetSessionValue('BrowseReturnOrderTracking_LocatorPic','@d17b')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
    loc:LocatorType = Net:Date
  Of upper('rtn:DateReceived')
    loc:SortHeader = p_web.Translate('Date Received')
    p_web.SetSessionValue('BrowseReturnOrderTracking_LocatorPic','@d17b')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
    loc:LocatorType = Net:Date
  Of upper('locAcceptReject')
    loc:SortHeader = p_web.Translate('Accept/Reject')
    p_web.SetSessionValue('BrowseReturnOrderTracking_LocatorPic','@s30')
  End
  If loc:selecting = 1
    p_web.GetSettings(p_web.GetSessionValue('Push1'))
    loc:selectaction = p_web.FormSettings.ParentPage !p_web.GetSessionValue('BrowseReturnOrderTracking:LookupFrom')
  End!Else
  loc:CloseAction = p_web.site.DefaultPage
  do SendPacket
  do SetFormAction
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseReturnOrderTracking:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseReturnOrderTracking:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseReturnOrderTracking:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="RTNORDER"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="rtn:RecordNumberKey"></input><13,10>'
  end
  case p_web.GetValue('refresh') ; of 'next' orof 'previous' orof 'first' orof 'last'
    p_web.Script('try{{BrowseReturnOrderTracking.restoreFocus();} catch(err){{};')
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseReturnOrderTracking',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      case loc:locatortype
      of Net:Position
      orof Net:Date
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
      of Net:Contains
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
      of Net:Begins
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
      End
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
      Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
      Case Loc:LocatorType
      of Net:Date
        packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator2BrowseReturnOrderTracking','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('BrowseReturnOrderTracking_LocatorPic'),,,'onchange="BrowseReturnOrderTracking.locate(''Locator2BrowseReturnOrderTracking'',this.value);"',,0,,,) & '</div></td>'
      Else
        packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator2BrowseReturnOrderTracking',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="BrowseReturnOrderTracking.locate(''Locator2BrowseReturnOrderTracking'',this.value);" ',,,,,,) & '</td>'
      End
      If loc:LocatorSearchButton or loc:LocatorClearButton
        packet = clip(packet) & '<td><div id="BrowseReturnOrderTracking_locate_a" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
        If loc:LocatorSearchButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
        End
        If loc:LocatorClearButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseReturnOrderTracking.cl(''BrowseReturnOrderTracking'');')
        End
        packet = clip(packet) & '</div><13,10></td><13,10>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
      If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'BrowseReturnOrderTracking_locate_a','buttonset',loc:options)
      End
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & p_web.DivHeader('BrowseReturnOrderTracking_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'','sortable')&'" id="BrowseReturnOrderTracking_tbl">'
  Else
    packet = clip(packet) & p_web.DivHeader('BrowseReturnOrderTracking_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'')&'" id="BrowseReturnOrderTracking_tbl">'
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'1','BrowseReturnOrderTracking',p_web.Translate('Waybill Number'),'Click here to sort by Waybill Number',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'2','BrowseReturnOrderTracking',p_web.Translate('Part / Model No'),'Click here to sort by Part Number',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'3','BrowseReturnOrderTracking',p_web.Translate('Description / IMEI No'),'Click here to sort by Description',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'4','BrowseReturnOrderTracking',p_web.Translate('Date Created'),'Click here to sort by Date Created',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'5','BrowseReturnOrderTracking',p_web.Translate('Date Returned'),'Click here to sort by Date Returned',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'6','BrowseReturnOrderTracking',p_web.Translate('Date Received'),'Click here to sort by Date Received',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'7','BrowseReturnOrderTracking',p_web.Translate('Accept/Reject'),,,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:LocateField = 'rtn:DateCreated' then Loc:NoBuffer = 1.
  If Loc:LocateField = 'rtn:DateOrdered' then Loc:NoBuffer = 1.
  If Loc:LocateField = 'rtn:DateReceived' then Loc:NoBuffer = 1.
  If Loc:NoBuffer = 0
    Buffer(ThisView,15,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  If Instring('rtn:recordnumber',lower(loc:vorder),1,1) = 0 !and RTNORDER{prop:SQLDriver} = 1
    loc:vorder = Choose(loc:vorder='','rtn:RecordNumber',clip(loc:vorder) & ',' & 'rtn:RecordNumber')
  End
  Loc:Selected = Choose(p_web.IfExistsValue('rtn:RecordNumber'),p_web.GetValue('rtn:RecordNumber'),p_web.GetSessionValue('rtn:RecordNumber'))
  ThisView{prop:order} = p_web.CleanFilter(ThisView,clip(loc:vorder))
      loc:FilterWas = 'rtn:Archived = 0 AND rtn:Ordered = 1 AND Upper(rtn:Location) = Upper(''' & p_web.GSV('Default:SiteLocation') & ''') AND rtn:ExchangeOrder = UPPER(''' & p_web.GSV('locReturnTrackingType') & ''')'
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseReturnOrderTracking',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseReturnOrderTracking_Filter')
    p_web.SetSessionValue('BrowseReturnOrderTracking_FirstValue','')
    p_web.SetSessionValue('BrowseReturnOrderTracking_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,RTNORDER,rtn:RecordNumberKey,loc:PageRows,'BrowseReturnOrderTracking',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,loc:ViewOptions,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled,Loc:LocatorCase)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or Loc:LocatorType = Net:Date or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position or loc:LocatorType = Net:Date
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If RTNORDER{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(RTNORDER,loc:firstvalue)
              Reset(ThisView,RTNORDER)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If RTNORDER{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(RTNORDER,loc:lastvalue)
            Reset(ThisView,RTNORDER)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      ! #12151 Display accepted/rejected column (Bryan: 26/09/2011)
      Access:RTNAWAIT.Clearkey(rta:RTNORDERKey)
      rta:RTNRecordNumber = rtn:RecordNumber
      IF (Access:RTNAWAIT.TryFetch(rta:RTNORDERKey))
          locAcceptReject = ''
      ELSE
          IF (rta:AcceptReject = 'ACCEPT')
              locAcceptReject = 'Accepted'
          ELSIF rta:AcceptReject = 'REJECT'
              locAcceptReject = 'Rejected'
          ELSE
              locAcceptReject = ''
          END
      END
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(rtn:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td class="'&p_web.combine(p_web.site.style.BrowseEmpty,)&'">'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:NavButtonPosition=Net:Above or (loc:NavButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseReturnOrderTracking_nav_a" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,' nt-left nt-margin-right',)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseReturnOrderTracking.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseReturnOrderTracking.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseReturnOrderTracking.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseReturnOrderTracking.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseReturnOrderTracking_nav_a','buttonset',loc:options)
          End
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:UpdateButtonPosition=Net:Above or (loc:UpdateButtonPosition=Net:Both and loc:found > 0))
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseReturnOrderTracking',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseReturnOrderTracking_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseReturnOrderTracking_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          case loc:locatortype
          of Net:Position
          orof Net:Date
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
          of Net:Contains
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
          of Net:Begins
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
          End
          packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
          Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
          Case Loc:LocatorType
          of Net:Date
            packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator1BrowseReturnOrderTracking','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('BrowseReturnOrderTracking_LocatorPic'),,,'onchange="BrowseReturnOrderTracking.locate(''Locator1BrowseReturnOrderTracking'',this.value);"',,0,,,) & '</div></td>'
          Else
            packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator1BrowseReturnOrderTracking',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="BrowseReturnOrderTracking.locate(''Locator1BrowseReturnOrderTracking'',this.value);" ',,,,,,) & '</td>'
          End
          If loc:LocatorSearchButton or loc:LocatorClearButton
            packet = clip(packet) & '<td><div id="BrowseReturnOrderTracking_locate_b" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
            If loc:LocatorSearchButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
            End
            If loc:LocatorClearButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseReturnOrderTracking.cl(''BrowseReturnOrderTracking'');')
            End
            packet = clip(packet) & '</div><13,10></td><13,10>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
          If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseReturnOrderTracking_locate_b','buttonset',loc:options)
          End
      End
    End
  End
  p_web.SetSessionValue('BrowseReturnOrderTracking_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseReturnOrderTracking_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:NavButtonPosition=Net:Below or loc:NavButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseReturnOrderTracking_nav_b" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,' nt-left nt-margin-right',)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseReturnOrderTracking.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseReturnOrderTracking.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseReturnOrderTracking.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseReturnOrderTracking.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseReturnOrderTracking_nav_b','buttonset',loc:options)
          End
        end
        do SendPacket
  End
  If loc:UpdateButtonPosition=Net:Below or loc:UpdateButtonPosition=Net:Both
  End
  If loc:selecting = 0 and loc:parent = ''
      if loc:popup
        packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:BrowseCloseButton,'BrowseReturnOrderTracking',,,loc:popup)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCloseButton,loc:Formname,loc:CloseAction)
      end
  End
    do SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
  data
loc:rowclick  string(1024)
  code
    loc:field = p_web.AddBrowseValue('BrowseReturnOrderTracking','RTNORDER',rtn:RecordNumberKey) !rtn:RecordNumber
    p_web._thisrow = p_web._nocolon('rtn:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if Loc:LocatorValue <> '' and loc:ActualSelection = ''
        loc:checked = 'checked'
        do SetSelection
      elsif loc:ActualSelection = '' and rtn:RecordNumber = p_web.GetValue('rtn:RecordNumber')
         loc:checked = 'checked'
         do SetSelection
      elsif loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseReturnOrderTracking:LookupField')) = rtn:RecordNumber and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      else
        loc:checked = Choose((rtn:RecordNumber = loc:selected) and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      end
      If(loc:SelectionMethod  = Net:Radio)
        loc:RowStyle = 'class="' & p_web.combine(p_web.site.style.browserow,) &'"'
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'data-nt-id="'& p_web.AddBrowseValue('BrowseReturnOrderTracking','RTNORDER',rtn:RecordNumberKey) &'" onclick="'
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If RTNORDER{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(RTNORDER)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            loc:DefaultSelection = loc:field
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If RTNORDER{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(RTNORDER)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            loc:DefaultSelection = loc:field
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'>'&p_web.CreateInput('radio','_bidv_',clip(loc:field),,loc:checked,'')&'</td>'&CRLF
          If loc:DefaultSelection = ''
            loc:DefaultSelection = loc:field
          End
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          If loc:DefaultSelection = '' or loc:direction < 0
            loc:DefaultSelection = loc:field
          End
        End
    end ! loc:eip = 0
      IF (loc:Checked = 'checked')
          p_web.SSV('RecordSelected',1)
      !            Access:RTNORDER.ClearKey(rtn:RecordNumberKey)
      !            rtn:RecordNumber = p_web.GSV('rtn:RecordNumber')
      !            IF (Access:RTNORDER.TryFetch(rtn:RecordNumberKey) = Level:Benign)
                p_web.SSV('rtn:CreditNoteRequestNumber',rtn:CreditNoteRequestNumber)
      
      ! Store Waybill Details Incase they choose to print
                p_web.SSV('locWaybillNumber',rtn:WaybillNumber)
                Access:WAYBILLS.Clearkey( way:WayBillNumberKey)
                way:WaybillNumber = rtn:WaybillNumber
                IF (Access:WAYBILLS.Tryfetch(way:WaybillNumberKey) = Level:Benign)
                    p_web.SSV('Waybill:Courier',way:Courier)
                    p_web.SSV('Waybill:FromType','TRA')
                    p_web.SSV('waybill:FromAccountNumber',way:FromAccount)
                    p_web.SSV('Waybill:ToType','TRA')
                    p_web.SSV('Waybill:ToAccountNumber',way:ToAccount)
                    p_web.SSV('Waybill:ConsignmentNumber',way:WayBillNumber)
                END
      !            END
        END
      
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::rtn:WaybillNumber
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::rtn:PartNumber
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::rtn:Description
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('RightJustify')&'"><13,10>'
          end ! loc:eip = 0
          do value::rtn:DateCreated
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('RightJustify')&'"><13,10>'
          end ! loc:eip = 0
          do value::rtn:DateOrdered
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('RightJustify')&'"><13,10>'
          end ! loc:eip = 0
          do value::rtn:DateReceived
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::locAcceptReject
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr ' & clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Idx = p_web.AddBrowseValue('BrowseReturnOrderTracking','RTNORDER',rtn:RecordNumberKey)
  TableQueue.Id[1] = rtn:RecordNumber

ClosingScripts  Routine
  data
Rtn_SecwinProcedureName         string(252)
  code
  If p_web.RequestAjax = 0
    do SetFormAction
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var btiBrowseReturnOrderTracking;if (btiBrowseReturnOrderTracking != 1){{var BrowseReturnOrderTracking=new browseTable(''BrowseReturnOrderTracking'','''&clip(loc:formname)&|
      ''','''&p_web._jsok('rtn:RecordNumber',Net:Parameter)&''',2,'''&clip(loc:divname)&''',1,1,1,'''&|
      clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&|
      p_web.Translate('Are you sure you want to delete this record?')&''','''&clip(loc:ActualSelection)&''','''&clip(loc:selectaction)&''','''&|
      clip(loc:formactiontarget)&''','''','&loc:popup&',0,'''','''',"' & clip(Rtn_SecwinProcedureName) & '");<13,10>'&|
      'BrowseReturnOrderTracking.setGreenBar('''&p_web.ColorWeb(p_web.Site.Style.BrowseHighlightColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOneColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseTwoColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOverColor)&''');<13,10>' &|
      'BrowseReturnOrderTracking.applyGreenBar();btiBrowseReturnOrderTracking=1};<13,10>' &|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseReturnOrderTracking')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseReturnOrderTracking')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseReturnOrderTracking')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseReturnOrderTracking')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(RTNORDER)
  p_web._CloseFile(WAYBILLS)
  p_web._CloseFile(RTNAWAIT)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(RTNORDER)
  Bind(rtn:Record)
  Clear(rtn:Record)
  NetWebSetSessionPics(p_web,RTNORDER)
  p_web._OpenFile(WAYBILLS)
  Bind(way:Record)
  NetWebSetSessionPics(p_web,WAYBILLS)
  p_web._OpenFile(RTNAWAIT)
  Bind(rta:Record)
  NetWebSetSessionPics(p_web,RTNAWAIT)

Children Routine
  if loc:selecting = 0
    If p_web.RequestAjax = 0
      do StartChildren
    Else
      do AjaxChildren
    End
  end

AjaxChildren  Routine
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue('rtn:RecordNumber',p_web.GetValue('rtn:RecordNumber'))
   p_web.DeleteValue('_Clicked')
   loc:found = 1
   do Children
! ----------------------------------------------------------------------------------------
CallRow  Routine
  data
loc:result  long
  code
  do OpenFilesB
  rtn:RecordNumber = p_web.GSV('rtn:RecordNumber')
  loc:result = p_web._GetFile(RTNORDER,rtn:RecordNumberKey)
  loc:eip = 1
  loc:viewstate = p_web.escape(p_web.Base64Encode(clip(rtn:RecordNumber)))
  do BrowseRow
  do SendPacket
  do ClosefilesB

! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(RTNORDER)
  Case upper(p_web.GetValue('_EIPClm'))
  Else
    case p_web.GetValue('_action_')
    of Net:InsertRecord
    orof Net:ChangeRecord
    orof Net:DeleteRecord
    orof Net:CopyRecord
      loc:eip = 0
      do CallBrowse
    end
  End
  do GotFocusBack
  p_web._CloseFile(RTNORDER)
! ----------------------------------------------------------------------------------------
value::rtn:WaybillNumber   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseReturnOrderTracking_rtn:WaybillNumber_'&rtn:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(rtn:WaybillNumber,'@n_10')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::rtn:PartNumber   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseReturnOrderTracking_rtn:PartNumber_'&rtn:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(rtn:PartNumber,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::rtn:Description   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseReturnOrderTracking_rtn:Description_'&rtn:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(rtn:Description,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::rtn:DateCreated   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseReturnOrderTracking_rtn:DateCreated_'&rtn:RecordNumber,'RightJustify',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(rtn:DateCreated,'@d17b')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::rtn:DateOrdered   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseReturnOrderTracking_rtn:DateOrdered_'&rtn:RecordNumber,'RightJustify',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(rtn:DateOrdered,'@d17b')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::rtn:DateReceived   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseReturnOrderTracking_rtn:DateReceived_'&rtn:RecordNumber,'RightJustify',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(rtn:DateReceived,'@d17b')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::locAcceptReject   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseReturnOrderTracking_locAcceptReject_'&rtn:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(locAcceptReject,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  p_web._OpenFile(WAYBILLS)
  p_web._OpenFile(RTNAWAIT)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(WAYBILLS)
  p_Web._CloseFile(RTNAWAIT)
     FilesOpened = False
  END
  return
!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
CheckForDuplicate  Routine
SetSelection  Routine
  loc:ActualSelection = loc:field
  p_web.SetSessionValue('rtn:RecordNumber',rtn:RecordNumber)

MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:ActualSelection = ''
    loc:ActualSelection = loc:DefaultSelection
    p_web.GetBrowseValue(loc:ActualSelection,Net:Web:SessionQueue) ! so children are primed with correct sessionValue
  End

  If loc:ActualSelection <> ''
    TableQueue.Kind = Net:RowTable
    get(TableQueue,TableQueue.Kind)
    if Errorcode() = 0
      TableQueue.Row = sub(TableQueue.Row,1,len(clip(TableQueue.Row))-1) &  ' data-nt-rows="'&loc:RowsHigh&'" data-nt-value="'&clip(loc:ActualSelection)&'"' & '>'
      Put(TableQueue)
    End
  End

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead class="'&p_web.combine(p_web.site.style.BrowseHeader,'')&'"><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot class="'&p_web.combine(p_web.site.style.BrowseFooter,)&'"><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody class="'&p_web.combine(p_web.site.style.BrowseBody,)&'"><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table><13,10>' & p_web.DivFooter(Net:NoSend) ! Table Div
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
BrowseAllModelStock  PROCEDURE  (NetWebServerWorker p_web)
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256),dim(Net:MaxKeyFields)
idx           String(Net:HashSize)
sub           Long
            End
stm:Part_Number:IsInvalid  Long
stm:Description:IsInvalid  Long
sto:Purchase_Cost:IsInvalid  Long
sto:Sale_Cost:IsInvalid  Long
sto:Quantity_Stock:IsInvalid  Long
sto:Shelf_Location:IsInvalid  Long
Change:IsInvalid  Long
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:DefaultSelection    String(Net:HashSize)
loc:ActualSelection     String(Net:HashSize)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
!loc:pagename            String(256)
loc:NavButtonPosition    Long
loc:UpdateButtonPosition Long
loc:SelectionMethod      Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorCase         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
loc:lookupdone          Long
loc:FormPopup           Long
ThisView            View(STOMODEL)
                      Project(stm:RecordNumber)
                      Project(stm:Part_Number)
                      Project(stm:Description)
                      Join(sto:Ref_Part_Description_Key,stm:Location,stm:Ref_Number,stm:Part_Number,stm:Description)
                        Project(sto:Purchase_Cost)
                        Project(sto:Sale_Cost)
                        Project(sto:Quantity_Stock)
                        Project(sto:Shelf_Location)
                      END
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
Loc:popup             Long
loc:poppedup          Long,static,thread
FilesOpened     Long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  GlobalErrors.SetProcedureName('BrowseAllModelStock')
  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseAllModelStock:NoForm')
      loc:NoForm = p_web.GetValue('BrowseAllModelStock:NoForm')
      loc:FormName = p_web.GetValue('BrowseAllModelStock:FormName')
    else
      loc:FormName = 'BrowseAllModelStock_frm'
    End
    p_web.SSV('BrowseAllModelStock:NoForm',loc:NoForm)
    p_web.SSV('BrowseAllModelStock:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseAllModelStock:NoForm')
    loc:FormName = p_web.GSV('BrowseAllModelStock:FormName')
  end
  case p_web.site.formpopups
  of -1 ; loc:FormPopup = Net:Page
  of 0 ; loc:FormPopup = Net:Page
  of 1 ; loc:FormPopup = Net:Popup
  End
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseAllModelStock') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseAllModelStock')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if 1 = 2
  elsif p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  elsif p_web.IfExistsValue('_EIPRow_')
    do CallRow
  elsif p_web.GetValue('_CallPopups') > 0
    do CallPopups
  elsif p_web.IfExistsValue('_GetSecwinSettings')
      p_web.SetValue('Secwin_' & 'BrowseAllModelStock' & 'AccessGroupsArray','1') 
      p_web.SetValue('Secwin_AccessWindowName','BrowseAllModelStock')
      
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallPopups  Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
loc:CallPopups   Long
loc:name         String(255)
  code
  if loc:poppedup = 0
    loc:CallPopups = p_web.GetValue('_CallPopups')
    if loc:CallPopups = 1 !and p_web.GetPreCall('BrowseAllModelStock') = 0! for = 1 include popup divs and scripts for this browse
      !p_web.AddPreCall('BrowseAllModelStock')
      p_web.DivHeader('popup_BrowseAllModelStock','nt-hidden')
      p_web.DivHeader('BrowseAllModelStock',p_web.combine(p_web.site.style.browsediv,))
      p_web.DivFooter()
      p_web.DivFooter()
      loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(400)&', modal: true, position: [''center'',15]'
      packet =  p_web.jQuery('#' & lower('popup_BrowseAllModelStock_div'),'dialog',loc:options) & |
      p_web.jQuery('#' & lower('popup_BrowseAllModelStock_div'),'removeClass','''nt-hidden''')
    End
    ! for > 0 add in form, EIP-lookups and browse object
    do Popups
    loc:popup = 1
    do ClosingScripts
    do SendPacket
    loc:poppedup = 1
  end

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web.DivHeader('BrowseAllModelStock',p_web.combine(p_web.site.style.browsediv,))
  if loc:ParentSilent = 0
    do GenerateBrowse
    if p_web.RequestAjax = 1
      p_web.Script('BrowseProducts.pcv();')
    end
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web.DivFooter()
  do Children
  do Popups
  if loc:poppedup = 0
    do ClosingScripts
  end
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(STOMODEL,stm:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'STM:PART_NUMBER') then p_web.SetValue('BrowseAllModelStock_sort','1')
    ElsIf (loc:vorder = 'STM:DESCRIPTION') then p_web.SetValue('BrowseAllModelStock_sort','2')
    ElsIf (loc:vorder = 'STO:PURCHASE_COST') then p_web.SetValue('BrowseAllModelStock_sort','3')
    ElsIf (loc:vorder = 'STO:SALE_COST') then p_web.SetValue('BrowseAllModelStock_sort','4')
    ElsIf (loc:vorder = 'STO:QUANTITY_STOCK') then p_web.SetValue('BrowseAllModelStock_sort','5')
    ElsIf (loc:vorder = 'STO:SHELF_LOCATION') then p_web.SetValue('BrowseAllModelStock_sort','6')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseAllModelStock:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseAllModelStock:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseAllModelStock:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseAllModelStock:LookupField')
    loc:selecting = 0
  End

Popups Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  If loc:popup = 0
  End
SetFormAction  Routine
  loc:formaction = 'FormStockControl'
  loc:formactiontarget = '_self'

GotFocusBack   Routine

GenerateBrowse Routine
  data
loc:viewoptions  Long
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  ! Set general Browse options
  loc:NavButtonPosition   = Net:Below
  loc:UpdateButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:PageLoad
  loc:Sorting          = Net:ServerSort
  if p_web.GetValue('_popup_') = 1
    loc:popup = 1
  end
  ! Set Locator Options
  loc:LocatorPosition  = Net:Above
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  p_web.ClearBrowse('BrowseAllModelStock')
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 15
  End
  loc:ActualSelection = ''
  !loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseAllModelStock_sort',net:DontEvaluate)
  p_web.SetSessionValue('BrowseAllModelStock_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(stm:Part_Number)','-UPPER(stm:Part_Number)')
    Loc:LocateField = 'stm:Part_Number'
    Loc:LocatorCase = 0
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(stm:Description)','-UPPER(stm:Description)')
    Loc:LocateField = 'stm:Description'
    Loc:LocatorCase = 0
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'sto:Purchase_Cost','-sto:Purchase_Cost')
    Loc:LocateField = 'sto:Purchase_Cost'
    Loc:LocatorCase = 0
  of 4
    loc:vorder = Choose(Loc:SortDirection=1,'sto:Sale_Cost','-sto:Sale_Cost')
    Loc:LocateField = 'sto:Sale_Cost'
    Loc:LocatorCase = 0
  of 5
    loc:vorder = Choose(Loc:SortDirection=1,'sto:Quantity_Stock','-sto:Quantity_Stock')
    Loc:LocateField = 'sto:Quantity_Stock'
    Loc:LocatorCase = 0
  of 6
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(sto:Shelf_Location)','-UPPER(sto:Shelf_Location)')
    Loc:LocateField = 'sto:Shelf_Location'
    Loc:LocatorCase = 0
  of 7
    Loc:LocateField = ''
    Loc:LocatorCase = 0
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('stm:Part_Number')
    loc:SortHeader = p_web.Translate('Part Number')
    p_web.SetSessionValue('BrowseAllModelStock_LocatorPic','@s30')
  Of upper('stm:Description')
    loc:SortHeader = p_web.Translate('Description')
    p_web.SetSessionValue('BrowseAllModelStock_LocatorPic','@s30')
  Of upper('sto:Purchase_Cost')
    loc:SortHeader = p_web.Translate('In Warr')
    p_web.SetSessionValue('BrowseAllModelStock_LocatorPic','@n14.2')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
  Of upper('sto:Sale_Cost')
    loc:SortHeader = p_web.Translate('Out Warr')
    p_web.SetSessionValue('BrowseAllModelStock_LocatorPic','@n14.2')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
  Of upper('sto:Quantity_Stock')
    loc:SortHeader = p_web.Translate('Qty In Stock')
    p_web.SetSessionValue('BrowseAllModelStock_LocatorPic','@N_8')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
  Of upper('sto:Shelf_Location')
    loc:SortHeader = p_web.Translate('Shelf Location')
    p_web.SetSessionValue('BrowseAllModelStock_LocatorPic','@s30')
  End
  If loc:selecting = 1
    p_web.GetSettings(p_web.GetSessionValue('Push1'))
    loc:selectaction = p_web.FormSettings.ParentPage !p_web.GetSessionValue('BrowseAllModelStock:LookupFrom')
  End!Else
  do SendPacket
  do SetFormAction
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseAllModelStock:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseAllModelStock:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseAllModelStock:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="STOMODEL"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="stm:RecordNumberKey"></input><13,10>'
  end
  case p_web.GetValue('refresh') ; of 'next' orof 'previous' orof 'first' orof 'last'
    p_web.Script('try{{BrowseAllModelStock.restoreFocus();} catch(err){{};')
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseAllModelStock',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      case loc:locatortype
      of Net:Position
      orof Net:Date
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
      of Net:Contains
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
      of Net:Begins
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
      End
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
      Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
      Case Loc:LocatorType
      of Net:Date
        packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator2BrowseAllModelStock','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('BrowseAllModelStock_LocatorPic'),,,'onchange="BrowseAllModelStock.locate(''Locator2BrowseAllModelStock'',this.value);"',,0,,,) & '</div></td>'
      Else
        packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator2BrowseAllModelStock',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="BrowseAllModelStock.locate(''Locator2BrowseAllModelStock'',this.value);" ',,,,,,) & '</td>'
      End
      If loc:LocatorSearchButton or loc:LocatorClearButton
        packet = clip(packet) & '<td><div id="BrowseAllModelStock_locate_a" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
        If loc:LocatorSearchButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
        End
        If loc:LocatorClearButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseAllModelStock.cl(''BrowseAllModelStock'');')
        End
        packet = clip(packet) & '</div><13,10></td><13,10>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
      If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'BrowseAllModelStock_locate_a','buttonset',loc:options)
      End
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & p_web.DivHeader('BrowseAllModelStock_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'','sortable')&'" id="BrowseAllModelStock_tbl">'
  Else
    packet = clip(packet) & p_web.DivHeader('BrowseAllModelStock_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'')&'" id="BrowseAllModelStock_tbl">'
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'1','BrowseAllModelStock',p_web.Translate('Part Number'),'Click here to sort by Part Number',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'2','BrowseAllModelStock',p_web.Translate('Description'),'Click here to sort by Description',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'3','BrowseAllModelStock',p_web.Translate('In Warr'),'Click here to sort by Purchase Cost',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'4','BrowseAllModelStock',p_web.Translate('Out Warr'),'Click here to sort by Sale Cost',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'5','BrowseAllModelStock',p_web.Translate('Qty In Stock'),'Click here to sort by Quantity In Stock',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'6','BrowseAllModelStock',p_web.Translate('Shelf Location'),'Click here to sort by Shelf Location',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'7','BrowseAllModelStock',p_web.Translate(),,,,,,,0,loc:Sorting,'Button')
          do AddPacket
          loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,15,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  If Instring('stm:recordnumber',lower(loc:vorder),1,1) = 0 !and STOMODEL{prop:SQLDriver} = 1
    loc:vorder = Choose(loc:vorder='','stm:RecordNumber',clip(loc:vorder) & ',' & 'stm:RecordNumber')
  End
  Loc:Selected = Choose(p_web.IfExistsValue('stm:RecordNumber'),p_web.GetValue('stm:RecordNumber'),p_web.GetSessionValue('stm:RecordNumber'))
  ThisView{prop:order} = p_web.CleanFilter(ThisView,clip(loc:vorder))
      loc:FilterWas = 'Upper(stm:Model_Number) = Upper(''' & p_web.GSV('locModelNumber') & ''') AND Upper(stm:Location) = Upper(''' & p_web.GSV('Default:SiteLocation') & ''')'
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseAllModelStock',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseAllModelStock_Filter')
    p_web.SetSessionValue('BrowseAllModelStock_FirstValue','')
    p_web.SetSessionValue('BrowseAllModelStock_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,STOMODEL,stm:RecordNumberKey,loc:PageRows,'BrowseAllModelStock',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,loc:ViewOptions,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled,Loc:LocatorCase)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or Loc:LocatorType = Net:Date or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position or loc:LocatorType = Net:Date
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If STOMODEL{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(STOMODEL,loc:firstvalue)
              Reset(ThisView,STOMODEL)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If STOMODEL{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(STOMODEL,loc:lastvalue)
            Reset(ThisView,STOMODEL)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(stm:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    p_web.SSV('stm:Ref_Number','')
    p_web.SSV('ShowBrowseModelStock',0)
    packet = clip(packet) & '<tr><13,10><td class="'&p_web.combine(p_web.site.style.BrowseEmpty,)&'">'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:NavButtonPosition=Net:Above or (loc:NavButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseAllModelStock_nav_a" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,' nt-left nt-margin-right',)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseAllModelStock.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseAllModelStock.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseAllModelStock.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseAllModelStock.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseAllModelStock_nav_a','buttonset',loc:options)
          End
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:UpdateButtonPosition=Net:Above or (loc:UpdateButtonPosition=Net:Both and loc:found > 0))
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseAllModelStock',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseAllModelStock_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseAllModelStock_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          case loc:locatortype
          of Net:Position
          orof Net:Date
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
          of Net:Contains
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
          of Net:Begins
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
          End
          packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
          Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
          Case Loc:LocatorType
          of Net:Date
            packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator1BrowseAllModelStock','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('BrowseAllModelStock_LocatorPic'),,,'onchange="BrowseAllModelStock.locate(''Locator1BrowseAllModelStock'',this.value);"',,0,,,) & '</div></td>'
          Else
            packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator1BrowseAllModelStock',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="BrowseAllModelStock.locate(''Locator1BrowseAllModelStock'',this.value);" ',,,,,,) & '</td>'
          End
          If loc:LocatorSearchButton or loc:LocatorClearButton
            packet = clip(packet) & '<td><div id="BrowseAllModelStock_locate_b" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
            If loc:LocatorSearchButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
            End
            If loc:LocatorClearButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseAllModelStock.cl(''BrowseAllModelStock'');')
            End
            packet = clip(packet) & '</div><13,10></td><13,10>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
          If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseAllModelStock_locate_b','buttonset',loc:options)
          End
      End
    End
  End
  p_web.SetSessionValue('BrowseAllModelStock_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseAllModelStock_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:NavButtonPosition=Net:Below or loc:NavButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseAllModelStock_nav_b" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,' nt-left nt-margin-right',)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseAllModelStock.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseAllModelStock.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseAllModelStock.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseAllModelStock.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseAllModelStock_nav_b','buttonset',loc:options)
          End
        end
        do SendPacket
  End
  If loc:UpdateButtonPosition=Net:Below or loc:UpdateButtonPosition=Net:Both
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
  data
loc:rowclick  string(1024)
  code
    loc:field = p_web.AddBrowseValue('BrowseAllModelStock','STOMODEL',stm:RecordNumberKey) !stm:RecordNumber
    p_web._thisrow = p_web._nocolon('stm:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if Loc:LocatorValue <> '' and loc:ActualSelection = ''
        loc:checked = 'checked'
        do SetSelection
      elsif loc:ActualSelection = '' and stm:RecordNumber = p_web.GetValue('stm:RecordNumber')
         loc:checked = 'checked'
         do SetSelection
      elsif loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseAllModelStock:LookupField')) = stm:RecordNumber and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      else
        loc:checked = Choose((stm:RecordNumber = loc:selected) and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      end
      If(loc:SelectionMethod  = Net:Radio)
        loc:RowStyle = 'class="' & p_web.combine(p_web.site.style.browserow,) &'"'
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'data-nt-id="'& p_web.AddBrowseValue('BrowseAllModelStock','STOMODEL',stm:RecordNumberKey) &'" onclick="'
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If STOMODEL{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(STOMODEL)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            loc:DefaultSelection = loc:field
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If STOMODEL{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(STOMODEL)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            loc:DefaultSelection = loc:field
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'>'&p_web.CreateInput('radio','_bidv_',clip(loc:field),,loc:checked,'')&'</td>'&CRLF
          If loc:DefaultSelection = ''
            loc:DefaultSelection = loc:field
          End
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          If loc:DefaultSelection = '' or loc:direction < 0
            loc:DefaultSelection = loc:field
          End
        End
    end ! loc:eip = 0
      IF (loc:Checked = 'checked')
          p_web.SSV('sto:Ref_Number',stm:Ref_Number)
      END ! IF (loc:Checked = 'checked')
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::stm:Part_Number
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::stm:Description
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('RightJustify')&'"><13,10>'
          end ! loc:eip = 0
          do value::sto:Purchase_Cost
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('RightJustify')&'"><13,10>'
          end ! loc:eip = 0
          do value::sto:Sale_Cost
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('RightJustify')&'"><13,10>'
          end ! loc:eip = 0
          do value::sto:Quantity_Stock
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('RightJustify')&'"><13,10>'
          end ! loc:eip = 0
          do value::sto:Shelf_Location
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::Change
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr ' & clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Idx = p_web.AddBrowseValue('BrowseAllModelStock','STOMODEL',stm:RecordNumberKey)
  TableQueue.Id[1] = stm:RecordNumber

ClosingScripts  Routine
  data
Rtn_SecwinProcedureName         string(252)
  code
  If p_web.RequestAjax = 0
    do SetFormAction
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var btiBrowseAllModelStock;if (btiBrowseAllModelStock != 1){{var BrowseAllModelStock=new browseTable(''BrowseAllModelStock'','''&clip(loc:formname)&|
      ''','''&p_web._jsok('stm:RecordNumber',Net:Parameter)&''',2,'''&clip(loc:divname)&''',1,1,1,'''&|
      clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&|
      p_web.Translate('Are you sure you want to delete this record?')&''','''&clip(loc:ActualSelection)&''','''&clip(loc:selectaction)&''','''&|
      clip(loc:formactiontarget)&''',''FormStockControl'','&loc:popup&',0,'''','''',"' & clip(Rtn_SecwinProcedureName) & '");<13,10>'&|
      'BrowseAllModelStock.setGreenBar('''&p_web.ColorWeb(p_web.Site.Style.BrowseHighlightColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOneColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseTwoColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOverColor)&''');<13,10>' &|
      'BrowseAllModelStock.applyGreenBar();btiBrowseAllModelStock=1};<13,10>' &|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseAllModelStock')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseAllModelStock')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseAllModelStock')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseAllModelStock')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(STOMODEL)
  p_web._CloseFile(STOCK)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(STOMODEL)
  Bind(stm:Record)
  Clear(stm:Record)
  NetWebSetSessionPics(p_web,STOMODEL)
  p_web._OpenFile(STOCK)
  Bind(sto:Record)
  NetWebSetSessionPics(p_web,STOCK)

Children Routine
  if loc:selecting = 0
    If p_web.RequestAjax = 0
      do StartChildren
    Else
      do AjaxChildren
    End
  end

AjaxChildren  Routine
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue('stm:RecordNumber',p_web.GetValue('stm:RecordNumber'))
  Access:STOMODEL.ClearKey(stm:RecordNumberKey)
  stm:RecordNumber = p_web.GSV('stm:RecordNumber')
  IF (Access:STOMODEL.TryFetch(stm:RecordNumberKey) = Level:Benign)
      p_web.SSV('sto:Ref_Number',stm:Ref_Number)
  END
  
   p_web.DeleteValue('_Clicked')
   loc:found = 1
   do Children
! ----------------------------------------------------------------------------------------
CallRow  Routine
  data
loc:result  long
  code
  do OpenFilesB
  stm:RecordNumber = p_web.GSV('stm:RecordNumber')
  loc:result = p_web._GetFile(STOMODEL,stm:RecordNumberKey)
  loc:eip = 1
  loc:viewstate = p_web.escape(p_web.Base64Encode(clip(stm:RecordNumber)))
  do BrowseRow
  do SendPacket
  do ClosefilesB

! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(STOMODEL)
  Case upper(p_web.GetValue('_EIPClm'))
  of upper('Change')
    do Validate::Change
  Else
    case p_web.GetValue('_action_')
    of Net:InsertRecord
    orof Net:ChangeRecord
    orof Net:DeleteRecord
    orof Net:CopyRecord
      loc:eip = 0
      do CallBrowse
    end
  End
  do GotFocusBack
  p_web._CloseFile(STOMODEL)
! ----------------------------------------------------------------------------------------
value::stm:Part_Number   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseAllModelStock_stm:Part_Number_'&stm:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(stm:Part_Number,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::stm:Description   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseAllModelStock_stm:Description_'&stm:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(stm:Description,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sto:Purchase_Cost   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseAllModelStock_sto:Purchase_Cost_'&stm:RecordNumber,'RightJustify',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(sto:Purchase_Cost,'@n14.2')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sto:Sale_Cost   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseAllModelStock_sto:Sale_Cost_'&stm:RecordNumber,'RightJustify',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(sto:Sale_Cost,'@n14.2')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sto:Quantity_Stock   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseAllModelStock_sto:Quantity_Stock_'&stm:RecordNumber,'RightJustify',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(sto:Quantity_Stock,'@N_8')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sto:Shelf_Location   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseAllModelStock_sto:Shelf_Location_'&stm:RecordNumber,'RightJustify',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(sto:Shelf_Location,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
Validate::Change  Routine
  data
loc:was                    Any
loc:result                 Long
loc:ok                     Long
loc:lookupdone             Long
  code
  do OpenFilesB
  loc:ViewState = p_web.GetValue('ViewState')
  stm:RecordNumber = p_web.Base64Decode(p_web._Unescape(loc:ViewState,Net:NoPlus))
  loc:result = p_web._GetFile(STOMODEL,stm:RecordNumberKey)
  p_web.FileToSessionQueue(STOMODEL)
  do ClosefilesB
! ----------------------------------------------------------------------------------------
value::Change   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseAllModelStock_Change_'&stm:RecordNumber,,net:crc,,loc:extra)
      loc:disabled = ''
      packet = clip(packet) & p_web.CreateButton('button','Edit','Edit',p_web.combine(Choose('Edit' <> '',p_web.site.style.BrowseOtherButtonWithText,p_web.site.style.BrowseOtherButtonWithOutText),'SmallButton'),loc:formname,,,p_web.WindowOpen(clip('FormStockControl?sto__ref_number=' & stm:Ref_Number & '&Change_btn=Change&')&''&'&PressedButton=change_btn','_self'),,loc:disabled,,,,,,0,,,,'nt-left') !e
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END
  return
!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
CheckForDuplicate  Routine
  If loc:invalid <> '' then exit. ! no need to check, record is already invalid
  If Duplicate(stm:Model_Number_Key)
    loc:Invalid = 'stm:Ref_Number'
    loc:Alert = clip(p_web.site.DuplicateText) & ' Model_Number_Key --> stm:Ref_Number, stm:Manufacturer, stm:Model_Number'
  End
SetSelection  Routine
  loc:ActualSelection = loc:field
  p_web.SetSessionValue('stm:RecordNumber',stm:RecordNumber)

MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:ActualSelection = ''
    loc:ActualSelection = loc:DefaultSelection
    p_web.GetBrowseValue(loc:ActualSelection,Net:Web:SessionQueue) ! so children are primed with correct sessionValue
  End

  If loc:ActualSelection <> ''
    TableQueue.Kind = Net:RowTable
    get(TableQueue,TableQueue.Kind)
    if Errorcode() = 0
      TableQueue.Row = sub(TableQueue.Row,1,len(clip(TableQueue.Row))-1) &  ' data-nt-rows="'&loc:RowsHigh&'" data-nt-value="'&clip(loc:ActualSelection)&'"' & '>'
      Put(TableQueue)
    End
  End

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead class="'&p_web.combine(p_web.site.style.BrowseHeader,'')&'"><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot class="'&p_web.combine(p_web.site.style.BrowseFooter,)&'"><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody class="'&p_web.combine(p_web.site.style.BrowseBody,)&'"><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table><13,10>' & p_web.DivFooter(Net:NoSend) ! Table Div
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
SaveSessionVars  ROUTINE
RestoreSessionVars  ROUTINE
BrowseRetailSalesItems PROCEDURE  (NetWebServerWorker p_web)
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256),dim(Net:MaxKeyFields)
idx           String(Net:HashSize)
sub           Long
            End
res:Part_Number:IsInvalid  Long
res:Description:IsInvalid  Long
res:Quantity:IsInvalid  Long
res:Purchase_Order_Number:IsInvalid  Long
res:Previous_Sale_Number:IsInvalid  Long
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:DefaultSelection    String(Net:HashSize)
loc:ActualSelection     String(Net:HashSize)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
!loc:pagename            String(256)
loc:NavButtonPosition    Long
loc:UpdateButtonPosition Long
loc:SelectionMethod      Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorCase         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
loc:lookupdone          Long
loc:FormPopup           Long
ThisView            View(RETSTOCK)
                      Project(res:Record_Number)
                      Project(res:Part_Number)
                      Project(res:Description)
                      Project(res:Quantity)
                      Project(res:Purchase_Order_Number)
                      Project(res:Previous_Sale_Number)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
Loc:popup             Long
loc:poppedup          Long,static,thread
FilesOpened     Long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  GlobalErrors.SetProcedureName('BrowseRetailSalesItems')
  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseRetailSalesItems:NoForm')
      loc:NoForm = p_web.GetValue('BrowseRetailSalesItems:NoForm')
      loc:FormName = p_web.GetValue('BrowseRetailSalesItems:FormName')
    else
      loc:FormName = 'BrowseRetailSalesItems_frm'
    End
    p_web.SSV('BrowseRetailSalesItems:NoForm',loc:NoForm)
    p_web.SSV('BrowseRetailSalesItems:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseRetailSalesItems:NoForm')
    loc:FormName = p_web.GSV('BrowseRetailSalesItems:FormName')
  end
  case p_web.site.formpopups
  of -1 ; loc:FormPopup = Net:Page
  of 0 ; loc:FormPopup = Net:Page
  of 1 ; loc:FormPopup = Net:Popup
  End
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseRetailSalesItems') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseRetailSalesItems')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if 1 = 2
  elsif p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  elsif p_web.IfExistsValue('_EIPRow_')
    do CallRow
  elsif p_web.GetValue('_CallPopups') > 0
    do CallPopups
  elsif p_web.IfExistsValue('_GetSecwinSettings')
      p_web.SetValue('Secwin_' & 'BrowseRetailSalesItems' & 'AccessGroupsArray','1') 
      p_web.SetValue('Secwin_AccessWindowName','BrowseRetailSalesItems')
      
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallPopups  Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
loc:CallPopups   Long
loc:name         String(255)
  code
  if loc:poppedup = 0
    loc:CallPopups = p_web.GetValue('_CallPopups')
    if loc:CallPopups = 1 !and p_web.GetPreCall('BrowseRetailSalesItems') = 0! for = 1 include popup divs and scripts for this browse
      !p_web.AddPreCall('BrowseRetailSalesItems')
      p_web.DivHeader('popup_BrowseRetailSalesItems','nt-hidden')
      p_web.DivHeader('BrowseRetailSalesItems',p_web.combine(p_web.site.style.browsediv,'adiv'))
      p_web.DivFooter()
      p_web.DivFooter()
      loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(400)&', modal: true, position: [''center'',15]'
      packet =  p_web.jQuery('#' & lower('popup_BrowseRetailSalesItems_div'),'dialog',loc:options) & |
      p_web.jQuery('#' & lower('popup_BrowseRetailSalesItems_div'),'removeClass','''nt-hidden''')
    End
    ! for > 0 add in form, EIP-lookups and browse object
    do Popups
    loc:popup = 1
    do ClosingScripts
    do SendPacket
    loc:poppedup = 1
  end

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web.DivHeader('BrowseRetailSalesItems',p_web.combine(p_web.site.style.browsediv,'adiv'))
  if loc:ParentSilent = 0
    do GenerateBrowse
    if p_web.RequestAjax = 1
      p_web.Script('BrowseProducts.pcv();')
    end
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web.DivFooter()
  do Children
  do Popups
  if loc:poppedup = 0
    do ClosingScripts
  end
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(RETSTOCK,res:Record_Number_Key,loc:vorder)
    If False
    ElsIf (loc:vorder = 'RES:PART_NUMBER') then p_web.SetValue('BrowseRetailSalesItems_sort','1')
    ElsIf (loc:vorder = 'RES:DESCRIPTION') then p_web.SetValue('BrowseRetailSalesItems_sort','2')
    ElsIf (loc:vorder = 'RES:QUANTITY') then p_web.SetValue('BrowseRetailSalesItems_sort','3')
    ElsIf (loc:vorder = 'RES:PURCHASE_ORDER_NUMBER') then p_web.SetValue('BrowseRetailSalesItems_sort','4')
    ElsIf (loc:vorder = 'RES:PREVIOUS_SALE_NUMBER') then p_web.SetValue('BrowseRetailSalesItems_sort','5')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseRetailSalesItems:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseRetailSalesItems:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseRetailSalesItems:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseRetailSalesItems:LookupField')
    loc:selecting = 0
  End

Popups Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  If loc:popup = 0
  End
SetFormAction  Routine
  loc:formaction = 'BrowseRetailSalesItems'

GotFocusBack   Routine

GenerateBrowse Routine
  data
loc:viewoptions  Long
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  ! Set general Browse options
  loc:NavButtonPosition   = Net:Below
  loc:UpdateButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:PageLoad
  loc:Sorting          = Net:ServerSort
  if p_web.GetValue('_popup_') = 1
    loc:popup = 1
  end
  ! Set Locator Options
  loc:LocatorPosition  = Net:Above
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  p_web.ClearBrowse('BrowseRetailSalesItems')
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 15
  End
  loc:ActualSelection = ''
  !loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseRetailSalesItems_sort',net:DontEvaluate)
  If loc:vordernumber = 0
    loc:vordernumber = 1
  End
  p_web.SetSessionValue('BrowseRetailSalesItems_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(res:Part_Number)','-UPPER(res:Part_Number)')
    Loc:LocateField = 'res:Part_Number'
    Loc:LocatorCase = 0
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(res:Description)','-UPPER(res:Description)')
    Loc:LocateField = 'res:Description'
    Loc:LocatorCase = 0
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'res:Quantity','-res:Quantity')
    Loc:LocateField = 'res:Quantity'
    Loc:LocatorCase = 0
  of 4
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(res:Purchase_Order_Number)','-UPPER(res:Purchase_Order_Number)')
    Loc:LocateField = 'res:Purchase_Order_Number'
    Loc:LocatorCase = 0
  of 5
    loc:vorder = Choose(Loc:SortDirection=1,'res:Previous_Sale_Number','-res:Previous_Sale_Number')
    Loc:LocateField = 'res:Previous_Sale_Number'
    Loc:LocatorCase = 0
  end
  if loc:vorder = ''
    loc:vorder = '+res:Ref_Number,+UPPER(res:Despatched),+UPPER(res:Part_Number)'
  end
  If False ! add range fields to sort order
  ElsIf (p_web.GSV('locRetailStockType') = 'B')
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('res:Part_Number')
    loc:SortHeader = p_web.Translate('Part Number')
    p_web.SetSessionValue('BrowseRetailSalesItems_LocatorPic','@s30')
  Of upper('res:Description')
    loc:SortHeader = p_web.Translate('Description')
    p_web.SetSessionValue('BrowseRetailSalesItems_LocatorPic','@s30')
  Of upper('res:Quantity')
    loc:SortHeader = p_web.Translate('Total')
    p_web.SetSessionValue('BrowseRetailSalesItems_LocatorPic','@n_8')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
  Of upper('res:Purchase_Order_Number')
    loc:SortHeader = p_web.Translate('Purchase Order Number')
    p_web.SetSessionValue('BrowseRetailSalesItems_LocatorPic','@s30')
  Of upper('res:Previous_Sale_Number')
    loc:SortHeader = p_web.Translate('Sales Number')
    p_web.SetSessionValue('BrowseRetailSalesItems_LocatorPic','@n_8')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
  End
  If loc:selecting = 1
    p_web.GetSettings(p_web.GetSessionValue('Push1'))
    loc:selectaction = p_web.FormSettings.ParentPage !p_web.GetSessionValue('BrowseRetailSalesItems:LookupFrom')
  End!Else
  do SendPacket
  do SetFormAction
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseRetailSalesItems:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseRetailSalesItems:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseRetailSalesItems:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="RETSTOCK"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="res:Record_Number_Key"></input><13,10>'
  end
  case p_web.GetValue('refresh') ; of 'next' orof 'previous' orof 'first' orof 'last'
    p_web.Script('try{{BrowseRetailSalesItems.restoreFocus();} catch(err){{};')
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseRetailSalesItems',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      case loc:locatortype
      of Net:Position
      orof Net:Date
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
      of Net:Contains
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
      of Net:Begins
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
      End
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,'Locator')&'"><tr>' &|
      Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
      Case Loc:LocatorType
      of Net:Date
        packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator2BrowseRetailSalesItems','',p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,, p_web.GSV('BrowseRetailSalesItems_LocatorPic'),,,'onchange="BrowseRetailSalesItems.locate(''Locator2BrowseRetailSalesItems'',this.value);"',,0,,,) & '</div></td>'
      Else
        packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator2BrowseRetailSalesItems',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,'size="30" onchange="BrowseRetailSalesItems.locate(''Locator2BrowseRetailSalesItems'',this.value);" ',,,,,,) & '</td>'
      End
      If loc:LocatorSearchButton or loc:LocatorClearButton
        packet = clip(packet) & '<td><div id="BrowseRetailSalesItems_locate_a" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
        If loc:LocatorSearchButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
        End
        If loc:LocatorClearButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseRetailSalesItems.cl(''BrowseRetailSalesItems'');')
        End
        packet = clip(packet) & '</div><13,10></td><13,10>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
      If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'BrowseRetailSalesItems_locate_a','buttonset',loc:options)
      End
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & p_web.DivHeader('BrowseRetailSalesItems_table',p_web.Combine(p_web.site.style.BrowseTableDiv,''),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,,'sortable')&'" id="BrowseRetailSalesItems_tbl">'
  Else
    packet = clip(packet) & p_web.DivHeader('BrowseRetailSalesItems_table',p_web.Combine(p_web.site.style.BrowseTableDiv,''),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,)&'" id="BrowseRetailSalesItems_tbl">'
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'1','BrowseRetailSalesItems',p_web.Translate('Part Number'),'Click here to sort by Part Number',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'2','BrowseRetailSalesItems',p_web.Translate('Description'),'Click here to sort by Description',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'3','BrowseRetailSalesItems',p_web.Translate('Total'),'Click here to sort by Quantity',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'4','BrowseRetailSalesItems',p_web.Translate('Purchase Order Number'),'Click here to sort by Purchase Order Number',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
  If (p_web.GSV('locRetailStockType') = 'B') AND  true ! [A]
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'5','BrowseRetailSalesItems',p_web.Translate('Sales Number'),'Click here to sort by Sales Number',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
  End ! Field condition [A]
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,15,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  If Instring('res:record_number',lower(loc:vorder),1,1) = 0 !and RETSTOCK{prop:SQLDriver} = 1
    loc:vorder = Choose(loc:vorder='','res:Record_Number',clip(loc:vorder) & ',' & 'res:Record_Number')
  End
  Loc:Selected = Choose(p_web.IfExistsValue('res:Record_Number'),p_web.GetValue('res:Record_Number'),p_web.GetSessionValue('res:Record_Number'))
  ThisView{prop:order} = p_web.CleanFilter(ThisView,clip(loc:vorder))
  If False  ! Generate Filter
  ElsIf (p_web.GSV('locRetailStockType') = 'B')
      loc:FilterWas = 'Upper(res:Ref_Number) = Upper(''' & p_web.GSV('ret:Ref_Number') & ''') AND (Upper(res:Despatched) = ''PEN'' Or Upper(res:Despatched) = ''CAN'' or Upper(res:Despatched) = ''OLD'')'
  Else
        loc:FilterWas = 'Upper(res:Ref_Number) = Upper(''' & p_web.GSV('ret:Ref_Number') & ''') AND Upper(res:Despatched) = ''YES'''
  End
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseRetailSalesItems',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseRetailSalesItems_Filter')
    p_web.SetSessionValue('BrowseRetailSalesItems_FirstValue','')
    p_web.SetSessionValue('BrowseRetailSalesItems_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,RETSTOCK,res:Record_Number_Key,loc:PageRows,'BrowseRetailSalesItems',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,loc:ViewOptions,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled,Loc:LocatorCase)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or Loc:LocatorType = Net:Date or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position or loc:LocatorType = Net:Date
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If RETSTOCK{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(RETSTOCK,loc:firstvalue)
              Reset(ThisView,RETSTOCK)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If RETSTOCK{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(RETSTOCK,loc:lastvalue)
            Reset(ThisView,RETSTOCK)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(res:Record_Number)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td class="'&p_web.combine(p_web.site.style.BrowseEmpty,)&'">'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:NavButtonPosition=Net:Above or (loc:NavButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseRetailSalesItems_nav_a" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,' nt-left nt-margin-right',)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseRetailSalesItems.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseRetailSalesItems.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseRetailSalesItems.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseRetailSalesItems.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseRetailSalesItems_nav_a','buttonset',loc:options)
          End
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:UpdateButtonPosition=Net:Above or (loc:UpdateButtonPosition=Net:Both and loc:found > 0))
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseRetailSalesItems',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseRetailSalesItems_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseRetailSalesItems_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          case loc:locatortype
          of Net:Position
          orof Net:Date
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
          of Net:Contains
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
          of Net:Begins
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
          End
          packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,'Locator')&'"><tr>' &|
          Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
          Case Loc:LocatorType
          of Net:Date
            packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator1BrowseRetailSalesItems','',p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,, p_web.GSV('BrowseRetailSalesItems_LocatorPic'),,,'onchange="BrowseRetailSalesItems.locate(''Locator1BrowseRetailSalesItems'',this.value);"',,0,,,) & '</div></td>'
          Else
            packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator1BrowseRetailSalesItems',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,'size="30" onchange="BrowseRetailSalesItems.locate(''Locator1BrowseRetailSalesItems'',this.value);" ',,,,,,) & '</td>'
          End
          If loc:LocatorSearchButton or loc:LocatorClearButton
            packet = clip(packet) & '<td><div id="BrowseRetailSalesItems_locate_b" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
            If loc:LocatorSearchButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
            End
            If loc:LocatorClearButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseRetailSalesItems.cl(''BrowseRetailSalesItems'');')
            End
            packet = clip(packet) & '</div><13,10></td><13,10>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
          If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseRetailSalesItems_locate_b','buttonset',loc:options)
          End
      End
    End
  End
  p_web.SetSessionValue('BrowseRetailSalesItems_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseRetailSalesItems_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:NavButtonPosition=Net:Below or loc:NavButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseRetailSalesItems_nav_b" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,' nt-left nt-margin-right',)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseRetailSalesItems.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseRetailSalesItems.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseRetailSalesItems.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseRetailSalesItems.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseRetailSalesItems_nav_b','buttonset',loc:options)
          End
        end
        do SendPacket
  End
  If loc:UpdateButtonPosition=Net:Below or loc:UpdateButtonPosition=Net:Both
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
  data
loc:rowclick  string(1024)
  code
    loc:field = p_web.AddBrowseValue('BrowseRetailSalesItems','RETSTOCK',res:Record_Number_Key) !res:Record_Number
    p_web._thisrow = p_web._nocolon('res:Record_Number')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if Loc:LocatorValue <> '' and loc:ActualSelection = ''
        loc:checked = 'checked'
        do SetSelection
      elsif loc:ActualSelection = '' and res:Record_Number = p_web.GetValue('res:Record_Number')
         loc:checked = 'checked'
         do SetSelection
      elsif loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseRetailSalesItems:LookupField')) = res:Record_Number and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      else
        loc:checked = Choose((res:Record_Number = loc:selected) and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      end
      If(loc:SelectionMethod  = Net:Radio)
        loc:RowStyle = 'class="' & p_web.combine(p_web.site.style.browserow,) &'"'
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'data-nt-id="'& p_web.AddBrowseValue('BrowseRetailSalesItems','RETSTOCK',res:Record_Number_Key) &'" onclick="'
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If RETSTOCK{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(RETSTOCK)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            loc:DefaultSelection = loc:field
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If RETSTOCK{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(RETSTOCK)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            loc:DefaultSelection = loc:field
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'>'&p_web.CreateInput('radio','_bidv_',clip(loc:field),,loc:checked,'')&'</td>'&CRLF
          If loc:DefaultSelection = ''
            loc:DefaultSelection = loc:field
          End
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          If loc:DefaultSelection = '' or loc:direction < 0
            loc:DefaultSelection = loc:field
          End
        End
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::res:Part_Number
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::res:Description
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('RightJustify')&'"><13,10>'
          end ! loc:eip = 0
          do value::res:Quantity
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::res:Purchase_Order_Number
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      If (p_web.GSV('locRetailStockType') = 'B') AND  true
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('RightJustify')&'"><13,10>'
          end ! loc:eip = 0
          do value::res:Previous_Sale_Number
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      End ! Field Condition
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr ' & clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Idx = p_web.AddBrowseValue('BrowseRetailSalesItems','RETSTOCK',res:Record_Number_Key)
  TableQueue.Id[1] = res:Record_Number

ClosingScripts  Routine
  data
Rtn_SecwinProcedureName         string(252)
  code
  If p_web.RequestAjax = 0
    do SetFormAction
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var btiBrowseRetailSalesItems;if (btiBrowseRetailSalesItems != 1){{var BrowseRetailSalesItems=new browseTable(''BrowseRetailSalesItems'','''&clip(loc:formname)&|
      ''','''&p_web._jsok('res:Record_Number',Net:Parameter)&''',2,'''&clip(loc:divname)&''',1,1,1,'''&|
      clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&|
      p_web.Translate('Are you sure you want to delete this record?')&''','''&clip(loc:ActualSelection)&''','''&clip(loc:selectaction)&''','''&|
      clip(loc:formactiontarget)&''','''','&loc:popup&',0,'''','''',"' & clip(Rtn_SecwinProcedureName) & '");<13,10>'&|
      'BrowseRetailSalesItems.setGreenBar('''&p_web.ColorWeb(p_web.Site.Style.BrowseHighlightColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOneColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseTwoColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOverColor)&''');<13,10>' &|
      'BrowseRetailSalesItems.applyGreenBar();btiBrowseRetailSalesItems=1};<13,10>' &|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseRetailSalesItems')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseRetailSalesItems')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseRetailSalesItems')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseRetailSalesItems')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(RETSTOCK)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(RETSTOCK)
  Bind(res:Record)
  Clear(res:Record)
  NetWebSetSessionPics(p_web,RETSTOCK)

Children Routine
  if loc:selecting = 0
    If p_web.RequestAjax = 0
      do StartChildren
    Else
      do AjaxChildren
    End
  end

AjaxChildren  Routine
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue('res:Record_Number',p_web.GetValue('res:Record_Number'))
   p_web.DeleteValue('_Clicked')
   loc:found = 1
   do Children
! ----------------------------------------------------------------------------------------
CallRow  Routine
  data
loc:result  long
  code
  do OpenFilesB
  res:Record_Number = p_web.GSV('res:Record_Number')
  loc:result = p_web._GetFile(RETSTOCK,res:Record_Number_Key)
  loc:eip = 1
  loc:viewstate = p_web.escape(p_web.Base64Encode(clip(res:Record_Number)))
  do BrowseRow
  do SendPacket
  do ClosefilesB

! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(RETSTOCK)
  Case upper(p_web.GetValue('_EIPClm'))
  Else
    case p_web.GetValue('_action_')
    of Net:InsertRecord
    orof Net:ChangeRecord
    orof Net:DeleteRecord
    orof Net:CopyRecord
      loc:eip = 0
      do CallBrowse
    end
  End
  do GotFocusBack
  p_web._CloseFile(RETSTOCK)
! ----------------------------------------------------------------------------------------
value::res:Part_Number   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseRetailSalesItems_res:Part_Number_'&res:Record_Number,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(res:Part_Number,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::res:Description   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseRetailSalesItems_res:Description_'&res:Record_Number,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(res:Description,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::res:Quantity   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseRetailSalesItems_res:Quantity_'&res:Record_Number,'RightJustify',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(res:Quantity,'@n_8')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::res:Purchase_Order_Number   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseRetailSalesItems_res:Purchase_Order_Number_'&res:Record_Number,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(res:Purchase_Order_Number,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::res:Previous_Sale_Number   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
  If (p_web.GSV('locRetailStockType') = 'B')
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseRetailSalesItems_res:Previous_Sale_Number_'&res:Record_Number,'RightJustify',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(res:Previous_Sale_Number,'@n_8')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
  End
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END
  return
!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
CheckForDuplicate  Routine
SetSelection  Routine
  loc:ActualSelection = loc:field
  p_web.SetSessionValue('res:Record_Number',res:Record_Number)

MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:ActualSelection = ''
    loc:ActualSelection = loc:DefaultSelection
    p_web.GetBrowseValue(loc:ActualSelection,Net:Web:SessionQueue) ! so children are primed with correct sessionValue
  End

  If loc:ActualSelection <> ''
    TableQueue.Kind = Net:RowTable
    get(TableQueue,TableQueue.Kind)
    if Errorcode() = 0
      TableQueue.Row = sub(TableQueue.Row,1,len(clip(TableQueue.Row))-1) &  ' data-nt-rows="'&loc:RowsHigh&'" data-nt-value="'&clip(loc:ActualSelection)&'"' & '>'
      Put(TableQueue)
    End
  End

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead class="'&p_web.combine(p_web.site.style.BrowseHeader,'')&'"><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot class="'&p_web.combine(p_web.site.style.BrowseFooter,'BrowseFooter')&'"><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody class="'&p_web.combine(p_web.site.style.BrowseBody,)&'"><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table><13,10>' & p_web.DivFooter(Net:NoSend) ! Table Div
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
