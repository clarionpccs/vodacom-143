  MEMBER('WebServer_Phase4.clw')
      !   - FileType 
      ! ACCESSOR  - FileType FILE
      ! ACCSTAT  - FileType FILE
      ! ACTION  - FileType FILE
      ! AUDIT  - FileType FILE
      ! AUDITE  - FileType FILE
      ! AUDSTATS  - FileType FILE
      ! CHARTYPE  - FileType FILE
      ! CONTHIST  - FileType FILE
      ! COURIER  - FileType FILE
      ! DEFAULT2  - FileType FILE
      ! DEFAULTS  - FileType FILE
      ! DEFPRINT  - FileType FILE
      ! DISCOUNT  - FileType FILE
      ! ESNMODAL  - FileType FILE
      ! ESNMODEL  - FileType FILE
      ! ESREJRES  - FileType FILE
      ! ESTPARTS  - FileType FILE
      ! EXCHACC  - FileType FILE
      ! EXCHAMF  - FileType FILE
      ! EXCHANGE  - FileType FILE
      ! EXCHANGE_ALIAS  - FileType ALIAS
      ! EXCHHIST  - FileType FILE
      ! EXCHOR48  - FileType FILE
      ! EXCHORDR  - FileType FILE
      ! GENSHORT  - FileType FILE
      ! GRNOTESR  - FileType FILE
      ! INVOICE  - FileType FILE
      ! JOBACC  - FileType FILE
      ! JOBACCNO  - FileType FILE
      ! JOBEXACC  - FileType FILE
      ! JOBNOTES  - FileType FILE
      ! JOBNOTES_ALIAS  - FileType ALIAS
      ! JOBOUTFL  - FileType FILE
      ! JOBPAYMT  - FileType FILE
      ! JOBPAYMT_ALIAS  - FileType ALIAS
      ! JOBRPNOT  - FileType FILE
      ! JOBS  - FileType FILE
      ! JOBS2_ALIAS  - FileType ALIAS
      ! JOBS_ALIAS  - FileType ALIAS
      ! JOBSCONS  - FileType FILE
      ! JOBSE  - FileType FILE
      ! JOBSE2  - FileType FILE
      ! JOBSE_ALIAS  - FileType ALIAS
      ! JOBSENG  - FileType FILE
      ! JOBSINV  - FileType FILE
      ! JOBSLOCK  - FileType FILE
      ! JOBSOBF  - FileType FILE
      ! JOBSTAGE  - FileType FILE
      ! JOBSTAMP  - FileType FILE
      ! JOBSWARR  - FileType FILE
      ! JOBTHIRD  - FileType FILE
      ! LOAN  - FileType FILE
      ! LOAN_ALIAS  - FileType ALIAS
      ! LOANACC  - FileType FILE
      ! LOANHIST  - FileType FILE
      ! LOAORDR  - FileType FILE
      ! LOCATION  - FileType FILE
      ! LOCATION_ALIAS  - FileType ALIAS
      ! LOCATLOG  - FileType FILE
      ! LOCSHELF  - FileType FILE
      ! MANFAUEX  - FileType FILE
      ! MANFAULO  - FileType FILE
      ! MANFAULO_ALIAS  - FileType ALIAS
      ! MANFAULT  - FileType FILE
      ! MANFAULT_ALIAS  - FileType ALIAS
      ! MANFAUPA  - FileType FILE
      ! MANFAUPA_ALIAS  - FileType ALIAS
      ! MANFAURL  - FileType FILE
      ! MANFPALO  - FileType FILE
      ! MANFPALO_ALIAS  - FileType ALIAS
      ! MANFPARL  - FileType FILE
      ! MANMARK  - FileType FILE
      ! MANUDATE  - FileType FILE
      ! MANUFACT  - FileType FILE
      ! MODELCCT  - FileType FILE
      ! MODELCOL  - FileType FILE
      ! MODELNUM  - FileType FILE
      ! MODPROD  - FileType FILE
      ! MULDESP  - FileType FILE
      ! MULDESP_ALIAS  - FileType ALIAS
      ! MULDESPJ  - FileType FILE
      ! MULDESPJ_ALIAS  - FileType ALIAS
      ! NETWORKS  - FileType FILE
      ! NOTESCON  - FileType FILE
      ! NOTESENG  - FileType FILE
      ! ORDHEAD  - FileType FILE
      ! ORDITEMS  - FileType FILE
      ! ORDWEBPR  - FileType FILE
      ! PARTS  - FileType FILE
      ! PARTS_ALIAS  - FileType ALIAS
      ! PAYTYPES  - FileType FILE
      ! PRODCODE  - FileType FILE
      ! REPTYDEF  - FileType FILE
      ! RETSALES  - FileType FILE
      ! RETSTOCK  - FileType FILE
      ! RETTYPES  - FileType FILE
      ! RTNAWAIT  - FileType FILE
      ! RTNORDER  - FileType FILE
      ! SBO_GenericFile  - FileType FILE
      ! SBO_OutFaultParts  - FileType FILE
      ! SBO_OutParts  - FileType FILE
      ! SMSMAIL  - FileType FILE
      ! SMSRECVD  - FileType FILE
      ! SMSRECVD_ALIAS  - FileType ALIAS
      ! SMSText  - FileType FILE
      ! STAHEAD  - FileType FILE
      ! STANTEXT  - FileType FILE
      ! STARECIP  - FileType FILE
      ! STATUS  - FileType FILE
      ! STDCHRGE  - FileType FILE
      ! STOAUDIT  - FileType FILE
      ! STOCK  - FileType FILE
      ! STOCK_ALIAS  - FileType ALIAS
      ! STOCKALL  - FileType FILE
      ! STOCKALX  - FileType FILE
      ! STOCKRECEIVETMP  - FileType FILE
      ! STOCKTYP  - FileType FILE
      ! STOFAULT  - FileType FILE
      ! STOHIST  - FileType FILE
      ! STOHISTE  - FileType FILE
      ! STOMJFAU  - FileType FILE
      ! STOMODEL  - FileType FILE
      ! STOMPFAU  - FileType FILE
      ! STOPARTS  - FileType FILE
      ! SUBACCAD  - FileType FILE
      ! SUBCHRGE  - FileType FILE
      ! SUBEMAIL  - FileType FILE
      ! SUBTRACC  - FileType FILE
      ! SUBURB  - FileType FILE
      ! SUPPLIER  - FileType FILE
      ! TagFile  - FileType FILE
      ! TRACHAR  - FileType FILE
      ! TRACHRGE  - FileType FILE
      ! TRADEAC2  - FileType FILE
      ! TRADEACC  - FileType FILE
      ! TRADEACC_ALIAS  - FileType ALIAS
      ! TRAEMAIL  - FileType FILE
      ! TRAHUBAC  - FileType FILE
      ! TRAHUBS  - FileType FILE
      ! TRANTYPE  - FileType FILE
      ! TRDBATCH  - FileType FILE
      ! TRDPARTY  - FileType FILE
      ! UNITTYPE  - FileType FILE
      ! USERS  - FileType FILE
      ! USERS_ALIAS  - FileType ALIAS
      ! VATCODE  - FileType FILE
      ! WARPARTS  - FileType FILE
      ! WARPARTS_ALIAS  - FileType ALIAS
      ! WAYBAWT  - FileType FILE
      ! WAYBILLJ  - FileType FILE
      ! WAYBILLS  - FileType FILE
      ! WAYCNR  - FileType FILE
      ! WAYSUND  - FileType FILE
      ! WEBJOB  - FileType FILE
      ! WEBJOBNO  - FileType FILE
  ! ----------------------------------------------------------------------------------------
! ----------------------------------------------------------------------------------------
! These procedures support the NetTalk Web Server templates. They are sufficiently generic
! that there is no need to put them in the application true, however are dependant on the
! dictionary and/or application such that they need to be generated, and cannot be inserted
! as methods in the class.
! ----------------------------------------------------------------------------------------
NetWebRelationManager PROCEDURE  (FILE p_file)
RM       &RelationManager
  CODE
  RM &= NULL
  If p_FILE &= NULL then Return RM.
  If p_File &= Relate:accareas.Me.File then RM &= Relate:accareas.
  If p_File &= Relate:accessor.Me.File then RM &= Relate:accessor.
  If p_File &= Relate:accstat.Me.File then RM &= Relate:accstat.
  If p_File &= Relate:action.Me.File then RM &= Relate:action.
  If p_File &= Relate:audit.Me.File then RM &= Relate:audit.
  If p_File &= Relate:audite.Me.File then RM &= Relate:audite.
  If p_File &= Relate:audstats.Me.File then RM &= Relate:audstats.
  If p_File &= Relate:chartype.Me.File then RM &= Relate:chartype.
  If p_File &= Relate:conthist.Me.File then RM &= Relate:conthist.
  If p_File &= Relate:courier.Me.File then RM &= Relate:courier.
  If p_File &= Relate:default2.Me.File then RM &= Relate:default2.
  If p_File &= Relate:defaults.Me.File then RM &= Relate:defaults.
  If p_File &= Relate:defprint.Me.File then RM &= Relate:defprint.
  If p_File &= Relate:discount.Me.File then RM &= Relate:discount.
  If p_File &= Relate:esnmodal.Me.File then RM &= Relate:esnmodal.
  If p_File &= Relate:esnmodel.Me.File then RM &= Relate:esnmodel.
  If p_File &= Relate:esrejres.Me.File then RM &= Relate:esrejres.
  If p_File &= Relate:estparts.Me.File then RM &= Relate:estparts.
  If p_File &= Relate:exchacc.Me.File then RM &= Relate:exchacc.
  If p_File &= Relate:exchamf.Me.File then RM &= Relate:exchamf.
  If p_File &= Relate:exchange.Me.File then RM &= Relate:exchange.
  If p_File &= Relate:exchange_alias.Me.File then RM &= Relate:exchange_alias.
  If p_File &= Relate:exchhist.Me.File then RM &= Relate:exchhist.
  If p_File &= Relate:exchor48.Me.File then RM &= Relate:exchor48.
  If p_File &= Relate:exchordr.Me.File then RM &= Relate:exchordr.
  If p_File &= Relate:genshort.Me.File then RM &= Relate:genshort.
  If p_File &= Relate:grnotesr.Me.File then RM &= Relate:grnotesr.
  If p_File &= Relate:invoice.Me.File then RM &= Relate:invoice.
  If p_File &= Relate:jobacc.Me.File then RM &= Relate:jobacc.
  If p_File &= Relate:jobaccno.Me.File then RM &= Relate:jobaccno.
  If p_File &= Relate:jobexacc.Me.File then RM &= Relate:jobexacc.
  If p_File &= Relate:jobnotes.Me.File then RM &= Relate:jobnotes.
  If p_File &= Relate:jobnotes_alias.Me.File then RM &= Relate:jobnotes_alias.
  If p_File &= Relate:joboutfl.Me.File then RM &= Relate:joboutfl.
  If p_File &= Relate:jobpaymt.Me.File then RM &= Relate:jobpaymt.
  If p_File &= Relate:jobpaymt_alias.Me.File then RM &= Relate:jobpaymt_alias.
  If p_File &= Relate:jobrpnot.Me.File then RM &= Relate:jobrpnot.
  If p_File &= Relate:jobs.Me.File then RM &= Relate:jobs.
  If p_File &= Relate:jobs2_alias.Me.File then RM &= Relate:jobs2_alias.
  If p_File &= Relate:jobs_alias.Me.File then RM &= Relate:jobs_alias.
  If p_File &= Relate:jobscons.Me.File then RM &= Relate:jobscons.
  If p_File &= Relate:jobse.Me.File then RM &= Relate:jobse.
  If p_File &= Relate:jobse2.Me.File then RM &= Relate:jobse2.
  If p_File &= Relate:jobse_alias.Me.File then RM &= Relate:jobse_alias.
  If p_File &= Relate:jobseng.Me.File then RM &= Relate:jobseng.
  If p_File &= Relate:jobsinv.Me.File then RM &= Relate:jobsinv.
  If p_File &= Relate:jobslock.Me.File then RM &= Relate:jobslock.
  If p_File &= Relate:jobsobf.Me.File then RM &= Relate:jobsobf.
  If p_File &= Relate:jobstage.Me.File then RM &= Relate:jobstage.
  If p_File &= Relate:jobstamp.Me.File then RM &= Relate:jobstamp.
  If p_File &= Relate:jobswarr.Me.File then RM &= Relate:jobswarr.
  If p_File &= Relate:jobthird.Me.File then RM &= Relate:jobthird.
  If p_File &= Relate:loan.Me.File then RM &= Relate:loan.
  If p_File &= Relate:loan_alias.Me.File then RM &= Relate:loan_alias.
  If p_File &= Relate:loanacc.Me.File then RM &= Relate:loanacc.
  If p_File &= Relate:loanhist.Me.File then RM &= Relate:loanhist.
  If p_File &= Relate:loaordr.Me.File then RM &= Relate:loaordr.
  If p_File &= Relate:location.Me.File then RM &= Relate:location.
  If p_File &= Relate:location_alias.Me.File then RM &= Relate:location_alias.
  If p_File &= Relate:locatlog.Me.File then RM &= Relate:locatlog.
  If p_File &= Relate:locshelf.Me.File then RM &= Relate:locshelf.
  If p_File &= Relate:manfauex.Me.File then RM &= Relate:manfauex.
  If p_File &= Relate:manfaulo.Me.File then RM &= Relate:manfaulo.
  If p_File &= Relate:manfaulo_alias.Me.File then RM &= Relate:manfaulo_alias.
  If p_File &= Relate:manfault.Me.File then RM &= Relate:manfault.
  If p_File &= Relate:manfault_alias.Me.File then RM &= Relate:manfault_alias.
  If p_File &= Relate:manfaupa.Me.File then RM &= Relate:manfaupa.
  If p_File &= Relate:manfaupa_alias.Me.File then RM &= Relate:manfaupa_alias.
  If p_File &= Relate:manfaurl.Me.File then RM &= Relate:manfaurl.
  If p_File &= Relate:manfpalo.Me.File then RM &= Relate:manfpalo.
  If p_File &= Relate:manfpalo_alias.Me.File then RM &= Relate:manfpalo_alias.
  If p_File &= Relate:manfparl.Me.File then RM &= Relate:manfparl.
  If p_File &= Relate:manmark.Me.File then RM &= Relate:manmark.
  If p_File &= Relate:manudate.Me.File then RM &= Relate:manudate.
  If p_File &= Relate:manufact.Me.File then RM &= Relate:manufact.
  If p_File &= Relate:modelcct.Me.File then RM &= Relate:modelcct.
  If p_File &= Relate:modelcol.Me.File then RM &= Relate:modelcol.
  If p_File &= Relate:modelnum.Me.File then RM &= Relate:modelnum.
  If p_File &= Relate:modprod.Me.File then RM &= Relate:modprod.
  If p_File &= Relate:muldesp.Me.File then RM &= Relate:muldesp.
  If p_File &= Relate:muldesp_alias.Me.File then RM &= Relate:muldesp_alias.
  If p_File &= Relate:muldespj.Me.File then RM &= Relate:muldespj.
  If p_File &= Relate:muldespj_alias.Me.File then RM &= Relate:muldespj_alias.
  If p_File &= Relate:networks.Me.File then RM &= Relate:networks.
  If p_File &= Relate:notescon.Me.File then RM &= Relate:notescon.
  If p_File &= Relate:noteseng.Me.File then RM &= Relate:noteseng.
  If p_File &= Relate:ordhead.Me.File then RM &= Relate:ordhead.
  If p_File &= Relate:orditems.Me.File then RM &= Relate:orditems.
  If p_File &= Relate:ordwebpr.Me.File then RM &= Relate:ordwebpr.
  If p_File &= Relate:parts.Me.File then RM &= Relate:parts.
  If p_File &= Relate:parts_alias.Me.File then RM &= Relate:parts_alias.
  If p_File &= Relate:paytypes.Me.File then RM &= Relate:paytypes.
  If p_File &= Relate:prodcode.Me.File then RM &= Relate:prodcode.
  If p_File &= Relate:reptydef.Me.File then RM &= Relate:reptydef.
  If p_File &= Relate:retsales.Me.File then RM &= Relate:retsales.
  If p_File &= Relate:retstock.Me.File then RM &= Relate:retstock.
  If p_File &= Relate:rettypes.Me.File then RM &= Relate:rettypes.
  If p_File &= Relate:rtnawait.Me.File then RM &= Relate:rtnawait.
  If p_File &= Relate:rtnorder.Me.File then RM &= Relate:rtnorder.
  If p_File &= Relate:sbo_genericfile.Me.File then RM &= Relate:sbo_genericfile.
  If p_File &= Relate:sbo_outfaultparts.Me.File then RM &= Relate:sbo_outfaultparts.
  If p_File &= Relate:sbo_outparts.Me.File then RM &= Relate:sbo_outparts.
  If p_File &= Relate:smsmail.Me.File then RM &= Relate:smsmail.
  If p_File &= Relate:smsrecvd.Me.File then RM &= Relate:smsrecvd.
  If p_File &= Relate:smsrecvd_alias.Me.File then RM &= Relate:smsrecvd_alias.
  If p_File &= Relate:smstext.Me.File then RM &= Relate:smstext.
  If p_File &= Relate:stahead.Me.File then RM &= Relate:stahead.
  If p_File &= Relate:stantext.Me.File then RM &= Relate:stantext.
  If p_File &= Relate:starecip.Me.File then RM &= Relate:starecip.
  If p_File &= Relate:status.Me.File then RM &= Relate:status.
  If p_File &= Relate:stdchrge.Me.File then RM &= Relate:stdchrge.
  If p_File &= Relate:stoaudit.Me.File then RM &= Relate:stoaudit.
  If p_File &= Relate:stock.Me.File then RM &= Relate:stock.
  If p_File &= Relate:stock_alias.Me.File then RM &= Relate:stock_alias.
  If p_File &= Relate:stockall.Me.File then RM &= Relate:stockall.
  If p_File &= Relate:stockalx.Me.File then RM &= Relate:stockalx.
  If p_File &= Relate:stockreceivetmp.Me.File then RM &= Relate:stockreceivetmp.
  If p_File &= Relate:stocktyp.Me.File then RM &= Relate:stocktyp.
  If p_File &= Relate:stofault.Me.File then RM &= Relate:stofault.
  If p_File &= Relate:stohist.Me.File then RM &= Relate:stohist.
  If p_File &= Relate:stohiste.Me.File then RM &= Relate:stohiste.
  If p_File &= Relate:stomjfau.Me.File then RM &= Relate:stomjfau.
  If p_File &= Relate:stomodel.Me.File then RM &= Relate:stomodel.
  If p_File &= Relate:stompfau.Me.File then RM &= Relate:stompfau.
  If p_File &= Relate:stoparts.Me.File then RM &= Relate:stoparts.
  If p_File &= Relate:subaccad.Me.File then RM &= Relate:subaccad.
  If p_File &= Relate:subchrge.Me.File then RM &= Relate:subchrge.
  If p_File &= Relate:subemail.Me.File then RM &= Relate:subemail.
  If p_File &= Relate:subtracc.Me.File then RM &= Relate:subtracc.
  If p_File &= Relate:suburb.Me.File then RM &= Relate:suburb.
  If p_File &= Relate:supplier.Me.File then RM &= Relate:supplier.
  If p_File &= Relate:tagfile.Me.File then RM &= Relate:tagfile.
  If p_File &= Relate:trachar.Me.File then RM &= Relate:trachar.
  If p_File &= Relate:trachrge.Me.File then RM &= Relate:trachrge.
  If p_File &= Relate:tradeac2.Me.File then RM &= Relate:tradeac2.
  If p_File &= Relate:tradeacc.Me.File then RM &= Relate:tradeacc.
  If p_File &= Relate:tradeacc_alias.Me.File then RM &= Relate:tradeacc_alias.
  If p_File &= Relate:traemail.Me.File then RM &= Relate:traemail.
  If p_File &= Relate:trahubac.Me.File then RM &= Relate:trahubac.
  If p_File &= Relate:trahubs.Me.File then RM &= Relate:trahubs.
  If p_File &= Relate:trantype.Me.File then RM &= Relate:trantype.
  If p_File &= Relate:trdbatch.Me.File then RM &= Relate:trdbatch.
  If p_File &= Relate:trdparty.Me.File then RM &= Relate:trdparty.
  If p_File &= Relate:unittype.Me.File then RM &= Relate:unittype.
  If p_File &= Relate:users.Me.File then RM &= Relate:users.
  If p_File &= Relate:users_alias.Me.File then RM &= Relate:users_alias.
  If p_File &= Relate:vatcode.Me.File then RM &= Relate:vatcode.
  If p_File &= Relate:warparts.Me.File then RM &= Relate:warparts.
  If p_File &= Relate:warparts_alias.Me.File then RM &= Relate:warparts_alias.
  If p_File &= Relate:waybawt.Me.File then RM &= Relate:waybawt.
  If p_File &= Relate:waybillj.Me.File then RM &= Relate:waybillj.
  If p_File &= Relate:waybills.Me.File then RM &= Relate:waybills.
  If p_File &= Relate:waycnr.Me.File then RM &= Relate:waycnr.
  If p_File &= Relate:waysund.Me.File then RM &= Relate:waysund.
  If p_File &= Relate:webjob.Me.File then RM &= Relate:webjob.
  If p_File &= Relate:webjobno.Me.File then RM &= Relate:webjobno.
  Return RM
! ----------------------------------------------------------------------------------------
NetWebFileNamed PROCEDURE  (string p_file)
F        &File
  CODE
  F &= NULL
  Case Lower(p_file)
  Of 'accareas'
    F &= accareas
  Of 'accessor'
    F &= accessor
  Of 'accstat'
    F &= accstat
  Of 'action'
    F &= action
  Of 'audit'
    F &= audit
  Of 'audite'
    F &= audite
  Of 'audstats'
    F &= audstats
  Of 'chartype'
    F &= chartype
  Of 'conthist'
    F &= conthist
  Of 'courier'
    F &= courier
  Of 'default2'
    F &= default2
  Of 'defaults'
    F &= defaults
  Of 'defprint'
    F &= defprint
  Of 'discount'
    F &= discount
  Of 'esnmodal'
    F &= esnmodal
  Of 'esnmodel'
    F &= esnmodel
  Of 'esrejres'
    F &= esrejres
  Of 'estparts'
    F &= estparts
  Of 'exchacc'
    F &= exchacc
  Of 'exchamf'
    F &= exchamf
  Of 'exchange'
    F &= exchange
  Of 'exchange_alias'
    F &= exchange_alias
  Of 'exchhist'
    F &= exchhist
  Of 'exchor48'
    F &= exchor48
  Of 'exchordr'
    F &= exchordr
  Of 'genshort'
    F &= genshort
  Of 'grnotesr'
    F &= grnotesr
  Of 'invoice'
    F &= invoice
  Of 'jobacc'
    F &= jobacc
  Of 'jobaccno'
    F &= jobaccno
  Of 'jobexacc'
    F &= jobexacc
  Of 'jobnotes'
    F &= jobnotes
  Of 'jobnotes_alias'
    F &= jobnotes_alias
  Of 'joboutfl'
    F &= joboutfl
  Of 'jobpaymt'
    F &= jobpaymt
  Of 'jobpaymt_alias'
    F &= jobpaymt_alias
  Of 'jobrpnot'
    F &= jobrpnot
  Of 'jobs'
    F &= jobs
  Of 'jobs2_alias'
    F &= jobs2_alias
  Of 'jobs_alias'
    F &= jobs_alias
  Of 'jobscons'
    F &= jobscons
  Of 'jobse'
    F &= jobse
  Of 'jobse2'
    F &= jobse2
  Of 'jobse_alias'
    F &= jobse_alias
  Of 'jobseng'
    F &= jobseng
  Of 'jobsinv'
    F &= jobsinv
  Of 'jobslock'
    F &= jobslock
  Of 'jobsobf'
    F &= jobsobf
  Of 'jobstage'
    F &= jobstage
  Of 'jobstamp'
    F &= jobstamp
  Of 'jobswarr'
    F &= jobswarr
  Of 'jobthird'
    F &= jobthird
  Of 'loan'
    F &= loan
  Of 'loan_alias'
    F &= loan_alias
  Of 'loanacc'
    F &= loanacc
  Of 'loanhist'
    F &= loanhist
  Of 'loaordr'
    F &= loaordr
  Of 'location'
    F &= location
  Of 'location_alias'
    F &= location_alias
  Of 'locatlog'
    F &= locatlog
  Of 'locshelf'
    F &= locshelf
  Of 'manfauex'
    F &= manfauex
  Of 'manfaulo'
    F &= manfaulo
  Of 'manfaulo_alias'
    F &= manfaulo_alias
  Of 'manfault'
    F &= manfault
  Of 'manfault_alias'
    F &= manfault_alias
  Of 'manfaupa'
    F &= manfaupa
  Of 'manfaupa_alias'
    F &= manfaupa_alias
  Of 'manfaurl'
    F &= manfaurl
  Of 'manfpalo'
    F &= manfpalo
  Of 'manfpalo_alias'
    F &= manfpalo_alias
  Of 'manfparl'
    F &= manfparl
  Of 'manmark'
    F &= manmark
  Of 'manudate'
    F &= manudate
  Of 'manufact'
    F &= manufact
  Of 'modelcct'
    F &= modelcct
  Of 'modelcol'
    F &= modelcol
  Of 'modelnum'
    F &= modelnum
  Of 'modprod'
    F &= modprod
  Of 'muldesp'
    F &= muldesp
  Of 'muldesp_alias'
    F &= muldesp_alias
  Of 'muldespj'
    F &= muldespj
  Of 'muldespj_alias'
    F &= muldespj_alias
  Of 'networks'
    F &= networks
  Of 'notescon'
    F &= notescon
  Of 'noteseng'
    F &= noteseng
  Of 'ordhead'
    F &= ordhead
  Of 'orditems'
    F &= orditems
  Of 'ordwebpr'
    F &= ordwebpr
  Of 'parts'
    F &= parts
  Of 'parts_alias'
    F &= parts_alias
  Of 'paytypes'
    F &= paytypes
  Of 'prodcode'
    F &= prodcode
  Of 'reptydef'
    F &= reptydef
  Of 'retsales'
    F &= retsales
  Of 'retstock'
    F &= retstock
  Of 'rettypes'
    F &= rettypes
  Of 'rtnawait'
    F &= rtnawait
  Of 'rtnorder'
    F &= rtnorder
  Of 'sbo_genericfile'
    F &= sbo_genericfile
  Of 'sbo_outfaultparts'
    F &= sbo_outfaultparts
  Of 'sbo_outparts'
    F &= sbo_outparts
  Of 'smsmail'
    F &= smsmail
  Of 'smsrecvd'
    F &= smsrecvd
  Of 'smsrecvd_alias'
    F &= smsrecvd_alias
  Of 'smstext'
    F &= smstext
  Of 'stahead'
    F &= stahead
  Of 'stantext'
    F &= stantext
  Of 'starecip'
    F &= starecip
  Of 'status'
    F &= status
  Of 'stdchrge'
    F &= stdchrge
  Of 'stoaudit'
    F &= stoaudit
  Of 'stock'
    F &= stock
  Of 'stock_alias'
    F &= stock_alias
  Of 'stockall'
    F &= stockall
  Of 'stockalx'
    F &= stockalx
  Of 'stockreceivetmp'
    F &= stockreceivetmp
  Of 'stocktyp'
    F &= stocktyp
  Of 'stofault'
    F &= stofault
  Of 'stohist'
    F &= stohist
  Of 'stohiste'
    F &= stohiste
  Of 'stomjfau'
    F &= stomjfau
  Of 'stomodel'
    F &= stomodel
  Of 'stompfau'
    F &= stompfau
  Of 'stoparts'
    F &= stoparts
  Of 'subaccad'
    F &= subaccad
  Of 'subchrge'
    F &= subchrge
  Of 'subemail'
    F &= subemail
  Of 'subtracc'
    F &= subtracc
  Of 'suburb'
    F &= suburb
  Of 'supplier'
    F &= supplier
  Of 'tagfile'
    F &= tagfile
  Of 'trachar'
    F &= trachar
  Of 'trachrge'
    F &= trachrge
  Of 'tradeac2'
    F &= tradeac2
  Of 'tradeacc'
    F &= tradeacc
  Of 'tradeacc_alias'
    F &= tradeacc_alias
  Of 'traemail'
    F &= traemail
  Of 'trahubac'
    F &= trahubac
  Of 'trahubs'
    F &= trahubs
  Of 'trantype'
    F &= trantype
  Of 'trdbatch'
    F &= trdbatch
  Of 'trdparty'
    F &= trdparty
  Of 'unittype'
    F &= unittype
  Of 'users'
    F &= users
  Of 'users_alias'
    F &= users_alias
  Of 'vatcode'
    F &= vatcode
  Of 'warparts'
    F &= warparts
  Of 'warparts_alias'
    F &= warparts_alias
  Of 'waybawt'
    F &= waybawt
  Of 'waybillj'
    F &= waybillj
  Of 'waybills'
    F &= waybills
  Of 'waycnr'
    F &= waycnr
  Of 'waysund'
    F &= waysund
  Of 'webjob'
    F &= webjob
  Of 'webjobno'
    F &= webjobno
  End
  Return F
! ----------------------------------------------------------------------------------------
NetWebSetSessionPics PROCEDURE  (NetWebServerWorker p_web, FILE p_File)
  CODE
  ! Trying to move away from this code - let's see what happens - 4.26
  Return
  If p_FILE &= NULL then Return.
    If p_File &= audit
        p_web.SetSessionPicture('aud:Ref_Number','@p<<<<<<<<#p')
        p_web.SetSessionPicture('aud:Date',p_web.site.DatePicture)
        p_web.SetSessionPicture('aud:Time','@t1')
    End
    If p_File &= audstats
        p_web.SetSessionPicture('aus:DateChanged',p_web.site.DatePicture)
        p_web.SetSessionPicture('aus:TimeChanged','@t1b')
    End
    If p_File &= conthist
        p_web.SetSessionPicture('cht:Date',p_web.site.DatePicture)
        p_web.SetSessionPicture('cht:Time','@t1')
    End
    If p_File &= courier
        p_web.SetSessionPicture('cou:Last_Despatch_Time','@t1')
        p_web.SetSessionPicture('cou:StartWorkHours','@t1b')
        p_web.SetSessionPicture('cou:EndWorkHours','@t1b')
        p_web.SetSessionPicture('cou:SatStartWorkHours','@t1b')
        p_web.SetSessionPicture('cou:SatEndWorkHours','@t1b')
        p_web.SetSessionPicture('cou:SunStartWorkHours','@t1b')
        p_web.SetSessionPicture('cou:SunEndWorkHours','@t1b')
    End
    If p_File &= default2
        p_web.SetSessionPicture('de2:PLE',p_web.site.DatePicture)
    End
    If p_File &= defaults
        p_web.SetSessionPicture('def:Start_Work_Hours','@t1')
        p_web.SetSessionPicture('def:End_Work_Hours','@t1')
        p_web.SetSessionPicture('def:Job_Batch_Number','@p<<<<<<<<#p')
    End
    If p_File &= estparts
        p_web.SetSessionPicture('epr:Date_Ordered',p_web.site.DatePicture)
        p_web.SetSessionPicture('epr:Date_Received',p_web.site.DatePicture)
        p_web.SetSessionPicture('epr:Status_Date',p_web.site.DatePicture)
    End
    If p_File &= exchacc
        p_web.SetSessionPicture('xca:Ref_Number','@p<<<<<<<<#p')
    End
    If p_File &= exchamf
        p_web.SetSessionPicture('emf:Date',p_web.site.DatePicture)
    End
    If p_File &= exchange
        p_web.SetSessionPicture('xch:Date_Booked',p_web.site.DatePicture)
        p_web.SetSessionPicture('xch:StatusChangeDate',p_web.site.DatePicture)
    End
    If p_File &= exchange_alias
        p_web.SetSessionPicture('xch_ali:Date_Booked',p_web.site.DatePicture)
        p_web.SetSessionPicture('xch_ali:StatusChangeDate',p_web.site.DatePicture)
    End
    If p_File &= exchhist
        p_web.SetSessionPicture('exh:Date',p_web.site.DatePicture)
        p_web.SetSessionPicture('exh:Time','@T1')
    End
    If p_File &= exchor48
        p_web.SetSessionPicture('ex4:DateCreated',p_web.site.DatePicture)
        p_web.SetSessionPicture('ex4:TimeCreated','@t1b')
        p_web.SetSessionPicture('ex4:DateOrdered',p_web.site.DatePicture)
        p_web.SetSessionPicture('ex4:TimeOrdered','@t1b')
    End
    If p_File &= exchordr
        p_web.SetSessionPicture('exo:DateCreated',p_web.site.DatePicture)
        p_web.SetSessionPicture('exo:DateOrdered',p_web.site.DatePicture)
        p_web.SetSessionPicture('exo:DateReceived',p_web.site.DatePicture)
    End
    If p_File &= grnotesr
        p_web.SetSessionPicture('grr:Goods_Received_Number','@p<<<<<<<<#p')
        p_web.SetSessionPicture('grr:Order_Number','@p<<<<<<<<#p')
        p_web.SetSessionPicture('grr:Goods_Received_Date',p_web.site.DatePicture)
    End
    If p_File &= invoice
        p_web.SetSessionPicture('inv:Invoice_Number','@p<<<<<<<<#p')
        p_web.SetSessionPicture('inv:Job_Number','@p<<<<<<<<#p')
        p_web.SetSessionPicture('inv:Date_Created',p_web.site.DatePicture)
        p_web.SetSessionPicture('inv:Reconciled_Date',p_web.site.DatePicture)
        p_web.SetSessionPicture('inv:ExportedOracleDate',p_web.site.DatePicture)
        p_web.SetSessionPicture('inv:RRCInvoiceDate',p_web.site.DatePicture)
        p_web.SetSessionPicture('inv:ARCInvoiceDate',p_web.site.DatePicture)
    End
    If p_File &= jobacc
        p_web.SetSessionPicture('jac:Ref_Number','@p<<<<<<<<#p')
    End
    If p_File &= jobexacc
        p_web.SetSessionPicture('jea:Job_Ref_Number','@p<<<<<<<<#p')
        p_web.SetSessionPicture('jea:Stock_Ref_Number','@p<<<<<<<<#p')
    End
    If p_File &= jobpaymt
        p_web.SetSessionPicture('jpt:Ref_Number','@p<<<<<<<p')
        p_web.SetSessionPicture('jpt:Date',p_web.site.DatePicture)
        p_web.SetSessionPicture('jpt:Expiry_Date','@p##/##p')
    End
    If p_File &= jobpaymt_alias
        p_web.SetSessionPicture('jpt_ali:Ref_Number','@p<<<<<<<p')
        p_web.SetSessionPicture('jpt_ali:Date',p_web.site.DatePicture)
        p_web.SetSessionPicture('jpt_ali:Expiry_Date','@p##/##p')
    End
    If p_File &= jobrpnot
        p_web.SetSessionPicture('jrn:TheDate',p_web.site.DatePicture)
        p_web.SetSessionPicture('jrn:TheTime','@t1b')
    End
    If p_File &= jobs
        p_web.SetSessionPicture('job:date_booked',p_web.site.DatePicture)
        p_web.SetSessionPicture('job:time_booked','@t1')
        p_web.SetSessionPicture('job:DOP',p_web.site.DatePicture)
        p_web.SetSessionPicture('job:Date_In_Repair',p_web.site.DatePicture)
        p_web.SetSessionPicture('job:Time_In_Repair','@t1b')
        p_web.SetSessionPicture('job:Date_On_Test',p_web.site.DatePicture)
        p_web.SetSessionPicture('job:Time_On_Test','@t1b')
        p_web.SetSessionPicture('job:Date_QA_Passed',p_web.site.DatePicture)
        p_web.SetSessionPicture('job:Time_QA_Passed','@t1b')
        p_web.SetSessionPicture('job:Date_QA_Rejected',p_web.site.DatePicture)
        p_web.SetSessionPicture('job:Time_QA_Rejected','@t1b')
        p_web.SetSessionPicture('job:Date_QA_Second_Passed',p_web.site.DatePicture)
        p_web.SetSessionPicture('job:Time_QA_Second_Passed','@t1b')
        p_web.SetSessionPicture('job:Date_Completed',p_web.site.DatePicture)
        p_web.SetSessionPicture('job:Time_Completed','@t1')
        p_web.SetSessionPicture('job:Date_Paid',p_web.site.DatePicture)
        p_web.SetSessionPicture('job:Loan_Issued_Date',p_web.site.DatePicture)
        p_web.SetSessionPicture('job:Loan_Despatched',p_web.site.DatePicture)
        p_web.SetSessionPicture('job:Exchange_Issued_Date',p_web.site.DatePicture)
        p_web.SetSessionPicture('job:Exchange_Despatched',p_web.site.DatePicture)
        p_web.SetSessionPicture('job:Date_Despatched',p_web.site.DatePicture)
        p_web.SetSessionPicture('job:Incoming_Date',p_web.site.DatePicture)
        p_web.SetSessionPicture('job:ThirdPartyDateDesp',p_web.site.DatePicture)
        p_web.SetSessionPicture('job:Status_End_Date',p_web.site.DatePicture)
        p_web.SetSessionPicture('job:Status_End_Time','@t1')
        p_web.SetSessionPicture('job:Turnaround_End_Date',p_web.site.DatePicture)
        p_web.SetSessionPicture('job:Turnaround_End_Time','@t1')
        p_web.SetSessionPicture('job:Invoice_Date',p_web.site.DatePicture)
        p_web.SetSessionPicture('job:Invoice_Date_Warranty',p_web.site.DatePicture)
    End
    If p_File &= jobs2_alias
        p_web.SetSessionPicture('job2:date_booked',p_web.site.DatePicture)
        p_web.SetSessionPicture('job2:time_booked','@t1')
        p_web.SetSessionPicture('job2:DOP',p_web.site.DatePicture)
        p_web.SetSessionPicture('job2:Date_In_Repair',p_web.site.DatePicture)
        p_web.SetSessionPicture('job2:Time_In_Repair','@t1b')
        p_web.SetSessionPicture('job2:Date_On_Test',p_web.site.DatePicture)
        p_web.SetSessionPicture('job2:Time_On_Test','@t1b')
        p_web.SetSessionPicture('job2:Date_QA_Passed',p_web.site.DatePicture)
        p_web.SetSessionPicture('job2:Time_QA_Passed','@t1b')
        p_web.SetSessionPicture('job2:Date_QA_Rejected',p_web.site.DatePicture)
        p_web.SetSessionPicture('job2:Time_QA_Rejected','@t1b')
        p_web.SetSessionPicture('job2:Date_QA_Second_Passed',p_web.site.DatePicture)
        p_web.SetSessionPicture('job2:Time_QA_Second_Passed','@t1b')
        p_web.SetSessionPicture('job2:Date_Completed',p_web.site.DatePicture)
        p_web.SetSessionPicture('job2:Time_Completed','@t1')
        p_web.SetSessionPicture('job2:Date_Paid',p_web.site.DatePicture)
        p_web.SetSessionPicture('job2:Loan_Issued_Date',p_web.site.DatePicture)
        p_web.SetSessionPicture('job2:Loan_Despatched',p_web.site.DatePicture)
        p_web.SetSessionPicture('job2:Exchange_Issued_Date',p_web.site.DatePicture)
        p_web.SetSessionPicture('job2:Exchange_Despatched',p_web.site.DatePicture)
        p_web.SetSessionPicture('job2:Date_Despatched',p_web.site.DatePicture)
        p_web.SetSessionPicture('job2:Incoming_Date',p_web.site.DatePicture)
        p_web.SetSessionPicture('job2:ThirdPartyDateDesp',p_web.site.DatePicture)
        p_web.SetSessionPicture('job2:Status_End_Date',p_web.site.DatePicture)
        p_web.SetSessionPicture('job2:Status_End_Time','@t1')
        p_web.SetSessionPicture('job2:Turnaround_End_Date',p_web.site.DatePicture)
        p_web.SetSessionPicture('job2:Turnaround_End_Time','@t1')
        p_web.SetSessionPicture('job2:Invoice_Date',p_web.site.DatePicture)
        p_web.SetSessionPicture('job2:Invoice_Date_Warranty',p_web.site.DatePicture)
    End
    If p_File &= jobs_alias
        p_web.SetSessionPicture('job_ali:date_booked',p_web.site.DatePicture)
        p_web.SetSessionPicture('job_ali:time_booked','@t1')
        p_web.SetSessionPicture('job_ali:DOP',p_web.site.DatePicture)
        p_web.SetSessionPicture('job_ali:Date_In_Repair',p_web.site.DatePicture)
        p_web.SetSessionPicture('job_ali:Time_In_Repair','@t1b')
        p_web.SetSessionPicture('job_ali:Date_On_Test',p_web.site.DatePicture)
        p_web.SetSessionPicture('job_ali:Time_On_Test','@t1b')
        p_web.SetSessionPicture('job_ali:Date_QA_Passed',p_web.site.DatePicture)
        p_web.SetSessionPicture('job_ali:Time_QA_Passed','@t1b')
        p_web.SetSessionPicture('job_ali:Date_QA_Rejected',p_web.site.DatePicture)
        p_web.SetSessionPicture('job_ali:Time_QA_Rejected','@t1b')
        p_web.SetSessionPicture('job_ali:Date_QA_Second_Passed',p_web.site.DatePicture)
        p_web.SetSessionPicture('job_ali:Time_QA_Second_Passed','@t1b')
        p_web.SetSessionPicture('job_ali:Date_Completed',p_web.site.DatePicture)
        p_web.SetSessionPicture('job_ali:Time_Completed','@t1')
        p_web.SetSessionPicture('job_ali:Date_Paid',p_web.site.DatePicture)
        p_web.SetSessionPicture('job_ali:Loan_Issued_Date',p_web.site.DatePicture)
        p_web.SetSessionPicture('job_ali:Loan_Despatched',p_web.site.DatePicture)
        p_web.SetSessionPicture('job_ali:Exchange_Issued_Date',p_web.site.DatePicture)
        p_web.SetSessionPicture('job_ali:Exchange_Despatched',p_web.site.DatePicture)
        p_web.SetSessionPicture('job_ali:Date_Despatched',p_web.site.DatePicture)
        p_web.SetSessionPicture('job_ali:Incoming_Date',p_web.site.DatePicture)
        p_web.SetSessionPicture('job_ali:ThirdPartyDateDesp',p_web.site.DatePicture)
        p_web.SetSessionPicture('job_ali:Status_End_Date',p_web.site.DatePicture)
        p_web.SetSessionPicture('job_ali:Status_End_Time','@t1')
        p_web.SetSessionPicture('job_ali:Turnaround_End_Date',p_web.site.DatePicture)
        p_web.SetSessionPicture('job_ali:Turnaround_End_Time','@t1')
        p_web.SetSessionPicture('job_ali:Invoice_Date',p_web.site.DatePicture)
        p_web.SetSessionPicture('job_ali:Invoice_Date_Warranty',p_web.site.DatePicture)
    End
    If p_File &= jobscons
        p_web.SetSessionPicture('joc:TheDate',p_web.site.DatePicture)
        p_web.SetSessionPicture('joc:TheTime','@t1b')
    End
    If p_File &= jobse
        p_web.SetSessionPicture('jobe:HubRepairDate',p_web.site.DatePicture)
        p_web.SetSessionPicture('jobe:HubRepairTime','@t1b')
        p_web.SetSessionPicture('jobe:ReturnDate',p_web.site.DatePicture)
        p_web.SetSessionPicture('jobe:OBFvalidateDate',p_web.site.DatePicture)
        p_web.SetSessionPicture('jobe:OBFvalidateTime','@t1')
        p_web.SetSessionPicture('jobe:WarrantyStatusDate',p_web.site.DatePicture)
        p_web.SetSessionPicture('jobe:ARC3rdPartyInvoiceDate',p_web.site.DatePicture)
    End
    If p_File &= jobse2
        p_web.SetSessionPicture('jobe2:InPendingDate',p_web.site.DatePicture)
        p_web.SetSessionPicture('jobe2:DateReceivedAtPUP',p_web.site.DatePicture)
        p_web.SetSessionPicture('jobe2:TimeReceivedAtPUP','@t1b')
        p_web.SetSessionPicture('jobe2:DateDespatchFromPUP',p_web.site.DatePicture)
        p_web.SetSessionPicture('jobe2:TimeDespatchFromPUP','@t1b')
        p_web.SetSessionPicture('jobe2:SMSDate',p_web.site.DatePicture)
    End
    If p_File &= jobse_alias
        p_web.SetSessionPicture('jobe_ali:HubRepairDate',p_web.site.DatePicture)
        p_web.SetSessionPicture('jobe_ali:HubRepairTime','@t1b')
        p_web.SetSessionPicture('jobe_ali:ReturnDate',p_web.site.DatePicture)
        p_web.SetSessionPicture('jobe_ali:OBFvalidateDate',p_web.site.DatePicture)
        p_web.SetSessionPicture('jobe_ali:OBFvalidateTime','@t1')
        p_web.SetSessionPicture('jobe_ali:WarrantyStatusDate',p_web.site.DatePicture)
        p_web.SetSessionPicture('jobe_ali:ARC3rdPartyInvoiceDate',p_web.site.DatePicture)
    End
    If p_File &= jobseng
        p_web.SetSessionPicture('joe:DateAllocated',p_web.site.DatePicture)
        p_web.SetSessionPicture('joe:StatusDate',p_web.site.DatePicture)
        p_web.SetSessionPicture('joe:StatusTime','@t1b')
    End
    If p_File &= jobsinv
        p_web.SetSessionPicture('jov:DateCreated',p_web.site.DatePicture)
        p_web.SetSessionPicture('jov:TimeCreated','@t1b')
    End
    If p_File &= jobslock
        p_web.SetSessionPicture('lock:DateLocked',p_web.site.DatePicture)
        p_web.SetSessionPicture('lock:TimeLocked','@t7')
    End
    If p_File &= jobsobf
        p_web.SetSessionPicture('jof:DateCompleted',p_web.site.DatePicture)
        p_web.SetSessionPicture('jof:TimeCompleted','@t1b')
        p_web.SetSessionPicture('jof:DateProcessed',p_web.site.DatePicture)
        p_web.SetSessionPicture('jof:TimeProcessed','@t1b')
    End
    If p_File &= jobstage
        p_web.SetSessionPicture('jst:Ref_Number','@p<<<<<<<<#p')
        p_web.SetSessionPicture('jst:Date',p_web.site.DatePicture)
        p_web.SetSessionPicture('jst:Time','@t1')
    End
    If p_File &= jobstamp
        p_web.SetSessionPicture('jos:DateStamp',p_web.site.DatePicture)
        p_web.SetSessionPicture('jos:TimeStamp','@t1b')
    End
    If p_File &= jobswarr
        p_web.SetSessionPicture('jow:ClaimSubmitted',p_web.site.DatePicture)
        p_web.SetSessionPicture('jow:DateAccepted',p_web.site.DatePicture)
        p_web.SetSessionPicture('jow:DateReconciled',p_web.site.DatePicture)
        p_web.SetSessionPicture('jow:RRCDateReconciled',p_web.site.DatePicture)
        p_web.SetSessionPicture('jow:DateRejected',p_web.site.DatePicture)
        p_web.SetSessionPicture('jow:DateFinalRejection',p_web.site.DatePicture)
        p_web.SetSessionPicture('jow:Orig_Sub_Date',p_web.site.DatePicture)
    End
    If p_File &= jobthird
        p_web.SetSessionPicture('jot:DateOut',p_web.site.DatePicture)
        p_web.SetSessionPicture('jot:DateDespatched',p_web.site.DatePicture)
        p_web.SetSessionPicture('jot:DateIn',p_web.site.DatePicture)
    End
    If p_File &= loan
        p_web.SetSessionPicture('loa:Date_Booked',p_web.site.DatePicture)
        p_web.SetSessionPicture('loa:StatusChangeDate',p_web.site.DatePicture)
    End
    If p_File &= loan_alias
        p_web.SetSessionPicture('loa_ali:Date_Booked',p_web.site.DatePicture)
        p_web.SetSessionPicture('loa_ali:StatusChangeDate',p_web.site.DatePicture)
    End
    If p_File &= loanacc
        p_web.SetSessionPicture('lac:Ref_Number','@p<<<<<<<<#p')
    End
    If p_File &= loanhist
        p_web.SetSessionPicture('loh:Ref_Number','@p<<<<<<<<#p')
        p_web.SetSessionPicture('loh:Date',p_web.site.DatePicture)
        p_web.SetSessionPicture('loh:Time','@T1')
    End
    If p_File &= loaordr
        p_web.SetSessionPicture('lor:DateCreated',p_web.site.DatePicture)
        p_web.SetSessionPicture('lor:DateOrdered',p_web.site.DatePicture)
        p_web.SetSessionPicture('lor:DateReceived',p_web.site.DatePicture)
    End
    If p_File &= locatlog
        p_web.SetSessionPicture('lot:TheDate',p_web.site.DatePicture)
        p_web.SetSessionPicture('lot:TheTime','@t1b')
    End
    If p_File &= manufact
        p_web.SetSessionPicture('man:SiemensDate',p_web.site.DatePicture)
    End
    If p_File &= ordhead
        p_web.SetSessionPicture('orh:thedate',p_web.site.DatePicture)
        p_web.SetSessionPicture('orh:thetime','@t1')
        p_web.SetSessionPicture('orh:pro_date',p_web.site.DatePicture)
        p_web.SetSessionPicture('orh:DateDespatched',p_web.site.DatePicture)
    End
    If p_File &= parts
        p_web.SetSessionPicture('par:Date_Ordered',p_web.site.DatePicture)
        p_web.SetSessionPicture('par:Date_Received',p_web.site.DatePicture)
        p_web.SetSessionPicture('par:Status_Date',p_web.site.DatePicture)
    End
    If p_File &= parts_alias
        p_web.SetSessionPicture('par_ali:Date_Ordered',p_web.site.DatePicture)
        p_web.SetSessionPicture('par_ali:Date_Received',p_web.site.DatePicture)
        p_web.SetSessionPicture('par_ali:Status_Date',p_web.site.DatePicture)
    End
    If p_File &= retsales
        p_web.SetSessionPicture('ret:date_booked',p_web.site.DatePicture)
        p_web.SetSessionPicture('ret:time_booked','@t1')
        p_web.SetSessionPicture('ret:Date_Despatched',p_web.site.DatePicture)
        p_web.SetSessionPicture('ret:Invoice_Date',p_web.site.DatePicture)
        p_web.SetSessionPicture('ret:WebDateCreated',p_web.site.DatePicture)
        p_web.SetSessionPicture('ret:DatePickingNotePrinted',p_web.site.DatePicture)
        p_web.SetSessionPicture('ret:TimePickingNotePrinted','@t1b')
    End
    If p_File &= retstock
        p_web.SetSessionPicture('res:Date_Ordered',p_web.site.DatePicture)
        p_web.SetSessionPicture('res:Date_Received',p_web.site.DatePicture)
        p_web.SetSessionPicture('res:Despatch_Date',p_web.site.DatePicture)
        p_web.SetSessionPicture('res:GRNNumber','@p<<<<<<<<#p')
        p_web.SetSessionPicture('res:DateReceived',p_web.site.DatePicture)
    End
    If p_File &= rtnawait
        p_web.SetSessionPicture('rta:DateCreated',p_web.site.DatePicture)
        p_web.SetSessionPicture('rta:TimeCreated','@t7')
        p_web.SetSessionPicture('rta:DateProcessed',p_web.site.DatePicture)
        p_web.SetSessionPicture('rta:TimeProcessed','@t7')
    End
    If p_File &= rtnorder
        p_web.SetSessionPicture('rtn:DateCreated',p_web.site.DatePicture)
        p_web.SetSessionPicture('rtn:TimeCreated','@t7')
        p_web.SetSessionPicture('rtn:DateOrdered',p_web.site.DatePicture)
        p_web.SetSessionPicture('rtn:TimeOrdered','@t7')
        p_web.SetSessionPicture('rtn:DateReceived',p_web.site.DatePicture)
        p_web.SetSessionPicture('rtn:TimeReceived','@t7')
    End
    If p_File &= sbo_outparts
        p_web.SetSessionPicture('sout:DateRaised',p_web.site.DatePicture)
        p_web.SetSessionPicture('sout:DateProcessed',p_web.site.DatePicture)
    End
    If p_File &= smsmail
        p_web.SetSessionPicture('sms:DateInserted',p_web.site.DatePicture)
        p_web.SetSessionPicture('sms:TimeInserted','@t1b')
        p_web.SetSessionPicture('sms:DateSMSSent',p_web.site.DatePicture)
        p_web.SetSessionPicture('sms:TimeSMSSent','@t1b')
        p_web.SetSessionPicture('sms:DateEmailSent',p_web.site.DatePicture)
        p_web.SetSessionPicture('sms:TimeEmailSent','@t1b')
    End
    If p_File &= smsrecvd
        p_web.SetSessionPicture('SMR:DateReceived',p_web.site.DatePicture)
        p_web.SetSessionPicture('SMR:TimeReceived','@t7')
    End
    If p_File &= smsrecvd_alias
        p_web.SetSessionPicture('SMR1:DateReceived',p_web.site.DatePicture)
        p_web.SetSessionPicture('SMR1:TimeReceived','@t7')
    End
    If p_File &= stock
        p_web.SetSessionPicture('sto:Superceeded_Ref_Number','@p<<<<<<<<#p')
        p_web.SetSessionPicture('sto:Pending_Ref_Number','@p<<<<<<<<#p')
        p_web.SetSessionPicture('sto:DateBooked',p_web.site.DatePicture)
    End
    If p_File &= stock_alias
        p_web.SetSessionPicture('sto_ali:Superceeded_Ref_Number','@p<<<<<<<<#p')
        p_web.SetSessionPicture('sto_ali:Pending_Ref_Number','@p<<<<<<<<#p')
        p_web.SetSessionPicture('sto_ali:DateBooked',p_web.site.DatePicture)
    End
    If p_File &= stockalx
        p_web.SetSessionPicture('STLX:RequestDate',p_web.site.DatePicture)
        p_web.SetSessionPicture('STLX:RequestTime','@t7')
        p_web.SetSessionPicture('STLX:AllocateDate',p_web.site.DatePicture)
        p_web.SetSessionPicture('STLX:AllocateTime','@t7')
    End
    If p_File &= stofault
        p_web.SetSessionPicture('stf:TheDate',p_web.site.DatePicture)
        p_web.SetSessionPicture('stf:TheTime','@t1b')
    End
    If p_File &= stohist
        p_web.SetSessionPicture('shi:Date',p_web.site.DatePicture)
    End
    If p_File &= stohiste
        p_web.SetSessionPicture('stoe:HistTime','@t7')
    End
    If p_File &= stoparts
        p_web.SetSessionPicture('spt:DateChanged',p_web.site.DatePicture)
        p_web.SetSessionPicture('spt:TimeChanged','@t1b')
    End
    If p_File &= subtracc
        p_web.SetSessionPicture('sub:SatStartWorkHours','@t1b')
        p_web.SetSessionPicture('sub:SatEndWorkHours','@t1b')
        p_web.SetSessionPicture('sub:SunStartWorkHours','@t1b')
        p_web.SetSessionPicture('sub:SunEndWorkHours','@t1b')
    End
    If p_File &= tradeacc
        p_web.SetSessionPicture('tra:StartWorkHours','@t1b')
        p_web.SetSessionPicture('tra:EndWorkHours','@t1b')
        p_web.SetSessionPicture('tra:SatStartWorkHours','@t1b')
        p_web.SetSessionPicture('tra:SatEndWorkHours','@t1b')
        p_web.SetSessionPicture('tra:SunStartWorkHours','@t1b')
        p_web.SetSessionPicture('tra:SunEndWorkHours','@t1b')
    End
    If p_File &= tradeacc_alias
        p_web.SetSessionPicture('tra_ali:StartWorkHours','@t1b')
        p_web.SetSessionPicture('tra_ali:EndWorkHours','@t1b')
        p_web.SetSessionPicture('tra_ali:SatStartWorkHours','@t1b')
        p_web.SetSessionPicture('tra_ali:SatEndWorkHours','@t1b')
        p_web.SetSessionPicture('tra_ali:SunStartWorkHours','@t1b')
        p_web.SetSessionPicture('tra_ali:SunEndWorkHours','@t1b')
    End
    If p_File &= trdbatch
        p_web.SetSessionPicture('trb:Date',p_web.site.DatePicture)
        p_web.SetSessionPicture('trb:Time','@t1')
        p_web.SetSessionPicture('trb:DateReturn',p_web.site.DatePicture)
        p_web.SetSessionPicture('trb:TimeReturn','@t1b')
        p_web.SetSessionPicture('trb:DateDespatched',p_web.site.DatePicture)
        p_web.SetSessionPicture('trb:TimeDespatched','@t1b')
        p_web.SetSessionPicture('trb:ThirdPartyInvoiceDate',p_web.site.DatePicture)
    End
    If p_File &= users
        p_web.SetSessionPicture('use:Logged_in_date',p_web.site.DatePicture)
        p_web.SetSessionPicture('use:Logged_In_Time','@t1')
        p_web.SetSessionPicture('use:PasswordLastChanged',p_web.site.DatePicture)
    End
    If p_File &= users_alias
        p_web.SetSessionPicture('use_ali:Logged_in_date',p_web.site.DatePicture)
        p_web.SetSessionPicture('use_ali:Logged_In_Time','@t1')
        p_web.SetSessionPicture('use_ali:PasswordLastChanged',p_web.site.DatePicture)
    End
    If p_File &= warparts
        p_web.SetSessionPicture('wpr:Date_Ordered',p_web.site.DatePicture)
        p_web.SetSessionPicture('wpr:Order_Part_Number','@p<<<<<<<<#p')
        p_web.SetSessionPicture('wpr:Date_Received',p_web.site.DatePicture)
        p_web.SetSessionPicture('wpr:Status_Date',p_web.site.DatePicture)
    End
    If p_File &= warparts_alias
        p_web.SetSessionPicture('war_ali:Date_Ordered',p_web.site.DatePicture)
        p_web.SetSessionPicture('war_ali:Order_Part_Number','@p<<<<<<<<#p')
        p_web.SetSessionPicture('war_ali:Date_Received',p_web.site.DatePicture)
        p_web.SetSessionPicture('war_ali:Status_Date',p_web.site.DatePicture)
    End
    If p_File &= waybills
        p_web.SetSessionPicture('way:TheDate',p_web.site.DatePicture)
        p_web.SetSessionPicture('way:TheTime','@t1b')
    End
    If p_File &= webjob
        p_web.SetSessionPicture('wob:Current_Status_Date',p_web.site.DatePicture)
        p_web.SetSessionPicture('wob:Exchange_Status_Date',p_web.site.DatePicture)
        p_web.SetSessionPicture('wob:Loan_Status_Date',p_web.site.DatePicture)
        p_web.SetSessionPicture('wob:DateJobDespatched',p_web.site.DatePicture)
        p_web.SetSessionPicture('wob:DateBooked',p_web.site.DatePicture)
        p_web.SetSessionPicture('wob:TimeBooked','@t1b')
        p_web.SetSessionPicture('wob:DateCompleted',p_web.site.DatePicture)
        p_web.SetSessionPicture('wob:TimeCompleted','@t1b')
    End
! ----------------------------------------------------------------------------------------
NetWebSetPics PROCEDURE  (NetWebServerWorker p_web, FILE p_File)
  CODE
  ! Trying to move away from this code - let's see what happens - 4.26
  Return
  If p_FILE &= NULL then Return.
    if p_File &= audit
      if p_web.IfExistsValue('aud:Ref_Number') then p_web.SetPicture('aud:Ref_Number','@p<<<<<<<<#p').
      if p_web.IfExistsValue('aud:Date') then p_web.SetPicture('aud:Date',p_web.site.DatePicture).
      if p_web.IfExistsValue('aud:Time') then p_web.SetPicture('aud:Time','@t1').
    End
    if p_File &= audstats
      if p_web.IfExistsValue('aus:DateChanged') then p_web.SetPicture('aus:DateChanged',p_web.site.DatePicture).
      if p_web.IfExistsValue('aus:TimeChanged') then p_web.SetPicture('aus:TimeChanged','@t1b').
    End
    if p_File &= conthist
      if p_web.IfExistsValue('cht:Date') then p_web.SetPicture('cht:Date',p_web.site.DatePicture).
      if p_web.IfExistsValue('cht:Time') then p_web.SetPicture('cht:Time','@t1').
    End
    if p_File &= courier
      if p_web.IfExistsValue('cou:Last_Despatch_Time') then p_web.SetPicture('cou:Last_Despatch_Time','@t1').
      if p_web.IfExistsValue('cou:StartWorkHours') then p_web.SetPicture('cou:StartWorkHours','@t1b').
      if p_web.IfExistsValue('cou:EndWorkHours') then p_web.SetPicture('cou:EndWorkHours','@t1b').
      if p_web.IfExistsValue('cou:SatStartWorkHours') then p_web.SetPicture('cou:SatStartWorkHours','@t1b').
      if p_web.IfExistsValue('cou:SatEndWorkHours') then p_web.SetPicture('cou:SatEndWorkHours','@t1b').
      if p_web.IfExistsValue('cou:SunStartWorkHours') then p_web.SetPicture('cou:SunStartWorkHours','@t1b').
      if p_web.IfExistsValue('cou:SunEndWorkHours') then p_web.SetPicture('cou:SunEndWorkHours','@t1b').
    End
    if p_File &= default2
      if p_web.IfExistsValue('de2:PLE') then p_web.SetPicture('de2:PLE',p_web.site.DatePicture).
    End
    if p_File &= defaults
      if p_web.IfExistsValue('def:Start_Work_Hours') then p_web.SetPicture('def:Start_Work_Hours','@t1').
      if p_web.IfExistsValue('def:End_Work_Hours') then p_web.SetPicture('def:End_Work_Hours','@t1').
      if p_web.IfExistsValue('def:Job_Batch_Number') then p_web.SetPicture('def:Job_Batch_Number','@p<<<<<<<<#p').
    End
    if p_File &= estparts
      if p_web.IfExistsValue('epr:Date_Ordered') then p_web.SetPicture('epr:Date_Ordered',p_web.site.DatePicture).
      if p_web.IfExistsValue('epr:Date_Received') then p_web.SetPicture('epr:Date_Received',p_web.site.DatePicture).
      if p_web.IfExistsValue('epr:Status_Date') then p_web.SetPicture('epr:Status_Date',p_web.site.DatePicture).
    End
    if p_File &= exchacc
      if p_web.IfExistsValue('xca:Ref_Number') then p_web.SetPicture('xca:Ref_Number','@p<<<<<<<<#p').
    End
    if p_File &= exchamf
      if p_web.IfExistsValue('emf:Date') then p_web.SetPicture('emf:Date',p_web.site.DatePicture).
    End
    if p_File &= exchange
      if p_web.IfExistsValue('xch:Date_Booked') then p_web.SetPicture('xch:Date_Booked',p_web.site.DatePicture).
      if p_web.IfExistsValue('xch:StatusChangeDate') then p_web.SetPicture('xch:StatusChangeDate',p_web.site.DatePicture).
    End
    if p_File &= exchange_alias
      if p_web.IfExistsValue('xch_ali:Date_Booked') then p_web.SetPicture('xch_ali:Date_Booked',p_web.site.DatePicture).
      if p_web.IfExistsValue('xch_ali:StatusChangeDate') then p_web.SetPicture('xch_ali:StatusChangeDate',p_web.site.DatePicture).
    End
    if p_File &= exchhist
      if p_web.IfExistsValue('exh:Date') then p_web.SetPicture('exh:Date',p_web.site.DatePicture).
      if p_web.IfExistsValue('exh:Time') then p_web.SetPicture('exh:Time','@T1').
    End
    if p_File &= exchor48
      if p_web.IfExistsValue('ex4:DateCreated') then p_web.SetPicture('ex4:DateCreated',p_web.site.DatePicture).
      if p_web.IfExistsValue('ex4:TimeCreated') then p_web.SetPicture('ex4:TimeCreated','@t1b').
      if p_web.IfExistsValue('ex4:DateOrdered') then p_web.SetPicture('ex4:DateOrdered',p_web.site.DatePicture).
      if p_web.IfExistsValue('ex4:TimeOrdered') then p_web.SetPicture('ex4:TimeOrdered','@t1b').
    End
    if p_File &= exchordr
      if p_web.IfExistsValue('exo:DateCreated') then p_web.SetPicture('exo:DateCreated',p_web.site.DatePicture).
      if p_web.IfExistsValue('exo:DateOrdered') then p_web.SetPicture('exo:DateOrdered',p_web.site.DatePicture).
      if p_web.IfExistsValue('exo:DateReceived') then p_web.SetPicture('exo:DateReceived',p_web.site.DatePicture).
    End
    if p_File &= grnotesr
      if p_web.IfExistsValue('grr:Goods_Received_Number') then p_web.SetPicture('grr:Goods_Received_Number','@p<<<<<<<<#p').
      if p_web.IfExistsValue('grr:Order_Number') then p_web.SetPicture('grr:Order_Number','@p<<<<<<<<#p').
      if p_web.IfExistsValue('grr:Goods_Received_Date') then p_web.SetPicture('grr:Goods_Received_Date',p_web.site.DatePicture).
    End
    if p_File &= invoice
      if p_web.IfExistsValue('inv:Invoice_Number') then p_web.SetPicture('inv:Invoice_Number','@p<<<<<<<<#p').
      if p_web.IfExistsValue('inv:Job_Number') then p_web.SetPicture('inv:Job_Number','@p<<<<<<<<#p').
      if p_web.IfExistsValue('inv:Date_Created') then p_web.SetPicture('inv:Date_Created',p_web.site.DatePicture).
      if p_web.IfExistsValue('inv:Reconciled_Date') then p_web.SetPicture('inv:Reconciled_Date',p_web.site.DatePicture).
      if p_web.IfExistsValue('inv:ExportedOracleDate') then p_web.SetPicture('inv:ExportedOracleDate',p_web.site.DatePicture).
      if p_web.IfExistsValue('inv:RRCInvoiceDate') then p_web.SetPicture('inv:RRCInvoiceDate',p_web.site.DatePicture).
      if p_web.IfExistsValue('inv:ARCInvoiceDate') then p_web.SetPicture('inv:ARCInvoiceDate',p_web.site.DatePicture).
    End
    if p_File &= jobacc
      if p_web.IfExistsValue('jac:Ref_Number') then p_web.SetPicture('jac:Ref_Number','@p<<<<<<<<#p').
    End
    if p_File &= jobexacc
      if p_web.IfExistsValue('jea:Job_Ref_Number') then p_web.SetPicture('jea:Job_Ref_Number','@p<<<<<<<<#p').
      if p_web.IfExistsValue('jea:Stock_Ref_Number') then p_web.SetPicture('jea:Stock_Ref_Number','@p<<<<<<<<#p').
    End
    if p_File &= jobpaymt
      if p_web.IfExistsValue('jpt:Ref_Number') then p_web.SetPicture('jpt:Ref_Number','@p<<<<<<<p').
      if p_web.IfExistsValue('jpt:Date') then p_web.SetPicture('jpt:Date',p_web.site.DatePicture).
      if p_web.IfExistsValue('jpt:Expiry_Date') then p_web.SetPicture('jpt:Expiry_Date','@p##/##p').
    End
    if p_File &= jobpaymt_alias
      if p_web.IfExistsValue('jpt_ali:Ref_Number') then p_web.SetPicture('jpt_ali:Ref_Number','@p<<<<<<<p').
      if p_web.IfExistsValue('jpt_ali:Date') then p_web.SetPicture('jpt_ali:Date',p_web.site.DatePicture).
      if p_web.IfExistsValue('jpt_ali:Expiry_Date') then p_web.SetPicture('jpt_ali:Expiry_Date','@p##/##p').
    End
    if p_File &= jobrpnot
      if p_web.IfExistsValue('jrn:TheDate') then p_web.SetPicture('jrn:TheDate',p_web.site.DatePicture).
      if p_web.IfExistsValue('jrn:TheTime') then p_web.SetPicture('jrn:TheTime','@t1b').
    End
    if p_File &= jobs
      if p_web.IfExistsValue('job:date_booked') then p_web.SetPicture('job:date_booked',p_web.site.DatePicture).
      if p_web.IfExistsValue('job:time_booked') then p_web.SetPicture('job:time_booked','@t1').
      if p_web.IfExistsValue('job:DOP') then p_web.SetPicture('job:DOP',p_web.site.DatePicture).
      if p_web.IfExistsValue('job:Date_In_Repair') then p_web.SetPicture('job:Date_In_Repair',p_web.site.DatePicture).
      if p_web.IfExistsValue('job:Time_In_Repair') then p_web.SetPicture('job:Time_In_Repair','@t1b').
      if p_web.IfExistsValue('job:Date_On_Test') then p_web.SetPicture('job:Date_On_Test',p_web.site.DatePicture).
      if p_web.IfExistsValue('job:Time_On_Test') then p_web.SetPicture('job:Time_On_Test','@t1b').
      if p_web.IfExistsValue('job:Date_QA_Passed') then p_web.SetPicture('job:Date_QA_Passed',p_web.site.DatePicture).
      if p_web.IfExistsValue('job:Time_QA_Passed') then p_web.SetPicture('job:Time_QA_Passed','@t1b').
      if p_web.IfExistsValue('job:Date_QA_Rejected') then p_web.SetPicture('job:Date_QA_Rejected',p_web.site.DatePicture).
      if p_web.IfExistsValue('job:Time_QA_Rejected') then p_web.SetPicture('job:Time_QA_Rejected','@t1b').
      if p_web.IfExistsValue('job:Date_QA_Second_Passed') then p_web.SetPicture('job:Date_QA_Second_Passed',p_web.site.DatePicture).
      if p_web.IfExistsValue('job:Time_QA_Second_Passed') then p_web.SetPicture('job:Time_QA_Second_Passed','@t1b').
      if p_web.IfExistsValue('job:Date_Completed') then p_web.SetPicture('job:Date_Completed',p_web.site.DatePicture).
      if p_web.IfExistsValue('job:Time_Completed') then p_web.SetPicture('job:Time_Completed','@t1').
      if p_web.IfExistsValue('job:Date_Paid') then p_web.SetPicture('job:Date_Paid',p_web.site.DatePicture).
      if p_web.IfExistsValue('job:Loan_Issued_Date') then p_web.SetPicture('job:Loan_Issued_Date',p_web.site.DatePicture).
      if p_web.IfExistsValue('job:Loan_Despatched') then p_web.SetPicture('job:Loan_Despatched',p_web.site.DatePicture).
      if p_web.IfExistsValue('job:Exchange_Issued_Date') then p_web.SetPicture('job:Exchange_Issued_Date',p_web.site.DatePicture).
      if p_web.IfExistsValue('job:Exchange_Despatched') then p_web.SetPicture('job:Exchange_Despatched',p_web.site.DatePicture).
      if p_web.IfExistsValue('job:Date_Despatched') then p_web.SetPicture('job:Date_Despatched',p_web.site.DatePicture).
      if p_web.IfExistsValue('job:Incoming_Date') then p_web.SetPicture('job:Incoming_Date',p_web.site.DatePicture).
      if p_web.IfExistsValue('job:ThirdPartyDateDesp') then p_web.SetPicture('job:ThirdPartyDateDesp',p_web.site.DatePicture).
      if p_web.IfExistsValue('job:Status_End_Date') then p_web.SetPicture('job:Status_End_Date',p_web.site.DatePicture).
      if p_web.IfExistsValue('job:Status_End_Time') then p_web.SetPicture('job:Status_End_Time','@t1').
      if p_web.IfExistsValue('job:Turnaround_End_Date') then p_web.SetPicture('job:Turnaround_End_Date',p_web.site.DatePicture).
      if p_web.IfExistsValue('job:Turnaround_End_Time') then p_web.SetPicture('job:Turnaround_End_Time','@t1').
      if p_web.IfExistsValue('job:Invoice_Date') then p_web.SetPicture('job:Invoice_Date',p_web.site.DatePicture).
      if p_web.IfExistsValue('job:Invoice_Date_Warranty') then p_web.SetPicture('job:Invoice_Date_Warranty',p_web.site.DatePicture).
    End
    if p_File &= jobs2_alias
      if p_web.IfExistsValue('job2:date_booked') then p_web.SetPicture('job2:date_booked',p_web.site.DatePicture).
      if p_web.IfExistsValue('job2:time_booked') then p_web.SetPicture('job2:time_booked','@t1').
      if p_web.IfExistsValue('job2:DOP') then p_web.SetPicture('job2:DOP',p_web.site.DatePicture).
      if p_web.IfExistsValue('job2:Date_In_Repair') then p_web.SetPicture('job2:Date_In_Repair',p_web.site.DatePicture).
      if p_web.IfExistsValue('job2:Time_In_Repair') then p_web.SetPicture('job2:Time_In_Repair','@t1b').
      if p_web.IfExistsValue('job2:Date_On_Test') then p_web.SetPicture('job2:Date_On_Test',p_web.site.DatePicture).
      if p_web.IfExistsValue('job2:Time_On_Test') then p_web.SetPicture('job2:Time_On_Test','@t1b').
      if p_web.IfExistsValue('job2:Date_QA_Passed') then p_web.SetPicture('job2:Date_QA_Passed',p_web.site.DatePicture).
      if p_web.IfExistsValue('job2:Time_QA_Passed') then p_web.SetPicture('job2:Time_QA_Passed','@t1b').
      if p_web.IfExistsValue('job2:Date_QA_Rejected') then p_web.SetPicture('job2:Date_QA_Rejected',p_web.site.DatePicture).
      if p_web.IfExistsValue('job2:Time_QA_Rejected') then p_web.SetPicture('job2:Time_QA_Rejected','@t1b').
      if p_web.IfExistsValue('job2:Date_QA_Second_Passed') then p_web.SetPicture('job2:Date_QA_Second_Passed',p_web.site.DatePicture).
      if p_web.IfExistsValue('job2:Time_QA_Second_Passed') then p_web.SetPicture('job2:Time_QA_Second_Passed','@t1b').
      if p_web.IfExistsValue('job2:Date_Completed') then p_web.SetPicture('job2:Date_Completed',p_web.site.DatePicture).
      if p_web.IfExistsValue('job2:Time_Completed') then p_web.SetPicture('job2:Time_Completed','@t1').
      if p_web.IfExistsValue('job2:Date_Paid') then p_web.SetPicture('job2:Date_Paid',p_web.site.DatePicture).
      if p_web.IfExistsValue('job2:Loan_Issued_Date') then p_web.SetPicture('job2:Loan_Issued_Date',p_web.site.DatePicture).
      if p_web.IfExistsValue('job2:Loan_Despatched') then p_web.SetPicture('job2:Loan_Despatched',p_web.site.DatePicture).
      if p_web.IfExistsValue('job2:Exchange_Issued_Date') then p_web.SetPicture('job2:Exchange_Issued_Date',p_web.site.DatePicture).
      if p_web.IfExistsValue('job2:Exchange_Despatched') then p_web.SetPicture('job2:Exchange_Despatched',p_web.site.DatePicture).
      if p_web.IfExistsValue('job2:Date_Despatched') then p_web.SetPicture('job2:Date_Despatched',p_web.site.DatePicture).
      if p_web.IfExistsValue('job2:Incoming_Date') then p_web.SetPicture('job2:Incoming_Date',p_web.site.DatePicture).
      if p_web.IfExistsValue('job2:ThirdPartyDateDesp') then p_web.SetPicture('job2:ThirdPartyDateDesp',p_web.site.DatePicture).
      if p_web.IfExistsValue('job2:Status_End_Date') then p_web.SetPicture('job2:Status_End_Date',p_web.site.DatePicture).
      if p_web.IfExistsValue('job2:Status_End_Time') then p_web.SetPicture('job2:Status_End_Time','@t1').
      if p_web.IfExistsValue('job2:Turnaround_End_Date') then p_web.SetPicture('job2:Turnaround_End_Date',p_web.site.DatePicture).
      if p_web.IfExistsValue('job2:Turnaround_End_Time') then p_web.SetPicture('job2:Turnaround_End_Time','@t1').
      if p_web.IfExistsValue('job2:Invoice_Date') then p_web.SetPicture('job2:Invoice_Date',p_web.site.DatePicture).
      if p_web.IfExistsValue('job2:Invoice_Date_Warranty') then p_web.SetPicture('job2:Invoice_Date_Warranty',p_web.site.DatePicture).
    End
    if p_File &= jobs_alias
      if p_web.IfExistsValue('job_ali:date_booked') then p_web.SetPicture('job_ali:date_booked',p_web.site.DatePicture).
      if p_web.IfExistsValue('job_ali:time_booked') then p_web.SetPicture('job_ali:time_booked','@t1').
      if p_web.IfExistsValue('job_ali:DOP') then p_web.SetPicture('job_ali:DOP',p_web.site.DatePicture).
      if p_web.IfExistsValue('job_ali:Date_In_Repair') then p_web.SetPicture('job_ali:Date_In_Repair',p_web.site.DatePicture).
      if p_web.IfExistsValue('job_ali:Time_In_Repair') then p_web.SetPicture('job_ali:Time_In_Repair','@t1b').
      if p_web.IfExistsValue('job_ali:Date_On_Test') then p_web.SetPicture('job_ali:Date_On_Test',p_web.site.DatePicture).
      if p_web.IfExistsValue('job_ali:Time_On_Test') then p_web.SetPicture('job_ali:Time_On_Test','@t1b').
      if p_web.IfExistsValue('job_ali:Date_QA_Passed') then p_web.SetPicture('job_ali:Date_QA_Passed',p_web.site.DatePicture).
      if p_web.IfExistsValue('job_ali:Time_QA_Passed') then p_web.SetPicture('job_ali:Time_QA_Passed','@t1b').
      if p_web.IfExistsValue('job_ali:Date_QA_Rejected') then p_web.SetPicture('job_ali:Date_QA_Rejected',p_web.site.DatePicture).
      if p_web.IfExistsValue('job_ali:Time_QA_Rejected') then p_web.SetPicture('job_ali:Time_QA_Rejected','@t1b').
      if p_web.IfExistsValue('job_ali:Date_QA_Second_Passed') then p_web.SetPicture('job_ali:Date_QA_Second_Passed',p_web.site.DatePicture).
      if p_web.IfExistsValue('job_ali:Time_QA_Second_Passed') then p_web.SetPicture('job_ali:Time_QA_Second_Passed','@t1b').
      if p_web.IfExistsValue('job_ali:Date_Completed') then p_web.SetPicture('job_ali:Date_Completed',p_web.site.DatePicture).
      if p_web.IfExistsValue('job_ali:Time_Completed') then p_web.SetPicture('job_ali:Time_Completed','@t1').
      if p_web.IfExistsValue('job_ali:Date_Paid') then p_web.SetPicture('job_ali:Date_Paid',p_web.site.DatePicture).
      if p_web.IfExistsValue('job_ali:Loan_Issued_Date') then p_web.SetPicture('job_ali:Loan_Issued_Date',p_web.site.DatePicture).
      if p_web.IfExistsValue('job_ali:Loan_Despatched') then p_web.SetPicture('job_ali:Loan_Despatched',p_web.site.DatePicture).
      if p_web.IfExistsValue('job_ali:Exchange_Issued_Date') then p_web.SetPicture('job_ali:Exchange_Issued_Date',p_web.site.DatePicture).
      if p_web.IfExistsValue('job_ali:Exchange_Despatched') then p_web.SetPicture('job_ali:Exchange_Despatched',p_web.site.DatePicture).
      if p_web.IfExistsValue('job_ali:Date_Despatched') then p_web.SetPicture('job_ali:Date_Despatched',p_web.site.DatePicture).
      if p_web.IfExistsValue('job_ali:Incoming_Date') then p_web.SetPicture('job_ali:Incoming_Date',p_web.site.DatePicture).
      if p_web.IfExistsValue('job_ali:ThirdPartyDateDesp') then p_web.SetPicture('job_ali:ThirdPartyDateDesp',p_web.site.DatePicture).
      if p_web.IfExistsValue('job_ali:Status_End_Date') then p_web.SetPicture('job_ali:Status_End_Date',p_web.site.DatePicture).
      if p_web.IfExistsValue('job_ali:Status_End_Time') then p_web.SetPicture('job_ali:Status_End_Time','@t1').
      if p_web.IfExistsValue('job_ali:Turnaround_End_Date') then p_web.SetPicture('job_ali:Turnaround_End_Date',p_web.site.DatePicture).
      if p_web.IfExistsValue('job_ali:Turnaround_End_Time') then p_web.SetPicture('job_ali:Turnaround_End_Time','@t1').
      if p_web.IfExistsValue('job_ali:Invoice_Date') then p_web.SetPicture('job_ali:Invoice_Date',p_web.site.DatePicture).
      if p_web.IfExistsValue('job_ali:Invoice_Date_Warranty') then p_web.SetPicture('job_ali:Invoice_Date_Warranty',p_web.site.DatePicture).
    End
    if p_File &= jobscons
      if p_web.IfExistsValue('joc:TheDate') then p_web.SetPicture('joc:TheDate',p_web.site.DatePicture).
      if p_web.IfExistsValue('joc:TheTime') then p_web.SetPicture('joc:TheTime','@t1b').
    End
    if p_File &= jobse
      if p_web.IfExistsValue('jobe:HubRepairDate') then p_web.SetPicture('jobe:HubRepairDate',p_web.site.DatePicture).
      if p_web.IfExistsValue('jobe:HubRepairTime') then p_web.SetPicture('jobe:HubRepairTime','@t1b').
      if p_web.IfExistsValue('jobe:ReturnDate') then p_web.SetPicture('jobe:ReturnDate',p_web.site.DatePicture).
      if p_web.IfExistsValue('jobe:OBFvalidateDate') then p_web.SetPicture('jobe:OBFvalidateDate',p_web.site.DatePicture).
      if p_web.IfExistsValue('jobe:OBFvalidateTime') then p_web.SetPicture('jobe:OBFvalidateTime','@t1').
      if p_web.IfExistsValue('jobe:WarrantyStatusDate') then p_web.SetPicture('jobe:WarrantyStatusDate',p_web.site.DatePicture).
      if p_web.IfExistsValue('jobe:ARC3rdPartyInvoiceDate') then p_web.SetPicture('jobe:ARC3rdPartyInvoiceDate',p_web.site.DatePicture).
    End
    if p_File &= jobse2
      if p_web.IfExistsValue('jobe2:InPendingDate') then p_web.SetPicture('jobe2:InPendingDate',p_web.site.DatePicture).
      if p_web.IfExistsValue('jobe2:DateReceivedAtPUP') then p_web.SetPicture('jobe2:DateReceivedAtPUP',p_web.site.DatePicture).
      if p_web.IfExistsValue('jobe2:TimeReceivedAtPUP') then p_web.SetPicture('jobe2:TimeReceivedAtPUP','@t1b').
      if p_web.IfExistsValue('jobe2:DateDespatchFromPUP') then p_web.SetPicture('jobe2:DateDespatchFromPUP',p_web.site.DatePicture).
      if p_web.IfExistsValue('jobe2:TimeDespatchFromPUP') then p_web.SetPicture('jobe2:TimeDespatchFromPUP','@t1b').
      if p_web.IfExistsValue('jobe2:SMSDate') then p_web.SetPicture('jobe2:SMSDate',p_web.site.DatePicture).
    End
    if p_File &= jobse_alias
      if p_web.IfExistsValue('jobe_ali:HubRepairDate') then p_web.SetPicture('jobe_ali:HubRepairDate',p_web.site.DatePicture).
      if p_web.IfExistsValue('jobe_ali:HubRepairTime') then p_web.SetPicture('jobe_ali:HubRepairTime','@t1b').
      if p_web.IfExistsValue('jobe_ali:ReturnDate') then p_web.SetPicture('jobe_ali:ReturnDate',p_web.site.DatePicture).
      if p_web.IfExistsValue('jobe_ali:OBFvalidateDate') then p_web.SetPicture('jobe_ali:OBFvalidateDate',p_web.site.DatePicture).
      if p_web.IfExistsValue('jobe_ali:OBFvalidateTime') then p_web.SetPicture('jobe_ali:OBFvalidateTime','@t1').
      if p_web.IfExistsValue('jobe_ali:WarrantyStatusDate') then p_web.SetPicture('jobe_ali:WarrantyStatusDate',p_web.site.DatePicture).
      if p_web.IfExistsValue('jobe_ali:ARC3rdPartyInvoiceDate') then p_web.SetPicture('jobe_ali:ARC3rdPartyInvoiceDate',p_web.site.DatePicture).
    End
    if p_File &= jobseng
      if p_web.IfExistsValue('joe:DateAllocated') then p_web.SetPicture('joe:DateAllocated',p_web.site.DatePicture).
      if p_web.IfExistsValue('joe:StatusDate') then p_web.SetPicture('joe:StatusDate',p_web.site.DatePicture).
      if p_web.IfExistsValue('joe:StatusTime') then p_web.SetPicture('joe:StatusTime','@t1b').
    End
    if p_File &= jobsinv
      if p_web.IfExistsValue('jov:DateCreated') then p_web.SetPicture('jov:DateCreated',p_web.site.DatePicture).
      if p_web.IfExistsValue('jov:TimeCreated') then p_web.SetPicture('jov:TimeCreated','@t1b').
    End
    if p_File &= jobslock
      if p_web.IfExistsValue('lock:DateLocked') then p_web.SetPicture('lock:DateLocked',p_web.site.DatePicture).
      if p_web.IfExistsValue('lock:TimeLocked') then p_web.SetPicture('lock:TimeLocked','@t7').
    End
    if p_File &= jobsobf
      if p_web.IfExistsValue('jof:DateCompleted') then p_web.SetPicture('jof:DateCompleted',p_web.site.DatePicture).
      if p_web.IfExistsValue('jof:TimeCompleted') then p_web.SetPicture('jof:TimeCompleted','@t1b').
      if p_web.IfExistsValue('jof:DateProcessed') then p_web.SetPicture('jof:DateProcessed',p_web.site.DatePicture).
      if p_web.IfExistsValue('jof:TimeProcessed') then p_web.SetPicture('jof:TimeProcessed','@t1b').
    End
    if p_File &= jobstage
      if p_web.IfExistsValue('jst:Ref_Number') then p_web.SetPicture('jst:Ref_Number','@p<<<<<<<<#p').
      if p_web.IfExistsValue('jst:Date') then p_web.SetPicture('jst:Date',p_web.site.DatePicture).
      if p_web.IfExistsValue('jst:Time') then p_web.SetPicture('jst:Time','@t1').
    End
    if p_File &= jobstamp
      if p_web.IfExistsValue('jos:DateStamp') then p_web.SetPicture('jos:DateStamp',p_web.site.DatePicture).
      if p_web.IfExistsValue('jos:TimeStamp') then p_web.SetPicture('jos:TimeStamp','@t1b').
    End
    if p_File &= jobswarr
      if p_web.IfExistsValue('jow:ClaimSubmitted') then p_web.SetPicture('jow:ClaimSubmitted',p_web.site.DatePicture).
      if p_web.IfExistsValue('jow:DateAccepted') then p_web.SetPicture('jow:DateAccepted',p_web.site.DatePicture).
      if p_web.IfExistsValue('jow:DateReconciled') then p_web.SetPicture('jow:DateReconciled',p_web.site.DatePicture).
      if p_web.IfExistsValue('jow:RRCDateReconciled') then p_web.SetPicture('jow:RRCDateReconciled',p_web.site.DatePicture).
      if p_web.IfExistsValue('jow:DateRejected') then p_web.SetPicture('jow:DateRejected',p_web.site.DatePicture).
      if p_web.IfExistsValue('jow:DateFinalRejection') then p_web.SetPicture('jow:DateFinalRejection',p_web.site.DatePicture).
      if p_web.IfExistsValue('jow:Orig_Sub_Date') then p_web.SetPicture('jow:Orig_Sub_Date',p_web.site.DatePicture).
    End
    if p_File &= jobthird
      if p_web.IfExistsValue('jot:DateOut') then p_web.SetPicture('jot:DateOut',p_web.site.DatePicture).
      if p_web.IfExistsValue('jot:DateDespatched') then p_web.SetPicture('jot:DateDespatched',p_web.site.DatePicture).
      if p_web.IfExistsValue('jot:DateIn') then p_web.SetPicture('jot:DateIn',p_web.site.DatePicture).
    End
    if p_File &= loan
      if p_web.IfExistsValue('loa:Date_Booked') then p_web.SetPicture('loa:Date_Booked',p_web.site.DatePicture).
      if p_web.IfExistsValue('loa:StatusChangeDate') then p_web.SetPicture('loa:StatusChangeDate',p_web.site.DatePicture).
    End
    if p_File &= loan_alias
      if p_web.IfExistsValue('loa_ali:Date_Booked') then p_web.SetPicture('loa_ali:Date_Booked',p_web.site.DatePicture).
      if p_web.IfExistsValue('loa_ali:StatusChangeDate') then p_web.SetPicture('loa_ali:StatusChangeDate',p_web.site.DatePicture).
    End
    if p_File &= loanacc
      if p_web.IfExistsValue('lac:Ref_Number') then p_web.SetPicture('lac:Ref_Number','@p<<<<<<<<#p').
    End
    if p_File &= loanhist
      if p_web.IfExistsValue('loh:Ref_Number') then p_web.SetPicture('loh:Ref_Number','@p<<<<<<<<#p').
      if p_web.IfExistsValue('loh:Date') then p_web.SetPicture('loh:Date',p_web.site.DatePicture).
      if p_web.IfExistsValue('loh:Time') then p_web.SetPicture('loh:Time','@T1').
    End
    if p_File &= loaordr
      if p_web.IfExistsValue('lor:DateCreated') then p_web.SetPicture('lor:DateCreated',p_web.site.DatePicture).
      if p_web.IfExistsValue('lor:DateOrdered') then p_web.SetPicture('lor:DateOrdered',p_web.site.DatePicture).
      if p_web.IfExistsValue('lor:DateReceived') then p_web.SetPicture('lor:DateReceived',p_web.site.DatePicture).
    End
    if p_File &= locatlog
      if p_web.IfExistsValue('lot:TheDate') then p_web.SetPicture('lot:TheDate',p_web.site.DatePicture).
      if p_web.IfExistsValue('lot:TheTime') then p_web.SetPicture('lot:TheTime','@t1b').
    End
    if p_File &= manufact
      if p_web.IfExistsValue('man:SiemensDate') then p_web.SetPicture('man:SiemensDate',p_web.site.DatePicture).
    End
    if p_File &= ordhead
      if p_web.IfExistsValue('orh:thedate') then p_web.SetPicture('orh:thedate',p_web.site.DatePicture).
      if p_web.IfExistsValue('orh:thetime') then p_web.SetPicture('orh:thetime','@t1').
      if p_web.IfExistsValue('orh:pro_date') then p_web.SetPicture('orh:pro_date',p_web.site.DatePicture).
      if p_web.IfExistsValue('orh:DateDespatched') then p_web.SetPicture('orh:DateDespatched',p_web.site.DatePicture).
    End
    if p_File &= parts
      if p_web.IfExistsValue('par:Date_Ordered') then p_web.SetPicture('par:Date_Ordered',p_web.site.DatePicture).
      if p_web.IfExistsValue('par:Date_Received') then p_web.SetPicture('par:Date_Received',p_web.site.DatePicture).
      if p_web.IfExistsValue('par:Status_Date') then p_web.SetPicture('par:Status_Date',p_web.site.DatePicture).
    End
    if p_File &= parts_alias
      if p_web.IfExistsValue('par_ali:Date_Ordered') then p_web.SetPicture('par_ali:Date_Ordered',p_web.site.DatePicture).
      if p_web.IfExistsValue('par_ali:Date_Received') then p_web.SetPicture('par_ali:Date_Received',p_web.site.DatePicture).
      if p_web.IfExistsValue('par_ali:Status_Date') then p_web.SetPicture('par_ali:Status_Date',p_web.site.DatePicture).
    End
    if p_File &= retsales
      if p_web.IfExistsValue('ret:date_booked') then p_web.SetPicture('ret:date_booked',p_web.site.DatePicture).
      if p_web.IfExistsValue('ret:time_booked') then p_web.SetPicture('ret:time_booked','@t1').
      if p_web.IfExistsValue('ret:Date_Despatched') then p_web.SetPicture('ret:Date_Despatched',p_web.site.DatePicture).
      if p_web.IfExistsValue('ret:Invoice_Date') then p_web.SetPicture('ret:Invoice_Date',p_web.site.DatePicture).
      if p_web.IfExistsValue('ret:WebDateCreated') then p_web.SetPicture('ret:WebDateCreated',p_web.site.DatePicture).
      if p_web.IfExistsValue('ret:DatePickingNotePrinted') then p_web.SetPicture('ret:DatePickingNotePrinted',p_web.site.DatePicture).
      if p_web.IfExistsValue('ret:TimePickingNotePrinted') then p_web.SetPicture('ret:TimePickingNotePrinted','@t1b').
    End
    if p_File &= retstock
      if p_web.IfExistsValue('res:Date_Ordered') then p_web.SetPicture('res:Date_Ordered',p_web.site.DatePicture).
      if p_web.IfExistsValue('res:Date_Received') then p_web.SetPicture('res:Date_Received',p_web.site.DatePicture).
      if p_web.IfExistsValue('res:Despatch_Date') then p_web.SetPicture('res:Despatch_Date',p_web.site.DatePicture).
      if p_web.IfExistsValue('res:GRNNumber') then p_web.SetPicture('res:GRNNumber','@p<<<<<<<<#p').
      if p_web.IfExistsValue('res:DateReceived') then p_web.SetPicture('res:DateReceived',p_web.site.DatePicture).
    End
    if p_File &= rtnawait
      if p_web.IfExistsValue('rta:DateCreated') then p_web.SetPicture('rta:DateCreated',p_web.site.DatePicture).
      if p_web.IfExistsValue('rta:TimeCreated') then p_web.SetPicture('rta:TimeCreated','@t7').
      if p_web.IfExistsValue('rta:DateProcessed') then p_web.SetPicture('rta:DateProcessed',p_web.site.DatePicture).
      if p_web.IfExistsValue('rta:TimeProcessed') then p_web.SetPicture('rta:TimeProcessed','@t7').
    End
    if p_File &= rtnorder
      if p_web.IfExistsValue('rtn:DateCreated') then p_web.SetPicture('rtn:DateCreated',p_web.site.DatePicture).
      if p_web.IfExistsValue('rtn:TimeCreated') then p_web.SetPicture('rtn:TimeCreated','@t7').
      if p_web.IfExistsValue('rtn:DateOrdered') then p_web.SetPicture('rtn:DateOrdered',p_web.site.DatePicture).
      if p_web.IfExistsValue('rtn:TimeOrdered') then p_web.SetPicture('rtn:TimeOrdered','@t7').
      if p_web.IfExistsValue('rtn:DateReceived') then p_web.SetPicture('rtn:DateReceived',p_web.site.DatePicture).
      if p_web.IfExistsValue('rtn:TimeReceived') then p_web.SetPicture('rtn:TimeReceived','@t7').
    End
    if p_File &= sbo_outparts
      if p_web.IfExistsValue('sout:DateRaised') then p_web.SetPicture('sout:DateRaised',p_web.site.DatePicture).
      if p_web.IfExistsValue('sout:DateProcessed') then p_web.SetPicture('sout:DateProcessed',p_web.site.DatePicture).
    End
    if p_File &= smsmail
      if p_web.IfExistsValue('sms:DateInserted') then p_web.SetPicture('sms:DateInserted',p_web.site.DatePicture).
      if p_web.IfExistsValue('sms:TimeInserted') then p_web.SetPicture('sms:TimeInserted','@t1b').
      if p_web.IfExistsValue('sms:DateSMSSent') then p_web.SetPicture('sms:DateSMSSent',p_web.site.DatePicture).
      if p_web.IfExistsValue('sms:TimeSMSSent') then p_web.SetPicture('sms:TimeSMSSent','@t1b').
      if p_web.IfExistsValue('sms:DateEmailSent') then p_web.SetPicture('sms:DateEmailSent',p_web.site.DatePicture).
      if p_web.IfExistsValue('sms:TimeEmailSent') then p_web.SetPicture('sms:TimeEmailSent','@t1b').
    End
    if p_File &= smsrecvd
      if p_web.IfExistsValue('SMR:DateReceived') then p_web.SetPicture('SMR:DateReceived',p_web.site.DatePicture).
      if p_web.IfExistsValue('SMR:TimeReceived') then p_web.SetPicture('SMR:TimeReceived','@t7').
    End
    if p_File &= smsrecvd_alias
      if p_web.IfExistsValue('SMR1:DateReceived') then p_web.SetPicture('SMR1:DateReceived',p_web.site.DatePicture).
      if p_web.IfExistsValue('SMR1:TimeReceived') then p_web.SetPicture('SMR1:TimeReceived','@t7').
    End
    if p_File &= stock
      if p_web.IfExistsValue('sto:Superceeded_Ref_Number') then p_web.SetPicture('sto:Superceeded_Ref_Number','@p<<<<<<<<#p').
      if p_web.IfExistsValue('sto:Pending_Ref_Number') then p_web.SetPicture('sto:Pending_Ref_Number','@p<<<<<<<<#p').
      if p_web.IfExistsValue('sto:DateBooked') then p_web.SetPicture('sto:DateBooked',p_web.site.DatePicture).
    End
    if p_File &= stock_alias
      if p_web.IfExistsValue('sto_ali:Superceeded_Ref_Number') then p_web.SetPicture('sto_ali:Superceeded_Ref_Number','@p<<<<<<<<#p').
      if p_web.IfExistsValue('sto_ali:Pending_Ref_Number') then p_web.SetPicture('sto_ali:Pending_Ref_Number','@p<<<<<<<<#p').
      if p_web.IfExistsValue('sto_ali:DateBooked') then p_web.SetPicture('sto_ali:DateBooked',p_web.site.DatePicture).
    End
    if p_File &= stockalx
      if p_web.IfExistsValue('STLX:RequestDate') then p_web.SetPicture('STLX:RequestDate',p_web.site.DatePicture).
      if p_web.IfExistsValue('STLX:RequestTime') then p_web.SetPicture('STLX:RequestTime','@t7').
      if p_web.IfExistsValue('STLX:AllocateDate') then p_web.SetPicture('STLX:AllocateDate',p_web.site.DatePicture).
      if p_web.IfExistsValue('STLX:AllocateTime') then p_web.SetPicture('STLX:AllocateTime','@t7').
    End
    if p_File &= stofault
      if p_web.IfExistsValue('stf:TheDate') then p_web.SetPicture('stf:TheDate',p_web.site.DatePicture).
      if p_web.IfExistsValue('stf:TheTime') then p_web.SetPicture('stf:TheTime','@t1b').
    End
    if p_File &= stohist
      if p_web.IfExistsValue('shi:Date') then p_web.SetPicture('shi:Date',p_web.site.DatePicture).
    End
    if p_File &= stohiste
      if p_web.IfExistsValue('stoe:HistTime') then p_web.SetPicture('stoe:HistTime','@t7').
    End
    if p_File &= stoparts
      if p_web.IfExistsValue('spt:DateChanged') then p_web.SetPicture('spt:DateChanged',p_web.site.DatePicture).
      if p_web.IfExistsValue('spt:TimeChanged') then p_web.SetPicture('spt:TimeChanged','@t1b').
    End
    if p_File &= subtracc
      if p_web.IfExistsValue('sub:SatStartWorkHours') then p_web.SetPicture('sub:SatStartWorkHours','@t1b').
      if p_web.IfExistsValue('sub:SatEndWorkHours') then p_web.SetPicture('sub:SatEndWorkHours','@t1b').
      if p_web.IfExistsValue('sub:SunStartWorkHours') then p_web.SetPicture('sub:SunStartWorkHours','@t1b').
      if p_web.IfExistsValue('sub:SunEndWorkHours') then p_web.SetPicture('sub:SunEndWorkHours','@t1b').
    End
    if p_File &= tradeacc
      if p_web.IfExistsValue('tra:StartWorkHours') then p_web.SetPicture('tra:StartWorkHours','@t1b').
      if p_web.IfExistsValue('tra:EndWorkHours') then p_web.SetPicture('tra:EndWorkHours','@t1b').
      if p_web.IfExistsValue('tra:SatStartWorkHours') then p_web.SetPicture('tra:SatStartWorkHours','@t1b').
      if p_web.IfExistsValue('tra:SatEndWorkHours') then p_web.SetPicture('tra:SatEndWorkHours','@t1b').
      if p_web.IfExistsValue('tra:SunStartWorkHours') then p_web.SetPicture('tra:SunStartWorkHours','@t1b').
      if p_web.IfExistsValue('tra:SunEndWorkHours') then p_web.SetPicture('tra:SunEndWorkHours','@t1b').
    End
    if p_File &= tradeacc_alias
      if p_web.IfExistsValue('tra_ali:StartWorkHours') then p_web.SetPicture('tra_ali:StartWorkHours','@t1b').
      if p_web.IfExistsValue('tra_ali:EndWorkHours') then p_web.SetPicture('tra_ali:EndWorkHours','@t1b').
      if p_web.IfExistsValue('tra_ali:SatStartWorkHours') then p_web.SetPicture('tra_ali:SatStartWorkHours','@t1b').
      if p_web.IfExistsValue('tra_ali:SatEndWorkHours') then p_web.SetPicture('tra_ali:SatEndWorkHours','@t1b').
      if p_web.IfExistsValue('tra_ali:SunStartWorkHours') then p_web.SetPicture('tra_ali:SunStartWorkHours','@t1b').
      if p_web.IfExistsValue('tra_ali:SunEndWorkHours') then p_web.SetPicture('tra_ali:SunEndWorkHours','@t1b').
    End
    if p_File &= trdbatch
      if p_web.IfExistsValue('trb:Date') then p_web.SetPicture('trb:Date',p_web.site.DatePicture).
      if p_web.IfExistsValue('trb:Time') then p_web.SetPicture('trb:Time','@t1').
      if p_web.IfExistsValue('trb:DateReturn') then p_web.SetPicture('trb:DateReturn',p_web.site.DatePicture).
      if p_web.IfExistsValue('trb:TimeReturn') then p_web.SetPicture('trb:TimeReturn','@t1b').
      if p_web.IfExistsValue('trb:DateDespatched') then p_web.SetPicture('trb:DateDespatched',p_web.site.DatePicture).
      if p_web.IfExistsValue('trb:TimeDespatched') then p_web.SetPicture('trb:TimeDespatched','@t1b').
      if p_web.IfExistsValue('trb:ThirdPartyInvoiceDate') then p_web.SetPicture('trb:ThirdPartyInvoiceDate',p_web.site.DatePicture).
    End
    if p_File &= users
      if p_web.IfExistsValue('use:Logged_in_date') then p_web.SetPicture('use:Logged_in_date',p_web.site.DatePicture).
      if p_web.IfExistsValue('use:Logged_In_Time') then p_web.SetPicture('use:Logged_In_Time','@t1').
      if p_web.IfExistsValue('use:PasswordLastChanged') then p_web.SetPicture('use:PasswordLastChanged',p_web.site.DatePicture).
    End
    if p_File &= users_alias
      if p_web.IfExistsValue('use_ali:Logged_in_date') then p_web.SetPicture('use_ali:Logged_in_date',p_web.site.DatePicture).
      if p_web.IfExistsValue('use_ali:Logged_In_Time') then p_web.SetPicture('use_ali:Logged_In_Time','@t1').
      if p_web.IfExistsValue('use_ali:PasswordLastChanged') then p_web.SetPicture('use_ali:PasswordLastChanged',p_web.site.DatePicture).
    End
    if p_File &= warparts
      if p_web.IfExistsValue('wpr:Date_Ordered') then p_web.SetPicture('wpr:Date_Ordered',p_web.site.DatePicture).
      if p_web.IfExistsValue('wpr:Order_Part_Number') then p_web.SetPicture('wpr:Order_Part_Number','@p<<<<<<<<#p').
      if p_web.IfExistsValue('wpr:Date_Received') then p_web.SetPicture('wpr:Date_Received',p_web.site.DatePicture).
      if p_web.IfExistsValue('wpr:Status_Date') then p_web.SetPicture('wpr:Status_Date',p_web.site.DatePicture).
    End
    if p_File &= warparts_alias
      if p_web.IfExistsValue('war_ali:Date_Ordered') then p_web.SetPicture('war_ali:Date_Ordered',p_web.site.DatePicture).
      if p_web.IfExistsValue('war_ali:Order_Part_Number') then p_web.SetPicture('war_ali:Order_Part_Number','@p<<<<<<<<#p').
      if p_web.IfExistsValue('war_ali:Date_Received') then p_web.SetPicture('war_ali:Date_Received',p_web.site.DatePicture).
      if p_web.IfExistsValue('war_ali:Status_Date') then p_web.SetPicture('war_ali:Status_Date',p_web.site.DatePicture).
    End
    if p_File &= waybills
      if p_web.IfExistsValue('way:TheDate') then p_web.SetPicture('way:TheDate',p_web.site.DatePicture).
      if p_web.IfExistsValue('way:TheTime') then p_web.SetPicture('way:TheTime','@t1b').
    End
    if p_File &= webjob
      if p_web.IfExistsValue('wob:Current_Status_Date') then p_web.SetPicture('wob:Current_Status_Date',p_web.site.DatePicture).
      if p_web.IfExistsValue('wob:Exchange_Status_Date') then p_web.SetPicture('wob:Exchange_Status_Date',p_web.site.DatePicture).
      if p_web.IfExistsValue('wob:Loan_Status_Date') then p_web.SetPicture('wob:Loan_Status_Date',p_web.site.DatePicture).
      if p_web.IfExistsValue('wob:DateJobDespatched') then p_web.SetPicture('wob:DateJobDespatched',p_web.site.DatePicture).
      if p_web.IfExistsValue('wob:DateBooked') then p_web.SetPicture('wob:DateBooked',p_web.site.DatePicture).
      if p_web.IfExistsValue('wob:TimeBooked') then p_web.SetPicture('wob:TimeBooked','@t1b').
      if p_web.IfExistsValue('wob:DateCompleted') then p_web.SetPicture('wob:DateCompleted',p_web.site.DatePicture).
      if p_web.IfExistsValue('wob:TimeCompleted') then p_web.SetPicture('wob:TimeCompleted','@t1b').
    End
