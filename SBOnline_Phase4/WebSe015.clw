

   MEMBER('WebServer_Phase4.clw')                          ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPRPDF.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE
   INCLUDE('ABRPPSEL.INC'),ONCE

                     MAP
                       INCLUDE('WEBSE015.INC'),ONCE        !Local module procedure declarations
                     END


ExchangeOrder PROCEDURE (<NetWebServerWorker p_web>)       ! Generated from procedure template - Report

  ! The NetTalk Extension to report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
Loc:Html      String(1024)
Progress:Thermometer BYTE                                  !
locRecordNumber      LONG                                  !
Address:User_Name    STRING(30)                            !
address:Address_Line1 STRING(30)                           !
address:Address_Line2 STRING(30)                           !
address:Address_Line3 STRING(30)                           !
address:Post_Code    STRING(20)                            !
address:Telephone_Number STRING(20)                        !
address:Fax_No       STRING(20)                            !
address:Email        STRING(50)                            !
tmp:DOP              DATE                                  !
Process:View         VIEW(EXCHOR48)
                       PROJECT(ex4:JobNumber)
                       PROJECT(ex4:Manufacturer)
                       PROJECT(ex4:ModelNumber)
                       PROJECT(ex4:RecordNumber)
                     END
ProgressWindow       WINDOW('Report EXCHOR48'),AT(,,142,59),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY,DOUBLE
                       PROGRESS,USE(Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),FLAT,LEFT,MSG('Cancel Report'),TIP('Cancel Report'),ICON('WACANCEL.ICO')
                     END

Report               REPORT,AT(385,2865,7521,7344),PAPER(PAPER:A4),PRE(RPT),FONT('Tahoma',10,,FONT:regular),THOUS
                       HEADER,AT(385,531,7521,1802),USE(?unnamed:2)
                         STRING(@s30),AT(52,646),USE(address:Address_Line3),TRN,FONT('Tahoma',8,,,CHARSET:ANSI)
                         STRING(@s20),AT(52,833),USE(address:Post_Code),TRN,FONT('Tahoma',8,,,CHARSET:ANSI)
                         STRING('Date Printed:'),AT(4938,948),USE(?String16),TRN,FONT('Tahoma',8,,,CHARSET:ANSI)
                         STRING('Email:'),AT(52,1302),USE(?String33),TRN,FONT('Tahoma',8,COLOR:Black,FONT:regular,CHARSET:ANSI)
                         STRING(@s50),AT(365,1302),USE(address:Email),TRN,FONT('Tahoma',8,COLOR:Black,FONT:regular,CHARSET:ANSI)
                         STRING('<<-- Date Stamp -->'),AT(5677,938),USE(?ReportDateStamp),TRN,FONT('Tahoma',8,,FONT:bold)
                         STRING(@s20),AT(365,990),USE(address:Telephone_Number),TRN,FONT('Tahoma',8,,,CHARSET:ANSI)
                         STRING('Fax:'),AT(52,1146),USE(?String34),TRN,FONT('Tahoma',8,COLOR:Black,FONT:regular,CHARSET:ANSI)
                         STRING(@s20),AT(365,1146),USE(address:Fax_No),TRN,FONT('Tahoma',8,COLOR:Black,FONT:regular,CHARSET:ANSI)
                         STRING('Tel:'),AT(52,990),USE(?String32),TRN,FONT('Tahoma',8,COLOR:Black,FONT:regular,CHARSET:ANSI)
                         STRING(@s30),AT(52,271),USE(address:Address_Line1),TRN,FONT('Tahoma',8,,,CHARSET:ANSI)
                         STRING(@s30),AT(52,458),USE(address:Address_Line2),TRN,FONT('Tahoma',8,,,CHARSET:ANSI)
                         STRING(@s30),AT(52,0),USE(Address:User_Name),TRN,FONT('Tahoma',14,COLOR:Black,FONT:bold,CHARSET:ANSI)
                       END
detail1                DETAIL,AT(,,,167),USE(?unnamed:4)
                         STRING(@s30),AT(104,0),USE(ex4:Manufacturer),TRN,FONT('Tahoma',8,,,CHARSET:ANSI)
                         STRING(@s30),AT(2135,0,,208),USE(ex4:ModelNumber),TRN,FONT('Tahoma',8,,)
                         STRING('1'),AT(4427,0),USE(?String30),TRN,FONT(,8,,)
                         STRING(@s15),AT(4844,0),USE(ex4:JobNumber),TRN,FONT(,8,,)
                         STRING(@d17b),AT(6458,0),USE(tmp:DOP),TRN,RIGHT,FONT(,8,,)
                       END
                       FOOTER,AT(396,10833,7521,302),USE(?unnamed:3)
                         LINE,AT(104,52,2146,0),USE(?Line3),COLOR(COLOR:Black)
                         STRING('Total Quantity Ordered:  1'),AT(104,104),USE(?String29),TRN,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                       END
                       FORM,AT(365,510,7521,10802),USE(?unnamed)
                         BOX,AT(104,2292,7344,7448),USE(?Box2),ROUND,COLOR(COLOR:Black)
                         BOX,AT(104,1875,7344,365),USE(?Box1),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H)
                         BOX,AT(4844,365,2552,1406),USE(?Box3),ROUND,COLOR(COLOR:Black)
                         STRING('EXCHANGES ORDERED'),AT(4740,52),USE(?String3),TRN,FONT('Tahoma',16,,FONT:bold,CHARSET:ANSI)
                         STRING('Quantity'),AT(4198,1979),USE(?String5),TRN,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Job Number'),AT(4875,1979),USE(?JobNumber),TRN,FONT(,8,,FONT:bold)
                         STRING('Activation Date'),AT(6458,1979),USE(?ActivationDate),TRN,FONT(,8,,FONT:bold)
                         STRING('Manufacturer'),AT(156,1979),USE(?String6),TRN,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Model Number'),AT(2167,1979),USE(?String31),TRN,FONT('Tahoma',8,,FONT:bold)
                       END
                     END
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeNoRecords          PROCEDURE(),DERIVED                 ! Method added to host embed code
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

ProgressMgr          StepLongClass                         ! Progress Manager
Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED                 ! Method added to host embed code
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ExchangeOrder')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:EXCHOR48.Open                                     ! File EXCHOR48 used by this procedure, so make sure it's RelationManager is open
  Relate:JOBS.SetOpenRelated()
  Relate:JOBS.Open                                         ! File JOBS used by this procedure, so make sure it's RelationManager is open
  Access:TRADEACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:SUBTRACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  Access:TRADEACC.Clearkey(tra:Account_Number_Key)
  tra:Account_Number  = p_web.GSV('BookingAccount')
  IF Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
     !Found
     address:User_Name = tra:company_name
     address:Address_Line1 = tra:address_line1
     address:Address_Line2 = tra:address_line2
     address:Address_Line3 = tra:address_line3
     address:Post_code = tra:postcode
     address:Telephone_Number = tra:telephone_number
     address:Fax_No = tra:Fax_Number
     address:Email = tra:EmailAddress
  ELSE ! If Access:TRADACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      !Error
  END !If Access:TRADACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  
  locRecordNumber = p_web.GSV('ExchangeOrder:RecordNumber')
  tmp:DOP = p_web.GSV('job:DOP')
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('ExchangeOrder',ProgressWindow)             ! Restore window settings from non-volatile store
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.SetReportTarget(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ProgressWindow{Prop:Timer} = 10                          ! Assign timer interval
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:EXCHOR48, ?Progress:PctText, Progress:Thermometer, ProgressMgr, ex4:RecordNumber)
  ThisReport.AddSortOrder(ex4:RecordNumberKey)
  ThisReport.AddRange(ex4:RecordNumber,locRecordNumber)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{Prop:Text}=''
  Relate:EXCHOR48.SetQuickScan(1,Propagate:OneMany)
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom=True
  Previewer.Maximize=True
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
      SELF.SetReportTarget(PDFReporter.IReportGenerator)
    self.SkipPreview = True
    ProgressWindow{prop:hide} = 1
    loc:PDFName = clip(p_web.site.WebFolderPath) & '\reports\$$$' & format(random(1,99999),@n05) &'.pdf'
  End
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EXCHOR48.Close
    Relate:JOBS.Close
  END
  IF SELF.Opened
    INIMgr.Update('ExchangeOrder',ProgressWindow)          ! Save window data to non-volatile store
  END
  ProgressMgr.Kill()
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      Loc:html = '<script type="text/javascript">alert('''&clip('No Records')&''');top.close();</script>'
      p_web.ParseHTML(loc:html)
    Else
      p_web.ForceNoCache = 1
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(loc:PDFName)
    End
  End
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text}=FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
    If Not p_Web &= NULL
      loc:NoRecords = 1
      Return
    End
  PARENT.TakeNoRecords


ThisWindow.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeRecord()
    If Not p_web &= Null
      p_web.NoOp()
    End
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
  PRINT(RPT:detail1)
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','WebServer_Phase4','ExchangeOrder','ExchangeOrder','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.CompressText   = True
  SELF.CompressImages = True
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    SELF.SetFileName(clip(loc:PDFName))
  End

UseReplenishmentProcess PROCEDURE  (fModelNumber,fAccountNumber) ! Declare Procedure
MANUFACT::State  USHORT
MODELNUM::State  USHORT
TRADEACC::State  USHORT
FilesOpened     BYTE(0)
  CODE
    do openFiles
    do saveFiles


    Return# = 0
    ! Inserting (DBH 21/02/2008) # 9717 - If model/manufacturer is used in Replenishment, then it can't be 48 Hour
    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
    tra:Account_Number = fAccountNumber
    If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
        If tra:IgnoreReplenishmentProcess = 0
            ! This account CAN use the replenishment process (DBH: 21/02/2008)
            Access:MODELNUM.Clearkey(mod:Model_Number_Key)
            mod:Model_Number = fModelNumber
            If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
                If mod:UseReplenishmentProcess = 1
                    Return# = 1
                Else ! If mod:UseReplenishmentProcess = 1
                    Access:MANUFACT.Clearkey(man:Manufacturer_Key)
                    man:Manufacturer = mod:Manufacturer
                    If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                        If man:UseReplenishmentProcess = 1
                            Return# = 1
                        End ! If man:UseReplenishmentProcess = 1
                    End ! If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                End ! If mod:UseReplenishmentProcess = 1
            End ! If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
        End ! If tra:IgnoreReplenishmentProcess = 0
    End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
    ! End (DBH 21/02/2008) #9717

    do restoreFiles
    do closeFiles

    Return Return#
!--------------------------------------
OpenFiles  ROUTINE
  Access:MANUFACT.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANUFACT.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:MODELNUM.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MODELNUM.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:MANUFACT.Close
     Access:MODELNUM.Close
     Access:TRADEACC.Close
     FilesOpened = False
  END
SaveFiles  ROUTINE
  MANUFACT::State = Access:MANUFACT.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  MODELNUM::State = Access:MODELNUM.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  TRADEACC::State = Access:TRADEACC.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF MANUFACT::State <> 0
    Access:MANUFACT.RestoreFile(MANUFACT::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF MODELNUM::State <> 0
    Access:MODELNUM.RestoreFile(MODELNUM::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF TRADEACC::State <> 0
    Access:TRADEACC.RestoreFile(TRADEACC::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
Allow48Hour          PROCEDURE  (fIMEINumber,fModelNumber,fAccountNumber) ! Declare Procedure
ESNMODAL::State  USHORT
FilesOpened     BYTE(0)
  CODE
    do openFiles
    do saveFiles

    Return# = 0

    Access:ESNMODEL.ClearKey(esn:ESN_Key)
    esn:ESN          = Sub(fIMEINumber,1,6)
    esn:Model_Number = fModelNumber
    If Access:ESNMODEL.TryFetch(esn:ESN_Key) = Level:Benign
        !Found
        If esn:Include48Hour
            Return# = 1
        End !If esn:Include48Hour
    Else !If Access:ESNMODEL.TryFetch(esn:ESN_Key) = Level:Benign
        !Error
    End !If Access:ESNMODEL.TryFetch(esn:ESN_Key) = Level:Benign

    Access:ESNMODEL.ClearKey(esn:ESN_Key)
    esn:ESN          = Sub(fIMEINumber,1,8)
    esn:Model_Number = fModelNumber
    If Access:ESNMODEL.TryFetch(esn:ESN_Key) = Level:Benign
        !Found
        If esn:Include48Hour
            Return# = 1
        End !If esn:Include48Hour
    Else !If Access:ESNMODEL.TryFetch(esn:ESN_Key) = Level:Benign
        !Error
    End !If Access:ESNMODEL.TryFetch(esn:ESN_Key) = Level:Benign

    ! Inserting (DBH 21/02/2008) # 9717 - If Man/Model is setup for replenishment, then don't use 48 Hour
    If UseReplenishmentProcess(fModelNumber,fAccountNumber) = 1
        Return# = 0
    End ! If UseReplenishmentProcess(f:ModelNumber,fAccountNumber) = 1
    ! End (DBH 21/02/2008) #9717

    do restoreFiles
    do closeFiles

    Return Return#

!--------------------------------------
OpenFiles  ROUTINE
  Access:ESNMODAL.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:ESNMODAL.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:ESNMODAL.Close
     FilesOpened = False
  END
SaveFiles  ROUTINE
  ESNMODAL::State = Access:ESNMODAL.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF ESNMODAL::State <> 0
    Access:ESNMODAL.RestoreFile(ESNMODAL::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
AccountActivate48Hour PROCEDURE  (fAccountNumber)          ! Declare Procedure
FilesOpened     BYTE(0)
  CODE
    return# = 0
    do openFiles

    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
    tra:Account_Number    = fAccountNumber
    if (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
        ! Found
        if (tra:Activate48Hour)
            return# = 1
        end ! if (tra:Activate48Hour)
    else ! if (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
        ! Error
    end ! if (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)

    do closeFiles

    return return#
!--------------------------------------
OpenFiles  ROUTINE
  Access:TRADEACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:TRADEACC.Close
     FilesOpened = False
  END
CustCollectionValidated PROCEDURE  (LONG jobNumber)        ! Declare Procedure
  CODE
    RETURN vod.CustomerCollectionValidated(jobNumber)
