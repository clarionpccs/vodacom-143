

   MEMBER('WebServer_Phase4.clw')                          ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('NetEmail.inc'),ONCE

                     MAP
                       INCLUDE('WEBSE027.INC'),ONCE        !Local module procedure declarations
                     END


Pricing_Routine      PROCEDURE  (f_type,f_labour,f_parts,f_pass,f_claim,f_handling,f_exchange,f_RRCRate,f_RRCParts) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
tmp:AccessoryExists  BYTE(0)                               !
tmp:TradeAccount     STRING(15)                            !
save_par_id   ushort,auto
save_wpr_id   ushort,auto
save_epr_id   ushort,auto
  CODE
    !This does nothing anymore...
    !Replaced by JobPricingRoutine
    Return




!
!
!! Source Bit
!    f_labour = 0
!    f_parts = 0
!    f_pass = 0
!    f_claim = 0
!    f_handling = 0
!    f_exchange = 0
!    f_RRCRate   = 0
!    f_RRCParts = 0
!! Chargeable Job?
!
!    If job:account_number = ''
!        Return
!    End
!    Case f_type
!        Of 'C'
!            If job:chargeable_job = 'YES'
!                labour_discount# = 0
!                parts_discount#  = 0
!                f_pass  = 1
!            ! No Charge?
!!                If job:ignore_chargeable_charges = 'YES'
!!                    Return
!!                End
!                access:chartype.clearkey(cha:charge_type_key)
!                cha:charge_type = job:charge_type
!                If access:chartype.fetch(cha:charge_type_key)
!                    f_labour    = 0
!                    f_parts     = 0
!                    f_pass      = 0
!                Else !If access:chartype.fetch(cha:charge_type_key)
!                    If cha:no_charge = 'YES'
!                        f_labour = 0
!                        f_parts  = 0
!                        f_RRCRate = 0
!                        f_RRCParts = 0
!
!                    Else !If cha:no_charge = 'YES'
!                        If cha:zero_parts <> 'YES'
!                            parts_total$ = 0
!                            rrc_total$   = 0
!                            save_par_id = access:parts.savefile()
!                            access:parts.clearkey(par:part_number_key)
!                            par:ref_number  = job:ref_number
!                            set(par:part_number_key,par:part_number_key)
!                            loop
!                                if access:parts.next()
!                                   break
!                                end !if
!                                if par:ref_number  <> job:ref_number      |
!                                    then break.  ! end if
!                                parts_total$ += par:sale_cost * par:quantity
!                                rrc_total$  += par:RRCSaleCost * par:Quantity   !was + changed JC
!                                !message(par:rrcSaleCost * par:quantity&' - '&rcc_total$)
!                            end !loop
!                            access:parts.restorefile(save_par_id)
!                            f_parts = parts_total$
!                            f_rrcparts  = rrc_total$
!
!                        Else !If cha:zero_parts <> 'YES'
!                            f_parts = 0
!                            f_rrcparts = 0
!
!                        End !If cha:zero_parts <> 'YES'
!
!                        access:subtracc.clearkey(sub:account_number_key)
!                        sub:account_number = job:account_number
!                        if access:subtracc.fetch(sub:account_number_key)
!                        Else!if access:subtracc.fetch(sub:account_number_key)
!                            access:tradeacc.clearkey(tra:account_number_key)
!                            tra:account_number = sub:main_account_number
!                            if access:tradeacc.fetch(tra:account_number_key)
!                            end!if access:tradeacc.fetch(tra:account_number_key)
!                        end!if access:subtracc.fetch(sub:account_number_key)
!                        use_standard# = 1
!                        If TRA:ZeroChargeable = 'YES'
!                            f_parts = 0
!                            f_RRCParts = 0
!
!                        End!If TRA:ZeroChargeable = 'YES'
!                        If tra:invoice_sub_accounts = 'YES' and tra:Invoice_Sub_Accounts = 'YES'
!                            access:subchrge.clearkey(suc:model_repair_type_key)
!                            suc:account_number = job:account_number
!                            suc:model_number   = job:model_number
!                            suc:charge_type    = job:charge_type
!                            suc:unit_type      = job:unit_type
!                            suc:repair_type    = job:repair_type
!                            if access:subchrge.fetch(suc:model_repair_type_key)
!                                access:trachrge.clearkey(trc:account_charge_key)
!                                trc:account_number = sub:main_account_number
!                                trc:model_number   = job:model_number
!                                trc:charge_type    = job:charge_type
!                                trc:unit_type      = job:unit_type
!                                trc:repair_type    = job:repair_type
!                                if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
!                                    f_labour    = trc:cost
!                                    f_RRCRate   = trc:RRCRate
!                                    f_handling  = trc:HandlingFee
!                                    use_standard# = 0
!                                End!if access:trachrge.fetch(trc:account_charge_key)
!                            Else
!                                f_labour    = suc:cost
!                                f_RRCRate   = suc:RRCRate
!                                f_handling  = suc:HandlingFee
!                                use_standard# = 0
!                            End!if access:subchrge.fetch(suc:model_repair_type_key)
!                        Else!If tra:invoice_sub_accounts = 'YES'
!                            access:trachrge.clearkey(trc:account_charge_key)
!                            trc:account_number = sub:main_account_number
!                            trc:model_number   = job:model_number
!                            trc:charge_type    = job:charge_type
!                            trc:unit_type      = job:unit_type
!                            trc:repair_type    = job:repair_type
!                            if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
!                                f_labour    = trc:cost
!                                f_RRCRate   = trc:RRCRate
!                                f_handling  = trc:HandlingFee
!                                use_standard# = 0
!
!                            End!if access:trachrge.fetch(trc:account_charge_key)
!                        End!If tra:invoice_sub_accounts = 'YES'
!
!                        If use_standard# = 1
!                            access:stdchrge.clearkey(sta:model_number_charge_key)
!                            sta:model_number = job:model_number
!                            sta:charge_type  = job:charge_type
!                            sta:unit_type    = job:unit_type
!                            sta:repair_type  = job:repair_type
!                            if access:stdchrge.fetch(sta:model_number_charge_key)
!                                f_labour     = 0
!                                f_pass       = 0
!                            Else !if access:stdchrge.fetch(sta:model_number_charge_key)
!                                f_labour    = sta:cost
!                                f_RRCRate   = sta:RRCRate
!                                f_handling  = sta:HandlingFee
!                            end !if access:stdchrge.fetch(sta:model_number_charge_key)
!                        End!If use_standard# = 1
!                    End !If cha:no_charge = 'YES'
!                End !If access:chartype.clearkey(cha:charge_type_key)
!            End !If job:chargeable_job = 'YES'
!
!        Of 'W'
!
!        !Warranty Job
!            If job:warranty_job = 'YES'
!                !Do not reprice Warranty Jobs that are completed - L945 (DBH: 03-09-2003)
!                If job:Date_Completed <> 0
!
!                    Return
!                End !If job:Date_Completed <> 0
!                parts_discount# = 0
!                labour_discount# = 0
!                f_pass = 1
!!               If job:ignore_warranty_charges = 'YES'
!!                   Return
!!               End
!            ! No Charge?
!                access:chartype.clearkey(cha:charge_type_key)
!                cha:charge_type = job:warranty_charge_type
!                If access:chartype.fetch(cha:charge_type_key)
!                    f_labour    = 0
!                    f_parts     = 0
!                    f_RRCParts  = 0
!                    f_RRCRate   = 0
!                    f_pass      = 0
!                Else !If access:chartype.fetch(cha:charge_type_key)
!                    If cha:no_charge = 'YES'
!                        f_labour = 0
!                        f_parts  = 0
!                        f_RRCParts  = 0
!                        f_RRCRate   = 0
!                    Else !If cha:no_charge = 'YES'
!
!                        If cha:zero_parts <> 'YES'
!                            parts_total$ = 0
!                            RRCParts_Total$ = 0
!
!                            Access:STOCK.Open()
!                            Access:STOCK.UseFile()
!
!                            tmp:AccessoryExists = false
!                            tmp:TradeAccount = ''
!
!                            save_wpr_id = access:warparts.savefile()
!                            access:warparts.clearkey(wpr:part_number_key)
!                            wpr:ref_number  = job:ref_number
!                            set(wpr:part_number_key,wpr:part_number_key)
!                            loop
!                                if access:warparts.next()
!                                   break
!                                end !if
!                                if wpr:ref_number  <> job:ref_number      |
!                                    then break.  ! end if
!                                parts_total$ += wpr:purchase_cost * wpr:quantity
!                                RRCParts_Total$ += wpr:RRCPurchaseCost * wpr:Quantity
!
!                                ! --------------------------------------------------------------------
!                                ! Check if any parts are accessories
!                                Access:STOCK.ClearKey(sto:Ref_Number_Key)
!                                sto:Ref_Number = wpr:Part_Ref_Number
!                                if not Access:STOCK.Fetch(sto:Ref_Number_Key)
!                                    if sto:Accessory = 'YES'
!                                        tmp:AccessoryExists = true
!                                    end
!                                    i# = instring('<32>',clip(sto:Location),1,1)
!                                    if i# <> 0
!                                        tmp:TradeAccount = sto:Location[1:i#]
!                                    end
!                                end
!                                ! --------------------------------------------------------------------
!                            end !loop
!                            access:warparts.restorefile(save_wpr_id)
!                            Access:STOCK.Close()
!                            f_parts = parts_total$
!                            f_RRCParts = RRCParts_Total$
!                        Else !If cha:zero_parts <> 'YES'
!                            f_parts = 0
!                            f_RRCParts = 0
!                        End !If cha:zero_parts <> 'YES'
!                        access:subtracc.clearkey(sub:account_number_key)
!                        sub:account_number = job:account_number
!                        if access:subtracc.fetch(sub:account_number_key)
!                        Else!if access:subtracc.fetch(sub:account_number_key)
!                            access:tradeacc.clearkey(tra:account_number_key)
!                            tra:account_number = sub:main_account_number
!                            if access:tradeacc.fetch(tra:account_number_key)
!                            end!if access:tradeacc.fetch(tra:account_number_key)
!                        end!if access:subtracc.fetch(sub:account_number_key)
!
!                        use_standard# = 1
!!                        If TRA:ZeroChargeable = 'YES'
!!                            f_parts = 0
!!                        End!If TRA:ZeroChargeable = 'YES'
!                        If tra:invoice_sub_accounts = 'YES' and tra:Use_Sub_Accounts = 'YES'
!                            access:subchrge.clearkey(suc:model_repair_type_key)
!                            suc:account_number = job:account_number
!                            suc:model_number   = job:model_number
!                            suc:charge_type    = job:Warranty_charge_type
!                            suc:unit_type      = job:unit_type
!                            suc:repair_type    = job:repair_type_warranty
!                            if access:subchrge.fetch(suc:model_repair_type_key)
!                                access:trachrge.clearkey(trc:account_charge_key)
!                                trc:account_number = sub:main_account_number
!                                trc:model_number   = job:model_number
!                                trc:charge_type    = job:warranty_charge_type
!                                trc:unit_type      = job:unit_type
!                                trc:repair_type    = job:repair_type_warranty
!                                if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
!                                    f_labour    = trc:cost
!                                    f_claim     = trc:WarrantyClaimRate
!                                    f_handling  = trc:HandlingFee
!                                    f_Exchange  = trc:Exchange
!                                    f_RRCRate   = trc:RRCRate
!                                    use_standard# = 0
!                                End!if access:trachrge.fetch(trc:account_charge_key)
!                            Else
!                                f_labour    = suc:cost
!                                f_claim     = suc:WarrantyClaimRate
!                                f_handling  = suc:HandlingFee
!                                f_Exchange  = suc:Exchange
!                                f_RRCRate   = suc:RRCRate
!                                use_standard# = 0
!                            End!if access:subchrge.fetch(suc:model_repair_type_key)
!                        Else!If tra:use_sub_accounts = 'YES'
!                            access:trachrge.clearkey(trc:account_charge_key)
!                            trc:account_number = tra:account_number
!                            trc:model_number   = job:model_number
!                            trc:charge_type    = job:warranty_charge_type
!                            trc:unit_type      = job:unit_type
!                            trc:repair_type    = job:repair_type_warranty
!                            if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
!                                f_labour    = trc:cost
!                                f_claim     = trc:WarrantyClaimRate
!                                f_handling  = trc:HandlingFee
!                                f_Exchange  = trc:Exchange
!                                f_RRCRate   = trc:RRCRate
!                                use_standard# = 0
!                            End!if access:trachrge.fetch(trc:account_charge_key)
!                        End!If tra:use_sub_accounts = 'YES'
!
!                        If use_standard# = 1
!                            access:stdchrge.clearkey(sta:model_number_charge_key)
!                            sta:model_number = job:model_number
!                            sta:charge_type  = job:warranty_charge_type
!                            sta:unit_type    = job:unit_type
!                            sta:repair_type  = job:repair_type_warranty
!                            if access:stdchrge.fetch(sta:model_number_charge_key)
!                                f_labour     = 0
!                                f_pass       = 0
!                            Else !if access:stdchrge.fetch(sta:model_number_charge_key)
!                                f_labour    = sta:cost
!                                f_claim     = sta:WarrantyClaimRate
!                                f_handling  = sta:HandlingFee
!                                f_Exchange  = sta:Exchange
!                                f_RRCRate   = sta:RRCRate
!                            end !if access:stdchrge.fetch(sta:model_number_charge_key)
!                        End!If use_standard# = 1
!
!                        ! ------------------------------------------------------------------------
!!                        if tmp:AccessoryExists = true and job:warranty_charge_type = 'WARRANTY (MFTR)'
!!
!!                            if tmp:TradeAccount = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
!!                                ! ARC
!!                                if SentToHub(job:Ref_Number) and jobe:WebJob = true
!!                                    f_Exchange = 0
!!                                else
!!                                    f_Exchange = 0
!!                                    f_handling  = 0
!!                                end
!!                            else
!!                                ! RRC
!!                                if SentToHub(job:Ref_Number) and jobe:WebJob = true
!!                                    f_handling  = 0
!!                                else
!!                                    f_handling  = 0
!!                                end
!!                            end
!!                        else
!                            If job:Exchange_Unit_Number <> 0 AND jobe:ExchangedATRRC = TRUE
!                                !jobe:ExchangeRate = Exchange"
!                                f_handling  = 0
!                            Else !If job:Exchange_Unit_Number <> 0 And ~jobe:ExchangeRate
!                                IF SentToHub(job:Ref_Number)
!                                    f_Exchange = 0
!                                    !jobe:HandlingFee  = Handling"
!                                ELSE
!                                    f_Exchange = 0
!                                    f_handling  = 0
!                                END
!                            End
!                        !End !If job:Exchange_Unit_Number <> 0 And ~jobe:ExchangeRate
!                        ! ------------------------------------------------------------------------
!
!                    End !If cha:no_charge = 'YES'
!                End !If access:chartype.clearkey(cha:charge_type_key)
!            End !If job:chargeable_job = 'YES'
!
!        Of 'E'
!            If job:estimate = 'YES'
!                labour_discount# = 0
!                parts_discount#  = 0
!                f_pass  = 1
!!                If job:ignore_estimate_charges = 'YES'
!!                    Return
!!                End!If job:ignore_estimate_charges = 'YES'
!            ! No Charge?
!                access:chartype.clearkey(cha:charge_type_key)
!                cha:charge_type = job:charge_type
!                If access:chartype.fetch(cha:charge_type_key)
!                    f_labour    = 0
!                    f_parts     = 0
!                    f_RRCRate   = 0
!                    f_RRCParts  = 0
!                    f_pass      = 0
!                Else !If access:chartype.fetch(cha:charge_type_key)
!                    If cha:no_charge = 'YES'
!                        f_labour = 0
!                        f_parts  = 0
!                        f_RRCRate  = 0
!                        f_RRCParts = 0
!                    Else !If cha:no_charge = 'YES'
!
!                        If cha:zero_parts <> 'YES'
!                            parts_total$ = 0
!                            RRCParts_Total$ = 0
!                            save_epr_id = access:estparts.savefile()
!                            access:estparts.clearkey(epr:part_number_key)
!                            epr:ref_number  = job:ref_number
!                            set(epr:part_number_key,epr:part_number_key)
!                            loop
!                                if access:estparts.next()
!                                   break
!                                end !if
!                                if epr:ref_number  <> job:ref_number      |
!                                    then break.  ! end if
!                                parts_total$ += epr:sale_cost * epr:quantity
!                                RRCParts_Total$ += epr:RRCSaleCost * epr:Quantity
!                            end !loop
!                            access:estparts.restorefile(save_epr_id)
!                            f_parts = parts_total$
!                            f_RRCParts = RRCParts_Total$
!                        Else !If cha:zero_parts <> 'YES'
!                            f_parts = 0
!                            f_RRCParts = 0
!                        End !If cha:zero_parts <> 'YES'
!
!                        access:subtracc.clearkey(sub:account_number_key)
!                        sub:account_number = job:account_number
!                        if access:subtracc.fetch(sub:account_number_key)
!                        Else!if access:subtracc.fetch(sub:account_number_key)
!                            access:tradeacc.clearkey(tra:account_number_key)
!                            tra:account_number = sub:main_account_number
!                            if access:tradeacc.fetch(tra:account_number_key)
!                            end!if access:tradeacc.fetch(tra:account_number_key)
!                        end!if access:subtracc.fetch(sub:account_number_key)
!                        use_standard# = 1
!                        If TRA:ZeroChargeable = 'YES'
!                            f_parts = 0
!                        End!If TRA:ZeroChargeable = 'YES'
!
!                        If tra:invoice_sub_accounts = 'YES'
!                            access:subchrge.clearkey(suc:model_repair_type_key)
!                            suc:account_number = job:account_number
!                            suc:model_number   = job:model_number
!                            suc:charge_type    = job:charge_type
!                            suc:unit_type      = job:unit_type
!                            suc:repair_type    = job:repair_type
!                            if access:subchrge.fetch(suc:model_repair_type_key)
!                                access:trachrge.clearkey(trc:account_charge_key)
!                                trc:account_number = sub:main_account_number
!                                trc:model_number   = job:model_number
!                                trc:charge_type    = job:charge_type
!                                trc:unit_type      = job:unit_type
!                                trc:repair_type    = job:repair_type
!                                if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
!                                    f_labour    = trc:cost
!                                    f_RRCRate   = trc:RRCRate
!                                    use_standard# = 0
!                                End!if access:trachrge.fetch(trc:account_charge_key)
!                            Else
!                                f_labour    = suc:cost
!                                f_RRCRate   = suc:RRCRate
!                                use_standard# = 0
!                            End!if access:subchrge.fetch(suc:model_repair_type_key)
!                        Else!If tra:use_sub_accounts = 'YES'
!                            access:trachrge.clearkey(trc:account_charge_key)
!                            trc:account_number = sub:main_account_number
!                            trc:model_number   = job:model_number
!                            trc:charge_type    = job:charge_type
!                            trc:unit_type      = job:unit_type
!                            trc:repair_type    = job:repair_type
!                            if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
!                                f_labour    = trc:cost
!                                f_RRCRate   = trc:RRCRate
!                                use_standard# = 0
!                            End!if access:trachrge.fetch(trc:account_charge_key)
!                        End!If tra:use_sub_accounts = 'YES'
!
!                        If use_standard# = 1
!                            access:stdchrge.clearkey(sta:model_number_charge_key)
!                            sta:model_number = job:model_number
!                            sta:charge_type  = job:charge_type
!                            sta:unit_type    = job:unit_type
!                            sta:repair_type  = job:repair_type
!                            if access:stdchrge.fetch(sta:model_number_charge_key)
!                                f_labour     = 0
!                                f_pass       = 0
!                            Else !if access:stdchrge.fetch(sta:model_number_charge_key)
!                                f_labour    = sta:cost
!                                f_RRCRate   = sta:RRCRate
!                            end !if access:stdchrge.fetch(sta:model_number_charge_key)
!                        End!If use_standard# = 1
!
!                    End !If cha:no_charge = 'YES'
!                End !If access:chartype.clearkey(cha:charge_type_key)
!            End !If job:estimate = 'YES'
!    End!Case f_type
!
DateCodeValidation   PROCEDURE  (func:Manufacturer,func:DateCode,func:DateBooked) ! Declare Procedure
tmp:YearCode         STRING(30)                            !Year COde
tmp:MonthCode        STRING(30)                            !Month Code
tmp:Year             STRING(30)                            !Year
tmp:Month            STRING(30)                            !Month
  CODE
    Access:MANUFACT.Clearkey(man:Manufacturer_Key)
    man:Manufacturer    = func:Manufacturer

    If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
        !Found

    Else! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign


    !Now depending on the manufacturer determines where the
    !The date code is picked up from
    DoWeekCheck# = 0
    Case func:Manufacturer
        Of 'ALCATEL'
            tmp:YearCode    = Sub(func:DateCode,3,1)
            tmp:MonthCode   = Sub(func:DateCode,2,1)
        Of 'ERICSSON'
            tmp:YearCode    = Sub(func:DateCode,1,2)
            tmp:MonthCode   = Sub(func:DateCode,3,2)
            DoWeekCheck#    = 1
        Of 'PHILIPS'
            tmp:YearCode    = Sub(func:DateCode,5,2)
            tmp:MonthCode   = Sub(func:DateCode,7,2)
            DoWeekCheck#    = 1
        Of 'SAMSUNG'
            tmp:YearCode    = Sub(func:DateCode,4,1)
            tmp:MonthCode   = Sub(func:DateCode,5,1)
        Of 'BOSCH'
            tmp:YearCode    = Sub(func:DateCode,1,1)
            tmp:MonthCode   = Sub(func:DateCode,2,1)
        Of 'SIEMENS'
            tmp:YearCode    = Sub(func:DateCode,1,1)
            tmp:MonthCode    = Sub(func:DateCode,2,1)
        Of 'MOTOROLA'
            tmp:YearCode    = Sub(func:DateCode,5,1)
            tmp:MonthCode   = Sub(func:DateCode,6,1)
    End !Case func:Manufacturer

    If DoWeekCheck#
        tmp:Year    = tmp:YearCode
        StartOfYear# = Deformat('1/1/' & tmp:Year,@d5)

        DayOfYear#   = StartOfYear# + (tmp:MonthCode * 7)

        tmp:Year    = Year(DayOfYear#)
        tmp:Month   = Month(DayOfYear#)

    Else
        Access:MANUDATE.Clearkey(mad:DateCodeKey)
        mad:Manufacturer    = func:Manufacturer
        If func:Manufacturer = 'ALCATEL'
            mad:DateCode        = Sub(func:DateCode,1,1) & Clip(tmp:MonthCode) & Clip(tmp:YearCode)
        Else !If func:Manufacturer = 'ALCATEL'
            mad:DateCode        = Clip(tmp:YearCode) & Clip(tmp:MonthCode)
        End !If func:Manufacturer = 'ALCATEL'

        If Access:MANUDATE.Tryfetch(mad:DateCodeKey) = Level:Benign
            !Found
            tmp:Year    = mad:TheYear
            tmp:Month   = mad:TheMonth

        Else! If Access:MANUDATE.Tryfetch(mad:DateCodeKey) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
            Return Level:Fatal
        End! If Access:MANUDATE.Tryfetch(mad:DateCodeKey) = Level:Benign
    End !If DoWeekCheck#

    Loop x# = 1 To man:ClaimPeriod
        tmp:Month += 1
        If tmp:Month > 12
            tmp:Month = 1
            tmp:Year += 1
        End !If tmp:Month > 12
    End !Loop x# = 1 To man:ClaimDate

    If tmp:Year < Year(func:DateBooked)
        !POP Required
        Return Level:Fatal
    End !If tmp:Year > func:DateBooked
    If tmp:Year = Year(func:DateBooked) And tmp:Month < Month(func:DateBooked)
        Return Level:Fatal
    End !If tmp:Year = func:DateBooked And tmp:Month < Month(func:DateBooked)

    Return Level:Benign
SendEmail PROCEDURE (string pEmailFrom, string pEmailTo, string pEmailSubject, string pEmailCC, string pEmailBcc, string pEmailFileList, string pEmailMessageText) ! Generated from procedure template - Window

FilesOpened          BYTE                                  !
EmailServer          STRING(80)                            !
EmailPort            USHORT                                !
EmailFrom            STRING(252)                           !
EmailTo              STRING(1024)                          !
EmailSubject         STRING(252)                           !
EmailCC              STRING(1024)                          !
EmailBCC             STRING(1024)                          !
EmailFileList        STRING(8192)                          !
EmailMessageText     STRING(16384)                         !
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
Email_Spacer         USHORT                                !
window               WINDOW('Sending Email'),AT(,,135,29),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),RESIZE
                       BUTTON('&Send'),AT(9,8,51,16),USE(?EmailSend),LEFT,TIP('Send Email Now')
                       BUTTON('Close'),AT(69,8,51,16),USE(?Close)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass
!Local Data Classes
ThisSendEmail        CLASS(NetEmailSend)                   ! Generated by NetTalk Extension (Class Definition)
ErrorTrap              PROCEDURE(string errorStr,string functionName),DERIVED
MessageSent            PROCEDURE(),DERIVED

                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('SendEmail')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?EmailSend
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:DEFAULTS.Open                                     ! File DEFAULTS used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(window)                                        ! Open window
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  !Window{prop:Hide} = 1
  !post(event:accepted,?EmailSend)
                                               ! Generated by NetTalk Extension (Start)
  ThisSendEmail.init()
  if ThisSendEmail.error <> 0
    ! Put code in here to handle if the object does not initialise properly
  end
  ! Generated by NetTalk Extension
  ThisSendEmail.OptionsMimeTextTransferEncoding = '7bit'           ! '7bit', '8bit' or 'quoted-printable'
  ThisSendEmail.OptionsMimeHtmlTransferEncoding = 'quoted-printable'           ! '7bit', '8bit' or 'quoted-printable'
  Do DefineListboxStyle
  INIMgr.Fetch('SendEmail',window)                         ! Restore window settings from non-volatile store
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ThisSendEmail.Kill()                      ! Generated by NetTalk Extension
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
  END
  IF SELF.Opened
    INIMgr.Update('SendEmail',window)                      ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?EmailSend
      ThisWindow.Update
      EmailTo          = pEmailTo
      EmailFrom        = pEmailFrom
      EmailCC          = pEmailCC
      EmailBCC         = pEmailBCC
      EmailSubject     = pEmailSubject
      EmailFileList    = pEmailFileList
      EmailMessageText = pEmailMessageText
      ! Generated by NetTalk Extension
      ThisSendEmail.Server = def:EmailServerAddress
      ThisSendEmail.Port = def:EmailServerPort
      ThisSendEmail.From = EmailFrom
      ThisSendEmail.ToList = EmailTo
      ThisSendEmail.ccList = EmailCC
      ThisSendEmail.bccList = EmailBCC
      
      ThisSendEmail.Subject = EmailSubject
      ThisSendEmail.AttachmentList = EmailFileList
      ThisSendEmail.SetRequiredMessageSize (0, len(clip(EmailMessageText)), 0) ! You must call this function before populating self.MessageText  #ELSIF ( <> '')
      if ThisSendEmail.Error = 0
        ThisSendEmail.MessageText = EmailMessageText
        display()
        ThisSendEmail.SendMail(NET:EMailMadeFromPartsMode)
        display()
      end
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    ThisSendEmail.TakeEvent()                 ! Generated by NetTalk Extension
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:CloseWindow
      ! Generated by NetTalk Extension
      if records (ThisSendEmail.DataQueue) > 0
        if Message ('The email is still being sent.|Are you sure you want to quit?','Email Sending',ICON:Question,BUTTON:Yes+BUTTON:No,BUTTON:No) = Button:No
          cycle
        end
      end
      ! Generated by NetTalk Extension
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisSendEmail.ErrorTrap PROCEDURE(string errorStr,string functionName)


  CODE
  PARENT.ErrorTrap(errorStr,functionName)
  !post(event:closewindow)


ThisSendEmail.MessageSent PROCEDURE


  CODE
  PARENT.MessageSent
  !post(event:closewindow)

TransferParts_W      PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
  CODE
    count# = 0

    access:parts.clearkey(par:part_number_key)
    par:ref_number  = p_web.GSV('job:ref_number')
    set(par:part_number_key,par:part_number_key)
    loop
        if access:parts.next()
           break
        end !if
        if par:ref_number  <> p_web.GSV('job:ref_number')      |
            then break.  ! end if
        If par:pending_ref_number <> ''
            access:ordpend.clearkey(ope:ref_number_key)
            ope:ref_number = par:pending_ref_number
            if access:ordpend.tryfetch(ope:ref_number_key) = Level:Benign
                ope:part_type = 'JOB'
                access:ordpend.update()
            End!if access:ordpend.tryfetch(ope:ref_number_key) = Level:Benign
        End!If wpr:pending_ref_number <> ''
        If par:order_number <> ''
            access:ordparts.clearkey(orp:record_number_key)
            orp:record_number   = par:order_number
            If access:ordparts.tryfetch(orp:record_number_key) = Level:Benign
                orp:part_type = 'JOB'
                access:ordparts.update()
            End!If access:ordparts.tryfetch(orp:record_number_key) = Level:Benign
        End!If wpr:order_number <> ''
        get(warparts,0)
        if access:warparts.primerecord() = Level:Benign
            record_number$  = wpr:record_number
            wpr:record      :=: par:record
            wpr:record_number   = record_number$
            if access:warparts.insert()
                access:warparts.cancelautoinc()
            End!if access:parts.insert()
        End!if access:parts.primerecord() = Level:Benign
        Delete(parts)
        count# += 1
    end !loop

    if count# <> 0

        get(audit,0)
        if access:audit.primerecord() = level:benign
            aud:ref_number    = p_web.GSV('job:ref_number')
            aud:date          = today()
            aud:time          = clock()
            aud:type          = 'JOB'
            access:users.clearkey(use:password_key)
            use:password = glo:password
            access:users.fetch(use:password_key)
            aud:user = use:user_code
            aud:action        = 'CHARGEABLE PARTS TRANSFERRED TO WARRANTY'
            access:audit.insert()
        end!�if access:audit.primerecord() = level:benign
    End!if count# <> 0
    Access:MODELNUM.Clearkey(mod:Model_Number_Key)
    mod:Model_Number    = p_web.GSV('job:Model_NUmber')
    If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
        !Found
        pendingJob(p_web)
        p_web.SSV('job:Manufacturer',mod:Manufacturer)
    Else! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
GetVATRate           PROCEDURE  (fAccountNumber,fType)     ! Declare Procedure
rtnValue             REAL                                  !
FilesOpened     BYTE(0)
  CODE
    DO openFiles
    rtnValue = 0
    If InvoiceSubAccounts(fAccountNumber)
        Access:VATCODE.Clearkey(vat:Vat_Code_Key)
        Case fType
        Of 'L'
            vat:Vat_Code    = sub:Labour_Vat_Code
        Of 'P'
            vat:Vat_Code    = sub:Parts_Vat_Code
        End !Case func:Type
        If Access:VATCODE.Tryfetch(vat:Vat_Code_Key) = Level:Benign
            !Found
            rtnvalue =  vat:Vat_Rate
        Else ! If Access:VATCODE.Tryfetch(vat:Vat_Code_Key) = Level:Benign
            !Error
        End !If Access:VATCODE.Tryfetch(vat:Vat_Code_Key) = Level:Benign
    Else !If InvoiceSubAccounts(func:AccountNumber)
        Access:VATCODE.Clearkey(vat:Vat_Code_Key)
        Case fType
        Of 'L'
            vat:Vat_Code    = tra:Labour_Vat_Code
        Of 'P'
            vat:Vat_Code    = tra:Parts_Vat_Code
        End !Case func:Type
        If Access:VATCODE.Tryfetch(vat:Vat_Code_Key) = Level:Benign
            !Found
            rtnValue =  vat:Vat_Rate
        Else ! If Access:VATCODE.Tryfetch(vat:Vat_Code_Key) = Level:Benign
            !Error
        End !If Access:VATCODE.Tryfetch(vat:Vat_Code_Key) = Level:Benign
    End !If InvoiceSubAccounts(func:AccountNumber)
    Do CloseFiles
    Return rtnValue


!--------------------------------------
OpenFiles  ROUTINE
  Access:SUBTRACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBTRACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:VATCODE.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:VATCODE.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:SUBTRACC.Close
     Access:TRADEACC.Close
     Access:VATCODE.Close
     FilesOpened = False
  END
