

   MEMBER('WebServer_Phase4.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSE040.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSE013.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE031.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE038.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE039.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE041.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE042.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE060.INC'),ONCE        !Req'd for module callout resolution
                     END


JobFaultCodes        PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
tmp:FaultCode1       STRING(30)                            !
tmp:FaultCode2       STRING(30)                            !
tmp:FaultCode3       STRING(30)                            !
tmp:FaultCode4       STRING(30)                            !
tmp:FaultCode5       STRING(30)                            !
tmp:FaultCode6       STRING(30)                            !
tmp:FaultCode7       STRING(30)                            !
tmp:FaultCode8       STRING(30)                            !
tmp:FaultCode9       STRING(30)                            !
tmp:FaultCode10      STRING(30)                            !
tmp:FaultCode11      STRING(30)                            !
tmp:FaultCode12      STRING(30)                            !
tmp:FaultCode13      STRING(30)                            !
tmp:FaultCode14      STRING(30)                            !
tmp:FaultCode15      STRING(30)                            !
tmp:FaultCode16      STRING(30)                            !
tmp:FaultCode17      STRING(30)                            !
tmp:FaultCode18      STRING(30)                            !
tmp:FaultCode19      STRING(30)                            !
tmp:FaultCode20      STRING(30)                            !
tmp:MSN              STRING(30)                            !
tmp:VerifyMSN        STRING(30)                            !
tmp:ConfirmMSNChange BYTE                                  !
tmp:processExchange  BYTE                                  !
FilesOpened     Long
AUDIT::State  USHORT
CHARTYPE::State  USHORT
REPTYDEF::State  USHORT
MANFAULO_ALIAS::State  USHORT
MANFAULO::State  USHORT
JOBOUTFL::State  USHORT
MANFAULT_ALIAS::State  USHORT
MANFAULT::State  USHORT
WARPARTS::State  USHORT
STOCK::State  USHORT
STOMODEL::State  USHORT
STOMJFAU::State  USHORT
MANFAUPA::State  USHORT
MANFPALO::State  USHORT
TRADEACC::State  USHORT
LOCATION::State  USHORT
MANUFACT::State  USHORT
JOBS::State  USHORT
MODPROD::State  USHORT
SBO_OutFaultParts::State  USHORT
PARTS::State  USHORT
USERS::State  USHORT
tmp:MSN:IsInvalid  Long
buttonVerifyMSN:IsInvalid  Long
tmp:VerifyMSN:IsInvalid  Long
buttonVerifyMSN2:IsInvalid  Long
tmp:ConfirmMSNChange:IsInvalid  Long
job:ProductCode:IsInvalid  Long
tmp:FaultCode1:IsInvalid  Long
tmp:FaultCode2:IsInvalid  Long
tmp:FaultCode3:IsInvalid  Long
tmp:FaultCode4:IsInvalid  Long
tmp:FaultCode5:IsInvalid  Long
tmp:FaultCode6:IsInvalid  Long
tmp:FaultCode7:IsInvalid  Long
tmp:FaultCode8:IsInvalid  Long
tmp:FaultCode9:IsInvalid  Long
tmp:FaultCode10:IsInvalid  Long
tmp:FaultCode11:IsInvalid  Long
tmp:FaultCode12:IsInvalid  Long
tmp:FaultCode13:IsInvalid  Long
tmp:FaultCode14:IsInvalid  Long
tmp:FaultCode15:IsInvalid  Long
tmp:FaultCode16:IsInvalid  Long
tmp:FaultCode17:IsInvalid  Long
tmp:FaultCode18:IsInvalid  Long
tmp:FaultCode19:IsInvalid  Long
tmp:FaultCode20:IsInvalid  Long
buttonRepairNotes:IsInvalid  Long
buttonOutFaults:IsInvalid  Long
promptExchange:IsInvalid  Long
tmp:processExchange:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
local       class
AllocateExchangePart      Procedure(String    func:Type,Byte  func:Allocated,Byte func:SecondUnit)
PartForceFaultCode    Procedure(Long f:PartFieldNumber,String f:FaultCode, Long f:JobFieldNumber ),Byte
AfterFaultCodeLookup        Procedure(Long fNumber)
SetLookupButton      Procedure(Long fNumber)
            end
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('JobFaultCodes')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'JobFaultCodes_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('JobFaultCodes','')
    p_web.DivHeader('JobFaultCodes',p_web.combine(p_web.site.style.formdiv,))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('JobFaultCodes') = 0
        p_web.AddPreCall('JobFaultCodes')
        p_web.DivHeader('popup_JobFaultCodes','nt-hidden')
        p_web.DivHeader('JobFaultCodes',p_web.combine(p_web.site.style.formdiv,))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_JobFaultCodes_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_JobFaultCodes_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferJobFaultCodes',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_JobFaultCodes',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferJobFaultCodes',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_JobFaultCodes',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferJobFaultCodes',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_JobFaultCodes',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_JobFaultCodes',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferJobFaultCodes',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_JobFaultCodes',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferJobFaultCodes',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_JobFaultCodes',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_JobFaultCodes',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('JobFaultCodes')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
CheckStockModelFaultCodes        Routine
    Access:WARPARTS.Clearkey(wpr:Part_Number_Key)
    wpr:Ref_Number = p_web.GSV('job:Ref_Number')
    Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
    Loop
        If Access:WARPARTS.Next()
            Break
        End ! If Access:WARPARTS.Next()
        If wpr:Ref_Number <> p_web.GSV('job:Ref_Number')
            Break
        End ! If wpr:Ref_Number <> job:Ref_Number

        Access:STOCK.Clearkey(sto:Ref_Number_Key)
        sto:Ref_Number = wpr:Part_Ref_Number
        If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
            If sto:Assign_Fault_Codes = 'YES'
                Access:STOMODEL.Clearkey(stm:Mode_Number_Only_Key)
                stm:Ref_Number = sto:Ref_Number
                stm:Model_NUmber = p_web.GSV('job:Ref_Number')
                If Access:STOMODEL.TryFetch(stm:Mode_Number_Only_Key) = Level:Benign
                    Access:STOMJFAU.Clearkey(stj:FieldKey)
                    stj:RefNumber = stm:RecordNumber
                    If Access:STOMJFAU.TryFetch(stj:FieldKey) = Level:Benign
                        Case maf:Field_Number
                        Of 1
                            If stj:FaultCode1 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 30/10/2007)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 30/10/2007)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode1
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 30/10/2007)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 30/10/2007)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 30/10/2007)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode1)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode1)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode1 <> ''
                        Of 2
                            If stj:FaultCode2 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 30/20/2007)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 30/20/2007)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode2
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 30/20/2007)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 30/20/2007)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 30/20/2007)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode2)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode2)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode2 <> ''

                        Of 3
                            If stj:FaultCode3 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 30/30/3007)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 30/30/3007)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode3
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 30/30/3007)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 30/30/3007)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 30/30/3007)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode3)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode3)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode3 <> ''

                        Of 4
                            If stj:FaultCode4 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 40/40/4007)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 40/40/4007)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode4
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 40/40/4007)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 40/40/4007)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 40/40/4007)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode4)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode4)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode4 <> ''

                        Of 5
                            If stj:FaultCode5 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 50/50/5007)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 50/50/5007)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode5
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 50/50/5007)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 50/50/5007)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 50/50/5007)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode5)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode5)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode5 <> ''

                        Of 6
                            If stj:FaultCode6 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 60/60/6007)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 60/60/6007)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode6
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 60/60/6007)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 60/60/6007)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 60/60/6007)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode6)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode6)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode6 <> ''

                        Of 7
                            If stj:FaultCode7 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 70/70/7007)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 70/70/7007)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode7
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 70/70/7007)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 70/70/7007)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 70/70/7007)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode7)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode7)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode7 <> ''

                        Of 8
                            If stj:FaultCode8 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 80/80/8008)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 80/80/8008)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode8
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 80/80/8008)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 80/80/8008)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 80/80/8008)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode8)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode8)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode8 <> ''

                        Of 9
                            If stj:FaultCode9 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 90/90/9009)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 90/90/9009)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode9
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 90/90/9009)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 90/90/9009)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 90/90/9009)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode9)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode9)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode9 <> ''

                        Of 10
                            If stj:FaultCode10 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 100/100/100010)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 100/100/100010)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode10
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 100/100/100010)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 100/100/100010)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 100/100/100010)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode10)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode10)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode10 <> ''

                        Of 11
                            If stj:FaultCode11 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 110/110/110011)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 110/110/110011)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode11
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 110/110/110011)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 110/110/110011)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 110/110/110011)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode11)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode11)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode11 <> ''

                        Of 12
                            If stj:FaultCode12 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 120/120/120012)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 120/120/120012)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode12
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 120/120/120012)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 120/120/120012)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 120/120/120012)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode12)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode12)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode12 <> ''

                        Of 13
                            If stj:FaultCode13 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 130/130/130013)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 130/130/130013)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode13
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 130/130/130013)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 130/130/130013)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 130/130/130013)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode13)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode13)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode13 <> ''

                        Of 14
                            If stj:FaultCode14 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 140/140/140014)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 140/140/140014)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode14
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 140/140/140014)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 140/140/140014)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 140/140/140014)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode14)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode14)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode14 <> ''

                        Of 15
                            If stj:FaultCode15 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 150/150/150015)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 150/150/150015)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode15
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 150/150/150015)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 150/150/150015)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 150/150/150015)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode15)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode15)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode15 <> ''

                        Of 16
                            If stj:FaultCode16 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 160/160/160016)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 160/160/160016)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode16
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 160/160/160016)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = job:Manufacturer
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 160/160/160016)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 160/160/160016)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode16)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode16)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode16 <> ''

                        Of 17
                            If stj:FaultCode17 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 170/170/170017)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 170/170/170017)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode17
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 170/170/170017)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 170/170/170017)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 170/170/170017)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode17)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode17)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode17 <> ''

                        Of 18
                            If stj:FaultCode18 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 180/180/180018)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 180/180/180018)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode18
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 180/180/180018)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 180/180/180018)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 180/180/180018)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode18)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode18)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode18 <> ''

                        Of 19
                            If stj:FaultCode19 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 190/190/190019)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 190/190/190019)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode19
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 190/190/190019)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = job:Manufacturer
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 190/190/190019)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 190/190/190019)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode19)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode19)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode19 <> ''

                        Of 20
                            If stj:FaultCode20 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 200/200/200020)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 200/200/200020)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode20
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 200/200/200020)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = job:Manufacturer
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 200/200/200020)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 200/200/200020)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode20)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode20)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode20 <> ''

                        End ! Case maf:Field_Number
                    End ! If Access:STOMJFAU.TryFetch(stj:FieldKey) = Level:Benign
                End ! If Access:STOMODEL.TryFetch(stm:Mode_Number_Only_Key) = Level:Benign
            End ! If sto:Assign_Fault_Codes = 'YES'
        End ! If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
    End ! Loop
buildFaultCodes  routine
    data
locRequired     byte(0)
locFaultRequired    byte(0)
locChargeable   byte(0)
locWarranty     byte(0)
locHide         byte(0)
locReadOnly     byte(0)
locViewUnavailable  byte(0)
locAmendUnavailable byte(0)
locField            String(30)
locRepairIndex      Long()
locSetFaultCode     String(30)
    code
    if (p_web.GSV('JobFaultCodes:FirstTime') = 0)
        loop x# = 1 to 20
            !p_web.SSV('tmp:FaultCode' & x#,'')
            p_web.SSV('Hide:JobFaultCode' & x#,1)
            p_web.SSV('Req:JobFaultCode' & x#,0)
            p_web.SSV('ReadOnly:JobFaultCode' & x#,0)
            p_web.SSV('Prompt:JobFaultCode' & x#,'Fault Code ' & x#)
            p_web.SSV('Picture:JobFaultCode' & x#,'@s30')
            p_web.SSV('ShowDate:JobFaultCode' & x#,0)
            p_web.SSV('Lookup:JobFaultCode' & x#,0)
            p_web.SSV('Comment:JobFaultCode' & x#,'')
        end ! loop x# = 1 to 20
    end ! if (p_web.GSV('JobFaultCodes:FirstTime') = 0)

    if (p_web.GSV('job:Chargeable_Job') = 'YES')
        locChargeable = 1
    end ! if (p_web.GSV('job:Chargeable_Job') = 'YES')
    if (p_web.GSV('job:Warranty_Job') = 'YES')
        locWarranty = 1
    end ! if (p_web.GSV('job:Warranty_Job') = 'YES')

    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
    tra:Account_Number    = p_web.GSV('wob:HeadACcountNumber')
    if (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
        ! Found
        Access:LOCATION.Clearkey(loc:Location_Key)
        loc:Location    = tra:SiteLocation
        if (Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign)
            ! Found
        else ! if (Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign)
            ! Error
        end ! if (Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign)

    else ! if (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
        ! Error
    end ! if (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)

    if (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'VIEW UNAVAILABLE FAULT CODES') = 0)
        locViewUnavailable = 1
        if (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'AMEND UNAVAILABLE FAULT CODES') = 0)
            locAmendUnavailable = 1
        end ! if (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'AMEND UNAVAILABLE FAULT CODES') = 0)
    end ! if (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'VIEW UNAVAILABLE FAULT CODES') = 0)


    if (locChargeable = 1)
        Access:CHARTYPE.Clearkey(cha:Charge_Type_Key)
        cha:Charge_Type    = p_web.GSV('job:Charge_Type')
        if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)
            ! Found
            if (cha:Force_Warranty = 'YES')
                if (p_web.GSV('job:Repair_Type') <> '')
                    Access:REPTYDEF.Clearkey(rtd:ChaManRepairTypeKey)
                    rtd:Manufacturer    = p_web.GSV('job:Manufacturer')
                    rtd:Chargeable    = 'YES'
                    rtd:Repair_Type    = p_web.GSV('job:Repair_Type')
                    if (Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign)
                        ! Found
                        if (rtd:CompFaultCoding = 1)
                            locRequired = 1
                        end ! if (rtd:CompFaultCoding = 1)
                    else ! if (Access:RETYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign)
                        ! Error
                    end ! if (Access:RETYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign)

                else ! if (p_web.GSV('job:Repair_Type') <> '')
                    locRequired = 1
                end ! if (p_web.GSV('job:Repair_Type') <> '')

            end ! if (cha:Force_Warranty = 'YES')
        else ! if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)
            ! Error
        end ! if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)
    end ! if (p_web.GSV('job:Chargeable_Job') = 'YES')
    if (locWarranty = 1 and locRequired <> 1)
        Access:CHARTYPE.Clearkey(cha:Charge_Type_Key)
        cha:Charge_Type    = p_web.GSV('job:Warranty_Charge_Type')
        if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)
            ! Found
            if (cha:Force_Warranty = 'YES')
                if (p_web.GSV('job:Repair_Type') <> '')
                    Access:REPTYDEF.Clearkey(rtd:WarManRepairTypeKey)
                    rtd:Manufacturer    = p_web.GSV('job:Manufacturer')
                    rtd:Warranty    = 'YES'
                    rtd:Repair_Type    = p_web.GSV('job:Repair_Type_Warranty')
                    if (Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign)
                        ! Found
                        if (rtd:CompFaultCoding = 1)
                            locRequired = 1
                        end ! if (rtd:CompFaultCoding = 1)
                    else ! if (Access:RETYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign)
                        ! Error
                    end ! if (Access:RETYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign)

                else ! if (p_web.GSV('job:Repair_Type') <> '')
                    locRequired = 1
                end ! if (p_web.GSV('job:Repair_Type') <> '')
            end !if (cha:Force_Warranty = 'YES')
        else ! if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)
            ! Error
        end ! if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)
    end ! if (p_web.GSV('job:Warranty_Job') = 'YES')

    Access:MANFAULT.Clearkey(maf:ScreenOrderKey)
    maf:Manufacturer    = p_web.GSV('job:Manufacturer')
    maf:ScreenOrder    = 0
    set(maf:ScreenOrderKey,maf:ScreenOrderKey)
    loop
        if (Access:MANFAULT.Next())
            Break
        end ! if (Access:MANFAULT.Next())
        if (maf:Manufacturer    <> p_web.GSV('job:Manufacturer'))
            Break
        end ! if (maf:Manufacturer    <> job:Manufacturer)
        if (maf:ScreenOrder    = 0)
            Cycle
        end ! if (maf:ScreenOrder    <> 0)
        if (maf:MainFault)
            p_web.SSV('ReadOnly:JobFaultCode' & maf:ScreenOrder,1)
            p_web.SSV('Lookup:JobFaultCode' & maf:ScreenOrder,0)
            p_web.SSV('Req:JobFaultCode' & maf:ScreenOrder,0)
            p_web.SSV('Hide:JobFaultCode' & maf:ScreenOrder,0)
            p_web.SSV('Prompt:JobFaultCode' & maf:ScreenOrder,maf:Field_Name)
            Access:MANFAULO.Clearkey(mfo:Field_Key)
            mfo:Manufacturer    = maf:Manufacturer
            mfo:Field_Number    = maf:Field_Number
            mfo:Field    = p_web.GSV('tmp:FaultCode' & maf:ScreenOrder)
            if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                        ! Found
                p_web.SSV('Comment:JobFaultCode' & maf:ScreenOrder,clip(mfo:Description))
            else ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                        ! Error
                p_web.SSV('Comment:JobFaultCode' & maf:ScreenOrder,'')
            end ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
            cycle
        end ! if (maf:MainFault)

        locHide = 0
        locReadOnly = 0

        if (locWarranty = 1)
            if (maf:Compulsory_At_Booking = 'YES')
                if (locRequired)
                    locFaultRequired = 1
                end ! if (locRequired)
            end ! if (maf:Compulsory_At_Booking = 'YES')
            if (maf:Compulsory = 'YES' and locRequired)
                locFaultRequired = 1
            end !if (maf:Compulsory = 'YES' and locRequired)
        end ! if (loc:Warranty = 1)

        if (locChargeable = 1)
            if (maf:CharCompulsoryBooking)
                if (locRequired)
                    locFaultRequired = 1
                end ! if (locRequird)
            end ! if (maf:CharCompulsoryBooking)
            if (maf:CharCompulsory and locRequired)
                locFaultRequired = 1
            end ! if (maf:CharCompulsory and locRequired)
        end ! if (locChargeable = 1)

        if (maf:RestrictAvailability)
            If ((maf:RestrictServiceCentre = 1 and ~loc:Level1) Or |
                (maf:RestrictServiceCentre = 2 and ~loc:Level2) Or |
                (maf:RestrictServiceCentre = 3 and ~loc:Level3))
                if (locViewUnavailable = 0)
                    locHide = 1
                else ! if (locViewUnavailable = 0)
                    if (locAmendUnavailable = 0)
                        locReadOnly = 1
                    end ! if (locAmendUnavailable = 0)
                end ! if (locViewUnavailable = 0)
            end
        end ! if (maf:RestrictAvailability)

        if (maf:NotAvailable)
            if (locViewUnavailable = 0)
                locHide = 1
            else ! if (locViewUnavailable = 0)
                if (locAmendUnavailable = 0)
                    if (p_web.GSV('tmp:FaultCode' & maf:ScreenOrder) = '')
                        locHide = 1
                    else ! if (p_web.GSV('tmp:FaultCode' & maf:ScreenOrder) = '')
                        locReadonly = 1
                    end ! if (p_web.GSV('tmp:FaultCode' & maf:ScreenOrder) = '')
                end ! if (locAmendUnavailable = 0)
            end ! if (locViewUnavailable = 0)
        end ! if (maf:NotAvailable)

        if (locHide = 0)
            if (maf:HideRelatedCodeIfBlank)

                Access:MANFAULT_ALIAS.Clearkey(maf_ali:Field_Number_Key)
                maf_ali:Manufacturer    = p_web.GSV('job:Manufacturer')
                maf_ali:Field_Number    = maf:Field_Number
                if (Access:MANFAULT_ALIAS.TryFetch(maf_ali:Field_Number_Key) = Level:Benign)
                    ! Found
                    if (p_web.GSV('tmp:FaultCode' & maf:ScreenOrder) = '')
                        locHide = 1
                    end ! if (p_web.GSV('tmp:FaultCode' & maf:ScreenOrder) = '')
                else ! if (Access:MANFAULT_ALIAS.TryFetch(maf_ali:Field_Number_Key) = Level:Benign)
                    ! Error
                end ! if (Access:MANFAULT_ALIAS.TryFetch(maf_ali:Field_Number_Key) = Level:Benign)

            end ! if (maf:HideRelatedCodeIfBlank)
        end ! if (locHide = 0)

        if (p_web.GSV('job:Third_party_Site') <> '')
            if (locRequired = 1)
                if (maf:NotCompulsoryThirdParty)
                    locRequired = 0
                    if (maf:HideThirdParty)
                        locHide = 1
                    end ! if (maf:HideThirdParty)
                end ! if (maf:NotCompulsoryThirdParty)
            end ! if (locRequired = 1)
        end ! if (p_web.GSV('job:Third_party_Site') <> '')

        if (locHide = 0)
            p_web.SSV('Hide:JobFaultCode' & maf:ScreenOrder,0)
            p_web.SSV('Prompt:JobFaultCode' & maf:ScreenOrder,maf:Field_Name)

            if (locRequired = 0)
                Access:WARPARTS.Clearkey(wpr:Part_Number_Key)
                wpr:Ref_Number    = p_web.GSV('job:Ref_Number')
                set(wpr:Part_Number_Key,wpr:Part_Number_Key)
                loop
                    if (Access:WARPARTS.Next())
                        Break
                    end ! if (Access:WARPARTS.Next())
                    if (wpr:Ref_Number    <> p_web.GSV('job:Ref_Number'))
                        Break
                    end ! if (wpr:Ref_Number    <> p_web.GSV('job:Ref_Number'))

                    If local.PartForceFaultCode(1,wpr:Fault_Code1,maf:Field_Number)
                        locRequired = 1
                        Break
                    End ! If local.PartForceFaultCode(1,wpr:Fault_Code1,maf:Field_Number)
                    If local.PartForceFaultCode(2,wpr:Fault_Code2,maf:Field_Number)
                        locRequired = 1
                        Break
                    End ! If local.PartForceFaultCode(1,wpr:Fault_Code1,maf:Field_Number)
                    If local.PartForceFaultCode(3,wpr:Fault_Code3,maf:Field_Number)
                        locRequired = 1
                        Break
                    End ! If local.PartForceFaultCode(1,wpr:Fault_Code1,maf:Field_Number)
                    If local.PartForceFaultCode(4,wpr:Fault_Code4,maf:Field_Number)
                        locRequired = 1
                        Break
                    End ! If local.PartForceFaultCode(1,wpr:Fault_Code1,maf:Field_Number)
                    If local.PartForceFaultCode(5,wpr:Fault_Code5,maf:Field_Number)
                        locRequired = 1
                        Break
                    End ! If local.PartForceFaultCode(1,wpr:Fault_Code1,maf:Field_Number)
                    If local.PartForceFaultCode(6,wpr:Fault_Code6,maf:Field_Number)
                        locRequired = 1
                        Break
                    End ! If local.PartForceFaultCode(1,wpr:Fault_Code1,maf:Field_Number)
                    If local.PartForceFaultCode(7,wpr:Fault_Code7,maf:Field_Number)
                        locRequired = 1
                        Break
                    End ! If local.PartForceFaultCode(1,wpr:Fault_Code1,maf:Field_Number)
                    If local.PartForceFaultCode(8,wpr:Fault_Code8,maf:Field_Number)
                        locRequired = 1
                        Break
                    End ! If local.PartForceFaultCode(1,wpr:Fault_Code1,maf:Field_Number)
                    If local.PartForceFaultCode(9,wpr:Fault_Code9,maf:Field_Number)
                        locRequired = 1
                        Break
                    End ! If local.PartForceFaultCode(1,wpr:Fault_Code1,maf:Field_Number)
                    If local.PartForceFaultCode(10,wpr:Fault_Code10,maf:Field_Number)
                        locRequired = 1
                        Break
                    End ! If local.PartForceFaultCode(1,wpr:Fault_Code1,maf:Field_Number)
                    If local.PartForceFaultCode(11,wpr:Fault_Code11,maf:Field_Number)
                        locRequired = 1
                        Break
                    End ! If local.PartForceFaultCode(1,wpr:Fault_Code1,maf:Field_Number)
                    If local.PartForceFaultCode(12,wpr:Fault_Code12,maf:Field_Number)
                        locRequired = 1
                        Break
                    End ! If local.PartForceFaultCode(1,wpr:Fault_Code1,maf:Field_Number)
                end ! loop
            end ! if (locRequired = 0)

!            if (locRequired)
!                p_web.SSV('Req:JobFaultCode' & maf:ScreenOrder,1)
!            else ! if (locRequired)
!                p_web.SSV('Req:JobFaultCode' & maf:ScreenOrder,0)
!            end ! if (locRequired)

            case (maf:Field_Type)
            of 'DATE'
                p_web.SSV('Picture:JobFaultCode' & maf:ScreenOrder,clip(maf:DateType))
                p_web.SSV('ShowDate:JobFaultCode' & maf:ScreenOrder,1)
            of 'STRING'
                if (maf:RestrictLength)
                    p_web.SSV('Picture:JobFaultCode' & maf:ScreenOrder,'@s' & maf:LengthTo)
                else ! if (maf:RestrictLength)
                    if (maf:Field_Number = 10 or maf:Field_Number = 11 or maf:Field_Number = 12)
                        p_web.SSV('Picture:JobFaultCode' & maf:ScreenOrder,'@s255')
                    else ! if (maf:Field_Number = 10 or maf:Field_Number = 11 or maf:Field_Number = 12)
                        p_web.SSV('Picture:JobFaultCode' & maf:ScreenOrder,'@s30')
                    end ! if (maf:Field_Number = 10 or maf:Field_Number = 11 or maf:Field_Number = 12)
                end ! if (maf:RestrictLength)

                if (maf:Lookup = 'YES')
                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                    mfo:Manufacturer    = maf:Manufacturer
                    mfo:Field_Number    = maf:Field_Number
                    mfo:Field    = p_web.GSV('tmp:FaultCode' & maf:ScreenOrder)
                    if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                        ! Found
                        p_web.SSV('Comment:JobFaultCode' & maf:ScreenOrder,clip(mfo:Description))
                    else ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                        ! Error
                        p_web.SSV('Comment:JobFaultCode' & maf:ScreenOrder,'')
                    end ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                    if (maf:Force_Lookup = 'YES')
                        locReadOnly =1
                    end ! if (maf:Force_Lookup = 'YES')
                else ! if (maf:Lookup = 'YES')
                    p_web.SSV('Comment:JobFaultCode' & maf:ScreenOrder,'')
                end ! if (maf:Lookup = 'YES')
            of 'NUMBER'
                if (maf:RestrictLength)
                    p_web.SSV('Picture:JobFaultCode' & maf:ScreenOrder,'@n_' & maf:LengthTo)
                else ! if (maf:RestrictLength)
                    p_web.SSV('Picture:JobFaultCode' & maf:ScreenOrder,'@n_9')
                end ! if (maf:RestrictLength)
            end ! case (maf:Field_Type)

            if (maf:GenericFault)
                if (securityCheckFailed(p_web.GSV('BookingUserPassword'),'GENERIC FAULT - AMEND'))
                    locReadOnly = 1
                end ! if (securityCheckFailed(p_web.GSV('BookingUserPassword'),'GENERIC FAULT - AMEND'))
            end ! if (maf:GenericFault)

            if (maf:MainFault)
                locReadOnly = 1
            end ! if (maf:MainFault)

            if (locReadonly)
                p_web.SSV('ReadOnly:JobFaulCode' & maf:ScreenOrder,1)
                p_web.SSV('Req:JobFaultCode' & maf:ScreenOrder,0)
                p_web.SSV('Lookup:JobFaultCode' & maf:ScreenOrder,0)
            end ! if (locReadonly)

            if (maf:Lookup = 'YES')
                p_web.SSV('Lookup:JobFaultCode' & maf:ScreenOrder,1)
                if (p_web.GSV('tmp:FaultCode' & maf:ScreenOrder) <> '' and |
                    p_web.GSV('Hide:JobFaultCode' & maf:ScreenOrder) = 0 and |
                    p_web.GSV('ReadOnly:JobFaultCode' & maf:ScreenOrder) = 0)

                    Access:MANFAULO.Clearkey(mfo:PrimaryLookupKey)
                    mfo:Manufacturer    = maf:Manufacturer
                    mfo:Field_Number    = maf:Field_Number
                    mfo:PrimaryLookup    = 1
                    if (Access:MANFAULO.TryFetch(mfo:PrimaryLookupKey) = Level:Benign)
                        ! Found
                        if (mfo:JobTypeAvailability = 0)
                            p_web.SSV('tmp:FaultCode' & maf:ScreenOrder,mfo:Field)
                        elsif mfo:JobTypeAvailability = 1
                            if (locChargeable = 1)
                                p_web.SSV('tmp:FaultCode' & maf:ScreenOrder,mfo:Field)
                            end ! if (locChargeable = 1)

                        else ! if (mfo:JobTypeAvailability = 0)
                           if (locWarranty = 1)
                                p_web.SSV('tmp:FaultCode' & maf:ScreenOrder,mfo:Field)
                            end ! if (locWarranty = 1)
                        end !if (mfo:JobTypeAvailability = 0)
                    else ! if (Access:MANFAULO.TryFetch(mfo:PrimaryLookupKey) = Level:Benign)
                        ! Error
                    end ! if (Access:MANFAULO.TryFetch(mfo:PrimaryLookupKey) = Level:Benign)

                end ! if (maf:Lookup = 'YES')
                if (p_web.GSV('tmp:FaultCode' & maf:ScreenOrder) <> '' and |
                    p_web.GSV('Hide:JobFaultCode' & maf:ScreenOrder) = 0 and |
                    p_web.GSV('ReadOnly:JobFaultCode' & maf:ScreenOrder) = 0)

                    locField = ''
                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                    mfo:Manufacturer    = maf:Manufacturer
                    mfo:Field_Number    = maf:Field_Number
                    set(mfo:Field_Key,mfo:Field_Key)
                    loop
                        if (Access:MANFAULO.Next())
                            Break
                        end ! if (Access:MANFAULO.Next())
                        if (mfo:Manufacturer    <> maf:Manufacturer)
                            Break
                        end ! if (mfo:Manufacturer    <> maf:Manufacturer)
                        if (mfo:Field_Number    <> maf:Field_Number)
                            Break
                        end ! if (mfo:Field_Number    <> maf:Field_Number)

                        if (mfo:JobTypeAvailability = 1)
                            if (locChargeable = 0)
                                cycle
                            end ! if (locWarranty <> 1)
                        end ! if (mfo:JobTypeAvailability = 1)
                        if (mfo:JobTypeAvailability = 2)
                            if (locWarranty = 0)
                                cycle
                            end ! if (locWarranty = 0)
                        end ! if (mfo:JobTypeAvailability = 2)

                        count# += 1
                        if (count# > 1)
                           locField = ''
                            break
                        end ! if (count# > 1)
                        locField = mfo:Field
                    end ! loop
                    if (locField <> '')
                        p_web.SSV('tmp:FaultCode' & maf:ScreenOrder,locField)
                    end ! if (locField <> '')
                end
            end ! if (maf:Lookup = 'YES')

            if (maf:FillFromDOP)
                if (p_web.GSV('tmp:FaultCode' & maf:ScreenOrder) = '')
                    p_web.SSV('tmp:FaultCode' & maf:ScreenOrder,p_web.GSV('job:DOP'))
                end ! if (p_web.GSV('tmp:FaultCode' & maf:ScreenOrder) = '')
            end ! if (maf:FillFromDOP)

            if (p_web.GSV('tmp:FaultCode' & maf:ScreenOrder) = '')
                do checkStockModelFaultCodes
            end ! if (p_web.GSV('tmp:FaultCode' & maf:ScreenOrder) = '')

            locRepairIndex = 0
            locSetFaultCode = ''

            Access:MANFAULT_ALIAS.Clearkey(maf_ali:MainFaultKey)
            maf_ali:Manufacturer    = p_web.GSV('job:Manufacturer')
            maf_ali:MainFault    = 1
            if (Access:MANFAULT_ALIAS.TryFetch(maf_ali:MainFaultKey) = Level:Benign)
                ! Found

                Access:JOBOUTFL.Clearkey(joo:JobNumberKey)
                joo:JobNumber    = p_web.GSV('job:Ref_number')
                set(joo:JobNumberKey,joo:JobNumberKey)
                loop
                    if (Access:JOBOUTFL.Next())
                        Break
                    end ! if (Access:JOBOUTFL.Next())
                    if (joo:JobNumber    <> p_web.GSV('job:Ref_number'))
                        Break
                    end ! if (joo:JobNumber    <> p_web.GSV('job:Ref_number'))

                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                    mfo:Manufacturer    = maf:Manufacturer
                    mfo:Field_Number    = maf_ali:Field_Number
                    mfo:Field    = joo:FaultCode
                    if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                        ! Found
                        if (mfo:SetJobFaultCode)
                            if (mfo:SelectJobFaultCode = maf:Field_Number)
                                if (mfo:SkillLevel >= locRepairIndex)
                                    locSetFaultCode = mfo:JobFaultCodeValue
                                end ! if (mfo:SkillLevel >= locRepairIndex)
                            end ! if (mfo:SelectJobFaultCode = maf:Field_Number)
                        end ! if (mfo:SetJobFaultCode)
                    else ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                        ! Error
                    end ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)

                end ! loop
            else ! if (Access:MANFAULT_ALIAS.TryFetch(maf_ali:MainFaultKey) = Level:Benign)
                ! Error
            end ! if (Access:MANFAULT_ALIAS.TryFetch(maf_ali:MainFaultKey) = Level:Benign)
            if (locSetFaultCode <> '')
                p_web.SSV('tmp:FaultCode' & maf:ScreenOrder,locSetFaultCode)
            end ! if (locSetFaultCode <> '')
        else! if (locHide = 0)
            p_web.SSV('Prompt:JobFaultCode' & maf:ScreenOrder,'')
            p_web.SSV('Comment:JobFaultCode' & maf:ScreenOrder,'')
            p_web.SSV('Hide:JobFaultCode' & maf:ScreenOrder,1)
        end ! if (locHide = 0)
    end ! loop
ValidateFaultCodes      routine
    case p_web.GSV('job:manufacturer')
    of 'MOTOROLA' Orof 'SAMSUNG' orof 'PHILIPS'
        if (dateCodeValidation(p_web.GSV('job:manufacturer'),p_web.GSV('job:MSN'),p_web.GSV('job:date_Booked')))
            p_web.SSV('locNextURL','ProofOfPurchase')
        else ! if (dateCodeValidation('MOTOROLA',p_web.GSV('job:MSN'),p_web.GSV('job:date_Booked'))
            p_web.SSV('job:warranty_Job','YES')
            p_web.SSV('job:warranty_Charge_Type','WARRANTY (MFTR)')
            if (p_web.GSV('job:POP') = '')
                p_web.SSV('job:POP','F')
            end ! if (p_web.GSV('job:POP') = '')
        end ! if (dateCodeValidation('MOTOROLA',p_web.GSV('job:MSN'),p_web.GSV('job:date_Booked'))
    of 'ALCATEL' orof 'SIEMENS'
        if (p_web.GSV('job:fault_code3') <> '' and p_web.GSV('job:POP') = '')
            if (dateCodeValidation(p_web.GSV('job:manufacturer'),p_web.GSV('job:fault_code3'),p_web.GSV('job:date_Booked')))
                p_web.SSV('locNextURL','ProofOfPurchase')
            else ! if (dateCodeValidation('MOTOROLA',p_web.GSV('job:MSN'),p_web.GSV('job:date_Booked'))
                p_web.SSV('job:warranty_Job','YES')
                p_web.SSV('job:warranty_Charge_Type','WARRANTY (MFTR)')
                if (p_web.GSV('job:POP') = '')
                    p_web.SSV('job:POP','F')
                end ! if (p_web.GSV('job:POP') = '')
            end ! if (dateCodeValidation('MOTOROLA',p_web.GSV('job:MSN'),p_web.GSV('job:date_Booked'))
        end ! if (p_web.GSV('job:fault_code3') <> '')
    of 'ERICSSON'
        if (p_web.GSV('job:fault_code7') <> '' and p_web.GSV('job:POP') = '')
            if (dateCodeValidation(p_web.GSV('job:manufacturer'),p_web.GSV('job:fault_code7') & p_web.GSV('job:fault_code8'),p_web.GSV('job:date_Booked')))
                p_web.SSV('locNextURL','ProofOfPurchase')
            else ! if (dateCodeValidation('MOTOROLA',p_web.GSV('job:MSN'),p_web.GSV('job:date_Booked'))
                p_web.SSV('job:warranty_Job','YES')
                p_web.SSV('job:warranty_Charge_Type','WARRANTY (MFTR)')
                if (p_web.GSV('job:POP') = '')
                    p_web.SSV('job:POP','F')
                end ! if (p_web.GSV('job:POP') = '')
            end ! if (dateCodeValidation('MOTOROLA',p_web.GSV('job:MSN'),p_web.GSV('job:date_Booked'))
        end ! if (p_web.GSV('job:fault_code7') <> '')
    of 'BOSCH'
        if (p_web.GSV('job:fault_code7') <> '' and p_web.GSV('job:POP') = '')
            if (dateCodeValidation(p_web.GSV('job:manufacturer'),p_web.GSV('job:fault_code7') ,p_web.GSV('job:date_Booked')))
                p_web.SSV('locNextURL','ProofOfPurchase')
            else ! if (dateCodeValidation('MOTOROLA',p_web.GSV('job:MSN'),p_web.GSV('job:date_Booked'))
                p_web.SSV('job:warranty_Job','YES')
                p_web.SSV('job:warranty_Charge_Type','WARRANTY (MFTR)')
                if (p_web.GSV('job:POP') = '')
                    p_web.SSV('job:POP','F')
                end ! if (p_web.GSV('job:POP') = '')
            end ! if (dateCodeValidation('MOTOROLA',p_web.GSV('job:MSN'),p_web.GSV('job:date_Booked'))
        end ! if (p_web.GSV('job:fault_code7') <> '')
    end ! case p_web.GSV('job:manufacturer')

    if (p_web.GSV('job:DOP') = '')
        p_web.SSV('locNextURL','ProofOfPurchase')
    end ! if (p_web.GSV('job:DOP') = '')
OpenFiles  ROUTINE
  p_web._OpenFile(AUDIT)
  p_web._OpenFile(CHARTYPE)
  p_web._OpenFile(REPTYDEF)
  p_web._OpenFile(MANFAULO_ALIAS)
  p_web._OpenFile(MANFAULO)
  p_web._OpenFile(JOBOUTFL)
  p_web._OpenFile(MANFAULT_ALIAS)
  p_web._OpenFile(MANFAULT)
  p_web._OpenFile(WARPARTS)
  p_web._OpenFile(STOCK)
  p_web._OpenFile(STOMODEL)
  p_web._OpenFile(STOMJFAU)
  p_web._OpenFile(MANFAUPA)
  p_web._OpenFile(MANFPALO)
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(LOCATION)
  p_web._OpenFile(MANUFACT)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(MODPROD)
  p_web._OpenFile(SBO_OutFaultParts)
  p_web._OpenFile(PARTS)
  p_web._OpenFile(USERS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(AUDIT)
  p_Web._CloseFile(CHARTYPE)
  p_Web._CloseFile(REPTYDEF)
  p_Web._CloseFile(MANFAULO_ALIAS)
  p_Web._CloseFile(MANFAULO)
  p_Web._CloseFile(JOBOUTFL)
  p_Web._CloseFile(MANFAULT_ALIAS)
  p_Web._CloseFile(MANFAULT)
  p_Web._CloseFile(WARPARTS)
  p_Web._CloseFile(STOCK)
  p_Web._CloseFile(STOMODEL)
  p_Web._CloseFile(STOMJFAU)
  p_Web._CloseFile(MANFAUPA)
  p_Web._CloseFile(MANFPALO)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(LOCATION)
  p_Web._CloseFile(MANUFACT)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(MODPROD)
  p_Web._CloseFile(SBO_OutFaultParts)
  p_Web._CloseFile(PARTS)
  p_Web._CloseFile(USERS)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE
  loc:EipClm = upper(p_web.GetValue('_EIPClm'))
  p_web.DeleteValue('_EIPClm')
  case loc:EipClm
  of upper('buttonRepairNotes')
    do Validate::buttonRepairNotes
  of upper('buttonOutFaults')
    do Validate::buttonOutFaults
  of ''
    case upper(p_web.GetValue('_calledfrom_'))
    end
  end

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('JobFaultCodes_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'JobFaultCodes'
    end
    p_web.formsettings.proc = 'JobFaultCodes'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine
  p_web.deletesessionvalue('JobFaultCodes:FirstTime')

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('job:ProductCode')
    p_web.SetPicture('job:ProductCode','@s30')
  End
  p_web.SetSessionPicture('job:ProductCode','@s30')
  If p_web.IfExistsValue('tmp:FaultCode1')
    p_web.SetPicture('tmp:FaultCode1',p_web.GSV('Picture:JobFaultCode1'))
  End
  p_web.SetSessionPicture('tmp:FaultCode1',p_web.GSV('Picture:JobFaultCode1'))
  If p_web.IfExistsValue('tmp:FaultCode2')
    p_web.SetPicture('tmp:FaultCode2',p_web.GSV('Picture:JobFaultCode2'))
  End
  p_web.SetSessionPicture('tmp:FaultCode2',p_web.GSV('Picture:JobFaultCode2'))
  If p_web.IfExistsValue('tmp:FaultCode3')
    p_web.SetPicture('tmp:FaultCode3',p_web.GSV('Picture:JobFaultCode3'))
  End
  p_web.SetSessionPicture('tmp:FaultCode3',p_web.GSV('Picture:JobFaultCode3'))
  If p_web.IfExistsValue('tmp:FaultCode4')
    p_web.SetPicture('tmp:FaultCode4',p_web.GSV('Picture:JobFaultCode4'))
  End
  p_web.SetSessionPicture('tmp:FaultCode4',p_web.GSV('Picture:JobFaultCode4'))
  If p_web.IfExistsValue('tmp:FaultCode5')
    p_web.SetPicture('tmp:FaultCode5',p_web.GSV('Picture:JobFaultCode5'))
  End
  p_web.SetSessionPicture('tmp:FaultCode5',p_web.GSV('Picture:JobFaultCode5'))
  If p_web.IfExistsValue('tmp:FaultCode6')
    p_web.SetPicture('tmp:FaultCode6',p_web.GSV('Picture:JobFaultCode6'))
  End
  p_web.SetSessionPicture('tmp:FaultCode6',p_web.GSV('Picture:JobFaultCode6'))
  If p_web.IfExistsValue('tmp:FaultCode7')
    p_web.SetPicture('tmp:FaultCode7',p_web.GSV('Picture:JobFaultCode7'))
  End
  p_web.SetSessionPicture('tmp:FaultCode7',p_web.GSV('Picture:JobFaultCode7'))
  If p_web.IfExistsValue('tmp:FaultCode8')
    p_web.SetPicture('tmp:FaultCode8',p_web.GSV('Picture:JobFaultCode8'))
  End
  p_web.SetSessionPicture('tmp:FaultCode8',p_web.GSV('Picture:JobFaultCode8'))
  If p_web.IfExistsValue('tmp:FaultCode9')
    p_web.SetPicture('tmp:FaultCode9',p_web.GSV('Picture:JobFaultCode9'))
  End
  p_web.SetSessionPicture('tmp:FaultCode9',p_web.GSV('Picture:JobFaultCode9'))
  If p_web.IfExistsValue('tmp:FaultCode10')
    p_web.SetPicture('tmp:FaultCode10',p_web.GSV('Picture:JobFaultCode10'))
  End
  p_web.SetSessionPicture('tmp:FaultCode10',p_web.GSV('Picture:JobFaultCode10'))
  If p_web.IfExistsValue('tmp:FaultCode11')
    p_web.SetPicture('tmp:FaultCode11',p_web.GSV('Picture:JobFaultCode11'))
  End
  p_web.SetSessionPicture('tmp:FaultCode11',p_web.GSV('Picture:JobFaultCode11'))
  If p_web.IfExistsValue('tmp:FaultCode12')
    p_web.SetPicture('tmp:FaultCode12',p_web.GSV('Picture:JobFaultCode12'))
  End
  p_web.SetSessionPicture('tmp:FaultCode12',p_web.GSV('Picture:JobFaultCode12'))
  If p_web.IfExistsValue('tmp:FaultCode13')
    p_web.SetPicture('tmp:FaultCode13',p_web.GSV('Picture:JobFaultCode13'))
  End
  p_web.SetSessionPicture('tmp:FaultCode13',p_web.GSV('Picture:JobFaultCode13'))
  If p_web.IfExistsValue('tmp:FaultCode14')
    p_web.SetPicture('tmp:FaultCode14',p_web.GSV('Picture:JobFaultCode14'))
  End
  p_web.SetSessionPicture('tmp:FaultCode14',p_web.GSV('Picture:JobFaultCode14'))
  If p_web.IfExistsValue('tmp:FaultCode15')
    p_web.SetPicture('tmp:FaultCode15',p_web.GSV('Picture:JobFaultCode15'))
  End
  p_web.SetSessionPicture('tmp:FaultCode15',p_web.GSV('Picture:JobFaultCode15'))
  If p_web.IfExistsValue('tmp:FaultCode16')
    p_web.SetPicture('tmp:FaultCode16',p_web.GSV('Picture:JobFaultCode16'))
  End
  p_web.SetSessionPicture('tmp:FaultCode16',p_web.GSV('Picture:JobFaultCode16'))
  If p_web.IfExistsValue('tmp:FaultCode17')
    p_web.SetPicture('tmp:FaultCode17',p_web.GSV('Picture:JobFaultCode17'))
  End
  p_web.SetSessionPicture('tmp:FaultCode17',p_web.GSV('Picture:JobFaultCode17'))
  If p_web.IfExistsValue('tmp:FaultCode18')
    p_web.SetPicture('tmp:FaultCode18',p_web.GSV('Picture:JobFaultCode18'))
  End
  p_web.SetSessionPicture('tmp:FaultCode18',p_web.GSV('Picture:JobFaultCode18'))
  If p_web.IfExistsValue('tmp:FaultCode19')
    p_web.SetPicture('tmp:FaultCode19',p_web.GSV('Picture:JobFaultCode19'))
  End
  p_web.SetSessionPicture('tmp:FaultCode19',p_web.GSV('Picture:JobFaultCode19'))
  If p_web.IfExistsValue('tmp:FaultCode20')
    p_web.SetPicture('tmp:FaultCode20',p_web.GSV('Picture:JobFaultCode20'))
  End
  p_web.SetSessionPicture('tmp:FaultCode20',p_web.GSV('Picture:JobFaultCode20'))

AfterLookup Routine
  loc:TabNumber = -1
  If p_web.GSV('MSNValidation') = 1
    loc:TabNumber += 1
  End
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'job:ProductCode'
    p_web.setsessionvalue('showtab_JobFaultCodes',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(MODPROD)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:FaultCode1')
  of 'tmp:FaultCode1'
      local.AfterFaultCodeLookup(1)
  of 'tmp:FaultCode2'
      local.AfterFaultCodeLookup(2)
  of 'tmp:FaultCode3'
      local.AfterFaultCodeLookup(3)
  of 'tmp:FaultCode4'
      local.AfterFaultCodeLookup(4)
  of 'tmp:FaultCode5'
      local.AfterFaultCodeLookup(5)
  of 'tmp:FaultCode6'
      local.AfterFaultCodeLookup(6)
  of 'tmp:FaultCode7'
      local.AfterFaultCodeLookup(7)
  of 'tmp:FaultCode8'
      local.AfterFaultCodeLookup(8)
  of 'tmp:FaultCode9'
      local.AfterFaultCodeLookup(9)
  of 'tmp:FaultCode10'
      local.AfterFaultCodeLookup(10)
  of 'tmp:FaultCode11'
      local.AfterFaultCodeLookup(11)
  of 'tmp:FaultCode12'
      local.AfterFaultCodeLookup(12)
  of 'tmp:FaultCode13'
      local.AfterFaultCodeLookup(13)
  of 'tmp:FaultCode14'
      local.AfterFaultCodeLookup(14)
  of 'tmp:FaultCode15'
      local.AfterFaultCodeLookup(15)
  of 'tmp:FaultCode16'
      local.AfterFaultCodeLookup(16)
  of 'tmp:FaultCode17'
      local.AfterFaultCodeLookup(17)
  of 'tmp:FaultCode18'
      local.AfterFaultCodeLookup(18)
  of 'tmp:FaultCode19'
      local.AfterFaultCodeLookup(19)
  of 'tmp:FaultCode20'
      local.AfterFaultCodeLookup(20)
  
  End
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  if p_web.IfExistsValue('tmp:MSN') = 0
    p_web.SetSessionValue('tmp:MSN',tmp:MSN)
  Else
    tmp:MSN = p_web.GetSessionValue('tmp:MSN')
  End
  if p_web.IfExistsValue('tmp:VerifyMSN') = 0
    p_web.SetSessionValue('tmp:VerifyMSN',tmp:VerifyMSN)
  Else
    tmp:VerifyMSN = p_web.GetSessionValue('tmp:VerifyMSN')
  End
  if p_web.IfExistsValue('tmp:ConfirmMSNChange') = 0
    p_web.SetSessionValue('tmp:ConfirmMSNChange',tmp:ConfirmMSNChange)
  Else
    tmp:ConfirmMSNChange = p_web.GetSessionValue('tmp:ConfirmMSNChange')
  End
  if p_web.IfExistsValue('job:ProductCode') = 0
    p_web.SetSessionValue('job:ProductCode',job:ProductCode)
  Else
    job:ProductCode = p_web.GetSessionValue('job:ProductCode')
  End
  if p_web.IfExistsValue('tmp:FaultCode1') = 0
    p_web.SetSessionValue('tmp:FaultCode1',tmp:FaultCode1)
  Else
    tmp:FaultCode1 = p_web.GetSessionValue('tmp:FaultCode1')
  End
  if p_web.IfExistsValue('tmp:FaultCode2') = 0
    p_web.SetSessionValue('tmp:FaultCode2',tmp:FaultCode2)
  Else
    tmp:FaultCode2 = p_web.GetSessionValue('tmp:FaultCode2')
  End
  if p_web.IfExistsValue('tmp:FaultCode3') = 0
    p_web.SetSessionValue('tmp:FaultCode3',tmp:FaultCode3)
  Else
    tmp:FaultCode3 = p_web.GetSessionValue('tmp:FaultCode3')
  End
  if p_web.IfExistsValue('tmp:FaultCode4') = 0
    p_web.SetSessionValue('tmp:FaultCode4',tmp:FaultCode4)
  Else
    tmp:FaultCode4 = p_web.GetSessionValue('tmp:FaultCode4')
  End
  if p_web.IfExistsValue('tmp:FaultCode5') = 0
    p_web.SetSessionValue('tmp:FaultCode5',tmp:FaultCode5)
  Else
    tmp:FaultCode5 = p_web.GetSessionValue('tmp:FaultCode5')
  End
  if p_web.IfExistsValue('tmp:FaultCode6') = 0
    p_web.SetSessionValue('tmp:FaultCode6',tmp:FaultCode6)
  Else
    tmp:FaultCode6 = p_web.GetSessionValue('tmp:FaultCode6')
  End
  if p_web.IfExistsValue('tmp:FaultCode7') = 0
    p_web.SetSessionValue('tmp:FaultCode7',tmp:FaultCode7)
  Else
    tmp:FaultCode7 = p_web.GetSessionValue('tmp:FaultCode7')
  End
  if p_web.IfExistsValue('tmp:FaultCode8') = 0
    p_web.SetSessionValue('tmp:FaultCode8',tmp:FaultCode8)
  Else
    tmp:FaultCode8 = p_web.GetSessionValue('tmp:FaultCode8')
  End
  if p_web.IfExistsValue('tmp:FaultCode9') = 0
    p_web.SetSessionValue('tmp:FaultCode9',tmp:FaultCode9)
  Else
    tmp:FaultCode9 = p_web.GetSessionValue('tmp:FaultCode9')
  End
  if p_web.IfExistsValue('tmp:FaultCode10') = 0
    p_web.SetSessionValue('tmp:FaultCode10',tmp:FaultCode10)
  Else
    tmp:FaultCode10 = p_web.GetSessionValue('tmp:FaultCode10')
  End
  if p_web.IfExistsValue('tmp:FaultCode11') = 0
    p_web.SetSessionValue('tmp:FaultCode11',tmp:FaultCode11)
  Else
    tmp:FaultCode11 = p_web.GetSessionValue('tmp:FaultCode11')
  End
  if p_web.IfExistsValue('tmp:FaultCode12') = 0
    p_web.SetSessionValue('tmp:FaultCode12',tmp:FaultCode12)
  Else
    tmp:FaultCode12 = p_web.GetSessionValue('tmp:FaultCode12')
  End
  if p_web.IfExistsValue('tmp:FaultCode13') = 0
    p_web.SetSessionValue('tmp:FaultCode13',tmp:FaultCode13)
  Else
    tmp:FaultCode13 = p_web.GetSessionValue('tmp:FaultCode13')
  End
  if p_web.IfExistsValue('tmp:FaultCode14') = 0
    p_web.SetSessionValue('tmp:FaultCode14',tmp:FaultCode14)
  Else
    tmp:FaultCode14 = p_web.GetSessionValue('tmp:FaultCode14')
  End
  if p_web.IfExistsValue('tmp:FaultCode15') = 0
    p_web.SetSessionValue('tmp:FaultCode15',tmp:FaultCode15)
  Else
    tmp:FaultCode15 = p_web.GetSessionValue('tmp:FaultCode15')
  End
  if p_web.IfExistsValue('tmp:FaultCode16') = 0
    p_web.SetSessionValue('tmp:FaultCode16',tmp:FaultCode16)
  Else
    tmp:FaultCode16 = p_web.GetSessionValue('tmp:FaultCode16')
  End
  if p_web.IfExistsValue('tmp:FaultCode17') = 0
    p_web.SetSessionValue('tmp:FaultCode17',tmp:FaultCode17)
  Else
    tmp:FaultCode17 = p_web.GetSessionValue('tmp:FaultCode17')
  End
  if p_web.IfExistsValue('tmp:FaultCode18') = 0
    p_web.SetSessionValue('tmp:FaultCode18',tmp:FaultCode18)
  Else
    tmp:FaultCode18 = p_web.GetSessionValue('tmp:FaultCode18')
  End
  if p_web.IfExistsValue('tmp:FaultCode19') = 0
    p_web.SetSessionValue('tmp:FaultCode19',tmp:FaultCode19)
  Else
    tmp:FaultCode19 = p_web.GetSessionValue('tmp:FaultCode19')
  End
  if p_web.IfExistsValue('tmp:FaultCode20') = 0
    p_web.SetSessionValue('tmp:FaultCode20',tmp:FaultCode20)
  Else
    tmp:FaultCode20 = p_web.GetSessionValue('tmp:FaultCode20')
  End
  if p_web.IfExistsValue('tmp:processExchange') = 0
    p_web.SetSessionValue('tmp:processExchange',tmp:processExchange)
  Else
    tmp:processExchange = p_web.GetSessionValue('tmp:processExchange')
  End

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('tmp:MSN')
    tmp:MSN = p_web.GetValue('tmp:MSN')
    p_web.SetSessionValue('tmp:MSN',tmp:MSN)
  Else
    tmp:MSN = p_web.GetSessionValue('tmp:MSN')
  End
  if p_web.IfExistsValue('tmp:VerifyMSN')
    tmp:VerifyMSN = p_web.GetValue('tmp:VerifyMSN')
    p_web.SetSessionValue('tmp:VerifyMSN',tmp:VerifyMSN)
  Else
    tmp:VerifyMSN = p_web.GetSessionValue('tmp:VerifyMSN')
  End
  if p_web.IfExistsValue('tmp:ConfirmMSNChange')
    tmp:ConfirmMSNChange = p_web.GetValue('tmp:ConfirmMSNChange')
    p_web.SetSessionValue('tmp:ConfirmMSNChange',tmp:ConfirmMSNChange)
  Else
    tmp:ConfirmMSNChange = p_web.GetSessionValue('tmp:ConfirmMSNChange')
  End
  if p_web.IfExistsValue('job:ProductCode')
    job:ProductCode = p_web.GetValue('job:ProductCode')
    p_web.SetSessionValue('job:ProductCode',job:ProductCode)
  Else
    job:ProductCode = p_web.GetSessionValue('job:ProductCode')
  End
  if p_web.IfExistsValue('tmp:FaultCode1')
    tmp:FaultCode1 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode1')),p_web.GSV('Picture:JobFaultCode1'))
    p_web.SetSessionValue('tmp:FaultCode1',tmp:FaultCode1)
  Else
    tmp:FaultCode1 = p_web.GetSessionValue('tmp:FaultCode1')
  End
  if p_web.IfExistsValue('tmp:FaultCode2')
    tmp:FaultCode2 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode2')),p_web.GSV('Picture:JobFaultCode2'))
    p_web.SetSessionValue('tmp:FaultCode2',tmp:FaultCode2)
  Else
    tmp:FaultCode2 = p_web.GetSessionValue('tmp:FaultCode2')
  End
  if p_web.IfExistsValue('tmp:FaultCode3')
    tmp:FaultCode3 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode3')),p_web.GSV('Picture:JobFaultCode3'))
    p_web.SetSessionValue('tmp:FaultCode3',tmp:FaultCode3)
  Else
    tmp:FaultCode3 = p_web.GetSessionValue('tmp:FaultCode3')
  End
  if p_web.IfExistsValue('tmp:FaultCode4')
    tmp:FaultCode4 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode4')),p_web.GSV('Picture:JobFaultCode4'))
    p_web.SetSessionValue('tmp:FaultCode4',tmp:FaultCode4)
  Else
    tmp:FaultCode4 = p_web.GetSessionValue('tmp:FaultCode4')
  End
  if p_web.IfExistsValue('tmp:FaultCode5')
    tmp:FaultCode5 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode5')),p_web.GSV('Picture:JobFaultCode5'))
    p_web.SetSessionValue('tmp:FaultCode5',tmp:FaultCode5)
  Else
    tmp:FaultCode5 = p_web.GetSessionValue('tmp:FaultCode5')
  End
  if p_web.IfExistsValue('tmp:FaultCode6')
    tmp:FaultCode6 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode6')),p_web.GSV('Picture:JobFaultCode6'))
    p_web.SetSessionValue('tmp:FaultCode6',tmp:FaultCode6)
  Else
    tmp:FaultCode6 = p_web.GetSessionValue('tmp:FaultCode6')
  End
  if p_web.IfExistsValue('tmp:FaultCode7')
    tmp:FaultCode7 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode7')),p_web.GSV('Picture:JobFaultCode7'))
    p_web.SetSessionValue('tmp:FaultCode7',tmp:FaultCode7)
  Else
    tmp:FaultCode7 = p_web.GetSessionValue('tmp:FaultCode7')
  End
  if p_web.IfExistsValue('tmp:FaultCode8')
    tmp:FaultCode8 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode8')),p_web.GSV('Picture:JobFaultCode8'))
    p_web.SetSessionValue('tmp:FaultCode8',tmp:FaultCode8)
  Else
    tmp:FaultCode8 = p_web.GetSessionValue('tmp:FaultCode8')
  End
  if p_web.IfExistsValue('tmp:FaultCode9')
    tmp:FaultCode9 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode9')),p_web.GSV('Picture:JobFaultCode9'))
    p_web.SetSessionValue('tmp:FaultCode9',tmp:FaultCode9)
  Else
    tmp:FaultCode9 = p_web.GetSessionValue('tmp:FaultCode9')
  End
  if p_web.IfExistsValue('tmp:FaultCode10')
    tmp:FaultCode10 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode10')),p_web.GSV('Picture:JobFaultCode10'))
    p_web.SetSessionValue('tmp:FaultCode10',tmp:FaultCode10)
  Else
    tmp:FaultCode10 = p_web.GetSessionValue('tmp:FaultCode10')
  End
  if p_web.IfExistsValue('tmp:FaultCode11')
    tmp:FaultCode11 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode11')),p_web.GSV('Picture:JobFaultCode11'))
    p_web.SetSessionValue('tmp:FaultCode11',tmp:FaultCode11)
  Else
    tmp:FaultCode11 = p_web.GetSessionValue('tmp:FaultCode11')
  End
  if p_web.IfExistsValue('tmp:FaultCode12')
    tmp:FaultCode12 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode12')),p_web.GSV('Picture:JobFaultCode12'))
    p_web.SetSessionValue('tmp:FaultCode12',tmp:FaultCode12)
  Else
    tmp:FaultCode12 = p_web.GetSessionValue('tmp:FaultCode12')
  End
  if p_web.IfExistsValue('tmp:FaultCode13')
    tmp:FaultCode13 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode13')),p_web.GSV('Picture:JobFaultCode13'))
    p_web.SetSessionValue('tmp:FaultCode13',tmp:FaultCode13)
  Else
    tmp:FaultCode13 = p_web.GetSessionValue('tmp:FaultCode13')
  End
  if p_web.IfExistsValue('tmp:FaultCode14')
    tmp:FaultCode14 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode14')),p_web.GSV('Picture:JobFaultCode14'))
    p_web.SetSessionValue('tmp:FaultCode14',tmp:FaultCode14)
  Else
    tmp:FaultCode14 = p_web.GetSessionValue('tmp:FaultCode14')
  End
  if p_web.IfExistsValue('tmp:FaultCode15')
    tmp:FaultCode15 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode15')),p_web.GSV('Picture:JobFaultCode15'))
    p_web.SetSessionValue('tmp:FaultCode15',tmp:FaultCode15)
  Else
    tmp:FaultCode15 = p_web.GetSessionValue('tmp:FaultCode15')
  End
  if p_web.IfExistsValue('tmp:FaultCode16')
    tmp:FaultCode16 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode16')),p_web.GSV('Picture:JobFaultCode16'))
    p_web.SetSessionValue('tmp:FaultCode16',tmp:FaultCode16)
  Else
    tmp:FaultCode16 = p_web.GetSessionValue('tmp:FaultCode16')
  End
  if p_web.IfExistsValue('tmp:FaultCode17')
    tmp:FaultCode17 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode17')),p_web.GSV('Picture:JobFaultCode17'))
    p_web.SetSessionValue('tmp:FaultCode17',tmp:FaultCode17)
  Else
    tmp:FaultCode17 = p_web.GetSessionValue('tmp:FaultCode17')
  End
  if p_web.IfExistsValue('tmp:FaultCode18')
    tmp:FaultCode18 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode18')),p_web.GSV('Picture:JobFaultCode18'))
    p_web.SetSessionValue('tmp:FaultCode18',tmp:FaultCode18)
  Else
    tmp:FaultCode18 = p_web.GetSessionValue('tmp:FaultCode18')
  End
  if p_web.IfExistsValue('tmp:FaultCode19')
    tmp:FaultCode19 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode19')),p_web.GSV('Picture:JobFaultCode19'))
    p_web.SetSessionValue('tmp:FaultCode19',tmp:FaultCode19)
  Else
    tmp:FaultCode19 = p_web.GetSessionValue('tmp:FaultCode19')
  End
  if p_web.IfExistsValue('tmp:FaultCode20')
    tmp:FaultCode20 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode20')),p_web.GSV('Picture:JobFaultCode20'))
    p_web.SetSessionValue('tmp:FaultCode20',tmp:FaultCode20)
  Else
    tmp:FaultCode20 = p_web.GetSessionValue('tmp:FaultCode20')
  End
  if p_web.IfExistsValue('tmp:processExchange')
    tmp:processExchange = p_web.GetValue('tmp:processExchange')
    p_web.SetSessionValue('tmp:processExchange',tmp:processExchange)
  Else
    tmp:processExchange = p_web.GetSessionValue('tmp:processExchange')
  End

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('JobFaultCodes_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.GSV('locNextURL')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('JobFaultCodes_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('JobFaultCodes_ChainTo')
    loc:formaction = p_web.GetSessionValue('JobFaultCodes_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'ViewJob'

GenerateForm   Routine
  do LoadRelatedRecords
      p_web.SSV('locNextURL','BillingConfirmation')
      if (p_web.GSV('JobFaultCodes:FirstTime') = 0)
          CalculateBilling(p_web)
          Access:MANFAULT.Clearkey(maf:ScreenOrderKey)
          maf:Manufacturer    = p_web.GSV('job:Manufacturer')
          maf:ScreenOrder    = 0
          set(maf:ScreenOrderKey,maf:ScreenOrderKey)
          loop
              if (Access:MANFAULT.Next())
                  Break
              end ! if (Access:MANFAULT.Next())
              if (maf:Manufacturer    <> p_web.GSV('job:Manufacturer'))
                  Break
              end ! if (maf:Manufacturer    <> p_web.GSV('job:Manufacturer'))
              if (maf:Field_Number < 13)
                 p_web.SSV('tmp:FaultCode' & maf:ScreenOrder,p_web.GSV('job:Fault_Code' & maf:Field_Number))
              else ! if (maf:ScreenOrder < 13)
                 p_web.SSV('tmp:FaultCode' & maf:ScreenOrder,p_web.GSV('wob:FaultCode' & maf:Field_Number))
              end ! if (maf:ScreenOrder < 13)
          end ! loop
          p_web.SSV('Hide:ExchangePrompt',1)
          p_web.SSV('tmp:processExchange',0)
          p_web.SSV('tmp:MSN','')
          p_web.SSV('tmp:VerifyMSN','')
  
          ! Verify MSN
          p_web.SSV('MSNValidation',0)
          p_web.SSV('MSNValidated',0)
          p_web.SSV('Hide:ButtonVerifyMSN',1)
          p_web.SSV('Hide:VerifyMSN',1)
          p_web.SSV('Hide:ButtonVerifyMSN2',1)
          p_web.SSV('Hide:ConfirmMSNChange',1)
  
          if (MSNRequired(p_web.GSV('job:Manufacturer')))
              found# = 0
  
              Access:AUDIT.Clearkey(aud:typeRefKey)
              aud:ref_Number    = p_web.GSV('job:Ref_Number')
              aud:type    = 'JOB'
              set(aud:typeRefKey,aud:typeRefKey)
              loop
                  if (Access:AUDIT.Next())
                      Break
                  end ! if (Access:AUDIT.Next())
                  if (aud:ref_Number    <> p_web.GSV('job:Ref_Number'))
                      Break
                  end ! if (aud:ref_Number    <> p_web.GSV('job:Ref_Number'))
                  if (aud:type    <> 'JOB')
                      Break
                  end ! if (aud:type    <> 'JOB')
                  if (instring('MSN VERIFICATION',upper(aud:Action),1,1))
                      found# = 1
                      break
                  end ! if (instring('MSN VERIFICATION',upper(aud:Action),1,1)
              end ! loop
  
              if (found# = 0)
                  p_web.SSV('MSNValidation',1)
                  p_web.SSV('Comment:MSN','Verify MSN')
                  p_web.SSV('Hide:ButtonVerifyMSN',0)
              end ! if (found# = 0)
          end ! if (MSNRequired(p_web.GSV('job:Manufacturer'))
  
      end ! if (p_web.GSV('JobFaultCodes:FirstTime') = 0)
  
      do buildfaultcodes
  
      p_web.SSV('JobFaultCodes:FirstTime',1)
  
  
      if (productCodeRequired(p_web.GSV('job:Manufacturer') )= 0)
          p_web.SSV('Hide:ProductCode',0)
          if (securityCheckFailed(p_web.GSV('BookingUserPassword'),'PRODUCT CODE - AMEND'))
              ! No Lookup
              p_web.SSV('ReadOnly:ProdutCode',1)
          else
              p_web.SSV('ReadOnly:ProdutCode',0)
          end ! if (securityCheckFailed(p_web.GSV('BookingUserPassword'),'PRODUCT CODE - AMEND'))
      else ! if (productCodeRequired(p_web.GSV('job:Manufacturer')))
          p_web.SSV('Hide:ProductCode',1)
      end ! if (productCodeRequired(p_web.GSV('job:Manufacturer')))
  
  
      !Clear Part Out Fault Table
  
      Access:SBO_OUTFAULTPARTS.Clearkey(sofp:faultKey)
      sofp:sessionID    = p_web.sessionID
      set(sofp:faultKey,sofp:faultKey)
      loop
          if (Access:SBO_OUTFAULTPARTS.Next())
              Break
          end ! if (Access:SBO_OUTFAULTPARTS.Next())
          if (sofp:sessionID    <> p_web.sessionID)
              Break
          end ! if (sofp:sessionID    <> p_web.sessionID)
          access:SBO_OUTFAULTPARTS.deleterecord(0)
      end ! loop
  
  
 tmp:MSN = p_web.RestoreValue('tmp:MSN')
 tmp:VerifyMSN = p_web.RestoreValue('tmp:VerifyMSN')
 tmp:ConfirmMSNChange = p_web.RestoreValue('tmp:ConfirmMSNChange')
 tmp:FaultCode1 = p_web.RestoreValue('tmp:FaultCode1')
 tmp:FaultCode2 = p_web.RestoreValue('tmp:FaultCode2')
 tmp:FaultCode3 = p_web.RestoreValue('tmp:FaultCode3')
 tmp:FaultCode4 = p_web.RestoreValue('tmp:FaultCode4')
 tmp:FaultCode5 = p_web.RestoreValue('tmp:FaultCode5')
 tmp:FaultCode6 = p_web.RestoreValue('tmp:FaultCode6')
 tmp:FaultCode7 = p_web.RestoreValue('tmp:FaultCode7')
 tmp:FaultCode8 = p_web.RestoreValue('tmp:FaultCode8')
 tmp:FaultCode9 = p_web.RestoreValue('tmp:FaultCode9')
 tmp:FaultCode10 = p_web.RestoreValue('tmp:FaultCode10')
 tmp:FaultCode11 = p_web.RestoreValue('tmp:FaultCode11')
 tmp:FaultCode12 = p_web.RestoreValue('tmp:FaultCode12')
 tmp:FaultCode13 = p_web.RestoreValue('tmp:FaultCode13')
 tmp:FaultCode14 = p_web.RestoreValue('tmp:FaultCode14')
 tmp:FaultCode15 = p_web.RestoreValue('tmp:FaultCode15')
 tmp:FaultCode16 = p_web.RestoreValue('tmp:FaultCode16')
 tmp:FaultCode17 = p_web.RestoreValue('tmp:FaultCode17')
 tmp:FaultCode18 = p_web.RestoreValue('tmp:FaultCode18')
 tmp:FaultCode19 = p_web.RestoreValue('tmp:FaultCode19')
 tmp:FaultCode20 = p_web.RestoreValue('tmp:FaultCode20')
 tmp:processExchange = p_web.RestoreValue('tmp:processExchange')
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
    loc:viewonly = Choose(p_web.GSV('Job:ViewOnly') = 1,1,0)
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Job Fault Codes') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Job Fault Codes',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_JobFaultCodes',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      If p_web.GSV('MSNValidation') = 1
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_JobFaultCodes0_div')&'">'&p_web.Translate('Verify MSN')&'</a></li>'& CRLF
      End
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_JobFaultCodes1_div')&'">'&p_web.Translate('Job Fault Codes')&'</a></li>'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_JobFaultCodes2_div')&'">'&p_web.Translate('Tools')&'</a></li>'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_JobFaultCodes3_div')&'">'&p_web.Translate()&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  do GenerateTab3
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="JobFaultCodes_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.cancel();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CancelButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="JobFaultCodes_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'JobFaultCodes_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="JobFaultCodes_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'JobFaultCodes_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
    If upper(p_web.getvalue('LookupFile'))='MODPROD'
          If Not (p_web.GSV('Hide:JobFaultCode1') = 1)
            p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:FaultCode1')
          End
    End
  Else
    If False
    ElsIf p_web.GSV('MSNValidation') = 1
        p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:MSN')
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.job:ProductCode')
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_JobFaultCodes')>0,p_web.GSV('showtab_JobFaultCodes'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_JobFaultCodes'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_JobFaultCodes') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_JobFaultCodes'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_JobFaultCodes')>0,p_web.GSV('showtab_JobFaultCodes'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_JobFaultCodes') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
      If p_web.GSV('MSNValidation') = 1
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Verify MSN') & ''''
      End
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Job Fault Codes') & ''''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Tools') & ''''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_JobFaultCodes_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_JobFaultCodes')>0,p_web.GSV('showtab_JobFaultCodes'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"JobFaultCodes",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_JobFaultCodes')>0,p_web.GSV('showtab_JobFaultCodes'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_JobFaultCodes_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('JobFaultCodes') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    p_web.SetValue('_CallPopups',1)
      If p_web.RequestAjax = 0 and p_web.GetPreCall('LookupProductCodes') = 0 then LookupProductCodes(p_web).
    p_web.SetValue('_CallPopups',1)
      If p_web.RequestAjax = 0 and p_web.GetPreCall('BrowseRepairNotes') = 0 then BrowseRepairNotes(p_web).
    p_web.SetValue('_CallPopups',1)
      If p_web.RequestAjax = 0 and p_web.GetPreCall('DisplayOutFaults') = 0 then DisplayOutFaults(p_web,Net:Web:Popup).
    p_web.SetValue('_CallPopups',0)
    do AutoLookups
    p_web.AddPreCall('JobFaultCodes')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
  If p_web.GSV('MSNValidation') = 1
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Verify MSN')&'</a></h3>' & CRLF & p_web.DivHeader('tab_JobFaultCodes0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_JobFaultCodes0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_JobFaultCodes0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_JobFaultCodes0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Verify MSN')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_JobFaultCodes0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Verify MSN')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_JobFaultCodes0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Verify MSN')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_JobFaultCodes0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'25%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:MSN
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:MSN
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'40%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:MSN
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'25%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::buttonVerifyMSN
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'40%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::buttonVerifyMSN
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'25%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:VerifyMSN
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:VerifyMSN
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'40%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:VerifyMSN
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'25%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::buttonVerifyMSN2
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'40%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::buttonVerifyMSN2
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'25%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:ConfirmMSNChange
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:ConfirmMSNChange
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'40%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:ConfirmMSNChange
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
  end
GenerateTab1  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Job Fault Codes')&'</a></h3>' & CRLF & p_web.DivHeader('tab_JobFaultCodes1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_JobFaultCodes1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_JobFaultCodes1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_JobFaultCodes1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Job Fault Codes')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_JobFaultCodes1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Job Fault Codes')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_JobFaultCodes1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Job Fault Codes')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_JobFaultCodes1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'25%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:ProductCode
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:ProductCode
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'40%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::job:ProductCode
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'25%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:FaultCode1
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:FaultCode1
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'40%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:FaultCode1
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'25%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:FaultCode2
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:FaultCode2
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'40%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:FaultCode2
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'25%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:FaultCode3
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:FaultCode3
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'40%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:FaultCode3
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'25%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:FaultCode4
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:FaultCode4
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'40%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:FaultCode4
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'25%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:FaultCode5
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:FaultCode5
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'40%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:FaultCode5
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'25%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:FaultCode6
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:FaultCode6
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'40%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:FaultCode6
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'25%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:FaultCode7
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:FaultCode7
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'40%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:FaultCode7
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'25%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:FaultCode8
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:FaultCode8
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'40%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:FaultCode8
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'25%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:FaultCode9
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:FaultCode9
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'40%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:FaultCode9
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'25%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:FaultCode10
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:FaultCode10
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'40%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:FaultCode10
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'25%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:FaultCode11
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:FaultCode11
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'40%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:FaultCode11
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'25%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:FaultCode12
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:FaultCode12
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'40%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:FaultCode12
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'25%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:FaultCode13
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:FaultCode13
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'40%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:FaultCode13
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'25%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:FaultCode14
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:FaultCode14
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'40%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:FaultCode14
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'25%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:FaultCode15
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:FaultCode15
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'40%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:FaultCode15
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'25%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:FaultCode16
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:FaultCode16
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'40%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:FaultCode16
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'25%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:FaultCode17
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:FaultCode17
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'40%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:FaultCode17
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'25%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:FaultCode18
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:FaultCode18
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'40%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:FaultCode18
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'25%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:FaultCode19
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:FaultCode19
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'40%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:FaultCode19
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'25%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:FaultCode20
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:FaultCode20
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'40%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:FaultCode20
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab2  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Tools')&'</a></h3>' & CRLF & p_web.DivHeader('tab_JobFaultCodes2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_JobFaultCodes2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_JobFaultCodes2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_JobFaultCodes2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Tools')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_JobFaultCodes2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Tools')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_JobFaultCodes2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Tools')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_JobFaultCodes2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::buttonRepairNotes
        do Comment::buttonRepairNotes
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::buttonOutFaults
        do Comment::buttonOutFaults
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab3  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate()&'</a></h3>' & CRLF & p_web.DivHeader('tab_JobFaultCodes3',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_JobFaultCodes3',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_JobFaultCodes3',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_JobFaultCodes3',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,)
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_JobFaultCodes3',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend)
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_JobFaultCodes3',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain">' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_JobFaultCodes3',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::promptExchange
        do Comment::promptExchange
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'25%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:processExchange
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:processExchange
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'40%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:processExchange
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Prompt::tmp:MSN  Routine
  packet = clip(packet) & p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:MSN') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('M.S.N.'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:MSN  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:MSN = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:MSN = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:MSN  ! copies value to session value if valid.
  do Value::tmp:MSN
  do SendAlert
  do Comment::tmp:MSN ! allows comment style to be updated.
  do Prompt::tmp:VerifyMSN
  do Value::tmp:VerifyMSN  !1
  do Comment::tmp:VerifyMSN

ValidateValue::tmp:MSN  Routine
    If not (1=0)
  If tmp:MSN = ''
    loc:Invalid = 'tmp:MSN'
    tmp:MSN:IsInvalid = true
    loc:alert = p_web.translate('M.S.N.') & ' ' & p_web.site.RequiredText
  End
    tmp:MSN = Upper(tmp:MSN)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:MSN',tmp:MSN).
    End

Value::tmp:MSN  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:MSN') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.GSV('MSNValidated') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  If loc:retrying
    tmp:MSN = p_web.RestoreValue('tmp:MSN')
    do ValidateValue::tmp:MSN
    If tmp:MSN:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- tmp:MSN
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('MSNValidated') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:MSN'',''jobfaultcodes_tmp:msn_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:MSN')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:MSN',p_web.GetSessionValueFormat('tmp:MSN'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:MSN  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:MSN:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:MSN'))
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:MSN') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonVerifyMSN  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonVerifyMSN  ! copies value to session value if valid.
  if (p_web.GSV('tmp:MSN') <> '')
      error# = 0
      if (len(clip(p_web.GSV('tmp:MSN'))) = 11 and p_web.GSV('job:Manufacturer') = 'ERICSSON')
          p_web.SSV('tmp:MSN',sub(p_web.GSV('tmp:MSN'),2,10))
      end ! if (len(clip(p_web.GSV('tmp:MSN'))) and p_web.GSV('job:Manufacturer') = 'ERICSSON')
  
      if (checkLength('MSN',p_web.GSV('job:Model_Number'),p_web.GSV('tmp:MSN')))
          error# = 1
      else !if (checkLength('MSN',p_web.GSV('job:Model_Number'),p_web.GSV('tmp:MSN'))
          Access:MANUFACT.Clearkey(man:Manufacturer_Key)
          man:Manufacturer    = p_web.GSV('job:Manufacturer')
          if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
              ! Found
              if (man:ApplyMSNFormat)
                  if (checkFaultFormat(p_web.GSV('tmp:MSN'),man:MSNFormat))
                      p_web.SSV('Comment:MSN','M.S.N. Failed Format Validation')
                      error# = 1
                  end ! if (checkFaultFormat(p_web.GSV('tmp:MSN'),man:MSNFormat))
              end ! if (man:ApplyMSNFormat)
          else ! if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
              ! Error
          end ! if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
      end ! if (checkLength('MSN   ',p_web.GSV('job:Model_Number'),p_web.GSV('tmp:MSN'))
  
      if (error# = 0)
          if (p_web.GSV('tmp:MSN') = p_web.GSV('job:MSN'))
              p_web.SSV('MSNValidated',1)
              p_web.SSV('Comment:MSN','M.S.N. Verification Passed')
              p_web.SSV('AddToAudit:Type','JOB')
              p_web.SSV('AddToAudit:Action','MSN VERIFICATION: PASSED')
              p_web.SSV('AddToAudit:Notes','MSN: ' & p_web.GSV('job:MSN'))
              AddToAudit(p_web)
          else ! if (p_web.GSV('tmp:MSN') = p_web.GSV('job:MSN'))
              p_web.SSV('Hide:VerifyMSN',0)
              p_web.SSV('Hide:ButtonVerifyMSN',1)
              p_web.SSV('Hide:ButtonVerifyMSN2',0)
              p_web.SSV('Comment:MSN','MSN does not match job. Re-Verify entered MSN Below')
              p_web.SSV('Comment:MSN2','Verify MSN')
          end ! if (p_web.GSV('tmp:MSN') = p_web.GSV('job:MSN'))
      end ! if (error# = 0)
  end ! if (p_web.GSV('tmp:MSN') <> '')
  do Value::buttonVerifyMSN
  do Comment::buttonVerifyMSN ! allows comment style to be updated.
  do Prompt::tmp:VerifyMSN
  do Value::tmp:VerifyMSN  !1
  do Comment::tmp:VerifyMSN
  do Prompt::tmp:MSN
  do Value::tmp:MSN  !1
  do Comment::tmp:MSN
  do Value::buttonVerifyMSN2  !1

ValidateValue::buttonVerifyMSN  Routine
    If not (p_web.GSV('Hide:ButtonVerifyMSN') = 1 Or p_web.GSV('MSNValidated') = 1)
    End

Value::buttonVerifyMSN  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:ButtonVerifyMSN') = 1 Or p_web.GSV('MSNValidated') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('buttonVerifyMSN') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('Hide:ButtonVerifyMSN') = 1 Or p_web.GSV('MSNValidated') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonVerifyMSN'',''jobfaultcodes_buttonverifymsn_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','VerifyMSN','Verify M.S.N.',p_web.combine(Choose('Verify M.S.N.' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'button-entryfield'),loc:formname,,,,loc:javascript,loc:disabled,,,,,,,,,,'nt-left')!h
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonVerifyMSN  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonVerifyMSN:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:ButtonVerifyMSN') = 1 Or p_web.GSV('MSNValidated') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('buttonVerifyMSN') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:ButtonVerifyMSN') = 1 Or p_web.GSV('MSNValidated') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:VerifyMSN  Routine
  packet = clip(packet) & p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:VerifyMSN') & '_prompt',Choose(p_web.GSV('Hide:VerifyMSN') = 1 or p_web.GSV('MSNValidated') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:VerifyMSN') = 1 or p_web.GSV('MSNValidated') = 1,'',p_web.Translate('Verify M.S.N.'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:VerifyMSN  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:VerifyMSN = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:VerifyMSN = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:VerifyMSN  ! copies value to session value if valid.
  do Value::tmp:VerifyMSN
  do SendAlert
  do Comment::tmp:VerifyMSN ! allows comment style to be updated.

ValidateValue::tmp:VerifyMSN  Routine
    If not (p_web.GSV('Hide:VerifyMSN') = 1 or p_web.GSV('MSNValidated') = 1)
  If tmp:VerifyMSN = ''
    loc:Invalid = 'tmp:VerifyMSN'
    tmp:VerifyMSN:IsInvalid = true
    loc:alert = p_web.translate('Verify M.S.N.') & ' ' & p_web.site.RequiredText
  End
    tmp:VerifyMSN = Upper(tmp:VerifyMSN)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:VerifyMSN',tmp:VerifyMSN).
    End

Value::tmp:VerifyMSN  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:VerifyMSN') = 1 or p_web.GSV('MSNValidated') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:VerifyMSN') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  If loc:retrying
    tmp:VerifyMSN = p_web.RestoreValue('tmp:VerifyMSN')
    do ValidateValue::tmp:VerifyMSN
    If tmp:VerifyMSN:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:VerifyMSN') = 1 or p_web.GSV('MSNValidated') = 1)
  ! --- STRING --- tmp:VerifyMSN
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:VerifyMSN'',''jobfaultcodes_tmp:verifymsn_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:VerifyMSN',p_web.GetSessionValueFormat('tmp:VerifyMSN'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:VerifyMSN  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:VerifyMSN:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:VerifyMSN'))
  loc:class = Choose(p_web.GSV('Hide:VerifyMSN') = 1 or p_web.GSV('MSNValidated') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:VerifyMSN') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:VerifyMSN') = 1 or p_web.GSV('MSNValidated') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonVerifyMSN2  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonVerifyMSN2  ! copies value to session value if valid.
  ! Second MSN Validation
  if (p_web.GSV('tmp:VerifyMSN') <> '')
      error# = 0
      if (len(clip(p_web.GSV('tmp:VerifyMSN'))) = 11 and p_web.GSV('job:Manufacturer') = 'ERICSSON')
          p_web.SSV('tmp:VerifyMSN',sub(p_web.GSV('tmp:VerifyMSN'),2,10))
      end ! if (len(clip(p_web.GSV('tmp:VerifyMSN'))) and p_web.GSV('job:Manufacturer') = 'ERICSSON')
  
      if (checkLength('MSN',p_web.GSV('job:Model_Number'),p_web.GSV('tmp:VerifyMSN')))
          error# = 1
      else !if (checkLength('MSN',p_web.GSV('job:Model_Number'),p_web.GSV('tmp:VerifyMSN'))
          Access:MANUFACT.Clearkey(man:Manufacturer_Key)
          man:Manufacturer    = p_web.GSV('job:Manufacturer')
          if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
              ! Found
              if (man:ApplyMSNFormat)
                  if (checkFaultFormat(p_web.GSV('tmp:VerifyMSN'),man:MSNFormat))
                      p_web.SSV('Comment:VerifyMSN','M.S.N. Failed Format Validation')
                      error# = 1
                  end ! if (checkFaultFormat(p_web.GSV('tmp:VerifyMSN'),man:MSNFormat))
              end ! if (man:ApplyMSNFormat)
          else ! if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
              ! Error
          end ! if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
      end ! if (checkLength('MSN   ',p_web.GSV('job:Model_Number'),p_web.GSV('tmp:VerifyMSN'))
  
      if (error# = 0)
          if (p_web.GSV('tmp:VerifyMSN') = p_web.GSV('tmp:MSN'))
              p_web.SSV('Hide:ConfirmMSNChange',0)
              p_web.SSV('Hide:ButtonVerifyMSN2',1)
              p_web.SSV('Comment:VerifyMSN','MSNs Match. Confirm Change To Job')
          else ! if (p_web.GSV('tmp:VerifyMSN') = p_web.GSV('job:MSN'))
              p_web.SSV('Comment:VerifyMSN','MSN does not match')
          end ! if (p_web.GSV('tmp:VerifyMSN') = p_web.GSV('job:MSN'))
      end ! if (error# = 0)
  end ! if (p_web.GSV('tmp:VerifyMSN') <> '')
  do Value::buttonVerifyMSN2
  do Comment::buttonVerifyMSN2 ! allows comment style to be updated.
  do Prompt::tmp:VerifyMSN
  do Value::tmp:VerifyMSN  !1
  do Comment::tmp:VerifyMSN
  do Value::buttonVerifyMSN  !1
  do Prompt::tmp:MSN
  do Value::tmp:MSN  !1
  do Comment::tmp:MSN
  do Prompt::tmp:ConfirmMSNChange
  do Value::tmp:ConfirmMSNChange  !1
  do Comment::tmp:ConfirmMSNChange

ValidateValue::buttonVerifyMSN2  Routine
    If not (p_web.GSV('Hide:ButtonVerifyMSN2') = 1 Or p_web.GSV('MSNValidated') = 1)
    End

Value::buttonVerifyMSN2  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:ButtonVerifyMSN2') = 1 Or p_web.GSV('MSNValidated') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('buttonVerifyMSN2') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('Hide:ButtonVerifyMSN2') = 1 Or p_web.GSV('MSNValidated') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonVerifyMSN2'',''jobfaultcodes_buttonverifymsn2_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','VerifyMSN2','Verify M.S.N.',p_web.combine(Choose('Verify M.S.N.' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'button-entryfield'),loc:formname,,,,loc:javascript,loc:disabled,,,,,,,,,,'nt-left')!h
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonVerifyMSN2  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonVerifyMSN2:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:ButtonVerifyMSN2') = 1 Or p_web.GSV('MSNValidated') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('buttonVerifyMSN2') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:ButtonVerifyMSN2') = 1 Or p_web.GSV('MSNValidated') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:ConfirmMSNChange  Routine
  packet = clip(packet) & p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:ConfirmMSNChange') & '_prompt',Choose(p_web.GSV('Hide:ConfirmMSNChange') =1 OR p_web.GSV('MSNValidated') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:ConfirmMSNChange') =1 OR p_web.GSV('MSNValidated') = 1,'',p_web.Translate('Change Job MSN?'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:ConfirmMSNChange  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:ConfirmMSNChange = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:ConfirmMSNChange = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:ConfirmMSNChange  ! copies value to session value if valid.
  ! Confirm MSN Change
  if (p_web.GSV('tmp:ConfirmMSNChange') = 'Y')
      p_web.SSV('MSNValidated',1)
  
      p_web.SSV('tmp:ConfirmMSNChange','')
  
      p_web.SSV('AddToAudit:Type','JOB')
      p_web.SSV('AddToAudit:Action','MSN VERIFICATION: AMENDMENT')
      p_web.SSV('AddToAudit:Notes','ORIGINAL MSN: ' & p_web.GSV('job:MSN') & '<13,10>NEW MSN: ' & p_web.GSV('tmp:MSN'))
      addToAudit(p_web)
      p_web.SSV('job:MSN',p_web.GSV('tmp:MSN'))
      p_web.SSV('Comment:MSN','Verified')
  else ! if (p_web.GSV('tmp:ConfirmMSNChange') = 'Y')
      p_web.SSV('Hide:ButtonVerifyMSN2',1)
      p_web.SSV('Hide:ConfirmMSNChange',1)
      p_web.SSV('Hide:ButtonVerifyMSN',0)
      p_web.SSV('Hide:VerifyMSN',1)
      p_web.SSV('tmp:ConfirmMSNChange','')
      p_web.SSV('Comment:MSN','Verify MSN')
      p_web.SSV('Comment:VerifyMSN','')
  end  !if (p_web.GSV('tmp:ConfirmMSNChange') = 'Y')
  do Value::tmp:ConfirmMSNChange
  do SendAlert
  do Comment::tmp:ConfirmMSNChange ! allows comment style to be updated.
  do Prompt::tmp:ConfirmMSNChange
  do Comment::tmp:ConfirmMSNChange
  do Value::buttonVerifyMSN2  !1
  do Value::buttonVerifyMSN  !1
  do Prompt::tmp:MSN
  do Value::tmp:MSN  !1
  do Comment::tmp:MSN
  do Prompt::tmp:VerifyMSN
  do Value::tmp:VerifyMSN  !1
  do Comment::tmp:VerifyMSN

ValidateValue::tmp:ConfirmMSNChange  Routine
    If not (p_web.GSV('Hide:ConfirmMSNChange') =1 OR p_web.GSV('MSNValidated') = 1)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:ConfirmMSNChange',tmp:ConfirmMSNChange).
    End

Value::tmp:ConfirmMSNChange  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:ConfirmMSNChange') =1 OR p_web.GSV('MSNValidated') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:ConfirmMSNChange') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If loc:retrying
    tmp:ConfirmMSNChange = p_web.RestoreValue('tmp:ConfirmMSNChange')
    do ValidateValue::tmp:ConfirmMSNChange
    If tmp:ConfirmMSNChange:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:ConfirmMSNChange') =1 OR p_web.GSV('MSNValidated') = 1)
  ! --- RADIO --- tmp:ConfirmMSNChange
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:ConfirmMSNChange') = 'Y'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:ConfirmMSNChange'',''jobfaultcodes_tmp:confirmmsnchange_value'',1,'''&p_web._jsok('Y')&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:ConfirmMSNChange')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:ConfirmMSNChange',clip('Y'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:ConfirmMSNChange_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Yes') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:ConfirmMSNChange') = 'N'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:ConfirmMSNChange'',''jobfaultcodes_tmp:confirmmsnchange_value'',1,'''&p_web._jsok('N')&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:ConfirmMSNChange')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:ConfirmMSNChange',clip('N'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:ConfirmMSNChange_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('No') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:ConfirmMSNChange  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:ConfirmMSNChange:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:ConfirmMSNChange') =1 OR p_web.GSV('MSNValidated') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:ConfirmMSNChange') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:ConfirmMSNChange') =1 OR p_web.GSV('MSNValidated') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::job:ProductCode  Routine
  packet = clip(packet) & p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('job:ProductCode') & '_prompt',Choose(p_web.GSV('Hide:ProductCode') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:ProductCode') = 1,'',p_web.Translate('Product Code'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:ProductCode  Routine
  ! GetDescription resets the 'NewValue' to the code, if the description is found. Returns 1 if code valid.
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('Value')
    if p_web.GetBrowseValue(p_web.GetValue('Value'),Net:Web:Record)
      p_web.DeleteValue('value')
    else
      loc:ok = p_web.GetDescription(MODPROD,mop:ProductCodeKey,mop:ProductCodeKey,mop:ProductCode,mop:ProductCode,p_web.GetValue('Value')) !7
    end
  Else
    loc:ok = p_web.GetDescription(MODPROD,mop:ProductCodeKey,mop:ProductCodeKey,mop:ProductCode,mop:ProductCode,p_web.GetSessionValue('job:ProductCode')) !7
  End
  loc:lookupdone = 1
  If p_web.IfExistsValue('NewValue') then p_web.SetValue('Value',p_web.GetValue('NewValue')).
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:ProductCode = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    job:ProductCode = p_web.GetValue('Value')
  ElsIf p_web.RequestAjax = 1 and p_web.IfExistsValue('mop:ProductCode')
    job:ProductCode = p_web.GetValue('mop:ProductCode')
  ElsIf p_web.RequestAjax = 1
    job:ProductCode = mop:ProductCode
  End
  do ValidateValue::job:ProductCode  ! copies value to session value if valid.
  p_Web.SetValue('lookupfield','job:ProductCode')
  do AfterLookup
  do Value::job:ProductCode
  do SendAlert
  do Comment::job:ProductCode

ValidateValue::job:ProductCode  Routine
    If not (p_web.GSV('Hide:ProductCode') = 1)
    job:ProductCode = Upper(job:ProductCode)
      if loc:invalid = '' then p_web.SetSessionValue('job:ProductCode',job:ProductCode).
    End

Value::job:ProductCode  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:ProductCode') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('job:ProductCode') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.GSV('ReadOnly:ProductCode') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    job:ProductCode = p_web.RestoreValue('job:ProductCode')
    do ValidateValue::job:ProductCode
    If job:ProductCode:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:ProductCode') = 1)
  ! --- STRING --- job:ProductCode
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:ProductCode') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:ProductCode'',''jobfaultcodes_job:productcode_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:ProductCode')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = 'readonly'
    packet = clip(packet) & p_web.CreateInput('text','job:ProductCode',p_web.GetSessionValue('job:ProductCode'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s30',loc:javascript,p_web.PicLength('@s30'),,,) & '<13,10>'!4
    if not loc:viewonly and not loc:readonly
      loc:fieldclass = ''
      packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LookupButton,loc:formname,,,'ntd.push(''LookupProductCodes'','''&p_web._nocolon('job:ProductCode')&''','''&p_web.translate('Select Product Code')&''',1,'&Net:LookupRecord&',''mop:ProductCode'',''JobFaultCodes'')',,,loc:fieldclass)
    End
  do SendPacket
  End
  p_web.DivFooter()
Comment::job:ProductCode  Routine
  data
loc:class  string(255)
  code
  ! GetDescription resets the 'NewValue' to the code, if the description is found. Returns 1 if code valid.
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('Value')
    loc:ok = p_web.GetDescription(MODPROD,mop:ProductCodeKey,mop:ProductCodeKey,mop:ProductCode,mop:ProductCode,p_web.GetValue('Value')) !1
    loc:lookupdone = 1
  Else
    loc:ok = p_web.GetDescription(MODPROD,mop:ProductCodeKey,mop:ProductCodeKey,mop:ProductCode,mop:ProductCode,p_web.GetSessionValue('job:ProductCode')) !1
  End
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if job:ProductCode:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate('')
  loc:class = Choose(p_web.GSV('Hide:ProductCode') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('job:ProductCode') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:ProductCode') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:FaultCode1  Routine
  packet = clip(packet) & p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode1') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode1') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:JobFaultCode1') = 1,'',p_web.Translate(p_web.GSV('Prompt:JobFaultCode1')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:FaultCode1  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:FaultCode1 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = p_web.GSV('Picture:JobFaultCode1')  !FieldPicture = 
    tmp:FaultCode1 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode1'))
  End
  do ValidateValue::tmp:FaultCode1  ! copies value to session value if valid.
  do Value::tmp:FaultCode1
  do SendAlert
  do Comment::tmp:FaultCode1 ! allows comment style to be updated.

ValidateValue::tmp:FaultCode1  Routine
    If not (p_web.GSV('Hide:JobFaultCode1') = 1)
  If tmp:FaultCode1 = '' and p_web.GSV('Req:JobFaultCode1') = 1
    loc:Invalid = 'tmp:FaultCode1'
    tmp:FaultCode1:IsInvalid = true
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode1')) & ' ' & p_web.site.RequiredText
  End
      if loc:invalid = '' then p_web.SetSessionValue('tmp:FaultCode1',tmp:FaultCode1).
    End

Value::tmp:FaultCode1  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:JobFaultCode1') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode1') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If p_web.GSV('ReadOnly:JobFaultCode1') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If (p_web.GSV('Req:JobFaultCode1') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  End
  If loc:retrying
    tmp:FaultCode1 = p_web.RestoreValue('tmp:FaultCode1')
    do ValidateValue::tmp:FaultCode1
    If tmp:FaultCode1:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode1') = 1)
  ! --- STRING --- tmp:FaultCode1
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode1') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode1'',''jobfaultcodes_tmp:faultcode1_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode1',p_web.GetSessionValue('tmp:FaultCode1'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode1'),loc:javascript,,,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(1)
  End
  p_web.DivFooter()
Comment::tmp:FaultCode1  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:FaultCode1:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode1'))
  loc:class = Choose(p_web.GSV('Hide:JobFaultCode1') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode1') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:JobFaultCode1') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:FaultCode2  Routine
  packet = clip(packet) & p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode2') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode2') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:JobFaultCode2') = 1,'',p_web.Translate(p_web.GSV('Prompt:JobFaultCode2')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:FaultCode2  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:FaultCode2 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = p_web.GSV('Picture:JobFaultCode2')  !FieldPicture = 
    tmp:FaultCode2 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode2'))
  End
  do ValidateValue::tmp:FaultCode2  ! copies value to session value if valid.
  do Value::tmp:FaultCode2
  do SendAlert
  do Comment::tmp:FaultCode2 ! allows comment style to be updated.

ValidateValue::tmp:FaultCode2  Routine
    If not (p_web.GSV('Hide:JobFaultCode2') = 1)
  If tmp:FaultCode2 = '' and p_web.GSV('Req:JobFaultCode2') = 1
    loc:Invalid = 'tmp:FaultCode2'
    tmp:FaultCode2:IsInvalid = true
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode2')) & ' ' & p_web.site.RequiredText
  End
      if loc:invalid = '' then p_web.SetSessionValue('tmp:FaultCode2',tmp:FaultCode2).
    End

Value::tmp:FaultCode2  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:JobFaultCode2') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode2') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If p_web.GSV('ReadOnly:JobFaultCode2') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If (p_web.GSV('Req:JobFaultCode2') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  End
  If loc:retrying
    tmp:FaultCode2 = p_web.RestoreValue('tmp:FaultCode2')
    do ValidateValue::tmp:FaultCode2
    If tmp:FaultCode2:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode2') = 1)
  ! --- STRING --- tmp:FaultCode2
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode2') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode2'',''jobfaultcodes_tmp:faultcode2_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode2',p_web.GetSessionValue('tmp:FaultCode2'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode2'),loc:javascript,,,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(2)
  End
  p_web.DivFooter()
Comment::tmp:FaultCode2  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:FaultCode2:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode2'))
  loc:class = Choose(p_web.GSV('Hide:JobFaultCode2') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode2') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:JobFaultCode2') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:FaultCode3  Routine
  packet = clip(packet) & p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode3') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode3') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:JobFaultCode3') = 1,'',p_web.Translate(p_web.GSV('Prompt:JobFaultCode3')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:FaultCode3  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:FaultCode3 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = p_web.GSV('Picture:JobFaultCode3')  !FieldPicture = 
    tmp:FaultCode3 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode3'))
  End
  do ValidateValue::tmp:FaultCode3  ! copies value to session value if valid.
  do Value::tmp:FaultCode3
  do SendAlert
  do Comment::tmp:FaultCode3 ! allows comment style to be updated.

ValidateValue::tmp:FaultCode3  Routine
    If not (p_web.GSV('Hide:JobFaultCode3') = 1)
  If tmp:FaultCode3 = '' and p_web.GSV('Req:JobFaultCode3') = 1
    loc:Invalid = 'tmp:FaultCode3'
    tmp:FaultCode3:IsInvalid = true
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode3')) & ' ' & p_web.site.RequiredText
  End
      if loc:invalid = '' then p_web.SetSessionValue('tmp:FaultCode3',tmp:FaultCode3).
    End

Value::tmp:FaultCode3  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:JobFaultCode3') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode3') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If p_web.GSV('ReadOnly:JobFaultCode3') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If (p_web.GSV('Req:JobFaultCode3') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  End
  If loc:retrying
    tmp:FaultCode3 = p_web.RestoreValue('tmp:FaultCode3')
    do ValidateValue::tmp:FaultCode3
    If tmp:FaultCode3:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode3') = 1)
  ! --- STRING --- tmp:FaultCode3
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode3') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode3'',''jobfaultcodes_tmp:faultcode3_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode3',p_web.GetSessionValue('tmp:FaultCode3'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode3'),loc:javascript,,,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(3)
  End
  p_web.DivFooter()
Comment::tmp:FaultCode3  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:FaultCode3:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode3'))
  loc:class = Choose(p_web.GSV('Hide:JobFaultCode3') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode3') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:JobFaultCode3') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:FaultCode4  Routine
  packet = clip(packet) & p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode4') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode4') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:JobFaultCode4') = 1,'',p_web.Translate(p_web.GSV('Prompt:JobFaultCode4')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:FaultCode4  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:FaultCode4 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = p_web.GSV('Picture:JobFaultCode4')  !FieldPicture = 
    tmp:FaultCode4 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode4'))
  End
  do ValidateValue::tmp:FaultCode4  ! copies value to session value if valid.
  do Value::tmp:FaultCode4
  do SendAlert
  do Comment::tmp:FaultCode4 ! allows comment style to be updated.

ValidateValue::tmp:FaultCode4  Routine
    If not (p_web.GSV('Hide:JobFaultCode4') = 1)
  If tmp:FaultCode4 = '' and p_web.GSV('Req:JobFaultCode4') = 1
    loc:Invalid = 'tmp:FaultCode4'
    tmp:FaultCode4:IsInvalid = true
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode4')) & ' ' & p_web.site.RequiredText
  End
      if loc:invalid = '' then p_web.SetSessionValue('tmp:FaultCode4',tmp:FaultCode4).
    End

Value::tmp:FaultCode4  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:JobFaultCode4') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode4') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If p_web.GSV('ReadOnly:JobFaultCode4') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If (p_web.GSV('Req:JobFaultCode4') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  End
  If loc:retrying
    tmp:FaultCode4 = p_web.RestoreValue('tmp:FaultCode4')
    do ValidateValue::tmp:FaultCode4
    If tmp:FaultCode4:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode4') = 1)
  ! --- STRING --- tmp:FaultCode4
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode4') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode4'',''jobfaultcodes_tmp:faultcode4_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode4',p_web.GetSessionValue('tmp:FaultCode4'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode4'),loc:javascript,,,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(4)
  End
  p_web.DivFooter()
Comment::tmp:FaultCode4  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:FaultCode4:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode4'))
  loc:class = Choose(p_web.GSV('Hide:JobFaultCode4') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode4') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:JobFaultCode4') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:FaultCode5  Routine
  packet = clip(packet) & p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode5') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode5') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:JobFaultCode5') = 1,'',p_web.Translate(p_web.GSV('Prompt:JobFaultCode5')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:FaultCode5  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:FaultCode5 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = p_web.GSV('Picture:JobFaultCode5')  !FieldPicture = 
    tmp:FaultCode5 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode5'))
  End
  do ValidateValue::tmp:FaultCode5  ! copies value to session value if valid.
  do Value::tmp:FaultCode5
  do SendAlert
  do Comment::tmp:FaultCode5 ! allows comment style to be updated.

ValidateValue::tmp:FaultCode5  Routine
    If not (p_web.GSV('Hide:JobFaultCode5') = 1)
  If tmp:FaultCode5 = '' and p_web.GSV('Req:JobFaultCode5') = 1
    loc:Invalid = 'tmp:FaultCode5'
    tmp:FaultCode5:IsInvalid = true
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode5')) & ' ' & p_web.site.RequiredText
  End
      if loc:invalid = '' then p_web.SetSessionValue('tmp:FaultCode5',tmp:FaultCode5).
    End

Value::tmp:FaultCode5  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:JobFaultCode5') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode5') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If p_web.GSV('ReadOnly:JobFaultCode5') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If (p_web.GSV('Req:JobFaultCode5') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  End
  If loc:retrying
    tmp:FaultCode5 = p_web.RestoreValue('tmp:FaultCode5')
    do ValidateValue::tmp:FaultCode5
    If tmp:FaultCode5:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode5') = 1)
  ! --- STRING --- tmp:FaultCode5
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode5') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode5'',''jobfaultcodes_tmp:faultcode5_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode5',p_web.GetSessionValue('tmp:FaultCode5'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode5'),loc:javascript,,,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(5)
  End
  p_web.DivFooter()
Comment::tmp:FaultCode5  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:FaultCode5:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode5'))
  loc:class = Choose(p_web.GSV('Hide:JobFaultCode5') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode5') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:JobFaultCode5') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:FaultCode6  Routine
  packet = clip(packet) & p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode6') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode6') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:JobFaultCode6') = 1,'',p_web.Translate(p_web.GSV('Prompt:JobFaultCode6')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:FaultCode6  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:FaultCode6 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = p_web.GSV('Picture:JobFaultCode6')  !FieldPicture = 
    tmp:FaultCode6 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode6'))
  End
  do ValidateValue::tmp:FaultCode6  ! copies value to session value if valid.
  do Value::tmp:FaultCode6
  do SendAlert
  do Comment::tmp:FaultCode6 ! allows comment style to be updated.

ValidateValue::tmp:FaultCode6  Routine
    If not (p_web.GSV('Hide:JobFaultCode6') = 1)
  If tmp:FaultCode6 = '' and p_web.GSV('Req:JobFaultCode6') = 1
    loc:Invalid = 'tmp:FaultCode6'
    tmp:FaultCode6:IsInvalid = true
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode6')) & ' ' & p_web.site.RequiredText
  End
      if loc:invalid = '' then p_web.SetSessionValue('tmp:FaultCode6',tmp:FaultCode6).
    End

Value::tmp:FaultCode6  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:JobFaultCode6') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode6') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If p_web.GSV('ReadOnly:JobFaultCode6') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If (p_web.GSV('Req:JobFaultCode6') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  End
  If loc:retrying
    tmp:FaultCode6 = p_web.RestoreValue('tmp:FaultCode6')
    do ValidateValue::tmp:FaultCode6
    If tmp:FaultCode6:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode6') = 1)
  ! --- STRING --- tmp:FaultCode6
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode6') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode6'',''jobfaultcodes_tmp:faultcode6_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode6',p_web.GetSessionValue('tmp:FaultCode6'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode6'),loc:javascript,,,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(6)
  End
  p_web.DivFooter()
Comment::tmp:FaultCode6  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:FaultCode6:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode6'))
  loc:class = Choose(p_web.GSV('Hide:JobFaultCode6') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode6') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:JobFaultCode6') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:FaultCode7  Routine
  packet = clip(packet) & p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode7') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode7') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:JobFaultCode7') = 1,'',p_web.Translate(p_web.GSV('Prompt:JobFaultCode7')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:FaultCode7  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:FaultCode7 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = p_web.GSV('Picture:JobFaultCode7')  !FieldPicture = 
    tmp:FaultCode7 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode7'))
  End
  do ValidateValue::tmp:FaultCode7  ! copies value to session value if valid.
  do Value::tmp:FaultCode7
  do SendAlert
  do Comment::tmp:FaultCode7 ! allows comment style to be updated.

ValidateValue::tmp:FaultCode7  Routine
    If not (p_web.GSV('Hide:JobFaultCode7') = 1)
  If tmp:FaultCode7 = '' and p_web.GSV('Req:JobFaultCode7') = 1
    loc:Invalid = 'tmp:FaultCode7'
    tmp:FaultCode7:IsInvalid = true
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode7')) & ' ' & p_web.site.RequiredText
  End
      if loc:invalid = '' then p_web.SetSessionValue('tmp:FaultCode7',tmp:FaultCode7).
    End

Value::tmp:FaultCode7  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:JobFaultCode7') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode7') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If p_web.GSV('ReadOnly:JobFaultCode7') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If (p_web.GSV('Req:JobFaultCode7') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  End
  If loc:retrying
    tmp:FaultCode7 = p_web.RestoreValue('tmp:FaultCode7')
    do ValidateValue::tmp:FaultCode7
    If tmp:FaultCode7:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode7') = 1)
  ! --- STRING --- tmp:FaultCode7
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode7') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode7'',''jobfaultcodes_tmp:faultcode7_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode7',p_web.GetSessionValue('tmp:FaultCode7'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode7'),loc:javascript,,,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(7)
  End
  p_web.DivFooter()
Comment::tmp:FaultCode7  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:FaultCode7:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode7'))
  loc:class = Choose(p_web.GSV('Hide:JobFaultCode7') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode7') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:JobFaultCode7') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:FaultCode8  Routine
  packet = clip(packet) & p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode8') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode8') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:JobFaultCode8') = 1,'',p_web.Translate(p_web.GSV('Prompt:JobFaultCode8')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:FaultCode8  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:FaultCode8 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = p_web.GSV('Picture:JobFaultCode8')  !FieldPicture = 
    tmp:FaultCode8 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode8'))
  End
  do ValidateValue::tmp:FaultCode8  ! copies value to session value if valid.
  do Value::tmp:FaultCode8
  do SendAlert
  do Comment::tmp:FaultCode8 ! allows comment style to be updated.

ValidateValue::tmp:FaultCode8  Routine
    If not (p_web.GSV('Hide:JobFaultCode8') = 1)
  If tmp:FaultCode8 = '' and p_web.GSV('Req:JobFaultCode8') = 1
    loc:Invalid = 'tmp:FaultCode8'
    tmp:FaultCode8:IsInvalid = true
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode8')) & ' ' & p_web.site.RequiredText
  End
      if loc:invalid = '' then p_web.SetSessionValue('tmp:FaultCode8',tmp:FaultCode8).
    End

Value::tmp:FaultCode8  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:JobFaultCode8') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode8') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If p_web.GSV('ReadOnly:JobFaultCode8') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If (p_web.GSV('Req:JobFaultCode8') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  End
  If loc:retrying
    tmp:FaultCode8 = p_web.RestoreValue('tmp:FaultCode8')
    do ValidateValue::tmp:FaultCode8
    If tmp:FaultCode8:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode8') = 1)
  ! --- STRING --- tmp:FaultCode8
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode8') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode8'',''jobfaultcodes_tmp:faultcode8_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode8',p_web.GetSessionValue('tmp:FaultCode8'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode8'),loc:javascript,,,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(8)
  End
  p_web.DivFooter()
Comment::tmp:FaultCode8  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:FaultCode8:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode8'))
  loc:class = Choose(p_web.GSV('Hide:JobFaultCode8') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode8') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:JobFaultCode8') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:FaultCode9  Routine
  packet = clip(packet) & p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode9') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode9') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:JobFaultCode9') = 1,'',p_web.Translate(p_web.GSV('Prompt:JobFaultCode9')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:FaultCode9  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:FaultCode9 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = p_web.GSV('Picture:JobFaultCode9')  !FieldPicture = 
    tmp:FaultCode9 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode9'))
  End
  do ValidateValue::tmp:FaultCode9  ! copies value to session value if valid.
  do Value::tmp:FaultCode9
  do SendAlert
  do Comment::tmp:FaultCode9 ! allows comment style to be updated.

ValidateValue::tmp:FaultCode9  Routine
    If not (p_web.GSV('Hide:JobFaultCode9') = 1)
  If tmp:FaultCode9 = '' and p_web.GSV('Req:JobFaultCode9') = 1
    loc:Invalid = 'tmp:FaultCode9'
    tmp:FaultCode9:IsInvalid = true
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode9')) & ' ' & p_web.site.RequiredText
  End
      if loc:invalid = '' then p_web.SetSessionValue('tmp:FaultCode9',tmp:FaultCode9).
    End

Value::tmp:FaultCode9  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:JobFaultCode9') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode9') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If p_web.GSV('ReadOnly:JobFaultCode9') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If (p_web.GSV('Req:JobFaultCode9') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  End
  If loc:retrying
    tmp:FaultCode9 = p_web.RestoreValue('tmp:FaultCode9')
    do ValidateValue::tmp:FaultCode9
    If tmp:FaultCode9:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode9') = 1)
  ! --- STRING --- tmp:FaultCode9
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode9') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode9'',''jobfaultcodes_tmp:faultcode9_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode9',p_web.GetSessionValue('tmp:FaultCode9'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode9'),loc:javascript,,,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(9)
  End
  p_web.DivFooter()
Comment::tmp:FaultCode9  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:FaultCode9:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode9'))
  loc:class = Choose(p_web.GSV('Hide:JobFaultCode9') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode9') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:JobFaultCode9') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:FaultCode10  Routine
  packet = clip(packet) & p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode10') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode10') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:JobFaultCode10') = 1,'',p_web.Translate(p_web.GSV('Prompt:JobFaultCode10')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:FaultCode10  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:FaultCode10 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = p_web.GSV('Picture:JobFaultCode10')  !FieldPicture = 
    tmp:FaultCode10 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode10'))
  End
  do ValidateValue::tmp:FaultCode10  ! copies value to session value if valid.
  do Value::tmp:FaultCode10
  do SendAlert
  do Comment::tmp:FaultCode10 ! allows comment style to be updated.

ValidateValue::tmp:FaultCode10  Routine
    If not (p_web.GSV('Hide:JobFaultCode10') = 1)
  If tmp:FaultCode10 = '' and p_web.GSV('Req:JobFaultCode10') = 1
    loc:Invalid = 'tmp:FaultCode10'
    tmp:FaultCode10:IsInvalid = true
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode10')) & ' ' & p_web.site.RequiredText
  End
      if loc:invalid = '' then p_web.SetSessionValue('tmp:FaultCode10',tmp:FaultCode10).
    End

Value::tmp:FaultCode10  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:JobFaultCode10') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode10') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If p_web.GSV('ReadOnly:JobFaultCode10') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If (p_web.GSV('Req:JobFaultCode10') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  End
  If loc:retrying
    tmp:FaultCode10 = p_web.RestoreValue('tmp:FaultCode10')
    do ValidateValue::tmp:FaultCode10
    If tmp:FaultCode10:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode10') = 1)
  ! --- STRING --- tmp:FaultCode10
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode10') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode10'',''jobfaultcodes_tmp:faultcode10_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode10',p_web.GetSessionValue('tmp:FaultCode10'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode10'),loc:javascript,,,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(10)
  End
  p_web.DivFooter()
Comment::tmp:FaultCode10  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:FaultCode10:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode10'))
  loc:class = Choose(p_web.GSV('Hide:JobFaultCode10') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode10') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:JobFaultCode10') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:FaultCode11  Routine
  packet = clip(packet) & p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode11') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode11') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:JobFaultCode11') = 1,'',p_web.Translate(p_web.GSV('Prompt:JobFaultCode11')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:FaultCode11  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:FaultCode11 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = p_web.GSV('Picture:JobFaultCode11')  !FieldPicture = 
    tmp:FaultCode11 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode11'))
  End
  do ValidateValue::tmp:FaultCode11  ! copies value to session value if valid.
  do Value::tmp:FaultCode11
  do SendAlert
  do Comment::tmp:FaultCode11 ! allows comment style to be updated.

ValidateValue::tmp:FaultCode11  Routine
    If not (p_web.GSV('Hide:JobFaultCode11') = 1)
  If tmp:FaultCode11 = '' and p_web.GSV('Req:JobFaultCode11') = 1
    loc:Invalid = 'tmp:FaultCode11'
    tmp:FaultCode11:IsInvalid = true
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode11')) & ' ' & p_web.site.RequiredText
  End
      if loc:invalid = '' then p_web.SetSessionValue('tmp:FaultCode11',tmp:FaultCode11).
    End

Value::tmp:FaultCode11  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:JobFaultCode11') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode11') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If p_web.GSV('ReadOnly:JobFaultCode11') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If (p_web.GSV('Req:JobFaultCode11') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  End
  If loc:retrying
    tmp:FaultCode11 = p_web.RestoreValue('tmp:FaultCode11')
    do ValidateValue::tmp:FaultCode11
    If tmp:FaultCode11:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode11') = 1)
  ! --- STRING --- tmp:FaultCode11
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode11') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode11'',''jobfaultcodes_tmp:faultcode11_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode11',p_web.GetSessionValue('tmp:FaultCode11'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode11'),loc:javascript,,,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(11)
  End
  p_web.DivFooter()
Comment::tmp:FaultCode11  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:FaultCode11:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode11'))
  loc:class = Choose(p_web.GSV('Hide:JobFaultCode11') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode11') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:JobFaultCode11') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:FaultCode12  Routine
  packet = clip(packet) & p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode12') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode12') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:JobFaultCode12') = 1,'',p_web.Translate(p_web.GSV('Prompt:JobFaultCode12')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:FaultCode12  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:FaultCode12 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = p_web.GSV('Picture:JobFaultCode12')  !FieldPicture = 
    tmp:FaultCode12 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode12'))
  End
  do ValidateValue::tmp:FaultCode12  ! copies value to session value if valid.
  do Value::tmp:FaultCode12
  do SendAlert
  do Comment::tmp:FaultCode12 ! allows comment style to be updated.

ValidateValue::tmp:FaultCode12  Routine
    If not (p_web.GSV('Hide:JobFaultCode12') = 1)
  If tmp:FaultCode12 = '' and p_web.GSV('Req:JobFaultCode12') = 1
    loc:Invalid = 'tmp:FaultCode12'
    tmp:FaultCode12:IsInvalid = true
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode12')) & ' ' & p_web.site.RequiredText
  End
      if loc:invalid = '' then p_web.SetSessionValue('tmp:FaultCode12',tmp:FaultCode12).
    End

Value::tmp:FaultCode12  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:JobFaultCode12') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode12') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If p_web.GSV('ReadOnly:JobFaultCode12') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If (p_web.GSV('Req:JobFaultCode12') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  End
  If loc:retrying
    tmp:FaultCode12 = p_web.RestoreValue('tmp:FaultCode12')
    do ValidateValue::tmp:FaultCode12
    If tmp:FaultCode12:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode12') = 1)
  ! --- STRING --- tmp:FaultCode12
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode12') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode12'',''jobfaultcodes_tmp:faultcode12_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode12',p_web.GetSessionValue('tmp:FaultCode12'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode12'),loc:javascript,,,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(12)
  End
  p_web.DivFooter()
Comment::tmp:FaultCode12  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:FaultCode12:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode12'))
  loc:class = Choose(p_web.GSV('Hide:JobFaultCode12') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode12') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:JobFaultCode12') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:FaultCode13  Routine
  packet = clip(packet) & p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode13') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode13') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:JobFaultCode13') = 1,'',p_web.Translate(p_web.GSV('Prompt:JobFaultCode13')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:FaultCode13  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:FaultCode13 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = p_web.GSV('Picture:JobFaultCode13')  !FieldPicture = 
    tmp:FaultCode13 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode13'))
  End
  do ValidateValue::tmp:FaultCode13  ! copies value to session value if valid.
  do Value::tmp:FaultCode13
  do SendAlert
  do Comment::tmp:FaultCode13 ! allows comment style to be updated.

ValidateValue::tmp:FaultCode13  Routine
    If not (p_web.GSV('Hide:JobFaultCode13') = 1)
  If tmp:FaultCode13 = '' and p_web.GSV('Req:JobFaultCode13') = 1
    loc:Invalid = 'tmp:FaultCode13'
    tmp:FaultCode13:IsInvalid = true
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode13')) & ' ' & p_web.site.RequiredText
  End
      if loc:invalid = '' then p_web.SetSessionValue('tmp:FaultCode13',tmp:FaultCode13).
    End

Value::tmp:FaultCode13  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:JobFaultCode13') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode13') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If p_web.GSV('ReadOnly:JobFaultCode13') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If (p_web.GSV('Req:JobFaultCode13') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  End
  If loc:retrying
    tmp:FaultCode13 = p_web.RestoreValue('tmp:FaultCode13')
    do ValidateValue::tmp:FaultCode13
    If tmp:FaultCode13:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode13') = 1)
  ! --- STRING --- tmp:FaultCode13
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode13') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode13'',''jobfaultcodes_tmp:faultcode13_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode13',p_web.GetSessionValue('tmp:FaultCode13'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode13'),loc:javascript,,,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(13)
  End
  p_web.DivFooter()
Comment::tmp:FaultCode13  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:FaultCode13:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode13'))
  loc:class = Choose(p_web.GSV('Hide:JobFaultCode13') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode13') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:JobFaultCode13') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:FaultCode14  Routine
  packet = clip(packet) & p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode14') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode14') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:JobFaultCode14') = 1,'',p_web.Translate(p_web.GSV('Prompt:JobFaultCode14')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:FaultCode14  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:FaultCode14 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = p_web.GSV('Picture:JobFaultCode14')  !FieldPicture = 
    tmp:FaultCode14 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode14'))
  End
  do ValidateValue::tmp:FaultCode14  ! copies value to session value if valid.
  do Value::tmp:FaultCode14
  do SendAlert
  do Comment::tmp:FaultCode14 ! allows comment style to be updated.

ValidateValue::tmp:FaultCode14  Routine
    If not (p_web.GSV('Hide:JobFaultCode14') = 1)
  If tmp:FaultCode14 = '' and p_web.GSV('Req:JobFaultCode14') = 1
    loc:Invalid = 'tmp:FaultCode14'
    tmp:FaultCode14:IsInvalid = true
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode14')) & ' ' & p_web.site.RequiredText
  End
      if loc:invalid = '' then p_web.SetSessionValue('tmp:FaultCode14',tmp:FaultCode14).
    End

Value::tmp:FaultCode14  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:JobFaultCode14') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode14') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If p_web.GSV('ReadOnly:JobFaultCode14') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If (p_web.GSV('Req:JobFaultCode14') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  End
  If loc:retrying
    tmp:FaultCode14 = p_web.RestoreValue('tmp:FaultCode14')
    do ValidateValue::tmp:FaultCode14
    If tmp:FaultCode14:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode14') = 1)
  ! --- STRING --- tmp:FaultCode14
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode14') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode14'',''jobfaultcodes_tmp:faultcode14_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode14',p_web.GetSessionValue('tmp:FaultCode14'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode14'),loc:javascript,,,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(14)
  End
  p_web.DivFooter()
Comment::tmp:FaultCode14  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:FaultCode14:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode14'))
  loc:class = Choose(p_web.GSV('Hide:JobFaultCode14') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode14') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:JobFaultCode14') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:FaultCode15  Routine
  packet = clip(packet) & p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode15') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode15') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:JobFaultCode15') = 1,'',p_web.Translate(p_web.GSV('Prompt:JobFaultCode15')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:FaultCode15  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:FaultCode15 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = p_web.GSV('Picture:JobFaultCode15')  !FieldPicture = 
    tmp:FaultCode15 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode15'))
  End
  do ValidateValue::tmp:FaultCode15  ! copies value to session value if valid.
  do Value::tmp:FaultCode15
  do SendAlert
  do Comment::tmp:FaultCode15 ! allows comment style to be updated.

ValidateValue::tmp:FaultCode15  Routine
    If not (p_web.GSV('Hide:JobFaultCode15') = 1)
  If tmp:FaultCode15 = '' and p_web.GSV('Req:JobFaultCode15') = 1
    loc:Invalid = 'tmp:FaultCode15'
    tmp:FaultCode15:IsInvalid = true
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode15')) & ' ' & p_web.site.RequiredText
  End
      if loc:invalid = '' then p_web.SetSessionValue('tmp:FaultCode15',tmp:FaultCode15).
    End

Value::tmp:FaultCode15  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:JobFaultCode15') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode15') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If p_web.GSV('ReadOnly:JobFaultCode15') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If (p_web.GSV('Req:JobFaultCode15') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  End
  If loc:retrying
    tmp:FaultCode15 = p_web.RestoreValue('tmp:FaultCode15')
    do ValidateValue::tmp:FaultCode15
    If tmp:FaultCode15:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode15') = 1)
  ! --- STRING --- tmp:FaultCode15
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode15') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode15'',''jobfaultcodes_tmp:faultcode15_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode15',p_web.GetSessionValue('tmp:FaultCode15'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode15'),loc:javascript,,,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(15)
  End
  p_web.DivFooter()
Comment::tmp:FaultCode15  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:FaultCode15:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode15'))
  loc:class = Choose(p_web.GSV('Hide:JobFaultCode15') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode15') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:JobFaultCode15') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:FaultCode16  Routine
  packet = clip(packet) & p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode16') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode16') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:JobFaultCode16') = 1,'',p_web.Translate(p_web.GSV('Prompt:JobFaultCode16')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:FaultCode16  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:FaultCode16 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = p_web.GSV('Picture:JobFaultCode16')  !FieldPicture = 
    tmp:FaultCode16 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode16'))
  End
  do ValidateValue::tmp:FaultCode16  ! copies value to session value if valid.
  do Value::tmp:FaultCode16
  do SendAlert
  do Comment::tmp:FaultCode16 ! allows comment style to be updated.

ValidateValue::tmp:FaultCode16  Routine
    If not (p_web.GSV('Hide:JobFaultCode16') = 1)
  If tmp:FaultCode16 = '' and p_web.GSV('Req:JobFaultCode16') = 1
    loc:Invalid = 'tmp:FaultCode16'
    tmp:FaultCode16:IsInvalid = true
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode16')) & ' ' & p_web.site.RequiredText
  End
      if loc:invalid = '' then p_web.SetSessionValue('tmp:FaultCode16',tmp:FaultCode16).
    End

Value::tmp:FaultCode16  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:JobFaultCode16') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode16') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If p_web.GSV('ReadOnly:JobFaultCode16') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If (p_web.GSV('Req:JobFaultCode16') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  End
  If loc:retrying
    tmp:FaultCode16 = p_web.RestoreValue('tmp:FaultCode16')
    do ValidateValue::tmp:FaultCode16
    If tmp:FaultCode16:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode16') = 1)
  ! --- STRING --- tmp:FaultCode16
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode16') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode16'',''jobfaultcodes_tmp:faultcode16_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode16',p_web.GetSessionValue('tmp:FaultCode16'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode16'),loc:javascript,,,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(16)
  End
  p_web.DivFooter()
Comment::tmp:FaultCode16  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:FaultCode16:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode16'))
  loc:class = Choose(p_web.GSV('Hide:JobFaultCode16') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode16') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:JobFaultCode16') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:FaultCode17  Routine
  packet = clip(packet) & p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode17') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode17') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:JobFaultCode17') = 1,'',p_web.Translate(p_web.GSV('Prompt:JobFaultCode17')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:FaultCode17  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:FaultCode17 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = p_web.GSV('Picture:JobFaultCode17')  !FieldPicture = 
    tmp:FaultCode17 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode17'))
  End
  do ValidateValue::tmp:FaultCode17  ! copies value to session value if valid.
  do Value::tmp:FaultCode17
  do SendAlert
  do Comment::tmp:FaultCode17 ! allows comment style to be updated.

ValidateValue::tmp:FaultCode17  Routine
    If not (p_web.GSV('Hide:JobFaultCode17') = 1)
  If tmp:FaultCode17 = '' and p_web.GSV('Req:JobFaultCode17') = 1
    loc:Invalid = 'tmp:FaultCode17'
    tmp:FaultCode17:IsInvalid = true
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode17')) & ' ' & p_web.site.RequiredText
  End
      if loc:invalid = '' then p_web.SetSessionValue('tmp:FaultCode17',tmp:FaultCode17).
    End

Value::tmp:FaultCode17  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:JobFaultCode17') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode17') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If p_web.GSV('ReadOnly:JobFaultCode17') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If (p_web.GSV('Req:JobFaultCode17') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  End
  If loc:retrying
    tmp:FaultCode17 = p_web.RestoreValue('tmp:FaultCode17')
    do ValidateValue::tmp:FaultCode17
    If tmp:FaultCode17:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode17') = 1)
  ! --- STRING --- tmp:FaultCode17
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode17') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode17'',''jobfaultcodes_tmp:faultcode17_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode17',p_web.GetSessionValue('tmp:FaultCode17'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode17'),loc:javascript,,,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(17)
  End
  p_web.DivFooter()
Comment::tmp:FaultCode17  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:FaultCode17:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode17'))
  loc:class = Choose(p_web.GSV('Hide:JobFaultCode17') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode17') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:JobFaultCode17') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:FaultCode18  Routine
  packet = clip(packet) & p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode18') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode18') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:JobFaultCode18') = 1,'',p_web.Translate(p_web.GSV('Prompt:JobFaultCode18')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:FaultCode18  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:FaultCode18 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = p_web.GSV('Picture:JobFaultCode18')  !FieldPicture = 
    tmp:FaultCode18 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode18'))
  End
  do ValidateValue::tmp:FaultCode18  ! copies value to session value if valid.
  do Value::tmp:FaultCode18
  do SendAlert
  do Comment::tmp:FaultCode18 ! allows comment style to be updated.

ValidateValue::tmp:FaultCode18  Routine
    If not (p_web.GSV('Hide:JobFaultCode18') = 1)
  If tmp:FaultCode18 = '' and p_web.GSV('Req:JobFaultCode18') = 1
    loc:Invalid = 'tmp:FaultCode18'
    tmp:FaultCode18:IsInvalid = true
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode18')) & ' ' & p_web.site.RequiredText
  End
      if loc:invalid = '' then p_web.SetSessionValue('tmp:FaultCode18',tmp:FaultCode18).
    End

Value::tmp:FaultCode18  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:JobFaultCode18') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode18') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If p_web.GSV('ReadOnly:JobFaultCode18') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If (p_web.GSV('Req:JobFaultCode18') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  End
  If loc:retrying
    tmp:FaultCode18 = p_web.RestoreValue('tmp:FaultCode18')
    do ValidateValue::tmp:FaultCode18
    If tmp:FaultCode18:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode18') = 1)
  ! --- STRING --- tmp:FaultCode18
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode18') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode18'',''jobfaultcodes_tmp:faultcode18_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode18',p_web.GetSessionValue('tmp:FaultCode18'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode18'),loc:javascript,,,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(18)
  End
  p_web.DivFooter()
Comment::tmp:FaultCode18  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:FaultCode18:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode18'))
  loc:class = Choose(p_web.GSV('Hide:JobFaultCode18') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode18') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:JobFaultCode18') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:FaultCode19  Routine
  packet = clip(packet) & p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode19') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode19') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:JobFaultCode19') = 1,'',p_web.Translate(p_web.GSV('Prompt:JobFaultCode19')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:FaultCode19  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:FaultCode19 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = p_web.GSV('Picture:JobFaultCode19')  !FieldPicture = 
    tmp:FaultCode19 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode19'))
  End
  do ValidateValue::tmp:FaultCode19  ! copies value to session value if valid.
  do Value::tmp:FaultCode19
  do SendAlert
  do Comment::tmp:FaultCode19 ! allows comment style to be updated.

ValidateValue::tmp:FaultCode19  Routine
    If not (p_web.GSV('Hide:JobFaultCode19') = 1)
  If tmp:FaultCode19 = '' and p_web.GSV('Req:JobFaultCode19') = 1
    loc:Invalid = 'tmp:FaultCode19'
    tmp:FaultCode19:IsInvalid = true
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode19')) & ' ' & p_web.site.RequiredText
  End
      if loc:invalid = '' then p_web.SetSessionValue('tmp:FaultCode19',tmp:FaultCode19).
    End

Value::tmp:FaultCode19  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:JobFaultCode19') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode19') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If p_web.GSV('ReadOnly:JobFaultCode19') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If (p_web.GSV('Req:JobFaultCode19') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  End
  If loc:retrying
    tmp:FaultCode19 = p_web.RestoreValue('tmp:FaultCode19')
    do ValidateValue::tmp:FaultCode19
    If tmp:FaultCode19:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode19') = 1)
  ! --- STRING --- tmp:FaultCode19
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode19') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode19'',''jobfaultcodes_tmp:faultcode19_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode19',p_web.GetSessionValue('tmp:FaultCode19'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode19'),loc:javascript,,,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(19)
  End
  p_web.DivFooter()
Comment::tmp:FaultCode19  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:FaultCode19:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode19'))
  loc:class = Choose(p_web.GSV('Hide:JobFaultCode19') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode19') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:JobFaultCode19') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:FaultCode20  Routine
  packet = clip(packet) & p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode20') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode20') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:JobFaultCode20') = 1,'',p_web.Translate(p_web.GSV('Prompt:JobFaultCode20')))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:FaultCode20  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:FaultCode20 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = p_web.GSV('Picture:JobFaultCode20')  !FieldPicture = 
    tmp:FaultCode20 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode20'))
  End
  do ValidateValue::tmp:FaultCode20  ! copies value to session value if valid.
  do Value::tmp:FaultCode20
  do SendAlert
  do Comment::tmp:FaultCode20 ! allows comment style to be updated.

ValidateValue::tmp:FaultCode20  Routine
    If not (p_web.GSV('Hide:JobFaultCode20') = 1)
  If tmp:FaultCode20 = '' and p_web.GSV('Req:JobFaultCode20') = 1
    loc:Invalid = 'tmp:FaultCode20'
    tmp:FaultCode20:IsInvalid = true
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode20')) & ' ' & p_web.site.RequiredText
  End
      if loc:invalid = '' then p_web.SetSessionValue('tmp:FaultCode20',tmp:FaultCode20).
    End

Value::tmp:FaultCode20  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:JobFaultCode20') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode20') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If p_web.GSV('ReadOnly:JobFaultCode20') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If (p_web.GSV('Req:JobFaultCode20') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  End
  If loc:retrying
    tmp:FaultCode20 = p_web.RestoreValue('tmp:FaultCode20')
    do ValidateValue::tmp:FaultCode20
    If tmp:FaultCode20:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode20') = 1)
  ! --- STRING --- tmp:FaultCode20
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode20') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode20'',''jobfaultcodes_tmp:faultcode20_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode20',p_web.GetSessionValue('tmp:FaultCode20'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode20'),loc:javascript,,,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(20)
  End
  p_web.DivFooter()
Comment::tmp:FaultCode20  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:FaultCode20:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode20'))
  loc:class = Choose(p_web.GSV('Hide:JobFaultCode20') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode20') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:JobFaultCode20') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonRepairNotes  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonRepairNotes  ! copies value to session value if valid.
  do Comment::buttonRepairNotes ! allows comment style to be updated.

ValidateValue::buttonRepairNotes  Routine
    If not (1=0)
    End

Value::buttonRepairNotes  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('buttonRepairNotes') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','RepairNotes','Repair Notes',p_web.combine(Choose('Repair Notes' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'SmallButtonFixed'),loc:formname,,,p_web.OpenDialog('BrowseRepairNotes',p_web._jsok('Browse Repair Notes'),0,'''' & lower('JobFaultCodes') & '''','','',p_web._jsok('buttonRepairNotes'),p_web._jsok('')),,loc:disabled,,,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonRepairNotes  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonRepairNotes:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('buttonRepairNotes') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonOutFaults  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonOutFaults  ! copies value to session value if valid.
  do Comment::buttonOutFaults ! allows comment style to be updated.

ValidateValue::buttonOutFaults  Routine
    If not (1=0)
    End

Value::buttonOutFaults  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('buttonOutFaults') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','OutFaults','Edit Out Faults List',p_web.combine(Choose('Edit Out Faults List' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'SmallButtonFixed'),loc:formname,,,p_web.OpenDialog('DisplayOutFaults',p_web._jsok('Out Faults'),0,'''' & lower('JobFaultCodes') & '''','','',p_web._jsok('buttonOutFaults'),p_web._jsok('')),,loc:disabled,,,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonOutFaults  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonOutFaults:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('buttonOutFaults') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::promptExchange  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::promptExchange  ! copies value to session value if valid.
  do Comment::promptExchange ! allows comment style to be updated.

ValidateValue::promptExchange  Routine
    If not (p_web.GSV('Hide:ExchangePrompt') = 1)
    End

Value::promptExchange  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:ExchangePrompt') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('promptExchange') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('Hide:ExchangePrompt') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="promptExchange" class="'&clip('red bold')&'"'&clip(loc:extra)&'>' & p_web.Translate('You have requested an Exchange Unit for this repair.',) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::promptExchange  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if promptExchange:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:ExchangePrompt') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('promptExchange') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:ExchangePrompt') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:processExchange  Routine
  packet = clip(packet) & p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:processExchange') & '_prompt',Choose(p_web.GSV('Hide:ExchangePrompt') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:ExchangePrompt') = 1,'',p_web.Translate('Process Exchange Request?'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:processExchange  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:processExchange = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:processExchange = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:processExchange  ! copies value to session value if valid.
  do Value::tmp:processExchange
  do SendAlert
  do Comment::tmp:processExchange ! allows comment style to be updated.

ValidateValue::tmp:processExchange  Routine
    If not (p_web.GSV('Hide:ExchangePrompt') = 1)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:processExchange',tmp:processExchange).
    End

Value::tmp:processExchange  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:ExchangePrompt') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:processExchange') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If loc:retrying
    tmp:processExchange = p_web.RestoreValue('tmp:processExchange')
    do ValidateValue::tmp:processExchange
    If tmp:processExchange:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:ExchangePrompt') = 1)
  ! --- RADIO --- tmp:processExchange
  if p_web.GSV('job:Warranty_Job') = 'YES'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:processExchange') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:processExchange'',''jobfaultcodes_tmp:processexchange_value'',1,'''&p_web._jsok(1)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:processExchange',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:processExchange_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Add Warranty Exchange') & '<13,10>'
    packet = clip(packet) & p_web.br
  end
  if p_web.GSV('job:Chargeable_Job') = 'YES'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:processExchange') = 2
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:processExchange'',''jobfaultcodes_tmp:processexchange_value'',1,'''&p_web._jsok(2)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:processExchange',clip(2),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:processExchange_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Add Chargeable Exchange') & '<13,10>'
    packet = clip(packet) & p_web.br
  end
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:processExchange') = 3
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:processExchange'',''jobfaultcodes_tmp:processexchange_value'',1,'''&p_web._jsok(3)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:processExchange',clip(3),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:processExchange_3') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Cancel Exchange Request') & '<13,10>'
    packet = clip(packet) & p_web.br
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:processExchange  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:processExchange:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:ExchangePrompt') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:processExchange') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:ExchangePrompt') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('JobFaultCodes_nexttab_' & 0)
    tmp:MSN = p_web.GSV('tmp:MSN')
    do ValidateValue::tmp:MSN
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:MSN
      !do SendAlert
      do Comment::tmp:MSN ! allows comment style to be updated.
      !exit
    End
    tmp:VerifyMSN = p_web.GSV('tmp:VerifyMSN')
    do ValidateValue::tmp:VerifyMSN
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:VerifyMSN
      !do SendAlert
      do Comment::tmp:VerifyMSN ! allows comment style to be updated.
      !exit
    End
    tmp:ConfirmMSNChange = p_web.GSV('tmp:ConfirmMSNChange')
    do ValidateValue::tmp:ConfirmMSNChange
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:ConfirmMSNChange
      !do SendAlert
      do Comment::tmp:ConfirmMSNChange ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('JobFaultCodes_nexttab_' & 1)
    job:ProductCode = p_web.GSV('job:ProductCode')
    do ValidateValue::job:ProductCode
    If loc:Invalid
      loc:retrying = 1
      do Value::job:ProductCode
      !do SendAlert
      do Comment::job:ProductCode ! allows comment style to be updated.
      !exit
    End
    tmp:FaultCode1 = p_web.GSV('tmp:FaultCode1')
    do ValidateValue::tmp:FaultCode1
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:FaultCode1
      !do SendAlert
      do Comment::tmp:FaultCode1 ! allows comment style to be updated.
      !exit
    End
    tmp:FaultCode2 = p_web.GSV('tmp:FaultCode2')
    do ValidateValue::tmp:FaultCode2
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:FaultCode2
      !do SendAlert
      do Comment::tmp:FaultCode2 ! allows comment style to be updated.
      !exit
    End
    tmp:FaultCode3 = p_web.GSV('tmp:FaultCode3')
    do ValidateValue::tmp:FaultCode3
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:FaultCode3
      !do SendAlert
      do Comment::tmp:FaultCode3 ! allows comment style to be updated.
      !exit
    End
    tmp:FaultCode4 = p_web.GSV('tmp:FaultCode4')
    do ValidateValue::tmp:FaultCode4
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:FaultCode4
      !do SendAlert
      do Comment::tmp:FaultCode4 ! allows comment style to be updated.
      !exit
    End
    tmp:FaultCode5 = p_web.GSV('tmp:FaultCode5')
    do ValidateValue::tmp:FaultCode5
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:FaultCode5
      !do SendAlert
      do Comment::tmp:FaultCode5 ! allows comment style to be updated.
      !exit
    End
    tmp:FaultCode6 = p_web.GSV('tmp:FaultCode6')
    do ValidateValue::tmp:FaultCode6
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:FaultCode6
      !do SendAlert
      do Comment::tmp:FaultCode6 ! allows comment style to be updated.
      !exit
    End
    tmp:FaultCode7 = p_web.GSV('tmp:FaultCode7')
    do ValidateValue::tmp:FaultCode7
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:FaultCode7
      !do SendAlert
      do Comment::tmp:FaultCode7 ! allows comment style to be updated.
      !exit
    End
    tmp:FaultCode8 = p_web.GSV('tmp:FaultCode8')
    do ValidateValue::tmp:FaultCode8
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:FaultCode8
      !do SendAlert
      do Comment::tmp:FaultCode8 ! allows comment style to be updated.
      !exit
    End
    tmp:FaultCode9 = p_web.GSV('tmp:FaultCode9')
    do ValidateValue::tmp:FaultCode9
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:FaultCode9
      !do SendAlert
      do Comment::tmp:FaultCode9 ! allows comment style to be updated.
      !exit
    End
    tmp:FaultCode10 = p_web.GSV('tmp:FaultCode10')
    do ValidateValue::tmp:FaultCode10
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:FaultCode10
      !do SendAlert
      do Comment::tmp:FaultCode10 ! allows comment style to be updated.
      !exit
    End
    tmp:FaultCode11 = p_web.GSV('tmp:FaultCode11')
    do ValidateValue::tmp:FaultCode11
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:FaultCode11
      !do SendAlert
      do Comment::tmp:FaultCode11 ! allows comment style to be updated.
      !exit
    End
    tmp:FaultCode12 = p_web.GSV('tmp:FaultCode12')
    do ValidateValue::tmp:FaultCode12
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:FaultCode12
      !do SendAlert
      do Comment::tmp:FaultCode12 ! allows comment style to be updated.
      !exit
    End
    tmp:FaultCode13 = p_web.GSV('tmp:FaultCode13')
    do ValidateValue::tmp:FaultCode13
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:FaultCode13
      !do SendAlert
      do Comment::tmp:FaultCode13 ! allows comment style to be updated.
      !exit
    End
    tmp:FaultCode14 = p_web.GSV('tmp:FaultCode14')
    do ValidateValue::tmp:FaultCode14
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:FaultCode14
      !do SendAlert
      do Comment::tmp:FaultCode14 ! allows comment style to be updated.
      !exit
    End
    tmp:FaultCode15 = p_web.GSV('tmp:FaultCode15')
    do ValidateValue::tmp:FaultCode15
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:FaultCode15
      !do SendAlert
      do Comment::tmp:FaultCode15 ! allows comment style to be updated.
      !exit
    End
    tmp:FaultCode16 = p_web.GSV('tmp:FaultCode16')
    do ValidateValue::tmp:FaultCode16
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:FaultCode16
      !do SendAlert
      do Comment::tmp:FaultCode16 ! allows comment style to be updated.
      !exit
    End
    tmp:FaultCode17 = p_web.GSV('tmp:FaultCode17')
    do ValidateValue::tmp:FaultCode17
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:FaultCode17
      !do SendAlert
      do Comment::tmp:FaultCode17 ! allows comment style to be updated.
      !exit
    End
    tmp:FaultCode18 = p_web.GSV('tmp:FaultCode18')
    do ValidateValue::tmp:FaultCode18
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:FaultCode18
      !do SendAlert
      do Comment::tmp:FaultCode18 ! allows comment style to be updated.
      !exit
    End
    tmp:FaultCode19 = p_web.GSV('tmp:FaultCode19')
    do ValidateValue::tmp:FaultCode19
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:FaultCode19
      !do SendAlert
      do Comment::tmp:FaultCode19 ! allows comment style to be updated.
      !exit
    End
    tmp:FaultCode20 = p_web.GSV('tmp:FaultCode20')
    do ValidateValue::tmp:FaultCode20
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:FaultCode20
      !do SendAlert
      do Comment::tmp:FaultCode20 ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('JobFaultCodes_nexttab_' & 2)
    If loc:Invalid then exit.
  of lower('JobFaultCodes_nexttab_' & 3)
    tmp:processExchange = p_web.GSV('tmp:processExchange')
    do ValidateValue::tmp:processExchange
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:processExchange
      !do SendAlert
      do Comment::tmp:processExchange ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_JobFaultCodes_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('JobFaultCodes_tab_' & 0)
    do GenerateTab0
  of lower('JobFaultCodes_tmp:MSN_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:MSN
      of event:timer
        do Value::tmp:MSN
        do Comment::tmp:MSN
      else
        do Value::tmp:MSN
      end
  of lower('JobFaultCodes_buttonVerifyMSN_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonVerifyMSN
      of event:timer
        do Value::buttonVerifyMSN
        do Comment::buttonVerifyMSN
      else
        do Value::buttonVerifyMSN
      end
  of lower('JobFaultCodes_tmp:VerifyMSN_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:VerifyMSN
      of event:timer
        do Value::tmp:VerifyMSN
        do Comment::tmp:VerifyMSN
      else
        do Value::tmp:VerifyMSN
      end
  of lower('JobFaultCodes_buttonVerifyMSN2_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonVerifyMSN2
      of event:timer
        do Value::buttonVerifyMSN2
        do Comment::buttonVerifyMSN2
      else
        do Value::buttonVerifyMSN2
      end
  of lower('JobFaultCodes_tmp:ConfirmMSNChange_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ConfirmMSNChange
      of event:timer
        do Value::tmp:ConfirmMSNChange
        do Comment::tmp:ConfirmMSNChange
      else
        do Value::tmp:ConfirmMSNChange
      end
  of lower('JobFaultCodes_tab_' & 1)
    do GenerateTab1
  of lower('JobFaultCodes_job:ProductCode_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:ProductCode
      of event:timer
        do Value::job:ProductCode
        do Comment::job:ProductCode
      else
        do Value::job:ProductCode
      end
  of lower('JobFaultCodes_tmp:FaultCode1_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode1
      of event:timer
        do Value::tmp:FaultCode1
        do Comment::tmp:FaultCode1
      else
        do Value::tmp:FaultCode1
      end
  of lower('JobFaultCodes_tmp:FaultCode2_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode2
      of event:timer
        do Value::tmp:FaultCode2
        do Comment::tmp:FaultCode2
      else
        do Value::tmp:FaultCode2
      end
  of lower('JobFaultCodes_tmp:FaultCode3_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode3
      of event:timer
        do Value::tmp:FaultCode3
        do Comment::tmp:FaultCode3
      else
        do Value::tmp:FaultCode3
      end
  of lower('JobFaultCodes_tmp:FaultCode4_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode4
      of event:timer
        do Value::tmp:FaultCode4
        do Comment::tmp:FaultCode4
      else
        do Value::tmp:FaultCode4
      end
  of lower('JobFaultCodes_tmp:FaultCode5_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode5
      of event:timer
        do Value::tmp:FaultCode5
        do Comment::tmp:FaultCode5
      else
        do Value::tmp:FaultCode5
      end
  of lower('JobFaultCodes_tmp:FaultCode6_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode6
      of event:timer
        do Value::tmp:FaultCode6
        do Comment::tmp:FaultCode6
      else
        do Value::tmp:FaultCode6
      end
  of lower('JobFaultCodes_tmp:FaultCode7_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode7
      of event:timer
        do Value::tmp:FaultCode7
        do Comment::tmp:FaultCode7
      else
        do Value::tmp:FaultCode7
      end
  of lower('JobFaultCodes_tmp:FaultCode8_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode8
      of event:timer
        do Value::tmp:FaultCode8
        do Comment::tmp:FaultCode8
      else
        do Value::tmp:FaultCode8
      end
  of lower('JobFaultCodes_tmp:FaultCode9_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode9
      of event:timer
        do Value::tmp:FaultCode9
        do Comment::tmp:FaultCode9
      else
        do Value::tmp:FaultCode9
      end
  of lower('JobFaultCodes_tmp:FaultCode10_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode10
      of event:timer
        do Value::tmp:FaultCode10
        do Comment::tmp:FaultCode10
      else
        do Value::tmp:FaultCode10
      end
  of lower('JobFaultCodes_tmp:FaultCode11_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode11
      of event:timer
        do Value::tmp:FaultCode11
        do Comment::tmp:FaultCode11
      else
        do Value::tmp:FaultCode11
      end
  of lower('JobFaultCodes_tmp:FaultCode12_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode12
      of event:timer
        do Value::tmp:FaultCode12
        do Comment::tmp:FaultCode12
      else
        do Value::tmp:FaultCode12
      end
  of lower('JobFaultCodes_tmp:FaultCode13_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode13
      of event:timer
        do Value::tmp:FaultCode13
        do Comment::tmp:FaultCode13
      else
        do Value::tmp:FaultCode13
      end
  of lower('JobFaultCodes_tmp:FaultCode14_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode14
      of event:timer
        do Value::tmp:FaultCode14
        do Comment::tmp:FaultCode14
      else
        do Value::tmp:FaultCode14
      end
  of lower('JobFaultCodes_tmp:FaultCode15_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode15
      of event:timer
        do Value::tmp:FaultCode15
        do Comment::tmp:FaultCode15
      else
        do Value::tmp:FaultCode15
      end
  of lower('JobFaultCodes_tmp:FaultCode16_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode16
      of event:timer
        do Value::tmp:FaultCode16
        do Comment::tmp:FaultCode16
      else
        do Value::tmp:FaultCode16
      end
  of lower('JobFaultCodes_tmp:FaultCode17_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode17
      of event:timer
        do Value::tmp:FaultCode17
        do Comment::tmp:FaultCode17
      else
        do Value::tmp:FaultCode17
      end
  of lower('JobFaultCodes_tmp:FaultCode18_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode18
      of event:timer
        do Value::tmp:FaultCode18
        do Comment::tmp:FaultCode18
      else
        do Value::tmp:FaultCode18
      end
  of lower('JobFaultCodes_tmp:FaultCode19_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode19
      of event:timer
        do Value::tmp:FaultCode19
        do Comment::tmp:FaultCode19
      else
        do Value::tmp:FaultCode19
      end
  of lower('JobFaultCodes_tmp:FaultCode20_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode20
      of event:timer
        do Value::tmp:FaultCode20
        do Comment::tmp:FaultCode20
      else
        do Value::tmp:FaultCode20
      end
  of lower('JobFaultCodes_tab_' & 2)
    do GenerateTab2
  of lower('JobFaultCodes_tab_' & 3)
    do GenerateTab3
  of lower('JobFaultCodes_tmp:processExchange_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:processExchange
      of event:timer
        do Value::tmp:processExchange
        do Comment::tmp:processExchange
      else
        do Value::tmp:processExchange
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('JobFaultCodes_form:ready_',1)

  p_web.SetSessionValue('JobFaultCodes_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_JobFaultCodes',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('JobFaultCodes_form:ready_',1)
  p_web.SetSessionValue('JobFaultCodes_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_JobFaultCodes',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('JobFaultCodes_form:ready_',1)
  p_web.SetSessionValue('JobFaultCodes_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('JobFaultCodes:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('JobFaultCodes_form:ready_',1)
  p_web.SetSessionValue('JobFaultCodes_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('JobFaultCodes:Primed',0)
  p_web.setsessionvalue('showtab_JobFaultCodes',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  ! GetDescription resets the 'NewValue' to the code, if the description is found. Returns 1 if code valid.
  loc:ok = p_web.GetDescription(MODPROD,mop:ProductCodeKey,mop:ProductCodeKey,mop:ProductCode,,p_web.GetSessionValue('job:ProductCode')) !2
  if loc:ok then p_web.FileToSessionQueue(MODPROD).
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
  If p_web.GSV('MSNValidation') = 1
      If not (1=0)
        If not (p_web.GSV('MSNValidated') = 1)
          If p_web.IfExistsValue('tmp:MSN')
            tmp:MSN = p_web.GetValue('tmp:MSN')
          End
        End
      End
      If not (p_web.GSV('Hide:VerifyMSN') = 1 or p_web.GSV('MSNValidated') = 1)
          If p_web.IfExistsValue('tmp:VerifyMSN')
            tmp:VerifyMSN = p_web.GetValue('tmp:VerifyMSN')
          End
      End
      If not (p_web.GSV('Hide:ConfirmMSNChange') =1 OR p_web.GSV('MSNValidated') = 1)
          If p_web.IfExistsValue('tmp:ConfirmMSNChange')
            tmp:ConfirmMSNChange = p_web.GetValue('tmp:ConfirmMSNChange')
          End
      End
  End
      If not (p_web.GSV('Hide:ProductCode') = 1)
        If not (p_web.GSV('ReadOnly:ProductCode') = 1)
          If p_web.IfExistsValue('job:ProductCode')
            job:ProductCode = p_web.GetValue('job:ProductCode')
          End
        End
      End
      If not (p_web.GSV('Hide:JobFaultCode1') = 1)
        If not (p_web.GSV('ReadOnly:JobFaultCode1') = 1)
          If p_web.IfExistsValue('tmp:FaultCode1')
            tmp:FaultCode1 = p_web.GetValue('tmp:FaultCode1')
          End
        End
      End
      If not (p_web.GSV('Hide:JobFaultCode2') = 1)
        If not (p_web.GSV('ReadOnly:JobFaultCode2') = 1)
          If p_web.IfExistsValue('tmp:FaultCode2')
            tmp:FaultCode2 = p_web.GetValue('tmp:FaultCode2')
          End
        End
      End
      If not (p_web.GSV('Hide:JobFaultCode3') = 1)
        If not (p_web.GSV('ReadOnly:JobFaultCode3') = 1)
          If p_web.IfExistsValue('tmp:FaultCode3')
            tmp:FaultCode3 = p_web.GetValue('tmp:FaultCode3')
          End
        End
      End
      If not (p_web.GSV('Hide:JobFaultCode4') = 1)
        If not (p_web.GSV('ReadOnly:JobFaultCode4') = 1)
          If p_web.IfExistsValue('tmp:FaultCode4')
            tmp:FaultCode4 = p_web.GetValue('tmp:FaultCode4')
          End
        End
      End
      If not (p_web.GSV('Hide:JobFaultCode5') = 1)
        If not (p_web.GSV('ReadOnly:JobFaultCode5') = 1)
          If p_web.IfExistsValue('tmp:FaultCode5')
            tmp:FaultCode5 = p_web.GetValue('tmp:FaultCode5')
          End
        End
      End
      If not (p_web.GSV('Hide:JobFaultCode6') = 1)
        If not (p_web.GSV('ReadOnly:JobFaultCode6') = 1)
          If p_web.IfExistsValue('tmp:FaultCode6')
            tmp:FaultCode6 = p_web.GetValue('tmp:FaultCode6')
          End
        End
      End
      If not (p_web.GSV('Hide:JobFaultCode7') = 1)
        If not (p_web.GSV('ReadOnly:JobFaultCode7') = 1)
          If p_web.IfExistsValue('tmp:FaultCode7')
            tmp:FaultCode7 = p_web.GetValue('tmp:FaultCode7')
          End
        End
      End
      If not (p_web.GSV('Hide:JobFaultCode8') = 1)
        If not (p_web.GSV('ReadOnly:JobFaultCode8') = 1)
          If p_web.IfExistsValue('tmp:FaultCode8')
            tmp:FaultCode8 = p_web.GetValue('tmp:FaultCode8')
          End
        End
      End
      If not (p_web.GSV('Hide:JobFaultCode9') = 1)
        If not (p_web.GSV('ReadOnly:JobFaultCode9') = 1)
          If p_web.IfExistsValue('tmp:FaultCode9')
            tmp:FaultCode9 = p_web.GetValue('tmp:FaultCode9')
          End
        End
      End
      If not (p_web.GSV('Hide:JobFaultCode10') = 1)
        If not (p_web.GSV('ReadOnly:JobFaultCode10') = 1)
          If p_web.IfExistsValue('tmp:FaultCode10')
            tmp:FaultCode10 = p_web.GetValue('tmp:FaultCode10')
          End
        End
      End
      If not (p_web.GSV('Hide:JobFaultCode11') = 1)
        If not (p_web.GSV('ReadOnly:JobFaultCode11') = 1)
          If p_web.IfExistsValue('tmp:FaultCode11')
            tmp:FaultCode11 = p_web.GetValue('tmp:FaultCode11')
          End
        End
      End
      If not (p_web.GSV('Hide:JobFaultCode12') = 1)
        If not (p_web.GSV('ReadOnly:JobFaultCode12') = 1)
          If p_web.IfExistsValue('tmp:FaultCode12')
            tmp:FaultCode12 = p_web.GetValue('tmp:FaultCode12')
          End
        End
      End
      If not (p_web.GSV('Hide:JobFaultCode13') = 1)
        If not (p_web.GSV('ReadOnly:JobFaultCode13') = 1)
          If p_web.IfExistsValue('tmp:FaultCode13')
            tmp:FaultCode13 = p_web.GetValue('tmp:FaultCode13')
          End
        End
      End
      If not (p_web.GSV('Hide:JobFaultCode14') = 1)
        If not (p_web.GSV('ReadOnly:JobFaultCode14') = 1)
          If p_web.IfExistsValue('tmp:FaultCode14')
            tmp:FaultCode14 = p_web.GetValue('tmp:FaultCode14')
          End
        End
      End
      If not (p_web.GSV('Hide:JobFaultCode15') = 1)
        If not (p_web.GSV('ReadOnly:JobFaultCode15') = 1)
          If p_web.IfExistsValue('tmp:FaultCode15')
            tmp:FaultCode15 = p_web.GetValue('tmp:FaultCode15')
          End
        End
      End
      If not (p_web.GSV('Hide:JobFaultCode16') = 1)
        If not (p_web.GSV('ReadOnly:JobFaultCode16') = 1)
          If p_web.IfExistsValue('tmp:FaultCode16')
            tmp:FaultCode16 = p_web.GetValue('tmp:FaultCode16')
          End
        End
      End
      If not (p_web.GSV('Hide:JobFaultCode17') = 1)
        If not (p_web.GSV('ReadOnly:JobFaultCode17') = 1)
          If p_web.IfExistsValue('tmp:FaultCode17')
            tmp:FaultCode17 = p_web.GetValue('tmp:FaultCode17')
          End
        End
      End
      If not (p_web.GSV('Hide:JobFaultCode18') = 1)
        If not (p_web.GSV('ReadOnly:JobFaultCode18') = 1)
          If p_web.IfExistsValue('tmp:FaultCode18')
            tmp:FaultCode18 = p_web.GetValue('tmp:FaultCode18')
          End
        End
      End
      If not (p_web.GSV('Hide:JobFaultCode19') = 1)
        If not (p_web.GSV('ReadOnly:JobFaultCode19') = 1)
          If p_web.IfExistsValue('tmp:FaultCode19')
            tmp:FaultCode19 = p_web.GetValue('tmp:FaultCode19')
          End
        End
      End
      If not (p_web.GSV('Hide:JobFaultCode20') = 1)
        If not (p_web.GSV('ReadOnly:JobFaultCode20') = 1)
          If p_web.IfExistsValue('tmp:FaultCode20')
            tmp:FaultCode20 = p_web.GetValue('tmp:FaultCode20')
          End
        End
      End
      If not (p_web.GSV('Hide:ExchangePrompt') = 1)
          If p_web.IfExistsValue('tmp:processExchange')
            tmp:processExchange = p_web.GetValue('tmp:processExchange')
          End
      End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('JobFaultCodes_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
      if (p_web.GSV('MSNValidation') = 1 And p_web.GSV('MSNValidated') = 0)
          loc:alert = 'You must Verify the MSN'
          loc:invalid = 'tmp:MSN'
          exit
      end ! if (p_web.GSV('MSNValidation') = 1 And p_web.GSV('MSNValidated') = 0)
  
      ! Double Check Fault Code Values
  
      loop x# = 1 to 20
          if (p_web.GSV('Hide:JobFaultCode' & x#) = 1 or |
              p_web.GSV('ReadOnly:JobFaultCode' & x#) = 1 or |
              p_web.GSV('Lookup:JobFaultCode' & x#) <> 1 or |
              p_web.GSV('tmp:FaultCode' & x#) = '')
              cycle
          end ! p_web.GSV('ReadOnly:JobFaultCode' & x#) = 1)
  
  
          Access:MANFAULT.Clearkey(maf:ScreenOrderKey)
          maf:ScreenOrder    = x#
          maf:Manufacturer    = p_web.GSV('job:Manufacturer')
          if (Access:MANFAULT.TryFetch(maf:ScreenOrderKey) = Level:Benign)
              ! Found
              if (maf:Lookup = 'YES' and maf:Force_Lookup = 'YES' and maf:MainFault <> 1)
                  Access:MANFAULO.Clearkey(mfo:HideFieldKey)
                  mfo:NotAvailable    = 0
                  mfo:Manufacturer    = p_web.GSV('job:Manufacturer')
                  mfo:Field_Number    = maf:Field_Number
                  mfo:Field    = p_web.GSV('tmp:FaultCode' & x#)
                  if (Access:MANFAULO.TryFetch(mfo:HideFieldKey) = Level:Benign)
                      ! Found
                  else ! if (Access:MANFAULO.TryFetch(mfo:HideFieldKey) = Level:Benign)
                      ! Error
                      loc:Invalid = 'tmp:FaultCode' & x#
                      loc:Alert = 'Invalid Fault Code: ' & p_web.GSV('Prompt:JobFaultCode' & x#)
                      break
                  end ! if (Access:MANFAULO.TryFetch(mfo:HideFieldKey) = Level:Benign)
              end ! if (maf:Lookup = 'YES' and maf:Force_Lookup = 'YES')
          else ! if (Access:MANFAULT.TryFetch(maf:ScreenOrderKey) = Level:Benign)
              ! Error
          end ! if (Access:MANFAULT.TryFetch(maf:ScreenOrderKey) = Level:Benign)
      end ! loop x# = 1 to 20
  
      if (loc:Invalid = '' And p_web.GSV('Hide:ExchangePrompt') = 0 And p_web.GSV('tmp:ProcessExchange') < 1)
          loc:alert = 'Please select how you wish to process the Exchange Request'
          loc:invalid = 'tmp:promptExchange'
          exit
      end ! if (loc:Invalid = '' And p_web.GSV('Hide:ExchangePrompt') = 0 And p_web.GSV('tmp:ProcessExchange') = 0)
  
  
  p_web.DeleteSessionValue('JobFaultCodes_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
  If p_web.GSV('MSNValidation') = 1
    loc:InvalidTab += 1
    do ValidateValue::tmp:MSN
    If loc:Invalid then exit.
    do ValidateValue::buttonVerifyMSN
    If loc:Invalid then exit.
    do ValidateValue::tmp:VerifyMSN
    If loc:Invalid then exit.
    do ValidateValue::buttonVerifyMSN2
    If loc:Invalid then exit.
    do ValidateValue::tmp:ConfirmMSNChange
    If loc:Invalid then exit.
  End
  ! tab = 2
    loc:InvalidTab += 1
    do ValidateValue::job:ProductCode
    If loc:Invalid then exit.
    do ValidateValue::tmp:FaultCode1
    If loc:Invalid then exit.
    do ValidateValue::tmp:FaultCode2
    If loc:Invalid then exit.
    do ValidateValue::tmp:FaultCode3
    If loc:Invalid then exit.
    do ValidateValue::tmp:FaultCode4
    If loc:Invalid then exit.
    do ValidateValue::tmp:FaultCode5
    If loc:Invalid then exit.
    do ValidateValue::tmp:FaultCode6
    If loc:Invalid then exit.
    do ValidateValue::tmp:FaultCode7
    If loc:Invalid then exit.
    do ValidateValue::tmp:FaultCode8
    If loc:Invalid then exit.
    do ValidateValue::tmp:FaultCode9
    If loc:Invalid then exit.
    do ValidateValue::tmp:FaultCode10
    If loc:Invalid then exit.
    do ValidateValue::tmp:FaultCode11
    If loc:Invalid then exit.
    do ValidateValue::tmp:FaultCode12
    If loc:Invalid then exit.
    do ValidateValue::tmp:FaultCode13
    If loc:Invalid then exit.
    do ValidateValue::tmp:FaultCode14
    If loc:Invalid then exit.
    do ValidateValue::tmp:FaultCode15
    If loc:Invalid then exit.
    do ValidateValue::tmp:FaultCode16
    If loc:Invalid then exit.
    do ValidateValue::tmp:FaultCode17
    If loc:Invalid then exit.
    do ValidateValue::tmp:FaultCode18
    If loc:Invalid then exit.
    do ValidateValue::tmp:FaultCode19
    If loc:Invalid then exit.
    do ValidateValue::tmp:FaultCode20
    If loc:Invalid then exit.
  ! tab = 3
    loc:InvalidTab += 1
    do ValidateValue::buttonRepairNotes
    If loc:Invalid then exit.
    do ValidateValue::buttonOutFaults
    If loc:Invalid then exit.
  ! tab = 4
    loc:InvalidTab += 1
    do ValidateValue::promptExchange
    If loc:Invalid then exit.
    do ValidateValue::tmp:processExchange
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
      !Write Back The Fault Codes
      loop x# = 1 to 20
          Access:MANFAULT.Clearkey(maf:ScreenOrderKey)
          maf:Manufacturer = p_web.GSV('job:Manufacturer')
          maf:ScreenOrder    = x#
          if (Access:MANFAULT.TryFetch(maf:ScreenOrderKey) = Level:Benign)
              ! Found
              if (p_web.GSV('Hide:JobFaultCode' & x#) <> 1 and p_web.GSV('ReadOnly:JobFaultCode' & x#) <> 1)
                  if (maf:Field_Number < 13)
                      p_web.SSV('job:Fault_Code' & maf:Field_Number,p_web.GSV('tmp:FaultCode' & x#))
                  else ! if (maf:Field_Number < 13)
                      p_web.SSV('wob:FaultCode' & maf:Field_Number,p_web.GSV('tmp:FaultCode' & x#))
                  end ! if (maf:Field_Number < 13)
              end !if (p_web.GSV('Hide:JobFaultCode') <> 1)
          else ! if (Access:MANFAULT.TryFetch(maf:ScreenOrderKey) = Level:Benign)
              ! Error
          end ! if (Access:MANFAULT.TryFetch(maf:ScreenOrderKey) = Level:Benign)
      end !loop x# = 1 to 20
  
      do ValidateFaultCodes
  
      IF (p_web.GSV('Job:ViewOnly') = 1)
          ! Don't call POP screen if view only job.
          p_web.SSV('locNextURL','BillingConfirmation')
      END
  
  
      if (p_web.GSV('Hide:ExchangePrompt') = 0)
          if (p_web.GSV('tmp:processExchange') = 1)
              local.AllocateExchangePart('CHA',0,0)
          end
          if (p_web.GSV('tmp:processExchange') = 2)
              local.AllocateExchangePart('CHA',0,0)
          end
      end
  
  
      p_web.deletesessionvalue('JobFaultCodes:FirstTime')
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('JobFaultCodes:Primed',0)
  p_web.StoreValue('tmp:MSN')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:VerifyMSN')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:ConfirmMSNChange')
  p_web.StoreValue('job:ProductCode')
  p_web.StoreValue('tmp:FaultCode1')
  p_web.StoreValue('tmp:FaultCode2')
  p_web.StoreValue('tmp:FaultCode3')
  p_web.StoreValue('tmp:FaultCode4')
  p_web.StoreValue('tmp:FaultCode5')
  p_web.StoreValue('tmp:FaultCode6')
  p_web.StoreValue('tmp:FaultCode7')
  p_web.StoreValue('tmp:FaultCode8')
  p_web.StoreValue('tmp:FaultCode9')
  p_web.StoreValue('tmp:FaultCode10')
  p_web.StoreValue('tmp:FaultCode11')
  p_web.StoreValue('tmp:FaultCode12')
  p_web.StoreValue('tmp:FaultCode13')
  p_web.StoreValue('tmp:FaultCode14')
  p_web.StoreValue('tmp:FaultCode15')
  p_web.StoreValue('tmp:FaultCode16')
  p_web.StoreValue('tmp:FaultCode17')
  p_web.StoreValue('tmp:FaultCode18')
  p_web.StoreValue('tmp:FaultCode19')
  p_web.StoreValue('tmp:FaultCode20')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:processExchange')

local.AfterFaultCodeLookup        Procedure(Long fNumber)
code
    p_web.setsessionvalue('showtab_JobFaultCodes',Loc:TabNumber)
    if loc:LookupDone
        ! Prompt For Exchange
        p_web.SSV('Hide:ExchangePrompt',1)
        loop x# = 1 to 20
            if (p_web.GSV('Hide:JobFaultCode' & x#) = 1)
                cycle
            end ! if (p_web.GSV('Hide:JobFaultCode' & x#) = 1)

            Access:MANFAULT.Clearkey(maf:screenOrderKey)
            maf:Manufacturer    = p_web.GSV('job:manufacturer')
            maf:screenOrder    = x#
            if (Access:MANFAULT.TryFetch(maf:screenOrderKey) = Level:Benign)
                ! Found
                Access:MANFAULO.Clearkey(mfo:field_Key)
                mfo:manufacturer    = p_web.GSV('job:manufacturer')
                mfo:field_Number    = maf:field_number
                mfo:field    = p_web.GSV('tmp:FaultCode' & x#)
                if (Access:MANFAULO.TryFetch(mfo:field_Key) = Level:Benign)
                    ! Found
                    if (mfo:promptForExchange)
                        p_web.SSV('Hide:ExchangePrompt',0)
                        break
                    end ! if (mfo:promptForExchange)
                else ! if (Access:MANFAULO.TryFetch(mfo:field_Key) = Level:Benign)
                    ! Error
                end ! if (Access:MANFAULO.TryFetch(mfo:field_Key) = Level:Benign)

            else ! if (Access:MANFAULT.TryFetch(maf:screenOrderKey) = Level:Benign)
                ! Error
            end ! if (Access:MANFAULT.TryFetch(maf:screenOrderKey) = Level:Benign)

        end ! loop x# = 1 to 20
!        do buildFaultCodes
!        do UpdateComments
    end
    p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:FaultCode' & fNumber)
Local.AllocateExchangePart      Procedure(String    func:Type,Byte  func:Allocated,Byte func:SecondUnit)
local:SecondExchangeUnit        Byte(0)
local:ExchangePartAttached      Byte(0)
local:AttachUnit                Byte(0)
local:AttachSecondUnit          Byte(0)
local:FoundExchange             Byte(0)
Code
    Access:USERS.Clearkey(use:user_code_key)
    use:user_code    = p_web.GSV('job:engineer')
    if (Access:USERS.TryFetch(use:user_code_key) = Level:Benign)
        ! Found
    else ! if (Access:USERS.TryFetch(use:user_code_key) = Level:Benign)
        ! Error
    end ! if (Access:USERS.TryFetch(use:user_code_key) = Level:Benign)


    Access:STOCK.Clearkey(sto:location_key)
    sto:location    = use:location
    sto:part_number    = 'EXCH'
    if (Access:STOCK.TryFetch(sto:location_key) = Level:Benign)
        ! Found
    else ! if (Access:STOCK.TryFetch(sto:location_key) = Level:Benign)
        ! Error
    end ! if (Access:STOCK.TryFetch(sto:location_key) = Level:Benign)

    Case func:Type
        Of 'WAR'
            !Check to see if an Exchange Part line already exists -  (DBH: 19-12-2003)
            Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
            wpr:Ref_Number  = p_web.GSV('job:ref_number')
            wpr:Part_Number = 'EXCH'
            Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
            Loop
                If Access:WARPARTS.NEXT()
                   Break
                End !If
                If wpr:Ref_Number  <> p_web.GSV('job:ref_number')      |
                Or wpr:Part_Number <> 'EXCH'      |
                    Then Break.  ! End If
                If func:SecondUnit = True
                    If wpr:SecondExchangeUnit
                        Return
                    End !If func:SecondUnit And wpr:SecondExchangeUnit
                Else !If func:SecondUnit = True
                    Return
                End !If func:SecondUnit = True
            End !Loop

            If Access:WARPARTS.PrimeRecord() = Level:Benign
                wpr:Part_Ref_Number = sto:Ref_Number
                wpr:Ref_Number      = p_web.GSV('job:ref_number')
                wpr:Adjustment      = 'YES'
                wpr:Part_Number     = 'EXCH'
                wpr:Description     = Clip(p_web.GSV('job:manufacturer')) & ' EXCHANGE UNIT'
                wpr:Quantity        = 1
                wpr:Warranty_Part   = 'NO'
                wpr:Exclude_From_Order = 'YES'
                wpr:PartAllocated   = func:Allocated
                If func:Allocated = 0
                    wpr:Status  = 'REQ'
                Else !If func:Allocated = 0
                    wpr:Status  = 'PIK'
                End !If func:Allocated = 0
                wpr:ExchangeUnit        = True
                wpr:SecondExchangeUnit = func:SecondUnit

                If sto:Assign_Fault_Codes = 'YES'
                    !Try and get the fault codes. This key should get the only record
                    Access:STOMODEL.ClearKey(stm:Ref_Part_Description)
                    stm:Ref_Number  = sto:Ref_Number
                    stm:Part_Number = sto:Part_Number
                    stm:Location    = sto:Location
                    stm:Description = sto:Description
                    If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                        !Found
                        wpr:Fault_Code1  = stm:FaultCode1
                        wpr:Fault_Code2  = stm:FaultCode2
                        wpr:Fault_Code3  = stm:FaultCode3
                        wpr:Fault_Code4  = stm:FaultCode4
                        wpr:Fault_Code5  = stm:FaultCode5
                        wpr:Fault_Code6  = stm:FaultCode6
                        wpr:Fault_Code7  = stm:FaultCode7
                        wpr:Fault_Code8  = stm:FaultCode8
                        wpr:Fault_Code9  = stm:FaultCode9
                        wpr:Fault_Code10 = stm:FaultCode10
                        wpr:Fault_Code11 = stm:FaultCode11
                        wpr:Fault_Code12 = stm:FaultCode12
                    Else!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                    End!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign

                End !If sto:Assign_Fault_Codes = 'YES'
                If Access:WARPARTS.TryInsert() = Level:Benign
                    !Insert Successful
                           AddToStockAllocation(wpr:Record_Number,'WAR',1,'',p_web.GSV('job:Engineer'),p_web)
                Else !If Access:WARPARTS.TryInsert() = Level:Benign
                    !Insert Failed
                    Access:WARPARTS.CancelAutoInc()
                End !If Access:WARPARTS.TryInsert() = Level:Benign
            End !If Access:WARPARTS.PrimeRecord() = Level:Benign

        Of 'CHA'
            Access:PARTS.ClearKey(par:Part_Number_Key)
            par:Ref_Number  = p_web.GSV('job:ref_number')
            par:Part_Number = 'EXCH'
            Set(par:Part_Number_Key,par:Part_Number_Key)
            Loop
                If Access:PARTS.NEXT()
                   Break
                End !If
                If par:Ref_Number  <> p_web.GSV('job:ref_number')      |
                Or par:Part_Number <> 'EXCH'      |
                    Then Break.  ! End If
                If func:SecondUnit = True
                    If par:SecondExchangeUnit
                        Return
                    End !If func:SecondUnit And wpr:SecondExchangeUnit
                Else !If func:SecondUnit = True
                    Return
                End !If func:SecondUnit = True
            End !Loop

            if access:parts.primerecord() = level:benign
                par:PArt_Ref_Number      = sto:Ref_Number
                par:ref_number            = p_web.GSV('job:ref_number')
                par:adjustment            = 'YES'
                par:part_number           = 'EXCH'
                par:description           = Clip(p_web.GSV('job:manufacturer')) & ' EXCHANGE UNIT'
                par:quantity              = 1
                par:warranty_part         = 'NO'
                par:exclude_from_order    = 'YES'
                par:PartAllocated         = func:Allocated
                par:ExchangeUnit        = True
                par:SecondExchangeUnit = func:SecondUnit

                !see above for confusion
                if func:allocated = 0 then
                    par:Status            = 'REQ'
                ELSE
                    par:status            = 'PIK'
                END
                If sto:Assign_Fault_Codes = 'YES'
                   !Try and get the fault codes. This key should get the only record
                   Access:STOMODEL.ClearKey(stm:Ref_Part_Description)
                   stm:Ref_Number  = sto:Ref_Number
                   stm:Part_Number = sto:Part_Number
                   stm:Location    = sto:Location
                   stm:Description = sto:Description
                   If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                       !Found
                       par:Fault_Code1  = stm:FaultCode1
                       par:Fault_Code2  = stm:FaultCode2
                       par:Fault_Code3  = stm:FaultCode3
                       par:Fault_Code4  = stm:FaultCode4
                       par:Fault_Code5  = stm:FaultCode5
                       par:Fault_Code6  = stm:FaultCode6
                       par:Fault_Code7  = stm:FaultCode7
                       par:Fault_Code8  = stm:FaultCode8
                       par:Fault_Code9  = stm:FaultCode9
                       par:Fault_Code10 = stm:FaultCode10
                       par:Fault_Code11 = stm:FaultCode11
                       par:Fault_Code12 = stm:FaultCode12
                   Else!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                       !Error
                       !Assert(0,'<13,10>Fetch Error<13,10>')
                   End!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                End !If sto:Assign_Fault_Codes = 'YES'
                if access:parts.insert()
                    access:parts.cancelautoinc()
                end
                AddToStockAllocation(par:Record_Number,'CHA',1,'',p_web.GSV('job:Engineer'),p_web)
            End !If access:Prime
    End !Case func:Type
local.PartForceFaultCode    Procedure(Long f:PartFieldNumber,String f:FaultCode, Long f:JobFieldNumber )
Code
    If f:FaultCode = ''
        Return 0
    End ! If f:FaultCode = ''

    Access:MANFAUPA.Clearkey(map:Field_Number_Key)
    map:Manufacturer = job:Manufacturer
    map:Field_Number = f:PartFieldNumber
    If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
        If map:NotAvailable = 0
            Access:MANFPALO.Clearkey(mfp:Field_Key)
            mfp:Manufacturer = job:Manufacturer
            mfp:Field_Number = f:PartFieldNumber
            mfp:Field = f:FaultCode
            If Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign
                If mfp:ForceJobFaultCode
                    If mfp:ForceFaultCodeNumber = f:JobFieldNumber
                        Return 1
                    End ! If mfp:ForceFaultCodeNumber= maf:Field_Number
                End ! If mfp:ForceJobFaultCode
            Else ! Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign
            End ! Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign
        End ! If map:NotAvailable = 0
    Else ! If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
    End ! If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
    Return 0
local.SetLookupButton      Procedure(Long fNumber)
Code
    if (p_web.GSV('ReadOnly:JobFaultCode' & fNumber) = 1)
        return
    end ! if (p_web.GSV('ReadOnly:JobFaultCode' & fNumber) = 1)
    if (p_web.GSV('ShowDate:JobFaultCode' & fNumber) = 1)
        packet = clip(packet) & '<button id="lookup_btn" class="LookupButton" onclick="displayCalendar(tmp__FaultCode' & fNumber & ',''dd/mm/yyyy'',this); ' & |
                  'Date.disabled=false;sv(''...'',''JobFaultCodes_pickdate_value'',1,FieldValue(this,1));nextFocus(JobFaultCodes_frm,'''',0);"' & |
                  'value="Select Date" name="Date" type="button">...</button>'
        do SendPacket
    end ! if (p_web.GSV('ShowDate:PartFaultCode1') = 1)
    if (p_web.GSV('Lookup:JobFaultCode' & fNumber) = 1)

        Access:MANFAULT.Clearkey(maf:ScreenOrderKey)
        maf:Manufacturer    = p_web.GSV('job:Manufacturer')
        maf:ScreenOrder    = fNumber
        if (Access:MANFAULT.TryFetch(maf:ScreenOrderKey) = Level:Benign)
            ! Found
        else ! if (Access:MANFAULT.TryFetch(maf:ScreenOrderKey) = Level:Benign)
            ! Error
        end ! if (Access:MANFAULT.TryFetch(maf:ScreenOrderKey) = Level:Benign)

        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('BrowseJobFaultCodeLookup')&|
                    '?LookupField=tmp:FaultCode' & fNumber & '&Tab=2&ForeignField=mfo:Field&_sort=mfo:Field&Refresh=' & |
                    'sort&LookupFrom=JobFaultCodes&' & |
                    'fieldNumber=' & maf:Field_Number & '&partType=&partMainFault='),) !lookupextra
    end
BrowseOutFaultsEstimateParts PROCEDURE  (NetWebServerWorker p_web)
Local                CLASS
EstimateCode         Procedure(Long func:FieldNumber,String func:Field)
                     END
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256),dim(Net:MaxKeyFields)
idx           String(Net:HashSize)
sub           Long
            End
sofp:fault:IsInvalid  Long
sofp:description:IsInvalid  Long
sofp:level:IsInvalid  Long
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:DefaultSelection    String(Net:HashSize)
loc:ActualSelection     String(Net:HashSize)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
!loc:pagename            String(256)
loc:NavButtonPosition    Long
loc:UpdateButtonPosition Long
loc:SelectionMethod      Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorCase         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
loc:lookupdone          Long
loc:FormPopup           Long
ThisView            View(SBO_OutFaultParts)
                      Project(sofp:sessionID)
                      Project(sofp:partType)
                      Project(sofp:fault)
                      Project(sofp:fault)
                      Project(sofp:description)
                      Project(sofp:level)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
Loc:popup             Long
loc:poppedup          Long,static,thread
FilesOpened     Long
MANFAULO::State  USHORT
PARTS::State  USHORT
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
    Access:ESTPARTS.ClearKey(epr:Part_Number_Key)
    epr:Ref_Number  = p_web.GSV('wob:RefNumber')
    Set(epr:Part_Number_Key,epr:Part_Number_Key)
    Loop
        If Access:ESTPARTS.NEXT()
           Break
        End !If
        If epr:Ref_Number  <> p_web.GSV('wob:RefNumber')      |
            Then Break.  ! End If
        If epr:Fault_Code1 <> ''
            Local.EstimateCode(1,epr:fault_Code1)
        End !If epr:Fault_Code1
        If epr:Fault_Code2 <> ''
            Local.EstimateCode(2,epr:fault_Code2)
        End !If epr:Fault_Code1
        If epr:Fault_Code3 <> ''
            Local.EstimateCode(3,epr:fault_Code3)
        End !If epr:Fault_Code1
        If epr:Fault_Code4 <> ''
            Local.EstimateCode(4,epr:fault_Code4)
        End !If epr:Fault_Code1
        If epr:Fault_Code5 <> ''
            Local.EstimateCode(5,epr:fault_Code5)
        End !If epr:Fault_Code1
        If epr:Fault_Code6 <> ''
            Local.EstimateCode(6,epr:fault_Code6)
        End !If epr:Fault_Code1
        If epr:Fault_Code7 <> ''
            Local.EstimateCode(7,epr:fault_Code7)
        End !If epr:Fault_Code1
        If epr:Fault_Code8 <> ''
            Local.EstimateCode(8,epr:fault_Code8)
        End !If epr:Fault_Code1
        If epr:Fault_Code9 <> ''
            Local.EstimateCode(9,epr:fault_Code9)
        End !If epr:Fault_Code1
        If epr:Fault_Code10 <> ''
            Local.EstimateCode(10,epr:fault_Code10)
        End !If epr:Fault_Code1
        If epr:Fault_Code11 <> ''
            Local.EstimateCode(11,epr:fault_Code11)
        End !If epr:Fault_Code1
        If epr:Fault_Code12 <> ''
            Local.EstimateCode(12,epr:fault_Code12)
        End !If epr:Fault_Code1
    End !Loop
  If p_web.GetSessionLoggedIn() = 0
    Return 
  End
  GlobalErrors.SetProcedureName('BrowseOutFaultsEstimateParts')
  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseOutFaultsEstimateParts:NoForm')
      loc:NoForm = p_web.GetValue('BrowseOutFaultsEstimateParts:NoForm')
      loc:FormName = p_web.GetValue('BrowseOutFaultsEstimateParts:FormName')
    else
      loc:FormName = 'BrowseOutFaultsEstimateParts_frm'
    End
    p_web.SSV('BrowseOutFaultsEstimateParts:NoForm',loc:NoForm)
    p_web.SSV('BrowseOutFaultsEstimateParts:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseOutFaultsEstimateParts:NoForm')
    loc:FormName = p_web.GSV('BrowseOutFaultsEstimateParts:FormName')
  end
  case p_web.site.formpopups
  of -1 ; loc:FormPopup = Net:Page
  of 0 ; loc:FormPopup = Net:Page
  of 1 ; loc:FormPopup = Net:Popup
  End
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseOutFaultsEstimateParts') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseOutFaultsEstimateParts')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if 1 = 2
  elsif p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  elsif p_web.IfExistsValue('_EIPRow_')
    do CallRow
  elsif p_web.GetValue('_CallPopups') > 0
    do CallPopups
  elsif p_web.IfExistsValue('_GetSecwinSettings')
      p_web.SetValue('Secwin_' & 'BrowseOutFaultsEstimateParts' & 'AccessGroupsArray','1') 
      p_web.SetValue('Secwin_AccessWindowName','BrowseOutFaultsEstimateParts')
      
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallPopups  Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
loc:CallPopups   Long
loc:name         String(255)
  code
  if loc:poppedup = 0
    loc:CallPopups = p_web.GetValue('_CallPopups')
    if loc:CallPopups = 1 !and p_web.GetPreCall('BrowseOutFaultsEstimateParts') = 0! for = 1 include popup divs and scripts for this browse
      !p_web.AddPreCall('BrowseOutFaultsEstimateParts')
      p_web.DivHeader('popup_BrowseOutFaultsEstimateParts','nt-hidden')
      p_web.DivHeader('BrowseOutFaultsEstimateParts',p_web.combine(p_web.site.style.browsediv,'fdiv'))
      p_web.DivFooter()
      p_web.DivFooter()
      loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(400)&', modal: true, position: [''center'',15]'
      packet =  p_web.jQuery('#' & lower('popup_BrowseOutFaultsEstimateParts_div'),'dialog',loc:options) & |
      p_web.jQuery('#' & lower('popup_BrowseOutFaultsEstimateParts_div'),'removeClass','''nt-hidden''')
    End
    ! for > 0 add in form, EIP-lookups and browse object
    do Popups
    loc:popup = 1
    do ClosingScripts
    do SendPacket
    loc:poppedup = 1
  end

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web.DivHeader('BrowseOutFaultsEstimateParts',p_web.combine(p_web.site.style.browsediv,'fdiv'))
  if loc:ParentSilent = 0
    do GenerateBrowse
    if p_web.RequestAjax = 1
      p_web.Script('BrowseProducts.pcv();')
    end
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web.DivFooter()
  do Children
  do Popups
  if loc:poppedup = 0
    do ClosingScripts
  end
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(SBO_OutFaultParts,sofp:FaultKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'SOFP:FAULT') then p_web.SetValue('BrowseOutFaultsEstimateParts_sort','1')
    ElsIf (loc:vorder = 'SOFP:DESCRIPTION') then p_web.SetValue('BrowseOutFaultsEstimateParts_sort','2')
    ElsIf (loc:vorder = 'SOFP:LEVEL') then p_web.SetValue('BrowseOutFaultsEstimateParts_sort','3')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseOutFaultsEstimateParts:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseOutFaultsEstimateParts:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseOutFaultsEstimateParts:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseOutFaultsEstimateParts:LookupField')
    loc:selecting = 0
  End

Popups Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  If loc:popup = 0
  End
SetFormAction  Routine
  loc:formaction = 'BrowseOutFaultsEstimateParts'

GotFocusBack   Routine

GenerateBrowse Routine
  data
loc:viewoptions  Long
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  ! Set general Browse options
  loc:NavButtonPosition   = Net:Below
  loc:UpdateButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:FileLoad
  loc:FillBack         = 0
  loc:Sorting          = Net:ServerSort
  if p_web.GetValue('_popup_') = 1
    loc:popup = 1
  end
  ! Set Locator Options
  loc:LocatorPosition  = Net:Below
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  p_web.ClearBrowse('BrowseOutFaultsEstimateParts')
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 10
  End
  loc:ActualSelection = ''
  !loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseOutFaultsEstimateParts_sort',net:DontEvaluate)
  p_web.SetSessionValue('BrowseOutFaultsEstimateParts_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(sofp:fault)','-UPPER(sofp:fault)')
    Loc:LocateField = 'sofp:fault'
    Loc:LocatorCase = 0
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(sofp:description)','-UPPER(sofp:description)')
    Loc:LocateField = 'sofp:description'
    Loc:LocatorCase = 0
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'sofp:level','-sofp:level')
    Loc:LocateField = 'sofp:level'
    Loc:LocatorCase = 0
  end
  if loc:vorder = ''
    loc:vorder = '+sofp:sessionID,+UPPER(sofp:partType),+UPPER(sofp:fault)'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('sofp:fault')
    loc:SortHeader = p_web.Translate('Fault')
    p_web.SetSessionValue('BrowseOutFaultsEstimateParts_LocatorPic','@s30')
  Of upper('sofp:description')
    loc:SortHeader = p_web.Translate('Description')
    p_web.SetSessionValue('BrowseOutFaultsEstimateParts_LocatorPic','@s60')
  Of upper('sofp:level')
    loc:SortHeader = p_web.Translate('Repair Index')
    p_web.SetSessionValue('BrowseOutFaultsEstimateParts_LocatorPic','@n8')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
  End
  If loc:selecting = 1
    p_web.GetSettings(p_web.GetSessionValue('Push1'))
    loc:selectaction = p_web.FormSettings.ParentPage !p_web.GetSessionValue('BrowseOutFaultsEstimateParts:LookupFrom')
  End!Else
  do SendPacket
  do SetFormAction
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseOutFaultsEstimateParts:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseOutFaultsEstimateParts:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseOutFaultsEstimateParts:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="SBO_OutFaultParts"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="sofp:FaultKey"></input><13,10>'
  end
  If p_web.Translate('Estimate Parts') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.browseSubHeading,)&'">'&p_web.Translate('Estimate Parts',0)&'</div>'&CRLF
  End
  case p_web.GetValue('refresh') ; of 'next' orof 'previous' orof 'first' orof 'last'
    p_web.Script('try{{BrowseOutFaultsEstimateParts.restoreFocus();} catch(err){{};')
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseOutFaultsEstimateParts',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      case loc:locatortype
      of Net:Position
      orof Net:Date
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
      of Net:Contains
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
      of Net:Begins
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
      End
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,'Locator')&'"><tr>' &|
      Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
      Case Loc:LocatorType
      of Net:Date
        packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator2BrowseOutFaultsEstimateParts','',p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,, p_web.GSV('BrowseOutFaultsEstimateParts_LocatorPic'),,,'onchange="BrowseOutFaultsEstimateParts.locate(''Locator2BrowseOutFaultsEstimateParts'',this.value);"',,0,,,) & '</div></td>'
      Else
        packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator2BrowseOutFaultsEstimateParts',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,'size="30" onchange="BrowseOutFaultsEstimateParts.locate(''Locator2BrowseOutFaultsEstimateParts'',this.value);" ',,,,,,) & '</td>'
      End
      If loc:LocatorSearchButton or loc:LocatorClearButton
        packet = clip(packet) & '<td><div id="BrowseOutFaultsEstimateParts_locate_a" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
        If loc:LocatorSearchButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
        End
        If loc:LocatorClearButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseOutFaultsEstimateParts.cl(''BrowseOutFaultsEstimateParts'');')
        End
        packet = clip(packet) & '</div><13,10></td><13,10>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
      If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'BrowseOutFaultsEstimateParts_locate_a','buttonset',loc:options)
      End
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & p_web.DivHeader('BrowseOutFaultsEstimateParts_table',p_web.Combine(p_web.site.style.BrowseTableDiv,''),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,,'sortable')&'" id="BrowseOutFaultsEstimateParts_tbl">'
  Else
    packet = clip(packet) & p_web.DivHeader('BrowseOutFaultsEstimateParts_table',p_web.Combine(p_web.site.style.BrowseTableDiv,''),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,)&'" id="BrowseOutFaultsEstimateParts_tbl">'
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'1','BrowseOutFaultsEstimateParts',p_web.Translate('Fault'),'Click here to sort by Fault',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'2','BrowseOutFaultsEstimateParts',p_web.Translate('Description'),'Click here to sort by Description',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'3','BrowseOutFaultsEstimateParts',p_web.Translate('Repair Index'),'Click here to sort by Repair Index',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,10,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  If Instring('sofp:sessionid',lower(loc:vorder),1,1) = 0 !and SBO_OutFaultParts{prop:SQLDriver} = 1
    loc:vorder = Choose(loc:vorder='','sofp:sessionID',clip(loc:vorder) & ',' & 'sofp:sessionID')
  End
  Loc:Selected = Choose(p_web.IfExistsValue('sofp:sessionID'),p_web.GetValue('sofp:sessionID'),p_web.GetSessionValue('sofp:sessionID'))
  If Instring('sofp:parttype',lower(loc:vorder),1,1) = 0 !and SBO_OutFaultParts{prop:SQLDriver} = 1
    loc:vorder = Choose(loc:vorder='','sofp:partType',clip(loc:vorder) & ',' & 'sofp:partType')
  End
  Loc:Selected = Choose(p_web.IfExistsValue('sofp:partType'),p_web.GetValue('sofp:partType'),p_web.GetSessionValue('sofp:partType'))
  If Instring('sofp:fault',lower(loc:vorder),1,1) = 0 !and SBO_OutFaultParts{prop:SQLDriver} = 1
    loc:vorder = Choose(loc:vorder='','sofp:fault',clip(loc:vorder) & ',' & 'sofp:fault')
  End
  Loc:Selected = Choose(p_web.IfExistsValue('sofp:fault'),p_web.GetValue('sofp:fault'),p_web.GetSessionValue('sofp:fault'))
  ThisView{prop:order} = p_web.CleanFilter(ThisView,clip(loc:vorder))
      loc:FilterWas = 'sofp:sessionID = ' & p_web.sessionID & ' and Upper(sofp:partType) = ''E'''
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseOutFaultsEstimateParts',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseOutFaultsEstimateParts_Filter')
    p_web.SetSessionValue('BrowseOutFaultsEstimateParts_FirstValue','')
    p_web.SetSessionValue('BrowseOutFaultsEstimateParts_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,SBO_OutFaultParts,sofp:FaultKey,loc:PageRows,'BrowseOutFaultsEstimateParts',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,loc:ViewOptions,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled,Loc:LocatorCase)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or Loc:LocatorType = Net:Date or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position or loc:LocatorType = Net:Date
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If SBO_OutFaultParts{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(SBO_OutFaultParts,loc:firstvalue)
              Reset(ThisView,SBO_OutFaultParts)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If SBO_OutFaultParts{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(SBO_OutFaultParts,loc:lastvalue)
            Reset(ThisView,SBO_OutFaultParts)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(sofp:fault)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td class="'&p_web.combine(p_web.site.style.BrowseEmpty,)&'">'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:NavButtonPosition=Net:Above or (loc:NavButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseOutFaultsEstimateParts_nav_a" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseOutFaultsEstimateParts.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseOutFaultsEstimateParts.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseOutFaultsEstimateParts.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseOutFaultsEstimateParts.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseOutFaultsEstimateParts_nav_a','buttonset',loc:options)
          End
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:UpdateButtonPosition=Net:Above or (loc:UpdateButtonPosition=Net:Both and loc:found > 0))
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseOutFaultsEstimateParts',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseOutFaultsEstimateParts_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseOutFaultsEstimateParts_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          case loc:locatortype
          of Net:Position
          orof Net:Date
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
          of Net:Contains
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
          of Net:Begins
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
          End
          packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,'Locator')&'"><tr>' &|
          Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
          Case Loc:LocatorType
          of Net:Date
            packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator1BrowseOutFaultsEstimateParts','',p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,, p_web.GSV('BrowseOutFaultsEstimateParts_LocatorPic'),,,'onchange="BrowseOutFaultsEstimateParts.locate(''Locator1BrowseOutFaultsEstimateParts'',this.value);"',,0,,,) & '</div></td>'
          Else
            packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator1BrowseOutFaultsEstimateParts',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,'size="30" onchange="BrowseOutFaultsEstimateParts.locate(''Locator1BrowseOutFaultsEstimateParts'',this.value);" ',,,,,,) & '</td>'
          End
          If loc:LocatorSearchButton or loc:LocatorClearButton
            packet = clip(packet) & '<td><div id="BrowseOutFaultsEstimateParts_locate_b" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
            If loc:LocatorSearchButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
            End
            If loc:LocatorClearButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseOutFaultsEstimateParts.cl(''BrowseOutFaultsEstimateParts'');')
            End
            packet = clip(packet) & '</div><13,10></td><13,10>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
          If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseOutFaultsEstimateParts_locate_b','buttonset',loc:options)
          End
      End
    End
  End
  p_web.SetSessionValue('BrowseOutFaultsEstimateParts_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseOutFaultsEstimateParts_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:NavButtonPosition=Net:Below or loc:NavButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseOutFaultsEstimateParts_nav_b" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseOutFaultsEstimateParts.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseOutFaultsEstimateParts.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseOutFaultsEstimateParts.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseOutFaultsEstimateParts.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseOutFaultsEstimateParts_nav_b','buttonset',loc:options)
          End
        end
        do SendPacket
  End
  If loc:UpdateButtonPosition=Net:Below or loc:UpdateButtonPosition=Net:Both
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
  data
loc:rowclick  string(1024)
  code
    loc:field = p_web.AddBrowseValue('BrowseOutFaultsEstimateParts','SBO_OutFaultParts',sofp:FaultKey) !sofp:fault
    p_web._thisrow = p_web._nocolon('sofp:fault')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if Loc:LocatorValue <> '' and loc:ActualSelection = ''
        loc:checked = 'checked'
        do SetSelection
      elsif loc:ActualSelection = '' and sofp:fault = p_web.GetValue('sofp:fault')
         loc:checked = 'checked'
         do SetSelection
      elsif loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseOutFaultsEstimateParts:LookupField')) = sofp:fault and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      else
        loc:checked = Choose((sofp:fault = loc:selected) and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      end
      If(loc:SelectionMethod  = Net:Radio)
        loc:RowStyle = 'class="' & p_web.combine(p_web.site.style.browserow,) &'"'
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'data-nt-id="'& p_web.AddBrowseValue('BrowseOutFaultsEstimateParts','SBO_OutFaultParts',sofp:FaultKey) &'" onclick="'
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If SBO_OutFaultParts{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(SBO_OutFaultParts)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            loc:DefaultSelection = loc:field
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If SBO_OutFaultParts{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(SBO_OutFaultParts)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            loc:DefaultSelection = loc:field
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'>'&p_web.CreateInput('radio','_bidv_',clip(loc:field),,loc:checked,'')&'</td>'&CRLF
          If loc:DefaultSelection = ''
            loc:DefaultSelection = loc:field
          End
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          If loc:DefaultSelection = '' or loc:direction < 0
            loc:DefaultSelection = loc:field
          End
        End
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::sofp:fault
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::sofp:description
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::sofp:level
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr ' & clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Idx = p_web.AddBrowseValue('BrowseOutFaultsEstimateParts','SBO_OutFaultParts',sofp:FaultKey)
  TableQueue.Id[1] = sofp:sessionID
  TableQueue.Id[2] = sofp:partType
  TableQueue.Id[3] = sofp:fault

ClosingScripts  Routine
  data
Rtn_SecwinProcedureName         string(252)
  code
  If p_web.RequestAjax = 0
    do SetFormAction
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var btiBrowseOutFaultsEstimateParts;if (btiBrowseOutFaultsEstimateParts != 1){{var BrowseOutFaultsEstimateParts=new browseTable(''BrowseOutFaultsEstimateParts'','''&clip(loc:formname)&|
      ''','''&p_web._jsok('sofp:sessionID',Net:Parameter)&''',2,'''&clip(loc:divname)&''',1,1,1,'''&|
      clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&|
      p_web.Translate('Are you sure you want to delete this record?')&''','''&clip(loc:ActualSelection)&''','''&clip(loc:selectaction)&''','''&|
      clip(loc:formactiontarget)&''','''','&loc:popup&',0,'''','''',"' & clip(Rtn_SecwinProcedureName) & '");<13,10>'&|
      'BrowseOutFaultsEstimateParts.setGreenBar('''&p_web.ColorWeb(p_web.Site.Style.BrowseHighlightColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOneColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseTwoColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOverColor)&''');<13,10>' &|
      'BrowseOutFaultsEstimateParts.applyGreenBar();btiBrowseOutFaultsEstimateParts=1};<13,10>' &|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseOutFaultsEstimateParts')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseOutFaultsEstimateParts')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseOutFaultsEstimateParts')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseOutFaultsEstimateParts')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(SBO_OutFaultParts)
  p_web._CloseFile(MANFAULO)
  p_web._CloseFile(PARTS)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(SBO_OutFaultParts)
  Bind(sofp:Record)
  Clear(sofp:Record)
  NetWebSetSessionPics(p_web,SBO_OutFaultParts)
  p_web._OpenFile(MANFAULO)
  Bind(mfo:Record)
  NetWebSetSessionPics(p_web,MANFAULO)
  p_web._OpenFile(PARTS)
  Bind(par:Record)
  NetWebSetSessionPics(p_web,PARTS)

Children Routine
  if loc:selecting = 0
    If p_web.RequestAjax = 0
      do StartChildren
    Else
      do AjaxChildren
    End
  end

AjaxChildren  Routine
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue('sofp:sessionID',p_web.GetValue('sofp:sessionID'))
  p_web.SetSessionValue('sofp:partType',p_web.GetValue('sofp:partType'))
  p_web.SetSessionValue('sofp:fault',p_web.GetValue('sofp:fault'))
   p_web.DeleteValue('_Clicked')
   loc:found = 1
   do Children
! ----------------------------------------------------------------------------------------
CallRow  Routine
  data
loc:result  long
  code
  do OpenFilesB
  sofp:sessionID = p_web.GSV('sofp:sessionID')
  sofp:partType = p_web.GSV('sofp:partType')
  sofp:fault = p_web.GSV('sofp:fault')
  loc:result = p_web._GetFile(SBO_OutFaultParts,sofp:FaultKey)
  loc:eip = 1
  loc:viewstate = p_web.escape(p_web.Base64Encode(clip(sofp:sessionID)))
  do BrowseRow
  do SendPacket
  do ClosefilesB

! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(SBO_OutFaultParts)
  Case upper(p_web.GetValue('_EIPClm'))
  Else
    case p_web.GetValue('_action_')
    of Net:InsertRecord
    orof Net:ChangeRecord
    orof Net:DeleteRecord
    orof Net:CopyRecord
      loc:eip = 0
      do CallBrowse
    end
  End
  do GotFocusBack
  p_web._CloseFile(SBO_OutFaultParts)
! ----------------------------------------------------------------------------------------
value::sofp:fault   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseOutFaultsEstimateParts_sofp:fault_'&sofp:fault,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(sofp:fault,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sofp:description   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseOutFaultsEstimateParts_sofp:description_'&sofp:fault,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(sofp:description,'@s60')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sofp:level   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseOutFaultsEstimateParts_sofp:level_'&sofp:fault,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(sofp:level,'@n8')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  p_web._OpenFile(MANFAULO)
  p_web._OpenFile(PARTS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(MANFAULO)
  p_Web._CloseFile(PARTS)
     FilesOpened = False
  END
  return
!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
CheckForDuplicate  Routine
SetSelection  Routine
  loc:ActualSelection = loc:field
  p_web.SetSessionValue('sofp:sessionID',sofp:sessionID)
  p_web.SetSessionValue('sofp:partType',sofp:partType)
  p_web.SetSessionValue('sofp:fault',sofp:fault)

MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:ActualSelection = ''
    loc:ActualSelection = loc:DefaultSelection
    p_web.GetBrowseValue(loc:ActualSelection,Net:Web:SessionQueue) ! so children are primed with correct sessionValue
  End

  If loc:ActualSelection <> ''
    TableQueue.Kind = Net:RowTable
    get(TableQueue,TableQueue.Kind)
    if Errorcode() = 0
      TableQueue.Row = sub(TableQueue.Row,1,len(clip(TableQueue.Row))-1) &  ' data-nt-rows="'&loc:RowsHigh&'" data-nt-value="'&clip(loc:ActualSelection)&'"' & '>'
      Put(TableQueue)
    End
  End

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead class="'&p_web.combine(p_web.site.style.BrowseHeader,'')&'"><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot class="'&p_web.combine(p_web.site.style.BrowseFooter,'BrowseFooter')&'"><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody class="'&p_web.combine(p_web.site.style.BrowseBody,)&'"><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table><13,10>' & p_web.DivFooter(Net:NoSend) ! Table Div
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
Local.EstimateCode      Procedure(Long func:FieldNumber,String func:Field)
Code
    !Is this fault code a "Main Fault"
    Access:MANFAUPA.ClearKey(map:Field_Number_Key)
    map:Manufacturer = p_web.GSV('job:manufacturer')
    map:Field_Number = func:FieldNumber
    If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
        !Found
        If map:MainFault

            !Ok, get the details from the Job "Main Fault"
            Access:MANFAULT.ClearKey(maf:MainFaultKey)
            maf:Manufacturer = p_web.GSV('job:manufacturer')
            maf:MainFault    = 1
            If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                !Found
                Access:MANFAULO.ClearKey(mfo:Field_Key)
                mfo:Manufacturer = p_web.GSV('job:manufacturer')
                mfo:Field_Number = maf:Field_Number
                mfo:Field        = func:Field
                Set(mfo:Field_Key,mfo:Field_Key)
                Loop
                    If Access:MANFAULO.NEXT()
                       Break
                    End !If
                    If mfo:Manufacturer <> p_web.GSV('job:manufacturer')      |
                    Or mfo:Field_Number <> maf:Field_Number      |
                    Or mfo:Field        <> func:Field      |
                        Then Break.  ! End If
                    If mfo:RelatedPartCode <> 0
                        If mfo:RelatedPartCode <> func:FieldNumber
                            Cycle
                        End !If mfo:RelatedPartCode <> func:FieldNumber
                    !This 'END' was at the bottom which would
                    !mean only Ericsson faults would ever appear - 234694 (DBH: 25-07-2003)
                    End !If mfo:RelatedPartCode <> 0

                    if (mfo:NotAvailable)
                        ! #11655 Don't show "Not Available" Fault Codes (Bryan: 23/08/2010)
                        CYCLE
                    END


                    Access:SBO_OUTFAULTPARTS.Clearkey(sofp:faultKey)
                    sofp:sessionID    = p_web.sessionID
                    sofp:partType    = 'E'
                    sofp:fault    = func:field
                    if (Access:SBO_OUTFAULTPARTS.TryFetch(sofp:faultKey) = Level:Benign)
                        ! Found
                    else ! if (Access:SBO_OUTFAULTPARTS.TryFetch(sofp:faultKey) = Level:Benign)
                        ! Error
                        if (Access:SBO_OUTFAULTPARTS.PrimeRecord() = Level:Benign)
                            sofp:sessionID    = p_web.sessionID
                            sofp:partType    = 'E'
                            sofp:fault    = func:Field
                            sofp:description    = mfo:description
                            sofp:level    = mfo:importancelevel

                            if (Access:SBO_OUTFAULTPARTS.TryInsert() = Level:Benign)
                                ! Inserted
                            else ! if (Access:SBO_OUTFAULTPARTS.TryInsert() = Level:Benign)
                                ! Error
                            end ! if (Access:SBO_OUTFAULTPARTS.TryInsert() = Level:Benign)
                        end ! if (Access:SBO_OUTFAULTPARTS.PrimeRecord() = Level:Benign)
                    end ! if (Access:SBO_OUTFAULTPARTS.TryFetch(sofp:faultKey) = Level:Benign)
                    Break
                End !Loop
            Else!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
        Else !If map:MainFault

        End !If map:MainFault
    Else!If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End            !If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
DisplayOutFaults     PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
FilesOpened     Long
BrowseJobOutFaults:IsInvalid  Long
BrowseOutFaultsChargeableParts:IsInvalid  Long
BrowseOutFaultsWarrantyParts:IsInvalid  Long
BrowseOutFaultsEstimateParts:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  GlobalErrors.SetProcedureName('DisplayOutFaults')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'DisplayOutFaults_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('DisplayOutFaults','')
    p_web.DivHeader('DisplayOutFaults',p_web.combine(p_web.site.style.formdiv,'fdiv'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('DisplayOutFaults') = 0
        p_web.AddPreCall('DisplayOutFaults')
        p_web.DivHeader('popup_DisplayOutFaults','nt-hidden')
        p_web.DivHeader('DisplayOutFaults',p_web.combine(p_web.site.style.formdiv,'fdiv'))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_DisplayOutFaults_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_DisplayOutFaults_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferDisplayOutFaults',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_DisplayOutFaults',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferDisplayOutFaults',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_DisplayOutFaults',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferDisplayOutFaults',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_DisplayOutFaults',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_DisplayOutFaults',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferDisplayOutFaults',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_DisplayOutFaults',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferDisplayOutFaults',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_DisplayOutFaults',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_DisplayOutFaults',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('DisplayOutFaults')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE
  loc:EipClm = upper(p_web.GetValue('_EIPClm'))
  p_web.DeleteValue('_EIPClm')
  case loc:EipClm
  of ''
    case upper(p_web.GetValue('_calledfrom_'))
    of upper('BrowseJobOutFaults')
      do Value::BrowseJobOutFaults
    of upper('BrowseOutFaultsChargeableParts')
      do Value::BrowseOutFaultsChargeableParts
    of upper('BrowseOutFaultsWarrantyParts')
      do Value::BrowseOutFaultsWarrantyParts
    of upper('BrowseOutFaultsEstimateParts')
      do Value::BrowseOutFaultsEstimateParts
    end
  end

InitForm       Routine
  DATA
LF  &FILE
  CODE
   p_web.site.SaveButton.TextValue = 'OK'
  p_web.SetValue('DisplayOutFaults_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'DisplayOutFaults'
    end
    p_web.formsettings.proc = 'DisplayOutFaults'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('DisplayOutFaults_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'JobFaultCodes'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('DisplayOutFaults_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('DisplayOutFaults_ChainTo')
    loc:formaction = p_web.GetSessionValue('DisplayOutFaults_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'JobFaultCodes'

GenerateForm   Routine
  do LoadRelatedRecords
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Out Faults') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Out Faults',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_DisplayOutFaults',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_DisplayOutFaults0_div')&'">'&p_web.Translate('Job Out Faults')&'</a></li>'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_DisplayOutFaults1_div')&'">'&p_web.Translate('Part Out Faults')&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  do GenerateTab1
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="DisplayOutFaults_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('DisplayOutFaults_BrowseJobOutFaults_embedded_div')&''');'
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('DisplayOutFaults_BrowseOutFaultsChargeableParts_embedded_div')&''');'
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('DisplayOutFaults_BrowseOutFaultsWarrantyParts_embedded_div')&''');'
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('DisplayOutFaults_BrowseOutFaultsEstimateParts_embedded_div')&''');'
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="DisplayOutFaults_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'DisplayOutFaults_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="DisplayOutFaults_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('DisplayOutFaults_BrowseJobOutFaults_embedded_div')&''');'
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('DisplayOutFaults_BrowseOutFaultsChargeableParts_embedded_div')&''');'
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('DisplayOutFaults_BrowseOutFaultsWarrantyParts_embedded_div')&''');'
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('DisplayOutFaults_BrowseOutFaultsEstimateParts_embedded_div')&''');'
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'DisplayOutFaults_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_DisplayOutFaults')>0,p_web.GSV('showtab_DisplayOutFaults'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_DisplayOutFaults'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_DisplayOutFaults') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_DisplayOutFaults'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_DisplayOutFaults')>0,p_web.GSV('showtab_DisplayOutFaults'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_DisplayOutFaults') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Job Out Faults') & ''''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Part Out Faults') & ''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_DisplayOutFaults_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_DisplayOutFaults')>0,p_web.GSV('showtab_DisplayOutFaults'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"DisplayOutFaults",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_DisplayOutFaults')>0,p_web.GSV('showtab_DisplayOutFaults'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_DisplayOutFaults_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('DisplayOutFaults') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('DisplayOutFaults')
    p_web.SetValue('_CallPopups',2)
    If p_web.GetPreCall('BrowseJobOutFaults') = 0
      p_web.SetValue('BrowseJobOutFaults:NoForm',1)
      p_web.SetValue('BrowseJobOutFaults:FormName',loc:formname)
      p_web.SetValue('BrowseJobOutFaults:parentIs','Form')
      p_web.SetValue('_parentProc','DisplayOutFaults')
      BrowseJobOutFaults(p_web)
      p_web.SetValue('_CallPopups',0)
      p_web.DeleteValue('BrowseJobOutFaults:NoForm')
      p_web.DeleteValue('BrowseJobOutFaults:FormName')
      p_web.DeleteValue('BrowseJobOutFaults:parentIs')
      p_web.DeleteValue('_parentProc')
    End
    p_web.SetValue('_CallPopups',2)
    If p_web.GetPreCall('BrowseOutFaultsChargeableParts') = 0
      p_web.SetValue('BrowseOutFaultsChargeableParts:NoForm',1)
      p_web.SetValue('BrowseOutFaultsChargeableParts:FormName',loc:formname)
      p_web.SetValue('BrowseOutFaultsChargeableParts:parentIs','Form')
      p_web.SetValue('_parentProc','DisplayOutFaults')
      BrowseOutFaultsChargeableParts(p_web)
      p_web.SetValue('_CallPopups',0)
      p_web.DeleteValue('BrowseOutFaultsChargeableParts:NoForm')
      p_web.DeleteValue('BrowseOutFaultsChargeableParts:FormName')
      p_web.DeleteValue('BrowseOutFaultsChargeableParts:parentIs')
      p_web.DeleteValue('_parentProc')
    End
    p_web.SetValue('_CallPopups',2)
    If p_web.GetPreCall('BrowseOutFaultsWarrantyParts') = 0
      p_web.SetValue('BrowseOutFaultsWarrantyParts:NoForm',1)
      p_web.SetValue('BrowseOutFaultsWarrantyParts:FormName',loc:formname)
      p_web.SetValue('BrowseOutFaultsWarrantyParts:parentIs','Form')
      p_web.SetValue('_parentProc','DisplayOutFaults')
      BrowseOutFaultsWarrantyParts(p_web)
      p_web.SetValue('_CallPopups',0)
      p_web.DeleteValue('BrowseOutFaultsWarrantyParts:NoForm')
      p_web.DeleteValue('BrowseOutFaultsWarrantyParts:FormName')
      p_web.DeleteValue('BrowseOutFaultsWarrantyParts:parentIs')
      p_web.DeleteValue('_parentProc')
    End
    p_web.SetValue('_CallPopups',2)
    If p_web.GetPreCall('BrowseOutFaultsEstimateParts') = 0
      p_web.SetValue('BrowseOutFaultsEstimateParts:NoForm',1)
      p_web.SetValue('BrowseOutFaultsEstimateParts:FormName',loc:formname)
      p_web.SetValue('BrowseOutFaultsEstimateParts:parentIs','Form')
      p_web.SetValue('_parentProc','DisplayOutFaults')
      BrowseOutFaultsEstimateParts(p_web)
      p_web.SetValue('_CallPopups',0)
      p_web.DeleteValue('BrowseOutFaultsEstimateParts:NoForm')
      p_web.DeleteValue('BrowseOutFaultsEstimateParts:FormName')
      p_web.DeleteValue('BrowseOutFaultsEstimateParts:parentIs')
      p_web.DeleteValue('_parentProc')
    End
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Job Out Faults')&'</a></h3>' & CRLF & p_web.DivHeader('tab_DisplayOutFaults0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_DisplayOutFaults0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_DisplayOutFaults0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_DisplayOutFaults0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Job Out Faults')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_DisplayOutFaults0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Job Out Faults')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_DisplayOutFaults0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Job Out Faults')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_DisplayOutFaults0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::BrowseJobOutFaults
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab1  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Part Out Faults')&'</a></h3>' & CRLF & p_web.DivHeader('tab_DisplayOutFaults1',p_web.combine(p_web.site.style.FormTabInner,,'FormCentreFixed'),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_DisplayOutFaults1',p_web.combine(p_web.site.style.FormTabInner,,'FormCentreFixed'),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_DisplayOutFaults1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,'FormCentreFixed'),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_DisplayOutFaults1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,'FormCentreFixed'),Net:NoSend,'Part Out Faults')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_DisplayOutFaults1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,'FormCentreFixed'),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Part Out Faults')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_DisplayOutFaults1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,'FormCentreFixed'),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Part Out Faults')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_DisplayOutFaults1',p_web.combine(p_web.site.style.FormTabInner,,'FormCentreFixed'),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::BrowseOutFaultsChargeableParts
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::BrowseOutFaultsWarrantyParts
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::BrowseOutFaultsEstimateParts
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Validate::BrowseJobOutFaults  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  Elsif true
    p_web.StoreValue('joo:RecordNumber')
  End
  do ValidateValue::BrowseJobOutFaults  ! copies value to session value if valid.
  do Comment::BrowseJobOutFaults ! allows comment style to be updated.

ValidateValue::BrowseJobOutFaults  Routine
    If not (1=0)
    End

Value::BrowseJobOutFaults  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(1=0,1,0))
  ! --- BROWSE ---  BrowseJobOutFaults --
  p_web.SetValue('BrowseJobOutFaults:NoForm',1)
  p_web.SetValue('BrowseJobOutFaults:FormName',loc:formname)
  p_web.SetValue('BrowseJobOutFaults:parentIs','Form')
  p_web.SetValue('_parentProc','DisplayOutFaults')
  if p_web.RequestAjax = 0
    p_web.SSV('DisplayOutFaults:_popup_',p_web.GetValue('_popup_')) ! stores the current browse popup state
    packet = clip(packet) & '<div id="'&lower('DisplayOutFaults_BrowseJobOutFaults_embedded_div')&'"><!-- Net:BrowseJobOutFaults --></div><13,10>'
    do SendPacket
    p_web.DivHeader('DisplayOutFaults_' & lower('BrowseJobOutFaults') & '_value')
    p_web.DivFooter()
  else
    if p_web.GetValue('_popup_') = 1
      p_web.SSV('DisplayOutFaults:_popup_',1)
    elsif p_web.GSV('DisplayOutFaults:_popup_') = 1
      p_web.SetValue('_popup_',1)
    end
    packet = clip(packet) & '<!-- Net:BrowseJobOutFaults --><13,10>'
  end
  do SendPacket
Comment::BrowseJobOutFaults  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if BrowseJobOutFaults:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('DisplayOutFaults_' & p_web._nocolon('BrowseJobOutFaults') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::BrowseOutFaultsChargeableParts  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  Elsif true
    p_web.StoreValue('sofp:sessionID')
    p_web.StoreValue('sofp:partType')
    p_web.StoreValue('sofp:fault')
  End
  do ValidateValue::BrowseOutFaultsChargeableParts  ! copies value to session value if valid.
  do Comment::BrowseOutFaultsChargeableParts ! allows comment style to be updated.

ValidateValue::BrowseOutFaultsChargeableParts  Routine
    If not (p_web.GSV('job:Chargeable_Job') <>'YES')
    End

Value::BrowseOutFaultsChargeableParts  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(p_web.GSV('job:Chargeable_Job') <>'YES',1,0))
  ! --- BROWSE ---  BrowseOutFaultsChargeableParts --
  p_web.SetValue('BrowseOutFaultsChargeableParts:NoForm',1)
  p_web.SetValue('BrowseOutFaultsChargeableParts:FormName',loc:formname)
  p_web.SetValue('BrowseOutFaultsChargeableParts:parentIs','Form')
  p_web.SetValue('_parentProc','DisplayOutFaults')
  if p_web.RequestAjax = 0
    p_web.SSV('DisplayOutFaults:_popup_',p_web.GetValue('_popup_')) ! stores the current browse popup state
    packet = clip(packet) & '<div id="'&lower('DisplayOutFaults_BrowseOutFaultsChargeableParts_embedded_div')&'"><!-- Net:BrowseOutFaultsChargeableParts --></div><13,10>'
    do SendPacket
    p_web.DivHeader('DisplayOutFaults_' & lower('BrowseOutFaultsChargeableParts') & '_value')
    p_web.DivFooter()
  else
    if p_web.GetValue('_popup_') = 1
      p_web.SSV('DisplayOutFaults:_popup_',1)
    elsif p_web.GSV('DisplayOutFaults:_popup_') = 1
      p_web.SetValue('_popup_',1)
    end
    packet = clip(packet) & '<!-- Net:BrowseOutFaultsChargeableParts --><13,10>'
  end
  do SendPacket
Comment::BrowseOutFaultsChargeableParts  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if BrowseOutFaultsChargeableParts:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('job:Chargeable_Job') <>'YES','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('DisplayOutFaults_' & p_web._nocolon('BrowseOutFaultsChargeableParts') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('job:Chargeable_Job') <>'YES'
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::BrowseOutFaultsWarrantyParts  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  Elsif true
    p_web.StoreValue('sofp:sessionID')
    p_web.StoreValue('sofp:partType')
    p_web.StoreValue('sofp:fault')
  End
  do ValidateValue::BrowseOutFaultsWarrantyParts  ! copies value to session value if valid.
  do Comment::BrowseOutFaultsWarrantyParts ! allows comment style to be updated.

ValidateValue::BrowseOutFaultsWarrantyParts  Routine
    If not (p_web.GSV('job:Warranty_Job') <> 'YES')
    End

Value::BrowseOutFaultsWarrantyParts  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(p_web.GSV('job:Warranty_Job') <> 'YES',1,0))
  ! --- BROWSE ---  BrowseOutFaultsWarrantyParts --
  p_web.SetValue('BrowseOutFaultsWarrantyParts:NoForm',1)
  p_web.SetValue('BrowseOutFaultsWarrantyParts:FormName',loc:formname)
  p_web.SetValue('BrowseOutFaultsWarrantyParts:parentIs','Form')
  p_web.SetValue('_parentProc','DisplayOutFaults')
  if p_web.RequestAjax = 0
    p_web.SSV('DisplayOutFaults:_popup_',p_web.GetValue('_popup_')) ! stores the current browse popup state
    packet = clip(packet) & '<div id="'&lower('DisplayOutFaults_BrowseOutFaultsWarrantyParts_embedded_div')&'"><!-- Net:BrowseOutFaultsWarrantyParts --></div><13,10>'
    do SendPacket
    p_web.DivHeader('DisplayOutFaults_' & lower('BrowseOutFaultsWarrantyParts') & '_value')
    p_web.DivFooter()
  else
    if p_web.GetValue('_popup_') = 1
      p_web.SSV('DisplayOutFaults:_popup_',1)
    elsif p_web.GSV('DisplayOutFaults:_popup_') = 1
      p_web.SetValue('_popup_',1)
    end
    packet = clip(packet) & '<!-- Net:BrowseOutFaultsWarrantyParts --><13,10>'
  end
  do SendPacket
Comment::BrowseOutFaultsWarrantyParts  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if BrowseOutFaultsWarrantyParts:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('job:Warranty_Job') <> 'YES','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('DisplayOutFaults_' & p_web._nocolon('BrowseOutFaultsWarrantyParts') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('job:Warranty_Job') <> 'YES'
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::BrowseOutFaultsEstimateParts  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  Elsif true
    p_web.StoreValue('sofp:sessionID')
    p_web.StoreValue('sofp:partType')
    p_web.StoreValue('sofp:fault')
  End
  do ValidateValue::BrowseOutFaultsEstimateParts  ! copies value to session value if valid.
  do Comment::BrowseOutFaultsEstimateParts ! allows comment style to be updated.

ValidateValue::BrowseOutFaultsEstimateParts  Routine
    If not (p_web.GSV('job:Estimate') <> 'YES')
    End

Value::BrowseOutFaultsEstimateParts  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(p_web.GSV('job:Estimate') <> 'YES',1,0))
  ! --- BROWSE ---  BrowseOutFaultsEstimateParts --
  p_web.SetValue('BrowseOutFaultsEstimateParts:NoForm',1)
  p_web.SetValue('BrowseOutFaultsEstimateParts:FormName',loc:formname)
  p_web.SetValue('BrowseOutFaultsEstimateParts:parentIs','Form')
  p_web.SetValue('_parentProc','DisplayOutFaults')
  if p_web.RequestAjax = 0
    p_web.SSV('DisplayOutFaults:_popup_',p_web.GetValue('_popup_')) ! stores the current browse popup state
    packet = clip(packet) & '<div id="'&lower('DisplayOutFaults_BrowseOutFaultsEstimateParts_embedded_div')&'"><!-- Net:BrowseOutFaultsEstimateParts --></div><13,10>'
    do SendPacket
    p_web.DivHeader('DisplayOutFaults_' & lower('BrowseOutFaultsEstimateParts') & '_value')
    p_web.DivFooter()
  else
    if p_web.GetValue('_popup_') = 1
      p_web.SSV('DisplayOutFaults:_popup_',1)
    elsif p_web.GSV('DisplayOutFaults:_popup_') = 1
      p_web.SetValue('_popup_',1)
    end
    packet = clip(packet) & '<!-- Net:BrowseOutFaultsEstimateParts --><13,10>'
  end
  do SendPacket
Comment::BrowseOutFaultsEstimateParts  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if BrowseOutFaultsEstimateParts:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('job:Estimate') <> 'YES','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('DisplayOutFaults_' & p_web._nocolon('BrowseOutFaultsEstimateParts') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('job:Estimate') <> 'YES'
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('DisplayOutFaults_nexttab_' & 0)
    If loc:Invalid then exit.
  of lower('DisplayOutFaults_nexttab_' & 1)
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_DisplayOutFaults_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('DisplayOutFaults_tab_' & 0)
    do GenerateTab0
  of lower('DisplayOutFaults_tab_' & 1)
    do GenerateTab1
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('DisplayOutFaults_form:ready_',1)

  p_web.SetSessionValue('DisplayOutFaults_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_DisplayOutFaults',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('DisplayOutFaults_form:ready_',1)
  p_web.SetSessionValue('DisplayOutFaults_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_DisplayOutFaults',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('DisplayOutFaults_form:ready_',1)
  p_web.SetSessionValue('DisplayOutFaults_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('DisplayOutFaults:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('DisplayOutFaults_form:ready_',1)
  p_web.SetSessionValue('DisplayOutFaults_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('DisplayOutFaults:Primed',0)
  p_web.setsessionvalue('showtab_DisplayOutFaults',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('DisplayOutFaults_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('DisplayOutFaults_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
    do ValidateValue::BrowseJobOutFaults
    If loc:Invalid then exit.
  ! tab = 2
    loc:InvalidTab += 1
    do ValidateValue::BrowseOutFaultsChargeableParts
    If loc:Invalid then exit.
    do ValidateValue::BrowseOutFaultsWarrantyParts
    If loc:Invalid then exit.
    do ValidateValue::BrowseOutFaultsEstimateParts
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('DisplayOutFaults:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')

BrowseOutFaultsChargeableParts PROCEDURE  (NetWebServerWorker p_web)
Local                CLASS
ChargeableCode         Procedure(Long func:FieldNumber,String func:Field)
                     END
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256),dim(Net:MaxKeyFields)
idx           String(Net:HashSize)
sub           Long
            End
sofp:fault:IsInvalid  Long
sofp:description:IsInvalid  Long
sofp:level:IsInvalid  Long
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:DefaultSelection    String(Net:HashSize)
loc:ActualSelection     String(Net:HashSize)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
!loc:pagename            String(256)
loc:NavButtonPosition    Long
loc:UpdateButtonPosition Long
loc:SelectionMethod      Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorCase         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
loc:lookupdone          Long
loc:FormPopup           Long
ThisView            View(SBO_OutFaultParts)
                      Project(sofp:sessionID)
                      Project(sofp:partType)
                      Project(sofp:fault)
                      Project(sofp:fault)
                      Project(sofp:description)
                      Project(sofp:level)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
Loc:popup             Long
loc:poppedup          Long,static,thread
FilesOpened     Long
MANFAULO::State  USHORT
PARTS::State  USHORT
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
    Access:PARTS.ClearKey(par:Part_Number_Key)
    par:Ref_Number  = p_web.GSV('wob:RefNumber')
    Set(par:Part_Number_Key,par:Part_Number_Key)
    Loop
        If Access:PARTS.NEXT()
           Break
        End !If
        If par:Ref_Number  <> p_web.GSV('wob:RefNumber')      |
            Then Break.  ! End If
        If par:Fault_Code1 <> ''
            Local.ChargeableCode(1,par:fault_Code1)
        End !If par:Fault_Code1
        If par:Fault_Code2 <> ''
            Local.ChargeableCode(2,par:fault_Code2)
        End !If par:Fault_Code1
        If par:Fault_Code3 <> ''
            Local.ChargeableCode(3,par:fault_Code3)
        End !If par:Fault_Code1
        If par:Fault_Code4 <> ''
            Local.ChargeableCode(4,par:fault_Code4)
        End !If par:Fault_Code1
        If par:Fault_Code5 <> ''
            Local.ChargeableCode(5,par:fault_Code5)
        End !If par:Fault_Code1
        If par:Fault_Code6 <> ''
            Local.ChargeableCode(6,par:fault_Code6)
        End !If par:Fault_Code1
        If par:Fault_Code7 <> ''
            Local.ChargeableCode(7,par:fault_Code7)
        End !If par:Fault_Code1
        If par:Fault_Code8 <> ''
            Local.ChargeableCode(8,par:fault_Code8)
        End !If par:Fault_Code1
        If par:Fault_Code9 <> ''
            Local.ChargeableCode(9,par:fault_Code9)
        End !If par:Fault_Code1
        If par:Fault_Code10 <> ''
            Local.ChargeableCode(10,par:fault_Code10)
        End !If par:Fault_Code1
        If par:Fault_Code11 <> ''
            Local.ChargeableCode(11,par:fault_Code11)
        End !If par:Fault_Code1
        If par:Fault_Code12 <> ''
            Local.ChargeableCode(12,par:fault_Code12)
        End !If par:Fault_Code1
    End !Loop
  If p_web.GetSessionLoggedIn() = 0
    Return 
  End
  GlobalErrors.SetProcedureName('BrowseOutFaultsChargeableParts')
  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseOutFaultsChargeableParts:NoForm')
      loc:NoForm = p_web.GetValue('BrowseOutFaultsChargeableParts:NoForm')
      loc:FormName = p_web.GetValue('BrowseOutFaultsChargeableParts:FormName')
    else
      loc:FormName = 'BrowseOutFaultsChargeableParts_frm'
    End
    p_web.SSV('BrowseOutFaultsChargeableParts:NoForm',loc:NoForm)
    p_web.SSV('BrowseOutFaultsChargeableParts:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseOutFaultsChargeableParts:NoForm')
    loc:FormName = p_web.GSV('BrowseOutFaultsChargeableParts:FormName')
  end
  case p_web.site.formpopups
  of -1 ; loc:FormPopup = Net:Page
  of 0 ; loc:FormPopup = Net:Page
  of 1 ; loc:FormPopup = Net:Popup
  End
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseOutFaultsChargeableParts') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseOutFaultsChargeableParts')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if 1 = 2
  elsif p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  elsif p_web.IfExistsValue('_EIPRow_')
    do CallRow
  elsif p_web.GetValue('_CallPopups') > 0
    do CallPopups
  elsif p_web.IfExistsValue('_GetSecwinSettings')
      p_web.SetValue('Secwin_' & 'BrowseOutFaultsChargeableParts' & 'AccessGroupsArray','1') 
      p_web.SetValue('Secwin_AccessWindowName','BrowseOutFaultsChargeableParts')
      
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallPopups  Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
loc:CallPopups   Long
loc:name         String(255)
  code
  if loc:poppedup = 0
    loc:CallPopups = p_web.GetValue('_CallPopups')
    if loc:CallPopups = 1 !and p_web.GetPreCall('BrowseOutFaultsChargeableParts') = 0! for = 1 include popup divs and scripts for this browse
      !p_web.AddPreCall('BrowseOutFaultsChargeableParts')
      p_web.DivHeader('popup_BrowseOutFaultsChargeableParts','nt-hidden')
      p_web.DivHeader('BrowseOutFaultsChargeableParts',p_web.combine(p_web.site.style.browsediv,'fdiv'))
      p_web.DivFooter()
      p_web.DivFooter()
      loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(400)&', modal: true, position: [''center'',15]'
      packet =  p_web.jQuery('#' & lower('popup_BrowseOutFaultsChargeableParts_div'),'dialog',loc:options) & |
      p_web.jQuery('#' & lower('popup_BrowseOutFaultsChargeableParts_div'),'removeClass','''nt-hidden''')
    End
    ! for > 0 add in form, EIP-lookups and browse object
    do Popups
    loc:popup = 1
    do ClosingScripts
    do SendPacket
    loc:poppedup = 1
  end

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web.DivHeader('BrowseOutFaultsChargeableParts',p_web.combine(p_web.site.style.browsediv,'fdiv'))
  if loc:ParentSilent = 0
    do GenerateBrowse
    if p_web.RequestAjax = 1
      p_web.Script('BrowseProducts.pcv();')
    end
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web.DivFooter()
  do Children
  do Popups
  if loc:poppedup = 0
    do ClosingScripts
  end
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(SBO_OutFaultParts,sofp:FaultKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'SOFP:FAULT') then p_web.SetValue('BrowseOutFaultsChargeableParts_sort','1')
    ElsIf (loc:vorder = 'SOFP:DESCRIPTION') then p_web.SetValue('BrowseOutFaultsChargeableParts_sort','2')
    ElsIf (loc:vorder = 'SOFP:LEVEL') then p_web.SetValue('BrowseOutFaultsChargeableParts_sort','3')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseOutFaultsChargeableParts:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseOutFaultsChargeableParts:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseOutFaultsChargeableParts:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseOutFaultsChargeableParts:LookupField')
    loc:selecting = 0
  End

Popups Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  If loc:popup = 0
  End
SetFormAction  Routine
  loc:formaction = 'BrowseOutFaultsChargeableParts'

GotFocusBack   Routine

GenerateBrowse Routine
  data
loc:viewoptions  Long
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  ! Set general Browse options
  loc:NavButtonPosition   = Net:Below
  loc:UpdateButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:FileLoad
  loc:FillBack         = 0
  loc:Sorting          = Net:ServerSort
  if p_web.GetValue('_popup_') = 1
    loc:popup = 1
  end
  ! Set Locator Options
  loc:LocatorPosition  = Net:Below
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  p_web.ClearBrowse('BrowseOutFaultsChargeableParts')
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 10
  End
  loc:ActualSelection = ''
  !loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseOutFaultsChargeableParts_sort',net:DontEvaluate)
  p_web.SetSessionValue('BrowseOutFaultsChargeableParts_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(sofp:fault)','-UPPER(sofp:fault)')
    Loc:LocateField = 'sofp:fault'
    Loc:LocatorCase = 0
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(sofp:description)','-UPPER(sofp:description)')
    Loc:LocateField = 'sofp:description'
    Loc:LocatorCase = 0
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'sofp:level','-sofp:level')
    Loc:LocateField = 'sofp:level'
    Loc:LocatorCase = 0
  end
  if loc:vorder = ''
    loc:vorder = '+sofp:sessionID,+UPPER(sofp:partType),+UPPER(sofp:fault)'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('sofp:fault')
    loc:SortHeader = p_web.Translate('Fault')
    p_web.SetSessionValue('BrowseOutFaultsChargeableParts_LocatorPic','@s30')
  Of upper('sofp:description')
    loc:SortHeader = p_web.Translate('Description')
    p_web.SetSessionValue('BrowseOutFaultsChargeableParts_LocatorPic','@s60')
  Of upper('sofp:level')
    loc:SortHeader = p_web.Translate('Repair Index')
    p_web.SetSessionValue('BrowseOutFaultsChargeableParts_LocatorPic','@n8')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
  End
  If loc:selecting = 1
    p_web.GetSettings(p_web.GetSessionValue('Push1'))
    loc:selectaction = p_web.FormSettings.ParentPage !p_web.GetSessionValue('BrowseOutFaultsChargeableParts:LookupFrom')
  End!Else
  do SendPacket
  do SetFormAction
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseOutFaultsChargeableParts:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseOutFaultsChargeableParts:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseOutFaultsChargeableParts:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="SBO_OutFaultParts"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="sofp:FaultKey"></input><13,10>'
  end
  If p_web.Translate('Chargeable Parts') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.browseSubHeading,)&'">'&p_web.Translate('Chargeable Parts',0)&'</div>'&CRLF
  End
  case p_web.GetValue('refresh') ; of 'next' orof 'previous' orof 'first' orof 'last'
    p_web.Script('try{{BrowseOutFaultsChargeableParts.restoreFocus();} catch(err){{};')
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseOutFaultsChargeableParts',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      case loc:locatortype
      of Net:Position
      orof Net:Date
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
      of Net:Contains
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
      of Net:Begins
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
      End
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,'Locator')&'"><tr>' &|
      Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
      Case Loc:LocatorType
      of Net:Date
        packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator2BrowseOutFaultsChargeableParts','',p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,, p_web.GSV('BrowseOutFaultsChargeableParts_LocatorPic'),,,'onchange="BrowseOutFaultsChargeableParts.locate(''Locator2BrowseOutFaultsChargeableParts'',this.value);"',,0,,,) & '</div></td>'
      Else
        packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator2BrowseOutFaultsChargeableParts',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,'size="30" onchange="BrowseOutFaultsChargeableParts.locate(''Locator2BrowseOutFaultsChargeableParts'',this.value);" ',,,,,,) & '</td>'
      End
      If loc:LocatorSearchButton or loc:LocatorClearButton
        packet = clip(packet) & '<td><div id="BrowseOutFaultsChargeableParts_locate_a" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
        If loc:LocatorSearchButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
        End
        If loc:LocatorClearButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseOutFaultsChargeableParts.cl(''BrowseOutFaultsChargeableParts'');')
        End
        packet = clip(packet) & '</div><13,10></td><13,10>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
      If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'BrowseOutFaultsChargeableParts_locate_a','buttonset',loc:options)
      End
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & p_web.DivHeader('BrowseOutFaultsChargeableParts_table',p_web.Combine(p_web.site.style.BrowseTableDiv,''),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,,'sortable')&'" id="BrowseOutFaultsChargeableParts_tbl">'
  Else
    packet = clip(packet) & p_web.DivHeader('BrowseOutFaultsChargeableParts_table',p_web.Combine(p_web.site.style.BrowseTableDiv,''),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,)&'" id="BrowseOutFaultsChargeableParts_tbl">'
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'1','BrowseOutFaultsChargeableParts',p_web.Translate('Fault'),'Click here to sort by Fault',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'2','BrowseOutFaultsChargeableParts',p_web.Translate('Description'),'Click here to sort by Description',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'3','BrowseOutFaultsChargeableParts',p_web.Translate('Repair Index'),'Click here to sort by Repair Index',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,10,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  If Instring('sofp:sessionid',lower(loc:vorder),1,1) = 0 !and SBO_OutFaultParts{prop:SQLDriver} = 1
    loc:vorder = Choose(loc:vorder='','sofp:sessionID',clip(loc:vorder) & ',' & 'sofp:sessionID')
  End
  Loc:Selected = Choose(p_web.IfExistsValue('sofp:sessionID'),p_web.GetValue('sofp:sessionID'),p_web.GetSessionValue('sofp:sessionID'))
  If Instring('sofp:parttype',lower(loc:vorder),1,1) = 0 !and SBO_OutFaultParts{prop:SQLDriver} = 1
    loc:vorder = Choose(loc:vorder='','sofp:partType',clip(loc:vorder) & ',' & 'sofp:partType')
  End
  Loc:Selected = Choose(p_web.IfExistsValue('sofp:partType'),p_web.GetValue('sofp:partType'),p_web.GetSessionValue('sofp:partType'))
  If Instring('sofp:fault',lower(loc:vorder),1,1) = 0 !and SBO_OutFaultParts{prop:SQLDriver} = 1
    loc:vorder = Choose(loc:vorder='','sofp:fault',clip(loc:vorder) & ',' & 'sofp:fault')
  End
  Loc:Selected = Choose(p_web.IfExistsValue('sofp:fault'),p_web.GetValue('sofp:fault'),p_web.GetSessionValue('sofp:fault'))
  ThisView{prop:order} = p_web.CleanFilter(ThisView,clip(loc:vorder))
      loc:FilterWas = 'sofp:sessionID = ' & p_web.sessionID & ' and Upper(sofp:partType) = ''C'''
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseOutFaultsChargeableParts',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseOutFaultsChargeableParts_Filter')
    p_web.SetSessionValue('BrowseOutFaultsChargeableParts_FirstValue','')
    p_web.SetSessionValue('BrowseOutFaultsChargeableParts_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,SBO_OutFaultParts,sofp:FaultKey,loc:PageRows,'BrowseOutFaultsChargeableParts',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,loc:ViewOptions,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled,Loc:LocatorCase)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or Loc:LocatorType = Net:Date or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position or loc:LocatorType = Net:Date
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If SBO_OutFaultParts{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(SBO_OutFaultParts,loc:firstvalue)
              Reset(ThisView,SBO_OutFaultParts)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If SBO_OutFaultParts{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(SBO_OutFaultParts,loc:lastvalue)
            Reset(ThisView,SBO_OutFaultParts)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(sofp:fault)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td class="'&p_web.combine(p_web.site.style.BrowseEmpty,)&'">'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:NavButtonPosition=Net:Above or (loc:NavButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseOutFaultsChargeableParts_nav_a" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseOutFaultsChargeableParts.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseOutFaultsChargeableParts.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseOutFaultsChargeableParts.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseOutFaultsChargeableParts.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseOutFaultsChargeableParts_nav_a','buttonset',loc:options)
          End
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:UpdateButtonPosition=Net:Above or (loc:UpdateButtonPosition=Net:Both and loc:found > 0))
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseOutFaultsChargeableParts',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseOutFaultsChargeableParts_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseOutFaultsChargeableParts_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          case loc:locatortype
          of Net:Position
          orof Net:Date
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
          of Net:Contains
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
          of Net:Begins
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
          End
          packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,'Locator')&'"><tr>' &|
          Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
          Case Loc:LocatorType
          of Net:Date
            packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator1BrowseOutFaultsChargeableParts','',p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,, p_web.GSV('BrowseOutFaultsChargeableParts_LocatorPic'),,,'onchange="BrowseOutFaultsChargeableParts.locate(''Locator1BrowseOutFaultsChargeableParts'',this.value);"',,0,,,) & '</div></td>'
          Else
            packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator1BrowseOutFaultsChargeableParts',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,'size="30" onchange="BrowseOutFaultsChargeableParts.locate(''Locator1BrowseOutFaultsChargeableParts'',this.value);" ',,,,,,) & '</td>'
          End
          If loc:LocatorSearchButton or loc:LocatorClearButton
            packet = clip(packet) & '<td><div id="BrowseOutFaultsChargeableParts_locate_b" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
            If loc:LocatorSearchButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
            End
            If loc:LocatorClearButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseOutFaultsChargeableParts.cl(''BrowseOutFaultsChargeableParts'');')
            End
            packet = clip(packet) & '</div><13,10></td><13,10>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
          If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseOutFaultsChargeableParts_locate_b','buttonset',loc:options)
          End
      End
    End
  End
  p_web.SetSessionValue('BrowseOutFaultsChargeableParts_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseOutFaultsChargeableParts_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:NavButtonPosition=Net:Below or loc:NavButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseOutFaultsChargeableParts_nav_b" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseOutFaultsChargeableParts.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseOutFaultsChargeableParts.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseOutFaultsChargeableParts.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseOutFaultsChargeableParts.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseOutFaultsChargeableParts_nav_b','buttonset',loc:options)
          End
        end
        do SendPacket
  End
  If loc:UpdateButtonPosition=Net:Below or loc:UpdateButtonPosition=Net:Both
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
  data
loc:rowclick  string(1024)
  code
    loc:field = p_web.AddBrowseValue('BrowseOutFaultsChargeableParts','SBO_OutFaultParts',sofp:FaultKey) !sofp:fault
    p_web._thisrow = p_web._nocolon('sofp:fault')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if Loc:LocatorValue <> '' and loc:ActualSelection = ''
        loc:checked = 'checked'
        do SetSelection
      elsif loc:ActualSelection = '' and sofp:fault = p_web.GetValue('sofp:fault')
         loc:checked = 'checked'
         do SetSelection
      elsif loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseOutFaultsChargeableParts:LookupField')) = sofp:fault and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      else
        loc:checked = Choose((sofp:fault = loc:selected) and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      end
      If(loc:SelectionMethod  = Net:Radio)
        loc:RowStyle = 'class="' & p_web.combine(p_web.site.style.browserow,) &'"'
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'data-nt-id="'& p_web.AddBrowseValue('BrowseOutFaultsChargeableParts','SBO_OutFaultParts',sofp:FaultKey) &'" onclick="'
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If SBO_OutFaultParts{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(SBO_OutFaultParts)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            loc:DefaultSelection = loc:field
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If SBO_OutFaultParts{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(SBO_OutFaultParts)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            loc:DefaultSelection = loc:field
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'>'&p_web.CreateInput('radio','_bidv_',clip(loc:field),,loc:checked,'')&'</td>'&CRLF
          If loc:DefaultSelection = ''
            loc:DefaultSelection = loc:field
          End
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          If loc:DefaultSelection = '' or loc:direction < 0
            loc:DefaultSelection = loc:field
          End
        End
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::sofp:fault
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::sofp:description
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::sofp:level
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr ' & clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Idx = p_web.AddBrowseValue('BrowseOutFaultsChargeableParts','SBO_OutFaultParts',sofp:FaultKey)
  TableQueue.Id[1] = sofp:sessionID
  TableQueue.Id[2] = sofp:partType
  TableQueue.Id[3] = sofp:fault

ClosingScripts  Routine
  data
Rtn_SecwinProcedureName         string(252)
  code
  If p_web.RequestAjax = 0
    do SetFormAction
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var btiBrowseOutFaultsChargeableParts;if (btiBrowseOutFaultsChargeableParts != 1){{var BrowseOutFaultsChargeableParts=new browseTable(''BrowseOutFaultsChargeableParts'','''&clip(loc:formname)&|
      ''','''&p_web._jsok('sofp:sessionID',Net:Parameter)&''',2,'''&clip(loc:divname)&''',1,1,1,'''&|
      clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&|
      p_web.Translate('Are you sure you want to delete this record?')&''','''&clip(loc:ActualSelection)&''','''&clip(loc:selectaction)&''','''&|
      clip(loc:formactiontarget)&''','''','&loc:popup&',0,'''','''',"' & clip(Rtn_SecwinProcedureName) & '");<13,10>'&|
      'BrowseOutFaultsChargeableParts.setGreenBar('''&p_web.ColorWeb(p_web.Site.Style.BrowseHighlightColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOneColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseTwoColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOverColor)&''');<13,10>' &|
      'BrowseOutFaultsChargeableParts.applyGreenBar();btiBrowseOutFaultsChargeableParts=1};<13,10>' &|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseOutFaultsChargeableParts')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseOutFaultsChargeableParts')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseOutFaultsChargeableParts')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseOutFaultsChargeableParts')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(SBO_OutFaultParts)
  p_web._CloseFile(MANFAULO)
  p_web._CloseFile(PARTS)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(SBO_OutFaultParts)
  Bind(sofp:Record)
  Clear(sofp:Record)
  NetWebSetSessionPics(p_web,SBO_OutFaultParts)
  p_web._OpenFile(MANFAULO)
  Bind(mfo:Record)
  NetWebSetSessionPics(p_web,MANFAULO)
  p_web._OpenFile(PARTS)
  Bind(par:Record)
  NetWebSetSessionPics(p_web,PARTS)

Children Routine
  if loc:selecting = 0
    If p_web.RequestAjax = 0
      do StartChildren
    Else
      do AjaxChildren
    End
  end

AjaxChildren  Routine
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue('sofp:sessionID',p_web.GetValue('sofp:sessionID'))
  p_web.SetSessionValue('sofp:partType',p_web.GetValue('sofp:partType'))
  p_web.SetSessionValue('sofp:fault',p_web.GetValue('sofp:fault'))
   p_web.DeleteValue('_Clicked')
   loc:found = 1
   do Children
! ----------------------------------------------------------------------------------------
CallRow  Routine
  data
loc:result  long
  code
  do OpenFilesB
  sofp:sessionID = p_web.GSV('sofp:sessionID')
  sofp:partType = p_web.GSV('sofp:partType')
  sofp:fault = p_web.GSV('sofp:fault')
  loc:result = p_web._GetFile(SBO_OutFaultParts,sofp:FaultKey)
  loc:eip = 1
  loc:viewstate = p_web.escape(p_web.Base64Encode(clip(sofp:sessionID)))
  do BrowseRow
  do SendPacket
  do ClosefilesB

! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(SBO_OutFaultParts)
  Case upper(p_web.GetValue('_EIPClm'))
  Else
    case p_web.GetValue('_action_')
    of Net:InsertRecord
    orof Net:ChangeRecord
    orof Net:DeleteRecord
    orof Net:CopyRecord
      loc:eip = 0
      do CallBrowse
    end
  End
  do GotFocusBack
  p_web._CloseFile(SBO_OutFaultParts)
! ----------------------------------------------------------------------------------------
value::sofp:fault   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseOutFaultsChargeableParts_sofp:fault_'&sofp:fault,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(sofp:fault,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sofp:description   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseOutFaultsChargeableParts_sofp:description_'&sofp:fault,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(sofp:description,'@s60')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sofp:level   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseOutFaultsChargeableParts_sofp:level_'&sofp:fault,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(sofp:level,'@n8')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  p_web._OpenFile(MANFAULO)
  p_web._OpenFile(PARTS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(MANFAULO)
  p_Web._CloseFile(PARTS)
     FilesOpened = False
  END
  return
!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
CheckForDuplicate  Routine
SetSelection  Routine
  loc:ActualSelection = loc:field
  p_web.SetSessionValue('sofp:sessionID',sofp:sessionID)
  p_web.SetSessionValue('sofp:partType',sofp:partType)
  p_web.SetSessionValue('sofp:fault',sofp:fault)

MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:ActualSelection = ''
    loc:ActualSelection = loc:DefaultSelection
    p_web.GetBrowseValue(loc:ActualSelection,Net:Web:SessionQueue) ! so children are primed with correct sessionValue
  End

  If loc:ActualSelection <> ''
    TableQueue.Kind = Net:RowTable
    get(TableQueue,TableQueue.Kind)
    if Errorcode() = 0
      TableQueue.Row = sub(TableQueue.Row,1,len(clip(TableQueue.Row))-1) &  ' data-nt-rows="'&loc:RowsHigh&'" data-nt-value="'&clip(loc:ActualSelection)&'"' & '>'
      Put(TableQueue)
    End
  End

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead class="'&p_web.combine(p_web.site.style.BrowseHeader,'')&'"><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot class="'&p_web.combine(p_web.site.style.BrowseFooter,'BrowseFooter')&'"><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody class="'&p_web.combine(p_web.site.style.BrowseBody,)&'"><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table><13,10>' & p_web.DivFooter(Net:NoSend) ! Table Div
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
Local.ChargeableCode      Procedure(Long func:FieldNumber,String func:Field)
Code
    !Is this fault code a "Main Fault"
    Access:MANFAUPA.ClearKey(map:Field_Number_Key)
    map:Manufacturer = p_web.GSV('job:manufacturer')
    map:Field_Number = func:FieldNumber
    If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
        !Found
        If map:MainFault

            !Ok, get the details from the Job "Main Fault"
            Access:MANFAULT.ClearKey(maf:MainFaultKey)
            maf:Manufacturer = p_web.GSV('job:manufacturer')
            maf:MainFault    = 1
            If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                !Found
                Access:MANFAULO.ClearKey(mfo:Field_Key)
                mfo:Manufacturer = p_web.GSV('job:manufacturer')
                mfo:Field_Number = maf:Field_Number
                mfo:Field        = func:Field
                Set(mfo:Field_Key,mfo:Field_Key)
                Loop
                    If Access:MANFAULO.NEXT()
                       Break
                    End !If
                    If mfo:Manufacturer <> p_web.GSV('job:manufacturer')      |
                    Or mfo:Field_Number <> maf:Field_Number      |
                    Or mfo:Field        <> func:Field      |
                        Then Break.  ! End If
                    If mfo:RelatedPartCode <> 0
                        If mfo:RelatedPartCode <> func:FieldNumber
                            Cycle
                        End !If mfo:RelatedPartCode <> func:FieldNumber
                    !This 'END' was at the bottom which would
                    !mean only Ericsson faults would ever appear - 234694 (DBH: 25-07-2003)
                    End !If mfo:RelatedPartCode <> 0
                    if (mfo:NotAvailable)
                        ! #11655 Don't show "Not Available" Fault Codes (Bryan: 23/08/2010)
                        CYCLE
                    END



                    Access:SBO_OUTFAULTPARTS.Clearkey(sofp:faultKey)
                    sofp:sessionID    = p_web.sessionID
                    sofp:partType    = 'C'
                    sofp:fault    = func:field
                    if (Access:SBO_OUTFAULTPARTS.TryFetch(sofp:faultKey) = Level:Benign)
                        ! Found
                    else ! if (Access:SBO_OUTFAULTPARTS.TryFetch(sofp:faultKey) = Level:Benign)
                        ! Error
                        if (Access:SBO_OUTFAULTPARTS.PrimeRecord() = Level:Benign)
                            sofp:sessionID    = p_web.sessionID
                            sofp:partType    = 'C'
                            sofp:fault    = func:Field
                            sofp:description    = mfo:description
                            sofp:level    = mfo:importancelevel

                            if (Access:SBO_OUTFAULTPARTS.TryInsert() = Level:Benign)
                                ! Inserted
                            else ! if (Access:SBO_OUTFAULTPARTS.TryInsert() = Level:Benign)
                                ! Error
                            end ! if (Access:SBO_OUTFAULTPARTS.TryInsert() = Level:Benign)
                        end ! if (Access:SBO_OUTFAULTPARTS.PrimeRecord() = Level:Benign)
                    end ! if (Access:SBO_OUTFAULTPARTS.TryFetch(sofp:faultKey) = Level:Benign)
                    Break
                End !Loop
            Else!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
        Else !If map:MainFault

        End !If map:MainFault
    Else!If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End            !If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
BrowseOutFaultsWarrantyParts PROCEDURE  (NetWebServerWorker p_web)
Local                CLASS
WarrantyCode         Procedure(Long func:FieldNumber,String func:Field)
                     END
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256),dim(Net:MaxKeyFields)
idx           String(Net:HashSize)
sub           Long
            End
sofp:fault:IsInvalid  Long
sofp:description:IsInvalid  Long
sofp:level:IsInvalid  Long
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:DefaultSelection    String(Net:HashSize)
loc:ActualSelection     String(Net:HashSize)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
!loc:pagename            String(256)
loc:NavButtonPosition    Long
loc:UpdateButtonPosition Long
loc:SelectionMethod      Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorCase         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
loc:lookupdone          Long
loc:FormPopup           Long
ThisView            View(SBO_OutFaultParts)
                      Project(sofp:sessionID)
                      Project(sofp:partType)
                      Project(sofp:fault)
                      Project(sofp:fault)
                      Project(sofp:description)
                      Project(sofp:level)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
Loc:popup             Long
loc:poppedup          Long,static,thread
FilesOpened     Long
MANFAULO::State  USHORT
WARPARTS::State  USHORT
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
    Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
    wpr:Ref_Number  = p_web.GSV('wob:RefNumber')
    Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
    Loop
        If Access:WARPARTS.NEXT()
           Break
        End !If
        If wpr:Ref_Number  <> p_web.GSV('wob:RefNumber')      |
            Then Break.  ! End If
        If wpr:Fault_Code1 <> ''
            Local.WarrantyCode(1,wpr:fault_Code1)
        End !If wpr:Fault_Code1
        If wpr:Fault_Code2 <> ''
            Local.WarrantyCode(2,wpr:fault_Code2)
        End !If wpr:Fault_Code1
        If wpr:Fault_Code3 <> ''
            Local.WarrantyCode(3,wpr:fault_Code3)
        End !If wpr:Fault_Code1
        If wpr:Fault_Code4 <> ''
            Local.WarrantyCode(4,wpr:fault_Code4)
        End !If wpr:Fault_Code1
        If wpr:Fault_Code5 <> ''
            Local.WarrantyCode(5,wpr:fault_Code5)
        End !If wpr:Fault_Code1
        If wpr:Fault_Code6 <> ''
            Local.WarrantyCode(6,wpr:fault_Code6)
        End !If wpr:Fault_Code1
        If wpr:Fault_Code7 <> ''
            Local.WarrantyCode(7,wpr:fault_Code7)
        End !If wpr:Fault_Code1
        If wpr:Fault_Code8 <> ''
            Local.WarrantyCode(8,wpr:fault_Code8)
        End !If wpr:Fault_Code1
        If wpr:Fault_Code9 <> ''
            Local.WarrantyCode(9,wpr:fault_Code9)
        End !If wpr:Fault_Code1
        If wpr:Fault_Code10 <> ''
            Local.WarrantyCode(10,wpr:fault_Code10)
        End !If wpr:Fault_Code1
        If wpr:Fault_Code11 <> ''
            Local.WarrantyCode(11,wpr:fault_Code11)
        End !If wpr:Fault_Code1
        If wpr:Fault_Code12 <> ''
            Local.WarrantyCode(12,wpr:fault_Code12)
        End !If wpr:Fault_Code1
    End !Loop
  If p_web.GetSessionLoggedIn() = 0
    Return 
  End
  GlobalErrors.SetProcedureName('BrowseOutFaultsWarrantyParts')
  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseOutFaultsWarrantyParts:NoForm')
      loc:NoForm = p_web.GetValue('BrowseOutFaultsWarrantyParts:NoForm')
      loc:FormName = p_web.GetValue('BrowseOutFaultsWarrantyParts:FormName')
    else
      loc:FormName = 'BrowseOutFaultsWarrantyParts_frm'
    End
    p_web.SSV('BrowseOutFaultsWarrantyParts:NoForm',loc:NoForm)
    p_web.SSV('BrowseOutFaultsWarrantyParts:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseOutFaultsWarrantyParts:NoForm')
    loc:FormName = p_web.GSV('BrowseOutFaultsWarrantyParts:FormName')
  end
  case p_web.site.formpopups
  of -1 ; loc:FormPopup = Net:Page
  of 0 ; loc:FormPopup = Net:Page
  of 1 ; loc:FormPopup = Net:Popup
  End
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseOutFaultsWarrantyParts') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseOutFaultsWarrantyParts')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if 1 = 2
  elsif p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  elsif p_web.IfExistsValue('_EIPRow_')
    do CallRow
  elsif p_web.GetValue('_CallPopups') > 0
    do CallPopups
  elsif p_web.IfExistsValue('_GetSecwinSettings')
      p_web.SetValue('Secwin_' & 'BrowseOutFaultsWarrantyParts' & 'AccessGroupsArray','1') 
      p_web.SetValue('Secwin_AccessWindowName','BrowseOutFaultsWarrantyParts')
      
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallPopups  Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
loc:CallPopups   Long
loc:name         String(255)
  code
  if loc:poppedup = 0
    loc:CallPopups = p_web.GetValue('_CallPopups')
    if loc:CallPopups = 1 !and p_web.GetPreCall('BrowseOutFaultsWarrantyParts') = 0! for = 1 include popup divs and scripts for this browse
      !p_web.AddPreCall('BrowseOutFaultsWarrantyParts')
      p_web.DivHeader('popup_BrowseOutFaultsWarrantyParts','nt-hidden')
      p_web.DivHeader('BrowseOutFaultsWarrantyParts',p_web.combine(p_web.site.style.browsediv,'fdiv'))
      p_web.DivFooter()
      p_web.DivFooter()
      loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(400)&', modal: true, position: [''center'',15]'
      packet =  p_web.jQuery('#' & lower('popup_BrowseOutFaultsWarrantyParts_div'),'dialog',loc:options) & |
      p_web.jQuery('#' & lower('popup_BrowseOutFaultsWarrantyParts_div'),'removeClass','''nt-hidden''')
    End
    ! for > 0 add in form, EIP-lookups and browse object
    do Popups
    loc:popup = 1
    do ClosingScripts
    do SendPacket
    loc:poppedup = 1
  end

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web.DivHeader('BrowseOutFaultsWarrantyParts',p_web.combine(p_web.site.style.browsediv,'fdiv'))
  if loc:ParentSilent = 0
    do GenerateBrowse
    if p_web.RequestAjax = 1
      p_web.Script('BrowseProducts.pcv();')
    end
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web.DivFooter()
  do Children
  do Popups
  if loc:poppedup = 0
    do ClosingScripts
  end
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(SBO_OutFaultParts,sofp:FaultKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'SOFP:FAULT') then p_web.SetValue('BrowseOutFaultsWarrantyParts_sort','1')
    ElsIf (loc:vorder = 'SOFP:DESCRIPTION') then p_web.SetValue('BrowseOutFaultsWarrantyParts_sort','2')
    ElsIf (loc:vorder = 'SOFP:LEVEL') then p_web.SetValue('BrowseOutFaultsWarrantyParts_sort','3')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseOutFaultsWarrantyParts:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseOutFaultsWarrantyParts:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseOutFaultsWarrantyParts:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseOutFaultsWarrantyParts:LookupField')
    loc:selecting = 0
  End

Popups Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  If loc:popup = 0
  End
SetFormAction  Routine
  loc:formaction = 'BrowseOutFaultsWarrantyParts'

GotFocusBack   Routine

GenerateBrowse Routine
  data
loc:viewoptions  Long
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  ! Set general Browse options
  loc:NavButtonPosition   = Net:Below
  loc:UpdateButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:FileLoad
  loc:FillBack         = 0
  loc:Sorting          = Net:ServerSort
  if p_web.GetValue('_popup_') = 1
    loc:popup = 1
  end
  ! Set Locator Options
  loc:LocatorPosition  = Net:Below
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  p_web.ClearBrowse('BrowseOutFaultsWarrantyParts')
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 10
  End
  loc:ActualSelection = ''
  !loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseOutFaultsWarrantyParts_sort',net:DontEvaluate)
  p_web.SetSessionValue('BrowseOutFaultsWarrantyParts_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(sofp:fault)','-UPPER(sofp:fault)')
    Loc:LocateField = 'sofp:fault'
    Loc:LocatorCase = 0
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(sofp:description)','-UPPER(sofp:description)')
    Loc:LocateField = 'sofp:description'
    Loc:LocatorCase = 0
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'sofp:level','-sofp:level')
    Loc:LocateField = 'sofp:level'
    Loc:LocatorCase = 0
  end
  if loc:vorder = ''
    loc:vorder = '+sofp:sessionID,+UPPER(sofp:partType),+UPPER(sofp:fault)'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('sofp:fault')
    loc:SortHeader = p_web.Translate('Fault')
    p_web.SetSessionValue('BrowseOutFaultsWarrantyParts_LocatorPic','@s30')
  Of upper('sofp:description')
    loc:SortHeader = p_web.Translate('Description')
    p_web.SetSessionValue('BrowseOutFaultsWarrantyParts_LocatorPic','@s60')
  Of upper('sofp:level')
    loc:SortHeader = p_web.Translate('Repair Index')
    p_web.SetSessionValue('BrowseOutFaultsWarrantyParts_LocatorPic','@n8')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
  End
  If loc:selecting = 1
    p_web.GetSettings(p_web.GetSessionValue('Push1'))
    loc:selectaction = p_web.FormSettings.ParentPage !p_web.GetSessionValue('BrowseOutFaultsWarrantyParts:LookupFrom')
  End!Else
  do SendPacket
  do SetFormAction
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseOutFaultsWarrantyParts:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseOutFaultsWarrantyParts:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseOutFaultsWarrantyParts:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="SBO_OutFaultParts"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="sofp:FaultKey"></input><13,10>'
  end
  If p_web.Translate('Warranty Parts') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.browseSubHeading,)&'">'&p_web.Translate('Warranty Parts',0)&'</div>'&CRLF
  End
  case p_web.GetValue('refresh') ; of 'next' orof 'previous' orof 'first' orof 'last'
    p_web.Script('try{{BrowseOutFaultsWarrantyParts.restoreFocus();} catch(err){{};')
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseOutFaultsWarrantyParts',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      case loc:locatortype
      of Net:Position
      orof Net:Date
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
      of Net:Contains
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
      of Net:Begins
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
      End
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,'Locator')&'"><tr>' &|
      Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
      Case Loc:LocatorType
      of Net:Date
        packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator2BrowseOutFaultsWarrantyParts','',p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,, p_web.GSV('BrowseOutFaultsWarrantyParts_LocatorPic'),,,'onchange="BrowseOutFaultsWarrantyParts.locate(''Locator2BrowseOutFaultsWarrantyParts'',this.value);"',,0,,,) & '</div></td>'
      Else
        packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator2BrowseOutFaultsWarrantyParts',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,'size="30" onchange="BrowseOutFaultsWarrantyParts.locate(''Locator2BrowseOutFaultsWarrantyParts'',this.value);" ',,,,,,) & '</td>'
      End
      If loc:LocatorSearchButton or loc:LocatorClearButton
        packet = clip(packet) & '<td><div id="BrowseOutFaultsWarrantyParts_locate_a" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
        If loc:LocatorSearchButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
        End
        If loc:LocatorClearButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseOutFaultsWarrantyParts.cl(''BrowseOutFaultsWarrantyParts'');')
        End
        packet = clip(packet) & '</div><13,10></td><13,10>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
      If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'BrowseOutFaultsWarrantyParts_locate_a','buttonset',loc:options)
      End
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & p_web.DivHeader('BrowseOutFaultsWarrantyParts_table',p_web.Combine(p_web.site.style.BrowseTableDiv,''),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,,'sortable')&'" id="BrowseOutFaultsWarrantyParts_tbl">'
  Else
    packet = clip(packet) & p_web.DivHeader('BrowseOutFaultsWarrantyParts_table',p_web.Combine(p_web.site.style.BrowseTableDiv,''),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,)&'" id="BrowseOutFaultsWarrantyParts_tbl">'
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'1','BrowseOutFaultsWarrantyParts',p_web.Translate('Fault'),'Click here to sort by Fault',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'2','BrowseOutFaultsWarrantyParts',p_web.Translate('Description'),'Click here to sort by Description',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'3','BrowseOutFaultsWarrantyParts',p_web.Translate('Repair Index'),'Click here to sort by Repair Index',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,10,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  If Instring('sofp:sessionid',lower(loc:vorder),1,1) = 0 !and SBO_OutFaultParts{prop:SQLDriver} = 1
    loc:vorder = Choose(loc:vorder='','sofp:sessionID',clip(loc:vorder) & ',' & 'sofp:sessionID')
  End
  Loc:Selected = Choose(p_web.IfExistsValue('sofp:sessionID'),p_web.GetValue('sofp:sessionID'),p_web.GetSessionValue('sofp:sessionID'))
  If Instring('sofp:parttype',lower(loc:vorder),1,1) = 0 !and SBO_OutFaultParts{prop:SQLDriver} = 1
    loc:vorder = Choose(loc:vorder='','sofp:partType',clip(loc:vorder) & ',' & 'sofp:partType')
  End
  Loc:Selected = Choose(p_web.IfExistsValue('sofp:partType'),p_web.GetValue('sofp:partType'),p_web.GetSessionValue('sofp:partType'))
  If Instring('sofp:fault',lower(loc:vorder),1,1) = 0 !and SBO_OutFaultParts{prop:SQLDriver} = 1
    loc:vorder = Choose(loc:vorder='','sofp:fault',clip(loc:vorder) & ',' & 'sofp:fault')
  End
  Loc:Selected = Choose(p_web.IfExistsValue('sofp:fault'),p_web.GetValue('sofp:fault'),p_web.GetSessionValue('sofp:fault'))
  ThisView{prop:order} = p_web.CleanFilter(ThisView,clip(loc:vorder))
      loc:FilterWas = 'sofp:sessionID = ' & p_web.sessionID & ' and Upper(sofp:partType) = ''W'''
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseOutFaultsWarrantyParts',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseOutFaultsWarrantyParts_Filter')
    p_web.SetSessionValue('BrowseOutFaultsWarrantyParts_FirstValue','')
    p_web.SetSessionValue('BrowseOutFaultsWarrantyParts_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,SBO_OutFaultParts,sofp:FaultKey,loc:PageRows,'BrowseOutFaultsWarrantyParts',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,loc:ViewOptions,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled,Loc:LocatorCase)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or Loc:LocatorType = Net:Date or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position or loc:LocatorType = Net:Date
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If SBO_OutFaultParts{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(SBO_OutFaultParts,loc:firstvalue)
              Reset(ThisView,SBO_OutFaultParts)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If SBO_OutFaultParts{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(SBO_OutFaultParts,loc:lastvalue)
            Reset(ThisView,SBO_OutFaultParts)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(sofp:fault)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td class="'&p_web.combine(p_web.site.style.BrowseEmpty,)&'">'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:NavButtonPosition=Net:Above or (loc:NavButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseOutFaultsWarrantyParts_nav_a" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseOutFaultsWarrantyParts.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseOutFaultsWarrantyParts.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseOutFaultsWarrantyParts.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseOutFaultsWarrantyParts.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseOutFaultsWarrantyParts_nav_a','buttonset',loc:options)
          End
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:UpdateButtonPosition=Net:Above or (loc:UpdateButtonPosition=Net:Both and loc:found > 0))
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseOutFaultsWarrantyParts',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseOutFaultsWarrantyParts_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseOutFaultsWarrantyParts_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          case loc:locatortype
          of Net:Position
          orof Net:Date
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
          of Net:Contains
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
          of Net:Begins
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
          End
          packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,'Locator')&'"><tr>' &|
          Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
          Case Loc:LocatorType
          of Net:Date
            packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator1BrowseOutFaultsWarrantyParts','',p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,, p_web.GSV('BrowseOutFaultsWarrantyParts_LocatorPic'),,,'onchange="BrowseOutFaultsWarrantyParts.locate(''Locator1BrowseOutFaultsWarrantyParts'',this.value);"',,0,,,) & '</div></td>'
          Else
            packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator1BrowseOutFaultsWarrantyParts',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,'size="30" onchange="BrowseOutFaultsWarrantyParts.locate(''Locator1BrowseOutFaultsWarrantyParts'',this.value);" ',,,,,,) & '</td>'
          End
          If loc:LocatorSearchButton or loc:LocatorClearButton
            packet = clip(packet) & '<td><div id="BrowseOutFaultsWarrantyParts_locate_b" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
            If loc:LocatorSearchButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
            End
            If loc:LocatorClearButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseOutFaultsWarrantyParts.cl(''BrowseOutFaultsWarrantyParts'');')
            End
            packet = clip(packet) & '</div><13,10></td><13,10>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
          If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseOutFaultsWarrantyParts_locate_b','buttonset',loc:options)
          End
      End
    End
  End
  p_web.SetSessionValue('BrowseOutFaultsWarrantyParts_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseOutFaultsWarrantyParts_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:NavButtonPosition=Net:Below or loc:NavButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseOutFaultsWarrantyParts_nav_b" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseOutFaultsWarrantyParts.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseOutFaultsWarrantyParts.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseOutFaultsWarrantyParts.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseOutFaultsWarrantyParts.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseOutFaultsWarrantyParts_nav_b','buttonset',loc:options)
          End
        end
        do SendPacket
  End
  If loc:UpdateButtonPosition=Net:Below or loc:UpdateButtonPosition=Net:Both
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
  data
loc:rowclick  string(1024)
  code
    loc:field = p_web.AddBrowseValue('BrowseOutFaultsWarrantyParts','SBO_OutFaultParts',sofp:FaultKey) !sofp:fault
    p_web._thisrow = p_web._nocolon('sofp:fault')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if Loc:LocatorValue <> '' and loc:ActualSelection = ''
        loc:checked = 'checked'
        do SetSelection
      elsif loc:ActualSelection = '' and sofp:fault = p_web.GetValue('sofp:fault')
         loc:checked = 'checked'
         do SetSelection
      elsif loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseOutFaultsWarrantyParts:LookupField')) = sofp:fault and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      else
        loc:checked = Choose((sofp:fault = loc:selected) and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      end
      If(loc:SelectionMethod  = Net:Radio)
        loc:RowStyle = 'class="' & p_web.combine(p_web.site.style.browserow,) &'"'
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'data-nt-id="'& p_web.AddBrowseValue('BrowseOutFaultsWarrantyParts','SBO_OutFaultParts',sofp:FaultKey) &'" onclick="'
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If SBO_OutFaultParts{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(SBO_OutFaultParts)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            loc:DefaultSelection = loc:field
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If SBO_OutFaultParts{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(SBO_OutFaultParts)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            loc:DefaultSelection = loc:field
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'>'&p_web.CreateInput('radio','_bidv_',clip(loc:field),,loc:checked,'')&'</td>'&CRLF
          If loc:DefaultSelection = ''
            loc:DefaultSelection = loc:field
          End
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          If loc:DefaultSelection = '' or loc:direction < 0
            loc:DefaultSelection = loc:field
          End
        End
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::sofp:fault
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::sofp:description
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::sofp:level
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr ' & clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Idx = p_web.AddBrowseValue('BrowseOutFaultsWarrantyParts','SBO_OutFaultParts',sofp:FaultKey)
  TableQueue.Id[1] = sofp:sessionID
  TableQueue.Id[2] = sofp:partType
  TableQueue.Id[3] = sofp:fault

ClosingScripts  Routine
  data
Rtn_SecwinProcedureName         string(252)
  code
  If p_web.RequestAjax = 0
    do SetFormAction
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var btiBrowseOutFaultsWarrantyParts;if (btiBrowseOutFaultsWarrantyParts != 1){{var BrowseOutFaultsWarrantyParts=new browseTable(''BrowseOutFaultsWarrantyParts'','''&clip(loc:formname)&|
      ''','''&p_web._jsok('sofp:sessionID',Net:Parameter)&''',2,'''&clip(loc:divname)&''',1,1,1,'''&|
      clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&|
      p_web.Translate('Are you sure you want to delete this record?')&''','''&clip(loc:ActualSelection)&''','''&clip(loc:selectaction)&''','''&|
      clip(loc:formactiontarget)&''','''','&loc:popup&',0,'''','''',"' & clip(Rtn_SecwinProcedureName) & '");<13,10>'&|
      'BrowseOutFaultsWarrantyParts.setGreenBar('''&p_web.ColorWeb(p_web.Site.Style.BrowseHighlightColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOneColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseTwoColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOverColor)&''');<13,10>' &|
      'BrowseOutFaultsWarrantyParts.applyGreenBar();btiBrowseOutFaultsWarrantyParts=1};<13,10>' &|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseOutFaultsWarrantyParts')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseOutFaultsWarrantyParts')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseOutFaultsWarrantyParts')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseOutFaultsWarrantyParts')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(SBO_OutFaultParts)
  p_web._CloseFile(MANFAULO)
  p_web._CloseFile(WARPARTS)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(SBO_OutFaultParts)
  Bind(sofp:Record)
  Clear(sofp:Record)
  NetWebSetSessionPics(p_web,SBO_OutFaultParts)
  p_web._OpenFile(MANFAULO)
  Bind(mfo:Record)
  NetWebSetSessionPics(p_web,MANFAULO)
  p_web._OpenFile(WARPARTS)
  Bind(wpr:Record)
  NetWebSetSessionPics(p_web,WARPARTS)

Children Routine
  if loc:selecting = 0
    If p_web.RequestAjax = 0
      do StartChildren
    Else
      do AjaxChildren
    End
  end

AjaxChildren  Routine
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue('sofp:sessionID',p_web.GetValue('sofp:sessionID'))
  p_web.SetSessionValue('sofp:partType',p_web.GetValue('sofp:partType'))
  p_web.SetSessionValue('sofp:fault',p_web.GetValue('sofp:fault'))
   p_web.DeleteValue('_Clicked')
   loc:found = 1
   do Children
! ----------------------------------------------------------------------------------------
CallRow  Routine
  data
loc:result  long
  code
  do OpenFilesB
  sofp:sessionID = p_web.GSV('sofp:sessionID')
  sofp:partType = p_web.GSV('sofp:partType')
  sofp:fault = p_web.GSV('sofp:fault')
  loc:result = p_web._GetFile(SBO_OutFaultParts,sofp:FaultKey)
  loc:eip = 1
  loc:viewstate = p_web.escape(p_web.Base64Encode(clip(sofp:sessionID)))
  do BrowseRow
  do SendPacket
  do ClosefilesB

! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(SBO_OutFaultParts)
  Case upper(p_web.GetValue('_EIPClm'))
  Else
    case p_web.GetValue('_action_')
    of Net:InsertRecord
    orof Net:ChangeRecord
    orof Net:DeleteRecord
    orof Net:CopyRecord
      loc:eip = 0
      do CallBrowse
    end
  End
  do GotFocusBack
  p_web._CloseFile(SBO_OutFaultParts)
! ----------------------------------------------------------------------------------------
value::sofp:fault   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseOutFaultsWarrantyParts_sofp:fault_'&sofp:fault,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(sofp:fault,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sofp:description   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseOutFaultsWarrantyParts_sofp:description_'&sofp:fault,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(sofp:description,'@s60')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sofp:level   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseOutFaultsWarrantyParts_sofp:level_'&sofp:fault,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(sofp:level,'@n8')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  p_web._OpenFile(MANFAULO)
  p_web._OpenFile(WARPARTS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(MANFAULO)
  p_Web._CloseFile(WARPARTS)
     FilesOpened = False
  END
  return
!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
CheckForDuplicate  Routine
SetSelection  Routine
  loc:ActualSelection = loc:field
  p_web.SetSessionValue('sofp:sessionID',sofp:sessionID)
  p_web.SetSessionValue('sofp:partType',sofp:partType)
  p_web.SetSessionValue('sofp:fault',sofp:fault)

MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:ActualSelection = ''
    loc:ActualSelection = loc:DefaultSelection
    p_web.GetBrowseValue(loc:ActualSelection,Net:Web:SessionQueue) ! so children are primed with correct sessionValue
  End

  If loc:ActualSelection <> ''
    TableQueue.Kind = Net:RowTable
    get(TableQueue,TableQueue.Kind)
    if Errorcode() = 0
      TableQueue.Row = sub(TableQueue.Row,1,len(clip(TableQueue.Row))-1) &  ' data-nt-rows="'&loc:RowsHigh&'" data-nt-value="'&clip(loc:ActualSelection)&'"' & '>'
      Put(TableQueue)
    End
  End

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead class="'&p_web.combine(p_web.site.style.BrowseHeader,'')&'"><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot class="'&p_web.combine(p_web.site.style.BrowseFooter,'BrowseFooter')&'"><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody class="'&p_web.combine(p_web.site.style.BrowseBody,)&'"><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table><13,10>' & p_web.DivFooter(Net:NoSend) ! Table Div
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
Local.WarrantyCode      Procedure(Long func:FieldNumber,String func:Field)
Code
    !Is this fault code a "Main Fault"
    Access:MANFAUPA.ClearKey(map:Field_Number_Key)
    map:Manufacturer = p_web.GSV('job:manufacturer')
    map:Field_Number = func:FieldNumber
    If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
        !Found
        If map:MainFault

            !Ok, get the details from the Job "Main Fault"
            Access:MANFAULT.ClearKey(maf:MainFaultKey)
            maf:Manufacturer = p_web.GSV('job:manufacturer')
            maf:MainFault    = 1
            If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                !Found
                Access:MANFAULO.ClearKey(mfo:Field_Key)
                mfo:Manufacturer = p_web.GSV('job:manufacturer')
                mfo:Field_Number = maf:Field_Number
                mfo:Field        = func:Field
                Set(mfo:Field_Key,mfo:Field_Key)
                Loop
                    If Access:MANFAULO.NEXT()
                       Break
                    End !If
                    If mfo:Manufacturer <> p_web.GSV('job:manufacturer')      |
                    Or mfo:Field_Number <> maf:Field_Number      |
                    Or mfo:Field        <> func:Field      |
                        Then Break.  ! End If
                    If mfo:RelatedPartCode <> 0
                        If mfo:RelatedPartCode <> func:FieldNumber
                            Cycle
                        End !If mfo:RelatedPartCode <> func:FieldNumber
                    !This 'END' was at the bottom which would
                    !mean only Ericsson faults would ever appear - 234694 (DBH: 25-07-2003)
                    End !If mfo:RelatedPartCode <> 0

                    if (mfo:NotAvailable)
                        ! #11655 Don't show "Not Available" Fault Codes (Bryan: 23/08/2010)
                        CYCLE
                    END


                    Access:SBO_OUTFAULTPARTS.Clearkey(sofp:faultKey)
                    sofp:sessionID    = p_web.sessionID
                    sofp:partType    = 'W'
                    sofp:fault    = func:field
                    if (Access:SBO_OUTFAULTPARTS.TryFetch(sofp:faultKey) = Level:Benign)
                        ! Found
                    else ! if (Access:SBO_OUTFAULTPARTS.TryFetch(sofp:faultKey) = Level:Benign)
                        ! Error
                        if (Access:SBO_OUTFAULTPARTS.PrimeRecord() = Level:Benign)
                            sofp:sessionID    = p_web.sessionID
                            sofp:partType    = 'W'
                            sofp:fault    = func:Field
                            sofp:description    = mfo:description
                            sofp:level    = mfo:importancelevel

                            if (Access:SBO_OUTFAULTPARTS.TryInsert() = Level:Benign)
                                ! Inserted
                            else ! if (Access:SBO_OUTFAULTPARTS.TryInsert() = Level:Benign)
                                ! Error
                            end ! if (Access:SBO_OUTFAULTPARTS.TryInsert() = Level:Benign)
                        end ! if (Access:SBO_OUTFAULTPARTS.PrimeRecord() = Level:Benign)
                    end ! if (Access:SBO_OUTFAULTPARTS.TryFetch(sofp:faultKey) = Level:Benign)
                    Break
                End !Loop
            Else!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
        Else !If map:MainFault

        End !If map:MainFault
    Else!If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End            !If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
