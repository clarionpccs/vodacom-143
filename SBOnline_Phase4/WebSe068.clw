

   MEMBER('WebServer_Phase4.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSE068.INC'),ONCE        !Local module procedure declarations
                     END


FormRepairNotes      PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
FilesOpened     Long
JOBRPNOT::State  USHORT
USERS::State  USHORT
jrn:TheDate:IsInvalid  Long
jrn:TheTime:IsInvalid  Long
jrn:Notes:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormRepairNotes')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'FormRepairNotes_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('FormRepairNotes','')
    p_web.DivHeader('FormRepairNotes',p_web.combine(p_web.site.style.formdiv,'fdiv'))
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('FormRepairNotes') = 0
        p_web.AddPreCall('FormRepairNotes')
        p_web.DivHeader('popup_FormRepairNotes','nt-hidden')
        p_web.DivHeader('FormRepairNotes',p_web.combine(p_web.site.style.formdiv,'fdiv'))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_FormRepairNotes_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_FormRepairNotes_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormRepairNotes',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:StagePost
    do RestoreMem
    do PostInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_FormRepairNotes',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormRepairNotes',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:StagePost
    do RestoreMem
    do PostCopy
  of Net:CopyRecord + NET:WEB:Populate
    If p_web.IfExistsValue('jrn:RecordNumber') = 0 then p_web.SetValue('jrn:RecordNumber',p_web.GSV('jrn:RecordNumber')).
    do PreCopy
    p_web.setsessionvalue('showtab_FormRepairNotes',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormRepairNotes',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormRepairNotes',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do PostInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do PostUpdate
    End
  of Net:ChangeRecord + NET:WEB:Populate
    If p_web.IfExistsValue('jrn:RecordNumber') = 0 then p_web.SetValue('jrn:RecordNumber',p_web.GSV('jrn:RecordNumber')).
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_FormRepairNotes',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormRepairNotes',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:DeleteRecord + NET:WEB:StagePost
    do RestoreMem
    do PostDelete
  of Net:ViewRecord + NET:WEB:Populate
    If p_web.IfExistsValue('jrn:RecordNumber') = 0 then p_web.SetValue('jrn:RecordNumber',p_web.GSV('jrn:RecordNumber')).
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_FormRepairNotes',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormRepairNotes',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormRepairNotes',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormRepairNotes',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('FormRepairNotes')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(JOBRPNOT)
  p_web._OpenFile(USERS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(JOBRPNOT)
  p_Web._CloseFile(USERS)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FormRepairNotes_form:inited_',1)
  p_web.formsettings.file = 'JOBRPNOT'
  p_web.formsettings.key = 'jrn:RecordNumberKey'
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = 'JOBRPNOT'
    p_web.formsettings.key = 'jrn:RecordNumberKey'
      clear(p_web.formsettings.FieldName)
    p_web.formsettings.recordid[1] = jrn:RecordNumber
    p_web.formsettings.FieldName[1] = 'jrn:RecordNumber'
    do SetAction
    if p_web.GetSessionValue('FormRepairNotes:Primed') = 1
      p_web.formsettings.action = Net:ChangeRecord
    Else
      p_web.formsettings.action = Loc:Act
    End
    p_web.formsettings.OriginalAction = Loc:Act
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'FormRepairNotes'
    end
    p_web.formsettings.proc = 'FormRepairNotes'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine
  IF p_web.GetSessionValue('FormRepairNotes:Primed') = 1
    p_web._deleteFile(JOBRPNOT)
    p_web.SetSessionValue('FormRepairNotes:Primed',0)
  End

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine
  p_web.SetValue('UpdateFile','JOBRPNOT')
  p_web.SetValue('UpdateKey','jrn:RecordNumberKey')
  If p_web.IfExistsValue('jrn:TheTime')
    p_web.SetPicture('jrn:TheTime','@t1b')
  End
  p_web.SetSessionPicture('jrn:TheTime','@t1b')
  If p_web.IfExistsValue('jrn:TheDate')
    p_web.SetPicture('jrn:TheDate',p_web.site.DatePicture)
  End
  p_web.SetSessionPicture('jrn:TheDate',p_web.site.DatePicture)

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=File

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('FormRepairNotes_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferFormRepairNotes')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormRepairNotes_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormRepairNotes_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormRepairNotes_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction

GenerateForm   Routine
  do LoadRelatedRecords
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Insert / Amend Repair Notes') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Insert / Amend Repair Notes',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_FormRepairNotes',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_FormRepairNotes0_div')&'">'&p_web.Translate()&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="FormRepairNotes_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.cancel();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CancelButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="FormRepairNotes_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'FormRepairNotes_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="FormRepairNotes_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'FormRepairNotes_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.jrn:Notes')
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_FormRepairNotes')>0,p_web.GSV('showtab_FormRepairNotes'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_FormRepairNotes'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_FormRepairNotes') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_FormRepairNotes'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_FormRepairNotes')>0,p_web.GSV('showtab_FormRepairNotes'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_FormRepairNotes') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_FormRepairNotes_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_FormRepairNotes')>0,p_web.GSV('showtab_FormRepairNotes'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"FormRepairNotes",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_FormRepairNotes')>0,p_web.GSV('showtab_FormRepairNotes'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_FormRepairNotes_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('FormRepairNotes') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('FormRepairNotes')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate()&'</a></h3>' & CRLF & p_web.DivHeader('tab_FormRepairNotes0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_FormRepairNotes0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_FormRepairNotes0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_FormRepairNotes0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,)
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_FormRepairNotes0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend)
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_FormRepairNotes0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain">' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_FormRepairNotes0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jrn:TheDate
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jrn:TheDate
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jrn:TheTime
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jrn:TheTime
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jrn:Notes
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jrn:Notes
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Prompt::jrn:TheDate  Routine
  packet = clip(packet) & p_web.DivHeader('FormRepairNotes_' & p_web._nocolon('jrn:TheDate') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Date'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jrn:TheDate  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jrn:TheDate = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value')
    jrn:TheDate = p_web.dformat(clip(p_web.GetValue('Value')),p_web.site.DatePicture)
  End
  do ValidateValue::jrn:TheDate  ! copies value to session value if valid.
  do Value::jrn:TheDate
  do SendAlert

ValidateValue::jrn:TheDate  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('jrn:TheDate',jrn:TheDate).
    End

Value::jrn:TheDate  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormRepairNotes_' & p_web._nocolon('jrn:TheDate') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  If loc:retrying
    jrn:TheDate = p_web.RestoreValue('jrn:TheDate')
    do ValidateValue::jrn:TheDate
    If jrn:TheDate:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- DATE --- jrn:TheDate
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jrn:TheDate'',''formrepairnotes_jrn:thedate_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = '1'
  loc:options = p_web.site.Dateoptions
  ! example of loc:options; loc:options = Choose(loc:options='','',clip(loc:options) & ',') & 'numberOfMonths: 3,showButtonPanel: true' ! see http://jqueryui.com/demos/datepicker/#options
  packet = clip(packet) & p_web.CreateDateInput ('jrn:TheDate',p_web.GetSessionValue('jrn:TheDate'),loc:fieldclass,loc:readonly,,,loc:javascript,loc:options,loc:extra,'Date',15,,,,0)
  do SendPacket
  End
  p_web.DivFooter()

Prompt::jrn:TheTime  Routine
  packet = clip(packet) & p_web.DivHeader('FormRepairNotes_' & p_web._nocolon('jrn:TheTime') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Time'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jrn:TheTime  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jrn:TheTime = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@t1b'  !FieldPicture = @t1b
    jrn:TheTime = p_web.Dformat(p_web.GetValue('Value'),'@t1b')
  End
  do ValidateValue::jrn:TheTime  ! copies value to session value if valid.
  do Value::jrn:TheTime
  do SendAlert

ValidateValue::jrn:TheTime  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('jrn:TheTime',jrn:TheTime).
    End

Value::jrn:TheTime  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormRepairNotes_' & p_web._nocolon('jrn:TheTime') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  If loc:retrying
    jrn:TheTime = p_web.RestoreValue('jrn:TheTime')
    do ValidateValue::jrn:TheTime
    If jrn:TheTime:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- jrn:TheTime
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jrn:TheTime'',''formrepairnotes_jrn:thetime_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jrn:TheTime',p_web.GetSessionValue('jrn:TheTime'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@t1b',loc:javascript,,'Time',,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::jrn:Notes  Routine
  packet = clip(packet) & p_web.DivHeader('FormRepairNotes_' & p_web._nocolon('jrn:Notes') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Notes'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jrn:Notes  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jrn:Notes = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s255
    jrn:Notes = p_web.Dformat(p_web.GetValue('Value'),'@s255')
  End
  do ValidateValue::jrn:Notes  ! copies value to session value if valid.
  do Value::jrn:Notes
  do SendAlert

ValidateValue::jrn:Notes  Routine
    If not (1=0)
  If jrn:Notes = ''
    loc:Invalid = 'jrn:Notes'
    jrn:Notes:IsInvalid = true
    loc:alert = p_web.translate('Notes') & ' ' & p_web.site.RequiredText
  End
    jrn:Notes = Upper(jrn:Notes)
      if loc:invalid = '' then p_web.SetSessionValue('jrn:Notes',jrn:Notes).
    End

Value::jrn:Notes  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormRepairNotes_' & p_web._nocolon('jrn:Notes') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,'formrqd')
  If loc:retrying
    jrn:Notes = p_web.RestoreValue('jrn:Notes')
    do ValidateValue::jrn:Notes
    If jrn:Notes:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- TEXT --- jrn:Notes
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jrn:Notes'',''formrepairnotes_jrn:notes_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'readonly','')
  do SendPacket
  p_web.CreateTextArea('jrn:Notes',p_web.GetSessionValue('jrn:Notes'),8,40,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,255,'Notes',,,Net:Send)
  do SendPacket
  End
  p_web.DivFooter()

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('FormRepairNotes_nexttab_' & 0)
    jrn:TheDate = p_web.GSV('jrn:TheDate')
    do ValidateValue::jrn:TheDate
    If loc:Invalid
      loc:retrying = 1
      do Value::jrn:TheDate
      !do SendAlert
      !exit
    End
    jrn:TheTime = p_web.GSV('jrn:TheTime')
    do ValidateValue::jrn:TheTime
    If loc:Invalid
      loc:retrying = 1
      do Value::jrn:TheTime
      !do SendAlert
      !exit
    End
    jrn:Notes = p_web.GSV('jrn:Notes')
    do ValidateValue::jrn:Notes
    If loc:Invalid
      loc:retrying = 1
      do Value::jrn:Notes
      !do SendAlert
      !exit
    End
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_FormRepairNotes_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('FormRepairNotes_tab_' & 0)
    do GenerateTab0
  of lower('FormRepairNotes_jrn:TheDate_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jrn:TheDate
      of event:timer
        do Value::jrn:TheDate
      else
        do Value::jrn:TheDate
      end
  of lower('FormRepairNotes_jrn:TheTime_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jrn:TheTime
      of event:timer
        do Value::jrn:TheTime
      else
        do Value::jrn:TheTime
      end
  of lower('FormRepairNotes_jrn:Notes_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jrn:Notes
      of event:timer
        do Value::jrn:Notes
      else
        do Value::jrn:Notes
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('FormRepairNotes_form:ready_',1)

  p_web.SetSessionValue('FormRepairNotes_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_FormRepairNotes',0)
  jrn:TheDate = Today()
  p_web.SetSessionValue('jrn:TheDate',jrn:TheDate)
  jrn:TheTime = Clock()
  p_web.SetSessionValue('jrn:TheTime',jrn:TheTime)
  jrn:User = p_web.GSV('BookingUserCode')
  p_web.SetSessionValue('jrn:User',jrn:User)
  jrn:RefNumber = p_web.GSV('wob:RefNumber')
  p_web.SetSessionValue('jrn:RefNumber',jrn:RefNumber)
  jrn:TheDate = Today()
  p_web.SetSessionValue('jrn:TheDate',jrn:TheDate)
  jrn:TheTime = Clock()
  p_web.SetSessionValue('jrn:TheTime',jrn:TheTime)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('FormRepairNotes_form:ready_',1)
  p_web.SetSessionValue('FormRepairNotes_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormRepairNotes',0)
  p_web._PreCopyRecord(JOBRPNOT,jrn:RecordNumberKey)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('FormRepairNotes_form:ready_',1)
  p_web.SetSessionValue('FormRepairNotes_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('FormRepairNotes:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('FormRepairNotes_form:ready_',1)
  p_web.SetSessionValue('FormRepairNotes_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('FormRepairNotes:Primed',0)
  p_web.setsessionvalue('showtab_FormRepairNotes',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
      If not (1=0)
          If p_web.IfExistsValue('jrn:Notes')
            jrn:Notes = p_web.GetValue('jrn:Notes')
          End
      End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormRepairNotes_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormRepairNotes_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
    do ValidateValue::jrn:TheDate
    If loc:Invalid then exit.
    do ValidateValue::jrn:TheTime
    If loc:Invalid then exit.
    do ValidateValue::jrn:Notes
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostInsert      Routine
PostCopy        Routine
  p_web.SetSessionValue('FormRepairNotes:Primed',0)
PostUpdate      Routine
  p_web.SetSessionValue('FormRepairNotes:Primed',0)


PostDelete      Routine
BrowseRepairNotes    PROCEDURE  (NetWebServerWorker p_web)
locUserName          STRING(60)                            !
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256),dim(Net:MaxKeyFields)
idx           String(Net:HashSize)
sub           Long
            End
jrn:TheDate:IsInvalid  Long
jrn:TheTime:IsInvalid  Long
locUserName:IsInvalid  Long
jrn:Notes:IsInvalid  Long
Edit:IsInvalid  Long
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:DefaultSelection    String(Net:HashSize)
loc:ActualSelection     String(Net:HashSize)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
!loc:pagename            String(256)
loc:NavButtonPosition    Long
loc:UpdateButtonPosition Long
loc:SelectionMethod      Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorCase         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
loc:lookupdone          Long
loc:FormPopup           Long
ThisView            View(JOBRPNOT)
                      Project(jrn:RecordNumber)
                      Project(jrn:TheDate)
                      Project(jrn:TheTime)
                      Project(jrn:Notes)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
Loc:popup             Long
loc:poppedup          Long,static,thread
FilesOpened     Long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return 
  End
  GlobalErrors.SetProcedureName('BrowseRepairNotes')
  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseRepairNotes:NoForm')
      loc:NoForm = p_web.GetValue('BrowseRepairNotes:NoForm')
      loc:FormName = p_web.GetValue('BrowseRepairNotes:FormName')
    else
      loc:FormName = 'BrowseRepairNotes_frm'
    End
    p_web.SSV('BrowseRepairNotes:NoForm',loc:NoForm)
    p_web.SSV('BrowseRepairNotes:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseRepairNotes:NoForm')
    loc:FormName = p_web.GSV('BrowseRepairNotes:FormName')
  end
  case p_web.site.formpopups
  of -1 ; loc:FormPopup = Net:Page
  of 0 ; loc:FormPopup = Net:Page
  of 1 ; loc:FormPopup = Net:Popup
  End
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseRepairNotes') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseRepairNotes')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if 1 = 2
  elsif p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  elsif p_web.IfExistsValue('_EIPRow_')
    do CallRow
  elsif p_web.GetValue('_CallPopups') > 0
    do CallPopups
  elsif p_web.IfExistsValue('_GetSecwinSettings')
      p_web.SetValue('Secwin_' & 'BrowseRepairNotes' & 'AccessGroupsArray','1') 
      p_web.SetValue('Secwin_AccessWindowName','BrowseRepairNotes')
      
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallPopups  Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
loc:CallPopups   Long
loc:name         String(255)
  code
  if loc:poppedup = 0
    loc:CallPopups = p_web.GetValue('_CallPopups')
    if loc:CallPopups = 1 !and p_web.GetPreCall('BrowseRepairNotes') = 0! for = 1 include popup divs and scripts for this browse
      !p_web.AddPreCall('BrowseRepairNotes')
      p_web.DivHeader('popup_BrowseRepairNotes','nt-hidden')
      p_web.DivHeader('BrowseRepairNotes',p_web.combine(p_web.site.style.browsediv,'fdiv'))
      p_web.DivFooter()
      p_web.DivFooter()
      loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(400)&', modal: true, position: [''center'',15]'
      packet =  p_web.jQuery('#' & lower('popup_BrowseRepairNotes_div'),'dialog',loc:options) & |
      p_web.jQuery('#' & lower('popup_BrowseRepairNotes_div'),'removeClass','''nt-hidden''')
    End
    ! for > 0 add in form, EIP-lookups and browse object
    do Popups
    loc:popup = 1
    do ClosingScripts
    do SendPacket
    loc:poppedup = 1
  end

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web.DivHeader('BrowseRepairNotes',p_web.combine(p_web.site.style.browsediv,'fdiv'))
  if loc:ParentSilent = 0
    do GenerateBrowse
    if p_web.RequestAjax = 1
      p_web.Script('BrowseProducts.pcv();')
    end
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web.DivFooter()
  do Children
  do Popups
  if loc:poppedup = 0
    do ClosingScripts
  end
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(JOBRPNOT,jrn:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'JRN:THEDATE') then p_web.SetValue('BrowseRepairNotes_sort','1')
    ElsIf (loc:vorder = 'JRN:THETIME') then p_web.SetValue('BrowseRepairNotes_sort','2')
    ElsIf (loc:vorder = 'LOCUSERNAME') then p_web.SetValue('BrowseRepairNotes_sort','3')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseRepairNotes:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseRepairNotes:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseRepairNotes:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseRepairNotes:LookupField')
    loc:selecting = 0
  End

Popups Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  If loc:popup = 0
  End
SetFormAction  Routine
  loc:formaction = 'FormRepairNotes'
  loc:formactiontarget = '_self'

GotFocusBack   Routine

GenerateBrowse Routine
  data
loc:viewoptions  Long
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  ! Set general Browse options
  loc:NavButtonPosition   = Net:Below
  loc:UpdateButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:FileLoad
  loc:FillBack         = 0
  loc:Sorting          = Net:ServerSort
  if p_web.GetValue('_popup_') = 1
    loc:popup = 1
  end
  ! Set Locator Options
  loc:LocatorPosition  = Net:Below
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  p_web.ClearBrowse('BrowseRepairNotes')
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 10
  End
  loc:ActualSelection = ''
  !loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseRepairNotes_sort',net:DontEvaluate)
  p_web.SetSessionValue('BrowseRepairNotes_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'jrn:TheDate','-jrn:TheDate')
    Loc:LocateField = 'jrn:TheDate'
    Loc:LocatorCase = 0
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'jrn:TheTime','-jrn:TheTime')
    Loc:LocateField = 'jrn:TheTime'
    Loc:LocatorCase = 0
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'locUserName','-locUserName')
    Loc:LocateField = 'locUserName'
    Loc:LocatorCase = 0
  of 5
    Loc:LocateField = ''
    Loc:LocatorCase = 0
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('jrn:TheDate')
    loc:SortHeader = p_web.Translate('Date')
    p_web.SetSessionValue('BrowseRepairNotes_LocatorPic','@d6')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
    loc:LocatorType = Net:Date
  Of upper('jrn:TheTime')
    loc:SortHeader = p_web.Translate('Time')
    p_web.SetSessionValue('BrowseRepairNotes_LocatorPic','@t1b')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
  Of upper('locUserName')
    loc:SortHeader = p_web.Translate('Username')
    p_web.SetSessionValue('BrowseRepairNotes_LocatorPic','@s60')
  Of upper('jrn:Notes')
    loc:SortHeader = p_web.Translate('Notes')
    p_web.SetSessionValue('BrowseRepairNotes_LocatorPic','@s255')
  End
  If loc:selecting = 1
    p_web.GetSettings(p_web.GetSessionValue('Push1'))
    loc:selectaction = p_web.FormSettings.ParentPage !p_web.GetSessionValue('BrowseRepairNotes:LookupFrom')
  End!Else
  loc:CloseAction = 'JobFaultCodes'
  do SendPacket
  do SetFormAction
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseRepairNotes:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseRepairNotes:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseRepairNotes:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="JOBRPNOT"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="jrn:RecordNumberKey"></input><13,10>'
  end
  If p_web.Translate('Job Repair Notes') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.browseSubHeading,)&'">'&p_web.Translate('Job Repair Notes',0)&'</div>'&CRLF
  End
  case p_web.GetValue('refresh') ; of 'next' orof 'previous' orof 'first' orof 'last'
    p_web.Script('try{{BrowseRepairNotes.restoreFocus();} catch(err){{};')
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseRepairNotes',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      case loc:locatortype
      of Net:Position
      orof Net:Date
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
      of Net:Contains
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
      of Net:Begins
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
      End
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,'Locator')&'"><tr>' &|
      Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
      Case Loc:LocatorType
      of Net:Date
        packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator2BrowseRepairNotes','',p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,, p_web.GSV('BrowseRepairNotes_LocatorPic'),,,'onchange="BrowseRepairNotes.locate(''Locator2BrowseRepairNotes'',this.value);"',,0,,,) & '</div></td>'
      Else
        packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator2BrowseRepairNotes',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,'size="30" onchange="BrowseRepairNotes.locate(''Locator2BrowseRepairNotes'',this.value);" ',,,,,,) & '</td>'
      End
      If loc:LocatorSearchButton or loc:LocatorClearButton
        packet = clip(packet) & '<td><div id="BrowseRepairNotes_locate_a" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
        If loc:LocatorSearchButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
        End
        If loc:LocatorClearButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseRepairNotes.cl(''BrowseRepairNotes'');')
        End
        packet = clip(packet) & '</div><13,10></td><13,10>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
      If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'BrowseRepairNotes_locate_a','buttonset',loc:options)
      End
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & p_web.DivHeader('BrowseRepairNotes_table',p_web.Combine(p_web.site.style.BrowseTableDiv,'BrowseLookup'),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,,'sortable')&'" id="BrowseRepairNotes_tbl">'
  Else
    packet = clip(packet) & p_web.DivHeader('BrowseRepairNotes_table',p_web.Combine(p_web.site.style.BrowseTableDiv,'BrowseLookup'),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,)&'" id="BrowseRepairNotes_tbl">'
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'1','BrowseRepairNotes',p_web.Translate('Date'),'Click here to sort by Date',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'2','BrowseRepairNotes',p_web.Translate('Time'),'Click here to sort by Time',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'3','BrowseRepairNotes',p_web.Translate('Username'),,,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'4','BrowseRepairNotes',p_web.Translate('Notes'),'Click here to sort by Notes',,,,,,1,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
      If loc:Selecting = 0
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'5','BrowseRepairNotes',p_web.Translate(),,,,,,,0,loc:Sorting,'Button')
          do AddPacket
          loc:columns += 1
      End ! Selecting
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:LocateField = 'jrn:TheDate' then Loc:NoBuffer = 1.
  If Loc:NoBuffer = 0
    Buffer(ThisView,10,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  If Instring('jrn:recordnumber',lower(loc:vorder),1,1) = 0 !and JOBRPNOT{prop:SQLDriver} = 1
    loc:vorder = Choose(loc:vorder='','jrn:RecordNumber',clip(loc:vorder) & ',' & 'jrn:RecordNumber')
  End
  Loc:Selected = Choose(p_web.IfExistsValue('jrn:RecordNumber'),p_web.GetValue('jrn:RecordNumber'),p_web.GetSessionValue('jrn:RecordNumber'))
  ThisView{prop:order} = p_web.CleanFilter(ThisView,clip(loc:vorder))
      loc:FilterWas = 'jrn:RefNumber = ' & p_web.GSV('wob:RefNumber')
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseRepairNotes',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseRepairNotes_Filter')
    p_web.SetSessionValue('BrowseRepairNotes_FirstValue','')
    p_web.SetSessionValue('BrowseRepairNotes_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,JOBRPNOT,jrn:RecordNumberKey,loc:PageRows,'BrowseRepairNotes',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,loc:ViewOptions,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled,Loc:LocatorCase)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or Loc:LocatorType = Net:Date or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position or loc:LocatorType = Net:Date
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If JOBRPNOT{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(JOBRPNOT,loc:firstvalue)
              Reset(ThisView,JOBRPNOT)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If JOBRPNOT{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(JOBRPNOT,loc:lastvalue)
            Reset(ThisView,JOBRPNOT)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(jrn:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td class="'&p_web.combine(p_web.site.style.BrowseEmpty,)&'">'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:NavButtonPosition=Net:Above or (loc:NavButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseRepairNotes_nav_a" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseRepairNotes.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseRepairNotes.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseRepairNotes.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseRepairNotes.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseRepairNotes_nav_a','buttonset',loc:options)
          End
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:UpdateButtonPosition=Net:Above or (loc:UpdateButtonPosition=Net:Both and loc:found > 0))
    packet = clip(packet) & '<div id="BrowseRepairNotes_update_a" class="'&p_web.combine(p_web.site.style.BrowseUpdateButtonSet,)&'">'
    If loc:selecting = 0 or loc:popup
      if p_web.GSV('Job:ViewOnly') <> 1 and loc:viewOnly = 0
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:InsertButton,'BrowseRepairNotes',,,loc:FormPopup,'FormRepairNotes')
          TableQueue.Kind = Net:BeforeTable
          do AddPacket
      End
    End
    If loc:found
          Loc:IsChange = 0
          If loc:selecting = 0 or loc:popup
            If p_web.GSV('Job:ViewOnly') <> 1 and loc:viewOnly = 0
              packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:ChangeButton,'BrowseRepairNotes',,,loc:FormPopup,'FormRepairNotes')
              loc:isChange = 1
            End
          End
          If loc:selecting = 0 or loc:popup
            If p_web.GSV('Job:ViewOnly') <> 1 and loc:viewonly = 0
              packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:DeletebButton,'BrowseRepairNotes',,,loc:FormPopup,'FormRepairNotes') & '<13,10>'
            End
          End
          TableQueue.Kind = Net:BeforeTable
          do AddPacket
    End
    packet = clip(packet) & '</div><13,10>'
    If p_web.site.UseUpdateButtonSet
      loc:options = ''
      packet = clip(packet) & p_web.jQuery('#' & 'BrowseRepairNotes_update_a','buttonset',loc:options)
    End ! If p_web.site.UseUpdateButtonSet
      TableQueue.Kind = Net:BeforeTable
      do AddPacket
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseRepairNotes',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseRepairNotes_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseRepairNotes_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          case loc:locatortype
          of Net:Position
          orof Net:Date
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
          of Net:Contains
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
          of Net:Begins
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
          End
          packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,'Locator')&'"><tr>' &|
          Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
          Case Loc:LocatorType
          of Net:Date
            packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator1BrowseRepairNotes','',p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,, p_web.GSV('BrowseRepairNotes_LocatorPic'),,,'onchange="BrowseRepairNotes.locate(''Locator1BrowseRepairNotes'',this.value);"',,0,,,) & '</div></td>'
          Else
            packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator1BrowseRepairNotes',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,'size="30" onchange="BrowseRepairNotes.locate(''Locator1BrowseRepairNotes'',this.value);" ',,,,,,) & '</td>'
          End
          If loc:LocatorSearchButton or loc:LocatorClearButton
            packet = clip(packet) & '<td><div id="BrowseRepairNotes_locate_b" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
            If loc:LocatorSearchButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
            End
            If loc:LocatorClearButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseRepairNotes.cl(''BrowseRepairNotes'');')
            End
            packet = clip(packet) & '</div><13,10></td><13,10>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
          If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseRepairNotes_locate_b','buttonset',loc:options)
          End
      End
    End
  End
  p_web.SetSessionValue('BrowseRepairNotes_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseRepairNotes_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:NavButtonPosition=Net:Below or loc:NavButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseRepairNotes_nav_b" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseRepairNotes.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseRepairNotes.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseRepairNotes.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseRepairNotes.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseRepairNotes_nav_b','buttonset',loc:options)
          End
        end
        do SendPacket
  End
  If loc:UpdateButtonPosition=Net:Below or loc:UpdateButtonPosition=Net:Both
  packet = clip(packet) & '<div id="BrowseRepairNotes_update_b" class="'&p_web.combine(p_web.site.style.BrowseUpdateButtonSet,)&'">'
  If loc:selecting = 0 or loc:popup
    if p_web.GSV('Job:ViewOnly') <> 1 and loc:viewOnly = 0
          packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:InsertButton,'BrowseRepairNotes',,,loc:FormPopup,'FormRepairNotes')
        do SendPacket
    End
  End
  If loc:found
        Loc:IsChange = 0
        If loc:selecting = 0 or loc:popup
          If p_web.GSV('Job:ViewOnly') <> 1 and loc:viewOnly = 0
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:ChangeButton,'BrowseRepairNotes',,,loc:FormPopup,'FormRepairNotes')
            loc:isChange = 1
          End
        End
        If loc:selecting = 0 or loc:popup
          If p_web.GSV('Job:ViewOnly') <> 1 and loc:viewonly = 0
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:DeletebButton,'BrowseRepairNotes',,,loc:FormPopup,'FormRepairNotes') & '<13,10>'
          End
        End
        do SendPacket
  End
  packet = clip(packet) & '</div><13,10>'
  If p_web.site.UseUpdateButtonSet
    loc:options = ''
    packet = clip(packet) & p_web.jQuery('#' & 'BrowseRepairNotes_update_b','buttonset',loc:options)
  End ! If p_web.site.UseUpdateButtonSet
    do SendPacket
  End
  If loc:selecting = 0 and loc:parent = ''
      if loc:popup
        packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:BrowseCloseButton,'BrowseRepairNotes',,,loc:popup)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCloseButton,loc:Formname,loc:CloseAction)
      end
  End
    do SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
  data
loc:rowclick  string(1024)
  code
    Access:USERS.Clearkey(use:user_Code_Key)
    use:User_Code    = jrn:user
    if (Access:USERS.TryFetch(use:user_Code_Key) = Level:Benign)
        ! Found
        locUsername = clip(use:forename) & ' ' & clip(use:surname)
    else ! if (Access:USERS.TryFetch(use:user_Code_Key) = Level:Benign)
        ! Error
        locUsername = '** Unknown User (' & clip(jrn:user) & ') **'
    end ! if (Access:USERS.TryFetch(use:user_Code_Key) = Level:Benign)
    loc:field = p_web.AddBrowseValue('BrowseRepairNotes','JOBRPNOT',jrn:RecordNumberKey) !jrn:RecordNumber
    p_web._thisrow = p_web._nocolon('jrn:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if Loc:LocatorValue <> '' and loc:ActualSelection = ''
        loc:checked = 'checked'
        do SetSelection
      elsif loc:ActualSelection = '' and jrn:RecordNumber = p_web.GetValue('jrn:RecordNumber')
         loc:checked = 'checked'
         do SetSelection
      elsif loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseRepairNotes:LookupField')) = jrn:RecordNumber and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      else
        loc:checked = Choose((jrn:RecordNumber = loc:selected) and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      end
      If(loc:SelectionMethod  = Net:Radio)
        loc:RowStyle = 'class="' & p_web.combine(p_web.site.style.browserow,) &'"'
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'data-nt-id="'& p_web.AddBrowseValue('BrowseRepairNotes','JOBRPNOT',jrn:RecordNumberKey) &'" onclick="'
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If JOBRPNOT{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(JOBRPNOT)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            loc:DefaultSelection = loc:field
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If JOBRPNOT{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(JOBRPNOT)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            loc:DefaultSelection = loc:field
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'>'&p_web.CreateInput('radio','_bidv_',clip(loc:field),,loc:checked,'')&'</td>'&CRLF
          If loc:DefaultSelection = ''
            loc:DefaultSelection = loc:field
          End
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          If loc:DefaultSelection = '' or loc:direction < 0
            loc:DefaultSelection = loc:field
          End
        End
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::jrn:TheDate
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::jrn:TheTime
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::locUserName
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::jrn:Notes
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        If Loc:Selecting = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::Edit
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        End     !Selecting
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr ' & clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Idx = p_web.AddBrowseValue('BrowseRepairNotes','JOBRPNOT',jrn:RecordNumberKey)
  TableQueue.Id[1] = jrn:RecordNumber

ClosingScripts  Routine
  data
Rtn_SecwinProcedureName         string(252)
  code
  If p_web.RequestAjax = 0
    do SetFormAction
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var btiBrowseRepairNotes;if (btiBrowseRepairNotes != 1){{var BrowseRepairNotes=new browseTable(''BrowseRepairNotes'','''&clip(loc:formname)&|
      ''','''&p_web._jsok('jrn:RecordNumber',Net:Parameter)&''',2,'''&clip(loc:divname)&''',1,1,1,'''&|
      clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&|
      p_web.Translate('Are you sure you want to delete this record?')&''','''&clip(loc:ActualSelection)&''','''&clip(loc:selectaction)&''','''&|
      clip(loc:formactiontarget)&''',''FormRepairNotes'','&loc:popup&',0,'''','''',"' & clip(Rtn_SecwinProcedureName) & '");<13,10>'&|
      'BrowseRepairNotes.setGreenBar('''&p_web.ColorWeb(p_web.Site.Style.BrowseHighlightColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOneColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseTwoColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOverColor)&''');<13,10>' &|
      'BrowseRepairNotes.applyGreenBar();btiBrowseRepairNotes=1};<13,10>' &|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseRepairNotes')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseRepairNotes')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseRepairNotes')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseRepairNotes')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(JOBRPNOT)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(JOBRPNOT)
  Bind(jrn:Record)
  Clear(jrn:Record)
  NetWebSetSessionPics(p_web,JOBRPNOT)

Children Routine
  if loc:selecting = 0
    If p_web.RequestAjax = 0
      do StartChildren
    Else
      do AjaxChildren
    End
  end

AjaxChildren  Routine
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue('jrn:RecordNumber',p_web.GetValue('jrn:RecordNumber'))
   p_web.DeleteValue('_Clicked')
   loc:found = 1
   do Children
! ----------------------------------------------------------------------------------------
CallRow  Routine
  data
loc:result  long
  code
  do OpenFilesB
  jrn:RecordNumber = p_web.GSV('jrn:RecordNumber')
  loc:result = p_web._GetFile(JOBRPNOT,jrn:RecordNumberKey)
  loc:eip = 1
  loc:viewstate = p_web.escape(p_web.Base64Encode(clip(jrn:RecordNumber)))
  do BrowseRow
  do SendPacket
  do ClosefilesB

! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(JOBRPNOT)
  Case upper(p_web.GetValue('_EIPClm'))
  Else
    case p_web.GetValue('_action_')
    of Net:InsertRecord
    orof Net:ChangeRecord
    orof Net:DeleteRecord
    orof Net:CopyRecord
      loc:eip = 0
      do CallBrowse
    end
  End
  do GotFocusBack
  p_web._CloseFile(JOBRPNOT)
! ----------------------------------------------------------------------------------------
value::jrn:TheDate   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
  If not (p_web.GSV('Job:ViewOnly') <> 1) then loc:FormOk = 0.
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseRepairNotes_jrn:TheDate_'&jrn:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(jrn:TheDate,'@d6')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::jrn:TheTime   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
  If not (p_web.GSV('Job:ViewOnly') <> 1) then loc:FormOk = 0.
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseRepairNotes_jrn:TheTime_'&jrn:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(jrn:TheTime,'@t1b')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::locUserName   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
  If not (p_web.GSV('Job:ViewOnly') <> 1) then loc:FormOk = 0.
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseRepairNotes_locUserName_'&jrn:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(locUserName,'@s60')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::jrn:Notes   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
  If not (p_web.GSV('Job:ViewOnly') <> 1) then loc:FormOk = 0.
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseRepairNotes_jrn:Notes_'&jrn:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(jrn:Notes,'@s255')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::Edit   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
  If not (p_web.GSV('Job:ViewOnly') <> 1) then loc:FormOk = 0.
    p_web.site.SmallChangeButton.TextValue = p_web.Translate('Edit')
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseRepairNotes_Edit_'&jrn:RecordNumber,,net:crc,,loc:extra)
          If p_web.GSV('Job:ViewOnly') <> 1 and loc:viewonly = 0
             packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallChangeButton,'BrowseRepairNotes',p_web.AddBrowseValue('BrowseRepairNotes','JOBRPNOT',jrn:RecordNumberKey),,loc:FormPopup,'FormRepairNotes') & '<13,10>'
          End
    End
    p_web.site.SmallChangeButton = p_web.RequestData.WebServer._SitesQueue.Defaults.SmallChangeButton  !bruce
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END
  return
!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
CheckForDuplicate  Routine
SetSelection  Routine
  loc:ActualSelection = loc:field
  p_web.SetSessionValue('jrn:RecordNumber',jrn:RecordNumber)

MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:ActualSelection = ''
    loc:ActualSelection = loc:DefaultSelection
    p_web.GetBrowseValue(loc:ActualSelection,Net:Web:SessionQueue) ! so children are primed with correct sessionValue
  End

  If loc:ActualSelection <> ''
    TableQueue.Kind = Net:RowTable
    get(TableQueue,TableQueue.Kind)
    if Errorcode() = 0
      TableQueue.Row = sub(TableQueue.Row,1,len(clip(TableQueue.Row))-1) &  ' data-nt-rows="'&loc:RowsHigh&'" data-nt-value="'&clip(loc:ActualSelection)&'"' & '>'
      Put(TableQueue)
    End
  End

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead class="'&p_web.combine(p_web.site.style.BrowseHeader,'')&'"><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot class="'&p_web.combine(p_web.site.style.BrowseFooter,'BrowseFooter')&'"><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody class="'&p_web.combine(p_web.site.style.BrowseBody,)&'"><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table><13,10>' & p_web.DivFooter(Net:NoSend) ! Table Div
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
BrowseJobFaultCodeLookup PROCEDURE  (NetWebServerWorker p_web)
local:FaultCodeValue STRING(255)                           !
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256),dim(Net:MaxKeyFields)
idx           String(Net:HashSize)
sub           Long
            End
Select:IsInvalid  Long
mfo:Field:IsInvalid  Long
mfo:Description:IsInvalid  Long
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:DefaultSelection    String(Net:HashSize)
loc:ActualSelection     String(Net:HashSize)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
!loc:pagename            String(256)
loc:NavButtonPosition    Long
loc:UpdateButtonPosition Long
loc:SelectionMethod      Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorCase         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
loc:lookupdone          Long
loc:FormPopup           Long
ThisView            View(MANFAULO)
                      Project(mfo:RecordNumber)
                      Project(mfo:Field)
                      Project(mfo:Description)
                      Project(mfo:Manufacturer)
                      Project(mfo:Field_Number)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
Loc:popup             Long
loc:poppedup          Long,static,thread
FilesOpened     Long
MANFAUPA::State  USHORT
MODELCCT::State  USHORT
MANFAULO_ALIAS::State  USHORT
MANFPARL::State  USHORT
MANFAUPA_ALIAS::State  USHORT
MANFPALO_ALIAS::State  USHORT
MANFAURL::State  USHORT
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return 
  End
  GlobalErrors.SetProcedureName('BrowseJobFaultCodeLookup')
  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseJobFaultCodeLookup:NoForm')
      loc:NoForm = p_web.GetValue('BrowseJobFaultCodeLookup:NoForm')
      loc:FormName = p_web.GetValue('BrowseJobFaultCodeLookup:FormName')
    else
      loc:FormName = 'BrowseJobFaultCodeLookup_frm'
    End
    p_web.SSV('BrowseJobFaultCodeLookup:NoForm',loc:NoForm)
    p_web.SSV('BrowseJobFaultCodeLookup:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseJobFaultCodeLookup:NoForm')
    loc:FormName = p_web.GSV('BrowseJobFaultCodeLookup:FormName')
  end
  case p_web.site.formpopups
  of -1 ; loc:FormPopup = Net:Page
  of 0 ; loc:FormPopup = Net:Page
  of 1 ; loc:FormPopup = Net:Popup
  End
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseJobFaultCodeLookup') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseJobFaultCodeLookup')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if 1 = 2
  elsif p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  elsif p_web.IfExistsValue('_EIPRow_')
    do CallRow
  elsif p_web.GetValue('_CallPopups') > 0
    do CallPopups
  elsif p_web.IfExistsValue('_GetSecwinSettings')
      p_web.SetValue('Secwin_' & 'BrowseJobFaultCodeLookup' & 'AccessGroupsArray','1') 
      p_web.SetValue('Secwin_AccessWindowName','BrowseJobFaultCodeLookup')
      
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallPopups  Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
loc:CallPopups   Long
loc:name         String(255)
  code
  if loc:poppedup = 0
    loc:CallPopups = p_web.GetValue('_CallPopups')
    if loc:CallPopups = 1 !and p_web.GetPreCall('BrowseJobFaultCodeLookup') = 0! for = 1 include popup divs and scripts for this browse
      !p_web.AddPreCall('BrowseJobFaultCodeLookup')
      p_web.DivHeader('popup_BrowseJobFaultCodeLookup','nt-hidden')
      p_web.DivHeader('BrowseJobFaultCodeLookup',p_web.combine(p_web.site.style.browsediv,))
      p_web.DivFooter()
      p_web.DivFooter()
      loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(400)&', modal: true, position: [''center'',15]'
      packet =  p_web.jQuery('#' & lower('popup_BrowseJobFaultCodeLookup_div'),'dialog',loc:options) & |
      p_web.jQuery('#' & lower('popup_BrowseJobFaultCodeLookup_div'),'removeClass','''nt-hidden''')
    End
    ! for > 0 add in form, EIP-lookups and browse object
    do Popups
    loc:popup = 1
    do ClosingScripts
    do SendPacket
    loc:poppedup = 1
  end

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web.DivHeader('BrowseJobFaultCodeLookup',p_web.combine(p_web.site.style.browsediv,))
  if loc:ParentSilent = 0
    do GenerateBrowse
    if p_web.RequestAjax = 1
      p_web.Script('BrowseProducts.pcv();')
    end
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web.DivFooter()
  do Children
  do Popups
  if loc:poppedup = 0
    do ClosingScripts
  end
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(MANFAULO,mfo:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'MFO:FIELD') then p_web.SetValue('BrowseJobFaultCodeLookup_sort','1')
    ElsIf (loc:vorder = 'MFO:DESCRIPTION') then p_web.SetValue('BrowseJobFaultCodeLookup_sort','2')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseJobFaultCodeLookup:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseJobFaultCodeLookup:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseJobFaultCodeLookup:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseJobFaultCodeLookup:LookupField')
    loc:selecting = 0
  End

Popups Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  If loc:popup = 0
  End
SetFormAction  Routine
  loc:formaction = 'BrowseJobFaultCodeLookup'

GotFocusBack   Routine

GenerateBrowse Routine
  data
loc:viewoptions  Long
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  ! Set general Browse options
  loc:NavButtonPosition   = Net:Below
  loc:UpdateButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:FileLoad
  loc:FillBack         = 0
  loc:Sorting          = Net:ServerSort
  if p_web.GetValue('_popup_') = 1
    loc:popup = 1
  end
  ! Set Locator Options
  loc:LocatorPosition  = Net:Above
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  p_web.ClearBrowse('BrowseJobFaultCodeLookup')
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  if (p_web.ifExistsValue('fieldNumber'))
      p_web.storeValue('fieldNumber')
  end !if (p_web.ifExistsValue('fieldNumber'))
  if (p_web.ifExistsValue('partType'))
      p_web.storeValue('partType')
  end ! if (p_web.ifExistsValue('partType'))
  ! Has this been called from Parts Main Fault?
  if (p_web.ifExistsValue('partMainFault'))
      p_web.storeValue('partMainFault')
  end ! if (p_web.ifExistsValue('partMainFault'))
  if (p_web.ifExistsValue('relatedPartCode'))
      p_web.storeValue('relatedPartCode')
  end ! if (p-web.ifExistsValue('relatedPartCode'))
  
  !Clear queue for this user
  clear(tmpfau:Record)
  tmpfau:sessionID = p_web.SessionID
  set(tmpfau:keySessionID,tmpfau:keySessionID)
  loop
      next(tempFaultCodes)
      if (error())
          break
      end! if (error())
      if (tmpfau:sessionID <> p_web.sessionID)
          break
      end ! if (tmpfau:sessionID <> p_web.sessionID)
      delete(tempFaultCodes)
  end ! loop
  
  if (p_web.GSV('BrowseJobFaultCodeLookup:LookupFrom') = 'JobFaultCodes')
      loop x# = 1 to 20
          if (p_web.GSV('Hide:JobFaultCode' & x#) = 1 )
              cycle
          end ! if (p_web.GSV('Hide:JobFaultCode' & x#) = 1 )
          if (p_web.GSV('tmp:FaultCode' & x#) = '')
              cycle
          end ! if (p_web.GSV('tmp:FaultCode' & x#) = '')
  
          Access:MANFAULT.Clearkey(maf:ScreenOrderKey)
          maf:Manufacturer    = p_web.GSV('job:Manufacturer')
          maf:ScreenOrder    = x#
          if (Access:MANFAULT.TryFetch(maf:ScreenOrderKey) = Level:Benign)
              ! Found
              if (maf:Lookup = 'YES')
                  Access:MANFAULO_ALIAS.Clearkey(mfo_ali:Field_Key)
                  mfo_ali:Manufacturer    = p_web.GSV('job:Manufacturer')
                  mfo_ali:Field_Number    = maf:Field_Number
                  mfo_ali:Field = p_web.GSV('tmp:FaultCode' & x#)
                  if (Access:MANFAULO_ALIAS.TryFetch(mfo_ali:Field_Key) = Level:Benign)
                      ! Found
                      found# = 0
                      Access:MANFAURL.Clearkey(mnr:FieldKey)
                      mnr:MANFAULORecordNumber    = mfo_ali:RecordNumber
  !                    mnr:PartFaultCode    = 1
                      mnr:FieldNumber    = p_web.GSV('fieldNumber')
                      set(mnr:FieldKey,mnr:FieldKey)
                      loop
                          if (Access:MANFAURL.Next())
                              Break
                          end ! if (Access:MANFAURL.Next())
                          if (mnr:MANFAULORecordNumber    <> mfo_ali:RecordNumber)
                              Break
                          end ! if (mnr:MANFALORecordNumber    <> mfo:RecordNumber)
  !                        if (mnr:PartFaultCode    <> 1)
  !                            Break
  !                        end ! if (mnr:PartFaultCode    <> 1)
                          if (mnr:FieldNumber    <> p_web.GSV('fieldNumber'))
                              Break
                          end ! if (mnr:FieldNumber    <> fieldNumber)
                          found# = 1
                          break
                      end ! loop
                      if (found# = 1)
                          tmpfau:sessionID = p_web.sessionID
                          tmpfau:recordNumber = mfo_ali:RecordNumber
                          tmpfau:faultType = 'J'
                          add(tempFaultCodes)
                      end ! if (found# = 1)
                  else ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                      ! Error
                  end ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
              end ! if (maf:Lookup = 'YES')
          else ! if (Access:MANFAULT.TryFetch(maf:Field_Number_Key) = Level:Benign)
              ! Error
          end ! if (Access:MANFAULT.TryFetch(maf:Field_Number_Key) = Level:Benign)
  
  
          local:FaultCodeValue = ''
          Access:WARPARTS.Clearkey(wpr:Part_Number_Key)
          wpr:Ref_Number = p_web.GSV('job:Ref_Number')
          Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
          Loop
              If Access:WARPARTS.Next()
                  Break
              End ! If Access:WARPARTS.Next()
              If wpr:Ref_Number <> p_web.GSV('job:Ref_Number')
                  Break
              End ! If wpr:Ref_Number <> job:Ref_Number
              Loop f# = 1 To 12
                  Case f#
                  Of 1
                      If wpr:Fault_Code1 = ''
                          Cycle
                      End ! If wpr:Fault_Code1 = ''
                      local:FaultCodeValue = wpr:Fault_Code1
                  Of 2
                      If wpr:Fault_Code2 = ''
                          Cycle
                      End ! If wpr:Fault_Code1 = ''
                      local:FaultCodeValue = wpr:Fault_Code2
                  Of 3
                      If wpr:Fault_Code3 = ''
                          Cycle
                      End ! If wpr:Fault_Code1 = ''
                      local:FaultCodeValue = wpr:Fault_Code3
                  Of 4
                      If wpr:Fault_Code4 = ''
                          Cycle
                      End ! If wpr:Fault_Code1 = ''
                      local:FaultCodeValue = wpr:Fault_Code4
                  Of 5
                      If wpr:Fault_Code5 = ''
                          Cycle
                      End ! If wpr:Fault_Code1 = ''
                      local:FaultCodeValue = wpr:Fault_Code5
                  Of 6
                      If wpr:Fault_Code6 = ''
                          Cycle
                      End ! If wpr:Fault_Code1 = ''
                      local:FaultCodeValue = wpr:Fault_Code6
                  Of 7
                      If wpr:Fault_Code7 = ''
                          Cycle
                      End ! If wpr:Fault_Code1 = ''
                      local:FaultCodeValue = wpr:Fault_Code7
                  Of 8
                      If wpr:Fault_Code8 = ''
                          Cycle
                      End ! If wpr:Fault_Code1 = ''
                      local:FaultCodeValue = wpr:Fault_Code8
                  Of 9
                      If wpr:Fault_Code9 = ''
                          Cycle
                      End ! If wpr:Fault_Code1 = ''
                      local:FaultCodeValue = wpr:Fault_Code9
                  Of 10
                      If wpr:Fault_Code10 = ''
                          Cycle
                      End ! If wpr:Fault_Code1 = ''
                      local:FaultCodeValue = wpr:Fault_Code10
                  Of 11
                      If wpr:Fault_Code11 = ''
                          Cycle
                      End ! If wpr:Fault_Code1 = ''
                      local:FaultCodeValue = wpr:Fault_Code11
                  Of 12
                      If wpr:Fault_Code12 = ''
                          Cycle
                      End ! If wpr:Fault_Code1 = ''
                      local:FaultCodeValue = wpr:Fault_Code12
                  End ! Case
  
                  Access:MANFAUPA.Clearkey(map:Field_Number_Key)
                  map:Manufacturer = p_web.GSV('job:Manufacturer')
                  map:Field_Number = f#
                  If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
                      ! Found
                      If map:UseRelatedJobCode
                          Access:MANFAULT_ALIAS.Clearkey(maf_ali:MainFaultKey)
                          maf_ali:Manufacturer = p_web.GSV('job:Manufacturer')
                          maf_ali:MainFault = 1
                          If Access:MANFAULT_ALIAS.TryFetch(maf_ali:MainFaultKey) = Level:Benign
                              ! Found
                              Access:MANFAULO_ALIAS.Clearkey(mfo_ali:RelatedFieldKey)
                              mfo_ali:Manufacturer = p_web.GSV('job:Manufacturer')
                              mfo_ali:RelatedPartCode = map:Field_Number
                              mfo_ali:Field_Number = maf_ali:Field_Number
                              mfo_ali:Field = local:FaultCodeValue
                              If Access:MANFAULO_ALIAS.TryFetch(mfo_ali:RelatedFieldKey) = Level:Benign
                                  ! Found
                                  Found# = 0
                                  Access:MANFAURL.Clearkey(mnr:FieldKey)
                                  mnr:MANFAULORecordNumber = mfo_ali:RecordNumber
                                  mnr:PartFaultCode = 0
                                  mnr:FieldNumber = p_web.GSV('fieldNumber')
                                  Set(mnr:FieldKey,mnr:FieldKey)
                                  Loop
                                      If Access:MANFAURL.Next()
                                          Break
                                      End ! If Access:MANFAURL.Next()
                                      If mnr:MANFAULORecordNumber <> mfo_ali:RecordNumber
                                          Break
                                      End ! If mnr:MANFAULORecordNumber <> mfo_ali:RecordNumber
                                      If mnr:PartFaultCode <> 0
                                          Break
                                      End ! If mnr:PartFaultCOde <> 1
                                      If mnr:FieldNUmber <> p_web.GSV('fieldNumber')
                                          Break
                                      End ! If mnr:FIeldNUmber <> maf:Field_Number
                                      If mnr:RelatedPartFaultCode > 0
                                          If mnr:RelatedPartFaultCode <> map:Field_Number
                                              Cycle
                                          End ! If mnr:RelatedPartFaultCode <> map_ali:Field_Number
                                      End ! If mnr:RelaterdPartFaultCode > 0
                                      Found# = 1
                                      Break
                                  End ! Loop (MANFAURL)
                                  If Found# = 1
                                      tmpfau:sessionID = p_web.sessionID
                                      tmpfau:recordNumber = mfo_ali:RecordNumber
                                      tmpfau:faultType = 'J'
                                      add(tempFaultCodes)
                                  End ! If Found# = 1
  
                              Else ! If Access:MANFAULO_ALIAS.TryFetch(mfo_ali:RelatedFieldKey) = Level:Benign
                              End ! If Access:MANFAULO_ALIAS.TryFetch(mfo_ali:RelatedFieldKey) = Level:Benign
                          Else ! If Access:MANFAULT_ALIAS.TryFetch(maf_ali:MainFaultKey) = Level:Benign
                          End ! If Access:MANFAULT_ALIAS.TryFetch(maf_ali:MainFaultKey) = Level:Benign
                      Else ! If map:UseRelatedJobCode
                          Access:MANFPALO.Clearkey(mfp:Field_Key)
                          mfp:Manufacturer = p_web.GSV('job:Manufacturer')
                          mfp:Field_Number = map:Field_Number
                          mfp:Field = local:FaultCodeValue
                          If Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign
                              ! Found
                              Access:MANFPARL.Clearkey(mpr:FieldKey)
                              mpr:MANFPALORecordNumber = mfp:RecordNumber
                              mpr:JobFaultCode = 1
                              mpr:FieldNumber = p_web.GSV('fieldNumber')
                              Set(mpr:FieldKey,mpr:FieldKey)
                              Loop
                                  If Access:MANFPARL.Next()
                                      Break
                                  End ! If Access:MANFPARL.Next()
                                  If mpr:MANFPALORecordNumber <> mfp:RecordNumber
                                      Break
                                  End ! If mpr:MANFPALORecordNumber <> mfp:RecordNumber
                                  If mpr:JobFaultCode <> 1
                                      Break
                                  End ! If mpr:JobFaultCode <> 1
                                  If mpr:FieldNumber <> p_web.GSV('fieldNumber')
                                      Break
                                  End ! If mpr:FieldNumber <> maf:Field_Number
  
                                  Found# = 1
                                  Break
                              End ! Loop (MANFPARL)
                              If Found# = 1
                                  tmpfau:sessionID = p_web.sessionID
                                  tmpfau:recordNumber = mfp:recordNumber
                                  tmpfau:faultType = 'P'
                                  add(tempFaultCodes)
                              End ! If Found# = 1
                          Else ! If Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign
                          End ! If Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign
                      End ! If map:UseRelatedJobCode
                  Else ! If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
                  End ! If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
              End ! Loop f# = 1 To 20
  
          End ! Loop (WARPARTS)
  
      end
  else ! if (p_web.GSV('BrowseJobFaultCodeLookup:LookupFrom') = 'JobFaultCodes')
      if (p_web.GSV('partMainFault') = 0)
          ! Build Associated Fault Codes Queue
  
          loop x# = 1 to 12
              if (x# = p_web.GSV('fieldNumber'))
                  cycle
              end ! if (x# = fieldNumber)
  
              if (p_web.GSV('Hide:PartFaultCode' & x#))
                  cycle
              end ! if (p_web.GSV('Hide:PartFaultCode' & x#))
  
              if (p_web.GSV('tmp:FaultCodes' & x#) = '')
                  cycle
              end ! if (p_web.GSV('tmp:FaultCodes' & x#) = '')
  
              Access:MANFAUPA_ALIAS.Clearkey(map_ali:ScreenOrderKey)
              map_ali:Manufacturer    = p_web.GSV('job:Manufacturer')
              map_ali:ScreenOrder    = x#
              if (Access:MANFAUPA_ALIAS.TryFetch(map_ali:ScreenOrderKey) = Level:Benign)
                  ! Found
                  Access:MANFPALO_ALIAS.Clearkey(mfp_ali:Field_Key)
                  mfp_ali:Manufacturer    = p_web.GSV('job:Manufacturer')
                  mfp_ali:Field_Number    = map_ali:Field_Number
                  mfp_ali:Field    = p_web.GSV('tmp:FaultCodes' & x#)
                  if (Access:MANFPALO_ALIAS.TryFetch(mfp_ali:Field_Key) = Level:Benign)
                      ! Found
                      found# = 0
  
                      Access:MANFPARL.Clearkey(mpr:FieldKey)
                      mpr:MANFPALORecordNumber    = mfp_ali:RecordNumber
                      mpr:FieldNumber    = p_web.GSV('fieldNumber')
                      set(mpr:FieldKey,mpr:FieldKey)
                      loop
                          if (Access:MANFPARL.Next())
                              Break
                          end ! if (Access:MANFPARL.Next())
                          if (mpr:MANFPALORecordNumber    <> mfp_ali:RecordNumber)
                              Break
                          end ! if (mfp:MANFPALORecordNumber    <> mfp_ali:RecordNumber)
                          if (mpr:FieldNumber    <> p_web.GSV('fieldNumber'))
                              Break
                          end ! if (mfp:FieldNumber    <> p_web.GSV('fieldNumber'))
                          found# = 1
                          break
                      end ! loop
  
                      if (found# = 1)
                          tmpfau:sessionID = p_web.sessionID
                          tmpfau:recordNumber = mfp_ali:recordNumber
                          tmpfau:faultType = 'P'
                          add(tempFaultCodes)
                      end ! if (found# = 1)
  
                  else ! if (Access:MANFPALO_ALIAS.TryFetch(mfp_ali:Field_Key) = Level:Benign)
                      ! Error
                  end ! if (Access:MANFPALO_ALIAS.TryFetch(mfp_ali:Field_Key) = Level:Benign)
              else ! if (Access:MANFAUPA_ALIAS.TryFetch(map_ali:Field_Number_Key) = Level:Benign)
                  ! Error
              end ! if (Access:MANFAUPA_ALIAS.TryFetch(map_ali:Field_Number_Key) = Level:Benign)
  
          end
          loop x# = 1 to 20
              Access:MANFAULT.Clearkey(maf:Field_Number_Key)
              maf:Manufacturer    = p_web.GSV('job:Manufacturer')
              maf:Field_Number    = x#
              if (Access:MANFAULT.TryFetch(maf:Field_Number_Key) = Level:Benign)
                  ! Found
                  if (maf:Lookup = 'YES')
  
                      Access:MANFAULO.Clearkey(mfo:Field_Key)
                      mfo:Manufacturer    = p_web.GSV('job:Manufacturer')
                      mfo:Field_Number    = maf:Field_Number
                      if (x# < 13)
                          mfo:Field = p_web.GSV('job:Fault_Code' & x#)
                      else ! if (x# < 13)
                          mfo:Field = p_web.GSV('wob:FaultCode' & x#)
                      end !if (x# < 13)
                      if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                          ! Found
                          found# = 0
                          Access:MANFAURL.Clearkey(mnr:FieldKey)
                          mnr:MANFAULORecordNumber    = mfo:RecordNumber
                          mnr:PartFaultCode    = 1
                          mnr:FieldNumber    = p_web.GSV('fieldNumber')
                          set(mnr:FieldKey,mnr:FieldKey)
                          loop
                              if (Access:MANFAURL.Next())
                                  Break
                              end ! if (Access:MANFAURL.Next())
                              if (mnr:MANFAULORecordNumber    <> mfo:RecordNumber)
                                  Break
                              end ! if (mnr:MANFALORecordNumber    <> mfo:RecordNumber)
                              if (mnr:PartFaultCode    <> 1)
                                  Break
                              end ! if (mnr:PartFaultCode    <> 1)
                              if (mnr:FieldNumber    <> p_web.GSV('fieldNumber'))
                                  Break
                              end ! if (mnr:FieldNumber    <> fieldNumber)
                              found# = 1
                              break
                          end ! loop
                          if (found# = 1)
                              tmpfau:sessionID = p_web.sessionID
                              tmpfau:recordNumber = mfo:recordNumber
                              tmpfau:faultType = 'J'
                              add(tempFaultCodes)
                          end ! if (found# = 1)
                      else ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                          ! Error
                      end ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                  end ! if (maf:Lookup = 'YES')
              else ! if (Access:MANFAULT.TryFetch(maf:Field_Number_Key) = Level:Benign)
                  ! Error
              end ! if (Access:MANFAULT.TryFetch(maf:Field_Number_Key) = Level:Benign)
  
          end ! loop x# = 1 to 12
  
      end ! if (p_web.GSV('partMainFault') = 0)
  
  end !if (p_web.GSV('BrowseJobFaultCodeLookup:LookupFrom') = 'JobFaultCodes')
  
  
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 15
  End
  loc:ActualSelection = ''
  !loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseJobFaultCodeLookup_sort',net:DontEvaluate)
  p_web.SetSessionValue('BrowseJobFaultCodeLookup_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 3
    Loc:LocateField = ''
    Loc:LocatorCase = 0
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(mfo:Field)','-UPPER(mfo:Field)')
    Loc:LocateField = 'mfo:Field'
    Loc:LocatorCase = 0
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(mfo:Description)','-UPPER(mfo:Description)')
    Loc:LocateField = 'mfo:Description'
    Loc:LocatorCase = 0
  end
  if loc:vorder = ''
    loc:vorder = '+UPPER(mfo:Manufacturer),+mfo:Field_Number,+UPPER(mfo:Field)'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('mfo:Field')
    loc:SortHeader = p_web.Translate('Field')
    p_web.SetSessionValue('BrowseJobFaultCodeLookup_LocatorPic','@s30')
  Of upper('mfo:Description')
    loc:SortHeader = p_web.Translate('Description')
    p_web.SetSessionValue('BrowseJobFaultCodeLookup_LocatorPic','@s60')
  End
  If loc:selecting = 1
    p_web.GetSettings(p_web.GetSessionValue('Push1'))
    loc:selectaction = p_web.FormSettings.ParentPage !p_web.GetSessionValue('BrowseJobFaultCodeLookup:LookupFrom')
  End!Else
  do SendPacket
  do SetFormAction
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseJobFaultCodeLookup:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseJobFaultCodeLookup:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseJobFaultCodeLookup:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="MANFAULO"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="mfo:RecordNumberKey"></input><13,10>'
  end
  If p_web.Translate('Select Fault Codes') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.browseSubHeading,)&'">'&p_web.Translate('Select Fault Codes',0)&'</div>'&CRLF
  End
  case p_web.GetValue('refresh') ; of 'next' orof 'previous' orof 'first' orof 'last'
    p_web.Script('try{{BrowseJobFaultCodeLookup.restoreFocus();} catch(err){{};')
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseJobFaultCodeLookup',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      case loc:locatortype
      of Net:Position
      orof Net:Date
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
      of Net:Contains
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
      of Net:Begins
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
      End
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
      Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
      Case Loc:LocatorType
      of Net:Date
        packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator2BrowseJobFaultCodeLookup','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('BrowseJobFaultCodeLookup_LocatorPic'),,,'onchange="BrowseJobFaultCodeLookup.locate(''Locator2BrowseJobFaultCodeLookup'',this.value);"',,0,,,) & '</div></td>'
      Else
        packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator2BrowseJobFaultCodeLookup',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="BrowseJobFaultCodeLookup.locate(''Locator2BrowseJobFaultCodeLookup'',this.value);" ',,,,,,) & '</td>'
      End
      If loc:LocatorSearchButton or loc:LocatorClearButton
        packet = clip(packet) & '<td><div id="BrowseJobFaultCodeLookup_locate_a" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
        If loc:LocatorSearchButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
        End
        If loc:LocatorClearButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseJobFaultCodeLookup.cl(''BrowseJobFaultCodeLookup'');')
        End
        packet = clip(packet) & '</div><13,10></td><13,10>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
      If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'BrowseJobFaultCodeLookup_locate_a','buttonset',loc:options)
      End
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & p_web.DivHeader('BrowseJobFaultCodeLookup_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'','sortable')&'" id="BrowseJobFaultCodeLookup_tbl">'
  Else
    packet = clip(packet) & p_web.DivHeader('BrowseJobFaultCodeLookup_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'')&'" id="BrowseJobFaultCodeLookup_tbl">'
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
      If loc:Selecting = 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'3','BrowseJobFaultCodeLookup',p_web.Translate('Pick'),,,,,,,0,loc:Sorting,'Button')
          do AddPacket
          loc:columns += 1
      End ! Selecting
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'1','BrowseJobFaultCodeLookup',p_web.Translate('Field'),'Click here to sort by Field',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'2','BrowseJobFaultCodeLookup',p_web.Translate('Description'),'Click here to sort by Description',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,15,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  If Instring('mfo:recordnumber',lower(loc:vorder),1,1) = 0 !and MANFAULO{prop:SQLDriver} = 1
    loc:vorder = Choose(loc:vorder='','mfo:RecordNumber',clip(loc:vorder) & ',' & 'mfo:RecordNumber')
  End
  Loc:Selected = Choose(p_web.IfExistsValue('mfo:RecordNumber'),p_web.GetValue('mfo:RecordNumber'),p_web.GetSessionValue('mfo:RecordNumber'))
  ThisView{prop:order} = p_web.CleanFilter(ThisView,clip(loc:vorder))
      loc:FilterWas = 'Upper(mfo:Manufacturer) = Upper(''' & p_web.GSV('job:Manufacturer') & ''') AND Upper(mfo:Field_Number) = ' & p_web.GSV('fieldNumber')
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseJobFaultCodeLookup',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseJobFaultCodeLookup_Filter')
    p_web.SetSessionValue('BrowseJobFaultCodeLookup_FirstValue','')
    p_web.SetSessionValue('BrowseJobFaultCodeLookup_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,MANFAULO,mfo:RecordNumberKey,loc:PageRows,'BrowseJobFaultCodeLookup',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,loc:ViewOptions,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled,Loc:LocatorCase)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or Loc:LocatorType = Net:Date or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position or loc:LocatorType = Net:Date
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If MANFAULO{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(MANFAULO,loc:firstvalue)
              Reset(ThisView,MANFAULO)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If MANFAULO{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(MANFAULO,loc:lastvalue)
            Reset(ThisView,MANFAULO)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      if (mfo:NotAvailable)
          ! #11655 Don't show if not available (Bryan: 24/08/2010)
          CYCLE
      end
      
      if (p_web.GSV('partType') = 'C')
          if (mfo:RestrictLookup = 1)
              if (p_web.GSV('par:Adjustment') = 'YES')
                  if (mfo:RestrictLookupType <> 3)
                      Cycle
                  end ! if (mfo:RestrictLookupType <> 3)
              else ! if (p_web.GSV('par:Adjustment') = 'YES')
                  if (p_web.GSV('par:Correction') = 1)
                      if (mfo:RestrictLookupType <> 2)
                          Cycle
                      end ! if (mfo:RestrictLookupType <> 2)
                  else ! if (p_web.GSV('par:Correction') = 1)
                      if (mfo:RestrictLookupType <> 1)
                          Cycle
                      end ! if (mfo:RestrictLookupType <> 1)
                  end ! if (p_web.GSV('par:Correction') = 1)
              end ! if (p_web.GSV('par:Adjustment') = 'YES')
          end ! if (mfo:RestrictLookup = 1)
      end ! if (p_web.GSV('partType') = 'C')
      
      if (p_web.GSV('partType') = 'W')
          if (mfo:RestrictLookup = 1)
              if (p_web.GSV('wpr:Adjustment') = 'YES')
                  if (mfo:RestrictLookupType <> 3)
                      Cycle
                  end ! if (mfo:RestrictLookupType <> 3)
              else ! if (p_web.GSV('par:Adjustment') = 'YES')
                  if (p_web.GSV('wpr:Correction') = 1)
                      if (mfo:RestrictLookupType <> 2)
                          Cycle
                      end ! if (mfo:RestrictLookupType <> 2)
                  else ! if (p_web.GSV('par:Correction') = 1)
                      if (mfo:RestrictLookupType <> 1)
                          Cycle
                      end ! if (mfo:RestrictLookupType <> 1)
                  end ! if (p_web.GSV('par:Correction') = 1)
              end ! if (p_web.GSV('par:Adjustment') = 'YES')
          end ! if (mfo:RestrictLookup = 1)
      end ! if (p_web.GSV('partType') = 'C')
      
      if (mfo:JobTypeAvailability = 1 and p_web.GSV('partType') <> '')
          if (p_web.GSV('partType') <> 'C')
              Cycle
          end ! if (p_web.GSV('job:Warranty_Job') = 'YES')
      end ! if (mfo:JobTypeAvailability = 1)
      
      if (mfo:JobTypeAvailability = 2 and p_web.GSV('partType') <> '')
          if (p_web.GSV('parType') <> 'W')
              Cycle
          end ! if (p_web.GSV('job:Chargeable_Job') = 'YES')
      end ! if (mfo:JobTypeAvailability = 2)
      
      
      !if (p_web.GSV('partMainFault') = 0)
          found# = 1
          clear(tmpfau:record)
          tmpfau:sessionID = p_web.sessionID
          set(tmpfau:keySessionID,tmpfau:keySessionID)
          loop
              next(tempFaultCodes)
              if (error())
                  break
              end ! if (error())
              if (tmpfau:sessionID <> p_web.sessionID)
                  break
              end ! if (tmpfau:sessionID <> p_web.sessionID)
      
              found# = 0
              case tmpfau:FaultType
              of 'P' ! Part Fault Code
                  Access:MANFPARL.Clearkey(mpr:LinkedRecordNumberKey)
                  mpr:MANFPALORecordNumber    = tmpfau:RecordNumber
                  mpr:LinkedRecordNumber    = mfo:RecordNumber
                  mpr:JobFaultCode    = 0
                  if (Access:MANFPARL.TryFetch(mpr:LinkedRecordNumberKey) = Level:Benign)
                      ! Found
                      found# = 1
                      break
                  else ! if (Access:MANFPARL.TryFetch(mpr:LinkedRecordNumberKey) = Level:Benign)
                      ! Error
                  end ! if (Access:MANFPARL.TryFetch(mpr:LinkedRecordNumberKey) = Level:Benign)
      
              of 'J' ! Job Fault Code
      
                  Access:MANFAURL.Clearkey(mnr:LinkedRecordNumberKey)
                  mnr:MANFAULORecordNumber    = tmpfau:RecordNumber
                  mnr:LinkedRecordNumber    = mfo:RecordNumber
                  mnr:PartFaultCode    = 1
                  if (Access:MANFAURL.TryFetch(mnr:LinkedRecordNumberKey) = Level:Benign)
                      ! Found
                      found# = 1
                      break
                  else ! if (Access:MANFAURL.TryFetch(mnr:LinkedRecordNumberKey) = Level:Benign)
                      ! Error
                  end ! if (Access:MANFAURL.TryFetch(mnr:LinkedRecordNumberKey) = Level:Benign)
              end ! case faultQueue.FaultType
          end ! loop
      
          if (found# = 0)
              cycle
          end ! if (found# = 0)
      !end !if (p_web.GSV('partMainFault') = 0)
      
      if (p_web.GSV('partMainFault') = 1 and p_web.GSV('relatedPartCode') <> 0)
          if (mfo:RelatedPartCode <> p_web.GSV('relatedPartCode'))
              Cycle
          end ! if (mfo:RelatedPartCode <> p_web.GSV('relatedPartCode'))
      end ! if (p_web.GSV('relatedPartCode') <> 0)
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(mfo:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td class="'&p_web.combine(p_web.site.style.BrowseEmpty,)&'">'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:NavButtonPosition=Net:Above or (loc:NavButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseJobFaultCodeLookup_nav_a" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseJobFaultCodeLookup.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseJobFaultCodeLookup.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseJobFaultCodeLookup.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseJobFaultCodeLookup.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseJobFaultCodeLookup_nav_a','buttonset',loc:options)
          End
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:UpdateButtonPosition=Net:Above or (loc:UpdateButtonPosition=Net:Both and loc:found > 0))
    If loc:selecting = 1
      If loc:found
        packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SelectButton,'BrowseJobFaultCodeLookup',,,loc:popup)
      End
      TableQueue.Kind = Net:BeforeTable
      do AddPacket
    End
    If loc:selecting = 1 !and loc:parent = ''
      if loc:popup
        packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:BrowseCancelButton,'BrowseJobFaultCodeLookup',,,loc:popup)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
      end
      TableQueue.Kind = Net:BeforeTable
      do AddPacket
    End
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseJobFaultCodeLookup',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseJobFaultCodeLookup_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseJobFaultCodeLookup_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          case loc:locatortype
          of Net:Position
          orof Net:Date
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
          of Net:Contains
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
          of Net:Begins
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
          End
          packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
          Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
          Case Loc:LocatorType
          of Net:Date
            packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator1BrowseJobFaultCodeLookup','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('BrowseJobFaultCodeLookup_LocatorPic'),,,'onchange="BrowseJobFaultCodeLookup.locate(''Locator1BrowseJobFaultCodeLookup'',this.value);"',,0,,,) & '</div></td>'
          Else
            packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator1BrowseJobFaultCodeLookup',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="BrowseJobFaultCodeLookup.locate(''Locator1BrowseJobFaultCodeLookup'',this.value);" ',,,,,,) & '</td>'
          End
          If loc:LocatorSearchButton or loc:LocatorClearButton
            packet = clip(packet) & '<td><div id="BrowseJobFaultCodeLookup_locate_b" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
            If loc:LocatorSearchButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
            End
            If loc:LocatorClearButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseJobFaultCodeLookup.cl(''BrowseJobFaultCodeLookup'');')
            End
            packet = clip(packet) & '</div><13,10></td><13,10>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
          If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseJobFaultCodeLookup_locate_b','buttonset',loc:options)
          End
      End
    End
  End
  p_web.SetSessionValue('BrowseJobFaultCodeLookup_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseJobFaultCodeLookup_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:NavButtonPosition=Net:Below or loc:NavButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseJobFaultCodeLookup_nav_b" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseJobFaultCodeLookup.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseJobFaultCodeLookup.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseJobFaultCodeLookup.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseJobFaultCodeLookup.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseJobFaultCodeLookup_nav_b','buttonset',loc:options)
          End
        end
        do SendPacket
  End
  If loc:UpdateButtonPosition=Net:Below or loc:UpdateButtonPosition=Net:Both
  If loc:selecting = 1
    If loc:found
      packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SelectButton,'BrowseJobFaultCodeLookup',,,loc:popup)
    End
    do SendPacket
  End
  If loc:selecting = 1 !and loc:parent = ''
    if loc:popup
      packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:BrowseCancelButton,'BrowseJobFaultCodeLookup',,,loc:popup)
    else
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
    end
    do SendPacket
  End
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
  data
loc:rowclick  string(1024)
  code
    loc:field = p_web.AddBrowseValue('BrowseJobFaultCodeLookup','MANFAULO',mfo:RecordNumberKey) !mfo:RecordNumber
    p_web._thisrow = p_web._nocolon('mfo:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if Loc:LocatorValue <> '' and loc:ActualSelection = ''
        loc:checked = 'checked'
        do SetSelection
      elsif loc:ActualSelection = '' and mfo:RecordNumber = p_web.GetValue('mfo:RecordNumber')
         loc:checked = 'checked'
         do SetSelection
      elsif loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseJobFaultCodeLookup:LookupField')) = mfo:RecordNumber and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      else
        loc:checked = Choose((mfo:RecordNumber = loc:selected) and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      end
      If(loc:SelectionMethod  = Net:Radio)
        loc:RowStyle = 'class="' & p_web.combine(p_web.site.style.browserow,) &'"'
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'data-nt-id="'& p_web.AddBrowseValue('BrowseJobFaultCodeLookup','MANFAULO',mfo:RecordNumberKey) &'" onclick="'
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If MANFAULO{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(MANFAULO)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            loc:DefaultSelection = loc:field
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If MANFAULO{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(MANFAULO)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            loc:DefaultSelection = loc:field
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'>'&p_web.CreateInput('radio','_bidv_',clip(loc:field),,loc:checked,'')&'</td>'&CRLF
          If loc:DefaultSelection = ''
            loc:DefaultSelection = loc:field
          End
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          If loc:DefaultSelection = '' or loc:direction < 0
            loc:DefaultSelection = loc:field
          End
        End
    end ! loc:eip = 0
        If Loc:Selecting = 1
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::Select
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        End     !Selecting
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::mfo:Field
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::mfo:Description
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr ' & clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Idx = p_web.AddBrowseValue('BrowseJobFaultCodeLookup','MANFAULO',mfo:RecordNumberKey)
  TableQueue.Id[1] = mfo:RecordNumber

ClosingScripts  Routine
  data
Rtn_SecwinProcedureName         string(252)
  code
  If p_web.RequestAjax = 0
    do SetFormAction
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var btiBrowseJobFaultCodeLookup;if (btiBrowseJobFaultCodeLookup != 1){{var BrowseJobFaultCodeLookup=new browseTable(''BrowseJobFaultCodeLookup'','''&clip(loc:formname)&|
      ''','''&p_web._jsok('mfo:RecordNumber',Net:Parameter)&''',2,'''&clip(loc:divname)&''',1,1,1,'''&|
      clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&|
      p_web.Translate('Are you sure you want to delete this record?')&''','''&clip(loc:ActualSelection)&''','''&clip(loc:selectaction)&''','''&|
      clip(loc:formactiontarget)&''','''','&loc:popup&',0,'''','''',"' & clip(Rtn_SecwinProcedureName) & '");<13,10>'&|
      'BrowseJobFaultCodeLookup.setGreenBar('''&p_web.ColorWeb(p_web.Site.Style.BrowseHighlightColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOneColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseTwoColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOverColor)&''');<13,10>' &|
      'BrowseJobFaultCodeLookup.applyGreenBar();btiBrowseJobFaultCodeLookup=1};<13,10>' &|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseJobFaultCodeLookup')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseJobFaultCodeLookup')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseJobFaultCodeLookup')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseJobFaultCodeLookup')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(MANFAULO)
  p_web._CloseFile(MANFAUPA)
  p_web._CloseFile(MODELCCT)
  p_web._CloseFile(MANFAULO_ALIAS)
  p_web._CloseFile(MANFPARL)
  p_web._CloseFile(MANFAUPA_ALIAS)
  p_web._CloseFile(MANFPALO_ALIAS)
  p_web._CloseFile(MANFAURL)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(MANFAULO)
  Bind(mfo:Record)
  Clear(mfo:Record)
  NetWebSetSessionPics(p_web,MANFAULO)
  p_web._OpenFile(MANFAUPA)
  Bind(map:Record)
  NetWebSetSessionPics(p_web,MANFAUPA)
  p_web._OpenFile(MODELCCT)
  Bind(mcc:Record)
  NetWebSetSessionPics(p_web,MODELCCT)
  p_web._OpenFile(MANFAULO_ALIAS)
  Bind(mfo_ali:Record)
  NetWebSetSessionPics(p_web,MANFAULO_ALIAS)
  p_web._OpenFile(MANFPARL)
  Bind(mpr:Record)
  NetWebSetSessionPics(p_web,MANFPARL)
  p_web._OpenFile(MANFAUPA_ALIAS)
  Bind(map_ali:Record)
  NetWebSetSessionPics(p_web,MANFAUPA_ALIAS)
  p_web._OpenFile(MANFPALO_ALIAS)
  Bind(mfp_ali:Record)
  NetWebSetSessionPics(p_web,MANFPALO_ALIAS)
  p_web._OpenFile(MANFAURL)
  Bind(mnr:Record)
  NetWebSetSessionPics(p_web,MANFAURL)

Children Routine
  if loc:selecting = 0
    If p_web.RequestAjax = 0
      do StartChildren
    Else
      do AjaxChildren
    End
  end

AjaxChildren  Routine
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue('mfo:RecordNumber',p_web.GetValue('mfo:RecordNumber'))
   p_web.DeleteValue('_Clicked')
   loc:found = 1
   do Children
! ----------------------------------------------------------------------------------------
CallRow  Routine
  data
loc:result  long
  code
  do OpenFilesB
  mfo:RecordNumber = p_web.GSV('mfo:RecordNumber')
  loc:result = p_web._GetFile(MANFAULO,mfo:RecordNumberKey)
  loc:eip = 1
  loc:viewstate = p_web.escape(p_web.Base64Encode(clip(mfo:RecordNumber)))
  do BrowseRow
  do SendPacket
  do ClosefilesB

! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(MANFAULO)
  Case upper(p_web.GetValue('_EIPClm'))
  Else
    case p_web.GetValue('_action_')
    of Net:InsertRecord
    orof Net:ChangeRecord
    orof Net:DeleteRecord
    orof Net:CopyRecord
      loc:eip = 0
      do CallBrowse
    end
  End
  do GotFocusBack
  p_web._CloseFile(MANFAULO)
! ----------------------------------------------------------------------------------------
value::Select   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    p_web.site.SmallSelectButton.TextValue = p_web.Translate('>>')
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseJobFaultCodeLookup_Select_'&mfo:RecordNumber,,net:crc,,loc:extra)
        packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'BrowseJobFaultCodeLookup',p_web.AddBrowseValue('BrowseJobFaultCodeLookup','MANFAULO',mfo:RecordNumberKey),,loc:popup)
    End
    p_web.site.SmallSelectButton = p_web.RequestData.WebServer._SitesQueue.Defaults.SmallSelectButton  !bruce
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::mfo:Field   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseJobFaultCodeLookup_mfo:Field_'&mfo:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(mfo:Field,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::mfo:Description   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseJobFaultCodeLookup_mfo:Description_'&mfo:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(mfo:Description,'@s60')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  p_web._OpenFile(MANFAUPA)
  p_web._OpenFile(MODELCCT)
  p_web._OpenFile(MANFAULO_ALIAS)
  p_web._OpenFile(MANFPARL)
  p_web._OpenFile(MANFAUPA_ALIAS)
  p_web._OpenFile(MANFPALO_ALIAS)
  p_web._OpenFile(MANFAURL)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(MANFAUPA)
  p_Web._CloseFile(MODELCCT)
  p_Web._CloseFile(MANFAULO_ALIAS)
  p_Web._CloseFile(MANFPARL)
  p_Web._CloseFile(MANFAUPA_ALIAS)
  p_Web._CloseFile(MANFPALO_ALIAS)
  p_Web._CloseFile(MANFAURL)
     FilesOpened = False
  END
  p_web.deleteSessionValue('fieldNumber')
  p_web.deleteSessionValue('partType')
  p_web.deleteSessionValue('partMainFault')
  p_web.deleteSessionValue('relatedPartCode')
  return
!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
CheckForDuplicate  Routine
SetSelection  Routine
  loc:ActualSelection = loc:field
  p_web.SetSessionValue('mfo:RecordNumber',mfo:RecordNumber)

MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:ActualSelection = ''
    loc:ActualSelection = loc:DefaultSelection
    p_web.GetBrowseValue(loc:ActualSelection,Net:Web:SessionQueue) ! so children are primed with correct sessionValue
  End

  If loc:ActualSelection <> ''
    TableQueue.Kind = Net:RowTable
    get(TableQueue,TableQueue.Kind)
    if Errorcode() = 0
      TableQueue.Row = sub(TableQueue.Row,1,len(clip(TableQueue.Row))-1) &  ' data-nt-rows="'&loc:RowsHigh&'" data-nt-value="'&clip(loc:ActualSelection)&'"' & '>'
      Put(TableQueue)
    End
  End

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead class="'&p_web.combine(p_web.site.style.BrowseHeader,'')&'"><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot class="'&p_web.combine(p_web.site.style.BrowseFooter,)&'"><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody class="'&p_web.combine(p_web.site.style.BrowseBody,)&'"><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table><13,10>' & p_web.DivFooter(Net:NoSend) ! Table Div
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
