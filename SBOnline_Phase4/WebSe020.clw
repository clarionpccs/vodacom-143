

   MEMBER('WebServer_Phase4.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSE020.INC'),ONCE        !Local module procedure declarations
                     END


ForceDOP             PROCEDURE  (func:TransitType,func:Manufacturer,func:WarrantyJob,func:Type) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
DEFAULTS::State  USHORT
TRANTYPE::State  USHORT
MANUFACT::State  USHORT
FilesOpened     BYTE(0)
  CODE
    Do OpenFiles
    Do SaveFiles
    Return# = 0
    Access:DEFAULTS.Clearkey(def:RecordNumberKey)
    def:Record_Number = 1
    Set(def:RecordNumberKey,def:RecordNumberKey)
    Loop ! Begin Loop
        If Access:DEFAULTS.Next()
            Break
        End ! If Access:DEFAULTS.Next()
        Break
    End ! Loop

    !Date Of Purchase
    access:trantype.clearkey(trt:transit_type_key)
    trt:transit_type = func:TransitType
    if access:trantype.fetch(trt:transit_type_key) = Level:Benign
        error# = 0
        If trt:force_dop = 'YES' And func:WarrantyJob = 1
            Return# = 1
        End !If trt:force_dop = 'YES'
    end

    If Return# = 0
        !Only check this if it hasn't already been forced by the Transit Type
        Access:MANUFACT.ClearKey(man:Manufacturer_Key)
        man:Manufacturer = func:Manufacturer
        If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
            !Found
            IF man:DOPCompulsory And func:WarrantyJob = 1
                Return Level:Fatal
            End !IF man:DOPCompulsory and job:dop = '' and func:Type = 'C'
        Else!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
    End ! If Return# = 0
    Do RestoreFiles
    Do CloseFiles
    Return Return#
!--------------------------------------
OpenFiles  ROUTINE
  Access:DEFAULTS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:DEFAULTS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:TRANTYPE.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRANTYPE.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:MANUFACT.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANUFACT.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:DEFAULTS.Close
     Access:TRANTYPE.Close
     Access:MANUFACT.Close
     FilesOpened = False
  END
SaveFiles  ROUTINE
  DEFAULTS::State = Access:DEFAULTS.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  TRANTYPE::State = Access:TRANTYPE.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  MANUFACT::State = Access:MANUFACT.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF DEFAULTS::State <> 0
    Access:DEFAULTS.RestoreFile(DEFAULTS::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF TRANTYPE::State <> 0
    Access:TRANTYPE.RestoreFile(TRANTYPE::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF MANUFACT::State <> 0
    Access:MANUFACT.RestoreFile(MANUFACT::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
ForceDeliveryPostcode PROCEDURE  (func:Type)               ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
  CODE
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    !Delivery Postcode
    If (def:ForceDelPostcode = 'B' And func:Type = 'B') Or |
        (def:ForceDelPostcode <> 'I' and func:Type = 'C')
        Return Level:Fatal
    End!If def:ForceDelPostcode = 'B'
    Return Level:Benign
ForceCustomerName    PROCEDURE  (func:AccountNumber,func:Type) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
  CODE
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    !Customer Name
    If (def:Customer_Name = 'B' and func:Type = 'B') Or |
         (def:Customer_Name <> 'I' and func:Type = 'C')
         If CustomerNameRequired(func:AccountNumber) = Level:Benign
            Return Level:Fatal
         End!If CustomerNameRequired(job:Account_Number) = Level:Benign
    End!If def:Customer_Name = 'B'
    Return Level:Benign
ForceCourier         PROCEDURE  (func:Type)                ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
DEFAULTS::State  USHORT
TRADEACC::State  USHORT
SUBTRACC::State  USHORT
FilesOpened     BYTE(0)
  CODE
    Do OpenFiles
    Do SaveFiles

    Access:DEFAULTS.Clearkey(def:RecordNumberKey)
    def:Record_Number = 1
    Set(def:RecordNumberKey,def:RecordNumberKey)
    Loop ! Begin Loop
        If Access:DEFAULTS.Next()
            Break
        End ! If Access:DEFAULTS.Next()
        Break
    End ! Loop

    Return# = 0
    !Incoming Courier
    If (def:Force_Outoing_Courier = 'B' And func:Type = 'B') Or |
        (def:Force_Outoing_Courier <> 'I' And func:Type = 'C')
        Return# = 1
    End!If def:Force_Incoming_Courier = 'B'

    Do RestoreFiles
    Do CloseFiles
    Return Return#
!--------------------------------------
OpenFiles  ROUTINE
  Access:DEFAULTS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:DEFAULTS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBTRACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBTRACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:DEFAULTS.Close
     Access:TRADEACC.Close
     Access:SUBTRACC.Close
     FilesOpened = False
  END
SaveFiles  ROUTINE
  DEFAULTS::State = Access:DEFAULTS.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  TRADEACC::State = Access:TRADEACC.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  SUBTRACC::State = Access:SUBTRACC.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF DEFAULTS::State <> 0
    Access:DEFAULTS.RestoreFile(DEFAULTS::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF TRADEACC::State <> 0
    Access:TRADEACC.RestoreFile(TRADEACC::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF SUBTRACC::State <> 0
    Access:SUBTRACC.RestoreFile(SUBTRACC::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
ForceColour          PROCEDURE  (func:Type)                ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
DEFAULTS::State  USHORT
FilesOpened     BYTE(0)
  CODE
    Do OpenFiles
    Do SaveFiles

    Access:DEFAULTS.Clearkey(def:RecordNumberKey)
    def:Record_Number = 1
    Set(def:RecordNumberKey,def:RecordNumberKey)
    Loop ! Begin Loop
        If Access:DEFAULTS.Next()
            Break
        End ! If Access:DEFAULTS.Next()
        Break
    End ! Loop

    Return# = 0

    !Colour!
    If (def:ForceCommonFault = 'B' And func:Type = 'B') Or |
        (def:ForceCommonFault <> 'I' And func:Type = 'C')
        Return# = 1
    End!If def:Force_Unit_Type = 'B'

    Do RestoreFiles
    Do CloseFiles

    Return Return#
!--------------------------------------
OpenFiles  ROUTINE
  Access:DEFAULTS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:DEFAULTS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:DEFAULTS.Close
     FilesOpened = False
  END
SaveFiles  ROUTINE
  DEFAULTS::State = Access:DEFAULTS.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF DEFAULTS::State <> 0
    Access:DEFAULTS.RestoreFile(DEFAULTS::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
