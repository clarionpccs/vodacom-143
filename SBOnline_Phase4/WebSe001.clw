

   MEMBER('WebServer_Phase4.clw')                          ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPRPDF.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE
   INCLUDE('ABRPPSEL.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('NetWeb.inc'),ONCE

                     MAP
                       INCLUDE('WEBSE001.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSE023.INC'),ONCE        !Req'd for module callout resolution
                     END


Main PROCEDURE                                             ! Generated from procedure template - Window

WebLog               GROUP,PRE(web)                        !
EnableLogging        LONG(1)                               !
LastGet              STRING(4096)                          !
LastPost             STRING(4096)                          !
StartDate            LONG                                  !
StartTime            LONG                                  !
PagesServed          LONG                                  !
                     END                                   !
LogQueue             QUEUE,PRE(LogQ)                       !
Port                 LONG                                  !
Date                 LONG                                  !
Time                 LONG                                  !
Desc                 STRING(4096)                          !
                     END                                   !
tmp:DataPath         STRING(255)                           !Data Path
Path:Scripts         STRING(255)                           !Path:Scripts
Path:Web             STRING(255)                           !Path:Web
Path:Styles          STRING(255)                           !Path:Styles
AmendDetails         BYTE                                  !
tmp:OpenError        BYTE(0)                               !Open Error
tmp:VersionNumber    STRING(30)                            !Version Number
s_web              &NetWebServer
Net:ShortInit      Long
loc:RequestData    Group(NetWebServerRequestDataType).
loc:OverString     String(size(loc:RequestData)),over(loc:RequestData)
Window               WINDOW('ServiceBase 3g Online'),AT(0,0,351,240),FONT('Tahoma',8,,FONT:regular),ICON('Cellular3g.ico'),GRAY,ICONIZE,DOUBLE,IMM
                       SHEET,AT(4,2,344,220),USE(?Sheet1),COLOR(COLOR:White),SPREAD
                         TAB('General'),USE(?Tab1)
                           PROMPT('Verison Number:'),AT(8,20,336,14),USE(?VersionNumber:Prompt),TRN,CENTER,FONT(,12,,FONT:bold)
                           IMAGE('SBOnline.jpg'),AT(76,34),USE(?Image1),CENTERED
                           CHECK('Amend Defaults'),AT(144,130),USE(AmendDetails),VALUE('1','0')
                           PROMPT('Server Port'),AT(28,142),USE(?Prompt1)
                           TEXT,AT(104,142,64,10),USE(glo:WebserverPort),BOXED,FLAT,SINGLE
                           TEXT,AT(104,158,200,10),USE(glo:LocalPath),DISABLE,BOXED,FLAT,SINGLE
                           PROMPT('Local Path'),AT(28,158),USE(?PROMPT2)
                           TEXT,AT(104,174,200,10),USE(glo:DataPath),BOXED,FLAT,SINGLE
                           BUTTON('...'),AT(308,174,12,10),USE(?LookupFile)
                           TEXT,AT(104,190,200,10),USE(Path:Web),BOXED,FLAT,SINGLE
                           BUTTON('...'),AT(308,190,12,10),USE(?LookupFile:2)
                           PROMPT('Path To "Web" Folder'),AT(28,190),USE(?Prompt1:2)
                           BUTTON('Save Changes And Close WebServer'),AT(104,204,148,14),USE(?Button4),LEFT,ICON('psave.ico')
                           PROMPT('Data Path'),AT(28,174),USE(?Prompt1:3)
                         END
                         TAB('Logging'),USE(?Tab2)
                           LIST,AT(8,18,336,70),USE(?LogQueue),VSCROLL,COLOR(,COLOR:Black,0F0F0F0H),FORMAT('51L(2)|M~Port~@n5@51L(2)|M~Date~@D17B@36L(2)|M~Time~@T4B@1020L(2)|M~Description~' &|
   '@s255@'),FROM(LogQueue),GRID(0F0F0F0H)
                           GROUP('Logging Group'),AT(9,23,339,199),USE(?LogGroup)
                             TEXT,AT(8,90,164,110),USE(web:LastGet),BOXED,VSCROLL
                             TEXT,AT(176,90,168,110),USE(web:LastPost),BOXED,VSCROLL
                             OPTION('Log:'),AT(8,198,141,22),USE(web:EnableLogging),BOXED
                               RADIO('No'),AT(16,209),USE(?Option1:Radio1),VALUE('0')
                               RADIO('Screen'),AT(44,210),USE(?Option1:Radio2),VALUE('1')
                               RADIO('Screen && Disk'),AT(88,210),USE(?Option1:Radio3),VALUE('2')
                             END
                             BUTTON('Clear'),AT(148,202,45,16),USE(?Clear)
                             STRING('Started At:'),AT(232,202,36,8),USE(?StartedAt)
                             STRING(@d1),AT(272,202),USE(web:StartDate)
                             STRING(@t1),AT(316,202),USE(web:StartTime)
                             STRING('Pages:'),AT(244,210),USE(?Pages)
                             STRING(@n14),AT(272,210),USE(web:PagesServed)
                           END
                         END
                       END
                       BUTTON('Close WebServer'),AT(268,224,80,14),USE(?Cancel),LEFT,ICON('pcancel.ico'),STD(STD:Close)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass
!Local Data Classes
p_web                CLASS(NetWebServer)                   ! Generated by NetTalk Extension (Class Definition)
AddLog                 PROCEDURE(String p_Data,<string p_ip>),DERIVED
StartNewThread         PROCEDURE(NetWebServerRequestDataType p_RequestData),DERIVED
TakeEvent              PROCEDURE(),DERIVED

                     END

FileLookup1          SelectFileClass
FileLookup4          SelectFileClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
CheckOmittedWeb  routine

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

!s_web    &NetWebServer
locTempFolder       CSTRING(255)
  CODE
  ! Get Defaults
  glo:LocalPath = Clip(LongPath(Path()))
  tmp:OpenError = 0
  Path:Web = GETINI('DEFAULTS','pathtoweb','web',Clip(glo:LocalPath) & '\WEBSERVER.INI')
  If GETINI('DEFAULTS','PathToData',,Clip(Path()) & '\WEBSERVER.INI') <> ''
      glo:WebserverPort = GETINI('DEFAULTS','port',,Clip(glo:LocalPath) & '\WEBSERVER.INI')
      glo:DataPath = GETINI('DEFAULTS','pathtodata',,Clip(glo:LocalPath) & '\WEBSERVER.INI')
      SetPath(Clip(glo:DataPath))
      tmp:DataPath = Path()
  
        ! Create the "temp" folder
        locTempFolder = CLIP(Path:Web) & '\temp'
        IF (NOT EXISTS(locTempFolder))
  
          rtn# = MKDir(locTempFolder)
      END
  
  
      glo:TagFile = clip(Path:Web) & '\temp\tagfile' & clock() & '.tmp'
      glo:sbo_outfaultparts = clip(Path:Web) & '\temp\outfaultparts' & clock() & '.tmp'
      glo:sbo_outparts = CLIP(Path:Web) & '\temp\outparts' & CLOCK() & '.tmp'
      glo:tempFaultCodes = clip(Path:Web) & '\temp\faultcodes' & clock() & '.tmp'
      glo:tempAuditQueue = clip(Path:Web) & '\temp\auditqueue' & clock() & '.tmp'
      glo:StockReceiveTmp = CLIP(Path:Web) & '\temp\stockreceive' & CLOCK() & '.tmp'
      glo:SBO_GenericFile = CLIP(Path:Web) & '\temp\genericfile' & CLOCK() & '.tmp'
      create(TempFaultCodes)
      share(TempFaultCodes)
      create(TempAuditQueue)
      share(TempAuditQueue)
  Else ! If GETINI('DEFAULTS','PathToData',,Clip(Path()) & '\WEBSERVER.INI') = ''
      Beep(Beep:SystemHand)  ;  Yield()
      Case Message('No defaults have been setup for this WebServer.'&|
          '|You must ensure they are setup correctly before this WebServer can function correctly.','Amend Details',|
                     icon:Hand,'&OK',1,1)
          Of 1 ! &OK Button
      End!Case Message
      tmp:OpenError = 1
  End ! If GETINI('DEFAULTS','PathToData',,Clip(Path()) & '\WEBSERVER.INI') = ''
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?VersionNumber:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:STOCK.SetOpenRelated()
  Relate:STOCK.Open                                        ! File STOCK used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(Window)                                        ! Open window
                                               ! Generated by NetTalk Extension (Start)
  do CheckOmittedWeb
    s_web &= p_web
    p_web.SuppressErrorMsg = 1         ! No Object Generated Error Messages ! Generated by NetTalk Extension
    p_web.init()
    p_web.Port = glo:WebserverPort
    p_web.Open()
    Get(p_web._SitesQueue,1)
  !---------------------------------
  s_web._SitesQueue.Defaults.WebHandlerProcName = 'WebHandler'
  s_web._SitesQueue.Defaults.DefaultPage = 'LoginForm'
  s_web._SitesQueue.Defaults.SessionExpiryAfterHS = 90001
  s_web._SitesQueue.Defaults.LoginPage = 'LoginForm'
  s_web._SitesQueue.Defaults.LoginPageIsControl = 0
  s_web._SitesQueue.Defaults.WebFolderPath = Path:Web
  If instring('\',s_web._SitesQueue.Defaults.WebFolderPath,1,1) = 0
    s_web._SitesQueue.Defaults.WebFolderPath = clip(s_web._SitesQueue.defaults.appPath) & s_web._SitesQueue.Defaults.WebFolderPath
  End
  s_web._SitesQueue.Defaults.UploadsPath = clip(s_web._SitesQueue.Defaults.WebFolderPath) & '\uploads'
  s_web._SitesQueue.Defaults.HtmlCharset = 'ISO-8859-1'
  s_web._SitesQueue.Defaults.LocatePromptText = clip('Search For')
  s_web._SitesQueue.Defaults.LocatePromptTextPosition = clip('Search For')
  s_web._SitesQueue.Defaults.LocatePromptTextBegins = clip('Search For')
  s_web._SitesQueue.Defaults.LocatePromptTextContains = clip('Search For')
  s_web._SitesQueue.Defaults.scriptsdir = 'scripts'
  s_web._SitesQueue.Defaults.stylesdir  = 'styles'
  s_web.MakeFolders()
  !s_web._SitesQueue.defaults.AllowAjax = 1
  s_web._SitesQueue.defaults._CheckForParseHeader = 1         ! Check for Parse Header String
  s_web._SitesQueue.defaults._CheckForParseHeaderSize = 1000  ! Check for the Parse Header in the first x bytes
  s_web._SitesQueue.defaults._CheckParseHeader = '<!-- NetWebServer --><13,10>'
  s_web._SitesQueue.defaults.securedir = 'secure'
  s_web._SitesQueue.defaults.loggedindir = 'loggedin'
  s_web._SitesQueue.defaults.InsertPromptText = s_web.Translate('Insert')
  s_web._SitesQueue.defaults.CopyPromptText = s_web.Translate('Copy')
  s_web._SitesQueue.defaults.ChangePromptText = s_web.Translate('Change')
  s_web._SitesQueue.defaults.ViewPromptText   = s_web.Translate('View')
  s_web._SitesQueue.defaults.DeletePromptText = s_web.Translate('Delete')
  s_web._SitesQueue.defaults.RequiredText = s_web.Translate('Required')
  s_web._SitesQueue.defaults.NumericText = s_web.Translate('A Number')
  s_web._SitesQueue.defaults.MoreThanText = s_web.Translate('More than or equal to')
  s_web._SitesQueue.defaults.LessThanText = s_web.Translate('Less than or equal to')
  s_web._SitesQueue.defaults.NotZeroText = s_web.Translate('Must not be Zero or Blank')
  s_web._SitesQueue.defaults.OneOfText = s_web.Translate('Must be one of')
  s_web._SitesQueue.defaults.InListText = s_web.Translate('Must be one of')
  s_web._SitesQueue.defaults.InFileText = s_web.Translate('Must be in table')
  s_web._SitesQueue.defaults.DuplicateText = s_web.Translate('Creates Duplicate Record on')
  s_web._SitesQueue.defaults.RestrictText = s_web.Translate('Unable to Delete, Child records exist in table')
  s_web._SitesQueue.Defaults.StoreDataAs = net:StoreAsUTF8
  s_web._SitesQueue.Defaults.DatePicture = '@D6'
  s_web._SitesQueue.Defaults.PageHeaderTag = '<!-- Net:BannerBlank -->'
  s_web._SitesQueue.Defaults.PageFooterTag = '<!-- Net:PageFooter -->'
  s_web._SitesQueue.Defaults.PageTitle = 'ServiceBase Online'
  s_web._SitesQueue.Defaults.WebFormStyle = Net:Web:Rounded
  s_web._SitesQueue.Defaults.Style.FormHeading = 'nt-header nt-form-header'
  s_web._SitesQueue.Defaults.Style.FormSubHeading = 'nt-header nt-form-header-sub'
  s_web._SitesQueue.Defaults.Style.FormTable = 'nt-form-table'
  s_web._SitesQueue.Defaults.Style.FormPrompt = 'nt-form-div nt-prompt nt-formcell'
  s_web._SitesQueue.Defaults.Style.FormEntryDiv = 'nt-form-div nt-formcell'
  s_web._SitesQueue.Defaults.Style.FormButtonDiv = 'nt-form-div'
  s_web._SitesQueue.Defaults.Style.FormEntry = 'nt-entry'
  s_web._SitesQueue.Defaults.Style.FormSelect = ' nt-select'
  s_web._SitesQueue.Defaults.Style.FormEntryRequired = 'nt-entry-required'
  s_web._SitesQueue.Defaults.Style.FormEntryReadonly = 'nt-entry-readonly'
  s_web._SitesQueue.Defaults.Style.FormEntryError = 'nt-entry-error'
  s_web._SitesQueue.Defaults.Style.FormComment = 'FormComments'
  s_web._SitesQueue.Defaults.Style.FormCommentError = 'nt-comment-error ui-state-error ui-corner-all'
  s_web._SitesQueue.Defaults.Style.FormTabOuter = 'nt-tab-outer'
  s_web._SitesQueue.Defaults.Style.FormTabTitle = 'nt-tab-title'
  s_web._SitesQueue.Defaults.Style.FormSaveButtonSet = ''
  s_web._SitesQueue.Defaults.Style.MonthSet = 'nt-month-set'
  s_web._SitesQueue.Defaults.Style.Month = 'nt-month-big ui-widget-content ui-corner-all'
  s_web._SitesQueue.Defaults.Style.MonthSmall = 'nt-month-small ui-widget-content ui-corner-all'
  s_web._SitesQueue.Defaults.Style.MonthHeader = 'ui-widget-header ui-corner-top nt-month-header'
  s_web._SitesQueue.Defaults.Style.MonthHeaderCell = 'nt-month-header-cell nt-wide'
  s_web._SitesQueue.Defaults.Style.MonthEmptyDayCell = 'nt-monthday-empty-cell'
  s_web._SitesQueue.Defaults.Style.MonthDayCell = 'nt-monthday-cell ui-corner-all'
  s_web._SitesQueue.Defaults.Style.MonthContentCell = 'nt-content'
  s_web._SitesQueue.Defaults.Style.MonthLabelCell = 'nt-label'
  s_web._SitesQueue.Defaults.Style.MonthEmptyLabelCell = 'nt-label-empty'
  s_web._SitesQueue.Defaults.Style.MonthContentCellSmall = 'nt-hidden'
  s_web._SitesQueue.Defaults.Style.MonthLabelCellSmall = 'nt-label-small'
  s_web._SitesQueue.Defaults.Style.MonthEmptyLabelCellSmall = 'nt-label-empty-small'
  s_web._SitesQueue.Defaults.Style.FormTabInner = 'nt-tab-inner'
  s_web._SitesQueue.Defaults.Style.BrowseDiv = 'nt-full'
  s_web._SitesQueue.Defaults.Style.BrowseTable = 'nt-browse-table ui-widget-content ui-corner-all'
  s_web._SitesQueue.Defaults.Style.BrowseTableDiv = 'ui-widget'
  s_web._SitesQueue.Defaults.Style.BrowseHeader = 'ui-widget-header'
  s_web._SitesQueue.Defaults.Style.BrowseBody = 'nt-browse-table-body'
  s_web._SitesQueue.Defaults.Style.BrowseRow = 'nt-browse-table-row'
  s_web._SitesQueue.Defaults.Style.BrowseFooter = 'nt-browse-table-footer'
  s_web._SitesQueue.Defaults.Style.BrowseHeading = 'nt-header nt-browse-header'
  s_web._SitesQueue.Defaults.Style.BrowseSubHeading = 'nt-header nt-browse-header-sub'
  s_web._SitesQueue.Defaults.Style.BrowseLocator = 'nt-locator'
  s_web._SitesQueue.Defaults.Style.BrowseLocateButtonSet = ''
  s_web._SitesQueue.Defaults.Style.BrowseNavButtonSet = ''
  s_web._SitesQueue.Defaults.Style.BrowseUpdateButtonSet = ''
  s_web._SitesQueue.Defaults.Style.BrowseEntry = 'nt-browse-entry'
  s_web._SitesQueue.Defaults.Style.BrowseText = 'nt-browse-entry'
  s_web._SitesQueue.Defaults.Style.BrowseDate = 'nt-browse-entry'
  s_web._SitesQueue.Defaults.Style.BrowseDrop = 'nt-browse-entry'
  s_web._SitesQueue.Defaults.Style.BrowseDropOption = 'nt-browse-entry'
  s_web._SitesQueue.Defaults.Style.BrowseCheck = 'nt-browse-entry'
  s_web._SitesQueue.Defaults.Style.BrowseOtherButtonWithText = ''
  s_web._SitesQueue.Defaults.Style.BrowseOtherButtonWithoutText = ''
  s_web._SitesQueue.Defaults.Style.FormOtherButtonWithText = ''
  s_web._SitesQueue.Defaults.Style.FormOtherButtonWithoutText = ''
  s_web._SitesQueue.Defaults.Style.BrowseHighlightColor = 314111
  s_web._SitesQueue.Defaults.Style.BrowseOverColor = -1
  s_web._SitesQueue.Defaults.Style.BrowseOneColor = 16777215
  s_web._SitesQueue.Defaults.Style.BrowseTwoColor = 16315633
  s_web._SitesQueue.Defaults.HtmlClass = 'nt-html'
  s_web._SitesQueue.Defaults.BodyClass = 'PageBody'
  s_web._SitesQueue.Defaults.BodyDivClass = 'PageBodyDiv'
  s_web._SitesQueue.Defaults.BusyClass = 'nt-busy'
  s_web._SitesQueue.Defaults.BusyImage = '/images/_busy.gif'
  s_web._SitesQueue.Defaults.MessageClass = 'nt-width-99 nt-alert ui-state-error ui-corner-all'
  s_web._SitesQueue.Defaults.UploadButton.Name = 'upload_btn'
  s_web._SitesQueue.Defaults.UploadButton.TextValue = clip('Upload') !s_web.Translate('Upload',Net:HtmlOk) ! button text will be cleaned later on.
  s_web._SitesQueue.Defaults.UploadButton.Class = 'MainButton'
  s_web._SitesQueue.Defaults.UploadButton.ToolTip = clip('Click here to Upload the file') !s_web.Translate('Click here to Upload the file')
  s_web._SitesQueue.Defaults.LookupButton.Name = 'lookup_btn'
  s_web._SitesQueue.Defaults.LookupButton.TextValue = clip('...') !s_web.Translate('...',Net:HtmlOk) ! button text will be cleaned later on.
  s_web._SitesQueue.Defaults.LookupButton.Class = 'LookupButton'
  s_web._SitesQueue.Defaults.LookupButton.ToolTip = clip('Click here to Search for a value') !s_web.Translate('Click here to Search for a value')
  s_web._SitesQueue.Defaults.SaveButton.Name = 'save_btn'
  s_web._SitesQueue.Defaults.SaveButton.TextValue = clip('Save') !s_web.Translate('Save',Net:HtmlOk) ! button text will be cleaned later on.
  s_web._SitesQueue.Defaults.SaveButton.Image = '/images/psave.png'
  s_web._SitesQueue.Defaults.SaveButton.Class = 'MainButtonIcon'
  s_web._SitesQueue.Defaults.SaveButton.ToolTip = clip('Click on this to Save the form') !s_web.Translate('Click on this to Save the form')
  s_web._SitesQueue.Defaults.CancelButton.Name = 'cancel_btn'
  s_web._SitesQueue.Defaults.CancelButton.TextValue = clip('Cancel') !s_web.Translate('Cancel',Net:HtmlOk) ! button text will be cleaned later on.
  s_web._SitesQueue.Defaults.CancelButton.Image = '/images/pcancel.png'
  s_web._SitesQueue.Defaults.CancelButton.Class = 'MainButtonIcon'
  s_web._SitesQueue.Defaults.CancelButton.ToolTip = clip('Click on this to Cancel the form') !s_web.Translate('Click on this to Cancel the form')
  s_web._SitesQueue.Defaults.DeletefButton.Name = 'deletef_btn'
  s_web._SitesQueue.Defaults.DeletefButton.TextValue = clip('Delete') !s_web.Translate('Delete',Net:HtmlOk) ! button text will be cleaned later on.
  s_web._SitesQueue.Defaults.DeletefButton.Image = '/images/pdelete.png'
  s_web._SitesQueue.Defaults.DeletefButton.Class = 'MainButtonIcon'
  s_web._SitesQueue.Defaults.DeletefButton.ToolTip = clip('Click here to Delete this record') !s_web.Translate('Click here to Delete this record')
  s_web._SitesQueue.Defaults.CloseButton.Name = 'close_btn'
  s_web._SitesQueue.Defaults.CloseButton.TextValue = clip('Close') !s_web.Translate('Close',Net:HtmlOk) ! button text will be cleaned later on.
  s_web._SitesQueue.Defaults.CloseButton.Image = '/images/pcancel.png'
  s_web._SitesQueue.Defaults.CloseButton.Class = 'MainButtonIcon'
  s_web._SitesQueue.Defaults.CloseButton.ToolTip = clip('Click here to Close this form') !s_web.Translate('Click here to Close this form')
  s_web._SitesQueue.Defaults.InsertButton.Name = 'insert_btn'
  s_web._SitesQueue.Defaults.InsertButton.TextValue = clip('Insert') !s_web.Translate('Insert',Net:HtmlOk) ! button text will be cleaned later on.
  s_web._SitesQueue.Defaults.InsertButton.Image = '/images/pinsert.png'
  s_web._SitesQueue.Defaults.InsertButton.Class = 'MainButtonIcon'
  s_web._SitesQueue.Defaults.InsertButton.ToolTip = clip('Click here to Insert a new record') !s_web.Translate('Click here to Insert a new record')
  s_web._SitesQueue.Defaults.ChangeButton.Name = 'change_btn'
  s_web._SitesQueue.Defaults.ChangeButton.TextValue = clip('Change') !s_web.Translate('Change',Net:HtmlOk) ! button text will be cleaned later on.
  s_web._SitesQueue.Defaults.ChangeButton.Image = '/images/pchange.png'
  s_web._SitesQueue.Defaults.ChangeButton.Class = 'MainButtonIcon'
  s_web._SitesQueue.Defaults.ChangeButton.ToolTip = clip('Click here to Change the highlighted record') !s_web.Translate('Click here to Change the highlighted record')
  s_web._SitesQueue.Defaults.ViewButton.Name = 'view_btn'
  s_web._SitesQueue.Defaults.ViewButton.TextValue = clip('View') !s_web.Translate('View',Net:HtmlOk) ! button text will be cleaned later on.
  s_web._SitesQueue.Defaults.ViewButton.Image = '/images/psearch.png'
  s_web._SitesQueue.Defaults.ViewButton.Class = 'MainButtonIcon'
  s_web._SitesQueue.Defaults.ViewButton.ToolTip = clip('Click here to view details of the highlighted record') !s_web.Translate('Click here to view details of the highlighted record')
  s_web._SitesQueue.Defaults.DeletebButton.Name = 'deleteb_btn'
  s_web._SitesQueue.Defaults.DeletebButton.TextValue = clip('Delete') !s_web.Translate('Delete',Net:HtmlOk) ! button text will be cleaned later on.
  s_web._SitesQueue.Defaults.DeletebButton.Image = '/images/pdelete.png'
  s_web._SitesQueue.Defaults.DeletebButton.Class = 'MainButtonIcon'
  s_web._SitesQueue.Defaults.DeletebButton.ToolTip = clip('Click here to Delete the highlighted record') !s_web.Translate('Click here to Delete the highlighted record')
  s_web._SitesQueue.Defaults.SelectButton.Name = 'select_btn'
  s_web._SitesQueue.Defaults.SelectButton.TextValue = clip('Select') !s_web.Translate('Select',Net:HtmlOk) ! button text will be cleaned later on.
  s_web._SitesQueue.Defaults.SelectButton.Image = '/images/pselect.png'
  s_web._SitesQueue.Defaults.SelectButton.Class = 'MainButtonIcon'
  s_web._SitesQueue.Defaults.SelectButton.ToolTip = clip('Click here to Select the highlighted record') !s_web.Translate('Click here to Select the highlighted record')
  s_web._SitesQueue.Defaults.BrowseCancelButton.Name = 'browsecancel_btn'
  s_web._SitesQueue.Defaults.BrowseCancelButton.TextValue = clip('Cancel') !s_web.Translate('Cancel',Net:HtmlOk) ! button text will be cleaned later on.
  s_web._SitesQueue.Defaults.BrowseCancelButton.Image = '/images/pcancel.png'
  s_web._SitesQueue.Defaults.BrowseCancelButton.Class = 'MainButtonIcon'
  s_web._SitesQueue.Defaults.BrowseCancelButton.ToolTip = clip('Click here to return without selecting anything') !s_web.Translate('Click here to return without selecting anything')
  s_web._SitesQueue.Defaults.BrowseCloseButton.Name = 'browseclose_btn'
  s_web._SitesQueue.Defaults.BrowseCloseButton.TextValue = clip('Close') !s_web.Translate('Close',Net:HtmlOk) ! button text will be cleaned later on.
  s_web._SitesQueue.Defaults.BrowseCloseButton.Image = '/images/pcancel.png'
  s_web._SitesQueue.Defaults.BrowseCloseButton.Class = 'MainButtonIcon'
  s_web._SitesQueue.Defaults.BrowseCloseButton.ToolTip = clip('Click here to Close this browse') !s_web.Translate('Click here to Close this browse')
  s_web._SitesQueue.Defaults.SmallInsertButton.Name = 'insert_btn'
  s_web._SitesQueue.Defaults.SmallInsertButton.TextValue = clip('Insert') !s_web.Translate('Insert',Net:HtmlOk) ! button text will be cleaned later on.
  s_web._SitesQueue.Defaults.SmallInsertButton.Class = 'SmallButtonIcon'
  s_web._SitesQueue.Defaults.SmallInsertButton.ToolTip = clip('Click here to Insert a new record') !s_web.Translate('Click here to Insert a new record')
  s_web._SitesQueue.Defaults.SmallChangeButton.Name = 'change_btn'
  s_web._SitesQueue.Defaults.SmallChangeButton.TextValue = clip('Change') !s_web.Translate('Change',Net:HtmlOk) ! button text will be cleaned later on.
  s_web._SitesQueue.Defaults.SmallChangeButton.Class = 'SmallButtonIcon'
  s_web._SitesQueue.Defaults.SmallChangeButton.ToolTip = clip('Click here to Change this record') !s_web.Translate('Click here to Change this record')
  s_web._SitesQueue.Defaults.SmallViewButton.Name = 'view_btn'
  s_web._SitesQueue.Defaults.SmallViewButton.TextValue = clip('View') !s_web.Translate('View',Net:HtmlOk) ! button text will be cleaned later on.
  s_web._SitesQueue.Defaults.SmallViewButton.Class = 'SmallButtonIcon'
  s_web._SitesQueue.Defaults.SmallViewButton.ToolTip = clip('Click here to view details of this record') !s_web.Translate('Click here to view details of this record')
  s_web._SitesQueue.Defaults.SmallDeleteButton.Name = 'deleteb_btn'
  s_web._SitesQueue.Defaults.SmallDeleteButton.TextValue = clip('Delete') !s_web.Translate('Delete',Net:HtmlOk) ! button text will be cleaned later on.
  s_web._SitesQueue.Defaults.SmallDeleteButton.Class = 'SmallButtonIcon'
  s_web._SitesQueue.Defaults.SmallDeleteButton.ToolTip = clip('Click here to Delete this record') !s_web.Translate('Click here to Delete this record')
  s_web._SitesQueue.Defaults.SmallSelectButton.Name = 'select_btn'
  s_web._SitesQueue.Defaults.SmallSelectButton.TextValue = clip('Select') !s_web.Translate('Select',Net:HtmlOk) ! button text will be cleaned later on.
  s_web._SitesQueue.Defaults.SmallSelectButton.Class = 'SmallButtonIcon'
  s_web._SitesQueue.Defaults.SmallSelectButton.ToolTip = clip('Click here to Select this record') !s_web.Translate('Click here to Select this record')
  s_web._SitesQueue.Defaults.LocateButton.Name = 'locate_btn'
  s_web._SitesQueue.Defaults.LocateButton.TextValue = clip('Search') !s_web.Translate('Search',Net:HtmlOk) ! button text will be cleaned later on.
  s_web._SitesQueue.Defaults.LocateButton.Image = '/images/psearch.png'
  s_web._SitesQueue.Defaults.LocateButton.Class = 'MainButtonIcon'
  s_web._SitesQueue.Defaults.LocateButton.ToolTip = clip('Click here to start the Search') !s_web.Translate('Click here to start the Search')
  s_web._SitesQueue.Defaults.FirstButton.Name = 'first_btn'
  s_web._SitesQueue.Defaults.FirstButton.TextValue = clip('Top') !s_web.Translate('Top',Net:HtmlOk) ! button text will be cleaned later on.
  s_web._SitesQueue.Defaults.FirstButton.Image = '/images/listtop.png'
  s_web._SitesQueue.Defaults.FirstButton.ImageWidth = '16'
  s_web._SitesQueue.Defaults.FirstButton.ImageHeight = '16'
  s_web._SitesQueue.Defaults.FirstButton.ImageAlt = clip('') !s_web.Translate('')
  s_web._SitesQueue.Defaults.FirstButton.Class = 'MainButtonIcon'
  s_web._SitesQueue.Defaults.FirstButton.ToolTip = clip('Click here to go to the First page in the list') !s_web.Translate('Click here to go to the First page in the list')
  s_web._SitesQueue.Defaults.PreviousButton.Name = 'previous_btn'
  s_web._SitesQueue.Defaults.PreviousButton.TextValue = clip('Back') !s_web.Translate('Back',Net:HtmlOk) ! button text will be cleaned later on.
  s_web._SitesQueue.Defaults.PreviousButton.Image = '/images/listback.png'
  s_web._SitesQueue.Defaults.PreviousButton.ImageWidth = '16'
  s_web._SitesQueue.Defaults.PreviousButton.ImageHeight = '16'
  s_web._SitesQueue.Defaults.PreviousButton.ImageAlt = clip('') !s_web.Translate('')
  s_web._SitesQueue.Defaults.PreviousButton.Class = 'MainButtonIcon'
  s_web._SitesQueue.Defaults.PreviousButton.ToolTip = clip('Click here to go to the Previous page in the list') !s_web.Translate('Click here to go to the Previous page in the list')
  s_web._SitesQueue.Defaults.NextButton.Name = 'next_btn'
  s_web._SitesQueue.Defaults.NextButton.TextValue = clip('Next') !s_web.Translate('Next',Net:HtmlOk) ! button text will be cleaned later on.
  s_web._SitesQueue.Defaults.NextButton.Image = '/images/listnext.png'
  s_web._SitesQueue.Defaults.NextButton.ImageWidth = '16'
  s_web._SitesQueue.Defaults.NextButton.ImageHeight = '16'
  s_web._SitesQueue.Defaults.NextButton.ImageAlt = clip('') !s_web.Translate('')
  s_web._SitesQueue.Defaults.NextButton.Class = 'MainButtonIcon'
  s_web._SitesQueue.Defaults.NextButton.ToolTip = clip('Click here to go to the Next page in the list') !s_web.Translate('Click here to go to the Next page in the list')
  s_web._SitesQueue.Defaults.LastButton.Name = 'last_btn'
  s_web._SitesQueue.Defaults.LastButton.TextValue = clip('Bottom') !s_web.Translate('Bottom',Net:HtmlOk) ! button text will be cleaned later on.
  s_web._SitesQueue.Defaults.LastButton.Image = '/images/listbottom.png'
  s_web._SitesQueue.Defaults.LastButton.ImageWidth = '16'
  s_web._SitesQueue.Defaults.LastButton.ImageHeight = '16'
  s_web._SitesQueue.Defaults.LastButton.ImageAlt = clip('') !s_web.Translate('')
  s_web._SitesQueue.Defaults.LastButton.Class = 'MainButtonIcon'
  s_web._SitesQueue.Defaults.LastButton.ToolTip = clip('Click here to go to the Last page in the list') !s_web.Translate('Click here to go to the Last page in the list')
  s_web._SitesQueue.Defaults.PrintButton.Name = 'print_btn'
  s_web._SitesQueue.Defaults.PrintButton.TextValue = clip('Print') !s_web.Translate('Print',Net:HtmlOk) ! button text will be cleaned later on.
  s_web._SitesQueue.Defaults.PrintButton.Class = 'MainButton'
  s_web._SitesQueue.Defaults.PrintButton.ToolTip = clip('Click here to Print this page') !s_web.Translate('Click here to Print this page')
  s_web._SitesQueue.Defaults.StartButton.Name = 'start_btn'
  s_web._SitesQueue.Defaults.StartButton.TextValue = clip('Start') !s_web.Translate('Start',Net:HtmlOk) ! button text will be cleaned later on.
  s_web._SitesQueue.Defaults.StartButton.Class = 'MainButton'
  s_web._SitesQueue.Defaults.StartButton.ToolTip = clip('Click here to Start the report') !s_web.Translate('Click here to Start the report')
  s_web._SitesQueue.Defaults.DateLookupButton.Name = 'lookup_btn'
  s_web._SitesQueue.Defaults.DateLookupButton.TextValue = clip('...') !s_web.Translate('...',Net:HtmlOk) ! button text will be cleaned later on.
  s_web._SitesQueue.Defaults.DateLookupButton.Class = 'LookupButton'
  s_web._SitesQueue.Defaults.DateLookupButton.ToolTip = clip('Click here to select a date') !s_web.Translate('Click here to select a date')
  s_web._SitesQueue.Defaults.WizPreviousButton.Name = 'wizprevious_btn'
  s_web._SitesQueue.Defaults.WizPreviousButton.TextValue = clip('Previous') !s_web.Translate('Previous',Net:HtmlOk) ! button text will be cleaned later on.
  s_web._SitesQueue.Defaults.WizPreviousButton.Class = 'MainButton'
  s_web._SitesQueue.Defaults.WizPreviousButton.ToolTip = clip('Click here to go back to the Previous step') !s_web.Translate('Click here to go back to the Previous step')
  s_web._SitesQueue.Defaults.WizNextButton.Name = 'wiznext_btn'
  s_web._SitesQueue.Defaults.WizNextButton.TextValue = clip('Next') !s_web.Translate('Next',Net:HtmlOk) ! button text will be cleaned later on.
  s_web._SitesQueue.Defaults.WizNextButton.Class = 'MainButton'
  s_web._SitesQueue.Defaults.WizNextButton.ToolTip = clip('Click here to go to the Next step') !s_web.Translate('Click here to go to the Next step')
  s_web._SitesQueue.Defaults.SmallOtherButton.Name = 'other_btn'
  s_web._SitesQueue.Defaults.SmallOtherButton.TextValue = clip('Other') !s_web.Translate('Other',Net:HtmlOk) ! button text will be cleaned later on.
  s_web._SitesQueue.Defaults.SmallOtherButton.Class = 'SmallButton'
  s_web._SitesQueue.Defaults.SmallOtherButton.ToolTip = clip('') !s_web.Translate('')
  s_web._SitesQueue.Defaults.SmallPrintButton.Name = 'print_btn'
  s_web._SitesQueue.Defaults.SmallPrintButton.TextValue = clip('Print') !s_web.Translate('Print',Net:HtmlOk) ! button text will be cleaned later on.
  s_web._SitesQueue.Defaults.SmallPrintButton.Class = 'SmallButton'
  s_web._SitesQueue.Defaults.SmallPrintButton.ToolTip = clip('Click here to print this record') !s_web.Translate('Click here to print this record')
  s_web._SitesQueue.Defaults.CopyButton.Name = 'copy_btn'
  s_web._SitesQueue.Defaults.CopyButton.TextValue = clip('Copy') !s_web.Translate('Copy',Net:HtmlOk) ! button text will be cleaned later on.
  s_web._SitesQueue.Defaults.CopyButton.Class = 'MainButton'
  s_web._SitesQueue.Defaults.CopyButton.ToolTip = clip('Click here to copy the highlighted record') !s_web.Translate('Click here to copy the highlighted record')
  s_web._SitesQueue.Defaults.SmallCopyButton.Name = 'copy_btn'
  s_web._SitesQueue.Defaults.SmallCopyButton.TextValue = clip('Copy') !s_web.Translate('Copy',Net:HtmlOk) ! button text will be cleaned later on.
  s_web._SitesQueue.Defaults.SmallCopyButton.Class = 'SmallButton'
  s_web._SitesQueue.Defaults.SmallCopyButton.ToolTip = clip('Click here to copy this record') !s_web.Translate('Click here to copy this record')
  s_web._SitesQueue.Defaults.ClearButton.Name = 'clear_btn'
  s_web._SitesQueue.Defaults.ClearButton.TextValue = clip('Clear') !s_web.Translate('Clear',Net:HtmlOk) ! button text will be cleaned later on.
  s_web._SitesQueue.Defaults.ClearButton.Class = 'MainButton'
  s_web._SitesQueue.Defaults.ClearButton.ToolTip = clip('Click here to clear the locator') !s_web.Translate('Click here to clear the locator')
  s_web._SitesQueue.Defaults.LogoutButton.Name = 'logout_btn'
  s_web._SitesQueue.Defaults.LogoutButton.TextValue = clip('Logout') !s_web.Translate('Logout',Net:HtmlOk) ! button text will be cleaned later on.
  s_web._SitesQueue.Defaults.LogoutButton.Class = 'MainButton'
  s_web._SitesQueue.Defaults.LogoutButton.ToolTip = clip('Click here to logout') !s_web.Translate('Click here to logout')
  s_web._SitesQueue.Defaults.PreCompressed = 1
  s_web._SitesQueue.Defaults.HtmlCommonScripts = |
    s_web.AddScript('all.js') &|
    s_web.AddScript('dhtmlgoodies_calendar.js') &|
  ''
  s_web._SitesQueue.Defaults.HtmlMSIE6Scripts = |
    s_web.AddScript('msie6.js') &|
  ''
  s_web._SitesQueue.Defaults.HtmlCommonStyles = |
    s_web.AddStyle('all.css') &|
    s_web.AddStyle('pccs.css') &|
  ''
  s_web._SitesQueue.Defaults.HtmlMSIE6Styles = |
    s_web.AddStyle('msie6.css') &|
  ''
  s_web._SitesQueue.Defaults.HtmlMSIE7Styles = |
    s_web.AddStyle('msie7.css') &|
  ''
  s_web._SitesQueue.Defaults.HtmlMSIE8Styles = |
    s_web.AddStyle('msie8.css') &|
  ''
  s_web._SitesQueue.Defaults.HtmlMSIE9Styles = |
    s_web.AddStyle('msie9.css') &|
  ''
  s_web._SitesQueue.Defaults.HtmlFireFoxStyles = |
    s_web.AddStyle('firefox.css') &|
  ''
  s_web._SitesQueue.Defaults.HtmlSafariStyles = |
    s_web.AddStyle('safari.css') &|
  ''
  s_web._SitesQueue.Defaults.HtmlChromeStyles = |
    s_web.AddStyle('chrome.css') &|
    s_web.AddStyle('pccschrome.css') &|
  ''
  s_web._SitesQueue.Defaults.HtmlOperaStyles = |
    s_web.AddStyle('opera.css') &|
  ''
  s_web._SitesQueue.Defaults.HtmlMozillaStyles = |
    s_web.AddStyle('firefox.css') &|
  ''
  Put(s_web._SitesQueue)
  If Net:ShortInit
    ReturnValue = Level:Notify
    Return ReturnValue
  End
  !--------------------------------------------------------------
  if p_web.error <> 0
    ! Put code in here to handle if the object does not initialise properly
  end
  !! Change Version Here To Determine Which Phase This Program is !!
  tmp:VersionNumber = 'Version Number: ' & kVersionNumber & '.' & kWebserverCurrentVersion
  ?VersionNumber:Prompt{prop:Text} = tmp:VersionNumber
  glo:VersionNumber = tmp:VersionNumber
  
  ! Do not start logging (DBH: 02/03/2009)
  web:EnableLogging = 0
  Do DefineListboxStyle
  IF ?AmendDetails{Prop:Checked} = True
    UNHIDE(?LookupFile)
    UNHIDE(?Button4)
    UNHIDE(?LookupFile:2)
    ENABLE(?glo:WebserverPort)
    ENABLE(?glo:DataPath)
    ENABLE(?Path:Web)
  END
  IF ?AmendDetails{Prop:Checked} = False
    HIDE(?LookupFile)
    HIDE(?Button4)
    HIDE(?LookupFile:2)
    DISABLE(?glo:WebserverPort)
    DISABLE(?glo:DataPath)
    DISABLE(?Path:Web)
  END
  FileLookup1.Init
  FileLookup1.ClearOnCancel = True
  FileLookup1.Flags=BOR(FileLookup1.Flags,FILE:LongName)   ! Allow long filenames
  FileLookup1.Flags=BOR(FileLookup1.Flags,FILE:Directory)  ! Allow Directory Dialog
  FileLookup1.SetMask('All Files','*.*')                   ! Set the file mask
  FileLookup4.Init
  FileLookup4.ClearOnCancel = True
  FileLookup4.Flags=BOR(FileLookup4.Flags,FILE:LongName)   ! Allow long filenames
  FileLookup4.Flags=BOR(FileLookup4.Flags,FILE:Directory)  ! Allow Directory Dialog
  FileLookup4.SetMask('All Files','*.*')                   ! Set the file mask
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  p_web.Kill()                              ! Generated by NetTalk Extension
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:STOCK.Close
  END
  GlobalErrors.SetProcedureName
  remove(TagFile)
  remove(SBO_OutFaultParts)
  REMOVE(StockReceiveTmp)
  REMOVE(sbo_outparts)
  close(tempFaultCodes)
  remove(tempFaultCodes)
  close(tempAuditQueue)
  remove(tempAuditQueue)
  REMOVE(SBO_GenericFile)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?AmendDetails
      IF ?AmendDetails{Prop:Checked} = True
        UNHIDE(?LookupFile)
        UNHIDE(?Button4)
        UNHIDE(?LookupFile:2)
        ENABLE(?glo:WebserverPort)
        ENABLE(?glo:DataPath)
        ENABLE(?Path:Web)
      END
      IF ?AmendDetails{Prop:Checked} = False
        HIDE(?LookupFile)
        HIDE(?Button4)
        HIDE(?LookupFile:2)
        DISABLE(?glo:WebserverPort)
        DISABLE(?glo:DataPath)
        DISABLE(?Path:Web)
      END
      ThisWindow.Reset
      If ~0{prop:AcceptAll}
          If AmendDetails = 1
              Beep(Beep:SystemExclamation)  ;  Yield()
              Case Message('After any changes are made, the program will quit. '&|
                  '|Anyone connected to this websever at the time will lose information.'&|
                  '|'&|
                  '|Are you sure you want to continue?','Amend Details',|
                             icon:exclamation,'&Yes|&No',2,2)
                  Of 1 ! &Yes Button
                      ?AmendDetails{prop:Disable} = 1
                      ?Cancel{prop:Disable} = 1
                  Of 2 ! &No Button
                      AmendDetails = 0
                      Display()
                      Cycle
              End!Case Message
          End
      End ! If ~0{prop:AcceptAll}
    OF ?LookupFile
      ThisWindow.Update
      glo:DataPath = FileLookup1.Ask(1)
      DISPLAY
    OF ?LookupFile:2
      ThisWindow.Update
      Path:Web = FileLookup4.Ask(1)
      DISPLAY
    OF ?Button4
      ThisWindow.Update
      If glo:WebServerPort <> '' And glo:DataPath <> '' And Path:Web <> ''
          PUTINI('DEFAULTS','port',glo:WebserverPort,Clip(glo:LocalPath) & '\WEBSERVER.INI')
          PUTINI('DEFAULTS','pathtodata',Clip(glo:DataPath),Clip(glo:LocalPath) & '\WEBSERVER.INI')
          PUTINI('DEFAULTS','pathtoweb',Clip(Path:Web),Clip(glo:LocalPath) & '\WEBSERVER.INI')
          Halt()
      End !If glo:WebServerPort <> '' And glo:DatPath <> ''
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    p_web.TakeEvent()                 ! Generated by NetTalk Extension
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      If glo:webserverport = ''
          0{prop:Iconize} = 0
      End ! If glo:webserverpost = ''
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


p_web.AddLog PROCEDURE(String p_Data,<string p_ip>)


  CODE
    self._wait()
    If web:EnableLogging > 0
      clear(LogQueue)
      LogQueue.Port = Self.Port
      LogQueue.Date = today()
      LogQueue.Time = clock()
      LogQueue.Desc = p_Data
      Add(LogQueue,1)
      Loop While Records(LogQueue) > 500
        Get(LogQueue,501)
        Delete(LogQueue)
      End
    End
    self._release()
  PARENT.AddLog(p_Data,p_ip)


p_web.StartNewThread PROCEDURE(NetWebServerRequestDataType p_RequestData)

!loc:RequestData    Group(NetWebServerRequestDataType).
!loc:OverString     String(size(loc:RequestData)),over(loc:RequestData)

  CODE
    loc:RequestData :=: p_RequestData
    web:PagesServed = self._PagesServed + 1
    if p_RequestData.DataStringLen >= 4
      case (upper(p_RequestData.DataString[1 : 4]))
      of 'POST'
        web:LastPost = p_RequestData.DataString[1 : p_RequestData.DataStringLen]
        display (?web:LastPost)
      of 'GET '
        web:LastGet = p_RequestData.DataString[1 : p_RequestData.DataStringLen]
        display (?web:LastGet)
      else
        web:LastGet = p_RequestData.DataString[1 : p_RequestData.DataStringLen]
        display (?web:LastGet)
      end
    end
    self.AddLog(p_RequestData.DataString,p_RequestData.FromIP)
   ! cwversion = 6300
    RESUME(START (WebHandler, 35000, loc:OverString))
    RETURN ! Don't call parent
  PARENT.StartNewThread(p_RequestData)


p_web.TakeEvent PROCEDURE


  CODE
  PARENT.TakeEvent
    if event() = Event:openWindow
      web:StartDate = Today()
      web:StartTime = Clock()
    End
    If Event() = Event:Accepted and Field() = ?Clear
      Free(LogQueue)
      web:LastGet = ''
      web:LastPost = ''
      display()
    End
    If Field() = ?LogQueue and Event() = Event:NewSelection
      Get(LogQueue,Choice(?LogQueue))
      If ErrorCode() = 0
        Case Upper(Sub(LogQueue.Desc,1,3))
        Of 'POS'
          web:LastPost = LogQueue.Desc
        Of 'GET'
          web:LastGet = LogQueue.Desc
        Else
          web:LastGet = LogQueue.Desc
        End
        Display()
      End
    End

Waybill PROCEDURE (<NetWebServerWorker p_web>)             ! Generated from procedure template - Report

  ! The NetTalk Extension to report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
Loc:Html      String(1024)
Progress:Thermometer BYTE                                  !
locWaybillNumber     LONG                                  !
FromAddressGroup     GROUP,PRE(from)                       !
CompanyName          STRING(30)                            !
AddressLine1         STRING(30)                            !
AddressLine2         STRING(30)                            !
AddressLine3         STRING(39)                            !
TelephoneNumber      STRING(30)                            !
ContactName          STRING(60)                            !
EmailAddress         STRING(255)                           !
                     END                                   !
ToAddressGroup       GROUP,PRE(to)                         !
AccountNumber        STRING(30)                            !
CompanyName          STRING(30)                            !
AddressLine1         STRING(30)                            !
AddressLine2         STRING(30)                            !
AddressLine3         STRING(30)                            !
TelephoneNumber      STRING(30)                            !
ContactName          STRING(30)                            !
EmailAddress         STRING(255)                           !
                     END                                   !
locDateDespatched    DATE                                  !
locTimeDespatched    TIME                                  !
locCourier           STRING(30)                            !
locConsignmentNumber STRING(30)                            !
locBarCode           STRING(60)                            !
locJobNumber         STRING(30)                            !
locIMEINumber        STRING(30)                            !
locOrderNumber       STRING(30)                            !
locSecurityPackNumber STRING(30)                           !
locAccessories       STRING(255)                           !
locExchanged         STRING(1)                             !
Process:View         VIEW(WAYBILLJ)
                       PROJECT(waj:JobNumber)
                       PROJECT(waj:WayBillNumber)
                     END
ReportPageNumber     LONG,AUTO
ProgressWindow       WINDOW('Report JOBS'),AT(,,142,59),FONT('Tahoma',8,COLOR:Black,FONT:regular),CENTER,TIMER(1),GRAY,DOUBLE
                       PROGRESS,USE(Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),FLAT,LEFT,MSG('Cancel Report'),TIP('Cancel Report'),ICON('WACANCEL.ICO')
                     END

Report               REPORT('JOBS Report'),AT(250,3875,7750,6594),PAPER(PAPER:A4),PRE(RPT),FONT('Tahoma',8,COLOR:Black,FONT:regular),THOUS
                       HEADER,AT(250,250,7750,3250),USE(?Header),FONT('Tahoma',8,COLOR:Black,FONT:regular)
                         STRING('Courier:'),AT(4896,781),USE(?String23),TRN,FONT(,8,,)
                         STRING('Despatch Date:'),AT(4896,365),USE(?DespatchDate),TRN,FONT(,8,,)
                         STRING(@d6),AT(6042,365),USE(locDateDespatched),TRN,FONT(,8,,FONT:bold)
                         STRING(@t1b),AT(6042,573),USE(locTimeDespatched),TRN,FONT(,8,,FONT:bold)
                         STRING('From Sender:'),AT(83,83),USE(?String16),TRN,FONT(,8,,)
                         STRING('WAYBILL REJECTION'),AT(3750,31,3781,),USE(?WaybillRejection),TRN,HIDE,RIGHT,FONT(,16,,FONT:bold)
                         STRING('Deliver To:'),AT(104,1667),USE(?String16:5),TRN,FONT(,8,,)
                         STRING('Tel No:'),AT(104,1042),USE(?String16:2),TRN,FONT(,8,,)
                         STRING('Page Number:'),AT(4896,990),USE(?String62),TRN
                         STRING(@s30),AT(938,1042),USE(from:TelephoneNumber),TRN,FONT(,8,,FONT:bold)
                         STRING('Contact Name:'),AT(104,2813),USE(?String16:7),TRN,FONT(,8,,)
                         STRING(@s60),AT(938,1198),USE(from:ContactName),TRN,FONT(,8,,FONT:bold)
                         STRING(@s255),AT(938,1354),USE(from:EmailAddress),TRN,FONT(,8,,FONT:bold)
                         STRING('Email:'),AT(104,1354),USE(?String16:4),TRN,FONT(,8,,)
                         STRING('WAYBILL NUMBER'),AT(3490,1667,3906,260),USE(?String39),TRN,CENTER,FONT(,12,,FONT:bold)
                         STRING('Contact Name:'),AT(104,1198),USE(?String16:3),TRN,FONT(,8,,)
                         STRING(@s30),AT(83,250,4687,365),USE(from:CompanyName),TRN,FONT(,12,,FONT:bold)
                         STRING(@s30),AT(3490,1979,3906,260),USE(locBarCode),CENTER,FONT('C39 High 12pt LJ3',12,COLOR:Black,,CHARSET:ANSI),COLOR(COLOR:White)
                         STRING(@s30),AT(83,542),USE(from:AddressLine1),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(83,708),USE(from:AddressLine2),TRN,FONT(,8,,FONT:bold)
                         STRING('Account Number:'),AT(104,1875),USE(?AccountNumber),TRN,FONT(,8,,)
                         STRING(@s30),AT(1042,1875,1927,188),USE(to:AccountNumber),TRN,FONT(,8,,FONT:bold)
                         STRING('Company Name:'),AT(104,2031),USE(?CompanyName),TRN,FONT(,8,,)
                         STRING(@s30),AT(1042,2031),USE(to:CompanyName),TRN,FONT(,8,,FONT:bold)
                         STRING('Address 1:'),AT(104,2188),USE(?Address1),TRN,FONT(,8,,)
                         STRING(@s30),AT(1042,2188),USE(to:AddressLine1),TRN,FONT(,8,,FONT:bold)
                         BOX,AT(52,2031,3177,156),USE(?Box1:2),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING('Address 2:'),AT(104,2344),USE(?Address2),TRN,FONT(,8,,)
                         STRING(@s30),AT(1042,2344),USE(to:AddressLine2),TRN,FONT(,8,,FONT:bold)
                         STRING('Suburb:'),AT(104,2500),USE(?Suburb),TRN,FONT(,8,,)
                         STRING(@s30),AT(1042,2500),USE(to:AddressLine3),TRN,FONT(,8,,FONT:bold)
                         STRING('Contact Name:'),AT(104,2813),USE(?ContactName),TRN,FONT(,8,,)
                         STRING(@s30),AT(1042,2813),USE(to:ContactName),TRN,FONT(,8,,FONT:bold)
                         LINE,AT(990,1875,0,1250),USE(?Line5),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(52,2969,3177,156),USE(?Box1:8),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(52,2656,3177,156),USE(?Box1:6),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING('Contact Number:'),AT(104,2656),USE(?ContactNumber),TRN,FONT(,8,,)
                         STRING(@s30),AT(1042,2656),USE(to:TelephoneNumber),TRN,FONT(,8,,FONT:bold)
                         BOX,AT(52,2813,3177,156),USE(?Box1:7),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(52,2188,3177,156),USE(?Box1:3),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING('Email:'),AT(104,2969),USE(?Email),TRN,FONT(,8,,)
                         STRING(@s255),AT(1042,2969),USE(to:EmailAddress),TRN,FONT(,8,,FONT:bold)
                         STRING('Despatch Time:'),AT(4896,573),USE(?DespatchTime),TRN,FONT(,8,,)
                         STRING(@s30),AT(83,875),USE(from:AddressLine3),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(3490,2292,3906,260),USE(locConsignmentNumber),TRN,CENTER,FONT('Arial',12,,FONT:bold,CHARSET:ANSI)
                         STRING(@s255),AT(4323,2969,3385,208),USE(glo:ErrorText),TRN,RIGHT,FONT(,8,,FONT:bold)
                         BOX,AT(52,2500,3177,156),USE(?Box1:5),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(52,2344,3177,156),USE(?Box1:4),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(52,1875,3177,156),USE(?Box1),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING(@s30),AT(6042,781),USE(locCourier),FONT(,,,FONT:bold)
                         STRING(@N3),AT(6042,990),USE(ReportPageNumber),FONT(,,,FONT:bold)
                       END
Detail                 DETAIL,AT(,,7750,229),USE(?Detail)
                         STRING(@s30),AT(104,0,1094,),USE(locJobNumber)
                         STRING(@s30),AT(1260,0,1302,),USE(locIMEINumber)
                         STRING(@s30),AT(2625,-10),USE(locOrderNumber),FONT(,7,,)
                         STRING(@s30),AT(4354,-10,958,),USE(locSecurityPackNumber),FONT(,7,,)
                         TEXT,AT(5375,-10,2021,146),USE(locAccessories),RESIZE
                         STRING(@s1),AT(7458,-10),USE(locExchanged)
                       END
                       FOOTER,AT(250,10531,7750,906),USE(?Footer)
                         TEXT,AT(125,0,7500,833),USE(stt:Text),TRN
                       END
                       FORM,AT(250,250,7750,11188),USE(?Form),FONT('Tahoma',8,COLOR:Black,FONT:regular)
                         STRING('Job No'),AT(156,3333,,156),USE(?JobNo),TRN,FONT(,7,,)
                         STRING('I.M.E.I. No'),AT(1313,3333),USE(?IMEINo),TRN,FONT(,7,,)
                         STRING('Order No'),AT(2615,3333),USE(?OrderNo),TRN,FONT(,7,,)
                         STRING('Accessories'),AT(5479,3333),USE(?Accessories),TRN,FONT(,7,,)
                         STRING('Security Pack No'),AT(4438,3333),USE(?SecurityPackNo),TRN,FONT(,7,,)
                         LINE,AT(104,3281,7292,0),USE(?Line6),COLOR(COLOR:Black)
                         LINE,AT(104,3542,7292,0),USE(?Line6:2),COLOR(COLOR:Black)
                       END
                     END
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeNoRecords          PROCEDURE(),DERIVED                 ! Method added to host embed code
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

ProgressMgr          StepLongClass                         ! Progress Manager
Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED                 ! Method added to host embed code
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Waybill Variables
  ! p_web.SSV('locWaybillNumber',)
  ! p_web.SSV('Waybill:Courier,)
  ! p_web.SSV('Waybill:FromType,)
  ! p_web.SSV('Waybill:FromAccount,)
  ! p_web.SSV('Waybill:ToType,)
  ! p_web.SSV('Waybill:ToAccount,)
  GlobalErrors.SetProcedureName('Waybill')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open                                     ! File DEFAULTS used by this procedure, so make sure it's RelationManager is open
  Relate:EXCHANGE.Open                                     ! File EXCHANGE used by this procedure, so make sure it's RelationManager is open
  Relate:JOBACC.SetOpenRelated()
  Relate:JOBACC.Open                                       ! File JOBACC used by this procedure, so make sure it's RelationManager is open
  Relate:STANTEXT.Open                                     ! File STANTEXT used by this procedure, so make sure it's RelationManager is open
  Relate:WAYBILLJ.SetOpenRelated()
  Relate:WAYBILLJ.Open                                     ! File WAYBILLJ used by this procedure, so make sure it's RelationManager is open
  Relate:WEBJOB.Open                                       ! File WEBJOB used by this procedure, so make sure it's RelationManager is open
  Access:JOBSE.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:SUBTRACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:WAYBILLS.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TRADEACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBS.UseFile                                      ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  locWaybillNumber = p_web.GSV('locWaybillNumber')
  Access:WAYBILLS.Clearkey(way:WayBillNumberKey)
  way:WayBillNumber = locWaybillNumber
  IF (Access:WAYBILLS.TryFetch(way:WayBillNumberKey))
  
  END
  SET(DEFAULTS,0)
  Access:DEFAULTS.Next()
  
  locCourier = p_web.GSV('Waybill:Courier')
  
  ! Set consignment number
  IF (way:FromAccount = p_web.GSV('ARC:AccountNumber'))
      IF (GETINI('PRINTING','SetWaybillPrefix',,Clip(Path()) & '\SB2KDEF.INI') = 1)
          locConsignmentNumber = Clip(GETINI('PRINTING','WaybillPrefix',,Clip(Path()) & '\SB2KDEF.INI')) & Format(way:WaybillNumber,@n07)
      ELSE
          locConsignmentNumber = 'VDC' & Format(way:WaybillNumber,@n07)
      END
  ELSE
      Access:TRADEACC.ClearKey(tra:Account_Number_Key)
      tra:Account_Number = way:FromAccount
      IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
          If clip(tra:RRCWaybillPrefix) <> ''
              locConsignmentNumber = clip(tra:RRCWaybillPrefix) & Format(way:WaybillNumber,@n07)
          ELSE
              locConsignmentNumber = 'VDC' & Format(way:WaybillNumber,@n07)
          END
      END
  END
  
  
  
  locDateDespatched = way:TheDate
  locTimeDespatched = way:TheTime
  
  ! Set addresses
  CASE p_web.GSV('Waybill:FromType')
  OF 'TRA'
      Access:TRADEACC.ClearKey(tra:Account_Number_Key)
      tra:Account_Number = p_web.GSV('Waybill:FromAccount')
      IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
          from:CompanyName        = tra:Company_Name
          from:AddressLine1       = tra:Address_Line1
          from:AddressLine2       = tra:Address_Line2
          from:AddressLine3       = tra:Address_Line3
          from:TelephoneNumber    = tra:Telephone_Number
          from:ContactName        = tra:Contact_Name
          from:EmailAddress       = tra:EmailAddress
      END
  
  OF 'SUB'
      Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
      sub:Account_Number = p_web.GSV('Waybill:FromAccount')
      IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
          from:CompanyName        = sub:Company_Name
          from:AddressLine1       = sub:Address_Line1
          from:AddressLine2       = sub:Address_Line2
          from:AddressLine3       = sub:Address_Line3
          from:TelephoneNumber    = sub:Telephone_Number
          from:ContactName        = sub:Contact_Name
          from:EmailAddress       = sub:EmailAddress
      END
  
  OF 'DEF'
      from:CompanyName        = def:User_Name
      from:AddressLine1       = def:Address_Line1
      from:AddressLine2       = def:Address_Line2
      from:AddressLine3       = def:Address_Line3
      from:TelephoneNumber    = def:Telephone_Number
      from:ContactName        = ''
      from:EmailAddress       = def:EmailAddress
  END
  
  CASE p_web.GSV('Waybill:ToType')
  OF 'TRA'
      Access:TRADEACC.ClearKey(tra:Account_Number_Key)
      tra:Account_Number = p_web.GSV('Waybill:ToAccount')
      IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
          to:AccountNumber      = tra:Account_Number
          to:CompanyName        = tra:Company_Name
          to:AddressLine1       = tra:Address_Line1
          to:AddressLine2       = tra:Address_Line2
          to:AddressLine3       = tra:Address_Line3
          to:TelephoneNumber    = tra:Telephone_Number
          to:ContactName        = tra:Contact_Name
          to:EmailAddress       = tra:EmailAddress
      END
  
  OF 'SUB'
      Access:SUBTRACC.clearkey(sub:Account_Number_Key)
      sub:Account_Number = p_web.GSV('Waybill:ToAccount')
      IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
          to:AccountNumber      = sub:Account_Number
          to:CompanyName        = sub:Company_Name
          to:AddressLine1       = sub:Address_Line1
          to:AddressLine2       = sub:Address_Line2
          to:AddressLine3       = sub:Address_Line3
          to:TelephoneNumber    = sub:Telephone_Number
          to:ContactName        = sub:Contact_Name
          to:EmailAddress       = sub:EmailAddress
      END
  
  END
  
  
  IF (way:WaybillID <> 0)
      IF (way:WaybillID = 13)
          ! This is a rejection
          SETTARGET(Report)
          ?WaybillRejection{PROP:Hide} = FALSE
          SETTARGET()
      END
  
      Access:TRADEACC.ClearKey(tra:Account_Number_Key)
      tra:Account_Number = way:FromAccount
      IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
          from:CompanyName    = tra:Company_Name
          from:AddressLine1   = tra:Address_Line1
          from:AddressLine2   = tra:Address_Line2
          from:AddressLine3   = tra:Address_Line3
          from:TelephoneNumber= tra:Telephone_Number
          from:ContactName    = tra:Contact_Name
          from:EmailAddress   = tra:EmailAddress
      END
  
      IF (way:WaybillID = 20) ! PUP to RRC. Get address from job
          Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
          sub:Account_Number = way:FromAccount
          IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
              from:CompanyName    = sub:Company_Name
              from:AddressLine1   = sub:Address_Line1
              from:AddressLine2   = sub:Address_Line2
              from:AddressLine3   = sub:Address_Line3
              from:TelephoneNumber= sub:Telephone_Number
              from:ContactName    = sub:Contact_Name
              from:EmailAddress   = sub:EmailAddress
          END
  
          IF (sub:VCPWaybillPrefix <> '')
              locConsignmentNumber = clip(sub:VCPWaybillPrefix) & Format(way:WayBillNumber,@n07)
          ELSE
              Access:TRADEACC.ClearKey(tra:Account_Number_Key)
              tra:Account_Number = sub:Main_Account_Number
              IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
                  IF (tra:VCPWaybillPrefix <> '')
                      locConsignmentNumber = clip(tra:VCPWaybillPrefix) & Format(way:WayBillNumber,@n07)
                  END
  
              END
          END
      END
  
      CASE way:WaybillID
      OF 1 OROF 5 OROF 6 OROF 11 OROF 12 OROF 13 OROF 20
          IF (way:ToAccount = 'CUSTOMER') ! PUP to Customer
              to:ContactName = ''
              Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
              sub:Account_Number = way:ToAccount
              IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:benign)
                  IF (sub:Use_Delivery_Address = 'YES')
                      to:ContactName = sub:Contact_Name
                  END
              END
              to:AccountNumber   = job:Account_Number
              to:CompanyName     = job:Company_Name
              to:AddressLine1    = job:Address_Line1
              to:AddressLine2    = job:Address_Line2
              to:AddressLine3    = job:Address_Line3
              to:TelephoneNumber = job:Telephone_Number
  
              to:EmailAddress    = jobe:EndUserEmailAddress
          ELSE
              Access:TRADEACC.ClearKey(tra:Account_Number_Key)
              tra:Account_Number = way:ToAccount
              IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
                  to:AccountNumber  = tra:Account_Number
                  to:CompanyName    = tra:Company_Name
                  to:AddressLine1   = tra:Address_Line1
                  to:AddressLine2   = tra:Address_Line2
                  to:AddressLine3   = tra:Address_Line3
                  to:TelephoneNumber= tra:Telephone_Number
                  to:ContactName    = tra:Contact_Name
                  to:EmailAddress   = tra:EmailAddress
              END
  
          END
      OF 21 OROF 22 ! RRC to PUP
          Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
          sub:Account_Number = way:ToAccount
          IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
              from:CompanyName    = sub:Company_Name
              from:AddressLine1   = sub:Address_Line1
              from:AddressLine2   = sub:Address_Line2
              from:AddressLine3   = sub:Address_Line3
              from:TelephoneNumber= sub:Telephone_Number
              from:ContactName    = sub:Contact_Name
              from:EmailAddress   = sub:EmailAddress
          END
      OF 300 ! Sundry Waybill
          ! Do this later.
      ELSE
          ! Get details from the job on the waybill
          Access:WAYBILLJ.ClearKey(waj:DescWaybillNoKey)
          waj:WayBillNumber = way:WayBillNumber
          IF (Access:WAYBILLJ.TryFetch(waj:DescWaybillNoKey) = Level:Benign)
              Access:JOBS.ClearKey(job:Ref_Number_Key)
              job:Ref_Number = waj:JobNumber
              IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
  
                  Access:JOBSE.ClearKey(jobe:RefNumberKey)
                  jobe:RefNumber = job:Ref_Number
                  IF (Access:JOBSE.tryfetch(jobe:RefNumberKey) = Level:Benign)
                  END
  
                  Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
                  sub:Account_Number = way:ToAccount
                  IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
                      IF (sub:UseCustDespAdd = 'YES')
                          to:AccountNumber   = job:Account_Number
                          to:CompanyName     = job:Company_Name
                          to:AddressLine1    = job:Address_Line1
                          to:AddressLine2    = job:Address_Line2
                          to:AddressLine3    = job:Address_Line3
                          to:TelephoneNumber = job:Telephone_Number
                          !changed bt Paul 28/04/2010 - log no 11419
                          if sub:Use_Delivery_Address = 'YES' then
                              to:ContactName     = clip(sub:Contact_Name)
                          Else
                              to:ContactName     = ''
                          End
                          !end change
                          to:EmailAddress    = jobe:EndUserEmailAddress
                      ELSE
                          to:AccountNumber   = job:Account_Number
                          to:CompanyName     = job:Company_Name_Delivery
                          to:AddressLine1    = job:Address_Line1_Delivery
                          to:AddressLine2    = job:Address_Line2_Delivery
                          to:AddressLine3    = job:Address_Line3_Delivery
                          to:TelephoneNumber = job:Telephone_Delivery
                          !changed bt Paul 28/04/2010 - log no 11419
                          if sub:Use_Delivery_Address = 'YES' then
                              to:ContactName     = clip(sub:Contact_Name)
                          Else
                              to:ContactName     = ''
                          End
                          !end change
                          to:EmailAddress    = jobe:EndUserEmailAddress
                      END
                  end
              END
          END
      END
  END
  
  locBarcode = '*' & CLIP(locConsignmentNumber) & '*'
  ! #12084 New standard texts (Bryan: 17/05/2011)
  Access:STANTEXT.Clearkey(stt:Description_Key)
  stt:Description = 'WAYBILL'
  IF (Access:STANTEXT.TryFetch(stt:Description_Key))
  END
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('Waybill',ProgressWindow)                   ! Restore window settings from non-volatile store
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.SetReportTarget(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ProgressWindow{Prop:Timer} = 10                          ! Assign timer interval
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:WAYBILLJ, ?Progress:PctText, Progress:Thermometer, ProgressMgr, waj:JobNumber)
  ThisReport.AddSortOrder(waj:JobNumberKey)
  ThisReport.AddRange(waj:WayBillNumber,locWaybillNumber)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{Prop:Text}=''
  Relate:WAYBILLJ.SetQuickScan(1,Propagate:OneMany)
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom=True
  Previewer.Maximize=True
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
      SELF.SetReportTarget(PDFReporter.IReportGenerator)
    self.SkipPreview = True
    ProgressWindow{prop:hide} = 1
    loc:PDFName = clip(p_web.site.WebFolderPath) & '\reports\$$$' & format(random(1,99999),@n05) &'.pdf'
  End
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:EXCHANGE.Close
    Relate:JOBACC.Close
    Relate:STANTEXT.Close
    Relate:WAYBILLJ.Close
    Relate:WEBJOB.Close
  END
  IF SELF.Opened
    INIMgr.Update('Waybill',ProgressWindow)                ! Save window data to non-volatile store
  END
  ProgressMgr.Kill()
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      Loc:html = '<script type="text/javascript">alert('''&clip('No Records')&''');top.close();</script>'
      p_web.ParseHTML(loc:html)
    Else
      p_web.ForceNoCache = 1
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(loc:PDFName)
    End
  End
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    Report$?ReportPageNumber{PROP:PageNo}=True
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
    If Not p_Web &= NULL
      loc:NoRecords = 1
      Return
    End
  PARENT.TakeNoRecords


ThisWindow.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeRecord()
    If Not p_web &= Null
      p_web.NoOp()
    End
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  Access:JOBS.ClearKey(job:Ref_Number_Key)
  job:Ref_Number = waj:JobNumber
  IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
      RETURN Level:User
  END
  Access:WEBJOB.ClearKey(wob:RefNumberKey)
  wob:RefNumber = job:Ref_Number
  IF (Access:WEBJOB.TryFetch(wob:RefNumberKey))
      RETURN Level:User
  END
  
  locExchanged = ''
  locJobNumber = CLIP(job:Ref_Number) & '-' & p_web.GSV('BookingBranchID') & CLIP(wob:JobNumber)
  IF (waj:WayBillNumber = job:Exchange_Consignment_Number)
      Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
      xch:Ref_Number = job:Exchange_Unit_Number
      IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key))
          locIMEINumber = waj:IMEINumber
      ELSE
          locIMEINumber = xch:ESN
      END
  ELSE
      locIMEINumber = waj:IMEINumber
      IF (job:Exchange_Unit_Number <> 0)
          locExchanged = 'E'
      END
  END
  locSecurityPackNumber = waj:SecurityPackNumber
  locAccessories = ''
  
  If job:Exchange_Unit_Number = 0 Or way:WaybillID = 3 Or way:WaybillID = 6 Or way:WayBillID = 8 Or |
      way:WaybillID = 10 Or way:WaybillID = 11 Or way:WaybillID = 12
  
      Access:JOBACC.ClearKey(jac:Ref_Number_Key)
      jac:Ref_Number = job:Ref_Number
      Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
      Loop
          If Access:JOBACC.NEXT()
              Break
          End !If
          If jac:Ref_Number <> job:Ref_Number      |
              Then Break.  ! End If
          ! Inserting (DBH 08/03/2007) # 8703 - Show all accessories if the isn't a 0 (to ARC) or 1 (to RRC)
          If way:WaybillType = 0 Or way:WaybillType = 1
              ! End (DBH 08/03/2007) #8703
              !Only show accessories that were sent to ARC - 4285 (DBH: 26-05-2004)
              If jac:Attached <> True
                  Cycle
              End !If jac:Attached <> True
          End ! If way:WaybillType <> 0 And way:WaybillType <> 1
          If locAccessories = ''
              locAccessories = Clip(jac:Accessory)
          Else !If locAccessories = ''
              locAccessories = Clip(locAccessories) & ', ' & Clip(jac:Accessory)
          End !If tmp:Accessories = ''
      End !Loop
  End !If waj:JobType = 'JOB' And job:Exchange_Unit_Number = 0
  
  locOrderNumber = job:Order_Number
  ReturnValue = PARENT.TakeRecord()
  PRINT(RPT:Detail)
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','WebServer_Phase4','Waybill','Waybill','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.CompressText   = True
  SELF.CompressImages = True
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    SELF.SetFileName(clip(loc:PDFName))
  End

InvoiceSubAccounts   PROCEDURE  (func:AccountNumber)       ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
  CODE
    Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
    sub:Account_Number  = func:AccountNumber
    If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Found
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = sub:Main_Account_Number
        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Found
            If tra:Use_Sub_Accounts = 'YES' And tra:Invoice_Sub_Accounts = 'YES'
                Return Level:Fatal
            End !If tra:Use_Sub_Accounts = 'YES' And tra:Invoice_Sub_Accounts = 'YES'
        Else! If Access:TRADEACC.Tryfetch(tra:Account_Number) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End! If Access:TRADEACC.Tryfetch(tra:Account_Number) = Level:Benign
    Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
    Return Level:Benign
VATRate_OLD          PROCEDURE  (fAccountNumber,fType)     ! Declare Procedure
rtnValue             REAL                                  !
FilesOpened     BYTE(0)
  CODE
    DO openFiles
    rtnValue = 0
    If InvoiceSubAccounts(fAccountNumber)
        Access:VATCODE.Clearkey(vat:Vat_Code_Key)
        Case fType
        Of 'L'
            vat:Vat_Code    = sub:Labour_Vat_Code
        Of 'P'
            vat:Vat_Code    = sub:Parts_Vat_Code
        End !Case func:Type
        If Access:VATCODE.Tryfetch(vat:Vat_Code_Key) = Level:Benign
            !Found
            rtnvalue =  vat:Vat_Rate
        Else ! If Access:VATCODE.Tryfetch(vat:Vat_Code_Key) = Level:Benign
            !Error
        End !If Access:VATCODE.Tryfetch(vat:Vat_Code_Key) = Level:Benign
    Else !If InvoiceSubAccounts(func:AccountNumber)
        Access:VATCODE.Clearkey(vat:Vat_Code_Key)
        Case fType
        Of 'L'
            vat:Vat_Code    = tra:Labour_Vat_Code
        Of 'P'
            vat:Vat_Code    = tra:Parts_Vat_Code
        End !Case func:Type
        If Access:VATCODE.Tryfetch(vat:Vat_Code_Key) = Level:Benign
            !Found
            rtnValue =  vat:Vat_Rate
        Else ! If Access:VATCODE.Tryfetch(vat:Vat_Code_Key) = Level:Benign
            !Error
        End !If Access:VATCODE.Tryfetch(vat:Vat_Code_Key) = Level:Benign
    End !If InvoiceSubAccounts(func:AccountNumber)
    Do CloseFiles
    Return rtnValue


!--------------------------------------
OpenFiles  ROUTINE
  Access:SUBTRACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBTRACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:VATCODE.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:VATCODE.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:SUBTRACC.Close
     Access:TRADEACC.Close
     Access:VATCODE.Close
     FilesOpened = False
  END
JobCard PROCEDURE (<NetWebServerWorker p_web>)             ! Generated from procedure template - Report

  ! The NetTalk Extension to report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
Loc:Html      String(1024)
tmp:JobNumber        LONG                                  !Job Number
tmp:PrintedBy        STRING(60)                            !
who_booked           STRING(50)                            !
Webmaster_Group      GROUP,PRE(tmp)                        !===============================================
Ref_number           STRING(20)                            !
BarCode              STRING(20)                            !
BranchIdentification STRING(2)                             !
                     END                                   !
save_job2_id         USHORT,AUTO                           !
tmp:accessories      STRING(255)                           !
RejectRecord         LONG,AUTO                             !
save_joo_id          USHORT,AUTO                           !
save_maf_id          USHORT,AUTO                           !
save_jac_id          USHORT,AUTO                           !
save_wpr_id          USHORT,AUTO                           !
save_par_id          USHORT,AUTO                           !
save_lac_id          USHORT                                !
save_loa_id          USHORT                                !
LocalRequest         LONG,AUTO                             !
LocalResponse        LONG,AUTO                             !
FilesOpened          LONG                                  !
WindowOpened         LONG                                  !
RecordsToProcess     LONG,AUTO                             !
RecordsProcessed     LONG,AUTO                             !
RecordsPerCycle      LONG,AUTO                             !
RecordsThisCycle     LONG,AUTO                             !
PercentProgress      BYTE                                  !
RecordStatus         BYTE,AUTO                             !
EndOfReport          BYTE,AUTO                             !
ReportRunDate        LONG,AUTO                             !
ReportRunTime        LONG,AUTO                             !
ReportPageNo         SHORT,AUTO                            !
FileOpensReached     BYTE                                  !
PartialPreviewReq    BYTE                                  !
DisplayProgress      BYTE                                  !
InitialPath          CSTRING(128)                          !
Progress:Thermometer BYTE                                  !
IniFileToUse         STRING(64)                            !
code_temp            BYTE                                  !
save_jea_id          USHORT,AUTO                           !
fault_code_field_temp STRING(30),DIM(12)                   !
option_temp          BYTE                                  !
Bar_code_string_temp CSTRING(21)                           !
Bar_Code_Temp        CSTRING(21)                           !
Bar_Code2_Temp       CSTRING(21)                           !
Address_Line1_Temp   STRING(30)                            !
Address_Line2_Temp   STRING(30)                            !
Address_Line3_Temp   STRING(30)                            !
Address_Line4_Temp   STRING(30)                            !
Invoice_Name_Temp    STRING(30)                            !
Delivery_Address1_Temp STRING(30)                          !
Delivery_address2_temp STRING(30)                          !
Delivery_address3_temp STRING(30)                          !
Delivery_address4_temp STRING(30)                          !
Delivery_Telephone_Temp STRING(30)                         !Delivery Telephone
InvoiceAddress_Group GROUP,PRE()                           !===============================================
Invoice_Company_Temp STRING(30)                            !
Invoice_address1_temp STRING(30)                           !
invoice_address2_temp STRING(30)                           !
invoice_address3_temp STRING(30)                           !
invoice_address4_temp STRING(30)                           !
invoice_EMail_Address STRING(255)                          !Email Address
                     END                                   !
accessories_temp     STRING(30),DIM(6)                     !
estimate_value_temp  STRING(70)                            !
despatched_user_temp STRING(40)                            !
vat_temp             REAL                                  !
total_temp           REAL                                  !
part_number_temp     STRING(30)                            !
line_cost_temp       REAL                                  !
job_number_temp      STRING(20)                            !
esn_temp             STRING(30)                            !ESN
charge_type_temp     STRING(22)                            !
repair_type_temp     STRING(22)                            !
labour_temp          REAL                                  !
parts_temp           REAL                                  !
courier_cost_temp    REAL                                  !
Quantity_temp        REAL                                  !
Description_temp     STRING(30)                            !
Cost_Temp            REAL                                  !
engineer_temp        STRING(30)                            !
part_type_temp       STRING(4)                             !
customer_name_temp   STRING(40)                            !
delivery_name_temp   STRING(40)                            !
exchange_unit_number_temp STRING(20)                       !
exchange_model_number STRING(30)                           !
exchange_manufacturer_temp STRING(30)                      !
exchange_unit_type_temp STRING(30)                         !
exchange_esn_temp    STRING(16)                            !
exchange_msn_temp    STRING(20)                            !
exchange_unit_title_temp STRING(15)                        !
invoice_company_name_temp STRING(30)                       !
invoice_telephone_number_temp STRING(15)                   !
invoice_fax_number_temp STRING(15)                         !
tmp:DefaultTelephone STRING(20)                            !
tmp:DefaultFax       STRING(20)                            !
tmp:bouncers         STRING(255)                           !
tmp:InvoiceText      STRING(255)                           !Invoice Text
tmp:LoanModel        STRING(30)                            !Loan Model Details
tmp:LoanIMEI         STRING(30)                            !Loan IMEI number
tmp:LoanAccessories  STRING(255)                           !Loan Accessories
tmp:LoanDepositPaid  REAL                                  !Loan Deposit Paid
DefaultAddress       GROUP,PRE(address)                    !
SiteName             STRING(40)                            !
Name                 STRING(40)                            !Name
Name2                STRING(40)                            !
Location             STRING(40)                            !
AddressLine1         STRING(40)                            !Address Line 1
AddressLine2         STRING(40)                            !Address Line 2
AddressLine3         STRING(40)                            !Address Line 3
AddressLine4         STRING(40)                            !Postcode
Telephone            STRING(30)                            !Telephone
RegistrationNo       STRING(40)                            !
VATNumber            STRING(40)                            !
Fax                  STRING(30)                            !Fax
EmailAddress         STRING(255)                           !Email Address
                     END                                   !
delivery_Company_Name_temp STRING(30)                      !Delivery Name
endUserTelNo         STRING(15)                            !
clientName           STRING(65)                            !
tmp:ReplacementValue REAL                                  !Replacement Value
tmp:FaultCodeDescription STRING(255),DIM(6)                !Fault Code Description
tmp:BookingOption    STRING(30)                            !Booking Option
tmp:ExportReport     BYTE                                  !
barcodeJobNumber     STRING(20)                            !
barcodeIMEINumber    STRING(20)                            !
locTermsText         STRING(255)                           !
Process:View         VIEW(JOBS)
                       PROJECT(job:Account_Number)
                       PROJECT(job:Charge_Type)
                       PROJECT(job:Courier)
                       PROJECT(job:DOP)
                       PROJECT(job:Date_Completed)
                       PROJECT(job:Date_QA_Passed)
                       PROJECT(job:ESN)
                       PROJECT(job:Location)
                       PROJECT(job:MSN)
                       PROJECT(job:Manufacturer)
                       PROJECT(job:Model_Number)
                       PROJECT(job:Order_Number)
                       PROJECT(job:Ref_Number)
                       PROJECT(job:Repair_Type)
                       PROJECT(job:Repair_Type_Warranty)
                       PROJECT(job:Time_Completed)
                       PROJECT(job:Time_QA_Passed)
                       PROJECT(job:Unit_Type)
                       PROJECT(job:Warranty_Charge_Type)
                       PROJECT(job:date_booked)
                       PROJECT(job:time_booked)
                     END
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),CENTER,TIMER(1),GRAY,DOUBLE
                       PROGRESS,USE(Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

Report               REPORT('ORDERS Report'),AT(260,6458,7750,813),PAPER(PAPER:A4),PRE(RPT),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),THOUS
                       HEADER,AT(260,615,7750,8250),USE(?unnamed),FONT('Tahoma',7,,)
                         STRING(@s50),AT(6094,469),USE(who_booked),TRN,FONT(,8,,FONT:bold)
                         STRING('Job Number: '),AT(5104,52),USE(?String25),TRN,FONT(,8,,)
                         STRING(@s16),AT(6010,21),USE(tmp:Ref_number),TRN,LEFT,FONT(,11,,FONT:bold)
                         STRING('Date Booked: '),AT(5104,313),USE(?String58),TRN,FONT(,8,,)
                         STRING(@d6b),AT(6094,313),USE(job:date_booked),TRN,RIGHT,FONT(,8,,FONT:bold)
                         STRING(@t1b),AT(6979,313),USE(job:time_booked),TRN,RIGHT,FONT(,8,,FONT:bold)
                         STRING('Booked By:'),AT(5104,469),USE(?String158),TRN,FONT(,8,,)
                         STRING(@d6b),AT(6094,781),USE(job:DOP),TRN,RIGHT,FONT(,8,,FONT:bold)
                         STRING('Engineer:'),AT(5104,625),USE(?String96),TRN,FONT(,8,,)
                         STRING(@s30),AT(6094,625),USE(engineer_temp),TRN,FONT(,8,,FONT:bold)
                         STRING('Date Of Purchase:'),AT(5104,781),USE(?String96:2),TRN,FONT(,8,,)
                         STRING('Model'),AT(177,3500),USE(?String40),TRN,FONT(,8,,FONT:bold)
                         STRING('Location'),AT(177,2833),USE(?String40:3),TRN,FONT(,8,,FONT:bold)
                         STRING('Warranty Type'),AT(4708,2833),USE(?warranty_Type),TRN,FONT(,8,,FONT:bold)
                         STRING('Warranty Repair Type'),AT(6219,2833),USE(?warranty_repair_type),TRN,FONT(,8,,FONT:bold)
                         STRING('Chargeable Type'),AT(1688,2833),USE(?Chargeable_Type),TRN,FONT(,8,,FONT:bold)
                         STRING('Chargeable Repair Type'),AT(3198,2833),USE(?Repair_Type),TRN,FONT(,8,,FONT:bold)
                         STRING('Make'),AT(1688,3500),USE(?String41),TRN,FONT(,8,,FONT:bold)
                         STRING('Unit Type'),AT(3198,3500),USE(?String42),TRN,FONT(,8,,FONT:bold)
                         STRING('I.M.E.I. Number'),AT(4708,3510),USE(?String43),TRN,FONT(,8,,FONT:bold)
                         STRING('M.S.N.'),AT(6219,3510),USE(?String44),TRN,FONT(,8,,FONT:bold)
                         STRING(@s20),AT(1688,3073),USE(job:Charge_Type),TRN,LEFT,FONT(,8,,)
                         STRING(@s20),AT(3198,3073),USE(job:Repair_Type),TRN,FONT(,8,,)
                         STRING(@s20),AT(177,3073),USE(job:Location),TRN,FONT(,8,,)
                         STRING(@s30),AT(177,3698,1000,156),USE(job:Model_Number),TRN,LEFT,FONT(,8,,)
                         STRING(@s30),AT(1688,3698,1323,156),USE(job:Manufacturer),TRN,LEFT,FONT(,8,,)
                         STRING(@s30),AT(3198,3698,1396,156),USE(job:Unit_Type),TRN,LEFT,FONT(,8,,)
                         STRING(@s15),AT(4708,3698,1396,156),USE(job:ESN),TRN,LEFT,FONT(,8,,)
                         STRING(@s20),AT(6219,3698),USE(job:MSN),TRN,FONT(,8,,)
                         STRING(@s20),AT(6219,4010),USE(exchange_msn_temp),TRN,FONT(,8,,)
                         STRING(@s16),AT(4708,4010,1396,156),USE(exchange_esn_temp),TRN,LEFT,FONT(,8,,)
                         STRING(@s30),AT(3198,4010,1396,156),USE(exchange_unit_type_temp),TRN,LEFT,FONT(,8,,)
                         STRING(@s30),AT(1688,4010,1323,156),USE(exchange_manufacturer_temp),TRN,LEFT,FONT(,8,,)
                         STRING(@s30),AT(177,4010,1000,156),USE(exchange_model_number),TRN,LEFT,FONT(,8,,)
                         STRING(@s15),AT(177,3854),USE(exchange_unit_title_temp),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@s20),AT(1354,3854),USE(exchange_unit_number_temp),TRN,LEFT
                         STRING('REPORTED FAULT'),AT(3562,4229,1062,156),USE(?String64),TRN,FONT(,8,,FONT:bold)
                         TEXT,AT(4625,4219,2406,844),USE(jbn:Fault_Description),TRN,FONT(,7,,)
                         STRING('ESTIMATE'),AT(156,4583,1354,156),USE(?Estimate),TRN,HIDE,FONT(,8,,FONT:bold)
                         STRING(@s70),AT(1625,5031,5625,208),USE(estimate_value_temp),TRN,FONT(,8,,)
                         STRING('ENGINEERS REPORT'),AT(156,5031,1354,156),USE(?String88),TRN,FONT(,8,,FONT:bold)
                         STRING('REPAIR NOTES'),AT(156,4229),USE(?String88:3),TRN,FONT(,8,,FONT:bold)
                         TEXT,AT(1615,4646,1927,417),USE(tmp:InvoiceText),TRN,FONT(,7,,)
                         TEXT,AT(1615,5208,5417,313),USE(tmp:accessories),TRN,FONT(,7,,)
                         STRING('ACCESSORIES'),AT(156,5208,1354,156),USE(?String100),TRN,FONT(,8,,FONT:bold)
                         STRING('Type'),AT(521,5677),USE(?String98),TRN,FONT(,7,,FONT:bold)
                         STRING('PARTS REQUIRED'),AT(156,5521,1135,156),USE(?String79),TRN,FONT(,8,,FONT:bold)
                         STRING('Qty'),AT(156,5677),USE(?String80),TRN,FONT(,7,,FONT:bold)
                         STRING('Part Number'),AT(885,5677),USE(?String81),TRN,FONT(,7,,FONT:bold)
                         STRING('Description'),AT(2292,5677),USE(?String82),TRN,FONT(,7,,FONT:bold)
                         STRING('Unit Cost'),AT(4063,5677),USE(?String83),TRN,FONT(,7,,FONT:bold)
                         STRING('Line Cost'),AT(4948,5677),USE(?String89),TRN,FONT(,7,,FONT:bold)
                         LINE,AT(156,5833,7083,0),USE(?Line1),COLOR(COLOR:Black)
                         LINE,AT(156,6667,7083,0),USE(?Line3),COLOR(COLOR:Black)
                         STRING('BOUNCER'),AT(156,6719,1354,156),USE(?BouncerTitle),TRN,HIDE,FONT('Tahoma',8,,FONT:bold)
                         TEXT,AT(1667,6719,5417,208),USE(tmp:bouncers),TRN,HIDE,FONT(,8,,)
                         STRING('LOAN UNIT ISSUED'),AT(156,6927,1354,156),USE(?LoanUnitIssued),TRN,HIDE,FONT('Tahoma',8,COLOR:Black,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(1667,6927),USE(tmp:LoanModel),TRN,HIDE,LEFT,FONT('Tahoma',8,COLOR:Black,FONT:regular,CHARSET:ANSI)
                         STRING('IMEI'),AT(3646,6927),USE(?IMEITitle),TRN,HIDE,FONT('Tahoma',9,COLOR:Black,FONT:bold,CHARSET:ANSI)
                         STRING(@s16),AT(4010,6927),USE(tmp:LoanIMEI),TRN,HIDE,LEFT,FONT('Tahoma',8,COLOR:Black,FONT:regular,CHARSET:ANSI)
                         STRING('CUSTOMER SIGNATURE'),AT(167,8021,1583,156),USE(?String820),TRN,FONT('Tahoma',8,,FONT:bold)
                         STRING('REPLACEMENT VALUE'),AT(5104,6927),USE(?LoanValueText),TRN,HIDE,FONT('Tahoma',9,COLOR:Black,FONT:bold,CHARSET:ANSI)
                         STRING(@n10.2),AT(6406,6927),USE(tmp:ReplacementValue),TRN,HIDE,RIGHT,FONT(,8,,)
                         LINE,AT(4719,8156,1406,0),USE(?Line5),COLOR(COLOR:Black)
                         STRING('Date: {22}/ {23}/'),AT(4500,8031,1563,156),USE(?String830),TRN,FONT(,,,FONT:bold)
                         LINE,AT(1594,8156,2917,0),USE(?Line5:2),COLOR(COLOR:Black)
                         STRING('LOAN ACCESSORIES'),AT(156,7083,1354,156),USE(?LoanAccessoriesTitle),TRN,HIDE,FONT('Tahoma',8,COLOR:Black,FONT:bold,CHARSET:ANSI)
                         STRING(@s255),AT(1667,7083,5417,208),USE(tmp:LoanAccessories),TRN,HIDE,LEFT,FONT('Tahoma',8,COLOR:Black,FONT:regular,CHARSET:ANSI)
                         STRING('EXTERNAL DAMAGE CHECK LIST'),AT(156,7219),USE(?ExternalDamageCheckList),TRN,FONT('Tahoma',8,COLOR:Black,FONT:bold,CHARSET:ANSI)
                         STRING('Notes:'),AT(5781,7240),USE(?Notes),TRN,FONT(,7,,)
                         TEXT,AT(6094,7240,1458,469),USE(jobe2:XNotes),TRN,FONT(,6,,)
                         CHECK('None'),AT(5156,7240,625,156),USE(jobe2:XNone),TRN
                         CHECK('Keypad'),AT(156,7365,729,156),USE(jobe2:XKeypad),TRN
                         CHECK('Charger'),AT(2083,7365,781,156),USE(jobe2:XCharger),TRN
                         CHECK('Antenna'),AT(2083,7240,833,156),USE(jobe2:XAntenna),TRN
                         CHECK('Lens'),AT(2927,7240,573,156),USE(jobe2:XLens),TRN
                         CHECK('F/Cover'),AT(3552,7240,781,156),USE(jobe2:XFCover),TRN
                         CHECK('B/Cover'),AT(4437,7240,729,156),USE(jobe2:XBCover),TRN
                         CHECK('Battery'),AT(990,7365,729,156),USE(jobe2:XBattery),TRN
                         CHECK('LCD'),AT(2927,7365,625,156),USE(jobe2:XLCD),TRN
                         CHECK('System Connector'),AT(4437,7365,1250,156),USE(jobe2:XSystemConnector),TRN
                         CHECK('Sim Reader'),AT(3552,7365,885,156),USE(jobe2:XSimReader),TRN
                         STRING(@s30),AT(156,1927),USE(invoice_address4_temp),TRN,FONT(,8,,)
                         STRING('Tel: '),AT(4167,2083),USE(?String32:2),TRN,FONT(,8,,)
                         STRING(@s30),AT(4427,2083),USE(Delivery_Telephone_Temp),TRN,FONT(,8,,)
                         STRING(@s50),AT(521,2083,3490,156),USE(invoice_EMail_Address),TRN,LEFT,FONT(,8,,)
                         STRING('Email:'),AT(156,2083),USE(?unnamed:4),TRN,FONT(,8,,)
                         STRING('Tel:'),AT(156,2240),USE(?String34:2),TRN,FONT(,8,,)
                         STRING(@s15),AT(521,2240),USE(invoice_telephone_number_temp),TRN,LEFT,FONT(,8,,)
                         STRING('Fax: '),AT(156,2396),USE(?String35:2),TRN,FONT(,8,,)
                         STRING(@s15),AT(521,2396),USE(invoice_fax_number_temp),TRN,LEFT,FONT(,8,,)
                         STRING(@s15),AT(2760,1302),USE(job:Account_Number),TRN,FONT(,8,,)
                         STRING(@s20),AT(4708,3073),USE(job:Warranty_Charge_Type),TRN,FONT(,8,,)
                         STRING(@s20),AT(6219,3073),USE(job:Repair_Type_Warranty),TRN,FONT(,8,,)
                         STRING('Acc No:'),AT(2188,1302),USE(?String94),TRN,FONT(,8,COLOR:Black,)
                         STRING(@s30),AT(4167,1302),USE(delivery_Company_Name_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(4167,1458,1917,156),USE(Delivery_Address1_Temp),TRN,FONT(,8,,)
                         STRING(@s20),AT(2760,1458),USE(job:Order_Number),TRN,FONT(,8,,)
                         STRING('Order No:'),AT(2188,1458),USE(?String140),TRN,FONT(,8,,)
                         STRING(@s30),AT(4167,1615),USE(Delivery_address2_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(4167,1771),USE(Delivery_address3_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(4167,1927),USE(Delivery_address4_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,1302),USE(invoice_company_name_temp),TRN,FONT(,8,COLOR:Black,)
                         STRING(@s30),AT(156,1458),USE(Invoice_address1_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,1771),USE(invoice_address3_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,1615),USE(invoice_address2_temp),TRN,FONT(,8,,)
                         STRING(@s65),AT(4167,2396,2865,156),USE(clientName),TRN,FONT('Tahoma',8,COLOR:Black,FONT:regular,CHARSET:ANSI)
                         TEXT,AT(156,7781,7375,250),USE(stt:Text),TRN
                         TEXT,AT(156,7531,5885,260),USE(locTermsText)
                       END
EndOfReportBreak       BREAK(EndOfReport),USE(?unnamed:3)
DETAIL                   DETAIL,AT(,,,115),USE(?DetailBand)
                           STRING(@n8b),AT(-104,0),USE(Quantity_temp),TRN,RIGHT,FONT(,7,,)
                           STRING(@s25),AT(885,0),USE(part_number_temp),TRN,FONT(,7,,)
                           STRING(@s25),AT(2292,0),USE(Description_temp),TRN,FONT(,7,,)
                           STRING(@n14.2b),AT(3802,0),USE(Cost_Temp),TRN,RIGHT,FONT(,7,,)
                           STRING(@n14.2b),AT(4740,0),USE(line_cost_temp),TRN,RIGHT,FONT(,7,,)
                           STRING(@s4),AT(521,0),USE(part_type_temp),TRN,FONT(,7,,)
                         END
                         FOOTER,AT(396,8896,,2406),USE(?unnamed:2),TOGETHER,ABSOLUTE
                           STRING('Loan Deposit Paid'),AT(5313,21),USE(?LoanDepositPaidTitle),TRN,HIDE,FONT(,8,,FONT:regular,CHARSET:ANSI)
                           STRING(@n14.2b),AT(6396,21),USE(tmp:LoanDepositPaid),TRN,HIDE,RIGHT,FONT(,8,,,CHARSET:ANSI)
                           STRING('Completed:'),AT(52,156),USE(?String66),TRN,FONT(,8,,)
                           STRING(@D6b),AT(719,167),USE(job:Date_Completed),TRN,RIGHT,FONT(,8,,FONT:bold)
                           STRING(@t1b),AT(1677,167),USE(job:Time_Completed),TRN,RIGHT,FONT(,8,,FONT:bold)
                           STRING('QA Passed:'),AT(52,313),USE(?qa_passed),TRN,HIDE,FONT(,8,,)
                           STRING('Labour:'),AT(5313,167),USE(?labour_string),TRN,FONT(,8,,)
                           STRING(@n14.2b),AT(6396,167),USE(labour_temp),TRN,RIGHT,FONT(,8,,)
                           STRING(@s20),AT(2500,260,2552,208),USE(barcodeJobNumber),CENTER,FONT('C39 High 12pt LJ3',12,,),COLOR(COLOR:White)
                           STRING('Outgoing Courier:'),AT(52,573),USE(?String66:2),TRN,FONT(,8,,)
                           STRING(@s20),AT(2917,469,1760,198),USE(job_number_temp),TRN,CENTER,FONT('Tahoma',8,,FONT:bold)
                           STRING('Carriage:'),AT(5313,458),USE(?carriage_string),TRN,FONT(,8,,)
                           STRING(@n14.2b),AT(6396,313),USE(parts_temp),TRN,RIGHT,FONT(,8,,)
                           STRING(@s20),AT(2500,729,2552,208),USE(barcodeIMEINumber),CENTER,FONT('C39 High 12pt LJ3',12,,),COLOR(COLOR:White)
                           STRING('V.A.T.'),AT(5313,604),USE(?vat_String),TRN,FONT(,8,,)
                           STRING(@n14.2b),AT(6396,458),USE(courier_cost_temp),TRN,RIGHT,FONT(,8,,)
                           STRING(@d6b),AT(719,313),USE(job:Date_QA_Passed),TRN,RIGHT,FONT(,8,,FONT:bold)
                           STRING('Total:'),AT(5313,781),USE(?total_string),TRN,FONT(,8,,FONT:bold)
                           LINE,AT(6281,781,1000,0),USE(?line),COLOR(COLOR:Black)
                           STRING(@n14.2b),AT(6302,781),USE(total_temp),TRN,RIGHT,FONT(,8,,FONT:bold)
                           STRING('<128>'),AT(6250,781),USE(?Euro),TRN,HIDE,FONT(,8,,FONT:bold)
                           STRING('FAULT CODES'),AT(52,1146),USE(?String107),TRN,FONT(,8,,FONT:bold)
                           STRING('Booking Option:'),AT(5313,938),USE(?BookingOption),TRN,FONT(,8,,FONT:bold)
                           STRING(@s30),AT(6354,938,1406,156),USE(tmp:BookingOption),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           STRING(@s30),AT(52,1302),USE(fault_code_field_temp[1]),TRN,FONT(,8,,)
                           STRING(@s255),AT(2135,1302,4844,208),USE(tmp:FaultCodeDescription[1]),TRN,FONT(,8,,)
                           STRING(@s30),AT(52,1458),USE(fault_code_field_temp[2]),TRN,FONT(,8,,)
                           STRING(@s255),AT(2135,1458,4844,208),USE(tmp:FaultCodeDescription[2]),FONT(,8,,)
                           STRING(@s30),AT(52,1615),USE(fault_code_field_temp[3]),TRN,FONT(,8,,)
                           STRING(@s255),AT(2135,1615,4844,208),USE(tmp:FaultCodeDescription[3]),FONT(,8,,)
                           STRING(@s30),AT(52,1771),USE(fault_code_field_temp[4]),TRN,FONT(,8,,)
                           STRING(@s255),AT(2135,1771,4844,208),USE(tmp:FaultCodeDescription[4]),FONT(,8,,)
                           STRING(@s30),AT(52,1927),USE(fault_code_field_temp[5]),TRN,FONT(,8,,)
                           STRING(@s255),AT(2135,1927,4844,208),USE(tmp:FaultCodeDescription[5]),FONT(,8,,)
                           STRING(@s30),AT(52,2083),USE(fault_code_field_temp[6]),TRN,FONT(,8,,)
                           STRING(@s255),AT(2135,2083,4844,208),USE(tmp:FaultCodeDescription[6]),FONT(,8,,)
                           STRING(@n14.2b),AT(6396,604),USE(vat_temp),TRN,RIGHT,FONT(,8,,)
                           STRING(@s13),AT(990,729,1406,156),USE(jobe2:IDNumber),TRN,FONT(,8,,FONT:bold)
                           STRING('Customer ID No:'),AT(52,729),USE(?String66:3),TRN,FONT(,8,,)
                           STRING(@s15),AT(990,573,1563,156),USE(job:Courier),TRN,FONT(,8,,FONT:bold)
                           STRING(@t1b),AT(1677,313),USE(job:Time_QA_Passed),TRN,RIGHT,FONT(,8,,FONT:bold)
                           STRING(@s30),AT(2760,938,2031,260),USE(esn_temp),TRN,CENTER,FONT(,8,,FONT:bold)
                           STRING('Parts:'),AT(5313,313),USE(?parts_string),TRN,FONT(,8,,)
                         END
                       END
                       FOOTER,AT(396,10271,7521,1156),USE(?Fault_Code9:2)
                       END
                       FORM,AT(250,250,7750,11188),USE(?Text:CurrencyItemCost:2),FONT('Tahoma',8,,FONT:regular)
                         STRING('JOB CARD'),AT(5521,0,1917,240),USE(?Chargeable_Repair_Type:2),TRN,RIGHT,FONT(,16,,FONT:bold)
                         STRING(@s40),AT(104,0,4167,260),USE(address:SiteName),LEFT,FONT(,14,,FONT:bold)
                         STRING(@s40),AT(104,313,3073,208),USE(address:Name2),TRN,FONT(,8,COLOR:Black,FONT:bold)
                         STRING(@s40),AT(104,208,3073,208),USE(address:Name),TRN,FONT(,8,COLOR:Black,FONT:bold)
                         STRING(@s40),AT(104,417,2760,208),USE(address:Location),TRN,FONT(,8,COLOR:Black,FONT:bold)
                         STRING('REG NO:'),AT(104,573),USE(?stringREGNO),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s40),AT(625,573,1667,208),USE(address:RegistrationNo),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING('VAT NO: '),AT(104,677),USE(?stringVATNO),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s20),AT(625,677,1771,156),USE(address:VATNumber),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s40),AT(104,833,2240,156),USE(address:AddressLine1),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s40),AT(104,938,2240,156),USE(address:AddressLine2),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s40),AT(104,1042,2240,156),USE(address:AddressLine3),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s40),AT(104,1146),USE(address:AddressLine4),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING('TEL:'),AT(104,1250),USE(?stringTEL),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s30),AT(469,1250,1458,208),USE(address:Telephone),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING('FAX:'),AT(2083,1250,313,208),USE(?stringFAX),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s30),AT(2396,1250,1510,188),USE(address:Fax),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING('EMAIL:'),AT(104,1354,417,208),USE(?stringEMAIL),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s255),AT(469,1354,3333,208),USE(address:EmailAddress),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING('JOB DETAILS'),AT(5000,208),USE(?String57),TRN,FONT(,8,,FONT:bold)
                         STRING('INVOICE ADDRESS'),AT(156,1510),USE(?String24),TRN,FONT(,8,,FONT:bold)
                         STRING('DELIVERY ADDRESS'),AT(4115,1510,1677,156),USE(?DeliveryAddress),TRN,FONT(,8,,FONT:bold)
                         STRING('GENERAL DETAILS'),AT(156,2990),USE(?String91),TRN,FONT(,8,,FONT:bold)
                         STRING('COMPLETION DETAILS'),AT(156,8563),USE(?String73),TRN,FONT(,8,,FONT:bold)
                         STRING('CHARGE DETAILS'),AT(5365,8563),USE(?String74),TRN,FONT(,8,,FONT:bold)
                         STRING('REPAIR DETAILS'),AT(156,3667),USE(?String50),TRN,FONT(,8,,FONT:bold)
                         BOX,AT(5000,365,2646,1042),USE(?Box:TopDetails),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H),LINEWIDTH(1)
                         BOX,AT(104,1667,3604,1302),USE(?Box:Address1),ROUND,COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1)
                         BOX,AT(4063,1667,3604,1302),USE(?Box:Address2),ROUND,COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1)
                         BOX,AT(104,3125,1510,260),USE(?Box:Title1),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H),LINEWIDTH(1)
                         BOX,AT(1615,3125,1510,260),USE(?Box:Title2),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H),LINEWIDTH(1)
                         BOX,AT(3125,3125,1510,260),USE(?Box:Title3),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H),LINEWIDTH(1)
                         BOX,AT(4635,3125,1510,260),USE(?Box:Title4),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H),LINEWIDTH(1)
                         BOX,AT(6146,3125,1510,260),USE(?Box:Title5),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H),LINEWIDTH(1)
                         BOX,AT(104,3385,1510,260),USE(?Box:Title1a),ROUND,COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1)
                         BOX,AT(1615,3385,1510,260),USE(?Box:Title2a),ROUND,COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1)
                         BOX,AT(3125,3385,1510,260),USE(?Box:Title3a),ROUND,COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1)
                         BOX,AT(4635,3385,1510,260),USE(?Box:Title4a),ROUND,COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1)
                         BOX,AT(6146,3385,1510,260),USE(?Box:Title5a),ROUND,COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1)
                         BOX,AT(104,3802,7552,260),USE(?Box:Heading),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H),LINEWIDTH(1)
                         BOX,AT(104,4063,7552,4479),USE(?Box:Detail),ROUND,COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1)
                         BOX,AT(104,8698,2344,1042),USE(?Box:Total1),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H),LINEWIDTH(1)
                         BOX,AT(5313,8698,2344,1042),USE(?Box:Total2),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H),LINEWIDTH(1)
                       END
                     END
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeNoRecords          PROCEDURE(),DERIVED                 ! Method added to host embed code
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

ProgressMgr          StepLongClass                         ! Progress Manager
Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED                 ! Method added to host embed code
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('JobCard')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  tmp:JobNumber = p_web.GetSessionValue('tmp:JobNumber')
  
  Relate:EXCHANGE.Open                                     ! File EXCHANGE used by this procedure, so make sure it's RelationManager is open
  Relate:INVOICE.SetOpenRelated()
  Relate:INVOICE.Open                                      ! File INVOICE used by this procedure, so make sure it's RelationManager is open
  Relate:JOBEXACC.Open                                     ! File JOBEXACC used by this procedure, so make sure it's RelationManager is open
  Relate:JOBPAYMT.Open                                     ! File JOBPAYMT used by this procedure, so make sure it's RelationManager is open
  Relate:JOBS2_ALIAS.Open                                  ! File JOBS2_ALIAS used by this procedure, so make sure it's RelationManager is open
  Relate:STANTEXT.Open                                     ! File STANTEXT used by this procedure, so make sure it's RelationManager is open
  Relate:WEBJOB.Open                                       ! File WEBJOB used by this procedure, so make sure it's RelationManager is open
  Access:JOBNOTES.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBSE.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBSE2.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBACC.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:MANUFACT.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TRADEACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:SUBTRACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:MANFAULO.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBOUTFL.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:LOAN.UseFile                                      ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:LOANACC.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  ! #12084 New standard texts (Bryan: 17/05/2011)
  Access:STANTEXT.Clearkey(stt:Description_Key)
  stt:Description = 'JOB CARD'
  IF (Access:STANTEXT.TryFetch(stt:Description_Key))
  END
  locTermsText = p_web.GSV('Default:TermsText')
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('JobCard',ProgressWindow)                   ! Restore window settings from non-volatile store
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ProgressWindow{Prop:Timer} = 10                          ! Assign timer interval
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:JOBS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, job:Ref_Number)
  ThisReport.AddSortOrder(job:Ref_Number_Key)
  ThisReport.AddRange(job:Ref_Number,tmp:JobNumber)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{Prop:Text}=''
  Relate:JOBS.SetQuickScan(1,Propagate:OneMany)
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom=True
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
      SELF.SetReportTarget(PDFReporter.IReportGenerator)
    self.SkipPreview = True
    ProgressWindow{prop:hide} = 1
    loc:PDFName = clip(p_web.site.WebFolderPath) & '\reports\$$$' & format(random(1,99999),@n05) &'.pdf'
  End
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EXCHANGE.Close
    Relate:INVOICE.Close
    Relate:JOBEXACC.Close
    Relate:JOBPAYMT.Close
    Relate:JOBS2_ALIAS.Close
    Relate:STANTEXT.Close
    Relate:WEBJOB.Close
  END
  IF SELF.Opened
    INIMgr.Update('JobCard',ProgressWindow)                ! Save window data to non-volatile store
  END
  ProgressMgr.Kill()
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      Loc:html = '<script type="text/javascript">alert('''&clip('No Records')&''');top.close();</script>'
      p_web.ParseHTML(loc:html)
    Else
      p_web.ForceNoCache = 1
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(loc:PDFName)
    End
  End
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
    job_Number_Temp = 'Job No: ' & clip(job:Ref_Number)
    esn_Temp = 'I.M.E.I.: ' & clip(job:ESN)
    barcodeJobNumber = '*' & clip(job:Ref_Number) & '*'
    barcodeIMEINumber = '*' & clip(job:ESN) & '*'

!  !Barcode Bit and setup refno
!  code_temp            = 1
!  option_temp          = 0
!
!  bar_code_string_temp = Clip(job:ref_number)
!  !job_number_temp      = 'Job No: ' & Clip(job:ref_number)
!  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code_Temp)
!
!  bar_code_string_temp = Clip(job:esn)
!  !esn_temp             = 'I.M.E.I.: ' & Clip(job:esn)
!  !SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code2_Temp)
!
!  job_number_temp = '*' & job:Ref_Number & '*'
!
!  Settarget(Report)
!  Draw_JobNo.Init256()
!  Draw_JobNo.RenderWholeStrings = 1
!  Draw_JobNo.Resize(Draw_JobNo.Width * 5, Draw_JobNo.Height * 5)
!  Draw_JobNo.Blank(Color:White)
!  Draw_JobNo.FontName = 'C39 High 12pt LJ3'
!  Draw_JobNo.FontStyle = font:Regular
!  Draw_JobNo.FontSize = 24
!  Draw_JobNo.Show(0,0,Clip(job_Number_temp))
!  Draw_JobNo.Display()
!  Draw_JobNo.Kill256()
!
!  drawer4.Init256()
!  drawer4.RenderWholeStrings = 1
!  drawer4.SetFontMode(Draw:NONANTIALIASED_QUALITY)
!  drawer4.Resize(drawer4.Width * 5, drawer4.Height * 5)
!  drawer4.Blank(Color:White)
!  drawer4.FontName = 'C39 High 12pt LJ3'
!  drawer4.FontStyle = font:Regular
!  drawer4.FontSize = 24
!  drawer4.Show(0,0,Clip(job_Number_temp))
!  drawer4.Display()
!  drawer4.Kill256()
!
!  drawer6.Init256()
!  drawer6.RenderWholeStrings = 1
!  drawer6.Resize(drawer6.Width * 5, drawer6.Height * 5)
!  drawer6.Blank(Color:White)
!  drawer6.FontName = 'C39 High 12pt LJ3'
!  drawer6.FontStyle = font:Regular
!  drawer6.FontSize = 24
!  drawer6.Show(0,0,Bar_Code_Temp)
!  drawer6.Display()
!  drawer6.Kill256()
!!
!  drawer7.Init256()
!  drawer7.RenderWholeStrings = 1
!  drawer7.SetFontMode(Draw:NONANTIALIASED_QUALITY)
!  drawer7.Resize()
!  drawer7.Blank(Color:White)
!  drawer7.FontName = 'C39 High 12pt LJ3'
!  drawer7.FontStyle = font:Regular
!  drawer7.FontSize = 24
!  drawer7.Show(0,0,Clip(job_Number_temp))
!  drawer7.Display()
!  drawer7.Kill256()
!!
!  drawer8.Init256()
!  drawer8.RenderWholeStrings = 1
!  !drawer8.SetFontMode(Draw:NONANTIALIASED_QUALITY)
!  drawer8.Resize()
!  drawer8.Blank(Color:White)
!  drawer8.FontName = 'C39 High 12pt LJ3'
!  drawer8.FontStyle = font:Regular
!  drawer8.FontSize = 24
!  drawer8.Show(0,0,Clip(job_Number_temp))
!  drawer8.Display()
!  drawer8.Kill256()
!!
!!  drawer9.Init256()
!!  drawer9.RenderWholeStrings = 1
!!  drawer9.Resize(drawer9.Width * 5, drawer9.Height * 5)
!!  drawer9.Blank(Color:White)
!!  drawer9.FontName = 'C39 High 12pt LJ3'
!!  drawer9.FontStyle = font:Regular
!!  drawer9.FontSize = 22
!!  drawer9.Show(2,2,Clip(job_Number_temp))
!!  drawer9.Display()
!!  drawer9.Kill256()
!!
!!  !Draw_IMEI.Init256()
!!  !Draw_IMEI.RenderWholeStrings = 1
!!  !Draw_IMEI.Resize(Draw_IMEI.Width * 5, Draw_IMEI.Height * 5)
!!  !Draw_IMEI.Blank(Color:White)
!!  !Draw_IMEI.FontName = 'C39 High 12pt LJ3'
!!  !Draw_IMEI.FontStyle = font:Regular
!!  !Draw_IMEI.FontSize = 48
!!  !Draw_IMEI.Show(0,0,Bar_Code2_Temp)
!!  !Draw_IMEI.Display()
!!  !Draw_IMEI.Kill256()
!  SetTarget()
!
!
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
    If Not p_Web &= NULL
      loc:NoRecords = 1
      Return
    End
  PARENT.TakeNoRecords


ThisWindow.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  Access:WEBJOB.ClearKey(wob:RefNumberKey)
  wob:RefNumber = job:Ref_Number
  If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
      !Found
      Access:TRADeACC.ClearKey(tra:Account_Number_Key)
      tra:Account_Number = wob:HeadAccountNumber
      If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
          !Found
          tmp:Ref_Number = job:Ref_Number & '-' & tra:BranchIdentification & wob:JobNumber
      Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
          !Error
      End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
  
  Else ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
      !Error
  End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
  
  Access:JOBSE.ClearKey(jobe:RefNumberKey)
  jobe:RefNumber = job:Ref_Number
  If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
      !Found
  Else ! If AccESS:JOBE.TryFetch(jobe:RefNumberKey) = Level:Benign
      !Error
  End ! If Access:JOBE.TryFetch(jobe:RefNumberKey) = Level:Benign
  
  Access:JOBSE2.ClearKey(jobe2:RefNumberKey)
  jobe2:RefNumber = job:Ref_Number
  If Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign
      !Found
  Else ! If Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign
      !Error
  End ! If Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign
  
  Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
  jbn:RefNumber = job:Ref_Number
  If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
      !Found
  Else ! If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
      !Error
  End ! If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
  
  
  
  Access:DEFAULTS.Clearkey(def:RecordNumberKEy)
  def:Record_Number = 1
  Set(def:RecordNumberKEy,def:RecordNumberKEy)
  Loop ! Begin Loop
      If Access:DEFAULTS.Next()
          Break
      End ! If Access:DEFAULTS.Next()
      Break
  End ! Loop
  
  
  Access:USERS.ClearKey(use:User_Code_Key)
  use:User_Code = job:Engineer
  If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
      !Found
      Engineer_Temp = Clip(use:Forename) & ' ' & Clip(use:Surname)
  Else ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
      !Error
      Engineer_Temp = ''
  End ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
  
  Access:USERS.ClearKey(use:User_Code_Key)
  use:User_Code = job:Who_Booked
  If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
      !Found
      Who_Booked = Clip(use:Forename) & ' ' & Clip(use:Surname)
  Else ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
      !Error
      Who_Booked = ''
  End ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
  
  Despatched_User_Temp = ''
  
  Access:TRADEACC.Clearkey(tra:Account_Number_Key)
  If (jobe:WebJob = 1)
      tra:Account_Number = wob:HeadAccountNumber
  ELSE ! If (glo:WebJob = 1)
      tra:Account_Number = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
  END ! If (glo:WebJob = 1)
  If (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
      address:SiteName        = tra:Company_Name
      address:Name            = tra:coTradingName
      address:Name2           = tra:coTradingName2  ! #12079 New address fields. (Bryan: 13/04/2011)
      address:Location        = tra:coLocation
      address:RegistrationNo  = tra:coRegistrationNo
      address:AddressLine1    = tra:coAddressLine1
      address:AddressLine2    = tra:coAddressLine2
      address:AddressLine3    = tra:coAddressLine3
      address:AddressLine4    = tra:coAddressLine4
      address:Telephone       = tra:coTelephoneNumber
      address:Fax             = tra:coFaxNumber
      address:EmailAddress    = tra:coEmailAddress
      address:VatNumber       = tra:coVATNumber
  END
  Settarget(Report)
  
  ! Show Engineering Option If Set - 4285 (DBH: 10-06-2004)
  Case jobe:Engineer48HourOption
  Of 1
      tmp:BookingOption         = '48 Hour Exchange'
      ?BookingOPtion{prop:Text} = 'Engineer Option:'
  Of 2
      tmp:BookingOption         = 'ARC Repair'
      ?BookingOPtion{prop:Text} = 'Engineer Option:'
  Of 3
      tmp:BookingOption         = '7 Day TAT'
      ?BookingOPtion{prop:Text} = 'Engineer Option:'
  Of 4
      tmp:BookingOption         = 'Standard Repair'
      ?BookingOPtion{prop:Text} = 'Engineer Option:'
  Else
      Case jobe:Booking48HourOption
      Of 1
          tmp:BookingOption = '48 Hour Exchange'
      Of 2
          tmp:BookingOption = 'ARC Repair'
      Of 3
          tmp:BookingOption = '7 Day TAT'
      Else
          tmp:BookingOption = 'N/A'
      End ! Case jobe:Booking48HourOption
  End ! Case jobe:Engineer48HourOption
  
  If job:warranty_job <> 'YES'
      Hide(?job:warranty_charge_type)
      Hide(?job:repair_type_warranty)
      Hide(?warranty_type)
      Hide(?Warranty_repair_Type)
  End! If job:warranty_job <> 'YES'
  
  If job:chargeable_job <> 'YES'
      Hide(?job:charge_type)
      Hide(?job:repair_type)
      Hide(?Chargeable_type)
      Hide(?repair_Type)
  End! If job:chargeable_job <> 'YES'
  
  Access:MANUFACT.Clearkey(man:Manufacturer_Key)
  man:Manufacturer    = job:Manufacturer
  If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
      ! Found
      If man:UseQA
          UnHide(?qa_passed)
      End! If def:qa_required <> 'YES'
  
  Else ! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
  ! Error
  End ! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
  
  
  If job:warranty_job = 'YES' And job:chargeable_job <> 'YES'
      Hide(?labour_string)
      Hide(?parts_string)
      Hide(?Carriage_string)
      Hide(?vat_string)
      Hide(?total_string)
      Hide(?line)
      Hide(?Euro)
  End! If job:chargeable_job = 'YES' And job:warranty_job = 'YES'
  
  If job:estimate = 'YES' and job:Chargeable_Job = 'YES'
      Unhide(?estimate)
      estimate_value_temp = 'ESTIMATE REQUIRED IF REPAIR COST EXCEEDS ' & Clip(Format(job:estimate_if_over, @n14.2)) & ' (Plus V.A.T.)'
  End ! If
  
  
  
  
  
  !------------------------------------------------------------------
  IF CLIP(job:title) = ''
      customer_name_temp = job:initial
  ELSIF CLIP(job:initial) = ''
      customer_name_temp = job:title
  ELSE
      customer_name_temp = CLIP(job:title) & ' ' & CLIP(job:initial)
  END ! IF
  !------------------------------------------------------------------
  IF CLIP(customer_name_temp) = ''
      customer_name_temp = job:surname
  ELSIF CLIP(job:surname) = ''
  ! customer_name_temp = customer_name_temp ! tautology
  ELSE
      customer_name_temp = CLIP(customer_name_temp) & ' ' & CLIP(job:surname)
  END ! IF
  !------------------------------------------------------------------
  Settarget()
  
  endUserTelNo = ''
  
  if jobe:EndUserTelNo <> ''
      endUserTelNo = clip(jobe:EndUserTelNo)
  end ! if
  
  if customer_name_temp <> ''
      clientName = 'Client: ' & clip(customer_name_temp) & '  Tel: ' & clip(endUserTelNo)
  else
      if endUserTelNo <> ''
          clientName = 'Tel: ' & clip(endUserTelNo)
      end ! if
  end ! if
  !**************************************************
  delivery_name_temp         = customer_name_temp
  delivery_company_Name_temp = job:Company_Name_Delivery
  
  delivery_address1_temp = job:address_line1_delivery
  delivery_address2_temp = job:address_line2_delivery
  If job:address_line3_delivery   = ''
      delivery_address3_temp = job:postcode_delivery
      delivery_address4_temp = ''
  Else
      delivery_address3_temp = job:address_line3_delivery
      delivery_address4_temp = job:postcode_delivery
  End ! If
  delivery_telephone_temp     = job:Telephone_Delivery
  !**************************************************
  
  
  !**************************************************
  
  ! If an RRC job, then put the RRC as the invoice address,
  
  !If jobe:WebJob
  !    SetTarget(Report)
  !    ?DeliveryAddress{prop:Text} = 'CUSTOMER ADDRESS'
  !    SetTarget()
  !    Access:WEBJOB.Clearkey(wob:RefNumberKey)
  !    wob:RefNumber   = job:Ref_Number
  !    If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
  !        ! Found
  !        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
  !        tra:Account_Number  = wob:HeadAccountNumber
  !        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  !            ! Found
  !            invoice_address1_temp = tra:address_line1
  !            invoice_address2_temp = tra:address_line2
  !            If job:address_line3_delivery   = ''
  !                invoice_address3_temp = tra:postcode
  !                invoice_address4_temp = ''
  !            Else
  !                invoice_address3_temp = tra:address_line3
  !                invoice_address4_temp = tra:postcode
  !            End ! If
  !            invoice_company_name_temp     = tra:company_name
  !            invoice_telephone_number_temp = tra:telephone_number
  !            invoice_fax_number_temp       = tra:fax_number
  !            invoice_EMail_Address         = tra:EmailAddress
  !        Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  !        ! Error
  !        End ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  !    Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
  !    ! Error
  !    End ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
  !
  !Else ! If ~glo:WebJob And jobe:WebJob
  
  
      access:subtracc.clearkey(sub:account_number_key)
      sub:account_number = job:account_number
      access:subtracc.fetch(sub:account_number_key)
  
      access:tradeacc.clearkey(tra:account_number_key)
      tra:account_number = sub:main_account_number
      access:tradeacc.fetch(tra:account_number_key)
      if tra:invoice_sub_accounts = 'YES' And tra:Use_Sub_Accounts = 'YES'
          If sub:invoice_customer_address = 'YES'
              invoice_address1_temp = job:address_line1
              invoice_address2_temp = job:address_line2
              If job:address_line3_delivery   = ''
                  invoice_address3_temp = job:postcode
                  invoice_address4_temp = ''
              Else
                  invoice_address3_temp = job:address_line3
                  invoice_address4_temp = job:postcode
              End ! If
              invoice_company_name_temp     = job:company_name
              invoice_telephone_number_temp = job:telephone_number
              invoice_fax_number_temp       = job:fax_number
              invoice_EMail_Address         = jobe:EndUserEmailAddress  ! '' ! tra:EmailAddress
          ! MEssage('case 1')
          Else! If sub:invoice_customer_address = 'YES'
              invoice_address1_temp = sub:address_line1
              invoice_address2_temp = sub:address_line2
              If job:address_line3_delivery   = ''
                  invoice_address3_temp = sub:postcode
                  invoice_address4_temp = ''
              Else
                  invoice_address3_temp = sub:address_line3
                  invoice_address4_temp = sub:postcode
              End ! If
              invoice_company_name_temp     = sub:company_name
              invoice_telephone_number_temp = sub:telephone_number
              invoice_fax_number_temp       = sub:fax_number
              invoice_EMail_Address         = sub:EmailAddress
  
          End! If sub:invoice_customer_address = 'YES'
  
      else! if tra:use_sub_accounts = 'YES'
          If tra:invoice_customer_address = 'YES'
              invoice_address1_temp = job:address_line1
              invoice_address2_temp = job:address_line2
              If job:address_line3_delivery   = ''
                  invoice_address3_temp = job:postcode
                  invoice_address4_temp = ''
              Else
                  invoice_address3_temp = job:address_line3
                  invoice_address4_temp = job:postcode
              End ! If
              invoice_company_name_temp     = job:company_name
              invoice_telephone_number_temp = job:telephone_number
              invoice_fax_number_temp       = job:fax_number
              invoice_EMail_Address         = jobe:EndUserEmailAddress ! '' ! tra:EmailAddress
  
          Else! If tra:invoice_customer_address = 'YES'
              !            UseAlternativeAddress# = 1
              invoice_address1_temp = tra:address_line1
              invoice_address2_temp = tra:address_line2
              If job:address_line3_delivery   = ''
                  invoice_address3_temp = tra:postcode
                  invoice_address4_temp = ''
              Else
                  invoice_address3_temp = tra:address_line3
                  invoice_address4_temp = tra:postcode
              End ! If
              invoice_company_name_temp     = tra:company_name
              invoice_telephone_number_temp = tra:telephone_number
              invoice_fax_number_temp       = tra:fax_number
              invoice_EMail_Address         = tra:EmailAddress
  
          End! If tra:invoice_customer_address = 'YES'
      end!!if tra:use_sub_accounts = 'YES'
  !End ! If ~glo:WebJob And jobe:WebJob
  
  !**************************************************
  
  
  !*************************set the display address to the head account if booked on as a web job***********************
  ! but only if this was not from a generic account
  
  
  !*********************************************************************************************************************
  
  
  !**************************************************
  If job:chargeable_job <> 'YES'
      labour_temp       = ''
      parts_temp        = ''
      courier_cost_temp = ''
      vat_temp          = ''
      total_temp        = ''
  Else! If job:chargeable_job <> 'YES'
      Access:JOBSE.Clearkey(jobe:RefNumberKey)
      jobe:RefNumber  = job:Ref_Number
      If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      ! Found
  
      Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      ! Error
      End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
  
      If job:invoice_number <> ''
          ! Total_Price('I',vat",total",balance")
          access:invoice.clearkey(inv:invoice_number_key)
          inv:invoice_number = job:invoice_number
          access:invoice.fetch(inv:invoice_number_key)
          if glo:webjob then
              If inv:ExportedRRCOracle
                  Labour_Temp = jobe:InvRRCCLabourCost
                  Parts_Temp  = jobe:InvRRCCPartsCost
                  Vat_Temp    = jobe:InvRRCCLabourCost * inv:Vat_Rate_Labour / 100 + |
                  jobe:InvRRCCPartsCost * inv:Vat_Rate_Parts / 100 +                 |
                  job:Invoice_Courier_Cost * inv:Vat_Rate_Labour / 100
                  courier_cost_temp = job:invoice_courier_cost
              Else ! If inv:ExportedRRCOracle
                  Labour_Temp = jobe:RRCCLabourCost
                  Parts_Temp  = jobe:RRCCPartsCost
                  Vat_Temp    = jobe:RRCClabourCost * GetVATRate(job:Account_Number, 'L') / 100 + |
                  jobe:RRCCPartsCost * GetVATRate(job:Account_Number, 'P') / 100 +                |
                  job:Courier_Cost * GetVATRate(job:Account_Number, 'L') / 100
                  courier_cost_temp = job:courier_cost
              End ! If inv:ExportedRRCOracle
          else
              labour_temp = job:invoice_labour_cost
              Parts_temp  = job:Invoice_Parts_Cost
              Vat_temp    = job:Invoice_Labour_Cost * inv:Vat_Rate_Labour / 100 + |
              job:Invoice_Parts_Cost * inv:Vat_Rate_Parts / 100 +                 |
              job:Invoice_Courier_Cost * inv:Vat_Rate_Labour / 100
              courier_cost_temp = job:invoice_courier_cost
          end ! if
  
      Else! If job:invoice_number <> ''
          If glo:Webjob Then
              Labour_Temp = jobe:RRCCLabourCost
              Parts_Temp  = jobe:RRCCPartsCost
              Vat_Temp    = jobe:RRCClabourCost * GetVATRate(job:Account_Number, 'L') / 100 + |
              jobe:RRCCPartsCost * GetVATRate(job:Account_Number, 'P') / 100 +                |
              job:Courier_Cost * GetVATRate(job:Account_Number, 'L') / 100
          Else
              labour_temp = job:labour_cost
              parts_temp  = job:parts_cost
              Vat_Temp    = job:Labour_Cost * GetVATRate(job:Account_Number, 'L') / 100 + |
              job:Parts_Cost * GetVATRate(job:Account_Number, 'P') / 100 +                |
              job:Courier_Cost * GetVATRate(job:Account_Number, 'L') / 100
          End ! If
  
          courier_cost_temp = job:courier_cost
      End! If job:invoice_number <> ''
      Total_Temp  = Labour_Temp + Parts_Temp + Courier_Cost_Temp + Vat_Temp
  
  End! If job:chargeable_job <> 'YES'
  !**************************************************
  
  setcursor(cursor:wait)
  
  FieldNumber# = 1
  save_maf_id  = access:manfault.savefile()
  access:manfault.clearkey(maf:field_number_key)
  maf:manufacturer = job:manufacturer
  set(maf:field_number_key, maf:field_number_key)
  loop
      if access:manfault.next()
          break
      end ! if
      if maf:manufacturer <> job:manufacturer      |
          then break   ! end if
      end ! if
      yldcnt# += 1
      if yldcnt# > 25
          yield()
          yldcnt# = 0
      end ! if
  
      If maf:Compulsory_At_Booking <> 'YES'
          Cycle
      End ! If maf:Compulsory_At_Booking <> 'YES'
  
      fault_code_field_temp[FieldNumber#] = maf:field_name
  
      Access:MANFAULO.ClearKey(mfo:Field_Key)
      mfo:Manufacturer = job:Manufacturer
      mfo:Field_Number = maf:Field_Number
  
  
      Case maf:Field_Number
      Of 1
          mfo:Field = job:Fault_Code1
      Of 2
          mfo:Field = job:Fault_Code2
      Of 3
          mfo:Field = job:Fault_Code3
      Of 4
          mfo:Field = job:Fault_Code4
      Of 5
          mfo:Field = job:Fault_Code5
      Of 6
          mfo:Field = job:Fault_Code6
      Of 7
          mfo:Field = job:Fault_Code7
      Of 8
          mfo:Field = job:Fault_Code8
      Of 9
          mfo:Field = job:Fault_Code9
      Of 10
          mfo:Field = job:Fault_Code10
      Of 11
          mfo:Field = job:Fault_Code11
      Of 12
          mfo:Field = job:Fault_Code12
      ! Inserting (DBH 21/04/2006) #7551 - Display the extra fault codes, if comp at booking
      Of 13
          mfo:Field = wob:FaultCode13
      Of 14
          mfo:Field = wob:FaultCode14
      Of 15
          mfo:Field = wob:FaultCode15
      Of 16
          mfo:Field = wob:FaultCode16
      Of 17
          mfo:Field = wob:FaultCode17
      Of 18
          mfo:Field = wob:FaultCode18
      Of 19
          mfo:Field = wob:FaultCode19
      Of 20
          mfo:Field = wob:FaultCode20
      End ! Case maf:Field_Number
      ! End (DBH 21/04/2006) #7551
  
      tmp:FaultCodeDescription[FieldNumber#] = mfo:Field
  
      If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
          ! Found
          tmp:FaultCodeDescription[FieldNumber#] = Clip(tmp:FaultCodeDescription[FieldNumber#]) & ' - ' & Clip(mfo:Description)
      Else! If Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign
          ! Error
          ! Assert(0,'<13,10>Fetch Error<13,10>')
      End! If Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign
  
      FieldNumber# += 1
      If FieldNumber# > 6
          Break
      End ! If FieldNumber# > 6
  end ! loop
  access:manfault.restorefile(save_maf_id)
  setcursor()
  
  tmp:accessories = ''
  setcursor(cursor:wait)
  x#          = 0
  Save_jac_ID = Access:JOBACC.SaveFile()
  Access:JOBACC.ClearKey(jac:DamagedKey)
  jac:Ref_Number = job:Ref_Number
  jac:Damaged    = 0
  jac:Pirate     = 0
  Set(jac:DamagedPirateKey, jac:DamagedPirateKey)
  Loop
      If Access:JOBACC.NEXT()
          Break
      End ! If
      If jac:Ref_Number <> job:Ref_Number |
          Then Break
      End ! If
      yldcnt# += 1
      if yldcnt# > 25
          yield()
          yldcnt# = 0
      end ! if
      x# += 1
      If jac:Damaged
          jac:Accessory = 'DAMAGED ' & Clip(jac:Accessory)
      End ! If jac:Damaged
      If jac:Pirate
          jac:Accessory = 'PIRATE ' & Clip(jac:Accessory)
      End ! If jac:Pirate
      If tmp:accessories = ''
          tmp:accessories = jac:accessory
      Else! If tmp:accessories = ''
          tmp:accessories = Clip(tmp:accessories) & ',  ' & Clip(jac:accessory)
      End! If tmp:accessories = ''
  End ! loop
  access:jobacc.restorefile(save_jac_id)
  setcursor()
  
  
  If job:exchange_unit_number <> ''
      access:exchange.clearkey(xch:ref_number_key)
      xch:ref_number = job:exchange_unit_number
      if access:exchange.fetch(xch:ref_number_key) = Level:Benign
          exchange_unit_number_temp  = 'Unit: ' & Clip(job:exchange_unit_number)
          exchange_model_number      = xch:model_number
          exchange_manufacturer_temp = xch:manufacturer
          exchange_unit_type_temp    = 'N/A'
          exchange_esn_temp          = xch:esn
          exchange_unit_title_temp   = 'EXCHANGE UNIT'
          exchange_msn_temp          = xch:msn
      end! if access:exchange.fetch(xch:ref_number_key) = Level:Benign
  Else! If job:exchange_unit_number <> ''
      x#          = 0
      save_jea_id = access:jobexacc.savefile()
      access:jobexacc.clearkey(jea:part_number_key)
      jea:job_ref_number = job:ref_number
      set(jea:part_number_key, jea:part_number_key)
      loop
          if access:jobexacc.next()
              break
          end ! if
          if jea:job_ref_number <> job:ref_number      |
              then break   ! end if
          end ! if
          x# = 1
          Break
      end ! loop
      access:jobexacc.restorefile(save_jea_id)
      If x# = 1
          exchange_unit_number_temp  = ''
          exchange_model_number      = 'N/A'
          exchange_manufacturer_temp = job:manufacturer
          exchange_unit_type_temp    = 'ACCESSORY'
          exchange_esn_temp          = 'N/A'
          exchange_unit_title_temp   = 'EXCHANGE UNIT'
          exchange_msn_temp          = 'N/A'
      End! If x# = 1
  End! If job:exchange_unit_number <> ''
  
  ! Check For Bouncer
  access:manufact.clearkey(man:manufacturer_key)
  man:manufacturer = job:manufacturer
  access:manufact.tryfetch(man:manufacturer_key)
  
  tmp:bouncers = ''
  If job:ESN <> 'N/A' And job:ESN <> ''
      setcursor(cursor:wait)
      save_job2_id = access:jobs2_alias.savefile()
      access:jobs2_alias.clearkey(job2:esn_key)
      job2:esn = job:esn
      set(job2:esn_key, job2:esn_key)
      loop
          if access:jobs2_alias.next()
              break
          end ! if
          if job2:esn <> job:esn      |
              then break   ! end if
          end ! if
          yldcnt# += 1
          if yldcnt# > 25
              yield()
              yldcnt# = 0
          end ! if
          If job2:Cancelled = 'YES'
              Cycle
          End ! If job2:Cancelled = 'YES'
  
          If job2:ref_number <> job:ref_number
  !            If job2:date_booked + man:warranty_period > job:date_booked And job2:date_booked <= job:date_booked
                  If tmp:bouncers = ''
                      tmp:bouncers = 'Previous Job Number(s): ' & job2:ref_number
                  Else! If tmp:bouncer = ''
                      tmp:bouncers = CLip(tmp:bouncers) & ', ' & job2:ref_number
                  End! If tmp:bouncer = ''
  !            End! If job2:date_booked + man:warranty_period < Today()
          End! If job2:esn <> job2:ref_number
      end ! loop
      access:jobs2_alias.restorefile(save_job2_id)
      setcursor()
  End ! job:ESN <> 'N/A' And job:ESN <> ''
  
  If tmp:bouncers <> ''
      Settarget(report)
      Unhide(?bouncertitle)
      Unhide(?tmp:bouncers)
      Settarget()
  End! If CheckBouncer(job:ref_number)
  
  If man:UseInvTextForFaults
      tmp:InvoiceText = ''
      Save_joo_ID     = Access:JOBOUTFL.SaveFile()
      Access:JOBOUTFL.ClearKey(joo:LevelKey)
      joo:JobNumber = job:Ref_Number
      joo:Level     = 10
      Set(joo:LevelKey, joo:LevelKey)
      Loop
          If Access:JOBOUTFL.PREVIOUS()
              Break
          End ! If
          If joo:JobNumber <> job:Ref_Number      |
              Then Break   ! End If
          End ! If
          If tmp:InvoiceText = ''
              tmp:InvoiceText = Clip(joo:FaultCode) & ' - ' & Clip(joo:Description)
          Else ! If tmp:InvoiceText = ''
              tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(joo:FaultCode) & ' - ' & Clip(joo:Description)
          End ! If tmp:InvoiceText = ''
      End ! Loop
      Access:JOBOUTFL.RestoreFile(Save_joo_ID)
  Else ! man:UseInvTextForFaults
      tmp:InvoiceText = jbn:Invoice_Text
  End ! man:UseInvTextForFaults
  
  ! Loan unit information (Added by Gary (7th Aug 2002))
  ! //------------------------------------------------------------------------
  tmp:LoanModel       = ''
  tmp:LoanIMEI        = ''
  tmp:LoanAccessories = ''
  
  if job:Loan_Unit_Number <> 0                                            ! Is a loan unit attached to this job ?
  
      save_loa_id = Access:Loan.SaveFile()
      Access:Loan.Clearkey(loa:Ref_Number_Key)
      loa:Ref_Number  = job:Loan_Unit_Number
      if not Access:Loan.TryFetch(loa:Ref_Number_Key)                         ! Fetch loan unit record
          tmp:LoanModel        = clip(loa:Manufacturer) & ' ' & loa:Model_Number
          tmp:LoanIMEI         = loa:ESN
          tmp:ReplacementValue = jobe:LoanReplacementValue
  
          save_lac_id = Access:LoanAcc.SaveFile()
          Access:LoanAcc.ClearKey(lac:ref_number_key)
          lac:ref_number = loa:ref_number
          set(lac:ref_number_key, lac:ref_number_key)
          loop until Access:LoanAcc.Next()                                    ! Fetch loan accessories
              if lac:ref_number <> loa:ref_number then break.
              if lac:Accessory_Status <> 'ISSUE' then cycle.                  ! Only display issued loan accessories
              if tmp:LoanAccessories = ''
                  tmp:LoanAccessories = lac:Accessory
              else
                  tmp:LoanAccessories = clip(tmp:LoanAccessories) & ', ' & lac:Accessory
              end ! if
  
          end ! loop
          Access:LoanAcc.RestoreFile(save_lac_id)
  
      end ! if
      Access:Loan.RestoreFile(save_loa_id)
  
  end ! if
  
  if tmp:LoanModel <> ''                                                      ! Display loan details on report
      SetTarget(report)
      Unhide(?LoanUnitIssued)
      Unhide(?tmp:LoanModel)
      Unhide(?IMEITitle)
      Unhide(?tmp:LoanIMEI)
      Unhide(?LoanAccessoriesTitle)
      Unhide(?tmp:LoanAccessories)
      Unhide(?tmp:ReplacementValue)
      Unhide(?LoanValueText)
      SetTarget()
  end ! if
  
  
  tmp:LoanDepositPaid = 0
  
  Access:JobPaymt.ClearKey(jpt:Loan_Deposit_Key)
  jpt:Ref_Number   = job:Ref_Number
  jpt:Loan_Deposit = True
  set(jpt:Loan_Deposit_Key, jpt:Loan_Deposit_Key)
  loop until Access:JobPaymt.Next()                                           ! Loop through payments attached to job
      if jpt:Ref_Number <> job:Ref_Number then break.
      if jpt:Loan_Deposit <> True then break.
      tmp:LoanDepositPaid += jpt:Amount                                       ! Check for deposit value
  end ! loop
  
  if tmp:LoanDepositPaid <> 0
      SetTarget(report)
      Unhide(?LoanDepositPaidTitle)
      Unhide(?tmp:LoanDepositPaid)                                            ! Display fields if a deposit was found
      SetTarget()
  end ! if
  ! //------------------------------------------------------------------------
  
  ReturnValue = PARENT.TakeRecord()
    If Not p_web &= Null
      p_web.NoOp()
    End
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
  PRINT(RPT:DETAIL)
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','WebServer_Phase4','JobCard','JobCard','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.CompressText   = True
  SELF.CompressImages = True
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    SELF.SetFileName(clip(loc:PDFName))
  End

