

   MEMBER('WebServer_Phase4.clw')                          ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPRPDF.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE
   INCLUDE('ABRPPSEL.INC'),ONCE
   INCLUDE('ABWMFPAR.INC'),ONCE

                     MAP
                       INCLUDE('WEBSE014.INC'),ONCE        !Local module procedure declarations
                     END


CheckFaultFormat     PROCEDURE  (func:FaultCode,func:FieldFormat) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
tmp:MonthStart       LONG                                  !Month Start
tmp:MonthValue       LONG                                  !Month Value
tmp:YearStart        LONG                                  !Year Start
tmp:YearValue        LONG                                  !Year Value
tmp:StartQuotes      LONG                                  !Start Quotes
tmp:FaultYearStart   LONG                                  !Fault Year Start
tmp:FaultMonthStart  LONG                                  !Fault Month Start
  CODE
    y# = 1
    tmp:MonthStart = 0
    tmp:YearStart = 0
    Loop x# = 1 To Len(Clip(func:FieldFormat))
        !If this is in a quotes just do a straight comparison of characters
        If tmp:StartQuotes
            If Sub(func:FieldFormat,x#,1) = '"'
                !End Of The Quotes
                tmp:StartQuotes = 0
                Cycle
            End !If Sub(func:FieldFormat,x#,1) = '"'
            If Sub(func:FieldFormat,x#,1) <> Sub(func:FaultCode,y#,1)
                Return Level:Fatal
            Else! !If Sub(func:FieldFormat,x#,1) <> Sub(func:FaultCode,x#,1)
                y# += 1
                Cycle
            End !If Sub(func:FieldFormat,x#,1) <> Sub(func:FaultCode,x#,1)

        End !If tmp:StartQuotes

        If tmp:YearStart
            If Sub(func:FieldFormat,x#,1) <> 'Y'
                If x# - tmp:YearStart = 4
                    If Sub(func:FaultCode,tmp:FaultYearStart,4) < 1990 Or Sub(func:FaultCode,tmp:FaultYearStart,4) > 2010
                        !Failed Year
                        Return Level:Fatal
                    End !If Sub(func:FaultCode,tmp:YearStart,4) < 1990 Or Sub(func:FaultCode,tmp:YearStart,4) > 2010
                End !If x# - tmp:YearStart = 4

                If x# - tmp:YearStart = 2
                    If Sub(func:FaultCode,tmp:FaultYearStart,2) > 10 And Sub(func:FaultCode,tmp:FaultYearStart,2) < 90
                        !Failed Year
                        Return Level:Fatal
                    End !If Sub(func:FaultCode,tmp:YearStart,4) < 1990 Or Sub(func:FaultCode,tmp:YearStart,4) > 2010
                End !If x# - tmp:YearStart = 2
                tmp:YearStart = 0
                y# += 1
            Else !If Sub(func:FieldFormat,x#,1) <> 'Y'
                Cycle
            End !If Sub(func:FieldFormat,x#,1) <> 'Y'
        End !If tmp:YearStart

        If tmp:MonthStart
            If Sub(func:FieldFormat,x#,1) <> 'M'
                If Sub(func:FaultCode,tmp:FaultMonthStart,2) < 1 Or Sub(func:FaultCode,tmp:FaultMonthStart,2) > 12
                    !Month Fail
                    Return Level:Fatal
                End !If Sub(func:FaultCode,tmp:FaultMonthStart,2) < 1 Or Sub(func:FaultCode,tmp:FaultMonthStart,2) > 12
                tmp:MonthStart = 0
                y# += 1
            Else !If Sub(func:FieldFormat,x#,1) <> 'M'
                Cycle
            End !If Sub(func:FieldFormat,x#,1) <> 'M'
        End !If tmp:MonthStart

        Case Sub(func:FieldFormat,x#,1)
            Of 'A'
                If Val(Sub(func:FaultCode,y#,1)) < 65 Or Val(Sub(func:FaultCode,y#,1)) > 90
                    !Alpha Error
                    Return Level:Fatal
                End !If Val(Sub(func:FaultCode,x#,1)) < 65 Or Val(Sub(func:FaultCode,x#,1)) > 90
            Of '0'
                If Val(Sub(func:FaultCode,y#,1)) < 48 Or Val(Sub(func:FaultCode,y#,1)) > 57
                    !Numeric Error

                    Return Level:Fatal
                End !If Val(Sub(func:FaultCode,x#,1)) < 48 Or Val(Sub(func:FaultCode,x#,1)) > 57
            Of 'X'
                If ~((Val(Sub(func:FaultCode,y#,1)) >=48 And Val(Sub(func:FaultCode,y#,1)) <= 57) Or |
                    (Val(Sub(func:FaultCode,y#,1)) >=65 And Val(Sub(func:FaultCode,y#,1)) <=90))

                    !Alphanumeric Error
                    Return Level:Fatal
                End !(Val(Sub(func:FaultCode,x#,1)) >=65 And Val(Sub(func:FaultCode,x#,1)) <=90))
            Of '"'
                tmp:StartQuotes = 1
                Cycle
            Of 'Y'
                tmp:YearStart   = x#
                tmp:FaultYearStart  = y#
            Of 'M'
                tmp:MonthStart  = x#
                tmp:FaultMonthStart = y#
        End !Case Sub(func:FaultCode,x#,1)
        y# += 1
    End !Loop x# = 1 To Len(func:FaultCode)

    If tmp:YearStart

        If x# - tmp:YearStart = 4
            If Sub(func:FaultCode,tmp:FaultYearStart,4) < 1990 Or Sub(func:FaultCode,tmp:FaultYearStart,4) > 2010
                !Failed Year
                Return Level:Fatal
            End !If Sub(func:FaultCode,tmp:YearStart,4) < 1990 Or Sub(func:FaultCode,tmp:YearStart,4) > 2010
        End !If x# - tmp:YearStart = 4

        If x# - tmp:YearStart = 2
            If Sub(func:FaultCode,tmp:FaultYearStart,2) < 90 And Sub(func:FaultCode,tmp:FaultYearStart,2) > 10
                !Failed Year
                Return Level:Fatal
            End !If Sub(func:FaultCode,tmp:YearStart,4) < 1990 Or Sub(func:FaultCode,tmp:YearStart,4) > 2010
        End !If x# - tmp:YearStart = 2
    End !If tmp:YearStart

    If tmp:MonthStart
        If Sub(func:FaultCode,tmp:FaultMonthStart,2) <1 Or Sub(func:FaultCode,tmp:FaultMonthStart,2) > 12
            !Month Fail
            Return Level:Fatal
        End !If Sub(func:FaultCode,tmp:FaultMonthStart,2) < 1 Or Sub(func:FaultCode,tmp:FaultMonthStart,2) > 12
    End !If tmp:MonthStart

    Return Level:Benign




VodacomSingleInvoice PROCEDURE(<NetWebServerWorker p_web>)
Default_Invoice_Company_Name_Temp STRING(30)
locInvoiceNumber     LONG
locInvoiceDate       DATE
save_tradeacc_id     USHORT,AUTO
save_invoice_id      USHORT,AUTO
save_jobs_id         USHORT,AUTO
save_jobse_id        USHORT,AUTO
save_audit_id        USHORT,AUTO
save_aud_id          USHORT,AUTO
tmp:PrintA5Invoice   BYTE(0)
tmp:Ref_Number       STRING(20)
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
RejectRecord         LONG,AUTO
save_par_id          USHORT,AUTO
save_jpt_id          USHORT,AUTO
tmp:PrintedBy        STRING(255)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
code_temp            BYTE
option_temp          BYTE
Bar_code_string_temp CSTRING(21)
Bar_Code_Temp        CSTRING(21)
Bar_Code2_Temp       CSTRING(21)
accessories_temp     STRING(30),DIM(6)
estimate_value_temp  STRING(40)
despatched_user_temp STRING(40)
vat_temp             REAL
total_temp           REAL
part_number_temp     STRING(30)
line_cost_temp       REAL
job_number_temp      STRING(20)
esn_temp             STRING(24)
customer_name_temp   STRING(60)
sub_total_temp       REAL
payment_date_temp    DATE,DIM(4)
other_amount_temp    REAL
total_paid_temp      REAL
payment_type_temp    STRING(20),DIM(4)
payment_amount_temp  REAL,DIM(4)
invoice_company_name_temp STRING(30)
invoice_telephone_number_temp STRING(15)
invoice_fax_number_temp STRING(15)
DefaultAddress       GROUP,PRE(address)
SiteName             STRING(40)
Name                 STRING(30)
Name2                STRING(40)
Location             STRING(40)
RegistrationNo       STRING(30)
AddressLine1         STRING(40)
AddressLine2         STRING(40)
AddressLine3         STRING(40)
AddressLine4         STRING(40)
Telephone            STRING(30)
Fax                  STRING(30)
EmailAddress         STRING(255)
VatNumber            STRING(30)
                     END
tmp:LabourCost       REAL
tmp:PartsCost        REAL
tmp:CourierCost      REAL
tmp:InvoiceNumber    STRING(30)
InvoiceAddressGroup  GROUP,PRE(ia)
CustomerName         STRING(60)
CompanyName          STRING(30)
AddressLine1         STRING(30)
AddressLine2         STRING(30)
AddressLine3         STRING(30)
AddressLine4         STRING(30)
TelephoneNumber      STRING(30)
FaxNumber            STRING(30)
EmailAddress         STRING(255)
VatNumber            STRING(30)
                     END
DeliveryAddressGroup GROUP,PRE(da)
CustomerName         STRING(60)
CompanyName          STRING(30)
AddressLine1         STRING(30)
AddressLine2         STRING(30)
AddressLine3         STRING(30)
AddressLine4         STRING(30)
TelephoneNumber      STRING(30)
                     END
locFranchiseAccountNumber STRING(20)
locDateIn            DATE
locDateOut           DATE
locTimeIn            TIME
locTimeOut           TIME
locMainOutFault      STRING(1000)
locAuditNotes        STRING(255)
LOC:SaveToQueue      PrintPreviewFileQueue
!-----------------------------------------------------------------------------
Process:View         VIEW(INVOICE)
                     END
Report               REPORT,AT(385,6646,7521,2260),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,2240,7521,6687),USE(?unnamed)
                         STRING(@s16),AT(6354,1615),USE(tmp:Ref_Number),TRN,LEFT,FONT(,8,,)
                         STRING(@s10),AT(3906,1615),USE(wob:HeadAccountNumber),TRN,LEFT,FONT(,8,,)
                         STRING(@s15),AT(2917,1615),USE(tmp:InvoiceNumber),TRN,LEFT,FONT(,8,,)
                         STRING(@s20),AT(5000,1615),USE(job:Order_Number),TRN,LEFT,FONT(,8,,)
                         STRING(@s60),AT(156,52),USE(ia:CustomerName),TRN,FONT(,8,,)
                         STRING(@s60),AT(4115,52),USE(da:CustomerName),TRN,FONT(,8,,)
                         STRING(@d6b),AT(156,1615),USE(locInvoiceDate),TRN,RIGHT,FONT(,8,,)
                         STRING(@s25),AT(1458,2135,1000,156),USE(job:Model_Number),TRN,LEFT,FONT(,8,,)
                         STRING(@s20),AT(156,2135,1323,156),USE(job:Manufacturer),TRN,LEFT,FONT(,8,,)
                         STRING(@s16),AT(5000,2135,990,156),USE(job:ESN),TRN,LEFT,FONT(,8,,)
                         STRING(@s16),AT(2917,2135),USE(job:MSN),TRN,FONT(,8,,)
                         STRING('REPORTED FAULT'),AT(104,2552,1187,),USE(?String64),TRN,FONT(,9,,FONT:bold)
                         TEXT,AT(1625,2542,5313,656),USE(jbn:Fault_Description),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('ENGINEERS REPORT'),AT(83,3208,1354,),USE(?String88),TRN,FONT(,9,,FONT:bold)
                         STRING('PARTS USED'),AT(104,4219),USE(?String79),TRN,FONT(,9,,FONT:bold)
                         STRING('Qty'),AT(1521,4219),USE(?String80),TRN,FONT(,8,,FONT:bold)
                         STRING('Part Number'),AT(1917,4219),USE(?String81),TRN,FONT(,8,,FONT:bold)
                         STRING('Description'),AT(3677,4219),USE(?String82),TRN,FONT(,8,,FONT:bold)
                         STRING('Unit Cost'),AT(5719,4219),USE(?String83),TRN,FONT(,8,,FONT:bold)
                         STRING('Line Cost'),AT(6677,4219),USE(?String89),TRN,FONT(,8,,FONT:bold)
                         LINE,AT(1354,4375,6042,0),USE(?Line1),COLOR(COLOR:Black)
                         STRING(@s30),AT(156,833),USE(ia:AddressLine4),TRN,FONT(,8,,)
                         STRING('Tel: '),AT(4115,990),USE(?String32:2),TRN,FONT(,8,,)
                         STRING(@s30),AT(4323,990),USE(da:TelephoneNumber),FONT(,8,,)
                         STRING('Vat No:'),AT(156,990),USE(?String34:3),TRN,FONT(,8,,)
                         STRING(@s30),AT(625,990,1406,208),USE(ia:VatNumber),TRN,LEFT,FONT(,8,,)
                         STRING('Tel:'),AT(2083,677),USE(?String34:2),TRN,FONT(,8,,)
                         STRING(@s30),AT(2344,677,1406,208),USE(ia:TelephoneNumber),TRN,LEFT,FONT(,8,,)
                         STRING('Fax: '),AT(2083,833),USE(?String35:2),TRN,FONT(,8,,)
                         STRING(@s30),AT(2344,833,1094,208),USE(ia:FaxNumber),TRN,LEFT,FONT(,8,,)
                         STRING(@s30),AT(4115,208),USE(da:CompanyName),TRN,FONT(,8,,)
                         STRING(@s30),AT(4115,365,1917,156),USE(da:AddressLine1),TRN,FONT(,8,,)
                         STRING(@s30),AT(4115,521),USE(da:AddressLine2),TRN,FONT(,8,,)
                         STRING(@s30),AT(4115,677),USE(da:AddressLine3),TRN,FONT(,8,,)
                         STRING(@s30),AT(4115,833),USE(da:AddressLine4),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,208),USE(ia:CompanyName),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,365),USE(ia:AddressLine1),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,677,1885,),USE(ia:AddressLine3),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,521),USE(ia:AddressLine2),TRN,FONT(,8,,)
                         TEXT,AT(1625,3208,5312,875),USE(locMainOutFault),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                       END
EndOfReportBreak       BREAK(EndOfReport),USE(?BREAK1)
DETAIL                   DETAIL,AT(,,,198),USE(?DetailBand)
                           STRING(@n8b),AT(1156,0),USE(par:Quantity),TRN,RIGHT,FONT(,8,,)
                           STRING(@s25),AT(1917,0),USE(part_number_temp),TRN,FONT(,8,,)
                           STRING(@s25b),AT(3677,0),USE(par:Description),TRN,FONT(,8,,)
                           STRING(@n14.2b),AT(5396,0),USE(par:Sale_Cost),TRN,RIGHT,FONT(,8,,)
                           STRING(@n14.2b),AT(6396,0),USE(line_cost_temp),TRN,RIGHT,FONT(,8,,)
                         END
                       END
ANewPage               DETAIL,PAGEAFTER(1),AT(,,7521,83),USE(?detailANewPage)
                       END
                       FOOTER,AT(385,9104,7521,2406),USE(?unnamed:4)
                         STRING('Labour:'),AT(4844,104),USE(?String90),TRN,FONT(,8,,)
                         STRING(@n-14.2),AT(6417,104),USE(tmp:LabourCost),TRN,RIGHT,FONT(,8,,)
                         STRING('Time In:'),AT(1615,156),USE(?String90:3),TRN,FONT(,8,,)
                         STRING(@d17b),AT(781,573),USE(locDateOut),TRN,FONT(,8,,)
                         STRING('Other:'),AT(4844,417),USE(?String93),TRN,FONT(,8,,)
                         STRING(@n-14.2),AT(6417,260),USE(tmp:PartsCost),TRN,RIGHT,FONT(,8,,)
                         STRING(@t7b),AT(2240,573),USE(locTimeOut),TRN,FONT(,8,,)
                         STRING(@t7b),AT(2240,156),USE(locTimeIn),TRN,FONT(,8,,)
                         STRING('Date Out:'),AT(156,573),USE(?String90:4),TRN,FONT(,8,,)
                         STRING('V.A.T.'),AT(4844,729),USE(?String94),TRN,FONT(,8,,)
                         STRING(@n-14.2),AT(6417,417),USE(tmp:CourierCost),TRN,RIGHT,FONT(,8,,)
                         STRING('Technician:'),AT(156,990),USE(?String90:6),TRN,FONT(,8,,)
                         STRING(@s30),AT(833,990),USE(job:Engineer),TRN,FONT(,8,,)
                         STRING('Time Out:'),AT(1615,573),USE(?String90:5),TRN,FONT(,8,,)
                         STRING('Date In:'),AT(156,156),USE(?String90:2),TRN,FONT(,8,,)
                         STRING('Total:'),AT(4844,990),USE(?String95),TRN,FONT(,8,,FONT:bold)
                         LINE,AT(6302,937,1000,0),USE(?Line2),COLOR(COLOR:Black)
                         STRING(@n-14.2),AT(6417,990),USE(total_temp),TRN,RIGHT,FONT(,8,,FONT:bold)
                         STRING('Received By: '),AT(156,1510),USE(?String90:7),TRN,FONT(,10,,)
                         STRING('Signature: '),AT(3021,1510),USE(?String90:8),TRN,FONT(,10,,)
                         STRING('Date:'),AT(5885,1510),USE(?String90:9),TRN,FONT(,10,,)
                         LINE,AT(990,1719,2031,0),USE(?Line9),COLOR(COLOR:Black)
                         LINE,AT(3646,1719,2240,0),USE(?Line9:2),COLOR(COLOR:Black)
                         LINE,AT(6198,1719,1250,0),USE(?Line9:3),COLOR(COLOR:Black)
                         STRING(@d17b),AT(781,156),USE(locDateIn),TRN,FONT(,8,,)
                         LINE,AT(6302,573,1000,0),USE(?Line2:2),COLOR(COLOR:Black)
                         STRING(@n14.2),AT(6396,573),USE(sub_total_temp),TRN,RIGHT,FONT(,8,,)
                         STRING('Sub Total:'),AT(4844,573),USE(?String106),TRN,FONT(,8,,)
                         STRING(@n14.2),AT(6396,729),USE(vat_temp),TRN,RIGHT,FONT(,8,,)
                         STRING('<128>'),AT(6344,990),USE(?Euro),TRN,HIDE,FONT(,8,,FONT:bold)
                         STRING('Parts:'),AT(4844,260),USE(?String92),TRN,FONT(,8,,)
                       END
                       FORM,AT(385,406,7521,11198),USE(?unnamed:3),COLOR(COLOR:White)
                         BOX,AT(52,3125,7396,260),USE(?Box2),ROUND,COLOR(COLOR:Black),FILL(0ECECECH),LINEWIDTH(1)
                         BOX,AT(52,3646,7396,260),USE(?Box2:3),ROUND,COLOR(COLOR:Black),FILL(0ECECECH),LINEWIDTH(1)
                         STRING('TAX INVOICE'),AT(4167,0,3281,),USE(?Text:Invoice),TRN,RIGHT,FONT(,18,,FONT:bold)
                         STRING(@s40),AT(104,0,4167,260),USE(address:SiteName),LEFT,FONT(,14,,FONT:bold)
                         STRING(@s40),AT(104,417,3073,208),USE(address:Name2),TRN,FONT(,8,COLOR:Black,FONT:bold)
                         STRING(@s40),AT(104,938,2240,156),USE(address:AddressLine1),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s40),AT(104,1042,2240,156),USE(address:AddressLine2),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s40),AT(104,1146,2240,156),USE(address:AddressLine3),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s40),AT(104,1250),USE(address:AddressLine4),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING('TEL:'),AT(104,1406),USE(?String16),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s30),AT(521,1406,1458,208),USE(address:Telephone),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s40),AT(104,260,3073,208),USE(address:Name),TRN,FONT(,8,COLOR:Black,FONT:bold)
                         STRING(@s40),AT(104,573,2760,208),USE(address:Location),TRN,FONT(,8,COLOR:Black,FONT:bold)
                         STRING(@s40),AT(625,729,1667,208),USE(address:RegistrationNo),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING('FAX:'),AT(2083,1406,313,208),USE(?String19),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s30),AT(2396,1406,1510,188),USE(address:Fax),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING('EMAIL:'),AT(104,1510,417,208),USE(?String19:2),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s255),AT(521,1510,3333,208),USE(address:EmailAddress),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING(@s20),AT(625,833,1771,156),USE(address:VatNumber),TRN,FONT(,7,,,CHARSET:ANSI)
                         STRING('INVOICE ADDRESS'),AT(104,1667,1229,),USE(?String119),TRN,FONT(,8,,FONT:bold)
                         STRING('Make'),AT(156,3698),USE(?String40),TRN,FONT(,8,,FONT:bold)
                         STRING('Date'),AT(156,3177),USE(?String40:2),TRN,FONT(,8,,FONT:bold)
                         LINE,AT(1354,3125,0,1042),USE(?Line5),COLOR(COLOR:Black)
                         STRING('Our Ref'),AT(1458,3177),USE(?String40:3),TRN,FONT(,8,,FONT:bold)
                         STRING('Invoice No'),AT(2917,3177),USE(?String40:4),TRN,FONT(,8,,FONT:bold)
                         STRING('Your Order No'),AT(5000,3177),USE(?String40:5),TRN,FONT(,8,,FONT:bold)
                         STRING('Job No'),AT(6354,3177),USE(?String40:6),TRN,FONT(,8,,FONT:bold)
                         STRING('Account No'),AT(3906,3177),USE(?String40:7),TRN,FONT(,8,,FONT:bold)
                         BOX,AT(52,3385,7396,260),USE(?Box2:2),ROUND,COLOR(COLOR:Black),LINEWIDTH(1)
                         LINE,AT(6250,3125,0,521),USE(?Line5:4),COLOR(COLOR:Black)
                         LINE,AT(2812,3125,0,1042),USE(?Line5:2),COLOR(COLOR:Black)
                         LINE,AT(3802,3125,0,521),USE(?Line5:8),COLOR(COLOR:Black)
                         BOX,AT(52,3906,7396,260),USE(?Box2:4),ROUND,COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING('Model'),AT(1458,3698),USE(?String41),TRN,FONT(,8,,FONT:bold)
                         STRING('Serial Number'),AT(2917,3698),USE(?String42),TRN,FONT(,8,,FONT:bold)
                         STRING('I.M.E.I. Number'),AT(5000,3698),USE(?String43),TRN,FONT(,8,,FONT:bold)
                         BOX,AT(52,8698,2760,1302),USE(?Box4),ROUND,COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(4688,8698,2760,1302),USE(?Box5),ROUND,COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(52,4219,7396,4323),USE(?Box3),ROUND,COLOR(COLOR:Black),LINEWIDTH(1)
                         LINE,AT(4896,3125,0,1042),USE(?Line5:3),COLOR(COLOR:Black)
                         BOX,AT(52,1823,3438,1250),USE(?Box1),ROUND,COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(4010,1823,3438,1250),USE(?Box6),ROUND,COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING('DELIVERY ADDRESS'),AT(4063,1667,1396,),USE(?String119:2),TRN,FONT(,8,,FONT:bold)
                         TEXT,AT(4542,292,2917,1333),USE(de2:GlobalPrintText),TRN,RIGHT,FONT(,8,,)
                         STRING('REG NO:'),AT(104,729,542,),USE(?String16:2),TRN,FONT(,7,,,CHARSET:ANSI),COLOR(COLOR:White)
                         STRING('VAT NO:'),AT(104,833,542,156),USE(?String16:3),TRN,FONT(,7,,,CHARSET:ANSI),COLOR(COLOR:White)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END


LocalTargetSelector  ReportTargetSelectorClass             ! TargetSelector for the Report Processors
LocalReportTarget    &IReportGenerator                     ! ReportTarget for the Report Processors
LocalAttribute       ReportAttributeManager                ! Attribute manager for the Report Processors
LocalWMFParser       WMFDocumentParser                     ! WMFParser for the Report Processors
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED                 ! Method added to host embed code
                     END

  ! The NetTalk Extension to report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
Loc:Html      String(1024)


PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       PrintPreviewFileQueue

PreviewQueueIndex       BYTE


CPCSEmailDialog         BYTE(0)
PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          LONG(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(128)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE



LocalOutputFileQueue PrintPreviewFileQueue

! CPCS Template version  v6.30
! CW Template version    v6.3
! CW Version             6300
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('VodacomSingleInvoice')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  SETCURSOR
  PreviewReq = True
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  locInvoiceNumber = p_web.GSV('job:Invoice_Number')
  
  FileOpensReached = True
  FilesOpened = True
  Relate:INVOICE.Open
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:JOBPAYMT.Open
  Relate:STANTEXT.Open
  Relate:WEBJOB.Open
  Access:JOBOUTFL.UseFile
  Access:JOBACC.UseFile
  Access:JOBS.UseFile
  Access:USERS.UseFile
  Access:PARTS.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:JOBNOTES.UseFile
  Access:JOBSE.UseFile
  Access:AUDIT.UseFile
  Access:JOBSINV.UseFile
  Access:MANFAULT.UseFile
  Access:MANFAULO.UseFile
  Access:AUDSTATS.UseFile
  Access:LOCATLOG.UseFile
  DO BindFileFields
  
  
  LocalTargetSelector.AddItem(PDFReporter.IReportGenerator)
  IF PreviewReq = True
    LocalReportTarget &= PDFReporter.IReportGenerator
  END
  RecordsToProcess = RECORDS(INVOICE)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  WindowOpened = True
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    PreviewReq = True
    CPCS:SVOutSkipPreview# = True
    ProgressWindow{prop:hide} = 1
    loc:PDFName = clip(p_web.site.WebFolderPath) & '\reports\$$$' & format(random(1,99999),@n05) &'.pdf'
  End
  SEND(INVOICE,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END
    OF Event:OpenWindow
      SET(inv:Invoice_Number_Key)
      Process:View{Prop:Filter} = |
      'inv:Invoice_Number = locInvoiceNumber'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
      
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Printing Details
        
        ! Is this a credit note?
        IF (p_web.GSV('CreditNoteCreated') = 1)
            Access:JOBSINV.ClearKey(jov:TypeSuffixKey)
            jov:RefNumber = p_web.GSV('job:ref_Number')
            jov:Type = 'C'
            jov:Suffix = p_web.GSV('CreditSuffix')
            IF (Access:JOBSINV.TryFetch(jov:TypeSuffixKey))
            END
        !    PrintReport(2)
        !    Print(rpt:Detail)
            tmp:InvoiceNumber = 'CN' & Clip(tmp:InvoiceNumber) & p_web.GSV('CreditSuffix')
            Total_Temp = -jov:CreditAmount
            SetTarget(Report)
            ?Text:Invoice{prop:Text} = 'CREDIT NOTE'
            SetTarget()
            DO PrintParts
            Print(rpt:ANewPage)
        !    locDetail = ''
        
            Access:JOBSINV.ClearKey(jov:TypeSuffixKey)
            jov:RefNumber = p_web.GSV('job:ref_Number')
            jov:Type = 'I'
            jov:Suffix = p_web.GSV('InvoiceSuffix')
            IF (Access:JOBSINV.TryFetch(jov:TypeSuffixKey))
            END
            tmp:InvoiceNumber = Clip(tmp:InvoiceNumber) & p_web.GSV('InvoiceSuffix')
            Total_Temp = jov:NewTotalCost
        !    PrintReport(1)
            DO PrintParts
        
        
        
        
        ELSE
        !    PrintReport(0)
            DO PrintParts
        END
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,CPCS:ButtonYesNo,2)
          OF 2
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,CPCS:ButtonYesNoIgnore,2)
            OF 2
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF 3
              CancelRequested = False
              CYCLE
            OF 1
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(INVOICE,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        if not p_web &= null   ! NetTalk, Report procedure should have prototype of (<NetWebServerWorker p_web>)
          loc:noRecords = 1
          CLOSE(Report)
          DO ProcedureReturn
        End
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation,CPCS:ButtonOk)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        IF ~CPCS:SVOutSkipPreview#
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,,,'','',,,,,,,,,,,,,LOC:SaveToQueue)
        ELSE
          LOOP PP# = 1 To RECORDS(PrintPreviewQueue)
            GET(PrintPreviewQueue, PP#)
            LOC:SaveToQueue = PrintPreviewQueue
            ADD(LOC:SaveToQueue)
          END
          GlobalResponse = RequestCompleted
          FREE(PrintPreviewQueue)
        END
          DO GenerateSVReportOutput
  
  
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      if not p_web &= null   ! NetTalk, Report procedure should have prototype of (<NetWebServerWorker p_web>)
        loc:noRecords = 1
        CLOSE(Report)
        DO ProcedureReturn
      End
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation,CPCS:ButtonOk)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        if not p_web &= null   ! NetTalk, Report procedure should have prototype of (<NetWebServerWorker p_web>)
          loc:noRecords = 1
          CLOSE(Report)
          DO ProcedureReturn
        End
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation,CPCS:ButtonOk)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      if not p_web &= null   ! NetTalk, Report procedure should have prototype of (<NetWebServerWorker p_web>)
        loc:noRecords = 1
        CLOSE(Report)
        DO ProcedureReturn
      End
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation,CPCS:ButtonOk)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(Report)
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    CLOSE(Process:View)
    Relate:AUDIT.Close
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:JOBPAYMT.Close
    Relate:STANTEXT.Close
    Relate:WEBJOB.Close
  END
  IF WindowOpened
    ProgressWindow{PROP:HIDE}=False
    CLOSE(ProgressWindow)
  END
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      Loc:html = '<script type="text/javascript">alert('''&clip('No Records')&''');top.close();</script>'
      p_web.ParseHTML(loc:html)
    Else
      p_web.ForceNoCache = 1
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(loc:PDFName)
    End
  End

  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  FREE(LOC:SaveToQueue)
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
    If Not p_web &= Null
      p_web.NoOp()
    End
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'INVOICE')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  SYSTEM{PROP:PrintMode} = 3
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  ! Get Records
  Access:JOBS.ClearKey(job:Ref_Number_Key)
  job:Ref_Number = p_web.GSV('job:Ref_Number')
  IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
  END
  Access:WEBJOB.ClearKey(wob:RefNumberKey)
  wob:RefNumber = p_web.GSV('wob:RefNumber')
  IF (Access:WEBJOB.TryFetch(wob:RefNumberKey))
  END
  Access:JOBSE.ClearKey(jobe:RefNumberKey)
  jobe:RefNumber = p_web.GSV('jobe:RefNumber')
  IF (Access:JOBSE.Tryfetch(jobe:RefNumberKey))
  END
  
  Access:JOBNOTES.Clearkey(jbn:RefNumberKey)
  jbn:RefNumber = p_web.GSV('job:Ref_Number')
  IF (Access:JOBNOTES.TryFetch(jbn:RefNumberKey))
  END
  
  SET(DEFAULT2,0) ! #12079 Pick up new "Global Text" (Bryan: 13/04/2011)
  Access:DEFAULT2.Next()
  ! Set Default Address
  ! Set Job Number / Invoice Number
  Access:TRADEACC.Clearkey(tra:Account_Number_Key)
  tra:Account_Number = p_web.GSV('wob:HeadAccountNumber')
  IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
      tmp:Ref_Number = p_web.GSV('job:Ref_Number') & '-' & Clip(tra:BranchIdentification) & p_web.GSV('wob:JobNumber')
      tmp:InvoiceNumber = CLIP(p_web.GSV('job:Invoice_Number')) & '-' & Clip(tra:BranchIdentification)
  END
  
  Access:TRADEACC.Clearkey(tra:Account_Number_Key)
  If (p_web.GSV('BookingSite') = 'RRC')
      tra:Account_Number = p_web.GSV('wob:HeadAccountNumber')
  ELSE ! If (glo:WebJob = 1)
      tra:Account_Number = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
  END ! If (glo:WebJob = 1)
  If (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
      address:SiteName        = tra:Company_Name
      address:Name            = tra:coTradingName
      address:Name2           = tra:coTradingName2  ! #12079 New address fields. (Bryan: 13/04/2011)
      address:Location        = tra:coLocation
      address:RegistrationNo  = tra:coRegistrationNo
      address:AddressLine1    = tra:coAddressLine1
      address:AddressLine2    = tra:coAddressLine2
      address:AddressLine3    = tra:coAddressLine3
      address:AddressLine4    = tra:coAddressLine4
      address:Telephone       = tra:coTelephoneNumber
      address:Fax             = tra:coFaxNumber
      address:EmailAddress    = tra:coEmailAddress
      address:VatNumber       = tra:coVATNumber
  
      foundPrinted# = 0
      Access:AUDIT.Clearkey(aud:Action_Key)
      aud:Ref_Number = p_web.GSV('job:Ref_Number')
      aud:Action = 'INVOICE PRINTED'
      SET(aud:Action_Key,aud:Action_Key)
      LOOP UNTIL Access:AUDIT.Next()
          IF (aud:Ref_Number <> p_web.GSV('job:Ref_Number') OR |
              aud:Action <> 'INVOICE PRINTED')
              BREAK
          END
          IF (INSTRING('INVOICE NUMBER: ' & CLIP(inv:Invoice_Number) & '-' & |
              CLIP(tra:BranchIdentification),aud:Notes,1,1))
              foundPrinted# = 1
              BREAK
          END
      END
  
      If (foundPrinted# = 1)
          REPORT $ ?Text:Invoice{prop:Text} = 'COPY TAX INVOICE'
      END
  
      Do CreateARCRRCInvoices
  END
  
  ! Set Invoice  / Delivery Address
  Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
  sub:Account_Number = p_web.GSV('job:Account_Number')
  IF (Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign)
      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
      tra:Account_Number = sub:Main_Account_Number
      IF (Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign)
      END
  END
  CLEAR(InvoiceAddressGroup)
  
  IF (sub:OverrideHeadVATNo = 1)
      ia:VatNumber = sub:Vat_Number
  ELSE
      ia:VatNumber = tra:Vat_Number
  END
  
  IF (tra:Invoice_Sub_Accounts = 'YES')
      IF (sub:Invoice_Customer_Address <> 'YES')
          ia:CompanyName = sub:Company_Name
          ia:AddressLine1 = sub:Address_Line1
          ia:AddressLine2 = sub:Address_Line2
          ia:AddressLine3 = sub:Address_Line3
          ia:AddressLine4 = sub:Postcode
          ia:TelephoneNumber = sub:Telephone_Number
          ia:FaxNumber = sub:Fax_Number
          ia:CustomerName = sub:Contact_Name
      END
      IF (ia:VatNumber = '')
          ia:VatNumber = p_web.GSV('jobe:VatNumber')
      END
  ELSE
      IF (tra:Invoice_Customer_Address <> 'YES')
          ia:CompanyName = tra:Company_Name
          ia:AddressLine1 = tra:Address_Line1
          ia:AddressLine2 = tra:Address_Line2
          ia:AddressLine3 = tra:Address_Line3
          ia:AddressLine4 = tra:Postcode
          ia:TelephoneNumber = tra:Telephone_Number
          ia:FaxNumber = tra:Fax_Number
          ia:CustomerName = tra:Contact_Name
      END
  
  END
  IF (ia:CompanyName) = ''
      IF (p_web.GSV('job:Surname') <> '')
          IF (p_web.GSV('job:Title') = '')
              ia:CustomerName = p_web.GSV('job:Surname')
          ELSE
              IF (p_web.GSV('job:Initial') <> '')
                  ia:CustomerName = CLIP(p_web.GSV('job:Title')) & ' ' & CLIP(p_web.GSV('job:Initial')) & ' ' & CLIP(p_web.GSV('job:Surname'))
              ELSE
                  ia:CustomerName = CLIP(p_web.GSV('job:Title')) & ' ' & CLIP(p_web.GSV('job:Surname'))
              END
          END
      END
  
      ia:CompanyName = p_web.GSV('job:Company_Name')
      ia:AddressLine1 = p_web.GSV('job:Address_Line1')
      ia:AddressLine2 = p_web.GSV('job:Address_Line2')
      ia:AddressLine3 = p_web.GSV('job:Address_Line3')
      ia:AddressLine4 = p_web.GSV('job:Postcode')
      ia:TelephoneNumber = p_web.GSV('job:Telephone_Number')
      ia:FaxNumber = p_web.GSV('job:Fax_Number')
  END
  
  da:CompanyName = p_web.GSV('job:Company_Name_Delivery')
  da:AddressLine1 = p_web.GSV('job:Address_Line1_Delivery')
  da:AddressLine2 = p_web.GSV('job:Address_Line2_Delivery')
  da:AddressLine3 = p_web.GSV('job:Address_Line3_Delivery')
  da:AddressLine4 = p_web.GSV('job:Postcode_Delivery')
  da:TelephoneNumber = p_web.GSV('job:Telephone_Delivery')
  
  IF (glo:WebJob <> 1)
      IF (p_web.GSV('jobe:WebJob') = 1)
          ! RRC to ARC job.
          Access:TRADEACC.Clearkey(tra:Account_Number_Key)
          tra:Account_Number = p_web.GSV('wob:HeadAccountNumber')
          IF (Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign)
              da:CompanyName  = tra:Company_Name
              da:AddressLine1 = tra:Address_Line1
              da:AddressLine2 = tra:Address_Line2
              da:AddressLine3 = tra:Address_Line3
              da:AddressLine4 = tra:Postcode
              da:TelephoneNumber = tra:Telephone_Number
  
              ia:CompanyName  = tra:Company_Name
              ia:AddressLine1 = tra:Address_Line1
              ia:AddressLine2 = tra:Address_Line2
              ia:AddressLine3 = tra:Address_Line3
              ia:AddressLine4 = tra:Postcode
              ia:TelephoneNumber = tra:Telephone_Number
              ia:VatNumber = tra:Vat_Number
  
          END
      ELSE ! ! IF (p_web.GSV('jobe:WebJob = 1)
  
  
  
  
      END ! IF (p_web.GSV('jobe:WebJob = 1)
  
  END ! IF (glo:WebJob = 1)
  
  
  ! Lookups
  ! OutFault
  locMainOutFault = ''
  ! #12091 Display outfaults list like the old A5 invoice. (Bryan: 09/05/2011)
  line# = 0
  Access:JOBOUTFL.Clearkey(joo:JobNumberKey)
  joo:JobNumber = job:Ref_Number
  Set(joo:JobNumberKey,joo:JobNumberKey)
  Loop ! Begin Loop
      If Access:JOBOUTFL.Next()
          Break
      End ! If Access:JOBOUTFL.Next()
      If joo:JobNumber <> job:Ref_Number
          Break
      End ! If joo:JobNumber <> job:Ref_Number
      If Instring('IMEI VALIDATION',joo:Description,1,1)
          Cycle
      End ! If Instring('IMEI VALIDATION',joo:Description,1,1)
      line# += 1
      locMainOutFault = Clip(locMainoutfault) & '<13,10>' & Format(line#,@n2) & ' - ' & Clip(joo:Description)
  End ! Loop
  locMainOutFault = CLIP(Sub(locMainOutFault,3,1000))
  
  
  !Costs
  
  
  If (p_Web.GSV('BookingSite') = 'RRC')
      tmp:LabourCost = p_web.GSV('jobe:InvRRCCLabourCost')
      tmp:PartsCost = p_web.GSV('jobe:InvRRCCPartsCost')
      tmp:CourierCost = p_web.GSV('job:Invoice_Courier_Cost')
  
      vat_temp = (p_web.GSV('jobe:InvRRCCLabourCost') * inv:RRCVatRateLabour/100) + |
          (p_web.GSV('jobe:InvRRCCPartsCost') * inv:RRCVatRateParts/100) + |
          (p_web.GSV('job:Invoice_Courier_Cost') * inv:RRCVatRateLabour/100)
      total_temp = p_web.GSV('jobe:InvRRCCLabourCost') + |
          p_web.GSV('jobe:InvRRCCPartsCost') + |
          p_web.GSV('job:Invoice_Courier_Cost') + vat_temp
  ELSE
      tmp:LabourCost = p_web.GSV('job:Invoice_Labour_Cost')
      tmp:PartsCost = p_web.GSV('job:Invoice_Parts_Cost')
      tmp:CourierCost = p_web.GSV('job:Invoice_Courier_Cost')
      vat_temp = (p_web.GSV('jobe:InvRRCCLabourCost') * inv:RRCVatRateLabour/100) + |
          (p_web.GSV('jobe:InvRRCCPartsCost') * inv:RRCVatRateParts/100) + |
          (p_web.GSV('job:Invoice_Courier_Cost') * inv:RRCVatRateLabour/100)
      total_temp = p_web.GSV('jobe:InvRRCCLabourCost') + |
          p_web.GSV('jobe:InvRRCCPartsCost') + |
          p_web.GSV('job:Invoice_Courier_Cost') + vat_temp
  END
  Sub_Total_Temp = tmp:LabourCost + tmp:PartsCost + tmp:CourierCost
  
  
  ! Find Date In/Out
  
      locDateIn = job:Date_Booked
      locTimeIn = job:Time_Booked
      ! #11881 Use booking date unless RRC job AT ARC. Then use the "date received at ARC".
      ! (Note: Vodacom didn't specifically mention this, but i've assumed
      ! for VCP jobs to use the "received at RRC" date, rather than the booking date (Bryan: 16/03/2011)
      If (p_web.GSV('BookingSite') = 'RRC')
          IF (p_web.GSV('job:Who_Booked') = 'WEB')
              Access:AUDIT.Clearkey(aud:TypeActionKey)
              aud:Ref_Number = p_web.GSV('job:Ref_Number')
              aud:Type = 'JOB'
              aud:Action = 'UNIT RECEIVED AT RRC FROM PUP'
              SET(aud:TypeActionKey,aud:TypeActionKey)
              LOOP UNTIL Access:AUDIT.Next()
                  IF (aud:Ref_Number <> p_web.GSV('job:Ref_Number') OR |
                      aud:Type <> 'JOB' OR |
                      aud:Action <> 'UNIT RECEIVED AT RRC FROM PUP')
                      BREAK
                  END
                  locDateIn = aud:Date
                  locTimeIn = aud:Time
                  BREAK
              END
  
          END ! IF (job:Who_Booked = 'WEB')
  
      ELSE ! If (glo:WebJob = 1)
          IF (p_web.GSV('jobe:WebJob') = 1)
              ! RRC Job
              foundDateIn# = 0
              Access:AUDSTATS.Clearkey(aus:DateChangedKey)
              aus:RefNumber = p_web.GSV('job:Ref_Number')
              aus:Type = 'JOB'
              SET(aus:DateChangedKey,aus:DateChangedKey)
              LOOP UNTIL Access:AUDSTATS.Next()
                  IF (aus:RefNumber <> p_web.GSV('job:Ref_Number') OR |
                      aus:TYPE <> 'JOB')
                      BREAK
                  END
                  IF (SUB(UPPER(aus:NewStatus),1,3) = '452')
                      locDateIn = aus:DateChanged
                      locTimeIn = aus:TimeChanged
                      foundDateIn# = 1
                      BREAK
                  END
              END
              IF (foundDateIn# = 0)
                  Access:LOCATLOG.Clearkey(lot:DateKey)
                  lot:RefNumber = p_web.GSV('job:Ref_Number')
                  SET(lot:DateKey,lot:DateKey)
                  LOOP UNTIL Access:LOCATLOG.Next()
                      IF (lot:RefNumber <> p_web.GSV('job:Ref_Number'))
                          BREAK
                      END
                      IF (lot:NewLocation = Clip(GETINI('RRC','ARCLocation',,CLIP(PATH())&'\SB2KDEF.INI')))
                          locDateIn = lot:TheDate
                          locTimeIn = lot:TheTime
                          BREAK
                      END
                  END
              END
          END
      END ! If (glo:WebJob = 1)
  
  
      ! Use despatch to customer date (use last occurance), unless ARC to RRC job
      locDateOut = 0
      locTimeOut = 0
      Access:LOCATLOG.Clearkey(lot:DateKey)
      lot:RefNumber = p_web.GSV('job:Ref_Number')
      SET(lot:DateKey,lot:DateKey)
      LOOP UNTIL Access:LOCATLOG.Next()
          IF (lot:RefNumber <> p_web.GSV('job:Ref_Number'))
              BREAK
          END
          IF (lot:NewLocation = Clip(GETINI('RRC','DespatchToCustomer',,CLIP(PATH())&'\SB2KDEF.INI')))
              locDateOut = lot:TheDate
              locTimeOut = lot:TheTime
  !            BREAK
          END
      END
  
      If (p_web.GSV('BookingSite') <> 'RRC')
          IF (p_web.GSV('jobe:WebJob') = 1)
              ! RRC Job
              Access:LOCATLOG.Clearkey(lot:DateKey)
              lot:RefNumber = p_web.GSV('job:Ref_Number')
              SET(lot:DateKey,lot:DateKey)
              LOOP UNTIL Access:LOCATLOG.Next()
                  IF (lot:RefNumber <> p_web.GSV('job:Ref_Number'))
                      BREAK
                  END
                  IF (lot:NewLocation = Clip(GETINI('RRC','InTransitRRC',,CLIP(PATH())&'\SB2KDEF.INI')))
                      locDateOut = lot:TheDate
                      locTimeOut = lot:TheTime
                      BREAK
                  END
              END
          END
      END ! If (glo:WebJob = 1)
  Report{PROPPRINT:Extend}=True
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='VodacomSingleInvoice'
  END
  IF PreviewReq = True OR (Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    Report{Prop:Preview} = PrintPreviewQueue.PrintPreviewImage
  END
  LocalAttribute.Init(Report)
  Do SetStaticControlsAttributes
  



CreateARCRRCInvoices        Routine
    If (p_web.GSV('BookingSite') = 'RRC')
        IF (inv:RRCInvoiceDate = 0)
            ! RRC Invoice has not been created
            inv:RRCInvoiceDate = TODAY()
            IF (inv:ExportedRRCOracle = 0)
                p_web.SSV('jobe:InvRRCCLabourCost',p_web.GSV('jobe:RRCCLabourCost'))
                p_web.SSV('jobe:InvRRCCPartsCost',p_web.GSV('jobe:RRCCPartsCost'))
                p_web.SSV('jobe:InvRRCCSubTotal',p_web.GSV('jobe:RRCCSubTotal'))
                p_web.SSV('jobe:InvoiceHandlingFee',p_web.GSV('jobe:HandlingFee'))
                p_web.SSV('jobe:InvoiceExchangeRate',p_web.GSV('jobe:ExchangeRate'))
                Access:JOBSE.Clearkey(jobe:RefNumberKey)
                jobe:RefNumber = p_web.GSV('jobe:RefNumber')
                IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
                    p_web.SessionQueueToFile(JOBSE)
                    Access:JOBSE.TryUpdate()

                END

                inv:ExportedRRCOracle = 1
                Access:INVOICE.TryUpdate()

                Line500_XML('RIV')
            END

            foundInvoiceCreated# = 0
            Access:AUDIT.Clearkey(aud:Action_Key)
            aud:Ref_Number = p_web.GSV('job:Ref_Number')
            aud:Action = 'INVOICE CREATED'
            SET(aud:Action_Key,aud:Action_Key)
            LOOP UNTIL Access:AUDIT.Next()
                IF (aud:Ref_Number <> p_web.GSV('job:Ref_Number') OR |
                    aud:Action <> 'INVOICE CREATED')
                    BREAK
                END
                IF (INSTRING('INVOICE NUMBER: ' & CLIP(inv:Invoice_Number) & '-' & |
                    CLIP(tra:BranchIdentification),1,1))
                    foundInvoiceCreated# = 1
                    BREAK
                END
            END

            IF (foundInvoiceCreated# = 0)
                locAuditNotes = 'INVOICE NUMBER: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification)
                LoanAttachedToJob(p_web)
                IF (p_web.GSV('LoanAttachedToJob') = 1)
                    locAuditNotes   = Clip(locAuditNotes) & '<13,10>LOST LOAN FEE: ' & Format(p_web.GSV('job:Invoice_Courier_Cost'),@n14.2)
                END
                IF (p_web.GSV('jobe:Engineer48HourOption') = 1)
                    IF (p_web.GSV('jobe:ExcReplcamentCharge') = 1)
                        locAuditNotes   = Clip(locAuditNotes) & '<13,10>EXC REPL: ' & Format(p_web.GSV('job:Invoice_Courier_Cost'),@n14.2)
                    END
                END
                locAuditNotes   = Clip(locAuditNotes) & '<13,10>PARTS: ' & Format(p_web.GSV('jobe:InvRRCCPartsCost'),@n14.2)
                locAuditNotes   = Clip(locAuditNotes) & '<13,10>LABOUR: ' & Format(p_web.GSV('jobe:InvRRCCLabourCost'),@n14.2)
                locAuditNotes   = Clip(locAuditNotes) & '<13,10>SUB TOTAL: ' & Format(p_web.GSV('jobe:InvRRCCSubTotal'),@n14.2)
                locAuditNotes   = Clip(locAuditNotes) & '<13,10>VAT: ' & Format((p_web.GSV('jobe:InvRRCCPartsCOst') * inv:RRCVatRateParts/100) + |
                    (p_web.GSV('jobe:InvRRCCLabourCost') * inv:RRCVatRateLabour/100) + |
                    (p_web.GSV('job:Invoice_Courier_Cost') * inv:RRCVatRateLabour/100),@n14.2)
                locAuditNotes   = Clip(locAuditNotes) & '<13,10>TOTAL: ' & Format(p_web.GSV('jobe:InvRRCCSubTotal') + (p_web.GSV('jobe:InvRRCCPartsCost') * inv:RRCVatRateParts/100) + |
                    (p_web.GSV('jobe:InvRRCCLabourCost') * inv:RRCVatRateLabour/100) + |
                    (p_web.GSV('job:Invoice_Courier_Cost') * inv:RRCVatRateLabour/100),@n14.2)

                p_web.SSV('AddToAudit:Type','JOB')
                p_web.SSV('AddToAudit:Action','INVOICE CREATED')
                p_web.SSV('AddToAudit:Notes',CLIP(locAuditNotes))
                AddToAudit(p_web)
            END
            p_web.SSV('AddToAudit:Type','JOB')
            p_web.SSV('AddToAudit:Action','INVOICE PRINTED')
            p_web.SSV('AddToAudit:Notes','INVOICE NUMBER: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification) & |
                '<13,10>INVOICE DATE: ' & FOrmat(inv:RRCInvoiceDate,@d06))
            AddToAudit(p_web)
        ELSE


        END
        p_web.SSV('AddToAudit:Type','JOB')
        p_web.SSV('AddToAudit:Action','INVOICE PRINTED')
        p_web.SSV('AddToAudit:Notes','INVOICE NUMBER: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification) & |
            '<13,10>INVOICE DATE: ' & FOrmat(inv:RRCInvoiceDate,@d06))
        AddToAudit(p_web)
        locInvoiceDate = inv:RRCInvoiceDate
    ELSE ! If (glo:WebJob = 1)
        IF (inv:ARCInvoiceDate = 0)
            inv:ARCInvoiceDate = TODAY()
            p_web.SSV('job:Invoice_Labour_Cost',p_web.GSV('job:Labour_Cost'))
            p_web.SSV('job:Invoice_Courier_Cost',p_web.GSV('job:Courier_Cost'))
            p_web.SSV('job:Invoice_Parts_Cost',p_web.GSV('job:Parts_Cost'))
            p_web.SSV('job:Invoice_Sub_Total',p_web.GSV('job:Invoice_Labour_Cost') + |
                p_web.GSV('job:Invoice_Parts_Cost') + |
                p_web.GSV('job:Invoice_Courier_Cost'))
            Access:JOBS.ClearKey(job:Ref_Number_Key)
            job:Ref_Number = p_web.GSV('job:Ref_Number')
            IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                p_web.SessionQueueToFile(JOBS)
                Access:JOBS.TryUpdate()
            END



            inv:ExportedARCOracle = 1
            Access:INVOICe.TryUpdate()


            Line500_XML('AOW')

            foundInvoiceCreated# = 0
            Access:AUDIT.Clearkey(aud:Action_Key)
            aud:Ref_Number = p_web.GSV('job:Ref_Number')
            aud:Action = 'INVOICE CREATED'
            SET(aud:Action_Key,aud:Action_Key)
            LOOP UNTIL Access:AUDIT.Next()
                IF (aud:Ref_Number <> p_web.GSV('job:Ref_Number') OR |
                    aud:Action <> 'INVOICE CREATED')
                    BREAK
                END
                IF (INSTRING('INVOICE NUMBER: ' & CLIP(inv:Invoice_Number) & '-' & |
                    CLIP(tra:BranchIdentification),1,1))
                    foundInvoiceCreated# = 1
                    BREAK
                END
            END

            IF (foundInvoiceCreated# = 0)
                locAuditNotes   = 'INVOICE NUMBER: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification)
                LoanAttachedToJob(p_web)
                If (p_web.GSV('LoanAttachedToJob') = 1)
                    ! Changing (DBH 27/06/2006) #7584 - If loan attached, then invoice number is not being written to the audit trail
                    ! locAuditNotes   = '<13,10>LOST LOAN FEE: ' & Format(p_web.GSV('job:Invoice_Courier_Cost,@n14.2)
                    ! to (DBH 27/06/2006) #7584
                    locAuditNotes   = Clip(locAuditNotes) & '<13,10>LOST LOAN FEE: ' & Format(p_web.GSV('job:Invoice_Courier_Cost'),@n14.2)
                    ! End (DBH 27/06/2006) #7584

                End !If LoanAttachedToJob(p_web.GSV('job:Ref_Number)
                If p_web.GSV('jobe:Engineer48HourOption') = 1
                    If p_web.GSV('jobe:ExcReplcamentCharge') = 1
                        locAuditNotes   = Clip(locAuditNotes) & '<13,10>EXC REPL: ' & Format(p_web.GSV('job:Invoice_Courier_Cost'),@n14.2)
                    End !If p_web.GSV('jobe:ExcReplcamentCharge
                End !If p_web.GSV('jobe:Engineer48HourOption = 1
                locAuditNotes   = Clip(locAuditNotes) & '<13,10>PARTS: ' & Format(p_web.GSV('job:Invoice_Parts_Cost'),@n14.2)
                locAuditNotes   = Clip(locAuditNotes) & '<13,10>LABOUR: ' & Format(p_web.GSV('job:Invoice_Labour_Cost'),@n14.2)
                locAuditNotes   = Clip(locAuditNotes) & '<13,10>SUB TOTAL: ' & Format(p_web.GSV('job:Invoice_Sub_Total'),@n14.2)
                locAuditNotes   = Clip(locAuditNotes) & '<13,10>VAT: ' & Format((p_web.GSV('job:Invoice_Parts_Cost') * inv:Vat_Rate_Parts/100) + |
                    (p_web.GSV('job:Invoice_Labour_Cost') * inv:Vat_Rate_Labour/100) + |
                    (p_web.GSV('job:Invoice_Courier_Cost') * inv:Vat_Rate_Labour/100),@n14.2)
                locAuditNotes   = Clip(locAuditNotes) & '<13,10>TOTAL: ' & Format(p_web.GSV('job:Invoice_Sub_Total') + (p_web.GSV('job:Invoice_Parts_Cost') * inv:Vat_Rate_Parts/100) + |
                    (p_web.GSV('job:Invoice_Labour_Cost') * inv:Vat_Rate_Labour/100) + |
                    (p_web.GSV('job:Invoice_Courier_Cost') * inv:Vat_Rate_Labour/100),@n14.2)
                p_web.SSV('AddToAudit:Type','JOB')
                p_web.SSV('AddToAudit:Action','INVOICE CREATED')
                p_web.SSV('AddToAudit:Notes',CLIP(locAuditNotes))
                AddToAudit(p_web)
            END
            p_web.SSV('AddToAudit:Type','JOB')
            p_web.SSV('AddToAudit:Action','INVOICE PRINTED')
            p_web.SSV('AddToAudit:Notes','INVOICE NUMBER: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification) & |
                '<13,10>INVOICE DATE: ' & FOrmat(inv:ARCInvoiceDate,@d06))
            AddToAudit(p_web)

        ELSE ! IF (inv:ARCInvoiceDate = 0)

        END ! IF (inv:ARCInvoiceDate = 0)
        p_web.SSV('AddToAudit:Type','JOB')
        p_web.SSV('AddToAudit:Action','INVOICE PRINTED')
        p_web.SSV('AddToAudit:Notes','INVOICE NUMBER: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification) & |
            '<13,10>INVOICE DATE: ' & FOrmat(inv:ARCInvoiceDate,@d06))
        AddToAudit(p_web)
        locInvoiceDate = inv:ARCInvoiceDate
    END !If (glo:WebJob = 1)
PrintParts          ROUTINE
DATA
PartsAttached       BYTE(0)
CODE

    Access:PARTS.ClearKey(par:Part_Number_Key)
    par:Ref_Number = p_web.GSV('job:Ref_Number')
    SET(par:Part_Number_Key,par:Part_Number_Key)
    LOOP UNTIL Access:PARTS.Next()
        IF (par:Ref_Number <> p_web.GSV('job:Ref_Number'))
            BREAK
        END

        part_number_temp = par:Part_Number
        line_cost_temp = par:Quantity * par:Sale_Cost
        Print(rpt:Detail)
        PartsAttached = 1
    END
    IF (PartsAttached = 0)
        part_number_temp = 'No Parts Attached'
        Print(rpt:Detail)
    END
GenerateSVReportOutput        ROUTINE
  IF NOT LocalReportTarget &= NULL THEN
    IF RECORDS(LOC:SaveToQueue) THEN
      IF LocalReportTarget.SupportResultQueue()=True THEN
        LocalReportTarget.SetResultQueue(LocalOutputFileQueue)
      END
       ! The false parameter indicates that the AskProperties must ask for a name
       ! only if the target name is blank
      IF LocalReportTarget.AskProperties(False)=Level:Benign THEN
        LocalWMFParser.Init(LOC:SaveToQueue, LocalReportTarget)
        IF LocalWMFParser.GenerateReport()=Level:Benign THEN
          IF LocalReportTarget.SupportResultQueue()=True THEN
            Do ProcessOutputFileQueue
          END
        END
      END
    END
  ELSE
    FREE(LocalOutputFileQueue)
    LOOP PreviewQueueIndex=1 TO RECORDS(LOC:SaveToQueue)
      GET(LOC:SaveToQueue,PreviewQueueIndex)
      IF NOT ERRORCODE() THEN
        LocalOutputFileQueue.FileName = LOC:SaveToQueue.FileName
        ADD(LocalOutputFileQueue)
      END
    END
    Do ProcessOutputFileQueue
    FREE(LocalOutputFileQueue)
  END

ProcessOutputFileQueue          ROUTINE

SetStaticControlsAttributes     ROUTINE

SetDynamicControlsAttributes    ROUTINE


BindFileFields ROUTINE
      BIND('locInvoiceNumber',locInvoiceNumber)
  BIND(aud:RECORD)
  BIND(aus:RECORD)
  BIND(de2:RECORD)
  BIND(def:RECORD)
  BIND(dep:RECORD)
  BIND(inv:RECORD)
  BIND(jac:RECORD)
  BIND(jbn:RECORD)
  BIND(joo:RECORD)
  BIND(jpt:RECORD)
  BIND(job:RECORD)
  BIND(jobe:RECORD)
  BIND(jov:RECORD)
  BIND(lot:RECORD)
  BIND(mfo:RECORD)
  BIND(maf:RECORD)
  BIND(par:RECORD)
  BIND(stt:RECORD)
  BIND(sub:RECORD)
  BIND(tra:RECORD)
  BIND(use:RECORD)
  BIND(wob:RECORD)
UnBindFields ROUTINE





PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','WebServer_Phase4','VodacomSingleInvoice','VodacomSingleInvoice','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.CompressText   = True
  SELF.CompressImages = True
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    SELF.SetFileName(clip(loc:PDFName))
  End

InvoiceNote PROCEDURE (<NetWebServerWorker p_web>)         ! Generated from procedure template - Report

  ! The NetTalk Extension to report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
Loc:Html      String(1024)
Progress:Thermometer BYTE                                  !
locJobNumber         LONG                                  !
locDetail            STRING(10000)                         !
locCompanyName       STRING(30)                            !
locAddressLine1      STRING(30)                            !
locAddressLine2      STRING(30)                            !
locAddressLine3      STRING(30)                            !
locPostcode          STRING(30)                            !
locTelephoneNumber   STRING(30)                            !
locCustCompanyName   STRING(30)                            !
locCustAddressLine1  STRING(30)                            !
locCustAddressLine2  STRING(30)                            !
locCustAddressLine3  STRING(30)                            !
locCustPostcode      STRING(30)                            !
locCustTelephoneNumber STRING(30)                          !
locCustVatNumber     STRING(30)                            !
locSubTotal          REAL                                  !
locVat               REAL                                  !
locTotal             REAL                                  !
locInvoiceTime       TIME                                  !
locInvoiceDate       DATE                                  !
Process:View         VIEW(JOBS)
                       PROJECT(job:Ref_Number)
                     END
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),CENTER,TIMER(1),GRAY,DOUBLE
                       PROGRESS,USE(Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

Report               REPORT,AT(0,0,8430,8230),PAPER(PAPER:USER,8430,8230),PRE(RPT),FONT('Arial',10,,FONT:regular,CHARSET:ANSI),LANDSCAPE,THOUS
Detail                 DETAIL,AT(,,8430,8230),USE(?Detail),ABSOLUTE
                         TEXT,AT(375,-10,7656,7800),USE(locDetail),FONT('Courier New',11,,)
                       END
ANewPage               DETAIL,PAGEAFTER(1),AT(,,8427,83),USE(?ANewPage)
                       END
                     END
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeNoRecords          PROCEDURE(),DERIVED                 ! Method added to host embed code
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

ProgressMgr          StepLongClass                         ! Progress Manager
Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED                 ! Method added to host embed code
                     END

                    MAP
addToDetail             PROCEDURE(<STRING textString>)
newLine                 PROCEDURE(<LONG number>)
PrintReport             PROCEDURE(BYTE fCredit)
CheckCreateInvoiceRRC   PROCEDURE(BYTE fCredit)
CheckCreateInvoiceARC   PROCEDURE(BYTE fCredit)
                    END

Detail_Queue        Queue,Pre(detail)
Line                    String(56)
Filler1                 String(4)
Amount                  String(20)
                    End ! Detail_Queue        Queue,Pre(detail)

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
CreatePartsList     Routine
    Data
local:FaultDescription  String(255)
    Code
        Free(Detail_Queue)
        Clear(Detail_Queue)

        local:FaultDescription = Clip(BHStripReplace(p_web.GSV('jbn:Fault_Description'),'<13,10>','. '))

        If Clip(local:FaultDescription) <> ''
            detail:Line = Clip(Sub(local:FaultDescription,1,55))
            detail:Amount = ''
            Add(Detail_Queue)
            detail:Line = Clip(Sub(local:FaultDescription,56,55))
            detail:Amount = ''
            Add(Detail_Queue)
            If p_web.GSV('jobe2:InvDiscountAmnt') <> 0 then
                detail:Line = 'OOW Discount ' & Format(p_web.GSV('jobe2:InvDiscountAmnt'),@n_10.2)
                detail:Amount = ''
                Add(Detail_Queue)
            Else
                detail:Line = ''
                detail:Amount = ''
                Add(Detail_Queue)
            End
        Else ! If Clip(jbn:Fault_Description) <> ''
            detail:Line = ''
            detail:Amount = ''
            Add(Detail_Queue)
            detail:Line = ''
            detail:Amount = ''
            Add(Detail_Queue)
            If p_web.GSV('jobe2:InvDiscountAmnt') <> 0 then
                detail:Line = 'OOW Discount ' & Format(p_web.GSV('jobe2:InvDiscountAmnt'),@n_10.2)
                detail:Amount = ''
                Add(Detail_Queue)
            Else
                detail:Line = ''
                detail:Amount = ''
                Add(Detail_Queue)
            End
        End ! If Clip(jbn:Fault_Description) <> ''

        detail:Line = ALL('-',55)
        detail:Amount = ''
        Add(Detail_Queue)

    ! Show Parts (DBH: 27/07/2007)
        RowNum# = 0
        Access:PARTS.Clearkey(par:Order_Number_Key)
        par:Ref_Number = p_web.GSV('job:Ref_Number')
        Set(par:Order_Number_Key,par:Order_Number_Key)
        Loop ! Begin Loop
            If Access:PARTS.Next()
                Break
            End ! If Access:PARTS.Next()
            If par:Ref_Number <> p_web.GSV('job:Ref_Number')
                Break
            End ! If par:Ref_Number <> job:Ref_Number
            RowNum# += 1
            detail:Line = ' ' & Format(RowNum#,@n2) & ' ' & Sub(par:Description,1,50)
            detail:Amount = ''
            Add(Detail_Queue)
        End ! Loop

    ! Show Fault Codes (DBH: 27/07/2007)
        Access:JOBOUTFL.Clearkey(joo:JobNumberKey)
        joo:JobNumber = p_web.GSV('job:Ref_Number')
        Set(joo:JobNumberKey,joo:JobNumberKey)
        Loop ! Begin Loop
            If Access:JOBOUTFL.Next()
                Break
            End ! If Access:JOBOUTFL.Next()
            If joo:JobNumber <> p_web.GSV('job:Ref_Number')
                Break
            End ! If joo:JobNumber <> job:Ref_Number
            If Instring('IMEI VALIDATION',joo:Description,1,1)
                Cycle
            End ! If Instring('IMEI VALIDATION',joo:Description,1,1)
            RowNum# += 1
            detail:Line = ' ' & Format(RowNum#,@n2) & ' ' & Sub(joo:Description,1,50)
            detail:Amount = ''
            Add(Detail_Queue)
        End ! Loop

!    ! I guess this is filling in the blank lines (DBH: 27/07/2007)
!        detail:Line = ''
!        detail:Amount = ''
!        Loop
!            If Records(Detail_Queue) % prn:DetailRows = 0
!                Break
!            End ! If Records(Detail_Queue) % prn:DetailRows = 0
!            Add(Detail_Queue)
!        End ! Loop

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !Print A5 Invoice
  IF (GETINI('PRINTING','PrintA5Invoice',,CLIP(PATH())&'\SB2KDEF.INI') <> 1)
      VodacomSingleInvoice(p_web)
      RETURN RequestCompleted
  END
  GlobalErrors.SetProcedureName('InvoiceNote')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  locJobNumber = p_web.GSV('job:Ref_Number')
  
  Relate:AUDIT.SetOpenRelated()
  Relate:AUDIT.Open                                        ! File AUDIT used by this procedure, so make sure it's RelationManager is open
  Access:JOBSE2.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBOUTFL.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:INVOICE.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:WARPARTS.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:PARTS.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:SUBTRACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TRADEACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('InvoiceNote',ProgressWindow)               ! Restore window settings from non-volatile store
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ProgressWindow{Prop:Timer} = 10                          ! Assign timer interval
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:JOBS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, job:Ref_Number)
  ThisReport.AddSortOrder(job:Ref_Number_Key)
  ThisReport.AddRange(job:Ref_Number,locJobNumber)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{Prop:Text}=''
  Relate:JOBS.SetQuickScan(1,Propagate:OneMany)
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom=True
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
      SELF.SetReportTarget(PDFReporter.IReportGenerator)
    self.SkipPreview = True
    ProgressWindow{prop:hide} = 1
    loc:PDFName = clip(p_web.site.WebFolderPath) & '\reports\$$$' & format(random(1,99999),@n05) &'.pdf'
  End
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
  END
  IF SELF.Opened
    INIMgr.Update('InvoiceNote',ProgressWindow)            ! Save window data to non-volatile store
  END
  ProgressMgr.Kill()
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      Loc:html = '<script type="text/javascript">alert('''&clip('No Records')&''');top.close();</script>'
      p_web.ParseHTML(loc:html)
    Else
      p_web.ForceNoCache = 1
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(loc:PDFName)
    End
  End
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
    If Not p_Web &= NULL
      loc:NoRecords = 1
      Return
    End
  PARENT.TakeNoRecords


ThisWindow.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeRecord()
    If Not p_web &= Null
      p_web.NoOp()
    End
  RETURN ReturnValue

addToDetail         PROCEDURE(<STRING textString>)
    CODE
        locDetail = CLIP(locDetail) & textString

newLine             PROCEDURE(<LONG number>)
    CODE
        IF (number = 0)
            locDetail = CLIP(locDetail) & '<13,10>'
        ELSE
            LOOP xx# = 1 TO number
                locDetail = CLIP(locDetail) & '<13,10>'
            END
        END

CheckCreateInvoiceARC       Procedure(BYTE fCredit)
local:AuditNotes        String(255)
    Code
        If inv:ARCInvoiceDate = 0
        ! Create an RRC Invoice (DBH: 27/07/2007)
            inv:ARCInvoiceDate = Today()

            locInvoiceTime = Clock()

            p_web.SSV('job:Invoice_Labour_Cost',p_web.GSV('job:Labour_Cost'))
            p_web.SSV('job:Invoice_Courier_Cost',p_web.GSV('job:Courier_Cost'))
            p_web.SSV('job:Invoice_Parts_Cost',p_web.GSV('job:Parts_Cost'))
            p_web.SSV('job:Invoice_Sub_Total',p_web.GSV('job:Invoice_Labour_Cost') + p_web.GSV('job:Invoice_Parts_Cost') |
                + p_web.GSV('job:Invoice_Courier_Cost'))
            Access:JOBSE.ClearKey(jobe:RefNumberKey)
            jobe:RefNumber = p_web.GSV('jobe:RefNumber')
            IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
                p_web.SessionQueueToFile(JOBSE)
                Access:JOBSE.tryupdate()
            END


            inv:ExportedARCOracle = 1

            Access:INVOICE.Update()

            Line500_XML('AOW')

        ! Add Relevant Audit Entry (DBH: 27/07/2007)
            local:AuditNotes = 'INVOICE NUMBER: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification)
            LoanAttachedToJob(p_web)

            If p_web.GSV('LoanAttachedToJob') = 1
                local:AuditNotes = Clip(local:AuditNotes) & '<13,10>LOST LOAN FEE: ' & Format(p_web.GSV('job:Invoice_Courier_Cost'),@n14.2)
            End ! If LoanAttachedToJob(p_web.GSV('job:Ref_Number)

        ! Show replacement charge (DBH: 27/07/2007)
            If (p_web.GSV('jobe:Engineer48HourOption') = 1)
                If (p_web.GSV('jobe:ExcReplcamentCharge') = 1)
                    local:AuditNotes = Clip(local:AuditNotes) & '<13,10>EXC REPL: ' & Format(p_web.GSV('job:Invoice_Courier_Cost'),@n14.2)
                End ! If p_web.GSV('jobe:ExcReplcamentCharge
            End  ! If p_web.GSV('jobe:Engineer48HourOption = 1


            local:AuditNotes = Clip(local:AuditNotes) & '<13,10>PARTS: ' & Format(p_web.GSV('job:Invoice_Parts_Cost'),@n14.2) & |
                '<13,10>LABOUR: ' & Format(p_web.GSV('job:Invoice_Labour_Cost'),@n14.2) & |
                '<13,10>SUB TOTAL: ' & Format(p_web.GSV('job:Invoice_Sub_Total'),@n14.2) & |
                '<13,10>VAT: ' & Format((p_web.GSV('job:Invoice_Parts_Cost') * inv:Vat_Rate_Parts/100) + |
                (p_web.GSV('job:Invoice_Labour_Cost') * inv:Vat_Rate_Labour/100) + |
                (p_web.GSV('job:Invoice_Courier_Cost') * inv:Vat_Rate_Labour/100),@n14.2) & |
                '<13,10>TOTAL: ' & Format(p_web.GSV('job:Invoice_Sub_Total') + (p_web.GSV('job:Invoice_Parts_Cost') * inv:Vat_Rate_Parts/100) + |
                (p_web.GSV('job:Invoice_Labour_Cost') * inv:Vat_Rate_Labour/100) + |
                (p_web.GSV('job:Invoice_Courier_Cost') * inv:Vat_Rate_Labour/100),@n14.2)



            p_web.SSV('AddToAudit:Type','JOB')
            p_web.SSV('AddToAudit:Action','INVOICE CREATED')
            p_web.SSV('AddToAudit:Notes',Clip(local:AuditNotes))
            AddToAudit(p_web)
        Else ! If inv:RRCInvoiceDate = 0
        ! Reprint (DBH: 27/07/2007)
        ! Normal Invoice Reprint (DBH: 27/07/2007)
            p_web.SSV('AddToAudit:Type','JOB')
            p_web.SSV('AddToAudit:Action','INVOICE REPRINTED')
            p_web.SSV('AddToAudit:Notes','INVOICE NUMBER: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification) & |
                '<13,10>INVOICE DATE: ' & FOrmat(inv:ARCInvoiceDate,@d06) )
            AddToAudit(p_web)

            locInvoiceTime = p_web.GSV('job:Time_Booked')
            Access:AUDIT.Clearkey(aud:TypeActionKey)
            aud:Ref_Number = p_web.GSV('job:Ref_Number')
            aud:Type = 'JOB'
            aud:Action = 'INVOICE CREATED'
            aud:Date = inv:ARCInvoiceDate
            Set(aud:TypeActionKey,aud:TypeActionKey)
            Loop ! Begin Loop
                If Access:AUDIT.Next()
                    Break
                End ! If Access:AUDIT.Next()
                If aud:Ref_Number <> p_web.GSV('job:Ref_Number')
                    Break
                End ! If aud:Ref_Number <> p_web.GSV('job:Ref_Number
                If aud:Type <> 'JOB'
                    Break
                End ! If aud:Type <> 'JOB'
                If aud:Action <> 'INVOICE CREATED'
                    Break
                End ! If aud:Action <> 'INVOICE CREATED'
                If aud:Date <> inv:ARCInvoiceDate
                    Break
                End ! If aud:Date <> inv:RRCInvoiceDate
                locInvoiceTime = aud:Time
                Break
            End ! Loop
        End ! If inv:RRCInvoiceDate = 0

        locVAT = (p_web.GSV('job:Invoice_Parts_Cost') * inv:Vat_Rate_Parts/100) + |
            (p_web.GSV('job:Invoice_Labour_Cost') * inv:Vat_Rate_Labour/100) + |
            (p_web.GSV('job:Invoice_Courier_Cost') * inv:Vat_Rate_Labour/100)
        locSubTotal = p_web.GSV('job:Invoice_Sub_Total')
        locTotal = locSubTotal + locVAT

CheckCreateInvoiceRRC       Procedure(BYTE fCredit)
local:AuditNotes                String(255)
Code
    If inv:RRCInvoiceDate = 0
        ! Create an RRC Invoice (DBH: 27/07/2007)
        inv:RRCInvoiceDate = Today()

        locInvoiceTime = Clock()

        p_web.SSV('jobe:InvRRCCLabourCost',p_web.GSV('jobe:RRCCLabourCost'))
        p_web.SSV('jobe:InvRRCCPartsCost',p_web.GSV('jobe:RRCCPartsCost'))
        p_web.SSV('jobe:InvRRCCSubTotal',p_web.GSV('jobe:RRCCSubTotal'))
        p_web.SSV('jobe:InvoiceHandlingFee',p_web.GSV('jobe:HandlingFee'))
        p_web.SSV('jobe:InvoiceExchangeRate',p_web.GSV('jobe:ExchangeRate'))
        p_web.SSV('jobe2:InvDiscountAmnt',p_web.GSV('jobe2:JobDiscountAmnt'))

        Access:JOBSE.ClearKey(jobe:RefNumberKey)
        jobe:RefNumber = p_web.GSV('jobe:RefNumber')
        IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = level:Benign)
            p_web.SessionQueueToFile(JOBSE)
            Access:JOBSE.TryUpdate()
        END

        Access:JOBSE2.ClearKey(jobe2:RefNumberKey)
        jobe2:RefNumber = p_web.GSV('jobe2:RefNumber')
        IF (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
            p_web.SessionQueueToFile(JOBSE2)
            Access:JOBSE2.TryUpdate()
        END


        inv:ExportedRRCOracle = 1

        Access:INVOICE.Update()

!    If f:Suffix = '' And f:Prefix = ''
!            ! Don't export credit notes (DBH: 02/08/2007)
!        Line500_XML('RIV')
!    End ! If f:Suffix = '' And f:Prefix = ''

        ! Add Relevant Audit Entry (DBH: 27/07/2007)
        local:AuditNotes = 'INVOICE NUMBER: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification)

        LoanAttachedToJob(p_web)
        If (p_web.GSV('LoanAttachedToJob') = 1)
            local:AuditNotes = Clip(local:AuditNotes) & '<13,10>LOST LOAN FEE: ' & Format(p_web.GSV('job:Invoice_Courier_Cost'),@n14.2)
        End ! If LoanAttachedToJob(job:Ref_Number)

        ! Show replacement charge (DBH: 27/07/2007)
        If p_web.GSV('jobe:Engineer48HourOption') = 1
            If p_web.GSV('jobe:ExcReplcamentCharge') = 1
                local:AuditNotes = Clip(local:AuditNotes) & '<13,10>EXC REPL: ' & Format(p_web.GSV('job:Invoice_Courier_Cost'),@n14.2)
            End ! If jobe:ExcReplcamentCharge
        End ! If jobe:Engineer48HourOption = 1

        local:AuditNotes = Clip(local:AuditNotes) & '<13,10>PARTS: ' & Format(p_web.GSV('jobe:InvRRCCPartsCost'),@n14.2) & |
            '<13,10>LABOUR: ' & Format(p_web.GSV('jobe:InvRRCCLabourCost'),@n14.2) & |
            '<13,10>SUB TOTAL: ' & Format(p_web.GSV('jobe:InvRRCCSubTotal'),@n14.2) & |
            '<13,10>VAT: ' & Format((p_web.GSV('jobe:InvRRCCPartsCOst') * inv:RRCVatRateParts/100) + |
            (p_web.GSV('jobe:InvRRCCLabourCost') * inv:RRCVatRateLabour/100) + |
            (p_web.GSV('job:Invoice_Courier_Cost') * inv:RRCVatRateLabour/100),@n14.2) & |
            '<13,10>TOTAL: ' & Format(p_web.GSV('jobe:InvRRCCSubTotal') + (p_web.GSV('jobe:InvRRCCPartsCost') * inv:RRCVatRateParts/100) + |
            (p_web.GSV('jobe:InvRRCCLabourCost') * inv:RRCVatRateLabour/100) + |
            (p_web.GSV('job:Invoice_Courier_Cost') * inv:RRCVatRateLabour/100),@n14.2)

        p_web.SSV('AddToAudit:Type','JOB')
        p_web.SSV('AddToAudit:Notes',CLIP(local:AuditNotes))
        p_web.SSV('AddToAudit:Action','INVOICE CREATED')
        AddToAudit(p_web)

    Else ! If inv:RRCInvoiceDate = 0
        ! Reprint (DBH: 27/07/2007)

        IF (fCredit = 2)

            ! Credit Note (DBH: 27/07/2007)
            p_web.SSV('AddToAudit:Notes','CREDIT NOTE: ' & Clip('CN') & Clip(jov:InvoiceNumber) & '-' & Clip(tra:BranchIdentification) & Clip(p_web.GSV('InvoiceSuffix')) & |
                'AMOUNT: ' & Format(jov:CreditAmount,@n14.2))
            p_web.SSV('AddToAudit:Action','CREDIT NOTE PRINTED')
            p_web.SSV('AddToAudit:Type','JOB')
            AddToAudit(p_web)
        Else ! If f:Prefix <> ''
            If (fCredit = 1)
                ! Credit Invoice (DBH: 27/07/2007)
                p_web.SSV('AddToAudit:Notes','INVOICE NUMBER: ' & Clip(jov:InvoiceNumber) & '-' & Clip(tra:BranchIdentification) & Clip(p_web.GSV('InvoiceSuffix')) & |
                    'AMOUNT: ' & Format(jov:NewTotalCost,@n14.2))
                p_web.SSV('AddToAudit:Action','CREDITED INVOICE PRINTED')
                p_web.SSV('AddToAudit:Type','JOB')
                AddToAudit(p_web)
            Else ! If f:Suffix <> ''
                ! Normal Invoice Reprint (DBH: 27/07/2007)
                p_web.SSV('AddToAudit:Type','JOB')
                p_web.SSV('AddToAudit:Notes','INVOICE NUMBER: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification) & |
                    '<13,10>INVOICE DATE: ' & FOrmat(inv:RRCInvoiceDate,@d06) )
                p_web.SSV('AddToAudit:Action','INVOICE REPRINTED')
                AddToAudit(p_web)

                locInvoiceTime = p_web.GSV('job:Time_Booked')
                Access:AUDIT.Clearkey(aud:TypeActionKey)
                aud:Ref_Number = p_web.GSV('job:Ref_Number')
                aud:Type = 'JOB'
                aud:Action = 'INVOICE CREATED'
                aud:Date = inv:RRCInvoiceDate
                Set(aud:TypeActionKey,aud:TypeActionKey)
                Loop ! Begin Loop
                    If Access:AUDIT.Next()
                        Break
                    End ! If Access:AUDIT.Next()
                    If aud:Ref_Number <> p_web.GSV('job:Ref_Number')
                        Break
                    End ! If aud:Ref_Number <> job:Ref_Number
                    If aud:Type <> 'JOB'
                        Break
                    End ! If aud:Type <> 'JOB'
                    If aud:Action <> 'INVOICE CREATED'
                        Break
                    End ! If aud:Action <> 'INVOICE CREATED'
                    If aud:Date <> inv:RRCInvoiceDate
                        Break
                    End ! If aud:Date <> inv:RRCInvoiceDate
                    locInvoiceTime = aud:Time
                    Break
                End ! Loop


            End ! If f:Suffix <> ''
        End ! If f:Prefix <> ''
    End ! If inv:RRCInvoiceDate = 0

    locVAT = (p_web.GSV('jobe:InvRRCCLabourCost') * inv:RRCVatRateLabour/100) + |
        (p_web.GSV('jobe:InvRRCCPartsCost') * inv:RRCVatRateParts/100) + |
        (p_web.GSV('job:Invoice_Courier_Cost') * inv:RRCVatRateLabour/100)
    locSubTotal = p_web.GSV('jobe:InvRRCCLabourCost') + |
        p_web.GSV('jobe:InvRRCCPartsCost') + |
        p_web.GSV('job:Invoice_Courier_Cost')
    locTotal = locSubTotal + locVAT

    IF (fCredit = 1)
        locTotal = jov:NewTotalCost
    END
    IF (fCredit = 2)
        locTotal = -jov:CreditAmount
    END
PrintReport         procedure(BYTE fCredit)
CODE


    newLine(7)

    IF (p_web.GSV('BookingSite') = 'ARC')
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = p_web.GSV('ARC:AccountNumber')
        IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
        END
        locCompanyName = tra:Company_Name
        locAddressLine1 = tra:Address_Line1
        locADdressLine2 = tra:Address_Line2
        locAddressLine3 = tra:Address_Line3
        locPostcode = tra:Postcode
        locTelephoneNumber = tra:Telephone_Number

        locInvoiceDate = inv:ARCInvoiceDate
        CheckCreateInvoiceARC(fCredit)
    ELSE
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = p_web.GSV('wob:HeadAccountNumber')
        IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
        END
        locCompanyName = tra:Company_Name
        locAddressLine1 = tra:Address_Line1
        locADdressLine2 = tra:Address_Line2
        locAddressLine3 = tra:Address_Line3
        locPostcode = tra:Postcode
        locTelephoneNumber = tra:Telephone_Number

        locInvoiceDate = inv:RRCInvoiceDate
        CheckCreateInvoiceRRC(fCredit)
    END


    IF (p_web.GSV('jobe:WebJob') = 1)
        Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
        sub:Account_Number = p_web.GSV('job:Account_Number')
        IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key))
        END
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = sub:Main_Account_Number
        IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
        END
        IF (sub:OverrideHeadVATNo = TRUE)
            locCustVatNumber = sub:VAT_Number
        ELSE
            locCustVatNumber = tra:VAT_Number
        END

        IF (sub:Invoice_Customer_Address = 'YES')
            IF (p_web.GSV('job:Company_Name') = '')
                locCustCompanyName = p_web.GSV('job:Initial') & ' ' & p_web.GSV('job:Initial') & |
                    ' ' & p_web.GSV('job:Surname')
            ELSE
                locCustCompanyName = p_web.GSV('job:Company_Name')
            END
            locCustAddressLine1 = p_web.GSV('job:Address_Line1')
            locCustAddressLine2 = p_web.GSV('job:Address_Line2')
            locCustAddressLine3 = p_web.GSV('job:Address_Line3')
            locCustPostcode = p_web.GSV('job:Postcode')
            locCustTelephoneNumber = p_web.GSV('job:Telephone_Number')
        ELSE
            locCustCompanyName = sub:Company_Name
            locCustAddressLine1 = sub:Address_Line1
            locCustAddressLine2 = sub:Address_Line2
            locCustAddressLine3 = sub:Address_Line3
            locCustPostcode = sub:Postcode
            locCustTelephoneNumber = sub:Telephone_Number
        END
    END


    addToDetail(LEFT(locCustCOmpanyName,33) & locCompanyName)
    newLine()
    addToDetail(LEFT(locCustAddressLine1,33) & locAddressLine1)
    newLine()
    addToDetail(LEFT(locCustAddressLine2,33) & locAddressLine2)
    newLine()
    addToDetail(LEFT(locCustAddressLine3,33) & locAddressLine3)
    newLine()
    addToDetail(LEFT(locCustPostcode,33) & locPostcode)
    newLine()
    addToDetail(LEFT(locCustTelephoneNumber,33) & locTelephoneNumber)
    newLine()
    addToDetail('VAT NO.: ' & locCustVatNumber)
    newLine(2)

! Job Number
    Access:TRADEACC.ClearKey(tra:Account_Number_Key)
    tra:Account_Number = p_web.GSV('wob:HeadAccountNumber')
    IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
    END
    addToDetail(RIGHT('',33) & 'Job Number ' & p_web.GSV('job:Ref_Number') & |
        '-' & tra:BranchIdentification & p_web.GSV('wob:JobNumber'))
    newLine(5)

! Order Line

    IF (fCredit = 1)
        addToDetail(LEFT(FORMAT(locInvoiceDate,@d06),23) & |
            LEFT(CLIP(inv:Invoice_Number) & p_web.GSV('InvoiceSuffix'),13) & |
            LEFT(p_web.GSV('job:Account_Number'),15) & |
            LEFT(p_web.GSV('job:Order_Number'),9))
    ELSIF (fCredit = 2)
        addToDetail(LEFT(FORMAT(locInvoiceDate,@d06),23) & |
            LEFT('CN' & CLIP(inv:Invoice_Number) & p_web.GSV('CreditSuffix'),13) & |
            LEFT(p_web.GSV('job:Account_Number'),15) & |
            LEFT(p_web.GSV('job:Order_Number'),9))
    ELSE
        addToDetail(LEFT(FORMAT(locInvoiceDate,@d06),23) & |
            LEFT(inv:Invoice_Number,13) & |
            LEFT(p_web.GSV('job:Account_Number'),15) & |
            LEFT(p_web.GSV('job:Order_Number'),9))

    END


    newLine(5)

! IMEI LIne
    addToDetail(LEFT(p_web.GSV('job:Manufacturer'),16) & |
        LEFT(p_web.GSV('job:Model_Number'),17) & |
        LEFT(p_web.GSV('job:MSN'),25) & |
        LEFT(p_web.GSV('job:ESN'),20))

    newLine(2)

    Do CreatePartsList

    Loop x# = 1 To 14
        Get(Detail_Queue,x#)
        IF (ERROR())
            ERROR# = TRUE
        ELSE
            ERROR# = FALSE
        END
        detail:Amount = ''
        Case x#
        Of 1
            IF (p_web.GSV('BookingSite') = 'RRC')
                detail:Amount = (Format(p_web.GSV('jobe:invRRCCLabourCost'),@n-14.2))
            ELSE
                detail:Amount = (Format(p_web.GSV('job:Invoice_Labour_Cost'),@n-14.2))
            END

        Of 3
            IF (p_web.GSV('BookingSite') = 'RRC')
                detail:Amount = (Format(p_web.GSV('jobe:invRRCCPartsCost'),@n-14.2))
            ELSE
                detail:Amount = (Format(p_web.GSV('job:Invoice_Parts_Cost'),@n-14.2))
            END

        Of 5
            IF (p_web.GSV('BookingSite') = 'RRC')
                detail:Amount = (Format(p_web.GSV('job:invoice_Courier_Cost'),@n-14.2))
            ELSE
                detail:Amount = (Format(p_web.GSV('job:invoice_Courier_Cost'),@n-14.2))
            END

        Of 10
            detail:Amount = (Format(locSubTotal,@n-14.2))
        Of 12
            detail:Amount = (Format(locVAT,@n-14.2))
        Of 14
            detail:Amount = (Format(locTotal,@n-14.2))
        End ! Case x#

        IF (ERROR#)
            detail:Line = '-'
            Add(Detail_Queue)
        ELSE
            Put(Detail_Queue)
        END

    End ! Loop x# = 1 To Records(Detail_Queue)

    Get(Detail_Queue,1)

    Loop x# = 1 To 15
        Get(Detail_Queue,x#)
        IF ~(ERROR())
            addToDetail(LEFT(detail:Line,68) & |
                RIGHT(detail:Amount,10))
        END
        newLine()
    End ! Loop x# = 1 To Records(Detail_Queue)

! Two Dates
    addToDetail(LEFT('',10) & |
        LEFT(FORMAT(p_web.GSV('job:Date_Booked'),@d06),32) & |
        FORMAT(p_web.GSV('job:Time_Booked'),@t01))
    newLine(2)

    IF (fCredit <> 0)
        ! If credit use dates
        locInvoiceDate = jov:DateCreated
        locInvoiceTime = jov:TimeCreated
    END

    addToDetail(LEFT('',10) & |
        LEFT(FORMAT(locInvoiceDate,@d06),32) & |
        LEFT(FORMAT(locInvoiceTime,@t01),20) & |
        p_web.GSV('job:Engineer'))



ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ! Printing Details
  
  Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
  inv:Invoice_Number = p_web.GSV('job:Invoice_Number')
  IF (Access:INVOICE.TryFetch(inv:Invoice_Number_Key))
  END
  
  ! Is this a credit note?
  IF (p_web.GSV('CreditNoteCreated') = 1)
      Access:JOBSINV.ClearKey(jov:TypeSuffixKey)
      jov:RefNumber = p_web.GSV('job:ref_Number')
      jov:Type = 'C'
      jov:Suffix = p_web.GSV('CreditSuffix')
      IF (Access:JOBSINV.TryFetch(jov:TypeSuffixKey))
      END
      PrintReport(2)
      Print(rpt:Detail)
      Print(rpt:ANewPage)
      locDetail = ''
  
      Access:JOBSINV.ClearKey(jov:TypeSuffixKey)
      jov:RefNumber = p_web.GSV('job:ref_Number')
      jov:Type = 'I'
      jov:Suffix = p_web.GSV('InvoiceSuffix')
      IF (Access:JOBSINV.TryFetch(jov:TypeSuffixKey))
      END
      PrintReport(1)
      Print(rpt:Detail)
  
  
  
  
  ELSE
      PrintReport(0)
      Print(rpt:Detail)
  END
  
  
  
  ReturnValue = PARENT.TakeRecord()
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName('')
  SELF.SetDocumentInfo('CW Report','WebServer_Phase4','InvoiceNote','InvoiceNote','','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.CompressText   = True
  SELF.CompressImages = True
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    SELF.SetFileName(clip(loc:PDFName))
  End

Is48HourOrderProcessed PROCEDURE  (func:Location,func:JobNumber) ! Declare Procedure
EXCHOR48::State  USHORT
FilesOpened     BYTE(0)
  CODE
    Do OpenFiles
    Do SaveFiles

    Return# = 0
    Access:EXCHOR48.ClearKey(ex4:LocationJobKey)
    ex4:Received  = 1
    ex4:Returning = 0
    ex4:Location  = func:Location
    ex4:JobNumber = func:JobNumber
    Set(ex4:LocationJobKey,ex4:LocationJobKey)
    Loop
        If Access:EXCHOR48.NEXT()
           Break
        End !If
        If ex4:Received  <> 1      |
        Or ex4:Returning <> 0      |
        Or ex4:Location  <> func:Location      |
        Or ex4:JobNumber <> func:JobNumber      |
            Then Break.  ! End If
        If ex4:AttachedToJob = 0 And ex4:DateOrdered = 0
            Return# = 1
            Break
        End !If ex4:AttachedToJob = 0 And ex4:DateOrdered = 0
    End !Loop

    Do RestoreFiles
    Do CloseFiles

    Return Return#
!--------------------------------------
OpenFiles  ROUTINE
  Access:EXCHOR48.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:EXCHOR48.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:EXCHOR48.Close
     FilesOpened = False
  END
SaveFiles  ROUTINE
  EXCHOR48::State = Access:EXCHOR48.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF EXCHOR48::State <> 0
    Access:EXCHOR48.RestoreFile(EXCHOR48::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
Is48HourOrderCreated PROCEDURE  (func:Location,func:JobNumber) ! Declare Procedure
EXCHOR48::State  USHORT
FilesOpened     BYTE(0)
  CODE
    Do OpenFiles
    Do SaveFiles

    Return# = 0
    Access:EXCHOR48.ClearKey(ex4:LocationJobKey)
    ex4:Received  = 0
    ex4:Returning = 0
    ex4:Location  = func:Location
    ex4:JobNumber = func:JobNumber
    Set(ex4:LocationJobKey,ex4:LocationJobKey)
    Loop
        If Access:EXCHOR48.NEXT()
           Break
        End !If
        If ex4:Received  <> 0      |
        Or ex4:Returning <> 0      |
        Or ex4:Location  <> func:Location      |
        Or ex4:JobNumber <> func:JobNumber      |
            Then Break.  ! End If
        If ex4:AttachedToJob = 0 And ex4:DateOrdered = 0
            Return# = 1
            Break
        End !If ex4:AttachedToJob = 0 And ex4:DateOrdered = 0
    End !Loop

    Do RestoreFiles
    Do CloseFiles

    Return Return#


!--------------------------------------
OpenFiles  ROUTINE
  Access:EXCHOR48.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:EXCHOR48.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:EXCHOR48.Close
     FilesOpened = False
  END
SaveFiles  ROUTINE
  EXCHOR48::State = Access:EXCHOR48.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF EXCHOR48::State <> 0
    Access:EXCHOR48.RestoreFile(EXCHOR48::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
