

   MEMBER('WebServer_Phase4.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSE013.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSE012.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE060.INC'),ONCE        !Req'd for module callout resolution
                     END


createWebOrder       PROCEDURE  (STRING fAccountNumber,STRING fPartNumber,STRING fDescription,LONG fQuantity,REAL fRetailCost) ! Declare Procedure
locAccountNumber     STRING(30)                            !
TRADEACC::State  USHORT
ORDWEBPR::State  USHORT
FilesOpened     BYTE(0)
  CODE
    do openFiles
    do saveFiles

    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
    tra:Account_Number    = fAccountNumber
    if (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
        ! Found
        if (tra:StoresAccount <> '')
            locAccountNumber = tra:StoresAccount
        else ! if (tra:StoresAccount <> '')
            locAccountNumber = tra:Account_Number
        end ! if (tra:StoresAccount <> '')
    else ! if (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
        ! Error
    end ! if (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)

    found# = 0
    Access:ORDWEBPR.Clearkey(orw:PartNumberKey)
    orw:AccountNumber    = locAccountNumber
    orw:PartNumber    = fPartNumber
    set(orw:PartNumberKey,orw:PartNumberKey)
    loop
        if (Access:ORDWEBPR.Next())
            Break
        end ! if (Access:ORDWEBPR.Next())
        if (orw:AccountNumber    <> locAccountNumber)
            Break
        end ! if (orw:AccountNumber    <> locAccountNumber)
        if (orw:PartNumber    <> fPartNumber)
            Break
        end ! if (orw:PartNumber    <> fPartNumber)
        if (orw:Description = fDescription)
            orw:Quantity += fQuantity
            access:ORDWEBPR.tryUpdate()
            found# = 1
            break
        end ! if (orw:Description = fDescription)
    end ! loop

    if (found# = 0)
        if (Access:ORDWEBPR.PrimeRecord() = Level:Benign)
            orw:AccountNumber    = locAccountNumber
            orw:PartNumber    = fPartNumber
            orw:Quantity    = fQuantity
            orw:ItemCost    = fRetailCost
            orw:Description = fDescription
            if (Access:ORDWEBPR.TryInsert() = Level:Benign)
                ! Inserted
            else ! if (Access:ORDWEBPR.TryInsert() = Level:Benign)
                ! Error
                Access:ORDWEBPR.CancelAutoInc()
            end ! if (Access:ORDWEBPR.TryInsert() = Level:Benign)
        end ! if (Access:ORDWEBPR.PrimeRecord() = Level:Benign)
    end !if (found# = 0)

    do restoreFiles
    do closeFiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:TRADEACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:ORDWEBPR.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:ORDWEBPR.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:TRADEACC.Close
     Access:ORDWEBPR.Close
     FilesOpened = False
  END
SaveFiles  ROUTINE
  TRADEACC::State = Access:TRADEACC.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  ORDWEBPR::State = Access:ORDWEBPR.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF TRADEACC::State <> 0
    Access:TRADEACC.RestoreFile(TRADEACC::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF ORDWEBPR::State <> 0
    Access:ORDWEBPR.RestoreFile(ORDWEBPR::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
ConvertEstimateParts PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
locCreateWebOrder    BYTE                                  !
locPartNumber        LONG                                  !
locQuantity          LONG                                  !
local       class
AllocateExchangePart      Procedure(String    func:Type,Byte  func:Allocated)
            end
tmp:AddToStockAllocation Byte(0)
FilesOpened     BYTE(0)
  CODE
    do openFiles

    tmp:AddToStockAllocation = 0

    access:estparts.clearkey(epr:part_number_key)
    epr:ref_number  = p_web.GSV('job:ref_number')
    set(epr:part_number_key,epr:part_number_key)
    loop
        if access:estparts.next()
           break
        end !if
        if epr:ref_number  <> p_web.GSV('job:ref_number')      |
            then break.  ! end if
        yldcnt# += 1
        if yldcnt# > 25
           yield() ; yldcnt# = 0
        end !if

        Access:STOCK.ClearKey(sto:ref_number_key)
        sto:ref_number = epr:Part_Ref_Number
        if not Access:STOCK.Fetch(sto:ref_number_key)
            if sto:ExchangeUnit = 'YES'

                !Check for existing unit - then exchange
                !chargeable parts and partnumber = EXCH?
                access:parts.clearkey(par:Part_Number_Key)
                par:Ref_Number = p_web.GSV('job:ref_number')
                par:Part_Number = 'EXCH'
                if access:parts.fetch(par:Part_NUmber_Key) = level:benign
                    !found an existing Exchange unit
                ELSE

                    Access:USERS.Clearkey(use:User_Code_Key)
                    use:User_Code   = p_web.GSV('job:Engineer')
                    If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                        !Found
                        Access:STOCK.Clearkey(sto:Location_Key)
                        sto:Location    = use:Location
                        sto:Part_Number = 'EXCH'
                        If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                            access:location.clearkey(loc:Location_Key)
                            loc:Location = use:location
                            access:location.fetch(loc:Location_Key)
                            !changed 19/11 alway allocate at once
                            !change back 20/11
!                            if loc:UseRapidStock then
                                !Found
                                Local.AllocateExchangePart('CHA',0)
!                            ELSE
!                                ExchangeUnitNumber# = p_web.GSV('job:Exchange_Unit_Number')
!                                ViewExchangeUnit()
!                                Access:JOBS.TryUpdate()
!                                ! Inserting (DBH 16/09/2008) # 10253 - Update Date/Time Stamp
!                                UpdateDateTimeStamp(p_web.GSV('job:Ref_Number'))
!                                ! End (DBH 16/09/2008) #10253
!                                If p_web.GSV('job:Exchange_Unit_Number') <> ExchangeUnitNumber#
!                                    Local.AllocateExchangePart('CHA',1)
!                                End !If p_web.GSV('job:Exchange_Unit_Number <> ExchangeUnitNumber#
!                            END !if loc:useRapidStock
                        Else ! If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                            !Error
                        End !If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                    Else ! If Access:USERS.Tryfetch(use:User_Code_KEy) = Level:Benign
                        !Error
                    End !If Access:USERS.Tryfetch(use:User_Code_KEy) = Level:Benign

                    getStatus(108,0,'EXC',p_web)
                End !If exchang unit already existed

                !Remove the tick so that if the job is made "Unaccpeted"
                !then you won't have to remove these parts to stock twice
                If epr:UsedOnRepair
                    epr:UsedOnRepair = 0
                    Access:ESTPARTS.Update()
                End !If epr:UsedOnRepair
                cycle
            end
        end

        get(parts,0)
        glo:select1 = ''
        if access:parts.primerecord() = level:benign
            par:ref_number           = p_web.GSV('job:ref_number')
            par:adjustment           = epr:adjustment
            par:part_number     = epr:part_number
            par:description     = epr:description
            par:supplier        = epr:supplier
            par:purchase_cost   = epr:purchase_cost
            par:sale_cost       = epr:sale_cost
            par:retail_cost     = epr:retail_cost
            par:quantity             = epr:quantity
            par:exclude_from_order   = epr:exclude_from_order
            par:part_ref_number      = epr:part_ref_number
            !If not used, then mark to be allocated
            If epr:UsedOnRepair
                par:PartAllocated       = epr:PartAllocated
            Else !If epr:UsedOnRepair
                par:PartAllocated       = 0
                Access:STOCK.Clearkey(sto:Ref_Number_Key)
                sto:Ref_Number  = par:Part_Ref_Number
                If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                    !Found
                    IF (sto:Sundry_Item = 'YES') ! ! #11837 Don't decrement a sundry item. (Bryan: 08/12/2010)
                        par:PartAllocated = 1
                        par:Date_Ordered = TODAY()
                    ELSE
                        If par:Quantity <= sto:Quantity_Stock
                            !If its not a Rapid site, turn part allocated off
!                        If RapidLocation(sto:Location)
                            par:PartAllocated = 0
!                        End !If RapidLocation(sto:Location) = Level:Benign
                            sto:Quantity_Stock  -= par:Quantity
                            If sto:Quantity_Stock < 0
                                sto:Quantity_Stock = 0
                            End !If sto:Quantity_Stock < 0

                            tmp:AddToStockAllocation = 1

                            If Access:STOCK.Update() = Level:Benign
                                If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                    'DEC', | ! Transaction_Type
                                    par:Despatch_Note_Number, | ! Depatch_Note_Number
                                    p_web.GSV('job:Ref_Number'), | ! Job_Number
                                    0, | ! Sales_Number
                                    par:Quantity, | ! Quantity
                                    sto:Purchase_Cost, | ! Purchase_Cost
                                    sto:Sale_Cost, | ! Sale_Cost
                                    sto:Retail_Cost, | ! Retail_Cost
                                    'STOCK DECREMENTED', | ! Notes
                                    '', | !Information
                                    p_web.GSV('BookingUserCode'), |
                                    sto:Quantity_Stock) ! Information
                                    ! Added OK
                                Else ! AddToStockHistory
                                    ! Error
                                End ! AddToStockHistory
                            End! If Access:STOCK.Update() = Level:Benign
                        Else !If par:Quantity < sto:Quantity

                            CreateWebOrder(p_web.GSV('BookingAccount'),par:Part_Number,par:Description,par:Quantity - sto:Quantity_Stock,par:Retail_Cost)
                            If sto:Quantity_Stock > 0
                                locCreateWebOrder = 1
                                locPartNumber = par:Part_Ref_Number
                                locQuantity = par:Quantity - sto:Quantity_Stock
                                !glo:Select1 = 'NEW PENDING WEB'
                                !glo:Select2 = par:Part_Ref_Number
                                !glo:Select3 = par:Quantity - sto:Quantity_Stock
                                !glo:Select4 =
                                par:Quantity    = sto:Quantity_Stock
                                par:Date_Ordered    = Today()
                                tmp:AddToStockAllocation = 0
                            Else !If sto:Quantity_Stock > 0
                                par:WebOrder    = 1
                                tmp:AddToStockAllocation = 2
                            End !If sto:Quantity_Stock > 0
                        End !If par:Quantity > sto:Quantity
                    END

                Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                    !Error
                End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign

            End !If epr:UsedOnRepair

            par:Fault_Code1         = epr:Fault_Code1
            par:Fault_Code2         = epr:Fault_Code2
            par:Fault_Code3         = epr:Fault_Code3
            par:Fault_Code4         = epr:Fault_Code4
            par:Fault_Code5         = epr:Fault_Code5
            par:Fault_Code6         = epr:Fault_Code6
            par:Fault_Code7         = epr:Fault_Code7
            par:Fault_Code8         = epr:Fault_Code8
            par:Fault_Code9         = epr:Fault_Code9
            par:Fault_Code10        = epr:Fault_Code10
            par:Fault_Code11        = epr:Fault_Code11
            par:Fault_Code12        = epr:Fault_Code12
            par:Fault_Codes_Checked = epr:Fault_Codes_Checked
            !Neil
            par:RRCPurchaseCost        = epr:RRCPurchaseCost
            par:RRCSaleCost            = epr:RRCSaleCost
            par:InWarrantyMarkUp       = epr:InWarrantyMarkUp
            par:OutWarrantyMarkUp      = epr:OutWarrantyMarkUp
            par:RRCAveragePurchaseCost = epr:RRCAveragePurchaseCost
            par:AveragePurchaseCost    = epr:AveragePurchaseCost

            access:parts.insert()

            !Add part to stock allocation list - L873 (DBH: 11-08-2003)
            Case tmp:AddToStockAllocation
            Of 1
                AddToStockAllocation(par:Record_Number,'CHA',par:Quantity,'',p_web.GSV('job:Engineer'),p_web)
            Of 2
                AddToStockAllocation(par:Record_Number,'CHA',par:Quantity,'WEB',p_web.GSV('job:Engineer'),p_web)
            End !If tmp:AddToStockAllocation

            If (locCreateWebOrder = 1)
                access:stock.clearkey(sto:ref_number_key)
                sto:ref_number = locPartNumber
                If access:stock.fetch(sto:ref_number_key)
                Else!If access:stock.fetch(sto:ref_number_key)
                    sto:quantity_stock     = 0
                    access:stock.update()

                    If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                         'DEC', | ! Transaction_Type
                                         par:Despatch_Note_Number, | ! Depatch_Note_Number
                                         p_web.GSV('job:Ref_Number'), | ! Job_Number
                                         0, | ! Sales_Number
                                         sto:Quantity_stock, | ! Quantity
                                         par:Purchase_Cost, | ! Purchase_Cost
                                         par:Sale_Cost, | ! Sale_Cost
                                         par:Retail_Cost, | ! Retail_Cost
                                         'STOCK DECREMENTED', | ! Notes
                                         '', |
                                         p_web.GSV('BookingUserCode'), |
                                         sto:Quantity_Stock) ! Information
                        ! Added OK
                    Else ! AddToStockHistory
                        ! Error
                    End ! AddToStockHistory

                    access:parts_alias.clearkey(par_ali:RefPartRefNoKey)
                    par_ali:ref_number      = p_web.GSV('job:ref_number')
                    par_ali:part_ref_number = par:part_Ref_Number
                    If access:parts_alias.fetch(par_ali:RefPartRefNoKey) = Level:Benign
                        par:ref_number           = par_ali:ref_number
                        par:adjustment           = par_ali:adjustment
                        par:part_ref_number      = par_ali:part_ref_number

                        par:part_number     = par_ali:part_number
                        par:description     = par_ali:description
                        par:supplier        = par_ali:supplier
                        par:purchase_cost   = par_ali:purchase_cost
                        par:sale_cost       = par_ali:sale_cost
                        par:retail_cost     = par_ali:retail_cost
                        par:quantity             = locQuantity
                        par:warranty_part        = 'NO'
                        par:exclude_from_order   = 'NO'
                        par:despatch_note_number = ''
                        par:date_ordered         = ''
                        par:date_received        = ''
                        par:pending_ref_number   = ''
                        par:order_number         = ''
                        par:fault_code1    = par_ali:fault_code1
                        par:fault_code2    = par_ali:fault_code2
                        par:fault_code3    = par_ali:fault_code3
                        par:fault_code4    = par_ali:fault_code4
                        par:fault_code5    = par_ali:fault_code5
                        par:fault_code6    = par_ali:fault_code6
                        par:fault_code7    = par_ali:fault_code7
                        par:fault_code8    = par_ali:fault_code8
                        par:fault_code9    = par_ali:fault_code9
                        par:fault_code10   = par_ali:fault_code10
                        par:fault_code11   = par_ali:fault_code11
                        par:fault_code12   = par_ali:fault_code12
                        par:WebOrder = 1
                        access:parts.insert()
                                 AddToStockAllocation(par:Record_Number,'CHA',par:Quantity,'WEB',p_web.GSV('job:Engineer'),p_web)
                    end !If access:parts_alias.clearkey(par_ali:part_ref_number_key) = Level:Benign

                end !if access:stock.fetch(sto:ref_number_key = Level:Benign
            End !If glo:Select1 = 'NEW PENDING WEB'
            !End
        end!if access:parts.primerecord() = level:benign

        !Remove the tick so that if the job is made "Unaccpeted"
        !then you won't have to remove these parts to stock twice
        If epr:UsedOnRepair
            epr:UsedOnRepair = 0
            Access:ESTPARTS.Update()
        End !If epr:UsedOnRepair
    end !loop

    do closeFiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:PARTS.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:PARTS.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:PARTS_ALIAS.Open                                  ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:PARTS_ALIAS.UseFile                               ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:STOCK.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:STOCK.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:ESTPARTS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:ESTPARTS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:USERS.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:USERS.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:PARTS.Close
     Access:PARTS_ALIAS.Close
     Access:STOCK.Close
     Access:ESTPARTS.Close
     Access:USERS.Close
     FilesOpened = False
  END
Local.AllocateExchangePart      Procedure(String    func:Type,Byte  func:Allocated)
Code
        Case func:Type
            Of 'WAR'
                get(warparts,0)
                if access:warparts.primerecord() = level:benign
                    wpr:PArt_Ref_Number      = sto:Ref_Number
                    wpr:ref_number            = p_web.GSV('job:ref_number')
                    wpr:adjustment            = 'YES'
                    wpr:part_number           = 'EXCH'
                    wpr:description           = Clip(p_web.GSV('job:Manufacturer')) & ' EXCHANGE UNIT'
                    wpr:quantity              = 1
                    wpr:warranty_part         = 'NO'
                    wpr:exclude_from_order    = 'YES'
                    wpr:PartAllocated         = func:Allocated !was func:Allocated but see VP116 - must always be allocated straight off!
                                                  !changed back from '1' 20/11 - original system reinstalled
                    if func:Allocated = 0 then
                        wpr:Status            = 'REQ'
                    ELSE
                        WPR:Status            = 'PIK'
                    END
                    If sto:Assign_Fault_Codes = 'YES'
                        !Try and get the fault codes. This key should get the only record
                        Access:STOMODEL.ClearKey(stm:Ref_Part_Description)
                        stm:Ref_Number  = sto:Ref_Number
                        stm:Part_Number = sto:Part_Number
                        stm:Location    = sto:Location
                        stm:Description = sto:Description
                        If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                            !Found
                            wpr:Fault_Code1  = stm:FaultCode1
                            wpr:Fault_Code2  = stm:FaultCode2
                            wpr:Fault_Code3  = stm:FaultCode3
                            wpr:Fault_Code4  = stm:FaultCode4
                            wpr:Fault_Code5  = stm:FaultCode5
                            wpr:Fault_Code6  = stm:FaultCode6
                            wpr:Fault_Code7  = stm:FaultCode7
                            wpr:Fault_Code8  = stm:FaultCode8
                            wpr:Fault_Code9  = stm:FaultCode9
                            wpr:Fault_Code10 = stm:FaultCode10
                            wpr:Fault_Code11 = stm:FaultCode11
                            wpr:Fault_Code12 = stm:FaultCode12
                        Else!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                        !Error
                        !Assert(0,'<13,10>Fetch Error<13,10>')
                        End!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                    end !If sto:Assign_Fault_Codes = 'YES'
                    if access:warparts.insert()
                        access:warparts.cancelautoinc()
                    end
                End

            Of 'CHA'
                get(parts,0)
                if access:parts.primerecord() = level:benign
                    par:PArt_Ref_Number      = sto:Ref_Number
                    par:ref_number            = p_web.GSV('job:ref_number')
                    par:adjustment            = 'YES'
                    par:part_number           = 'EXCH'
                    par:description           = Clip(p_web.GSV('job:Manufacturer')) & ' EXCHANGE UNIT'
                    par:quantity              = 1
                    par:warranty_part         = 'NO'
                    par:exclude_from_order    = 'YES'
                    par:PartAllocated         = func:Allocated
                    !see above for confusion
                    if func:allocated = 0 then
                        par:Status            = 'REQ'
                    ELSE
                        par:status            = 'PIK'
                    END
                    If sto:Assign_Fault_Codes = 'YES'
                       !Try and get the fault codes. This key should get the only record
                       Access:STOMODEL.ClearKey(stm:Ref_Part_Description)
                       stm:Ref_Number  = sto:Ref_Number
                       stm:Part_Number = sto:Part_Number
                       stm:Location    = sto:Location
                       stm:Description = sto:Description
                       If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                           !Found
                           par:Fault_Code1  = stm:FaultCode1
                           par:Fault_Code2  = stm:FaultCode2
                           par:Fault_Code3  = stm:FaultCode3
                           par:Fault_Code4  = stm:FaultCode4
                           par:Fault_Code5  = stm:FaultCode5
                           par:Fault_Code6  = stm:FaultCode6
                           par:Fault_Code7  = stm:FaultCode7
                           par:Fault_Code8  = stm:FaultCode8
                           par:Fault_Code9  = stm:FaultCode9
                           par:Fault_Code10 = stm:FaultCode10
                           par:Fault_Code11 = stm:FaultCode11
                           par:Fault_Code12 = stm:FaultCode12
                       Else!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                           !Error
                           !Assert(0,'<13,10>Fetch Error<13,10>')
                       End!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                    End !If sto:Assign_Fault_Codes = 'YES'
                    if access:parts.insert()
                        access:parts.cancelautoinc()
                    end
                End !If access:Prime
        End !Case func:Type
AddToStockHistory    PROCEDURE  (Long f:RefNo,String f:Trans,String f:Desp,Long f:JobNo,Long f:SaleNo,Long f:Qty,Real f:Purch,Real f:Sale,Real f:Retail,String f:Notes,String f:Info,String f:UserCode,Long f:QtyStock,<Real f:AvPC>,<Real f:PC>,<Real f:SC>,<Real f:RC>) ! Declare Procedure
FilesOpened     BYTE(0)
  CODE
    rtnValue# = 0
    do openFiles
    If Access:STOHIST.PrimeRecord() = Level:Benign
        shi:Ref_Number           = f:RefNo
        shi:User                 = f:UserCode
        shi:Transaction_Type     = f:Trans
        shi:Despatch_Note_Number = f:Desp
        shi:Job_Number           = f:JobNo
        shi:Sales_Number         = f:SaleNo
        shi:Quantity             = f:Qty
        shi:Date                 = Today()
        shi:Purchase_Cost        = f:Purch
        shi:Sale_Cost            = f:Sale
        shi:Retail_Cost          = f:Retail
        shi:Notes                = f:Notes
        shi:Information          = f:Info
        shi:StockOnHand          = f:QtyStock
        If Access:STOHIST.TryInsert() = Level:Benign
            ! Insert Successful
            rtnValue# = 1
            IF (Access:STOHISTE.PrimeRecord() = Level:Benign)
                stoe:SHIRecordNumber = shi:Record_Number
                IF (f:AvPC = 0)
                    f:AvPC = f:Purch
                END
                IF (f:PC = 0)
                    f:PC = f:Purch
                END
                IF (f:SC = 0)
                    f:SC = f:Sale
                END
                IF (f:RC = 0)
                    f:RC = f:Retail
                END

                stoe:PreviousAveragePurchaseCost = f:AvPC
                stoe:PurchaseCost = f:PC
                stoe:SaleCost = f:SC
                stoe:RetailCost = f:RC
                IF (Access:STOHISTE.TryInsert())
                    Access:STOHISTE.CancelAutoInc()
                END
            END

        Else ! If Access:STOHIST.TryInsert() = Level:Benign
            ! Insert Failed
            Access:STOHIST.CancelAutoInc()
        End ! If Access:STOHIST.TryInsert() = Level:Benign
    End ! If Access:STOHIST.PrimeRecord() = Level:Benign

    do closeFiles

    Return rtnValue#
!--------------------------------------
OpenFiles  ROUTINE
  Access:STOHISTE.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:STOHISTE.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:STOHIST.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:STOHIST.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:STOHISTE.Close
     Access:STOHIST.Close
     FilesOpened = False
  END
CalculateBilling     PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
local:CFaultCode        String(255)
local:CIndex            Byte(0)
local:CRepairType       String(30)
local:WFaultCOde        String(255)
local:WIndex            Byte(0)
local:WRepairType       String(30)
local:KeyRepair         Long()
local:FaultCode         String(255)
FilesOpened     BYTE(0)
  CODE
    do openfiles

    Access:MANUFACT.Clearkey(man:Manufacturer_Key)
    man:Manufacturer = p_web.GSV('job:Manufacturer')
    If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
        If man:UseInvTextForFaults = True
            If man:AutoRepairType = True
                ! Get the main out fault (DBH: 23/11/2007)
                Access:MANFAULT.Clearkey(maf:MainFaultKey)
                maf:Manufacturer = p_web.GSV('job:Manufacturer')
                maf:MainFault = 1
                If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign

                    ! Check the out faults list (DBH: 23/11/2007)
                    Access:JOBOUTFL.Clearkey(joo:JobNumberKey)
                    joo:JobNumber = p_web.GSV('job:Ref_Number')
                    Set(joo:JobNumberKey,joo:JobNumberKey)
                    Loop
                        If Access:JOBOUTFL.Next()
                            Break
                        End ! If Access:JOBOUTFL.Next()
                        If joo:JobNumber <> p_web.GSV('job:Ref_Number')
                            Break
                        End ! If joo:JobNumber <> job:Ref_Number

                        ! Get the lookup details (e.g. index and repair types) (DBH: 23/11/2007)
                        Access:MANFAULO.Clearkey(mfo:Field_Key)
                        mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                        mfo:Field_Number = maf:Field_Number
                        mfo:Field = joo:FaultCode
                        If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                            ! Is there a repair type? (DBH: 23/11/2007)
                            If mfo:RepairType <> ''
                                ! Record the fault code and index if it's the HIGHEST index (DBH: 23/11/2007)
                                If mfo:ImportanceLevel > local:CIndex
                                    local:CFaultCode = mfo:Field
                                    local:CIndex = mfo:ImportanceLevel
                                    local:CRepairType = mfo:RepairType
                                End ! If mfo:ImportanceLevel > local:CIndex
                            End ! If mfo:RepairType <> ''
                            If mfo:RepairTypeWarranty <> ''
                                ! Record the fault code and index if it's the HIGHEST index (DBH: 23/11/2007)
                                If mfo:ImportanceLevel > local:WIndex
                                    local:WFaultCode = mfo:Field
                                    local:WIndex = mfo:ImportanceLevel
                                    local:WRepairType = mfo:RepairTypeWarranty
                                End ! If mfo:ImportanceLevel > local:CIndex
                            End ! If mfo:RepairTypeWarranty <> ''
                        End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                    End ! Loop

                    ! Inserting (DBH 30/04/2008) # 9723 - Is there a "Key Repair" fault code? If so, that is counted as the OUT Fault
                    Access:MANFAUPA.Clearkey(map:KeyRepairKey)
                    map:Manufacturer = p_web.GSV('job:Manufacturer')
                    map:KeyRepair = 1
                    If Access:MANFAUPA.TryFetch(map:KeyRepairKey) = Level:Benign
                        ! Found
                        local:KeyRepair = map:Field_Number
                    Else ! If Access:MANFAUPA.TryFetch(map:KeyRepairKey) = Level:Benign
                        local:KeyRepair = 0
                    End ! If Access:MANFAUPA.TryFetch(map:KeyRepairKey) = Level:Benign
                    ! End (DBH 30/04/2008) #9723

! Changing (DBH 21/05/2008) # 9723 - Can be more than one part out fault
!                    Access:MANFAUPA.Clearkey(map:MainFaultKey)
!                    map:Manufacturer = job:Manufacturer
!                    map:MainFault = 1
!                    If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
! to (DBH 21/05/2008) # 9723
                    ! Is the out fault held on the parts. If so, check for the highest repair index there too (DBH: 23/11/2007)
                    Access:MANFAUPA.Clearkey(map:MainFaultKey)
                    map:Manufacturer = p_web.GSV('job:Manufacturer')
                    map:MainFault = 1
                    Set(map:MainFaultKey,map:MainFaultKey)
                    Loop ! Begin Loop
                        If Access:MANFAUPA.Next()
                            Break
                        End ! If Access:MANFAUPA.Next()
                        If map:Manufacturer <> p_web.GSV('job:Manufacturer')
                            Break
                        End ! If map:Manufacturer <> job:Manufacturer
                        If map:MainFault <> 1
                            Break
                        End ! If map:MainFault <> 1

! End (DBH 21/05/2008) #9723
                        ! Check the Warranty Parts (DBH: 23/11/2007)
                        Access:WARPARTS.Clearkey(wpr:Part_Number_Key)
                        wpr:Ref_Number = p_web.GSV('job:Ref_Number')
                        Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
                        Loop
                            If Access:WARPARTS.Next()
                                Break
                            End ! If Access:WARPARTS.Next()
                            If wpr:Ref_Number <> p_web.GSV('job:Ref_Number')
                                Break
                            End ! If wpr:Ref_Number <> job:Ref_Number

                            ! Inserting (DBH 30/04/2008) # 9723 - If there is a key repair. Only count the part that ISthe Key Repair
                            Case local:KeyRepair
                            Of 1
                                If wpr:Fault_Code1 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code1 <> 1
                            Of 2
                                If wpr:Fault_Code2 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code2 <> 1
                            Of 3
                                If wpr:Fault_Code3 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            Of 4
                                If wpr:Fault_Code4 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            Of 5
                                If wpr:Fault_Code5 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            Of 6
                                If wpr:Fault_Code6 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            Of 7
                                If wpr:Fault_Code7 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            Of 8
                                If wpr:Fault_Code8 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            Of 9
                                If wpr:Fault_Code9 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            Of 10
                                If wpr:Fault_Code10 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            Of 11
                                If wpr:Fault_Code11 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            Of 12
                                If wpr:Fault_Code12 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            End ! Case local:KeyRepair
                            ! End (DBH 30/04/2008) #9723

                            ! Lookup the fault code details using whichever is designated as the out fault (DBH: 23/11/2007)
                            Access:MANFAULO.Clearkey(mfo:Field_Key)
                            mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                            mfo:Field_Number = maf:Field_NUmber
                            Case map:Field_Number
                            Of 1
                                mfo:Field = wpr:Fault_Code1
                            Of 2
                                mfo:Field = wpr:Fault_Code2
                            Of 3
                                mfo:Field = wpr:Fault_Code3
                            Of 4
                                mfo:Field = wpr:Fault_Code4
                            Of 5
                                mfo:Field = wpr:Fault_Code5
                            Of 6
                                mfo:Field = wpr:Fault_Code6
                            Of 7
                                mfo:Field = wpr:Fault_Code7
                            Of 8
                                mfo:Field = wpr:Fault_Code8
                            Of 9
                                mfo:Field = wpr:Fault_Code9
                            Of 10
                                mfo:Field = wpr:Fault_Code10
                            Of 11
                                mfo:Field = wpr:Fault_Code11
                            Of 12
                                mfo:Field = wpr:Fault_Code12
                            End ! Case map:Field_Number
                            If Access:MANFAULO.Tryfetch(mfo:Field_Key) = Level:Benign
                                ! Is there a repair type? (DBH: 23/11/2007)
                                If mfo:RepairType <> ''
                                    ! Record the fault code and index if it's the HIGHEST index (DBH: 23/11/2007)
                                    If mfo:ImportanceLevel > local:CIndex
                                        local:CFaultCode = mfo:Field
                                        local:CIndex = mfo:ImportanceLevel
                                        local:CRepairType = mfo:RepairType
                                    End ! If mfo:ImportanceLevel > local:CIndex
                                End ! If mfo:RepairType <> ''
                                If mfo:RepairTypeWarranty <> ''
                                    ! Record the fault code and index if it's the HIGHEST index (DBH: 23/11/2007)
                                    If mfo:ImportanceLevel > local:WIndex
                                        local:WFaultCode = mfo:Field
                                        local:WIndex = mfo:ImportanceLevel
                                        local:WRepairType = mfo:RepairTypeWarranty
                                    End ! If mfo:ImportanceLevel > local:CIndex
                                End ! If mfo:RepairTypeWarranty <> ''
                            End ! If Access:MANFAULO.Clearkey(mfo:Field_Key) = Level:Benign
                        End ! Loop

                        ! Check the estimate parts (DBH: 23/11/2007)
                        If p_web.GSV('job:Estimate') = 'YES' And p_web.GSV('job:Chargeable_Job') = 'YES' And p_web.GSV('job:Estimate_Accepted') <> 'YES' And p_web.GSV('job:Estimate_Rejected') <> 'YES'
                            Access:ESTPARTS.Clearkey(epr:Part_Number_Key)
                            epr:Ref_Number = p_web.GSV('job:Ref_Number')
                            Set(epr:Part_Number_Key,epr:Part_Number_Key)
                            Loop
                                If Access:ESTPARTS.Next()
                                    Break
                                End ! If Access:ESTPARTS.Next()
                                If epr:Ref_Number <> p_web.GSV('job:Ref_Number')
                                    Break
                                End ! If epr:Ref_Number <> job:Ref_Number

                                ! Inserting (DBH 30/04/2008) # 9723 - If there is a key repair. Only count the part that ISthe Key Repair
                                Case local:KeyRepair
                                Of 1
                                    If epr:Fault_Code1 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code1 <> 1
                                Of 2
                                    If epr:Fault_Code2 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code2 <> 1
                                Of 3
                                    If epr:Fault_Code3 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                Of 4
                                    If epr:Fault_Code4 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                Of 5
                                    If epr:Fault_Code5 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                Of 6
                                    If epr:Fault_Code6 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                Of 7
                                    If epr:Fault_Code7 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                Of 8
                                    If epr:Fault_Code8 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                Of 9
                                    If epr:Fault_Code9 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                Of 10
                                    If epr:Fault_Code10 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                Of 11
                                    If epr:Fault_Code11 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                Of 12
                                    If epr:Fault_Code12 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                End ! Case local:KeyRepair
                                ! End (DBH 30/04/2008) #9723

                                ! Lookup the fault code details using whichever is designated as the out fault (DBH: 23/11/2007)
                                Access:MANFAULO.Clearkey(mfo:Field_Key)
                                mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                mfo:Field_Number = maf:Field_NUmber
                                Case map:Field_Number
                                Of 1
                                    mfo:Field = epr:Fault_Code1
                                Of 2
                                    mfo:Field = epr:Fault_Code2
                                Of 3
                                    mfo:Field = epr:Fault_Code3
                                Of 4
                                    mfo:Field = epr:Fault_Code4
                                Of 5
                                    mfo:Field = epr:Fault_Code5
                                Of 6
                                    mfo:Field = epr:Fault_Code6
                                Of 7
                                    mfo:Field = epr:Fault_Code7
                                Of 8
                                    mfo:Field = epr:Fault_Code8
                                Of 9
                                    mfo:Field = epr:Fault_Code9
                                Of 10
                                    mfo:Field = epr:Fault_Code10
                                Of 11
                                    mfo:Field = epr:Fault_Code11
                                Of 12
                                    mfo:Field = epr:Fault_Code12
                                End ! Case map:Field_Number
                                If Access:MANFAULO.Tryfetch(mfo:Field_Key) = Level:Benign
                                    ! Is there a repair type? (DBH: 23/11/2007)
                                    If mfo:RepairType <> ''
                                        ! Record the fault code and index if it's the HIGHEST index (DBH: 23/11/2007)
                                        If mfo:ImportanceLevel > local:CIndex
                                            local:CFaultCode = mfo:Field
                                            local:CIndex = mfo:ImportanceLevel
                                            local:CRepairType = mfo:RepairType
                                        End ! If mfo:ImportanceLevel > local:CIndex
                                    End ! If mfo:RepairType <> ''
                                    If mfo:RepairTypeWarranty <> ''
                                        ! Record the fault code and index if it's the HIGHEST index (DBH: 23/11/2007)
                                        If mfo:ImportanceLevel > local:WIndex
                                            local:WFaultCode = mfo:Field
                                            local:WIndex = mfo:ImportanceLevel
                                            local:WRepairType = mfo:RepairTypeWarranty
                                        End ! If mfo:ImportanceLevel > local:CIndex
                                    End ! If mfo:RepairTypeWarranty <> ''
                                End ! If Access:MANFAULO.Clearkey(mfo:Field_Key) = Level:Benign
                            End ! Loop
                        Else ! If job:Estimate = 'YES' And job:Chargeabe_Job = 'YES' And job:Estimate_Accepted <> 'YES' And job:Estimate_Rejected <> 'YES'
                            Access:PARTS.Clearkey(par:Part_Number_Key)
                            par:Ref_Number = p_web.GSV('job:Ref_Number')
                            Set(par:Part_Number_Key,par:Part_Number_Key)
                            Loop
                                If Access:PARTS.Next()
                                    Break
                                End ! If Access:PARTS.Next()
                                If par:Ref_Number <> p_web.GSV('job:Ref_Number')
                                    Break
                                End ! If par:Ref_Number <> job:Ref_Number
                                ! Inserting (DBH 30/04/2008) # 9723 - If there is a key repair. Only count the part that ISthe Key Repair
                                Case local:KeyRepair
                                Of 1
                                    If par:Fault_Code1 <> 1
                                        Cycle
                                    End ! If par:Fault_Code1 <> 1
                                Of 2
                                    If par:Fault_Code2 <> 1
                                        Cycle
                                    End ! If par:Fault_Code2 <> 1
                                Of 3
                                    If par:Fault_Code3 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                Of 4
                                    If par:Fault_Code4 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                Of 5
                                    If par:Fault_Code5 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                Of 6
                                    If par:Fault_Code6 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                Of 7
                                    If par:Fault_Code7 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                Of 8
                                    If par:Fault_Code8 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                Of 9
                                    If par:Fault_Code9 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                Of 10
                                    If par:Fault_Code10 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                Of 11
                                    If par:Fault_Code11 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                Of 12
                                    If par:Fault_Code12 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                End ! Case local:KeyRepair
                                ! End (DBH 30/04/2008) #9723

                                ! Lookup the fault code details using whichever is designated as the out fault (DBH: 23/11/2007)
                                Access:MANFAULO.Clearkey(mfo:Field_Key)
                                mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                mfo:Field_Number = maf:Field_NUmber
                                Case map:Field_Number
                                Of 1
                                    mfo:Field = par:Fault_Code1
                                Of 2
                                    mfo:Field = par:Fault_Code2
                                Of 3
                                    mfo:Field = par:Fault_Code3
                                Of 4
                                    mfo:Field = par:Fault_Code4
                                Of 5
                                    mfo:Field = par:Fault_Code5
                                Of 6
                                    mfo:Field = par:Fault_Code6
                                Of 7
                                    mfo:Field = par:Fault_Code7
                                Of 8
                                    mfo:Field = par:Fault_Code8
                                Of 9
                                    mfo:Field = par:Fault_Code9
                                Of 10
                                    mfo:Field = par:Fault_Code10
                                Of 11
                                    mfo:Field = par:Fault_Code11
                                Of 12
                                    mfo:Field = par:Fault_Code12
                                End ! Case map:Field_Number
                                If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                    ! Is there a repair type? (DBH: 23/11/2007)
                                    If mfo:RepairType <> ''
                                        ! Record the fault code and index if it's the HIGHEST index (DBH: 23/11/2007)
                                        If mfo:ImportanceLevel > local:CIndex
                                            local:CFaultCode = mfo:Field
                                            local:CIndex = mfo:ImportanceLevel
                                            local:CRepairType = mfo:RepairType
                                        End ! If mfo:ImportanceLevel > local:CIndex
                                    End ! If mfo:RepairType <> ''
                                    If mfo:RepairTypeWarranty <> ''
                                        ! Record the fault code and index if it's the HIGHEST index (DBH: 23/11/2007)
                                        If mfo:ImportanceLevel > local:WIndex
                                            local:WFaultCode = mfo:Field
                                            local:WIndex = mfo:ImportanceLevel
                                            local:WRepairType = mfo:RepairTypeWarranty
                                        End ! If mfo:ImportanceLevel > local:CIndex
                                    End ! If mfo:RepairTypeWarranty <> ''
                                End ! If Access:MANFAULO.Clearkey(mfo:Field_Key) = Level:Benign

                            End ! Loop
                        End ! If job:Estimate = 'YES' And job:Chargeabe_Job = 'YES' And job:Estimate_Accepted <> 'YES' And job:Estimate_Rejected <> 'YES'
                    End ! If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
                End ! If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
            End ! If man:AutoRepairType = True
        End ! If man:UseInvTextForFaults = True
    End ! If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign

    ! If the charge types are already filled then assume "overwrite" box has been ticked.
!    ! So don't change. (DBH: 23/11/2007)
!    if (p_web.GSV('jobe:COverwriteRepairType') <> 1)
!        p_web.SSV('job:Repair_Type',local:CRepairType)
!    end ! if (p_web.GSV('jobe:COverwriteRepairType') = 0)
!
!    if (p_web.GSV('jobe:WOverwriteRepairType') <> 1)
!        p_web.SSV('job:Warranty_Repair_Type',local:WRepairType)
!    end ! if (p_web.GSV('jobe:WOverwriteRepairType') = 0)

    if (local:CRepairType <> '' and local:WRepairType <> '')
        if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') ='YES')
            if (p_web.GSV('jobe:COverwriteRepairType') <> 1)
                p_web.SSV('job:repair_Type',local:CRepairType)
            end
            if (p_web.GSV('jobe:WOverwriteRepairType') <> 1)
                p_web.SSV('job:repair_Type_Warranty',local:WRepairType)
            end
        end ! if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') ='YES')
        if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') <> 'YES')
            if (p_web.GSV('jobe:COverwriteRepairType') <> 1)
                p_web.SSV('job:repair_Type',local:CRepairType)
            end
        end ! if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') ='YES')
        if (p_web.GSV('job:chargeable_job') <> 'YES' and p_web.GSV('job:warranty_job') = 'YES')
            if (p_web.GSV('jobe:WOverwriteRepairType') <> 1)
                p_web.SSV('job:repair_Type_Warranty',local:WRepairType)
            end
        end ! if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') ='YES')
    end  !if (local:CRepairType <> '' and local:WRepairType <> '')
    if (local:CRepairType = '' and local:WRepairType <> '')
        if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') ='YES')
            if (p_web.GSV('jobe:COverwriteRepairType') <> 1)
                p_web.SSV('job:repair_Type','')
            end
        end ! if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') ='YES')
        if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') <> 'YES')
            if (p_web.GSV('jobe:COverwriteRepairType') <> 1)
                p_web.SSV('job:repair_Type','')
            end
        end ! if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') ='YES')
        if (p_web.GSV('job:chargeable_job') <> 'YES' and p_web.GSV('job:warranty_job') = 'YES')
            if (p_web.GSV('jobe:WOverwriteRepairType') <> 1)
                p_web.SSV('job:repair_Type_Warranty',local:WRepairType)
            end
        end ! if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') ='YES')
    end  !if (local:CRepairType <> '' and local:WRepairType <> '')
    if (local:CRepairType <> '' and local:WRepairType = '')
        if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') ='YES')
            if (p_web.GSV('jobe:COverwriteRepairType') <> 1)
                p_web.SSV('job:repair_Type',local:CRepairType)
            end
            if (p_web.GSV('jobe:WOverwriteRepairType') <> 1)
                p_web.SSV('job:Warranty_Job','NO')
            end
            transferParts_C(p_web)
        end ! if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') ='YES')
        if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') <> 'YES')
            if (p_web.GSV('jobe:COverwriteRepairType') <> 1)
                p_web.SSV('job:repair_Type',local:CRepairType)
            end
        end ! if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') ='YES')
        if (p_web.GSV('job:chargeable_job') <> 'YES' and p_web.GSV('job:warranty_job') = 'YES')
            if (p_web.GSV('jobe:WOverwriteRepairType') <> 1)
                p_web.SSV('job:warranty_Job','NO')
            end
            p_web.SSV('job:chargeable_Job','YES')
            if (p_web.GSV('jobe:COverwriteRepairType') <> 1)
                p_web.SSV('job:repair_Type',local:CRepairType)
            end
        end ! if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') ='YES')
    end  !if (local:CRepairType <> '' and local:WRepairType <> '')


    ! Inserting (DBH 13/06/2006) #6733 - Work out if to use Warranty or Chargeable Fault Code
    local:FaultCode = ''
    if (p_web.GSV('job:Chargeable_Job') <> 'YES' And p_web.GSV('job:Warranty_Job') = 'YES')
        local:FaultCode = local:WFaultCode
    End ! If job:Chargeable_Job <> 'YES' And job:Warranty_Job = 'YES'
    if (p_web.GSV('job:Chargeable_Job') = 'YES' And p_web.GSV('job:Warranty_Job') <> 'YES')
        local:FaultCode = local:CFaultCode
    End ! If job:Chargeable_Job = 'YES' And job:Warranty_Job <> 'YES'
    if (p_web.GSV('job:Chargeable_Job') = 'YES' And p_web.GSV('job:Warranty_Job') = 'YES')
        If local:WFaultCode <> ''
            local:FaultCode = local:WFaultCode
        Else
            local:FaultCode = local:CFaultCode
        End ! If tmp:FaultCodeW_T <> ''
    End ! If job:Chargeable_Job = 'YES' ANd job:Warranty_Job = 'YES'
    ! End (DBH 13/06/2006) #6733



    Access:MANFAULT.Clearkey(maf:mainFaultKey)
    maf:manufacturer    = p_web.GSV('job:manufacturer')
    maf:mainFault    = 1
    if (Access:MANFAULT.TryFetch(maf:mainFaultKey) = Level:Benign)
        ! Found
        if (maf:field_Number < 13)
            p_web.SSV('job:fault_Code' & maf:field_Number,local:faultCode)
        else ! if (maf:field_Number < 13)
            p_web.SSV('wob:faultCode' & maf:field_Number,local:faultCode)
        end ! if (maf:field_Number < 13)
    else ! if (Access:MANFAULT.TryFetch(maf:mainFaultKey) = Level:Benign)
        ! Error
    end ! if (Access:MANFAULT.TryFetch(maf:mainFaultKey) = Level:Benign)

    do closefiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:MANUFACT.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANUFACT.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:MANFAULT.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANFAULT.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBOUTFL.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBOUTFL.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:MANFAULO.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANFAULO.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:MANFAUPA.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANFAUPA.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:WARPARTS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:WARPARTS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:ESTPARTS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:ESTPARTS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:PARTS.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:PARTS.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:MANUFACT.Close
     Access:MANFAULT.Close
     Access:JOBOUTFL.Close
     Access:MANFAULO.Close
     Access:MANFAUPA.Close
     Access:WARPARTS.Close
     Access:ESTPARTS.Close
     Access:PARTS.Close
     FilesOpened = False
  END
ProductCodeRequired  PROCEDURE  (func:Manufacturer)        ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
MANUFACT::State  USHORT
FilesOpened     BYTE(0)
  CODE
    Do OpenFiles
    Do SaveFiles

    Return# = 0
    Access:MANUFACT.ClearKey(man:Manufacturer_Key)
    man:Manufacturer = func:Manufacturer
    If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
        If man:UseProductCode
            Return# = 1
        End!If man:Use_MSN = 'YES'
    End!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign

    Do RestoreFiles
    Do CloseFiles
    Return Return#
!--------------------------------------
OpenFiles  ROUTINE
  Access:MANUFACT.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANUFACT.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:MANUFACT.Close
     FilesOpened = False
  END
SaveFiles  ROUTINE
  MANUFACT::State = Access:MANUFACT.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF MANUFACT::State <> 0
    Access:MANUFACT.RestoreFile(MANUFACT::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
