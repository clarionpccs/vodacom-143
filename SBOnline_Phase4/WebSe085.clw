

   MEMBER('WebServer_Phase4.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSE085.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSE021.INC'),ONCE        !Req'd for module callout resolution
                     END


BrowseStockHistory   PROCEDURE  (NetWebServerWorker p_web)
locDateIn            DATE                                  !
locDateOut           DATE                                  !
locJobRetail         STRING(30)                            !
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256),dim(Net:MaxKeyFields)
idx           String(Net:HashSize)
sub           Long
            End
locdateIn:IsInvalid  Long
locDateOut:IsInvalid  Long
shi:Quantity:IsInvalid  Long
locJobRetail:IsInvalid  Long
shi:Purchase_Cost:IsInvalid  Long
shi:Sale_Cost:IsInvalid  Long
shi:User:IsInvalid  Long
shi:Notes:IsInvalid  Long
shi:Information:IsInvalid  Long
shi:StockOnHand:IsInvalid  Long
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:DefaultSelection    String(Net:HashSize)
loc:ActualSelection     String(Net:HashSize)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
!loc:pagename            String(256)
loc:NavButtonPosition    Long
loc:UpdateButtonPosition Long
loc:SelectionMethod      Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorCase         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
loc:lookupdone          Long
loc:FormPopup           Long
ThisView            View(STOHIST)
                      Project(shi:Record_Number)
                      Project(shi:Quantity)
                      Project(shi:Purchase_Cost)
                      Project(shi:Sale_Cost)
                      Project(shi:User)
                      Project(shi:Notes)
                      Project(shi:Information)
                      Project(shi:StockOnHand)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
Loc:popup             Long
loc:poppedup          Long,static,thread
FilesOpened     Long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return 
  End
  GlobalErrors.SetProcedureName('BrowseStockHistory')
  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseStockHistory:NoForm')
      loc:NoForm = p_web.GetValue('BrowseStockHistory:NoForm')
      loc:FormName = p_web.GetValue('BrowseStockHistory:FormName')
    else
      loc:FormName = 'BrowseStockHistory_frm'
    End
    p_web.SSV('BrowseStockHistory:NoForm',loc:NoForm)
    p_web.SSV('BrowseStockHistory:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseStockHistory:NoForm')
    loc:FormName = p_web.GSV('BrowseStockHistory:FormName')
  end
  case p_web.site.formpopups
  of -1 ; loc:FormPopup = Net:Page
  of 0 ; loc:FormPopup = Net:Page
  of 1 ; loc:FormPopup = Net:Popup
  End
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseStockHistory') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseStockHistory')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if 1 = 2
  elsif p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  elsif p_web.IfExistsValue('_EIPRow_')
    do CallRow
  elsif p_web.GetValue('_CallPopups') > 0
    do CallPopups
  elsif p_web.IfExistsValue('_GetSecwinSettings')
      p_web.SetValue('Secwin_' & 'BrowseStockHistory' & 'AccessGroupsArray','1') 
      p_web.SetValue('Secwin_AccessWindowName','BrowseStockHistory')
      
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallPopups  Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
loc:CallPopups   Long
loc:name         String(255)
  code
  if loc:poppedup = 0
    loc:CallPopups = p_web.GetValue('_CallPopups')
    if loc:CallPopups = 1 !and p_web.GetPreCall('BrowseStockHistory') = 0! for = 1 include popup divs and scripts for this browse
      !p_web.AddPreCall('BrowseStockHistory')
      p_web.DivHeader('popup_BrowseStockHistory','nt-hidden')
      p_web.DivHeader('BrowseStockHistory',p_web.combine(p_web.site.style.browsediv,))
      p_web.DivFooter()
      p_web.DivFooter()
      loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(400)&', modal: true, position: [''center'',15]'
      packet =  p_web.jQuery('#' & lower('popup_BrowseStockHistory_div'),'dialog',loc:options) & |
      p_web.jQuery('#' & lower('popup_BrowseStockHistory_div'),'removeClass','''nt-hidden''')
    End
    ! for > 0 add in form, EIP-lookups and browse object
    do Popups
    loc:popup = 1
    do ClosingScripts
    do SendPacket
    loc:poppedup = 1
  end

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web.DivHeader('BrowseStockHistory',p_web.combine(p_web.site.style.browsediv,))
  if loc:ParentSilent = 0
    do GenerateBrowse
    if p_web.RequestAjax = 1
      p_web.Script('BrowseProducts.pcv();')
    end
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web.DivFooter()
  do Children
  do Popups
  if loc:poppedup = 0
    do ClosingScripts
  end
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(STOHIST,shi:record_number_key,loc:vorder)
    If False
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseStockHistory:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseStockHistory:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseStockHistory:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseStockHistory:LookupField')
    loc:selecting = 0
  End

Popups Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  If loc:popup = 0
  End
SetFormAction  Routine
  loc:formaction = 'BrowseStockHistory'

GotFocusBack   Routine

GenerateBrowse Routine
  data
loc:viewoptions  Long
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  ! Set general Browse options
  loc:NavButtonPosition   = Net:Below
  loc:UpdateButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:PageLoad
  loc:Sorting          = Net:ServerSort
  if p_web.GetValue('_popup_') = 1
    loc:popup = 1
  end
  ! Set Locator Options
  loc:LocatorPosition  = Net:Below
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  p_web.ClearBrowse('BrowseStockHistory')
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  ! Store return URL
  IF (p_web.IfExistsValue('ReturnURL'))
      p_web.StoreValue('ReturnURL')
  END
      p_web.site.CloseButton.Image = '/images/psave.png'
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 12
  End
  loc:ActualSelection = ''
  !loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseStockHistory_sort',net:DontEvaluate)
  p_web.SetSessionValue('BrowseStockHistory_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  if loc:vorder = ''
    loc:vorder = '+shi:Ref_Number,+shi:Date'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('locDateIn')
    loc:SortHeader = p_web.Translate('Date In')
    p_web.SetSessionValue('BrowseStockHistory_LocatorPic','@d06b')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
    loc:LocatorType = Net:Date
  Of upper('locDateOut')
    loc:SortHeader = p_web.Translate('Date Out')
    p_web.SetSessionValue('BrowseStockHistory_LocatorPic','@d06b')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
    loc:LocatorType = Net:Date
  Of upper('shi:Quantity')
    loc:SortHeader = p_web.Translate('Quantity')
    p_web.SetSessionValue('BrowseStockHistory_LocatorPic','@n8')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
  Of upper('locJobRetail')
    loc:SortHeader = p_web.Translate('Job/Retail')
    p_web.SetSessionValue('BrowseStockHistory_LocatorPic','@s10')
  Of upper('shi:Purchase_Cost')
    loc:SortHeader = p_web.Translate('In Warr')
    p_web.SetSessionValue('BrowseStockHistory_LocatorPic','@n-14.2')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
  Of upper('shi:Sale_Cost')
    loc:SortHeader = p_web.Translate('Out Warr')
    p_web.SetSessionValue('BrowseStockHistory_LocatorPic','@n-14.2')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
  Of upper('shi:User')
    loc:SortHeader = p_web.Translate('User')
    p_web.SetSessionValue('BrowseStockHistory_LocatorPic','@s3')
  Of upper('shi:Notes')
    loc:SortHeader = p_web.Translate('Status')
    p_web.SetSessionValue('BrowseStockHistory_LocatorPic','@s255')
  Of upper('shi:Information')
    loc:SortHeader = p_web.Translate('Information')
    p_web.SetSessionValue('BrowseStockHistory_LocatorPic','@s255')
  Of upper('shi:StockOnHand')
    loc:SortHeader = p_web.Translate('Stock On Hand')
    p_web.SetSessionValue('BrowseStockHistory_LocatorPic','@s8')
  End
  If loc:selecting = 1
    p_web.GetSettings(p_web.GetSessionValue('Push1'))
    loc:selectaction = p_web.FormSettings.ParentPage !p_web.GetSessionValue('BrowseStockHistory:LookupFrom')
  End!Else
  loc:CloseAction = p_web.GSV('ReturnURL')
  do SendPacket
  do SetFormAction
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseStockHistory:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseStockHistory:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseStockHistory:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="STOHIST"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="shi:record_number_key"></input><13,10>'
  end
  If p_web.Translate('Browse Stock History') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.browseSubHeading,)&'">'&p_web.Translate('Browse Stock History',0)&'</div>'&CRLF
  End
  case p_web.GetValue('refresh') ; of 'next' orof 'previous' orof 'first' orof 'last'
    p_web.Script('try{{BrowseStockHistory.restoreFocus();} catch(err){{};')
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseStockHistory',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      case loc:locatortype
      of Net:Position
      orof Net:Date
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
      of Net:Contains
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
      of Net:Begins
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
      End
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
      Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
      Case Loc:LocatorType
      of Net:Date
        packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator2BrowseStockHistory','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('BrowseStockHistory_LocatorPic'),,,'onchange="BrowseStockHistory.locate(''Locator2BrowseStockHistory'',this.value);"',,0,,,) & '</div></td>'
      Else
        packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator2BrowseStockHistory',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="BrowseStockHistory.locate(''Locator2BrowseStockHistory'',this.value);" ',,,,,,) & '</td>'
      End
      If loc:LocatorSearchButton or loc:LocatorClearButton
        packet = clip(packet) & '<td><div id="BrowseStockHistory_locate_a" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
        If loc:LocatorSearchButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
        End
        If loc:LocatorClearButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseStockHistory.cl(''BrowseStockHistory'');')
        End
        packet = clip(packet) & '</div><13,10></td><13,10>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
      If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'BrowseStockHistory_locate_a','buttonset',loc:options)
      End
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & p_web.DivHeader('BrowseStockHistory_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'','sortable')&'" id="BrowseStockHistory_tbl">'
  Else
    packet = clip(packet) & p_web.DivHeader('BrowseStockHistory_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'')&'" id="BrowseStockHistory_tbl">'
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'4','BrowseStockHistory',p_web.Translate('Date In'),,,,,,,1,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'5','BrowseStockHistory',p_web.Translate('Date Out'),,,,,,,1,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'6','BrowseStockHistory',p_web.Translate('Quantity'),'Click here to sort by Quantity',,,,,,1,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'7','BrowseStockHistory',p_web.Translate('Job/Retail'),,,,,,,1,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'8','BrowseStockHistory',p_web.Translate('In Warr'),'Click here to sort by Purchase Cost',,,,,,1,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'9','BrowseStockHistory',p_web.Translate('Out Warr'),'Click here to sort by Sale Cost',,,,,,1,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'10','BrowseStockHistory',p_web.Translate('User'),'Click here to sort by User',,,,,,1,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'1','BrowseStockHistory',p_web.Translate('Status'),'Click here to sort by Notes',,,,,,1,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'2','BrowseStockHistory',p_web.Translate('Information'),'Click here to sort by Information',,,,,,1,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'3','BrowseStockHistory',p_web.Translate('Stock On Hand'),'Click here to sort by Stock On Hand',,,,,,1,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,12,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  If Instring('shi:record_number',lower(loc:vorder),1,1) = 0 !and STOHIST{prop:SQLDriver} = 1
    loc:vorder = Choose(loc:vorder='','shi:Record_Number',clip(loc:vorder) & ',' & 'shi:Record_Number')
  End
  Loc:Selected = Choose(p_web.IfExistsValue('shi:Record_Number'),p_web.GetValue('shi:Record_Number'),p_web.GetSessionValue('shi:Record_Number'))
  ThisView{prop:order} = p_web.CleanFilter(ThisView,clip(loc:vorder))
      loc:FilterWas = 'shi:Ref_Number = ' & p_web.GSV('sto:Ref_Number')
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseStockHistory',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseStockHistory_Filter')
    p_web.SetSessionValue('BrowseStockHistory_FirstValue','')
    p_web.SetSessionValue('BrowseStockHistory_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,STOHIST,shi:record_number_key,loc:PageRows,'BrowseStockHistory',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,loc:ViewOptions,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled,Loc:LocatorCase)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or Loc:LocatorType = Net:Date or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position or loc:LocatorType = Net:Date
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If STOHIST{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(STOHIST,loc:firstvalue)
              Reset(ThisView,STOHIST)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If STOHIST{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(STOHIST,loc:lastvalue)
            Reset(ThisView,STOHIST)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      locDateIn = ''
      locDateOut = ''
      locJobRetail = 'Stock'
      
      CASE shi:Transaction_Type
      OF 'ADD' OROF 'REC'
          locDateIn = shi:Date
      
      OF 'DEC'
          locDateOut = shi:Date
      
      
      END
      
      IF (shi:Job_Number > 0)
          locJobRetail = shi:Job_Number
      END
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(shi:Record_Number)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td class="'&p_web.combine(p_web.site.style.BrowseEmpty,)&'">'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:NavButtonPosition=Net:Above or (loc:NavButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseStockHistory_nav_a" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,' nt-left nt-margin-right',)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseStockHistory.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseStockHistory.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseStockHistory.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseStockHistory.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseStockHistory_nav_a','buttonset',loc:options)
          End
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:UpdateButtonPosition=Net:Above or (loc:UpdateButtonPosition=Net:Both and loc:found > 0))
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseStockHistory',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseStockHistory_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseStockHistory_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          case loc:locatortype
          of Net:Position
          orof Net:Date
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
          of Net:Contains
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
          of Net:Begins
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
          End
          packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
          Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
          Case Loc:LocatorType
          of Net:Date
            packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator1BrowseStockHistory','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('BrowseStockHistory_LocatorPic'),,,'onchange="BrowseStockHistory.locate(''Locator1BrowseStockHistory'',this.value);"',,0,,,) & '</div></td>'
          Else
            packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator1BrowseStockHistory',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="BrowseStockHistory.locate(''Locator1BrowseStockHistory'',this.value);" ',,,,,,) & '</td>'
          End
          If loc:LocatorSearchButton or loc:LocatorClearButton
            packet = clip(packet) & '<td><div id="BrowseStockHistory_locate_b" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
            If loc:LocatorSearchButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
            End
            If loc:LocatorClearButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseStockHistory.cl(''BrowseStockHistory'');')
            End
            packet = clip(packet) & '</div><13,10></td><13,10>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
          If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseStockHistory_locate_b','buttonset',loc:options)
          End
      End
    End
  End
  p_web.SetSessionValue('BrowseStockHistory_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseStockHistory_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:NavButtonPosition=Net:Below or loc:NavButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseStockHistory_nav_b" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,' nt-left nt-margin-right',)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseStockHistory.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseStockHistory.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseStockHistory.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseStockHistory.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseStockHistory_nav_b','buttonset',loc:options)
          End
        end
        do SendPacket
  End
  If loc:UpdateButtonPosition=Net:Below or loc:UpdateButtonPosition=Net:Both
  End
  If loc:selecting = 0 and loc:parent = ''
      if loc:popup
        packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:BrowseCloseButton,'BrowseStockHistory',,,loc:popup)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCloseButton,loc:Formname,loc:CloseAction)
      end
  End
    do SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
  data
loc:rowclick  string(1024)
  code
    loc:field = p_web.AddBrowseValue('BrowseStockHistory','STOHIST',shi:record_number_key) !shi:Record_Number
    p_web._thisrow = p_web._nocolon('shi:Record_Number')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if Loc:LocatorValue <> '' and loc:ActualSelection = ''
        loc:checked = 'checked'
        do SetSelection
      elsif loc:ActualSelection = '' and shi:Record_Number = p_web.GetValue('shi:Record_Number')
         loc:checked = 'checked'
         do SetSelection
      elsif loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseStockHistory:LookupField')) = shi:Record_Number and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      else
        loc:checked = Choose((shi:Record_Number = loc:selected) and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      end
      If(loc:SelectionMethod  = Net:Radio)
        loc:RowStyle = 'class="' & p_web.combine(p_web.site.style.browserow,) &'"'
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'data-nt-id="'& p_web.AddBrowseValue('BrowseStockHistory','STOHIST',shi:record_number_key) &'" onclick="'
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If STOHIST{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(STOHIST)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            loc:DefaultSelection = loc:field
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If STOHIST{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(STOHIST)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            loc:DefaultSelection = loc:field
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'>'&p_web.CreateInput('radio','_bidv_',clip(loc:field),,loc:checked,'')&'</td>'&CRLF
          If loc:DefaultSelection = ''
            loc:DefaultSelection = loc:field
          End
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          If loc:DefaultSelection = '' or loc:direction < 0
            loc:DefaultSelection = loc:field
          End
        End
    end ! loc:eip = 0
      IF (loc:Checked = 'checked')
      END ! IF (loc:Checked = 'checked')
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('SmallTextRight')&'"><13,10>'
          end ! loc:eip = 0
          do value::locdateIn
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('SmallTextRight')&'"><13,10>'
          end ! loc:eip = 0
          do value::locDateOut
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('SmallTextRight')&'"><13,10>'
          end ! loc:eip = 0
          do value::shi:Quantity
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('SmallText')&'"><13,10>'
          end ! loc:eip = 0
          do value::locJobRetail
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('SmallTextRight')&'"><13,10>'
          end ! loc:eip = 0
          do value::shi:Purchase_Cost
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('SmallTextRight')&'"><13,10>'
          end ! loc:eip = 0
          do value::shi:Sale_Cost
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('SmallTextCenter')&'"><13,10>'
          end ! loc:eip = 0
          do value::shi:User
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('SmallText')&'" width="'&clip(150)&'"><13,10>'
          end ! loc:eip = 0
          do value::shi:Notes
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('SmallText')&'" width="'&clip(200)&'"><13,10>'
          end ! loc:eip = 0
          do value::shi:Information
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('SmallTextCenter')&'"><13,10>'
          end ! loc:eip = 0
          do value::shi:StockOnHand
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr ' & clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Idx = p_web.AddBrowseValue('BrowseStockHistory','STOHIST',shi:record_number_key)
  TableQueue.Id[1] = shi:Record_Number

ClosingScripts  Routine
  data
Rtn_SecwinProcedureName         string(252)
  code
  If p_web.RequestAjax = 0
    do SetFormAction
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var btiBrowseStockHistory;if (btiBrowseStockHistory != 1){{var BrowseStockHistory=new browseTable(''BrowseStockHistory'','''&clip(loc:formname)&|
      ''','''&p_web._jsok('shi:Record_Number',Net:Parameter)&''',2,'''&clip(loc:divname)&''',1,1,1,'''&|
      clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&|
      p_web.Translate('Are you sure you want to delete this record?')&''','''&clip(loc:ActualSelection)&''','''&clip(loc:selectaction)&''','''&|
      clip(loc:formactiontarget)&''','''','&loc:popup&',0,'''','''',"' & clip(Rtn_SecwinProcedureName) & '");<13,10>'&|
      'BrowseStockHistory.setGreenBar('''&p_web.ColorWeb(p_web.Site.Style.BrowseHighlightColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOneColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseTwoColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOverColor)&''');<13,10>' &|
      'BrowseStockHistory.applyGreenBar();btiBrowseStockHistory=1};<13,10>' &|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseStockHistory')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseStockHistory')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseStockHistory')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseStockHistory')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(STOHIST)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(STOHIST)
  Bind(shi:Record)
  Clear(shi:Record)
  NetWebSetSessionPics(p_web,STOHIST)

Children Routine
  if loc:selecting = 0
    If p_web.RequestAjax = 0
      do StartChildren
    Else
      do AjaxChildren
    End
  end

AjaxChildren  Routine
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue('shi:Record_Number',p_web.GetValue('shi:Record_Number'))
   p_web.DeleteValue('_Clicked')
   loc:found = 1
   do Children
! ----------------------------------------------------------------------------------------
CallRow  Routine
  data
loc:result  long
  code
  do OpenFilesB
  shi:Record_Number = p_web.GSV('shi:Record_Number')
  loc:result = p_web._GetFile(STOHIST,shi:record_number_key)
  loc:eip = 1
  loc:viewstate = p_web.escape(p_web.Base64Encode(clip(shi:Record_Number)))
  do BrowseRow
  do SendPacket
  do ClosefilesB

! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(STOHIST)
  Case upper(p_web.GetValue('_EIPClm'))
  Else
    case p_web.GetValue('_action_')
    of Net:InsertRecord
    orof Net:ChangeRecord
    orof Net:DeleteRecord
    orof Net:CopyRecord
      loc:eip = 0
      do CallBrowse
    end
  End
  do GotFocusBack
  p_web._CloseFile(STOHIST)
! ----------------------------------------------------------------------------------------
value::locdateIn   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseStockHistory_locdateIn_'&shi:Record_Number,'SmallTextRight',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(locDateIn,'@d06b')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::locDateOut   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseStockHistory_locDateOut_'&shi:Record_Number,'SmallTextRight',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(locDateOut,'@d06b')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::shi:Quantity   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseStockHistory_shi:Quantity_'&shi:Record_Number,'SmallTextRight',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(shi:Quantity,'@n8')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::locJobRetail   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseStockHistory_locJobRetail_'&shi:Record_Number,'SmallText',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(locJobRetail,'@s10')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::shi:Purchase_Cost   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseStockHistory_shi:Purchase_Cost_'&shi:Record_Number,'SmallTextRight',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(shi:Purchase_Cost,'@n-14.2')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::shi:Sale_Cost   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseStockHistory_shi:Sale_Cost_'&shi:Record_Number,'SmallTextRight',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(shi:Sale_Cost,'@n-14.2')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::shi:User   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseStockHistory_shi:User_'&shi:Record_Number,'SmallTextCenter',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(shi:User,'@s3')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::shi:Notes   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseStockHistory_shi:Notes_'&shi:Record_Number,'SmallText',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(shi:Notes,'@s255')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::shi:Information   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseStockHistory_shi:Information_'&shi:Record_Number,'SmallText',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(shi:Information,'@s255')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::shi:StockOnHand   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseStockHistory_shi:StockOnHand_'&shi:Record_Number,'SmallTextCenter',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(shi:StockOnHand,'@s8')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END
  return
!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
CheckForDuplicate  Routine
SetSelection  Routine
  loc:ActualSelection = loc:field
  p_web.SetSessionValue('shi:Record_Number',shi:Record_Number)

MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:ActualSelection = ''
    loc:ActualSelection = loc:DefaultSelection
    p_web.GetBrowseValue(loc:ActualSelection,Net:Web:SessionQueue) ! so children are primed with correct sessionValue
  End

  If loc:ActualSelection <> ''
    TableQueue.Kind = Net:RowTable
    get(TableQueue,TableQueue.Kind)
    if Errorcode() = 0
      TableQueue.Row = sub(TableQueue.Row,1,len(clip(TableQueue.Row))-1) &  ' data-nt-rows="'&loc:RowsHigh&'" data-nt-value="'&clip(loc:ActualSelection)&'"' & '>'
      Put(TableQueue)
    End
  End

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead class="'&p_web.combine(p_web.site.style.BrowseHeader,'')&'"><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot class="'&p_web.combine(p_web.site.style.BrowseFooter,)&'"><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody class="'&p_web.combine(p_web.site.style.BrowseBody,)&'"><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table><13,10>' & p_web.DivFooter(Net:NoSend) ! Table Div
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
FormStockOrder       PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locQuantity          LONG                                  !
locSupplier          STRING(30)                            !
locComments          STRING(255)                           !
locUsage0_7          LONG                                  !
locUsage0_30         LONG                                  !
locUsage31_60        LONG                                  !
locUsage61_90        LONG                                  !
locAverageDailyUse   LONG                                  !
locAverageText       STRING(30)                            !
FilesOpened     Long
SUPPLIER::State  USHORT
STOCK::State  USHORT
sto:Part_Number:IsInvalid  Long
txtUsage:IsInvalid  Long
sto:Description:IsInvalid  Long
locUsage0_7:IsInvalid  Long
locQuantity:IsInvalid  Long
locUsage0_30:IsInvalid  Long
locComments:IsInvalid  Long
locUsage31_60:IsInvalid  Long
locUsage61_90:IsInvalid  Long
locAverageText:IsInvalid  Long
sto:Sale_Cost:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormStockOrder')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'FormStockOrder_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('FormStockOrder','')
    p_web.DivHeader('FormStockOrder',p_web.combine(p_web.site.style.formdiv,))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('FormStockOrder') = 0
        p_web.AddPreCall('FormStockOrder')
        p_web.DivHeader('popup_FormStockOrder','nt-hidden')
        p_web.DivHeader('FormStockOrder',p_web.combine(p_web.site.style.formdiv,))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_FormStockOrder_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_FormStockOrder_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormStockOrder',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_FormStockOrder',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormStockOrder',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_FormStockOrder',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormStockOrder',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormStockOrder',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_FormStockOrder',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormStockOrder',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_FormStockOrder',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormStockOrder',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormStockOrder',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormStockOrder',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('FormStockOrder')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(SUPPLIER)
  p_web._OpenFile(STOCK)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(SUPPLIER)
  p_Web._CloseFile(STOCK)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  ! Clear values
  p_web.SSV('locQuantity',1)
  p_web.SSV('locComments','')
  
  Access:STOCK.ClearKey(sto:Ref_Number_Key)
  sto:Ref_Number = p_web.GSV('sto:Ref_Number')
  IF (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
      p_web.FileToSessionQueue(STOCK)
  
  
      VodacomClass.StockUsage(sto:Ref_Number,locUsage0_7,locUsage0_30, |
          locUsage31_60,locUsage61_90,locAverageDailyUse)
  
      If locAverageDailyUse < 1
          locAverageText = '< 1'
      Else!If average_temp < 1
          locAverageText = Int(locAverageDailyUse)
      End!If average_temp < 1
  
      p_web.SSV('locUsage0_7',locUsage0_7)
      p_web.SSV('locUsage0_30',locUsage0_30)
      p_web.SSV('locUsage31_60',locUsage31_60)
      p_web.SSV('locUsage61_90',locUsage61_90)
      p_web.SSV('locAverageText',locAverageText)
  
  END
  p_web.SetValue('FormStockOrder_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'FormStockOrder'
    end
    p_web.formsettings.proc = 'FormStockOrder'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('sto:Part_Number')
    p_web.SetPicture('sto:Part_Number','@s30')
  End
  p_web.SetSessionPicture('sto:Part_Number','@s30')
  If p_web.IfExistsValue('sto:Description')
    p_web.SetPicture('sto:Description','@s30')
  End
  p_web.SetSessionPicture('sto:Description','@s30')
  If p_web.IfExistsValue('locQuantity')
    p_web.SetPicture('locQuantity','@n_7')
  End
  p_web.SetSessionPicture('locQuantity','@n_7')
  If p_web.IfExistsValue('sto:Sale_Cost')
    p_web.SetPicture('sto:Sale_Cost','@n_14.2')
  End
  p_web.SetSessionPicture('sto:Sale_Cost','@n_14.2')

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  if p_web.IfExistsValue('sto:Part_Number') = 0
    p_web.SetSessionValue('sto:Part_Number',sto:Part_Number)
  Else
    sto:Part_Number = p_web.GetSessionValue('sto:Part_Number')
  End
  if p_web.IfExistsValue('sto:Description') = 0
    p_web.SetSessionValue('sto:Description',sto:Description)
  Else
    sto:Description = p_web.GetSessionValue('sto:Description')
  End
  if p_web.IfExistsValue('locUsage0_7') = 0
    p_web.SetSessionValue('locUsage0_7',locUsage0_7)
  Else
    locUsage0_7 = p_web.GetSessionValue('locUsage0_7')
  End
  if p_web.IfExistsValue('locQuantity') = 0
    p_web.SetSessionValue('locQuantity',locQuantity)
  Else
    locQuantity = p_web.GetSessionValue('locQuantity')
  End
  if p_web.IfExistsValue('locUsage0_30') = 0
    p_web.SetSessionValue('locUsage0_30',locUsage0_30)
  Else
    locUsage0_30 = p_web.GetSessionValue('locUsage0_30')
  End
  if p_web.IfExistsValue('locComments') = 0
    p_web.SetSessionValue('locComments',locComments)
  Else
    locComments = p_web.GetSessionValue('locComments')
  End
  if p_web.IfExistsValue('locUsage31_60') = 0
    p_web.SetSessionValue('locUsage31_60',locUsage31_60)
  Else
    locUsage31_60 = p_web.GetSessionValue('locUsage31_60')
  End
  if p_web.IfExistsValue('locUsage61_90') = 0
    p_web.SetSessionValue('locUsage61_90',locUsage61_90)
  Else
    locUsage61_90 = p_web.GetSessionValue('locUsage61_90')
  End
  if p_web.IfExistsValue('locAverageText') = 0
    p_web.SetSessionValue('locAverageText',locAverageText)
  Else
    locAverageText = p_web.GetSessionValue('locAverageText')
  End
  if p_web.IfExistsValue('sto:Sale_Cost') = 0
    p_web.SetSessionValue('sto:Sale_Cost',sto:Sale_Cost)
  Else
    sto:Sale_Cost = p_web.GetSessionValue('sto:Sale_Cost')
  End

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('sto:Part_Number')
    sto:Part_Number = p_web.GetValue('sto:Part_Number')
    p_web.SetSessionValue('sto:Part_Number',sto:Part_Number)
  Else
    sto:Part_Number = p_web.GetSessionValue('sto:Part_Number')
  End
  if p_web.IfExistsValue('sto:Description')
    sto:Description = p_web.GetValue('sto:Description')
    p_web.SetSessionValue('sto:Description',sto:Description)
  Else
    sto:Description = p_web.GetSessionValue('sto:Description')
  End
  if p_web.IfExistsValue('locUsage0_7')
    locUsage0_7 = p_web.GetValue('locUsage0_7')
    p_web.SetSessionValue('locUsage0_7',locUsage0_7)
  Else
    locUsage0_7 = p_web.GetSessionValue('locUsage0_7')
  End
  if p_web.IfExistsValue('locQuantity')
    locQuantity = p_web.dformat(clip(p_web.GetValue('locQuantity')),'@n_7')
    p_web.SetSessionValue('locQuantity',locQuantity)
  Else
    locQuantity = p_web.GetSessionValue('locQuantity')
  End
  if p_web.IfExistsValue('locUsage0_30')
    locUsage0_30 = p_web.GetValue('locUsage0_30')
    p_web.SetSessionValue('locUsage0_30',locUsage0_30)
  Else
    locUsage0_30 = p_web.GetSessionValue('locUsage0_30')
  End
  if p_web.IfExistsValue('locComments')
    locComments = p_web.GetValue('locComments')
    p_web.SetSessionValue('locComments',locComments)
  Else
    locComments = p_web.GetSessionValue('locComments')
  End
  if p_web.IfExistsValue('locUsage31_60')
    locUsage31_60 = p_web.GetValue('locUsage31_60')
    p_web.SetSessionValue('locUsage31_60',locUsage31_60)
  Else
    locUsage31_60 = p_web.GetSessionValue('locUsage31_60')
  End
  if p_web.IfExistsValue('locUsage61_90')
    locUsage61_90 = p_web.GetValue('locUsage61_90')
    p_web.SetSessionValue('locUsage61_90',locUsage61_90)
  Else
    locUsage61_90 = p_web.GetSessionValue('locUsage61_90')
  End
  if p_web.IfExistsValue('locAverageText')
    locAverageText = p_web.GetValue('locAverageText')
    p_web.SetSessionValue('locAverageText',locAverageText)
  Else
    locAverageText = p_web.GetSessionValue('locAverageText')
  End
  if p_web.IfExistsValue('sto:Sale_Cost')
    sto:Sale_Cost = p_web.dformat(clip(p_web.GetValue('sto:Sale_Cost')),'@n_14.2')
    p_web.SetSessionValue('sto:Sale_Cost',sto:Sale_Cost)
  Else
    sto:Sale_Cost = p_web.GetSessionValue('sto:Sale_Cost')
  End

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('FormStockOrder_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.GSV('ReturnURL')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormStockOrder_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormStockOrder_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormStockOrder_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = p_web.GSV('ReturnURL')

GenerateForm   Routine
  do LoadRelatedRecords
  ! Store return URL
  IF (p_web.IfExistsValue('ReturnURL'))
      p_web.StoreValue('ReturnURL')
  END
  
 locUsage0_7 = p_web.RestoreValue('locUsage0_7')
 locQuantity = p_web.RestoreValue('locQuantity')
 locUsage0_30 = p_web.RestoreValue('locUsage0_30')
 locComments = p_web.RestoreValue('locComments')
 locUsage31_60 = p_web.RestoreValue('locUsage31_60')
 locUsage61_90 = p_web.RestoreValue('locUsage61_90')
 locAverageText = p_web.RestoreValue('locAverageText')
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Order Stock') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Order Stock',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_FormStockOrder',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_FormStockOrder0_div')&'">'&p_web.Translate('Part Details')&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="FormStockOrder_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.cancel();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CancelButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="FormStockOrder_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'FormStockOrder_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="FormStockOrder_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'FormStockOrder_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locQuantity')
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_FormStockOrder')>0,p_web.GSV('showtab_FormStockOrder'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_FormStockOrder'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_FormStockOrder') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_FormStockOrder'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_FormStockOrder')>0,p_web.GSV('showtab_FormStockOrder'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_FormStockOrder') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Part Details') & ''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_FormStockOrder_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_FormStockOrder')>0,p_web.GSV('showtab_FormStockOrder'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"FormStockOrder",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_FormStockOrder')>0,p_web.GSV('showtab_FormStockOrder'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_FormStockOrder_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('FormStockOrder') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('FormStockOrder')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Part Details')&'</a></h3>' & CRLF & p_web.DivHeader('tab_FormStockOrder0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_FormStockOrder0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_FormStockOrder0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_FormStockOrder0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Part Details')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_FormStockOrder0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Part Details')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_FormStockOrder0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Part Details')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_FormStockOrder0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::sto:Part_Number
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::sto:Part_Number
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::txtUsage
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::txtUsage
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::sto:Description
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::sto:Description
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locUsage0_7
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locUsage0_7
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locQuantity
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locQuantity
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locUsage0_30
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locUsage0_30
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td rowspan="'&clip(3)&'" valign="top"'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locComments
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td rowspan="'&clip(3)&'"'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locComments
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locUsage31_60
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locUsage31_60
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locUsage61_90
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locUsage61_90
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locAverageText
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locAverageText
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::sto:Sale_Cost
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::sto:Sale_Cost
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Prompt::sto:Part_Number  Routine
  packet = clip(packet) & p_web.DivHeader('FormStockOrder_' & p_web._nocolon('sto:Part_Number') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Part Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::sto:Part_Number  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    sto:Part_Number = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    sto:Part_Number = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::sto:Part_Number  ! copies value to session value if valid.

ValidateValue::sto:Part_Number  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('sto:Part_Number',sto:Part_Number).
    End

Value::sto:Part_Number  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormStockOrder_' & p_web._nocolon('sto:Part_Number') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- sto:Part_Number
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('sto:Part_Number'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::txtUsage  Routine
  packet = clip(packet) & p_web.DivHeader('FormStockOrder_' & p_web._nocolon('txtUsage') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Usage'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::txtUsage  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::txtUsage  ! copies value to session value if valid.

ValidateValue::txtUsage  Routine
    If not (1=0)
    End

Value::txtUsage  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormStockOrder_' & p_web._nocolon('txtUsage') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::sto:Description  Routine
  packet = clip(packet) & p_web.DivHeader('FormStockOrder_' & p_web._nocolon('sto:Description') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Description'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::sto:Description  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    sto:Description = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    sto:Description = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::sto:Description  ! copies value to session value if valid.

ValidateValue::sto:Description  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('sto:Description',sto:Description).
    End

Value::sto:Description  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormStockOrder_' & p_web._nocolon('sto:Description') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- sto:Description
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('sto:Description'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::locUsage0_7  Routine
  packet = clip(packet) & p_web.DivHeader('FormStockOrder_' & p_web._nocolon('locUsage0_7') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('0 - 7 Days'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locUsage0_7  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locUsage0_7 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locUsage0_7 = p_web.GetValue('Value')
  End
  do ValidateValue::locUsage0_7  ! copies value to session value if valid.

ValidateValue::locUsage0_7  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locUsage0_7',locUsage0_7).
    End

Value::locUsage0_7  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormStockOrder_' & p_web._nocolon('locUsage0_7') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- locUsage0_7
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locUsage0_7'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::locQuantity  Routine
  packet = clip(packet) & p_web.DivHeader('FormStockOrder_' & p_web._nocolon('locQuantity') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Quantity'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locQuantity  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locQuantity = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n_7'  !FieldPicture = 
    locQuantity = p_web.Dformat(p_web.GetValue('Value'),'@n_7')
  End
  do ValidateValue::locQuantity  ! copies value to session value if valid.
  do Value::locQuantity
  do SendAlert

ValidateValue::locQuantity  Routine
    If not (1=0)
  If locQuantity = ''
    loc:Invalid = 'locQuantity'
    locQuantity:IsInvalid = true
    loc:alert = p_web.translate('Quantity') & ' ' & p_web.site.RequiredText
  End
      if loc:invalid = '' then p_web.SetSessionValue('locQuantity',locQuantity).
    End

Value::locQuantity  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormStockOrder_' & p_web._nocolon('locQuantity') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  If loc:retrying
    locQuantity = p_web.RestoreValue('locQuantity')
    do ValidateValue::locQuantity
    If locQuantity:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- locQuantity
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locQuantity'',''formstockorder_locquantity_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locQuantity',p_web.GetSessionValue('locQuantity'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n_7',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::locUsage0_30  Routine
  packet = clip(packet) & p_web.DivHeader('FormStockOrder_' & p_web._nocolon('locUsage0_30') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('0 - 30 Days'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locUsage0_30  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locUsage0_30 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locUsage0_30 = p_web.GetValue('Value')
  End
  do ValidateValue::locUsage0_30  ! copies value to session value if valid.

ValidateValue::locUsage0_30  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locUsage0_30',locUsage0_30).
    End

Value::locUsage0_30  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormStockOrder_' & p_web._nocolon('locUsage0_30') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- locUsage0_30
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locUsage0_30'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::locComments  Routine
  packet = clip(packet) & p_web.DivHeader('FormStockOrder_' & p_web._nocolon('locComments') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Comments'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locComments  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locComments = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locComments = p_web.GetValue('Value')
  End
  do ValidateValue::locComments  ! copies value to session value if valid.
  do Value::locComments
  do SendAlert

ValidateValue::locComments  Routine
    If not (1=0)
    locComments = Upper(locComments)
      if loc:invalid = '' then p_web.SetSessionValue('locComments',locComments).
    End

Value::locComments  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormStockOrder_' & p_web._nocolon('locComments') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    locComments = p_web.RestoreValue('locComments')
    do ValidateValue::locComments
    If locComments:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- TEXT --- locComments
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locComments'',''formstockorder_loccomments_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'readonly','')
  do SendPacket
  p_web.CreateTextArea('locComments',p_web.GetSessionValue('locComments'),5,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(locComments),,,,Net:Send)
  do SendPacket
  End
  p_web.DivFooter()

Prompt::locUsage31_60  Routine
  packet = clip(packet) & p_web.DivHeader('FormStockOrder_' & p_web._nocolon('locUsage31_60') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('31 - 60 Days'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locUsage31_60  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locUsage31_60 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locUsage31_60 = p_web.GetValue('Value')
  End
  do ValidateValue::locUsage31_60  ! copies value to session value if valid.

ValidateValue::locUsage31_60  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locUsage31_60',locUsage31_60).
    End

Value::locUsage31_60  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormStockOrder_' & p_web._nocolon('locUsage31_60') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- locUsage31_60
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locUsage31_60'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::locUsage61_90  Routine
  packet = clip(packet) & p_web.DivHeader('FormStockOrder_' & p_web._nocolon('locUsage61_90') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('61 - 90 Days'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locUsage61_90  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locUsage61_90 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locUsage61_90 = p_web.GetValue('Value')
  End
  do ValidateValue::locUsage61_90  ! copies value to session value if valid.

ValidateValue::locUsage61_90  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locUsage61_90',locUsage61_90).
    End

Value::locUsage61_90  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormStockOrder_' & p_web._nocolon('locUsage61_90') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- locUsage61_90
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locUsage61_90'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::locAverageText  Routine
  packet = clip(packet) & p_web.DivHeader('FormStockOrder_' & p_web._nocolon('locAverageText') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Average Daily Use'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locAverageText  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locAverageText = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locAverageText = p_web.GetValue('Value')
  End
  do ValidateValue::locAverageText  ! copies value to session value if valid.

ValidateValue::locAverageText  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locAverageText',locAverageText).
    End

Value::locAverageText  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormStockOrder_' & p_web._nocolon('locAverageText') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- locAverageText
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locAverageText'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::sto:Sale_Cost  Routine
  packet = clip(packet) & p_web.DivHeader('FormStockOrder_' & p_web._nocolon('sto:Sale_Cost') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Current Price'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::sto:Sale_Cost  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    sto:Sale_Cost = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n_14.2'  !FieldPicture = @n14.2
    sto:Sale_Cost = p_web.Dformat(p_web.GetValue('Value'),'@n_14.2')
  End
  do ValidateValue::sto:Sale_Cost  ! copies value to session value if valid.

ValidateValue::sto:Sale_Cost  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('sto:Sale_Cost',sto:Sale_Cost).
    End

Value::sto:Sale_Cost  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormStockOrder_' & p_web._nocolon('sto:Sale_Cost') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- sto:Sale_Cost
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(format(p_web.GetSessionValue('sto:Sale_Cost'),'@n_14.2')) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('FormStockOrder_nexttab_' & 0)
    sto:Part_Number = p_web.GSV('sto:Part_Number')
    do ValidateValue::sto:Part_Number
    If loc:Invalid
      loc:retrying = 1
      do Value::sto:Part_Number
      !do SendAlert
      !exit
    End
    sto:Description = p_web.GSV('sto:Description')
    do ValidateValue::sto:Description
    If loc:Invalid
      loc:retrying = 1
      do Value::sto:Description
      !do SendAlert
      !exit
    End
    locUsage0_7 = p_web.GSV('locUsage0_7')
    do ValidateValue::locUsage0_7
    If loc:Invalid
      loc:retrying = 1
      do Value::locUsage0_7
      !do SendAlert
      !exit
    End
    locQuantity = p_web.GSV('locQuantity')
    do ValidateValue::locQuantity
    If loc:Invalid
      loc:retrying = 1
      do Value::locQuantity
      !do SendAlert
      !exit
    End
    locUsage0_30 = p_web.GSV('locUsage0_30')
    do ValidateValue::locUsage0_30
    If loc:Invalid
      loc:retrying = 1
      do Value::locUsage0_30
      !do SendAlert
      !exit
    End
    locComments = p_web.GSV('locComments')
    do ValidateValue::locComments
    If loc:Invalid
      loc:retrying = 1
      do Value::locComments
      !do SendAlert
      !exit
    End
    locUsage31_60 = p_web.GSV('locUsage31_60')
    do ValidateValue::locUsage31_60
    If loc:Invalid
      loc:retrying = 1
      do Value::locUsage31_60
      !do SendAlert
      !exit
    End
    locUsage61_90 = p_web.GSV('locUsage61_90')
    do ValidateValue::locUsage61_90
    If loc:Invalid
      loc:retrying = 1
      do Value::locUsage61_90
      !do SendAlert
      !exit
    End
    locAverageText = p_web.GSV('locAverageText')
    do ValidateValue::locAverageText
    If loc:Invalid
      loc:retrying = 1
      do Value::locAverageText
      !do SendAlert
      !exit
    End
    sto:Sale_Cost = p_web.GSV('sto:Sale_Cost')
    do ValidateValue::sto:Sale_Cost
    If loc:Invalid
      loc:retrying = 1
      do Value::sto:Sale_Cost
      !do SendAlert
      !exit
    End
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_FormStockOrder_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('FormStockOrder_tab_' & 0)
    do GenerateTab0
  of lower('FormStockOrder_locQuantity_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locQuantity
      of event:timer
        do Value::locQuantity
      else
        do Value::locQuantity
      end
  of lower('FormStockOrder_locComments_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locComments
      of event:timer
        do Value::locComments
      else
        do Value::locComments
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('FormStockOrder_form:ready_',1)

  p_web.SetSessionValue('FormStockOrder_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_FormStockOrder',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('FormStockOrder_form:ready_',1)
  p_web.SetSessionValue('FormStockOrder_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormStockOrder',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('FormStockOrder_form:ready_',1)
  p_web.SetSessionValue('FormStockOrder_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('FormStockOrder:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('FormStockOrder_form:ready_',1)
  p_web.SetSessionValue('FormStockOrder_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('FormStockOrder:Primed',0)
  p_web.setsessionvalue('showtab_FormStockOrder',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
      If not (1=0)
          If p_web.IfExistsValue('locQuantity')
            locQuantity = p_web.GetValue('locQuantity')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('locComments')
            locComments = p_web.GetValue('locComments')
          End
      End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormStockOrder_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  ! Create Order
  IF (p_web.GSV('sto:Ref_Number') <> '')
      IF (p_web.GSV('sto:Sundry_Item') = 'YES')
          loc:Alert = 'You cannot order Sundry Items.'
          loc:Invalid = 'sto:Part_Number'
          EXIT
      END
  
      createWebOrder(p_web.GSV('BookingAccount'),p_web.GSV('sto:Part_Number'),p_web.GSV('sto:Description'), |
          p_web.GSV('locQuantity'),p_web.GSV('sto:Retail_Cost'))
  END
  p_web.DeleteSessionValue('FormStockOrder_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
    do ValidateValue::sto:Part_Number
    If loc:Invalid then exit.
    do ValidateValue::txtUsage
    If loc:Invalid then exit.
    do ValidateValue::sto:Description
    If loc:Invalid then exit.
    do ValidateValue::locUsage0_7
    If loc:Invalid then exit.
    do ValidateValue::locQuantity
    If loc:Invalid then exit.
    do ValidateValue::locUsage0_30
    If loc:Invalid then exit.
    do ValidateValue::locComments
    If loc:Invalid then exit.
    do ValidateValue::locUsage31_60
    If loc:Invalid then exit.
    do ValidateValue::locUsage61_90
    If loc:Invalid then exit.
    do ValidateValue::locAverageText
    If loc:Invalid then exit.
    do ValidateValue::sto:Sale_Cost
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('FormStockOrder:Primed',0)
  p_web.StoreValue('sto:Part_Number')
  p_web.StoreValue('')
  p_web.StoreValue('sto:Description')
  p_web.StoreValue('locUsage0_7')
  p_web.StoreValue('locQuantity')
  p_web.StoreValue('locUsage0_30')
  p_web.StoreValue('locComments')
  p_web.StoreValue('locUsage31_60')
  p_web.StoreValue('locUsage61_90')
  p_web.StoreValue('locAverageText')
  p_web.StoreValue('sto:Sale_Cost')

FormReturnStock      PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locReturnType        STRING(30)                            !
locInvoiceNumber     STRING(30)                            !
locSalesNumber       STRING(30)                            !
locQuantity          LONG                                  !
locNotes             STRING(255)                           !
locDateReceived      DATE                                  !
FilesOpened     Long
RTNORDER::State  USHORT
SUBTRACC::State  USHORT
TRADEACC::State  USHORT
RETSALES::State  USHORT
RETTYPES::State  USHORT
STOCK::State  USHORT
sto:Part_Number:IsInvalid  Long
sto:Description:IsInvalid  Long
locReturnType:IsInvalid  Long
locInvoiceNumber:IsInvalid  Long
locSalesNumber:IsInvalid  Long
locQuantity:IsInvalid  Long
locNotes:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
locReturnType_OptionView   View(RETTYPES)
                          Project(rtt:Description)
                        End
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  GlobalErrors.SetProcedureName('FormReturnStock')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'FormReturnStock_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('FormReturnStock','')
    p_web.DivHeader('FormReturnStock',p_web.combine(p_web.site.style.formdiv,))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('FormReturnStock') = 0
        p_web.AddPreCall('FormReturnStock')
        p_web.DivHeader('popup_FormReturnStock','nt-hidden')
        p_web.DivHeader('FormReturnStock',p_web.combine(p_web.site.style.formdiv,))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_FormReturnStock_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_FormReturnStock_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormReturnStock',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_FormReturnStock',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormReturnStock',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_FormReturnStock',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormReturnStock',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormReturnStock',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_FormReturnStock',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormReturnStock',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_FormReturnStock',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormReturnStock',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormReturnStock',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormReturnStock',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('FormReturnStock')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
DeleteSessionValues ROUTINE ! Auto generated by Bryan's Template
    ! Local Variables
    p_web.DeleteSessionValue('Ans')
    p_web.DeleteSessionValue('locReturnType')
    p_web.DeleteSessionValue('locInvoiceNumber')
    p_web.DeleteSessionValue('locSalesNumber')
    p_web.DeleteSessionValue('locQuantity')
    p_web.DeleteSessionValue('locNotes')
    p_web.DeleteSessionValue('locDateReceived')

    ! Other Variables
    p_web.DeleteSessionValue('Prompt:locInvoiceNumber')
    p_web.DeleteSessionValue('Prompt:locQuantity')
Validate:locInvoiceNumber   ROUTINE
DATA
lReturnType        EQUATE('locReturnType')
lInvoiceNumber EQUATE('locInvoiceNumber')
lSalesNumber EQUATE('locSalesNumber')
lQuantity   EQUATE('locQuantity')
lQuantityHigh       EQUATE('locQuantityHigh')
Prompt:lInvoiceNumber       EQUATE('Prompt:locInvoiceNumber')
Prompt:lQuantity    EQUATE('Prompt:locQuantity')
CODE

    p_web.SSV(lQuantity,0)
    p_web.SSV(lSalesNumber,0)
    p_web.SSV(lQuantityHigh,0)
    p_web.SSV(Prompt:lInvoiceNumber,'')
    p_web.SSV(Prompt:lQuantity,'')

    Access:RETSALES.Clearkey(ret:Invoice_Number_Key)
    ret:Invoice_Number = p_web.GSV(lInvoiceNumber)
    IF (Access:RETSALES.TryFetch(ret:Invoice_Number_Key))
        p_web.SSV(Prompt:lInvoiceNumber,'Unable to find the selected Invoice Number')
        EXIT
    END

    DoNormalCheck# = 0
    If glo:WebJob
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = p_web.GSV('BookingAccountNumber')
        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Found
            If tra:StoresAccount <> ''
                If tra:StoresAccount <> ret:Account_Number
                    Error# = 1
                End !If tra:StoresAccount <> ret:Account_Number
            Else !If tra:Stores_Account <> ''
                DoNormalCheck# = 1
            End !If tra:Stores_Account <> ''
        Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Error
        End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign

    End !If glo:WebJob

    If DoNormalCheck#
        !Look up Head Account!
        Access:SubTrAcc.ClearKey(sub:Account_Number_Key)
        sub:Account_Number = ret:Account_Number
        IF Access:SubTracc.Fetch(sub:Account_Number_Key)
          !Error!
        ELSE
            Access:TradeAcc.ClearKey(tra:Account_Number_Key)
            tra:Account_Number = sub:Main_Account_Number
            IF Access:TradeAcc.Fetch(tra:Account_Number_Key)
                !Error!
            ELSE
                IF glo:Location <> tra:SiteLocation
                    Error# = 1
                END
            END
        END

    End !If DoNormalCheck#

    If Error# = 1
        p_web.SSV(Prompt:lInvoiceNumber,'This invoice number is not for this site location.')
        EXIT
    END

    ! How many of this part were on the retail sale?
    locAvailableQty# = 0
    found# = 0
    Access:RETSTOCK.Clearkey(res:DespatchedPartKey)
    res:Ref_Number = ret:Ref_Number
    res:Despatched = 'YES'
    res:Part_Number = p_web.GSV('sto:Part_Number')
    SET(res:DespatchedPartKey,res:DespatchedPartKey)
    LOOP UNTIL Access:RETSTOCK.Next()
        IF (res:Ref_Number <> ret:Ref_Number OR |
            res:Despatched <> 'YES' OR |
            res:Part_Number <> p_web.GSV('sto:Part_Number'))
            BREAK
        END
        found# = 1
        IF (res:Received <> 1)
            CYCLE
        END
        locAvailableQty# += res:QuantityReceived
        ! Don't think this should happen, but received dates could all be different, so pick the newest
        IF (p_web.GSV('locDateReceived') < res:DateReceived)
            p_web.SSV('locDateReceived',res:DateReceived)
        END

    END
    IF (found# = 0)
        p_web.SSV(Prompt:lInvoiceNumber,'The selected part is not on the entered invoice.')
        EXIT
    END

    IF (locAvailableQty# = 0)
        p_web.SSV(Prompt:lInvoiceNumber,'The selected order has not yet been received.')
        EXIT
    END

    ! How many of this part have already been ordered
    locAvailableQty# -= VodacomClass.ReturnUnitAlreadyOnOrder(p_web.GSV('sto:Ref_Number'),0,ret:Ref_Number)

    ! Don't allow to return more than is in stock
    IF (locAvailableQty# > p_web.GSV('sto:Quantity_Stock'))
        locAvailableQty# = p_web.GSV('sto:Quantity_Stock')
    END

    IF (locAvailableQty# <= 0)
        p_web.SSV(Prompt:lInvoiceNumber,'All the available quantity for this part has already been returned.')
        EXIT
    ELSE
        p_web.SSV(lQuantityHigh,locAvailableQty#)

        p_web.SSV(Prompt:lQuantity,'Qty Available: ' & locAvailableQty#)
    END

    p_web.SSV(Prompt:lInvoiceNumber,'Invoice Found.')
    p_web.SSV(lSalesNumber,ret:Ref_Number)
OpenFiles  ROUTINE
  p_web._OpenFile(RTNORDER)
  p_web._OpenFile(SUBTRACC)
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(RETSALES)
  p_web._OpenFile(RETTYPES)
  p_web._OpenFile(STOCK)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(RTNORDER)
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(RETSALES)
  p_Web._CloseFile(RETTYPES)
  p_Web._CloseFile(STOCK)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  DO DeleteSessionvalues
  Access:STOCK.ClearKey(sto:Ref_Number_Key)
  sto:Ref_Number = p_web.GSV('sto:Ref_Number')
  IF (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
      p_web.FileToSessionQueue(STOCK)
  
  END
  p_web.SetValue('FormReturnStock_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'FormReturnStock'
    end
    p_web.formsettings.proc = 'FormReturnStock'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine
  DO DeleteSessionvalues

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('sto:Part_Number')
    p_web.SetPicture('sto:Part_Number','@s30')
  End
  p_web.SetSessionPicture('sto:Part_Number','@s30')
  If p_web.IfExistsValue('sto:Description')
    p_web.SetPicture('sto:Description','@s30')
  End
  p_web.SetSessionPicture('sto:Description','@s30')
  If p_web.IfExistsValue('locInvoiceNumber')
    p_web.SetPicture('locInvoiceNumber','@n_7')
  End
  p_web.SetSessionPicture('locInvoiceNumber','@n_7')
  If p_web.IfExistsValue('locQuantity')
    p_web.SetPicture('locQuantity','@n_7')
  End
  p_web.SetSessionPicture('locQuantity','@n_7')

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  if p_web.IfExistsValue('sto:Part_Number') = 0
    p_web.SetSessionValue('sto:Part_Number',sto:Part_Number)
  Else
    sto:Part_Number = p_web.GetSessionValue('sto:Part_Number')
  End
  if p_web.IfExistsValue('sto:Description') = 0
    p_web.SetSessionValue('sto:Description',sto:Description)
  Else
    sto:Description = p_web.GetSessionValue('sto:Description')
  End
  if p_web.IfExistsValue('locReturnType') = 0
    p_web.SetSessionValue('locReturnType',locReturnType)
  Else
    locReturnType = p_web.GetSessionValue('locReturnType')
  End
  if p_web.IfExistsValue('locInvoiceNumber') = 0
    p_web.SetSessionValue('locInvoiceNumber',locInvoiceNumber)
  Else
    locInvoiceNumber = p_web.GetSessionValue('locInvoiceNumber')
  End
  if p_web.IfExistsValue('locSalesNumber') = 0
    p_web.SetSessionValue('locSalesNumber',locSalesNumber)
  Else
    locSalesNumber = p_web.GetSessionValue('locSalesNumber')
  End
  if p_web.IfExistsValue('locQuantity') = 0
    p_web.SetSessionValue('locQuantity',locQuantity)
  Else
    locQuantity = p_web.GetSessionValue('locQuantity')
  End
  if p_web.IfExistsValue('locNotes') = 0
    p_web.SetSessionValue('locNotes',locNotes)
  Else
    locNotes = p_web.GetSessionValue('locNotes')
  End

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('sto:Part_Number')
    sto:Part_Number = p_web.GetValue('sto:Part_Number')
    p_web.SetSessionValue('sto:Part_Number',sto:Part_Number)
  Else
    sto:Part_Number = p_web.GetSessionValue('sto:Part_Number')
  End
  if p_web.IfExistsValue('sto:Description')
    sto:Description = p_web.GetValue('sto:Description')
    p_web.SetSessionValue('sto:Description',sto:Description)
  Else
    sto:Description = p_web.GetSessionValue('sto:Description')
  End
  if p_web.IfExistsValue('locReturnType')
    locReturnType = p_web.GetValue('locReturnType')
    p_web.SetSessionValue('locReturnType',locReturnType)
  Else
    locReturnType = p_web.GetSessionValue('locReturnType')
  End
  if p_web.IfExistsValue('locInvoiceNumber')
    locInvoiceNumber = p_web.dformat(clip(p_web.GetValue('locInvoiceNumber')),'@n_7')
    p_web.SetSessionValue('locInvoiceNumber',locInvoiceNumber)
  Else
    locInvoiceNumber = p_web.GetSessionValue('locInvoiceNumber')
  End
  if p_web.IfExistsValue('locSalesNumber')
    locSalesNumber = p_web.GetValue('locSalesNumber')
    p_web.SetSessionValue('locSalesNumber',locSalesNumber)
  Else
    locSalesNumber = p_web.GetSessionValue('locSalesNumber')
  End
  if p_web.IfExistsValue('locQuantity')
    locQuantity = p_web.dformat(clip(p_web.GetValue('locQuantity')),'@n_7')
    p_web.SetSessionValue('locQuantity',locQuantity)
  Else
    locQuantity = p_web.GetSessionValue('locQuantity')
  End
  if p_web.IfExistsValue('locNotes')
    locNotes = p_web.GetValue('locNotes')
    p_web.SetSessionValue('locNotes',locNotes)
  Else
    locNotes = p_web.GetSessionValue('locNotes')
  End

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('FormReturnStock_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.GSV('ReturnURL')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormReturnStock_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormReturnStock_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormReturnStock_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = p_web.GSV('ReturnURL')

GenerateForm   Routine
  do LoadRelatedRecords
  ! Store return URL
  IF (p_web.IfExistsValue('ReturnURL'))
      p_web.StoreValue('ReturnURL')
  END
      p_web.site.SaveButton.TextValue = 'Return Stock'
      p_web.site.SaveButton.Class = 'DoubleButton'
 locReturnType = p_web.RestoreValue('locReturnType')
 locInvoiceNumber = p_web.RestoreValue('locInvoiceNumber')
 locSalesNumber = p_web.RestoreValue('locSalesNumber')
 locQuantity = p_web.RestoreValue('locQuantity')
 locNotes = p_web.RestoreValue('locNotes')
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Return Stock') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Return Stock',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_FormReturnStock',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_FormReturnStock0_div')&'">'&p_web.Translate('Part Details')&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="FormReturnStock_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.cancel();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CancelButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="FormReturnStock_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'FormReturnStock_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="FormReturnStock_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'FormReturnStock_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locReturnType')
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_FormReturnStock')>0,p_web.GSV('showtab_FormReturnStock'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_FormReturnStock'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_FormReturnStock') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_FormReturnStock'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_FormReturnStock')>0,p_web.GSV('showtab_FormReturnStock'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_FormReturnStock') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Part Details') & ''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_FormReturnStock_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_FormReturnStock')>0,p_web.GSV('showtab_FormReturnStock'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"FormReturnStock",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_FormReturnStock')>0,p_web.GSV('showtab_FormReturnStock'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_FormReturnStock_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('FormReturnStock') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('FormReturnStock')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Part Details')&'</a></h3>' & CRLF & p_web.DivHeader('tab_FormReturnStock0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_FormReturnStock0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_FormReturnStock0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_FormReturnStock0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Part Details')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_FormReturnStock0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Part Details')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_FormReturnStock0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Part Details')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_FormReturnStock0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::sto:Part_Number
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::sto:Part_Number
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::sto:Part_Number
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::sto:Description
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::sto:Description
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::sto:Description
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locReturnType
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locReturnType
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locReturnType
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locInvoiceNumber
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locInvoiceNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locInvoiceNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locSalesNumber
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locSalesNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locSalesNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locQuantity
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locQuantity
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locQuantity
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locNotes
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locNotes
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locNotes
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Prompt::sto:Part_Number  Routine
  packet = clip(packet) & p_web.DivHeader('FormReturnStock_' & p_web._nocolon('sto:Part_Number') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Part Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::sto:Part_Number  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    sto:Part_Number = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    sto:Part_Number = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::sto:Part_Number  ! copies value to session value if valid.
  do Comment::sto:Part_Number ! allows comment style to be updated.

ValidateValue::sto:Part_Number  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('sto:Part_Number',sto:Part_Number).
    End

Value::sto:Part_Number  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormReturnStock_' & p_web._nocolon('sto:Part_Number') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- sto:Part_Number
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('sto:Part_Number'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::sto:Part_Number  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if sto:Part_Number:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormReturnStock_' & p_web._nocolon('sto:Part_Number') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::sto:Description  Routine
  packet = clip(packet) & p_web.DivHeader('FormReturnStock_' & p_web._nocolon('sto:Description') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Description'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::sto:Description  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    sto:Description = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    sto:Description = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::sto:Description  ! copies value to session value if valid.
  do Comment::sto:Description ! allows comment style to be updated.

ValidateValue::sto:Description  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('sto:Description',sto:Description).
    End

Value::sto:Description  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormReturnStock_' & p_web._nocolon('sto:Description') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- sto:Description
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('sto:Description'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::sto:Description  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if sto:Description:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormReturnStock_' & p_web._nocolon('sto:Description') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locReturnType  Routine
  packet = clip(packet) & p_web.DivHeader('FormReturnStock_' & p_web._nocolon('locReturnType') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Return Type'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locReturnType  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locReturnType = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locReturnType = p_web.GetValue('Value')
  End
  do ValidateValue::locReturnType  ! copies value to session value if valid.
  do Value::locReturnType
  do SendAlert
  do Comment::locReturnType ! allows comment style to be updated.

ValidateValue::locReturnType  Routine
    If not (1=0)
  If locReturnType = ''
    loc:Invalid = 'locReturnType'
    locReturnType:IsInvalid = true
    loc:alert = p_web.translate('Return Type') & ' ' & p_web.site.RequiredText
  End
    locReturnType = Upper(locReturnType)
      if loc:invalid = '' then p_web.SetSessionValue('locReturnType',locReturnType).
    End

Value::locReturnType  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormReturnStock_' & p_web._nocolon('locReturnType') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,p_web.site.style.formselect,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  If loc:retrying
    locReturnType = p_web.RestoreValue('locReturnType')
    do ValidateValue::locReturnType
    If locReturnType:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- DROPLIST ---
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locReturnType'',''formreturnstock_locreturntype_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('locReturnType',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
  loc:fieldclass = ''
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('locReturnType') = 0
    p_web.SetSessionValue('locReturnType','')
  end
    packet = clip(packet) & p_web.CreateOption('','',choose('' = p_web.getsessionvalue('locReturnType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(RTNORDER)
  bind(rtn:Record)
  p_web._OpenFile(SUBTRACC)
  bind(sub:Record)
  p_web._OpenFile(TRADEACC)
  bind(tra:Record)
  p_web._OpenFile(RETSALES)
  bind(ret:Record)
  p_web._OpenFile(RETTYPES)
  bind(rtt:Record)
  p_web._OpenFile(STOCK)
  bind(sto:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(locReturnType_OptionView)
  locReturnType_OptionView{prop:filter} = p_web.CleanFilter(locReturnType_OptionView,'rtt:Active = 1')
  locReturnType_OptionView{prop:order} = p_web.CleanFilter(locReturnType_OptionView,'UPPER(rtt:Description)')
  Set(locReturnType_OptionView)
  Loop
    Next(locReturnType_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('locReturnType') = 0
      p_web.SetSessionValue('locReturnType',rtt:Description)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,rtt:Description,choose(rtt:Description = p_web.getsessionvalue('locReturnType')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(locReturnType_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(RTNORDER)
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(RETSALES)
  p_Web._CloseFile(RETTYPES)
  p_Web._CloseFile(STOCK)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web.DivFooter()
Comment::locReturnType  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locReturnType:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormReturnStock_' & p_web._nocolon('locReturnType') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locInvoiceNumber  Routine
  packet = clip(packet) & p_web.DivHeader('FormReturnStock_' & p_web._nocolon('locInvoiceNumber') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Invoice Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locInvoiceNumber  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locInvoiceNumber = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n_7'  !FieldPicture = 
    locInvoiceNumber = p_web.Dformat(p_web.GetValue('Value'),'@n_7')
  End
  do ValidateValue::locInvoiceNumber  ! copies value to session value if valid.
  DO Validate:locInvoiceNumber
  do Value::locInvoiceNumber
  do SendAlert
  do Comment::locInvoiceNumber ! allows comment style to be updated.
  do Value::locSalesNumber  !1
  do Comment::locInvoiceNumber
  do Value::locQuantity  !1
  do Comment::locQuantity

ValidateValue::locInvoiceNumber  Routine
    If not (1=0)
  If locInvoiceNumber = ''
    loc:Invalid = 'locInvoiceNumber'
    locInvoiceNumber:IsInvalid = true
    loc:alert = p_web.translate('Invoice Number') & ' ' & p_web.site.RequiredText
  End
    locInvoiceNumber = Upper(locInvoiceNumber)
      if loc:invalid = '' then p_web.SetSessionValue('locInvoiceNumber',locInvoiceNumber).
    End

Value::locInvoiceNumber  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormReturnStock_' & p_web._nocolon('locInvoiceNumber') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  If loc:retrying
    locInvoiceNumber = p_web.RestoreValue('locInvoiceNumber')
    do ValidateValue::locInvoiceNumber
    If locInvoiceNumber:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- locInvoiceNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locInvoiceNumber'',''formreturnstock_locinvoicenumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locInvoiceNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locInvoiceNumber',p_web.GetSessionValue('locInvoiceNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n_7',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locInvoiceNumber  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locInvoiceNumber:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Prompt:locInvoiceNumber'))
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormReturnStock_' & p_web._nocolon('locInvoiceNumber') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locSalesNumber  Routine
  packet = clip(packet) & p_web.DivHeader('FormReturnStock_' & p_web._nocolon('locSalesNumber') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Sales Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locSalesNumber  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locSalesNumber = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locSalesNumber = p_web.GetValue('Value')
  End
  do ValidateValue::locSalesNumber  ! copies value to session value if valid.
  do Comment::locSalesNumber ! allows comment style to be updated.

ValidateValue::locSalesNumber  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locSalesNumber',locSalesNumber).
    End

Value::locSalesNumber  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormReturnStock_' & p_web._nocolon('locSalesNumber') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- locSalesNumber
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locSalesNumber'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locSalesNumber  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locSalesNumber:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormReturnStock_' & p_web._nocolon('locSalesNumber') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locQuantity  Routine
  packet = clip(packet) & p_web.DivHeader('FormReturnStock_' & p_web._nocolon('locQuantity') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Quantity'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locQuantity  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locQuantity = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n_7'  !FieldPicture = 
    locQuantity = p_web.Dformat(p_web.GetValue('Value'),'@n_7')
  End
  do ValidateValue::locQuantity  ! copies value to session value if valid.
  do Value::locQuantity
  do SendAlert
  do Comment::locQuantity ! allows comment style to be updated.

ValidateValue::locQuantity  Routine
    If not (1=0)
  If locQuantity = ''
    loc:Invalid = 'locQuantity'
    locQuantity:IsInvalid = true
    loc:alert = p_web.translate('Quantity') & ' ' & p_web.site.RequiredText
  End
  If Numeric(locQuantity) = 0
    loc:Invalid = 'locQuantity'
    locQuantity:IsInvalid = true
    loc:alert = p_web.translate('Quantity') & ' ' & p_web.site.NumericText
  ElsIf InRange(locQuantity,1,p_web.GSV('locQuantityHigh')) = false
    loc:Invalid = 'locQuantity'
    locQuantity:IsInvalid = true
    loc:alert = p_web.translate('Quantity') & ' ' & clip(p_web.site.MoreThanText) & ' ' & 1 & ', ' & clip(p_web.site.LessThanText) & ' ' & p_web.GSV('locQuantityHigh')
  End
      if loc:invalid = '' then p_web.SetSessionValue('locQuantity',locQuantity).
    End

Value::locQuantity  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormReturnStock_' & p_web._nocolon('locQuantity') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  If loc:retrying
    locQuantity = p_web.RestoreValue('locQuantity')
    do ValidateValue::locQuantity
    If locQuantity:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- locQuantity
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locQuantity'',''formreturnstock_locquantity_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locQuantity',p_web.GetSessionValue('locQuantity'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n_7',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locQuantity  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locQuantity:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Prompt:LocQuantity'))
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormReturnStock_' & p_web._nocolon('locQuantity') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locNotes  Routine
  packet = clip(packet) & p_web.DivHeader('FormReturnStock_' & p_web._nocolon('locNotes') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Notes'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locNotes  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locNotes = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locNotes = p_web.GetValue('Value')
  End
  do ValidateValue::locNotes  ! copies value to session value if valid.
  do Value::locNotes
  do SendAlert
  do Comment::locNotes ! allows comment style to be updated.

ValidateValue::locNotes  Routine
    If not (1=0)
    locNotes = Upper(locNotes)
      if loc:invalid = '' then p_web.SetSessionValue('locNotes',locNotes).
    End

Value::locNotes  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormReturnStock_' & p_web._nocolon('locNotes') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    locNotes = p_web.RestoreValue('locNotes')
    do ValidateValue::locNotes
    If locNotes:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- TEXT --- locNotes
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locNotes'',''formreturnstock_locnotes_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'readonly','')
  do SendPacket
  p_web.CreateTextArea('locNotes',p_web.GetSessionValue('locNotes'),5,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,255,,,,Net:Send)
  do SendPacket
  End
  p_web.DivFooter()
Comment::locNotes  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locNotes:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormReturnStock_' & p_web._nocolon('locNotes') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('FormReturnStock_nexttab_' & 0)
    sto:Part_Number = p_web.GSV('sto:Part_Number')
    do ValidateValue::sto:Part_Number
    If loc:Invalid
      loc:retrying = 1
      do Value::sto:Part_Number
      !do SendAlert
      do Comment::sto:Part_Number ! allows comment style to be updated.
      !exit
    End
    sto:Description = p_web.GSV('sto:Description')
    do ValidateValue::sto:Description
    If loc:Invalid
      loc:retrying = 1
      do Value::sto:Description
      !do SendAlert
      do Comment::sto:Description ! allows comment style to be updated.
      !exit
    End
    locReturnType = p_web.GSV('locReturnType')
    do ValidateValue::locReturnType
    If loc:Invalid
      loc:retrying = 1
      do Value::locReturnType
      !do SendAlert
      do Comment::locReturnType ! allows comment style to be updated.
      !exit
    End
    locInvoiceNumber = p_web.GSV('locInvoiceNumber')
    do ValidateValue::locInvoiceNumber
    If loc:Invalid
      loc:retrying = 1
      do Value::locInvoiceNumber
      !do SendAlert
      do Comment::locInvoiceNumber ! allows comment style to be updated.
      !exit
    End
    locSalesNumber = p_web.GSV('locSalesNumber')
    do ValidateValue::locSalesNumber
    If loc:Invalid
      loc:retrying = 1
      do Value::locSalesNumber
      !do SendAlert
      do Comment::locSalesNumber ! allows comment style to be updated.
      !exit
    End
    locQuantity = p_web.GSV('locQuantity')
    do ValidateValue::locQuantity
    If loc:Invalid
      loc:retrying = 1
      do Value::locQuantity
      !do SendAlert
      do Comment::locQuantity ! allows comment style to be updated.
      !exit
    End
    locNotes = p_web.GSV('locNotes')
    do ValidateValue::locNotes
    If loc:Invalid
      loc:retrying = 1
      do Value::locNotes
      !do SendAlert
      do Comment::locNotes ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_FormReturnStock_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('FormReturnStock_tab_' & 0)
    do GenerateTab0
  of lower('FormReturnStock_locReturnType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locReturnType
      of event:timer
        do Value::locReturnType
        do Comment::locReturnType
      else
        do Value::locReturnType
      end
  of lower('FormReturnStock_locInvoiceNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locInvoiceNumber
      of event:timer
        do Value::locInvoiceNumber
        do Comment::locInvoiceNumber
      else
        do Value::locInvoiceNumber
      end
  of lower('FormReturnStock_locQuantity_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locQuantity
      of event:timer
        do Value::locQuantity
        do Comment::locQuantity
      else
        do Value::locQuantity
      end
  of lower('FormReturnStock_locNotes_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locNotes
      of event:timer
        do Value::locNotes
        do Comment::locNotes
      else
        do Value::locNotes
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('FormReturnStock_form:ready_',1)

  p_web.SetSessionValue('FormReturnStock_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_FormReturnStock',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('FormReturnStock_form:ready_',1)
  p_web.SetSessionValue('FormReturnStock_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormReturnStock',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('FormReturnStock_form:ready_',1)
  p_web.SetSessionValue('FormReturnStock_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('FormReturnStock:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('FormReturnStock_form:ready_',1)
  p_web.SetSessionValue('FormReturnStock_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('FormReturnStock:Primed',0)
  p_web.setsessionvalue('showtab_FormReturnStock',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
      If not (1=0)
          If p_web.IfExistsValue('locReturnType')
            locReturnType = p_web.GetValue('locReturnType')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('locInvoiceNumber')
            locInvoiceNumber = p_web.GetValue('locInvoiceNumber')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('locQuantity')
            locQuantity = p_web.GetValue('locQuantity')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('locNotes')
            locNotes = p_web.GetValue('locNotes')
          End
      End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormReturnStock_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  Access:STOCK.ClearKey(sto:Ref_Number_Key)
  sto:Ref_Number = p_web.GSV('sto:Ref_Number')
  IF (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
  
      Access:RETTYPES.Clearkey(rtt:DescriptionKey)
      rtt:Description = p_web.GSV('locReturnType')
      IF (Access:RETTYPES.TryFetch(rtt:DescriptionKey) = Level:Benign)
          IF (rtt:UseReturnDays)
              IF (p_web.GSV('locDateReceived') + rtt:SparesReturnDays < TODAY())
                  loc:alert = 'Part cannot be returned as was received at Franchise over the set period.'
                  loc:invalid = 'locReturnType'
                  EXIT
  
              END
          END ! IF (rtt:UseReturnDays)
      END
  
      IF (p_web.GSV('locQuantity') > sto:Quantity_Stock)
          loc:alert = 'The quantity entered is greater than the quantity in stock.'
          loc:invalid = 'locQuantity'
          EXIT
  
      END
  
  
  
  
      Pointer# = Pointer(STOCK)
      Hold(STOCK,1)
      Get(STOCK,Pointer#)
      If Errorcode() = 43
          loc:Alert = 'This stock item is current in use by another station'
          loc:invalid = 'loc:Quantity'
          EXIT
      END
  
      Access:RETSALES.Clearkey(ret:Invoice_Number_Key)
      ret:Invoice_Number = p_web.GSV('locInvoiceNumber')
      IF (Access:RETSALES.TryFetch(ret:Invoice_Number_Key))
      END
  
      sto:Quantity_Stock -= p_web.GSV('locQuantity')
      IF (sto:Quantity_Stock < 0)
          sto:Quantity_Stock = 0
      END
  
      IF (Access:STOCK.TryUpdate() = Level:Benign)
      ! Does an item already exist for this order?
          Access:RTNORDER.Clearkey(rtn:OrderStatusReturnRefNoKey)
          rtn:OrderNumber = ret:Ref_Number
          rtn:ExchangeOrder = ret:ExchangeOrder
          rtn:ReturnType = p_web.GSV('locReturnType')
          rtn:Status = 'NEW'
          rtn:RefNumber = sto:Ref_Number
          IF (Access:RTNORDER.TryFetch(rtn:OrderStatusReturnRefNoKey) = Level:Benign)
              rtn:QuantityReturned += p_web.GSV('locQuantity')
              rtn:Notes = CLIP(rtn:Notes) & '<13,10>' & p_web.GSV('locNotes')
              Access:RTNORDER.TryUpdate()
          ELSE
          ! Create Return Order
              IF (Access:RTNORDER.PrimeRecord() = Level:Benign)
                  rtn:Location = p_web.GSV('BookingSiteLocation')
                  rtn:UserCode = p_web.GSV('BookingUserCode')
                  rtn:RefNumber = sto:Ref_Number
                  rtn:OrderNumber = ret:Ref_Number
                  rtn:InvoiceNumber = ret:Invoice_Number
                  rtn:PartNumber = sto:Part_Number
                  rtn:Description = sto:Description
                  rtn:QuantityReturned = p_web.GSV('locQuantity')
                  rtn:ExchangeOrder = ret:ExchangeOrder
                  rtn:Status = 'NEW'
                  rtn:Notes = locNotes
                  rtn:ReturnType = locReturnType
                  rtn:PurchaseCost = sto:Purchase_Cost
                  rtn:SaleCost = sto:Sale_Cost
                  IF (Access:RTNORDER.TryInsert())
                      Access:RTNORDER.CancelAutoInc()
                  END
              END
          END
          If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
              'DEC', | ! Transaction_Type
              '', | ! Depatch_Note_Number
              0, | ! Job_Number
              0, | ! Sales_Number
              p_web.GSV('locQuantity'), | ! Quantity
              sto:Purchase_Cost, | ! Purchase_Cost
              sto:Sale_Cost, | ! Sale_Cost
              sto:Retail_Cost, | ! Retail_Cost
              'RETURN ORDER GENERATED', | ! Notes
              '', |
              p_web.GSV('BookingUserCode'), |
              sto:Quantity_stock) ! Information
                    ! Added OK
          Else ! AddToStockHistory
                    ! Error
          End ! AddToStockHistory
      END
      RELEASE(STOCK)
  
  END
  p_web.DeleteSessionValue('FormReturnStock_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
    do ValidateValue::sto:Part_Number
    If loc:Invalid then exit.
    do ValidateValue::sto:Description
    If loc:Invalid then exit.
    do ValidateValue::locReturnType
    If loc:Invalid then exit.
    do ValidateValue::locInvoiceNumber
    If loc:Invalid then exit.
    do ValidateValue::locSalesNumber
    If loc:Invalid then exit.
    do ValidateValue::locQuantity
    If loc:Invalid then exit.
    do ValidateValue::locNotes
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('FormReturnStock:Primed',0)
  p_web.StoreValue('sto:Part_Number')
  p_web.StoreValue('sto:Description')
  p_web.StoreValue('locReturnType')
  p_web.StoreValue('locInvoiceNumber')
  p_web.StoreValue('locSalesNumber')
  p_web.StoreValue('locQuantity')
  p_web.StoreValue('locNotes')

