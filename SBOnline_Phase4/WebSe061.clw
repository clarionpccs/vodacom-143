

   MEMBER('WebServer_Phase4.clw')                          ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('WEBSE061.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSE030.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE033.INC'),ONCE        !Req'd for module callout resolution
                     END


ds_Halt              PROCEDURE  (UNSIGNED Level=0,<STRING HaltText>) ! Declare Procedure
TempNoLogging         long(0)
  CODE
  if ~omitted(2) then
    if ThisMessageBox.GetGlobalSetting('TranslationFile') <> ''
      ds_Message(HaltText,getini('MessageBox_Text','HaltHeader','Halt',ThisMessageBox.GetGlobalSetting('TranslationFile')),ICON:Hand)
    else
      ds_Message(HaltText,'Halt',ICON:Hand)
    end
  end
  system{prop:HaltHook} = 0
  HALT(Level)
ds_Message PROCEDURE (STRING MessageTxt,<STRING HeadingTxt>,<STRING IconSent>,<STRING ButtonsPar>,UNSIGNED MsgDefaults=0,BOOL StylePar=FALSE) ! Generated from procedure template - Window

FilesOpened          BYTE                                  !
Loc:ButtonPressed    UNSIGNED                              !
EmailLink            CSTRING(4096)                         !
DontShowThisAgain       byte(0)         !CapeSoft MessageBox Data
LocalMessageBoxdata     group,pre(LMBD) !CapeSoft MessageBox Data
MessageText               cstring(4096)
HeadingText               cstring(1024)
UseIcon                   cstring(1024)
Buttons                   cstring(1024)
Defaults                  unsigned
                        end
window               WINDOW('Caption'),AT(,,404,108),FONT('MS Sans Serif',8,,FONT:regular),GRAY
                       IMAGE,AT(11,18),USE(?Image1),HIDE
                       PROMPT(''''),AT(118,32),USE(?MainTextPrompt)
                       STRING('HyperActive Link'),AT(91,46),USE(?HALink),HIDE
                       STRING('Time Out:'),AT(103,54),USE(?TimerCounter),HIDE
                       CHECK('Dont Show This Again'),AT(85,65),USE(DontShowThisAgain),HIDE
                       BUTTON('Button 1'),AT(9,81,45,14),USE(?Button1),HIDE
                       BUTTON('Button 2'),AT(59,81,45,14),USE(?Button2),HIDE
                       BUTTON('Button 3'),AT(109,81,45,14),USE(?Button3),HIDE
                       BUTTON('Button 4'),AT(159,81,45,14),USE(?Button4),HIDE
                       BUTTON('Button 5'),AT(209,81,45,14),USE(?Button5),HIDE
                       BUTTON('Button 6'),AT(259,81,45,14),USE(?Button6),HIDE
                       BUTTON('Button 7'),AT(309,81,45,14),USE(?Button7),HIDE
                       BUTTON('Button 8'),AT(359,81,45,14),USE(?Button8),HIDE
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Run                    PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop
  RETURN(Loc:ButtonPressed)

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
ThisMessageBoxLogInit          routine         !CapeSoft MessageBox Initialize properties routine
    if ~omitted(2) then LMBD:HeadingText = HeadingTxt .
    if ~omitted(3) then LMBD:UseIcon = IconSent .
    if ~omitted(4) then LMBD:Buttons = ButtonsPar .
    if ~omitted(5) then LMBD:Defaults = MsgDefaults .
    LMBD:MessageText = MessageTxt
    ThisMessageBox.FromExe = command(0)
    if instring('\',ThisMessageBox.FromExe,1,1)
      ThisMessageBox.FromExe = ThisMessageBox.FromExe[ (instring('\',ThisMessageBox.FromExe,-1,len(ThisMessageBox.FromExe)) + 1) : len(ThisMessageBox.FromExe) ]
    end
    ThisMessageBox.TrnStrings = 1
    ThisMessageBox.PromptControl = ?MainTextPrompt
    ThisMessageBox.IconControl = ?Image1
         !End of CapeSoft MessageBox Initialize properties routine

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
    ThisMessageBox.SavedResponse = GlobalResponse        !CapeSoft MessageBox Code - Preserves the GlobalResponse
  GlobalErrors.SetProcedureName('ds_Message')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Image1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
        GlobalRequest = ThisWindow.Request         !Keep GlobalRequest the correct value.
        if ThisMessageBox.MessagesUsed then
          return level:fatal
        end
        ThisMessageBox.MessagesUsed += 1
        do ThisMessageBoxLogInit                               !CapeSoft MessageBox Code
        ThisMessageBox.ReturnValue = ThisMessageBox.PreOpen(LMBD:MessageText,LMBD:HeadingText,LMBD:Defaults)
        if ThisMessageBox.ReturnValue then
          self.Response = RequestCompleted
          return level:notify
        end
        ThisMessageBox.DontShowThisAgain = ?DontShowThisAgain
        ThisMessageBox.TimeOutPrompt = ?TimerCounter
        ThisMessageBox.HAControl = ?HALink
  SELF.Open(window)                                        ! Open window
  Do DefineListboxStyle
      IF (INSTRING('WINDOWS 7',UPPER(SYSTEM {PROP:WindowsVersion}),1,1) OR |
          INSTRING('WINDOWS VISTA',UPPER(SYSTEM{PROP:WindowsVersion}),1,1) OR |
          INSTRING('SERVER 2008',UPPER(SYSTEM{PROP:WindowsVersion}),1,1))
          0{prop:FontName} = 'Segoe UI'
          0{prop:FontSize} = 9
      ELSE
          0{prop:FontName} = 'Tahoma'
          0{prop:FontSize} = 8
      END
  ThisMessageBox.open(LMBD:MessageText,LMBD:HeadingText,LMBD:UseIcon,LMBD:Buttons,LMBD:Defaults,StylePar)         !CapeSoft MessageBox Code
      alert(EscKey)
      IF (0{prop:Width} < 150)
          0{prop:Width} = 150
      END
      0{PROP:Height} = 0{PROP:Height} + 2
  
      SETPENCOLOR(COLOR:BTNSHADOW)
      BOX(-1,-1,0{PROP:Width} + 2,0{PROP:Height} - 18,COLOR:White)
      IMAGE(?Image1{PROP:Xpos},?Image1{PROP:Ypos},?Image1{PROP:Width},?Image1{PROP:Height},?Image1{PROP:Text})
  
      ?Button1{PROP:Ypos} = ?Button1{PROP:Ypos} + 4
      ?Button2{PROP:Ypos} = ?Button2{PROP:Ypos} + 4
      ?Button3{PROP:Ypos} = ?Button3{PROP:Ypos} + 4
      ?Button4{PROP:Ypos} = ?Button4{PROP:Ypos} + 4
      ?Button5{PROP:Ypos} = ?Button5{PROP:Ypos} + 4
      ?Button6{PROP:Ypos} = ?Button6{PROP:Ypos} + 4
      ?Button7{PROP:Ypos} = ?Button7{PROP:Ypos} + 4
      ?Button8{PROP:Ypos} = ?Button8{PROP:Ypos} + 4
  
      IF (?Button8{PROP:Hide} = 0)
          b8# = ?Button8{PROP:Width} + 4
      END
      IF (?Button7{PROP:Hide} = 0)
          b7# = ?Button7{PROP:Width} + 4
      END
      IF (?Button6{PROP:Hide} = 0)
          b6# = ?Button6{PROP:Width} + 4
      END
      IF (?Button5{PROP:Hide} = 0)
          b5# = ?Button5{PROP:Width} + 4
      END
      IF (?Button4{PROP:Hide} = 0)
          b4# = ?Button4{PROP:Width} + 4
      END
      IF (?Button3{PROP:Hide} = 0)
          b3# = ?Button3{PROP:Width} + 4
      END
      IF (?Button2{PROP:Hide} = 0)
          b2# = ?Button2{PROP:Width} + 4
      END
      IF (?Button1{PROP:Hide} = 0)
          b1# = ?Button1{PROP:Width} + 4
      END
  
      ?Button8{PROP:Xpos} = 0{PROP:Width} - b8#
      ?Button7{PROP:Xpos} = 0{PROP:Width} - b8# - b7#
      ?Button6{PROP:Xpos} = 0{PROP:Width} - b8# - b7# - b6#
      ?Button5{PROP:Xpos} = 0{PROP:Width} - b8# - b7# - b6# - b5#
      ?Button4{PROP:Xpos} = 0{PROP:Width} - b8# - b7# - b6# - b5# - b4#
      ?Button3{PROP:Xpos} = 0{PROP:Width} - b8# - b7# - b6# - b5# - b4# - b3#
      ?Button2{PROP:Xpos} = 0{PROP:Width} - b8# - b7# - b6# - b5# - b4# - b3# - b2#
      ?Button1{PROP:Xpos} = 0{PROP:Width} - b8# - b7# - b6# - b5# - b4# - b3# - b2# - b1#
  
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
              !CapeSoft MessageBox EndCode
    Loc:ButtonPressed = ThisMessageBox.ReturnValue
    ThisMessageBox.Close()
    if ThisMessageBox.MessagesUsed > 0 then ThisMessageBox.MessagesUsed -= 1 .
              !End of CapeSoft MessageBox EndCode
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
    ReturnValue = ThisMessageBox.SavedResponse           !CapeSoft MessageBox Code - Preserves the GlobalResponse
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ThisMessageBox.TakeEvent()                             !CapeSoft MessageBox Code
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

FormNewPassword      PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locNewPassword       STRING(30)                            !
locConfirmNewPassword STRING(30)                           !
locMobileNumber      STRING(20)                            !
FilesOpened     Long
USERS::State  USHORT
txtCompany:IsInvalid  Long
txtUser:IsInvalid  Long
locNewPassword:IsInvalid  Long
locConfirmNewPassword:IsInvalid  Long
locMobileNumber:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormNewPassword')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'FormNewPassword_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('FormNewPassword','')
    p_web.DivHeader('FormNewPassword',p_web.combine(p_web.site.style.formdiv,))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('FormNewPassword') = 0
        p_web.AddPreCall('FormNewPassword')
        p_web.DivHeader('popup_FormNewPassword','nt-hidden')
        p_web.DivHeader('FormNewPassword',p_web.combine(p_web.site.style.formdiv,))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_FormNewPassword_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_FormNewPassword_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormNewPassword',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_FormNewPassword',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormNewPassword',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_FormNewPassword',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormNewPassword',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormNewPassword',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_FormNewPassword',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormNewPassword',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_FormNewPassword',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormNewPassword',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormNewPassword',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  p_web._MessageDone = 1 ! suppress any messages on this form.
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormNewPassword',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('FormNewPassword')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(USERS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(USERS)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FormNewPassword_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'FormNewPassword'
    end
    p_web.formsettings.proc = 'FormNewPassword'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  If p_web.GSV('NewPasswordRequired') = 1
    loc:TabNumber += 1
  End
  If p_web.GSV('UserMobileRequired') = 1
    loc:TabNumber += 1
  End
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  if p_web.IfExistsValue('locNewPassword') = 0
    p_web.SetSessionValue('locNewPassword',locNewPassword)
  Else
    locNewPassword = p_web.GetSessionValue('locNewPassword')
  End
  if p_web.IfExistsValue('locConfirmNewPassword') = 0
    p_web.SetSessionValue('locConfirmNewPassword',locConfirmNewPassword)
  Else
    locConfirmNewPassword = p_web.GetSessionValue('locConfirmNewPassword')
  End
  if p_web.IfExistsValue('locMobileNumber') = 0
    p_web.SetSessionValue('locMobileNumber',locMobileNumber)
  Else
    locMobileNumber = p_web.GetSessionValue('locMobileNumber')
  End

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locNewPassword')
    locNewPassword = p_web.GetValue('locNewPassword')
    p_web.SetSessionValue('locNewPassword',locNewPassword)
  Else
    locNewPassword = p_web.GetSessionValue('locNewPassword')
  End
  if p_web.IfExistsValue('locConfirmNewPassword')
    locConfirmNewPassword = p_web.GetValue('locConfirmNewPassword')
    p_web.SetSessionValue('locConfirmNewPassword',locConfirmNewPassword)
  Else
    locConfirmNewPassword = p_web.GetSessionValue('locConfirmNewPassword')
  End
  if p_web.IfExistsValue('locMobileNumber')
    locMobileNumber = p_web.GetValue('locMobileNumber')
    p_web.SetSessionValue('locMobileNumber',locMobileNumber)
  Else
    locMobileNumber = p_web.GetSessionValue('locMobileNumber')
  End

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('FormNewPassword_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'IndexPage'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormNewPassword_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormNewPassword_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormNewPassword_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'LoginForm'

GenerateForm   Routine
  do LoadRelatedRecords
  p_web.SSV('locNewPassword','')
  p_web.SSV('locConfirmNewPassword','')
  p_web.SSV('locMobileNumber','')
      p_web.site.SaveButton.TextValue = 'Login'
 locNewPassword = p_web.RestoreValue('locNewPassword')
 locConfirmNewPassword = p_web.RestoreValue('locConfirmNewPassword')
 locMobileNumber = p_web.RestoreValue('locMobileNumber')
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
    Packet = clip(Packet) & p_web.DivHeader('Tab_FormNewPassword',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_FormNewPassword0_div')&'">'&p_web.Translate('Login Details')&'</a></li>'& CRLF
      If p_web.GSV('NewPasswordRequired') = 1
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_FormNewPassword1_div')&'">'&p_web.Translate('New Login Password Required')&'</a></li>'& CRLF
      End
      If p_web.GSV('UserMobileRequired') = 1
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_FormNewPassword2_div')&'">'&p_web.Translate('Mobile Number Required')&'</a></li>'& CRLF
      End
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="FormNewPassword_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.cancel();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CancelButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="FormNewPassword_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'FormNewPassword_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="FormNewPassword_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'FormNewPassword_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_FormNewPassword')>0,p_web.GSV('showtab_FormNewPassword'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_FormNewPassword'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_FormNewPassword') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_FormNewPassword'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_FormNewPassword')>0,p_web.GSV('showtab_FormNewPassword'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_FormNewPassword') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Login Details') & ''''
      If p_web.GSV('NewPasswordRequired') = 1
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('New Login Password Required') & ''''
      End
      If p_web.GSV('UserMobileRequired') = 1
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Mobile Number Required') & ''''
      End
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_FormNewPassword_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_FormNewPassword')>0,p_web.GSV('showtab_FormNewPassword'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"FormNewPassword",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_FormNewPassword')>0,p_web.GSV('showtab_FormNewPassword'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_FormNewPassword_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('FormNewPassword') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('FormNewPassword')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Login Details')&'</a></h3>' & CRLF & p_web.DivHeader('tab_FormNewPassword0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_FormNewPassword0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_FormNewPassword0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_FormNewPassword0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Login Details')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_FormNewPassword0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Login Details')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_FormNewPassword0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Login Details')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_FormNewPassword0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 2.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::txtCompany
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 2.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::txtUser
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab1  Routine
  If p_web.GSV('NewPasswordRequired') = 1
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('New Login Password Required')&'</a></h3>' & CRLF & p_web.DivHeader('tab_FormNewPassword1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_FormNewPassword1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_FormNewPassword1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_FormNewPassword1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'New Login Password Required')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_FormNewPassword1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('New Login Password Required')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_FormNewPassword1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('New Login Password Required')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_FormNewPassword1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locNewPassword
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locNewPassword
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locConfirmNewPassword
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locConfirmNewPassword
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
  end
GenerateTab2  Routine
  If p_web.GSV('UserMobileRequired') = 1
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Mobile Number Required')&'</a></h3>' & CRLF & p_web.DivHeader('tab_FormNewPassword2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_FormNewPassword2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_FormNewPassword2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_FormNewPassword2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Mobile Number Required')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_FormNewPassword2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Mobile Number Required')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_FormNewPassword2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Mobile Number Required')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_FormNewPassword2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locMobileNumber
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locMobileNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
  end


Validate::txtCompany  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::txtCompany  ! copies value to session value if valid.

ValidateValue::txtCompany  Routine
    If not (1=0)
    End

Value::txtCompany  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormNewPassword_' & p_web._nocolon('txtCompany') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="txtCompany" class="'&clip('green bold')&'"'&clip(loc:extra)&'>' & p_web.Translate(p_web.GSV('LoginDetails1'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Validate::txtUser  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::txtUser  ! copies value to session value if valid.

ValidateValue::txtUser  Routine
    If not (1=0)
    End

Value::txtUser  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormNewPassword_' & p_web._nocolon('txtUser') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="txtUser" class="'&clip('green bold')&'"'&clip(loc:extra)&'>' & p_web.Translate(p_web.GSV('LoginDetails2'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::locNewPassword  Routine
  packet = clip(packet) & p_web.DivHeader('FormNewPassword_' & p_web._nocolon('locNewPassword') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('New Password:'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locNewPassword  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locNewPassword = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locNewPassword = p_web.GetValue('Value')
  End
  do ValidateValue::locNewPassword  ! copies value to session value if valid.
  do Value::locNewPassword
  do SendAlert

ValidateValue::locNewPassword  Routine
    If not (1=0)
  If locNewPassword = '' and p_web.GSV('NewPasswordRequired') = 1
    loc:Invalid = 'locNewPassword'
    locNewPassword:IsInvalid = true
    loc:alert = p_web.translate('New Password:') & ' ' & p_web.site.RequiredText
  End
    locNewPassword = Upper(locNewPassword)
      if loc:invalid = '' then p_web.SetSessionValue('locNewPassword',locNewPassword).
    End

Value::locNewPassword  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormNewPassword_' & p_web._nocolon('locNewPassword') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If (p_web.GSV('NewPasswordRequired') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  End
  If loc:retrying
    locNewPassword = p_web.RestoreValue('locNewPassword')
    do ValidateValue::locNewPassword
    If locNewPassword:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- locNewPassword
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locNewPassword'',''formnewpassword_locnewpassword_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('password','locNewPassword',p_web.GetSessionValueFormat('locNewPassword'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::locConfirmNewPassword  Routine
  packet = clip(packet) & p_web.DivHeader('FormNewPassword_' & p_web._nocolon('locConfirmNewPassword') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Confirm New Password:'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locConfirmNewPassword  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locConfirmNewPassword = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locConfirmNewPassword = p_web.GetValue('Value')
  End
  do ValidateValue::locConfirmNewPassword  ! copies value to session value if valid.
  do Value::locConfirmNewPassword
  do SendAlert

ValidateValue::locConfirmNewPassword  Routine
    If not (1=0)
  If locConfirmNewPassword = '' and p_web.GSV('NewPasswordRequired') = 1
    loc:Invalid = 'locConfirmNewPassword'
    locConfirmNewPassword:IsInvalid = true
    loc:alert = p_web.translate('Confirm New Password:') & ' ' & p_web.site.RequiredText
  End
    locConfirmNewPassword = Upper(locConfirmNewPassword)
      if loc:invalid = '' then p_web.SetSessionValue('locConfirmNewPassword',locConfirmNewPassword).
    End

Value::locConfirmNewPassword  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormNewPassword_' & p_web._nocolon('locConfirmNewPassword') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If (p_web.GSV('NewPasswordRequired') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  End
  If loc:retrying
    locConfirmNewPassword = p_web.RestoreValue('locConfirmNewPassword')
    do ValidateValue::locConfirmNewPassword
    If locConfirmNewPassword:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- locConfirmNewPassword
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locConfirmNewPassword'',''formnewpassword_locconfirmnewpassword_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('password','locConfirmNewPassword',p_web.GetSessionValueFormat('locConfirmNewPassword'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::locMobileNumber  Routine
  packet = clip(packet) & p_web.DivHeader('FormNewPassword_' & p_web._nocolon('locMobileNumber') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Mobile Number:'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locMobileNumber  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locMobileNumber = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locMobileNumber = p_web.GetValue('Value')
  End
  do ValidateValue::locMobileNumber  ! copies value to session value if valid.
  do Value::locMobileNumber
  do SendAlert

ValidateValue::locMobileNumber  Routine
    If not (1=0)
  If locMobileNumber = '' and p_web.GSV('UserMobileRequired') = 1
    loc:Invalid = 'locMobileNumber'
    locMobileNumber:IsInvalid = true
    loc:alert = p_web.translate('Mobile Number:') & ' ' & p_web.site.RequiredText
  End
    locMobileNumber = Upper(locMobileNumber)
      if loc:invalid = '' then p_web.SetSessionValue('locMobileNumber',locMobileNumber).
    End

Value::locMobileNumber  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormNewPassword_' & p_web._nocolon('locMobileNumber') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If (p_web.GSV('UserMobileRequired') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  End
  If loc:retrying
    locMobileNumber = p_web.RestoreValue('locMobileNumber')
    do ValidateValue::locMobileNumber
    If locMobileNumber:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- locMobileNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locMobileNumber'',''formnewpassword_locmobilenumber_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locMobileNumber',p_web.GetSessionValueFormat('locMobileNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('FormNewPassword_nexttab_' & 0)
    If loc:Invalid then exit.
  of lower('FormNewPassword_nexttab_' & 1)
    locNewPassword = p_web.GSV('locNewPassword')
    do ValidateValue::locNewPassword
    If loc:Invalid
      loc:retrying = 1
      do Value::locNewPassword
      !do SendAlert
      !exit
    End
    locConfirmNewPassword = p_web.GSV('locConfirmNewPassword')
    do ValidateValue::locConfirmNewPassword
    If loc:Invalid
      loc:retrying = 1
      do Value::locConfirmNewPassword
      !do SendAlert
      !exit
    End
    If loc:Invalid then exit.
  of lower('FormNewPassword_nexttab_' & 2)
    locMobileNumber = p_web.GSV('locMobileNumber')
    do ValidateValue::locMobileNumber
    If loc:Invalid
      loc:retrying = 1
      do Value::locMobileNumber
      !do SendAlert
      !exit
    End
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_FormNewPassword_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('FormNewPassword_tab_' & 0)
    do GenerateTab0
  of lower('FormNewPassword_tab_' & 1)
    do GenerateTab1
  of lower('FormNewPassword_locNewPassword_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locNewPassword
      of event:timer
        do Value::locNewPassword
      else
        do Value::locNewPassword
      end
  of lower('FormNewPassword_locConfirmNewPassword_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locConfirmNewPassword
      of event:timer
        do Value::locConfirmNewPassword
      else
        do Value::locConfirmNewPassword
      end
  of lower('FormNewPassword_tab_' & 2)
    do GenerateTab2
  of lower('FormNewPassword_locMobileNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locMobileNumber
      of event:timer
        do Value::locMobileNumber
      else
        do Value::locMobileNumber
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('FormNewPassword_form:ready_',1)

  p_web.SetSessionValue('FormNewPassword_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_FormNewPassword',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('FormNewPassword_form:ready_',1)
  p_web.SetSessionValue('FormNewPassword_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormNewPassword',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('FormNewPassword_form:ready_',1)
  p_web.SetSessionValue('FormNewPassword_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('FormNewPassword:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('FormNewPassword_form:ready_',1)
  p_web.SetSessionValue('FormNewPassword_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('FormNewPassword:Primed',0)
  p_web.setsessionvalue('showtab_FormNewPassword',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
  If p_web.GSV('NewPasswordRequired') = 1
      If not (1=0)
          If p_web.IfExistsValue('locNewPassword')
            locNewPassword = p_web.GetValue('locNewPassword')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('locConfirmNewPassword')
            locConfirmNewPassword = p_web.GetValue('locConfirmNewPassword')
          End
      End
  End
  If p_web.GSV('UserMobileRequired') = 1
      If not (1=0)
          If p_web.IfExistsValue('locMobileNumber')
            locMobileNumber = p_web.GetValue('locMobileNumber')
          End
      End
  End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord
      IF (loc:Invalid)
          EXIT
      END
      IF (p_web.GSV('NewPasswordRequired') = 1)
          ! Do the passwords match?
          IF (p_web.GSV('locNewPassword') <> p_web.GSV('locConfirmNewPassword'))
              loc:Invalid = 'locConfirmNewPassword'
              loc:Alert = 'The entered passwords do not match.'
              EXIT
          END
  
          ! Has the password changed?
          IF (p_web.GSV('locNewPassword') = p_web.GSV('loc:Password'))
              loc:Invalid = 'locNewPassword'
              loc:Alert = 'You have not entered a NEW password.'
              EXIT
          END !IF
          ! Is the password already taken?
          Access:USERS.Clearkey(use:Password_Key)
          use:Password = p_web.GSV('locNewPassword')
          IF (Access:USERS.Tryfetch(use:Password_Key) = Level:Benign)
              loc:Invalid = 'locNewPassword'
              loc:Alert = 'The selected password is already in use. Please try again.'
              EXIT
          END  ! IF
  
      END ! IF
  
      IF (p_web.GSV('UserMobileRequired') = 1)
          IF (NOT PassMobileNumberFormat(p_web.GSV('locMobileNumber')))
              loc:Alert = 'The Mobile Number Format Is Not Correct'
              loc:Invalid = 'locMobileNumber'
              EXIT
          END ! IF
          ! All ok, get original user and update it
          Access:USERS.Clearkey(use:Password_Key)
          use:Password = p_web.GSV('loc:Password')
          IF (Access:USERS.Tryfetch(use:Password_Key))
              loc:Alert = 'An Error Has Occurred'
              loc:Invalid = 'locNewPassword'
              EXIT
          END !IF
      END ! IF
  
      ! All ok, get original user and update it
      Access:USERS.Clearkey(use:Password_Key)
      use:Password = p_web.GSV('loc:Password')
      IF (Access:USERS.Tryfetch(use:Password_Key))
          loc:Alert = 'An Error Has Occurred'
          loc:Invalid = 'locNewPassword'
          EXIT
      END !IF
  
      IF (p_web.GSV('NewPasswordRequired') = 1)
          IF (p_web.GSV('locNewPassword') <> '')
              use:Password = p_web.GSV('locNewPassword')
              use:PasswordLastChanged = TODAY()
          END ! IF
      END ! IF
  
      IF (p_web.GSV('UserMobileRequired') = 1)
          IF (p_web.GSV('locMobileNumber') <> '')
              use:MobileNumber = p_web.GSV('locMobileNumber')
          END ! IF
      END ! IF
  
      Access:USERS.TryUpdate()
  
      SetLoginDefaults(p_web)

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormNewPassword_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormNewPassword_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 3
    loc:InvalidTab += 1
    do ValidateValue::txtCompany
    If loc:Invalid then exit.
    do ValidateValue::txtUser
    If loc:Invalid then exit.
  ! tab = 1
  If p_web.GSV('NewPasswordRequired') = 1
    loc:InvalidTab += 1
    do ValidateValue::locNewPassword
    If loc:Invalid then exit.
    do ValidateValue::locConfirmNewPassword
    If loc:Invalid then exit.
  End
  ! tab = 2
  If p_web.GSV('UserMobileRequired') = 1
    loc:InvalidTab += 1
    do ValidateValue::locMobileNumber
    If loc:Invalid then exit.
  End
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('FormNewPassword:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locNewPassword')
  p_web.StoreValue('locConfirmNewPassword')
  p_web.StoreValue('locMobileNumber')

SaveSessionVars  ROUTINE
     p_web.SSV('Ans',Ans) ! LONG
     p_web.SSV('locNewPassword',locNewPassword) ! STRING(30)
     p_web.SSV('locConfirmNewPassword',locConfirmNewPassword) ! STRING(30)
     p_web.SSV('locMobileNumber',locMobileNumber) ! STRING(20)
RestoreSessionVars  ROUTINE
     Ans = p_web.GSV('Ans') ! LONG
     locNewPassword = p_web.GSV('locNewPassword') ! STRING(30)
     locConfirmNewPassword = p_web.GSV('locConfirmNewPassword') ! STRING(30)
     locMobileNumber = p_web.GSV('locMobileNumber') ! STRING(20)
SetLoginDefaults     PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
FilesOpened     BYTE(0)
  CODE
    Do OpenFiles

    Access:USERS.Clearkey(use:Password_Key)
    use:Password = p_web.GSV('loc:Password')
    IF (Access:USERS.Tryfetch(use:Password_Key) = Level:Benign)
        Access:TRADEACC.Clearkey(tra:SiteLocationKey)
        tra:SiteLocation = use:Location
        IF (Access:TRADEACC.Tryfetch(tra:SiteLocationKey) = Level:Benign)
            p_web.SSV('BookingBranchID',tra:BranchIdentification)
            p_web.SSV('BookingAccount',Clip(tra:Account_Number))
            p_web.SSV('BookingName',Clip(DoCaps(tra:Company_Name)))
            p_web.SSV('BookingUserPassword',use:Password)
            p_web.SSV('BookingUserCode',use:User_Code)
            p_web.SSV('BookingSiteLocation',tra:SiteLocation)
            p_web.SSV('BookingRestrictParts',use:RestrictParts)
            p_web.SSV('BookingRestrictChargeable',use:RestrictChargeable)
            p_web.SSV('BookingSkillLevel',use:SkillLevel)
            p_web.SSV('BookingStoresAccount',tra:StoresAccount)
            If tra:Account_Number = GETINI('BOOKING','HeadAccount',,Clip(Path()) & '\SB2KDEF.INI')
                p_web.SSV('BookingSite','ARC')
                !p_web.SSV('Filter:BrowseJobs','Upper(job:Location) <> <39,39> And Upper(job:ESN) <> <39,39>')
                p_web.SSV('Filter:BrowseJobs','')

            Else ! If tra:Account_Number = GETINI('BOOKING','HeadAccountNumber',,Clip(Path()) & '\SB2KDEF.INI')
                p_web.SSV('BookingSite','RRC')
                !p_web.SSV('Filter:BrowseJobs','Upper(wob:HeadAccountNumber) = Upper(<39>' & clip(tra:Account_Number) & '<39>)  And Upper(job:Location) <> <39,39> And Upper(job:ESN) <> <39,39>')
                p_web.SSV('Filter:BrowseJobs','Upper(wob:HeadAccountNumber) = Upper(<39>' & clip(tra:Account_Number) & '<39>)')
            End ! If tra:Account_Number = GETINI('BOOKING','HeadAccountNumber',,Clip(Path()) & '\SB2KDEF.INI')
            ! Set Some Defaults
            p_web.SSV('Default:InTransitPUP',GETINI('RRC','InTransitToPUPLocation',,Path() & '\SB2KDEF.INI'))
            p_web.SSV('Default:PUPLocation',GETINI('RRC','AtPUPLocation',,Path() & '\SB2KDEF.INI'))
            p_web.SSV('Default:ARCLocation',GETINI('RRC','ARCLocation',,CLIP(PATH())&'\SB2KDEF.INI'))
            p_web.SSV('Default:StatusSendToRRC',GETINI('RRC','StatusSendToRRC',,CLIP(PATH())&'\SB2KDEF.INI'))
            p_web.SSV('Default:DespatchToCustomer',GETINI('RRC','DespatchToCustomer',,CLIP(PATH())&'\SB2KDEF.INI'))
            p_web.SSV('Default:InTransitARC',GETINI('RRC','InTransit',,CLIP(PATH())&'\SB2KDEF.INI'))
            p_web.SSV('Default:RRCLocation',GETINI('RRC','RRCLocation',,CLIP(PATH())&'\SB2KDEF.INI'))
            p_web.SSV('Default:StatusSendToARC',GETINI('RRC','StatusSendToARC',,CLIP(PATH())&'\SB2KDEF.INI'))
            p_web.SSV('Default:InTransitRRC',GETINI('RRC','InTransitRRC',,CLIP(PATH())&'\SB2KDEF.INI'))
            p_web.SSV('Default:StatusReceivedFromPUP',GETINI('RRC','StatusReceivedFromPUP',,Clip(Path()) & '\SB2KDEF.INI'))
            p_web.SSV('Default:AccountNumber',tra:Account_Number)
            p_web.SSV('Default:SiteLocation',tra:SiteLocation)

            p_web.SSV('Default:TermsText',GETINI('PRINTING','TermsText',,PATH() & '\SB2KDEF.INI')) ! #12265 Set default for "terms" text (Bryan: 30/08/2011)
            p_web.DeleteSessionValue('loc:Password')
            glo:Password = use:Password

            p_web.SSV('ARC:AccountNumber',GETINI('BOOKING','HeadAccount',,Clip(Path()) & '\SB2KDEF.INI'))

            p_web.SSV('LoginDetails1','Site: ' & clip(tra:Company_Name))
            p_web.SSV('LoginDetails2','User: ' & clip(use:forename) & ' ' & clip(use:Surname))

            Access:TRADEACC.Clearkey(tra:Account_Number_Key)
            tra:Account_Number    = GETINI('BOOKING','HeadAccount',,Clip(Path()) & '\SB2KDEF.INI')
            if (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
                ! Found
                p_web.SSV('ARC:SiteLocation',tra:SiteLocation)
            else ! if (Access:TRADE.TryFetch(tra:Account_Number_Key) = Level:Benign)
                ! Error
            end ! if (Access:TRADE.TryFetch(tra:Account_Number_Key) = Level:Benign)

            IF (SecurityCheckFailed(use:Password,'JOBS - INSERT')) ! #11682 Check access. (Bryan: 07/09/2010)
                p_web.SSV('Hide:ButtonCreateNewJob',1)
            ELSE
                p_web.SSV('Hide:ButtonCreateNewJob',0)
            END

            IF (SecurityCheckFailed(use:Password,'JOBS - CHANGE'))
                p_web.SSV('Hide:ButtonJobSearch',1)
            ELSE
                p_web.SSV('Hide:ButtonJobSearch',0)
            END
            
        
        END  !IF
    END ! IF

    Do CloseFiles

    p_web.SSV('NewPasswordRequired','')
    p_web.SSV('UserMobileRequired','')
!--------------------------------------
OpenFiles  ROUTINE
  Access:USERS.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:USERS.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:USERS.Close
     Access:TRADEACC.Close
     FilesOpened = False
  END
SendSMSText          PROCEDURE  (String SentJob,String SentAuto,String SentSpecial,<NetWebServerWorker p_web>) ! Declare Procedure
  CODE
    VodacomClass.SendSMS(SentJob,SentAuto,SentSpecial,p_web)  ! #12477 Call the SMS routine (DBH: 17/05/2012)
AmendAddressOLD      PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
VariablesGroup       GROUP,PRE(tmp)                        !
CompanyName          STRING(30)                            !TempCompanyName
AddressLine1         STRING(30)                            !TempAddressLine1
AddressLine2         STRING(30)                            !TempAddressLine2
Suburb               STRING(30)                            !TempSuburb
Postcode             STRING(30)                            !TempPostcode
TelephoneNumber      STRING(30)                            !TempTelephoneNumber
FaxNumber            STRING(30)                            !TempFaxNumber
EmailAddress         STRING(255)                           !Email Address
EndUserTelephoneNumber STRING(30)                          !End User Tel No
IDNumber             STRING(30)                            !ID Number
SMSNotification      BYTE(0)                               !SMS Notification
NotificationMobileNumber STRING(30)                        !NotificationMobileNumber
EmailNotification    BYTE(0)                               !EmailNotification
NotificationEmailAddress STRING(255)                       !Notification Email Address
CompanyNameDelivery  STRING(30)                            !tmp:CompanyNameDelivery
AddressLine1Delivery STRING(30)                            !tmp:AddressLine1Delivery
AddressLine2Delivery STRING(30)                            !AddressLine2Delivery
SuburbDelivery       STRING(30)                            !SubrubDelivery
PostcodeDelivery     STRING(30)                            !PostcodeDelivery
TelephoneNumberDelivery STRING(30)                         !TelephoneNumberDelivery
CompanyNameCollection STRING(30)                           !CompanyNameCollection
AddressLine1Collection STRING(30)                          !AddressLine1Collection
AddressLine2Collection STRING(30)                          !AddressLine2Collection
SuburbCollection     STRING(30)                            !SuburbCollection
PostcodeCollection   STRING(30)                            !PostcodeCollection
TelephoneNumberCollection STRING(30)                       !TelephoneNumberCollection
HubCustomer          STRING(30)                            !Hub
HubCollection        STRING(30)                            !Hub
HubDelivery          STRING(30)                            !Hub
                     END                                   !
ReplicateCustomerToDelivery BYTE(0)                        !ReplicateCustomerToDelivery
ReplicateCustomerToCollection BYTE(0)                      !ReplicateCustomerToCollection
ReplicateCollectionToDelivery BYTE(0)                      !ReplicateCollectionToDelivery
ReplicateDeliveryToCollection BYTE(0)                      !ReplicateDeliveryToCollection
FilesOpened     Long
SUBTRACC::State  USHORT
COURIER::State  USHORT
JOBNOTES::State  USHORT
SUBACCAD::State  USHORT
SUBURB::State  USHORT
JOBS::State  USHORT
JOBSE::State  USHORT
WEBJOB::State  USHORT
JOBSE2::State  USHORT
TRANTYPE::State  USHORT
temp:Title:IsInvalid  Long
job:Company_Name:IsInvalid  Long
job:Address_Line1:IsInvalid  Long
job:Address_Line2:IsInvalid  Long
job:Address_Line3:IsInvalid  Long
jobe2:HubCustomer:IsInvalid  Long
job:Postcode:IsInvalid  Long
job:Telephone_Number:IsInvalid  Long
job:Fax_Number:IsInvalid  Long
jobe:EndUserEmailAddress:IsInvalid  Long
jobe:EndUserTelNo:IsInvalid  Long
jobe:VatNumber:IsInvalid  Long
jobe2:IDNumber:IsInvalid  Long
jobe2:SMSNotification:IsInvalid  Long
jobe2:SMSAlertNumber:IsInvalid  Long
jobe2:EmailNotification:IsInvalid  Long
jobe2:EmailAlertAddress:IsInvalid  Long
jobe2:CourierWaybillNumber:IsInvalid  Long
DeliveryAddress:IsInvalid  Long
CollectionAddress:IsInvalid  Long
job:Company_Name_Delivery:IsInvalid  Long
job:Company_Name_Collection:IsInvalid  Long
job:Address_Line1_Delivery:IsInvalid  Long
job:Address_Line1_Collection:IsInvalid  Long
job:Address_Line2_Delivery:IsInvalid  Long
job:Address_Line2_Collection:IsInvalid  Long
job:Address_Line3_Delivery:IsInvalid  Long
job:Address_Line3_Collection:IsInvalid  Long
jobe2:HubDelivery:IsInvalid  Long
jobe2:HubCollection:IsInvalid  Long
job:Postcode_Delivery:IsInvalid  Long
job:Postcode_Collection:IsInvalid  Long
job:Telephone_Delivery:IsInvalid  Long
job:Telephone_Collection:IsInvalid  Long
Button:CopyEndUserAddress:IsInvalid  Long
Button:CopyEndUserAddress1:IsInvalid  Long
jbn:Delivery_Text:IsInvalid  Long
jbn:Collection_Text:IsInvalid  Long
jbn:DelContactName:IsInvalid  Long
jbn:ColContatName:IsInvalid  Long
jbn:DelDepartment:IsInvalid  Long
jbn:ColDepartment:IsInvalid  Long
jobe:Sub_Sub_Account:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('AmendAddressOLD')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'AmendAddressOLD_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('AmendAddressOLD','')
    p_web.DivHeader('AmendAddressOLD',p_web.combine(p_web.site.style.formdiv,))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('AmendAddressOLD') = 0
        p_web.AddPreCall('AmendAddressOLD')
        p_web.DivHeader('popup_AmendAddressOLD','nt-hidden')
        p_web.DivHeader('AmendAddressOLD',p_web.combine(p_web.site.style.formdiv,))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_AmendAddressOLD_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_AmendAddressOLD_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferAmendAddressOLD',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_AmendAddressOLD',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferAmendAddressOLD',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_AmendAddressOLD',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferAmendAddressOLD',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_AmendAddressOLD',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_AmendAddressOLD',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferAmendAddressOLD',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_AmendAddressOLD',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferAmendAddressOLD',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_AmendAddressOLD',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_AmendAddressOLD',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('AmendAddressOLD')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(SUBTRACC)
  p_web._OpenFile(COURIER)
  p_web._OpenFile(JOBNOTES)
  p_web._OpenFile(SUBACCAD)
  p_web._OpenFile(SUBURB)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(WEBJOB)
  p_web._OpenFile(JOBSE2)
  p_web._OpenFile(TRANTYPE)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(COURIER)
  p_Web._CloseFile(JOBNOTES)
  p_Web._CloseFile(SUBACCAD)
  p_Web._CloseFile(SUBURB)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(JOBSE2)
  p_Web._CloseFile(TRANTYPE)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE
  loc:EipClm = upper(p_web.GetValue('_EIPClm'))
  p_web.DeleteValue('_EIPClm')
  case loc:EipClm
  of upper('Button:CopyEndUserAddress')
    do Validate::Button:CopyEndUserAddress
  of upper('Button:CopyEndUserAddress1')
    do Validate::Button:CopyEndUserAddress1
  of ''
    case upper(p_web.GetValue('_calledfrom_'))
    end
  end

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('AmendAddressOLD_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'AmendAddressOLD'
    end
    p_web.formsettings.proc = 'AmendAddressOLD'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('job:Company_Name')
    p_web.SetPicture('job:Company_Name','@s30')
  End
  p_web.SetSessionPicture('job:Company_Name','@s30')
  If p_web.IfExistsValue('job:Address_Line1')
    p_web.SetPicture('job:Address_Line1','@s30')
  End
  p_web.SetSessionPicture('job:Address_Line1','@s30')
  If p_web.IfExistsValue('job:Address_Line2')
    p_web.SetPicture('job:Address_Line2','@s30')
  End
  p_web.SetSessionPicture('job:Address_Line2','@s30')
  If p_web.IfExistsValue('job:Address_Line3')
    p_web.SetPicture('job:Address_Line3','@s30')
  End
  p_web.SetSessionPicture('job:Address_Line3','@s30')
  If p_web.IfExistsValue('jobe2:HubCustomer')
    p_web.SetPicture('jobe2:HubCustomer','@s30')
  End
  p_web.SetSessionPicture('jobe2:HubCustomer','@s30')
  If p_web.IfExistsValue('job:Postcode')
    p_web.SetPicture('job:Postcode','@s10')
  End
  p_web.SetSessionPicture('job:Postcode','@s10')
  If p_web.IfExistsValue('job:Telephone_Number')
    p_web.SetPicture('job:Telephone_Number','@s15')
  End
  p_web.SetSessionPicture('job:Telephone_Number','@s15')
  If p_web.IfExistsValue('job:Fax_Number')
    p_web.SetPicture('job:Fax_Number','@s15')
  End
  p_web.SetSessionPicture('job:Fax_Number','@s15')
  If p_web.IfExistsValue('jobe:EndUserEmailAddress')
    p_web.SetPicture('jobe:EndUserEmailAddress','@s255')
  End
  p_web.SetSessionPicture('jobe:EndUserEmailAddress','@s255')
  If p_web.IfExistsValue('jobe:EndUserTelNo')
    p_web.SetPicture('jobe:EndUserTelNo','@s15')
  End
  p_web.SetSessionPicture('jobe:EndUserTelNo','@s15')
  If p_web.IfExistsValue('jobe:VatNumber')
    p_web.SetPicture('jobe:VatNumber','@s30')
  End
  p_web.SetSessionPicture('jobe:VatNumber','@s30')
  If p_web.IfExistsValue('jobe2:IDNumber')
    p_web.SetPicture('jobe2:IDNumber','@s13')
  End
  p_web.SetSessionPicture('jobe2:IDNumber','@s13')
  If p_web.IfExistsValue('jobe2:SMSAlertNumber')
    p_web.SetPicture('jobe2:SMSAlertNumber','@s30')
  End
  p_web.SetSessionPicture('jobe2:SMSAlertNumber','@s30')
  If p_web.IfExistsValue('jobe2:EmailAlertAddress')
    p_web.SetPicture('jobe2:EmailAlertAddress','@s255')
  End
  p_web.SetSessionPicture('jobe2:EmailAlertAddress','@s255')
  If p_web.IfExistsValue('jobe2:CourierWaybillNumber')
    p_web.SetPicture('jobe2:CourierWaybillNumber','@s30')
  End
  p_web.SetSessionPicture('jobe2:CourierWaybillNumber','@s30')
  If p_web.IfExistsValue('job:Company_Name_Delivery')
    p_web.SetPicture('job:Company_Name_Delivery','@s30')
  End
  p_web.SetSessionPicture('job:Company_Name_Delivery','@s30')
  If p_web.IfExistsValue('job:Company_Name_Collection')
    p_web.SetPicture('job:Company_Name_Collection','@s30')
  End
  p_web.SetSessionPicture('job:Company_Name_Collection','@s30')
  If p_web.IfExistsValue('job:Address_Line1_Delivery')
    p_web.SetPicture('job:Address_Line1_Delivery','@s30')
  End
  p_web.SetSessionPicture('job:Address_Line1_Delivery','@s30')
  If p_web.IfExistsValue('job:Address_Line1_Collection')
    p_web.SetPicture('job:Address_Line1_Collection','@s30')
  End
  p_web.SetSessionPicture('job:Address_Line1_Collection','@s30')
  If p_web.IfExistsValue('job:Address_Line2_Delivery')
    p_web.SetPicture('job:Address_Line2_Delivery','@s30')
  End
  p_web.SetSessionPicture('job:Address_Line2_Delivery','@s30')
  If p_web.IfExistsValue('job:Address_Line2_Collection')
    p_web.SetPicture('job:Address_Line2_Collection','@s30')
  End
  p_web.SetSessionPicture('job:Address_Line2_Collection','@s30')
  If p_web.IfExistsValue('job:Address_Line3_Delivery')
    p_web.SetPicture('job:Address_Line3_Delivery','@s30')
  End
  p_web.SetSessionPicture('job:Address_Line3_Delivery','@s30')
  If p_web.IfExistsValue('job:Address_Line3_Collection')
    p_web.SetPicture('job:Address_Line3_Collection','@s30')
  End
  p_web.SetSessionPicture('job:Address_Line3_Collection','@s30')
  If p_web.IfExistsValue('jobe2:HubDelivery')
    p_web.SetPicture('jobe2:HubDelivery','@s30')
  End
  p_web.SetSessionPicture('jobe2:HubDelivery','@s30')
  If p_web.IfExistsValue('jobe2:HubCollection')
    p_web.SetPicture('jobe2:HubCollection','@s30')
  End
  p_web.SetSessionPicture('jobe2:HubCollection','@s30')
  If p_web.IfExistsValue('job:Postcode_Delivery')
    p_web.SetPicture('job:Postcode_Delivery','@s10')
  End
  p_web.SetSessionPicture('job:Postcode_Delivery','@s10')
  If p_web.IfExistsValue('job:Postcode_Collection')
    p_web.SetPicture('job:Postcode_Collection','@s10')
  End
  p_web.SetSessionPicture('job:Postcode_Collection','@s10')
  If p_web.IfExistsValue('job:Telephone_Delivery')
    p_web.SetPicture('job:Telephone_Delivery','@s15')
  End
  p_web.SetSessionPicture('job:Telephone_Delivery','@s15')
  If p_web.IfExistsValue('job:Telephone_Collection')
    p_web.SetPicture('job:Telephone_Collection','@s15')
  End
  p_web.SetSessionPicture('job:Telephone_Collection','@s15')
  If p_web.IfExistsValue('jbn:DelContactName')
    p_web.SetPicture('jbn:DelContactName','@s30')
  End
  p_web.SetSessionPicture('jbn:DelContactName','@s30')
  If p_web.IfExistsValue('jbn:ColContatName')
    p_web.SetPicture('jbn:ColContatName','@s30')
  End
  p_web.SetSessionPicture('jbn:ColContatName','@s30')
  If p_web.IfExistsValue('jbn:DelDepartment')
    p_web.SetPicture('jbn:DelDepartment','@s30')
  End
  p_web.SetSessionPicture('jbn:DelDepartment','@s30')
  If p_web.IfExistsValue('jbn:ColDepartment')
    p_web.SetPicture('jbn:ColDepartment','@s30')
  End
  p_web.SetSessionPicture('jbn:ColDepartment','@s30')
  If p_web.IfExistsValue('jobe:Sub_Sub_Account')
    p_web.SetPicture('jobe:Sub_Sub_Account','@s15')
  End
  p_web.SetSessionPicture('jobe:Sub_Sub_Account','@s15')

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'job:Address_Line3'
    p_web.setsessionvalue('showtab_AmendAddressOLD',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(SUBURB)
        p_web.setsessionvalue('job:Postcode',sur:Postcode)
        p_web.setsessionvalue('jobe2:HubCustomer',sur:Hub)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.jobe2:HubCustomer')
  End
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'job:Address_Line3_Delivery'
    p_web.setsessionvalue('showtab_AmendAddressOLD',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(SUBURB)
        p_web.setsessionvalue('job:Postcode_Delivery',sur:Postcode)
        p_web.setsessionvalue('jobe2:HubDelivery',sur:Hub)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.job:Address_Line3_Collection')
  Of 'job:Address_Line3_Collection'
    p_web.setsessionvalue('showtab_AmendAddressOLD',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(SUBURB)
        p_web.setsessionvalue('job:Postcode_Collection',sur:Postcode)
        p_web.setsessionvalue('jobe2:HubCollection',sur:Hub)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.jobe2:HubDelivery')
  End
  If p_web.GSV('Hide:SubSubAccount') <> 1
    loc:TabNumber += 1
  End
  Case p_Web.GetValue('lookupfield')
  Of 'jobe:Sub_Sub_Account'
    p_web.setsessionvalue('showtab_AmendAddressOLD',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(SUBACCAD)
        p_web.setsessionvalue('job:Postcode_Delivery',sua:Postcode)
        p_web.setsessionvalue('job:Address_Line1_Delivery',sua:addressline1)
        p_web.setsessionvalue('job:Address_Line2_Delivery',sua:addressline2)
        p_web.setsessionvalue('job:Address_Line3_Delivery',sua:addressline3)
        p_web.setsessionvalue('job:Telephone_Delivery',sua:TelephoneNumber)
        p_web.setsessionvalue('job:Company_Name_Delivery',sua:CompanyName)
    End
  End
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  if p_web.IfExistsValue('job:Company_Name') = 0
    p_web.SetSessionValue('job:Company_Name',job:Company_Name)
  Else
    job:Company_Name = p_web.GetSessionValue('job:Company_Name')
  End
  if p_web.IfExistsValue('job:Address_Line1') = 0
    p_web.SetSessionValue('job:Address_Line1',job:Address_Line1)
  Else
    job:Address_Line1 = p_web.GetSessionValue('job:Address_Line1')
  End
  if p_web.IfExistsValue('job:Address_Line2') = 0
    p_web.SetSessionValue('job:Address_Line2',job:Address_Line2)
  Else
    job:Address_Line2 = p_web.GetSessionValue('job:Address_Line2')
  End
  if p_web.IfExistsValue('job:Address_Line3') = 0
    p_web.SetSessionValue('job:Address_Line3',job:Address_Line3)
  Else
    job:Address_Line3 = p_web.GetSessionValue('job:Address_Line3')
  End
  if p_web.IfExistsValue('jobe2:HubCustomer') = 0
    p_web.SetSessionValue('jobe2:HubCustomer',jobe2:HubCustomer)
  Else
    jobe2:HubCustomer = p_web.GetSessionValue('jobe2:HubCustomer')
  End
  if p_web.IfExistsValue('job:Postcode') = 0
    p_web.SetSessionValue('job:Postcode',job:Postcode)
  Else
    job:Postcode = p_web.GetSessionValue('job:Postcode')
  End
  if p_web.IfExistsValue('job:Telephone_Number') = 0
    p_web.SetSessionValue('job:Telephone_Number',job:Telephone_Number)
  Else
    job:Telephone_Number = p_web.GetSessionValue('job:Telephone_Number')
  End
  if p_web.IfExistsValue('job:Fax_Number') = 0
    p_web.SetSessionValue('job:Fax_Number',job:Fax_Number)
  Else
    job:Fax_Number = p_web.GetSessionValue('job:Fax_Number')
  End
  if p_web.IfExistsValue('jobe:EndUserEmailAddress') = 0
    p_web.SetSessionValue('jobe:EndUserEmailAddress',jobe:EndUserEmailAddress)
  Else
    jobe:EndUserEmailAddress = p_web.GetSessionValue('jobe:EndUserEmailAddress')
  End
  if p_web.IfExistsValue('jobe:EndUserTelNo') = 0
    p_web.SetSessionValue('jobe:EndUserTelNo',jobe:EndUserTelNo)
  Else
    jobe:EndUserTelNo = p_web.GetSessionValue('jobe:EndUserTelNo')
  End
  if p_web.IfExistsValue('jobe:VatNumber') = 0
    p_web.SetSessionValue('jobe:VatNumber',jobe:VatNumber)
  Else
    jobe:VatNumber = p_web.GetSessionValue('jobe:VatNumber')
  End
  if p_web.IfExistsValue('jobe2:IDNumber') = 0
    p_web.SetSessionValue('jobe2:IDNumber',jobe2:IDNumber)
  Else
    jobe2:IDNumber = p_web.GetSessionValue('jobe2:IDNumber')
  End
  if p_web.IfExistsValue('jobe2:SMSNotification') = 0
    p_web.SetSessionValue('jobe2:SMSNotification',jobe2:SMSNotification)
  Else
    jobe2:SMSNotification = p_web.GetSessionValue('jobe2:SMSNotification')
  End
  if p_web.IfExistsValue('jobe2:SMSAlertNumber') = 0
    p_web.SetSessionValue('jobe2:SMSAlertNumber',jobe2:SMSAlertNumber)
  Else
    jobe2:SMSAlertNumber = p_web.GetSessionValue('jobe2:SMSAlertNumber')
  End
  if p_web.IfExistsValue('jobe2:EmailNotification') = 0
    p_web.SetSessionValue('jobe2:EmailNotification',jobe2:EmailNotification)
  Else
    jobe2:EmailNotification = p_web.GetSessionValue('jobe2:EmailNotification')
  End
  if p_web.IfExistsValue('jobe2:EmailAlertAddress') = 0
    p_web.SetSessionValue('jobe2:EmailAlertAddress',jobe2:EmailAlertAddress)
  Else
    jobe2:EmailAlertAddress = p_web.GetSessionValue('jobe2:EmailAlertAddress')
  End
  if p_web.IfExistsValue('jobe2:CourierWaybillNumber') = 0
    p_web.SetSessionValue('jobe2:CourierWaybillNumber',jobe2:CourierWaybillNumber)
  Else
    jobe2:CourierWaybillNumber = p_web.GetSessionValue('jobe2:CourierWaybillNumber')
  End
  if p_web.IfExistsValue('job:Company_Name_Delivery') = 0
    p_web.SetSessionValue('job:Company_Name_Delivery',job:Company_Name_Delivery)
  Else
    job:Company_Name_Delivery = p_web.GetSessionValue('job:Company_Name_Delivery')
  End
  if p_web.IfExistsValue('job:Company_Name_Collection') = 0
    p_web.SetSessionValue('job:Company_Name_Collection',job:Company_Name_Collection)
  Else
    job:Company_Name_Collection = p_web.GetSessionValue('job:Company_Name_Collection')
  End
  if p_web.IfExistsValue('job:Address_Line1_Delivery') = 0
    p_web.SetSessionValue('job:Address_Line1_Delivery',job:Address_Line1_Delivery)
  Else
    job:Address_Line1_Delivery = p_web.GetSessionValue('job:Address_Line1_Delivery')
  End
  if p_web.IfExistsValue('job:Address_Line1_Collection') = 0
    p_web.SetSessionValue('job:Address_Line1_Collection',job:Address_Line1_Collection)
  Else
    job:Address_Line1_Collection = p_web.GetSessionValue('job:Address_Line1_Collection')
  End
  if p_web.IfExistsValue('job:Address_Line2_Delivery') = 0
    p_web.SetSessionValue('job:Address_Line2_Delivery',job:Address_Line2_Delivery)
  Else
    job:Address_Line2_Delivery = p_web.GetSessionValue('job:Address_Line2_Delivery')
  End
  if p_web.IfExistsValue('job:Address_Line2_Collection') = 0
    p_web.SetSessionValue('job:Address_Line2_Collection',job:Address_Line2_Collection)
  Else
    job:Address_Line2_Collection = p_web.GetSessionValue('job:Address_Line2_Collection')
  End
  if p_web.IfExistsValue('job:Address_Line3_Delivery') = 0
    p_web.SetSessionValue('job:Address_Line3_Delivery',job:Address_Line3_Delivery)
  Else
    job:Address_Line3_Delivery = p_web.GetSessionValue('job:Address_Line3_Delivery')
  End
  if p_web.IfExistsValue('job:Address_Line3_Collection') = 0
    p_web.SetSessionValue('job:Address_Line3_Collection',job:Address_Line3_Collection)
  Else
    job:Address_Line3_Collection = p_web.GetSessionValue('job:Address_Line3_Collection')
  End
  if p_web.IfExistsValue('jobe2:HubDelivery') = 0
    p_web.SetSessionValue('jobe2:HubDelivery',jobe2:HubDelivery)
  Else
    jobe2:HubDelivery = p_web.GetSessionValue('jobe2:HubDelivery')
  End
  if p_web.IfExistsValue('jobe2:HubCollection') = 0
    p_web.SetSessionValue('jobe2:HubCollection',jobe2:HubCollection)
  Else
    jobe2:HubCollection = p_web.GetSessionValue('jobe2:HubCollection')
  End
  if p_web.IfExistsValue('job:Postcode_Delivery') = 0
    p_web.SetSessionValue('job:Postcode_Delivery',job:Postcode_Delivery)
  Else
    job:Postcode_Delivery = p_web.GetSessionValue('job:Postcode_Delivery')
  End
  if p_web.IfExistsValue('job:Postcode_Collection') = 0
    p_web.SetSessionValue('job:Postcode_Collection',job:Postcode_Collection)
  Else
    job:Postcode_Collection = p_web.GetSessionValue('job:Postcode_Collection')
  End
  if p_web.IfExistsValue('job:Telephone_Delivery') = 0
    p_web.SetSessionValue('job:Telephone_Delivery',job:Telephone_Delivery)
  Else
    job:Telephone_Delivery = p_web.GetSessionValue('job:Telephone_Delivery')
  End
  if p_web.IfExistsValue('job:Telephone_Collection') = 0
    p_web.SetSessionValue('job:Telephone_Collection',job:Telephone_Collection)
  Else
    job:Telephone_Collection = p_web.GetSessionValue('job:Telephone_Collection')
  End
  if p_web.IfExistsValue('jbn:Delivery_Text') = 0
    p_web.SetSessionValue('jbn:Delivery_Text',jbn:Delivery_Text)
  Else
    jbn:Delivery_Text = p_web.GetSessionValue('jbn:Delivery_Text')
  End
  if p_web.IfExistsValue('jbn:Collection_Text') = 0
    p_web.SetSessionValue('jbn:Collection_Text',jbn:Collection_Text)
  Else
    jbn:Collection_Text = p_web.GetSessionValue('jbn:Collection_Text')
  End
  if p_web.IfExistsValue('jbn:DelContactName') = 0
    p_web.SetSessionValue('jbn:DelContactName',jbn:DelContactName)
  Else
    jbn:DelContactName = p_web.GetSessionValue('jbn:DelContactName')
  End
  if p_web.IfExistsValue('jbn:ColContatName') = 0
    p_web.SetSessionValue('jbn:ColContatName',jbn:ColContatName)
  Else
    jbn:ColContatName = p_web.GetSessionValue('jbn:ColContatName')
  End
  if p_web.IfExistsValue('jbn:DelDepartment') = 0
    p_web.SetSessionValue('jbn:DelDepartment',jbn:DelDepartment)
  Else
    jbn:DelDepartment = p_web.GetSessionValue('jbn:DelDepartment')
  End
  if p_web.IfExistsValue('jbn:ColDepartment') = 0
    p_web.SetSessionValue('jbn:ColDepartment',jbn:ColDepartment)
  Else
    jbn:ColDepartment = p_web.GetSessionValue('jbn:ColDepartment')
  End
  if p_web.IfExistsValue('jobe:Sub_Sub_Account') = 0
    p_web.SetSessionValue('jobe:Sub_Sub_Account',jobe:Sub_Sub_Account)
  Else
    jobe:Sub_Sub_Account = p_web.GetSessionValue('jobe:Sub_Sub_Account')
  End

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('job:Company_Name')
    job:Company_Name = p_web.GetValue('job:Company_Name')
    p_web.SetSessionValue('job:Company_Name',job:Company_Name)
  Else
    job:Company_Name = p_web.GetSessionValue('job:Company_Name')
  End
  if p_web.IfExistsValue('job:Address_Line1')
    job:Address_Line1 = p_web.GetValue('job:Address_Line1')
    p_web.SetSessionValue('job:Address_Line1',job:Address_Line1)
  Else
    job:Address_Line1 = p_web.GetSessionValue('job:Address_Line1')
  End
  if p_web.IfExistsValue('job:Address_Line2')
    job:Address_Line2 = p_web.GetValue('job:Address_Line2')
    p_web.SetSessionValue('job:Address_Line2',job:Address_Line2)
  Else
    job:Address_Line2 = p_web.GetSessionValue('job:Address_Line2')
  End
  if p_web.IfExistsValue('job:Address_Line3')
    job:Address_Line3 = p_web.GetValue('job:Address_Line3')
    p_web.SetSessionValue('job:Address_Line3',job:Address_Line3)
  Else
    job:Address_Line3 = p_web.GetSessionValue('job:Address_Line3')
  End
  if p_web.IfExistsValue('jobe2:HubCustomer')
    jobe2:HubCustomer = p_web.GetValue('jobe2:HubCustomer')
    p_web.SetSessionValue('jobe2:HubCustomer',jobe2:HubCustomer)
  Else
    jobe2:HubCustomer = p_web.GetSessionValue('jobe2:HubCustomer')
  End
  if p_web.IfExistsValue('job:Postcode')
    job:Postcode = p_web.GetValue('job:Postcode')
    p_web.SetSessionValue('job:Postcode',job:Postcode)
  Else
    job:Postcode = p_web.GetSessionValue('job:Postcode')
  End
  if p_web.IfExistsValue('job:Telephone_Number')
    job:Telephone_Number = p_web.GetValue('job:Telephone_Number')
    p_web.SetSessionValue('job:Telephone_Number',job:Telephone_Number)
  Else
    job:Telephone_Number = p_web.GetSessionValue('job:Telephone_Number')
  End
  if p_web.IfExistsValue('job:Fax_Number')
    job:Fax_Number = p_web.GetValue('job:Fax_Number')
    p_web.SetSessionValue('job:Fax_Number',job:Fax_Number)
  Else
    job:Fax_Number = p_web.GetSessionValue('job:Fax_Number')
  End
  if p_web.IfExistsValue('jobe:EndUserEmailAddress')
    jobe:EndUserEmailAddress = p_web.GetValue('jobe:EndUserEmailAddress')
    p_web.SetSessionValue('jobe:EndUserEmailAddress',jobe:EndUserEmailAddress)
  Else
    jobe:EndUserEmailAddress = p_web.GetSessionValue('jobe:EndUserEmailAddress')
  End
  if p_web.IfExistsValue('jobe:EndUserTelNo')
    jobe:EndUserTelNo = p_web.GetValue('jobe:EndUserTelNo')
    p_web.SetSessionValue('jobe:EndUserTelNo',jobe:EndUserTelNo)
  Else
    jobe:EndUserTelNo = p_web.GetSessionValue('jobe:EndUserTelNo')
  End
  if p_web.IfExistsValue('jobe:VatNumber')
    jobe:VatNumber = p_web.GetValue('jobe:VatNumber')
    p_web.SetSessionValue('jobe:VatNumber',jobe:VatNumber)
  Else
    jobe:VatNumber = p_web.GetSessionValue('jobe:VatNumber')
  End
  if p_web.IfExistsValue('jobe2:IDNumber')
    jobe2:IDNumber = p_web.GetValue('jobe2:IDNumber')
    p_web.SetSessionValue('jobe2:IDNumber',jobe2:IDNumber)
  Else
    jobe2:IDNumber = p_web.GetSessionValue('jobe2:IDNumber')
  End
  if p_web.IfExistsValue('jobe2:SMSNotification')
    jobe2:SMSNotification = p_web.GetValue('jobe2:SMSNotification')
    p_web.SetSessionValue('jobe2:SMSNotification',jobe2:SMSNotification)
  Else
    jobe2:SMSNotification = p_web.GetSessionValue('jobe2:SMSNotification')
  End
  if p_web.IfExistsValue('jobe2:SMSAlertNumber')
    jobe2:SMSAlertNumber = p_web.GetValue('jobe2:SMSAlertNumber')
    p_web.SetSessionValue('jobe2:SMSAlertNumber',jobe2:SMSAlertNumber)
  Else
    jobe2:SMSAlertNumber = p_web.GetSessionValue('jobe2:SMSAlertNumber')
  End
  if p_web.IfExistsValue('jobe2:EmailNotification')
    jobe2:EmailNotification = p_web.GetValue('jobe2:EmailNotification')
    p_web.SetSessionValue('jobe2:EmailNotification',jobe2:EmailNotification)
  Else
    jobe2:EmailNotification = p_web.GetSessionValue('jobe2:EmailNotification')
  End
  if p_web.IfExistsValue('jobe2:EmailAlertAddress')
    jobe2:EmailAlertAddress = p_web.GetValue('jobe2:EmailAlertAddress')
    p_web.SetSessionValue('jobe2:EmailAlertAddress',jobe2:EmailAlertAddress)
  Else
    jobe2:EmailAlertAddress = p_web.GetSessionValue('jobe2:EmailAlertAddress')
  End
  if p_web.IfExistsValue('jobe2:CourierWaybillNumber')
    jobe2:CourierWaybillNumber = p_web.GetValue('jobe2:CourierWaybillNumber')
    p_web.SetSessionValue('jobe2:CourierWaybillNumber',jobe2:CourierWaybillNumber)
  Else
    jobe2:CourierWaybillNumber = p_web.GetSessionValue('jobe2:CourierWaybillNumber')
  End
  if p_web.IfExistsValue('job:Company_Name_Delivery')
    job:Company_Name_Delivery = p_web.GetValue('job:Company_Name_Delivery')
    p_web.SetSessionValue('job:Company_Name_Delivery',job:Company_Name_Delivery)
  Else
    job:Company_Name_Delivery = p_web.GetSessionValue('job:Company_Name_Delivery')
  End
  if p_web.IfExistsValue('job:Company_Name_Collection')
    job:Company_Name_Collection = p_web.GetValue('job:Company_Name_Collection')
    p_web.SetSessionValue('job:Company_Name_Collection',job:Company_Name_Collection)
  Else
    job:Company_Name_Collection = p_web.GetSessionValue('job:Company_Name_Collection')
  End
  if p_web.IfExistsValue('job:Address_Line1_Delivery')
    job:Address_Line1_Delivery = p_web.GetValue('job:Address_Line1_Delivery')
    p_web.SetSessionValue('job:Address_Line1_Delivery',job:Address_Line1_Delivery)
  Else
    job:Address_Line1_Delivery = p_web.GetSessionValue('job:Address_Line1_Delivery')
  End
  if p_web.IfExistsValue('job:Address_Line1_Collection')
    job:Address_Line1_Collection = p_web.GetValue('job:Address_Line1_Collection')
    p_web.SetSessionValue('job:Address_Line1_Collection',job:Address_Line1_Collection)
  Else
    job:Address_Line1_Collection = p_web.GetSessionValue('job:Address_Line1_Collection')
  End
  if p_web.IfExistsValue('job:Address_Line2_Delivery')
    job:Address_Line2_Delivery = p_web.GetValue('job:Address_Line2_Delivery')
    p_web.SetSessionValue('job:Address_Line2_Delivery',job:Address_Line2_Delivery)
  Else
    job:Address_Line2_Delivery = p_web.GetSessionValue('job:Address_Line2_Delivery')
  End
  if p_web.IfExistsValue('job:Address_Line2_Collection')
    job:Address_Line2_Collection = p_web.GetValue('job:Address_Line2_Collection')
    p_web.SetSessionValue('job:Address_Line2_Collection',job:Address_Line2_Collection)
  Else
    job:Address_Line2_Collection = p_web.GetSessionValue('job:Address_Line2_Collection')
  End
  if p_web.IfExistsValue('job:Address_Line3_Delivery')
    job:Address_Line3_Delivery = p_web.GetValue('job:Address_Line3_Delivery')
    p_web.SetSessionValue('job:Address_Line3_Delivery',job:Address_Line3_Delivery)
  Else
    job:Address_Line3_Delivery = p_web.GetSessionValue('job:Address_Line3_Delivery')
  End
  if p_web.IfExistsValue('job:Address_Line3_Collection')
    job:Address_Line3_Collection = p_web.GetValue('job:Address_Line3_Collection')
    p_web.SetSessionValue('job:Address_Line3_Collection',job:Address_Line3_Collection)
  Else
    job:Address_Line3_Collection = p_web.GetSessionValue('job:Address_Line3_Collection')
  End
  if p_web.IfExistsValue('jobe2:HubDelivery')
    jobe2:HubDelivery = p_web.GetValue('jobe2:HubDelivery')
    p_web.SetSessionValue('jobe2:HubDelivery',jobe2:HubDelivery)
  Else
    jobe2:HubDelivery = p_web.GetSessionValue('jobe2:HubDelivery')
  End
  if p_web.IfExistsValue('jobe2:HubCollection')
    jobe2:HubCollection = p_web.GetValue('jobe2:HubCollection')
    p_web.SetSessionValue('jobe2:HubCollection',jobe2:HubCollection)
  Else
    jobe2:HubCollection = p_web.GetSessionValue('jobe2:HubCollection')
  End
  if p_web.IfExistsValue('job:Postcode_Delivery')
    job:Postcode_Delivery = p_web.GetValue('job:Postcode_Delivery')
    p_web.SetSessionValue('job:Postcode_Delivery',job:Postcode_Delivery)
  Else
    job:Postcode_Delivery = p_web.GetSessionValue('job:Postcode_Delivery')
  End
  if p_web.IfExistsValue('job:Postcode_Collection')
    job:Postcode_Collection = p_web.GetValue('job:Postcode_Collection')
    p_web.SetSessionValue('job:Postcode_Collection',job:Postcode_Collection)
  Else
    job:Postcode_Collection = p_web.GetSessionValue('job:Postcode_Collection')
  End
  if p_web.IfExistsValue('job:Telephone_Delivery')
    job:Telephone_Delivery = p_web.GetValue('job:Telephone_Delivery')
    p_web.SetSessionValue('job:Telephone_Delivery',job:Telephone_Delivery)
  Else
    job:Telephone_Delivery = p_web.GetSessionValue('job:Telephone_Delivery')
  End
  if p_web.IfExistsValue('job:Telephone_Collection')
    job:Telephone_Collection = p_web.GetValue('job:Telephone_Collection')
    p_web.SetSessionValue('job:Telephone_Collection',job:Telephone_Collection)
  Else
    job:Telephone_Collection = p_web.GetSessionValue('job:Telephone_Collection')
  End
  if p_web.IfExistsValue('jbn:Delivery_Text')
    jbn:Delivery_Text = p_web.GetValue('jbn:Delivery_Text')
    p_web.SetSessionValue('jbn:Delivery_Text',jbn:Delivery_Text)
  Else
    jbn:Delivery_Text = p_web.GetSessionValue('jbn:Delivery_Text')
  End
  if p_web.IfExistsValue('jbn:Collection_Text')
    jbn:Collection_Text = p_web.GetValue('jbn:Collection_Text')
    p_web.SetSessionValue('jbn:Collection_Text',jbn:Collection_Text)
  Else
    jbn:Collection_Text = p_web.GetSessionValue('jbn:Collection_Text')
  End
  if p_web.IfExistsValue('jbn:DelContactName')
    jbn:DelContactName = p_web.GetValue('jbn:DelContactName')
    p_web.SetSessionValue('jbn:DelContactName',jbn:DelContactName)
  Else
    jbn:DelContactName = p_web.GetSessionValue('jbn:DelContactName')
  End
  if p_web.IfExistsValue('jbn:ColContatName')
    jbn:ColContatName = p_web.GetValue('jbn:ColContatName')
    p_web.SetSessionValue('jbn:ColContatName',jbn:ColContatName)
  Else
    jbn:ColContatName = p_web.GetSessionValue('jbn:ColContatName')
  End
  if p_web.IfExistsValue('jbn:DelDepartment')
    jbn:DelDepartment = p_web.GetValue('jbn:DelDepartment')
    p_web.SetSessionValue('jbn:DelDepartment',jbn:DelDepartment)
  Else
    jbn:DelDepartment = p_web.GetSessionValue('jbn:DelDepartment')
  End
  if p_web.IfExistsValue('jbn:ColDepartment')
    jbn:ColDepartment = p_web.GetValue('jbn:ColDepartment')
    p_web.SetSessionValue('jbn:ColDepartment',jbn:ColDepartment)
  Else
    jbn:ColDepartment = p_web.GetSessionValue('jbn:ColDepartment')
  End
  if p_web.IfExistsValue('jobe:Sub_Sub_Account')
    jobe:Sub_Sub_Account = p_web.GetValue('jobe:Sub_Sub_Account')
    p_web.SetSessionValue('jobe:Sub_Sub_Account',jobe:Sub_Sub_Account)
  Else
    jobe:Sub_Sub_Account = p_web.GetSessionValue('jobe:Sub_Sub_Account')
  End

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('AmendAddressOLD_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.GSV('FromURL')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('AmendAddressOLD_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('AmendAddressOLD_ChainTo')
    loc:formaction = p_web.GetSessionValue('AmendAddressOLD_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction

GenerateForm   Routine
  do LoadRelatedRecords
  !Prime
      p_web.SSV('Hide:DeliveryAddress',1)
      p_web.SSV('Hide:CollectionAddress',1)
      Access:TRANTYPE.Clearkey(trt:transit_Type_Key)
      trt:transit_Type    = p_web.GSV('job:transit_Type')
      if (Access:TRANTYPE.TryFetch(trt:transit_Type_Key) = Level:Benign)
      ! Found
          if (trt:Delivery_Address = 'YES')
              p_web.SSV('Hide:DeliveryAddress',0)
          end ! if (trt:DeliveryAddress = 'YES')
  
          if (trt:Collection_Address = 'YES')
              p_web.SSV('Hide:CollectionAddress',0)
          end ! if (trt:Collection_Address = 'YES')
  
      else ! if (Access:TRANTYPE.TryFetch(trt:transit_Type_Key) = Level:Benign)
      ! Error
      end ! if (Access:TRANTYPE.TryFetch(trt:transit_Type_Key) = Level:Benign)
  
      p_web.storeValue('FromURL')
  
  
      p_web.SSV('Comment:TelephoneNumber','')
      p_web.SSV('Comment:FaxNumber','')
      p_web.SSV('Comment:SMSAlertNumber','')
      p_web.SSV('Comment:TelephoneNumberDelivery','')
      p_web.SSV('Comment:TelephoneNumberCollection','')
  
      Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
      sub:Account_Number = p_web.GSV('job:Account_Number')
      IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
          IF (sub:UseAlternativeAdd)
              p_web.SSV('Hide:SubSubAccount',0)
          ELSE
              p_web.SSV('Hide:SubSubAccount',1)
          END
          ! Start - Only show VAT Number if the sub account is "overridden" and blank - TrkBs: 5364 (DBH: 26-04-2005)
          If sub:Vat_Number = '' AND sub:OverrideHeadVATNo = True
              p_web.SSV('Hide:VatNumber',0)
          Else ! sub:Vat_Number = '' AND tra:Vat_Number = ''
              p_web.SSV('Hide:VatNumber',1)
          End ! sub:Vat_Number = '' AND tra:Vat_Number = ''
          ! End   - Only show VAT Number if the sub account is "overridden" and blank - TrkBs: 5364 (DBH: 26-04-2005)
  
      END
  
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Amend Addresses') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Amend Addresses',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_AmendAddressOLD',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_AmendAddressOLD0_div')&'">'&p_web.Translate('End User Details')&'</a></li>'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_AmendAddressOLD1_div')&'">'&p_web.Translate('Collection / Delivery Details')&'</a></li>'& CRLF
      If p_web.GSV('Hide:SubSubAccount') <> 1
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_AmendAddressOLD2_div')&'">'&p_web.Translate('Other Details')&'</a></li>'& CRLF
      End
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="AmendAddressOLD_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="AmendAddressOLD_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'AmendAddressOLD_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="AmendAddressOLD_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'AmendAddressOLD_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
    If upper(p_web.getvalue('LookupFile'))='SUBURB'
          If Not (1=0)
            p_web.SetValue('SelectField',clip(loc:formname) & '.job:Telephone_Number')
          End
    End
    If upper(p_web.getvalue('LookupFile'))='SUBURB'
          If Not (p_web.GSV('Hide:CollectionAddress') = 1)
            p_web.SetValue('SelectField',clip(loc:formname) & '.job:Address_Line3_Collection')
          End
    End
    If upper(p_web.getvalue('LookupFile'))='SUBURB'
          If Not (p_web.GSV('Hide:DeliveryAddress') = 1)
            p_web.SetValue('SelectField',clip(loc:formname) & '.job:Telephone_Delivery')
          End
    End
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.job:Company_Name')
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_AmendAddressOLD')>0,p_web.GSV('showtab_AmendAddressOLD'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_AmendAddressOLD'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_AmendAddressOLD') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_AmendAddressOLD'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_AmendAddressOLD')>0,p_web.GSV('showtab_AmendAddressOLD'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_AmendAddressOLD') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('End User Details') & ''''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Collection / Delivery Details') & ''''
      If p_web.GSV('Hide:SubSubAccount') <> 1
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Other Details') & ''''
      End
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_AmendAddressOLD_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_AmendAddressOLD')>0,p_web.GSV('showtab_AmendAddressOLD'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"AmendAddressOLD",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_AmendAddressOLD')>0,p_web.GSV('showtab_AmendAddressOLD'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_AmendAddressOLD_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('AmendAddressOLD') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    p_web.SetValue('_CallPopups',1)
      If p_web.RequestAjax = 0 and p_web.GetPreCall('LookupSuburbs') = 0 then LookupSuburbs(p_web).
    p_web.SetValue('_CallPopups',1)
      If p_web.RequestAjax = 0 and p_web.GetPreCall('ClearAddressDetails') = 0 then ClearAddressDetails(p_web,Net:Web:Popup).
    p_web.SetValue('_CallPopups',0)
    do AutoLookups
    p_web.AddPreCall('AmendAddressOLD')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('End User Details')&'</a></h3>' & CRLF & p_web.DivHeader('tab_AmendAddressOLD0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_AmendAddressOLD0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_AmendAddressOLD0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_AmendAddressOLD0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'End User Details')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_AmendAddressOLD0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('End User Details')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_AmendAddressOLD0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('End User Details')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_AmendAddressOLD0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::temp:Title
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::temp:Title
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Company_Name
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Company_Name
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Address_Line1
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Address_Line1
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Address_Line2
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Address_Line2
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Address_Line3
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Address_Line3
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jobe2:HubCustomer
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jobe2:HubCustomer
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Postcode
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Postcode
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Telephone_Number
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Telephone_Number
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Fax_Number
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Fax_Number
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jobe:EndUserEmailAddress
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jobe:EndUserEmailAddress
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jobe:EndUserTelNo
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jobe:EndUserTelNo
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      If p_web.GSV('Hide:VatNumber') <> 1
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jobe:VatNumber
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jobe:VatNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jobe2:IDNumber
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jobe2:IDNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jobe2:SMSNotification
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jobe2:SMSNotification
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jobe2:SMSAlertNumber
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jobe2:SMSAlertNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jobe2:EmailNotification
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jobe2:EmailNotification
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jobe2:EmailAlertAddress
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jobe2:EmailAlertAddress
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jobe2:CourierWaybillNumber
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jobe2:CourierWaybillNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab1  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Collection / Delivery Details')&'</a></h3>' & CRLF & p_web.DivHeader('tab_AmendAddressOLD1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_AmendAddressOLD1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_AmendAddressOLD1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_AmendAddressOLD1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Collection / Delivery Details')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_AmendAddressOLD1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Collection / Delivery Details')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_AmendAddressOLD1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Collection / Delivery Details')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_AmendAddressOLD1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::DeliveryAddress
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::DeliveryAddress
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::CollectionAddress
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::CollectionAddress
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Company_Name_Delivery
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Company_Name_Delivery
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Company_Name_Collection
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Company_Name_Collection
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Address_Line1_Delivery
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Address_Line1_Delivery
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Address_Line1_Collection
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Address_Line1_Collection
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Address_Line2_Delivery
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Address_Line2_Delivery
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Address_Line2_Collection
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Address_Line2_Collection
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Address_Line3_Delivery
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Address_Line3_Delivery
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Address_Line3_Collection
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Address_Line3_Collection
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jobe2:HubDelivery
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jobe2:HubDelivery
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jobe2:HubCollection
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jobe2:HubCollection
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Postcode_Delivery
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Postcode_Delivery
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Postcode_Collection
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Postcode_Collection
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Telephone_Delivery
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Telephone_Delivery
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Telephone_Collection
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Telephone_Collection
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::Button:CopyEndUserAddress
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::Button:CopyEndUserAddress1
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::Button:CopyEndUserAddress1
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jbn:Delivery_Text
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jbn:Delivery_Text
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jbn:Collection_Text
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jbn:Collection_Text
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jbn:DelContactName
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jbn:DelContactName
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jbn:ColContatName
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jbn:ColContatName
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jbn:DelDepartment
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jbn:DelDepartment
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jbn:ColDepartment
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jbn:ColDepartment
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab2  Routine
  If p_web.GSV('Hide:SubSubAccount') <> 1
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Other Details')&'</a></h3>' & CRLF & p_web.DivHeader('tab_AmendAddressOLD2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_AmendAddressOLD2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_AmendAddressOLD2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_AmendAddressOLD2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Other Details')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_AmendAddressOLD2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Other Details')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_AmendAddressOLD2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Other Details')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_AmendAddressOLD2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jobe:Sub_Sub_Account
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jobe:Sub_Sub_Account
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
  end


Prompt::temp:Title  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('temp:Title') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::temp:Title  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::temp:Title  ! copies value to session value if valid.

ValidateValue::temp:Title  Routine
    If not (1=0)
    End

Value::temp:Title  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('temp:Title') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="temp:Title" class="'&clip('blue bold')&'"'&clip(loc:extra)&'>' & p_web.Translate('End User Address',) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::job:Company_Name  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('job:Company_Name') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Company Name'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Company_Name  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Company_Name = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    job:Company_Name = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::job:Company_Name  ! copies value to session value if valid.
  do Value::job:Company_Name
  do SendAlert

ValidateValue::job:Company_Name  Routine
    If not (1=0)
    job:Company_Name = Upper(job:Company_Name)
      if loc:invalid = '' then p_web.SetSessionValue('job:Company_Name',job:Company_Name).
    End

Value::job:Company_Name  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('job:Company_Name') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    job:Company_Name = p_web.RestoreValue('job:Company_Name')
    do ValidateValue::job:Company_Name
    If job:Company_Name:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- job:Company_Name
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Company_Name'',''amendaddressold_job:company_name_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Company_Name',p_web.GetSessionValueFormat('job:Company_Name'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::job:Address_Line1  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('job:Address_Line1') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Address'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Address_Line1  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Address_Line1 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    job:Address_Line1 = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::job:Address_Line1  ! copies value to session value if valid.
  do Value::job:Address_Line1
  do SendAlert

ValidateValue::job:Address_Line1  Routine
    If not (1=0)
    job:Address_Line1 = Upper(job:Address_Line1)
      if loc:invalid = '' then p_web.SetSessionValue('job:Address_Line1',job:Address_Line1).
    End

Value::job:Address_Line1  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('job:Address_Line1') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    job:Address_Line1 = p_web.RestoreValue('job:Address_Line1')
    do ValidateValue::job:Address_Line1
    If job:Address_Line1:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- job:Address_Line1
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Address_Line1'',''amendaddressold_job:address_line1_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Address_Line1',p_web.GetSessionValueFormat('job:Address_Line1'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::job:Address_Line2  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('job:Address_Line2') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Address_Line2  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Address_Line2 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    job:Address_Line2 = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::job:Address_Line2  ! copies value to session value if valid.
  do Value::job:Address_Line2
  do SendAlert

ValidateValue::job:Address_Line2  Routine
    If not (1=0)
    job:Address_Line2 = Upper(job:Address_Line2)
      if loc:invalid = '' then p_web.SetSessionValue('job:Address_Line2',job:Address_Line2).
    End

Value::job:Address_Line2  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('job:Address_Line2') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    job:Address_Line2 = p_web.RestoreValue('job:Address_Line2')
    do ValidateValue::job:Address_Line2
    If job:Address_Line2:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- job:Address_Line2
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Address_Line2'',''amendaddressold_job:address_line2_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Address_Line2',p_web.GetSessionValueFormat('job:Address_Line2'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::job:Address_Line3  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('job:Address_Line3') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Suburb'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Address_Line3  Routine
  ! GetDescription resets the 'NewValue' to the code, if the description is found. Returns 1 if code valid.
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('Value')
    if p_web.GetBrowseValue(p_web.GetValue('Value'),Net:Web:Record)
      p_web.DeleteValue('value')
    else
      loc:ok = p_web.GetDescription(SUBURB,sur:SuburbKey,sur:SuburbKey,sur:Suburb,sur:Suburb,p_web.GetValue('Value')) !7
    end
  Else
    loc:ok = p_web.GetDescription(SUBURB,sur:SuburbKey,sur:SuburbKey,sur:Suburb,sur:Suburb,p_web.GetSessionValue('job:Address_Line3')) !7
  End
  loc:lookupdone = 1
  If p_web.IfExistsValue('NewValue') then p_web.SetValue('Value',p_web.GetValue('NewValue')).
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Address_Line3 = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    job:Address_Line3 = p_web.GetValue('Value')
  ElsIf p_web.RequestAjax = 1 and p_web.IfExistsValue('sur:Suburb')
    job:Address_Line3 = p_web.GetValue('sur:Suburb')
  ElsIf p_web.RequestAjax = 1
    job:Address_Line3 = sur:Suburb
  End
  do ValidateValue::job:Address_Line3  ! copies value to session value if valid.
      Access:SUBURB.ClearKey(sur:SuburbKey)
      sur:Suburb = p_web.GSV('job:Address_Line3')
      IF (Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign)
          p_web.SSV('job:Postcode',sur:Postcode)
          p_web.SSV('jobe2:HubCustomer',sur:Hub)
      END
  
  p_Web.SetValue('lookupfield','job:Address_Line3')
  do AfterLookup
  do Value::job:Postcode
  do Value::jobe2:HubCustomer
  do Value::job:Address_Line3
  do SendAlert
  do Value::jobe2:HubCustomer  !1
  do Value::job:Postcode  !1

ValidateValue::job:Address_Line3  Routine
    If not (1=0)
    job:Address_Line3 = Upper(job:Address_Line3)
      if loc:invalid = '' then p_web.SetSessionValue('job:Address_Line3',job:Address_Line3).
    End

Value::job:Address_Line3  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('job:Address_Line3') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    job:Address_Line3 = p_web.RestoreValue('job:Address_Line3')
    do ValidateValue::job:Address_Line3
    If job:Address_Line3:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- job:Address_Line3
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Address_Line3'',''amendaddressold_job:address_line3_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Address_Line3')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','job:Address_Line3',p_web.GetSessionValue('job:Address_Line3'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s30',loc:javascript,p_web.PicLength('@s30'),,,) & '<13,10>'!4
    if not loc:viewonly and not loc:readonly
      loc:fieldclass = ''
      packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LookupButton,loc:formname,,,'ntd.push(''LookupSuburbs'','''&p_web._nocolon('job:Address_Line3')&''','''&p_web.translate('Select Suburb')&''',1,'&Net:LookupRecord&',''sur:Suburb'',''AmendAddressOLD'')',,,loc:fieldclass)
    End
  do SendPacket
  End
  p_web.DivFooter()

Prompt::jobe2:HubCustomer  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('jobe2:HubCustomer') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Hub'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jobe2:HubCustomer  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jobe2:HubCustomer = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    jobe2:HubCustomer = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::jobe2:HubCustomer  ! copies value to session value if valid.
  do Value::jobe2:HubCustomer
  do SendAlert

ValidateValue::jobe2:HubCustomer  Routine
    If not (1=0)
    jobe2:HubCustomer = Upper(jobe2:HubCustomer)
      if loc:invalid = '' then p_web.SetSessionValue('jobe2:HubCustomer',jobe2:HubCustomer).
    End

Value::jobe2:HubCustomer  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('jobe2:HubCustomer') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  If loc:retrying
    jobe2:HubCustomer = p_web.RestoreValue('jobe2:HubCustomer')
    do ValidateValue::jobe2:HubCustomer
    If jobe2:HubCustomer:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- jobe2:HubCustomer
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe2:HubCustomer'',''amendaddressold_jobe2:hubcustomer_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jobe2:HubCustomer',p_web.GetSessionValueFormat('jobe2:HubCustomer'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'Hub',,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::job:Postcode  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('job:Postcode') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Postcode'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Postcode  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Postcode = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s10
    job:Postcode = p_web.Dformat(p_web.GetValue('Value'),'@s10')
  End
  do ValidateValue::job:Postcode  ! copies value to session value if valid.
  do Value::job:Postcode
  do SendAlert

ValidateValue::job:Postcode  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('job:Postcode',job:Postcode).
    End

Value::job:Postcode  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('job:Postcode') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  If loc:retrying
    job:Postcode = p_web.RestoreValue('job:Postcode')
    do ValidateValue::job:Postcode
    If job:Postcode:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- job:Postcode
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Postcode'',''amendaddressold_job:postcode_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Postcode',p_web.GetSessionValueFormat('job:Postcode'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s10'),,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::job:Telephone_Number  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('job:Telephone_Number') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Telephone Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Telephone_Number  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Telephone_Number = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s15
    job:Telephone_Number = p_web.Dformat(p_web.GetValue('Value'),'@s15')
  End
  do ValidateValue::job:Telephone_Number  ! copies value to session value if valid.
      If PassMobileNumberFormat(p_web.GSV('job:Telephone_Number')) = 0
          p_web.SSV('job:Telephone_Number','')
          p_web.SSV('Comment:TelephoneNumber','Number Format/Length Incorrect.')
      Else ! If PassMobileNumberFormat(p_web.GSV('job:Mobile_Number')) = 0
          p_web.SSV('Comment:TelephoneNumber','')
      End ! If PassMobileNumberFormat(p_web.GSV('job:Mobile_Number')) = 0
  do Value::job:Telephone_Number
  do SendAlert

ValidateValue::job:Telephone_Number  Routine
    If not (1=0)
    job:Telephone_Number = Upper(job:Telephone_Number)
      if loc:invalid = '' then p_web.SetSessionValue('job:Telephone_Number',job:Telephone_Number).
    End

Value::job:Telephone_Number  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('job:Telephone_Number') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    job:Telephone_Number = p_web.RestoreValue('job:Telephone_Number')
    do ValidateValue::job:Telephone_Number
    If job:Telephone_Number:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- job:Telephone_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Telephone_Number'',''amendaddressold_job:telephone_number_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Telephone_Number')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Telephone_Number',p_web.GetSessionValueFormat('job:Telephone_Number'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s15'),,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::job:Fax_Number  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('job:Fax_Number') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Fax Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Fax_Number  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Fax_Number = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s15
    job:Fax_Number = p_web.Dformat(p_web.GetValue('Value'),'@s15')
  End
  do ValidateValue::job:Fax_Number  ! copies value to session value if valid.
      If PassMobileNumberFormat(p_web.GSV('job:Fax_Number')) = 0
          p_web.SSV('job:Fax_Number','')
          p_web.SSV('Comment:FaxNumber','Number Format/Length Incorrect.')
      Else ! If PassMobileNumberFormat(p_web.GSV('job:Mobile_Number')) = 0
          p_web.SSV('Comment:FaxNumber','')
      End ! If PassMobileNumberFormat(p_web.GSV('job:Mobile_Number')) = 0
  do Value::job:Fax_Number
  do SendAlert

ValidateValue::job:Fax_Number  Routine
    If not (1=0)
    job:Fax_Number = Upper(job:Fax_Number)
      if loc:invalid = '' then p_web.SetSessionValue('job:Fax_Number',job:Fax_Number).
    End

Value::job:Fax_Number  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('job:Fax_Number') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    job:Fax_Number = p_web.RestoreValue('job:Fax_Number')
    do ValidateValue::job:Fax_Number
    If job:Fax_Number:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- job:Fax_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Fax_Number'',''amendaddressold_job:fax_number_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Fax_Number')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Fax_Number',p_web.GetSessionValueFormat('job:Fax_Number'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s15'),,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::jobe:EndUserEmailAddress  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('jobe:EndUserEmailAddress') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Email Address'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jobe:EndUserEmailAddress  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jobe:EndUserEmailAddress = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s255
    jobe:EndUserEmailAddress = p_web.Dformat(p_web.GetValue('Value'),'@s255')
  End
  do ValidateValue::jobe:EndUserEmailAddress  ! copies value to session value if valid.
  do Value::jobe:EndUserEmailAddress
  do SendAlert

ValidateValue::jobe:EndUserEmailAddress  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('jobe:EndUserEmailAddress',jobe:EndUserEmailAddress).
    End

Value::jobe:EndUserEmailAddress  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('jobe:EndUserEmailAddress') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    jobe:EndUserEmailAddress = p_web.RestoreValue('jobe:EndUserEmailAddress')
    do ValidateValue::jobe:EndUserEmailAddress
    If jobe:EndUserEmailAddress:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- jobe:EndUserEmailAddress
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe:EndUserEmailAddress'',''amendaddressold_jobe:enduseremailaddress_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jobe:EndUserEmailAddress',p_web.GetSessionValueFormat('jobe:EndUserEmailAddress'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s255'),'Email Address',,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::jobe:EndUserTelNo  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('jobe:EndUserTelNo') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('End User Tel No'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jobe:EndUserTelNo  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jobe:EndUserTelNo = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s15
    jobe:EndUserTelNo = p_web.Dformat(p_web.GetValue('Value'),'@s15')
  End
  do ValidateValue::jobe:EndUserTelNo  ! copies value to session value if valid.
  do Value::jobe:EndUserTelNo
  do SendAlert

ValidateValue::jobe:EndUserTelNo  Routine
    If not (1=0)
    jobe:EndUserTelNo = Upper(jobe:EndUserTelNo)
      if loc:invalid = '' then p_web.SetSessionValue('jobe:EndUserTelNo',jobe:EndUserTelNo).
    End

Value::jobe:EndUserTelNo  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('jobe:EndUserTelNo') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    jobe:EndUserTelNo = p_web.RestoreValue('jobe:EndUserTelNo')
    do ValidateValue::jobe:EndUserTelNo
    If jobe:EndUserTelNo:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- jobe:EndUserTelNo
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe:EndUserTelNo'',''amendaddressold_jobe:endusertelno_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jobe:EndUserTelNo',p_web.GetSessionValueFormat('jobe:EndUserTelNo'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s15'),,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::jobe:VatNumber  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('jobe:VatNumber') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Vat Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jobe:VatNumber  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jobe:VatNumber = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    jobe:VatNumber = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::jobe:VatNumber  ! copies value to session value if valid.
  do Value::jobe:VatNumber
  do SendAlert

ValidateValue::jobe:VatNumber  Routine
  If p_web.GSV('Hide:VatNumber') <> 1
    If not (1=0)
    jobe:VatNumber = Upper(jobe:VatNumber)
      if loc:invalid = '' then p_web.SetSessionValue('jobe:VatNumber',jobe:VatNumber).
    End
  End

Value::jobe:VatNumber  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('jobe:VatNumber') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    jobe:VatNumber = p_web.RestoreValue('jobe:VatNumber')
    do ValidateValue::jobe:VatNumber
    If jobe:VatNumber:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- jobe:VatNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe:VatNumber'',''amendaddressold_jobe:vatnumber_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jobe:VatNumber',p_web.GetSessionValueFormat('jobe:VatNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'Vat Number',,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::jobe2:IDNumber  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('jobe2:IDNumber') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('ID Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jobe2:IDNumber  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jobe2:IDNumber = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s13
    jobe2:IDNumber = p_web.Dformat(p_web.GetValue('Value'),'@s13')
  End
  do ValidateValue::jobe2:IDNumber  ! copies value to session value if valid.
  do Value::jobe2:IDNumber
  do SendAlert

ValidateValue::jobe2:IDNumber  Routine
    If not (1=0)
    jobe2:IDNumber = Upper(jobe2:IDNumber)
      if loc:invalid = '' then p_web.SetSessionValue('jobe2:IDNumber',jobe2:IDNumber).
    End

Value::jobe2:IDNumber  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('jobe2:IDNumber') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    jobe2:IDNumber = p_web.RestoreValue('jobe2:IDNumber')
    do ValidateValue::jobe2:IDNumber
    If jobe2:IDNumber:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- jobe2:IDNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe2:IDNumber'',''amendaddressold_jobe2:idnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('jobe2:IDNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jobe2:IDNumber',p_web.GetSessionValueFormat('jobe2:IDNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s13'),'I.D. Number',,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::jobe2:SMSNotification  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('jobe2:SMSNotification') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('SMS Notification'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jobe2:SMSNotification  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jobe2:SMSNotification = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    jobe2:SMSNotification = p_web.GetValue('Value')
  End
  do ValidateValue::jobe2:SMSNotification  ! copies value to session value if valid.
  do Value::jobe2:SMSNotification
  do SendAlert
  do Prompt::jobe2:SMSAlertNumber
  do Value::jobe2:SMSAlertNumber  !1

ValidateValue::jobe2:SMSNotification  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('jobe2:SMSNotification',jobe2:SMSNotification).
    End

Value::jobe2:SMSNotification  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('jobe2:SMSNotification') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If loc:retrying
    jobe2:SMSNotification = p_web.RestoreValue('jobe2:SMSNotification')
    do ValidateValue::jobe2:SMSNotification
    If jobe2:SMSNotification:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- CHECKBOX --- jobe2:SMSNotification
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''jobe2:SMSNotification'',''amendaddressold_jobe2:smsnotification_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('jobe2:SMSNotification')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('jobe2:SMSNotification') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','jobe2:SMSNotification',clip(1),,loc:readonly,,,loc:javascript,,'SMS Notification') & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::jobe2:SMSAlertNumber  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('jobe2:SMSAlertNumber') & '_prompt',Choose(p_web.GSV('jobe2:SMSNotification') = 0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('jobe2:SMSNotification') = 0,'',p_web.Translate('Mobile No'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jobe2:SMSAlertNumber  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jobe2:SMSAlertNumber = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    jobe2:SMSAlertNumber = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::jobe2:SMSAlertNumber  ! copies value to session value if valid.
      If PassMobileNumberFormat(p_web.GSV('jobe2:SMSAlertNumber')) = 0
          p_web.SSV('jobe2:SMSAlertNumber','')
          p_web.SSV('Comment:SMSAlertNumber','Number Format/Length Incorrect.')
      Else ! If PassMobileNumberFormat(p_web.GSV('job:Mobile_Number')) = 0
          p_web.SSV('Comment:SMSAlertNumber','')
      End ! If PassMobileNumberFormat(p_web.GSV('job:Mobile_Number')) = 0
  do Value::jobe2:SMSAlertNumber
  do SendAlert

ValidateValue::jobe2:SMSAlertNumber  Routine
    If not (p_web.GSV('jobe2:SMSNotification') = 0)
    jobe2:SMSAlertNumber = Upper(jobe2:SMSAlertNumber)
      if loc:invalid = '' then p_web.SetSessionValue('jobe2:SMSAlertNumber',jobe2:SMSAlertNumber).
    End

Value::jobe2:SMSAlertNumber  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('jobe2:SMSNotification') = 0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('jobe2:SMSAlertNumber') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    jobe2:SMSAlertNumber = p_web.RestoreValue('jobe2:SMSAlertNumber')
    do ValidateValue::jobe2:SMSAlertNumber
    If jobe2:SMSAlertNumber:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('jobe2:SMSNotification') = 0)
  ! --- STRING --- jobe2:SMSAlertNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe2:SMSAlertNumber'',''amendaddressold_jobe2:smsalertnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('jobe2:SMSAlertNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jobe2:SMSAlertNumber',p_web.GetSessionValueFormat('jobe2:SMSAlertNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'SMS Alert Mobile Number',,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::jobe2:EmailNotification  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('jobe2:EmailNotification') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Email Notification'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jobe2:EmailNotification  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jobe2:EmailNotification = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    jobe2:EmailNotification = p_web.GetValue('Value')
  End
  do ValidateValue::jobe2:EmailNotification  ! copies value to session value if valid.
  do Value::jobe2:EmailNotification
  do SendAlert
  do Prompt::jobe2:EmailAlertAddress
  do Value::jobe2:EmailAlertAddress  !1

ValidateValue::jobe2:EmailNotification  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('jobe2:EmailNotification',jobe2:EmailNotification).
    End

Value::jobe2:EmailNotification  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('jobe2:EmailNotification') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If loc:retrying
    jobe2:EmailNotification = p_web.RestoreValue('jobe2:EmailNotification')
    do ValidateValue::jobe2:EmailNotification
    If jobe2:EmailNotification:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- CHECKBOX --- jobe2:EmailNotification
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''jobe2:EmailNotification'',''amendaddressold_jobe2:emailnotification_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('jobe2:EmailNotification')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('jobe2:EmailNotification') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','jobe2:EmailNotification',clip(1),,loc:readonly,,,loc:javascript,,'Email Notification') & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::jobe2:EmailAlertAddress  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('jobe2:EmailAlertAddress') & '_prompt',Choose(p_web.GSV('jobe2:EmailNotification') = 0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('jobe2:EmailNotification') = 0,'',p_web.Translate('Email Address'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jobe2:EmailAlertAddress  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jobe2:EmailAlertAddress = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s255
    jobe2:EmailAlertAddress = p_web.Dformat(p_web.GetValue('Value'),'@s255')
  End
  do ValidateValue::jobe2:EmailAlertAddress  ! copies value to session value if valid.
  do Value::jobe2:EmailAlertAddress
  do SendAlert

ValidateValue::jobe2:EmailAlertAddress  Routine
    If not (p_web.GSV('jobe2:EmailNotification') = 0)
      if loc:invalid = '' then p_web.SetSessionValue('jobe2:EmailAlertAddress',jobe2:EmailAlertAddress).
    End

Value::jobe2:EmailAlertAddress  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('jobe2:EmailNotification') = 0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('jobe2:EmailAlertAddress') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    jobe2:EmailAlertAddress = p_web.RestoreValue('jobe2:EmailAlertAddress')
    do ValidateValue::jobe2:EmailAlertAddress
    If jobe2:EmailAlertAddress:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('jobe2:EmailNotification') = 0)
  ! --- STRING --- jobe2:EmailAlertAddress
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe2:EmailAlertAddress'',''amendaddressold_jobe2:emailalertaddress_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jobe2:EmailAlertAddress',p_web.GetSessionValueFormat('jobe2:EmailAlertAddress'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s255'),'Email Alert Address',,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::jobe2:CourierWaybillNumber  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('jobe2:CourierWaybillNumber') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Courier Waybill Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jobe2:CourierWaybillNumber  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jobe2:CourierWaybillNumber = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    jobe2:CourierWaybillNumber = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::jobe2:CourierWaybillNumber  ! copies value to session value if valid.
  do Value::jobe2:CourierWaybillNumber
  do SendAlert

ValidateValue::jobe2:CourierWaybillNumber  Routine
    If not (1=0)
    jobe2:CourierWaybillNumber = Upper(jobe2:CourierWaybillNumber)
      if loc:invalid = '' then p_web.SetSessionValue('jobe2:CourierWaybillNumber',jobe2:CourierWaybillNumber).
    End

Value::jobe2:CourierWaybillNumber  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('jobe2:CourierWaybillNumber') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    jobe2:CourierWaybillNumber = p_web.RestoreValue('jobe2:CourierWaybillNumber')
    do ValidateValue::jobe2:CourierWaybillNumber
    If jobe2:CourierWaybillNumber:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- jobe2:CourierWaybillNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe2:CourierWaybillNumber'',''amendaddressold_jobe2:courierwaybillnumber_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jobe2:CourierWaybillNumber',p_web.GetSessionValueFormat('jobe2:CourierWaybillNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'Courier Waybill Number',,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::DeliveryAddress  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('DeliveryAddress') & '_prompt',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::DeliveryAddress  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::DeliveryAddress  ! copies value to session value if valid.

ValidateValue::DeliveryAddress  Routine
    If not (p_web.GSV('Hide:DeliveryAddress') = 1)
    End

Value::DeliveryAddress  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:DeliveryAddress') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('DeliveryAddress') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('Hide:DeliveryAddress') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="DeliveryAddress" class="'&clip('blue bold')&'"'&clip(loc:extra)&'>' & p_web.Translate('Delivery Address',) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::CollectionAddress  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('CollectionAddress') & '_prompt',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:CollectionAddress') = 1,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::CollectionAddress  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::CollectionAddress  ! copies value to session value if valid.

ValidateValue::CollectionAddress  Routine
    If not (p_web.GSV('Hide:CollectionAddress') = 1)
    End

Value::CollectionAddress  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:CollectionAddress') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('CollectionAddress') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('Hide:CollectionAddress') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="CollectionAddress" class="'&clip('blue bold')&'"'&clip(loc:extra)&'>' & p_web.Translate('Collection Address',) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::job:Company_Name_Delivery  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('job:Company_Name_Delivery') & '_prompt',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'',p_web.Translate('Company Name'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Company_Name_Delivery  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Company_Name_Delivery = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    job:Company_Name_Delivery = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::job:Company_Name_Delivery  ! copies value to session value if valid.
  do Value::job:Company_Name_Delivery
  do SendAlert

ValidateValue::job:Company_Name_Delivery  Routine
    If not (p_web.GSV('Hide:DeliveryAddress') = 1)
    job:Company_Name_Delivery = Upper(job:Company_Name_Delivery)
      if loc:invalid = '' then p_web.SetSessionValue('job:Company_Name_Delivery',job:Company_Name_Delivery).
    End

Value::job:Company_Name_Delivery  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:DeliveryAddress') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('job:Company_Name_Delivery') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.GetSessionValue('ReadOnly:DeliveryAddress') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    job:Company_Name_Delivery = p_web.RestoreValue('job:Company_Name_Delivery')
    do ValidateValue::job:Company_Name_Delivery
    If job:Company_Name_Delivery:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:DeliveryAddress') = 1)
  ! --- STRING --- job:Company_Name_Delivery
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GetSessionValue('ReadOnly:DeliveryAddress') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Company_Name_Delivery'',''amendaddressold_job:company_name_delivery_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Company_Name_Delivery',p_web.GetSessionValueFormat('job:Company_Name_Delivery'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::job:Company_Name_Collection  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('job:Company_Name_Collection') & '_prompt',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:CollectionAddress') = 1,'',p_web.Translate('Company Name'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Company_Name_Collection  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Company_Name_Collection = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    job:Company_Name_Collection = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::job:Company_Name_Collection  ! copies value to session value if valid.
  do Value::job:Company_Name_Collection
  do SendAlert

ValidateValue::job:Company_Name_Collection  Routine
    If not (p_web.GSV('Hide:CollectionAddress') = 1)
      if loc:invalid = '' then p_web.SetSessionValue('job:Company_Name_Collection',job:Company_Name_Collection).
    End

Value::job:Company_Name_Collection  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:CollectionAddress') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('job:Company_Name_Collection') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If p_web.GetSessionValue('ReadOnly:CollectionAddress') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    job:Company_Name_Collection = p_web.RestoreValue('job:Company_Name_Collection')
    do ValidateValue::job:Company_Name_Collection
    If job:Company_Name_Collection:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:CollectionAddress') = 1)
  ! --- STRING --- job:Company_Name_Collection
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GetSessionValue('ReadOnly:CollectionAddress') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Company_Name_Collection'',''amendaddressold_job:company_name_collection_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Company_Name_Collection',p_web.GetSessionValueFormat('job:Company_Name_Collection'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::job:Address_Line1_Delivery  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('job:Address_Line1_Delivery') & '_prompt',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'',p_web.Translate('Address'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Address_Line1_Delivery  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Address_Line1_Delivery = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    job:Address_Line1_Delivery = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::job:Address_Line1_Delivery  ! copies value to session value if valid.
  do Value::job:Address_Line1_Delivery
  do SendAlert

ValidateValue::job:Address_Line1_Delivery  Routine
    If not (p_web.GSV('Hide:DeliveryAddress') = 1)
    job:Address_Line1_Delivery = Upper(job:Address_Line1_Delivery)
      if loc:invalid = '' then p_web.SetSessionValue('job:Address_Line1_Delivery',job:Address_Line1_Delivery).
    End

Value::job:Address_Line1_Delivery  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:DeliveryAddress') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('job:Address_Line1_Delivery') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.GetSessionValue('ReadOnly:DeliveryAddress') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    job:Address_Line1_Delivery = p_web.RestoreValue('job:Address_Line1_Delivery')
    do ValidateValue::job:Address_Line1_Delivery
    If job:Address_Line1_Delivery:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:DeliveryAddress') = 1)
  ! --- STRING --- job:Address_Line1_Delivery
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GetSessionValue('ReadOnly:DeliveryAddress') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Address_Line1_Delivery'',''amendaddressold_job:address_line1_delivery_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Address_Line1_Delivery',p_web.GetSessionValueFormat('job:Address_Line1_Delivery'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::job:Address_Line1_Collection  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('job:Address_Line1_Collection') & '_prompt',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:CollectionAddress') = 1,'',p_web.Translate('Address'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Address_Line1_Collection  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Address_Line1_Collection = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    job:Address_Line1_Collection = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::job:Address_Line1_Collection  ! copies value to session value if valid.
  do Value::job:Address_Line1_Collection
  do SendAlert

ValidateValue::job:Address_Line1_Collection  Routine
    If not (p_web.GSV('Hide:CollectionAddress') = 1)
    job:Address_Line1_Collection = Upper(job:Address_Line1_Collection)
      if loc:invalid = '' then p_web.SetSessionValue('job:Address_Line1_Collection',job:Address_Line1_Collection).
    End

Value::job:Address_Line1_Collection  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:CollectionAddress') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('job:Address_Line1_Collection') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.GetSessionValue('ReadOnly:CollectionAddress') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    job:Address_Line1_Collection = p_web.RestoreValue('job:Address_Line1_Collection')
    do ValidateValue::job:Address_Line1_Collection
    If job:Address_Line1_Collection:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:CollectionAddress') = 1)
  ! --- STRING --- job:Address_Line1_Collection
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GetSessionValue('ReadOnly:CollectionAddress') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Address_Line1_Collection'',''amendaddressold_job:address_line1_collection_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Address_Line1_Collection',p_web.GetSessionValueFormat('job:Address_Line1_Collection'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::job:Address_Line2_Delivery  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('job:Address_Line2_Delivery') & '_prompt',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Address_Line2_Delivery  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Address_Line2_Delivery = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    job:Address_Line2_Delivery = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::job:Address_Line2_Delivery  ! copies value to session value if valid.
  do Value::job:Address_Line2_Delivery
  do SendAlert

ValidateValue::job:Address_Line2_Delivery  Routine
    If not (p_web.GSV('Hide:DeliveryAddress') = 1)
    job:Address_Line2_Delivery = Upper(job:Address_Line2_Delivery)
      if loc:invalid = '' then p_web.SetSessionValue('job:Address_Line2_Delivery',job:Address_Line2_Delivery).
    End

Value::job:Address_Line2_Delivery  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:DeliveryAddress') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('job:Address_Line2_Delivery') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.GetSessionValue('ReadOnly:DeliveryAddress') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    job:Address_Line2_Delivery = p_web.RestoreValue('job:Address_Line2_Delivery')
    do ValidateValue::job:Address_Line2_Delivery
    If job:Address_Line2_Delivery:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:DeliveryAddress') = 1)
  ! --- STRING --- job:Address_Line2_Delivery
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GetSessionValue('ReadOnly:DeliveryAddress') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Address_Line2_Delivery'',''amendaddressold_job:address_line2_delivery_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Address_Line2_Delivery',p_web.GetSessionValueFormat('job:Address_Line2_Delivery'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::job:Address_Line2_Collection  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('job:Address_Line2_Collection') & '_prompt',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:CollectionAddress') = 1,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Address_Line2_Collection  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Address_Line2_Collection = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    job:Address_Line2_Collection = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::job:Address_Line2_Collection  ! copies value to session value if valid.
  do Value::job:Address_Line2_Collection
  do SendAlert

ValidateValue::job:Address_Line2_Collection  Routine
    If not (p_web.GSV('Hide:CollectionAddress') = 1)
    job:Address_Line2_Collection = Upper(job:Address_Line2_Collection)
      if loc:invalid = '' then p_web.SetSessionValue('job:Address_Line2_Collection',job:Address_Line2_Collection).
    End

Value::job:Address_Line2_Collection  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:CollectionAddress') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('job:Address_Line2_Collection') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.GetSessionValue('ReadOnly:CollectionAddress') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    job:Address_Line2_Collection = p_web.RestoreValue('job:Address_Line2_Collection')
    do ValidateValue::job:Address_Line2_Collection
    If job:Address_Line2_Collection:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:CollectionAddress') = 1)
  ! --- STRING --- job:Address_Line2_Collection
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GetSessionValue('ReadOnly:CollectionAddress') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Address_Line2_Collection'',''amendaddressold_job:address_line2_collection_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Address_Line2_Collection',p_web.GetSessionValueFormat('job:Address_Line2_Collection'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::job:Address_Line3_Delivery  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('job:Address_Line3_Delivery') & '_prompt',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'',p_web.Translate('Suburb'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Address_Line3_Delivery  Routine
  ! GetDescription resets the 'NewValue' to the code, if the description is found. Returns 1 if code valid.
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('Value')
    if p_web.GetBrowseValue(p_web.GetValue('Value'),Net:Web:Record)
      p_web.DeleteValue('value')
    else
      loc:ok = p_web.GetDescription(SUBURB,sur:SuburbKey,sur:SuburbKey,sur:Suburb,sur:Suburb,p_web.GetValue('Value')) !7
    end
  Else
    loc:ok = p_web.GetDescription(SUBURB,sur:SuburbKey,sur:SuburbKey,sur:Suburb,sur:Suburb,p_web.GetSessionValue('job:Address_Line3_Delivery')) !7
  End
  loc:lookupdone = 1
  If p_web.IfExistsValue('NewValue') then p_web.SetValue('Value',p_web.GetValue('NewValue')).
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Address_Line3_Delivery = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    job:Address_Line3_Delivery = p_web.GetValue('Value')
  ElsIf p_web.RequestAjax = 1 and p_web.IfExistsValue('sur:Suburb')
    job:Address_Line3_Delivery = p_web.GetValue('sur:Suburb')
  ElsIf p_web.RequestAjax = 1
    job:Address_Line3_Delivery = sur:Suburb
  End
  do ValidateValue::job:Address_Line3_Delivery  ! copies value to session value if valid.
      Access:SUBURB.ClearKey(sur:SuburbKey)
      sur:Suburb = p_web.GSV('job:Address_Line3_Delivery')
      IF (Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign)
          p_web.SSV('job:Postcode_Delivery',sur:postcode)
          p_web.SSV('jobe2:HubDelivery',sur:hub)
      END
  p_Web.SetValue('lookupfield','job:Address_Line3_Delivery')
  do AfterLookup
  do Value::job:Postcode_Delivery
  do Value::jobe2:HubDelivery
  do Value::job:Address_Line3_Delivery
  do SendAlert
  do Value::jobe2:HubDelivery  !1

ValidateValue::job:Address_Line3_Delivery  Routine
    If not (p_web.GSV('Hide:DeliveryAddress') = 1)
    job:Address_Line3_Delivery = Upper(job:Address_Line3_Delivery)
      if loc:invalid = '' then p_web.SetSessionValue('job:Address_Line3_Delivery',job:Address_Line3_Delivery).
    End

Value::job:Address_Line3_Delivery  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:DeliveryAddress') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('job:Address_Line3_Delivery') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.GetSessionValue('ReadOnly:DeliveryAddress') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    job:Address_Line3_Delivery = p_web.RestoreValue('job:Address_Line3_Delivery')
    do ValidateValue::job:Address_Line3_Delivery
    If job:Address_Line3_Delivery:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:DeliveryAddress') = 1)
  ! --- STRING --- job:Address_Line3_Delivery
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GetSessionValue('ReadOnly:DeliveryAddress') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Address_Line3_Delivery'',''amendaddressold_job:address_line3_delivery_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Address_Line3_Delivery')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','job:Address_Line3_Delivery',p_web.GetSessionValue('job:Address_Line3_Delivery'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s30',loc:javascript,p_web.PicLength('@s30'),,,) & '<13,10>'!4
    if not loc:viewonly and not loc:readonly
      loc:fieldclass = ''
      packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LookupButton,loc:formname,,,'ntd.push(''LookupSuburbs'','''&p_web._nocolon('job:Address_Line3_Delivery')&''','''&p_web.translate('Select Suburb')&''',1,'&Net:LookupRecord&',''sur:Suburb'',''AmendAddressOLD'')',,,loc:fieldclass)
    End
  do SendPacket
  End
  p_web.DivFooter()

Prompt::job:Address_Line3_Collection  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('job:Address_Line3_Collection') & '_prompt',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:CollectionAddress') = 1,'',p_web.Translate('Suburb'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Address_Line3_Collection  Routine
  ! GetDescription resets the 'NewValue' to the code, if the description is found. Returns 1 if code valid.
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('Value')
    if p_web.GetBrowseValue(p_web.GetValue('Value'),Net:Web:Record)
      p_web.DeleteValue('value')
    else
      loc:ok = p_web.GetDescription(SUBURB,sur:SuburbKey,sur:SuburbKey,sur:Suburb,sur:Suburb,p_web.GetValue('Value')) !7
    end
  Else
    loc:ok = p_web.GetDescription(SUBURB,sur:SuburbKey,sur:SuburbKey,sur:Suburb,sur:Suburb,p_web.GetSessionValue('job:Address_Line3_Collection')) !7
  End
  loc:lookupdone = 1
  If p_web.IfExistsValue('NewValue') then p_web.SetValue('Value',p_web.GetValue('NewValue')).
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Address_Line3_Collection = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    job:Address_Line3_Collection = p_web.GetValue('Value')
  ElsIf p_web.RequestAjax = 1 and p_web.IfExistsValue('sur:Suburb')
    job:Address_Line3_Collection = p_web.GetValue('sur:Suburb')
  ElsIf p_web.RequestAjax = 1
    job:Address_Line3_Collection = sur:Suburb
  End
  do ValidateValue::job:Address_Line3_Collection  ! copies value to session value if valid.
      Access:SUBURB.ClearKey(sur:SuburbKey)
      sur:Suburb = p_web.GSV('job:Address_Line3_Collection')
      IF (Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign)
          p_web.SSV('job:Postcode_Collection',sur:postcode)
          p_web.SSV('jobe2:HubCollection',sur:hub)
      END
  p_Web.SetValue('lookupfield','job:Address_Line3_Collection')
  do AfterLookup
  do Value::job:Postcode_Collection
  do Value::jobe2:HubCollection
  do Value::job:Address_Line3_Collection
  do SendAlert
  do Value::jobe2:HubCollection  !1

ValidateValue::job:Address_Line3_Collection  Routine
    If not (p_web.GSV('Hide:CollectionAddress') = 1)
    job:Address_Line3_Collection = Upper(job:Address_Line3_Collection)
      if loc:invalid = '' then p_web.SetSessionValue('job:Address_Line3_Collection',job:Address_Line3_Collection).
    End

Value::job:Address_Line3_Collection  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:CollectionAddress') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('job:Address_Line3_Collection') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.GetSessionValue('ReadOnly:CollectionAddress') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    job:Address_Line3_Collection = p_web.RestoreValue('job:Address_Line3_Collection')
    do ValidateValue::job:Address_Line3_Collection
    If job:Address_Line3_Collection:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:CollectionAddress') = 1)
  ! --- STRING --- job:Address_Line3_Collection
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GetSessionValue('ReadOnly:CollectionAddress') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Address_Line3_Collection'',''amendaddressold_job:address_line3_collection_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Address_Line3_Collection')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','job:Address_Line3_Collection',p_web.GetSessionValue('job:Address_Line3_Collection'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s30',loc:javascript,p_web.PicLength('@s30'),,,) & '<13,10>'!4
    if not loc:viewonly and not loc:readonly
      loc:fieldclass = ''
      packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LookupButton,loc:formname,,,'ntd.push(''LookupSuburbs'','''&p_web._nocolon('job:Address_Line3_Collection')&''','''&p_web.translate('Select Suburb')&''',1,'&Net:LookupRecord&',''sur:Suburb'',''AmendAddressOLD'')',,,loc:fieldclass)
    End
  do SendPacket
  End
  p_web.DivFooter()

Prompt::jobe2:HubDelivery  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('jobe2:HubDelivery') & '_prompt',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'',p_web.Translate('Hub'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jobe2:HubDelivery  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jobe2:HubDelivery = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    jobe2:HubDelivery = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::jobe2:HubDelivery  ! copies value to session value if valid.
  do Value::jobe2:HubDelivery
  do SendAlert
  do Value::job:Postcode_Delivery  !1

ValidateValue::jobe2:HubDelivery  Routine
    If not (p_web.GSV('Hide:DeliveryAddress') = 1)
    jobe2:HubDelivery = Upper(jobe2:HubDelivery)
      if loc:invalid = '' then p_web.SetSessionValue('jobe2:HubDelivery',jobe2:HubDelivery).
    End

Value::jobe2:HubDelivery  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:DeliveryAddress') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('jobe2:HubDelivery') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  If loc:retrying
    jobe2:HubDelivery = p_web.RestoreValue('jobe2:HubDelivery')
    do ValidateValue::jobe2:HubDelivery
    If jobe2:HubDelivery:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:DeliveryAddress') = 1)
  ! --- STRING --- jobe2:HubDelivery
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe2:HubDelivery'',''amendaddressold_jobe2:hubdelivery_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('jobe2:HubDelivery')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jobe2:HubDelivery',p_web.GetSessionValueFormat('jobe2:HubDelivery'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'Hub',,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::jobe2:HubCollection  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('jobe2:HubCollection') & '_prompt',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:CollectionAddress') = 1,'',p_web.Translate('Hub'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jobe2:HubCollection  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jobe2:HubCollection = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    jobe2:HubCollection = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::jobe2:HubCollection  ! copies value to session value if valid.
  do Value::jobe2:HubCollection
  do SendAlert
  do Value::job:Postcode_Collection  !1

ValidateValue::jobe2:HubCollection  Routine
    If not (p_web.GSV('Hide:CollectionAddress') = 1)
    jobe2:HubCollection = Upper(jobe2:HubCollection)
      if loc:invalid = '' then p_web.SetSessionValue('jobe2:HubCollection',jobe2:HubCollection).
    End

Value::jobe2:HubCollection  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:CollectionAddress') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('jobe2:HubCollection') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  If loc:retrying
    jobe2:HubCollection = p_web.RestoreValue('jobe2:HubCollection')
    do ValidateValue::jobe2:HubCollection
    If jobe2:HubCollection:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:CollectionAddress') = 1)
  ! --- STRING --- jobe2:HubCollection
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe2:HubCollection'',''amendaddressold_jobe2:hubcollection_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('jobe2:HubCollection')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jobe2:HubCollection',p_web.GetSessionValueFormat('jobe2:HubCollection'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'Hub',,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::job:Postcode_Delivery  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('job:Postcode_Delivery') & '_prompt',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'',p_web.Translate('Postcode'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Postcode_Delivery  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Postcode_Delivery = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s10
    job:Postcode_Delivery = p_web.Dformat(p_web.GetValue('Value'),'@s10')
  End
  do ValidateValue::job:Postcode_Delivery  ! copies value to session value if valid.
  do Value::job:Postcode_Delivery
  do SendAlert

ValidateValue::job:Postcode_Delivery  Routine
    If not (p_web.GSV('Hide:DeliveryAddress') = 1)
      if loc:invalid = '' then p_web.SetSessionValue('job:Postcode_Delivery',job:Postcode_Delivery).
    End

Value::job:Postcode_Delivery  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:DeliveryAddress') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('job:Postcode_Delivery') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  If loc:retrying
    job:Postcode_Delivery = p_web.RestoreValue('job:Postcode_Delivery')
    do ValidateValue::job:Postcode_Delivery
    If job:Postcode_Delivery:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:DeliveryAddress') = 1)
  ! --- STRING --- job:Postcode_Delivery
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Postcode_Delivery'',''amendaddressold_job:postcode_delivery_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Postcode_Delivery',p_web.GetSessionValueFormat('job:Postcode_Delivery'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s10'),,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::job:Postcode_Collection  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('job:Postcode_Collection') & '_prompt',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:CollectionAddress') = 1,'',p_web.Translate('Postcode'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Postcode_Collection  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Postcode_Collection = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s10
    job:Postcode_Collection = p_web.Dformat(p_web.GetValue('Value'),'@s10')
  End
  do ValidateValue::job:Postcode_Collection  ! copies value to session value if valid.
  do Value::job:Postcode_Collection
  do SendAlert

ValidateValue::job:Postcode_Collection  Routine
    If not (p_web.GSV('Hide:CollectionAddress') = 1)
      if loc:invalid = '' then p_web.SetSessionValue('job:Postcode_Collection',job:Postcode_Collection).
    End

Value::job:Postcode_Collection  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:CollectionAddress') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('job:Postcode_Collection') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  If loc:retrying
    job:Postcode_Collection = p_web.RestoreValue('job:Postcode_Collection')
    do ValidateValue::job:Postcode_Collection
    If job:Postcode_Collection:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:CollectionAddress') = 1)
  ! --- STRING --- job:Postcode_Collection
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Postcode_Collection'',''amendaddressold_job:postcode_collection_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Postcode_Collection',p_web.GetSessionValueFormat('job:Postcode_Collection'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s10'),,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::job:Telephone_Delivery  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('job:Telephone_Delivery') & '_prompt',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'',p_web.Translate('Telephone Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Telephone_Delivery  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Telephone_Delivery = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s15
    job:Telephone_Delivery = p_web.Dformat(p_web.GetValue('Value'),'@s15')
  End
  do ValidateValue::job:Telephone_Delivery  ! copies value to session value if valid.
      If PassMobileNumberFormat(p_web.GSV('job:Telephone_Delivery')) = 0
          p_web.SSV('job:Telephone_Delivery','')
          p_web.SSV('Comment:TelephoneNumberDelivery','Number Format/Length Incorrect.')
      Else ! If PassMobileNumberFormat(p_web.GSV('job:Mobile_Number')) = 0
          p_web.SSV('Comment:TelephoneNumberDelivery','')
      End ! If PassMobileNumberFormat(p_web.GSV('job:Mobile_Number')) = 0
  do Value::job:Telephone_Delivery
  do SendAlert

ValidateValue::job:Telephone_Delivery  Routine
    If not (p_web.GSV('Hide:DeliveryAddress') = 1)
    job:Telephone_Delivery = Upper(job:Telephone_Delivery)
      if loc:invalid = '' then p_web.SetSessionValue('job:Telephone_Delivery',job:Telephone_Delivery).
    End

Value::job:Telephone_Delivery  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:DeliveryAddress') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('job:Telephone_Delivery') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.GetSessionValue('ReadOnly:DeliveryAddress') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    job:Telephone_Delivery = p_web.RestoreValue('job:Telephone_Delivery')
    do ValidateValue::job:Telephone_Delivery
    If job:Telephone_Delivery:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:DeliveryAddress') = 1)
  ! --- STRING --- job:Telephone_Delivery
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GetSessionValue('ReadOnly:DeliveryAddress') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Telephone_Delivery'',''amendaddressold_job:telephone_delivery_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Telephone_Delivery')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Telephone_Delivery',p_web.GetSessionValueFormat('job:Telephone_Delivery'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s15'),,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::job:Telephone_Collection  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('job:Telephone_Collection') & '_prompt',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:CollectionAddress') = 1,'',p_web.Translate('Telephone Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Telephone_Collection  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Telephone_Collection = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s15
    job:Telephone_Collection = p_web.Dformat(p_web.GetValue('Value'),'@s15')
  End
  do ValidateValue::job:Telephone_Collection  ! copies value to session value if valid.
      If PassMobileNumberFormat(p_web.GSV('job:Telephone_Collection')) = 0
          p_web.SSV('job:Telephone_Collection','')
          p_web.SSV('Comment:TelephoneNumberCollection','Number Format/Length Incorrect.')
      Else ! If PassMobileNumberFormat(p_web.GSV('job:Mobile_Number')) = 0
          p_web.SSV('Comment:TelephoneNumberCollection','')
      End ! If PassMobileNumberFormat(p_web.GSV('job:Mobile_Number')) = 0
  do Value::job:Telephone_Collection
  do SendAlert

ValidateValue::job:Telephone_Collection  Routine
    If not (p_web.GSV('Hide:CollectionAddress') = 1)
    job:Telephone_Collection = Upper(job:Telephone_Collection)
      if loc:invalid = '' then p_web.SetSessionValue('job:Telephone_Collection',job:Telephone_Collection).
    End

Value::job:Telephone_Collection  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:CollectionAddress') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('job:Telephone_Collection') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.GetSessionValue('ReadOnly:CollectionAddress') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    job:Telephone_Collection = p_web.RestoreValue('job:Telephone_Collection')
    do ValidateValue::job:Telephone_Collection
    If job:Telephone_Collection:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:CollectionAddress') = 1)
  ! --- STRING --- job:Telephone_Collection
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GetSessionValue('ReadOnly:CollectionAddress') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Telephone_Collection'',''amendaddressold_job:telephone_collection_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Telephone_Collection')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Telephone_Collection',p_web.GetSessionValueFormat('job:Telephone_Collection'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s15'),,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Validate::Button:CopyEndUserAddress  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::Button:CopyEndUserAddress  ! copies value to session value if valid.
  ! Copy End User
  !STOP('er')
  !p_web.SSV('job:Company_Name_Delivery',p_web.GSV('job:Company_Name'))
  !p_web.SSV('job:Address_Line1_Delivery',p_web.GSV('job:Address_Line1'))
  !p_web.SSV('job:Address_Line2_Delivery',p_web.GSV('job:Address_Line2'))
  !p_web.SSV('job:Address_Line3_Delivery',p_web.GSV('job:Address_Line3'))
  !p_web.SSV('job:Postcode_Delivery',p_web.GSV('job:Postcode'))
  !p_web.SSV('jobe2:HubDelivery',p_web.GSV('jobe2:HubCustomer'))
  !p_web.SSV('job:Telephone_Delivery',p_web.GSV('job:Telephone_Number'))
  do Value::Button:CopyEndUserAddress
  do Value::job:Address_Line1_Delivery  !1
  do Value::job:Address_Line2_Delivery  !1
  do Value::job:Address_Line3_Delivery  !1
  do Value::job:Company_Name_Delivery  !1
  do Value::job:Telephone_Delivery  !1
  do Value::jobe2:HubDelivery  !1
  do Value::job:Postcode_Delivery  !1

ValidateValue::Button:CopyEndUserAddress  Routine
    If not (p_web.GetSessionValue('Hide:DeliveryAddress') = 1)
    End

Value::Button:CopyEndUserAddress  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GetSessionValue('Hide:DeliveryAddress') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('Button:CopyEndUserAddress') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GetSessionValue('Hide:DeliveryAddress') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon(''&clip('CopyEndUserAddress')&'.disabled=true;sv(''Button:CopyEndUserAddress'',''amendaddressold_button:copyenduseraddress_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','CopyEndUserAddress','Copy / Clear Address',p_web.combine(Choose('Copy / Clear Address' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'SmallButtonFixed'),loc:formname,,,p_web.OpenDialog('ClearAddressDetails',p_web._jsok('Copy/Clear Address'),0,'''' & lower('AmendAddressOLD') & '''','','',p_web._jsok('Button:CopyEndUserAddress'),p_web._jsok('AddType=D')),,loc:disabled,,,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()

Prompt::Button:CopyEndUserAddress1  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('Button:CopyEndUserAddress1') & '_prompt',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:CollectionAddress') = 1,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::Button:CopyEndUserAddress1  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::Button:CopyEndUserAddress1  ! copies value to session value if valid.
  !p_web.SSV('job:Company_Name_Collection',p_web.GSV('job:Company_Name'))
  !p_web.SSV('job:Address_Line1_Collection',p_web.GSV('job:Address_Line1'))
  !p_web.SSV('job:Address_Line2_Collection',p_web.GSV('job:Address_Line2'))
  !p_web.SSV('job:Address_Line3_Collection',p_web.GSV('job:Address_Line3'))
  !p_web.SSV('job:Postcode_Collection',p_web.GSV('job:Postcode'))
  !p_web.SSV('jobe2:HubCollection',p_web.GSV('jobe2:HubCustomer'))
  !p_web.SSV('job:Telephone_Collection',p_web.GSV('job:Telephone_Number'))
  do Value::Button:CopyEndUserAddress1
  do Value::job:Address_Line1_Collection  !1
  do Value::job:Address_Line2_Collection  !1
  do Value::job:Address_Line3_Collection  !1
  do Value::job:Company_Name_Collection  !1
  do Value::job:Telephone_Collection  !1
  do Value::jobe2:HubCollection  !1
  do Value::job:Postcode_Collection  !1

ValidateValue::Button:CopyEndUserAddress1  Routine
    If not (p_web.GSV('Hide:CollectionAddress') = 1)
    End

Value::Button:CopyEndUserAddress1  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:CollectionAddress') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('Button:CopyEndUserAddress1') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('Hide:CollectionAddress') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon(''&clip('CopyEndUserAddress1')&'.disabled=true;sv(''Button:CopyEndUserAddress1'',''amendaddressold_button:copyenduseraddress1_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','CopyEndUserAddress1','Copy / Clear Address',p_web.combine(Choose('Copy / Clear Address' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'SmallButtonFixed'),loc:formname,,,p_web.OpenDialog('ClearAddressDetails',p_web._jsok('Copy/Clear Address'),0,'''' & lower('AmendAddressOLD') & '''','','',p_web._jsok('Button:CopyEndUserAddress1'),p_web._jsok('AddType=C')),,loc:disabled,,,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()

Prompt::jbn:Delivery_Text  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('jbn:Delivery_Text') & '_prompt',Choose(p_web.GetSessionValue('Hide:DeliveryAddress') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GetSessionValue('Hide:DeliveryAddress') = 1,'',p_web.Translate('Delivery Text'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jbn:Delivery_Text  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jbn:Delivery_Text = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s255
    jbn:Delivery_Text = p_web.Dformat(p_web.GetValue('Value'),'@s255')
  End
  do ValidateValue::jbn:Delivery_Text  ! copies value to session value if valid.
  do Value::jbn:Delivery_Text
  do SendAlert

ValidateValue::jbn:Delivery_Text  Routine
    If not (p_web.GetSessionValue('Hide:DeliveryAddress') = 1)
    jbn:Delivery_Text = Upper(jbn:Delivery_Text)
      if loc:invalid = '' then p_web.SetSessionValue('jbn:Delivery_Text',jbn:Delivery_Text).
    End

Value::jbn:Delivery_Text  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GetSessionValue('Hide:DeliveryAddress') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('jbn:Delivery_Text') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    jbn:Delivery_Text = p_web.RestoreValue('jbn:Delivery_Text')
    do ValidateValue::jbn:Delivery_Text
    If jbn:Delivery_Text:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GetSessionValue('Hide:DeliveryAddress') = 1)
  ! --- TEXT --- jbn:Delivery_Text
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jbn:Delivery_Text'',''amendaddressold_jbn:delivery_text_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'readonly','')
  do SendPacket
  p_web.CreateTextArea('jbn:Delivery_Text',p_web.GetSessionValue('jbn:Delivery_Text'),5,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,255,,,,Net:Send)
  do SendPacket
  End
  p_web.DivFooter()

Prompt::jbn:Collection_Text  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('jbn:Collection_Text') & '_prompt',Choose(p_web.GetSessionValue('Hide:CollectionAddress') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GetSessionValue('Hide:CollectionAddress') = 1,'',p_web.Translate('Collection Text'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jbn:Collection_Text  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jbn:Collection_Text = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s255
    jbn:Collection_Text = p_web.Dformat(p_web.GetValue('Value'),'@s255')
  End
  do ValidateValue::jbn:Collection_Text  ! copies value to session value if valid.
  do Value::jbn:Collection_Text
  do SendAlert

ValidateValue::jbn:Collection_Text  Routine
    If not (p_web.GetSessionValue('Hide:CollectionAddress') = 1)
    jbn:Collection_Text = Upper(jbn:Collection_Text)
      if loc:invalid = '' then p_web.SetSessionValue('jbn:Collection_Text',jbn:Collection_Text).
    End

Value::jbn:Collection_Text  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GetSessionValue('Hide:CollectionAddress') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('jbn:Collection_Text') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    jbn:Collection_Text = p_web.RestoreValue('jbn:Collection_Text')
    do ValidateValue::jbn:Collection_Text
    If jbn:Collection_Text:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GetSessionValue('Hide:CollectionAddress') = 1)
  ! --- TEXT --- jbn:Collection_Text
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jbn:Collection_Text'',''amendaddressold_jbn:collection_text_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'readonly','')
  do SendPacket
  p_web.CreateTextArea('jbn:Collection_Text',p_web.GetSessionValue('jbn:Collection_Text'),5,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(jbn:Collection_Text),,,,Net:Send)
  do SendPacket
  End
  p_web.DivFooter()

Prompt::jbn:DelContactName  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('jbn:DelContactName') & '_prompt',Choose(p_web.GetSessionValue('Hide:DeliveryAddress') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GetSessionValue('Hide:DeliveryAddress') = 1,'',p_web.Translate('Contact Name'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jbn:DelContactName  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jbn:DelContactName = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    jbn:DelContactName = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::jbn:DelContactName  ! copies value to session value if valid.
  do Value::jbn:DelContactName
  do SendAlert

ValidateValue::jbn:DelContactName  Routine
    If not (p_web.GetSessionValue('Hide:DeliveryAddress') = 1)
    jbn:DelContactName = Upper(jbn:DelContactName)
      if loc:invalid = '' then p_web.SetSessionValue('jbn:DelContactName',jbn:DelContactName).
    End

Value::jbn:DelContactName  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GetSessionValue('Hide:DeliveryAddress') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('jbn:DelContactName') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    jbn:DelContactName = p_web.RestoreValue('jbn:DelContactName')
    do ValidateValue::jbn:DelContactName
    If jbn:DelContactName:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GetSessionValue('Hide:DeliveryAddress') = 1)
  ! --- STRING --- jbn:DelContactName
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jbn:DelContactName'',''amendaddressold_jbn:delcontactname_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jbn:DelContactName',p_web.GetSessionValueFormat('jbn:DelContactName'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'Contact Name',,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::jbn:ColContatName  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('jbn:ColContatName') & '_prompt',Choose(p_web.GetSessionValue('Hide:CollectionAddress') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GetSessionValue('Hide:CollectionAddress') = 1,'',p_web.Translate('Contact Name'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jbn:ColContatName  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jbn:ColContatName = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    jbn:ColContatName = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::jbn:ColContatName  ! copies value to session value if valid.
  do Value::jbn:ColContatName
  do SendAlert

ValidateValue::jbn:ColContatName  Routine
    If not (p_web.GetSessionValue('Hide:CollectionAddress') = 1)
    jbn:ColContatName = Upper(jbn:ColContatName)
      if loc:invalid = '' then p_web.SetSessionValue('jbn:ColContatName',jbn:ColContatName).
    End

Value::jbn:ColContatName  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GetSessionValue('Hide:CollectionAddress') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('jbn:ColContatName') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    jbn:ColContatName = p_web.RestoreValue('jbn:ColContatName')
    do ValidateValue::jbn:ColContatName
    If jbn:ColContatName:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GetSessionValue('Hide:CollectionAddress') = 1)
  ! --- STRING --- jbn:ColContatName
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jbn:ColContatName'',''amendaddressold_jbn:colcontatname_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jbn:ColContatName',p_web.GetSessionValueFormat('jbn:ColContatName'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'Contact Name',,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::jbn:DelDepartment  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('jbn:DelDepartment') & '_prompt',Choose(p_web.GetSessionValue('Hide:DeliveryAddress') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GetSessionValue('Hide:DeliveryAddress') = 1,'',p_web.Translate('Department'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jbn:DelDepartment  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jbn:DelDepartment = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    jbn:DelDepartment = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::jbn:DelDepartment  ! copies value to session value if valid.
  do Value::jbn:DelDepartment
  do SendAlert

ValidateValue::jbn:DelDepartment  Routine
    If not (p_web.GetSessionValue('Hide:DeliveryAddress') = 1)
    jbn:DelDepartment = Upper(jbn:DelDepartment)
      if loc:invalid = '' then p_web.SetSessionValue('jbn:DelDepartment',jbn:DelDepartment).
    End

Value::jbn:DelDepartment  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GetSessionValue('Hide:DeliveryAddress') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('jbn:DelDepartment') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    jbn:DelDepartment = p_web.RestoreValue('jbn:DelDepartment')
    do ValidateValue::jbn:DelDepartment
    If jbn:DelDepartment:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GetSessionValue('Hide:DeliveryAddress') = 1)
  ! --- STRING --- jbn:DelDepartment
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jbn:DelDepartment'',''amendaddressold_jbn:deldepartment_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jbn:DelDepartment',p_web.GetSessionValueFormat('jbn:DelDepartment'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'Department',,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::jbn:ColDepartment  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('jbn:ColDepartment') & '_prompt',Choose(p_web.GetSessionValue('Hide:CollectionAddress') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GetSessionValue('Hide:CollectionAddress') = 1,'',p_web.Translate('Department'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jbn:ColDepartment  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jbn:ColDepartment = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    jbn:ColDepartment = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::jbn:ColDepartment  ! copies value to session value if valid.
  do Value::jbn:ColDepartment
  do SendAlert

ValidateValue::jbn:ColDepartment  Routine
    If not (p_web.GetSessionValue('Hide:CollectionAddress') = 1)
    jbn:ColDepartment = Upper(jbn:ColDepartment)
      if loc:invalid = '' then p_web.SetSessionValue('jbn:ColDepartment',jbn:ColDepartment).
    End

Value::jbn:ColDepartment  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GetSessionValue('Hide:CollectionAddress') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('jbn:ColDepartment') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    jbn:ColDepartment = p_web.RestoreValue('jbn:ColDepartment')
    do ValidateValue::jbn:ColDepartment
    If jbn:ColDepartment:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GetSessionValue('Hide:CollectionAddress') = 1)
  ! --- STRING --- jbn:ColDepartment
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jbn:ColDepartment'',''amendaddressold_jbn:coldepartment_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jbn:ColDepartment',p_web.GetSessionValueFormat('jbn:ColDepartment'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'Department',,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::jobe:Sub_Sub_Account  Routine
  packet = clip(packet) & p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('jobe:Sub_Sub_Account') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Account Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jobe:Sub_Sub_Account  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jobe:Sub_Sub_Account = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s15
    jobe:Sub_Sub_Account = p_web.Dformat(p_web.GetValue('Value'),'@s15')
  ElsIf p_web.RequestAjax = 1 and p_web.IfExistsValue('sua:AccountNumber')
    jobe:Sub_Sub_Account = p_web.GetValue('sua:AccountNumber')
  ElsIf p_web.RequestAjax = 1
    jobe:Sub_Sub_Account = sua:AccountNumber
  End
  do ValidateValue::jobe:Sub_Sub_Account  ! copies value to session value if valid.
  p_Web.SetValue('lookupfield','jobe:Sub_Sub_Account')
  do AfterLookup
  do Value::job:Postcode_Delivery
  do Value::job:Address_Line1_Delivery
  do Value::job:Address_Line2_Delivery
  do Value::job:Address_Line3_Delivery
  do Value::job:Telephone_Delivery
  do Value::job:Company_Name_Delivery
  do Value::jobe:Sub_Sub_Account
  do SendAlert

ValidateValue::jobe:Sub_Sub_Account  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('jobe:Sub_Sub_Account',jobe:Sub_Sub_Account).
    End

Value::jobe:Sub_Sub_Account  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('AmendAddressOLD_' & p_web._nocolon('jobe:Sub_Sub_Account') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    jobe:Sub_Sub_Account = p_web.RestoreValue('jobe:Sub_Sub_Account')
    do ValidateValue::jobe:Sub_Sub_Account
    If jobe:Sub_Sub_Account:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- jobe:Sub_Sub_Account
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe:Sub_Sub_Account'',''amendaddressold_jobe:sub_sub_account_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('jobe:Sub_Sub_Account')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = 'readonly'
    packet = clip(packet) & p_web.CreateInput('text','jobe:Sub_Sub_Account',p_web.GetSessionValue('jobe:Sub_Sub_Account'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s15',loc:javascript,p_web.PicLength('@s15'),,,) & '<13,10>'!4
    if not loc:viewonly and not loc:readonly
      loc:fieldclass = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('BrowseSubAddresses')&'?LookupField=jobe:Sub_Sub_Account&Tab=2&ForeignField=sua:AccountNumber&_sort=sua:CompanyName&Refresh=sort'),,,,,loc:fieldclass)
    End
  do SendPacket
  End
  p_web.DivFooter()

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('AmendAddressOLD_nexttab_' & 0)
    job:Company_Name = p_web.GSV('job:Company_Name')
    do ValidateValue::job:Company_Name
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Company_Name
      !do SendAlert
      !exit
    End
    job:Address_Line1 = p_web.GSV('job:Address_Line1')
    do ValidateValue::job:Address_Line1
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Address_Line1
      !do SendAlert
      !exit
    End
    job:Address_Line2 = p_web.GSV('job:Address_Line2')
    do ValidateValue::job:Address_Line2
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Address_Line2
      !do SendAlert
      !exit
    End
    job:Address_Line3 = p_web.GSV('job:Address_Line3')
    do ValidateValue::job:Address_Line3
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Address_Line3
      !do SendAlert
      !exit
    End
    jobe2:HubCustomer = p_web.GSV('jobe2:HubCustomer')
    do ValidateValue::jobe2:HubCustomer
    If loc:Invalid
      loc:retrying = 1
      do Value::jobe2:HubCustomer
      !do SendAlert
      !exit
    End
    job:Postcode = p_web.GSV('job:Postcode')
    do ValidateValue::job:Postcode
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Postcode
      !do SendAlert
      !exit
    End
    job:Telephone_Number = p_web.GSV('job:Telephone_Number')
    do ValidateValue::job:Telephone_Number
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Telephone_Number
      !do SendAlert
      !exit
    End
    job:Fax_Number = p_web.GSV('job:Fax_Number')
    do ValidateValue::job:Fax_Number
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Fax_Number
      !do SendAlert
      !exit
    End
    jobe:EndUserEmailAddress = p_web.GSV('jobe:EndUserEmailAddress')
    do ValidateValue::jobe:EndUserEmailAddress
    If loc:Invalid
      loc:retrying = 1
      do Value::jobe:EndUserEmailAddress
      !do SendAlert
      !exit
    End
    jobe:EndUserTelNo = p_web.GSV('jobe:EndUserTelNo')
    do ValidateValue::jobe:EndUserTelNo
    If loc:Invalid
      loc:retrying = 1
      do Value::jobe:EndUserTelNo
      !do SendAlert
      !exit
    End
    jobe:VatNumber = p_web.GSV('jobe:VatNumber')
    do ValidateValue::jobe:VatNumber
    If loc:Invalid
      loc:retrying = 1
      do Value::jobe:VatNumber
      !do SendAlert
      !exit
    End
    jobe2:IDNumber = p_web.GSV('jobe2:IDNumber')
    do ValidateValue::jobe2:IDNumber
    If loc:Invalid
      loc:retrying = 1
      do Value::jobe2:IDNumber
      !do SendAlert
      !exit
    End
    jobe2:SMSNotification = p_web.GSV('jobe2:SMSNotification')
    do ValidateValue::jobe2:SMSNotification
    If loc:Invalid
      loc:retrying = 1
      do Value::jobe2:SMSNotification
      !do SendAlert
      !exit
    End
    jobe2:SMSAlertNumber = p_web.GSV('jobe2:SMSAlertNumber')
    do ValidateValue::jobe2:SMSAlertNumber
    If loc:Invalid
      loc:retrying = 1
      do Value::jobe2:SMSAlertNumber
      !do SendAlert
      !exit
    End
    jobe2:EmailNotification = p_web.GSV('jobe2:EmailNotification')
    do ValidateValue::jobe2:EmailNotification
    If loc:Invalid
      loc:retrying = 1
      do Value::jobe2:EmailNotification
      !do SendAlert
      !exit
    End
    jobe2:EmailAlertAddress = p_web.GSV('jobe2:EmailAlertAddress')
    do ValidateValue::jobe2:EmailAlertAddress
    If loc:Invalid
      loc:retrying = 1
      do Value::jobe2:EmailAlertAddress
      !do SendAlert
      !exit
    End
    jobe2:CourierWaybillNumber = p_web.GSV('jobe2:CourierWaybillNumber')
    do ValidateValue::jobe2:CourierWaybillNumber
    If loc:Invalid
      loc:retrying = 1
      do Value::jobe2:CourierWaybillNumber
      !do SendAlert
      !exit
    End
    If loc:Invalid then exit.
  of lower('AmendAddressOLD_nexttab_' & 1)
    job:Company_Name_Delivery = p_web.GSV('job:Company_Name_Delivery')
    do ValidateValue::job:Company_Name_Delivery
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Company_Name_Delivery
      !do SendAlert
      !exit
    End
    job:Company_Name_Collection = p_web.GSV('job:Company_Name_Collection')
    do ValidateValue::job:Company_Name_Collection
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Company_Name_Collection
      !do SendAlert
      !exit
    End
    job:Address_Line1_Delivery = p_web.GSV('job:Address_Line1_Delivery')
    do ValidateValue::job:Address_Line1_Delivery
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Address_Line1_Delivery
      !do SendAlert
      !exit
    End
    job:Address_Line1_Collection = p_web.GSV('job:Address_Line1_Collection')
    do ValidateValue::job:Address_Line1_Collection
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Address_Line1_Collection
      !do SendAlert
      !exit
    End
    job:Address_Line2_Delivery = p_web.GSV('job:Address_Line2_Delivery')
    do ValidateValue::job:Address_Line2_Delivery
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Address_Line2_Delivery
      !do SendAlert
      !exit
    End
    job:Address_Line2_Collection = p_web.GSV('job:Address_Line2_Collection')
    do ValidateValue::job:Address_Line2_Collection
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Address_Line2_Collection
      !do SendAlert
      !exit
    End
    job:Address_Line3_Delivery = p_web.GSV('job:Address_Line3_Delivery')
    do ValidateValue::job:Address_Line3_Delivery
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Address_Line3_Delivery
      !do SendAlert
      !exit
    End
    job:Address_Line3_Collection = p_web.GSV('job:Address_Line3_Collection')
    do ValidateValue::job:Address_Line3_Collection
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Address_Line3_Collection
      !do SendAlert
      !exit
    End
    jobe2:HubDelivery = p_web.GSV('jobe2:HubDelivery')
    do ValidateValue::jobe2:HubDelivery
    If loc:Invalid
      loc:retrying = 1
      do Value::jobe2:HubDelivery
      !do SendAlert
      !exit
    End
    jobe2:HubCollection = p_web.GSV('jobe2:HubCollection')
    do ValidateValue::jobe2:HubCollection
    If loc:Invalid
      loc:retrying = 1
      do Value::jobe2:HubCollection
      !do SendAlert
      !exit
    End
    job:Postcode_Delivery = p_web.GSV('job:Postcode_Delivery')
    do ValidateValue::job:Postcode_Delivery
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Postcode_Delivery
      !do SendAlert
      !exit
    End
    job:Postcode_Collection = p_web.GSV('job:Postcode_Collection')
    do ValidateValue::job:Postcode_Collection
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Postcode_Collection
      !do SendAlert
      !exit
    End
    job:Telephone_Delivery = p_web.GSV('job:Telephone_Delivery')
    do ValidateValue::job:Telephone_Delivery
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Telephone_Delivery
      !do SendAlert
      !exit
    End
    job:Telephone_Collection = p_web.GSV('job:Telephone_Collection')
    do ValidateValue::job:Telephone_Collection
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Telephone_Collection
      !do SendAlert
      !exit
    End
    jbn:Delivery_Text = p_web.GSV('jbn:Delivery_Text')
    do ValidateValue::jbn:Delivery_Text
    If loc:Invalid
      loc:retrying = 1
      do Value::jbn:Delivery_Text
      !do SendAlert
      !exit
    End
    jbn:Collection_Text = p_web.GSV('jbn:Collection_Text')
    do ValidateValue::jbn:Collection_Text
    If loc:Invalid
      loc:retrying = 1
      do Value::jbn:Collection_Text
      !do SendAlert
      !exit
    End
    jbn:DelContactName = p_web.GSV('jbn:DelContactName')
    do ValidateValue::jbn:DelContactName
    If loc:Invalid
      loc:retrying = 1
      do Value::jbn:DelContactName
      !do SendAlert
      !exit
    End
    jbn:ColContatName = p_web.GSV('jbn:ColContatName')
    do ValidateValue::jbn:ColContatName
    If loc:Invalid
      loc:retrying = 1
      do Value::jbn:ColContatName
      !do SendAlert
      !exit
    End
    jbn:DelDepartment = p_web.GSV('jbn:DelDepartment')
    do ValidateValue::jbn:DelDepartment
    If loc:Invalid
      loc:retrying = 1
      do Value::jbn:DelDepartment
      !do SendAlert
      !exit
    End
    jbn:ColDepartment = p_web.GSV('jbn:ColDepartment')
    do ValidateValue::jbn:ColDepartment
    If loc:Invalid
      loc:retrying = 1
      do Value::jbn:ColDepartment
      !do SendAlert
      !exit
    End
    If loc:Invalid then exit.
  of lower('AmendAddressOLD_nexttab_' & 2)
    jobe:Sub_Sub_Account = p_web.GSV('jobe:Sub_Sub_Account')
    do ValidateValue::jobe:Sub_Sub_Account
    If loc:Invalid
      loc:retrying = 1
      do Value::jobe:Sub_Sub_Account
      !do SendAlert
      !exit
    End
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_AmendAddressOLD_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('AmendAddressOLD_tab_' & 0)
    do GenerateTab0
  of lower('AmendAddressOLD_job:Company_Name_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Company_Name
      of event:timer
        do Value::job:Company_Name
      else
        do Value::job:Company_Name
      end
  of lower('AmendAddressOLD_job:Address_Line1_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Address_Line1
      of event:timer
        do Value::job:Address_Line1
      else
        do Value::job:Address_Line1
      end
  of lower('AmendAddressOLD_job:Address_Line2_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Address_Line2
      of event:timer
        do Value::job:Address_Line2
      else
        do Value::job:Address_Line2
      end
  of lower('AmendAddressOLD_job:Address_Line3_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Address_Line3
      of event:timer
        do Value::job:Address_Line3
      else
        do Value::job:Address_Line3
      end
  of lower('AmendAddressOLD_jobe2:HubCustomer_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:HubCustomer
      of event:timer
        do Value::jobe2:HubCustomer
      else
        do Value::jobe2:HubCustomer
      end
  of lower('AmendAddressOLD_job:Postcode_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Postcode
      of event:timer
        do Value::job:Postcode
      else
        do Value::job:Postcode
      end
  of lower('AmendAddressOLD_job:Telephone_Number_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Telephone_Number
      of event:timer
        do Value::job:Telephone_Number
      else
        do Value::job:Telephone_Number
      end
  of lower('AmendAddressOLD_job:Fax_Number_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Fax_Number
      of event:timer
        do Value::job:Fax_Number
      else
        do Value::job:Fax_Number
      end
  of lower('AmendAddressOLD_jobe:EndUserEmailAddress_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe:EndUserEmailAddress
      of event:timer
        do Value::jobe:EndUserEmailAddress
      else
        do Value::jobe:EndUserEmailAddress
      end
  of lower('AmendAddressOLD_jobe:EndUserTelNo_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe:EndUserTelNo
      of event:timer
        do Value::jobe:EndUserTelNo
      else
        do Value::jobe:EndUserTelNo
      end
  of lower('AmendAddressOLD_jobe:VatNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe:VatNumber
      of event:timer
        do Value::jobe:VatNumber
      else
        do Value::jobe:VatNumber
      end
  of lower('AmendAddressOLD_jobe2:IDNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:IDNumber
      of event:timer
        do Value::jobe2:IDNumber
      else
        do Value::jobe2:IDNumber
      end
  of lower('AmendAddressOLD_jobe2:SMSNotification_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:SMSNotification
      of event:timer
        do Value::jobe2:SMSNotification
      else
        do Value::jobe2:SMSNotification
      end
  of lower('AmendAddressOLD_jobe2:SMSAlertNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:SMSAlertNumber
      of event:timer
        do Value::jobe2:SMSAlertNumber
      else
        do Value::jobe2:SMSAlertNumber
      end
  of lower('AmendAddressOLD_jobe2:EmailNotification_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:EmailNotification
      of event:timer
        do Value::jobe2:EmailNotification
      else
        do Value::jobe2:EmailNotification
      end
  of lower('AmendAddressOLD_jobe2:EmailAlertAddress_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:EmailAlertAddress
      of event:timer
        do Value::jobe2:EmailAlertAddress
      else
        do Value::jobe2:EmailAlertAddress
      end
  of lower('AmendAddressOLD_jobe2:CourierWaybillNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:CourierWaybillNumber
      of event:timer
        do Value::jobe2:CourierWaybillNumber
      else
        do Value::jobe2:CourierWaybillNumber
      end
  of lower('AmendAddressOLD_tab_' & 1)
    do GenerateTab1
  of lower('AmendAddressOLD_job:Company_Name_Delivery_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Company_Name_Delivery
      of event:timer
        do Value::job:Company_Name_Delivery
      else
        do Value::job:Company_Name_Delivery
      end
  of lower('AmendAddressOLD_job:Company_Name_Collection_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Company_Name_Collection
      of event:timer
        do Value::job:Company_Name_Collection
      else
        do Value::job:Company_Name_Collection
      end
  of lower('AmendAddressOLD_job:Address_Line1_Delivery_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Address_Line1_Delivery
      of event:timer
        do Value::job:Address_Line1_Delivery
      else
        do Value::job:Address_Line1_Delivery
      end
  of lower('AmendAddressOLD_job:Address_Line1_Collection_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Address_Line1_Collection
      of event:timer
        do Value::job:Address_Line1_Collection
      else
        do Value::job:Address_Line1_Collection
      end
  of lower('AmendAddressOLD_job:Address_Line2_Delivery_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Address_Line2_Delivery
      of event:timer
        do Value::job:Address_Line2_Delivery
      else
        do Value::job:Address_Line2_Delivery
      end
  of lower('AmendAddressOLD_job:Address_Line2_Collection_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Address_Line2_Collection
      of event:timer
        do Value::job:Address_Line2_Collection
      else
        do Value::job:Address_Line2_Collection
      end
  of lower('AmendAddressOLD_job:Address_Line3_Delivery_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Address_Line3_Delivery
      of event:timer
        do Value::job:Address_Line3_Delivery
      else
        do Value::job:Address_Line3_Delivery
      end
  of lower('AmendAddressOLD_job:Address_Line3_Collection_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Address_Line3_Collection
      of event:timer
        do Value::job:Address_Line3_Collection
      else
        do Value::job:Address_Line3_Collection
      end
  of lower('AmendAddressOLD_jobe2:HubDelivery_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:HubDelivery
      of event:timer
        do Value::jobe2:HubDelivery
      else
        do Value::jobe2:HubDelivery
      end
  of lower('AmendAddressOLD_jobe2:HubCollection_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:HubCollection
      of event:timer
        do Value::jobe2:HubCollection
      else
        do Value::jobe2:HubCollection
      end
  of lower('AmendAddressOLD_job:Postcode_Delivery_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Postcode_Delivery
      of event:timer
        do Value::job:Postcode_Delivery
      else
        do Value::job:Postcode_Delivery
      end
  of lower('AmendAddressOLD_job:Postcode_Collection_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Postcode_Collection
      of event:timer
        do Value::job:Postcode_Collection
      else
        do Value::job:Postcode_Collection
      end
  of lower('AmendAddressOLD_job:Telephone_Delivery_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Telephone_Delivery
      of event:timer
        do Value::job:Telephone_Delivery
      else
        do Value::job:Telephone_Delivery
      end
  of lower('AmendAddressOLD_job:Telephone_Collection_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Telephone_Collection
      of event:timer
        do Value::job:Telephone_Collection
      else
        do Value::job:Telephone_Collection
      end
  of lower('AmendAddressOLD_Button:CopyEndUserAddress_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::Button:CopyEndUserAddress
      of event:timer
        do Value::Button:CopyEndUserAddress
      else
        do Value::Button:CopyEndUserAddress
      end
  of lower('AmendAddressOLD_Button:CopyEndUserAddress1_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::Button:CopyEndUserAddress1
      of event:timer
        do Value::Button:CopyEndUserAddress1
      else
        do Value::Button:CopyEndUserAddress1
      end
  of lower('AmendAddressOLD_jbn:Delivery_Text_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jbn:Delivery_Text
      of event:timer
        do Value::jbn:Delivery_Text
      else
        do Value::jbn:Delivery_Text
      end
  of lower('AmendAddressOLD_jbn:Collection_Text_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jbn:Collection_Text
      of event:timer
        do Value::jbn:Collection_Text
      else
        do Value::jbn:Collection_Text
      end
  of lower('AmendAddressOLD_jbn:DelContactName_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jbn:DelContactName
      of event:timer
        do Value::jbn:DelContactName
      else
        do Value::jbn:DelContactName
      end
  of lower('AmendAddressOLD_jbn:ColContatName_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jbn:ColContatName
      of event:timer
        do Value::jbn:ColContatName
      else
        do Value::jbn:ColContatName
      end
  of lower('AmendAddressOLD_jbn:DelDepartment_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jbn:DelDepartment
      of event:timer
        do Value::jbn:DelDepartment
      else
        do Value::jbn:DelDepartment
      end
  of lower('AmendAddressOLD_jbn:ColDepartment_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jbn:ColDepartment
      of event:timer
        do Value::jbn:ColDepartment
      else
        do Value::jbn:ColDepartment
      end
  of lower('AmendAddressOLD_tab_' & 2)
    do GenerateTab2
  of lower('AmendAddressOLD_jobe:Sub_Sub_Account_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe:Sub_Sub_Account
      of event:timer
        do Value::jobe:Sub_Sub_Account
      else
        do Value::jobe:Sub_Sub_Account
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('AmendAddressOLD_form:ready_',1)

  p_web.SetSessionValue('AmendAddressOLD_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_AmendAddressOLD',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('AmendAddressOLD_form:ready_',1)
  p_web.SetSessionValue('AmendAddressOLD_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_AmendAddressOLD',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('AmendAddressOLD_form:ready_',1)
  p_web.SetSessionValue('AmendAddressOLD_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('AmendAddressOLD:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('AmendAddressOLD_form:ready_',1)
  p_web.SetSessionValue('AmendAddressOLD_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('AmendAddressOLD:Primed',0)
  p_web.setsessionvalue('showtab_AmendAddressOLD',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  ! GetDescription resets the 'NewValue' to the code, if the description is found. Returns 1 if code valid.
  loc:ok = p_web.GetDescription(SUBURB,sur:SuburbKey,sur:SuburbKey,sur:Suburb,,p_web.GetSessionValue('job:Address_Line3')) !2
  if loc:ok then p_web.FileToSessionQueue(SUBURB).
  ! GetDescription resets the 'NewValue' to the code, if the description is found. Returns 1 if code valid.
  loc:ok = p_web.GetDescription(SUBURB,sur:SuburbKey,sur:SuburbKey,sur:Suburb,,p_web.GetSessionValue('job:Address_Line3_Delivery')) !2
  if loc:ok then p_web.FileToSessionQueue(SUBURB).
  ! GetDescription resets the 'NewValue' to the code, if the description is found. Returns 1 if code valid.
  loc:ok = p_web.GetDescription(SUBURB,sur:SuburbKey,sur:SuburbKey,sur:Suburb,,p_web.GetSessionValue('job:Address_Line3_Collection')) !2
  if loc:ok then p_web.FileToSessionQueue(SUBURB).
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
      If not (1=0)
          If p_web.IfExistsValue('job:Company_Name')
            job:Company_Name = p_web.GetValue('job:Company_Name')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('job:Address_Line1')
            job:Address_Line1 = p_web.GetValue('job:Address_Line1')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('job:Address_Line2')
            job:Address_Line2 = p_web.GetValue('job:Address_Line2')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('job:Address_Line3')
            job:Address_Line3 = p_web.GetValue('job:Address_Line3')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('job:Telephone_Number')
            job:Telephone_Number = p_web.GetValue('job:Telephone_Number')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('job:Fax_Number')
            job:Fax_Number = p_web.GetValue('job:Fax_Number')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('jobe:EndUserEmailAddress')
            jobe:EndUserEmailAddress = p_web.GetValue('jobe:EndUserEmailAddress')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('jobe:EndUserTelNo')
            jobe:EndUserTelNo = p_web.GetValue('jobe:EndUserTelNo')
          End
      End
    If (p_web.GSV('Hide:VatNumber') <> 1)
      If not (1=0)
          If p_web.IfExistsValue('jobe:VatNumber')
            jobe:VatNumber = p_web.GetValue('jobe:VatNumber')
          End
      End
    End
      If not (1=0)
          If p_web.IfExistsValue('jobe2:IDNumber')
            jobe2:IDNumber = p_web.GetValue('jobe2:IDNumber')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('jobe2:SMSNotification') = 0
            p_web.SetValue('jobe2:SMSNotification',0)
            jobe2:SMSNotification = 0
          Else
            jobe2:SMSNotification = p_web.GetValue('jobe2:SMSNotification')
          End
      End
      If not (p_web.GSV('jobe2:SMSNotification') = 0)
          If p_web.IfExistsValue('jobe2:SMSAlertNumber')
            jobe2:SMSAlertNumber = p_web.GetValue('jobe2:SMSAlertNumber')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('jobe2:EmailNotification') = 0
            p_web.SetValue('jobe2:EmailNotification',0)
            jobe2:EmailNotification = 0
          Else
            jobe2:EmailNotification = p_web.GetValue('jobe2:EmailNotification')
          End
      End
      If not (p_web.GSV('jobe2:EmailNotification') = 0)
          If p_web.IfExistsValue('jobe2:EmailAlertAddress')
            jobe2:EmailAlertAddress = p_web.GetValue('jobe2:EmailAlertAddress')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('jobe2:CourierWaybillNumber')
            jobe2:CourierWaybillNumber = p_web.GetValue('jobe2:CourierWaybillNumber')
          End
      End
      If not (p_web.GSV('Hide:DeliveryAddress') = 1)
        If not (p_web.GetSessionValue('ReadOnly:DeliveryAddress') = 1)
          If p_web.IfExistsValue('job:Company_Name_Delivery')
            job:Company_Name_Delivery = p_web.GetValue('job:Company_Name_Delivery')
          End
        End
      End
      If not (p_web.GSV('Hide:CollectionAddress') = 1)
        If not (p_web.GetSessionValue('ReadOnly:CollectionAddress') = 1)
          If p_web.IfExistsValue('job:Company_Name_Collection')
            job:Company_Name_Collection = p_web.GetValue('job:Company_Name_Collection')
          End
        End
      End
      If not (p_web.GSV('Hide:DeliveryAddress') = 1)
        If not (p_web.GetSessionValue('ReadOnly:DeliveryAddress') = 1)
          If p_web.IfExistsValue('job:Address_Line1_Delivery')
            job:Address_Line1_Delivery = p_web.GetValue('job:Address_Line1_Delivery')
          End
        End
      End
      If not (p_web.GSV('Hide:CollectionAddress') = 1)
        If not (p_web.GetSessionValue('ReadOnly:CollectionAddress') = 1)
          If p_web.IfExistsValue('job:Address_Line1_Collection')
            job:Address_Line1_Collection = p_web.GetValue('job:Address_Line1_Collection')
          End
        End
      End
      If not (p_web.GSV('Hide:DeliveryAddress') = 1)
        If not (p_web.GetSessionValue('ReadOnly:DeliveryAddress') = 1)
          If p_web.IfExistsValue('job:Address_Line2_Delivery')
            job:Address_Line2_Delivery = p_web.GetValue('job:Address_Line2_Delivery')
          End
        End
      End
      If not (p_web.GSV('Hide:CollectionAddress') = 1)
        If not (p_web.GetSessionValue('ReadOnly:CollectionAddress') = 1)
          If p_web.IfExistsValue('job:Address_Line2_Collection')
            job:Address_Line2_Collection = p_web.GetValue('job:Address_Line2_Collection')
          End
        End
      End
      If not (p_web.GSV('Hide:DeliveryAddress') = 1)
        If not (p_web.GetSessionValue('ReadOnly:DeliveryAddress') = 1)
          If p_web.IfExistsValue('job:Address_Line3_Delivery')
            job:Address_Line3_Delivery = p_web.GetValue('job:Address_Line3_Delivery')
          End
        End
      End
      If not (p_web.GSV('Hide:CollectionAddress') = 1)
        If not (p_web.GetSessionValue('ReadOnly:CollectionAddress') = 1)
          If p_web.IfExistsValue('job:Address_Line3_Collection')
            job:Address_Line3_Collection = p_web.GetValue('job:Address_Line3_Collection')
          End
        End
      End
      If not (p_web.GSV('Hide:DeliveryAddress') = 1)
        If not (p_web.GetSessionValue('ReadOnly:DeliveryAddress') = 1)
          If p_web.IfExistsValue('job:Telephone_Delivery')
            job:Telephone_Delivery = p_web.GetValue('job:Telephone_Delivery')
          End
        End
      End
      If not (p_web.GSV('Hide:CollectionAddress') = 1)
        If not (p_web.GetSessionValue('ReadOnly:CollectionAddress') = 1)
          If p_web.IfExistsValue('job:Telephone_Collection')
            job:Telephone_Collection = p_web.GetValue('job:Telephone_Collection')
          End
        End
      End
      If not (p_web.GetSessionValue('Hide:DeliveryAddress') = 1)
          If p_web.IfExistsValue('jbn:Delivery_Text')
            jbn:Delivery_Text = p_web.GetValue('jbn:Delivery_Text')
          End
      End
      If not (p_web.GetSessionValue('Hide:CollectionAddress') = 1)
          If p_web.IfExistsValue('jbn:Collection_Text')
            jbn:Collection_Text = p_web.GetValue('jbn:Collection_Text')
          End
      End
      If not (p_web.GetSessionValue('Hide:DeliveryAddress') = 1)
          If p_web.IfExistsValue('jbn:DelContactName')
            jbn:DelContactName = p_web.GetValue('jbn:DelContactName')
          End
      End
      If not (p_web.GetSessionValue('Hide:CollectionAddress') = 1)
          If p_web.IfExistsValue('jbn:ColContatName')
            jbn:ColContatName = p_web.GetValue('jbn:ColContatName')
          End
      End
      If not (p_web.GetSessionValue('Hide:DeliveryAddress') = 1)
          If p_web.IfExistsValue('jbn:DelDepartment')
            jbn:DelDepartment = p_web.GetValue('jbn:DelDepartment')
          End
      End
      If not (p_web.GetSessionValue('Hide:CollectionAddress') = 1)
          If p_web.IfExistsValue('jbn:ColDepartment')
            jbn:ColDepartment = p_web.GetValue('jbn:ColDepartment')
          End
      End
  If p_web.GSV('Hide:SubSubAccount') <> 1
      If not (1=0)
          If p_web.IfExistsValue('jobe:Sub_Sub_Account')
            jobe:Sub_Sub_Account = p_web.GetValue('jobe:Sub_Sub_Account')
          End
      End
  End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('AmendAddressOLD_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('AmendAddressOLD_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
    do ValidateValue::temp:Title
    If loc:Invalid then exit.
    do ValidateValue::job:Company_Name
    If loc:Invalid then exit.
    do ValidateValue::job:Address_Line1
    If loc:Invalid then exit.
    do ValidateValue::job:Address_Line2
    If loc:Invalid then exit.
    do ValidateValue::job:Address_Line3
    If loc:Invalid then exit.
    do ValidateValue::jobe2:HubCustomer
    If loc:Invalid then exit.
    do ValidateValue::job:Postcode
    If loc:Invalid then exit.
    do ValidateValue::job:Telephone_Number
    If loc:Invalid then exit.
    do ValidateValue::job:Fax_Number
    If loc:Invalid then exit.
    do ValidateValue::jobe:EndUserEmailAddress
    If loc:Invalid then exit.
    do ValidateValue::jobe:EndUserTelNo
    If loc:Invalid then exit.
    do ValidateValue::jobe:VatNumber
    If loc:Invalid then exit.
    do ValidateValue::jobe2:IDNumber
    If loc:Invalid then exit.
    do ValidateValue::jobe2:SMSNotification
    If loc:Invalid then exit.
    do ValidateValue::jobe2:SMSAlertNumber
    If loc:Invalid then exit.
    do ValidateValue::jobe2:EmailNotification
    If loc:Invalid then exit.
    do ValidateValue::jobe2:EmailAlertAddress
    If loc:Invalid then exit.
    do ValidateValue::jobe2:CourierWaybillNumber
    If loc:Invalid then exit.
  ! tab = 2
    loc:InvalidTab += 1
    do ValidateValue::DeliveryAddress
    If loc:Invalid then exit.
    do ValidateValue::CollectionAddress
    If loc:Invalid then exit.
    do ValidateValue::job:Company_Name_Delivery
    If loc:Invalid then exit.
    do ValidateValue::job:Company_Name_Collection
    If loc:Invalid then exit.
    do ValidateValue::job:Address_Line1_Delivery
    If loc:Invalid then exit.
    do ValidateValue::job:Address_Line1_Collection
    If loc:Invalid then exit.
    do ValidateValue::job:Address_Line2_Delivery
    If loc:Invalid then exit.
    do ValidateValue::job:Address_Line2_Collection
    If loc:Invalid then exit.
    do ValidateValue::job:Address_Line3_Delivery
    If loc:Invalid then exit.
    do ValidateValue::job:Address_Line3_Collection
    If loc:Invalid then exit.
    do ValidateValue::jobe2:HubDelivery
    If loc:Invalid then exit.
    do ValidateValue::jobe2:HubCollection
    If loc:Invalid then exit.
    do ValidateValue::job:Postcode_Delivery
    If loc:Invalid then exit.
    do ValidateValue::job:Postcode_Collection
    If loc:Invalid then exit.
    do ValidateValue::job:Telephone_Delivery
    If loc:Invalid then exit.
    do ValidateValue::job:Telephone_Collection
    If loc:Invalid then exit.
    do ValidateValue::Button:CopyEndUserAddress
    If loc:Invalid then exit.
    do ValidateValue::Button:CopyEndUserAddress1
    If loc:Invalid then exit.
    do ValidateValue::jbn:Delivery_Text
    If loc:Invalid then exit.
    do ValidateValue::jbn:Collection_Text
    If loc:Invalid then exit.
    do ValidateValue::jbn:DelContactName
    If loc:Invalid then exit.
    do ValidateValue::jbn:ColContatName
    If loc:Invalid then exit.
    do ValidateValue::jbn:DelDepartment
    If loc:Invalid then exit.
    do ValidateValue::jbn:ColDepartment
    If loc:Invalid then exit.
  ! tab = 4
  If p_web.GSV('Hide:SubSubAccount') <> 1
    loc:InvalidTab += 1
    do ValidateValue::jobe:Sub_Sub_Account
    If loc:Invalid then exit.
  End
  ! The following fields are not on the form, but need to be checked anyway.
  !Validation
  !p_web.SetSessionValue('job:Company_Name',p_web.GetSessionValue('tmp:CompanyName'))
  !p_web.SetSessionValue('job:Address_Line1',p_web.GetSessionValue('tmp:AddressLine1'))
  !p_web.SetSessionValue('job:Address_Line2',p_web.GetSessionValue('tmp:AddressLine2'))
  !p_web.SetSessionValue('job:Address_Line3',p_web.GetSessionValue('tmp:Suburb'))
  !p_web.SetSessionValue('jobe2:HubCustomer',p_web.GetSessionValue('tmp:HubCustomer'))
  !p_web.SetSessionValue('job:Telephone_Number',p_web.GetSessionValue('tmp:TelephoneNumber'))
  !p_web.SetSessionValue('job:Fax_Number',p_web.GetSessionValue('tmp:FaxNumber'))
  !p_web.SetSessionValue('jobe:EndUserEmailAddress',p_web.GetSessionValue('tmp:EmailAddress'))
  !p_web.SetSessionValue('jobe:EndUserTelNo',p_web.GetSessionValue('tmp:EndUserTelephoneNumber'))
  !p_web.SetSessionValue('jobe2:IDNumber',p_web.GetSessionValue('tmp:IDNumber'))
  !p_web.SetSessionValue('jobe2:SMSNotification',p_web.GetSessionValue('tmp:SMSNotification'))
  !p_web.SetSessionValue('jobe2:SMSAlertNumber',p_web.GetSessionValue('tmp:NotificationMobileNumber'))
  !p_web.SetSessionValue('jobe2:EmailNotification',p_web.GetSessionValue('tmp:EmailNotification'))
  !p_web.SetSessionValue('jobe2:EmailAlertAddress',p_web.GetSessionValue('tmp:NotificationEmailAddress'))
  !
  !p_web.SetSessionValue('job:Company_Name_Delivery',p_web.GetSessionValue('tmp:CompanyNameDelivery'))
  !p_web.SetSessionValue('job:Company_Name_Collection',p_web.GetSessionValue('tmp:CompanyNameCollection'))
  !p_web.SetSessionValue('job:Address_Line1_Delivery',p_web.GetSessionValue('tmp:AddressLine1Delivery'))
  !p_web.SetSessionValue('job:Address_Line1_Collection',p_web.GetSessionValue('tmp:AddressLine1Collection'))
  !p_web.SetSessionValue('job:Address_Line3_Delivery',p_web.GetSessionValue('tmp:SuburbDelivery'))
  !p_web.SetSessionValue('job:Address_Line3_Collection',p_web.GetSessionValue('tmp:SuburbCollection'))
  !p_web.SetSessionValue('jobe2:HubDelivery',p_web.GetSessionValue('tmp:HubDelivery'))
  !p_web.SetSessionValue('jobe2:HubCollection',p_web.GetSessionValue('tmp:HubCollection'))
  !p_web.SetSessionValue('job:Telephone_Delivery',p_web.GetSessionValue('tmp:TelephoneNumberDelivery'))
  !p_web.SetSessionValue('job:Telephone_Collection',p_web.GetSessionValue('tmp:TelephoneNumberCollection'))
  !
  !
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('AmendAddressOLD:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('job:Company_Name')
  p_web.StoreValue('job:Address_Line1')
  p_web.StoreValue('job:Address_Line2')
  p_web.StoreValue('job:Address_Line3')
  p_web.StoreValue('jobe2:HubCustomer')
  p_web.StoreValue('job:Postcode')
  p_web.StoreValue('job:Telephone_Number')
  p_web.StoreValue('job:Fax_Number')
  p_web.StoreValue('jobe2:IDNumber')
  p_web.StoreValue('jobe2:SMSNotification')
  p_web.StoreValue('jobe2:SMSAlertNumber')
  p_web.StoreValue('jobe2:EmailNotification')
  p_web.StoreValue('jobe2:EmailAlertAddress')
  p_web.StoreValue('jobe2:CourierWaybillNumber')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('job:Company_Name_Delivery')
  p_web.StoreValue('job:Company_Name_Collection')
  p_web.StoreValue('job:Address_Line1_Delivery')
  p_web.StoreValue('job:Address_Line1_Collection')
  p_web.StoreValue('job:Address_Line2_Delivery')
  p_web.StoreValue('job:Address_Line2_Collection')
  p_web.StoreValue('job:Address_Line3_Delivery')
  p_web.StoreValue('job:Address_Line3_Collection')
  p_web.StoreValue('jobe2:HubDelivery')
  p_web.StoreValue('jobe2:HubCollection')
  p_web.StoreValue('job:Postcode_Delivery')
  p_web.StoreValue('job:Postcode_Collection')
  p_web.StoreValue('job:Telephone_Delivery')
  p_web.StoreValue('job:Telephone_Collection')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('jbn:Delivery_Text')
  p_web.StoreValue('jbn:Collection_Text')
  p_web.StoreValue('jbn:DelContactName')
  p_web.StoreValue('jbn:ColContatName')
  p_web.StoreValue('jbn:DelDepartment')
  p_web.StoreValue('jbn:ColDepartment')

FormCollectionText   PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
FilesOpened     Long
JOBNOTES::State  USHORT
jbn:Collection_Text:IsInvalid  Long
jbn:ColContatName:IsInvalid  Long
jbn:ColDepartment:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormCollectionText')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'FormCollectionText_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('FormCollectionText','')
    p_web.DivHeader('FormCollectionText',p_web.combine(p_web.site.style.formdiv,))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('FormCollectionText') = 0
        p_web.AddPreCall('FormCollectionText')
        p_web.DivHeader('popup_FormCollectionText','nt-hidden')
        p_web.DivHeader('FormCollectionText',p_web.combine(p_web.site.style.formdiv,))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(400)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_FormCollectionText_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_FormCollectionText_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormCollectionText',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_FormCollectionText',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormCollectionText',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_FormCollectionText',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormCollectionText',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormCollectionText',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_FormCollectionText',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormCollectionText',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_FormCollectionText',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormCollectionText',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormCollectionText',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormCollectionText',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('FormCollectionText')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(JOBNOTES)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(JOBNOTES)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FormCollectionText_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'FormCollectionText'
    end
    p_web.formsettings.proc = 'FormCollectionText'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('jbn:ColContatName')
    p_web.SetPicture('jbn:ColContatName','@s30')
  End
  p_web.SetSessionPicture('jbn:ColContatName','@s30')
  If p_web.IfExistsValue('jbn:ColDepartment')
    p_web.SetPicture('jbn:ColDepartment','@s30')
  End
  p_web.SetSessionPicture('jbn:ColDepartment','@s30')

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  if p_web.IfExistsValue('jbn:Collection_Text') = 0
    p_web.SetSessionValue('jbn:Collection_Text',jbn:Collection_Text)
  Else
    jbn:Collection_Text = p_web.GetSessionValue('jbn:Collection_Text')
  End
  if p_web.IfExistsValue('jbn:ColContatName') = 0
    p_web.SetSessionValue('jbn:ColContatName',jbn:ColContatName)
  Else
    jbn:ColContatName = p_web.GetSessionValue('jbn:ColContatName')
  End
  if p_web.IfExistsValue('jbn:ColDepartment') = 0
    p_web.SetSessionValue('jbn:ColDepartment',jbn:ColDepartment)
  Else
    jbn:ColDepartment = p_web.GetSessionValue('jbn:ColDepartment')
  End

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('jbn:Collection_Text')
    jbn:Collection_Text = p_web.GetValue('jbn:Collection_Text')
    p_web.SetSessionValue('jbn:Collection_Text',jbn:Collection_Text)
  Else
    jbn:Collection_Text = p_web.GetSessionValue('jbn:Collection_Text')
  End
  if p_web.IfExistsValue('jbn:ColContatName')
    jbn:ColContatName = p_web.GetValue('jbn:ColContatName')
    p_web.SetSessionValue('jbn:ColContatName',jbn:ColContatName)
  Else
    jbn:ColContatName = p_web.GetSessionValue('jbn:ColContatName')
  End
  if p_web.IfExistsValue('jbn:ColDepartment')
    jbn:ColDepartment = p_web.GetValue('jbn:ColDepartment')
    p_web.SetSessionValue('jbn:ColDepartment',jbn:ColDepartment)
  Else
    jbn:ColDepartment = p_web.GetSessionValue('jbn:ColDepartment')
  End

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('FormCollectionText_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferFormCollectionText')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormCollectionText_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormCollectionText_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormCollectionText_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction

GenerateForm   Routine
  do LoadRelatedRecords
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
    Packet = clip(Packet) & p_web.DivHeader('Tab_FormCollectionText',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_FormCollectionText0_div')&'">'&p_web.Translate('Details')&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="FormCollectionText_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.cancel();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CancelButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="FormCollectionText_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'FormCollectionText_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="FormCollectionText_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'FormCollectionText_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.jbn:Collection_Text')
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_FormCollectionText')>0,p_web.GSV('showtab_FormCollectionText'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_FormCollectionText'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_FormCollectionText') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_FormCollectionText'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_FormCollectionText')>0,p_web.GSV('showtab_FormCollectionText'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_FormCollectionText') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Details') & ''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_FormCollectionText_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_FormCollectionText')>0,p_web.GSV('showtab_FormCollectionText'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"FormCollectionText",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_FormCollectionText')>0,p_web.GSV('showtab_FormCollectionText'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_FormCollectionText_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('FormCollectionText') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('FormCollectionText')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Details')&'</a></h3>' & CRLF & p_web.DivHeader('tab_FormCollectionText0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_FormCollectionText0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_FormCollectionText0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_FormCollectionText0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Details')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_FormCollectionText0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Details')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_FormCollectionText0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Details')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_FormCollectionText0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jbn:Collection_Text
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jbn:Collection_Text
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jbn:ColContatName
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jbn:ColContatName
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jbn:ColDepartment
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jbn:ColDepartment
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Prompt::jbn:Collection_Text  Routine
  packet = clip(packet) & p_web.DivHeader('FormCollectionText_' & p_web._nocolon('jbn:Collection_Text') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Collection Text'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jbn:Collection_Text  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jbn:Collection_Text = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s255
    jbn:Collection_Text = p_web.Dformat(p_web.GetValue('Value'),'@s255')
  End
  do ValidateValue::jbn:Collection_Text  ! copies value to session value if valid.
  do Value::jbn:Collection_Text
  do SendAlert

ValidateValue::jbn:Collection_Text  Routine
    If not (1=0)
    jbn:Collection_Text = Upper(jbn:Collection_Text)
      if loc:invalid = '' then p_web.SetSessionValue('jbn:Collection_Text',jbn:Collection_Text).
    End

Value::jbn:Collection_Text  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormCollectionText_' & p_web._nocolon('jbn:Collection_Text') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    jbn:Collection_Text = p_web.RestoreValue('jbn:Collection_Text')
    do ValidateValue::jbn:Collection_Text
    If jbn:Collection_Text:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- TEXT --- jbn:Collection_Text
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jbn:Collection_Text'',''formcollectiontext_jbn:collection_text_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'readonly','')
  do SendPacket
  p_web.CreateTextArea('jbn:Collection_Text',p_web.GetSessionValue('jbn:Collection_Text'),5,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(jbn:Collection_Text),,,,Net:Send)
  do SendPacket
  End
  p_web.DivFooter()

Prompt::jbn:ColContatName  Routine
  packet = clip(packet) & p_web.DivHeader('FormCollectionText_' & p_web._nocolon('jbn:ColContatName') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Contact Name'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jbn:ColContatName  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jbn:ColContatName = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    jbn:ColContatName = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::jbn:ColContatName  ! copies value to session value if valid.
  do Value::jbn:ColContatName
  do SendAlert

ValidateValue::jbn:ColContatName  Routine
    If not (1=0)
    jbn:ColContatName = Upper(jbn:ColContatName)
      if loc:invalid = '' then p_web.SetSessionValue('jbn:ColContatName',jbn:ColContatName).
    End

Value::jbn:ColContatName  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormCollectionText_' & p_web._nocolon('jbn:ColContatName') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    jbn:ColContatName = p_web.RestoreValue('jbn:ColContatName')
    do ValidateValue::jbn:ColContatName
    If jbn:ColContatName:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- jbn:ColContatName
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jbn:ColContatName'',''formcollectiontext_jbn:colcontatname_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jbn:ColContatName',p_web.GetSessionValueFormat('jbn:ColContatName'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'Contact Name',,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::jbn:ColDepartment  Routine
  packet = clip(packet) & p_web.DivHeader('FormCollectionText_' & p_web._nocolon('jbn:ColDepartment') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Department'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jbn:ColDepartment  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jbn:ColDepartment = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    jbn:ColDepartment = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::jbn:ColDepartment  ! copies value to session value if valid.
  do Value::jbn:ColDepartment
  do SendAlert

ValidateValue::jbn:ColDepartment  Routine
    If not (1=0)
    jbn:ColDepartment = Upper(jbn:ColDepartment)
      if loc:invalid = '' then p_web.SetSessionValue('jbn:ColDepartment',jbn:ColDepartment).
    End

Value::jbn:ColDepartment  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormCollectionText_' & p_web._nocolon('jbn:ColDepartment') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    jbn:ColDepartment = p_web.RestoreValue('jbn:ColDepartment')
    do ValidateValue::jbn:ColDepartment
    If jbn:ColDepartment:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- jbn:ColDepartment
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jbn:ColDepartment'',''formcollectiontext_jbn:coldepartment_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jbn:ColDepartment',p_web.GetSessionValueFormat('jbn:ColDepartment'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'Department',,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('FormCollectionText_nexttab_' & 0)
    jbn:Collection_Text = p_web.GSV('jbn:Collection_Text')
    do ValidateValue::jbn:Collection_Text
    If loc:Invalid
      loc:retrying = 1
      do Value::jbn:Collection_Text
      !do SendAlert
      !exit
    End
    jbn:ColContatName = p_web.GSV('jbn:ColContatName')
    do ValidateValue::jbn:ColContatName
    If loc:Invalid
      loc:retrying = 1
      do Value::jbn:ColContatName
      !do SendAlert
      !exit
    End
    jbn:ColDepartment = p_web.GSV('jbn:ColDepartment')
    do ValidateValue::jbn:ColDepartment
    If loc:Invalid
      loc:retrying = 1
      do Value::jbn:ColDepartment
      !do SendAlert
      !exit
    End
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_FormCollectionText_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('FormCollectionText_tab_' & 0)
    do GenerateTab0
  of lower('FormCollectionText_jbn:Collection_Text_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jbn:Collection_Text
      of event:timer
        do Value::jbn:Collection_Text
      else
        do Value::jbn:Collection_Text
      end
  of lower('FormCollectionText_jbn:ColContatName_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jbn:ColContatName
      of event:timer
        do Value::jbn:ColContatName
      else
        do Value::jbn:ColContatName
      end
  of lower('FormCollectionText_jbn:ColDepartment_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jbn:ColDepartment
      of event:timer
        do Value::jbn:ColDepartment
      else
        do Value::jbn:ColDepartment
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('FormCollectionText_form:ready_',1)

  p_web.SetSessionValue('FormCollectionText_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_FormCollectionText',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('FormCollectionText_form:ready_',1)
  p_web.SetSessionValue('FormCollectionText_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormCollectionText',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('FormCollectionText_form:ready_',1)
  p_web.SetSessionValue('FormCollectionText_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('FormCollectionText:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('FormCollectionText_form:ready_',1)
  p_web.SetSessionValue('FormCollectionText_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('FormCollectionText:Primed',0)
  p_web.setsessionvalue('showtab_FormCollectionText',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
      If not (1=0)
          If p_web.IfExistsValue('jbn:Collection_Text')
            jbn:Collection_Text = p_web.GetValue('jbn:Collection_Text')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('jbn:ColContatName')
            jbn:ColContatName = p_web.GetValue('jbn:ColContatName')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('jbn:ColDepartment')
            jbn:ColDepartment = p_web.GetValue('jbn:ColDepartment')
          End
      End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormCollectionText_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormCollectionText_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
    do ValidateValue::jbn:Collection_Text
    If loc:Invalid then exit.
    do ValidateValue::jbn:ColContatName
    If loc:Invalid then exit.
    do ValidateValue::jbn:ColDepartment
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('FormCollectionText:Primed',0)

FormDeliveryText     PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
FilesOpened     Long
JOBNOTES::State  USHORT
jbn:Delivery_Text:IsInvalid  Long
jbn:DelContactName:IsInvalid  Long
jbn:DelDepartment:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormDeliveryText')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'FormDeliveryText_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('FormDeliveryText','')
    p_web.DivHeader('FormDeliveryText',p_web.combine(p_web.site.style.formdiv,))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('FormDeliveryText') = 0
        p_web.AddPreCall('FormDeliveryText')
        p_web.DivHeader('popup_FormDeliveryText','nt-hidden')
        p_web.DivHeader('FormDeliveryText',p_web.combine(p_web.site.style.formdiv,))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(400)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_FormDeliveryText_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_FormDeliveryText_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormDeliveryText',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_FormDeliveryText',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormDeliveryText',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_FormDeliveryText',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormDeliveryText',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormDeliveryText',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_FormDeliveryText',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormDeliveryText',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_FormDeliveryText',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormDeliveryText',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormDeliveryText',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormDeliveryText',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('FormDeliveryText')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(JOBNOTES)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(JOBNOTES)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FormDeliveryText_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'FormDeliveryText'
    end
    p_web.formsettings.proc = 'FormDeliveryText'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('jbn:DelContactName')
    p_web.SetPicture('jbn:DelContactName','@s30')
  End
  p_web.SetSessionPicture('jbn:DelContactName','@s30')
  If p_web.IfExistsValue('jbn:DelDepartment')
    p_web.SetPicture('jbn:DelDepartment','@s30')
  End
  p_web.SetSessionPicture('jbn:DelDepartment','@s30')

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  if p_web.IfExistsValue('jbn:Delivery_Text') = 0
    p_web.SetSessionValue('jbn:Delivery_Text',jbn:Delivery_Text)
  Else
    jbn:Delivery_Text = p_web.GetSessionValue('jbn:Delivery_Text')
  End
  if p_web.IfExistsValue('jbn:DelContactName') = 0
    p_web.SetSessionValue('jbn:DelContactName',jbn:DelContactName)
  Else
    jbn:DelContactName = p_web.GetSessionValue('jbn:DelContactName')
  End
  if p_web.IfExistsValue('jbn:DelDepartment') = 0
    p_web.SetSessionValue('jbn:DelDepartment',jbn:DelDepartment)
  Else
    jbn:DelDepartment = p_web.GetSessionValue('jbn:DelDepartment')
  End

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('jbn:Delivery_Text')
    jbn:Delivery_Text = p_web.GetValue('jbn:Delivery_Text')
    p_web.SetSessionValue('jbn:Delivery_Text',jbn:Delivery_Text)
  Else
    jbn:Delivery_Text = p_web.GetSessionValue('jbn:Delivery_Text')
  End
  if p_web.IfExistsValue('jbn:DelContactName')
    jbn:DelContactName = p_web.GetValue('jbn:DelContactName')
    p_web.SetSessionValue('jbn:DelContactName',jbn:DelContactName)
  Else
    jbn:DelContactName = p_web.GetSessionValue('jbn:DelContactName')
  End
  if p_web.IfExistsValue('jbn:DelDepartment')
    jbn:DelDepartment = p_web.GetValue('jbn:DelDepartment')
    p_web.SetSessionValue('jbn:DelDepartment',jbn:DelDepartment)
  Else
    jbn:DelDepartment = p_web.GetSessionValue('jbn:DelDepartment')
  End

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('FormDeliveryText_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferFormDeliveryText')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormDeliveryText_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormDeliveryText_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormDeliveryText_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction

GenerateForm   Routine
  do LoadRelatedRecords
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
    Packet = clip(Packet) & p_web.DivHeader('Tab_FormDeliveryText',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_FormDeliveryText0_div')&'">'&p_web.Translate('Details')&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="FormDeliveryText_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.cancel();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CancelButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="FormDeliveryText_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'FormDeliveryText_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="FormDeliveryText_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'FormDeliveryText_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.jbn:Delivery_Text')
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_FormDeliveryText')>0,p_web.GSV('showtab_FormDeliveryText'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_FormDeliveryText'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_FormDeliveryText') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_FormDeliveryText'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_FormDeliveryText')>0,p_web.GSV('showtab_FormDeliveryText'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_FormDeliveryText') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Details') & ''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_FormDeliveryText_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_FormDeliveryText')>0,p_web.GSV('showtab_FormDeliveryText'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"FormDeliveryText",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_FormDeliveryText')>0,p_web.GSV('showtab_FormDeliveryText'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_FormDeliveryText_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('FormDeliveryText') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('FormDeliveryText')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Details')&'</a></h3>' & CRLF & p_web.DivHeader('tab_FormDeliveryText0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_FormDeliveryText0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_FormDeliveryText0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_FormDeliveryText0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Details')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_FormDeliveryText0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Details')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_FormDeliveryText0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Details')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_FormDeliveryText0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jbn:Delivery_Text
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jbn:Delivery_Text
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jbn:DelContactName
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jbn:DelContactName
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jbn:DelDepartment
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jbn:DelDepartment
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Prompt::jbn:Delivery_Text  Routine
  packet = clip(packet) & p_web.DivHeader('FormDeliveryText_' & p_web._nocolon('jbn:Delivery_Text') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Delivery Text'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jbn:Delivery_Text  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jbn:Delivery_Text = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s255
    jbn:Delivery_Text = p_web.Dformat(p_web.GetValue('Value'),'@s255')
  End
  do ValidateValue::jbn:Delivery_Text  ! copies value to session value if valid.
  do Value::jbn:Delivery_Text
  do SendAlert

ValidateValue::jbn:Delivery_Text  Routine
    If not (1=0)
    jbn:Delivery_Text = Upper(jbn:Delivery_Text)
      if loc:invalid = '' then p_web.SetSessionValue('jbn:Delivery_Text',jbn:Delivery_Text).
    End

Value::jbn:Delivery_Text  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormDeliveryText_' & p_web._nocolon('jbn:Delivery_Text') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    jbn:Delivery_Text = p_web.RestoreValue('jbn:Delivery_Text')
    do ValidateValue::jbn:Delivery_Text
    If jbn:Delivery_Text:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- TEXT --- jbn:Delivery_Text
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jbn:Delivery_Text'',''formdeliverytext_jbn:delivery_text_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'readonly','')
  do SendPacket
  p_web.CreateTextArea('jbn:Delivery_Text',p_web.GetSessionValue('jbn:Delivery_Text'),5,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(jbn:Delivery_Text),,,,Net:Send)
  do SendPacket
  End
  p_web.DivFooter()

Prompt::jbn:DelContactName  Routine
  packet = clip(packet) & p_web.DivHeader('FormDeliveryText_' & p_web._nocolon('jbn:DelContactName') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Contact Name'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jbn:DelContactName  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jbn:DelContactName = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    jbn:DelContactName = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::jbn:DelContactName  ! copies value to session value if valid.
  do Value::jbn:DelContactName
  do SendAlert

ValidateValue::jbn:DelContactName  Routine
    If not (1=0)
    jbn:DelContactName = Upper(jbn:DelContactName)
      if loc:invalid = '' then p_web.SetSessionValue('jbn:DelContactName',jbn:DelContactName).
    End

Value::jbn:DelContactName  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormDeliveryText_' & p_web._nocolon('jbn:DelContactName') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    jbn:DelContactName = p_web.RestoreValue('jbn:DelContactName')
    do ValidateValue::jbn:DelContactName
    If jbn:DelContactName:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- jbn:DelContactName
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jbn:DelContactName'',''formdeliverytext_jbn:delcontactname_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jbn:DelContactName',p_web.GetSessionValueFormat('jbn:DelContactName'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'Contact Name',,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::jbn:DelDepartment  Routine
  packet = clip(packet) & p_web.DivHeader('FormDeliveryText_' & p_web._nocolon('jbn:DelDepartment') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Department'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jbn:DelDepartment  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jbn:DelDepartment = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    jbn:DelDepartment = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::jbn:DelDepartment  ! copies value to session value if valid.
  do Value::jbn:DelDepartment
  do SendAlert

ValidateValue::jbn:DelDepartment  Routine
    If not (1=0)
    jbn:DelDepartment = Upper(jbn:DelDepartment)
      if loc:invalid = '' then p_web.SetSessionValue('jbn:DelDepartment',jbn:DelDepartment).
    End

Value::jbn:DelDepartment  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormDeliveryText_' & p_web._nocolon('jbn:DelDepartment') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    jbn:DelDepartment = p_web.RestoreValue('jbn:DelDepartment')
    do ValidateValue::jbn:DelDepartment
    If jbn:DelDepartment:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- jbn:DelDepartment
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jbn:DelDepartment'',''formdeliverytext_jbn:deldepartment_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jbn:DelDepartment',p_web.GetSessionValueFormat('jbn:DelDepartment'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'Department',,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('FormDeliveryText_nexttab_' & 0)
    jbn:Delivery_Text = p_web.GSV('jbn:Delivery_Text')
    do ValidateValue::jbn:Delivery_Text
    If loc:Invalid
      loc:retrying = 1
      do Value::jbn:Delivery_Text
      !do SendAlert
      !exit
    End
    jbn:DelContactName = p_web.GSV('jbn:DelContactName')
    do ValidateValue::jbn:DelContactName
    If loc:Invalid
      loc:retrying = 1
      do Value::jbn:DelContactName
      !do SendAlert
      !exit
    End
    jbn:DelDepartment = p_web.GSV('jbn:DelDepartment')
    do ValidateValue::jbn:DelDepartment
    If loc:Invalid
      loc:retrying = 1
      do Value::jbn:DelDepartment
      !do SendAlert
      !exit
    End
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_FormDeliveryText_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('FormDeliveryText_tab_' & 0)
    do GenerateTab0
  of lower('FormDeliveryText_jbn:Delivery_Text_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jbn:Delivery_Text
      of event:timer
        do Value::jbn:Delivery_Text
      else
        do Value::jbn:Delivery_Text
      end
  of lower('FormDeliveryText_jbn:DelContactName_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jbn:DelContactName
      of event:timer
        do Value::jbn:DelContactName
      else
        do Value::jbn:DelContactName
      end
  of lower('FormDeliveryText_jbn:DelDepartment_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jbn:DelDepartment
      of event:timer
        do Value::jbn:DelDepartment
      else
        do Value::jbn:DelDepartment
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('FormDeliveryText_form:ready_',1)

  p_web.SetSessionValue('FormDeliveryText_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_FormDeliveryText',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('FormDeliveryText_form:ready_',1)
  p_web.SetSessionValue('FormDeliveryText_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormDeliveryText',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('FormDeliveryText_form:ready_',1)
  p_web.SetSessionValue('FormDeliveryText_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('FormDeliveryText:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('FormDeliveryText_form:ready_',1)
  p_web.SetSessionValue('FormDeliveryText_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('FormDeliveryText:Primed',0)
  p_web.setsessionvalue('showtab_FormDeliveryText',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
      If not (1=0)
          If p_web.IfExistsValue('jbn:Delivery_Text')
            jbn:Delivery_Text = p_web.GetValue('jbn:Delivery_Text')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('jbn:DelContactName')
            jbn:DelContactName = p_web.GetValue('jbn:DelContactName')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('jbn:DelDepartment')
            jbn:DelDepartment = p_web.GetValue('jbn:DelDepartment')
          End
      End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormDeliveryText_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormDeliveryText_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
    do ValidateValue::jbn:Delivery_Text
    If loc:Invalid then exit.
    do ValidateValue::jbn:DelContactName
    If loc:Invalid then exit.
    do ValidateValue::jbn:DelDepartment
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('FormDeliveryText:Primed',0)

_BrowseIMEIHistory   PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
FilesOpened     Long
brwBrowseIMEIHistory:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  GlobalErrors.SetProcedureName('_BrowseIMEIHistory')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = '_BrowseIMEIHistory_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('_BrowseIMEIHistory','')
    p_web.DivHeader('_BrowseIMEIHistory',p_web.combine(p_web.site.style.formdiv,))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('_BrowseIMEIHistory') = 0
        p_web.AddPreCall('_BrowseIMEIHistory')
        p_web.DivHeader('popup__BrowseIMEIHistory','nt-hidden')
        p_web.DivHeader('_BrowseIMEIHistory',p_web.combine(p_web.site.style.formdiv,))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(850)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup__BrowseIMEIHistory_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup__BrowseIMEIHistory_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveRefer_BrowseIMEIHistory',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab__BrowseIMEIHistory',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveRefer_BrowseIMEIHistory',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab__BrowseIMEIHistory',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveRefer_BrowseIMEIHistory',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab__BrowseIMEIHistory',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab__BrowseIMEIHistory',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveRefer_BrowseIMEIHistory',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab__BrowseIMEIHistory',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveRefer_BrowseIMEIHistory',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab__BrowseIMEIHistory',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab__BrowseIMEIHistory',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('_BrowseIMEIHistory')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE
  loc:EipClm = upper(p_web.GetValue('_EIPClm'))
  p_web.DeleteValue('_EIPClm')
  case loc:EipClm
  of ''
    case upper(p_web.GetValue('_calledfrom_'))
    of upper('BrowseIMEIHistory')
      do Value::brwBrowseIMEIHistory
    end
  end

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('_BrowseIMEIHistory_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = '_BrowseIMEIHistory'
    end
    p_web.formsettings.proc = '_BrowseIMEIHistory'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('_BrowseIMEIHistory_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveRefer_BrowseIMEIHistory')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('_BrowseIMEIHistory_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('_BrowseIMEIHistory_ChainTo')
    loc:formaction = p_web.GetSessionValue('_BrowseIMEIHistory_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction

GenerateForm   Routine
  do LoadRelatedRecords
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
    Packet = clip(Packet) & p_web.DivHeader('Tab__BrowseIMEIHistory',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab__BrowseIMEIHistory0_div')&'">'&p_web.Translate()&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="_BrowseIMEIHistory_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('_BrowseIMEIHistory_BrowseIMEIHistory_embedded_div')&''');'
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.cancel();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CancelButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="_BrowseIMEIHistory_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & '_BrowseIMEIHistory_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="_BrowseIMEIHistory_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & '_BrowseIMEIHistory_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab__BrowseIMEIHistory')>0,p_web.GSV('showtab__BrowseIMEIHistory'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab__BrowseIMEIHistory'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab__BrowseIMEIHistory') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab__BrowseIMEIHistory'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab__BrowseIMEIHistory')>0,p_web.GSV('showtab__BrowseIMEIHistory'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab__BrowseIMEIHistory') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab__BrowseIMEIHistory_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab__BrowseIMEIHistory')>0,p_web.GSV('showtab__BrowseIMEIHistory'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"_BrowseIMEIHistory",' &|
                      'activeTab:' & choose(p_web.GSV('showtab__BrowseIMEIHistory')>0,p_web.GSV('showtab__BrowseIMEIHistory'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab__BrowseIMEIHistory_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('_BrowseIMEIHistory') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('_BrowseIMEIHistory')
    p_web.SetValue('_CallPopups',2)
    If p_web.GetPreCall('BrowseIMEIHistory') = 0
      p_web.SetValue('BrowseIMEIHistory:NoForm',1)
      p_web.SetValue('BrowseIMEIHistory:FormName',loc:formname)
      p_web.SetValue('BrowseIMEIHistory:parentIs','Form')
      p_web.SetValue('_parentProc','_BrowseIMEIHistory')
      BrowseIMEIHistory(p_web)
      p_web.SetValue('_CallPopups',0)
      p_web.DeleteValue('BrowseIMEIHistory:NoForm')
      p_web.DeleteValue('BrowseIMEIHistory:FormName')
      p_web.DeleteValue('BrowseIMEIHistory:parentIs')
      p_web.DeleteValue('_parentProc')
    End
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate()&'</a></h3>' & CRLF & p_web.DivHeader('tab__BrowseIMEIHistory0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab__BrowseIMEIHistory0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab__BrowseIMEIHistory0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab__BrowseIMEIHistory0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,)
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab__BrowseIMEIHistory0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend)
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab__BrowseIMEIHistory0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain">' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab__BrowseIMEIHistory0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::brwBrowseIMEIHistory
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Validate::brwBrowseIMEIHistory  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  Elsif true
    p_web.StoreValue('job_ali:Ref_Number')
  End
  do ValidateValue::brwBrowseIMEIHistory  ! copies value to session value if valid.

ValidateValue::brwBrowseIMEIHistory  Routine
    If not (1=0)
    End

Value::brwBrowseIMEIHistory  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(1=0,1,0))
  ! --- BROWSE ---  BrowseIMEIHistory --
  p_web.SetValue('BrowseIMEIHistory:NoForm',1)
  p_web.SetValue('BrowseIMEIHistory:FormName',loc:formname)
  p_web.SetValue('BrowseIMEIHistory:parentIs','Form')
  p_web.SetValue('_parentProc','_BrowseIMEIHistory')
  if p_web.RequestAjax = 0
    p_web.SSV('_BrowseIMEIHistory:_popup_',p_web.GetValue('_popup_')) ! stores the current browse popup state
    packet = clip(packet) & '<div id="'&lower('_BrowseIMEIHistory_BrowseIMEIHistory_embedded_div')&'"><!-- Net:BrowseIMEIHistory --></div><13,10>'
    do SendPacket
    p_web.DivHeader('_BrowseIMEIHistory_' & lower('BrowseIMEIHistory') & '_value')
    p_web.DivFooter()
  else
    if p_web.GetValue('_popup_') = 1
      p_web.SSV('_BrowseIMEIHistory:_popup_',1)
    elsif p_web.GSV('_BrowseIMEIHistory:_popup_') = 1
      p_web.SetValue('_popup_',1)
    end
    packet = clip(packet) & '<!-- Net:BrowseIMEIHistory --><13,10>'
  end
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('_BrowseIMEIHistory_nexttab_' & 0)
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab__BrowseIMEIHistory_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('_BrowseIMEIHistory_tab_' & 0)
    do GenerateTab0
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('_BrowseIMEIHistory_form:ready_',1)

  p_web.SetSessionValue('_BrowseIMEIHistory_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab__BrowseIMEIHistory',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('_BrowseIMEIHistory_form:ready_',1)
  p_web.SetSessionValue('_BrowseIMEIHistory_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab__BrowseIMEIHistory',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('_BrowseIMEIHistory_form:ready_',1)
  p_web.SetSessionValue('_BrowseIMEIHistory_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('_BrowseIMEIHistory:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('_BrowseIMEIHistory_form:ready_',1)
  p_web.SetSessionValue('_BrowseIMEIHistory_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('_BrowseIMEIHistory:Primed',0)
  p_web.setsessionvalue('showtab__BrowseIMEIHistory',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('_BrowseIMEIHistory_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('_BrowseIMEIHistory_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
    do ValidateValue::brwBrowseIMEIHistory
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('_BrowseIMEIHistory:Primed',0)
  p_web.StoreValue('')

DisplayJobEngineerDetails PROCEDURE  (NetWebServerWorker p_web,STRING EngineerUserCode,LONG JobNumber) ! Declare Procedure
FilesOpened     BYTE(0)
  CODE
        Do OpenFiles
    
        p_web.SSV('locCurrentEngineer','Not Allocated')
        p_web.SSV('locEngineerAllocated','')

        Access:USERS.Clearkey(use:User_Code_Key)
        use:User_Code = EngineerUserCode
        IF (Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign)
            p_web.SSV('locCurrentEngineer',CLIP(use:Forename) & ' ' & CLIP(use:Surname))
        
            Access:JOBSENG.Clearkey(joe:UserCodeKey)
            joe:JobNumber = JobNumber
            joe:UserCode = use:User_Code
            joe:DateAllocated = TODAY()
            SET(joe:UserCodeKey,joe:UserCodeKey)
            LOOP UNTIL Access:JOBSENG.Next()
                IF (joe:JobNumber <> JobNumber OR |
                    joe:UserCode <> use:User_Code)
                    BREAK
                END ! IF
                
                p_web.SSV('locEngineerAllocated',FORMAT(joe:DateAllocated,@d06b) & ' (Eng Level: ' & joe:EngSKillLevel & ')')
                BREAK
            END ! LOOP
        END ! IF
        
        Do CloseFiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:USERS.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:USERS.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSENG.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSENG.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:USERS.Close
     Access:JOBSENG.Close
     FilesOpened = False
  END
