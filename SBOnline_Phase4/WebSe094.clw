

   MEMBER('WebServer_Phase4.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSE094.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSE063.INC'),ONCE        !Req'd for module callout resolution
                     END


FormPayments         PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
FilesOpened     Long
JOBPAYMT_ALIAS::State  USHORT
PAYTYPES::State  USHORT
JOBPAYMT::State  USHORT
jpt:Date:IsInvalid  Long
jpt:Payment_Type:IsInvalid  Long
jpt:Credit_Card_Number:IsInvalid  Long
jpt:Expiry_Date:IsInvalid  Long
jpt:Issue_Number:IsInvalid  Long
jpt:Amount:IsInvalid  Long
buttonViewCosts:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
jpt:Payment_Type_OptionView   View(PAYTYPES)
                          Project(pay:Payment_Type)
                        End
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormPayments')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'FormPayments_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('FormPayments','')
    p_web.DivHeader('FormPayments',p_web.combine(p_web.site.style.formdiv,))
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('FormPayments') = 0
        p_web.AddPreCall('FormPayments')
        p_web.DivHeader('popup_FormPayments','nt-hidden')
        p_web.DivHeader('FormPayments',p_web.combine(p_web.site.style.formdiv,))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_FormPayments_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_FormPayments_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormPayments',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:StagePost
    do RestoreMem
    do PostInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_FormPayments',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormPayments',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:StagePost
    do RestoreMem
    do PostCopy
  of Net:CopyRecord + NET:WEB:Populate
    If p_web.IfExistsValue('jpt:Record_Number') = 0 then p_web.SetValue('jpt:Record_Number',p_web.GSV('jpt:Record_Number')).
    do PreCopy
    p_web.setsessionvalue('showtab_FormPayments',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormPayments',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormPayments',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do PostInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do PostUpdate
    End
  of Net:ChangeRecord + NET:WEB:Populate
    If p_web.IfExistsValue('jpt:Record_Number') = 0 then p_web.SetValue('jpt:Record_Number',p_web.GSV('jpt:Record_Number')).
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_FormPayments',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormPayments',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:DeleteRecord + NET:WEB:StagePost
    do RestoreMem
    do PostDelete
  of Net:ViewRecord + NET:WEB:Populate
    If p_web.IfExistsValue('jpt:Record_Number') = 0 then p_web.SetValue('jpt:Record_Number',p_web.GSV('jpt:Record_Number')).
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_FormPayments',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormPayments',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormPayments',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormPayments',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('FormPayments')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
DeleteSessionValues ROUTINE
    p_web.DeleteSessionValue('PassedType')
CreditCardBit       ROUTINE

    p_web.SSV('Hide:CreditCard',1)
! #12274 Don't want to see credit card bits (Bryan: 05/09/2011)
!    IF (p_web.GSV('jpt:Payment_Type') <> '')
!        Access:PAYTYPES.ClearKey(pay:Payment_Type_Key)
!        pay:Payment_Type = p_web.GSV('jpt:Payment_Type')
!        IF (Access:PAYTYPES.TryFetch(pay:Payment_Type_Key) = Level:Benign)
!            IF (pay:Credit_Card = 'YES')
!                p_web.SSV('Hide:CreditCard',0)
!            ELSE
!                p_web.SSV('Hide:CreditCard',1)
!            END
!        END
!    END

OpenFiles  ROUTINE
  p_web._OpenFile(JOBPAYMT_ALIAS)
  p_web._OpenFile(PAYTYPES)
  p_web._OpenFile(JOBPAYMT)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(JOBPAYMT_ALIAS)
  p_Web._CloseFile(PAYTYPES)
  p_Web._CloseFile(JOBPAYMT)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SSV('Title:JobPayments','')
  IF (p_web.IfExistsValue('PassedType'))
      p_web.SToreValue('PassedType')
      p_web.SSV('Title:JobPayments',': ' & p_web.GSV('PassedType'))
  END
  
  p_web.SetValue('FormPayments_form:inited_',1)
  p_web.formsettings.file = 'JOBPAYMT'
  p_web.formsettings.key = 'jpt:Record_Number_Key'
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = 'JOBPAYMT'
    p_web.formsettings.key = 'jpt:Record_Number_Key'
      clear(p_web.formsettings.FieldName)
    p_web.formsettings.recordid[1] = jpt:Record_Number
    p_web.formsettings.FieldName[1] = 'jpt:Record_Number'
    do SetAction
    if p_web.GetSessionValue('FormPayments:Primed') = 1
      p_web.formsettings.action = Net:ChangeRecord
    Else
      p_web.formsettings.action = Loc:Act
    End
    p_web.formsettings.OriginalAction = Loc:Act
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'FormPayments'
    end
    p_web.formsettings.proc = 'FormPayments'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine
  Do DeleteSessionValues
  IF p_web.GetSessionValue('FormPayments:Primed') = 1
    p_web._deleteFile(JOBPAYMT)
    p_web.SetSessionValue('FormPayments:Primed',0)
  End

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine
  p_web.SetValue('UpdateFile','JOBPAYMT')
  p_web.SetValue('UpdateKey','jpt:Record_Number_Key')
  If p_web.IfExistsValue('jpt:Date')
    p_web.SetPicture('jpt:Date','@d06b')
  End
  p_web.SetSessionPicture('jpt:Date','@d06b')
  If p_web.IfExistsValue('jpt:Credit_Card_Number')
    p_web.SetPicture('jpt:Credit_Card_Number','@s20')
  End
  p_web.SetSessionPicture('jpt:Credit_Card_Number','@s20')
  If p_web.IfExistsValue('jpt:Expiry_Date')
    p_web.SetPicture('jpt:Expiry_Date','@p##/##p')
  End
  p_web.SetSessionPicture('jpt:Expiry_Date','@p##/##p')
  If p_web.IfExistsValue('jpt:Issue_Number')
    p_web.SetPicture('jpt:Issue_Number','@s5')
  End
  p_web.SetSessionPicture('jpt:Issue_Number','@s5')
  If p_web.IfExistsValue('jpt:Amount')
    p_web.SetPicture('jpt:Amount','@n-14.2')
  End
  p_web.SetSessionPicture('jpt:Amount','@n-14.2')

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=File

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('FormPayments_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'DisplayBrowsePayments'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormPayments_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormPayments_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormPayments_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'DisplayBrowsePayments'

GenerateForm   Routine
  do LoadRelatedRecords
  DO CreditCardBit
  
  
  
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Job Payments' & p_web.GSV('Title:JobPayments')) <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Job Payments' & p_web.GSV('Title:JobPayments'),0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_FormPayments',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_FormPayments0_div')&'">'&p_web.Translate('General')&'</a></li>'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_FormPayments1_div')&'">'&p_web.Translate()&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  do GenerateTab1
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="FormPayments_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.cancel();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CancelButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="FormPayments_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'FormPayments_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="FormPayments_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'FormPayments_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.jpt:Date')
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_FormPayments')>0,p_web.GSV('showtab_FormPayments'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_FormPayments'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_FormPayments') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_FormPayments'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_FormPayments')>0,p_web.GSV('showtab_FormPayments'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_FormPayments') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('General') & ''''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_FormPayments_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_FormPayments')>0,p_web.GSV('showtab_FormPayments'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"FormPayments",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_FormPayments')>0,p_web.GSV('showtab_FormPayments'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_FormPayments_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('FormPayments') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('FormPayments')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('General')&'</a></h3>' & CRLF & p_web.DivHeader('tab_FormPayments0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_FormPayments0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_FormPayments0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_FormPayments0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'General')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_FormPayments0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('General')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_FormPayments0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('General')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_FormPayments0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jpt:Date
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jpt:Date
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::jpt:Date
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jpt:Payment_Type
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jpt:Payment_Type
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::jpt:Payment_Type
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jpt:Credit_Card_Number
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jpt:Credit_Card_Number
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::jpt:Credit_Card_Number
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jpt:Expiry_Date
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jpt:Expiry_Date
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::jpt:Expiry_Date
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jpt:Issue_Number
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jpt:Issue_Number
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::jpt:Issue_Number
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jpt:Amount
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jpt:Amount
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::jpt:Amount
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab1  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate()&'</a></h3>' & CRLF & p_web.DivHeader('tab_FormPayments1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_FormPayments1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_FormPayments1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_FormPayments1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,)
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_FormPayments1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend)
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_FormPayments1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain">' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_FormPayments1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::buttonViewCosts
        do Comment::buttonViewCosts
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Prompt::jpt:Date  Routine
  packet = clip(packet) & p_web.DivHeader('FormPayments_' & p_web._nocolon('jpt:Date') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Date'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jpt:Date  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jpt:Date = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@d06b'  !FieldPicture = @d6b
    jpt:Date = p_web.Dformat(p_web.GetValue('Value'),'@d06b')
  End
  do ValidateValue::jpt:Date  ! copies value to session value if valid.
  do Value::jpt:Date
  do SendAlert
  do Comment::jpt:Date ! allows comment style to be updated.

ValidateValue::jpt:Date  Routine
    If not (1=0)
  If jpt:Date = ''
    loc:Invalid = 'jpt:Date'
    jpt:Date:IsInvalid = true
    loc:alert = p_web.translate('Date') & ' ' & p_web.site.RequiredText
  End
    jpt:Date = Upper(jpt:Date)
      if loc:invalid = '' then p_web.SetSessionValue('jpt:Date',jpt:Date).
    End

Value::jpt:Date  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormPayments_' & p_web._nocolon('jpt:Date') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  If loc:retrying
    jpt:Date = p_web.RestoreValue('jpt:Date')
    do ValidateValue::jpt:Date
    If jpt:Date:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- jpt:Date
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jpt:Date'',''formpayments_jpt:date_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jpt:Date',p_web.GetSessionValue('jpt:Date'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@d06b',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  !Handcode Date Lookup Button
      packet = clip(packet) & '<button id="lookup_btn" class="LookupButton" onclick="displayCalendar(jpt__Date,''dd/mm/yyyy'',this); Date.disabled=true;sv(''...'',''formpayments_pickdate_value'',1,FieldValue(this,1));nextFocus(FormPayments_frm,'''',0);" value="Select Date" name="Date" type="button">...</button>'
      do SendPacket
  End
  p_web.DivFooter()
Comment::jpt:Date  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if jpt:Date:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web._jsok(p_web.site.RequiredText)
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormPayments_' & p_web._nocolon('jpt:Date') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::jpt:Payment_Type  Routine
  packet = clip(packet) & p_web.DivHeader('FormPayments_' & p_web._nocolon('jpt:Payment_Type') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Payment Type'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jpt:Payment_Type  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jpt:Payment_Type = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    jpt:Payment_Type = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::jpt:Payment_Type  ! copies value to session value if valid.
  DO CreditCardBit
  do Value::jpt:Payment_Type
  do SendAlert
  do Comment::jpt:Payment_Type ! allows comment style to be updated.
  do Prompt::jpt:Credit_Card_Number
  do Value::jpt:Credit_Card_Number  !1
  do Comment::jpt:Credit_Card_Number
  do Prompt::jpt:Expiry_Date
  do Value::jpt:Expiry_Date  !1
  do Comment::jpt:Expiry_Date
  do Prompt::jpt:Issue_Number
  do Value::jpt:Issue_Number  !1
  do Comment::jpt:Issue_Number

ValidateValue::jpt:Payment_Type  Routine
    If not (1=0)
  If jpt:Payment_Type = ''
    loc:Invalid = 'jpt:Payment_Type'
    jpt:Payment_Type:IsInvalid = true
    loc:alert = p_web.translate('Payment Type') & ' ' & p_web.site.RequiredText
  End
      if loc:invalid = '' then p_web.SetSessionValue('jpt:Payment_Type',jpt:Payment_Type).
    End

Value::jpt:Payment_Type  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormPayments_' & p_web._nocolon('jpt:Payment_Type') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,p_web.site.style.formselect,,)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  If loc:retrying
    jpt:Payment_Type = p_web.RestoreValue('jpt:Payment_Type')
    do ValidateValue::jpt:Payment_Type
    If jpt:Payment_Type:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- DROPLIST ---
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jpt:Payment_Type'',''formpayments_jpt:payment_type_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('jpt:Payment_Type')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('jpt:Payment_Type',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
  loc:fieldclass = ''
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('jpt:Payment_Type') = 0
    p_web.SetSessionValue('jpt:Payment_Type','')
  end
    packet = clip(packet) & p_web.CreateOption('-------------------------------------','',choose('' = p_web.getsessionvalue('jpt:Payment_Type')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(JOBPAYMT_ALIAS)
  bind(jpt_ali:Record)
  p_web._OpenFile(PAYTYPES)
  bind(pay:Record)
  p_web._OpenFile(JOBPAYMT)
  bind(jpt:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(jpt:Payment_Type_OptionView)
  jpt:Payment_Type_OptionView{prop:order} = p_web.CleanFilter(jpt:Payment_Type_OptionView,'UPPER(pay:Payment_Type)')
  Set(jpt:Payment_Type_OptionView)
  Loop
    Next(jpt:Payment_Type_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('jpt:Payment_Type') = 0
      p_web.SetSessionValue('jpt:Payment_Type',pay:Payment_Type)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,pay:Payment_Type,choose(pay:Payment_Type = p_web.getsessionvalue('jpt:Payment_Type')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(jpt:Payment_Type_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(JOBPAYMT_ALIAS)
  p_Web._CloseFile(PAYTYPES)
  p_Web._CloseFile(JOBPAYMT)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web.DivFooter()
Comment::jpt:Payment_Type  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if jpt:Payment_Type:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormPayments_' & p_web._nocolon('jpt:Payment_Type') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::jpt:Credit_Card_Number  Routine
  packet = clip(packet) & p_web.DivHeader('FormPayments_' & p_web._nocolon('jpt:Credit_Card_Number') & '_prompt',Choose(p_web.GSV('Hide:CreditCard') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:CreditCard') = 1,'',p_web.Translate('Credit Card Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jpt:Credit_Card_Number  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jpt:Credit_Card_Number = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s20
    jpt:Credit_Card_Number = p_web.Dformat(p_web.GetValue('Value'),'@s20')
  End
  do ValidateValue::jpt:Credit_Card_Number  ! copies value to session value if valid.
  do Value::jpt:Credit_Card_Number
  do SendAlert
  do Comment::jpt:Credit_Card_Number ! allows comment style to be updated.

ValidateValue::jpt:Credit_Card_Number  Routine
    If not (p_web.GSV('Hide:CreditCard') = 1)
  If jpt:Credit_Card_Number = ''
    loc:Invalid = 'jpt:Credit_Card_Number'
    jpt:Credit_Card_Number:IsInvalid = true
    loc:alert = p_web.translate('Credit Card Number') & ' ' & p_web.site.RequiredText
  End
    jpt:Credit_Card_Number = Upper(jpt:Credit_Card_Number)
      if loc:invalid = '' then p_web.SetSessionValue('jpt:Credit_Card_Number',jpt:Credit_Card_Number).
    End

Value::jpt:Credit_Card_Number  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:CreditCard') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormPayments_' & p_web._nocolon('jpt:Credit_Card_Number') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  If loc:retrying
    jpt:Credit_Card_Number = p_web.RestoreValue('jpt:Credit_Card_Number')
    do ValidateValue::jpt:Credit_Card_Number
    If jpt:Credit_Card_Number:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:CreditCard') = 1)
  ! --- STRING --- jpt:Credit_Card_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jpt:Credit_Card_Number'',''formpayments_jpt:credit_card_number_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jpt:Credit_Card_Number',p_web.GetSessionValueFormat('jpt:Credit_Card_Number'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s20'),,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::jpt:Credit_Card_Number  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if jpt:Credit_Card_Number:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web._jsok(p_web.site.RequiredText)
  loc:class = Choose(p_web.GSV('Hide:CreditCard') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormPayments_' & p_web._nocolon('jpt:Credit_Card_Number') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:CreditCard') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::jpt:Expiry_Date  Routine
  packet = clip(packet) & p_web.DivHeader('FormPayments_' & p_web._nocolon('jpt:Expiry_Date') & '_prompt',Choose(p_web.GSV('Hide:CreditCard') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:CreditCard') = 1,'',p_web.Translate('Expiry Date'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jpt:Expiry_Date  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jpt:Expiry_Date = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@p##/##p'  !FieldPicture = @p##/##p
    jpt:Expiry_Date = p_web.Dformat(p_web.GetValue('Value'),'@p##/##p')
  End
  do ValidateValue::jpt:Expiry_Date  ! copies value to session value if valid.
  do Value::jpt:Expiry_Date
  do SendAlert
  do Comment::jpt:Expiry_Date ! allows comment style to be updated.

ValidateValue::jpt:Expiry_Date  Routine
    If not (p_web.GSV('Hide:CreditCard') = 1)
  If jpt:Expiry_Date = ''
    loc:Invalid = 'jpt:Expiry_Date'
    jpt:Expiry_Date:IsInvalid = true
    loc:alert = p_web.translate('Expiry Date') & ' ' & p_web.site.RequiredText
  End
      if loc:invalid = '' then p_web.SetSessionValue('jpt:Expiry_Date',jpt:Expiry_Date).
    End

Value::jpt:Expiry_Date  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:CreditCard') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormPayments_' & p_web._nocolon('jpt:Expiry_Date') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  If loc:retrying
    jpt:Expiry_Date = p_web.RestoreValue('jpt:Expiry_Date')
    do ValidateValue::jpt:Expiry_Date
    If jpt:Expiry_Date:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:CreditCard') = 1)
  ! --- STRING --- jpt:Expiry_Date
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jpt:Expiry_Date'',''formpayments_jpt:expiry_date_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jpt:Expiry_Date',p_web.GetSessionValue('jpt:Expiry_Date'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@p##/##p',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::jpt:Expiry_Date  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if jpt:Expiry_Date:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web._jsok(p_web.site.RequiredText)
  loc:class = Choose(p_web.GSV('Hide:CreditCard') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormPayments_' & p_web._nocolon('jpt:Expiry_Date') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:CreditCard') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::jpt:Issue_Number  Routine
  packet = clip(packet) & p_web.DivHeader('FormPayments_' & p_web._nocolon('jpt:Issue_Number') & '_prompt',Choose(p_web.GSV('Hide:CreditCard') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:CreditCard') = 1,'',p_web.Translate('Issue Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jpt:Issue_Number  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jpt:Issue_Number = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s5
    jpt:Issue_Number = p_web.Dformat(p_web.GetValue('Value'),'@s5')
  End
  do ValidateValue::jpt:Issue_Number  ! copies value to session value if valid.
  do Value::jpt:Issue_Number
  do SendAlert
  do Comment::jpt:Issue_Number ! allows comment style to be updated.

ValidateValue::jpt:Issue_Number  Routine
    If not (p_web.GSV('Hide:CreditCard') = 1)
      if loc:invalid = '' then p_web.SetSessionValue('jpt:Issue_Number',jpt:Issue_Number).
    End

Value::jpt:Issue_Number  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:CreditCard') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormPayments_' & p_web._nocolon('jpt:Issue_Number') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    jpt:Issue_Number = p_web.RestoreValue('jpt:Issue_Number')
    do ValidateValue::jpt:Issue_Number
    If jpt:Issue_Number:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:CreditCard') = 1)
  ! --- STRING --- jpt:Issue_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jpt:Issue_Number'',''formpayments_jpt:issue_number_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jpt:Issue_Number',p_web.GetSessionValueFormat('jpt:Issue_Number'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s5'),,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::jpt:Issue_Number  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if jpt:Issue_Number:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:CreditCard') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormPayments_' & p_web._nocolon('jpt:Issue_Number') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:CreditCard') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::jpt:Amount  Routine
  packet = clip(packet) & p_web.DivHeader('FormPayments_' & p_web._nocolon('jpt:Amount') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Payment'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jpt:Amount  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jpt:Amount = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n-14.2'  !FieldPicture = @n-14.2
    jpt:Amount = p_web.Dformat(p_web.GetValue('Value'),'@n-14.2')
  End
  do ValidateValue::jpt:Amount  ! copies value to session value if valid.
  do Value::jpt:Amount
  do SendAlert
  do Comment::jpt:Amount ! allows comment style to be updated.

ValidateValue::jpt:Amount  Routine
    If not (1=0)
  If jpt:Amount = ''
    loc:Invalid = 'jpt:Amount'
    jpt:Amount:IsInvalid = true
    loc:alert = p_web.translate('Payment') & ' ' & p_web.site.RequiredText
  End
      if loc:invalid = '' then p_web.SetSessionValue('jpt:Amount',jpt:Amount).
    End

Value::jpt:Amount  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormPayments_' & p_web._nocolon('jpt:Amount') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  If loc:retrying
    jpt:Amount = p_web.RestoreValue('jpt:Amount')
    do ValidateValue::jpt:Amount
    If jpt:Amount:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- jpt:Amount
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jpt:Amount'',''formpayments_jpt:amount_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jpt:Amount',p_web.GetSessionValue('jpt:Amount'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n-14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::jpt:Amount  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if jpt:Amount:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web._jsok(p_web.site.RequiredText)
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormPayments_' & p_web._nocolon('jpt:Amount') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonViewCosts  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonViewCosts  ! copies value to session value if valid.
  do Comment::buttonViewCosts ! allows comment style to be updated.

ValidateValue::buttonViewCosts  Routine
    If not (1=0)
    End

Value::buttonViewCosts  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormPayments_' & p_web._nocolon('buttonViewCosts') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','ViewCosts','Job Costs',p_web.combine(Choose('Job Costs' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.WindowOpen(clip('ViewCosts?ViewCostsReturnURL=FormPayments')&''&'','_self'),,loc:disabled,'images/moneys.png',,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonViewCosts  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonViewCosts:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormPayments_' & p_web._nocolon('buttonViewCosts') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('FormPayments_nexttab_' & 0)
    jpt:Date = p_web.GSV('jpt:Date')
    do ValidateValue::jpt:Date
    If loc:Invalid
      loc:retrying = 1
      do Value::jpt:Date
      !do SendAlert
      do Comment::jpt:Date ! allows comment style to be updated.
      !exit
    End
    jpt:Payment_Type = p_web.GSV('jpt:Payment_Type')
    do ValidateValue::jpt:Payment_Type
    If loc:Invalid
      loc:retrying = 1
      do Value::jpt:Payment_Type
      !do SendAlert
      do Comment::jpt:Payment_Type ! allows comment style to be updated.
      !exit
    End
    jpt:Credit_Card_Number = p_web.GSV('jpt:Credit_Card_Number')
    do ValidateValue::jpt:Credit_Card_Number
    If loc:Invalid
      loc:retrying = 1
      do Value::jpt:Credit_Card_Number
      !do SendAlert
      do Comment::jpt:Credit_Card_Number ! allows comment style to be updated.
      !exit
    End
    jpt:Expiry_Date = p_web.GSV('jpt:Expiry_Date')
    do ValidateValue::jpt:Expiry_Date
    If loc:Invalid
      loc:retrying = 1
      do Value::jpt:Expiry_Date
      !do SendAlert
      do Comment::jpt:Expiry_Date ! allows comment style to be updated.
      !exit
    End
    jpt:Issue_Number = p_web.GSV('jpt:Issue_Number')
    do ValidateValue::jpt:Issue_Number
    If loc:Invalid
      loc:retrying = 1
      do Value::jpt:Issue_Number
      !do SendAlert
      do Comment::jpt:Issue_Number ! allows comment style to be updated.
      !exit
    End
    jpt:Amount = p_web.GSV('jpt:Amount')
    do ValidateValue::jpt:Amount
    If loc:Invalid
      loc:retrying = 1
      do Value::jpt:Amount
      !do SendAlert
      do Comment::jpt:Amount ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('FormPayments_nexttab_' & 1)
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_FormPayments_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('FormPayments_tab_' & 0)
    do GenerateTab0
  of lower('FormPayments_jpt:Date_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jpt:Date
      of event:timer
        do Value::jpt:Date
        do Comment::jpt:Date
      else
        do Value::jpt:Date
      end
  of lower('FormPayments_jpt:Payment_Type_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jpt:Payment_Type
      of event:timer
        do Value::jpt:Payment_Type
        do Comment::jpt:Payment_Type
      else
        do Value::jpt:Payment_Type
      end
  of lower('FormPayments_jpt:Credit_Card_Number_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jpt:Credit_Card_Number
      of event:timer
        do Value::jpt:Credit_Card_Number
        do Comment::jpt:Credit_Card_Number
      else
        do Value::jpt:Credit_Card_Number
      end
  of lower('FormPayments_jpt:Expiry_Date_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jpt:Expiry_Date
      of event:timer
        do Value::jpt:Expiry_Date
        do Comment::jpt:Expiry_Date
      else
        do Value::jpt:Expiry_Date
      end
  of lower('FormPayments_jpt:Issue_Number_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jpt:Issue_Number
      of event:timer
        do Value::jpt:Issue_Number
        do Comment::jpt:Issue_Number
      else
        do Value::jpt:Issue_Number
      end
  of lower('FormPayments_jpt:Amount_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jpt:Amount
      of event:timer
        do Value::jpt:Amount
        do Comment::jpt:Amount
      else
        do Value::jpt:Amount
      end
  of lower('FormPayments_tab_' & 1)
    do GenerateTab1
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('FormPayments_form:ready_',1)

  p_web.SetSessionValue('FormPayments_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_FormPayments',0)
  jpt:Date = Today()
  p_web.SetSessionValue('jpt:Date',jpt:Date)
  jpt:Loan_Deposit = 0
  p_web.SetSessionValue('jpt:Loan_Deposit',jpt:Loan_Deposit)
  jpt:Ref_Number = p_web.GSV('job:Ref_Number')
  p_web.SetSessionValue('jpt:Ref_Number',jpt:Ref_Number)
  jpt:Date = Today()
  p_web.SetSessionValue('jpt:Date',jpt:Date)
  jpt:User_Code = p_web.GSV('BookingUserCode')
  p_web.SetSessionValue('jpt:User_Code',jpt:User_Code)
  IF (p_web.GSV('PassedType') = 'REFUND')
      jpt:Amount = 0
      Access:JOBPAYMT_ALIAS.ClearKey(jpt_ali:Loan_Deposit_Key)
      jpt_ali:Ref_Number = p_web.GSV('locSearchJobNumber')
      jpt_ali:Loan_Deposit = 1
      SET(jpt_ali:Loan_Deposit_Key,jpt_ali:Loan_Deposit_Key)
      LOOP UNTIL Access:JOBPAYMT_ALIAS.Next()
          IF (jpt_ali:Ref_Number <> p_web.GSV('locSearchJobNumber'))
              BREAK
          END
          IF (jpt_ali:Loan_Deposit <> 1)
              BREAK
          END
          jpt:Amount += jpt_ali:Amount
      END
  ELSIF (p_web.GSV('PassedType') = 'DEPOSIT')
      jpt:Amount = 0
  ELSE
      invoiced# = 0
      IF (p_web.GSV('job:Invoice_Number') > 0)
          Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
          inv:Invoice_Number = p_web.GSV('job:Invoice_Number')
          IF (Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign)
              IF (inv:RRCInvoiceDate <> '')
                  invoiced# = 1
              END
          END
      END
      IF (invoiced#)
          p_web.SSV('TotalPrice:Type','I')
      ELSE
          p_web.SSV('TotalPrice:Type','C')
      END
      TotalPrice(p_web)
      jpt:Amount = p_web.GSV('TotalPrice:Balance')
  END
  
  ! p_web.SSV('jpt:Amount',jpt:Amount)
  
  
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('FormPayments_form:ready_',1)
  p_web.SetSessionValue('FormPayments_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormPayments',0)
  p_web._PreCopyRecord(JOBPAYMT,jpt:Record_Number_Key)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('FormPayments_form:ready_',1)
  p_web.SetSessionValue('FormPayments_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('FormPayments:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('FormPayments_form:ready_',1)
  ! Delete: Add To Audit
  p_web.SSV('AddToAudit:Type','JOB')
  p_web.SSV('AddToAudit:Action','PAYMENT DETAIL DELETED')
  p_web.SSV('AddToAudit:Notes','PAYMENT AMOUNT: ' & Format(jpt:Amount,@n14.2) &|
      '<13,10>PAYMENT TYPE: ' & Clip(jpt:Payment_Type))
  IF (jpt:Credit_Card_Number <> '')
      p_web.SSV('AddToAudit:Notes',p_web.GSV('AddToAudit:Notes') & '<13,10>CREDIT CARD NO: ' & Clip(jpt:Credit_Card_Number) & |
          '<13,10>EXPIRY DATE: ' & Clip(jpt:Expiry_Date) & |
          '<13,10>ISSUE NO: ' & Clip(jpt:Issue_Number))
  END
  AddToAudit(p_web)
  p_web.SetSessionValue('FormPayments_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('FormPayments:Primed',0)
  p_web.setsessionvalue('showtab_FormPayments',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
      If not (1=0)
          If p_web.IfExistsValue('jpt:Date')
            jpt:Date = p_web.GetValue('jpt:Date')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('jpt:Payment_Type')
            jpt:Payment_Type = p_web.GetValue('jpt:Payment_Type')
          End
      End
      If not (p_web.GSV('Hide:CreditCard') = 1)
          If p_web.IfExistsValue('jpt:Credit_Card_Number')
            jpt:Credit_Card_Number = p_web.GetValue('jpt:Credit_Card_Number')
          End
      End
      If not (p_web.GSV('Hide:CreditCard') = 1)
          If p_web.IfExistsValue('jpt:Expiry_Date')
            jpt:Expiry_Date = p_web.GetValue('jpt:Expiry_Date')
          End
      End
      If not (p_web.GSV('Hide:CreditCard') = 1)
          If p_web.IfExistsValue('jpt:Issue_Number')
            jpt:Issue_Number = p_web.GetValue('jpt:Issue_Number')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('jpt:Amount')
            jpt:Amount = p_web.GetValue('jpt:Amount')
          End
      End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  CASE p_web.GSV('PassedType')
  OF 'DEPOSIT'
      p_web.SSV('AddToAudit:Action','LOAN DESPOSIT')
  OF 'REFUND'
      IF (jpt:Amount > 0)
          jpt:Amount = -(jpt:Amount)
      END
  
      IF (Access:CONTHIST.PrimeRecord() = Level:Benign)
          cht:Ref_Number = p_web.GSV('job:Ref_Number')
          cht:Date = TODAY()
          cht:Time = CLOCK()
          cht:User = p_web.GSV('BookingUsercode')
          cht:Action = 'REFUND MADE ON LOAN UNIT'
          cht:Notes = 'AMOUNT TAKEN: ' & FORMAT(jpt:Amount,@n14.2) & |
              '<13,10>PAYMENT TYPE: ' & CLIP(jpt:Payment_Type) & '<13,10>'
          IF (Access:CONTHIST.TryUpdate())
          END
  
      END
      p_web.SSV('AddToAudit:Action','REFUND ISSED')
  ELSE
      p_web.SSV('AddToAudit:Action','PAYMENT RECEIVED')
  END
  
  p_web.SSV('AddToAudit:Type','JOB')
  p_web.SSV('AddToAudit:Notes','PAYMENT TYPE: ' & clip(jpt:Payment_Type) & |
      '<13,10>AMOUNT: ' & FORMAT(jpt:Amount,@n14.2))
  AddToAudit(p_web)
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormPayments_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  IF (p_web.GSV('PassedType') = '')
      IF (p_web.GSV('jpt:Amount') > p_web.GSV('TotalPrice:Balance'))
          loc:Invalid = 'jpt:Amount'
          loc:Alert = 'This amount exceeds the current outstanding balance.'
          EXIT
      END
  END
  Do DeleteSessionValues
  p_web.DeleteSessionValue('FormPayments_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
    loc:InvalidTab += 1
    do ValidateValue::jpt:Date
    If loc:Invalid then exit.
    do ValidateValue::jpt:Payment_Type
    If loc:Invalid then exit.
    do ValidateValue::jpt:Credit_Card_Number
    If loc:Invalid then exit.
    do ValidateValue::jpt:Expiry_Date
    If loc:Invalid then exit.
    do ValidateValue::jpt:Issue_Number
    If loc:Invalid then exit.
    do ValidateValue::jpt:Amount
    If loc:Invalid then exit.
  ! tab = 3
    loc:InvalidTab += 1
    do ValidateValue::buttonViewCosts
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostInsert      Routine
PostCopy        Routine
  p_web.SetSessionValue('FormPayments:Primed',0)
PostUpdate      Routine
  p_web.SetSessionValue('FormPayments:Primed',0)
  p_web.StoreValue('')


PostDelete      Routine
frmUpdateWebOrder    PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
FilesOpened     Long
ORDWEBPR::State  USHORT
orw:PartNumber:IsInvalid  Long
orw:Description:IsInvalid  Long
orw:Quantity:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('frmUpdateWebOrder')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'frmUpdateWebOrder_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('frmUpdateWebOrder','')
    p_web.DivHeader('frmUpdateWebOrder',p_web.combine(p_web.site.style.formdiv,))
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('frmUpdateWebOrder') = 0
        p_web.AddPreCall('frmUpdateWebOrder')
        p_web.DivHeader('popup_frmUpdateWebOrder','nt-hidden')
        p_web.DivHeader('frmUpdateWebOrder',p_web.combine(p_web.site.style.formdiv,))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_frmUpdateWebOrder_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_frmUpdateWebOrder_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferfrmUpdateWebOrder',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:StagePost
    do RestoreMem
    do PostInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_frmUpdateWebOrder',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmUpdateWebOrder',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:StagePost
    do RestoreMem
    do PostCopy
  of Net:CopyRecord + NET:WEB:Populate
    If p_web.IfExistsValue('orw:RecordNumber') = 0 then p_web.SetValue('orw:RecordNumber',p_web.GSV('orw:RecordNumber')).
    do PreCopy
    p_web.setsessionvalue('showtab_frmUpdateWebOrder',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmUpdateWebOrder',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_frmUpdateWebOrder',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do PostInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do PostUpdate
    End
  of Net:ChangeRecord + NET:WEB:Populate
    If p_web.IfExistsValue('orw:RecordNumber') = 0 then p_web.SetValue('orw:RecordNumber',p_web.GSV('orw:RecordNumber')).
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_frmUpdateWebOrder',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmUpdateWebOrder',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:DeleteRecord + NET:WEB:StagePost
    do RestoreMem
    do PostDelete
  of Net:ViewRecord + NET:WEB:Populate
    If p_web.IfExistsValue('orw:RecordNumber') = 0 then p_web.SetValue('orw:RecordNumber',p_web.GSV('orw:RecordNumber')).
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_frmUpdateWebOrder',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmUpdateWebOrder',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_frmUpdateWebOrder',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_frmUpdateWebOrder',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('frmUpdateWebOrder')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(ORDWEBPR)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(ORDWEBPR)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('frmUpdateWebOrder_form:inited_',1)
  p_web.formsettings.file = 'ORDWEBPR'
  p_web.formsettings.key = 'orw:RecordNumberKey'
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = 'ORDWEBPR'
    p_web.formsettings.key = 'orw:RecordNumberKey'
      clear(p_web.formsettings.FieldName)
    p_web.formsettings.recordid[1] = orw:RecordNumber
    p_web.formsettings.FieldName[1] = 'orw:RecordNumber'
    do SetAction
    if p_web.GetSessionValue('frmUpdateWebOrder:Primed') = 1
      p_web.formsettings.action = Net:ChangeRecord
    Else
      p_web.formsettings.action = Loc:Act
    End
    p_web.formsettings.OriginalAction = Loc:Act
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'frmUpdateWebOrder'
    end
    p_web.formsettings.proc = 'frmUpdateWebOrder'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine
  IF p_web.GetSessionValue('frmUpdateWebOrder:Primed') = 1
    p_web._deleteFile(ORDWEBPR)
    p_web.SetSessionValue('frmUpdateWebOrder:Primed',0)
  End

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine
  p_web.SetValue('UpdateFile','ORDWEBPR')
  p_web.SetValue('UpdateKey','orw:RecordNumberKey')
  If p_web.IfExistsValue('orw:PartNumber')
    p_web.SetPicture('orw:PartNumber','@s30')
  End
  p_web.SetSessionPicture('orw:PartNumber','@s30')
  If p_web.IfExistsValue('orw:Description')
    p_web.SetPicture('orw:Description','@s30')
  End
  p_web.SetSessionPicture('orw:Description','@s30')
  If p_web.IfExistsValue('orw:Quantity')
    p_web.SetPicture('orw:Quantity','@s8')
  End
  p_web.SetSessionPicture('orw:Quantity','@s8')

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=File

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('frmUpdateWebOrder_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferfrmUpdateWebOrder')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('frmUpdateWebOrder_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('frmUpdateWebOrder_ChainTo')
    loc:formaction = p_web.GetSessionValue('frmUpdateWebOrder_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction

GenerateForm   Routine
  do LoadRelatedRecords
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Edit Web Order') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Edit Web Order',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_frmUpdateWebOrder',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_frmUpdateWebOrder0_div')&'">'&p_web.Translate('Details')&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="frmUpdateWebOrder_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.cancel();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CancelButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="frmUpdateWebOrder_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'frmUpdateWebOrder_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="frmUpdateWebOrder_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'frmUpdateWebOrder_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.orw:Quantity')
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_frmUpdateWebOrder')>0,p_web.GSV('showtab_frmUpdateWebOrder'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_frmUpdateWebOrder'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_frmUpdateWebOrder') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_frmUpdateWebOrder'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_frmUpdateWebOrder')>0,p_web.GSV('showtab_frmUpdateWebOrder'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_frmUpdateWebOrder') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Details') & ''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_frmUpdateWebOrder_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_frmUpdateWebOrder')>0,p_web.GSV('showtab_frmUpdateWebOrder'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"frmUpdateWebOrder",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_frmUpdateWebOrder')>0,p_web.GSV('showtab_frmUpdateWebOrder'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_frmUpdateWebOrder_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('frmUpdateWebOrder') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('frmUpdateWebOrder')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Details')&'</a></h3>' & CRLF & p_web.DivHeader('tab_frmUpdateWebOrder0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_frmUpdateWebOrder0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_frmUpdateWebOrder0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_frmUpdateWebOrder0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Details')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_frmUpdateWebOrder0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Details')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_frmUpdateWebOrder0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Details')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_frmUpdateWebOrder0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::orw:PartNumber
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::orw:PartNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::orw:PartNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::orw:Description
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::orw:Description
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::orw:Description
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::orw:Quantity
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::orw:Quantity
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::orw:Quantity
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Prompt::orw:PartNumber  Routine
  packet = clip(packet) & p_web.DivHeader('frmUpdateWebOrder_' & p_web._nocolon('orw:PartNumber') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Part Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::orw:PartNumber  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    orw:PartNumber = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    orw:PartNumber = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::orw:PartNumber  ! copies value to session value if valid.
  do Comment::orw:PartNumber ! allows comment style to be updated.

ValidateValue::orw:PartNumber  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('orw:PartNumber',orw:PartNumber).
    End

Value::orw:PartNumber  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('frmUpdateWebOrder_' & p_web._nocolon('orw:PartNumber') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- orw:PartNumber
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('orw:PartNumber'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::orw:PartNumber  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if orw:PartNumber:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('frmUpdateWebOrder_' & p_web._nocolon('orw:PartNumber') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::orw:Description  Routine
  packet = clip(packet) & p_web.DivHeader('frmUpdateWebOrder_' & p_web._nocolon('orw:Description') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Description'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::orw:Description  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    orw:Description = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    orw:Description = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::orw:Description  ! copies value to session value if valid.
  do Comment::orw:Description ! allows comment style to be updated.

ValidateValue::orw:Description  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('orw:Description',orw:Description).
    End

Value::orw:Description  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('frmUpdateWebOrder_' & p_web._nocolon('orw:Description') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- orw:Description
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('orw:Description'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::orw:Description  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if orw:Description:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('frmUpdateWebOrder_' & p_web._nocolon('orw:Description') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::orw:Quantity  Routine
  packet = clip(packet) & p_web.DivHeader('frmUpdateWebOrder_' & p_web._nocolon('orw:Quantity') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Quantity'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::orw:Quantity  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    orw:Quantity = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s8
    orw:Quantity = p_web.Dformat(p_web.GetValue('Value'),'@s8')
  End
  do ValidateValue::orw:Quantity  ! copies value to session value if valid.
  do Value::orw:Quantity
  do SendAlert
  do Comment::orw:Quantity ! allows comment style to be updated.

ValidateValue::orw:Quantity  Routine
    If not (1=0)
  If Numeric(orw:Quantity) = 0
    loc:Invalid = 'orw:Quantity'
    orw:Quantity:IsInvalid = true
    loc:alert = p_web.translate('Quantity') & ' ' & p_web.site.NumericText
  ElsIf InRange(orw:Quantity,1,9999) = false
    loc:Invalid = 'orw:Quantity'
    orw:Quantity:IsInvalid = true
    loc:alert = p_web.translate('Quantity') & ' ' & clip(p_web.site.MoreThanText) & ' ' & 1 & ', ' & clip(p_web.site.LessThanText) & ' ' & 9999
  End
      if loc:invalid = '' then p_web.SetSessionValue('orw:Quantity',orw:Quantity).
    End

Value::orw:Quantity  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('frmUpdateWebOrder_' & p_web._nocolon('orw:Quantity') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    orw:Quantity = p_web.RestoreValue('orw:Quantity')
    do ValidateValue::orw:Quantity
    If orw:Quantity:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- NUMBER --- orw:Quantity
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''orw:Quantity'',''frmupdateweborder_orw:quantity_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('number','orw:Quantity',p_web.GetSessionValueFormat('orw:Quantity'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s8'),'Quantity',,,1,9999,1) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::orw:Quantity  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if orw:Quantity:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate('')
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('frmUpdateWebOrder_' & p_web._nocolon('orw:Quantity') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('frmUpdateWebOrder_nexttab_' & 0)
    orw:PartNumber = p_web.GSV('orw:PartNumber')
    do ValidateValue::orw:PartNumber
    If loc:Invalid
      loc:retrying = 1
      do Value::orw:PartNumber
      !do SendAlert
      do Comment::orw:PartNumber ! allows comment style to be updated.
      !exit
    End
    orw:Description = p_web.GSV('orw:Description')
    do ValidateValue::orw:Description
    If loc:Invalid
      loc:retrying = 1
      do Value::orw:Description
      !do SendAlert
      do Comment::orw:Description ! allows comment style to be updated.
      !exit
    End
    orw:Quantity = p_web.GSV('orw:Quantity')
    do ValidateValue::orw:Quantity
    If loc:Invalid
      loc:retrying = 1
      do Value::orw:Quantity
      !do SendAlert
      do Comment::orw:Quantity ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_frmUpdateWebOrder_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('frmUpdateWebOrder_tab_' & 0)
    do GenerateTab0
  of lower('frmUpdateWebOrder_orw:Quantity_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::orw:Quantity
      of event:timer
        do Value::orw:Quantity
        do Comment::orw:Quantity
      else
        do Value::orw:Quantity
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('frmUpdateWebOrder_form:ready_',1)

  p_web.SetSessionValue('frmUpdateWebOrder_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_frmUpdateWebOrder',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('frmUpdateWebOrder_form:ready_',1)
  p_web.SetSessionValue('frmUpdateWebOrder_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_frmUpdateWebOrder',0)
  p_web._PreCopyRecord(ORDWEBPR,orw:RecordNumberKey)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('frmUpdateWebOrder_form:ready_',1)
  p_web.SetSessionValue('frmUpdateWebOrder_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('frmUpdateWebOrder:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('frmUpdateWebOrder_form:ready_',1)
  p_web.SetSessionValue('frmUpdateWebOrder_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('frmUpdateWebOrder:Primed',0)
  p_web.setsessionvalue('showtab_frmUpdateWebOrder',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
      If not (1=0)
          If p_web.IfExistsValue('orw:Quantity')
            orw:Quantity = p_web.GetValue('orw:Quantity')
          End
      End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('frmUpdateWebOrder_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('frmUpdateWebOrder_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
    do ValidateValue::orw:PartNumber
    If loc:Invalid then exit.
    do ValidateValue::orw:Description
    If loc:Invalid then exit.
    do ValidateValue::orw:Quantity
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostInsert      Routine
PostCopy        Routine
  p_web.SetSessionValue('frmUpdateWebOrder:Primed',0)
PostUpdate      Routine
  p_web.SetSessionValue('frmUpdateWebOrder:Primed',0)


PostDelete      Routine
frmUpdateStockReceive PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locQuantity          LONG                                  !
locQuantityReceived  LONG                                  !
locReason            STRING(255)                           !
FilesOpened     Long
RETSTOCK::State  USHORT
STOCKRECEIVETMP::State  USHORT
locQuantity:IsInvalid  Long
locQuantityReceived:IsInvalid  Long
locReason:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('frmUpdateStockReceive')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'frmUpdateStockReceive_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('frmUpdateStockReceive','')
    p_web.DivHeader('frmUpdateStockReceive',p_web.combine(p_web.site.style.formdiv,))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('frmUpdateStockReceive') = 0
        p_web.AddPreCall('frmUpdateStockReceive')
        p_web.DivHeader('popup_frmUpdateStockReceive','nt-hidden')
        p_web.DivHeader('frmUpdateStockReceive',p_web.combine(p_web.site.style.formdiv,))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_frmUpdateStockReceive_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_frmUpdateStockReceive_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferfrmUpdateStockReceive',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_frmUpdateStockReceive',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmUpdateStockReceive',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_frmUpdateStockReceive',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmUpdateStockReceive',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_frmUpdateStockReceive',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_frmUpdateStockReceive',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmUpdateStockReceive',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_frmUpdateStockReceive',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmUpdateStockReceive',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_frmUpdateStockReceive',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_frmUpdateStockReceive',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('frmUpdateStockReceive')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(RETSTOCK)
  p_web._OpenFile(STOCKRECEIVETMP)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(RETSTOCK)
  p_Web._CloseFile(STOCKRECEIVETMP)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('frmUpdateStockReceive_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'frmUpdateStockReceive'
    end
    p_web.formsettings.proc = 'frmUpdateStockReceive'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  if p_web.IfExistsValue('locQuantity') = 0
    p_web.SetSessionValue('locQuantity',locQuantity)
  Else
    locQuantity = p_web.GetSessionValue('locQuantity')
  End
  if p_web.IfExistsValue('locQuantityReceived') = 0
    p_web.SetSessionValue('locQuantityReceived',locQuantityReceived)
  Else
    locQuantityReceived = p_web.GetSessionValue('locQuantityReceived')
  End
  if p_web.IfExistsValue('locReason') = 0
    p_web.SetSessionValue('locReason',locReason)
  Else
    locReason = p_web.GetSessionValue('locReason')
  End

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locQuantity')
    locQuantity = p_web.GetValue('locQuantity')
    p_web.SetSessionValue('locQuantity',locQuantity)
  Else
    locQuantity = p_web.GetSessionValue('locQuantity')
  End
  if p_web.IfExistsValue('locQuantityReceived')
    locQuantityReceived = p_web.GetValue('locQuantityReceived')
    p_web.SetSessionValue('locQuantityReceived',locQuantityReceived)
  Else
    locQuantityReceived = p_web.GetSessionValue('locQuantityReceived')
  End
  if p_web.IfExistsValue('locReason')
    locReason = p_web.GetValue('locReason')
    p_web.SetSessionValue('locReason',locReason)
  Else
    locReason = p_web.GetSessionValue('locReason')
  End

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('frmUpdateStockReceive_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferfrmUpdateStockReceive')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('frmUpdateStockReceive_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('frmUpdateStockReceive_ChainTo')
    loc:formaction = p_web.GetSessionValue('frmUpdateStockReceive_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction

GenerateForm   Routine
  do LoadRelatedRecords
  p_web.StoreValue('stotmp:RecordNumber')
  p_web.SSV('ReadOnly:QuantityReceived',0)
  
  Access:STOCKRECEIVETMP.ClearKey(stotmp:RecordNumberKey)
  stotmp:RecordNumber = p_web.GSV('stotmp:RecordNumber')
  IF (Access:STOCKRECEIVETMP.TryFetch(stotmp:RecordNUmberKey) = Level:Benign)
      Access:RETSTOCK.Clearkey(res:Record_Number_Key)
      res:Record_Number = stotmp:RESRecordNumber
      IF (Access:RETSTOCK.Tryfetch(res:Record_Number_Key) = Level:Benign)
          p_web.SSV('locQuantity',res:Quantity)
          p_web.SSV('locQuantityReceived',res:QuantityReceived)
          IF (res:Received = 1)
              p_web.SSV('ReadOnly:QuantityReceived',1)
          END
      END
  END
 locQuantity = p_web.RestoreValue('locQuantity')
 locQuantityReceived = p_web.RestoreValue('locQuantityReceived')
 locReason = p_web.RestoreValue('locReason')
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Amend Order') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Amend Order',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_frmUpdateStockReceive',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_frmUpdateStockReceive0_div')&'">'&p_web.Translate('General')&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="frmUpdateStockReceive_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.cancel();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CancelButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="frmUpdateStockReceive_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'frmUpdateStockReceive_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="frmUpdateStockReceive_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'frmUpdateStockReceive_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locQuantityReceived')
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_frmUpdateStockReceive')>0,p_web.GSV('showtab_frmUpdateStockReceive'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_frmUpdateStockReceive'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_frmUpdateStockReceive') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_frmUpdateStockReceive'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_frmUpdateStockReceive')>0,p_web.GSV('showtab_frmUpdateStockReceive'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_frmUpdateStockReceive') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('General') & ''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_frmUpdateStockReceive_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_frmUpdateStockReceive')>0,p_web.GSV('showtab_frmUpdateStockReceive'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"frmUpdateStockReceive",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_frmUpdateStockReceive')>0,p_web.GSV('showtab_frmUpdateStockReceive'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_frmUpdateStockReceive_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('frmUpdateStockReceive') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('frmUpdateStockReceive')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('General')&'</a></h3>' & CRLF & p_web.DivHeader('tab_frmUpdateStockReceive0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_frmUpdateStockReceive0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_frmUpdateStockReceive0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_frmUpdateStockReceive0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'General')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_frmUpdateStockReceive0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('General')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_frmUpdateStockReceive0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('General')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_frmUpdateStockReceive0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locQuantity
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locQuantity
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locQuantity
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locQuantityReceived
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locQuantityReceived
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locQuantityReceived
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locReason
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locReason
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locReason
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Prompt::locQuantity  Routine
  packet = clip(packet) & p_web.DivHeader('frmUpdateStockReceive_' & p_web._nocolon('locQuantity') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Quantity'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locQuantity  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locQuantity = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locQuantity = p_web.GetValue('Value')
  End
  do ValidateValue::locQuantity  ! copies value to session value if valid.
  do Comment::locQuantity ! allows comment style to be updated.
  do Prompt::locReason
  do Value::locReason  !1

ValidateValue::locQuantity  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locQuantity',locQuantity).
    End

Value::locQuantity  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('frmUpdateStockReceive_' & p_web._nocolon('locQuantity') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- locQuantity
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locQuantity'',''frmupdatestockreceive_locquantity_value'',1,1)')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locQuantity')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locQuantity'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locQuantity  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locQuantity:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('frmUpdateStockReceive_' & p_web._nocolon('locQuantity') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locQuantityReceived  Routine
  packet = clip(packet) & p_web.DivHeader('frmUpdateStockReceive_' & p_web._nocolon('locQuantityReceived') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Quantity Received'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locQuantityReceived  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locQuantityReceived = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locQuantityReceived = p_web.GetValue('Value')
  End
  do ValidateValue::locQuantityReceived  ! copies value to session value if valid.
  do Value::locQuantityReceived
  do SendAlert
  do Comment::locQuantityReceived ! allows comment style to be updated.
  do Prompt::locReason
  do Value::locReason  !1

ValidateValue::locQuantityReceived  Routine
    If not (1=0)
  If locQuantityReceived = ''
    loc:Invalid = 'locQuantityReceived'
    locQuantityReceived:IsInvalid = true
    loc:alert = p_web.translate('Quantity Received') & ' ' & p_web.site.RequiredText
  End
  If Numeric(locQuantityReceived) = 0
    loc:Invalid = 'locQuantityReceived'
    locQuantityReceived:IsInvalid = true
    loc:alert = p_web.translate('Quantity Received') & ' ' & p_web.site.NumericText
  End
      if loc:invalid = '' then p_web.SetSessionValue('locQuantityReceived',locQuantityReceived).
    End

Value::locQuantityReceived  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('frmUpdateStockReceive_' & p_web._nocolon('locQuantityReceived') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If p_web.GSV('ReadOnly:QuantityReceived') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  If loc:retrying
    locQuantityReceived = p_web.RestoreValue('locQuantityReceived')
    do ValidateValue::locQuantityReceived
    If locQuantityReceived:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- NUMBER --- locQuantityReceived
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:QuantityReceived') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locQuantityReceived'',''frmupdatestockreceive_locquantityreceived_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locQuantityReceived')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('number','locQuantityReceived',p_web.GetSessionValueFormat('locQuantityReceived'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locQuantityReceived  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locQuantityReceived:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate('')
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('frmUpdateStockReceive_' & p_web._nocolon('locQuantityReceived') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locReason  Routine
  packet = clip(packet) & p_web.DivHeader('frmUpdateStockReceive_' & p_web._nocolon('locReason') & '_prompt',Choose(p_web.GSV('locQuantity') = p_web.GSV('locQuantityReceived'),'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('locQuantity') = p_web.GSV('locQuantityReceived'),'',p_web.Translate('Reason'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locReason  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locReason = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locReason = p_web.GetValue('Value')
  End
  do ValidateValue::locReason  ! copies value to session value if valid.
  do Value::locReason
  do SendAlert
  do Comment::locReason ! allows comment style to be updated.

ValidateValue::locReason  Routine
    If not (p_web.GSV('locQuantity') = p_web.GSV('locQuantityReceived'))
  If locReason = ''
    loc:Invalid = 'locReason'
    locReason:IsInvalid = true
    loc:alert = p_web.translate('Reason') & ' ' & p_web.site.RequiredText
  End
    locReason = Upper(locReason)
      if loc:invalid = '' then p_web.SetSessionValue('locReason',locReason).
    End

Value::locReason  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('locQuantity') = p_web.GSV('locQuantityReceived'),'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('frmUpdateStockReceive_' & p_web._nocolon('locReason') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,)
  If loc:retrying
    locReason = p_web.RestoreValue('locReason')
    do ValidateValue::locReason
    If locReason:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('locQuantity') = p_web.GSV('locQuantityReceived'))
  ! --- TEXT --- locReason
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locReason'',''frmupdatestockreceive_locreason_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'readonly','')
  do SendPacket
  p_web.CreateTextArea('locReason',p_web.GetSessionValue('locReason'),5,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,255,,,,Net:Send)
  do SendPacket
  End
  p_web.DivFooter()
Comment::locReason  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locReason:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web._jsok(p_web.site.RequiredText)
  loc:class = Choose(p_web.GSV('locQuantity') = p_web.GSV('locQuantityReceived'),'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('frmUpdateStockReceive_' & p_web._nocolon('locReason') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('locQuantity') = p_web.GSV('locQuantityReceived')
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('frmUpdateStockReceive_nexttab_' & 0)
    locQuantity = p_web.GSV('locQuantity')
    do ValidateValue::locQuantity
    If loc:Invalid
      loc:retrying = 1
      do Value::locQuantity
      !do SendAlert
      do Comment::locQuantity ! allows comment style to be updated.
      !exit
    End
    locQuantityReceived = p_web.GSV('locQuantityReceived')
    do ValidateValue::locQuantityReceived
    If loc:Invalid
      loc:retrying = 1
      do Value::locQuantityReceived
      !do SendAlert
      do Comment::locQuantityReceived ! allows comment style to be updated.
      !exit
    End
    locReason = p_web.GSV('locReason')
    do ValidateValue::locReason
    If loc:Invalid
      loc:retrying = 1
      do Value::locReason
      !do SendAlert
      do Comment::locReason ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_frmUpdateStockReceive_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('frmUpdateStockReceive_tab_' & 0)
    do GenerateTab0
  of lower('frmUpdateStockReceive_locQuantity_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locQuantity
      of event:timer
        do Value::locQuantity
        do Comment::locQuantity
      else
        do Value::locQuantity
      end
  of lower('frmUpdateStockReceive_locQuantityReceived_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locQuantityReceived
      of event:timer
        do Value::locQuantityReceived
        do Comment::locQuantityReceived
      else
        do Value::locQuantityReceived
      end
  of lower('frmUpdateStockReceive_locReason_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locReason
      of event:timer
        do Value::locReason
        do Comment::locReason
      else
        do Value::locReason
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('frmUpdateStockReceive_form:ready_',1)

  p_web.SetSessionValue('frmUpdateStockReceive_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_frmUpdateStockReceive',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('frmUpdateStockReceive_form:ready_',1)
  p_web.SetSessionValue('frmUpdateStockReceive_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_frmUpdateStockReceive',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('frmUpdateStockReceive_form:ready_',1)
  p_web.SetSessionValue('frmUpdateStockReceive_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('frmUpdateStockReceive:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('frmUpdateStockReceive_form:ready_',1)
  p_web.SetSessionValue('frmUpdateStockReceive_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('frmUpdateStockReceive:Primed',0)
  p_web.setsessionvalue('showtab_frmUpdateStockReceive',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
      If not (1=0)
        If not (p_web.GSV('ReadOnly:QuantityReceived') = 1)
          If p_web.IfExistsValue('locQuantityReceived')
            locQuantityReceived = p_web.GetValue('locQuantityReceived')
          End
        End
      End
      If not (p_web.GSV('locQuantity') = p_web.GSV('locQuantityReceived'))
          If p_web.IfExistsValue('locReason')
            locReason = p_web.GetValue('locReason')
          End
      End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('frmUpdateStockReceive_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  IF (p_web.GSV('locQuantity') <> p_web.GSV('locQuantityReceived'))
      Access:STOCKRECEIVETMP.ClearKey(stotmp:RecordNumberKey)
      stotmp:RecordNumber = p_web.GSV('stotmp:RecordNumber')
      IF (Access:STOCKRECEIVETMP.TryFetch(stotmp:RecordNUmberKey) = Level:Benign)
          Access:RETSTOCK.Clearkey(res:Record_Number_Key)
          res:Record_Number = stotmp:RESRecordNumber
          IF (Access:RETSTOCK.Tryfetch(res:Record_Number_Key) = Level:Benign)
              res:QuantityReceived = p_web.GSV('locQuantityReceived')
              res:Amended = 1
              res:Amend_Site_Loc = p_web.GSV('BookingStoresAccount')
              Access:RETSTOCK.TryUpdate()
          END
      END
  
  END
  p_web.DeleteSessionValue('frmUpdateStockReceive_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
    do ValidateValue::locQuantity
    If loc:Invalid then exit.
    do ValidateValue::locQuantityReceived
    If loc:Invalid then exit.
    do ValidateValue::locReason
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('frmUpdateStockReceive:Primed',0)
  p_web.StoreValue('locQuantity')
  p_web.StoreValue('locQuantityReceived')
  p_web.StoreValue('locReason')

