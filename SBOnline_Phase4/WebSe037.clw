

   MEMBER('WebServer_Phase4.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSE037.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSE009.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE010.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE011.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE030.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE036.INC'),ONCE        !Req'd for module callout resolution
                     END


ReceiptFromPUP       PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locIMEINumber        STRING(30)                            !
locMSN               STRING(30)                            !
locNetwork           STRING(30)                            !
locInFault           STRING(30)                            !
locIMEIMessage       STRING(100)                           !
locPassword          STRING(30)                            !
locValidationMessage STRING(100)                           !
FilesOpened     Long
WEBJOB::State  USHORT
NETWORKS::State  USHORT
MANFAULT::State  USHORT
TagFile::State  USHORT
JOBACC::State  USHORT
JOBS::State  USHORT
JOBSE::State  USHORT
locIMEINumber:IsInvalid  Long
locIMEIMessage:IsInvalid  Long
locPassword:IsInvalid  Long
locMSN:IsInvalid  Long
locNetwork:IsInvalid  Long
locInFault:IsInvalid  Long
TagValidateLoanAccessories:IsInvalid  Long
buttonValidateAccessories:IsInvalid  Long
locValidationMessage:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('ReceiptFromPUP')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'ReceiptFromPUP_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('ReceiptFromPUP','')
    p_web.DivHeader('ReceiptFromPUP',p_web.combine(p_web.site.style.formdiv,'fdiv'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('ReceiptFromPUP') = 0
        p_web.AddPreCall('ReceiptFromPUP')
        p_web.DivHeader('popup_ReceiptFromPUP','nt-hidden')
        p_web.DivHeader('ReceiptFromPUP',p_web.combine(p_web.site.style.formdiv,'fdiv'))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_ReceiptFromPUP_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_ReceiptFromPUP_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferReceiptFromPUP',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_ReceiptFromPUP',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferReceiptFromPUP',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_ReceiptFromPUP',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferReceiptFromPUP',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_ReceiptFromPUP',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_ReceiptFromPUP',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferReceiptFromPUP',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_ReceiptFromPUP',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferReceiptFromPUP',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_ReceiptFromPUP',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_ReceiptFromPUP',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('ReceiptFromPUP')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
deleteVariables         routine
    p_web.deleteSessionValue('locIMEINumber')
    p_web.deleteSessionValue('locIMEIMessage')
    p_web.deleteSessionValue('locIMEIAccepted')
    p_web.deleteSessionValue('locMSN')
    p_web.deleteSessionValue('locNetwork')
    p_web.deleteSessionValue('locInFault')
    p_web.deleteSessionValue('locPassword')
    p_web.deleteSessionValue('PasswordRequired')
    p_web.deleteSessionValue('Comment:IMEINumber')
    p_web.deleteSessionValue('Comment:Password')
    p_web.deleteSessionValue('tmp:LoanModelNumber')
    p_web.deleteSessionValue('locValidationMessage')
saveChanges         routine
    data
locNotes        String(255)
    code
        if (p_web.GSV('locIMEINumber') <> p_web.GSV('job:ESN'))
            locNotes = '<13,10>OLD I.M.E.I.: ' & p_web.GSV('job:ESN') & |
                '<13,10>NEW I.M.E.I.: ' & p_web.GSV('locIMEINumber')
            p_web.SSV('job:ESN',p_web.GSV('locIMEINumber'))
        end ! if (p_web.GSV('locIMEINumber') <> p_web.GSV('job:ESN'))

        if (p_web.GSV('locMSN') <> p_web.GSV('job:MSN'))
            locNotes = clip(locNotes) & |
                '<13,10>OLD M.S.N.: ' & p_web.GSV('job:MSN') & |
                '<13,10>NEW M.S.N.: ' & p_web.GSV('locMSN')
            p_web.SSV('job:MSN',p_web.GSV('locMSN'))
        end ! if (p_web.GSV('locMSN') <> p_web.GSV('job:MSN'))

        if (p_web.GSV('locNetwork') <> p_web.GSV('jobe:Network'))
            locNotes = clip(locNotes) & |
                '<13,10>OLD NETWORK: ' & p_web.GSV('jobe:Network') & |
                '<13,10>NEW NETWORK: ' & p_web.GSV('locNetwork')
            p_web.SSV('jobe:Network',p_web.GSV('locNetwork'))
        end ! if (p_web.GSV('locNetwork') <> p_web.GSV('job:Network'))

        Access:MANFAULT.Clearkey(maf:inFaultKey)
        maf:manufacturer    = p_web.GSV('job:manufacturer')
        maf:inFault    = 1
        if (Access:MANFAULT.TryFetch(maf:inFaultKey) = Level:Benign)
        ! Found
        else ! if (Access:MANFAULT.TryFetch(maf:inFaultKey) = Level:Benign)
        ! Error
        end ! if (Access:MANFAULT.TryFetch(maf:inFaultKey) = Level:Benign)

        if (maf:Field_Number < 13)
            if (p_web.GSV('job:Fault_Code' & maf:Field_Number) <> p_web.GSV('locInFault'))
                locNotes = clip(locNotes) & |
                    '<13,10>OLD IN FAULT: ' & p_web.GSV('job:Fault_Code' & maf:Field_Number) & |
                    '<13,10>NEW IN FAULT: ' & p_web.GSV('locInFault')
                p_web.SSV('job:Fault_Code' & maf:Field_Number,p_web.GSV('locInFault'))
            end ! if (p_web.GSV('job:Fault_Code' & maf:Field_Number) <> p_web.GSV('locInFault'))
        else ! if (maf:Field_Number < 13)
            if (p_web.GSV('wob:FaultCode' & maf:Field_Number) <> p_web.GSV('locInFault'))
                locNotes = clip(locNotes) & |
                    '<13,10>OLD IN FAULT: ' & p_web.GSV('wob:FaultCode' & maf:Field_Number) & |
                    '<13,10>NEW IN FAULT: ' & p_web.GSV('locInFault')
                p_web.SSV('wob:FaultCode' & maf:Field_Number,p_web.GSV('locInFault'))
            end ! if (p_web.GSV('job:Fault_Code' & maf:Field_Number) <> p_web.GSV('locInFault'))
        end ! if (maf:Field_Number < 13)

        if (clip(locNotes) <> '')
            p_web.SSV('AddToAudit:Type','JOB')
            p_web.SSV('AddToAudit:Notes','DETAILS CHANGED:' & clip(locNotes))
            p_web.SSV('AddToAudit:Action','PUP VALIDATION')
            addToAudit(p_web)
        end ! if (clip(locNotes) <> '')

        locNotes = ''
        if (instring('MISMATCH',upper(p_web.GSV('locValidationMessage')),1,1))
            locNotes = 'ACCESSORY MISMATCH. BOOKED AT PUP:-'
            Access:JOBACC.Clearkey(jac:ref_Number_Key)
            jac:ref_Number    = p_web.GSV('job:Ref_Number')
            set(jac:ref_Number_Key,jac:ref_Number_Key)
            loop
                if (Access:JOBACC.Next())
                    Break
                end ! if (Access:JOBACC.Next())
                if (jac:ref_Number    <> p_web.GSV('job:Ref_Number'))
                    Break
                end ! if (jac:ref_Number    <> p_web.GSV('job:Ref_Number'))
                locNotes = clip(locNotes) & |
                    '<13,10>' & clip(jac:accessory)
            end ! loop

            locNotes = clip(locNotes) & |
                '<13,10,13,0>ACCESSORIES RECEIVED:-'

            Access:TAGFILE.Clearkey(tag:keyTagged)
            tag:sessionID    = p_web.sessionID
            set(tag:keyTagged,tag:keyTagged)
            loop
                if (Access:TAGFILE.Next())
                    Break
                end ! if (Access:TAGFILE.Next())
                if (tag:sessionID    <> p_web.sessionID)
                    Break
                end ! if (tag:sessionID    <> p_web.sessionID)
                if (tag:Tagged = 0)
                    cycle
                end ! if (tag:Tagged = 0)
                locNotes = clip(locNotes) & |
                    '<13,10>' & clip(tag:taggedValue)
            end ! loop

        end ! if (instring('MISMATCH',upper(p_web.GSV('locValidationMessage')),1,1)

        p_web.SSV('AddToAudit:Type','JOB')
        p_web.SSV('AddToAudit:Notes',clip(locNotes))
        p_web.SSV('AddToAudit:Action','UNIT RECEIVED AT ' & p_web.GSV('BookingSite') & ' FROM PUP')
        addToAudit(p_web)

        p_web.SSV('LocationChange:Location',p_web.GSV('Default:' & p_web.GSV('BookingSite') & 'Location'))
        locationChange(p_web)

        p_web.SSV('job:Workshop','YES')

        p_web.SSV('GetStatus:Type','JOB')
        p_web.SSV('GetStatus:StatusNumber',sub(p_web.GSV('Default:StatusReceivedFromPUP'),1,3))
        GetStatus(p_web.GSV('GetStatus:StatusNumber'),0,p_web.GSV('GetStatus:Type'),p_web)

        Access:JOBS.Clearkey(job:ref_Number_Key)
        job:ref_Number    = p_web.GSV('job:Ref_Number')
        if (Access:JOBS.TryFetch(job:ref_Number_Key) = Level:Benign)
        ! Found
            p_web.SessionQueueToFile(JOBS)
            access:JOBS.tryUpdate()
            updateDateTimeStamp(job:Ref_Number)
        else ! if (Access:JOBS.TryFetch(job:ref_Number_Key) = Level:Benign)
        ! Error
        end ! if (Access:JOBS.TryFetch(job:ref_Number_Key) = Level:Benign)

        Access:WEBJOB.ClearKey(wob:RefNumberKey)
        wob:RefNumber = p_web.GSV('job:Ref_Number')
        if (Access:WEBJOB.TryFetch(wob:RefNumberKey)= Level:Benign)
            p_web.SessionQueueToFile(WEBJOB)
            access:WEBJOB.TryUpdate()
        END

        Access:JOBSE.Clearkey(jobe:refNumberKey)
        jobe:refNumber    = p_web.GSV('job:Ref_Number')
        if (Access:JOBSE.TryFetch(jobe:refNumberKey) = Level:Benign)
        ! Found
            p_web.SessionQueueToFile(JOBSE)
            access:JOBSE.tryUpdate()
        else ! if (Access:JOBSE.TryFetch(jobe:refNumberKey) = Level:Benign)
        ! Error
        end ! if (Access:JOBSE.TryFetch(jobe:refNumberKey) = Level:Benign)
OpenFiles  ROUTINE
  p_web._OpenFile(WEBJOB)
  p_web._OpenFile(NETWORKS)
  p_web._OpenFile(MANFAULT)
  p_web._OpenFile(TagFile)
  p_web._OpenFile(JOBACC)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(JOBSE)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(NETWORKS)
  p_Web._CloseFile(MANFAULT)
  p_Web._CloseFile(TagFile)
  p_Web._CloseFile(JOBACC)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(JOBSE)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE
  loc:EipClm = upper(p_web.GetValue('_EIPClm'))
  p_web.DeleteValue('_EIPClm')
  case loc:EipClm
  of ''
    case upper(p_web.GetValue('_calledfrom_'))
    of upper('TagValidateLoanAccessories')
      do Value::TagValidateLoanAccessories
    end
  end

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('ReceiptFromPUP_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'ReceiptFromPUP'
    end
    p_web.formsettings.proc = 'ReceiptFromPUP'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine
  do DeleteVariables

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'locNetwork'
    p_web.setsessionvalue('showtab_ReceiptFromPUP',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(NETWORKS)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.locInFault')
  End
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  if p_web.IfExistsValue('locIMEINumber') = 0
    p_web.SetSessionValue('locIMEINumber',locIMEINumber)
  Else
    locIMEINumber = p_web.GetSessionValue('locIMEINumber')
  End
  if p_web.IfExistsValue('locIMEIMessage') = 0
    p_web.SetSessionValue('locIMEIMessage',locIMEIMessage)
  Else
    locIMEIMessage = p_web.GetSessionValue('locIMEIMessage')
  End
  if p_web.IfExistsValue('locPassword') = 0
    p_web.SetSessionValue('locPassword',locPassword)
  Else
    locPassword = p_web.GetSessionValue('locPassword')
  End
  if p_web.IfExistsValue('locMSN') = 0
    p_web.SetSessionValue('locMSN',locMSN)
  Else
    locMSN = p_web.GetSessionValue('locMSN')
  End
  if p_web.IfExistsValue('locNetwork') = 0
    p_web.SetSessionValue('locNetwork',locNetwork)
  Else
    locNetwork = p_web.GetSessionValue('locNetwork')
  End
  if p_web.IfExistsValue('locInFault') = 0
    p_web.SetSessionValue('locInFault',locInFault)
  Else
    locInFault = p_web.GetSessionValue('locInFault')
  End
  if p_web.IfExistsValue('locValidationMessage') = 0
    p_web.SetSessionValue('locValidationMessage',locValidationMessage)
  Else
    locValidationMessage = p_web.GetSessionValue('locValidationMessage')
  End

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locIMEINumber')
    locIMEINumber = p_web.GetValue('locIMEINumber')
    p_web.SetSessionValue('locIMEINumber',locIMEINumber)
  Else
    locIMEINumber = p_web.GetSessionValue('locIMEINumber')
  End
  if p_web.IfExistsValue('locIMEIMessage')
    locIMEIMessage = p_web.GetValue('locIMEIMessage')
    p_web.SetSessionValue('locIMEIMessage',locIMEIMessage)
  Else
    locIMEIMessage = p_web.GetSessionValue('locIMEIMessage')
  End
  if p_web.IfExistsValue('locPassword')
    locPassword = p_web.GetValue('locPassword')
    p_web.SetSessionValue('locPassword',locPassword)
  Else
    locPassword = p_web.GetSessionValue('locPassword')
  End
  if p_web.IfExistsValue('locMSN')
    locMSN = p_web.GetValue('locMSN')
    p_web.SetSessionValue('locMSN',locMSN)
  Else
    locMSN = p_web.GetSessionValue('locMSN')
  End
  if p_web.IfExistsValue('locNetwork')
    locNetwork = p_web.GetValue('locNetwork')
    p_web.SetSessionValue('locNetwork',locNetwork)
  Else
    locNetwork = p_web.GetSessionValue('locNetwork')
  End
  if p_web.IfExistsValue('locInFault')
    locInFault = p_web.GetValue('locInFault')
    p_web.SetSessionValue('locInFault',locInFault)
  Else
    locInFault = p_web.GetSessionValue('locInFault')
  End
  if p_web.IfExistsValue('locValidationMessage')
    locValidationMessage = p_web.GetValue('locValidationMessage')
    p_web.SetSessionValue('locValidationMessage',locValidationMessage)
  Else
    locValidationMessage = p_web.GetSessionValue('locValidationMessage')
  End

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('ReceiptFromPUP_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'ViewJob'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('ReceiptFromPUP_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('ReceiptFromPUP_ChainTo')
    loc:formaction = p_web.GetSessionValue('ReceiptFromPUP_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'ViewJob'

GenerateForm   Routine
  do LoadRelatedRecords
  !INIT
      if (MSNRequired(p_web.GSV('job:Manufacturer')))
          p_web.SSV('Hide:MSN',0)
      else
          p_web.SSV('Hide:MSN',1)
      end ! if (MSNRequired(p_web.GSV('job:Manufacturer')))
  
      p_web.SSV('Comment:IMEINumber','Enter I.M.E.I. and press [TAB] to accept')
  
      ! Used for the tag browse
      p_web.SSV('tmp:LoanModelNumber',p_web.GSV('job:Model_Number'))
  
      clearTagFile(p_web)
 locIMEINumber = p_web.RestoreValue('locIMEINumber')
 locIMEIMessage = p_web.RestoreValue('locIMEIMessage')
 locPassword = p_web.RestoreValue('locPassword')
 locMSN = p_web.RestoreValue('locMSN')
 locNetwork = p_web.RestoreValue('locNetwork')
 locInFault = p_web.RestoreValue('locInFault')
 locValidationMessage = p_web.RestoreValue('locValidationMessage')
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Receipt From PUP') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Receipt From PUP',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_ReceiptFromPUP',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_ReceiptFromPUP0_div')&'">'&p_web.Translate('PUP Unit Validation')&'</a></li>'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_ReceiptFromPUP1_div')&'">'&p_web.Translate()&'</a></li>'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_ReceiptFromPUP2_div')&'">'&p_web.Translate()&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="ReceiptFromPUP_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('ReceiptFromPUP_TagValidateLoanAccessories_embedded_div')&''');'
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('ReceiptFromPUP_TagValidateLoanAccessories_embedded_div')&''');'
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.cancel();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CancelButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="ReceiptFromPUP_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'ReceiptFromPUP_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="ReceiptFromPUP_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('ReceiptFromPUP_TagValidateLoanAccessories_embedded_div')&''');'
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'ReceiptFromPUP_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
    If upper(p_web.getvalue('LookupFile'))='NETWORKS'
          If Not (1=0)
            p_web.SetValue('SelectField',clip(loc:formname) & '.locInFault')
          End
    End
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locIMEINumber')
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_ReceiptFromPUP')>0,p_web.GSV('showtab_ReceiptFromPUP'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_ReceiptFromPUP'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_ReceiptFromPUP') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_ReceiptFromPUP'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_ReceiptFromPUP')>0,p_web.GSV('showtab_ReceiptFromPUP'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_ReceiptFromPUP') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('PUP Unit Validation') & ''''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_ReceiptFromPUP_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_ReceiptFromPUP')>0,p_web.GSV('showtab_ReceiptFromPUP'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"ReceiptFromPUP",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_ReceiptFromPUP')>0,p_web.GSV('showtab_ReceiptFromPUP'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_ReceiptFromPUP_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('ReceiptFromPUP') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('ReceiptFromPUP')
    p_web.SetValue('_CallPopups',2)
    If p_web.GetPreCall('TagValidateLoanAccessories') = 0
      p_web.SetValue('TagValidateLoanAccessories:NoForm',1)
      p_web.SetValue('TagValidateLoanAccessories:FormName',loc:formname)
      p_web.SetValue('TagValidateLoanAccessories:parentIs','Form')
      p_web.SetValue('_parentProc','ReceiptFromPUP')
      TagValidateLoanAccessories(p_web)
      p_web.SetValue('_CallPopups',0)
      p_web.DeleteValue('TagValidateLoanAccessories:NoForm')
      p_web.DeleteValue('TagValidateLoanAccessories:FormName')
      p_web.DeleteValue('TagValidateLoanAccessories:parentIs')
      p_web.DeleteValue('_parentProc')
    End
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('PUP Unit Validation')&'</a></h3>' & CRLF & p_web.DivHeader('tab_ReceiptFromPUP0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_ReceiptFromPUP0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_ReceiptFromPUP0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_ReceiptFromPUP0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'PUP Unit Validation')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_ReceiptFromPUP0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('PUP Unit Validation')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_ReceiptFromPUP0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('PUP Unit Validation')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_ReceiptFromPUP0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'25%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locIMEINumber
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locIMEINumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locIMEINumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locIMEIMessage
        do Value::locIMEIMessage
        do Comment::locIMEIMessage
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'25%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locPassword
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locPassword
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locPassword
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab1  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate()&'</a></h3>' & CRLF & p_web.DivHeader('tab_ReceiptFromPUP1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_ReceiptFromPUP1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_ReceiptFromPUP1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_ReceiptFromPUP1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,)
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_ReceiptFromPUP1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend)
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_ReceiptFromPUP1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain">' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_ReceiptFromPUP1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
      If p_web.GSV('Hide:MSN') <> 1
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'25%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locMSN
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locMSN
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locMSN
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'25%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locNetwork
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locNetwork
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locNetwork
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'25%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locInFault
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locInFault
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locInFault
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab2  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate()&'</a></h3>' & CRLF & p_web.DivHeader('tab_ReceiptFromPUP2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_ReceiptFromPUP2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_ReceiptFromPUP2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_ReceiptFromPUP2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,)
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_ReceiptFromPUP2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend)
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_ReceiptFromPUP2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain">' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_ReceiptFromPUP2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'25%'&'"'
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::TagValidateLoanAccessories
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::buttonValidateAccessories
        do Comment::buttonValidateAccessories
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::locValidationMessage
        do Comment::locValidationMessage
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Prompt::locIMEINumber  Routine
  packet = clip(packet) & p_web.DivHeader('ReceiptFromPUP_' & p_web._nocolon('locIMEINumber') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('I.M.E.I. Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locIMEINumber  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locIMEINumber = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locIMEINumber = p_web.GetValue('Value')
  End
  do ValidateValue::locIMEINumber  ! copies value to session value if valid.
      if (p_web.GSV('locIMEINumber') <> p_web.GSV('job:ESN'))
          p_web.SSV('locIMEIMessage','IMEI Number does not match IMEI from booking. Enter password to accept change and continue.')
          p_web.SSV('PasswordRequired',1)
  !        p_web.SSV('locIMEIAccepted',0)
      else!
  !        p_web.SSV('locIMEIAccepted',1)
          p_web.SSV('PasswordRequired',0)
          p_web.SSV('locIMEIMessage','')
      end ! if (p_web.GSV('locIMEINumber') <> p_web.GSV('job:ESN'))
  do Value::locIMEINumber
  do SendAlert
  do Comment::locIMEINumber ! allows comment style to be updated.
  do Comment::locIMEINumber
  do Prompt::locPassword
  do Value::locPassword  !1
  do Comment::locPassword
  do Prompt::locInFault
  do Value::locInFault  !1
  do Prompt::locMSN
  do Value::locMSN  !1
  do Prompt::locNetwork
  do Value::locNetwork  !1
  do Value::locIMEIMessage  !1

ValidateValue::locIMEINumber  Routine
    If not (1=0)
  If locIMEINumber = ''
    loc:Invalid = 'locIMEINumber'
    locIMEINumber:IsInvalid = true
    loc:alert = p_web.translate('I.M.E.I. Number') & ' ' & p_web.site.RequiredText
  End
    locIMEINumber = Upper(locIMEINumber)
      if loc:invalid = '' then p_web.SetSessionValue('locIMEINumber',locIMEINumber).
    End

Value::locIMEINumber  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ReceiptFromPUP_' & p_web._nocolon('locIMEINumber') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,'formrqd')
  If loc:retrying
    locIMEINumber = p_web.RestoreValue('locIMEINumber')
    do ValidateValue::locIMEINumber
    If locIMEINumber:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- locIMEINumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locIMEINumber'',''receiptfrompup_locimeinumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locIMEINumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locIMEINumber',p_web.GetSessionValueFormat('locIMEINumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locIMEINumber  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locIMEINumber:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:IMEINumber'))
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ReceiptFromPUP_' & p_web._nocolon('locIMEINumber') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locIMEIMessage  Routine
  packet = clip(packet) & p_web.DivHeader('ReceiptFromPUP_' & p_web._nocolon('locIMEIMessage') & '_prompt',Choose(p_web.GSV('locIMEIMessage') = '','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('locIMEIMessage') = '','',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locIMEIMessage  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locIMEIMessage = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locIMEIMessage = p_web.GetValue('Value')
  End
  do ValidateValue::locIMEIMessage  ! copies value to session value if valid.
  do Comment::locIMEIMessage ! allows comment style to be updated.

ValidateValue::locIMEIMessage  Routine
    If not (p_web.GSV('locIMEIMessage') = '')
      if loc:invalid = '' then p_web.SetSessionValue('locIMEIMessage',locIMEIMessage).
    End

Value::locIMEIMessage  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('locIMEIMessage') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ReceiptFromPUP_' & p_web._nocolon('locIMEIMessage') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (p_web.GSV('locIMEIMessage') = '')
  ! --- DISPLAY --- locIMEIMessage
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('red bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locIMEIMessage'),0) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locIMEIMessage  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locIMEIMessage:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('locIMEIMessage') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ReceiptFromPUP_' & p_web._nocolon('locIMEIMessage') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('locIMEIMessage') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locPassword  Routine
  packet = clip(packet) & p_web.DivHeader('ReceiptFromPUP_' & p_web._nocolon('locPassword') & '_prompt',Choose(p_web.GSV('PasswordRequired') <> 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('PasswordRequired') <> 1,'',p_web.Translate('Enter Password'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locPassword  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locPassword = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locPassword = p_web.GetValue('Value')
  End
  do ValidateValue::locPassword  ! copies value to session value if valid.
  Access:users.Clearkey(use:Password_Key)
  use:Password    = p_web.GSV('locPassword')
  if (Access:users.TryFetch(use:Password_Key) = Level:Benign)
      ! Found
      Access:ACCAREAS.Clearkey(acc:access_Level_Key)
      acc:user_Level    = use:user_Level
      acc:access_Area    = 'PUP RECEIPT - CHANGE IMEI'
      if (Access:ACCAREAS.TryFetch(acc:access_Level_Key) = Level:Benign)
          ! Found
          p_web.SSV('Comment:Password','Password Accepted')
      else ! if (Access:ACCAREAS.TryFetch(acc:access_Level_Key) = Level:Benign)
          ! Error
          p_web.SSV('Comment:Password','You do not have access to change the IMEI')
          p_web.SSV('locPassword','')
      end ! if (Access:ACCAREAS.TryFetch(acc:access_Level_Key) = Level:Benign)
  else ! if (Access:users.TryFetch(use:Password_Key) = Level:Benign)
      ! Error
      p_web.SSV('Comment:Password','Invalid Password')
      p_web.SSV('locPassword','')
  
  end ! if (Access:users.TryFetch(use:Password_Key) = Level:Benign)
  do Value::locPassword
  do SendAlert
  do Comment::locPassword ! allows comment style to be updated.
  do Value::locIMEINumber  !1
  do Comment::locIMEINumber
  do Prompt::locPassword
  do Comment::locPassword

ValidateValue::locPassword  Routine
    If not (p_web.GSV('PasswordRequired') <> 1)
  If locPassword = ''
    loc:Invalid = 'locPassword'
    locPassword:IsInvalid = true
    loc:alert = p_web.translate('Enter Password') & ' ' & p_web.site.RequiredText
  End
    locPassword = Upper(locPassword)
      if loc:invalid = '' then p_web.SetSessionValue('locPassword',locPassword).
    End

Value::locPassword  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('PasswordRequired') <> 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ReceiptFromPUP_' & p_web._nocolon('locPassword') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,'formrqd')
  If loc:retrying
    locPassword = p_web.RestoreValue('locPassword')
    do ValidateValue::locPassword
    If locPassword:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('PasswordRequired') <> 1)
  ! --- STRING --- locPassword
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locPassword'',''receiptfrompup_locpassword_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locPassword')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('password','locPassword',p_web.GetSessionValueFormat('locPassword'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locPassword  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locPassword:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:Password'))
  loc:class = Choose(p_web.GSV('PasswordRequired') <> 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ReceiptFromPUP_' & p_web._nocolon('locPassword') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('PasswordRequired') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locMSN  Routine
  packet = clip(packet) & p_web.DivHeader('ReceiptFromPUP_' & p_web._nocolon('locMSN') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('M.S.N.'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locMSN  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locMSN = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locMSN = p_web.GetValue('Value')
  End
  do ValidateValue::locMSN  ! copies value to session value if valid.
  do Value::locMSN
  do SendAlert
  do Comment::locMSN ! allows comment style to be updated.

ValidateValue::locMSN  Routine
  If p_web.GSV('Hide:MSN') <> 1
    If not (1=0)
  If locMSN = ''
    loc:Invalid = 'locMSN'
    locMSN:IsInvalid = true
    loc:alert = p_web.translate('M.S.N.') & ' ' & p_web.site.RequiredText
  End
    locMSN = Upper(locMSN)
      if loc:invalid = '' then p_web.SetSessionValue('locMSN',locMSN).
    End
  End

Value::locMSN  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ReceiptFromPUP_' & p_web._nocolon('locMSN') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,'formrqd')
  If loc:retrying
    locMSN = p_web.RestoreValue('locMSN')
    do ValidateValue::locMSN
    If locMSN:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- locMSN
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locMSN'',''receiptfrompup_locmsn_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locMSN',p_web.GetSessionValueFormat('locMSN'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locMSN  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locMSN:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web._jsok(p_web.site.RequiredText)
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ReceiptFromPUP_' & p_web._nocolon('locMSN') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locNetwork  Routine
  packet = clip(packet) & p_web.DivHeader('ReceiptFromPUP_' & p_web._nocolon('locNetwork') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Network'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locNetwork  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locNetwork = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locNetwork = p_web.GetValue('Value')
  ElsIf p_web.RequestAjax = 1 and p_web.IfExistsValue('net:Network')
    locNetwork = p_web.GetValue('net:Network')
  ElsIf p_web.RequestAjax = 1
    locNetwork = net:Network
  End
  do ValidateValue::locNetwork  ! copies value to session value if valid.
  p_Web.SetValue('lookupfield','locNetwork')
  do AfterLookup
  do Value::locNetwork
  do SendAlert
  do Comment::locNetwork

ValidateValue::locNetwork  Routine
    If not (1=0)
  If locNetwork = ''
    loc:Invalid = 'locNetwork'
    locNetwork:IsInvalid = true
    loc:alert = p_web.translate('Network') & ' ' & p_web.site.RequiredText
  End
    locNetwork = Upper(locNetwork)
      if loc:invalid = '' then p_web.SetSessionValue('locNetwork',locNetwork).
    End

Value::locNetwork  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ReceiptFromPUP_' & p_web._nocolon('locNetwork') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,'formrqd')
  If loc:retrying
    locNetwork = p_web.RestoreValue('locNetwork')
    do ValidateValue::locNetwork
    If locNetwork:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- locNetwork
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locNetwork'',''receiptfrompup_locnetwork_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locNetwork')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','locNetwork',p_web.GetSessionValue('locNetwork'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'!4
    if not loc:viewonly and not loc:readonly
      loc:fieldclass = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('SelectNetworks')&'?LookupField=locNetwork&Tab=2&ForeignField=net:Network&_sort=net:Network&Refresh=sort'),,,,,loc:fieldclass)
    End
  do SendPacket
  End
  p_web.DivFooter()
Comment::locNetwork  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locNetwork:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web._jsok(p_web.site.RequiredText)
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ReceiptFromPUP_' & p_web._nocolon('locNetwork') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locInFault  Routine
  packet = clip(packet) & p_web.DivHeader('ReceiptFromPUP_' & p_web._nocolon('locInFault') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('In Fault'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locInFault  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locInFault = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locInFault = p_web.GetValue('Value')
  End
  do ValidateValue::locInFault  ! copies value to session value if valid.
  do Value::locInFault
  do SendAlert
  do Comment::locInFault ! allows comment style to be updated.

ValidateValue::locInFault  Routine
    If not (1=0)
  If locInFault = ''
    loc:Invalid = 'locInFault'
    locInFault:IsInvalid = true
    loc:alert = p_web.translate('In Fault') & ' ' & p_web.site.RequiredText
  End
    locInFault = Upper(locInFault)
      if loc:invalid = '' then p_web.SetSessionValue('locInFault',locInFault).
    End

Value::locInFault  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ReceiptFromPUP_' & p_web._nocolon('locInFault') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,'formrqd')
  If loc:retrying
    locInFault = p_web.RestoreValue('locInFault')
    do ValidateValue::locInFault
    If locInFault:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- locInFault
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locInFault'',''receiptfrompup_locinfault_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locInFault',p_web.GetSessionValueFormat('locInFault'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'
  do SendPacket
          Access:MANFAULT.Clearkey(maf:InFaultKey)
          maf:manufacturer    = p_web.GSV('job:manufacturer')
          maf:inFault    = 1
          if (Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign)
              ! Found
              packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('BrowseJobFaultCodeLookup')&|
                          '?LookupField=locInFault&Tab=2&ForeignField=mfo:Field&_sort=mfo:Field&Refresh=' & |
                          'sort&LookupFrom=ReceiptFromPUP&' & |
                          'fieldNumber=' & maf:Field_Number & '&partType=&partMainFault='),) !lookupextra
  
          else ! if (Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign)
              ! Error
          end ! if (Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign)
  
  End
  p_web.DivFooter()
Comment::locInFault  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locInFault:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web._jsok(p_web.site.RequiredText)
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ReceiptFromPUP_' & p_web._nocolon('locInFault') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::TagValidateLoanAccessories  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  Elsif true
    p_web.StoreValue('acr:Accessory')
  End
  do ValidateValue::TagValidateLoanAccessories  ! copies value to session value if valid.
  do Comment::TagValidateLoanAccessories ! allows comment style to be updated.

ValidateValue::TagValidateLoanAccessories  Routine
    If not (1=0)
    End

Value::TagValidateLoanAccessories  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(1=0,1,0))
  ! --- BROWSE ---  TagValidateLoanAccessories --
  p_web.SetValue('TagValidateLoanAccessories:NoForm',1)
  p_web.SetValue('TagValidateLoanAccessories:FormName',loc:formname)
  p_web.SetValue('TagValidateLoanAccessories:parentIs','Form')
  p_web.SetValue('_parentProc','ReceiptFromPUP')
  if p_web.RequestAjax = 0
    p_web.SSV('ReceiptFromPUP:_popup_',p_web.GetValue('_popup_')) ! stores the current browse popup state
    packet = clip(packet) & '<div id="'&lower('ReceiptFromPUP_TagValidateLoanAccessories_embedded_div')&'"><!-- Net:TagValidateLoanAccessories --></div><13,10>'
    do SendPacket
    p_web.DivHeader('ReceiptFromPUP_' & lower('TagValidateLoanAccessories') & '_value')
    p_web.DivFooter()
  else
    if p_web.GetValue('_popup_') = 1
      p_web.SSV('ReceiptFromPUP:_popup_',1)
    elsif p_web.GSV('ReceiptFromPUP:_popup_') = 1
      p_web.SetValue('_popup_',1)
    end
    packet = clip(packet) & '<!-- Net:TagValidateLoanAccessories --><13,10>'
  end
  do SendPacket
Comment::TagValidateLoanAccessories  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if TagValidateLoanAccessories:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ReceiptFromPUP_' & p_web._nocolon('TagValidateLoanAccessories') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonValidateAccessories  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonValidateAccessories  ! copies value to session value if valid.
  !Validate Accessories
      error# = 0
  
      Access:JOBACC.Clearkey(jac:ref_Number_Key)
      jac:ref_Number    = p_web.GSV('job:Ref_Number')
      set(jac:ref_Number_Key,jac:ref_Number_Key)
      loop
          if (Access:JOBACC.Next())
              Break
          end ! if (Access:JOBACC.Next())
          if (jac:ref_Number    <> p_web.GSV('job:Ref_Number'))
              Break
          end ! if (jac:ref_Number    <> p_web.GSV('job:Ref_Number'))
          Access:TAGFILE.Clearkey(tag:keyTagged)
          tag:sessionID    = p_web.sessionID
          tag:taggedValue  = jac:accessory
          if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
              ! Found
              if (tag:tagged <> 1)
                  error# = 1
                  break
              end ! if (tag:tagged <> 1)
          else ! if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
              ! Error
              error# = 1
              break
          end ! if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
  
      end ! loop
  
      if (error# = 0)
          Access:TAGFILE.Clearkey(tag:keyTagged)
          tag:sessionID    = p_web.sessionID
          set(tag:keyTagged,tag:keyTagged)
          loop
              if (Access:TAGFILE.Next())
                  Break
              end ! if (Access:TAGFILE.Next())
              if (tag:sessionID    <> p_web.sessionID)
                  Break
              end ! if (tag:sessionID    <> p_web.sessionID)
              if (tag:Tagged = 0)
                  cycle
              end ! if (tag:Tagged = 0)
  
              Access:JOBACC.Clearkey(jac:ref_Number_Key)
              jac:ref_Number    = p_web.GSV('job:Ref_Number')
              jac:accessory    = tag:taggedValue
              if (Access:JOBACC.TryFetch(jac:ref_Number_Key) = Level:Benign)
                  ! Found
              else ! if (Access:JOBSACC.TryFetch(jac:ref_Number_Key) = Level:Benign)
                  ! Error
                  error# = 1
                  break
              end ! if (Access:JOBSACC.TryFetch(jac:ref_Number_Key) = Level:Benign)
  
          end ! loop
      end ! if (error# = 0)
  
      case error#
      of 1
          p_web.SSV('locValidationMessage','<span class="RedBold">Accessories Validated. There is a mismatch.</span>')
      else
          p_web.SSV('locValidationMessage','<span class="GreenBold">Accessories Validated</span>')
      end
  do Value::buttonValidateAccessories
  do Comment::buttonValidateAccessories ! allows comment style to be updated.
  do Value::locValidationMessage  !1
  do Value::locValidationMessage  !1

ValidateValue::buttonValidateAccessories  Routine
    If not (1=0)
    End

Value::buttonValidateAccessories  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ReceiptFromPUP_' & p_web._nocolon('buttonValidateAccessories') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonValidateAccessories'',''receiptfrompup_buttonvalidateaccessories_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','ValidateAccessories','Validate Accessories',p_web.combine(Choose('Validate Accessories' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'SmallButtonFixed'),loc:formname,,,,loc:javascript,loc:disabled,,,,,,,,,,'nt-left')!h
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonValidateAccessories  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonValidateAccessories:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ReceiptFromPUP_' & p_web._nocolon('buttonValidateAccessories') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::locValidationMessage  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locValidationMessage = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locValidationMessage = p_web.GetValue('Value')
  End
  do ValidateValue::locValidationMessage  ! copies value to session value if valid.
  do Comment::locValidationMessage ! allows comment style to be updated.

ValidateValue::locValidationMessage  Routine
    If not (p_web.GSV('locValidationMessage') = '')
      if loc:invalid = '' then p_web.SetSessionValue('locValidationMessage',locValidationMessage).
    End

Value::locValidationMessage  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('locValidationMessage') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ReceiptFromPUP_' & p_web._nocolon('locValidationMessage') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (p_web.GSV('locValidationMessage') = '')
  ! --- DISPLAY --- locValidationMessage
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('locValidationMessage'),1) & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locValidationMessage  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locValidationMessage:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('locValidationMessage') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ReceiptFromPUP_' & p_web._nocolon('locValidationMessage') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('locValidationMessage') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('ReceiptFromPUP_nexttab_' & 0)
    locIMEINumber = p_web.GSV('locIMEINumber')
    do ValidateValue::locIMEINumber
    If loc:Invalid
      loc:retrying = 1
      do Value::locIMEINumber
      !do SendAlert
      do Comment::locIMEINumber ! allows comment style to be updated.
      !exit
    End
    locIMEIMessage = p_web.GSV('locIMEIMessage')
    do ValidateValue::locIMEIMessage
    If loc:Invalid
      loc:retrying = 1
      do Value::locIMEIMessage
      !do SendAlert
      do Comment::locIMEIMessage ! allows comment style to be updated.
      !exit
    End
    locPassword = p_web.GSV('locPassword')
    do ValidateValue::locPassword
    If loc:Invalid
      loc:retrying = 1
      do Value::locPassword
      !do SendAlert
      do Comment::locPassword ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('ReceiptFromPUP_nexttab_' & 1)
    locMSN = p_web.GSV('locMSN')
    do ValidateValue::locMSN
    If loc:Invalid
      loc:retrying = 1
      do Value::locMSN
      !do SendAlert
      do Comment::locMSN ! allows comment style to be updated.
      !exit
    End
    locNetwork = p_web.GSV('locNetwork')
    do ValidateValue::locNetwork
    If loc:Invalid
      loc:retrying = 1
      do Value::locNetwork
      !do SendAlert
      do Comment::locNetwork ! allows comment style to be updated.
      !exit
    End
    locInFault = p_web.GSV('locInFault')
    do ValidateValue::locInFault
    If loc:Invalid
      loc:retrying = 1
      do Value::locInFault
      !do SendAlert
      do Comment::locInFault ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('ReceiptFromPUP_nexttab_' & 2)
    locValidationMessage = p_web.GSV('locValidationMessage')
    do ValidateValue::locValidationMessage
    If loc:Invalid
      loc:retrying = 1
      do Value::locValidationMessage
      !do SendAlert
      do Comment::locValidationMessage ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_ReceiptFromPUP_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('ReceiptFromPUP_tab_' & 0)
    do GenerateTab0
  of lower('ReceiptFromPUP_locIMEINumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locIMEINumber
      of event:timer
        do Value::locIMEINumber
        do Comment::locIMEINumber
      else
        do Value::locIMEINumber
      end
  of lower('ReceiptFromPUP_locPassword_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locPassword
      of event:timer
        do Value::locPassword
        do Comment::locPassword
      else
        do Value::locPassword
      end
  of lower('ReceiptFromPUP_tab_' & 1)
    do GenerateTab1
  of lower('ReceiptFromPUP_locMSN_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locMSN
      of event:timer
        do Value::locMSN
        do Comment::locMSN
      else
        do Value::locMSN
      end
  of lower('ReceiptFromPUP_locNetwork_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locNetwork
      of event:timer
        do Value::locNetwork
        do Comment::locNetwork
      else
        do Value::locNetwork
      end
  of lower('ReceiptFromPUP_locInFault_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locInFault
      of event:timer
        do Value::locInFault
        do Comment::locInFault
      else
        do Value::locInFault
      end
  of lower('ReceiptFromPUP_tab_' & 2)
    do GenerateTab2
  of lower('ReceiptFromPUP_buttonValidateAccessories_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonValidateAccessories
      of event:timer
        do Value::buttonValidateAccessories
        do Comment::buttonValidateAccessories
      else
        do Value::buttonValidateAccessories
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('ReceiptFromPUP_form:ready_',1)

  p_web.SetSessionValue('ReceiptFromPUP_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_ReceiptFromPUP',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('ReceiptFromPUP_form:ready_',1)
  p_web.SetSessionValue('ReceiptFromPUP_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_ReceiptFromPUP',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('ReceiptFromPUP_form:ready_',1)
  p_web.SetSessionValue('ReceiptFromPUP_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('ReceiptFromPUP:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('ReceiptFromPUP_form:ready_',1)
  p_web.SetSessionValue('ReceiptFromPUP_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('ReceiptFromPUP:Primed',0)
  p_web.setsessionvalue('showtab_ReceiptFromPUP',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
      If not (1=0)
          If p_web.IfExistsValue('locIMEINumber')
            locIMEINumber = p_web.GetValue('locIMEINumber')
          End
      End
      If not (p_web.GSV('PasswordRequired') <> 1)
          If p_web.IfExistsValue('locPassword')
            locPassword = p_web.GetValue('locPassword')
          End
      End
    If (p_web.GSV('Hide:MSN') <> 1)
      If not (1=0)
          If p_web.IfExistsValue('locMSN')
            locMSN = p_web.GetValue('locMSN')
          End
      End
    End
      If not (1=0)
          If p_web.IfExistsValue('locNetwork')
            locNetwork = p_web.GetValue('locNetwork')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('locInFault')
            locInFault = p_web.GetValue('locInFault')
          End
      End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('ReceiptFromPUP_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  ! Validation
      if (p_web.GSV('locValidationMessage') = '')
          loc:Alert = 'You must validate the accessories'
          loc:invalid = 'buttonValidateAccessories'
          exit
      end ! if (p_web.GSV('locValidationMessage') = '')
  p_web.DeleteSessionValue('ReceiptFromPUP_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
    loc:InvalidTab += 1
    do ValidateValue::locIMEINumber
    If loc:Invalid then exit.
    do ValidateValue::locIMEIMessage
    If loc:Invalid then exit.
    do ValidateValue::locPassword
    If loc:Invalid then exit.
  ! tab = 1
    loc:InvalidTab += 1
    do ValidateValue::locMSN
    If loc:Invalid then exit.
    do ValidateValue::locNetwork
    If loc:Invalid then exit.
    do ValidateValue::locInFault
    If loc:Invalid then exit.
  ! tab = 3
    loc:InvalidTab += 1
    do ValidateValue::TagValidateLoanAccessories
    If loc:Invalid then exit.
    do ValidateValue::buttonValidateAccessories
    If loc:Invalid then exit.
    do ValidateValue::locValidationMessage
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('ReceiptFromPUP:Primed',0)
  p_web.StoreValue('locIMEINumber')
  p_web.StoreValue('locIMEIMessage')
  p_web.StoreValue('locPassword')
  p_web.StoreValue('locMSN')
  p_web.StoreValue('locNetwork')
  p_web.StoreValue('locInFault')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locValidationMessage')

  !Post Update
  do saveChanges


  do deleteVariables
PickLoanUnit         PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
tmp:LoanUnitNumber   LONG                                  !
tmp:UnitDetails      STRING(100)                           !
tmp:LoanIMEINumber   STRING(30)                            !
tmp:LoanModelNumber  STRING(30)                            !
tmp:MSN              STRING(30)                            !
tmp:LoanUnitDetails  STRING(60)                            !
tmp:LoanAccessories  STRING(100)                           !
tmp:LoanLocation     STRING(100)                           !
tmp:ReplacementValue REAL                                  !
locLoanAlertMessage  STRING(255)                           !
tmp:RemovalReason    BYTE                                  !
locRemovalAlertMessage STRING(255)                         !
tmp:LostLoanFee      REAL                                  !
locValidateMessage   STRING(255)                           !
locLoanIDOption      BYTE                                  !
locLoanIDAlert       STRING(100)                           !
locSMSDate           STRING(20)                            !
FilesOpened     Long
JOBSE2::State  USHORT
COURIER::State  USHORT
JOBS::State  USHORT
LOAN::State  USHORT
JOBEXACC::State  USHORT
LOAN_ALIAS::State  USHORT
EXCHOR48::State  USHORT
MANUFACT::State  USHORT
PARTS::State  USHORT
STOCK::State  USHORT
WARPARTS::State  USHORT
STOCKTYP::State  USHORT
EXCHHIST::State  USHORT
PRODCODE::State  USHORT
JOBSE::State  USHORT
TagFile::State  USHORT
LOANACC::State  USHORT
job:ESN:IsInvalid  Long
job:MSN:IsInvalid  Long
locUnitDetails:IsInvalid  Long
job:Charge_Type:IsInvalid  Long
job:Warranty_Charge_Type:IsInvalid  Long
tmp:LoanIMEINumber:IsInvalid  Long
locLoanAlertMessage:IsInvalid  Long
buttonPickLoanUnit:IsInvalid  Long
tmp:MSN:IsInvalid  Long
tmp:LoanUnitDetails:IsInvalid  Long
tmp:LoanAccessories:IsInvalid  Long
tmp:LoanLocation:IsInvalid  Long
tmp:ReplacementValue:IsInvalid  Long
jobe2:LoanIDNumber:IsInvalid  Long
locLoanIDAlert:IsInvalid  Long
locLoanIDOption:IsInvalid  Long
job:Loan_Courier:IsInvalid  Long
job:Loan_Consignment_Number:IsInvalid  Long
job:Loan_Despatched:IsInvalid  Long
job:Loan_Despatched_User:IsInvalid  Long
job:Loan_Despatch_Number:IsInvalid  Long
button:AmendDespatchDetails:IsInvalid  Long
locRemoveText:IsInvalid  Long
buttonRemoveAttachedUnit:IsInvalid  Long
locRemovalAlertMessage:IsInvalid  Long
tmp:RemovalReason:IsInvalid  Long
TagValidateLoanAccessories:IsInvalid  Long
buttonValidateAccessories:IsInvalid  Long
locValidateMessage:IsInvalid  Long
tmp:LostLoanFee:IsInvalid  Long
buttonConfirmLoanRemoval:IsInvalid  Long
link:SMSHistory:IsInvalid  Long
link:SendSMS:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
job:Loan_Courier_OptionView   View(COURIER)
                          Project(cou:Courier)
                        End
Loc:User            long
ThisSecwinAccess    string(252)
local       class
AllocateLoanPart    Procedure(String func:Status,Byte func:SecondUnit)
            end
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('PickLoanUnit')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'PickLoanUnit_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('PickLoanUnit','')
    p_web.DivHeader('PickLoanUnit',p_web.combine(p_web.site.style.formdiv,'fdiv'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('PickLoanUnit') = 0
        p_web.AddPreCall('PickLoanUnit')
        p_web.DivHeader('popup_PickLoanUnit','nt-hidden')
        p_web.DivHeader('PickLoanUnit',p_web.combine(p_web.site.style.formdiv,'fdiv'))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_PickLoanUnit_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_PickLoanUnit_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferPickLoanUnit',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_PickLoanUnit',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPickLoanUnit',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_PickLoanUnit',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPickLoanUnit',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_PickLoanUnit',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_PickLoanUnit',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPickLoanUnit',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_PickLoanUnit',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPickLoanUnit',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_PickLoanUnit',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_PickLoanUnit',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('PickLoanUnit')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
addLoanUnit         Routine
    data
locAuditNotes   String(255)
    code
        Access:Loan.Clearkey(loa:Ref_Number_Key)
        loa:Ref_Number    = p_web.GSV('tmp:LoanUnitNumber')
        if (Access:Loan.TryFetch(loa:Ref_Number_Key) = Level:Benign)
            ! Found
            Access:MANUFACT.Clearkey(man:Manufacturer_Key)
            man:Manufacturer    = p_web.GSV('job:Manufacturer')
            if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
                ! Found
                if (man:QALoan)
                    loa:Available = 'QA1'
                else ! if (man:QALoanLoan)
                    loa:Available = 'EXC'
                end !if (man:QALoanLoan)
                loa:Job_Number = p_web.GSV('job:Ref_Number')
                access:Loan.tryUpdate()

                if (Access:loanhist.PrimeRecord() = Level:Benign)
                    loh:Ref_Number    = tmp:LoanUnitNumber
                    loh:Date    = Today()
                    loh:Time    = Clock()
                    loh:User    = p_web.GSV('BookingUserCode')
                    if (man:QALoan)
                        loh:Status    = 'AWAITING QA. LOANED ON JOB NO: ' & p_web.GSV('job:ref_number')
                    else ! if (man:QALoanLoan)
                        loh:status = 'UNIT LOANED ON JOB NO: ' & p_web.GSV('job:Ref_number')
                    end !

                    if (Access:loanhist.TryInsert() = Level:Benign)
                        ! Inserted
                    else ! if (Access:loanhist.TryInsert() = Level:Benign)
                        ! Error
                        Access:loanhist.CancelAutoInc()
                    end ! if (Access:loanhist.TryInsert() = Level:Benign)
                end ! if (Access:loanhist.PrimeRecord() = Level:Benign)

                locAuditNotes = 'UNIT NUMBER: ' & CLip(loa:ref_number) & |
                    '<13,10>MODEL NUMBER: ' & CLip(loa:model_number) & |
                    '<13,10>I.M.E.I.: ' & CLip(loa:esn)

                if (MSNRequired(loa:Manufacturer))
                    locAuditNotes = Clip(locAuditNotes) & '<13,10>M.S.N.: ' & Clip(loa:MSN)
                end ! if (MSNRequired(loa:Manufacturer))

                locAuditNotes = clip(locAuditNotes) & '<13,10>STOCK TYPE: ' & Clip(loa:Stock_Type)

                p_web.SSV('AddToAudit:Type','LOA')
                p_web.SSV('AddToAudit:Action','LOAN UNIT ATTACHED TO JOB')
                p_web.SSV('AddToAudit:Notes',clip(locAuditNotes))
                addToAudit(p_web)

                if (man:QALoan)
                    p_web.SSV('job:Despatched','')
                    p_web.SSV('job:DespatchType','')

                    getStatus(605,0,'LOA',p_web)
                else ! if (man:QALoanLoan)

                    if (p_web.GSV('BookingSite') = 'RRC')
                        p_web.SSV('jobe:DespatchType','LOA')
                        p_web.SSV('jobe:Despatched','REA')
                        p_web.SSV('wob:ReadyToDespatch',1)
                        p_web.SSV('wob:DespatchCourier',p_web.GSV('job:Loan_Courier'))

                    else ! if (p_web.GSV('BookingSite') = 'RRC')
                        p_web.SSV('job:Loan_User',p_web.GSV('BookingUserCode'))
                        p_web.SSV('job:Loan_Despatched','')
                        p_web.SSV('job:Despatched','LAT')
                        p_web.SSV('job:DespatchType','LOA')
                    end ! if (p_web.GSV('BookingSite') = 'RRC')

                    getStatus(105,0,'LOA',p_web)

                end !if (man:QALoanLoan)

                p_web.SSV('job:Loan_unit_Number',p_web.GSV('tmp:LoanUnitNumber'))

                !               saveJob(p_web)

                loc:Alert = 'Loan Unit Added'

            else ! if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
                ! Error
            end ! if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
        else ! if (Access:Loan.TryFetch(loa:Ref_Number_Key) = Level:Benign)
            ! Error
        end ! if (Access:Loan.TryFetch(loa:Ref_Number_Key) = Level:Benign)
clearLoanDetails        routine
    p_web.SSV('tmp:MSN','')
    p_web.SSV('tmp:LoanUnitDetails','')
    p_web.SSV('tmp:LoanLocation','')
    p_web.SSV('tmp:ReplacementValue','')
    p_web.SSV('tmp:LoanIMEINumber','')
    p_web.SSV('tmp:LoanUnitNumber','')
    p_web.SSV('tmp:LoanModelNumber','')
    p_web.SSV('tmp:LoanAccessories','')
    p_web.SSV('tmp:LoanLocation','')


clearTagFile        routine

    clearTagFile(p_web)
deleteVariables     routine
    p_web.deleteSessionValue('PickLoan:FirstTime')
    p_web.deleteSessionValue('locLoanIDAlert')
    p_web.deleteSessionValue('locLoanIDOption')
    p_web.DeleteSessionValue('Hide:LoadIDAlert')
    p_web.DeleteSessionValue('ReadOnly:LoanDespatchDetails')
getLoanDetails      Routine
    p_web.SSV('locLoanAlertMessage','')

    do lookupLoanDetails


    Access:LOAN.Clearkey(loa:Ref_Number_Key)
    loa:Ref_Number    = p_web.GSV('tmp:LoanUnitNumber')
    if (Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign)
        ! Found
    else ! if (Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign)
        ! Error
    end ! if (Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign)

    if (loa:Model_Number <> p_web.GSV('job:Model_Number'))
        p_web.SSV('locLoanAlertMessage','Warning! The selected Loan Unit has a different Model Number!')
    end ! if (xch:Model_Number <> p_web.GSV('job:Model_Number')

lookupLoanDetails       Routine
    Access:Loan.Clearkey(loa:Ref_Number_Key)
    loa:Ref_Number    = p_web.GSV('tmp:LoanUnitNumber')
    if (Access:Loan.TryFetch(loa:Ref_Number_Key) = Level:Benign and loa:Ref_Number > 0)
        ! Found
        p_web.SSV('tmp:LoanIMEINumber',loa:ESN)
        p_web.SSV('tmp:MSN',loa:MSN)
        p_web.SSV('tmp:LoanUnitDetails',Clip(loa:Ref_Number) & ': ' & Clip(loa:Manufacturer) & ' ' & Clip(loa:Model_Number))
        p_web.SSV('tmp:LoanLocation',Clip(loa:Location) & ' / ' & Clip(loa:Stock_Type))
        p_web.SSV('tmp:LoanModelNumber',loa:Model_Number)

    else ! if (Access:Loan.TryFetch(loa:Ref_Number_Key) = Level:Benign)
        ! Error
        p_web.SSV('tmp:LoanIMEINumber','')
        p_web.SSV('tmp:MSN','')
        p_web.SSV('tmp:LoanUnitDetails','')
        p_web.SSV('tmp:LoanLocation','')
    end ! if (Access:Loan.TryFetch(loa:Ref_Number_Key) = Level:Benign)
OpenFiles  ROUTINE
  p_web._OpenFile(JOBSE2)
  p_web._OpenFile(COURIER)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(LOAN)
  p_web._OpenFile(JOBEXACC)
  p_web._OpenFile(LOAN_ALIAS)
  p_web._OpenFile(EXCHOR48)
  p_web._OpenFile(MANUFACT)
  p_web._OpenFile(PARTS)
  p_web._OpenFile(STOCK)
  p_web._OpenFile(WARPARTS)
  p_web._OpenFile(STOCKTYP)
  p_web._OpenFile(EXCHHIST)
  p_web._OpenFile(PRODCODE)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(TagFile)
  p_web._OpenFile(LOANACC)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(JOBSE2)
  p_Web._CloseFile(COURIER)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(LOAN)
  p_Web._CloseFile(JOBEXACC)
  p_Web._CloseFile(LOAN_ALIAS)
  p_Web._CloseFile(EXCHOR48)
  p_Web._CloseFile(MANUFACT)
  p_Web._CloseFile(PARTS)
  p_Web._CloseFile(STOCK)
  p_Web._CloseFile(WARPARTS)
  p_Web._CloseFile(STOCKTYP)
  p_Web._CloseFile(EXCHHIST)
  p_Web._CloseFile(PRODCODE)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(TagFile)
  p_Web._CloseFile(LOANACC)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE
  loc:EipClm = upper(p_web.GetValue('_EIPClm'))
  p_web.DeleteValue('_EIPClm')
  case loc:EipClm
  of ''
    case upper(p_web.GetValue('_calledfrom_'))
    of upper('TagValidateLoanAccessories')
      do Value::TagValidateLoanAccessories
    end
  end

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('PickLoanUnit_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'PickLoanUnit'
    end
    p_web.formsettings.proc = 'PickLoanUnit'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine
  do deleteVariables

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('job:ESN')
    p_web.SetPicture('job:ESN','@s20')
  End
  p_web.SetSessionPicture('job:ESN','@s20')
  If p_web.IfExistsValue('job:MSN')
    p_web.SetPicture('job:MSN','@s20')
  End
  p_web.SetSessionPicture('job:MSN','@s20')
  If p_web.IfExistsValue('job:Charge_Type')
    p_web.SetPicture('job:Charge_Type','@s30')
  End
  p_web.SetSessionPicture('job:Charge_Type','@s30')
  If p_web.IfExistsValue('job:Warranty_Charge_Type')
    p_web.SetPicture('job:Warranty_Charge_Type','@s30')
  End
  p_web.SetSessionPicture('job:Warranty_Charge_Type','@s30')
  If p_web.IfExistsValue('tmp:ReplacementValue')
    p_web.SetPicture('tmp:ReplacementValue','@n14.2')
  End
  p_web.SetSessionPicture('tmp:ReplacementValue','@n14.2')
  If p_web.IfExistsValue('jobe2:LoanIDNumber')
    p_web.SetPicture('jobe2:LoanIDNumber','@s13')
  End
  p_web.SetSessionPicture('jobe2:LoanIDNumber','@s13')
  If p_web.IfExistsValue('job:Loan_Consignment_Number')
    p_web.SetPicture('job:Loan_Consignment_Number','@s30')
  End
  p_web.SetSessionPicture('job:Loan_Consignment_Number','@s30')
  If p_web.IfExistsValue('job:Loan_Despatched')
    p_web.SetPicture('job:Loan_Despatched','@d06b')
  End
  p_web.SetSessionPicture('job:Loan_Despatched','@d06b')
  If p_web.IfExistsValue('job:Loan_Despatched_User')
    p_web.SetPicture('job:Loan_Despatched_User','@s3')
  End
  p_web.SetSessionPicture('job:Loan_Despatched_User','@s3')
  If p_web.IfExistsValue('job:Loan_Despatch_Number')
    p_web.SetPicture('job:Loan_Despatch_Number','@s8')
  End
  p_web.SetSessionPicture('job:Loan_Despatch_Number','@s8')
  If p_web.IfExistsValue('tmp:LostLoanFee')
    p_web.SetPicture('tmp:LostLoanFee','@n14.2')
  End
  p_web.SetSessionPicture('tmp:LostLoanFee','@n14.2')

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'tmp:LoanIMEINumber'
    p_web.setsessionvalue('showtab_PickLoanUnit',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(LOAN)
        p_web.setsessionvalue('tmp:LoanUnitNumber',loa:Ref_Number)
      do getLoanDetails
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.locLoanAlertMessage')
  End
  loc:TabNumber += 1
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'job:Loan_Courier'
    p_web.setsessionvalue('showtab_PickLoanUnit',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(COURIER)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.job:Loan_Consignment_Number')
  End
  If p_web.GSV('job:Loan_Unit_Number') > 0
    loc:TabNumber += 1
  End
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  if p_web.IfExistsValue('job:ESN') = 0
    p_web.SetSessionValue('job:ESN',job:ESN)
  Else
    job:ESN = p_web.GetSessionValue('job:ESN')
  End
  if p_web.IfExistsValue('job:MSN') = 0
    p_web.SetSessionValue('job:MSN',job:MSN)
  Else
    job:MSN = p_web.GetSessionValue('job:MSN')
  End
  if p_web.IfExistsValue('job:Charge_Type') = 0
    p_web.SetSessionValue('job:Charge_Type',job:Charge_Type)
  Else
    job:Charge_Type = p_web.GetSessionValue('job:Charge_Type')
  End
  if p_web.IfExistsValue('job:Warranty_Charge_Type') = 0
    p_web.SetSessionValue('job:Warranty_Charge_Type',job:Warranty_Charge_Type)
  Else
    job:Warranty_Charge_Type = p_web.GetSessionValue('job:Warranty_Charge_Type')
  End
  if p_web.IfExistsValue('tmp:LoanIMEINumber') = 0
    p_web.SetSessionValue('tmp:LoanIMEINumber',tmp:LoanIMEINumber)
  Else
    tmp:LoanIMEINumber = p_web.GetSessionValue('tmp:LoanIMEINumber')
  End
  if p_web.IfExistsValue('locLoanAlertMessage') = 0
    p_web.SetSessionValue('locLoanAlertMessage',locLoanAlertMessage)
  Else
    locLoanAlertMessage = p_web.GetSessionValue('locLoanAlertMessage')
  End
  if p_web.IfExistsValue('tmp:MSN') = 0
    p_web.SetSessionValue('tmp:MSN',tmp:MSN)
  Else
    tmp:MSN = p_web.GetSessionValue('tmp:MSN')
  End
  if p_web.IfExistsValue('tmp:LoanUnitDetails') = 0
    p_web.SetSessionValue('tmp:LoanUnitDetails',tmp:LoanUnitDetails)
  Else
    tmp:LoanUnitDetails = p_web.GetSessionValue('tmp:LoanUnitDetails')
  End
  if p_web.IfExistsValue('tmp:LoanAccessories') = 0
    p_web.SetSessionValue('tmp:LoanAccessories',tmp:LoanAccessories)
  Else
    tmp:LoanAccessories = p_web.GetSessionValue('tmp:LoanAccessories')
  End
  if p_web.IfExistsValue('tmp:LoanLocation') = 0
    p_web.SetSessionValue('tmp:LoanLocation',tmp:LoanLocation)
  Else
    tmp:LoanLocation = p_web.GetSessionValue('tmp:LoanLocation')
  End
  if p_web.IfExistsValue('tmp:ReplacementValue') = 0
    p_web.SetSessionValue('tmp:ReplacementValue',tmp:ReplacementValue)
  Else
    tmp:ReplacementValue = p_web.GetSessionValue('tmp:ReplacementValue')
  End
  if p_web.IfExistsValue('jobe2:LoanIDNumber') = 0
    p_web.SetSessionValue('jobe2:LoanIDNumber',jobe2:LoanIDNumber)
  Else
    jobe2:LoanIDNumber = p_web.GetSessionValue('jobe2:LoanIDNumber')
  End
  if p_web.IfExistsValue('locLoanIDAlert') = 0
    p_web.SetSessionValue('locLoanIDAlert',locLoanIDAlert)
  Else
    locLoanIDAlert = p_web.GetSessionValue('locLoanIDAlert')
  End
  if p_web.IfExistsValue('locLoanIDOption') = 0
    p_web.SetSessionValue('locLoanIDOption',locLoanIDOption)
  Else
    locLoanIDOption = p_web.GetSessionValue('locLoanIDOption')
  End
  if p_web.IfExistsValue('job:Loan_Courier') = 0
    p_web.SetSessionValue('job:Loan_Courier',job:Loan_Courier)
  Else
    job:Loan_Courier = p_web.GetSessionValue('job:Loan_Courier')
  End
  if p_web.IfExistsValue('job:Loan_Consignment_Number') = 0
    p_web.SetSessionValue('job:Loan_Consignment_Number',job:Loan_Consignment_Number)
  Else
    job:Loan_Consignment_Number = p_web.GetSessionValue('job:Loan_Consignment_Number')
  End
  if p_web.IfExistsValue('job:Loan_Despatched') = 0
    p_web.SetSessionValue('job:Loan_Despatched',job:Loan_Despatched)
  Else
    job:Loan_Despatched = p_web.GetSessionValue('job:Loan_Despatched')
  End
  if p_web.IfExistsValue('job:Loan_Despatched_User') = 0
    p_web.SetSessionValue('job:Loan_Despatched_User',job:Loan_Despatched_User)
  Else
    job:Loan_Despatched_User = p_web.GetSessionValue('job:Loan_Despatched_User')
  End
  if p_web.IfExistsValue('job:Loan_Despatch_Number') = 0
    p_web.SetSessionValue('job:Loan_Despatch_Number',job:Loan_Despatch_Number)
  Else
    job:Loan_Despatch_Number = p_web.GetSessionValue('job:Loan_Despatch_Number')
  End
  if p_web.IfExistsValue('locRemovalAlertMessage') = 0
    p_web.SetSessionValue('locRemovalAlertMessage',locRemovalAlertMessage)
  Else
    locRemovalAlertMessage = p_web.GetSessionValue('locRemovalAlertMessage')
  End
  if p_web.IfExistsValue('tmp:RemovalReason') = 0
    p_web.SetSessionValue('tmp:RemovalReason',tmp:RemovalReason)
  Else
    tmp:RemovalReason = p_web.GetSessionValue('tmp:RemovalReason')
  End
  if p_web.IfExistsValue('locValidateMessage') = 0
    p_web.SetSessionValue('locValidateMessage',locValidateMessage)
  Else
    locValidateMessage = p_web.GetSessionValue('locValidateMessage')
  End
  if p_web.IfExistsValue('tmp:LostLoanFee') = 0
    p_web.SetSessionValue('tmp:LostLoanFee',tmp:LostLoanFee)
  Else
    tmp:LostLoanFee = p_web.GetSessionValue('tmp:LostLoanFee')
  End

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('job:ESN')
    job:ESN = p_web.GetValue('job:ESN')
    p_web.SetSessionValue('job:ESN',job:ESN)
  Else
    job:ESN = p_web.GetSessionValue('job:ESN')
  End
  if p_web.IfExistsValue('job:MSN')
    job:MSN = p_web.GetValue('job:MSN')
    p_web.SetSessionValue('job:MSN',job:MSN)
  Else
    job:MSN = p_web.GetSessionValue('job:MSN')
  End
  if p_web.IfExistsValue('job:Charge_Type')
    job:Charge_Type = p_web.GetValue('job:Charge_Type')
    p_web.SetSessionValue('job:Charge_Type',job:Charge_Type)
  Else
    job:Charge_Type = p_web.GetSessionValue('job:Charge_Type')
  End
  if p_web.IfExistsValue('job:Warranty_Charge_Type')
    job:Warranty_Charge_Type = p_web.GetValue('job:Warranty_Charge_Type')
    p_web.SetSessionValue('job:Warranty_Charge_Type',job:Warranty_Charge_Type)
  Else
    job:Warranty_Charge_Type = p_web.GetSessionValue('job:Warranty_Charge_Type')
  End
  if p_web.IfExistsValue('tmp:LoanIMEINumber')
    tmp:LoanIMEINumber = p_web.GetValue('tmp:LoanIMEINumber')
    p_web.SetSessionValue('tmp:LoanIMEINumber',tmp:LoanIMEINumber)
  Else
    tmp:LoanIMEINumber = p_web.GetSessionValue('tmp:LoanIMEINumber')
  End
  if p_web.IfExistsValue('locLoanAlertMessage')
    locLoanAlertMessage = p_web.GetValue('locLoanAlertMessage')
    p_web.SetSessionValue('locLoanAlertMessage',locLoanAlertMessage)
  Else
    locLoanAlertMessage = p_web.GetSessionValue('locLoanAlertMessage')
  End
  if p_web.IfExistsValue('tmp:MSN')
    tmp:MSN = p_web.GetValue('tmp:MSN')
    p_web.SetSessionValue('tmp:MSN',tmp:MSN)
  Else
    tmp:MSN = p_web.GetSessionValue('tmp:MSN')
  End
  if p_web.IfExistsValue('tmp:LoanUnitDetails')
    tmp:LoanUnitDetails = p_web.GetValue('tmp:LoanUnitDetails')
    p_web.SetSessionValue('tmp:LoanUnitDetails',tmp:LoanUnitDetails)
  Else
    tmp:LoanUnitDetails = p_web.GetSessionValue('tmp:LoanUnitDetails')
  End
  if p_web.IfExistsValue('tmp:LoanAccessories')
    tmp:LoanAccessories = p_web.GetValue('tmp:LoanAccessories')
    p_web.SetSessionValue('tmp:LoanAccessories',tmp:LoanAccessories)
  Else
    tmp:LoanAccessories = p_web.GetSessionValue('tmp:LoanAccessories')
  End
  if p_web.IfExistsValue('tmp:LoanLocation')
    tmp:LoanLocation = p_web.GetValue('tmp:LoanLocation')
    p_web.SetSessionValue('tmp:LoanLocation',tmp:LoanLocation)
  Else
    tmp:LoanLocation = p_web.GetSessionValue('tmp:LoanLocation')
  End
  if p_web.IfExistsValue('tmp:ReplacementValue')
    tmp:ReplacementValue = p_web.dformat(clip(p_web.GetValue('tmp:ReplacementValue')),'@n14.2')
    p_web.SetSessionValue('tmp:ReplacementValue',tmp:ReplacementValue)
  Else
    tmp:ReplacementValue = p_web.GetSessionValue('tmp:ReplacementValue')
  End
  if p_web.IfExistsValue('jobe2:LoanIDNumber')
    jobe2:LoanIDNumber = p_web.GetValue('jobe2:LoanIDNumber')
    p_web.SetSessionValue('jobe2:LoanIDNumber',jobe2:LoanIDNumber)
  Else
    jobe2:LoanIDNumber = p_web.GetSessionValue('jobe2:LoanIDNumber')
  End
  if p_web.IfExistsValue('locLoanIDAlert')
    locLoanIDAlert = p_web.GetValue('locLoanIDAlert')
    p_web.SetSessionValue('locLoanIDAlert',locLoanIDAlert)
  Else
    locLoanIDAlert = p_web.GetSessionValue('locLoanIDAlert')
  End
  if p_web.IfExistsValue('locLoanIDOption')
    locLoanIDOption = p_web.GetValue('locLoanIDOption')
    p_web.SetSessionValue('locLoanIDOption',locLoanIDOption)
  Else
    locLoanIDOption = p_web.GetSessionValue('locLoanIDOption')
  End
  if p_web.IfExistsValue('job:Loan_Courier')
    job:Loan_Courier = p_web.GetValue('job:Loan_Courier')
    p_web.SetSessionValue('job:Loan_Courier',job:Loan_Courier)
  Else
    job:Loan_Courier = p_web.GetSessionValue('job:Loan_Courier')
  End
  if p_web.IfExistsValue('job:Loan_Consignment_Number')
    job:Loan_Consignment_Number = p_web.GetValue('job:Loan_Consignment_Number')
    p_web.SetSessionValue('job:Loan_Consignment_Number',job:Loan_Consignment_Number)
  Else
    job:Loan_Consignment_Number = p_web.GetSessionValue('job:Loan_Consignment_Number')
  End
  if p_web.IfExistsValue('job:Loan_Despatched')
    job:Loan_Despatched = p_web.dformat(clip(p_web.GetValue('job:Loan_Despatched')),'@d06b')
    p_web.SetSessionValue('job:Loan_Despatched',job:Loan_Despatched)
  Else
    job:Loan_Despatched = p_web.GetSessionValue('job:Loan_Despatched')
  End
  if p_web.IfExistsValue('job:Loan_Despatched_User')
    job:Loan_Despatched_User = p_web.GetValue('job:Loan_Despatched_User')
    p_web.SetSessionValue('job:Loan_Despatched_User',job:Loan_Despatched_User)
  Else
    job:Loan_Despatched_User = p_web.GetSessionValue('job:Loan_Despatched_User')
  End
  if p_web.IfExistsValue('job:Loan_Despatch_Number')
    job:Loan_Despatch_Number = p_web.GetValue('job:Loan_Despatch_Number')
    p_web.SetSessionValue('job:Loan_Despatch_Number',job:Loan_Despatch_Number)
  Else
    job:Loan_Despatch_Number = p_web.GetSessionValue('job:Loan_Despatch_Number')
  End
  if p_web.IfExistsValue('locRemovalAlertMessage')
    locRemovalAlertMessage = p_web.GetValue('locRemovalAlertMessage')
    p_web.SetSessionValue('locRemovalAlertMessage',locRemovalAlertMessage)
  Else
    locRemovalAlertMessage = p_web.GetSessionValue('locRemovalAlertMessage')
  End
  if p_web.IfExistsValue('tmp:RemovalReason')
    tmp:RemovalReason = p_web.GetValue('tmp:RemovalReason')
    p_web.SetSessionValue('tmp:RemovalReason',tmp:RemovalReason)
  Else
    tmp:RemovalReason = p_web.GetSessionValue('tmp:RemovalReason')
  End
  if p_web.IfExistsValue('locValidateMessage')
    locValidateMessage = p_web.GetValue('locValidateMessage')
    p_web.SetSessionValue('locValidateMessage',locValidateMessage)
  Else
    locValidateMessage = p_web.GetSessionValue('locValidateMessage')
  End
  if p_web.IfExistsValue('tmp:LostLoanFee')
    tmp:LostLoanFee = p_web.dformat(clip(p_web.GetValue('tmp:LostLoanFee')),'@n14.2')
    p_web.SetSessionValue('tmp:LostLoanFee',tmp:LostLoanFee)
  Else
    tmp:LostLoanFee = p_web.GetSessionValue('tmp:LostLoanFee')
  End

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('PickLoanUnit_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'ViewJob'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('PickLoanUnit_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('PickLoanUnit_ChainTo')
    loc:formaction = p_web.GetSessionValue('PickLoanUnit_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'ViewJob'

GenerateForm   Routine
  do LoadRelatedRecords
  ! Security Checks
  
      if (p_web.GSV('PickLoan:FirstTime') = 0)
          if (p_web.GSV('Job:ViewOnly') = 1)
              p_web.SSV('ReadOnly:LoanIMEINumber',1)
          else !if (p_web.GSV('Job:ViewOnly') = 1)
              if (securityCheckFailed(p_web.GSV('BookingUserPassword'),'JOBS - AMEND LOAN UNIT'))
                  p_web.SSV('ReadOnly:LoanIMEINumber',1)
              else
                  p_web.SSV('ReadOnly:LoanIMEINumber',0)
              end ! if (securityCheckFailed(p_web.GSV('BookingUserPassword'),' '))
          end !if (p_web.GSV('Job:ViewOnly') = 1)
  
          p_web.SSV('tmp:LoanUnitNumber',p_web.GSV('job:Loan_Unit_Number'))
  
          if (p_web.GSV('job:Loan_Unit_Number') > 0)
              do lookupLoanDetails
          else
              do clearLoanDetails
          end
  
          p_web.SSV('locLoanAlertMessage','')
          p_web.SSV('locRemoveAlertMessage','')
          p_web.SSV('Hide:RemovalReason',1)
          p_web.SSV('tmp:RemovalReason',0)
          p_web.SSV('tmp:NoUnitAvailable',0)
          p_web.SSV('locValidateMessage','')
          p_web.SSV('Hide:LostLoanFee',1)
          p_web.SSV('Hide:ConfirmRemovalButton',1)
          p_web.SSV('Hide:ValidateAccessoriesButton',0)
          p_web.SSV('locLoanIDOption',0)
          p_web.SSV('locLoanIDAlert','')
          p_web.SSV('Hide:LoanIDAlert',1)
  
          do clearTagFile
          p_web.SSV('PickLoan:FirstTime',1)
      end !if (p_web.GSV('PickLoan:FirstTime') = 0)
  
      if (p_web.GSV('job:Loan_Unit_Number') > 0 And ((p_web.GSV('BookingSite') = 'RRC' And p_web.GSV('jobe:Despatched') = 'REA' And p_web.GSV('jobe:DespatchedType') = 'EXC') Or |
          (p_web.GSV('BookingSite') = 'ARC' And p_web.GSV('job:Despatched') = 'REA' And p_web.GSV('job:Despatch_Type') = 'EXC')))
          packet = clip(packet) & '<script language="JavaScript" type="text/javascript">alert("You cannot attached a Loan Unit until the Exchange Unit has been despatched.")</script>'
          do sendPacket
          p_web.SSV('ReadOnly:LoanIMEINumber',1)
      End !
  
      p_web.SSV('ReadOnly:LoanDespatchDetails',1)
 tmp:LoanIMEINumber = p_web.RestoreValue('tmp:LoanIMEINumber')
 locLoanAlertMessage = p_web.RestoreValue('locLoanAlertMessage')
 tmp:MSN = p_web.RestoreValue('tmp:MSN')
 tmp:LoanUnitDetails = p_web.RestoreValue('tmp:LoanUnitDetails')
 tmp:LoanAccessories = p_web.RestoreValue('tmp:LoanAccessories')
 tmp:LoanLocation = p_web.RestoreValue('tmp:LoanLocation')
 tmp:ReplacementValue = p_web.RestoreValue('tmp:ReplacementValue')
 locLoanIDAlert = p_web.RestoreValue('locLoanIDAlert')
 locLoanIDOption = p_web.RestoreValue('locLoanIDOption')
 locRemovalAlertMessage = p_web.RestoreValue('locRemovalAlertMessage')
 tmp:RemovalReason = p_web.RestoreValue('tmp:RemovalReason')
 locValidateMessage = p_web.RestoreValue('locValidateMessage')
 tmp:LostLoanFee = p_web.RestoreValue('tmp:LostLoanFee')
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Pick Loan Unit') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Pick Loan Unit',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_PickLoanUnit',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_PickLoanUnit0_div')&'">'&p_web.Translate('Job Details')&'</a></li>'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_PickLoanUnit1_div')&'">'&p_web.Translate('Loan Unit Details')&'</a></li>'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_PickLoanUnit2_div')&'">'&p_web.Translate('Loan ID Details')&'</a></li>'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_PickLoanUnit3_div')&'">'&p_web.Translate('Despatch Details')&'</a></li>'& CRLF
      If p_web.GSV('job:Loan_Unit_Number') > 0
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_PickLoanUnit4_div')&'">'&p_web.Translate('Remove Loan Unit')&'</a></li>'& CRLF
      End
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_PickLoanUnit5_div')&'">'&p_web.Translate()&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  do GenerateTab3
  do GenerateTab4
  do GenerateTab5
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="PickLoanUnit_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('PickLoanUnit_TagValidateLoanAccessories_embedded_div')&''');'
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('PickLoanUnit_TagValidateLoanAccessories_embedded_div')&''');'
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.cancel();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CancelButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="PickLoanUnit_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'PickLoanUnit_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="PickLoanUnit_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('PickLoanUnit_TagValidateLoanAccessories_embedded_div')&''');'
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'PickLoanUnit_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
    If upper(p_web.getvalue('LookupFile'))='LOAN'
          If Not (1=0)
            p_web.SetValue('SelectField',clip(loc:formname) & '.jobe2:LoanIDNumber')
          End
    End
  Else
    If False
    Else
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_PickLoanUnit')>0,p_web.GSV('showtab_PickLoanUnit'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_PickLoanUnit'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_PickLoanUnit') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_PickLoanUnit'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_PickLoanUnit')>0,p_web.GSV('showtab_PickLoanUnit'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_PickLoanUnit') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Job Details') & ''''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Loan Unit Details') & ''''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Loan ID Details') & ''''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Despatch Details') & ''''
      If p_web.GSV('job:Loan_Unit_Number') > 0
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Remove Loan Unit') & ''''
      End
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_PickLoanUnit_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_PickLoanUnit')>0,p_web.GSV('showtab_PickLoanUnit'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"PickLoanUnit",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_PickLoanUnit')>0,p_web.GSV('showtab_PickLoanUnit'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_PickLoanUnit_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('PickLoanUnit') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('PickLoanUnit')
    p_web.SetValue('_CallPopups',2)
    If p_web.GetPreCall('TagValidateLoanAccessories') = 0
      p_web.SetValue('TagValidateLoanAccessories:NoForm',1)
      p_web.SetValue('TagValidateLoanAccessories:FormName',loc:formname)
      p_web.SetValue('TagValidateLoanAccessories:parentIs','Form')
      p_web.SetValue('_parentProc','PickLoanUnit')
      TagValidateLoanAccessories(p_web)
      p_web.SetValue('_CallPopups',0)
      p_web.DeleteValue('TagValidateLoanAccessories:NoForm')
      p_web.DeleteValue('TagValidateLoanAccessories:FormName')
      p_web.DeleteValue('TagValidateLoanAccessories:parentIs')
      p_web.DeleteValue('_parentProc')
    End
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Job Details')&'</a></h3>' & CRLF & p_web.DivHeader('tab_PickLoanUnit0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_PickLoanUnit0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_PickLoanUnit0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_PickLoanUnit0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Job Details')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_PickLoanUnit0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Job Details')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_PickLoanUnit0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Job Details')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_PickLoanUnit0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:ESN
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:ESN
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::job:ESN
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:MSN
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:MSN
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::job:MSN
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locUnitDetails
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locUnitDetails
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locUnitDetails
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Charge_Type
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Charge_Type
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::job:Charge_Type
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Warranty_Charge_Type
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Warranty_Charge_Type
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::job:Warranty_Charge_Type
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab1  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Loan Unit Details')&'</a></h3>' & CRLF & p_web.DivHeader('tab_PickLoanUnit1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_PickLoanUnit1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_PickLoanUnit1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_PickLoanUnit1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Loan Unit Details')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_PickLoanUnit1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Loan Unit Details')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_PickLoanUnit1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Loan Unit Details')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_PickLoanUnit1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:LoanIMEINumber
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:LoanIMEINumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:LoanIMEINumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td rowspan="'&clip(4)&'"'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locLoanAlertMessage
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td rowspan="'&clip(4)&'"'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locLoanAlertMessage
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td rowspan="'&clip(4)&'"'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locLoanAlertMessage
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      If p_web.GSV('ReadOnly:LoanIMEINumber') <> 1
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::buttonPickLoanUnit
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::buttonPickLoanUnit
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
      If p_web.GSV('tmp:MSN') <> ''
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:MSN
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:MSN
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:MSN
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:LoanUnitDetails
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:LoanUnitDetails
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:LoanUnitDetails
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:LoanAccessories
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:LoanAccessories
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:LoanAccessories
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:LoanLocation
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:LoanLocation
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:LoanLocation
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:ReplacementValue
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:ReplacementValue
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:ReplacementValue
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab2  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Loan ID Details')&'</a></h3>' & CRLF & p_web.DivHeader('tab_PickLoanUnit2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_PickLoanUnit2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_PickLoanUnit2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_PickLoanUnit2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Loan ID Details')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_PickLoanUnit2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Loan ID Details')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_PickLoanUnit2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Loan ID Details')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_PickLoanUnit2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jobe2:LoanIDNumber
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jobe2:LoanIDNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::jobe2:LoanIDNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locLoanIDAlert
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locLoanIDAlert
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locLoanIDOption
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locLoanIDOption
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locLoanIDOption
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab3  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Despatch Details')&'</a></h3>' & CRLF & p_web.DivHeader('tab_PickLoanUnit3',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_PickLoanUnit3',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_PickLoanUnit3',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_PickLoanUnit3',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Despatch Details')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_PickLoanUnit3',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Despatch Details')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_PickLoanUnit3',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Despatch Details')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_PickLoanUnit3',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Loan_Courier
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Loan_Courier
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::job:Loan_Courier
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Loan_Consignment_Number
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Loan_Consignment_Number
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::job:Loan_Consignment_Number
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Loan_Despatched
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Loan_Despatched
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::job:Loan_Despatched
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Loan_Despatched_User
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Loan_Despatched_User
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::job:Loan_Despatched_User
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Loan_Despatch_Number
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Loan_Despatch_Number
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::job:Loan_Despatch_Number
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::button:AmendDespatchDetails
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::button:AmendDespatchDetails
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab4  Routine
  If p_web.GSV('job:Loan_Unit_Number') > 0
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Remove Loan Unit')&'</a></h3>' & CRLF & p_web.DivHeader('tab_PickLoanUnit4',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_PickLoanUnit4',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_PickLoanUnit4',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_PickLoanUnit4',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Remove Loan Unit')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_PickLoanUnit4',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Remove Loan Unit')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_PickLoanUnit4',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Remove Loan Unit')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_PickLoanUnit4',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::locRemoveText
        do Comment::locRemoveText
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::buttonRemoveAttachedUnit
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::buttonRemoveAttachedUnit
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::buttonRemoveAttachedUnit
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td rowspan="'&clip(5)&'" valign="top"'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locRemovalAlertMessage
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td rowspan="'&clip(5)&'"'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locRemovalAlertMessage
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td rowspan="'&clip(5)&'"'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locRemovalAlertMessage
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:RemovalReason
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:RemovalReason
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:RemovalReason
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::TagValidateLoanAccessories
        do Value::TagValidateLoanAccessories
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::buttonValidateAccessories
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::buttonValidateAccessories
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locValidateMessage
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locValidateMessage
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locValidateMessage
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:LostLoanFee
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:LostLoanFee
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:LostLoanFee
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::buttonConfirmLoanRemoval
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::buttonConfirmLoanRemoval
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::buttonConfirmLoanRemoval
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
  end
GenerateTab5  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate()&'</a></h3>' & CRLF & p_web.DivHeader('tab_PickLoanUnit5',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_PickLoanUnit5',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_PickLoanUnit5',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_PickLoanUnit5',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,)
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_PickLoanUnit5',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend)
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_PickLoanUnit5',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain">' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_PickLoanUnit5',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::link:SMSHistory
        do Comment::link:SMSHistory
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::link:SendSMS
        do Comment::link:SendSMS
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Prompt::job:ESN  Routine
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('job:ESN') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('IMEI Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:ESN  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:ESN = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s20
    job:ESN = p_web.Dformat(p_web.GetValue('Value'),'@s20')
  End
  do ValidateValue::job:ESN  ! copies value to session value if valid.
  do Comment::job:ESN ! allows comment style to be updated.

ValidateValue::job:ESN  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('job:ESN',job:ESN).
    End

Value::job:ESN  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('job:ESN') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- job:ESN
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:ESN'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::job:ESN  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if job:ESN:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('job:ESN') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::job:MSN  Routine
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('job:MSN') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('M.S.N.'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:MSN  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:MSN = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s20
    job:MSN = p_web.Dformat(p_web.GetValue('Value'),'@s20')
  End
  do ValidateValue::job:MSN  ! copies value to session value if valid.
  do Comment::job:MSN ! allows comment style to be updated.

ValidateValue::job:MSN  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('job:MSN',job:MSN).
    End

Value::job:MSN  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('job:MSN') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- job:MSN
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:MSN'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::job:MSN  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if job:MSN:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('job:MSN') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locUnitDetails  Routine
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('locUnitDetails') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Unit Details'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locUnitDetails  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::locUnitDetails  ! copies value to session value if valid.
  do Comment::locUnitDetails ! allows comment style to be updated.

ValidateValue::locUnitDetails  Routine
    If not (1=0)
    End

Value::locUnitDetails  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('locUnitDetails') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="locUnitDetails" class="'&clip('green bold')&'"'&clip(loc:extra)&'>' & p_web.Translate(p_web.GSV('job:Manufacturer') & ' ' & p_web.GSV('job:Model_Number') & ' - ' & p_web.GSV('job:Unit_Type'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locUnitDetails  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locUnitDetails:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('locUnitDetails') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::job:Charge_Type  Routine
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('job:Charge_Type') & '_prompt',Choose(p_web.GSV('job:Chargeable_Job') <> 'YES','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('job:Chargeable_Job') <> 'YES','',p_web.Translate('Charge Type'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Charge_Type  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Charge_Type = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    job:Charge_Type = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::job:Charge_Type  ! copies value to session value if valid.
  do Comment::job:Charge_Type ! allows comment style to be updated.

ValidateValue::job:Charge_Type  Routine
    If not (p_web.GSV('job:Chargeable_Job') <> 'YES')
      if loc:invalid = '' then p_web.SetSessionValue('job:Charge_Type',job:Charge_Type).
    End

Value::job:Charge_Type  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('job:Chargeable_Job') <> 'YES','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('job:Charge_Type') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (p_web.GSV('job:Chargeable_Job') <> 'YES')
  ! --- DISPLAY --- job:Charge_Type
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Charge_Type'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::job:Charge_Type  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if job:Charge_Type:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('job:Chargeable_Job') <> 'YES','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('job:Charge_Type') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('job:Chargeable_Job') <> 'YES'
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::job:Warranty_Charge_Type  Routine
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('job:Warranty_Charge_Type') & '_prompt',Choose(p_web.GSV('job:Warranty_Job') <> 'YES','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('job:Warranty_Job') <> 'YES','',p_web.Translate('Warr Charge Type'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Warranty_Charge_Type  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Warranty_Charge_Type = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    job:Warranty_Charge_Type = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::job:Warranty_Charge_Type  ! copies value to session value if valid.
  do Comment::job:Warranty_Charge_Type ! allows comment style to be updated.

ValidateValue::job:Warranty_Charge_Type  Routine
    If not (p_web.GSV('job:Warranty_Job') <> 'YES')
      if loc:invalid = '' then p_web.SetSessionValue('job:Warranty_Charge_Type',job:Warranty_Charge_Type).
    End

Value::job:Warranty_Charge_Type  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('job:Warranty_Job') <> 'YES','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('job:Warranty_Charge_Type') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (p_web.GSV('job:Warranty_Job') <> 'YES')
  ! --- DISPLAY --- job:Warranty_Charge_Type
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Warranty_Charge_Type'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::job:Warranty_Charge_Type  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if job:Warranty_Charge_Type:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('job:Warranty_Job') <> 'YES','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('job:Warranty_Charge_Type') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('job:Warranty_Job') <> 'YES'
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:LoanIMEINumber  Routine
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:LoanIMEINumber') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Loan IMEI No'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:LoanIMEINumber  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:LoanIMEINumber = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:LoanIMEINumber = p_web.GetValue('Value')
  ElsIf p_web.RequestAjax = 1 and p_web.IfExistsValue('loa:ESN')
    tmp:LoanIMEINumber = p_web.GetValue('loa:ESN')
  ElsIf p_web.RequestAjax = 1
    tmp:LoanIMEINumber = loa:ESN
  End
  do ValidateValue::tmp:LoanIMEINumber  ! copies value to session value if valid.
  ! Validate IMEI
  p_web.SSV('locLoanAlertMessage','')
  Access:Loan.Clearkey(loa:ESN_Only_Key)
  loa:ESN    = p_web.GSV('tmp:LoanIMEINumber')
  if (Access:Loan.TryFetch(loa:ESN_Only_Key) = Level:Benign)
      ! Found
      if (loa:Location <> p_web.GSV('BookingSiteLocation'))
          p_web.SSV('locLoanAlertMessage','Selected IMEI is from a different location.')
      else !  !if (loa:Location <> p_web.GSV('BookingSiteLocation'))
          if (loa:Available = 'AVL')
              Access:STOCKTYP.Clearkey(stp:Stock_Type_Key)
              stp:Stock_Type    = loa:Stock_Type
              if (Access:STOCKTYP.TryFetch(stp:Stock_Type_Key) = Level:Benign)
                  ! Found
                  if (stp:Available <> 1)
                      p_web.SSV('locLoanAlertMessage','Selected Stock Type is not available')
                  else ! if (stp:Available <> 1)
                      p_web.SSV('tmp:LoanUnitNumber',loa:Ref_Number)
                  end ! if (stp:Available <> 1)
              else ! if (Access:STOCKTYP.TryFetch(stp:Stock_Type_Key) = Level:Benign)
                  ! Error
              end ! if (Access:STOCKTYP.TryFetch(stp:Stock_Type_Key) = Level:Benign)
          else ! if (loa:Available = 'AVL')
              p_web.SSV('locLoanAlertMessage','Selected IMEI is not available')
          end ! if (loa:Available = 'AVL')
      end !if (loa:Location <> p_web.GSV('BookingSiteLocation'))
  else ! if (Access:Loan.TryFetch(loa:ESN_Only_Key) = Level:Benign)
      ! Error
      p_web.SSV('locLoanAlertMessage','Cannot find the selected IMEI Number')
  end ! if (Access:Loan.TryFetch(loa:ESN_Only_Key) = Level:Benign)
  
  if (p_web.GSV('locLoanAlertMessage') = '')
      do getLoanDetails
  else
      do clearLoanDetails
  end ! if (p_web.GSV('locLoanAlertMessage') = '')
  p_Web.SetValue('lookupfield','tmp:LoanIMEINumber')
  do AfterLookup
  do Value::tmp:LoanIMEINumber
  do SendAlert
  do Comment::tmp:LoanIMEINumber
  do Prompt::locLoanAlertMessage
  do Value::locLoanAlertMessage  !1
  do Value::tmp:LoanLocation  !1
  do Value::tmp:LoanUnitDetails  !1
  do Value::tmp:ReplacementValue  !1

ValidateValue::tmp:LoanIMEINumber  Routine
    If not (1=0)
    tmp:LoanIMEINumber = Upper(tmp:LoanIMEINumber)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:LoanIMEINumber',tmp:LoanIMEINumber).
    End

Value::tmp:LoanIMEINumber  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:LoanIMEINumber') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.GSV('ReadOnly:LoanIMEINumber') = 1 OR p_web.GSV('job:Loan_Unit_Number') > 0 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    tmp:LoanIMEINumber = p_web.RestoreValue('tmp:LoanIMEINumber')
    do ValidateValue::tmp:LoanIMEINumber
    If tmp:LoanIMEINumber:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- tmp:LoanIMEINumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:LoanIMEINumber') = 1 OR p_web.GSV('job:Loan_Unit_Number') > 0,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:LoanIMEINumber'',''pickloanunit_tmp:loanimeinumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:LoanIMEINumber')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','tmp:LoanIMEINumber',p_web.GetSessionValue('tmp:LoanIMEINumber'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'!4
    if not loc:viewonly and not loc:readonly
      loc:fieldclass = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('FormLoanUnitFilter')&'?LookupField=tmp:LoanIMEINumber&Tab=5&ForeignField=loa:ESN&_sort=&Refresh=sort'),,,,,loc:fieldclass)
    End
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:LoanIMEINumber  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:LoanIMEINumber:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:LoanIMEINumber') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locLoanAlertMessage  Routine
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('locLoanAlertMessage') & '_prompt',Choose(p_web.GSV('locLoanAlertMessage') = '','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('locLoanAlertMessage') = '','',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locLoanAlertMessage  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locLoanAlertMessage = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locLoanAlertMessage = p_web.GetValue('Value')
  End
  do ValidateValue::locLoanAlertMessage  ! copies value to session value if valid.
  do Comment::locLoanAlertMessage ! allows comment style to be updated.

ValidateValue::locLoanAlertMessage  Routine
    If not (p_web.GSV('locLoanAlertMessage') = '')
      if loc:invalid = '' then p_web.SetSessionValue('locLoanAlertMessage',locLoanAlertMessage).
    End

Value::locLoanAlertMessage  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('locLoanAlertMessage') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('locLoanAlertMessage') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry','RedBoldSmall')
  loc:extra = ''
  If Not (p_web.GSV('locLoanAlertMessage') = '')
  ! --- DISPLAY --- locLoanAlertMessage
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="locLoanAlertMessage" class="'&clip('red bold')&'"'&clip(loc:extra)&'>' & p_web.Translate(p_web.GSV('locLoanAlertMessage'),1) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locLoanAlertMessage  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locLoanAlertMessage:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('locLoanAlertMessage') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('locLoanAlertMessage') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('locLoanAlertMessage') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonPickLoanUnit  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonPickLoanUnit  ! copies value to session value if valid.
  do Value::buttonPickLoanUnit
  do Comment::buttonPickLoanUnit ! allows comment style to be updated.

ValidateValue::buttonPickLoanUnit  Routine
  If p_web.GSV('ReadOnly:LoanIMEINumber') <> 1
    If not (1 or p_web.GSV('job:Loan_Unit_Number') > 0)
    End
  End

Value::buttonPickLoanUnit  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(1 or p_web.GSV('job:Loan_Unit_Number') > 0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('buttonPickLoanUnit') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1 or p_web.GSV('job:Loan_Unit_Number') > 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonPickLoanUnit'',''pickloanunit_buttonpickloanunit_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','PickLoanUnit','Pick Loan Unit',p_web.combine(Choose('Pick Loan Unit' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'SmallButtonFixedIcon'),loc:formname,,,p_web.WindowOpen(clip('FormLoanUnitFilter?LookupField=tmp:LoanIMEINumber&Tab=2&ForeignField=loa:ESN&_sort=loa:ESN&Refresh=sort&LookupFrom=PickLoanUnit&')&''&'','_self'),,loc:disabled,'images\packinsert.png',,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonPickLoanUnit  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonPickLoanUnit:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1 or p_web.GSV('job:Loan_Unit_Number') > 0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('buttonPickLoanUnit') & '_comment',loc:class,Net:NoSend)
  If 1 or p_web.GSV('job:Loan_Unit_Number') > 0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:MSN  Routine
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:MSN') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('M.S.N.'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:MSN  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:MSN = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:MSN = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:MSN  ! copies value to session value if valid.
  do Comment::tmp:MSN ! allows comment style to be updated.

ValidateValue::tmp:MSN  Routine
  If p_web.GSV('tmp:MSN') <> ''
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:MSN',tmp:MSN).
    End
  End

Value::tmp:MSN  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:MSN') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- tmp:MSN
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('tmp:MSN'),) & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:MSN  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:MSN:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:MSN') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:LoanUnitDetails  Routine
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:LoanUnitDetails') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Unit Details'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:LoanUnitDetails  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:LoanUnitDetails = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:LoanUnitDetails = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:LoanUnitDetails  ! copies value to session value if valid.
  do Comment::tmp:LoanUnitDetails ! allows comment style to be updated.

ValidateValue::tmp:LoanUnitDetails  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:LoanUnitDetails',tmp:LoanUnitDetails).
    End

Value::tmp:LoanUnitDetails  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:LoanUnitDetails') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- tmp:LoanUnitDetails
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('tmp:LoanUnitDetails'),) & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:LoanUnitDetails  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:LoanUnitDetails:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:LoanUnitDetails') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:LoanAccessories  Routine
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:LoanAccessories') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Accessories'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:LoanAccessories  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:LoanAccessories = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:LoanAccessories = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:LoanAccessories  ! copies value to session value if valid.
  do Comment::tmp:LoanAccessories ! allows comment style to be updated.

ValidateValue::tmp:LoanAccessories  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:LoanAccessories',tmp:LoanAccessories).
    End

Value::tmp:LoanAccessories  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:LoanAccessories') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- tmp:LoanAccessories
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('tmp:LoanAccessories'),) & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:LoanAccessories  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:LoanAccessories:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:LoanAccessories') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:LoanLocation  Routine
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:LoanLocation') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Location / Stock Type'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:LoanLocation  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:LoanLocation = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:LoanLocation = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:LoanLocation  ! copies value to session value if valid.
  do Comment::tmp:LoanLocation ! allows comment style to be updated.

ValidateValue::tmp:LoanLocation  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:LoanLocation',tmp:LoanLocation).
    End

Value::tmp:LoanLocation  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:LoanLocation') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- tmp:LoanLocation
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('tmp:LoanLocation'),) & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:LoanLocation  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:LoanLocation:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:LoanLocation') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:ReplacementValue  Routine
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:ReplacementValue') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Replacement Value'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:ReplacementValue  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:ReplacementValue = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    tmp:ReplacementValue = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::tmp:ReplacementValue  ! copies value to session value if valid.
  do Comment::tmp:ReplacementValue ! allows comment style to be updated.

ValidateValue::tmp:ReplacementValue  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:ReplacementValue',tmp:ReplacementValue).
    End

Value::tmp:ReplacementValue  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:ReplacementValue') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- tmp:ReplacementValue
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    p_web._jsok(format(p_web.GetSessionValue('tmp:ReplacementValue'),'@n14.2')) & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:ReplacementValue  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:ReplacementValue:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:ReplacementValue') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::jobe2:LoanIDNumber  Routine
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('jobe2:LoanIDNumber') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Loan ID Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jobe2:LoanIDNumber  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jobe2:LoanIDNumber = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s13
    jobe2:LoanIDNumber = p_web.Dformat(p_web.GetValue('Value'),'@s13')
  End
  do ValidateValue::jobe2:LoanIDNumber  ! copies value to session value if valid.
      Access:JOBSE2.ClearKey(jobe2:RefNumberKey)
      jobe2:RefNumber = p_web.GSV('job:Ref_Number')
      IF (access:jobse2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
          IF (jobe2:LoanIDNumber <> p_web.GSV('jobe2:LoanIDNumber') AND jobe2:LoanIDNumber <> '')
              p_web.SSV('Hide:LoanIDAlert',0)
              p_web.SSV('locLoanIDAlert','You have changed the Loan ID. Please select one of the following options:')
              p_web.SSV('locLoanIDOption',0)
          ELSE
              p_web.SSV('Hide:LoanIDAlert',1)
          END
      END
  
  do Value::jobe2:LoanIDNumber
  do SendAlert
  do Comment::jobe2:LoanIDNumber ! allows comment style to be updated.
  do Prompt::locLoanIDOption
  do Value::locLoanIDOption  !1
  do Value::locLoanIDAlert  !1

ValidateValue::jobe2:LoanIDNumber  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('jobe2:LoanIDNumber',jobe2:LoanIDNumber).
    End

Value::jobe2:LoanIDNumber  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('jobe2:LoanIDNumber') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    jobe2:LoanIDNumber = p_web.RestoreValue('jobe2:LoanIDNumber')
    do ValidateValue::jobe2:LoanIDNumber
    If jobe2:LoanIDNumber:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- jobe2:LoanIDNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe2:LoanIDNumber'',''pickloanunit_jobe2:loanidnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('jobe2:LoanIDNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jobe2:LoanIDNumber',p_web.GetSessionValueFormat('jobe2:LoanIDNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s13'),'Loan ID Number',,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::jobe2:LoanIDNumber  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if jobe2:LoanIDNumber:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('jobe2:LoanIDNumber') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::locLoanIDAlert  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locLoanIDAlert = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locLoanIDAlert = p_web.GetValue('Value')
  End
  do ValidateValue::locLoanIDAlert  ! copies value to session value if valid.
  do Comment::locLoanIDAlert ! allows comment style to be updated.

ValidateValue::locLoanIDAlert  Routine
    If not (p_web.GSV('Hide:LoanIDAlert') = 1)
      if loc:invalid = '' then p_web.SetSessionValue('locLoanIDAlert',locLoanIDAlert).
    End

Value::locLoanIDAlert  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:LoanIDAlert') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('locLoanIDAlert') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (p_web.GSV('Hide:LoanIDAlert') = 1)
  ! --- DISPLAY --- locLoanIDAlert
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('red bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locLoanIDAlert'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locLoanIDAlert  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locLoanIDAlert:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:LoanIDAlert') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('locLoanIDAlert') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:LoanIDAlert') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locLoanIDOption  Routine
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('locLoanIDOption') & '_prompt',Choose(p_web.GSV('Hide:LoanIDAlert') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:LoanIDAlert') = 1,'',p_web.Translate('Select Option'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locLoanIDOption  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locLoanIDOption = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locLoanIDOption = p_web.GetValue('Value')
  End
  do ValidateValue::locLoanIDOption  ! copies value to session value if valid.
      Access:JOBSE2.ClearKey(jobe2:RefNumberKey)
      jobe2:RefNumber = p_web.GSV('job:Ref_Number')
      IF (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
          CASE p_web.GSV('locLoanIDOption')
          OF 1
          OF 2
          OF 9
              p_web.SSV('jobe2:LoanIDNumber',jobe2:LoanIDNumber)
              p_web.SSV('Hide:LoanIDAlert',1)
          END
      END
  
  
  do Value::locLoanIDOption
  do SendAlert
  do Comment::locLoanIDOption ! allows comment style to be updated.
  do Value::jobe2:LoanIDNumber  !1
  do Prompt::locLoanIDOption
  do Value::locLoanIDAlert  !1

ValidateValue::locLoanIDOption  Routine
    If not (p_web.GSV('Hide:LoanIDAlert') = 1)
      if loc:invalid = '' then p_web.SetSessionValue('locLoanIDOption',locLoanIDOption).
    End

Value::locLoanIDOption  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:LoanIDAlert') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('locLoanIDOption') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If loc:retrying
    locLoanIDOption = p_web.RestoreValue('locLoanIDOption')
    do ValidateValue::locLoanIDOption
    If locLoanIDOption:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:LoanIDAlert') = 1)
  ! --- RADIO --- locLoanIDOption
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locLoanIDOption') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locLoanIDOption'',''pickloanunit_locloanidoption_value'',1,'''&p_web._jsok(1)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locLoanIDOption')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locLoanIDOption',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locLoanIDOption_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Change Loan ID Only') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locLoanIDOption') = 2
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locLoanIDOption'',''pickloanunit_locloanidoption_value'',1,'''&p_web._jsok(2)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locLoanIDOption')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locLoanIDOption',clip(2),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locLoanIDOption_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Change Loan And Customer ID') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locLoanIDOption') = 9
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locLoanIDOption'',''pickloanunit_locloanidoption_value'',1,'''&p_web._jsok(9)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locLoanIDOption')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locLoanIDOption',clip(9),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locLoanIDOption_3') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Do Not Change Loan ID') & '<13,10>'
    packet = clip(packet) & p_web.br
  do SendPacket
  End
  p_web.DivFooter()
Comment::locLoanIDOption  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locLoanIDOption:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:LoanIDAlert') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('locLoanIDOption') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:LoanIDAlert') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::job:Loan_Courier  Routine
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('job:Loan_Courier') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Courier'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Loan_Courier  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Loan_Courier = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    job:Loan_Courier = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::job:Loan_Courier  ! copies value to session value if valid.
  do Comment::job:Loan_Courier ! allows comment style to be updated.

ValidateValue::job:Loan_Courier  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('job:Loan_Courier',job:Loan_Courier).
    End

Value::job:Loan_Courier  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('job:Loan_Courier') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,p_web.site.style.formselect,'FormEntry',)
  If loc:retrying
    job:Loan_Courier = p_web.RestoreValue('job:Loan_Courier')
    do ValidateValue::job:Loan_Courier
    If job:Loan_Courier:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- DROPLIST ---
  loc:even = 1
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('job:Loan_Courier',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
  loc:fieldclass = ''
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('job:Loan_Courier') = 0
    p_web.SetSessionValue('job:Loan_Courier','')
  end
    packet = clip(packet) & p_web.CreateOption('-------------------------------------','',choose('' = p_web.getsessionvalue('job:Loan_Courier')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(JOBSE2)
  bind(jobe2:Record)
  p_web._OpenFile(COURIER)
  bind(cou:Record)
  p_web._OpenFile(JOBS)
  bind(job:Record)
  p_web._OpenFile(LOAN)
  bind(loa:Record)
  p_web._OpenFile(JOBEXACC)
  bind(jea:Record)
  p_web._OpenFile(LOAN_ALIAS)
  bind(loa_ali:Record)
  p_web._OpenFile(EXCHOR48)
  bind(ex4:Record)
  p_web._OpenFile(MANUFACT)
  bind(man:Record)
  p_web._OpenFile(PARTS)
  bind(par:Record)
  p_web._OpenFile(STOCK)
  bind(sto:Record)
  p_web._OpenFile(WARPARTS)
  bind(wpr:Record)
  p_web._OpenFile(STOCKTYP)
  bind(stp:Record)
  p_web._OpenFile(EXCHHIST)
  bind(exh:Record)
  p_web._OpenFile(PRODCODE)
  bind(prd:Record)
  p_web._OpenFile(JOBSE)
  bind(jobe:Record)
  p_web._OpenFile(TagFile)
  bind(tag:Record)
  p_web._OpenFile(LOANACC)
  bind(lac:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(job:Loan_Courier_OptionView)
  job:Loan_Courier_OptionView{prop:order} = p_web.CleanFilter(job:Loan_Courier_OptionView,'UPPER(cou:Courier)')
  Set(job:Loan_Courier_OptionView)
  Loop
    Next(job:Loan_Courier_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('job:Loan_Courier') = 0
      p_web.SetSessionValue('job:Loan_Courier',cou:Courier)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,cou:Courier,choose(cou:Courier = p_web.getsessionvalue('job:Loan_Courier')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(job:Loan_Courier_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(JOBSE2)
  p_Web._CloseFile(COURIER)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(LOAN)
  p_Web._CloseFile(JOBEXACC)
  p_Web._CloseFile(LOAN_ALIAS)
  p_Web._CloseFile(EXCHOR48)
  p_Web._CloseFile(MANUFACT)
  p_Web._CloseFile(PARTS)
  p_Web._CloseFile(STOCK)
  p_Web._CloseFile(WARPARTS)
  p_Web._CloseFile(STOCKTYP)
  p_Web._CloseFile(EXCHHIST)
  p_Web._CloseFile(PRODCODE)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(TagFile)
  p_Web._CloseFile(LOANACC)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web.DivFooter()
Comment::job:Loan_Courier  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if job:Loan_Courier:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('job:Loan_Courier') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::job:Loan_Consignment_Number  Routine
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('job:Loan_Consignment_Number') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Consignment Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Loan_Consignment_Number  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Loan_Consignment_Number = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    job:Loan_Consignment_Number = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::job:Loan_Consignment_Number  ! copies value to session value if valid.
  do Comment::job:Loan_Consignment_Number ! allows comment style to be updated.

ValidateValue::job:Loan_Consignment_Number  Routine
    If not (1=0)
    job:Loan_Consignment_Number = Upper(job:Loan_Consignment_Number)
      if loc:invalid = '' then p_web.SetSessionValue('job:Loan_Consignment_Number',job:Loan_Consignment_Number).
    End

Value::job:Loan_Consignment_Number  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('job:Loan_Consignment_Number') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.GSV('ReadOnly:LoanDespatchDetails') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    job:Loan_Consignment_Number = p_web.RestoreValue('job:Loan_Consignment_Number')
    do ValidateValue::job:Loan_Consignment_Number
    If job:Loan_Consignment_Number:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- job:Loan_Consignment_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:LoanDespatchDetails') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Loan_Consignment_Number',p_web.GetSessionValueFormat('job:Loan_Consignment_Number'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::job:Loan_Consignment_Number  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if job:Loan_Consignment_Number:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('job:Loan_Consignment_Number') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::job:Loan_Despatched  Routine
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('job:Loan_Despatched') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Date'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Loan_Despatched  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Loan_Despatched = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@d06b'  !FieldPicture = @d6b
    job:Loan_Despatched = p_web.Dformat(p_web.GetValue('Value'),'@d06b')
  End
  do ValidateValue::job:Loan_Despatched  ! copies value to session value if valid.
  do Comment::job:Loan_Despatched ! allows comment style to be updated.

ValidateValue::job:Loan_Despatched  Routine
    If not (1=0)
    job:Loan_Despatched = Upper(job:Loan_Despatched)
      if loc:invalid = '' then p_web.SetSessionValue('job:Loan_Despatched',job:Loan_Despatched).
    End

Value::job:Loan_Despatched  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('job:Loan_Despatched') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.GSV('ReadOnly:LoanDespatchDetails') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    job:Loan_Despatched = p_web.RestoreValue('job:Loan_Despatched')
    do ValidateValue::job:Loan_Despatched
    If job:Loan_Despatched:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- job:Loan_Despatched
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:LoanDespatchDetails') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Loan_Despatched',p_web.GetSessionValue('job:Loan_Despatched'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@d06b',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::job:Loan_Despatched  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if job:Loan_Despatched:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('job:Loan_Despatched') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::job:Loan_Despatched_User  Routine
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('job:Loan_Despatched_User') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('User'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Loan_Despatched_User  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Loan_Despatched_User = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s3
    job:Loan_Despatched_User = p_web.Dformat(p_web.GetValue('Value'),'@s3')
  End
  do ValidateValue::job:Loan_Despatched_User  ! copies value to session value if valid.
  do Comment::job:Loan_Despatched_User ! allows comment style to be updated.

ValidateValue::job:Loan_Despatched_User  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('job:Loan_Despatched_User',job:Loan_Despatched_User).
    End

Value::job:Loan_Despatched_User  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('job:Loan_Despatched_User') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If p_web.GSV('ReadOnly:LoanDespatchDetails') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    job:Loan_Despatched_User = p_web.RestoreValue('job:Loan_Despatched_User')
    do ValidateValue::job:Loan_Despatched_User
    If job:Loan_Despatched_User:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- job:Loan_Despatched_User
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:LoanDespatchDetails') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Loan_Despatched_User',p_web.GetSessionValueFormat('job:Loan_Despatched_User'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s3'),,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::job:Loan_Despatched_User  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if job:Loan_Despatched_User:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('job:Loan_Despatched_User') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::job:Loan_Despatch_Number  Routine
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('job:Loan_Despatch_Number') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Despatch No'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Loan_Despatch_Number  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Loan_Despatch_Number = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s8
    job:Loan_Despatch_Number = p_web.Dformat(p_web.GetValue('Value'),'@s8')
  End
  do ValidateValue::job:Loan_Despatch_Number  ! copies value to session value if valid.
  do Comment::job:Loan_Despatch_Number ! allows comment style to be updated.

ValidateValue::job:Loan_Despatch_Number  Routine
    If not (1=0)
    job:Loan_Despatch_Number = Upper(job:Loan_Despatch_Number)
      if loc:invalid = '' then p_web.SetSessionValue('job:Loan_Despatch_Number',job:Loan_Despatch_Number).
    End

Value::job:Loan_Despatch_Number  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('job:Loan_Despatch_Number') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.GSV('ReadOnly:LoanDespatchDetails') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    job:Loan_Despatch_Number = p_web.RestoreValue('job:Loan_Despatch_Number')
    do ValidateValue::job:Loan_Despatch_Number
    If job:Loan_Despatch_Number:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- job:Loan_Despatch_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:LoanDespatchDetails') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Loan_Despatch_Number',p_web.GetSessionValueFormat('job:Loan_Despatch_Number'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s8'),,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::job:Loan_Despatch_Number  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if job:Loan_Despatch_Number:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('job:Loan_Despatch_Number') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::button:AmendDespatchDetails  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::button:AmendDespatchDetails  ! copies value to session value if valid.
      if (p_web.GSV('ReadOnly:LoanDespatchDetails') = 1)
          p_web.SSV('ReadOnly:LoanDespatchDetails',0)
      ELSE
          p_web.SSV('ReadOnly:LoanDespatchDetails',1)
      END
  do Value::button:AmendDespatchDetails
  do Comment::button:AmendDespatchDetails ! allows comment style to be updated.
  do Value::job:Loan_Consignment_Number  !1
  do Value::job:Loan_Despatch_Number  !1
  do Value::job:Loan_Despatched  !1
  do Value::job:Loan_Despatched_User  !1

ValidateValue::button:AmendDespatchDetails  Routine
    If not (p_web.GSV('job:Loan_Consignment_Number') = '')
    End

Value::button:AmendDespatchDetails  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('job:Loan_Consignment_Number') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('button:AmendDespatchDetails') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (p_web.GSV('job:Loan_Consignment_Number') = '')
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''button:AmendDespatchDetails'',''pickloanunit_button:amenddespatchdetails_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','AmendDespatchDetails','Amend Despatch Details',p_web.combine(Choose('Amend Despatch Details' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'button-entryfield'),loc:formname,,,,loc:javascript,loc:disabled,,,,,,,,,,'nt-left')!h
  do SendPacket
  End
  p_web.DivFooter()
Comment::button:AmendDespatchDetails  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if button:AmendDespatchDetails:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('job:Loan_Consignment_Number') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('button:AmendDespatchDetails') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('job:Loan_Consignment_Number') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::locRemoveText  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::locRemoveText  ! copies value to session value if valid.
  do Comment::locRemoveText ! allows comment style to be updated.

ValidateValue::locRemoveText  Routine
    If not (p_web.GSV('job:Loan_unit_Number') = 0)
    End

Value::locRemoveText  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('job:Loan_unit_Number') = 0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('locRemoveText') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (p_web.GSV('job:Loan_unit_Number') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="locRemoveText" class="'&clip('red bold')&'"'&clip(loc:extra)&'>' & p_web.Translate('You must remove the existing Loan before you can add a new one',) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locRemoveText  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locRemoveText:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('job:Loan_unit_Number') = 0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('locRemoveText') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('job:Loan_unit_Number') = 0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::buttonRemoveAttachedUnit  Routine
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('buttonRemoveAttachedUnit') & '_prompt',Choose(p_web.GSV('Hide:RemovalReason') = 0 AND p_web.GSV('job:Loan_Unit_Number') > 0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:RemovalReason') = 0 AND p_web.GSV('job:Loan_Unit_Number') > 0,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::buttonRemoveAttachedUnit  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonRemoveAttachedUnit  ! copies value to session value if valid.
  p_web.SSV('Hide:RemovalReason',0)
  
  if (p_web.GSV('job:Loan_Consignment_Number') <> '')
      p_web.SSV('locRemoveAlertMessage','Warning!<br>The Loan Unit has already been despatched. '&|
                          'If the continue the unit will be removed and returned to Loan Stock.')
  end ! if (p_web.GSV('job:Loan_Consignment_Number') <> '')
  do Comment::buttonRemoveAttachedUnit ! allows comment style to be updated.
  do Prompt::tmp:RemovalReason
  do Value::tmp:RemovalReason  !1
  do Value::buttonConfirmLoanRemoval  !1
  do Value::buttonRemoveAttachedUnit  !1
  do Prompt::locRemovalAlertMessage
  do Value::locRemovalAlertMessage  !1
  do Value::TagValidateLoanAccessories  !1

ValidateValue::buttonRemoveAttachedUnit  Routine
    If not (p_web.GSV('Hide:RemovalReason') = 0 AND p_web.GSV('job:Loan_Unit_Number') > 0)
    End

Value::buttonRemoveAttachedUnit  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:RemovalReason') = 0 AND p_web.GSV('job:Loan_Unit_Number') > 0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('buttonRemoveAttachedUnit') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (p_web.GSV('Hide:RemovalReason') = 0 AND p_web.GSV('job:Loan_Unit_Number') > 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonRemoveAttachedUnit'',''pickloanunit_buttonremoveattachedunit_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','RemoveLoanUnit','Remove Loan Unit',p_web.combine(Choose('Remove Loan Unit' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'button-entryfield'),loc:formname,,,,loc:javascript,loc:disabled,,,,,,,,,,'nt-left')!i
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonRemoveAttachedUnit  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonRemoveAttachedUnit:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:RemovalReason') = 0 AND p_web.GSV('job:Loan_Unit_Number') > 0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('buttonRemoveAttachedUnit') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:RemovalReason') = 0 AND p_web.GSV('job:Loan_Unit_Number') > 0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locRemovalAlertMessage  Routine
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('locRemovalAlertMessage') & '_prompt',Choose(p_web.GSV('locRemovalAlertMessage') = '','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('locRemovalAlertMessage') = '','',p_web.Translate('Alert'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locRemovalAlertMessage  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locRemovalAlertMessage = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locRemovalAlertMessage = p_web.GetValue('Value')
  End
  do ValidateValue::locRemovalAlertMessage  ! copies value to session value if valid.
  do Value::locRemovalAlertMessage
  do SendAlert
  do Comment::locRemovalAlertMessage ! allows comment style to be updated.

ValidateValue::locRemovalAlertMessage  Routine
    If not (p_web.GSV('locRemovalAlertMessage') = '')
      if loc:invalid = '' then p_web.SetSessionValue('locRemovalAlertMessage',locRemovalAlertMessage).
    End

Value::locRemovalAlertMessage  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('locRemovalAlertMessage') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('locRemovalAlertMessage') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  If loc:retrying
    locRemovalAlertMessage = p_web.RestoreValue('locRemovalAlertMessage')
    do ValidateValue::locRemovalAlertMessage
    If locRemovalAlertMessage:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('locRemovalAlertMessage') = '')
  ! --- TEXT --- locRemovalAlertMessage
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locRemovalAlertMessage'',''pickloanunit_locremovalalertmessage_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'readonly'
  do SendPacket
  p_web.CreateTextArea('locRemovalAlertMessage',p_web.GetSessionValue('locRemovalAlertMessage'),5,25,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(locRemovalAlertMessage),,1,,Net:Send)
  do SendPacket
  End
  p_web.DivFooter()
Comment::locRemovalAlertMessage  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locRemovalAlertMessage:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('locRemovalAlertMessage') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('locRemovalAlertMessage') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('locRemovalAlertMessage') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:RemovalReason  Routine
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:RemovalReason') & '_prompt',Choose(p_web.GSV('Hide:RemovalReason') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:RemovalReason') = 1,'',p_web.Translate('Removal Reason'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:RemovalReason  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:RemovalReason = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:RemovalReason = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:RemovalReason  ! copies value to session value if valid.
  if p_web.GSV('tmp:RemovalReason') = 3 or p_web.GSV('tmp:RemovalReason') = 0
      p_web.SSV('Hide:ConfirmRemovalButton',1)
  else ! if p_web.GSV('tmp:RemovalReason') = 3
      p_web.SSV('Hide:ConfirmRemovalButton',0)
  end ! if p_web.GSV('tmp:RemovalReason') = 3
  do Value::tmp:RemovalReason
  do SendAlert
  do Comment::tmp:RemovalReason ! allows comment style to be updated.
  do Value::buttonConfirmLoanRemoval  !1
  do Value::TagValidateLoanAccessories  !1
  do Value::buttonValidateAccessories  !1

ValidateValue::tmp:RemovalReason  Routine
    If not (p_web.GSV('Hide:RemovalReason') = 1)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:RemovalReason',tmp:RemovalReason).
    End

Value::tmp:RemovalReason  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:RemovalReason') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:RemovalReason') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',' noButton')
  If loc:retrying
    tmp:RemovalReason = p_web.RestoreValue('tmp:RemovalReason')
    do ValidateValue::tmp:RemovalReason
    If tmp:RemovalReason:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:RemovalReason') = 1)
  ! --- RADIO --- tmp:RemovalReason
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:RemovalReason') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:RemovalReason'',''pickloanunit_tmp:removalreason_value'',1,'''&p_web._jsok(1)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:RemovalReason')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:RemovalReason',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:RemovalReason_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Replace Faulty Unit') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:RemovalReason') = 2
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:RemovalReason'',''pickloanunit_tmp:removalreason_value'',1,'''&p_web._jsok(2)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:RemovalReason')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:RemovalReason',clip(2),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:RemovalReason_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Alternative Model Required') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:RemovalReason') = 3
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:RemovalReason'',''pickloanunit_tmp:removalreason_value'',1,'''&p_web._jsok(3)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:RemovalReason')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:RemovalReason',clip(3),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:RemovalReason_3') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Restock Unit') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:RemovalReason') = 4
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:RemovalReason'',''pickloanunit_tmp:removalreason_value'',1,'''&p_web._jsok(4)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:RemovalReason')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:RemovalReason',clip(4),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:RemovalReason_4') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Unit Lost') & '<13,10>'
    packet = clip(packet) & p_web.br
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:RemovalReason  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:RemovalReason:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:RemovalReason') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:RemovalReason') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:RemovalReason') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::TagValidateLoanAccessories  Routine
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('TagValidateLoanAccessories') & '_prompt',Choose(p_web.GSV('Hide:RemovalReason') = 1 or p_web.GSV('tmp:RemovalReason') <> 3,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:RemovalReason') = 1 or p_web.GSV('tmp:RemovalReason') <> 3,'',p_web.Translate('Validate Accessories'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::TagValidateLoanAccessories  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  Elsif true
    p_web.StoreValue('acr:Accessory')
  End
  do ValidateValue::TagValidateLoanAccessories  ! copies value to session value if valid.
  do Comment::TagValidateLoanAccessories ! allows comment style to be updated.

ValidateValue::TagValidateLoanAccessories  Routine
    If not (p_web.GSV('Hide:RemovalReason') = 1 or p_web.GSV('tmp:RemovalReason') <> 3)
    End

Value::TagValidateLoanAccessories  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(p_web.GSV('Hide:RemovalReason') = 1 or p_web.GSV('tmp:RemovalReason') <> 3,1,0))
  ! --- BROWSE ---  TagValidateLoanAccessories --
  p_web.SetValue('TagValidateLoanAccessories:NoForm',1)
  p_web.SetValue('TagValidateLoanAccessories:FormName',loc:formname)
  p_web.SetValue('TagValidateLoanAccessories:parentIs','Form')
  p_web.SetValue('_parentProc','PickLoanUnit')
  if p_web.RequestAjax = 0
    p_web.SSV('PickLoanUnit:_popup_',p_web.GetValue('_popup_')) ! stores the current browse popup state
    packet = clip(packet) & '<div id="'&lower('PickLoanUnit_TagValidateLoanAccessories_embedded_div')&'"><!-- Net:TagValidateLoanAccessories --></div><13,10>'
    do SendPacket
    p_web.DivHeader('PickLoanUnit_' & lower('TagValidateLoanAccessories') & '_value')
    p_web.DivFooter()
  else
    if p_web.GetValue('_popup_') = 1
      p_web.SSV('PickLoanUnit:_popup_',1)
    elsif p_web.GSV('PickLoanUnit:_popup_') = 1
      p_web.SetValue('_popup_',1)
    end
    packet = clip(packet) & '<!-- Net:TagValidateLoanAccessories --><13,10>'
  end
  do SendPacket
Comment::TagValidateLoanAccessories  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if TagValidateLoanAccessories:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate('Do not tag if no accessories attached')
  loc:class = Choose(p_web.GSV('Hide:RemovalReason') = 1 or p_web.GSV('tmp:RemovalReason') <> 3,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('TagValidateLoanAccessories') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:RemovalReason') = 1 or p_web.GSV('tmp:RemovalReason') <> 3
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonValidateAccessories  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonValidateAccessories  ! copies value to session value if valid.
  !Validate Accessories
  p_web.SSV('AccessoryCheck:Type','LOA')
  p_web.SSV('AccessoryCheck:RefNumber',p_web.GSV('tmp:LoanUnitNumber'))
  AccessoryCheck(p_web)
  
  case p_web.GSV('AccessoryCheck:Return')
  of 1
      p_web.SSV('locValidateMessage','<span class="RedBold">There is a missing accessory. If required, enter a Lost Loan Charge below or revalidate the accessories.</span>')
      p_web.SSV('Hide:LostLoanFee',0)
  of 2
      p_web.SSV('locValidateMessage','<span class="RedBold">There is an accessory mismatch. If required, enter a Lost Loan Charge below or revalidate the accessories.</span>')
      p_web.SSV('Hide:LostLoanFee',0)
  else
      p_web.SSV('locValidateMessage','<span class="GreenBold">Accessories Validated</span>')
      p_web.SSV('Hide:LostLoanFee',1)
      p_web.SSV('Hide:ValidateAccessoriesButton',1)
  
  end
  
  p_web.SSV('Hide:ConfirmRemovalButton',0)
  
  
  !    error# = 0
  !    Access:LOANACC.Clearkey(lac:Ref_Number_Key)
  !    lac:Ref_Number    = p_web.GSV('tmp:LoanUnitNumber')
  !    set(lac:Ref_Number_Key,lac:Ref_Number_Key)
  !    loop
  !        if (Access:LOANACC.Next())
  !            Break
  !        end ! if (Access:LOANACC.Next())
  !        if (lac:Ref_Number    <> p_web.GSV('tmp:LoanUnitNumber'))
  !            Break
  !        end ! if (lac:Ref_Number    <> p_web.GSV('tmp:LoanUnitNumber'))
  !
  !
  !        Access:TAGFILE.Clearkey(tag:keyTagged)
  !        tag:sessionID    = p_web.sessionID
  !        tag:taggedValue  = lac:accessory
  !        if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
  !            ! Found
  !            if (tag:tagged <> 1)
  !               error# = 1
  !               break
  !            end ! if (tag:tagged <> 1)
  !        else ! if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
  !            ! Error
  !            error# = 1
  !            break
  !        end ! if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
  !    end ! loop
  !
  !    if (error# = 0)
  !        Access:TAGFILE.Clearkey(tag:keyTagged)
  !        tag:sessionID    = p_web.sessionID
  !        set(tag:keyTagged,tag:keyTagged)
  !        loop
  !            if (Access:TAGFILE.Next())
  !                Break
  !            end ! if (Access:TAGFILE.Next())
  !            if (tag:sessionID    <> p_web.sessionID)
  !                Break
  !            end ! if (tag:sessionID    <> p_web.sessionID)
  !            if (tag:Tagged = 0)
  !                cycle
  !            end ! if (tag:Tagged = 0)
  !            Access:LOANACC.Clearkey(lac:ref_number_Key)
  !            lac:ref_number    = p_web.GSV('tmp:loanunitnumber')
  !            lac:accessory    = tag:taggedValue
  !            if (Access:LOANACC.TryFetch(lac:ref_number_Key) = Level:Benign)
  !                ! Found
  !            else ! if (Access:LOANACC.TryFetch(lac:ref_number_Key) = Level:Benign)
  !                ! Error
  !                error# = 2
  !                break
  !            end ! if (Access:LOANACC.TryFetch(lac:ref_number_Key) = Level:Benign)
  !        end ! loop
  !    end ! if (error# = 0)
  
  do Value::buttonValidateAccessories
  do Comment::buttonValidateAccessories ! allows comment style to be updated.
  do Prompt::locValidateMessage
  do Value::locValidateMessage  !1
  do Prompt::tmp:LostLoanFee
  do Value::tmp:LostLoanFee  !1
  do Value::buttonConfirmLoanRemoval  !1
  do Value::TagValidateLoanAccessories  !1

ValidateValue::buttonValidateAccessories  Routine
    If not (p_web.GSV('Hide:RemovalReason') = 1 or p_web.GSV('tmp:RemovalReason') <> 3 Or p_web.GSV('Hide:ValidateAccessoriesButton') = 1)
    End

Value::buttonValidateAccessories  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:RemovalReason') = 1 or p_web.GSV('tmp:RemovalReason') <> 3 Or p_web.GSV('Hide:ValidateAccessoriesButton') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('buttonValidateAccessories') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (p_web.GSV('Hide:RemovalReason') = 1 or p_web.GSV('tmp:RemovalReason') <> 3 Or p_web.GSV('Hide:ValidateAccessoriesButton') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonValidateAccessories'',''pickloanunit_buttonvalidateaccessories_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','ValidateAccessories','Validate Accessories',p_web.combine(Choose('Validate Accessories' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'button-entryfield'),loc:formname,,,,loc:javascript,loc:disabled,,,,,,,,,,'nt-left')!h
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonValidateAccessories  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonValidateAccessories:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:RemovalReason') = 1 or p_web.GSV('tmp:RemovalReason') <> 3 Or p_web.GSV('Hide:ValidateAccessoriesButton') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('buttonValidateAccessories') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:RemovalReason') = 1 or p_web.GSV('tmp:RemovalReason') <> 3 Or p_web.GSV('Hide:ValidateAccessoriesButton') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locValidateMessage  Routine
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('locValidateMessage') & '_prompt',Choose(p_web.GSV('locValidateMessage') = '' or p_web.GSV('Hide:RemovalReason') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('locValidateMessage') = '' or p_web.GSV('Hide:RemovalReason') = 1,'',p_web.Translate('Message'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locValidateMessage  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locValidateMessage = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locValidateMessage = p_web.GetValue('Value')
  End
  do ValidateValue::locValidateMessage  ! copies value to session value if valid.
  do Comment::locValidateMessage ! allows comment style to be updated.

ValidateValue::locValidateMessage  Routine
    If not (p_web.GSV('locValidateMessage') = '' or p_web.GSV('Hide:RemovalReason') = 1)
      if loc:invalid = '' then p_web.SetSessionValue('locValidateMessage',locValidateMessage).
    End

Value::locValidateMessage  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('locValidateMessage') = '' or p_web.GSV('Hide:RemovalReason') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('locValidateMessage') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry','RedBoldSmall')
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  If loc:retrying
    locValidateMessage = p_web.RestoreValue('locValidateMessage')
    do ValidateValue::locValidateMessage
    If locValidateMessage:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('locValidateMessage') = '' or p_web.GSV('Hide:RemovalReason') = 1)
  ! --- TEXT --- locValidateMessage
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'readonly'
  do SendPacket
  p_web.CreateTextArea('locValidateMessage',p_web.GetSessionValue('locValidateMessage'),5,40,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(locValidateMessage),,1,,Net:Send)
  do SendPacket
  End
  p_web.DivFooter()
Comment::locValidateMessage  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locValidateMessage:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('locValidateMessage') = '' or p_web.GSV('Hide:RemovalReason') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('locValidateMessage') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('locValidateMessage') = '' or p_web.GSV('Hide:RemovalReason') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:LostLoanFee  Routine
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:LostLoanFee') & '_prompt',Choose(p_web.GSV('Hide:LostLoanFee') = 1 or p_web.GSV('Hide:RemovalReason') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:LostLoanFee') = 1 or p_web.GSV('Hide:RemovalReason') = 1,'',p_web.Translate('Lost Loan Fee'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:LostLoanFee  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:LostLoanFee = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    tmp:LostLoanFee = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::tmp:LostLoanFee  ! copies value to session value if valid.
  do Value::tmp:LostLoanFee
  do SendAlert
  do Comment::tmp:LostLoanFee ! allows comment style to be updated.

ValidateValue::tmp:LostLoanFee  Routine
    If not (p_web.GSV('Hide:LostLoanFee') = 1 or p_web.GSV('Hide:RemovalReason') = 1)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:LostLoanFee',tmp:LostLoanFee).
    End

Value::tmp:LostLoanFee  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:LostLoanFee') = 1 or p_web.GSV('Hide:RemovalReason') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:LostLoanFee') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    tmp:LostLoanFee = p_web.RestoreValue('tmp:LostLoanFee')
    do ValidateValue::tmp:LostLoanFee
    If tmp:LostLoanFee:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:LostLoanFee') = 1 or p_web.GSV('Hide:RemovalReason') = 1)
  ! --- STRING --- tmp:LostLoanFee
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:LostLoanFee'',''pickloanunit_tmp:lostloanfee_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:LostLoanFee',p_web.GetSessionValue('tmp:LostLoanFee'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:LostLoanFee  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:LostLoanFee:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:LostLoanFee') = 1 or p_web.GSV('Hide:RemovalReason') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:LostLoanFee') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:LostLoanFee') = 1 or p_web.GSV('Hide:RemovalReason') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::buttonConfirmLoanRemoval  Routine
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('buttonConfirmLoanRemoval') & '_prompt',Choose(p_web.GSV('Hide:RemovalReason') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:RemovalReason') = 1,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::buttonConfirmLoanRemoval  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonConfirmLoanRemoval  ! copies value to session value if valid.
      ! Remove Loan Unit
      p_web.SSV('AddToAudit:Type','LOA')
      if p_web.GSV('job:Loan_Consignemnt_Number') > 0
          p_web.SSV('AddToAudit:Notes','UNIT HAD BEEN DESPATCHED: ' & |
              '<13,10>UNIT NUMBER: ' & p_web.GSV('job:Loan_unit_number') & |
              '<13,10>COURIER: ' & p_web.GSV('job:Loan_Courier') & |
              '<13,10>CONSIGNMENT NUMBER: ' & p_web.GSV('job:Loan_consignment_number') & |
              '<13,10>DATE DESPATCHED: ' & Format(p_web.GSV('job:Loan_despatched'),@d6) &|
              '<13,10>DESPATCH USER: ' & p_web.GSV('job:Loan_despatched_user') &|
              '<13,10>DESPATCH NUMBER: ' & p_web.GSV('job:Loan_despatch_number'))
      else ! if p_web.GSV('job:Loan_Consignemnt_Number') > 0
          p_web.SSV('AddToAudit:Notes','UNIT NUMBER: ' & p_web.GSV('job:Loan_unit_Number'))
      end !if p_web.GSV('job:Loan_Consignemnt_Number') > 0
      case p_web.GSV('tmp:RemovalReason')
      of 1
          p_web.SSV('AddToAudit:Action','REPLACED FAULTY LOAN')
      of 2
          p_web.SSV('AddToAudit:Action','ALTERNATIVE MODEL REQUIRED')
      of 3
          p_web.SSV('AddToAudit:Action','RETURN LOAN: RE-STOCKED')
      of 4
          p_web.SSV('AddToAudit:Action','RETURN LOAN: LOAN LOST')
      end ! case p_web.GSV('tmp:RemovalReason')
  
      addToAudit(p_web)
  
      !p_web.SSV('job:Loan_Unit_Number','')
      p_web.SSV('job:Loan_Accessory','')
      p_web.SSV('job:Loan_Consignment_Number','')
      p_web.SSV('job:Loan_Despatched','')
      p_web.SSV('job:Loan_Despatched_User','')
      p_web.SSV('job:Loan_Issued_Date','')
      p_web.SSV('job:Loan_User','')
      if (p_web.GSV('job:Despatch_Type') = 'LOA')
          p_web.SSV('job:Despatched','NO')
          p_web.SSV('job:Despatch_type','')
      else ! if (p_web.GSV('job:Despatch_Type') = 'EXC')
          if (p_web.GSV('jobe:DespatchType') = 'LOA')
              p_web.SSV('jobe:DespatchType','')
              p_web.SSV('wob:ReadyToDespatch',0)
          end ! if (p_web.GSV('jobe:DespatchType') = 'EXC')
      end !
  
  
      getStatus(101,0,'LOA',p_web)
  
      IF (SUB(p_web.GSV('job:Current_Status'),1,3) = '811')
  
          GetStatus(810,0,'JOB',p_web)
      END
  
      Access:LOAN.ClearKey(loa:Ref_Number_Key)
      loa:Ref_Number = p_web.GSV('job:loan_unit_number')
      IF (Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign)
          IF (p_web.GSV('tmp:RemovalReason') = 4)
              loa:Available = 'LOS'
          ELSE
              loa:Available = 'AVL'
          END
          loa:StatusChangeDate = TODAY()
          loa:Job_Number = 0
          IF (Access:LOAN.TryUpdate() = Level:Benign)
              IF (Access:LOANHIST.PrimeRecord() = Level:Benign)
                  loh:Ref_Number = loa:Ref_Number
                  loh:Date = TODAY()
                  loh:Time = CLOCK()
                  loh:User = p_web.GSV('BookingUserCode')
                  loh:Status = 'UNIT RE-STOCKED FROM JOB NO: ' & clip(format(p_web.GSV('job:Ref_Number'),@p<<<<<<<#p))
                  IF (Access:LOANHIST.TryInsert())
                      Access:LOANHIST.CancelAutoInc()
                  END
              END
          END
      END
  
      p_web.SSV('job:Loan_Unit_Number','') ! Remove Loan Unit
  
      Access:JOBS.Clearkey(job:Ref_Number_Key)
      job:Ref_Number    = p_web.GSV('job:Ref_Number')
      if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
          ! Found
          p_web.SessionQueueToFile(JOBS)
          access:JOBS.tryUpdate()
      else ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
          ! Error
      end ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
  
      Access:JOBSE.Clearkey(jobe:RefNumberKey)
      jobe:RefNumber    = p_web.GSV('job:Ref_Number')
      if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
          ! Found
          p_web.SessionQueueToFile(JOBSE)
          access:JOBSE.tryUpdate()
      else ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
          ! Error
      end ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
  
      p_web.SessionQueueToFile(WEBJOB)
      access:WEBJOB.TryUpdate()
  
      p_web.SSV('tmp:LoanUnitNumber',0)
  
      do clearLoanDetails
  
      p_web.SSV('Hide:RemovalReason',1)
      p_web.SSV('Hide:ConfirmLoanRemoval',1)
  do Value::buttonConfirmLoanRemoval
  do Comment::buttonConfirmLoanRemoval ! allows comment style to be updated.
  do Value::buttonPickLoanUnit  !1
  do Value::locLoanAlertMessage  !1
  do Value::locRemovalAlertMessage  !1
  do Value::tmp:LoanAccessories  !1
  do Value::tmp:LoanIMEINumber  !1
  do Value::tmp:LoanLocation  !1
  do Value::tmp:LoanUnitDetails  !1
  do Value::tmp:MSN  !1
  do Prompt::tmp:RemovalReason
  do Value::tmp:RemovalReason  !1
  do Value::locRemoveText  !1
  do Value::TagValidateLoanAccessories  !1
  do Value::buttonValidateAccessories  !1
  do Prompt::locValidateMessage
  do Value::locValidateMessage  !1
  do Prompt::tmp:LostLoanFee
  do Value::tmp:LostLoanFee  !1

ValidateValue::buttonConfirmLoanRemoval  Routine
    If not (p_web.GSV('Hide:RemovalReason') = 1)
    End

Value::buttonConfirmLoanRemoval  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:RemovalReason') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('buttonConfirmLoanRemoval') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (p_web.GSV('Hide:RemovalReason') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonConfirmLoanRemoval'',''pickloanunit_buttonconfirmloanremoval_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','ConfirmLoanRemoval','Confirm Loan Removal',p_web.combine(Choose('Confirm Loan Removal' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'button-entryfield'),loc:formname,,,,loc:javascript,loc:disabled,'images\packdelete.png',,,,,,,,,'nt-left')!h
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonConfirmLoanRemoval  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonConfirmLoanRemoval:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:RemovalReason') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('buttonConfirmLoanRemoval') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:RemovalReason') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::link:SMSHistory  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::link:SMSHistory  ! copies value to session value if valid.
  do Comment::link:SMSHistory ! allows comment style to be updated.

ValidateValue::link:SMSHistory  Routine
    If not (1=0)
    End

Value::link:SMSHistory  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('link:SMSHistory') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','SMSHistory','SMS History',p_web.combine(Choose('SMS History' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.WindowOpen(clip('BrowseSMSHistory')&''&'','_self'),,loc:disabled,,,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::link:SMSHistory  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if link:SMSHistory:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('link:SMSHistory') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::link:SendSMS  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::link:SendSMS  ! copies value to session value if valid.
      IF (p_web.GSV('jobe2:SMSDate') = 0)
          p_web.SSV('jobe2:SMSDate',TODAY() + 30)
  
          Access:JOBSE2.ClearKey(jobe2:RefNumberKey)
          jobe2:RefNumber = p_web.GSV('job:Ref_Number')
          IF (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
              p_web.SessionQueueToFile(JOBSE2)
              Access:JOBSE2.TryUpdate()
          END
  
      END
  
  !Format the date before sending
      locSMSDate = Format(p_web.GSV('jobe2:SMSDate'),@d10)
  
  !now send the SMS message
      If p_web.GSV('jobe2:SMSNotification')
          AddEmailSMS(p_web.GSV('job:Ref_Number'),p_web.GSV('wob:HeadAccountNumber'),'LOAN','SMS',p_web.GSV('jobe2:SMSAlertNumber'),'','',clip(locSMSDate))
      End ! If jobe2:SMSNotification
      If p_web.GSV('jobe2:EmailNotification')
          AddEmailSMS(p_web.GSV('job:Ref_Number'),p_web.GSV('wob:HeadAccountNumber'),'LOAN','EMAIL','',p_web.GSV('jobe2:EmailAlertAddress'),'',clip(locSMSDate))
      End ! If jobe2:EmailNotification
  do Value::link:SendSMS
  do Comment::link:SendSMS ! allows comment style to be updated.

ValidateValue::link:SendSMS  Routine
    If not (1=0)
    End

Value::link:SendSMS  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('link:SendSMS') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''link:SendSMS'',''pickloanunit_link:sendsms_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','Send Return SMS','Send Return SMS',p_web.combine(Choose('Send Return SMS' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,,loc:javascript,loc:disabled,,,,,,,,,,'nt-left')!h
  do SendPacket
  End
  p_web.DivFooter()
Comment::link:SendSMS  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if link:SendSMS:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickLoanUnit_' & p_web._nocolon('link:SendSMS') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('PickLoanUnit_nexttab_' & 0)
    job:ESN = p_web.GSV('job:ESN')
    do ValidateValue::job:ESN
    If loc:Invalid
      loc:retrying = 1
      do Value::job:ESN
      !do SendAlert
      do Comment::job:ESN ! allows comment style to be updated.
      !exit
    End
    job:MSN = p_web.GSV('job:MSN')
    do ValidateValue::job:MSN
    If loc:Invalid
      loc:retrying = 1
      do Value::job:MSN
      !do SendAlert
      do Comment::job:MSN ! allows comment style to be updated.
      !exit
    End
    job:Charge_Type = p_web.GSV('job:Charge_Type')
    do ValidateValue::job:Charge_Type
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Charge_Type
      !do SendAlert
      do Comment::job:Charge_Type ! allows comment style to be updated.
      !exit
    End
    job:Warranty_Charge_Type = p_web.GSV('job:Warranty_Charge_Type')
    do ValidateValue::job:Warranty_Charge_Type
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Warranty_Charge_Type
      !do SendAlert
      do Comment::job:Warranty_Charge_Type ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('PickLoanUnit_nexttab_' & 1)
    tmp:LoanIMEINumber = p_web.GSV('tmp:LoanIMEINumber')
    do ValidateValue::tmp:LoanIMEINumber
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:LoanIMEINumber
      !do SendAlert
      do Comment::tmp:LoanIMEINumber ! allows comment style to be updated.
      !exit
    End
    locLoanAlertMessage = p_web.GSV('locLoanAlertMessage')
    do ValidateValue::locLoanAlertMessage
    If loc:Invalid
      loc:retrying = 1
      do Value::locLoanAlertMessage
      !do SendAlert
      do Comment::locLoanAlertMessage ! allows comment style to be updated.
      !exit
    End
    tmp:MSN = p_web.GSV('tmp:MSN')
    do ValidateValue::tmp:MSN
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:MSN
      !do SendAlert
      do Comment::tmp:MSN ! allows comment style to be updated.
      !exit
    End
    tmp:LoanUnitDetails = p_web.GSV('tmp:LoanUnitDetails')
    do ValidateValue::tmp:LoanUnitDetails
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:LoanUnitDetails
      !do SendAlert
      do Comment::tmp:LoanUnitDetails ! allows comment style to be updated.
      !exit
    End
    tmp:LoanAccessories = p_web.GSV('tmp:LoanAccessories')
    do ValidateValue::tmp:LoanAccessories
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:LoanAccessories
      !do SendAlert
      do Comment::tmp:LoanAccessories ! allows comment style to be updated.
      !exit
    End
    tmp:LoanLocation = p_web.GSV('tmp:LoanLocation')
    do ValidateValue::tmp:LoanLocation
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:LoanLocation
      !do SendAlert
      do Comment::tmp:LoanLocation ! allows comment style to be updated.
      !exit
    End
    tmp:ReplacementValue = p_web.GSV('tmp:ReplacementValue')
    do ValidateValue::tmp:ReplacementValue
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:ReplacementValue
      !do SendAlert
      do Comment::tmp:ReplacementValue ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('PickLoanUnit_nexttab_' & 2)
    jobe2:LoanIDNumber = p_web.GSV('jobe2:LoanIDNumber')
    do ValidateValue::jobe2:LoanIDNumber
    If loc:Invalid
      loc:retrying = 1
      do Value::jobe2:LoanIDNumber
      !do SendAlert
      do Comment::jobe2:LoanIDNumber ! allows comment style to be updated.
      !exit
    End
    locLoanIDAlert = p_web.GSV('locLoanIDAlert')
    do ValidateValue::locLoanIDAlert
    If loc:Invalid
      loc:retrying = 1
      do Value::locLoanIDAlert
      !do SendAlert
      do Comment::locLoanIDAlert ! allows comment style to be updated.
      !exit
    End
    locLoanIDOption = p_web.GSV('locLoanIDOption')
    do ValidateValue::locLoanIDOption
    If loc:Invalid
      loc:retrying = 1
      do Value::locLoanIDOption
      !do SendAlert
      do Comment::locLoanIDOption ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('PickLoanUnit_nexttab_' & 3)
    job:Loan_Courier = p_web.GSV('job:Loan_Courier')
    do ValidateValue::job:Loan_Courier
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Loan_Courier
      !do SendAlert
      do Comment::job:Loan_Courier ! allows comment style to be updated.
      !exit
    End
    job:Loan_Consignment_Number = p_web.GSV('job:Loan_Consignment_Number')
    do ValidateValue::job:Loan_Consignment_Number
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Loan_Consignment_Number
      !do SendAlert
      do Comment::job:Loan_Consignment_Number ! allows comment style to be updated.
      !exit
    End
    job:Loan_Despatched = p_web.GSV('job:Loan_Despatched')
    do ValidateValue::job:Loan_Despatched
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Loan_Despatched
      !do SendAlert
      do Comment::job:Loan_Despatched ! allows comment style to be updated.
      !exit
    End
    job:Loan_Despatched_User = p_web.GSV('job:Loan_Despatched_User')
    do ValidateValue::job:Loan_Despatched_User
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Loan_Despatched_User
      !do SendAlert
      do Comment::job:Loan_Despatched_User ! allows comment style to be updated.
      !exit
    End
    job:Loan_Despatch_Number = p_web.GSV('job:Loan_Despatch_Number')
    do ValidateValue::job:Loan_Despatch_Number
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Loan_Despatch_Number
      !do SendAlert
      do Comment::job:Loan_Despatch_Number ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('PickLoanUnit_nexttab_' & 4)
    locRemovalAlertMessage = p_web.GSV('locRemovalAlertMessage')
    do ValidateValue::locRemovalAlertMessage
    If loc:Invalid
      loc:retrying = 1
      do Value::locRemovalAlertMessage
      !do SendAlert
      do Comment::locRemovalAlertMessage ! allows comment style to be updated.
      !exit
    End
    tmp:RemovalReason = p_web.GSV('tmp:RemovalReason')
    do ValidateValue::tmp:RemovalReason
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:RemovalReason
      !do SendAlert
      do Comment::tmp:RemovalReason ! allows comment style to be updated.
      !exit
    End
    locValidateMessage = p_web.GSV('locValidateMessage')
    do ValidateValue::locValidateMessage
    If loc:Invalid
      loc:retrying = 1
      do Value::locValidateMessage
      !do SendAlert
      do Comment::locValidateMessage ! allows comment style to be updated.
      !exit
    End
    tmp:LostLoanFee = p_web.GSV('tmp:LostLoanFee')
    do ValidateValue::tmp:LostLoanFee
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:LostLoanFee
      !do SendAlert
      do Comment::tmp:LostLoanFee ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('PickLoanUnit_nexttab_' & 5)
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_PickLoanUnit_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('PickLoanUnit_tab_' & 0)
    do GenerateTab0
  of lower('PickLoanUnit_tab_' & 1)
    do GenerateTab1
  of lower('PickLoanUnit_tmp:LoanIMEINumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:LoanIMEINumber
      of event:timer
        do Value::tmp:LoanIMEINumber
        do Comment::tmp:LoanIMEINumber
      else
        do Value::tmp:LoanIMEINumber
      end
  of lower('PickLoanUnit_buttonPickLoanUnit_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonPickLoanUnit
      of event:timer
        do Value::buttonPickLoanUnit
        do Comment::buttonPickLoanUnit
      else
        do Value::buttonPickLoanUnit
      end
  of lower('PickLoanUnit_tab_' & 2)
    do GenerateTab2
  of lower('PickLoanUnit_jobe2:LoanIDNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:LoanIDNumber
      of event:timer
        do Value::jobe2:LoanIDNumber
        do Comment::jobe2:LoanIDNumber
      else
        do Value::jobe2:LoanIDNumber
      end
  of lower('PickLoanUnit_locLoanIDOption_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locLoanIDOption
      of event:timer
        do Value::locLoanIDOption
        do Comment::locLoanIDOption
      else
        do Value::locLoanIDOption
      end
  of lower('PickLoanUnit_tab_' & 3)
    do GenerateTab3
  of lower('PickLoanUnit_button:AmendDespatchDetails_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::button:AmendDespatchDetails
      of event:timer
        do Value::button:AmendDespatchDetails
        do Comment::button:AmendDespatchDetails
      else
        do Value::button:AmendDespatchDetails
      end
  of lower('PickLoanUnit_tab_' & 4)
    do GenerateTab4
  of lower('PickLoanUnit_buttonRemoveAttachedUnit_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonRemoveAttachedUnit
      of event:timer
        do Value::buttonRemoveAttachedUnit
        do Comment::buttonRemoveAttachedUnit
      else
        do Value::buttonRemoveAttachedUnit
      end
  of lower('PickLoanUnit_locRemovalAlertMessage_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locRemovalAlertMessage
      of event:timer
        do Value::locRemovalAlertMessage
        do Comment::locRemovalAlertMessage
      else
        do Value::locRemovalAlertMessage
      end
  of lower('PickLoanUnit_tmp:RemovalReason_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:RemovalReason
      of event:timer
        do Value::tmp:RemovalReason
        do Comment::tmp:RemovalReason
      else
        do Value::tmp:RemovalReason
      end
  of lower('PickLoanUnit_buttonValidateAccessories_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonValidateAccessories
      of event:timer
        do Value::buttonValidateAccessories
        do Comment::buttonValidateAccessories
      else
        do Value::buttonValidateAccessories
      end
  of lower('PickLoanUnit_tmp:LostLoanFee_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:LostLoanFee
      of event:timer
        do Value::tmp:LostLoanFee
        do Comment::tmp:LostLoanFee
      else
        do Value::tmp:LostLoanFee
      end
  of lower('PickLoanUnit_buttonConfirmLoanRemoval_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonConfirmLoanRemoval
      of event:timer
        do Value::buttonConfirmLoanRemoval
        do Comment::buttonConfirmLoanRemoval
      else
        do Value::buttonConfirmLoanRemoval
      end
  of lower('PickLoanUnit_tab_' & 5)
    do GenerateTab5
  of lower('PickLoanUnit_link:SendSMS_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::link:SendSMS
      of event:timer
        do Value::link:SendSMS
        do Comment::link:SendSMS
      else
        do Value::link:SendSMS
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('PickLoanUnit_form:ready_',1)

  p_web.SetSessionValue('PickLoanUnit_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_PickLoanUnit',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('PickLoanUnit_form:ready_',1)
  p_web.SetSessionValue('PickLoanUnit_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_PickLoanUnit',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('PickLoanUnit_form:ready_',1)
  p_web.SetSessionValue('PickLoanUnit_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('PickLoanUnit:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('PickLoanUnit_form:ready_',1)
  p_web.SetSessionValue('PickLoanUnit_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('PickLoanUnit:Primed',0)
  p_web.setsessionvalue('showtab_PickLoanUnit',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
      If not (1=0)
        If not (p_web.GSV('ReadOnly:LoanIMEINumber') = 1 OR p_web.GSV('job:Loan_Unit_Number') > 0)
          If p_web.IfExistsValue('tmp:LoanIMEINumber')
            tmp:LoanIMEINumber = p_web.GetValue('tmp:LoanIMEINumber')
          End
        End
      End
      If not (1=0)
          If p_web.IfExistsValue('jobe2:LoanIDNumber')
            jobe2:LoanIDNumber = p_web.GetValue('jobe2:LoanIDNumber')
          End
      End
      If not (p_web.GSV('Hide:LoanIDAlert') = 1)
          If p_web.IfExistsValue('locLoanIDOption')
            locLoanIDOption = p_web.GetValue('locLoanIDOption')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('job:Loan_Courier')
            job:Loan_Courier = p_web.GetValue('job:Loan_Courier')
          End
      End
      If not (1=0)
        If not (p_web.GSV('ReadOnly:LoanDespatchDetails') = 1)
          If p_web.IfExistsValue('job:Loan_Consignment_Number')
            job:Loan_Consignment_Number = p_web.GetValue('job:Loan_Consignment_Number')
          End
        End
      End
      If not (1=0)
        If not (p_web.GSV('ReadOnly:LoanDespatchDetails') = 1)
          If p_web.IfExistsValue('job:Loan_Despatched')
            job:Loan_Despatched = p_web.GetValue('job:Loan_Despatched')
          End
        End
      End
      If not (1=0)
        If not (p_web.GSV('ReadOnly:LoanDespatchDetails') = 1)
          If p_web.IfExistsValue('job:Loan_Despatched_User')
            job:Loan_Despatched_User = p_web.GetValue('job:Loan_Despatched_User')
          End
        End
      End
      If not (1=0)
        If not (p_web.GSV('ReadOnly:LoanDespatchDetails') = 1)
          If p_web.IfExistsValue('job:Loan_Despatch_Number')
            job:Loan_Despatch_Number = p_web.GetValue('job:Loan_Despatch_Number')
          End
        End
      End
  If p_web.GSV('job:Loan_Unit_Number') > 0
      If not (p_web.GSV('Hide:RemovalReason') = 1)
          If p_web.IfExistsValue('tmp:RemovalReason')
            tmp:RemovalReason = p_web.GetValue('tmp:RemovalReason')
          End
      End
      If not (p_web.GSV('Hide:LostLoanFee') = 1 or p_web.GSV('Hide:RemovalReason') = 1)
          If p_web.IfExistsValue('tmp:LostLoanFee')
            tmp:LostLoanFee = p_web.GetValue('tmp:LostLoanFee')
          End
      End
  End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('PickLoanUnit_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
      IF (p_web.GSV('Hide:LoanIDAlert') = 0)
          IF (p_web.GSV('locLoanIDOption') = 0)
              loc:alert = 'You must select an Loan ID option'
              loc:invalid = 'jobe2:LoanIDNumber'
              exit
          END
  
          Access:JOBSE2.ClearKey(jobe2:RefNumberKey)
          jobe2:RefNumber = p_web.GSV('job:Ref_Number')
          IF (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
              CASE(p_web.GSV('locLoanIDOption'))
              OF 1
                  p_web.SSV('AddToAudit:Type','JOB')
                  p_web.SSV('AddToAudit:Action','LOAN ID NUMBER CHANGED')
                  p_web.SSV('AddToAudit:Notes','OLD ID NO: ' & Clip(jobe2:LoanIDNumber) & |
                      '<13,10>NEW ID NO: ' & p_web.GSV('jobe2:LoanIDNumber'))
                  AddToAudit(p_web)
                  jobe2:LoanIDNumber = p_web.GSV('jobe2:LoanIDNumber')
                  IF (Access:JOBSE2.TryUpdate() = Level:Benign)
  
                  END
  
              OF 2
                  p_web.SSV('AddToAudit:Type','JOB')
                  p_web.SSV('AddToAudit:Action','LOAN ID NUMBER CHANGED')
                  p_web.SSV('AddToAudit:Notes','OLD ID NO: ' & Clip(jobe2:LoanIDNumber) & |
                      '<13,10>NEW ID NO: ' & p_web.GSV('jobe2:LoanIDNumber'))
                  AddToAudit(p_web)
                  p_web.SSV('AddToAudit:Type','JOB')
                  p_web.SSV('AddToAudit:Action','CUSTOMER ID NUMBER CHANGED')
                  p_web.SSV('AddToAudit:Notes','OLD ID NO: ' & Clip(jobe2:IDNumber) & |
                      '<13,10>NEW ID NO: ' & p_web.GSV('jobe2:LoanIDNumber'))
                  AddToAudit(p_web)
                  jobe2:LoanIDNumber = p_web.GSV('jobe2:LoanIDNumber')
                  p_web.SSV('jobe2:IDNumber',p_web.GSV('jobe2:LoanIDNumber'))
                  jobe2:IDNumber = p_web.GSV('jobe2:IDNumber')
                  IF (Access:JOBSE2.TryUpdate() = Level:Benign)
  
                  END
              END
  
          END
      END
  
      if (p_web.GSV('tmp:LoanUnitNumber') > 0)
          if (p_web.GSV('tmp:LoanUnitNumber') <> p_web.GSV('job:Loan_Unit_Number'))
              do addLoanUnit
          end ! if (p_web.GSV('tmp:LoanUnitNumber') <> p_web.GSV('job:Loan_Unit_Number'))
      else ! if (p_web.GSV('tmp:LoanUnitNumber') > 0)
          if (p_web.GSV('tmp:NoUnitAvailable') = 1)
  
              getStatus(350,0,'EXC',p_web)
  
  
              getStatus(355,0,'JOB',p_web)
  
              local.AllocateLoanPart('ORD',0)
          end ! if (p_web.GSV('tmp:NoUnitAvailable') = 1)
      end ! if (p_web.GSV('tmp:LoanUnitNumber') > 0)
  
      do clearTagFile
  p_web.DeleteSessionValue('PickLoanUnit_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
    do ValidateValue::job:ESN
    If loc:Invalid then exit.
    do ValidateValue::job:MSN
    If loc:Invalid then exit.
    do ValidateValue::locUnitDetails
    If loc:Invalid then exit.
    do ValidateValue::job:Charge_Type
    If loc:Invalid then exit.
    do ValidateValue::job:Warranty_Charge_Type
    If loc:Invalid then exit.
  ! tab = 2
    loc:InvalidTab += 1
    do ValidateValue::tmp:LoanIMEINumber
    If loc:Invalid then exit.
    do ValidateValue::locLoanAlertMessage
    If loc:Invalid then exit.
    do ValidateValue::buttonPickLoanUnit
    If loc:Invalid then exit.
    do ValidateValue::tmp:MSN
    If loc:Invalid then exit.
    do ValidateValue::tmp:LoanUnitDetails
    If loc:Invalid then exit.
    do ValidateValue::tmp:LoanAccessories
    If loc:Invalid then exit.
    do ValidateValue::tmp:LoanLocation
    If loc:Invalid then exit.
    do ValidateValue::tmp:ReplacementValue
    If loc:Invalid then exit.
  ! tab = 6
    loc:InvalidTab += 1
    do ValidateValue::jobe2:LoanIDNumber
    If loc:Invalid then exit.
    do ValidateValue::locLoanIDAlert
    If loc:Invalid then exit.
    do ValidateValue::locLoanIDOption
    If loc:Invalid then exit.
  ! tab = 5
    loc:InvalidTab += 1
    do ValidateValue::job:Loan_Courier
    If loc:Invalid then exit.
    do ValidateValue::job:Loan_Consignment_Number
    If loc:Invalid then exit.
    do ValidateValue::job:Loan_Despatched
    If loc:Invalid then exit.
    do ValidateValue::job:Loan_Despatched_User
    If loc:Invalid then exit.
    do ValidateValue::job:Loan_Despatch_Number
    If loc:Invalid then exit.
    do ValidateValue::button:AmendDespatchDetails
    If loc:Invalid then exit.
  ! tab = 3
  If p_web.GSV('job:Loan_Unit_Number') > 0
    loc:InvalidTab += 1
    do ValidateValue::locRemoveText
    If loc:Invalid then exit.
    do ValidateValue::buttonRemoveAttachedUnit
    If loc:Invalid then exit.
    do ValidateValue::locRemovalAlertMessage
    If loc:Invalid then exit.
    do ValidateValue::tmp:RemovalReason
    If loc:Invalid then exit.
    do ValidateValue::TagValidateLoanAccessories
    If loc:Invalid then exit.
    do ValidateValue::buttonValidateAccessories
    If loc:Invalid then exit.
    do ValidateValue::locValidateMessage
    If loc:Invalid then exit.
    do ValidateValue::tmp:LostLoanFee
    If loc:Invalid then exit.
    do ValidateValue::buttonConfirmLoanRemoval
    If loc:Invalid then exit.
  End
  ! tab = 7
    loc:InvalidTab += 1
    do ValidateValue::link:SMSHistory
    If loc:Invalid then exit.
    do ValidateValue::link:SendSMS
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
      do deleteVariables
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('PickLoanUnit:Primed',0)
  p_web.StoreValue('job:ESN')
  p_web.StoreValue('job:MSN')
  p_web.StoreValue('')
  p_web.StoreValue('job:Charge_Type')
  p_web.StoreValue('job:Warranty_Charge_Type')
  p_web.StoreValue('tmp:LoanIMEINumber')
  p_web.StoreValue('locLoanAlertMessage')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:MSN')
  p_web.StoreValue('tmp:LoanUnitDetails')
  p_web.StoreValue('tmp:LoanAccessories')
  p_web.StoreValue('tmp:LoanLocation')
  p_web.StoreValue('tmp:ReplacementValue')
  p_web.StoreValue('jobe2:LoanIDNumber')
  p_web.StoreValue('locLoanIDAlert')
  p_web.StoreValue('locLoanIDOption')
  p_web.StoreValue('job:Loan_Courier')
  p_web.StoreValue('job:Loan_Consignment_Number')
  p_web.StoreValue('job:Loan_Despatched')
  p_web.StoreValue('job:Loan_Despatched_User')
  p_web.StoreValue('job:Loan_Despatch_Number')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locRemovalAlertMessage')
  p_web.StoreValue('tmp:RemovalReason')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locValidateMessage')
  p_web.StoreValue('tmp:LostLoanFee')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')

Local.AllocateLoanPart        Procedure(String    func:Status,Byte func:SecondUnit)
local:FoundPart                 Byte(0)
Code
    If p_web.GSV('job:Warranty_Job') = 'YES'
        Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
        wpr:Ref_Number  = p_web.GSV('job:Ref_Number')
        wpr:Part_Number = 'EXCH'
        Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
        Loop
            If Access:WARPARTS.NEXT()
               Break
            End !If
            If wpr:Ref_Number  <> p_web.GSV('job:Ref_Number')      |
            Or wpr:Part_Number <> 'EXCH'      |
                Then Break.  ! End If
            If func:SecondUnit
            Else !If func:SecondUnit
                local:FoundPart = True
            End !If func:SecondUnit
            If local:FoundPart = True
                wpr:Status = func:Status
                If func:Status = 'PIK'
                    wpr:PartAllocated = 1
                End !If func:Status = 'PIK'
                Access:WARPARTS.Update()
                !Remove the EXCH line from Stock Allocation,
                !just incase the Loan isn't being added properly - L873 (DBH: 24-07-2003)
                RemoveFromStockAllocation(wpr:Record_Number,'WAR')
                Break
            End !If local:FoundPart = True
        End !Loop
    End !If job:Warranty_Job = 'YES'


    If local:FoundPart = False
        If p_web.GSV('job:Chargeable_Job') = 'YES'
            Access:PARTS.ClearKey(par:Part_Number_Key)
            par:Ref_Number  = p_web.GSV('job:Ref_Number')
            par:Part_Number = 'EXCH'
            Set(par:Part_Number_Key,par:Part_Number_Key)
            Loop
                If Access:PARTS.NEXT()
                   Break
                End !If
                If par:Ref_Number  <> p_web.GSV('job:Ref_Number')      |
                Or par:Part_Number <> 'EXCH'      |
                    Then Break.  ! End If
                If func:SecondUnit
                Else !If func:SecondUnit
                    local:FoundPart = True
                End !If func:SecondUnit
                If local:FoundPart = True
                    par:Status = func:Status
                    If func:Status = 'PIK'
                        par:PartAllocated = 1
                    End !If func:Status = 'PIK'
                    Access:PARTS.Update()
                    !Remove the EXCH line from Stock Allocation,
                    !just incase the Loan isn't being added properly - L873 (DBH: 24-07-2003)
                    RemoveFromStockAllocation(par:Record_Number,'CHA')
                    Break
                End !If local:FoundPart = True

            End !Loop
        End !If job:Chargeable_Job = 'YES'
    End !If local:FounPart = False

    If local:FoundPart = False
        If p_web.GSV('job:Engineer') = ''
            Access:USERS.Clearkey(use:Password_Key)
            use:Password    = p_web.GSV('BookingUserPassword')
            If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                !Found

            Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                !Error
            End !If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        Else !If job:Engineer = ''
            Access:USERS.Clearkey(use:User_Code_Key)
            use:User_Code   = p_web.GSV('job:Engineer')
            If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign

            End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign

        End !If job:Engineer = ''
        Access:STOCK.Clearkey(sto:Location_Key)
        sto:Location    = use:Location
        sto:Part_Number = 'EXCH'
        If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
            !Found
            Access:LOCATION.Clearkey(loc:Location_Key)
            loc:Location    = sto:Location
            If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
                !Found
!                    If ~loc:UseRapidStock
!                        Return
!                    End !If ~loc:UseRapidStock

            Else ! If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
                !Error
            End !If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
            If p_web.GSV('job:Warranty_Job') = 'YES'
               get(warparts,0)
               if access:warparts.primerecord() = level:benign
                   wpr:PArt_Ref_Number      = sto:Ref_Number
                   wpr:ref_number            = p_web.GSV('job:ref_number')
                   wpr:adjustment            = 'YES'
                   wpr:part_number           = 'EXCH'
                   wpr:description           = p_web.GSV('job:Manufacturer') & ' Loan UNIT'
                   wpr:quantity              = 1
                   wpr:warranty_part         = 'NO'
                   wpr:exclude_from_order    = 'YES'
                   wpr:PartAllocated         = 1
                   wpr:Status                = func:Status
!                   wpr:LoanUnit          = True
!                   wpr:SecondLoanUnit    = func:SecondUnit
                   If sto:Assign_Fault_Codes = 'YES'
                       !Try and get the fault codes. This key should get the only record
                       Access:STOMODEL.ClearKey(stm:Ref_Part_Description)
                       stm:Ref_Number  = sto:Ref_Number
                       stm:Part_Number = sto:Part_Number
                       stm:Location    = sto:Location
                       stm:Description = sto:Description
                       If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                           !Found
                           wpr:Fault_Code1  = stm:FaultCode1
                           wpr:Fault_Code2  = stm:FaultCode2
                           wpr:Fault_Code3  = stm:FaultCode3
                           wpr:Fault_Code4  = stm:FaultCode4
                           wpr:Fault_Code5  = stm:FaultCode5
                           wpr:Fault_Code6  = stm:FaultCode6
                           wpr:Fault_Code7  = stm:FaultCode7
                           wpr:Fault_Code8  = stm:FaultCode8
                           wpr:Fault_Code9  = stm:FaultCode9
                           wpr:Fault_Code10 = stm:FaultCode10
                           wpr:Fault_Code11 = stm:FaultCode11
                           wpr:Fault_Code12 = stm:FaultCode12
                       Else!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                       !Error
                       !Assert(0,'<13,10>Fetch Error<13,10>')
                       End!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                   end !If sto:Assign_Fault_Codes = 'YES'
                   if access:warparts.insert()
                       access:warparts.cancelautoinc()
                   end
                End !If Prime

             Else !If job:Warranty_Job = 'YES'
                If p_web.GSV('job:Chargeable_Job') = 'YES'
                    get(parts,0)
                    if access:parts.primerecord() = level:benign
                        !message('At break2')
                        par:PArt_Ref_Number      = sto:Ref_Number
                        par:ref_number            = p_web.GSV('job:ref_number')
                        par:adjustment            = 'YES'
                        par:part_number           = 'EXCH'
                        par:description           = p_web.GSV('job:Manufacturer') & ' Loan UNIT'
                        par:quantity              = 1
                        par:warranty_part         = 'NO'
                        par:exclude_from_order    = 'YES'
                        par:PartAllocated         = 1
                        par:Status                = func:Status
!                        par:LoanUnit          = True
!                        par:SecondLoanUnit    = func:SecondUnit
                        If sto:Assign_Fault_Codes = 'YES'
                           !Try and get the fault codes. This key should get the only record
                           Access:STOMODEL.ClearKey(stm:Ref_Part_Description)
                           stm:Ref_Number  = sto:Ref_Number
                           stm:Part_Number = sto:Part_Number
                           stm:Location    = sto:Location
                           stm:Description = sto:Description
                           If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                               !Found
                               par:Fault_Code1  = stm:FaultCode1
                               par:Fault_Code2  = stm:FaultCode2
                               par:Fault_Code3  = stm:FaultCode3
                               par:Fault_Code4  = stm:FaultCode4
                               par:Fault_Code5  = stm:FaultCode5
                               par:Fault_Code6  = stm:FaultCode6
                               par:Fault_Code7  = stm:FaultCode7
                               par:Fault_Code8  = stm:FaultCode8
                               par:Fault_Code9  = stm:FaultCode9
                               par:Fault_Code10 = stm:FaultCode10
                               par:Fault_Code11 = stm:FaultCode11
                               par:Fault_Code12 = stm:FaultCode12
                           Else!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                               !Error
                               !Assert(0,'<13,10>Fetch Error<13,10>')
                           End!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                        End !If sto:Assign_Fault_Codes = 'YES'
                        if access:parts.insert()
                            access:parts.cancelautoinc()
                        end
                    End !If access:Prime

                End !If job:Chargeable_Job = 'YES'
             End !If job:Warranty_Job = 'YES'

        Else ! If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
             !Error
!            Case Missive('You must have a Part setup in Stock Control with a part number of "EXCH" under the location ' & Clip(use:Location) & '.','ServiceBase 3g',|
!                           'mstop.jpg','/OK')
!                Of 1 ! OK Button
!            End ! Case Missive
        End !If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
    End !If local:FounPart = False
BrowseLoanUnits      PROCEDURE  (NetWebServerWorker p_web)
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256),dim(Net:MaxKeyFields)
idx           String(Net:HashSize)
sub           Long
            End
loa:ESN:IsInvalid  Long
loa:Ref_Number:IsInvalid  Long
loa:Manufacturer:IsInvalid  Long
loa:Model_Number:IsInvalid  Long
loa:MSN:IsInvalid  Long
loa:Colour:IsInvalid  Long
Select:IsInvalid  Long
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:DefaultSelection    String(Net:HashSize)
loc:ActualSelection     String(Net:HashSize)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
!loc:pagename            String(256)
loc:NavButtonPosition    Long
loc:UpdateButtonPosition Long
loc:SelectionMethod      Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorCase         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
loc:lookupdone          Long
loc:FormPopup           Long
ThisView            View(LOAN)
                      Project(loa:Ref_Number)
                      Project(loa:ESN)
                      Project(loa:Ref_Number)
                      Project(loa:Manufacturer)
                      Project(loa:Model_Number)
                      Project(loa:MSN)
                      Project(loa:Colour)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
Loc:popup             Long
loc:poppedup          Long,static,thread
FilesOpened     Long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return 
  End
  GlobalErrors.SetProcedureName('BrowseLoanUnits')
  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseLoanUnits:NoForm')
      loc:NoForm = p_web.GetValue('BrowseLoanUnits:NoForm')
      loc:FormName = p_web.GetValue('BrowseLoanUnits:FormName')
    else
      loc:FormName = 'BrowseLoanUnits_frm'
    End
    p_web.SSV('BrowseLoanUnits:NoForm',loc:NoForm)
    p_web.SSV('BrowseLoanUnits:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseLoanUnits:NoForm')
    loc:FormName = p_web.GSV('BrowseLoanUnits:FormName')
  end
  case p_web.site.formpopups
  of -1 ; loc:FormPopup = Net:Page
  of 0 ; loc:FormPopup = Net:Page
  of 1 ; loc:FormPopup = Net:Popup
  End
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseLoanUnits') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseLoanUnits')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if 1 = 2
  elsif p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  elsif p_web.IfExistsValue('_EIPRow_')
    do CallRow
  elsif p_web.GetValue('_CallPopups') > 0
    do CallPopups
  elsif p_web.IfExistsValue('_GetSecwinSettings')
      p_web.SetValue('Secwin_' & 'BrowseLoanUnits' & 'AccessGroupsArray','1') 
      p_web.SetValue('Secwin_AccessWindowName','BrowseLoanUnits')
      
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallPopups  Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
loc:CallPopups   Long
loc:name         String(255)
  code
  if loc:poppedup = 0
    loc:CallPopups = p_web.GetValue('_CallPopups')
    if loc:CallPopups = 1 !and p_web.GetPreCall('BrowseLoanUnits') = 0! for = 1 include popup divs and scripts for this browse
      !p_web.AddPreCall('BrowseLoanUnits')
      p_web.DivHeader('popup_BrowseLoanUnits','nt-hidden')
      p_web.DivHeader('BrowseLoanUnits',p_web.combine(p_web.site.style.browsediv,'fdiv'))
      p_web.DivFooter()
      p_web.DivFooter()
      loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(400)&', modal: true, position: [''center'',15]'
      packet =  p_web.jQuery('#' & lower('popup_BrowseLoanUnits_div'),'dialog',loc:options) & |
      p_web.jQuery('#' & lower('popup_BrowseLoanUnits_div'),'removeClass','''nt-hidden''')
    End
    ! for > 0 add in form, EIP-lookups and browse object
    do Popups
    loc:popup = 1
    do ClosingScripts
    do SendPacket
    loc:poppedup = 1
  end

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web.DivHeader('BrowseLoanUnits',p_web.combine(p_web.site.style.browsediv,'fdiv'))
  if loc:ParentSilent = 0
    do GenerateBrowse
    if p_web.RequestAjax = 1
      p_web.Script('BrowseProducts.pcv();')
    end
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web.DivFooter()
  do Children
  do Popups
  if loc:poppedup = 0
    do ClosingScripts
  end
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(LOAN,loa:Ref_Number_Key,loc:vorder)
    If False
    ElsIf (loc:vorder = 'LOA:ESN') then p_web.SetValue('BrowseLoanUnits_sort','1')
    ElsIf (loc:vorder = 'LOA:REF_NUMBER') then p_web.SetValue('BrowseLoanUnits_sort','3')
    ElsIf (loc:vorder = 'LOA:MANUFACTURER') then p_web.SetValue('BrowseLoanUnits_sort','7')
    ElsIf (loc:vorder = 'LOA:MODEL_NUMBER') then p_web.SetValue('BrowseLoanUnits_sort','4')
    ElsIf (loc:vorder = 'LOA:MSN') then p_web.SetValue('BrowseLoanUnits_sort','5')
    ElsIf (loc:vorder = 'LOA:COLOUR') then p_web.SetValue('BrowseLoanUnits_sort','6')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseLoanUnits:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseLoanUnits:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseLoanUnits:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseLoanUnits:LookupField')
    loc:selecting = 0
  End

Popups Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  If loc:popup = 0
  End
SetFormAction  Routine
  loc:formaction = 'BrowseLoanUnits'

GotFocusBack   Routine

GenerateBrowse Routine
  data
loc:viewoptions  Long
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  ! Set general Browse options
  loc:NavButtonPosition   = Net:Below
  loc:UpdateButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:PageLoad
  loc:Sorting          = Net:ServerSort
  if p_web.GetValue('_popup_') = 1
    loc:popup = 1
  end
  ! Set Locator Options
  loc:LocatorPosition  = Net:Above
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  p_web.ClearBrowse('BrowseLoanUnits')
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 12
  End
  loc:ActualSelection = ''
  !loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseLoanUnits_sort',net:DontEvaluate)
  If loc:vordernumber = 0
    loc:vordernumber = 1
  End
  p_web.SetSessionValue('BrowseLoanUnits_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(loa:ESN)','-UPPER(loa:ESN)')
    Loc:LocateField = 'loa:ESN'
    Loc:LocatorCase = 0
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'loa:Ref_Number','-loa:Ref_Number')
    Loc:LocateField = 'loa:Ref_Number'
    Loc:LocatorCase = 0
  of 7
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(loa:Manufacturer)','-UPPER(loa:Manufacturer)')
    Loc:LocateField = 'loa:Manufacturer'
    Loc:LocatorCase = 0
  of 4
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(loa:Model_Number)','-UPPER(loa:Model_Number)')
    Loc:LocateField = 'loa:Model_Number'
    Loc:LocatorCase = 0
  of 5
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(loa:MSN)','-UPPER(loa:MSN)')
    Loc:LocateField = 'loa:MSN'
    Loc:LocatorCase = 0
  of 6
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(loa:Colour)','-UPPER(loa:Colour)')
    Loc:LocateField = 'loa:Colour'
    Loc:LocatorCase = 0
  of 2
    Loc:LocateField = ''
    Loc:LocatorCase = 0
  end
  if loc:vorder = ''
    loc:vorder = '+UPPER(loa:Available),+UPPER(loa:Location),+UPPER(loa:ESN)'
  end
  If False ! add range fields to sort order
  ElsIf (p_web.GSV('tmp:LoanStockType') <> '' AND p_web.GSV('tmp:LoanManufacturer') <> '' AND p_web.GSV('tmp:LoanModelNumber') <> '')
  ElsIf (p_web.GSV('tmp:LoanStockType') <> '' AND (p_web.GSV('tmp:LoanModelNumber') = '' OR p_web.GSV('tmp:LoanManufacturer') = ''))
  ElsIf (p_web.GSV('tmp:LoanStockType') = '' AND p_web.GSV('tmp:LoanManufacturer') <> '' AND p_web.GSV('tmp:LoanModelNumber') <> '')
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('loa:ESN')
    loc:SortHeader = p_web.Translate('I.M.E.I. Number')
    p_web.SetSessionValue('BrowseLoanUnits_LocatorPic','@s30')
  Of upper('loa:Ref_Number')
    loc:SortHeader = p_web.Translate('Unit Number')
    p_web.SetSessionValue('BrowseLoanUnits_LocatorPic','@s8')
  Of upper('loa:Manufacturer')
    loc:SortHeader = p_web.Translate('Manufacturer')
    p_web.SetSessionValue('BrowseLoanUnits_LocatorPic','@s30')
  Of upper('loa:Model_Number')
    loc:SortHeader = p_web.Translate('Model Number')
    p_web.SetSessionValue('BrowseLoanUnits_LocatorPic','@s30')
  Of upper('loa:MSN')
    loc:SortHeader = p_web.Translate('M.S.N.')
    p_web.SetSessionValue('BrowseLoanUnits_LocatorPic','@s30')
  Of upper('loa:Colour')
    loc:SortHeader = p_web.Translate('Colour')
    p_web.SetSessionValue('BrowseLoanUnits_LocatorPic','@s30')
  End
  If loc:selecting = 1
    p_web.GetSettings(p_web.GetSessionValue('Push1'))
    loc:selectaction = p_web.FormSettings.ParentPage !p_web.GetSessionValue('BrowseLoanUnits:LookupFrom')
  End!Else
  do SendPacket
  do SetFormAction
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseLoanUnits:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseLoanUnits:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseLoanUnits:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="LOAN"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="loa:Ref_Number_Key"></input><13,10>'
  end
  case p_web.GetValue('refresh') ; of 'next' orof 'previous' orof 'first' orof 'last'
    p_web.Script('try{{BrowseLoanUnits.restoreFocus();} catch(err){{};')
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseLoanUnits',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      case loc:locatortype
      of Net:Position
      orof Net:Date
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
      of Net:Contains
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
      of Net:Begins
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
      End
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,'Locator')&'"><tr>' &|
      Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
      Case Loc:LocatorType
      of Net:Date
        packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator2BrowseLoanUnits','',p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,, p_web.GSV('BrowseLoanUnits_LocatorPic'),,,'onchange="BrowseLoanUnits.locate(''Locator2BrowseLoanUnits'',this.value);"',,0,,,) & '</div></td>'
      Else
        packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator2BrowseLoanUnits',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,'size="30" onchange="BrowseLoanUnits.locate(''Locator2BrowseLoanUnits'',this.value);" ',,,,,,) & '</td>'
      End
      If loc:LocatorSearchButton or loc:LocatorClearButton
        packet = clip(packet) & '<td><div id="BrowseLoanUnits_locate_a" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
        If loc:LocatorSearchButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
        End
        If loc:LocatorClearButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseLoanUnits.cl(''BrowseLoanUnits'');')
        End
        packet = clip(packet) & '</div><13,10></td><13,10>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
      If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'BrowseLoanUnits_locate_a','buttonset',loc:options)
      End
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & p_web.DivHeader('BrowseLoanUnits_table',p_web.Combine(p_web.site.style.BrowseTableDiv,''),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,,'sortable')&'" id="BrowseLoanUnits_tbl">'
  Else
    packet = clip(packet) & p_web.DivHeader('BrowseLoanUnits_table',p_web.Combine(p_web.site.style.BrowseTableDiv,''),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,)&'" id="BrowseLoanUnits_tbl">'
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'1','BrowseLoanUnits',p_web.Translate('I.M.E.I. Number'),'Click here to sort by I.M.E.I.',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'3','BrowseLoanUnits',p_web.Translate('Unit Number'),'Click here to sort by Unit Number',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'7','BrowseLoanUnits',p_web.Translate('Manufacturer'),'Click here to sort by Manufacturer',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'4','BrowseLoanUnits',p_web.Translate('Model Number'),'Click here to sort by Model Number',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'5','BrowseLoanUnits',p_web.Translate('M.S.N.'),'Click here to sort by M.S.N.',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'6','BrowseLoanUnits',p_web.Translate('Colour'),'Click here to sort by Colour',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
      If loc:Selecting = 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'2','BrowseLoanUnits',p_web.Translate(),,,,,,,0,loc:Sorting,'Button')
          do AddPacket
          loc:columns += 1
      End ! Selecting
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,12,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  If Instring('loa:ref_number',lower(loc:vorder),1,1) = 0 !and LOAN{prop:SQLDriver} = 1
    loc:vorder = Choose(loc:vorder='','loa:Ref_Number',clip(loc:vorder) & ',' & 'loa:Ref_Number')
  End
  Loc:Selected = Choose(p_web.IfExistsValue('loa:Ref_Number'),p_web.GetValue('loa:Ref_Number'),p_web.GetSessionValue('loa:Ref_Number'))
  ThisView{prop:order} = p_web.CleanFilter(ThisView,clip(loc:vorder))
  If False  ! Generate Filter
  ElsIf (p_web.GSV('tmp:LoanStockType') <> '' AND p_web.GSV('tmp:LoanManufacturer') <> '' AND p_web.GSV('tmp:LoanModelNumber') <> '')
      loc:FilterWas = 'Upper(loa:Location) = Upper(''' & p_web.GSV('BookingSiteLocation') & ''') AND Upper(loa:Stock_Type) = Upper(''' & p_web.GSV('tmp:LoanStockType') & ''') AND Upper(loa:Available) = ''AVL'' AND Upper(loa:Model_Number) = Upper(''' & p_web.GSV('tmp:LoanModelNumber') & ''')'
  ElsIf (p_web.GSV('tmp:LoanStockType') <> '' AND (p_web.GSV('tmp:LoanModelNumber') = '' OR p_web.GSV('tmp:LoanManufacturer') = ''))
      loc:FilterWas = 'Upper(loa:Location) = Upper(''' & p_web.GSV('BookingSiteLocation') & ''') AND Upper(loa:Stock_Type) = Upper(''' & p_web.GSV('tmp:LoanStockType') & ''') AND Upper(loa:Available) = ''AVL'''
  ElsIf (p_web.GSV('tmp:LoanStockType') = '' AND p_web.GSV('tmp:LoanManufacturer') <> '' AND p_web.GSV('tmp:LoanModelNumber') <> '')
      loc:FilterWas = 'Upper(loa:Available) = ''AVL'' AND Upper(loa:Location) = Upper(''' & p_web.GSV('BookingSiteLocation') & ''') AND Upper(loa:Model_Number) = Upper(''' & p_web.GSV('tmp:LoanModelNumber') & ''')'
  Else
        loc:FilterWas = 'Upper(loa:Available) = ''AVL'' AND Upper(loa:Location) = Upper(''' & p_web.GSV('BookingSiteLocation') & ''')'
  End
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseLoanUnits',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseLoanUnits_Filter')
    p_web.SetSessionValue('BrowseLoanUnits_FirstValue','')
    p_web.SetSessionValue('BrowseLoanUnits_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,LOAN,loa:Ref_Number_Key,loc:PageRows,'BrowseLoanUnits',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,loc:ViewOptions,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled,Loc:LocatorCase)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or Loc:LocatorType = Net:Date or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position or loc:LocatorType = Net:Date
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If LOAN{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(LOAN,loc:firstvalue)
              Reset(ThisView,LOAN)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If LOAN{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(LOAN,loc:lastvalue)
            Reset(ThisView,LOAN)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(loa:Ref_Number)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td class="'&p_web.combine(p_web.site.style.BrowseEmpty,)&'">'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:NavButtonPosition=Net:Above or (loc:NavButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseLoanUnits_nav_a" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseLoanUnits.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseLoanUnits.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseLoanUnits.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseLoanUnits.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseLoanUnits_nav_a','buttonset',loc:options)
          End
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:UpdateButtonPosition=Net:Above or (loc:UpdateButtonPosition=Net:Both and loc:found > 0))
    If loc:selecting = 1 !and loc:parent = ''
      if loc:popup
        packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:BrowseCancelButton,'BrowseLoanUnits',,,loc:popup)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
      end
      TableQueue.Kind = Net:BeforeTable
      do AddPacket
    End
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseLoanUnits',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseLoanUnits_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseLoanUnits_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          case loc:locatortype
          of Net:Position
          orof Net:Date
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
          of Net:Contains
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
          of Net:Begins
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
          End
          packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,'Locator')&'"><tr>' &|
          Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
          Case Loc:LocatorType
          of Net:Date
            packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator1BrowseLoanUnits','',p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,, p_web.GSV('BrowseLoanUnits_LocatorPic'),,,'onchange="BrowseLoanUnits.locate(''Locator1BrowseLoanUnits'',this.value);"',,0,,,) & '</div></td>'
          Else
            packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator1BrowseLoanUnits',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,'size="30" onchange="BrowseLoanUnits.locate(''Locator1BrowseLoanUnits'',this.value);" ',,,,,,) & '</td>'
          End
          If loc:LocatorSearchButton or loc:LocatorClearButton
            packet = clip(packet) & '<td><div id="BrowseLoanUnits_locate_b" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
            If loc:LocatorSearchButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
            End
            If loc:LocatorClearButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseLoanUnits.cl(''BrowseLoanUnits'');')
            End
            packet = clip(packet) & '</div><13,10></td><13,10>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
          If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseLoanUnits_locate_b','buttonset',loc:options)
          End
      End
    End
  End
  p_web.SetSessionValue('BrowseLoanUnits_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseLoanUnits_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:NavButtonPosition=Net:Below or loc:NavButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseLoanUnits_nav_b" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseLoanUnits.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseLoanUnits.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseLoanUnits.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseLoanUnits.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseLoanUnits_nav_b','buttonset',loc:options)
          End
        end
        do SendPacket
  End
  If loc:UpdateButtonPosition=Net:Below or loc:UpdateButtonPosition=Net:Both
  If loc:selecting = 1 !and loc:parent = ''
    if loc:popup
      packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:BrowseCancelButton,'BrowseLoanUnits',,,loc:popup)
    else
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
    end
    do SendPacket
  End
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
  data
loc:rowclick  string(1024)
  code
    loc:field = p_web.AddBrowseValue('BrowseLoanUnits','LOAN',loa:Ref_Number_Key) !loa:Ref_Number
    p_web._thisrow = p_web._nocolon('loa:Ref_Number')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if Loc:LocatorValue <> '' and loc:ActualSelection = ''
        loc:checked = 'checked'
        do SetSelection
      elsif loc:ActualSelection = '' and loa:Ref_Number = p_web.GetValue('loa:Ref_Number')
         loc:checked = 'checked'
         do SetSelection
      elsif loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseLoanUnits:LookupField')) = loa:Ref_Number and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      else
        loc:checked = Choose((loa:Ref_Number = loc:selected) and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      end
      If(loc:SelectionMethod  = Net:Radio)
        loc:RowStyle = 'class="' & p_web.combine(p_web.site.style.browserow,) &'"'
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'data-nt-id="'& p_web.AddBrowseValue('BrowseLoanUnits','LOAN',loa:Ref_Number_Key) &'" onclick="'
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If LOAN{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(LOAN)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            loc:DefaultSelection = loc:field
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If LOAN{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(LOAN)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            loc:DefaultSelection = loc:field
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'>'&p_web.CreateInput('radio','_bidv_',clip(loc:field),,loc:checked,'')&'</td>'&CRLF
          If loc:DefaultSelection = ''
            loc:DefaultSelection = loc:field
          End
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          If loc:DefaultSelection = '' or loc:direction < 0
            loc:DefaultSelection = loc:field
          End
        End
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::loa:ESN
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::loa:Ref_Number
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::loa:Manufacturer
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::loa:Model_Number
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::loa:MSN
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::loa:Colour
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        If Loc:Selecting = 1
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::Select
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        End     !Selecting
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr ' & clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Idx = p_web.AddBrowseValue('BrowseLoanUnits','LOAN',loa:Ref_Number_Key)
  TableQueue.Id[1] = loa:Ref_Number

ClosingScripts  Routine
  data
Rtn_SecwinProcedureName         string(252)
  code
  If p_web.RequestAjax = 0
    do SetFormAction
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var btiBrowseLoanUnits;if (btiBrowseLoanUnits != 1){{var BrowseLoanUnits=new browseTable(''BrowseLoanUnits'','''&clip(loc:formname)&|
      ''','''&p_web._jsok('loa:Ref_Number',Net:Parameter)&''',2,'''&clip(loc:divname)&''',1,1,1,'''&|
      clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&|
      p_web.Translate('Are you sure you want to delete this record?')&''','''&clip(loc:ActualSelection)&''','''&clip(loc:selectaction)&''','''&|
      clip(loc:formactiontarget)&''','''','&loc:popup&',0,'''','''',"' & clip(Rtn_SecwinProcedureName) & '");<13,10>'&|
      'BrowseLoanUnits.setGreenBar('''&p_web.ColorWeb(p_web.Site.Style.BrowseHighlightColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOneColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseTwoColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOverColor)&''');<13,10>' &|
      'BrowseLoanUnits.applyGreenBar();btiBrowseLoanUnits=1};<13,10>' &|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseLoanUnits')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseLoanUnits')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseLoanUnits')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseLoanUnits')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(LOAN)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(LOAN)
  Bind(loa:Record)
  Clear(loa:Record)
  NetWebSetSessionPics(p_web,LOAN)

Children Routine
  if loc:selecting = 0
    If p_web.RequestAjax = 0
      do StartChildren
    Else
      do AjaxChildren
    End
  end

AjaxChildren  Routine
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue('loa:Ref_Number',p_web.GetValue('loa:Ref_Number'))
   p_web.DeleteValue('_Clicked')
   loc:found = 1
   do Children
! ----------------------------------------------------------------------------------------
CallRow  Routine
  data
loc:result  long
  code
  do OpenFilesB
  loa:Ref_Number = p_web.GSV('loa:Ref_Number')
  loc:result = p_web._GetFile(LOAN,loa:Ref_Number_Key)
  loc:eip = 1
  loc:viewstate = p_web.escape(p_web.Base64Encode(clip(loa:Ref_Number)))
  do BrowseRow
  do SendPacket
  do ClosefilesB

! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(LOAN)
  Case upper(p_web.GetValue('_EIPClm'))
  Else
    case p_web.GetValue('_action_')
    of Net:InsertRecord
    orof Net:ChangeRecord
    orof Net:DeleteRecord
    orof Net:CopyRecord
      loc:eip = 0
      do CallBrowse
    end
  End
  do GotFocusBack
  p_web._CloseFile(LOAN)
! ----------------------------------------------------------------------------------------
value::loa:ESN   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseLoanUnits_loa:ESN_'&loa:Ref_Number,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(loa:ESN,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::loa:Ref_Number   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseLoanUnits_loa:Ref_Number_'&loa:Ref_Number,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(loa:Ref_Number,'@s8')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::loa:Manufacturer   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseLoanUnits_loa:Manufacturer_'&loa:Ref_Number,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(loa:Manufacturer,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::loa:Model_Number   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseLoanUnits_loa:Model_Number_'&loa:Ref_Number,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(loa:Model_Number,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::loa:MSN   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseLoanUnits_loa:MSN_'&loa:Ref_Number,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(loa:MSN,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::loa:Colour   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseLoanUnits_loa:Colour_'&loa:Ref_Number,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(loa:Colour,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::Select   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseLoanUnits_Select_'&loa:Ref_Number,,net:crc,,loc:extra)
        packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'BrowseLoanUnits',p_web.AddBrowseValue('BrowseLoanUnits','LOAN',loa:Ref_Number_Key),,loc:popup)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END
  return
!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
CheckForDuplicate  Routine
SetSelection  Routine
  loc:ActualSelection = loc:field
  p_web.SetSessionValue('loa:Ref_Number',loa:Ref_Number)

MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:ActualSelection = ''
    loc:ActualSelection = loc:DefaultSelection
    p_web.GetBrowseValue(loc:ActualSelection,Net:Web:SessionQueue) ! so children are primed with correct sessionValue
  End

  If loc:ActualSelection <> ''
    TableQueue.Kind = Net:RowTable
    get(TableQueue,TableQueue.Kind)
    if Errorcode() = 0
      TableQueue.Row = sub(TableQueue.Row,1,len(clip(TableQueue.Row))-1) &  ' data-nt-rows="'&loc:RowsHigh&'" data-nt-value="'&clip(loc:ActualSelection)&'"' & '>'
      Put(TableQueue)
    End
  End

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead class="'&p_web.combine(p_web.site.style.BrowseHeader,'')&'"><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot class="'&p_web.combine(p_web.site.style.BrowseFooter,'BrowseFooter')&'"><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody class="'&p_web.combine(p_web.site.style.BrowseBody,)&'"><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table><13,10>' & p_web.DivFooter(Net:NoSend) ! Table Div
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
FormLoanUnitFilter   PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
tmp:LoanStockType    STRING(30)                            !
tmp:LoanModelNumber  STRING(30)                            !
tmp:LoanManufacturer STRING(30)                            !
locAllStockTypes     BYTE                                  !
FilesOpened     Long
STOCKTYP::State  USHORT
MODELNUM::State  USHORT
MANUFACT::State  USHORT
tmp:LoanStockType:IsInvalid  Long
tmp:LoanManufacturer:IsInvalid  Long
tmp:LoanModelNumber:IsInvalid  Long
BrowseLoanUnits:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
tmp:LoanStockType_OptionView   View(STOCKTYP)
                          Project(stp:Stock_Type)
                        End
tmp:LoanManufacturer_OptionView   View(MANUFACT)
                          Project(man:Manufacturer)
                        End
tmp:LoanModelNumber_OptionView   View(MODELNUM)
                          Project(mod:Model_Number)
                        End
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormLoanUnitFilter')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'FormLoanUnitFilter_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('FormLoanUnitFilter','')
    p_web.DivHeader('FormLoanUnitFilter',p_web.combine(p_web.site.style.formdiv,'fdiv'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('FormLoanUnitFilter') = 0
        p_web.AddPreCall('FormLoanUnitFilter')
        p_web.DivHeader('popup_FormLoanUnitFilter','nt-hidden')
        p_web.DivHeader('FormLoanUnitFilter',p_web.combine(p_web.site.style.formdiv,'fdiv'))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_FormLoanUnitFilter_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_FormLoanUnitFilter_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormLoanUnitFilter',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_FormLoanUnitFilter',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormLoanUnitFilter',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_FormLoanUnitFilter',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormLoanUnitFilter',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormLoanUnitFilter',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_FormLoanUnitFilter',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormLoanUnitFilter',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_FormLoanUnitFilter',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormLoanUnitFilter',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormLoanUnitFilter',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormLoanUnitFilter',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('FormLoanUnitFilter')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(STOCKTYP)
  p_web._OpenFile(MODELNUM)
  p_web._OpenFile(MANUFACT)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(STOCKTYP)
  p_Web._CloseFile(MODELNUM)
  p_Web._CloseFile(MANUFACT)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE
  loc:EipClm = upper(p_web.GetValue('_EIPClm'))
  p_web.DeleteValue('_EIPClm')
  case loc:EipClm
  of ''
    case upper(p_web.GetValue('_calledfrom_'))
    of upper('BrowseLoanUnits')
      do Value::BrowseLoanUnits
    end
  end

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FormLoanUnitFilter_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'FormLoanUnitFilter'
    end
    p_web.formsettings.proc = 'FormLoanUnitFilter'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'tmp:LoanStockType'
    p_web.setsessionvalue('showtab_FormLoanUnitFilter',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(STOCKTYP)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:LoanManufacturer')
  Of 'tmp:LoanModelNumber'
    p_web.setsessionvalue('showtab_FormLoanUnitFilter',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(MODELNUM)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.')
  End
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  if p_web.IfExistsValue('tmp:LoanStockType') = 0
    p_web.SetSessionValue('tmp:LoanStockType',tmp:LoanStockType)
  Else
    tmp:LoanStockType = p_web.GetSessionValue('tmp:LoanStockType')
  End
  if p_web.IfExistsValue('tmp:LoanManufacturer') = 0
    p_web.SetSessionValue('tmp:LoanManufacturer',tmp:LoanManufacturer)
  Else
    tmp:LoanManufacturer = p_web.GetSessionValue('tmp:LoanManufacturer')
  End
  if p_web.IfExistsValue('tmp:LoanModelNumber') = 0
    p_web.SetSessionValue('tmp:LoanModelNumber',tmp:LoanModelNumber)
  Else
    tmp:LoanModelNumber = p_web.GetSessionValue('tmp:LoanModelNumber')
  End

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('tmp:LoanStockType')
    tmp:LoanStockType = p_web.GetValue('tmp:LoanStockType')
    p_web.SetSessionValue('tmp:LoanStockType',tmp:LoanStockType)
  Else
    tmp:LoanStockType = p_web.GetSessionValue('tmp:LoanStockType')
  End
  if p_web.IfExistsValue('tmp:LoanManufacturer')
    tmp:LoanManufacturer = p_web.GetValue('tmp:LoanManufacturer')
    p_web.SetSessionValue('tmp:LoanManufacturer',tmp:LoanManufacturer)
  Else
    tmp:LoanManufacturer = p_web.GetSessionValue('tmp:LoanManufacturer')
  End
  if p_web.IfExistsValue('tmp:LoanModelNumber')
    tmp:LoanModelNumber = p_web.GetValue('tmp:LoanModelNumber')
    p_web.SetSessionValue('tmp:LoanModelNumber',tmp:LoanModelNumber)
  Else
    tmp:LoanModelNumber = p_web.GetSessionValue('tmp:LoanModelNumber')
  End

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('FormLoanUnitFilter_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferFormLoanUnitFilter')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormLoanUnitFilter_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormLoanUnitFilter_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormLoanUnitFilter_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction

GenerateForm   Routine
  do LoadRelatedRecords
  p_web.SSV('filterStockType','LOAN')
 tmp:LoanStockType = p_web.RestoreValue('tmp:LoanStockType')
 tmp:LoanManufacturer = p_web.RestoreValue('tmp:LoanManufacturer')
 tmp:LoanModelNumber = p_web.RestoreValue('tmp:LoanModelNumber')
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Browse Loan Units') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Browse Loan Units',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_FormLoanUnitFilter',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_FormLoanUnitFilter0_div')&'">'&p_web.Translate('Filter Loan Units')&'</a></li>'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_FormLoanUnitFilter1_div')&'">'&p_web.Translate('Available Loan Units')&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  do GenerateTab1
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="FormLoanUnitFilter_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      packet = clip(packet) & '</div><13,10>' ! end id="FormLoanUnitFilter_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'FormLoanUnitFilter_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="FormLoanUnitFilter_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'FormLoanUnitFilter_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:LoanStockType')
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_FormLoanUnitFilter')>0,p_web.GSV('showtab_FormLoanUnitFilter'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_FormLoanUnitFilter'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_FormLoanUnitFilter') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_FormLoanUnitFilter'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_FormLoanUnitFilter')>0,p_web.GSV('showtab_FormLoanUnitFilter'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_FormLoanUnitFilter') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Filter Loan Units') & ''''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Available Loan Units') & ''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_FormLoanUnitFilter_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_FormLoanUnitFilter')>0,p_web.GSV('showtab_FormLoanUnitFilter'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"FormLoanUnitFilter",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_FormLoanUnitFilter')>0,p_web.GSV('showtab_FormLoanUnitFilter'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_FormLoanUnitFilter_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('FormLoanUnitFilter') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('FormLoanUnitFilter')
    p_web.SetValue('_CallPopups',2)
    If p_web.GetPreCall('BrowseLoanUnits') = 0
      p_web.SetValue('BrowseLoanUnits:NoForm',1)
      p_web.SetValue('BrowseLoanUnits:FormName',loc:formname)
      p_web.SetValue('BrowseLoanUnits:parentIs','Form')
      p_web.SetValue('_parentProc','FormLoanUnitFilter')
      BrowseLoanUnits(p_web)
      p_web.SetValue('_CallPopups',0)
      p_web.DeleteValue('BrowseLoanUnits:NoForm')
      p_web.DeleteValue('BrowseLoanUnits:FormName')
      p_web.DeleteValue('BrowseLoanUnits:parentIs')
      p_web.DeleteValue('_parentProc')
    End
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Filter Loan Units')&'</a></h3>' & CRLF & p_web.DivHeader('tab_FormLoanUnitFilter0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_FormLoanUnitFilter0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_FormLoanUnitFilter0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_FormLoanUnitFilter0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Filter Loan Units')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_FormLoanUnitFilter0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Filter Loan Units')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_FormLoanUnitFilter0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Filter Loan Units')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_FormLoanUnitFilter0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:LoanStockType
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&290&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:LoanStockType
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:LoanStockType
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:LoanManufacturer
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&290&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:LoanManufacturer
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:LoanManufacturer
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:LoanModelNumber
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&290&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:LoanModelNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:LoanModelNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab1  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Available Loan Units')&'</a></h3>' & CRLF & p_web.DivHeader('tab_FormLoanUnitFilter1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_FormLoanUnitFilter1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_FormLoanUnitFilter1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_FormLoanUnitFilter1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Available Loan Units')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_FormLoanUnitFilter1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Available Loan Units')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_FormLoanUnitFilter1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Available Loan Units')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_FormLoanUnitFilter1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::BrowseLoanUnits
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Prompt::tmp:LoanStockType  Routine
  packet = clip(packet) & p_web.DivHeader('FormLoanUnitFilter_' & p_web._nocolon('tmp:LoanStockType') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Stock Type'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:LoanStockType  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:LoanStockType = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:LoanStockType = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:LoanStockType  ! copies value to session value if valid.
  do Value::tmp:LoanStockType
  do SendAlert
  do Comment::tmp:LoanStockType ! allows comment style to be updated.
  do Value::BrowseLoanUnits  !1
  do Value::tmp:LoanManufacturer  !1
  do Value::tmp:LoanModelNumber  !1

ValidateValue::tmp:LoanStockType  Routine
    If not (1=0)
    tmp:LoanStockType = Upper(tmp:LoanStockType)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:LoanStockType',tmp:LoanStockType).
    End

Value::tmp:LoanStockType  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormLoanUnitFilter_' & p_web._nocolon('tmp:LoanStockType') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,p_web.site.style.formselect,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:retrying
    tmp:LoanStockType = p_web.RestoreValue('tmp:LoanStockType')
    do ValidateValue::tmp:LoanStockType
    If tmp:LoanStockType:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- DROPLIST ---
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:LoanStockType'',''formloanunitfilter_tmp:loanstocktype_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:LoanStockType')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('tmp:AllStockTypes') = 1,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:LoanStockType',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
  loc:fieldclass = ''
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('tmp:LoanStockType') = 0
    p_web.SetSessionValue('tmp:LoanStockType','')
  end
    packet = clip(packet) & p_web.CreateOption('== All Stock Types ==','',choose('' = p_web.getsessionvalue('tmp:LoanStockType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(STOCKTYP)
  bind(stp:Record)
  p_web._OpenFile(MODELNUM)
  bind(mod:Record)
  p_web._OpenFile(MANUFACT)
  bind(man:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(tmp:LoanStockType_OptionView)
  tmp:LoanStockType_OptionView{prop:filter} = p_web.CleanFilter(tmp:LoanStockType_OptionView,'stp:Available = 1 AND Upper(stp:Use_Loan) = ''YES''')
  tmp:LoanStockType_OptionView{prop:order} = p_web.CleanFilter(tmp:LoanStockType_OptionView,'UPPER(stp:Stock_Type)')
  Set(tmp:LoanStockType_OptionView)
  Loop
    Next(tmp:LoanStockType_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('tmp:LoanStockType') = 0
      p_web.SetSessionValue('tmp:LoanStockType',stp:Stock_Type)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(Upper(stp:Stock_Type),stp:Stock_Type,choose(stp:Stock_Type = p_web.getsessionvalue('tmp:LoanStockType')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(tmp:LoanStockType_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(STOCKTYP)
  p_Web._CloseFile(MODELNUM)
  p_Web._CloseFile(MANUFACT)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:LoanStockType  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:LoanStockType:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormLoanUnitFilter_' & p_web._nocolon('tmp:LoanStockType') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:LoanManufacturer  Routine
  packet = clip(packet) & p_web.DivHeader('FormLoanUnitFilter_' & p_web._nocolon('tmp:LoanManufacturer') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Manufacturer'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:LoanManufacturer  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:LoanManufacturer = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:LoanManufacturer = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:LoanManufacturer  ! copies value to session value if valid.
  do Value::tmp:LoanManufacturer
  do SendAlert
  do Comment::tmp:LoanManufacturer ! allows comment style to be updated.
  do Value::tmp:LoanModelNumber  !1
  do Value::BrowseLoanUnits  !1

ValidateValue::tmp:LoanManufacturer  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:LoanManufacturer',tmp:LoanManufacturer).
    End

Value::tmp:LoanManufacturer  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormLoanUnitFilter_' & p_web._nocolon('tmp:LoanManufacturer') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,p_web.site.style.formselect,'FormEntry',)
  If loc:retrying
    tmp:LoanManufacturer = p_web.RestoreValue('tmp:LoanManufacturer')
    do ValidateValue::tmp:LoanManufacturer
    If tmp:LoanManufacturer:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- DROPLIST ---
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:LoanManufacturer'',''formloanunitfilter_tmp:loanmanufacturer_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:LoanManufacturer')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:LoanManufacturer',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
  loc:fieldclass = ''
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('tmp:LoanManufacturer') = 0
    p_web.SetSessionValue('tmp:LoanManufacturer','')
  end
    packet = clip(packet) & p_web.CreateOption('== All Manufacturers ==','',choose('' = p_web.getsessionvalue('tmp:LoanManufacturer')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(STOCKTYP)
  bind(stp:Record)
  p_web._OpenFile(MODELNUM)
  bind(mod:Record)
  p_web._OpenFile(MANUFACT)
  bind(man:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(tmp:LoanManufacturer_OptionView)
  tmp:LoanManufacturer_OptionView{prop:order} = p_web.CleanFilter(tmp:LoanManufacturer_OptionView,'UPPER(man:Manufacturer)')
  Set(tmp:LoanManufacturer_OptionView)
  Loop
    Next(tmp:LoanManufacturer_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('tmp:LoanManufacturer') = 0
      p_web.SetSessionValue('tmp:LoanManufacturer',man:Manufacturer)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,man:Manufacturer,choose(man:Manufacturer = p_web.getsessionvalue('tmp:LoanManufacturer')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(tmp:LoanManufacturer_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(STOCKTYP)
  p_Web._CloseFile(MODELNUM)
  p_Web._CloseFile(MANUFACT)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:LoanManufacturer  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:LoanManufacturer:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormLoanUnitFilter_' & p_web._nocolon('tmp:LoanManufacturer') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:LoanModelNumber  Routine
  packet = clip(packet) & p_web.DivHeader('FormLoanUnitFilter_' & p_web._nocolon('tmp:LoanModelNumber') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Model Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:LoanModelNumber  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:LoanModelNumber = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:LoanModelNumber = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:LoanModelNumber  ! copies value to session value if valid.
  do Value::tmp:LoanModelNumber
  do SendAlert
  do Comment::tmp:LoanModelNumber ! allows comment style to be updated.
  do Value::BrowseLoanUnits  !1

ValidateValue::tmp:LoanModelNumber  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:LoanModelNumber',tmp:LoanModelNumber).
    End

Value::tmp:LoanModelNumber  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormLoanUnitFilter_' & p_web._nocolon('tmp:LoanModelNumber') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,p_web.site.style.formselect,'FormEntry',)
  If loc:retrying
    tmp:LoanModelNumber = p_web.RestoreValue('tmp:LoanModelNumber')
    do ValidateValue::tmp:LoanModelNumber
    If tmp:LoanModelNumber:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- DROPLIST ---
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:LoanModelNumber'',''formloanunitfilter_tmp:loanmodelnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:LoanModelNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:LoanModelNumber',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
  loc:fieldclass = ''
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('tmp:LoanModelNumber') = 0
    p_web.SetSessionValue('tmp:LoanModelNumber','')
  end
    packet = clip(packet) & p_web.CreateOption('== All Model Numbers ==','',choose('' = p_web.getsessionvalue('tmp:LoanModelNumber')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(STOCKTYP)
  bind(stp:Record)
  p_web._OpenFile(MODELNUM)
  bind(mod:Record)
  p_web._OpenFile(MANUFACT)
  bind(man:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(tmp:LoanModelNumber_OptionView)
  tmp:LoanModelNumber_OptionView{prop:filter} = p_web.CleanFilter(tmp:LoanModelNumber_OptionView,'Upper(mod:Manufacturer ) = Upper(''' & p_web.GSV('tmp:LoanManufacturer') & ''')')
  tmp:LoanModelNumber_OptionView{prop:order} = p_web.CleanFilter(tmp:LoanModelNumber_OptionView,'UPPER(mod:Model_Number)')
  Set(tmp:LoanModelNumber_OptionView)
  Loop
    Next(tmp:LoanModelNumber_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('tmp:LoanModelNumber') = 0
      p_web.SetSessionValue('tmp:LoanModelNumber',mod:Model_Number)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,mod:Model_Number,choose(mod:Model_Number = p_web.getsessionvalue('tmp:LoanModelNumber')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(tmp:LoanModelNumber_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(STOCKTYP)
  p_Web._CloseFile(MODELNUM)
  p_Web._CloseFile(MANUFACT)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:LoanModelNumber  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:LoanModelNumber:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormLoanUnitFilter_' & p_web._nocolon('tmp:LoanModelNumber') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::BrowseLoanUnits  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  Elsif true
    p_web.StoreValue('loa:Ref_Number')
  End
  do ValidateValue::BrowseLoanUnits  ! copies value to session value if valid.
  do Comment::BrowseLoanUnits ! allows comment style to be updated.

ValidateValue::BrowseLoanUnits  Routine
    If not (1=0)
    End

Value::BrowseLoanUnits  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(1=0,1,0))
  ! --- BROWSE ---  BrowseLoanUnits --
  p_web.SetValue('BrowseLoanUnits:NoForm',1)
  p_web.SetValue('BrowseLoanUnits:FormName',loc:formname)
  p_web.SetValue('BrowseLoanUnits:parentIs','Form')
  p_web.SetValue('_parentProc','FormLoanUnitFilter')
  if p_web.RequestAjax = 0
    p_web.SSV('FormLoanUnitFilter:_popup_',p_web.GetValue('_popup_')) ! stores the current browse popup state
    packet = clip(packet) & '<div id="'&lower('FormLoanUnitFilter_BrowseLoanUnits_embedded_div')&'"><!-- Net:BrowseLoanUnits --></div><13,10>'
    do SendPacket
    p_web.DivHeader('FormLoanUnitFilter_' & lower('BrowseLoanUnits') & '_value')
    p_web.DivFooter()
  else
    if p_web.GetValue('_popup_') = 1
      p_web.SSV('FormLoanUnitFilter:_popup_',1)
    elsif p_web.GSV('FormLoanUnitFilter:_popup_') = 1
      p_web.SetValue('_popup_',1)
    end
    packet = clip(packet) & '<!-- Net:BrowseLoanUnits --><13,10>'
  end
  do SendPacket
Comment::BrowseLoanUnits  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if BrowseLoanUnits:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormLoanUnitFilter_' & p_web._nocolon('BrowseLoanUnits') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('FormLoanUnitFilter_nexttab_' & 0)
    tmp:LoanStockType = p_web.GSV('tmp:LoanStockType')
    do ValidateValue::tmp:LoanStockType
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:LoanStockType
      !do SendAlert
      do Comment::tmp:LoanStockType ! allows comment style to be updated.
      !exit
    End
    tmp:LoanManufacturer = p_web.GSV('tmp:LoanManufacturer')
    do ValidateValue::tmp:LoanManufacturer
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:LoanManufacturer
      !do SendAlert
      do Comment::tmp:LoanManufacturer ! allows comment style to be updated.
      !exit
    End
    tmp:LoanModelNumber = p_web.GSV('tmp:LoanModelNumber')
    do ValidateValue::tmp:LoanModelNumber
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:LoanModelNumber
      !do SendAlert
      do Comment::tmp:LoanModelNumber ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('FormLoanUnitFilter_nexttab_' & 1)
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_FormLoanUnitFilter_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('FormLoanUnitFilter_tab_' & 0)
    do GenerateTab0
  of lower('FormLoanUnitFilter_tmp:LoanStockType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:LoanStockType
      of event:timer
        do Value::tmp:LoanStockType
        do Comment::tmp:LoanStockType
      else
        do Value::tmp:LoanStockType
      end
  of lower('FormLoanUnitFilter_tmp:LoanManufacturer_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:LoanManufacturer
      of event:timer
        do Value::tmp:LoanManufacturer
        do Comment::tmp:LoanManufacturer
      else
        do Value::tmp:LoanManufacturer
      end
  of lower('FormLoanUnitFilter_tmp:LoanModelNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:LoanModelNumber
      of event:timer
        do Value::tmp:LoanModelNumber
        do Comment::tmp:LoanModelNumber
      else
        do Value::tmp:LoanModelNumber
      end
  of lower('FormLoanUnitFilter_tab_' & 1)
    do GenerateTab1
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('FormLoanUnitFilter_form:ready_',1)

  p_web.SetSessionValue('FormLoanUnitFilter_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_FormLoanUnitFilter',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('FormLoanUnitFilter_form:ready_',1)
  p_web.SetSessionValue('FormLoanUnitFilter_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormLoanUnitFilter',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('FormLoanUnitFilter_form:ready_',1)
  p_web.SetSessionValue('FormLoanUnitFilter_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('FormLoanUnitFilter:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('FormLoanUnitFilter_form:ready_',1)
  p_web.SetSessionValue('FormLoanUnitFilter_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('FormLoanUnitFilter:Primed',0)
  p_web.setsessionvalue('showtab_FormLoanUnitFilter',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
      If not (1=0)
        If not (p_web.GSV('tmp:AllStockTypes') = 1)
          If p_web.IfExistsValue('tmp:LoanStockType')
            tmp:LoanStockType = p_web.GetValue('tmp:LoanStockType')
          End
        End
      End
      If not (1=0)
          If p_web.IfExistsValue('tmp:LoanManufacturer')
            tmp:LoanManufacturer = p_web.GetValue('tmp:LoanManufacturer')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('tmp:LoanModelNumber')
            tmp:LoanModelNumber = p_web.GetValue('tmp:LoanModelNumber')
          End
      End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormLoanUnitFilter_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormLoanUnitFilter_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
    do ValidateValue::tmp:LoanStockType
    If loc:Invalid then exit.
    do ValidateValue::tmp:LoanManufacturer
    If loc:Invalid then exit.
    do ValidateValue::tmp:LoanModelNumber
    If loc:Invalid then exit.
  ! tab = 2
    loc:InvalidTab += 1
    do ValidateValue::BrowseLoanUnits
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('FormLoanUnitFilter:Primed',0)
  p_web.StoreValue('tmp:LoanStockType')
  p_web.StoreValue('tmp:LoanManufacturer')
  p_web.StoreValue('tmp:LoanModelNumber')
  p_web.StoreValue('')

BrowseSMSHistory     PROCEDURE  (NetWebServerWorker p_web)
locSMSSent           STRING(5)                             !
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256),dim(Net:MaxKeyFields)
idx           String(Net:HashSize)
sub           Long
            End
sms:DateInserted:IsInvalid  Long
sms:TimeInserted:IsInvalid  Long
locSMSSent:IsInvalid  Long
sms:MSISDN:IsInvalid  Long
sms:MSG:IsInvalid  Long
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:DefaultSelection    String(Net:HashSize)
loc:ActualSelection     String(Net:HashSize)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
!loc:pagename            String(256)
loc:NavButtonPosition    Long
loc:UpdateButtonPosition Long
loc:SelectionMethod      Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorCase         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
loc:lookupdone          Long
loc:FormPopup           Long
ThisView            View(SMSMAIL)
                      Project(sms:RecordNumber)
                      Project(sms:DateInserted)
                      Project(sms:TimeInserted)
                      Project(sms:MSISDN)
                      Project(sms:MSG)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
Loc:popup             Long
loc:poppedup          Long,static,thread
FilesOpened     Long
JOBS::State  USHORT
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  GlobalErrors.SetProcedureName('BrowseSMSHistory')
  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseSMSHistory:NoForm')
      loc:NoForm = p_web.GetValue('BrowseSMSHistory:NoForm')
      loc:FormName = p_web.GetValue('BrowseSMSHistory:FormName')
    else
      loc:FormName = 'BrowseSMSHistory_frm'
    End
    p_web.SSV('BrowseSMSHistory:NoForm',loc:NoForm)
    p_web.SSV('BrowseSMSHistory:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseSMSHistory:NoForm')
    loc:FormName = p_web.GSV('BrowseSMSHistory:FormName')
  end
  case p_web.site.formpopups
  of -1 ; loc:FormPopup = Net:Page
  of 0 ; loc:FormPopup = Net:Page
  of 1 ; loc:FormPopup = Net:Popup
  End
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseSMSHistory') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseSMSHistory')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if 1 = 2
  elsif p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  elsif p_web.IfExistsValue('_EIPRow_')
    do CallRow
  elsif p_web.GetValue('_CallPopups') > 0
    do CallPopups
  elsif p_web.IfExistsValue('_GetSecwinSettings')
      p_web.SetValue('Secwin_' & 'BrowseSMSHistory' & 'AccessGroupsArray','1') 
      p_web.SetValue('Secwin_AccessWindowName','BrowseSMSHistory')
      
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallPopups  Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
loc:CallPopups   Long
loc:name         String(255)
  code
  if loc:poppedup = 0
    loc:CallPopups = p_web.GetValue('_CallPopups')
    if loc:CallPopups = 1 !and p_web.GetPreCall('BrowseSMSHistory') = 0! for = 1 include popup divs and scripts for this browse
      !p_web.AddPreCall('BrowseSMSHistory')
      p_web.DivHeader('popup_BrowseSMSHistory','nt-hidden')
      p_web.DivHeader('BrowseSMSHistory',p_web.combine(p_web.site.style.browsediv,'adiv'))
      p_web.DivFooter()
      p_web.DivFooter()
      loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(400)&', modal: true, position: [''center'',15]'
      packet =  p_web.jQuery('#' & lower('popup_BrowseSMSHistory_div'),'dialog',loc:options) & |
      p_web.jQuery('#' & lower('popup_BrowseSMSHistory_div'),'removeClass','''nt-hidden''')
    End
    ! for > 0 add in form, EIP-lookups and browse object
    do Popups
    loc:popup = 1
    do ClosingScripts
    do SendPacket
    loc:poppedup = 1
  end

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web.DivHeader('BrowseSMSHistory',p_web.combine(p_web.site.style.browsediv,'adiv'))
  if loc:ParentSilent = 0
    do GenerateBrowse
    if p_web.RequestAjax = 1
      p_web.Script('BrowseProducts.pcv();')
    end
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web.DivFooter()
  do Children
  do Popups
  if loc:poppedup = 0
    do ClosingScripts
  end
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(SMSMAIL,sms:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'SMS:DATEINSERTED') then p_web.SetValue('BrowseSMSHistory_sort','1')
    ElsIf (loc:vorder = 'SMS:TIMEINSERTED') then p_web.SetValue('BrowseSMSHistory_sort','2')
    ElsIf (loc:vorder = 'LOCSMSSENT') then p_web.SetValue('BrowseSMSHistory_sort','3')
    ElsIf (loc:vorder = 'SMS:MSISDN') then p_web.SetValue('BrowseSMSHistory_sort','4')
    ElsIf (loc:vorder = 'SMS:MSG') then p_web.SetValue('BrowseSMSHistory_sort','5')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseSMSHistory:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseSMSHistory:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseSMSHistory:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseSMSHistory:LookupField')
    loc:selecting = 0
  End

Popups Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  If loc:popup = 0
  End
SetFormAction  Routine
  loc:formaction = 'BrowseSMSHistory'

GotFocusBack   Routine

GenerateBrowse Routine
  data
loc:viewoptions  Long
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  ! Set general Browse options
  loc:NavButtonPosition   = Net:Below
  loc:UpdateButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:FileLoad
  loc:FillBack         = 0
  loc:Sorting          = Net:ServerSort
  if p_web.GetValue('_popup_') = 1
    loc:popup = 1
  end
  ! Set Locator Options
  loc:LocatorPosition  = Net:Below
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  p_web.ClearBrowse('BrowseSMSHistory')
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 10
  End
  loc:ActualSelection = ''
  !loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseSMSHistory_sort',net:DontEvaluate)
  p_web.SetSessionValue('BrowseSMSHistory_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'sms:DateInserted','-sms:DateInserted')
    Loc:LocateField = 'sms:DateInserted'
    Loc:LocatorCase = 0
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'sms:TimeInserted','-sms:TimeInserted')
    Loc:LocateField = 'sms:TimeInserted'
    Loc:LocatorCase = 0
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'locSMSSent','-locSMSSent')
    Loc:LocateField = 'locSMSSent'
    Loc:LocatorCase = 0
  of 4
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(sms:MSISDN)','-UPPER(sms:MSISDN)')
    Loc:LocateField = 'sms:MSISDN'
    Loc:LocatorCase = 0
  of 5
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(sms:MSG)','-UPPER(sms:MSG)')
    Loc:LocateField = 'sms:MSG'
    Loc:LocatorCase = 0
  end
  if loc:vorder = ''
    loc:vorder = '+UPPER(sms:SendToSMS),+UPPER(sms:SMSSent)'
  end
  If False ! add range fields to sort order
  Else
    If Instring('SMS:REFNUMBER',upper(loc:vOrder),1,1) = 0
      loc:vOrder = 'sms:RefNumber,' & loc:vorder
    End
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('sms:DateInserted')
    loc:SortHeader = p_web.Translate('Date Inserted')
    p_web.SetSessionValue('BrowseSMSHistory_LocatorPic','@d6')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
    loc:LocatorType = Net:Date
  Of upper('sms:TimeInserted')
    loc:SortHeader = p_web.Translate('Time Inserted')
    p_web.SetSessionValue('BrowseSMSHistory_LocatorPic','@t1b')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
  Of upper('locSMSSent')
    loc:SortHeader = p_web.Translate('Sent')
    p_web.SetSessionValue('BrowseSMSHistory_LocatorPic','@s5')
  Of upper('sms:MSISDN')
    loc:SortHeader = p_web.Translate('Receipient')
    p_web.SetSessionValue('BrowseSMSHistory_LocatorPic','@s12')
  Of upper('sms:MSG')
    loc:SortHeader = p_web.Translate('Message')
    p_web.SetSessionValue('BrowseSMSHistory_LocatorPic','@s160')
  End
  If loc:selecting = 1
    p_web.GetSettings(p_web.GetSessionValue('Push1'))
    loc:selectaction = p_web.FormSettings.ParentPage !p_web.GetSessionValue('BrowseSMSHistory:LookupFrom')
  End!Else
  loc:CloseAction = 'PickLoanUnit'
  do SendPacket
  do SetFormAction
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseSMSHistory:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseSMSHistory:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseSMSHistory:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="SMSMAIL"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="sms:RecordNumberKey"></input><13,10>'
  end
  If p_web.Translate('Browse SMS History') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.browseSubHeading,)&'">'&p_web.Translate('Browse SMS History',0)&'</div>'&CRLF
  End
  case p_web.GetValue('refresh') ; of 'next' orof 'previous' orof 'first' orof 'last'
    p_web.Script('try{{BrowseSMSHistory.restoreFocus();} catch(err){{};')
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseSMSHistory',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      case loc:locatortype
      of Net:Position
      orof Net:Date
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
      of Net:Contains
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
      of Net:Begins
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
      End
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,'Locator')&'"><tr>' &|
      Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
      Case Loc:LocatorType
      of Net:Date
        packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator2BrowseSMSHistory','',p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,, p_web.GSV('BrowseSMSHistory_LocatorPic'),,,'onchange="BrowseSMSHistory.locate(''Locator2BrowseSMSHistory'',this.value);"',,0,,,) & '</div></td>'
      Else
        packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator2BrowseSMSHistory',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,'size="30" onchange="BrowseSMSHistory.locate(''Locator2BrowseSMSHistory'',this.value);" ',,,,,,) & '</td>'
      End
      If loc:LocatorSearchButton or loc:LocatorClearButton
        packet = clip(packet) & '<td><div id="BrowseSMSHistory_locate_a" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
        If loc:LocatorSearchButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
        End
        If loc:LocatorClearButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseSMSHistory.cl(''BrowseSMSHistory'');')
        End
        packet = clip(packet) & '</div><13,10></td><13,10>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
      If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'BrowseSMSHistory_locate_a','buttonset',loc:options)
      End
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & p_web.DivHeader('BrowseSMSHistory_table',p_web.Combine(p_web.site.style.BrowseTableDiv,'BrowseLookup'),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,,'sortable')&'" id="BrowseSMSHistory_tbl">'
  Else
    packet = clip(packet) & p_web.DivHeader('BrowseSMSHistory_table',p_web.Combine(p_web.site.style.BrowseTableDiv,'BrowseLookup'),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,)&'" id="BrowseSMSHistory_tbl">'
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'1','BrowseSMSHistory',p_web.Translate('Date Inserted'),'Click here to sort by Date Inserted',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'2','BrowseSMSHistory',p_web.Translate('Time Inserted'),'Click here to sort by Time Inserted',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'3','BrowseSMSHistory',p_web.Translate('Sent'),,,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'4','BrowseSMSHistory',p_web.Translate('Receipient'),'Click here to sort by Receipient',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'5','BrowseSMSHistory',p_web.Translate('Message'),'Click here to sort by Message',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:LocateField = 'sms:DateInserted' then Loc:NoBuffer = 1.
  If Loc:NoBuffer = 0
    Buffer(ThisView,10,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  If Instring('sms:recordnumber',lower(loc:vorder),1,1) = 0 !and SMSMAIL{prop:SQLDriver} = 1
    loc:vorder = Choose(loc:vorder='','sms:RecordNumber',clip(loc:vorder) & ',' & 'sms:RecordNumber')
  End
  Loc:Selected = Choose(p_web.IfExistsValue('sms:RecordNumber'),p_web.GetValue('sms:RecordNumber'),p_web.GetSessionValue('sms:RecordNumber'))
  ThisView{prop:order} = p_web.CleanFilter(ThisView,clip(loc:vorder))
    job:Ref_Number = p_web.RestoreValue('job:Ref_Number')
    loc:FilterWas = 'sms:RefNumber = ' & job:Ref_Number
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseSMSHistory',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseSMSHistory_Filter')
    p_web.SetSessionValue('BrowseSMSHistory_FirstValue','')
    p_web.SetSessionValue('BrowseSMSHistory_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,SMSMAIL,sms:RecordNumberKey,loc:PageRows,'BrowseSMSHistory',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,loc:ViewOptions,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled,Loc:LocatorCase)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or Loc:LocatorType = Net:Date or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position or loc:LocatorType = Net:Date
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If SMSMAIL{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(SMSMAIL,loc:firstvalue)
              Reset(ThisView,SMSMAIL)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If SMSMAIL{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(SMSMAIL,loc:lastvalue)
            Reset(ThisView,SMSMAIL)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(sms:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td class="'&p_web.combine(p_web.site.style.BrowseEmpty,)&'">'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:NavButtonPosition=Net:Above or (loc:NavButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseSMSHistory_nav_a" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseSMSHistory.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseSMSHistory.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseSMSHistory.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseSMSHistory.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseSMSHistory_nav_a','buttonset',loc:options)
          End
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:UpdateButtonPosition=Net:Above or (loc:UpdateButtonPosition=Net:Both and loc:found > 0))
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseSMSHistory',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseSMSHistory_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseSMSHistory_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          case loc:locatortype
          of Net:Position
          orof Net:Date
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
          of Net:Contains
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
          of Net:Begins
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
          End
          packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,'Locator')&'"><tr>' &|
          Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
          Case Loc:LocatorType
          of Net:Date
            packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator1BrowseSMSHistory','',p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,, p_web.GSV('BrowseSMSHistory_LocatorPic'),,,'onchange="BrowseSMSHistory.locate(''Locator1BrowseSMSHistory'',this.value);"',,0,,,) & '</div></td>'
          Else
            packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator1BrowseSMSHistory',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,'Locator'),,'size="30" onchange="BrowseSMSHistory.locate(''Locator1BrowseSMSHistory'',this.value);" ',,,,,,) & '</td>'
          End
          If loc:LocatorSearchButton or loc:LocatorClearButton
            packet = clip(packet) & '<td><div id="BrowseSMSHistory_locate_b" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
            If loc:LocatorSearchButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
            End
            If loc:LocatorClearButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseSMSHistory.cl(''BrowseSMSHistory'');')
            End
            packet = clip(packet) & '</div><13,10></td><13,10>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
          If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseSMSHistory_locate_b','buttonset',loc:options)
          End
      End
    End
  End
  p_web.SetSessionValue('BrowseSMSHistory_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseSMSHistory_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:NavButtonPosition=Net:Below or loc:NavButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseSMSHistory_nav_b" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseSMSHistory.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseSMSHistory.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseSMSHistory.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseSMSHistory.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseSMSHistory_nav_b','buttonset',loc:options)
          End
        end
        do SendPacket
  End
  If loc:UpdateButtonPosition=Net:Below or loc:UpdateButtonPosition=Net:Both
  End
  If loc:selecting = 0 and loc:parent = ''
      if loc:popup
        packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:BrowseCloseButton,'BrowseSMSHistory',,,loc:popup)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCloseButton,loc:Formname,loc:CloseAction)
      end
  End
    do SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
  data
loc:rowclick  string(1024)
  code
    loc:field = p_web.AddBrowseValue('BrowseSMSHistory','SMSMAIL',sms:RecordNumberKey) !sms:RecordNumber
    p_web._thisrow = p_web._nocolon('sms:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if Loc:LocatorValue <> '' and loc:ActualSelection = ''
        loc:checked = 'checked'
        do SetSelection
      elsif loc:ActualSelection = '' and sms:RecordNumber = p_web.GetValue('sms:RecordNumber')
         loc:checked = 'checked'
         do SetSelection
      elsif loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseSMSHistory:LookupField')) = sms:RecordNumber and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      else
        loc:checked = Choose((sms:RecordNumber = loc:selected) and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      end
      If(loc:SelectionMethod  = Net:Radio)
        loc:RowStyle = 'class="' & p_web.combine(p_web.site.style.browserow,) &'"'
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'data-nt-id="'& p_web.AddBrowseValue('BrowseSMSHistory','SMSMAIL',sms:RecordNumberKey) &'" onclick="'
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If SMSMAIL{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(SMSMAIL)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            loc:DefaultSelection = loc:field
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If SMSMAIL{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(SMSMAIL)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            loc:DefaultSelection = loc:field
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'>'&p_web.CreateInput('radio','_bidv_',clip(loc:field),,loc:checked,'')&'</td>'&CRLF
          If loc:DefaultSelection = ''
            loc:DefaultSelection = loc:field
          End
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          If loc:DefaultSelection = '' or loc:direction < 0
            loc:DefaultSelection = loc:field
          End
        End
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::sms:DateInserted
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::sms:TimeInserted
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
            if false
            elsif sms:SMSSent = 'T'
              packet = clip(packet) & '<td><13,10>'
            elsif sms:SMSSent <> 'T'
              packet = clip(packet) & '<td><13,10>'
            else
              packet = clip(packet) & '<td><13,10>'
            end  !False
          end ! loc:eip = 0
          do value::locSMSSent
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::sms:MSISDN
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td Title="'&p_web._jsok(255)&'"><13,10>'
          end ! loc:eip = 0
          do value::sms:MSG
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr ' & clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Idx = p_web.AddBrowseValue('BrowseSMSHistory','SMSMAIL',sms:RecordNumberKey)
  TableQueue.Id[1] = sms:RecordNumber

ClosingScripts  Routine
  data
Rtn_SecwinProcedureName         string(252)
  code
  If p_web.RequestAjax = 0
    do SetFormAction
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var btiBrowseSMSHistory;if (btiBrowseSMSHistory != 1){{var BrowseSMSHistory=new browseTable(''BrowseSMSHistory'','''&clip(loc:formname)&|
      ''','''&p_web._jsok('sms:RecordNumber',Net:Parameter)&''',2,'''&clip(loc:divname)&''',1,1,1,'''&|
      clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&|
      p_web.Translate('Are you sure you want to delete this record?')&''','''&clip(loc:ActualSelection)&''','''&clip(loc:selectaction)&''','''&|
      clip(loc:formactiontarget)&''','''','&loc:popup&',0,'''','''',"' & clip(Rtn_SecwinProcedureName) & '");<13,10>'&|
      'BrowseSMSHistory.setGreenBar('''&p_web.ColorWeb(p_web.Site.Style.BrowseHighlightColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOneColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseTwoColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOverColor)&''');<13,10>' &|
      'BrowseSMSHistory.applyGreenBar();btiBrowseSMSHistory=1};<13,10>' &|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseSMSHistory')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseSMSHistory')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseSMSHistory')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseSMSHistory')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(SMSMAIL)
  p_web._CloseFile(JOBS)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(SMSMAIL)
  Bind(sms:Record)
  Clear(sms:Record)
  NetWebSetSessionPics(p_web,SMSMAIL)
  p_web._OpenFile(JOBS)
  Bind(job:Record)
  NetWebSetSessionPics(p_web,JOBS)

Children Routine
  if loc:selecting = 0
    If p_web.RequestAjax = 0
      do StartChildren
    Else
      do AjaxChildren
    End
  end

AjaxChildren  Routine
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue('sms:RecordNumber',p_web.GetValue('sms:RecordNumber'))
   p_web.DeleteValue('_Clicked')
   loc:found = 1
   do Children
! ----------------------------------------------------------------------------------------
CallRow  Routine
  data
loc:result  long
  code
  do OpenFilesB
  sms:RecordNumber = p_web.GSV('sms:RecordNumber')
  loc:result = p_web._GetFile(SMSMAIL,sms:RecordNumberKey)
  loc:eip = 1
  loc:viewstate = p_web.escape(p_web.Base64Encode(clip(sms:RecordNumber)))
  do BrowseRow
  do SendPacket
  do ClosefilesB

! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(SMSMAIL)
  Case upper(p_web.GetValue('_EIPClm'))
  Else
    case p_web.GetValue('_action_')
    of Net:InsertRecord
    orof Net:ChangeRecord
    orof Net:DeleteRecord
    orof Net:CopyRecord
      loc:eip = 0
      do CallBrowse
    end
  End
  do GotFocusBack
  p_web._CloseFile(SMSMAIL)
! ----------------------------------------------------------------------------------------
value::sms:DateInserted   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseSMSHistory_sms:DateInserted_'&sms:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(sms:DateInserted,'@d6')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sms:TimeInserted   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseSMSHistory_sms:TimeInserted_'&sms:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(sms:TimeInserted,'@t1b')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::locSMSSent   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    elsif sms:SMSSent = 'T' ! alternative condition for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseSMSHistory_locSMSSent_'&sms:RecordNumber,,net:crc,,loc:extra)
              packet = clip(packet) &  p_web.CreateHyperLink(Left(Format('YES','@s5')),,,,loc:javascript,,,0)
    elsif sms:SMSSent <> 'T' ! alternative condition for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseSMSHistory_locSMSSent_'&sms:RecordNumber,,net:crc,,loc:extra)
              packet = clip(packet) &  p_web.CreateHyperLink(Left(Format('NO','@s5')),,,,loc:javascript,,,0)
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseSMSHistory_locSMSSent_'&sms:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(locSMSSent,'@s5')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sms:MSISDN   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseSMSHistory_sms:MSISDN_'&sms:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(sms:MSISDN,'@s12')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sms:MSG   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseSMSHistory_sms:MSG_'&sms:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(sms:MSG,'@s160')),0),,,,loc:javascript,255,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  p_web._OpenFile(JOBS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(JOBS)
     FilesOpened = False
  END
  return
!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
CheckForDuplicate  Routine
SetSelection  Routine
  loc:ActualSelection = loc:field
  p_web.SetSessionValue('sms:RecordNumber',sms:RecordNumber)

MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:ActualSelection = ''
    loc:ActualSelection = loc:DefaultSelection
    p_web.GetBrowseValue(loc:ActualSelection,Net:Web:SessionQueue) ! so children are primed with correct sessionValue
  End

  If loc:ActualSelection <> ''
    TableQueue.Kind = Net:RowTable
    get(TableQueue,TableQueue.Kind)
    if Errorcode() = 0
      TableQueue.Row = sub(TableQueue.Row,1,len(clip(TableQueue.Row))-1) &  ' data-nt-rows="'&loc:RowsHigh&'" data-nt-value="'&clip(loc:ActualSelection)&'"' & '>'
      Put(TableQueue)
    End
  End

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead class="'&p_web.combine(p_web.site.style.BrowseHeader,'')&'"><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot class="'&p_web.combine(p_web.site.style.BrowseFooter,'BrowseFooter')&'"><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody class="'&p_web.combine(p_web.site.style.BrowseBody,)&'"><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table><13,10>' & p_web.DivFooter(Net:NoSend) ! Table Div
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
