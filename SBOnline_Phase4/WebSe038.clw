

   MEMBER('WebServer_Phase4.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSE038.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSE011.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE039.INC'),ONCE        !Req'd for module callout resolution
                     END


PickExchangeUnit     PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
tmp:ExchangeUnitNumber LONG                                !
tmp:UnitDetails      STRING(100)                           !
tmp:ExchangeIMEINumber STRING(30)                          !
tmp:exchangeModelNumber STRING(30)                         !
tmp:MSN              STRING(30)                            !
tmp:ExchangeUnitDetails STRING(60)                         !
tmp:ExchangeAccessories STRING(100)                        !
tmp:ExchangeLocation STRING(100)                           !
tmp:ReplacementValue REAL                                  !
locExchangeAlertMessage STRING(255)                        !
tmp:RemovalReason    BYTE                                  !
locRemovalAlertMessage STRING(255)                         !
tmp:HandsetPartNumber STRING(30)                           !
tmp:HandsetReplacementValue REAL                           !
tmp:NoUnitAvailable  BYTE                                  !
FilesOpened     Long
STOCKALL::State  USHORT
SMSRECVD::State  USHORT
COURIER::State  USHORT
JOBS::State  USHORT
EXCHANGE::State  USHORT
JOBEXACC::State  USHORT
EXCHANGE_ALIAS::State  USHORT
EXCHOR48::State  USHORT
MANUFACT::State  USHORT
PARTS::State  USHORT
STOCK::State  USHORT
WARPARTS::State  USHORT
STOCKTYP::State  USHORT
EXCHHIST::State  USHORT
PRODCODE::State  USHORT
JOBSE::State  USHORT
WEBJOB::State  USHORT
TRDBATCH::State  USHORT
job:ESN:IsInvalid  Long
job:MSN:IsInvalid  Long
locUnitDetails:IsInvalid  Long
job:Charge_Type:IsInvalid  Long
job:Warranty_Charge_Type:IsInvalid  Long
tmp:ExchangeIMEINumber:IsInvalid  Long
locExchangeAlertMessage:IsInvalid  Long
buttonPickExchangeUnit:IsInvalid  Long
tmp:MSN:IsInvalid  Long
tmp:ExchangeUnitDetails:IsInvalid  Long
tmp:ExchangeAccessories:IsInvalid  Long
tmp:ExchangeLocation:IsInvalid  Long
tmp:HandsetPartNumber:IsInvalid  Long
tmp:ReplacementValue:IsInvalid  Long
tmp:HandsetReplacementValue:IsInvalid  Long
tmp:NoUnitAvailable:IsInvalid  Long
job:Exchange_Courier:IsInvalid  Long
job:Exchange_Consignment_Number:IsInvalid  Long
job:Exchange_Despatched:IsInvalid  Long
job:Exchange_Despatched_User:IsInvalid  Long
job:Exchange_Despatch_Number:IsInvalid  Long
button:AmendDespatchDetails:IsInvalid  Long
locRemoveText:IsInvalid  Long
buttonRemoveAttachedUnit:IsInvalid  Long
locRemovalAlertMessage:IsInvalid  Long
tmp:RemovalReason:IsInvalid  Long
buttonConfirmExchangeRemoval:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
tmp:HandsetPartNumber_OptionView   View(PRODCODE)
                          Project(prd:ProductCode)
                        End
job:Exchange_Courier_OptionView   View(COURIER)
                          Project(cou:Courier)
                        End
Loc:User            long
ThisSecwinAccess    string(252)
local       class
AllocateExchangePart    Procedure(String func:Status,Byte func:SecondUnit)
            end
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('PickExchangeUnit')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'PickExchangeUnit_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('PickExchangeUnit','')
    p_web.DivHeader('PickExchangeUnit',p_web.combine(p_web.site.style.formdiv,))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('PickExchangeUnit') = 0
        p_web.AddPreCall('PickExchangeUnit')
        p_web.DivHeader('popup_PickExchangeUnit','nt-hidden')
        p_web.DivHeader('PickExchangeUnit',p_web.combine(p_web.site.style.formdiv,))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_PickExchangeUnit_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_PickExchangeUnit_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferPickExchangeUnit',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_PickExchangeUnit',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPickExchangeUnit',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_PickExchangeUnit',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPickExchangeUnit',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_PickExchangeUnit',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_PickExchangeUnit',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPickExchangeUnit',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_PickExchangeUnit',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPickExchangeUnit',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_PickExchangeUnit',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_PickExchangeUnit',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('PickExchangeUnit')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
addExchangeUnit     Routine
    data
locAuditNotes   String(255)
    code
        Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
        xch:Ref_Number    = p_web.GSV('tmp:ExchangeUnitNumber')
        if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
            ! Found
            Access:MANUFACT.Clearkey(man:Manufacturer_Key)
            man:Manufacturer    = p_web.GSV('job:Manufacturer')
            if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
                ! Found
                if (man:QALoanExchange)
                    xch:Available = 'QA1'

                else ! if (man:QALoanExchange)
                    xch:Available = 'EXC'
                end !if (man:QALoanExchange)
                xch:StatusChangeDate = TODAY() ! #12127 Record status change. (Bryan: 13/07/2011)
                xch:Job_Number = p_web.GSV('job:Ref_Number')
                access:EXCHANGE.tryUpdate()

                if (Access:EXCHHIST.PrimeRecord() = Level:Benign)
                    exh:Ref_Number    = tmp:ExchangeUnitNumber
                    exh:Date    = Today()
                    exh:Time    = Clock()
                    exh:User    = p_web.GSV('BookingUserCode')
                    if (man:QALoanExchange)
                        exh:Status    = 'AWAITING QA. EXCHANGED ON JOB NO: ' & p_web.GSV('job:ref_number')
                    else ! if (man:QALoanExchange)
                        exh:status = 'UNIT EXCHANGED ON JOB NO: ' & p_web.GSV('job:Ref_number')
                    end !

                    if (Access:EXCHHIST.TryInsert() = Level:Benign)
                        ! Inserted
                    else ! if (Access:EXCHHIST.TryInsert() = Level:Benign)
                        ! Error
                        Access:EXCHHIST.CancelAutoInc()
                    end ! if (Access:EXCHHIST.TryInsert() = Level:Benign)
                end ! if (Access:EXCHHIST.PrimeRecord() = Level:Benign)

                locAuditNotes = 'UNIT NUMBER: ' & CLip(xch:ref_number) & |
                    '<13,10>MODEL NUMBER: ' & CLip(xch:model_number) & |
                    '<13,10>I.M.E.I.: ' & CLip(xch:esn)

                if (MSNRequired(xch:Manufacturer))
                    locAuditNotes = Clip(locAuditNotes) & '<13,10>M.S.N.: ' & Clip(xch:MSN)
                end ! if (MSNRequired(xch:Manufacturer))

                locAuditNotes = clip(locAuditNotes) & '<13,10>STOCK TYPE: ' & Clip(xch:Stock_Type)

                p_web.SSV('AddToAudit:Type','EXC')
                p_web.SSV('AddToAudit:Action','EXCHANGE UNIT ATTACHED TO JOB')
                p_web.SSV('AddToAudit:Notes',clip(locAuditNotes))
                addToAudit(p_web)

                if (p_web.GSV('BookingSite') = 'RRC')
                    p_web.SSV('jobe:ExchangeATRRC',1)
                else ! if (p_web.GSV('BookingSite') = 'RRC')
                    p_web.SSV('jobe:ExchangeATRRC',0)
                end !if (p_web.GSV('BookingSite') = 'RRC')

                local.AllocateExchangePart('PIK',0)

                ! Create a new exchange unit for incoming unit

                Access:EXCHANGE_ALIAS.Clearkey(xch_ali:ESN_Only_Key)
                xch_ali:ESN    = xch:ESN
                if (Access:EXCHANGE_ALIAS.TryFetch(xch_ali:ESN_Only_Key) = Level:Benign)
                    ! Found
                    ! Unit already exists, use that one
                    if (p_web.GSV('job:Date_Completed') <> '')
                        xch_ali:Available = 'RTS'
                    else ! if (p_web.GSV('job:Date_Completed') <> '')
                        if (p_web.GSV('job:Workshop') = 'YES')
                            xch_ali:Available = 'REP'
                        else ! if (p_web.GSV('job:Workshop') = 'YES')
                            xch_ali:Available = 'INC'
                        end !if (p_web.GSV('job:Workshop') = 'YES')
                    end !if (p_web.GSV('job:Date_Completed') <> '')
                    xch_ali:StatusChangeDate = TODAY() ! #12127 Record status change. (Bryan: 13/07/2011)
                    xch_ali:Job_Number = p_web.GSV('job:Ref_Number')
                    access:EXCHANGE_ALIAS.tryUpdate()
                else ! if (Access:EXCHANGE_ALIAS.TryFetch(xch_ali:ESN_Only_Key) = Level:Benign)
                    ! Error
                    ! IMEI doesn't exist. Create a new one
                    if (Access:EXCHANGE_ALIAS.PrimeRecord() = Level:Benign)
                        refNo# = xch_ali:Ref_Number
                        xch_ali:Record :=: xch:Record
                        xch_ali:Ref_Number = refNo#

                        if (p_web.GSV('job:Date_Completed') <> '')
                            xch_ali:Available = 'RTS'
                        else ! if (p_web.GSV('job:Date_Completed') <> '')
                            if (p_web.GSV('job:Workshop') = 'YES')
                                xch_ali:Available = 'REP'
                            else ! if (p_web.GSV('job:Workshop') = 'YES')
                                xch_ali:Available = 'INC'
                            end !if (p_web.GSV('job:Workshop') = 'YES')
                        end !if (p_web.GSV('job:Date_Completed') <> '')
                        xch_ali:Job_Number = p_web.GSV('job:Ref_Number')
                        xch_ali:ESN = p_web.GSV('job:ESN')
                        xch_ali:MSN = p_web.GSV('job:MSN')
                        xch_ali:Model_Number = p_web.GSV('job:Model_Number')
                        xch_ali:Manufacturer = p_web.GSV('job:Manufacturer')

                        if (Access:EXCHANGE_ALIAS.TryInsert() = Level:Benign)
                            ! Inserted
                        else ! if (Access:EXCHANGE_ALIAS.TryInsert() = Level:Benign)
                            ! Error
                            Access:EXCHANGE_ALIAS.CancelAutoInc()
                        end ! if (Access:EXCHANGE_ALIAS.TryInsert() = Level:Benign)
                    end ! if (Access:EXCHANGE_ALIAS.PrimeRecord() = Level:Benign)
                End

                ! Mark exchange order as fulfilled
                if (p_web.GSV('jobe:Engineer48HourOption') = 1)

                    Access:EXCHOR48.Clearkey(ex4:AttachedToJobKey)
                    ex4:AttachedToJob    = 0
                    ex4:Location    = p_web.GSV('BookingSiteLocation')
                    ex4:JobNumber    = p_web.GSV('job:Ref_Number')
                    if (Access:EXCHOR48.TryFetch(ex4:AttachedToJobKey) = Level:Benign)
                        ! Found
                        ex4:attachedToJob = 1
                        access:EXCHOR48.tryUpdate()
                    else ! if (Access:EXCHOR48.TryFetch(ex4:AttachedToJobKey) = Level:Benign)
                        ! Error
                    end ! if (Access:EXCHOR48.TryFetch(ex4:AttachedToJobKey) = Level:Benign)
                end ! if (p_web.GSV('jobe:Engineer48HourOption') = 1)

                if (man:QALoanExchange)
                    p_web.SSV('job:Despatched','')
                    p_web.SSV('job:DespatchType','')
                    p_web.SSV('job:Exchange_Status','QA REQUIRED')
                    p_web.SSV('job:Exchange_Unit',p_web.GSV('BookingUserCode'))

                    getStatus(605,0,'EXC',p_web)
                else ! if (man:QALoanExchange)

                    if (p_web.GSV('BookingSite') = 'RRC')
                        p_web.SSV('jobe:DespatchType','EXC')
                        p_web.SSV('jobe:Despatched','REA')
                        p_web.SSV('wob:ReadyToDespatch',1)
                        p_web.SSV('wob:DespatchCourier',p_web.GSV('job:exchange_Courier'))
                        p_web.SSV('jobe:Despatched','REA')
                        p_web.SSV('jobe:DespatchType','EXC')
                        p_web.SSV('wob:DespatchCourier',p_web.GSV('job:Exchange_Courier'))

                    else ! if (p_web.GSV('BookingSite') = 'RRC')
                        p_web.SSV('job:Exchange_Status','AWAITING DESPATCH')
                        p_web.SSV('job:Exchange_User',p_web.GSV('BookingUserCode'))
                        p_web.SSV('job:Exchange_Despatched','')

                        p_web.SSV('job:Despatched','REA')
                        p_web.SSV('job:Despatch_Type','EXC')
                    end ! if (p_web.GSV('BookingSite') = 'RRC')

                    getStatus(110,0,'EXC',p_web)

                end !if (man:QALoanExchange)

                if (p_web.GSV('job:Third_Party_Site') <> '')
                    Access:TRDBATCH.Clearkey(trb:ESN_Only_Key)
                    trb:ESN    = p_web.GSV('job:ESN')
                    set(trb:ESN_Only_Key,trb:ESN_Only_Key)
                    loop
                        if (Access:TRDBATCH.Next())
                            Break
                        end ! if (Access:TRDBATCH.Next())
                        if (trb:ESN    <> p_web.GSV('job:ESN'))
                            Break
                        end ! if (trb:ESN    <> p_web.GSV('job:ESN'))
                        if (trb:Ref_Number = p_web.GSV('job:Ref_Number'))
                            trb:Exchanged = 'YES'
                            access:TRDBATCH.tryUpdate()
                        end ! if (trb:Ref_Number = p_web.GSV('job:Ref_Number'))
                    end ! loop
                end ! if (p_web.GSV('job:Third_Party_Site'))

                if (p_web.GSV('BookingSite') = 'RRC')
                    count# = 0
                    if (p_web.GSV('job:Chargeable_Job') = 'YES')

                        Access:PARTS.Clearkey(par:Part_Number_Key)
                        par:Ref_Number    = job:Ref_Number
                        set(par:Part_Number_Key,par:Part_Number_Key)
                        loop
                            if (Access:PARTS.Next())
                                Break
                            end ! if (Access:PARTS.Next())
                            if (par:Ref_Number    <> job:Ref_Number)
                                Break
                            end ! if (par:Ref_Number    <> job:Ref_Number)
                            if (par:Part_Number <> 'EXCH')
                                count# += 1
                            end ! if (par:Part_Number <> 'EXCH')
                        end ! loop
                    end ! end ! if (p_web.GSV('job:Chargeable_Job') = 'YES')
                    if (count# > 0)
                        loc:alert = 'This job has parts attached. These must be removed before you can send it to the ARC.'
                    else
                        p_web.SSV('jobe:HubRepair',1)
                        p_web.SSV('jobe:HubRepairDate',Today())
                        p_web.SSV('jobe:HubRepairTime',Clock())
                        p_web.SSV('GetStatus:StatusNumber',Sub(GETINI('RRC','StatusSendToARC',,CLIP(PATH())&'\SB2KDEF.INI'),1,3))
                        p_web.SSV('GetStatus:Type','JOB')
                        GetStatus(p_web.GSV('GetStatus:StatusNumber'),0,p_web.GSV('GetStatus:Type'),p_web)

                        Access:REPTYDEF.Clearkey(rtd:ManRepairTypeKey)
                        rtd:Manufacturer    = p_web.GSV('job:Manufacturer')
                        set(rtd:ManRepairTypeKey,rtd:ManRepairTypeKey)
                        loop
                            if (Access:REPTYDEF.Next())
                                Break
                            end ! if (Access:REPTYDEF.Next())
                            if (rtd:Manufacturer    <> p_web.GSV('job:Manufacturer'))
                                Break
                            end ! if (rtd:Manufacturer    <> p_web.GSV('job:Manufacturer'))
                            if (rtd:BER = 10)
                                if (p_web.GSV('job:Chargeable_Job') = 'YES' and p_web.GSV('job:Repair_Type') = '')
                                    p_web.SSV('job:Repair_Type',rtd:Repair_Type)
                                end
                                if (p_web.GSV('job:Warranty_Job') = 'YES' and p_web.GSV('job:Repair_Type_Warranty') = '')
                                    p_web.SSV('job:Repair_Type_Warranty',rtd:Repair_Type)
                                end
                                break
                            end ! if (rtd:BER = 10)
                        end ! loop

                        Access:JOBSENG.Clearkey(joe:UserCodeKey)
                        joe:JobNumber    = p_web.GSV('job:Ref_Number')
                        joe:UserCode    = p_web.GSV('job:Engineer')
                        joe:DateAllocated    = Today()
                        set(joe:UserCodeKey,joe:UserCodeKey)
                        loop
                            if (Access:JOBSENG.Previous())
                                Break
                            end ! if (Access:JOBSEND.Next())
                            if (joe:JobNumber    <> p_web.GSV('job:Ref_Number'))
                                Break
                            end ! if (joe:JobNumber    <> p_web.GSV('job:Ref_Number'))
                            if (joe:UserCode    <> p_web.GSV('job:Engineer'))
                                Break
                            end ! if (joe:UserCode    <> p_web.GSV('job:Engineer'))
                            if (joe:DateAllocated    > Today())
                                Break
                            end ! if (joe:DateAllocated    <> Today())
                            joe:Status = 'HUB'
                            joe:StatusDate = Today()
                            joe:StatusTime = Clock()
                            access:JOBSENG.tryUpdate()
                            break
                        end ! loop
                    end ! if (count# > 0)
                end ! if (p_web.GSV('BookingSite') = 'RRC')

                p_web.SSV('jobe:ExchangePRoductCode','')
                p_web.SSV('jobe:HandsetReplacmentValue',0)
                p_web.SSV('job:Exchange_Unit_Number',p_web.GSV('tmp:ExchangeUnitNumber'))

                if (p_web.GSV('Hide:HandsetPartNumber') = 0)
                    p_web.SSV('jobe:ExchangeProductCode',p_web.GSV('tmp:HandsetPartNumber'))
                    p_web.SSV('jobe:HandsetReplacmentValue',p_web.GSV('tmp:HandsetReplacementValue'))
                end ! if (p_web.GSV('BookingSite') = 'ARC')

                Access:JOBSE.Clearkey(jobe:RefNumberKey)
                jobe:RefNumber    = p_web.GSV('job:Ref_Number')
                if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
                    ! Found
                    p_web.SessionQueueToFile(JOBSE)
                    access:JOBSE.tryUpdate()
                else ! if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
                    ! Error
                end ! if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)

                !Access:WEBJOB.Clearkey(wob:RefNumberKey)
                !wob:RefNumber    = p_web.GSV('job:Ref_Number')
                !if (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
                ! Found
                p_web.SessionQueueToFile(WEBJOB)
                access:WEBJOB.tryUpdate()
                !else ! if (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
                !    ! Error
                !end ! if (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)

                Access:JOBS.Clearkey(job:Ref_Number_Key)
                job:Ref_Number    = p_web.GSV('job:Ref_Number')
                if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                    ! Found
                    p_web.SessionQueueToFile(JOBS)
                    access:JOBS.tryUpdate()
                else ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                    ! Error
                end ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)

                loc:Alert = 'Exchange Unit Added'

                ! Passed From Exchange Allocation
                IF (p_web.GSV('FromURL') = 'ExchangeAllocation')
                    Access:SBO_GenericFile.ClearKey(sbogen:RecordNumberKey)
                    sbogen:RecordNumber = p_web.GSV('sbogen:RecordNumber')
                    IF (Access:SBO_GenericFile.TryFetch(sbogen:RecordNumberKey) = Level:Benign)
                        Access:SBO_GenericFile.DeleteRecord(0)
                    END
                END

            else ! if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
                ! Error
            end ! if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
        else ! if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
            ! Error
        end ! if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
clearExchangeDetails        routine
    p_web.SSV('tmp:MSN','')
    p_web.SSV('tmp:ExchangeUnitDetails','')
    p_web.SSV('tmp:ExchangeLocation','')
    p_web.SSV('tmp:ReplacementValue','')
    p_web.SSV('tmp:ExchangeIMEINumber','')
    p_web.SSV('tmp:ExchangeUnitNumber','')
    p_web.SSV('tmp:ExchangeModelNumber','')
    p_web.SSV('tmp:ExchangeAccessories','')
    p_web.SSV('tmp:ExchangeLocation','')
    p_web.SSV('tmp:HandsetPartNumber','')
    p_web.SSV('tmp:HandsetReplacementValue','')


getExchangeDetails      Routine
    p_web.SSV('locExchangeAlertMessage','')

    do lookupExchangeDetails

    if (p_web.GSV('jobe:Engineer48HourOption') = 1)
        Access:EXCHOR48.Clearkey(ex4:AttachedToJobKey)
        ex4:AttachedToJob    = 0
        ex4:Location    = p_web.GSV('BookingSiteLocation')
        ex4:JobNumber    = p_web.GSV('job:Ref_Number')
        if (Access:EXCHOR48.TryFetch(ex4:AttachedToJobKey) = Level:Benign)
            ! Found
            Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
            xch:Ref_Number    = ex4:orderUnitNumber
            if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
                ! Found
                if (p_web.GSV('tmp:exchangeIMEINumber') <> xch:ESN)
                    p_web.SSV('locExchangeAlertMessage','Warning! An Exchange Unit has been ordered for this job under the 48 Hour Exchange Process. The I.M.E.I. Number you have selected is NOT the unit that has been ordered.')
                end ! if (p_web.GSV('tmp:exchangeIMEINumber') <> xch:ESN)
            else ! if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
                ! Error
            end ! if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
        else ! if (Access:EXCHOR48.TryFetch(ex4:AttachedToJobKey) = Level:Benign)
            ! Error
        end ! if (Access:EXCHOR48.TryFetch(ex4:AttachedToJobKey) = Level:Benign)
    end ! if (p_web.GSV('jobe:Engineer48HourOption') = 1)

    Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
    xch:Ref_Number    = p_web.GSV('tmp:ExchangeUnitNumber')
    if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
        ! Found
    else ! if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
        ! Error
    end ! if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
    if (xch:Model_Number <> p_web.GSV('job:Model_Number'))
        p_web.SSV('locExchangeAlertMessage','Warning! The selected Exchange Unit has a different Model Number!')
    end ! if (xch:Model_Number <> p_web.GSV('job:Model_Number')
lookupExchangeDetails       Routine
    p_web.SSV('Hide:HandsetPartNumber',1)
    p_web.SSV('tmp:HandsetPartNumber','')
    p_web.SSV('tmp:HandsetReplacementValue','')

    Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
    xch:Ref_Number    = p_web.GSV('tmp:ExchangeUnitNumber')
    if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign and xch:Ref_Number > 0)
        ! Found
        p_web.SSV('tmp:ExchangeIMEINumber',xch:ESN)
        p_web.SSV('tmp:MSN',xch:MSN)
        p_web.SSV('tmp:ExchangeUnitDetails',Clip(xch:Ref_Number) & ': ' & Clip(xch:Manufacturer) & ' ' & Clip(xch:Model_Number))
        p_web.SSV('tmp:ExchangeLocation',Clip(xch:Location) & ' / ' & Clip(xch:Stock_Type))
        p_web.SSV('tmp:exchangeModelNumber',xch:Model_Number)

        if (p_web.GSV('BookingSite') = 'ARC')
            Access:MANUFACT.Clearkey(man:Manufacturer_Key)
            man:Manufacturer    = xch:Manufacturer
            if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
                ! Found
            else ! if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
                ! Error
            end ! if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
            if (man:UseProdCodesForEXC = 1)
                p_web.SSV('Hide:HandsetPartNumber',0)
                p_web.SSV('tmp:HandsetPartNumber',p_web.GSV('jobe:ExchangeProductCode'))
                p_web.SSV('tmp:HandsetReplacementValue',p_web.GSV('jobe:HandsetReplacmentValue'))
            end ! if (man:UseProdCodesForEXC = 1)
        end ! if (p_web.GSV('job:Exchange_unit_Number') > 0 and p_web.GSV('BookingSite') = 'ARC')

    else ! if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
        ! Error
        p_web.SSV('tmp:ExchangeIMEINumber','')
        p_web.SSV('tmp:MSN','')
        p_web.SSV('tmp:ExchangeUnitDetails','')
        p_web.SSV('tmp:ExchangeLocation','')
    end ! if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
clearVariables      ROUTINE
    p_web.DeleteSessionValue('ReadOnly:ExchangeDespatchDetails')
OpenFiles  ROUTINE
  p_web._OpenFile(STOCKALL)
  p_web._OpenFile(SMSRECVD)
  p_web._OpenFile(COURIER)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(EXCHANGE)
  p_web._OpenFile(JOBEXACC)
  p_web._OpenFile(EXCHANGE_ALIAS)
  p_web._OpenFile(EXCHOR48)
  p_web._OpenFile(MANUFACT)
  p_web._OpenFile(PARTS)
  p_web._OpenFile(STOCK)
  p_web._OpenFile(WARPARTS)
  p_web._OpenFile(STOCKTYP)
  p_web._OpenFile(EXCHHIST)
  p_web._OpenFile(PRODCODE)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(WEBJOB)
  p_web._OpenFile(TRDBATCH)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(STOCKALL)
  p_Web._CloseFile(SMSRECVD)
  p_Web._CloseFile(COURIER)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(EXCHANGE)
  p_Web._CloseFile(JOBEXACC)
  p_Web._CloseFile(EXCHANGE_ALIAS)
  p_Web._CloseFile(EXCHOR48)
  p_Web._CloseFile(MANUFACT)
  p_Web._CloseFile(PARTS)
  p_Web._CloseFile(STOCK)
  p_Web._CloseFile(WARPARTS)
  p_Web._CloseFile(STOCKTYP)
  p_Web._CloseFile(EXCHHIST)
  p_Web._CloseFile(PRODCODE)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(TRDBATCH)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  IF (p_web.IfExistsValue('ReturnURL'))
      p_web.StoreValue('ReturnURL')
  END
  
  IF (p_web.IfExistsValue('FromURL'))
      p_web.StoreValue('FromURL')
  END
  
  IF (p_web.IfExistsValue('Warning'))
      p_web.StoreValue('Warning')
      IF p_web.GSV('Warning') <> 'EXCH'
          loc:Alert = 'Warning! The selected part is NOT an Exchange Part.'
      END
  END
  
  
  ! Passed From Exchange Allocation
  IF (p_web.IfExistsValue('sbogen:RecordNumber') AND p_web.GSV('FromURL') = 'ExchangeAllocation')
      p_web.StoreValue('sbogen:RecordNumber')
      Access:SBO_GenericFile.ClearKey(sbogen:RecordNumberKey)
      sbogen:RecordNumber = p_web.GSV('sbogen:RecordNumber')
      IF (Access:SBO_GenericFile.TryFetch(sbogen:RecordNumberKey) = Level:Benign)
          Access:JOBS.ClearKey(job:Ref_Number_Key)
          job:Ref_Number = sbogen:Long1
          IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
              p_web.FileToSessionQueue(JOBS)
          END
  
          Access:JOBSE.ClearKey(jobe:RefNumberKey)
          jobe:RefNumber = job:Ref_Number
          IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
              p_web.FileToSessionQueue(JOBSE)
          END
  
          Access:WEBJOB.ClearKey(wob:RefNumberKey)
          wob:RefNumber = job:Ref_Number
          IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
              p_web.FileToSessionQueue(WEBJOB)
          END
  
      END
  END
  
  ! Passed From Stock Allocation
  IF (p_web.IfExistsSessionValue('stl:RecordNumber') AND p_web.GSV('FromURL') = 'StockAllocation')
      Access:STOCKALL.ClearKey(stl:RecordNumberKey)
      stl:RecordNumber = p_web.GSV('stl:RecordNumber')
      IF (Access:STOCKALL.TryFetch(stl:RecordNumberKey) = Level:Benign)
  
          Access:JOBS.ClearKey(job:Ref_Number_Key)
          job:Ref_Number = stl:JobNumber
          IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
              p_web.FileToSessionQueue(JOBS)
          ELSE
              p_web.site.SaveButton.Class = 'NoShow'
              loc:alert = 'Unable To Find Job Details'
          END
  
          Access:JOBSE.ClearKey(jobe:RefNumberKey)
          jobe:RefNumber = job:Ref_Number
          IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
              p_web.FileToSessionQueue(JOBSE)
  
          END
  
          Access:WEBJOB.ClearKey(wob:RefNumberKey)
          wob:RefNumber = job:Ref_Number
          IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
              p_web.FileToSessionQueue(WEBJOB)
          END
  
      END
  
  END
  
  ! Security Checks
      if (p_web.GSV('Job:ViewOnly') = 1)
          p_web.SSV('ReadOnly:ExchangeIMEINumber',1)
      else !if (p_web.GSV('Job:ViewOnly') = 1)
          if (securityCheckFailed(p_web.GSV('BookingUserPassword'),'JOBS - AMEND EXCHANGE UNIT'))
              p_web.SSV('ReadOnly:ExchangeIMEINumber',1)
          else
              p_web.SSV('ReadOnly:ExchangeIMEINumber',0)
          end ! if (securityCheckFailed(p_web.GSV('BookingUserPassword'),' '))
      end !if (p_web.GSV('Job:ViewOnly') = 1)
  
      p_web.SSV('tmp:ExchangeUnitNumber',p_web.GSV('job:Exchange_Unit_Number'))
  
      if (p_web.GSV('job:Exchange_Unit_Number') > 0)
          do lookupExchangeDetails
      else
          do clearExchangeDetails
  
      end
  
      p_web.SSV('locExchangeAlertMessage','')
      p_web.SSV('locRemoveAlertMessage','')
      p_web.SSV('Hide:RemovalReason',1)
      p_web.SSV('tmp:RemovalReason',0)
      p_web.SSV('tmp:NoUnitAvailable',0)
  
  
  p_web.SetValue('PickExchangeUnit_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'PickExchangeUnit'
    end
    p_web.formsettings.proc = 'PickExchangeUnit'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine
      do clearVariables

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('job:ESN')
    p_web.SetPicture('job:ESN','@s20')
  End
  p_web.SetSessionPicture('job:ESN','@s20')
  If p_web.IfExistsValue('job:MSN')
    p_web.SetPicture('job:MSN','@s20')
  End
  p_web.SetSessionPicture('job:MSN','@s20')
  If p_web.IfExistsValue('job:Charge_Type')
    p_web.SetPicture('job:Charge_Type','@s30')
  End
  p_web.SetSessionPicture('job:Charge_Type','@s30')
  If p_web.IfExistsValue('job:Warranty_Charge_Type')
    p_web.SetPicture('job:Warranty_Charge_Type','@s30')
  End
  p_web.SetSessionPicture('job:Warranty_Charge_Type','@s30')
  If p_web.IfExistsValue('tmp:ReplacementValue')
    p_web.SetPicture('tmp:ReplacementValue','@n14.2')
  End
  p_web.SetSessionPicture('tmp:ReplacementValue','@n14.2')
  If p_web.IfExistsValue('tmp:HandsetReplacementValue')
    p_web.SetPicture('tmp:HandsetReplacementValue','@n14.2')
  End
  p_web.SetSessionPicture('tmp:HandsetReplacementValue','@n14.2')
  If p_web.IfExistsValue('job:Exchange_Consignment_Number')
    p_web.SetPicture('job:Exchange_Consignment_Number','@s30')
  End
  p_web.SetSessionPicture('job:Exchange_Consignment_Number','@s30')
  If p_web.IfExistsValue('job:Exchange_Despatched')
    p_web.SetPicture('job:Exchange_Despatched','@d06b')
  End
  p_web.SetSessionPicture('job:Exchange_Despatched','@d06b')
  If p_web.IfExistsValue('job:Exchange_Despatched_User')
    p_web.SetPicture('job:Exchange_Despatched_User','@s3')
  End
  p_web.SetSessionPicture('job:Exchange_Despatched_User','@s3')
  If p_web.IfExistsValue('job:Exchange_Despatch_Number')
    p_web.SetPicture('job:Exchange_Despatch_Number','@s8')
  End
  p_web.SetSessionPicture('job:Exchange_Despatch_Number','@s8')

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'tmp:ExchangeIMEINumber'
    p_web.setsessionvalue('showtab_PickExchangeUnit',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(EXCHANGE)
        p_web.setsessionvalue('tmp:ExchangeUnitNumber',xch:Ref_Number)
        do getExchangeDetails
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.locExchangeAlertMessage')
  End
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'job:Exchange_Courier'
    p_web.setsessionvalue('showtab_PickExchangeUnit',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(COURIER)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.job:Exchange_Consignment_Number')
  End
  If p_web.GSV('job:Exchange_Unit_Number') > 0
    loc:TabNumber += 1
  End
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  if p_web.IfExistsValue('job:ESN') = 0
    p_web.SetSessionValue('job:ESN',job:ESN)
  Else
    job:ESN = p_web.GetSessionValue('job:ESN')
  End
  if p_web.IfExistsValue('job:MSN') = 0
    p_web.SetSessionValue('job:MSN',job:MSN)
  Else
    job:MSN = p_web.GetSessionValue('job:MSN')
  End
  if p_web.IfExistsValue('job:Charge_Type') = 0
    p_web.SetSessionValue('job:Charge_Type',job:Charge_Type)
  Else
    job:Charge_Type = p_web.GetSessionValue('job:Charge_Type')
  End
  if p_web.IfExistsValue('job:Warranty_Charge_Type') = 0
    p_web.SetSessionValue('job:Warranty_Charge_Type',job:Warranty_Charge_Type)
  Else
    job:Warranty_Charge_Type = p_web.GetSessionValue('job:Warranty_Charge_Type')
  End
  if p_web.IfExistsValue('tmp:ExchangeIMEINumber') = 0
    p_web.SetSessionValue('tmp:ExchangeIMEINumber',tmp:ExchangeIMEINumber)
  Else
    tmp:ExchangeIMEINumber = p_web.GetSessionValue('tmp:ExchangeIMEINumber')
  End
  if p_web.IfExistsValue('locExchangeAlertMessage') = 0
    p_web.SetSessionValue('locExchangeAlertMessage',locExchangeAlertMessage)
  Else
    locExchangeAlertMessage = p_web.GetSessionValue('locExchangeAlertMessage')
  End
  if p_web.IfExistsValue('tmp:MSN') = 0
    p_web.SetSessionValue('tmp:MSN',tmp:MSN)
  Else
    tmp:MSN = p_web.GetSessionValue('tmp:MSN')
  End
  if p_web.IfExistsValue('tmp:ExchangeUnitDetails') = 0
    p_web.SetSessionValue('tmp:ExchangeUnitDetails',tmp:ExchangeUnitDetails)
  Else
    tmp:ExchangeUnitDetails = p_web.GetSessionValue('tmp:ExchangeUnitDetails')
  End
  if p_web.IfExistsValue('tmp:ExchangeAccessories') = 0
    p_web.SetSessionValue('tmp:ExchangeAccessories',tmp:ExchangeAccessories)
  Else
    tmp:ExchangeAccessories = p_web.GetSessionValue('tmp:ExchangeAccessories')
  End
  if p_web.IfExistsValue('tmp:ExchangeLocation') = 0
    p_web.SetSessionValue('tmp:ExchangeLocation',tmp:ExchangeLocation)
  Else
    tmp:ExchangeLocation = p_web.GetSessionValue('tmp:ExchangeLocation')
  End
  if p_web.IfExistsValue('tmp:HandsetPartNumber') = 0
    p_web.SetSessionValue('tmp:HandsetPartNumber',tmp:HandsetPartNumber)
  Else
    tmp:HandsetPartNumber = p_web.GetSessionValue('tmp:HandsetPartNumber')
  End
  if p_web.IfExistsValue('tmp:ReplacementValue') = 0
    p_web.SetSessionValue('tmp:ReplacementValue',tmp:ReplacementValue)
  Else
    tmp:ReplacementValue = p_web.GetSessionValue('tmp:ReplacementValue')
  End
  if p_web.IfExistsValue('tmp:HandsetReplacementValue') = 0
    p_web.SetSessionValue('tmp:HandsetReplacementValue',tmp:HandsetReplacementValue)
  Else
    tmp:HandsetReplacementValue = p_web.GetSessionValue('tmp:HandsetReplacementValue')
  End
  if p_web.IfExistsValue('tmp:NoUnitAvailable') = 0
    p_web.SetSessionValue('tmp:NoUnitAvailable',tmp:NoUnitAvailable)
  Else
    tmp:NoUnitAvailable = p_web.GetSessionValue('tmp:NoUnitAvailable')
  End
  if p_web.IfExistsValue('job:Exchange_Courier') = 0
    p_web.SetSessionValue('job:Exchange_Courier',job:Exchange_Courier)
  Else
    job:Exchange_Courier = p_web.GetSessionValue('job:Exchange_Courier')
  End
  if p_web.IfExistsValue('job:Exchange_Consignment_Number') = 0
    p_web.SetSessionValue('job:Exchange_Consignment_Number',job:Exchange_Consignment_Number)
  Else
    job:Exchange_Consignment_Number = p_web.GetSessionValue('job:Exchange_Consignment_Number')
  End
  if p_web.IfExistsValue('job:Exchange_Despatched') = 0
    p_web.SetSessionValue('job:Exchange_Despatched',job:Exchange_Despatched)
  Else
    job:Exchange_Despatched = p_web.GetSessionValue('job:Exchange_Despatched')
  End
  if p_web.IfExistsValue('job:Exchange_Despatched_User') = 0
    p_web.SetSessionValue('job:Exchange_Despatched_User',job:Exchange_Despatched_User)
  Else
    job:Exchange_Despatched_User = p_web.GetSessionValue('job:Exchange_Despatched_User')
  End
  if p_web.IfExistsValue('job:Exchange_Despatch_Number') = 0
    p_web.SetSessionValue('job:Exchange_Despatch_Number',job:Exchange_Despatch_Number)
  Else
    job:Exchange_Despatch_Number = p_web.GetSessionValue('job:Exchange_Despatch_Number')
  End
  if p_web.IfExistsValue('locRemovalAlertMessage') = 0
    p_web.SetSessionValue('locRemovalAlertMessage',locRemovalAlertMessage)
  Else
    locRemovalAlertMessage = p_web.GetSessionValue('locRemovalAlertMessage')
  End
  if p_web.IfExistsValue('tmp:RemovalReason') = 0
    p_web.SetSessionValue('tmp:RemovalReason',tmp:RemovalReason)
  Else
    tmp:RemovalReason = p_web.GetSessionValue('tmp:RemovalReason')
  End

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('job:ESN')
    job:ESN = p_web.GetValue('job:ESN')
    p_web.SetSessionValue('job:ESN',job:ESN)
  Else
    job:ESN = p_web.GetSessionValue('job:ESN')
  End
  if p_web.IfExistsValue('job:MSN')
    job:MSN = p_web.GetValue('job:MSN')
    p_web.SetSessionValue('job:MSN',job:MSN)
  Else
    job:MSN = p_web.GetSessionValue('job:MSN')
  End
  if p_web.IfExistsValue('job:Charge_Type')
    job:Charge_Type = p_web.GetValue('job:Charge_Type')
    p_web.SetSessionValue('job:Charge_Type',job:Charge_Type)
  Else
    job:Charge_Type = p_web.GetSessionValue('job:Charge_Type')
  End
  if p_web.IfExistsValue('job:Warranty_Charge_Type')
    job:Warranty_Charge_Type = p_web.GetValue('job:Warranty_Charge_Type')
    p_web.SetSessionValue('job:Warranty_Charge_Type',job:Warranty_Charge_Type)
  Else
    job:Warranty_Charge_Type = p_web.GetSessionValue('job:Warranty_Charge_Type')
  End
  if p_web.IfExistsValue('tmp:ExchangeIMEINumber')
    tmp:ExchangeIMEINumber = p_web.GetValue('tmp:ExchangeIMEINumber')
    p_web.SetSessionValue('tmp:ExchangeIMEINumber',tmp:ExchangeIMEINumber)
  Else
    tmp:ExchangeIMEINumber = p_web.GetSessionValue('tmp:ExchangeIMEINumber')
  End
  if p_web.IfExistsValue('locExchangeAlertMessage')
    locExchangeAlertMessage = p_web.GetValue('locExchangeAlertMessage')
    p_web.SetSessionValue('locExchangeAlertMessage',locExchangeAlertMessage)
  Else
    locExchangeAlertMessage = p_web.GetSessionValue('locExchangeAlertMessage')
  End
  if p_web.IfExistsValue('tmp:MSN')
    tmp:MSN = p_web.GetValue('tmp:MSN')
    p_web.SetSessionValue('tmp:MSN',tmp:MSN)
  Else
    tmp:MSN = p_web.GetSessionValue('tmp:MSN')
  End
  if p_web.IfExistsValue('tmp:ExchangeUnitDetails')
    tmp:ExchangeUnitDetails = p_web.GetValue('tmp:ExchangeUnitDetails')
    p_web.SetSessionValue('tmp:ExchangeUnitDetails',tmp:ExchangeUnitDetails)
  Else
    tmp:ExchangeUnitDetails = p_web.GetSessionValue('tmp:ExchangeUnitDetails')
  End
  if p_web.IfExistsValue('tmp:ExchangeAccessories')
    tmp:ExchangeAccessories = p_web.GetValue('tmp:ExchangeAccessories')
    p_web.SetSessionValue('tmp:ExchangeAccessories',tmp:ExchangeAccessories)
  Else
    tmp:ExchangeAccessories = p_web.GetSessionValue('tmp:ExchangeAccessories')
  End
  if p_web.IfExistsValue('tmp:ExchangeLocation')
    tmp:ExchangeLocation = p_web.GetValue('tmp:ExchangeLocation')
    p_web.SetSessionValue('tmp:ExchangeLocation',tmp:ExchangeLocation)
  Else
    tmp:ExchangeLocation = p_web.GetSessionValue('tmp:ExchangeLocation')
  End
  if p_web.IfExistsValue('tmp:HandsetPartNumber')
    tmp:HandsetPartNumber = p_web.GetValue('tmp:HandsetPartNumber')
    p_web.SetSessionValue('tmp:HandsetPartNumber',tmp:HandsetPartNumber)
  Else
    tmp:HandsetPartNumber = p_web.GetSessionValue('tmp:HandsetPartNumber')
  End
  if p_web.IfExistsValue('tmp:ReplacementValue')
    tmp:ReplacementValue = p_web.dformat(clip(p_web.GetValue('tmp:ReplacementValue')),'@n14.2')
    p_web.SetSessionValue('tmp:ReplacementValue',tmp:ReplacementValue)
  Else
    tmp:ReplacementValue = p_web.GetSessionValue('tmp:ReplacementValue')
  End
  if p_web.IfExistsValue('tmp:HandsetReplacementValue')
    tmp:HandsetReplacementValue = p_web.dformat(clip(p_web.GetValue('tmp:HandsetReplacementValue')),'@n14.2')
    p_web.SetSessionValue('tmp:HandsetReplacementValue',tmp:HandsetReplacementValue)
  Else
    tmp:HandsetReplacementValue = p_web.GetSessionValue('tmp:HandsetReplacementValue')
  End
  if p_web.IfExistsValue('tmp:NoUnitAvailable')
    tmp:NoUnitAvailable = p_web.GetValue('tmp:NoUnitAvailable')
    p_web.SetSessionValue('tmp:NoUnitAvailable',tmp:NoUnitAvailable)
  Else
    tmp:NoUnitAvailable = p_web.GetSessionValue('tmp:NoUnitAvailable')
  End
  if p_web.IfExistsValue('job:Exchange_Courier')
    job:Exchange_Courier = p_web.GetValue('job:Exchange_Courier')
    p_web.SetSessionValue('job:Exchange_Courier',job:Exchange_Courier)
  Else
    job:Exchange_Courier = p_web.GetSessionValue('job:Exchange_Courier')
  End
  if p_web.IfExistsValue('job:Exchange_Consignment_Number')
    job:Exchange_Consignment_Number = p_web.GetValue('job:Exchange_Consignment_Number')
    p_web.SetSessionValue('job:Exchange_Consignment_Number',job:Exchange_Consignment_Number)
  Else
    job:Exchange_Consignment_Number = p_web.GetSessionValue('job:Exchange_Consignment_Number')
  End
  if p_web.IfExistsValue('job:Exchange_Despatched')
    job:Exchange_Despatched = p_web.dformat(clip(p_web.GetValue('job:Exchange_Despatched')),'@d06b')
    p_web.SetSessionValue('job:Exchange_Despatched',job:Exchange_Despatched)
  Else
    job:Exchange_Despatched = p_web.GetSessionValue('job:Exchange_Despatched')
  End
  if p_web.IfExistsValue('job:Exchange_Despatched_User')
    job:Exchange_Despatched_User = p_web.GetValue('job:Exchange_Despatched_User')
    p_web.SetSessionValue('job:Exchange_Despatched_User',job:Exchange_Despatched_User)
  Else
    job:Exchange_Despatched_User = p_web.GetSessionValue('job:Exchange_Despatched_User')
  End
  if p_web.IfExistsValue('job:Exchange_Despatch_Number')
    job:Exchange_Despatch_Number = p_web.GetValue('job:Exchange_Despatch_Number')
    p_web.SetSessionValue('job:Exchange_Despatch_Number',job:Exchange_Despatch_Number)
  Else
    job:Exchange_Despatch_Number = p_web.GetSessionValue('job:Exchange_Despatch_Number')
  End
  if p_web.IfExistsValue('locRemovalAlertMessage')
    locRemovalAlertMessage = p_web.GetValue('locRemovalAlertMessage')
    p_web.SetSessionValue('locRemovalAlertMessage',locRemovalAlertMessage)
  Else
    locRemovalAlertMessage = p_web.GetSessionValue('locRemovalAlertMessage')
  End
  if p_web.IfExistsValue('tmp:RemovalReason')
    tmp:RemovalReason = p_web.GetValue('tmp:RemovalReason')
    p_web.SetSessionValue('tmp:RemovalReason',tmp:RemovalReason)
  Else
    tmp:RemovalReason = p_web.GetSessionValue('tmp:RemovalReason')
  End

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('PickExchangeUnit_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.GSV('ReturnURL')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('PickExchangeUnit_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('PickExchangeUnit_ChainTo')
    loc:formaction = p_web.GetSessionValue('PickExchangeUnit_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = p_web.GSV('ReturnURL')

GenerateForm   Routine
  do LoadRelatedRecords
      if (p_web.GSV('job:Loan_Unit_Number') > 0 And ((p_web.GSV('BookingSite') = 'RRC' And p_web.GSV('jobe:Despatched') = 'REA' And p_web.GSV('jobe:DespatchedType') = 'LOA') Or |
          (p_web.GSV('BookingSite') = 'ARC' And p_web.GSV('job:Despatched') = 'REA' And p_web.GSV('job:Despatch_Type') = 'LOA')))
          packet = clip(packet) & '<script language="JavaScript" type="text/javascript">alert("You cannot attached an Exchange Unit until the Loan Unit has been despatched.")</script>'
          do sendPacket
          p_web.SSV('ReadOnly:ExchangeIMEINumber',1)
      End !
  
      p_web.SSV('ReadOnly:ExchangeDespatchDetails',1)
 tmp:ExchangeIMEINumber = p_web.RestoreValue('tmp:ExchangeIMEINumber')
 locExchangeAlertMessage = p_web.RestoreValue('locExchangeAlertMessage')
 tmp:MSN = p_web.RestoreValue('tmp:MSN')
 tmp:ExchangeUnitDetails = p_web.RestoreValue('tmp:ExchangeUnitDetails')
 tmp:ExchangeAccessories = p_web.RestoreValue('tmp:ExchangeAccessories')
 tmp:ExchangeLocation = p_web.RestoreValue('tmp:ExchangeLocation')
 tmp:HandsetPartNumber = p_web.RestoreValue('tmp:HandsetPartNumber')
 tmp:ReplacementValue = p_web.RestoreValue('tmp:ReplacementValue')
 tmp:HandsetReplacementValue = p_web.RestoreValue('tmp:HandsetReplacementValue')
 tmp:NoUnitAvailable = p_web.RestoreValue('tmp:NoUnitAvailable')
 locRemovalAlertMessage = p_web.RestoreValue('locRemovalAlertMessage')
 tmp:RemovalReason = p_web.RestoreValue('tmp:RemovalReason')
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Pick Exchange Unit') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Pick Exchange Unit',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_PickExchangeUnit',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_PickExchangeUnit0_div')&'">'&p_web.Translate('Job Details')&'</a></li>'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_PickExchangeUnit1_div')&'">'&p_web.Translate('Exchange Unit Details')&'</a></li>'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_PickExchangeUnit2_div')&'">'&p_web.Translate('Despatch Details')&'</a></li>'& CRLF
      If p_web.GSV('job:Exchange_Unit_Number') > 0
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_PickExchangeUnit3_div')&'">'&p_web.Translate('Remove Exchange Unit')&'</a></li>'& CRLF
      End
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  do GenerateTab3
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="PickExchangeUnit_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.cancel();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CancelButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="PickExchangeUnit_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'PickExchangeUnit_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="PickExchangeUnit_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'PickExchangeUnit_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
    If upper(p_web.getvalue('LookupFile'))='EXCHANGE'
          If Not (1=0)
            p_web.SetValue('SelectField',clip(loc:formname) & '.job:Exchange_Consignment_Number')
          End
    End
  Else
    If False
    Else
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_PickExchangeUnit')>0,p_web.GSV('showtab_PickExchangeUnit'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_PickExchangeUnit'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_PickExchangeUnit') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_PickExchangeUnit'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_PickExchangeUnit')>0,p_web.GSV('showtab_PickExchangeUnit'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_PickExchangeUnit') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Job Details') & ''''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Exchange Unit Details') & ''''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Despatch Details') & ''''
      If p_web.GSV('job:Exchange_Unit_Number') > 0
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Remove Exchange Unit') & ''''
      End
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_PickExchangeUnit_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_PickExchangeUnit')>0,p_web.GSV('showtab_PickExchangeUnit'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"PickExchangeUnit",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_PickExchangeUnit')>0,p_web.GSV('showtab_PickExchangeUnit'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_PickExchangeUnit_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('PickExchangeUnit') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('PickExchangeUnit')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Job Details')&'</a></h3>' & CRLF & p_web.DivHeader('tab_PickExchangeUnit0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_PickExchangeUnit0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_PickExchangeUnit0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_PickExchangeUnit0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Job Details')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_PickExchangeUnit0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Job Details')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_PickExchangeUnit0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Job Details')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_PickExchangeUnit0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:ESN
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:ESN
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::job:ESN
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:MSN
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:MSN
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::job:MSN
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locUnitDetails
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locUnitDetails
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locUnitDetails
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Charge_Type
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Charge_Type
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::job:Charge_Type
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Warranty_Charge_Type
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Warranty_Charge_Type
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::job:Warranty_Charge_Type
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab1  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Exchange Unit Details')&'</a></h3>' & CRLF & p_web.DivHeader('tab_PickExchangeUnit1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_PickExchangeUnit1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_PickExchangeUnit1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_PickExchangeUnit1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Exchange Unit Details')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_PickExchangeUnit1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Exchange Unit Details')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_PickExchangeUnit1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Exchange Unit Details')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_PickExchangeUnit1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:ExchangeIMEINumber
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:ExchangeIMEINumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:ExchangeIMEINumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td rowspan="'&clip(4)&'"'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locExchangeAlertMessage
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td rowspan="'&clip(4)&'"'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locExchangeAlertMessage
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td rowspan="'&clip(4)&'"'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locExchangeAlertMessage
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      If p_web.GSV('ReadOnly:ExchangeIMEINumber') <> 1
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::buttonPickExchangeUnit
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::buttonPickExchangeUnit
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
      If p_web.GSV('tmp:MSN') <> ''
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:MSN
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:MSN
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:MSN
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      end
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:ExchangeUnitDetails
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:ExchangeUnitDetails
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:ExchangeUnitDetails
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:ExchangeAccessories
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:ExchangeAccessories
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:ExchangeAccessories
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:ExchangeLocation
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:ExchangeLocation
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:ExchangeLocation
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:HandsetPartNumber
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:HandsetPartNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:HandsetPartNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:ReplacementValue
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:ReplacementValue
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:ReplacementValue
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:HandsetReplacementValue
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:HandsetReplacementValue
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:HandsetReplacementValue
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:NoUnitAvailable
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:NoUnitAvailable
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:NoUnitAvailable
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab2  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Despatch Details')&'</a></h3>' & CRLF & p_web.DivHeader('tab_PickExchangeUnit2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_PickExchangeUnit2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_PickExchangeUnit2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_PickExchangeUnit2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Despatch Details')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_PickExchangeUnit2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Despatch Details')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_PickExchangeUnit2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Despatch Details')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_PickExchangeUnit2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Exchange_Courier
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Exchange_Courier
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::job:Exchange_Courier
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Exchange_Consignment_Number
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Exchange_Consignment_Number
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::job:Exchange_Consignment_Number
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Exchange_Despatched
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Exchange_Despatched
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::job:Exchange_Despatched
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Exchange_Despatched_User
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Exchange_Despatched_User
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::job:Exchange_Despatched_User
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Exchange_Despatch_Number
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Exchange_Despatch_Number
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::job:Exchange_Despatch_Number
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::button:AmendDespatchDetails
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::button:AmendDespatchDetails
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab3  Routine
  If p_web.GSV('job:Exchange_Unit_Number') > 0
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Remove Exchange Unit')&'</a></h3>' & CRLF & p_web.DivHeader('tab_PickExchangeUnit3',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_PickExchangeUnit3',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_PickExchangeUnit3',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_PickExchangeUnit3',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Remove Exchange Unit')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_PickExchangeUnit3',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Remove Exchange Unit')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_PickExchangeUnit3',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Remove Exchange Unit')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_PickExchangeUnit3',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::locRemoveText
        do Comment::locRemoveText
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::buttonRemoveAttachedUnit
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::buttonRemoveAttachedUnit
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::buttonRemoveAttachedUnit
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td rowspan="'&clip(5)&'" valign="top"'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locRemovalAlertMessage
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td rowspan="'&clip(5)&'"'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locRemovalAlertMessage
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td rowspan="'&clip(5)&'"'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locRemovalAlertMessage
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:RemovalReason
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:RemovalReason
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:RemovalReason
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&'20%'&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::buttonConfirmExchangeRemoval
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&'30%'&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::buttonConfirmExchangeRemoval
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::buttonConfirmExchangeRemoval
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
  end


Prompt::job:ESN  Routine
  packet = clip(packet) & p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('job:ESN') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('IMEI Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:ESN  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:ESN = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s20
    job:ESN = p_web.Dformat(p_web.GetValue('Value'),'@s20')
  End
  do ValidateValue::job:ESN  ! copies value to session value if valid.
  do Comment::job:ESN ! allows comment style to be updated.

ValidateValue::job:ESN  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('job:ESN',job:ESN).
    End

Value::job:ESN  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('job:ESN') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- job:ESN
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:ESN'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::job:ESN  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if job:ESN:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('job:ESN') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::job:MSN  Routine
  packet = clip(packet) & p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('job:MSN') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('M.S.N.'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:MSN  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:MSN = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s20
    job:MSN = p_web.Dformat(p_web.GetValue('Value'),'@s20')
  End
  do ValidateValue::job:MSN  ! copies value to session value if valid.
  do Comment::job:MSN ! allows comment style to be updated.

ValidateValue::job:MSN  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('job:MSN',job:MSN).
    End

Value::job:MSN  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('job:MSN') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- job:MSN
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:MSN'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::job:MSN  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if job:MSN:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('job:MSN') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locUnitDetails  Routine
  packet = clip(packet) & p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('locUnitDetails') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Unit Details'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locUnitDetails  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::locUnitDetails  ! copies value to session value if valid.
  do Comment::locUnitDetails ! allows comment style to be updated.

ValidateValue::locUnitDetails  Routine
    If not (1=0)
    End

Value::locUnitDetails  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('locUnitDetails') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="locUnitDetails" class="'&clip('green bold')&'"'&clip(loc:extra)&'>' & p_web.Translate(p_web.GSV('job:Manufacturer') & ' ' & p_web.GSV('job:Model_Number') & ' - ' & p_web.GSV('job:Unit_Type'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locUnitDetails  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locUnitDetails:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('locUnitDetails') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::job:Charge_Type  Routine
  packet = clip(packet) & p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Charge_Type') & '_prompt',Choose(p_web.GSV('job:Chargeable_Job') <> 'YES','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('job:Chargeable_Job') <> 'YES','',p_web.Translate('Charge Type'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Charge_Type  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Charge_Type = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    job:Charge_Type = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::job:Charge_Type  ! copies value to session value if valid.
  do Comment::job:Charge_Type ! allows comment style to be updated.

ValidateValue::job:Charge_Type  Routine
    If not (p_web.GSV('job:Chargeable_Job') <> 'YES')
      if loc:invalid = '' then p_web.SetSessionValue('job:Charge_Type',job:Charge_Type).
    End

Value::job:Charge_Type  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('job:Chargeable_Job') <> 'YES','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Charge_Type') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('job:Chargeable_Job') <> 'YES')
  ! --- DISPLAY --- job:Charge_Type
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Charge_Type'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::job:Charge_Type  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if job:Charge_Type:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('job:Chargeable_Job') <> 'YES','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Charge_Type') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('job:Chargeable_Job') <> 'YES'
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::job:Warranty_Charge_Type  Routine
  packet = clip(packet) & p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Warranty_Charge_Type') & '_prompt',Choose(p_web.GSV('job:Warranty_Job') <> 'YES','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('job:Warranty_Job') <> 'YES','',p_web.Translate('Warr Charge Type'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Warranty_Charge_Type  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Warranty_Charge_Type = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    job:Warranty_Charge_Type = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::job:Warranty_Charge_Type  ! copies value to session value if valid.
  do Comment::job:Warranty_Charge_Type ! allows comment style to be updated.

ValidateValue::job:Warranty_Charge_Type  Routine
    If not (p_web.GSV('job:Warranty_Job') <> 'YES')
      if loc:invalid = '' then p_web.SetSessionValue('job:Warranty_Charge_Type',job:Warranty_Charge_Type).
    End

Value::job:Warranty_Charge_Type  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('job:Warranty_Job') <> 'YES','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Warranty_Charge_Type') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('job:Warranty_Job') <> 'YES')
  ! --- DISPLAY --- job:Warranty_Charge_Type
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Warranty_Charge_Type'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::job:Warranty_Charge_Type  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if job:Warranty_Charge_Type:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('job:Warranty_Job') <> 'YES','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Warranty_Charge_Type') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('job:Warranty_Job') <> 'YES'
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:ExchangeIMEINumber  Routine
  packet = clip(packet) & p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:ExchangeIMEINumber') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Exchange IMEI No'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:ExchangeIMEINumber  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:ExchangeIMEINumber = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:ExchangeIMEINumber = p_web.GetValue('Value')
  ElsIf p_web.RequestAjax = 1 and p_web.IfExistsValue('xch:ESN')
    tmp:ExchangeIMEINumber = p_web.GetValue('xch:ESN')
  ElsIf p_web.RequestAjax = 1
    tmp:ExchangeIMEINumber = xch:ESN
  End
  do ValidateValue::tmp:ExchangeIMEINumber  ! copies value to session value if valid.
      ! Validate IMEI
      p_web.SSV('locExchangeAlertMessage','')
      Access:EXCHANGE.Clearkey(xch:ESN_Only_Key)
      xch:ESN    = p_web.GSV('tmp:ExchangeIMEINumber')
      if (Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign)
          ! Found
          if (xch:Location <> p_web.GSV('BookingSiteLocation'))
              p_web.SSV('locExchangeAlertMessage','Selected IMEI is from a different location.')
          else !  !if (xch:Location <> p_web.GSV('BookingSiteLocation'))
              if (xch:Available = 'AVL')
                  Access:STOCKTYP.Clearkey(stp:Stock_Type_Key)
                  stp:Stock_Type    = xch:Stock_Type
                  if (Access:STOCKTYP.TryFetch(stp:Stock_Type_Key) = Level:Benign)
                      ! Found
                      if (stp:Available <> 1)
                          p_web.SSV('locExchangeAlertMessage','Selected Stock Type is not available')
                      else ! if (stp:Available <> 1)
                          p_web.SSV('tmp:ExchangeUnitNumber',xch:Ref_Number)
                      end ! if (stp:Available <> 1)
                  else ! if (Access:STOCKTYP.TryFetch(stp:Stock_Type_Key) = Level:Benign)
                      ! Error
                  end ! if (Access:STOCKTYP.TryFetch(stp:Stock_Type_Key) = Level:Benign)
              else ! if (xch:Available = 'AVL')
                  p_web.SSV('locExchangeAlertMessage','Selected IMEI is not available')
              end ! if (xch:Available = 'AVL')
          end !if (xch:Location <> p_web.GSV('BookingSiteLocation'))
      else ! if (Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign)
          ! Error
          p_web.SSV('locExchangeAlertMessage','Cannot find the selected IMEI Number')
      end ! if (Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign)
  
      if (p_web.GSV('locExchangeAlertMessage') = '')
          do getExchangeDetails
      else
          do clearExchangeDetails
      end ! if (p_web.GSV('locExchangeAlertMessage') = '')
  p_Web.SetValue('lookupfield','tmp:ExchangeIMEINumber')
  do AfterLookup
  do Value::tmp:ExchangeIMEINumber
  do SendAlert
  do Comment::tmp:ExchangeIMEINumber
  do Value::tmp:ExchangeLocation  !1
  do Value::tmp:ExchangeUnitDetails  !1
  do Value::tmp:ReplacementValue  !1
  do Prompt::tmp:HandsetPartNumber
  do Value::tmp:HandsetPartNumber  !1
  do Prompt::tmp:HandsetReplacementValue
  do Value::tmp:HandsetReplacementValue  !1
  do Prompt::tmp:ReplacementValue
  do Value::tmp:ReplacementValue  !1
  do Prompt::tmp:NoUnitAvailable
  do Value::tmp:NoUnitAvailable  !1
  do Prompt::locExchangeAlertMessage
  do Value::locExchangeAlertMessage  !1

ValidateValue::tmp:ExchangeIMEINumber  Routine
    If not (1=0)
    tmp:ExchangeIMEINumber = Upper(tmp:ExchangeIMEINumber)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:ExchangeIMEINumber',tmp:ExchangeIMEINumber).
    End

Value::tmp:ExchangeIMEINumber  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:ExchangeIMEINumber') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.GSV('ReadOnly:ExchangeIMEINumber') = 1 OR p_web.GSV('job:Exchange_Unit_Number') > 0 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    tmp:ExchangeIMEINumber = p_web.RestoreValue('tmp:ExchangeIMEINumber')
    do ValidateValue::tmp:ExchangeIMEINumber
    If tmp:ExchangeIMEINumber:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- tmp:ExchangeIMEINumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:ExchangeIMEINumber') = 1 OR p_web.GSV('job:Exchange_Unit_Number') > 0,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ExchangeIMEINumber'',''pickexchangeunit_tmp:exchangeimeinumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:ExchangeIMEINumber')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','tmp:ExchangeIMEINumber',p_web.GetSessionValue('tmp:ExchangeIMEINumber'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'!4
    if not loc:viewonly and not loc:readonly
      loc:fieldclass = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('FormExchangeUnitFilter')&'?LookupField=tmp:ExchangeIMEINumber&Tab=3&ForeignField=xch:ESN&_sort=xch:ESN&Refresh=sort'),,,,,loc:fieldclass)
    End
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:ExchangeIMEINumber  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:ExchangeIMEINumber:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:ExchangeIMEINumber') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locExchangeAlertMessage  Routine
  packet = clip(packet) & p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('locExchangeAlertMessage') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locExchangeAlertMessage  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locExchangeAlertMessage = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locExchangeAlertMessage = p_web.GetValue('Value')
  End
  do ValidateValue::locExchangeAlertMessage  ! copies value to session value if valid.
  do Comment::locExchangeAlertMessage ! allows comment style to be updated.

ValidateValue::locExchangeAlertMessage  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locExchangeAlertMessage',locExchangeAlertMessage).
    End

Value::locExchangeAlertMessage  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('locExchangeAlertMessage') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,'RedBoldSmall')
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- locExchangeAlertMessage
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="locExchangeAlertMessage" class="'&clip('red bold')&'"'&clip(loc:extra)&'>' & p_web.Translate(p_web.GSV('locExchangeAlertMessage'),1) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locExchangeAlertMessage  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locExchangeAlertMessage:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('locExchangeAlertMessage') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::buttonPickExchangeUnit  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonPickExchangeUnit  ! copies value to session value if valid.
  do Value::buttonPickExchangeUnit
  do Comment::buttonPickExchangeUnit ! allows comment style to be updated.
  do Value::tmp:HandsetPartNumber  !1

ValidateValue::buttonPickExchangeUnit  Routine
  If p_web.GSV('ReadOnly:ExchangeIMEINumber') <> 1
    If not (1 or p_web.GSV('job:Exchange_Unit_Number') > 0)
    End
  End

Value::buttonPickExchangeUnit  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(1 or p_web.GSV('job:Exchange_Unit_Number') > 0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('buttonPickExchangeUnit') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1 or p_web.GSV('job:Exchange_Unit_Number') > 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonPickExchangeUnit'',''pickexchangeunit_buttonpickexchangeunit_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','PickExchangeUnit','Pick Exchange Unit',p_web.combine(Choose('Pick Exchange Unit' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'SmallButtonFixedIcon'),loc:formname,,,p_web.WindowOpen(clip('FormExchangeUnitFilter?LookupField=tmp:ExchangeIMEINumber&Tab=2&ForeignField=xch:ESN&_sort=xch:ESN&Refresh=sort&LookupFrom=PickExchangeUnit&')&''&'','_self'),,loc:disabled,'images\packinsert.png',,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonPickExchangeUnit  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonPickExchangeUnit:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1 or p_web.GSV('job:Exchange_Unit_Number') > 0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('buttonPickExchangeUnit') & '_comment',loc:class,Net:NoSend)
  If 1 or p_web.GSV('job:Exchange_Unit_Number') > 0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:MSN  Routine
  packet = clip(packet) & p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:MSN') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('M.S.N.'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:MSN  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:MSN = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:MSN = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:MSN  ! copies value to session value if valid.
  do Comment::tmp:MSN ! allows comment style to be updated.

ValidateValue::tmp:MSN  Routine
  If p_web.GSV('tmp:MSN') <> ''
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:MSN',tmp:MSN).
    End
  End

Value::tmp:MSN  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:MSN') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- tmp:MSN
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('tmp:MSN'),) & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:MSN  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:MSN:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:MSN') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:ExchangeUnitDetails  Routine
  packet = clip(packet) & p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:ExchangeUnitDetails') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Unit Details'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:ExchangeUnitDetails  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:ExchangeUnitDetails = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:ExchangeUnitDetails = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:ExchangeUnitDetails  ! copies value to session value if valid.
  do Comment::tmp:ExchangeUnitDetails ! allows comment style to be updated.

ValidateValue::tmp:ExchangeUnitDetails  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:ExchangeUnitDetails',tmp:ExchangeUnitDetails).
    End

Value::tmp:ExchangeUnitDetails  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:ExchangeUnitDetails') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- tmp:ExchangeUnitDetails
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('tmp:ExchangeUnitDetails'),) & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:ExchangeUnitDetails  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:ExchangeUnitDetails:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:ExchangeUnitDetails') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:ExchangeAccessories  Routine
  packet = clip(packet) & p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:ExchangeAccessories') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Accessories'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:ExchangeAccessories  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:ExchangeAccessories = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:ExchangeAccessories = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:ExchangeAccessories  ! copies value to session value if valid.
  do Comment::tmp:ExchangeAccessories ! allows comment style to be updated.

ValidateValue::tmp:ExchangeAccessories  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:ExchangeAccessories',tmp:ExchangeAccessories).
    End

Value::tmp:ExchangeAccessories  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:ExchangeAccessories') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- tmp:ExchangeAccessories
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('tmp:ExchangeAccessories'),) & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:ExchangeAccessories  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:ExchangeAccessories:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:ExchangeAccessories') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:ExchangeLocation  Routine
  packet = clip(packet) & p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:ExchangeLocation') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Location / Stock Type'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:ExchangeLocation  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:ExchangeLocation = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:ExchangeLocation = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:ExchangeLocation  ! copies value to session value if valid.
  do Comment::tmp:ExchangeLocation ! allows comment style to be updated.

ValidateValue::tmp:ExchangeLocation  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:ExchangeLocation',tmp:ExchangeLocation).
    End

Value::tmp:ExchangeLocation  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:ExchangeLocation') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- tmp:ExchangeLocation
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('tmp:ExchangeLocation'),) & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:ExchangeLocation  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:ExchangeLocation:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:ExchangeLocation') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:HandsetPartNumber  Routine
  packet = clip(packet) & p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:HandsetPartNumber') & '_prompt',Choose(p_web.GSV('BookingSite') <> 'ARC' Or p_web.GSV('Hide:HandsetPartNumber') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('BookingSite') <> 'ARC' Or p_web.GSV('Hide:HandsetPartNumber') = 1,'',p_web.Translate('Handset Part No'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:HandsetPartNumber  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:HandsetPartNumber = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:HandsetPartNumber = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:HandsetPartNumber  ! copies value to session value if valid.
  Access:PRODCODE.Clearkey(prd:ModelProductKey)
  prd:ModelNumber    = p_web.GSV('tmp:ExchangeModelNumber')
  prd:ProductCode    = p_web.GSV('tmp:HandsetPartNumber')
  if (Access:PRODCODE.TryFetch(prd:ModelProductKey) = Level:Benign)
      ! Found
      p_web.SSV('tmp:HandsetReplacementValue',prd:HandsetReplacementValue)
  else ! if (Access:PRODCODE.TryFetch(prd:ModelProductKey) = Level:Benign)
      ! Error
      p_web.SSV('tmp:HandsetReplacementValue','')
  end ! if (Access:PRODCODE.TryFetch(prd:ModelProductKey) = Level:Benign)
  do Value::tmp:HandsetPartNumber
  do SendAlert
  do Comment::tmp:HandsetPartNumber ! allows comment style to be updated.
  do Value::tmp:HandsetReplacementValue  !1

ValidateValue::tmp:HandsetPartNumber  Routine
    If not (p_web.GSV('BookingSite') <> 'ARC' Or p_web.GSV('Hide:HandsetPartNumber') = 1)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:HandsetPartNumber',tmp:HandsetPartNumber).
    End

Value::tmp:HandsetPartNumber  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('BookingSite') <> 'ARC' Or p_web.GSV('Hide:HandsetPartNumber') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:HandsetPartNumber') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,p_web.site.style.formselect,,)
  If loc:retrying
    tmp:HandsetPartNumber = p_web.RestoreValue('tmp:HandsetPartNumber')
    do ValidateValue::tmp:HandsetPartNumber
    If tmp:HandsetPartNumber:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('BookingSite') <> 'ARC' Or p_web.GSV('Hide:HandsetPartNumber') = 1)
  ! --- DROPLIST ---
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:HandsetPartNumber'',''pickexchangeunit_tmp:handsetpartnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:HandsetPartNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('ReadOnly:ExchangeIMEINumber') = 1,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:HandsetPartNumber',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
  loc:fieldclass = ''
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('tmp:HandsetPartNumber') = 0
    p_web.SetSessionValue('tmp:HandsetPartNumber','')
  end
    packet = clip(packet) & p_web.CreateOption('-------------------------------------','',choose('' = p_web.getsessionvalue('tmp:HandsetPartNumber')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(STOCKALL)
  bind(stl:Record)
  p_web._OpenFile(SMSRECVD)
  bind(SMR:Record)
  p_web._OpenFile(COURIER)
  bind(cou:Record)
  p_web._OpenFile(JOBS)
  bind(job:Record)
  p_web._OpenFile(EXCHANGE)
  bind(xch:Record)
  p_web._OpenFile(JOBEXACC)
  bind(jea:Record)
  p_web._OpenFile(EXCHANGE_ALIAS)
  bind(xch_ali:Record)
  p_web._OpenFile(EXCHOR48)
  bind(ex4:Record)
  p_web._OpenFile(MANUFACT)
  bind(man:Record)
  p_web._OpenFile(PARTS)
  bind(par:Record)
  p_web._OpenFile(STOCK)
  bind(sto:Record)
  p_web._OpenFile(WARPARTS)
  bind(wpr:Record)
  p_web._OpenFile(STOCKTYP)
  bind(stp:Record)
  p_web._OpenFile(EXCHHIST)
  bind(exh:Record)
  p_web._OpenFile(PRODCODE)
  bind(prd:Record)
  p_web._OpenFile(JOBSE)
  bind(jobe:Record)
  p_web._OpenFile(WEBJOB)
  bind(wob:Record)
  p_web._OpenFile(TRDBATCH)
  bind(trb:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(tmp:HandsetPartNumber_OptionView)
  tmp:HandsetPartNumber_OptionView{prop:filter} = p_web.CleanFilter(tmp:HandsetPartNumber_OptionView,'Upper(prd:ModelNumber) = Upper('''  & p_web.GSV('tmp:ExchangeModelNumber') & ''')')
  tmp:HandsetPartNumber_OptionView{prop:order} = p_web.CleanFilter(tmp:HandsetPartNumber_OptionView,'UPPER(prd:ProductCode)')
  Set(tmp:HandsetPartNumber_OptionView)
  Loop
    Next(tmp:HandsetPartNumber_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('tmp:HandsetPartNumber') = 0
      p_web.SetSessionValue('tmp:HandsetPartNumber',prd:ProductCode)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(clip(prd:ProductCode) & '  (' & format(prd:HandsetReplacementValue,@n_14.2) & '  )',prd:ProductCode,choose(prd:ProductCode = p_web.getsessionvalue('tmp:HandsetPartNumber')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(tmp:HandsetPartNumber_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(STOCKALL)
  p_Web._CloseFile(SMSRECVD)
  p_Web._CloseFile(COURIER)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(EXCHANGE)
  p_Web._CloseFile(JOBEXACC)
  p_Web._CloseFile(EXCHANGE_ALIAS)
  p_Web._CloseFile(EXCHOR48)
  p_Web._CloseFile(MANUFACT)
  p_Web._CloseFile(PARTS)
  p_Web._CloseFile(STOCK)
  p_Web._CloseFile(WARPARTS)
  p_Web._CloseFile(STOCKTYP)
  p_Web._CloseFile(EXCHHIST)
  p_Web._CloseFile(PRODCODE)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(TRDBATCH)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:HandsetPartNumber  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:HandsetPartNumber:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('BookingSite') <> 'ARC' Or p_web.GSV('Hide:HandsetPartNumber') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:HandsetPartNumber') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('BookingSite') <> 'ARC' Or p_web.GSV('Hide:HandsetPartNumber') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:ReplacementValue  Routine
  packet = clip(packet) & p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:ReplacementValue') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Replacement Value'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:ReplacementValue  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:ReplacementValue = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    tmp:ReplacementValue = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::tmp:ReplacementValue  ! copies value to session value if valid.
  do Comment::tmp:ReplacementValue ! allows comment style to be updated.

ValidateValue::tmp:ReplacementValue  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:ReplacementValue',tmp:ReplacementValue).
    End

Value::tmp:ReplacementValue  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:ReplacementValue') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- tmp:ReplacementValue
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    p_web._jsok(format(p_web.GetSessionValue('tmp:ReplacementValue'),'@n14.2')) & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:ReplacementValue  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:ReplacementValue:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:ReplacementValue') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:HandsetReplacementValue  Routine
  packet = clip(packet) & p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:HandsetReplacementValue') & '_prompt',Choose(p_web.GSV('BookingSite') <> 'ARC' Or p_web.GSV('Hide:HandsetPartNumber') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('BookingSite') <> 'ARC' Or p_web.GSV('Hide:HandsetPartNumber') = 1,'',p_web.Translate('Replacement Value'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:HandsetReplacementValue  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:HandsetReplacementValue = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = 
    tmp:HandsetReplacementValue = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::tmp:HandsetReplacementValue  ! copies value to session value if valid.
  do Value::tmp:HandsetReplacementValue
  do SendAlert
  do Comment::tmp:HandsetReplacementValue ! allows comment style to be updated.

ValidateValue::tmp:HandsetReplacementValue  Routine
    If not (p_web.GSV('BookingSite') <> 'ARC' Or p_web.GSV('Hide:HandsetPartNumber') = 1)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:HandsetReplacementValue',tmp:HandsetReplacementValue).
    End

Value::tmp:HandsetReplacementValue  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('BookingSite') <> 'ARC' Or p_web.GSV('Hide:HandsetPartNumber') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:HandsetReplacementValue') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  If loc:retrying
    tmp:HandsetReplacementValue = p_web.RestoreValue('tmp:HandsetReplacementValue')
    do ValidateValue::tmp:HandsetReplacementValue
    If tmp:HandsetReplacementValue:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('BookingSite') <> 'ARC' Or p_web.GSV('Hide:HandsetPartNumber') = 1)
  ! --- STRING --- tmp:HandsetReplacementValue
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:HandsetReplacementValue'',''pickexchangeunit_tmp:handsetreplacementvalue_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:HandsetReplacementValue',p_web.GetSessionValue('tmp:HandsetReplacementValue'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:HandsetReplacementValue  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:HandsetReplacementValue:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('BookingSite') <> 'ARC' Or p_web.GSV('Hide:HandsetPartNumber') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:HandsetReplacementValue') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('BookingSite') <> 'ARC' Or p_web.GSV('Hide:HandsetPartNumber') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:NoUnitAvailable  Routine
  packet = clip(packet) & p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:NoUnitAvailable') & '_prompt',Choose(p_web.GSV('tmp:ExchangeUnitNumber') > 0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('tmp:ExchangeUnitNumber') > 0,'',p_web.Translate('No Unit Available'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:NoUnitAvailable  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:NoUnitAvailable = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    tmp:NoUnitAvailable = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:NoUnitAvailable  ! copies value to session value if valid.
  do Comment::tmp:NoUnitAvailable ! allows comment style to be updated.

ValidateValue::tmp:NoUnitAvailable  Routine
    If not (p_web.GSV('tmp:ExchangeUnitNumber') > 0)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:NoUnitAvailable',tmp:NoUnitAvailable).
    End

Value::tmp:NoUnitAvailable  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('tmp:ExchangeUnitNumber') > 0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:NoUnitAvailable') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If loc:retrying
    tmp:NoUnitAvailable = p_web.RestoreValue('tmp:NoUnitAvailable')
    do ValidateValue::tmp:NoUnitAvailable
    If tmp:NoUnitAvailable:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('tmp:ExchangeUnitNumber') > 0)
  ! --- CHECKBOX --- tmp:NoUnitAvailable
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('ReadOnly:ExchangeIMEINumber') = 1 OR p_web.GSV('job:Exchange_Unit_Number') > 0,'disabled','')
  If p_web.GetSessionValue('tmp:NoUnitAvailable') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','tmp:NoUnitAvailable',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:NoUnitAvailable  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:NoUnitAvailable:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('tmp:ExchangeUnitNumber') > 0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:NoUnitAvailable') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('tmp:ExchangeUnitNumber') > 0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::job:Exchange_Courier  Routine
  packet = clip(packet) & p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Exchange_Courier') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Courier'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Exchange_Courier  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Exchange_Courier = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    job:Exchange_Courier = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::job:Exchange_Courier  ! copies value to session value if valid.
  do Value::job:Exchange_Courier
  do SendAlert
  do Comment::job:Exchange_Courier ! allows comment style to be updated.

ValidateValue::job:Exchange_Courier  Routine
    If not (1=0)
    job:Exchange_Courier = Upper(job:Exchange_Courier)
      if loc:invalid = '' then p_web.SetSessionValue('job:Exchange_Courier',job:Exchange_Courier).
    End

Value::job:Exchange_Courier  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Exchange_Courier') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,p_web.site.style.formselect,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:retrying
    job:Exchange_Courier = p_web.RestoreValue('job:Exchange_Courier')
    do ValidateValue::job:Exchange_Courier
    If job:Exchange_Courier:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- DROPLIST ---
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Exchange_Courier'',''pickexchangeunit_job:exchange_courier_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('job:Exchange_Courier',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
  loc:fieldclass = ''
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('job:Exchange_Courier') = 0
    p_web.SetSessionValue('job:Exchange_Courier','')
  end
    packet = clip(packet) & p_web.CreateOption('-------------------------------------','',choose('' = p_web.getsessionvalue('job:Exchange_Courier')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(STOCKALL)
  bind(stl:Record)
  p_web._OpenFile(SMSRECVD)
  bind(SMR:Record)
  p_web._OpenFile(COURIER)
  bind(cou:Record)
  p_web._OpenFile(JOBS)
  bind(job:Record)
  p_web._OpenFile(EXCHANGE)
  bind(xch:Record)
  p_web._OpenFile(JOBEXACC)
  bind(jea:Record)
  p_web._OpenFile(EXCHANGE_ALIAS)
  bind(xch_ali:Record)
  p_web._OpenFile(EXCHOR48)
  bind(ex4:Record)
  p_web._OpenFile(MANUFACT)
  bind(man:Record)
  p_web._OpenFile(PARTS)
  bind(par:Record)
  p_web._OpenFile(STOCK)
  bind(sto:Record)
  p_web._OpenFile(WARPARTS)
  bind(wpr:Record)
  p_web._OpenFile(STOCKTYP)
  bind(stp:Record)
  p_web._OpenFile(EXCHHIST)
  bind(exh:Record)
  p_web._OpenFile(PRODCODE)
  bind(prd:Record)
  p_web._OpenFile(JOBSE)
  bind(jobe:Record)
  p_web._OpenFile(WEBJOB)
  bind(wob:Record)
  p_web._OpenFile(TRDBATCH)
  bind(trb:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(job:Exchange_Courier_OptionView)
  job:Exchange_Courier_OptionView{prop:order} = p_web.CleanFilter(job:Exchange_Courier_OptionView,'UPPER(cou:Courier)')
  Set(job:Exchange_Courier_OptionView)
  Loop
    Next(job:Exchange_Courier_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('job:Exchange_Courier') = 0
      p_web.SetSessionValue('job:Exchange_Courier',cou:Courier)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,cou:Courier,choose(cou:Courier = p_web.getsessionvalue('job:Exchange_Courier')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(job:Exchange_Courier_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(STOCKALL)
  p_Web._CloseFile(SMSRECVD)
  p_Web._CloseFile(COURIER)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(EXCHANGE)
  p_Web._CloseFile(JOBEXACC)
  p_Web._CloseFile(EXCHANGE_ALIAS)
  p_Web._CloseFile(EXCHOR48)
  p_Web._CloseFile(MANUFACT)
  p_Web._CloseFile(PARTS)
  p_Web._CloseFile(STOCK)
  p_Web._CloseFile(WARPARTS)
  p_Web._CloseFile(STOCKTYP)
  p_Web._CloseFile(EXCHHIST)
  p_Web._CloseFile(PRODCODE)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(TRDBATCH)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web.DivFooter()
Comment::job:Exchange_Courier  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if job:Exchange_Courier:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Exchange_Courier') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::job:Exchange_Consignment_Number  Routine
  packet = clip(packet) & p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Exchange_Consignment_Number') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Consignment Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Exchange_Consignment_Number  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Exchange_Consignment_Number = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    job:Exchange_Consignment_Number = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::job:Exchange_Consignment_Number  ! copies value to session value if valid.
  do Comment::job:Exchange_Consignment_Number ! allows comment style to be updated.

ValidateValue::job:Exchange_Consignment_Number  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('job:Exchange_Consignment_Number',job:Exchange_Consignment_Number).
    End

Value::job:Exchange_Consignment_Number  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Exchange_Consignment_Number') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If p_web.GSV('ReadOnly:ExchangeDespatchDetails') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    job:Exchange_Consignment_Number = p_web.RestoreValue('job:Exchange_Consignment_Number')
    do ValidateValue::job:Exchange_Consignment_Number
    If job:Exchange_Consignment_Number:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- job:Exchange_Consignment_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:ExchangeDespatchDetails') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Exchange_Consignment_Number',p_web.GetSessionValueFormat('job:Exchange_Consignment_Number'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::job:Exchange_Consignment_Number  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if job:Exchange_Consignment_Number:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Exchange_Consignment_Number') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::job:Exchange_Despatched  Routine
  packet = clip(packet) & p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Exchange_Despatched') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Date'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Exchange_Despatched  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Exchange_Despatched = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@d06b'  !FieldPicture = @d6b
    job:Exchange_Despatched = p_web.Dformat(p_web.GetValue('Value'),'@d06b')
  End
  do ValidateValue::job:Exchange_Despatched  ! copies value to session value if valid.
  do Comment::job:Exchange_Despatched ! allows comment style to be updated.

ValidateValue::job:Exchange_Despatched  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('job:Exchange_Despatched',job:Exchange_Despatched).
    End

Value::job:Exchange_Despatched  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Exchange_Despatched') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If p_web.GSV('ReadOnly:ExchangeDespatchDetails') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    job:Exchange_Despatched = p_web.RestoreValue('job:Exchange_Despatched')
    do ValidateValue::job:Exchange_Despatched
    If job:Exchange_Despatched:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- job:Exchange_Despatched
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:ExchangeDespatchDetails') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Exchange_Despatched',p_web.GetSessionValue('job:Exchange_Despatched'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@d06b',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::job:Exchange_Despatched  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if job:Exchange_Despatched:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Exchange_Despatched') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::job:Exchange_Despatched_User  Routine
  packet = clip(packet) & p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Exchange_Despatched_User') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('User'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Exchange_Despatched_User  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Exchange_Despatched_User = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s3
    job:Exchange_Despatched_User = p_web.Dformat(p_web.GetValue('Value'),'@s3')
  End
  do ValidateValue::job:Exchange_Despatched_User  ! copies value to session value if valid.
  do Comment::job:Exchange_Despatched_User ! allows comment style to be updated.

ValidateValue::job:Exchange_Despatched_User  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('job:Exchange_Despatched_User',job:Exchange_Despatched_User).
    End

Value::job:Exchange_Despatched_User  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Exchange_Despatched_User') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If p_web.GSV('ReadOnly:ExchangeDespatchDetails') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    job:Exchange_Despatched_User = p_web.RestoreValue('job:Exchange_Despatched_User')
    do ValidateValue::job:Exchange_Despatched_User
    If job:Exchange_Despatched_User:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- job:Exchange_Despatched_User
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:ExchangeDespatchDetails') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Exchange_Despatched_User',p_web.GetSessionValueFormat('job:Exchange_Despatched_User'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s3'),,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::job:Exchange_Despatched_User  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if job:Exchange_Despatched_User:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Exchange_Despatched_User') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::job:Exchange_Despatch_Number  Routine
  packet = clip(packet) & p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Exchange_Despatch_Number') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Despatch Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Exchange_Despatch_Number  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Exchange_Despatch_Number = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s8
    job:Exchange_Despatch_Number = p_web.Dformat(p_web.GetValue('Value'),'@s8')
  End
  do ValidateValue::job:Exchange_Despatch_Number  ! copies value to session value if valid.
  do Comment::job:Exchange_Despatch_Number ! allows comment style to be updated.

ValidateValue::job:Exchange_Despatch_Number  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('job:Exchange_Despatch_Number',job:Exchange_Despatch_Number).
    End

Value::job:Exchange_Despatch_Number  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Exchange_Despatch_Number') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If p_web.GSV('ReadOnly:ExchangeDespatchDetails') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    job:Exchange_Despatch_Number = p_web.RestoreValue('job:Exchange_Despatch_Number')
    do ValidateValue::job:Exchange_Despatch_Number
    If job:Exchange_Despatch_Number:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- job:Exchange_Despatch_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:ExchangeDespatchDetails') = 1,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Exchange_Despatch_Number',p_web.GetSessionValueFormat('job:Exchange_Despatch_Number'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s8'),,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::job:Exchange_Despatch_Number  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if job:Exchange_Despatch_Number:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('job:Exchange_Despatch_Number') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::button:AmendDespatchDetails  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::button:AmendDespatchDetails  ! copies value to session value if valid.
      if (p_web.GSV('ReadOnly:ExchangeDespatchDetails') = 1)
          p_web.SSV('ReadOnly:ExchangeDespatchDetails',0)
      ELSE
          p_web.SSV('ReadOnly:ExchangeDespatchDetails',1)
      END
  do Value::button:AmendDespatchDetails
  do Comment::button:AmendDespatchDetails ! allows comment style to be updated.
  do Value::job:Exchange_Consignment_Number  !1
  do Value::job:Exchange_Despatch_Number  !1
  do Value::job:Exchange_Despatched  !1
  do Value::job:Exchange_Despatched_User  !1

ValidateValue::button:AmendDespatchDetails  Routine
    If not (p_web.GSV('job:Exchange_Consignment_Number') = '')
    End

Value::button:AmendDespatchDetails  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('job:Exchange_Consignment_Number') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('button:AmendDespatchDetails') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('job:Exchange_Consignment_Number') = '')
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''button:AmendDespatchDetails'',''pickexchangeunit_button:amenddespatchdetails_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','AmendDespatchDetails','Amend Despatch Details',p_web.combine(Choose('Amend Despatch Details' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'button-entryfield'),loc:formname,,,,loc:javascript,loc:disabled,,,,,,,,,,'nt-left')!h
  do SendPacket
  End
  p_web.DivFooter()
Comment::button:AmendDespatchDetails  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if button:AmendDespatchDetails:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('job:Exchange_Consignment_Number') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('button:AmendDespatchDetails') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('job:Exchange_Consignment_Number') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::locRemoveText  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::locRemoveText  ! copies value to session value if valid.
  do Comment::locRemoveText ! allows comment style to be updated.

ValidateValue::locRemoveText  Routine
    If not (p_web.GSV('job:exchange_unit_Number') = 0)
    End

Value::locRemoveText  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('job:exchange_unit_Number') = 0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('locRemoveText') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('job:exchange_unit_Number') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="locRemoveText" class="'&clip('red bold')&'"'&clip(loc:extra)&'>' & p_web.Translate('You must remove the existing exchange before you can add a new one',) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locRemoveText  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locRemoveText:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('job:exchange_unit_Number') = 0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('locRemoveText') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('job:exchange_unit_Number') = 0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::buttonRemoveAttachedUnit  Routine
  packet = clip(packet) & p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('buttonRemoveAttachedUnit') & '_prompt',Choose(p_web.GSV('Hide:RemovalReason') = 0 AND p_web.GSV('job:Exchange_Unit_Number') > 0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:RemovalReason') = 0 AND p_web.GSV('job:Exchange_Unit_Number') > 0,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::buttonRemoveAttachedUnit  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonRemoveAttachedUnit  ! copies value to session value if valid.
  p_web.SSV('Hide:RemovalReason',0)
  
  if (p_web.GSV('job:exchange_Consignment_Number') <> '')
      p_web.SSV('locRemoveAlertMessage','Warning!<br>The Exchange Unit has already been despatched. '&|
                          'If the continue the unit will be removed and returned to Exchange Stock.')
  end ! if (p_web.GSV('job:exchange_Consignment_Number') <> '')
  do Comment::buttonRemoveAttachedUnit ! allows comment style to be updated.
  do Prompt::tmp:RemovalReason
  do Value::tmp:RemovalReason  !1
  do Value::buttonConfirmExchangeRemoval  !1
  do Value::buttonRemoveAttachedUnit  !1
  do Prompt::locRemovalAlertMessage
  do Value::locRemovalAlertMessage  !1

ValidateValue::buttonRemoveAttachedUnit  Routine
    If not (p_web.GSV('Hide:RemovalReason') = 0 AND p_web.GSV('job:Exchange_Unit_Number') > 0)
    End

Value::buttonRemoveAttachedUnit  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:RemovalReason') = 0 AND p_web.GSV('job:Exchange_Unit_Number') > 0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('buttonRemoveAttachedUnit') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('Hide:RemovalReason') = 0 AND p_web.GSV('job:Exchange_Unit_Number') > 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonRemoveAttachedUnit'',''pickexchangeunit_buttonremoveattachedunit_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','RemoveExchangeUnit','Remove Exchange Unit',p_web.combine(Choose('Remove Exchange Unit' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'button-entryfield'),loc:formname,,,,loc:javascript,loc:disabled,,,,,,,,,,'nt-left')!i
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonRemoveAttachedUnit  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonRemoveAttachedUnit:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:RemovalReason') = 0 AND p_web.GSV('job:Exchange_Unit_Number') > 0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('buttonRemoveAttachedUnit') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:RemovalReason') = 0 AND p_web.GSV('job:Exchange_Unit_Number') > 0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locRemovalAlertMessage  Routine
  packet = clip(packet) & p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('locRemovalAlertMessage') & '_prompt',Choose(p_web.GSV('locRemovalAlertMessage') = '','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('locRemovalAlertMessage') = '','',p_web.Translate('Alert'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locRemovalAlertMessage  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locRemovalAlertMessage = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locRemovalAlertMessage = p_web.GetValue('Value')
  End
  do ValidateValue::locRemovalAlertMessage  ! copies value to session value if valid.
  do Value::locRemovalAlertMessage
  do SendAlert
  do Comment::locRemovalAlertMessage ! allows comment style to be updated.

ValidateValue::locRemovalAlertMessage  Routine
    If not (p_web.GSV('locRemovalAlertMessage') = '')
      if loc:invalid = '' then p_web.SetSessionValue('locRemovalAlertMessage',locRemovalAlertMessage).
    End

Value::locRemovalAlertMessage  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('locRemovalAlertMessage') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('locRemovalAlertMessage') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  If loc:retrying
    locRemovalAlertMessage = p_web.RestoreValue('locRemovalAlertMessage')
    do ValidateValue::locRemovalAlertMessage
    If locRemovalAlertMessage:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('locRemovalAlertMessage') = '')
  ! --- TEXT --- locRemovalAlertMessage
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locRemovalAlertMessage'',''pickexchangeunit_locremovalalertmessage_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'readonly'
  do SendPacket
  p_web.CreateTextArea('locRemovalAlertMessage',p_web.GetSessionValue('locRemovalAlertMessage'),5,25,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(locRemovalAlertMessage),,1,,Net:Send)
  do SendPacket
  End
  p_web.DivFooter()
Comment::locRemovalAlertMessage  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locRemovalAlertMessage:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('locRemovalAlertMessage') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('locRemovalAlertMessage') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('locRemovalAlertMessage') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:RemovalReason  Routine
  packet = clip(packet) & p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:RemovalReason') & '_prompt',Choose(p_web.GSV('Hide:RemovalReason') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:RemovalReason') = 1,'',p_web.Translate('Removal Reason'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:RemovalReason  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:RemovalReason = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:RemovalReason = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:RemovalReason  ! copies value to session value if valid.
  do Value::tmp:RemovalReason
  do SendAlert
  do Comment::tmp:RemovalReason ! allows comment style to be updated.
  do Value::buttonConfirmExchangeRemoval  !1

ValidateValue::tmp:RemovalReason  Routine
    If not (p_web.GSV('Hide:RemovalReason') = 1)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:RemovalReason',tmp:RemovalReason).
    End

Value::tmp:RemovalReason  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:RemovalReason') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:RemovalReason') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,' noButton')
  If loc:retrying
    tmp:RemovalReason = p_web.RestoreValue('tmp:RemovalReason')
    do ValidateValue::tmp:RemovalReason
    If tmp:RemovalReason:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:RemovalReason') = 1)
  ! --- RADIO --- tmp:RemovalReason
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If loc:readonly = '' and (p_web.GSV('jobe:Engineer48HourOption') = 1) then loc:readonly = 'disabled'.
    if p_web.GetSessionValue('tmp:RemovalReason') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:RemovalReason'',''pickexchangeunit_tmp:removalreason_value'',1,'''&p_web._jsok(1)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:RemovalReason')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:RemovalReason',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:RemovalReason_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Replace Faulty Unit') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If loc:readonly = '' and (p_web.GSV('jobe:Engineer48HourOption') = 1) then loc:readonly = 'disabled'.
    if p_web.GetSessionValue('tmp:RemovalReason') = 2
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:RemovalReason'',''pickexchangeunit_tmp:removalreason_value'',1,'''&p_web._jsok(2)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:RemovalReason')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:RemovalReason',clip(2),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:RemovalReason_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Alternative Model Required') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:RemovalReason') = 3
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:RemovalReason'',''pickexchangeunit_tmp:removalreason_value'',1,'''&p_web._jsok(3)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:RemovalReason')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:RemovalReason',clip(3),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:RemovalReason_3') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Restock Unit') & '<13,10>'
    packet = clip(packet) & p_web.br
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:RemovalReason  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:RemovalReason:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:RemovalReason') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('tmp:RemovalReason') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:RemovalReason') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::buttonConfirmExchangeRemoval  Routine
  packet = clip(packet) & p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('buttonConfirmExchangeRemoval') & '_prompt',Choose(p_web.GSV('Hide:RemovalReason') = 1 Or p_web.GSV('tmp:RemovalReason') = 0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:RemovalReason') = 1 Or p_web.GSV('tmp:RemovalReason') = 0,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::buttonConfirmExchangeRemoval  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonConfirmExchangeRemoval  ! copies value to session value if valid.
      ! Remove Exchange Unit
      p_web.SSV('AddToAudit:Type','EXC')
      if p_web.GSV('job:Exchange_Consignemnt_Number') > 0
          p_web.SSV('AddToAudit:Notes','UNIT HAD BEEN DESPATCHED: ' & |
              '<13,10>UNIT NUMBER: ' & p_web.GSV('job:Exchange_unit_number') & |
              '<13,10>COURIER: ' & p_web.GSV('job:exchange_Courier') & |
              '<13,10>CONSIGNMENT NUMBER: ' & p_web.GSV('job:exchange_consignment_number') & |
              '<13,10>DATE DESPATCHED: ' & Format(p_web.GSV('job:Exchange_despatched'),@d6) &|
              '<13,10>DESPATCH USER: ' & p_web.GSV('job:Exchange_despatched_user') &|
              '<13,10>DESPATCH NUMBER: ' & p_web.GSV('job:exchange_despatch_number'))
      else ! if p_web.GSV('job:Exchange_Consignemnt_Number') > 0
          p_web.SSV('AddToAudit:Notes','UNIT NUMBER: ' & p_web.GSV('job:exchange_unit_Number'))
      end !if p_web.GSV('job:Exchange_Consignemnt_Number') > 0
      case p_web.GSV('tmp:RemovalReason')
      of 1
          p_web.SSV('AddToAudit:Action','REPLACED FAULTY EXCHANGE UNIT')
      of 2
          p_web.SSV('AddToAudit:Action','ALTERNATIVE EXCHANGE MODEL REQUIRED')
      of 3
          p_web.SSV('AddToAudit:Action','EXCHANGE UNIT NOT REQUIRED: RE-STOCKED')
      end ! case p_web.GSV('tmp:RemovalReason')
  
      addToAudit(p_web)
  
      !p_web.SSV('job:Exchange_Unit_Number','')
      p_web.SSV('job:Exchange_Accessory','')
      p_web.SSV('job:Exchange_Consignment_Number','')
      p_web.SSV('job:Exchange_Despatched','')
      p_web.SSV('job:exchange_Despatched_User','')
      p_web.SSV('job:Exchange_Issued_Date','')
      p_web.SSV('job:Exchange_User','')
      if (p_web.GSV('job:Despatch_Type') = 'EXC')
          p_web.SSV('job:Despatched','NO')
          p_web.SSV('job:Despatch_type','')
      else ! if (p_web.GSV('job:Despatch_Type') = 'EXC')
          if (p_web.GSV('jobe:DespatchType') = 'EXC')
              p_web.SSV('jobe:DespatchType','')
              p_web.SSV('wob:ReadyToDespatch',0)
          end ! if (p_web.GSV('jobe:DespatchType') = 'EXC')
      end !
  
  
      getStatus(101,0,'EXC',p_web)
  
      restock# = 0
      Access:PARTS.Clearkey(par:Part_Number_Key)
      par:Ref_Number    = p_web.GSV('job:Ref_Number')
      par:Part_Number    = 'EXCH'
      SET(par:Part_Number_Key,par:part_Number_key)
      loop
          if (Access:parts.Next())
              break
          end
          if (par:Ref_Number <> p_web.GSV('job:Ref_Number'))
              break
          end
          if (par:Part_Number <> 'EXCH')
              break
          end
          ! Found
          removeFromStockAllocation(par:Record_Number,'CHA')
          access:PARTS.deleterecord(0)
          restock# = 1
      end ! if (Access:PARTS.TryFetch(par:Part_Number_Key) = Level:Benign)
  
      Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
      wpr:Ref_Number = p_web.GSV('job:Ref_Number')
      wpr:Part_Number  = 'EXCH'
      SET(wpr:Part_Number_Key,wpr:Part_Number_Key)
      loop
          if (Access:WARPARTS.Next())
              break
          end
          if (wpr:Ref_Number <> p_web.GSV('job:Ref_Number'))
              break
          end
          if (wpr:Part_Number <> 'EXCH')
              break
          end
  
          RemoveFromStockAllocation(wpr:Record_Number,'WAR')
          Access:WARPARTS.Deleterecord(0)
          restock# = 1
      end
  
      ! Remove incoming unit
      Access:EXCHANGE.Clearkey(xch:ESN_Only_Key)
      xch:ESN    = p_web.GSV('job:ESN')
      set(xch:ESN_Only_Key,xch:ESN_Only_Key)
      loop
          if (Access:EXCHANGE.Next())
              Break
          end ! if (Access:EXCHANGE.Next())
          if (xch:ESN    <> p_web.GSV('job:ESN'))
              Break
          end ! if (xch:ESN    <> p_web.GSV('job:ESN'))
          if (xch:Job_Number = p_web.GSV('job:Ref_Number'))
              relate:EXCHANGE.delete(0)
          end ! if (xch:Job_Number = p_web.GSV('job:Ref_Number'))
      end ! loop
  
  !    ! Dont think this is necessary?!
  !    if (restock# = 1)
  !        local.AllocateExchangePart('RTS',0)
  !    end ! if (restock# = 1)
  
  
      Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
      xch:Ref_Number    = p_web.GSV('job:Exchange_Unit_Number')
      if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
          ! Found
          if (p_web.GSV('tmp:RemovalReason') = 1)
                xch:Available = 'INR'
                xch:StatusChangeDate = TODAY() ! #12127 Record status change. (Bryan: 13/07/2011)
              p_web.SSV('locExchangeAlertMessage','Please book the replaced Exchange Unit as a new job')
          else ! if (p_web.GSV('tmp:RemovalReason') = 1)
              if (xch:Available <> 'QAF')
                    xch:available = 'AVL'
                    xch:StatusChangeDate = TODAY() ! #12127 Record status change. (Bryan: 13/07/2011)
              end ! if (xch:Available <> 'QAF')
          end ! if (p_web.GSV('tmp:RemovalReason') = 1)
          xch:Job_Number = ''
          access:EXCHANGE.tryUpdate()
          if (Access:EXCHHIST.PrimeRecord() = Level:Benign)
              exh:Ref_Number    = p_web.GSV('job:Exchange_Unit_Number')
              exh:Date = today()
              exh:Time = clock()
              exh:User = p_web.GSV('BookingUserCode')
              if (p_web.GSV('tmp:RemovalReason') = 1)
                  exh:Status = 'FAULTY UNIT RE-STOCKED FROM JOB NO: ' & clip(format(p_web.GSV('job:Ref_Number'),@p<<<<<<<#p))
              else ! if (p_web.GSV('tmp:RemovalReason') = 1)
                  exh:Status = 'UNIT RE-STOCKED FROM JOB NO: ' & clip(format(p_web.GSV('job:Ref_Number'),@p<<<<<<<#p))
              end !if (p_web.GSV('tmp:RemovalReason') = 1)
              if (Access:EXCHHIST.TryInsert() = Level:Benign)
                  ! Inserted
              else ! if (Access:EXCHHIST.TryInsert() = Level:Benign)
                  ! Error
                  Access:EXCHHIST.CancelAutoInc()
              end ! if (Access:EXCHHIST.TryInsert() = Level:Benign)
          end ! if (Access:EXCHHIST.PrimeRecord() = Level:Benign)
      else ! if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
          ! Error
      end ! if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
  
      p_web.SSV('job:Exchange_Unit_Number','')
  
  
      Access:JOBS.Clearkey(job:Ref_Number_Key)
      job:Ref_Number    = p_web.GSV('job:Ref_Number')
      if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
          ! Found
          p_web.SessionQueueToFile(JOBS)
          access:JOBS.tryUpdate()
      else ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
          ! Error
      end ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
  
      Access:JOBSE.Clearkey(jobe:RefNumberKey)
      jobe:RefNumber    = p_web.GSV('job:Ref_Number')
      if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
          ! Found
          p_web.SessionQueueToFile(JOBSE)
          access:JOBSE.tryUpdate()
      else ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
          ! Error
      end ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
  
      p_web.SessionQueueToFile(WEBJOB)
      access:WEBJOB.TryUpdate()
  
      p_web.SSV('tmp:ExchangeUnitNumber',0)
  
      do clearExchangeDetails
  
      p_web.SSV('Hide:RemovalReason',1)
  do Value::buttonConfirmExchangeRemoval
  do Comment::buttonConfirmExchangeRemoval ! allows comment style to be updated.
  do Value::buttonPickExchangeUnit  !1
  do Value::locExchangeAlertMessage  !1
  do Value::locRemovalAlertMessage  !1
  do Value::tmp:ExchangeAccessories  !1
  do Value::tmp:ExchangeIMEINumber  !1
  do Value::tmp:ExchangeLocation  !1
  do Value::tmp:ExchangeUnitDetails  !1
  do Value::tmp:MSN  !1
  do Prompt::tmp:RemovalReason
  do Value::tmp:RemovalReason  !1
  do Value::locRemoveText  !1

ValidateValue::buttonConfirmExchangeRemoval  Routine
    If not (p_web.GSV('Hide:RemovalReason') = 1 Or p_web.GSV('tmp:RemovalReason') = 0)
    End

Value::buttonConfirmExchangeRemoval  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:RemovalReason') = 1 Or p_web.GSV('tmp:RemovalReason') = 0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('buttonConfirmExchangeRemoval') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('Hide:RemovalReason') = 1 Or p_web.GSV('tmp:RemovalReason') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonConfirmExchangeRemoval'',''pickexchangeunit_buttonconfirmexchangeremoval_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','ConfirmExchangeRemoval','Confirm Exchange Removal',p_web.combine(Choose('Confirm Exchange Removal' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'button-entryfield'),loc:formname,,,,loc:javascript,loc:disabled,'images\packdelete.png',,,,,,,,,'nt-left')!h
  do SendPacket
  End
  p_web.DivFooter()
Comment::buttonConfirmExchangeRemoval  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if buttonConfirmExchangeRemoval:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:RemovalReason') = 1 Or p_web.GSV('tmp:RemovalReason') = 0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('PickExchangeUnit_' & p_web._nocolon('buttonConfirmExchangeRemoval') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:RemovalReason') = 1 Or p_web.GSV('tmp:RemovalReason') = 0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('PickExchangeUnit_nexttab_' & 0)
    job:ESN = p_web.GSV('job:ESN')
    do ValidateValue::job:ESN
    If loc:Invalid
      loc:retrying = 1
      do Value::job:ESN
      !do SendAlert
      do Comment::job:ESN ! allows comment style to be updated.
      !exit
    End
    job:MSN = p_web.GSV('job:MSN')
    do ValidateValue::job:MSN
    If loc:Invalid
      loc:retrying = 1
      do Value::job:MSN
      !do SendAlert
      do Comment::job:MSN ! allows comment style to be updated.
      !exit
    End
    job:Charge_Type = p_web.GSV('job:Charge_Type')
    do ValidateValue::job:Charge_Type
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Charge_Type
      !do SendAlert
      do Comment::job:Charge_Type ! allows comment style to be updated.
      !exit
    End
    job:Warranty_Charge_Type = p_web.GSV('job:Warranty_Charge_Type')
    do ValidateValue::job:Warranty_Charge_Type
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Warranty_Charge_Type
      !do SendAlert
      do Comment::job:Warranty_Charge_Type ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('PickExchangeUnit_nexttab_' & 1)
    tmp:ExchangeIMEINumber = p_web.GSV('tmp:ExchangeIMEINumber')
    do ValidateValue::tmp:ExchangeIMEINumber
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:ExchangeIMEINumber
      !do SendAlert
      do Comment::tmp:ExchangeIMEINumber ! allows comment style to be updated.
      !exit
    End
    locExchangeAlertMessage = p_web.GSV('locExchangeAlertMessage')
    do ValidateValue::locExchangeAlertMessage
    If loc:Invalid
      loc:retrying = 1
      do Value::locExchangeAlertMessage
      !do SendAlert
      do Comment::locExchangeAlertMessage ! allows comment style to be updated.
      !exit
    End
    tmp:MSN = p_web.GSV('tmp:MSN')
    do ValidateValue::tmp:MSN
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:MSN
      !do SendAlert
      do Comment::tmp:MSN ! allows comment style to be updated.
      !exit
    End
    tmp:ExchangeUnitDetails = p_web.GSV('tmp:ExchangeUnitDetails')
    do ValidateValue::tmp:ExchangeUnitDetails
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:ExchangeUnitDetails
      !do SendAlert
      do Comment::tmp:ExchangeUnitDetails ! allows comment style to be updated.
      !exit
    End
    tmp:ExchangeAccessories = p_web.GSV('tmp:ExchangeAccessories')
    do ValidateValue::tmp:ExchangeAccessories
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:ExchangeAccessories
      !do SendAlert
      do Comment::tmp:ExchangeAccessories ! allows comment style to be updated.
      !exit
    End
    tmp:ExchangeLocation = p_web.GSV('tmp:ExchangeLocation')
    do ValidateValue::tmp:ExchangeLocation
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:ExchangeLocation
      !do SendAlert
      do Comment::tmp:ExchangeLocation ! allows comment style to be updated.
      !exit
    End
    tmp:HandsetPartNumber = p_web.GSV('tmp:HandsetPartNumber')
    do ValidateValue::tmp:HandsetPartNumber
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:HandsetPartNumber
      !do SendAlert
      do Comment::tmp:HandsetPartNumber ! allows comment style to be updated.
      !exit
    End
    tmp:ReplacementValue = p_web.GSV('tmp:ReplacementValue')
    do ValidateValue::tmp:ReplacementValue
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:ReplacementValue
      !do SendAlert
      do Comment::tmp:ReplacementValue ! allows comment style to be updated.
      !exit
    End
    tmp:HandsetReplacementValue = p_web.GSV('tmp:HandsetReplacementValue')
    do ValidateValue::tmp:HandsetReplacementValue
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:HandsetReplacementValue
      !do SendAlert
      do Comment::tmp:HandsetReplacementValue ! allows comment style to be updated.
      !exit
    End
    tmp:NoUnitAvailable = p_web.GSV('tmp:NoUnitAvailable')
    do ValidateValue::tmp:NoUnitAvailable
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:NoUnitAvailable
      !do SendAlert
      do Comment::tmp:NoUnitAvailable ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('PickExchangeUnit_nexttab_' & 2)
    job:Exchange_Courier = p_web.GSV('job:Exchange_Courier')
    do ValidateValue::job:Exchange_Courier
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Exchange_Courier
      !do SendAlert
      do Comment::job:Exchange_Courier ! allows comment style to be updated.
      !exit
    End
    job:Exchange_Consignment_Number = p_web.GSV('job:Exchange_Consignment_Number')
    do ValidateValue::job:Exchange_Consignment_Number
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Exchange_Consignment_Number
      !do SendAlert
      do Comment::job:Exchange_Consignment_Number ! allows comment style to be updated.
      !exit
    End
    job:Exchange_Despatched = p_web.GSV('job:Exchange_Despatched')
    do ValidateValue::job:Exchange_Despatched
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Exchange_Despatched
      !do SendAlert
      do Comment::job:Exchange_Despatched ! allows comment style to be updated.
      !exit
    End
    job:Exchange_Despatched_User = p_web.GSV('job:Exchange_Despatched_User')
    do ValidateValue::job:Exchange_Despatched_User
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Exchange_Despatched_User
      !do SendAlert
      do Comment::job:Exchange_Despatched_User ! allows comment style to be updated.
      !exit
    End
    job:Exchange_Despatch_Number = p_web.GSV('job:Exchange_Despatch_Number')
    do ValidateValue::job:Exchange_Despatch_Number
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Exchange_Despatch_Number
      !do SendAlert
      do Comment::job:Exchange_Despatch_Number ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('PickExchangeUnit_nexttab_' & 3)
    locRemovalAlertMessage = p_web.GSV('locRemovalAlertMessage')
    do ValidateValue::locRemovalAlertMessage
    If loc:Invalid
      loc:retrying = 1
      do Value::locRemovalAlertMessage
      !do SendAlert
      do Comment::locRemovalAlertMessage ! allows comment style to be updated.
      !exit
    End
    tmp:RemovalReason = p_web.GSV('tmp:RemovalReason')
    do ValidateValue::tmp:RemovalReason
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:RemovalReason
      !do SendAlert
      do Comment::tmp:RemovalReason ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_PickExchangeUnit_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('PickExchangeUnit_tab_' & 0)
    do GenerateTab0
  of lower('PickExchangeUnit_tab_' & 1)
    do GenerateTab1
  of lower('PickExchangeUnit_tmp:ExchangeIMEINumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ExchangeIMEINumber
      of event:timer
        do Value::tmp:ExchangeIMEINumber
        do Comment::tmp:ExchangeIMEINumber
      else
        do Value::tmp:ExchangeIMEINumber
      end
  of lower('PickExchangeUnit_buttonPickExchangeUnit_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonPickExchangeUnit
      of event:timer
        do Value::buttonPickExchangeUnit
        do Comment::buttonPickExchangeUnit
      else
        do Value::buttonPickExchangeUnit
      end
  of lower('PickExchangeUnit_tmp:HandsetPartNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:HandsetPartNumber
      of event:timer
        do Value::tmp:HandsetPartNumber
        do Comment::tmp:HandsetPartNumber
      else
        do Value::tmp:HandsetPartNumber
      end
  of lower('PickExchangeUnit_tmp:HandsetReplacementValue_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:HandsetReplacementValue
      of event:timer
        do Value::tmp:HandsetReplacementValue
        do Comment::tmp:HandsetReplacementValue
      else
        do Value::tmp:HandsetReplacementValue
      end
  of lower('PickExchangeUnit_tab_' & 2)
    do GenerateTab2
  of lower('PickExchangeUnit_job:Exchange_Courier_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Exchange_Courier
      of event:timer
        do Value::job:Exchange_Courier
        do Comment::job:Exchange_Courier
      else
        do Value::job:Exchange_Courier
      end
  of lower('PickExchangeUnit_button:AmendDespatchDetails_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::button:AmendDespatchDetails
      of event:timer
        do Value::button:AmendDespatchDetails
        do Comment::button:AmendDespatchDetails
      else
        do Value::button:AmendDespatchDetails
      end
  of lower('PickExchangeUnit_tab_' & 3)
    do GenerateTab3
  of lower('PickExchangeUnit_buttonRemoveAttachedUnit_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonRemoveAttachedUnit
      of event:timer
        do Value::buttonRemoveAttachedUnit
        do Comment::buttonRemoveAttachedUnit
      else
        do Value::buttonRemoveAttachedUnit
      end
  of lower('PickExchangeUnit_locRemovalAlertMessage_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locRemovalAlertMessage
      of event:timer
        do Value::locRemovalAlertMessage
        do Comment::locRemovalAlertMessage
      else
        do Value::locRemovalAlertMessage
      end
  of lower('PickExchangeUnit_tmp:RemovalReason_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:RemovalReason
      of event:timer
        do Value::tmp:RemovalReason
        do Comment::tmp:RemovalReason
      else
        do Value::tmp:RemovalReason
      end
  of lower('PickExchangeUnit_buttonConfirmExchangeRemoval_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonConfirmExchangeRemoval
      of event:timer
        do Value::buttonConfirmExchangeRemoval
        do Comment::buttonConfirmExchangeRemoval
      else
        do Value::buttonConfirmExchangeRemoval
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('PickExchangeUnit_form:ready_',1)

  p_web.SetSessionValue('PickExchangeUnit_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_PickExchangeUnit',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('PickExchangeUnit_form:ready_',1)
  p_web.SetSessionValue('PickExchangeUnit_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_PickExchangeUnit',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('PickExchangeUnit_form:ready_',1)
  p_web.SetSessionValue('PickExchangeUnit_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('PickExchangeUnit:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('PickExchangeUnit_form:ready_',1)
  p_web.SetSessionValue('PickExchangeUnit_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('PickExchangeUnit:Primed',0)
  p_web.setsessionvalue('showtab_PickExchangeUnit',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
      If not (1=0)
        If not (p_web.GSV('ReadOnly:ExchangeIMEINumber') = 1 OR p_web.GSV('job:Exchange_Unit_Number') > 0)
          If p_web.IfExistsValue('tmp:ExchangeIMEINumber')
            tmp:ExchangeIMEINumber = p_web.GetValue('tmp:ExchangeIMEINumber')
          End
        End
      End
      If not (p_web.GSV('BookingSite') <> 'ARC' Or p_web.GSV('Hide:HandsetPartNumber') = 1)
        If not (p_web.GSV('ReadOnly:ExchangeIMEINumber') = 1)
          If p_web.IfExistsValue('tmp:HandsetPartNumber')
            tmp:HandsetPartNumber = p_web.GetValue('tmp:HandsetPartNumber')
          End
        End
      End
      If not (p_web.GSV('tmp:ExchangeUnitNumber') > 0)
        If not (p_web.GSV('ReadOnly:ExchangeIMEINumber') = 1 OR p_web.GSV('job:Exchange_Unit_Number') > 0)
          If p_web.IfExistsValue('tmp:NoUnitAvailable') = 0
            p_web.SetValue('tmp:NoUnitAvailable',0)
            tmp:NoUnitAvailable = 0
          Else
            tmp:NoUnitAvailable = p_web.GetValue('tmp:NoUnitAvailable')
          End
        End
      End
      If not (1=0)
          If p_web.IfExistsValue('job:Exchange_Courier')
            job:Exchange_Courier = p_web.GetValue('job:Exchange_Courier')
          End
      End
      If not (1=0)
        If not (p_web.GSV('ReadOnly:ExchangeDespatchDetails') = 1)
          If p_web.IfExistsValue('job:Exchange_Consignment_Number')
            job:Exchange_Consignment_Number = p_web.GetValue('job:Exchange_Consignment_Number')
          End
        End
      End
      If not (1=0)
        If not (p_web.GSV('ReadOnly:ExchangeDespatchDetails') = 1)
          If p_web.IfExistsValue('job:Exchange_Despatched')
            job:Exchange_Despatched = p_web.GetValue('job:Exchange_Despatched')
          End
        End
      End
      If not (1=0)
        If not (p_web.GSV('ReadOnly:ExchangeDespatchDetails') = 1)
          If p_web.IfExistsValue('job:Exchange_Despatched_User')
            job:Exchange_Despatched_User = p_web.GetValue('job:Exchange_Despatched_User')
          End
        End
      End
      If not (1=0)
        If not (p_web.GSV('ReadOnly:ExchangeDespatchDetails') = 1)
          If p_web.IfExistsValue('job:Exchange_Despatch_Number')
            job:Exchange_Despatch_Number = p_web.GetValue('job:Exchange_Despatch_Number')
          End
        End
      End
  If p_web.GSV('job:Exchange_Unit_Number') > 0
      If not (p_web.GSV('Hide:RemovalReason') = 1)
          If p_web.IfExistsValue('tmp:RemovalReason')
            tmp:RemovalReason = p_web.GetValue('tmp:RemovalReason')
          End
      End
  End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('PickExchangeUnit_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
      if (p_web.GSV('tmp:ExchangeUnitNumber') > 0)
          if (p_web.GSV('tmp:ExchangeUnitNumber') <> p_web.GSV('job:Exchange_Unit_Number'))
              do addExchangeUnit
          end ! if (p_web.GSV('tmp:ExchangeUnitNumber') <> p_web.GSV('job:Exchange_Unit_Number'))
      else ! if (p_web.GSV('tmp:ExchangeUnitNumber') > 0)
          if (p_web.GSV('tmp:NoUnitAvailable') = 1)
  
              getStatus(350,0,'EXC',p_web)
  
  
              getStatus(355,0,'JOB',p_web)
  
              local.AllocateExchangePart('ORD',0)
          end ! if (p_web.GSV('tmp:NoUnitAvailable') = 1)
      end ! if (p_web.GSV('tmp:ExchangeUnitNumber') > 0)
      do clearVariables
  p_web.DeleteSessionValue('PickExchangeUnit_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
    do ValidateValue::job:ESN
    If loc:Invalid then exit.
    do ValidateValue::job:MSN
    If loc:Invalid then exit.
    do ValidateValue::locUnitDetails
    If loc:Invalid then exit.
    do ValidateValue::job:Charge_Type
    If loc:Invalid then exit.
    do ValidateValue::job:Warranty_Charge_Type
    If loc:Invalid then exit.
  ! tab = 2
    loc:InvalidTab += 1
    do ValidateValue::tmp:ExchangeIMEINumber
    If loc:Invalid then exit.
    do ValidateValue::locExchangeAlertMessage
    If loc:Invalid then exit.
    do ValidateValue::buttonPickExchangeUnit
    If loc:Invalid then exit.
    do ValidateValue::tmp:MSN
    If loc:Invalid then exit.
    do ValidateValue::tmp:ExchangeUnitDetails
    If loc:Invalid then exit.
    do ValidateValue::tmp:ExchangeAccessories
    If loc:Invalid then exit.
    do ValidateValue::tmp:ExchangeLocation
    If loc:Invalid then exit.
    do ValidateValue::tmp:HandsetPartNumber
    If loc:Invalid then exit.
    do ValidateValue::tmp:ReplacementValue
    If loc:Invalid then exit.
    do ValidateValue::tmp:HandsetReplacementValue
    If loc:Invalid then exit.
    do ValidateValue::tmp:NoUnitAvailable
    If loc:Invalid then exit.
  ! tab = 5
    loc:InvalidTab += 1
    do ValidateValue::job:Exchange_Courier
    If loc:Invalid then exit.
    do ValidateValue::job:Exchange_Consignment_Number
    If loc:Invalid then exit.
    do ValidateValue::job:Exchange_Despatched
    If loc:Invalid then exit.
    do ValidateValue::job:Exchange_Despatched_User
    If loc:Invalid then exit.
    do ValidateValue::job:Exchange_Despatch_Number
    If loc:Invalid then exit.
    do ValidateValue::button:AmendDespatchDetails
    If loc:Invalid then exit.
  ! tab = 3
  If p_web.GSV('job:Exchange_Unit_Number') > 0
    loc:InvalidTab += 1
    do ValidateValue::locRemoveText
    If loc:Invalid then exit.
    do ValidateValue::buttonRemoveAttachedUnit
    If loc:Invalid then exit.
    do ValidateValue::locRemovalAlertMessage
    If loc:Invalid then exit.
    do ValidateValue::tmp:RemovalReason
    If loc:Invalid then exit.
    do ValidateValue::buttonConfirmExchangeRemoval
    If loc:Invalid then exit.
  End
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('PickExchangeUnit:Primed',0)
  p_web.StoreValue('job:ESN')
  p_web.StoreValue('job:MSN')
  p_web.StoreValue('')
  p_web.StoreValue('job:Charge_Type')
  p_web.StoreValue('job:Warranty_Charge_Type')
  p_web.StoreValue('tmp:ExchangeIMEINumber')
  p_web.StoreValue('locExchangeAlertMessage')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:MSN')
  p_web.StoreValue('tmp:ExchangeUnitDetails')
  p_web.StoreValue('tmp:ExchangeAccessories')
  p_web.StoreValue('tmp:ExchangeLocation')
  p_web.StoreValue('tmp:HandsetPartNumber')
  p_web.StoreValue('tmp:ReplacementValue')
  p_web.StoreValue('tmp:HandsetReplacementValue')
  p_web.StoreValue('tmp:NoUnitAvailable')
  p_web.StoreValue('job:Exchange_Courier')
  p_web.StoreValue('job:Exchange_Consignment_Number')
  p_web.StoreValue('job:Exchange_Despatched')
  p_web.StoreValue('job:Exchange_Despatched_User')
  p_web.StoreValue('job:Exchange_Despatch_Number')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locRemovalAlertMessage')
  p_web.StoreValue('tmp:RemovalReason')
  p_web.StoreValue('')

Local.AllocateExchangePart        Procedure(String    func:Status,Byte func:SecondUnit)
local:FoundPart                 Byte(0)
Code
    If p_web.GSV('job:Warranty_Job') = 'YES'
        Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
        wpr:Ref_Number  = p_web.GSV('job:Ref_Number')
        wpr:Part_Number = 'EXCH'
        Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
        Loop
            If Access:WARPARTS.NEXT()
               Break
            End !If
            If wpr:Ref_Number  <> p_web.GSV('job:Ref_Number')      |
            Or wpr:Part_Number <> 'EXCH'      |
                Then Break.  ! End If
            If func:SecondUnit
                If wpr:SecondExchangeUnit
                    local:FoundPart = True
                End !If wpr:SecondExchangeUnit
            Else !If func:SecondUnit
                local:FoundPart = True
            End !If func:SecondUnit
            If local:FoundPart = True
                wpr:Status = func:Status
                If func:Status = 'PIK'
                    wpr:PartAllocated = 1
                End !If func:Status = 'PIK'
                Access:WARPARTS.Update()
                !Remove the EXCH line from Stock Allocation,
                !just incase the Exchange isn't being added properly - L873 (DBH: 24-07-2003)
                RemoveFromStockAllocation(wpr:Record_Number,'WAR')
                Break
            End !If local:FoundPart = True
        End !Loop
    End !If job:Warranty_Job = 'YES'


    If local:FoundPart = False
        If p_web.GSV('job:Chargeable_Job') = 'YES'
            Access:PARTS.ClearKey(par:Part_Number_Key)
            par:Ref_Number  = p_web.GSV('job:Ref_Number')
            par:Part_Number = 'EXCH'
            Set(par:Part_Number_Key,par:Part_Number_Key)
            Loop
                If Access:PARTS.NEXT()
                   Break
                End !If
                If par:Ref_Number  <> p_web.GSV('job:Ref_Number')      |
                Or par:Part_Number <> 'EXCH'      |
                    Then Break.  ! End If
                If func:SecondUnit
                    If par:SecondExchangeUnit
                        local:FoundPart = True
                    End !If wpr:SecondExchangeUnit
                Else !If func:SecondUnit
                    local:FoundPart = True
                End !If func:SecondUnit
                If local:FoundPart = True
                    par:Status = func:Status
                    If func:Status = 'PIK'
                        par:PartAllocated = 1
                    End !If func:Status = 'PIK'
                    Access:PARTS.Update()
                    !Remove the EXCH line from Stock Allocation,
                    !just incase the Exchange isn't being added properly - L873 (DBH: 24-07-2003)
                    RemoveFromStockAllocation(par:Record_Number,'CHA')
                    Break
                End !If local:FoundPart = True

            End !Loop
        End !If job:Chargeable_Job = 'YES'
    End !If local:FounPart = False

    If local:FoundPart = False
        If p_web.GSV('job:Engineer') = ''
            Access:USERS.Clearkey(use:Password_Key)
            use:Password    = p_web.GSV('BookingUserPassword')
            If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                !Found

            Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                !Error
            End !If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        Else !If job:Engineer = ''
            Access:USERS.Clearkey(use:User_Code_Key)
            use:User_Code   = p_web.GSV('job:Engineer')
            If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign

            End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign

        End !If job:Engineer = ''
        Access:STOCK.Clearkey(sto:Location_Key)
        sto:Location    = use:Location
        sto:Part_Number = 'EXCH'
        If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
            !Found
            Access:LOCATION.Clearkey(loc:Location_Key)
            loc:Location    = sto:Location
            If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
                !Found
!                    If ~loc:UseRapidStock
!                        Return
!                    End !If ~loc:UseRapidStock

            Else ! If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
                !Error
            End !If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
            If p_web.GSV('job:Warranty_Job') = 'YES'
               get(warparts,0)
               if access:warparts.primerecord() = level:benign
                   wpr:PArt_Ref_Number      = sto:Ref_Number
                   wpr:ref_number            = p_web.GSV('job:ref_number')
                   wpr:adjustment            = 'YES'
                   wpr:part_number           = 'EXCH'
                   wpr:description           = p_web.GSV('job:Manufacturer') & ' EXCHANGE UNIT'
                   wpr:quantity              = 1
                   wpr:warranty_part         = 'NO'
                   wpr:exclude_from_order    = 'YES'
                   wpr:PartAllocated         = 1
                   wpr:Status                = func:Status
                   wpr:ExchangeUnit          = True
                   wpr:SecondExchangeUnit    = func:SecondUnit
                   If sto:Assign_Fault_Codes = 'YES'
                       !Try and get the fault codes. This key should get the only record
                       Access:STOMODEL.ClearKey(stm:Ref_Part_Description)
                       stm:Ref_Number  = sto:Ref_Number
                       stm:Part_Number = sto:Part_Number
                       stm:Location    = sto:Location
                       stm:Description = sto:Description
                       If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                           !Found
                           wpr:Fault_Code1  = stm:FaultCode1
                           wpr:Fault_Code2  = stm:FaultCode2
                           wpr:Fault_Code3  = stm:FaultCode3
                           wpr:Fault_Code4  = stm:FaultCode4
                           wpr:Fault_Code5  = stm:FaultCode5
                           wpr:Fault_Code6  = stm:FaultCode6
                           wpr:Fault_Code7  = stm:FaultCode7
                           wpr:Fault_Code8  = stm:FaultCode8
                           wpr:Fault_Code9  = stm:FaultCode9
                           wpr:Fault_Code10 = stm:FaultCode10
                           wpr:Fault_Code11 = stm:FaultCode11
                           wpr:Fault_Code12 = stm:FaultCode12
                       Else!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                       !Error
                       !Assert(0,'<13,10>Fetch Error<13,10>')
                       End!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                   end !If sto:Assign_Fault_Codes = 'YES'
                   if access:warparts.insert()
                       access:warparts.cancelautoinc()
                   end
                End !If Prime

             Else !If job:Warranty_Job = 'YES'
                If p_web.GSV('job:Chargeable_Job') = 'YES'
                    get(parts,0)
                    if access:parts.primerecord() = level:benign
                        !message('At break2')
                        par:PArt_Ref_Number      = sto:Ref_Number
                        par:ref_number            = p_web.GSV('job:ref_number')
                        par:adjustment            = 'YES'
                        par:part_number           = 'EXCH'
                        par:description           = p_web.GSV('job:Manufacturer') & ' EXCHANGE UNIT'
                        par:quantity              = 1
                        par:warranty_part         = 'NO'
                        par:exclude_from_order    = 'YES'
                        par:PartAllocated         = 1
                        par:Status                = func:Status
                        par:ExchangeUnit          = True
                        par:SecondExchangeUnit    = func:SecondUnit
                        If sto:Assign_Fault_Codes = 'YES'
                           !Try and get the fault codes. This key should get the only record
                           Access:STOMODEL.ClearKey(stm:Ref_Part_Description)
                           stm:Ref_Number  = sto:Ref_Number
                           stm:Part_Number = sto:Part_Number
                           stm:Location    = sto:Location
                           stm:Description = sto:Description
                           If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                               !Found
                               par:Fault_Code1  = stm:FaultCode1
                               par:Fault_Code2  = stm:FaultCode2
                               par:Fault_Code3  = stm:FaultCode3
                               par:Fault_Code4  = stm:FaultCode4
                               par:Fault_Code5  = stm:FaultCode5
                               par:Fault_Code6  = stm:FaultCode6
                               par:Fault_Code7  = stm:FaultCode7
                               par:Fault_Code8  = stm:FaultCode8
                               par:Fault_Code9  = stm:FaultCode9
                               par:Fault_Code10 = stm:FaultCode10
                               par:Fault_Code11 = stm:FaultCode11
                               par:Fault_Code12 = stm:FaultCode12
                           Else!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                               !Error
                               !Assert(0,'<13,10>Fetch Error<13,10>')
                           End!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                        End !If sto:Assign_Fault_Codes = 'YES'
                        if access:parts.insert()
                            access:parts.cancelautoinc()
                        end
                    End !If access:Prime

                End !If job:Chargeable_Job = 'YES'
             End !If job:Warranty_Job = 'YES'

        Else ! If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
             !Error
!            Case Missive('You must have a Part setup in Stock Control with a part number of "EXCH" under the location ' & Clip(use:Location) & '.','ServiceBase 3g',|
!                           'mstop.jpg','/OK')
!                Of 1 ! OK Button
!            End ! Case Missive
        End !If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
    End !If local:FounPart = False
BrowseExchangeUnits  PROCEDURE  (NetWebServerWorker p_web)
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256),dim(Net:MaxKeyFields)
idx           String(Net:HashSize)
sub           Long
            End
xch:ESN:IsInvalid  Long
xch:Ref_Number:IsInvalid  Long
xch:Manufacturer:IsInvalid  Long
xch:Model_Number:IsInvalid  Long
xch:MSN:IsInvalid  Long
xch:Colour:IsInvalid  Long
Select:IsInvalid  Long
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:DefaultSelection    String(Net:HashSize)
loc:ActualSelection     String(Net:HashSize)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
!loc:pagename            String(256)
loc:NavButtonPosition    Long
loc:UpdateButtonPosition Long
loc:SelectionMethod      Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorCase         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
loc:lookupdone          Long
loc:FormPopup           Long
ThisView            View(EXCHANGE)
                      Project(xch:Ref_Number)
                      Project(xch:ESN)
                      Project(xch:Ref_Number)
                      Project(xch:Manufacturer)
                      Project(xch:Model_Number)
                      Project(xch:MSN)
                      Project(xch:Colour)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
Loc:popup             Long
loc:poppedup          Long,static,thread
FilesOpened     Long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return 
  End
  GlobalErrors.SetProcedureName('BrowseExchangeUnits')
  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseExchangeUnits:NoForm')
      loc:NoForm = p_web.GetValue('BrowseExchangeUnits:NoForm')
      loc:FormName = p_web.GetValue('BrowseExchangeUnits:FormName')
    else
      loc:FormName = 'BrowseExchangeUnits_frm'
    End
    p_web.SSV('BrowseExchangeUnits:NoForm',loc:NoForm)
    p_web.SSV('BrowseExchangeUnits:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseExchangeUnits:NoForm')
    loc:FormName = p_web.GSV('BrowseExchangeUnits:FormName')
  end
  case p_web.site.formpopups
  of -1 ; loc:FormPopup = Net:Page
  of 0 ; loc:FormPopup = Net:Page
  of 1 ; loc:FormPopup = Net:Popup
  End
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseExchangeUnits') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseExchangeUnits')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if 1 = 2
  elsif p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  elsif p_web.IfExistsValue('_EIPRow_')
    do CallRow
  elsif p_web.GetValue('_CallPopups') > 0
    do CallPopups
  elsif p_web.IfExistsValue('_GetSecwinSettings')
      p_web.SetValue('Secwin_' & 'BrowseExchangeUnits' & 'AccessGroupsArray','1') 
      p_web.SetValue('Secwin_AccessWindowName','BrowseExchangeUnits')
      
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallPopups  Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
loc:CallPopups   Long
loc:name         String(255)
  code
  if loc:poppedup = 0
    loc:CallPopups = p_web.GetValue('_CallPopups')
    if loc:CallPopups = 1 !and p_web.GetPreCall('BrowseExchangeUnits') = 0! for = 1 include popup divs and scripts for this browse
      !p_web.AddPreCall('BrowseExchangeUnits')
      p_web.DivHeader('popup_BrowseExchangeUnits','nt-hidden')
      p_web.DivHeader('BrowseExchangeUnits',p_web.combine(p_web.site.style.browsediv,))
      p_web.DivFooter()
      p_web.DivFooter()
      loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(400)&', modal: true, position: [''center'',15]'
      packet =  p_web.jQuery('#' & lower('popup_BrowseExchangeUnits_div'),'dialog',loc:options) & |
      p_web.jQuery('#' & lower('popup_BrowseExchangeUnits_div'),'removeClass','''nt-hidden''')
    End
    ! for > 0 add in form, EIP-lookups and browse object
    do Popups
    loc:popup = 1
    do ClosingScripts
    do SendPacket
    loc:poppedup = 1
  end

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web.DivHeader('BrowseExchangeUnits',p_web.combine(p_web.site.style.browsediv,))
  if loc:ParentSilent = 0
    do GenerateBrowse
    if p_web.RequestAjax = 1
      p_web.Script('BrowseProducts.pcv();')
    end
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web.DivFooter()
  do Children
  do Popups
  if loc:poppedup = 0
    do ClosingScripts
  end
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(EXCHANGE,xch:Ref_Number_Key,loc:vorder)
    If False
    ElsIf (loc:vorder = 'XCH:ESN') then p_web.SetValue('BrowseExchangeUnits_sort','1')
    ElsIf (loc:vorder = 'XCH:REF_NUMBER') then p_web.SetValue('BrowseExchangeUnits_sort','3')
    ElsIf (loc:vorder = 'XCH:MANUFACTURER') then p_web.SetValue('BrowseExchangeUnits_sort','7')
    ElsIf (loc:vorder = 'XCH:MODEL_NUMBER') then p_web.SetValue('BrowseExchangeUnits_sort','4')
    ElsIf (loc:vorder = 'XCH:MSN') then p_web.SetValue('BrowseExchangeUnits_sort','5')
    ElsIf (loc:vorder = 'XCH:COLOUR') then p_web.SetValue('BrowseExchangeUnits_sort','6')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseExchangeUnits:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseExchangeUnits:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseExchangeUnits:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseExchangeUnits:LookupField')
    loc:selecting = 0
  End

Popups Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  If loc:popup = 0
  End
SetFormAction  Routine
  loc:formaction = 'BrowseExchangeUnits'

GotFocusBack   Routine

GenerateBrowse Routine
  data
loc:viewoptions  Long
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  ! Set general Browse options
  loc:NavButtonPosition   = Net:Below
  loc:UpdateButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:PageLoad
  loc:Sorting          = Net:ServerSort
  if p_web.GetValue('_popup_') = 1
    loc:popup = 1
  end
  ! Set Locator Options
  loc:LocatorPosition  = Net:Above
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  p_web.ClearBrowse('BrowseExchangeUnits')
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 12
  End
  loc:ActualSelection = ''
  !loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseExchangeUnits_sort',net:DontEvaluate)
  p_web.SetSessionValue('BrowseExchangeUnits_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(xch:ESN)','-UPPER(xch:ESN)')
    Loc:LocateField = 'xch:ESN'
    Loc:LocatorCase = 0
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'xch:Ref_Number','-xch:Ref_Number')
    Loc:LocateField = 'xch:Ref_Number'
    Loc:LocatorCase = 0
  of 7
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(xch:Manufacturer)','-UPPER(xch:Manufacturer)')
    Loc:LocateField = 'xch:Manufacturer'
    Loc:LocatorCase = 0
  of 4
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(xch:Model_Number)','-UPPER(xch:Model_Number)')
    Loc:LocateField = 'xch:Model_Number'
    Loc:LocatorCase = 0
  of 5
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(xch:MSN)','-UPPER(xch:MSN)')
    Loc:LocateField = 'xch:MSN'
    Loc:LocatorCase = 0
  of 6
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(xch:Colour)','-UPPER(xch:Colour)')
    Loc:LocateField = 'xch:Colour'
    Loc:LocatorCase = 0
  of 2
    Loc:LocateField = ''
    Loc:LocatorCase = 0
  end
  if loc:vorder = ''
    loc:vorder = '+UPPER(xch:Available),+UPPER(xch:Location),+UPPER(xch:ESN)'
  end
  If False ! add range fields to sort order
  ElsIf (p_web.GSV('tmp:ExchangeStockType') <> '' AND p_web.GSV('tmp:ExchangeManufacturer') <> '' AND p_web.GSV('tmp:ExchangeModelNumber') <> '')
  ElsIf (p_web.GSV('tmp:ExchangeStockType') <> '' AND (p_web.GSV('tmp:ExchangeModelNumber') = '' OR p_web.GSV('tmp:ExchangeManufacturer') = ''))
  ElsIf (p_web.GSV('tmp:ExchangeStockType') = '' AND p_web.GSV('tmp:ExchangeManufacturer') <> '' AND p_web.GSV('tmp:ExchangeModelNumber') <> '')
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('xch:ESN')
    loc:SortHeader = p_web.Translate('I.M.E.I. Number')
    p_web.SetSessionValue('BrowseExchangeUnits_LocatorPic','@s30')
  Of upper('xch:Ref_Number')
    loc:SortHeader = p_web.Translate('Unit Number')
    p_web.SetSessionValue('BrowseExchangeUnits_LocatorPic','@s8')
  Of upper('xch:Manufacturer')
    loc:SortHeader = p_web.Translate('Manufacturer')
    p_web.SetSessionValue('BrowseExchangeUnits_LocatorPic','@s30')
  Of upper('xch:Model_Number')
    loc:SortHeader = p_web.Translate('Model Number')
    p_web.SetSessionValue('BrowseExchangeUnits_LocatorPic','@s30')
  Of upper('xch:MSN')
    loc:SortHeader = p_web.Translate('M.S.N.')
    p_web.SetSessionValue('BrowseExchangeUnits_LocatorPic','@s30')
  Of upper('xch:Colour')
    loc:SortHeader = p_web.Translate('Colour')
    p_web.SetSessionValue('BrowseExchangeUnits_LocatorPic','@s30')
  End
  If loc:selecting = 1
    p_web.GetSettings(p_web.GetSessionValue('Push1'))
    loc:selectaction = p_web.FormSettings.ParentPage !p_web.GetSessionValue('BrowseExchangeUnits:LookupFrom')
  End!Else
  do SendPacket
  do SetFormAction
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseExchangeUnits:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseExchangeUnits:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseExchangeUnits:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="EXCHANGE"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="xch:Ref_Number_Key"></input><13,10>'
  end
  case p_web.GetValue('refresh') ; of 'next' orof 'previous' orof 'first' orof 'last'
    p_web.Script('try{{BrowseExchangeUnits.restoreFocus();} catch(err){{};')
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseExchangeUnits',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      case loc:locatortype
      of Net:Position
      orof Net:Date
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
      of Net:Contains
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
      of Net:Begins
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
      End
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
      Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
      Case Loc:LocatorType
      of Net:Date
        packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator2BrowseExchangeUnits','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('BrowseExchangeUnits_LocatorPic'),,,'onchange="BrowseExchangeUnits.locate(''Locator2BrowseExchangeUnits'',this.value);"',,0,,,) & '</div></td>'
      Else
        packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator2BrowseExchangeUnits',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="BrowseExchangeUnits.locate(''Locator2BrowseExchangeUnits'',this.value);" ',,,,,,) & '</td>'
      End
      If loc:LocatorSearchButton or loc:LocatorClearButton
        packet = clip(packet) & '<td><div id="BrowseExchangeUnits_locate_a" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
        If loc:LocatorSearchButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
        End
        If loc:LocatorClearButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseExchangeUnits.cl(''BrowseExchangeUnits'');')
        End
        packet = clip(packet) & '</div><13,10></td><13,10>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
      If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'BrowseExchangeUnits_locate_a','buttonset',loc:options)
      End
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & p_web.DivHeader('BrowseExchangeUnits_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'','sortable')&'" id="BrowseExchangeUnits_tbl">'
  Else
    packet = clip(packet) & p_web.DivHeader('BrowseExchangeUnits_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'')&'" id="BrowseExchangeUnits_tbl">'
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'1','BrowseExchangeUnits',p_web.Translate('I.M.E.I. Number'),'Click here to sort by I.M.E.I.',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'3','BrowseExchangeUnits',p_web.Translate('Unit Number'),'Click here to sort by Unit Number',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'7','BrowseExchangeUnits',p_web.Translate('Manufacturer'),'Click here to sort by Manufacturer',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'4','BrowseExchangeUnits',p_web.Translate('Model Number'),'Click here to sort by Model Number',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'5','BrowseExchangeUnits',p_web.Translate('M.S.N.'),'Click here to sort by M.S.N.',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'6','BrowseExchangeUnits',p_web.Translate('Colour'),'Click here to sort by Colour',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
      If loc:Selecting = 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'2','BrowseExchangeUnits',p_web.Translate(),,,,,,,0,loc:Sorting,'Button')
          do AddPacket
          loc:columns += 1
      End ! Selecting
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,12,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  If Instring('xch:ref_number',lower(loc:vorder),1,1) = 0 !and EXCHANGE{prop:SQLDriver} = 1
    loc:vorder = Choose(loc:vorder='','xch:Ref_Number',clip(loc:vorder) & ',' & 'xch:Ref_Number')
  End
  Loc:Selected = Choose(p_web.IfExistsValue('xch:Ref_Number'),p_web.GetValue('xch:Ref_Number'),p_web.GetSessionValue('xch:Ref_Number'))
  ThisView{prop:order} = p_web.CleanFilter(ThisView,clip(loc:vorder))
  If False  ! Generate Filter
  ElsIf (p_web.GSV('tmp:ExchangeStockType') <> '' AND p_web.GSV('tmp:ExchangeManufacturer') <> '' AND p_web.GSV('tmp:ExchangeModelNumber') <> '')
      loc:FilterWas = 'Upper(xch:Location) = Upper(''' & p_web.GSV('BookingSiteLocation') & ''') AND Upper(xch:Stock_Type) = Upper(''' & p_web.GSV('tmp:ExchangeStockType') & ''') AND Upper(xch:Available) = ''AVL'' AND Upper(xch:Model_Number) = Upper(''' & p_web.GSV('tmp:ExchangeModelNumber') & ''')'
  ElsIf (p_web.GSV('tmp:ExchangeStockType') <> '' AND (p_web.GSV('tmp:ExchangeModelNumber') = '' OR p_web.GSV('tmp:ExchangeManufacturer') = ''))
      loc:FilterWas = 'Upper(xch:Location) = Upper(''' & p_web.GSV('BookingSiteLocation') & ''') AND Upper(xch:Stock_Type) = Upper(''' & p_web.GSV('tmp:ExchangeStockType') & ''') AND Upper(xch:Available) = ''AVL'''
  ElsIf (p_web.GSV('tmp:ExchangeStockType') = '' AND p_web.GSV('tmp:ExchangeManufacturer') <> '' AND p_web.GSV('tmp:ExchangeModelNumber') <> '')
      loc:FilterWas = 'Upper(xch:Available) = ''AVL'' AND Upper(xch:Location) = Upper(''' & p_web.GSV('BookingSiteLocation') & ''') AND Upper(xch:Model_Number) = Upper(''' & p_web.GSV('tmp:ExchangeModelNumber') & ''')'
  Else
        loc:FilterWas = 'Upper(xch:Available) = ''AVL'' AND Upper(xch:Location) = Upper(''' & p_web.GSV('BookingSiteLocation') & ''')'
  End
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseExchangeUnits',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseExchangeUnits_Filter')
    p_web.SetSessionValue('BrowseExchangeUnits_FirstValue','')
    p_web.SetSessionValue('BrowseExchangeUnits_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,EXCHANGE,xch:Ref_Number_Key,loc:PageRows,'BrowseExchangeUnits',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,loc:ViewOptions,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled,Loc:LocatorCase)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or Loc:LocatorType = Net:Date or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position or loc:LocatorType = Net:Date
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If EXCHANGE{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(EXCHANGE,loc:firstvalue)
              Reset(ThisView,EXCHANGE)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If EXCHANGE{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(EXCHANGE,loc:lastvalue)
            Reset(ThisView,EXCHANGE)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(xch:Ref_Number)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td class="'&p_web.combine(p_web.site.style.BrowseEmpty,)&'">'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:NavButtonPosition=Net:Above or (loc:NavButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseExchangeUnits_nav_a" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseExchangeUnits.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseExchangeUnits.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseExchangeUnits.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseExchangeUnits.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseExchangeUnits_nav_a','buttonset',loc:options)
          End
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:UpdateButtonPosition=Net:Above or (loc:UpdateButtonPosition=Net:Both and loc:found > 0))
    If loc:selecting = 1 !and loc:parent = ''
      if loc:popup
        packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:BrowseCancelButton,'BrowseExchangeUnits',,,loc:popup)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
      end
      TableQueue.Kind = Net:BeforeTable
      do AddPacket
    End
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseExchangeUnits',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseExchangeUnits_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseExchangeUnits_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          case loc:locatortype
          of Net:Position
          orof Net:Date
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
          of Net:Contains
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
          of Net:Begins
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
          End
          packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
          Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
          Case Loc:LocatorType
          of Net:Date
            packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator1BrowseExchangeUnits','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('BrowseExchangeUnits_LocatorPic'),,,'onchange="BrowseExchangeUnits.locate(''Locator1BrowseExchangeUnits'',this.value);"',,0,,,) & '</div></td>'
          Else
            packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator1BrowseExchangeUnits',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="BrowseExchangeUnits.locate(''Locator1BrowseExchangeUnits'',this.value);" ',,,,,,) & '</td>'
          End
          If loc:LocatorSearchButton or loc:LocatorClearButton
            packet = clip(packet) & '<td><div id="BrowseExchangeUnits_locate_b" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
            If loc:LocatorSearchButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
            End
            If loc:LocatorClearButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseExchangeUnits.cl(''BrowseExchangeUnits'');')
            End
            packet = clip(packet) & '</div><13,10></td><13,10>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
          If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseExchangeUnits_locate_b','buttonset',loc:options)
          End
      End
    End
  End
  p_web.SetSessionValue('BrowseExchangeUnits_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseExchangeUnits_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:NavButtonPosition=Net:Below or loc:NavButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseExchangeUnits_nav_b" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseExchangeUnits.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseExchangeUnits.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseExchangeUnits.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseExchangeUnits.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseExchangeUnits_nav_b','buttonset',loc:options)
          End
        end
        do SendPacket
  End
  If loc:UpdateButtonPosition=Net:Below or loc:UpdateButtonPosition=Net:Both
  If loc:selecting = 1 !and loc:parent = ''
    if loc:popup
      packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:BrowseCancelButton,'BrowseExchangeUnits',,,loc:popup)
    else
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
    end
    do SendPacket
  End
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
  data
loc:rowclick  string(1024)
  code
    loc:field = p_web.AddBrowseValue('BrowseExchangeUnits','EXCHANGE',xch:Ref_Number_Key) !xch:Ref_Number
    p_web._thisrow = p_web._nocolon('xch:Ref_Number')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if Loc:LocatorValue <> '' and loc:ActualSelection = ''
        loc:checked = 'checked'
        do SetSelection
      elsif loc:ActualSelection = '' and xch:Ref_Number = p_web.GetValue('xch:Ref_Number')
         loc:checked = 'checked'
         do SetSelection
      elsif loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseExchangeUnits:LookupField')) = xch:Ref_Number and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      else
        loc:checked = Choose((xch:Ref_Number = loc:selected) and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      end
      If(loc:SelectionMethod  = Net:Radio)
        loc:RowStyle = 'class="' & p_web.combine(p_web.site.style.browserow,) &'"'
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'data-nt-id="'& p_web.AddBrowseValue('BrowseExchangeUnits','EXCHANGE',xch:Ref_Number_Key) &'" onclick="'
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If EXCHANGE{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(EXCHANGE)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            loc:DefaultSelection = loc:field
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If EXCHANGE{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(EXCHANGE)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            loc:DefaultSelection = loc:field
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'>'&p_web.CreateInput('radio','_bidv_',clip(loc:field),,loc:checked,'')&'</td>'&CRLF
          If loc:DefaultSelection = ''
            loc:DefaultSelection = loc:field
          End
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          If loc:DefaultSelection = '' or loc:direction < 0
            loc:DefaultSelection = loc:field
          End
        End
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::xch:ESN
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::xch:Ref_Number
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::xch:Manufacturer
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::xch:Model_Number
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::xch:MSN
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::xch:Colour
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        If Loc:Selecting = 1
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::Select
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        End     !Selecting
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr ' & clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Idx = p_web.AddBrowseValue('BrowseExchangeUnits','EXCHANGE',xch:Ref_Number_Key)
  TableQueue.Id[1] = xch:Ref_Number

ClosingScripts  Routine
  data
Rtn_SecwinProcedureName         string(252)
  code
  If p_web.RequestAjax = 0
    do SetFormAction
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var btiBrowseExchangeUnits;if (btiBrowseExchangeUnits != 1){{var BrowseExchangeUnits=new browseTable(''BrowseExchangeUnits'','''&clip(loc:formname)&|
      ''','''&p_web._jsok('xch:Ref_Number',Net:Parameter)&''',2,'''&clip(loc:divname)&''',1,1,1,'''&|
      clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&|
      p_web.Translate('Are you sure you want to delete this record?')&''','''&clip(loc:ActualSelection)&''','''&clip(loc:selectaction)&''','''&|
      clip(loc:formactiontarget)&''','''','&loc:popup&',0,'''','''',"' & clip(Rtn_SecwinProcedureName) & '");<13,10>'&|
      'BrowseExchangeUnits.setGreenBar('''&p_web.ColorWeb(p_web.Site.Style.BrowseHighlightColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOneColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseTwoColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOverColor)&''');<13,10>' &|
      'BrowseExchangeUnits.applyGreenBar();btiBrowseExchangeUnits=1};<13,10>' &|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseExchangeUnits')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseExchangeUnits')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseExchangeUnits')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseExchangeUnits')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(EXCHANGE)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(EXCHANGE)
  Bind(xch:Record)
  Clear(xch:Record)
  NetWebSetSessionPics(p_web,EXCHANGE)

Children Routine
  if loc:selecting = 0
    If p_web.RequestAjax = 0
      do StartChildren
    Else
      do AjaxChildren
    End
  end

AjaxChildren  Routine
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue('xch:Ref_Number',p_web.GetValue('xch:Ref_Number'))
   p_web.DeleteValue('_Clicked')
   loc:found = 1
   do Children
! ----------------------------------------------------------------------------------------
CallRow  Routine
  data
loc:result  long
  code
  do OpenFilesB
  xch:Ref_Number = p_web.GSV('xch:Ref_Number')
  loc:result = p_web._GetFile(EXCHANGE,xch:Ref_Number_Key)
  loc:eip = 1
  loc:viewstate = p_web.escape(p_web.Base64Encode(clip(xch:Ref_Number)))
  do BrowseRow
  do SendPacket
  do ClosefilesB

! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(EXCHANGE)
  Case upper(p_web.GetValue('_EIPClm'))
  Else
    case p_web.GetValue('_action_')
    of Net:InsertRecord
    orof Net:ChangeRecord
    orof Net:DeleteRecord
    orof Net:CopyRecord
      loc:eip = 0
      do CallBrowse
    end
  End
  do GotFocusBack
  p_web._CloseFile(EXCHANGE)
! ----------------------------------------------------------------------------------------
value::xch:ESN   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseExchangeUnits_xch:ESN_'&xch:Ref_Number,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(xch:ESN,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::xch:Ref_Number   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseExchangeUnits_xch:Ref_Number_'&xch:Ref_Number,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(xch:Ref_Number,'@s8')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::xch:Manufacturer   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseExchangeUnits_xch:Manufacturer_'&xch:Ref_Number,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(xch:Manufacturer,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::xch:Model_Number   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseExchangeUnits_xch:Model_Number_'&xch:Ref_Number,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(xch:Model_Number,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::xch:MSN   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseExchangeUnits_xch:MSN_'&xch:Ref_Number,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(xch:MSN,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::xch:Colour   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseExchangeUnits_xch:Colour_'&xch:Ref_Number,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(xch:Colour,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::Select   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseExchangeUnits_Select_'&xch:Ref_Number,,net:crc,,loc:extra)
        packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'BrowseExchangeUnits',p_web.AddBrowseValue('BrowseExchangeUnits','EXCHANGE',xch:Ref_Number_Key),,loc:popup)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END
  return
!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
CheckForDuplicate  Routine
SetSelection  Routine
  loc:ActualSelection = loc:field
  p_web.SetSessionValue('xch:Ref_Number',xch:Ref_Number)

MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:ActualSelection = ''
    loc:ActualSelection = loc:DefaultSelection
    p_web.GetBrowseValue(loc:ActualSelection,Net:Web:SessionQueue) ! so children are primed with correct sessionValue
  End

  If loc:ActualSelection <> ''
    TableQueue.Kind = Net:RowTable
    get(TableQueue,TableQueue.Kind)
    if Errorcode() = 0
      TableQueue.Row = sub(TableQueue.Row,1,len(clip(TableQueue.Row))-1) &  ' data-nt-rows="'&loc:RowsHigh&'" data-nt-value="'&clip(loc:ActualSelection)&'"' & '>'
      Put(TableQueue)
    End
  End

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead class="'&p_web.combine(p_web.site.style.BrowseHeader,'')&'"><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot class="'&p_web.combine(p_web.site.style.BrowseFooter,)&'"><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody class="'&p_web.combine(p_web.site.style.BrowseBody,)&'"><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table><13,10>' & p_web.DivFooter(Net:NoSend) ! Table Div
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
FormExchangeUnitFilter PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
tmp:ExchangeStockType STRING(30)                           !
tmp:ExchangeModelNumber STRING(30)                         !
tmp:ExchangeManufacturer STRING(30)                        !
FilesOpened     Long
STOCKTYP::State  USHORT
MODELNUM::State  USHORT
MANUFACT::State  USHORT
tmp:ExchangeStockType:IsInvalid  Long
tmp:ExchangeManufacturer:IsInvalid  Long
tmp:ExchangeModelNumber:IsInvalid  Long
BrowseExchangeUnits:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
tmp:ExchangeStockType_OptionView   View(STOCKTYP)
                          Project(stp:Stock_Type)
                        End
tmp:ExchangeManufacturer_OptionView   View(MANUFACT)
                          Project(man:Manufacturer)
                        End
tmp:ExchangeModelNumber_OptionView   View(MODELNUM)
                          Project(mod:Model_Number)
                        End
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormExchangeUnitFilter')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'FormExchangeUnitFilter_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('FormExchangeUnitFilter','')
    p_web.DivHeader('FormExchangeUnitFilter',p_web.combine(p_web.site.style.formdiv,))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('FormExchangeUnitFilter') = 0
        p_web.AddPreCall('FormExchangeUnitFilter')
        p_web.DivHeader('popup_FormExchangeUnitFilter','nt-hidden')
        p_web.DivHeader('FormExchangeUnitFilter',p_web.combine(p_web.site.style.formdiv,))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_FormExchangeUnitFilter_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_FormExchangeUnitFilter_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormExchangeUnitFilter',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_FormExchangeUnitFilter',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormExchangeUnitFilter',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_FormExchangeUnitFilter',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormExchangeUnitFilter',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormExchangeUnitFilter',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_FormExchangeUnitFilter',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormExchangeUnitFilter',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_FormExchangeUnitFilter',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormExchangeUnitFilter',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormExchangeUnitFilter',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormExchangeUnitFilter',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('FormExchangeUnitFilter')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(STOCKTYP)
  p_web._OpenFile(MODELNUM)
  p_web._OpenFile(MANUFACT)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(STOCKTYP)
  p_Web._CloseFile(MODELNUM)
  p_Web._CloseFile(MANUFACT)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE
  loc:EipClm = upper(p_web.GetValue('_EIPClm'))
  p_web.DeleteValue('_EIPClm')
  case loc:EipClm
  of ''
    case upper(p_web.GetValue('_calledfrom_'))
    of upper('BrowseExchangeUnits')
      do Value::BrowseExchangeUnits
    end
  end

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FormExchangeUnitFilter_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'FormExchangeUnitFilter'
    end
    p_web.formsettings.proc = 'FormExchangeUnitFilter'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'tmp:ExchangeModelNumber'
    p_web.setsessionvalue('showtab_FormExchangeUnitFilter',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(MODELNUM)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.')
  End
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  if p_web.IfExistsValue('tmp:ExchangeStockType') = 0
    p_web.SetSessionValue('tmp:ExchangeStockType',tmp:ExchangeStockType)
  Else
    tmp:ExchangeStockType = p_web.GetSessionValue('tmp:ExchangeStockType')
  End
  if p_web.IfExistsValue('tmp:ExchangeManufacturer') = 0
    p_web.SetSessionValue('tmp:ExchangeManufacturer',tmp:ExchangeManufacturer)
  Else
    tmp:ExchangeManufacturer = p_web.GetSessionValue('tmp:ExchangeManufacturer')
  End
  if p_web.IfExistsValue('tmp:ExchangeModelNumber') = 0
    p_web.SetSessionValue('tmp:ExchangeModelNumber',tmp:ExchangeModelNumber)
  Else
    tmp:ExchangeModelNumber = p_web.GetSessionValue('tmp:ExchangeModelNumber')
  End

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('tmp:ExchangeStockType')
    tmp:ExchangeStockType = p_web.GetValue('tmp:ExchangeStockType')
    p_web.SetSessionValue('tmp:ExchangeStockType',tmp:ExchangeStockType)
  Else
    tmp:ExchangeStockType = p_web.GetSessionValue('tmp:ExchangeStockType')
  End
  if p_web.IfExistsValue('tmp:ExchangeManufacturer')
    tmp:ExchangeManufacturer = p_web.GetValue('tmp:ExchangeManufacturer')
    p_web.SetSessionValue('tmp:ExchangeManufacturer',tmp:ExchangeManufacturer)
  Else
    tmp:ExchangeManufacturer = p_web.GetSessionValue('tmp:ExchangeManufacturer')
  End
  if p_web.IfExistsValue('tmp:ExchangeModelNumber')
    tmp:ExchangeModelNumber = p_web.GetValue('tmp:ExchangeModelNumber')
    p_web.SetSessionValue('tmp:ExchangeModelNumber',tmp:ExchangeModelNumber)
  Else
    tmp:ExchangeModelNumber = p_web.GetSessionValue('tmp:ExchangeModelNumber')
  End

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('FormExchangeUnitFilter_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferFormExchangeUnitFilter')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormExchangeUnitFilter_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormExchangeUnitFilter_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormExchangeUnitFilter_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction

GenerateForm   Routine
  do LoadRelatedRecords
 tmp:ExchangeStockType = p_web.RestoreValue('tmp:ExchangeStockType')
 tmp:ExchangeManufacturer = p_web.RestoreValue('tmp:ExchangeManufacturer')
 tmp:ExchangeModelNumber = p_web.RestoreValue('tmp:ExchangeModelNumber')
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Browse Exchange Units') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Browse Exchange Units',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_FormExchangeUnitFilter',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_FormExchangeUnitFilter0_div')&'">'&p_web.Translate('Filter Exchange Units')&'</a></li>'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_FormExchangeUnitFilter1_div')&'">'&p_web.Translate('Available Exchange Units')&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  do GenerateTab1
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="FormExchangeUnitFilter_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      packet = clip(packet) & '</div><13,10>' ! end id="FormExchangeUnitFilter_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'FormExchangeUnitFilter_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="FormExchangeUnitFilter_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'FormExchangeUnitFilter_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:ExchangeStockType')
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_FormExchangeUnitFilter')>0,p_web.GSV('showtab_FormExchangeUnitFilter'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_FormExchangeUnitFilter'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_FormExchangeUnitFilter') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_FormExchangeUnitFilter'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_FormExchangeUnitFilter')>0,p_web.GSV('showtab_FormExchangeUnitFilter'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_FormExchangeUnitFilter') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Filter Exchange Units') & ''''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Available Exchange Units') & ''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_FormExchangeUnitFilter_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_FormExchangeUnitFilter')>0,p_web.GSV('showtab_FormExchangeUnitFilter'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"FormExchangeUnitFilter",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_FormExchangeUnitFilter')>0,p_web.GSV('showtab_FormExchangeUnitFilter'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_FormExchangeUnitFilter_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('FormExchangeUnitFilter') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('FormExchangeUnitFilter')
    p_web.SetValue('_CallPopups',2)
    If p_web.GetPreCall('BrowseExchangeUnits') = 0
      p_web.SetValue('BrowseExchangeUnits:NoForm',1)
      p_web.SetValue('BrowseExchangeUnits:FormName',loc:formname)
      p_web.SetValue('BrowseExchangeUnits:parentIs','Form')
      p_web.SetValue('_parentProc','FormExchangeUnitFilter')
      BrowseExchangeUnits(p_web)
      p_web.SetValue('_CallPopups',0)
      p_web.DeleteValue('BrowseExchangeUnits:NoForm')
      p_web.DeleteValue('BrowseExchangeUnits:FormName')
      p_web.DeleteValue('BrowseExchangeUnits:parentIs')
      p_web.DeleteValue('_parentProc')
    End
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Filter Exchange Units')&'</a></h3>' & CRLF & p_web.DivHeader('tab_FormExchangeUnitFilter0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_FormExchangeUnitFilter0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_FormExchangeUnitFilter0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_FormExchangeUnitFilter0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Filter Exchange Units')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_FormExchangeUnitFilter0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Filter Exchange Units')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_FormExchangeUnitFilter0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Filter Exchange Units')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_FormExchangeUnitFilter0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:ExchangeStockType
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&290&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:ExchangeStockType
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:ExchangeStockType
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:ExchangeManufacturer
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&290&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:ExchangeManufacturer
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:ExchangeManufacturer
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:ExchangeModelNumber
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&290&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:ExchangeModelNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:ExchangeModelNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab1  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Available Exchange Units')&'</a></h3>' & CRLF & p_web.DivHeader('tab_FormExchangeUnitFilter1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_FormExchangeUnitFilter1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_FormExchangeUnitFilter1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_FormExchangeUnitFilter1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Available Exchange Units')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_FormExchangeUnitFilter1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Available Exchange Units')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_FormExchangeUnitFilter1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Available Exchange Units')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_FormExchangeUnitFilter1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::BrowseExchangeUnits
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Prompt::tmp:ExchangeStockType  Routine
  packet = clip(packet) & p_web.DivHeader('FormExchangeUnitFilter_' & p_web._nocolon('tmp:ExchangeStockType') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Stock Type'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:ExchangeStockType  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:ExchangeStockType = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:ExchangeStockType = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:ExchangeStockType  ! copies value to session value if valid.
  do Value::tmp:ExchangeStockType
  do SendAlert
  do Comment::tmp:ExchangeStockType ! allows comment style to be updated.
  do Value::BrowseExchangeUnits  !1
  do Value::tmp:ExchangeManufacturer  !1
  do Value::tmp:ExchangeModelNumber  !1

ValidateValue::tmp:ExchangeStockType  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:ExchangeStockType',tmp:ExchangeStockType).
    End

Value::tmp:ExchangeStockType  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormExchangeUnitFilter_' & p_web._nocolon('tmp:ExchangeStockType') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,p_web.site.style.formselect,,)
  If loc:retrying
    tmp:ExchangeStockType = p_web.RestoreValue('tmp:ExchangeStockType')
    do ValidateValue::tmp:ExchangeStockType
    If tmp:ExchangeStockType:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- DROPLIST ---
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ExchangeStockType'',''formexchangeunitfilter_tmp:exchangestocktype_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:ExchangeStockType')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('tmp:AllStockTypes') = 1,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:ExchangeStockType',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
  loc:fieldclass = ''
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('tmp:ExchangeStockType') = 0
    p_web.SetSessionValue('tmp:ExchangeStockType','')
  end
    packet = clip(packet) & p_web.CreateOption('== All Stock Types ==','',choose('' = p_web.getsessionvalue('tmp:ExchangeStockType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(STOCKTYP)
  bind(stp:Record)
  p_web._OpenFile(MODELNUM)
  bind(mod:Record)
  p_web._OpenFile(MANUFACT)
  bind(man:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(tmp:ExchangeStockType_OptionView)
  tmp:ExchangeStockType_OptionView{prop:filter} = p_web.CleanFilter(tmp:ExchangeStockType_OptionView,'stp:Available = 1 AND Upper(stp:Use_Exchange) = ''YES''')
  tmp:ExchangeStockType_OptionView{prop:order} = p_web.CleanFilter(tmp:ExchangeStockType_OptionView,'UPPER(stp:Stock_Type)')
  Set(tmp:ExchangeStockType_OptionView)
  Loop
    Next(tmp:ExchangeStockType_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('tmp:ExchangeStockType') = 0
      p_web.SetSessionValue('tmp:ExchangeStockType',stp:Stock_Type)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,stp:Stock_Type,choose(stp:Stock_Type = p_web.getsessionvalue('tmp:ExchangeStockType')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(tmp:ExchangeStockType_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(STOCKTYP)
  p_Web._CloseFile(MODELNUM)
  p_Web._CloseFile(MANUFACT)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:ExchangeStockType  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:ExchangeStockType:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormExchangeUnitFilter_' & p_web._nocolon('tmp:ExchangeStockType') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:ExchangeManufacturer  Routine
  packet = clip(packet) & p_web.DivHeader('FormExchangeUnitFilter_' & p_web._nocolon('tmp:ExchangeManufacturer') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Manufacturer'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:ExchangeManufacturer  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:ExchangeManufacturer = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:ExchangeManufacturer = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:ExchangeManufacturer  ! copies value to session value if valid.
  do Value::tmp:ExchangeManufacturer
  do SendAlert
  do Comment::tmp:ExchangeManufacturer ! allows comment style to be updated.
  do Value::tmp:ExchangeModelNumber  !1
  do Value::BrowseExchangeUnits  !1

ValidateValue::tmp:ExchangeManufacturer  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:ExchangeManufacturer',tmp:ExchangeManufacturer).
    End

Value::tmp:ExchangeManufacturer  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormExchangeUnitFilter_' & p_web._nocolon('tmp:ExchangeManufacturer') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,p_web.site.style.formselect,,)
  If loc:retrying
    tmp:ExchangeManufacturer = p_web.RestoreValue('tmp:ExchangeManufacturer')
    do ValidateValue::tmp:ExchangeManufacturer
    If tmp:ExchangeManufacturer:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- DROPLIST ---
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ExchangeManufacturer'',''formexchangeunitfilter_tmp:exchangemanufacturer_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:ExchangeManufacturer')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:ExchangeManufacturer',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
  loc:fieldclass = ''
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('tmp:ExchangeManufacturer') = 0
    p_web.SetSessionValue('tmp:ExchangeManufacturer','')
  end
    packet = clip(packet) & p_web.CreateOption('== All Manufacturers ==','',choose('' = p_web.getsessionvalue('tmp:ExchangeManufacturer')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(STOCKTYP)
  bind(stp:Record)
  p_web._OpenFile(MODELNUM)
  bind(mod:Record)
  p_web._OpenFile(MANUFACT)
  bind(man:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(tmp:ExchangeManufacturer_OptionView)
  tmp:ExchangeManufacturer_OptionView{prop:order} = p_web.CleanFilter(tmp:ExchangeManufacturer_OptionView,'UPPER(man:Manufacturer)')
  Set(tmp:ExchangeManufacturer_OptionView)
  Loop
    Next(tmp:ExchangeManufacturer_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('tmp:ExchangeManufacturer') = 0
      p_web.SetSessionValue('tmp:ExchangeManufacturer',man:Manufacturer)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,man:Manufacturer,choose(man:Manufacturer = p_web.getsessionvalue('tmp:ExchangeManufacturer')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(tmp:ExchangeManufacturer_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(STOCKTYP)
  p_Web._CloseFile(MODELNUM)
  p_Web._CloseFile(MANUFACT)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:ExchangeManufacturer  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:ExchangeManufacturer:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormExchangeUnitFilter_' & p_web._nocolon('tmp:ExchangeManufacturer') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:ExchangeModelNumber  Routine
  packet = clip(packet) & p_web.DivHeader('FormExchangeUnitFilter_' & p_web._nocolon('tmp:ExchangeModelNumber') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Model Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:ExchangeModelNumber  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:ExchangeModelNumber = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:ExchangeModelNumber = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:ExchangeModelNumber  ! copies value to session value if valid.
  do Value::tmp:ExchangeModelNumber
  do SendAlert
  do Comment::tmp:ExchangeModelNumber ! allows comment style to be updated.
  do Value::BrowseExchangeUnits  !1

ValidateValue::tmp:ExchangeModelNumber  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:ExchangeModelNumber',tmp:ExchangeModelNumber).
    End

Value::tmp:ExchangeModelNumber  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormExchangeUnitFilter_' & p_web._nocolon('tmp:ExchangeModelNumber') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,p_web.site.style.formselect,,)
  If loc:retrying
    tmp:ExchangeModelNumber = p_web.RestoreValue('tmp:ExchangeModelNumber')
    do ValidateValue::tmp:ExchangeModelNumber
    If tmp:ExchangeModelNumber:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- DROPLIST ---
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ExchangeModelNumber'',''formexchangeunitfilter_tmp:exchangemodelnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:ExchangeModelNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:ExchangeModelNumber',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
  loc:fieldclass = ''
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('tmp:ExchangeModelNumber') = 0
    p_web.SetSessionValue('tmp:ExchangeModelNumber','')
  end
    packet = clip(packet) & p_web.CreateOption('== All Model Numbers ==','',choose('' = p_web.getsessionvalue('tmp:ExchangeModelNumber')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(STOCKTYP)
  bind(stp:Record)
  p_web._OpenFile(MODELNUM)
  bind(mod:Record)
  p_web._OpenFile(MANUFACT)
  bind(man:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(tmp:ExchangeModelNumber_OptionView)
  tmp:ExchangeModelNumber_OptionView{prop:filter} = p_web.CleanFilter(tmp:ExchangeModelNumber_OptionView,'Upper(mod:Manufacturer ) = Upper(''' & p_web.GSV('tmp:ExchangeManufacturer') & ''')')
  tmp:ExchangeModelNumber_OptionView{prop:order} = p_web.CleanFilter(tmp:ExchangeModelNumber_OptionView,'UPPER(mod:Model_Number)')
  Set(tmp:ExchangeModelNumber_OptionView)
  Loop
    Next(tmp:ExchangeModelNumber_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('tmp:ExchangeModelNumber') = 0
      p_web.SetSessionValue('tmp:ExchangeModelNumber',mod:Model_Number)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,mod:Model_Number,choose(mod:Model_Number = p_web.getsessionvalue('tmp:ExchangeModelNumber')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(tmp:ExchangeModelNumber_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(STOCKTYP)
  p_Web._CloseFile(MODELNUM)
  p_Web._CloseFile(MANUFACT)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:ExchangeModelNumber  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:ExchangeModelNumber:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormExchangeUnitFilter_' & p_web._nocolon('tmp:ExchangeModelNumber') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::BrowseExchangeUnits  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  Elsif true
    p_web.StoreValue('xch:Ref_Number')
  End
  do ValidateValue::BrowseExchangeUnits  ! copies value to session value if valid.
  do Comment::BrowseExchangeUnits ! allows comment style to be updated.

ValidateValue::BrowseExchangeUnits  Routine
    If not (1=0)
    End

Value::BrowseExchangeUnits  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(1=0,1,0))
  ! --- BROWSE ---  BrowseExchangeUnits --
  p_web.SetValue('BrowseExchangeUnits:NoForm',1)
  p_web.SetValue('BrowseExchangeUnits:FormName',loc:formname)
  p_web.SetValue('BrowseExchangeUnits:parentIs','Form')
  p_web.SetValue('_parentProc','FormExchangeUnitFilter')
  if p_web.RequestAjax = 0
    p_web.SSV('FormExchangeUnitFilter:_popup_',p_web.GetValue('_popup_')) ! stores the current browse popup state
    packet = clip(packet) & '<div id="'&lower('FormExchangeUnitFilter_BrowseExchangeUnits_embedded_div')&'"><!-- Net:BrowseExchangeUnits --></div><13,10>'
    do SendPacket
    p_web.DivHeader('FormExchangeUnitFilter_' & lower('BrowseExchangeUnits') & '_value')
    p_web.DivFooter()
  else
    if p_web.GetValue('_popup_') = 1
      p_web.SSV('FormExchangeUnitFilter:_popup_',1)
    elsif p_web.GSV('FormExchangeUnitFilter:_popup_') = 1
      p_web.SetValue('_popup_',1)
    end
    packet = clip(packet) & '<!-- Net:BrowseExchangeUnits --><13,10>'
  end
  do SendPacket
Comment::BrowseExchangeUnits  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if BrowseExchangeUnits:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormExchangeUnitFilter_' & p_web._nocolon('BrowseExchangeUnits') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('FormExchangeUnitFilter_nexttab_' & 0)
    tmp:ExchangeStockType = p_web.GSV('tmp:ExchangeStockType')
    do ValidateValue::tmp:ExchangeStockType
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:ExchangeStockType
      !do SendAlert
      do Comment::tmp:ExchangeStockType ! allows comment style to be updated.
      !exit
    End
    tmp:ExchangeManufacturer = p_web.GSV('tmp:ExchangeManufacturer')
    do ValidateValue::tmp:ExchangeManufacturer
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:ExchangeManufacturer
      !do SendAlert
      do Comment::tmp:ExchangeManufacturer ! allows comment style to be updated.
      !exit
    End
    tmp:ExchangeModelNumber = p_web.GSV('tmp:ExchangeModelNumber')
    do ValidateValue::tmp:ExchangeModelNumber
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:ExchangeModelNumber
      !do SendAlert
      do Comment::tmp:ExchangeModelNumber ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('FormExchangeUnitFilter_nexttab_' & 1)
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_FormExchangeUnitFilter_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('FormExchangeUnitFilter_tab_' & 0)
    do GenerateTab0
  of lower('FormExchangeUnitFilter_tmp:ExchangeStockType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ExchangeStockType
      of event:timer
        do Value::tmp:ExchangeStockType
        do Comment::tmp:ExchangeStockType
      else
        do Value::tmp:ExchangeStockType
      end
  of lower('FormExchangeUnitFilter_tmp:ExchangeManufacturer_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ExchangeManufacturer
      of event:timer
        do Value::tmp:ExchangeManufacturer
        do Comment::tmp:ExchangeManufacturer
      else
        do Value::tmp:ExchangeManufacturer
      end
  of lower('FormExchangeUnitFilter_tmp:ExchangeModelNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ExchangeModelNumber
      of event:timer
        do Value::tmp:ExchangeModelNumber
        do Comment::tmp:ExchangeModelNumber
      else
        do Value::tmp:ExchangeModelNumber
      end
  of lower('FormExchangeUnitFilter_tab_' & 1)
    do GenerateTab1
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('FormExchangeUnitFilter_form:ready_',1)

  p_web.SetSessionValue('FormExchangeUnitFilter_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_FormExchangeUnitFilter',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('FormExchangeUnitFilter_form:ready_',1)
  p_web.SetSessionValue('FormExchangeUnitFilter_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormExchangeUnitFilter',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('FormExchangeUnitFilter_form:ready_',1)
  p_web.SetSessionValue('FormExchangeUnitFilter_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('FormExchangeUnitFilter:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('FormExchangeUnitFilter_form:ready_',1)
  p_web.SetSessionValue('FormExchangeUnitFilter_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('FormExchangeUnitFilter:Primed',0)
  p_web.setsessionvalue('showtab_FormExchangeUnitFilter',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
      If not (1=0)
        If not (p_web.GSV('tmp:AllStockTypes') = 1)
          If p_web.IfExistsValue('tmp:ExchangeStockType')
            tmp:ExchangeStockType = p_web.GetValue('tmp:ExchangeStockType')
          End
        End
      End
      If not (1=0)
          If p_web.IfExistsValue('tmp:ExchangeManufacturer')
            tmp:ExchangeManufacturer = p_web.GetValue('tmp:ExchangeManufacturer')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('tmp:ExchangeModelNumber')
            tmp:ExchangeModelNumber = p_web.GetValue('tmp:ExchangeModelNumber')
          End
      End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormExchangeUnitFilter_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormExchangeUnitFilter_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
    do ValidateValue::tmp:ExchangeStockType
    If loc:Invalid then exit.
    do ValidateValue::tmp:ExchangeManufacturer
    If loc:Invalid then exit.
    do ValidateValue::tmp:ExchangeModelNumber
    If loc:Invalid then exit.
  ! tab = 2
    loc:InvalidTab += 1
    do ValidateValue::BrowseExchangeUnits
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('FormExchangeUnitFilter:Primed',0)
  p_web.StoreValue('tmp:ExchangeStockType')
  p_web.StoreValue('tmp:ExchangeManufacturer')
  p_web.StoreValue('tmp:ExchangeModelNumber')
  p_web.StoreValue('')

PickEngineersNotes   PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locPickList          STRING(10000)                         !
locFinalList         STRING(10000)                         !
locFinalListField    STRING(10000)                         !
locFoundField        STRING(10000)                         !
FilesOpened     Long
JOBNOTES::State  USHORT
NOTESENG::State  USHORT
title:IsInvalid  Long
locPickList:IsInvalid  Long
locFinalList:IsInvalid  Long
button:AddSelected:IsInvalid  Long
button:RemoveSelected:IsInvalid  Long
gap:IsInvalid  Long
button:RemoveAll:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  GlobalErrors.SetProcedureName('PickEngineersNotes')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'PickEngineersNotes_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('PickEngineersNotes','')
    p_web.DivHeader('PickEngineersNotes',p_web.combine(p_web.site.style.formdiv,))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('PickEngineersNotes') = 0
        p_web.AddPreCall('PickEngineersNotes')
        p_web.DivHeader('popup_PickEngineersNotes','nt-hidden')
        p_web.DivHeader('PickEngineersNotes',p_web.combine(p_web.site.style.formdiv,))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(850)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_PickEngineersNotes_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_PickEngineersNotes_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferPickEngineersNotes',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_PickEngineersNotes',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPickEngineersNotes',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_PickEngineersNotes',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPickEngineersNotes',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_PickEngineersNotes',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_PickEngineersNotes',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPickEngineersNotes',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_PickEngineersNotes',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPickEngineersNotes',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_PickEngineersNotes',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_PickEngineersNotes',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('PickEngineersNotes')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
clearVariables      ROUTINE
    p_web.DeleteSessionValue('locPickList')
    p_web.DeleteSessionValue('locFinalList')
    p_web.DeleteSessionValue('locFinalListField')
    p_web.DeleteSessionValue('locFoundField')
OpenFiles  ROUTINE
  p_web._OpenFile(JOBNOTES)
  p_web._OpenFile(NOTESENG)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(JOBNOTES)
  p_Web._CloseFile(NOTESENG)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('PickEngineersNotes_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'PickEngineersNotes'
    end
    p_web.formsettings.proc = 'PickEngineersNotes'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine
  do clearVariables

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  if p_web.IfExistsValue('locPickList') = 0
    p_web.SetSessionValue('locPickList',locPickList)
  Else
    locPickList = p_web.GetSessionValue('locPickList')
  End
  if p_web.IfExistsValue('locFinalList') = 0
    p_web.SetSessionValue('locFinalList',locFinalList)
  Else
    locFinalList = p_web.GetSessionValue('locFinalList')
  End

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locPickList')
    locPickList = p_web.GetValue('locPickList')
    p_web.SetSessionValue('locPickList',locPickList)
  Else
    locPickList = p_web.GetSessionValue('locPickList')
  End
  if p_web.IfExistsValue('locFinalList')
    locFinalList = p_web.GetValue('locFinalList')
    p_web.SetSessionValue('locFinalList',locFinalList)
  Else
    locFinalList = p_web.GetSessionValue('locFinalList')
  End

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('PickEngineersNotes_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'ViewJob'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('PickEngineersNotes_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('PickEngineersNotes_ChainTo')
    loc:formaction = p_web.GetSessionValue('PickEngineersNotes_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'ViewJob'

GenerateForm   Routine
  do LoadRelatedRecords
 locPickList = p_web.RestoreValue('locPickList')
 locFinalList = p_web.RestoreValue('locFinalList')
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  loc:FormHeading = p_web.Translate('Engineers Notes',0)
  If loc:FormHeading
    if loc:popup
      packet = clip(packet) & p_web.jQuery('#popup_'&lower('PickEngineersNotes')&'_div','dialog','"option","title",'&p_web.jsString(loc:FormHeading,0),,0)
    else
      packet = clip(packet) & lower('<div id="form-access-PickEngineersNotes"></div>')
      packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formheading,)&'">'&clip(loc:FormHeading)&'</div>'&CRLF
    end
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_PickEngineersNotes',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_PickEngineersNotes0_div')&'">'&p_web.Translate()&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="PickEngineersNotes_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.cancel();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CancelButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="PickEngineersNotes_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'PickEngineersNotes_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="PickEngineersNotes_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'PickEngineersNotes_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locPickList')
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_PickEngineersNotes')>0,p_web.GSV('showtab_PickEngineersNotes'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_PickEngineersNotes'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_PickEngineersNotes') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_PickEngineersNotes'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_PickEngineersNotes')>0,p_web.GSV('showtab_PickEngineersNotes'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_PickEngineersNotes') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_PickEngineersNotes_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_PickEngineersNotes')>0,p_web.GSV('showtab_PickEngineersNotes'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"PickEngineersNotes",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_PickEngineersNotes')>0,p_web.GSV('showtab_PickEngineersNotes'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_PickEngineersNotes_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('PickEngineersNotes') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('PickEngineersNotes')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate()&'</a></h3>' & CRLF & p_web.DivHeader('tab_PickEngineersNotes0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_PickEngineersNotes0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_PickEngineersNotes0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_PickEngineersNotes0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,)
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_PickEngineersNotes0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend)
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_PickEngineersNotes0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain">' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_PickEngineersNotes0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::title
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locPickList
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locPickList
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locFinalList
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locFinalList
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::button:AddSelected
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::button:AddSelected
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::button:RemoveSelected
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::button:RemoveSelected
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::gap
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::button:RemoveAll
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Validate::title  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::title  ! copies value to session value if valid.

ValidateValue::title  Routine
    If not (1=0)
    End

Value::title  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickEngineersNotes_' & p_web._nocolon('title') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="title" class="'&clip('green bold')&'"'&clip(loc:extra)&'>' & p_web.Translate('Hold "CTRL" or "SHIFT" to select more than one entry.',) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::locPickList  Routine
  packet = clip(packet) & p_web.DivHeader('PickEngineersNotes_' & p_web._nocolon('locPickList') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,'hidden','hidden')),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locPickList  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locPickList = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locPickList = p_web.GetValue('Value')
  End
  do ValidateValue::locPickList  ! copies value to session value if valid.
  do Value::locFinalList  !1

ValidateValue::locPickList  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locPickList',locPickList).
    End

Value::locPickList  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickEngineersNotes_' & p_web._nocolon('locPickList') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,p_web.site.style.formselect,,)
  If loc:retrying
    locPickList = p_web.RestoreValue('locPickList')
    do ValidateValue::locPickList
    If locPickList:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- DROPLIST ---
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locPickList'',''pickengineersnotes_locpicklist_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locPickList')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('locPickList',loc:fieldclass,loc:readonly,30,420,1,loc:javascript,)
  loc:fieldclass = ''
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('locPickList') = 0
    p_web.SetSessionValue('locPickList','')
  end
    packet = clip(packet) & p_web.CreateOption('---- Select Engineers Note -----','',choose('' = p_web.getsessionvalue('locPickList')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
      access:noteseng.clearkey(noe:Notes_Key)
      noe:Reference = ''
      SET(noe:Notes_Key,noe:Notes_Key)
      LOOP
          IF (access:noteseng.next())
              BREAK
          END
          If Instring(';' & Clip(noe:notes),p_web.GSV('locFinalListField'),1,1)
              Cycle
          End ! If Instring(';' & Clip(acr:Accessory) & ';',p_web.GSV('locFinalListField'),1,1)
  
          loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
          packet = clip(packet) & p_web.CreateOption(CLIP(noe:Reference) & ' - ' & Clip(noe:Notes),noe:Notes,choose(noe:Notes = p_web.GSV('locPickList')),clip(loc:rowstyle),,)&CRLF
          loc:even = Choose(loc:even=1,2,1)
      END
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web.DivFooter()

Prompt::locFinalList  Routine
  packet = clip(packet) & p_web.DivHeader('PickEngineersNotes_' & p_web._nocolon('locFinalList') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,'hidden','hidden')),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locFinalList  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locFinalList = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locFinalList = p_web.GetValue('Value')
  End
  do ValidateValue::locFinalList  ! copies value to session value if valid.
  do Value::locFinalList
  do SendAlert
  do Value::locPickList  !1

ValidateValue::locFinalList  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locFinalList',locFinalList).
    End

Value::locFinalList  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickEngineersNotes_' & p_web._nocolon('locFinalList') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,p_web.site.style.formselect,,)
  If loc:retrying
    locFinalList = p_web.RestoreValue('locFinalList')
    do ValidateValue::locFinalList
    If locFinalList:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- DROPLIST ---
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locFinalList'',''pickengineersnotes_locfinallist_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locFinalList')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('locFinalList',loc:fieldclass,loc:readonly,30,350,,loc:javascript,)
  loc:fieldclass = ''
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('locFinalList') = 0
    p_web.SetSessionValue('locFinalList','')
  end
    packet = clip(packet) & p_web.CreateOption('--- Selected Engineers Note ----','',choose('' = p_web.getsessionvalue('locFinalList')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
      If p_web.GSV('locFinalListField') <> ''
          Loop x# = 1 To 10000
              If Sub(p_web.GSV('locFinalListField'),x#,2) = '|;'
                  Start# = x# + 2
                  Cycle
              End ! If Sub(p_web.GSV('locFinalListField'),x#,2) = '|;'
  
              If Start# > 0
                  If Sub(p_web.GSV('locFinalListField'),x#,2) = ';|'
                      locFoundField = Sub(p_web.GSV('locFinalListField'),Start#,x# - Start#)
  
                      If locFoundField <> ''
                          loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
                          packet = clip(packet) & p_web.CreateOption(CLIP(locFoundField),locFoundField,choose(locFoundField = p_web.GSV('locFinalList')),clip(loc:rowstyle),,)&CRLF
                          loc:even = Choose(loc:even=1,2,1)
                      End ! If tmp:FoundAccessory <> ''
                      Start# = 0
                  End ! If Sub(p_web.GSV('locFinalListField'),x#,2) = ';|'
              End ! If Start# > 0
          End ! Loop x# = 1 To 1000
  
          If Start# > 0
              locFoundField = Clip(Sub(p_web.GSV('locFinalListField'),Start#,30))
              If locFoundField <> ''
                  loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
                  packet = clip(packet) & p_web.CreateOption(CLIP(locFoundField),locFoundField,choose(locFoundField = p_web.GSV('locFinalList')),clip(loc:rowstyle),,)&CRLF
                  loc:even = Choose(loc:even=1,2,1)
              End ! If tmp:FoundAccessory <> ''
          End ! If Start# > 0
      End ! If p_web.GSV('locFinalListField') <> ''
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web.DivFooter()

Prompt::button:AddSelected  Routine
  packet = clip(packet) & p_web.DivHeader('PickEngineersNotes_' & p_web._nocolon('button:AddSelected') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,'hidden',)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::button:AddSelected  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::button:AddSelected  ! copies value to session value if valid.
      p_web.SSV('locFinalListField',p_web.GSV('locFinalListField') & p_web.GSV('locPickList'))
      p_web.SSV('locPickList','')
  do Value::button:AddSelected
  do Value::locFinalList  !1
  do Value::locPickList  !1

ValidateValue::button:AddSelected  Routine
    If not (1=0)
    End

Value::button:AddSelected  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickEngineersNotes_' & p_web._nocolon('button:AddSelected') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''button:AddSelected'',''pickengineersnotes_button:addselected_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','AddSelected','Add Selected',p_web.combine(Choose('Add Selected' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'SmallButtonFixed'),loc:formname,,,,loc:javascript,loc:disabled,,,,,,,,,,'nt-left')!h
  do SendPacket
  End
  p_web.DivFooter()

Prompt::button:RemoveSelected  Routine
  packet = clip(packet) & p_web.DivHeader('PickEngineersNotes_' & p_web._nocolon('button:RemoveSelected') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,'hidden',)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::button:RemoveSelected  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::button:RemoveSelected  ! copies value to session value if valid.
      locFinalListField = p_web.GSV('locFinalListField') & ';'
      locFinalList = '|;' & Clip(p_web.GSV('locFinalList')) & ';'
      Loop x# = 1 To 10000
          pos# = Instring(Clip(locFinalList),locFinalListField,1,1)
          If pos# > 0
              locFinalListField = Sub(locFinalListField,1,pos# - 1) & Sub(locFinalListField,pos# + Len(Clip(locFinalList)),10000)
              Break
          End ! If pos# > 0#
      End ! Loop x# = 1 To 1000
      p_web.SSV('locFinalListField',Sub(locFinalListField,1,Len(Clip(locFinalListField)) - 1))
      p_web.SSV('locFinalList','')
  do Value::button:RemoveSelected
  do Value::locFinalList  !1
  do Value::locPickList  !1

ValidateValue::button:RemoveSelected  Routine
    If not (1=0)
    End

Value::button:RemoveSelected  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickEngineersNotes_' & p_web._nocolon('button:RemoveSelected') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''button:RemoveSelected'',''pickengineersnotes_button:removeselected_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','DeleteSelected','Delete Selected',p_web.combine(Choose('Delete Selected' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'SmallButtonFixed'),loc:formname,,,,loc:javascript,loc:disabled,,,,,,,,,,'nt-left')!h
  do SendPacket
  End
  p_web.DivFooter()

Validate::gap  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::gap  ! copies value to session value if valid.

ValidateValue::gap  Routine
    If not (1)
    End

Value::gap  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickEngineersNotes_' & p_web._nocolon('gap') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Validate::button:RemoveAll  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::button:RemoveAll  ! copies value to session value if valid.
      p_web.SetSessionValue('locFinalListField','')
      p_web.SetSessionValue('locFinalList','')
      p_web.SetSessionValue('locPickList','')
  do Value::button:RemoveAll
  do Value::locFinalList  !1
  do Value::locPickList  !1

ValidateValue::button:RemoveAll  Routine
    If not (1=0)
    End

Value::button:RemoveAll  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickEngineersNotes_' & p_web._nocolon('button:RemoveAll') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''button:RemoveAll'',''pickengineersnotes_button:removeall_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','RemoveAll','Remove All',p_web.combine(Choose('Remove All' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'SmallButtonFixed'),loc:formname,,,,loc:javascript,loc:disabled,,,,,,,,,,'nt-left')!h
  do SendPacket
  End
  p_web.DivFooter()

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('PickEngineersNotes_nexttab_' & 0)
    locPickList = p_web.GSV('locPickList')
    do ValidateValue::locPickList
    If loc:Invalid
      loc:retrying = 1
      do Value::locPickList
      !do SendAlert
      !exit
    End
    locFinalList = p_web.GSV('locFinalList')
    do ValidateValue::locFinalList
    If loc:Invalid
      loc:retrying = 1
      do Value::locFinalList
      !do SendAlert
      !exit
    End
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_PickEngineersNotes_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('PickEngineersNotes_tab_' & 0)
    do GenerateTab0
  of lower('PickEngineersNotes_locPickList_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locPickList
      of event:timer
        do Value::locPickList
      else
        do Value::locPickList
      end
  of lower('PickEngineersNotes_locFinalList_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locFinalList
      of event:timer
        do Value::locFinalList
      else
        do Value::locFinalList
      end
  of lower('PickEngineersNotes_button:AddSelected_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::button:AddSelected
      of event:timer
        do Value::button:AddSelected
      else
        do Value::button:AddSelected
      end
  of lower('PickEngineersNotes_button:RemoveSelected_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::button:RemoveSelected
      of event:timer
        do Value::button:RemoveSelected
      else
        do Value::button:RemoveSelected
      end
  of lower('PickEngineersNotes_button:RemoveAll_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::button:RemoveAll
      of event:timer
        do Value::button:RemoveAll
      else
        do Value::button:RemoveAll
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('PickEngineersNotes_form:ready_',1)

  p_web.SetSessionValue('PickEngineersNotes_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_PickEngineersNotes',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('PickEngineersNotes_form:ready_',1)
  p_web.SetSessionValue('PickEngineersNotes_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_PickEngineersNotes',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('PickEngineersNotes_form:ready_',1)
  p_web.SetSessionValue('PickEngineersNotes_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('PickEngineersNotes:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('PickEngineersNotes_form:ready_',1)
  p_web.SetSessionValue('PickEngineersNotes_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('PickEngineersNotes:Primed',0)
  p_web.setsessionvalue('showtab_PickEngineersNotes',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
      If not (1=0)
          If p_web.IfExistsValue('locPickList')
            locPickList = p_web.GetValue('locPickList')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('locFinalList')
            locFinalList = p_web.GetValue('locFinalList')
          End
      End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('PickEngineersNotes_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
      If Clip(p_web.GSV('locFinalListField')) <> ''
          Loop x# = 1 To 10000
              If Sub(p_web.GSV('locFinalListField'),x#,2) = '|;'
                  Start# = x# + 2
                  Cycle
              End ! If Sub(p_web.GSV('locFinalListField'),x#,2) = '|;'
  
              If Start# > 0
                  If Sub(p_web.GSV('locFinalListField'),x#,2) = ';|'
                      locFoundField = Sub(p_web.GSV('locFinalListField'),Start#,x# - Start#)
  
                      If locFoundField <> ''
                          p_web.SSV('jbn:Engineers_Notes',p_web.GSV('jbn:Engineers_Notes') & '<13,10>' & |
                              CLIP(locFoundField))
                      End ! If locFoundField <> ''
                      Start# = 0
                  End ! If Sub(p_web.GSV('locFinalListField'),x#,2) = ';|'
              End ! If Start# > 0
          End ! Loop x# = 1 To 1000
  
          If Start# > 0
              locFoundField = Clip(Sub(p_web.GSV('locFinalListField'),Start#,30))
              If locFoundField <> ''
                  p_web.SSV('jbn:Engineers_Notes',p_web.GSV('jbn:Engineers_Notes') & '<13,10>' & |
                      CLIP(locFoundField))
              End ! If locFoundField <> ''
          End ! If Start# > 0
      End ! If Clip(p_web.GSV('locFinalListField') <> ''
  p_web.DeleteSessionValue('PickEngineersNotes_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
    do ValidateValue::title
    If loc:Invalid then exit.
    do ValidateValue::locPickList
    If loc:Invalid then exit.
    do ValidateValue::locFinalList
    If loc:Invalid then exit.
    do ValidateValue::button:AddSelected
    If loc:Invalid then exit.
    do ValidateValue::button:RemoveSelected
    If loc:Invalid then exit.
    do ValidateValue::gap
    If loc:Invalid then exit.
    do ValidateValue::button:RemoveAll
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
  do clearVariables
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('PickEngineersNotes:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('locPickList')
  p_web.StoreValue('locFinalList')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')

ProofOfPurchase      PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
tmp:DOP              DATE                                  !Date Of Purchase
tmp:JobType          STRING(1)                             !
tmp:POPType          STRING(20)                            !
tmp:WarrantyRefNo    STRING(30)                            !
tmp:POP              STRING(1)                             !
locPOPTypePassword   STRING(30)                            !
FilesOpened     Long
ACCAREAS::State  USHORT
USERS::State  USHORT
MANUFACT::State  USHORT
tmp:DOP:IsInvalid  Long
tmp:POP:IsInvalid  Long
locPOPTypePassword:IsInvalid  Long
tmp:POPType:IsInvalid  Long
tmp:WarrantyRefNo:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('ProofOfPurchase')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'ProofOfPurchase_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('ProofOfPurchase','')
    p_web.DivHeader('ProofOfPurchase',p_web.combine(p_web.site.style.formdiv,'fdiv'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('ProofOfPurchase') = 0
        p_web.AddPreCall('ProofOfPurchase')
        p_web.DivHeader('popup_ProofOfPurchase','nt-hidden')
        p_web.DivHeader('ProofOfPurchase',p_web.combine(p_web.site.style.formdiv,'fdiv'))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_ProofOfPurchase_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_ProofOfPurchase_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferProofOfPurchase',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_ProofOfPurchase',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferProofOfPurchase',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_ProofOfPurchase',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferProofOfPurchase',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_ProofOfPurchase',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_ProofOfPurchase',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferProofOfPurchase',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_ProofOfPurchase',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferProofOfPurchase',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_ProofOfPurchase',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_ProofOfPurchase',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('ProofOfPurchase')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(ACCAREAS)
  p_web._OpenFile(USERS)
  p_web._OpenFile(MANUFACT)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(ACCAREAS)
  p_Web._CloseFile(USERS)
  p_Web._CloseFile(MANUFACT)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  ! Reset Variables
  p_web.SSV('tmp:DOP','')
  p_web.SSV('tmp:JobType','')
  p_web.SSV('tmp:POPType','')
  p_web.SSV('tmp:WarrantyRefNo','')
  p_web.SSV('Comment:DOP','')
  p_web.SSV('Comment:POPTypePassword',kCommentPOPTypePassword)
  p_web.SSV('ReadOnly:POPType',1)
  p_web.SSV('locPOPTypePassword','')
  p_web.SetValue('ProofOfPurchase_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'ProofOfPurchase'
    end
    p_web.formsettings.proc = 'ProofOfPurchase'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('tmp:DOP')
    p_web.SetPicture('tmp:DOP','@d06b')
  End
  p_web.SetSessionPicture('tmp:DOP','@d06b')

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  if p_web.IfExistsValue('tmp:DOP') = 0
    p_web.SetSessionValue('tmp:DOP',tmp:DOP)
  Else
    tmp:DOP = p_web.GetSessionValue('tmp:DOP')
  End
  if p_web.IfExistsValue('tmp:POP') = 0
    p_web.SetSessionValue('tmp:POP',tmp:POP)
  Else
    tmp:POP = p_web.GetSessionValue('tmp:POP')
  End
  if p_web.IfExistsValue('locPOPTypePassword') = 0
    p_web.SetSessionValue('locPOPTypePassword',locPOPTypePassword)
  Else
    locPOPTypePassword = p_web.GetSessionValue('locPOPTypePassword')
  End
  if p_web.IfExistsValue('tmp:POPType') = 0
    p_web.SetSessionValue('tmp:POPType',tmp:POPType)
  Else
    tmp:POPType = p_web.GetSessionValue('tmp:POPType')
  End
  if p_web.IfExistsValue('tmp:WarrantyRefNo') = 0
    p_web.SetSessionValue('tmp:WarrantyRefNo',tmp:WarrantyRefNo)
  Else
    tmp:WarrantyRefNo = p_web.GetSessionValue('tmp:WarrantyRefNo')
  End

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('tmp:DOP')
    tmp:DOP = p_web.dformat(clip(p_web.GetValue('tmp:DOP')),'@d06b')
    p_web.SetSessionValue('tmp:DOP',tmp:DOP)
  Else
    tmp:DOP = p_web.GetSessionValue('tmp:DOP')
  End
  if p_web.IfExistsValue('tmp:POP')
    tmp:POP = p_web.GetValue('tmp:POP')
    p_web.SetSessionValue('tmp:POP',tmp:POP)
  Else
    tmp:POP = p_web.GetSessionValue('tmp:POP')
  End
  if p_web.IfExistsValue('locPOPTypePassword')
    locPOPTypePassword = p_web.GetValue('locPOPTypePassword')
    p_web.SetSessionValue('locPOPTypePassword',locPOPTypePassword)
  Else
    locPOPTypePassword = p_web.GetSessionValue('locPOPTypePassword')
  End
  if p_web.IfExistsValue('tmp:POPType')
    tmp:POPType = p_web.GetValue('tmp:POPType')
    p_web.SetSessionValue('tmp:POPType',tmp:POPType)
  Else
    tmp:POPType = p_web.GetSessionValue('tmp:POPType')
  End
  if p_web.IfExistsValue('tmp:WarrantyRefNo')
    tmp:WarrantyRefNo = p_web.GetValue('tmp:WarrantyRefNo')
    p_web.SetSessionValue('tmp:WarrantyRefNo',tmp:WarrantyRefNo)
  Else
    tmp:WarrantyRefNo = p_web.GetSessionValue('tmp:WarrantyRefNo')
  End

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('ProofOfPurchase_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'BillingConfirmation'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('ProofOfPurchase_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('ProofOfPurchase_ChainTo')
    loc:formaction = p_web.GetSessionValue('ProofOfPurchase_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'BillingConfirmation'

GenerateForm   Routine
  do LoadRelatedRecords
  p_web.SSV('tmp:WarrantyRefNo',p_web.GSV('jobe2:WarrantyRefNo'))
  
  p_web.SSV('tmp:DOP',p_web.GSV('job:DOP'))
  p_web.SSV('tmp:POP',p_web.GSV('job:POP'))
  p_web.SSV('tmp:POPType',p_web.GSV('jobe:POPType'))
 tmp:DOP = p_web.RestoreValue('tmp:DOP')
 tmp:POP = p_web.RestoreValue('tmp:POP')
 locPOPTypePassword = p_web.RestoreValue('locPOPTypePassword')
 tmp:POPType = p_web.RestoreValue('tmp:POPType')
 tmp:WarrantyRefNo = p_web.RestoreValue('tmp:WarrantyRefNo')
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Proof Of Purchase') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Proof Of Purchase',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_ProofOfPurchase',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_ProofOfPurchase0_div')&'">'&p_web.Translate('Details')&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="ProofOfPurchase_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.cancel();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CancelButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="ProofOfPurchase_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'ProofOfPurchase_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="ProofOfPurchase_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'ProofOfPurchase_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:DOP')
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_ProofOfPurchase')>0,p_web.GSV('showtab_ProofOfPurchase'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_ProofOfPurchase'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_ProofOfPurchase') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_ProofOfPurchase'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_ProofOfPurchase')>0,p_web.GSV('showtab_ProofOfPurchase'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_ProofOfPurchase') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Details') & ''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_ProofOfPurchase_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_ProofOfPurchase')>0,p_web.GSV('showtab_ProofOfPurchase'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"ProofOfPurchase",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_ProofOfPurchase')>0,p_web.GSV('showtab_ProofOfPurchase'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_ProofOfPurchase_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('ProofOfPurchase') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('ProofOfPurchase')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Details')&'</a></h3>' & CRLF & p_web.DivHeader('tab_ProofOfPurchase0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_ProofOfPurchase0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_ProofOfPurchase0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_ProofOfPurchase0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Details')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_ProofOfPurchase0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Details')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_ProofOfPurchase0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Details')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_ProofOfPurchase0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:DOP
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:DOP
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:DOP
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:POP
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:POP
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:POP
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locPOPTypePassword
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locPOPTypePassword
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locPOPTypePassword
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:POPType
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:POPType
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:POPType
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&180&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:WarrantyRefNo
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&300&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:WarrantyRefNo
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::tmp:WarrantyRefNo
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Prompt::tmp:DOP  Routine
  packet = clip(packet) & p_web.DivHeader('ProofOfPurchase_' & p_web._nocolon('tmp:DOP') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Activation / D.O.P.'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:DOP  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:DOP = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@d06b'  !FieldPicture = 
    tmp:DOP = p_web.Dformat(p_web.GetValue('Value'),'@d06b')
  End
  do ValidateValue::tmp:DOP  ! copies value to session value if valid.
  if (p_web.GSV('tmp:DOP') > today())
      p_web.SSV('comment:DOP','Invalid Date')
      p_web.SSV('tmp:DOP','')
  else !
      p_web.SSV('comment:DOP','')
  end ! if (p_web.GSV('tmp:DOP') > today())
  
  if (p_web.GSV('tmp:POP') = '' and p_web.GSV('tmp:DOP') <> '')
  
      Access:MANUFACT.Clearkey(man:Manufacturer_Key)
      man:Manufacturer    = p_web.GSV('job:Manufacturer')
      if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
          ! Found
      else ! if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
          ! Error
      end ! if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
      if (p_web.GSV('tmp:DOP') < (p_web.GSV('job:Date_Booked') - man:Warranty_Period))
          if (p_web.GSV('tmp:DOP') < (p_web.GSV('job:Date_Booked') - 730))
              p_web.SSV('tmp:POP','C')
          else ! if (p_web.GSV('tmp:DOP') < (p_web.GSV('job:Date_Booked') - 730))
              p_web.SSV('tmp:POP','S')
              ! DBH #11344 - Force Chargeable If Man/Model is One Year Warranty Only
              IF (IsOneYearWarranty(p_web.GSV('job:Manufacturer'),p_web.GSV('job:Model_Number')))
                  p_web.SSV('tmp:POP','C')
              END
          end ! if (p_web.GSV('tmp:DOP') < (p_web.GSV('job:Date_Booked') - 730))
      else ! if (p_web.GSV('tmp:DOP') < (p_web.GSV('job:Date_Booked') - man:Warranty_Period)
          p_web.SSV('tmp:POP','F')
      end ! if (p_web.GSV('tmp:DOP') < (p_web.GSV('job:Date_Booked') - man:Warranty_Period)
  end ! if (p_web.GSV('tmp:POP') = '')
  do Value::tmp:DOP
  do SendAlert
  do Comment::tmp:DOP ! allows comment style to be updated.
  do Comment::tmp:DOP
  do Value::tmp:POP  !1
  do Comment::tmp:POP

ValidateValue::tmp:DOP  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:DOP',tmp:DOP).
    End

Value::tmp:DOP  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ProofOfPurchase_' & p_web._nocolon('tmp:DOP') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    tmp:DOP = p_web.RestoreValue('tmp:DOP')
    do ValidateValue::tmp:DOP
    If tmp:DOP:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- tmp:DOP
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:DOP'',''proofofpurchase_tmp:dop_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:DOP')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:DOP',p_web.GetSessionValue('tmp:DOP'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@d06b',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:DOP  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:DOP:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:DOP'))
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ProofOfPurchase_' & p_web._nocolon('tmp:DOP') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:POP  Routine
  packet = clip(packet) & p_web.DivHeader('ProofOfPurchase_' & p_web._nocolon('tmp:POP') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Job Type'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:POP  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:POP = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:POP = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:POP  ! copies value to session value if valid.
  do Value::tmp:POP
  do SendAlert
  do Comment::tmp:POP ! allows comment style to be updated.

ValidateValue::tmp:POP  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:POP',tmp:POP).
    End

Value::tmp:POP  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ProofOfPurchase_' & p_web._nocolon('tmp:POP') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',' noButton')
  If loc:retrying
    tmp:POP = p_web.RestoreValue('tmp:POP')
    do ValidateValue::tmp:POP
    If tmp:POP:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- RADIO --- tmp:POP
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:POP') = 'F'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:POP'',''proofofpurchase_tmp:pop_value'',1,'''&p_web._jsok('F')&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:POP',clip('F'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:POP_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('First Year Warranty') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:POP') = 'S'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:POP'',''proofofpurchase_tmp:pop_value'',1,'''&p_web._jsok('S')&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:POP',clip('S'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:POP_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Second Year Warranty') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:POP') = 'O'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:POP'',''proofofpurchase_tmp:pop_value'',1,'''&p_web._jsok('O')&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:POP',clip('O'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:POP_3') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Out Of Box Failure') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:POP') = 'C'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:POP'',''proofofpurchase_tmp:pop_value'',1,'''&p_web._jsok('C')&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:POP',clip('C'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:POP_4') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Chargeable') & '<13,10>'
    packet = clip(packet) & p_web.br
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:POP  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:POP:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ProofOfPurchase_' & p_web._nocolon('tmp:POP') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locPOPTypePassword  Routine
  packet = clip(packet) & p_web.DivHeader('ProofOfPurchase_' & p_web._nocolon('locPOPTypePassword') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Enter Password'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locPOPTypePassword  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locPOPTypePassword = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locPOPTypePassword = p_web.GetValue('Value')
  End
  do ValidateValue::locPOPTypePassword  ! copies value to session value if valid.
  p_web.SSV('ReadOnly:POPType',1)
  p_web.SSV('Comment:POPTypePassword',kCommentPOPTypePassword)
  Access:USERS.ClearKey(use:password_key)
  use:Password = p_web.GSV('locPOPTypePassword')
  IF (Access:USERS.TryFetch(use:password_key) = Level:Benign)
      Access:ACCAREAS.ClearKey(acc:Access_level_key)
      acc:User_Level = use:User_Level
      acc:Access_Area = 'AMEND POP TYPE'
      IF (Access:ACCAREAS.TryFetch(acc:Access_level_key) = Level:Benign)
          p_web.SSV('ReadOnly:POPType',0)
  
      ELSE
          p_web.SSV('Comment:POPTypePassword','User Does Not Have Access')
      END
  ELSE
      p_web.SSV('Comment:POPTypePassword','Invalid Password')
  END
  do Value::locPOPTypePassword
  do SendAlert
  do Comment::locPOPTypePassword ! allows comment style to be updated.
  do Value::tmp:POPType  !1
  do Comment::locPOPTypePassword

ValidateValue::locPOPTypePassword  Routine
    If not (1=0)
    locPOPTypePassword = Upper(locPOPTypePassword)
      if loc:invalid = '' then p_web.SetSessionValue('locPOPTypePassword',locPOPTypePassword).
    End

Value::locPOPTypePassword  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ProofOfPurchase_' & p_web._nocolon('locPOPTypePassword') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    locPOPTypePassword = p_web.RestoreValue('locPOPTypePassword')
    do ValidateValue::locPOPTypePassword
    If locPOPTypePassword:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- locPOPTypePassword
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locPOPTypePassword'',''proofofpurchase_locpoptypepassword_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locPOPTypePassword')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('password','locPOPTypePassword',p_web.GetSessionValueFormat('locPOPTypePassword'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,'Enter Password And Press [TAB]',,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locPOPTypePassword  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locPOPTypePassword:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:POPTypePassword'))
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ProofOfPurchase_' & p_web._nocolon('locPOPTypePassword') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:POPType  Routine
  packet = clip(packet) & p_web.DivHeader('ProofOfPurchase_' & p_web._nocolon('tmp:POPType') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('POP Type'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:POPType  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:POPType = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:POPType = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:POPType  ! copies value to session value if valid.
  do Value::tmp:POPType
  do SendAlert
  do Comment::tmp:POPType ! allows comment style to be updated.

ValidateValue::tmp:POPType  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:POPType',tmp:POPType).
    End

Value::tmp:POPType  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ProofOfPurchase_' & p_web._nocolon('tmp:POPType') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,p_web.site.style.formselect,'FormEntry',)
  If loc:retrying
    tmp:POPType = p_web.RestoreValue('tmp:POPType')
    do ValidateValue::tmp:POPType
    If tmp:POPType:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- DROPLIST ---
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:POPType'',''proofofpurchase_tmp:poptype_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('ReadOnly:POPType'),'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:POPType',loc:fieldclass,loc:readonly,,,,loc:javascript,)
  loc:fieldclass = ''
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('tmp:POPType') = 0
    p_web.SetSessionValue('tmp:POPType','')
  end
    packet = clip(packet) & p_web.CreateOption('------------------','',choose('' = p_web.getsessionvalue('tmp:POPType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('POP','POP',choose('POP' = p_web.getsessionvalue('tmp:POPType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('BASTION','BASTION',choose('BASTION' = p_web.getsessionvalue('tmp:POPType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('NONE','NONE',choose('NONE' = p_web.getsessionvalue('tmp:POPType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:POPType  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:POPType:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ProofOfPurchase_' & p_web._nocolon('tmp:POPType') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::tmp:WarrantyRefNo  Routine
  packet = clip(packet) & p_web.DivHeader('ProofOfPurchase_' & p_web._nocolon('tmp:WarrantyRefNo') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Warranty Ref No'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:WarrantyRefNo  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:WarrantyRefNo = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:WarrantyRefNo = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:WarrantyRefNo  ! copies value to session value if valid.
  do Value::tmp:WarrantyRefNo
  do SendAlert
  do Comment::tmp:WarrantyRefNo ! allows comment style to be updated.

ValidateValue::tmp:WarrantyRefNo  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:WarrantyRefNo',tmp:WarrantyRefNo).
    End

Value::tmp:WarrantyRefNo  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('ProofOfPurchase_' & p_web._nocolon('tmp:WarrantyRefNo') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    tmp:WarrantyRefNo = p_web.RestoreValue('tmp:WarrantyRefNo')
    do ValidateValue::tmp:WarrantyRefNo
    If tmp:WarrantyRefNo:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- tmp:WarrantyRefNo
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:WarrantyRefNo'',''proofofpurchase_tmp:warrantyrefno_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:WarrantyRefNo',p_web.GetSessionValueFormat('tmp:WarrantyRefNo'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::tmp:WarrantyRefNo  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if tmp:WarrantyRefNo:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('ProofOfPurchase_' & p_web._nocolon('tmp:WarrantyRefNo') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('ProofOfPurchase_nexttab_' & 0)
    tmp:DOP = p_web.GSV('tmp:DOP')
    do ValidateValue::tmp:DOP
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:DOP
      !do SendAlert
      do Comment::tmp:DOP ! allows comment style to be updated.
      !exit
    End
    tmp:POP = p_web.GSV('tmp:POP')
    do ValidateValue::tmp:POP
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:POP
      !do SendAlert
      do Comment::tmp:POP ! allows comment style to be updated.
      !exit
    End
    locPOPTypePassword = p_web.GSV('locPOPTypePassword')
    do ValidateValue::locPOPTypePassword
    If loc:Invalid
      loc:retrying = 1
      do Value::locPOPTypePassword
      !do SendAlert
      do Comment::locPOPTypePassword ! allows comment style to be updated.
      !exit
    End
    tmp:POPType = p_web.GSV('tmp:POPType')
    do ValidateValue::tmp:POPType
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:POPType
      !do SendAlert
      do Comment::tmp:POPType ! allows comment style to be updated.
      !exit
    End
    tmp:WarrantyRefNo = p_web.GSV('tmp:WarrantyRefNo')
    do ValidateValue::tmp:WarrantyRefNo
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:WarrantyRefNo
      !do SendAlert
      do Comment::tmp:WarrantyRefNo ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_ProofOfPurchase_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('ProofOfPurchase_tab_' & 0)
    do GenerateTab0
  of lower('ProofOfPurchase_tmp:DOP_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:DOP
      of event:timer
        do Value::tmp:DOP
        do Comment::tmp:DOP
      else
        do Value::tmp:DOP
      end
  of lower('ProofOfPurchase_tmp:POP_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:POP
      of event:timer
        do Value::tmp:POP
        do Comment::tmp:POP
      else
        do Value::tmp:POP
      end
  of lower('ProofOfPurchase_locPOPTypePassword_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locPOPTypePassword
      of event:timer
        do Value::locPOPTypePassword
        do Comment::locPOPTypePassword
      else
        do Value::locPOPTypePassword
      end
  of lower('ProofOfPurchase_tmp:POPType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:POPType
      of event:timer
        do Value::tmp:POPType
        do Comment::tmp:POPType
      else
        do Value::tmp:POPType
      end
  of lower('ProofOfPurchase_tmp:WarrantyRefNo_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:WarrantyRefNo
      of event:timer
        do Value::tmp:WarrantyRefNo
        do Comment::tmp:WarrantyRefNo
      else
        do Value::tmp:WarrantyRefNo
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('ProofOfPurchase_form:ready_',1)

  p_web.SetSessionValue('ProofOfPurchase_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_ProofOfPurchase',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('ProofOfPurchase_form:ready_',1)
  p_web.SetSessionValue('ProofOfPurchase_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_ProofOfPurchase',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('ProofOfPurchase_form:ready_',1)
  p_web.SetSessionValue('ProofOfPurchase_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('ProofOfPurchase:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('ProofOfPurchase_form:ready_',1)
  p_web.SetSessionValue('ProofOfPurchase_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('ProofOfPurchase:Primed',0)
  p_web.setsessionvalue('showtab_ProofOfPurchase',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
      If not (1=0)
          If p_web.IfExistsValue('tmp:DOP')
            tmp:DOP = p_web.GetValue('tmp:DOP')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('tmp:POP')
            tmp:POP = p_web.GetValue('tmp:POP')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('locPOPTypePassword')
            locPOPTypePassword = p_web.GetValue('locPOPTypePassword')
          End
      End
      If not (1=0)
        If not (p_web.GSV('ReadOnly:POPType'))
          If p_web.IfExistsValue('tmp:POPType')
            tmp:POPType = p_web.GetValue('tmp:POPType')
          End
        End
      End
      If not (1=0)
          If p_web.IfExistsValue('tmp:WarrantyRefNo')
            tmp:WarrantyRefNo = p_web.GetValue('tmp:WarrantyRefNo')
          End
      End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('ProofOfPurchase_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  ! Validate
  if (p_web.GSV('tmp:POP') = '')
      loc:alert = 'You have not selected an option'
      loc:invalid = 'tmp:POP'
      exit
  end !if (p_web.GSV('tmp:POP') = '')
  
  case (p_web.GSV('tmp:POP'))
  of 'F' orof 'S' orof 'O'
      if (p_web.GSV('tmp:DOP') = '')
          loc:alert = 'Date Of Purchase Required'
          loc:invalid = 'tmp:DOP'
          exit
      end ! if (p_web.GSV('tmp:DOP') = '')
  end ! case (p_web.GSV('tmp:POP'))
  
  
  Access:MANUFACT.Clearkey(man:Manufacturer_Key)
  man:Manufacturer    = p_web.GSV('job:Manufacturer')
  if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
      ! Found
  else ! if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
      ! Error
  end ! if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
  
  if (p_web.GSV('tmp:POP') <> 'C' and man:validateDateCode)
      if (p_web.GSV('tmp:DOP') < (job:date_Booked - man:warranty_period))
          if (p_web.GSV('tmp:DOP') < (job:date_booked - 730))
              ! Out Of 2nd Year
          else ! if (p_web.GSV('tmp:DOP') < (job:date_booked - 730))
              if (p_web.GSV('tmp:POP') <> 'O')
                  p_web.SSV('tmp:POP','S')
              end ! if (p_web.GSV('tmp:POP') <> 'O')
          end ! if (p_web.GSV('tmp:DOP') < (job:date_booked - 730))
      end ! if (p_web.GSV('tmp:DOP') < (job:date_Booked - man:warranty_period))
  end ! if (p_web.GSV('tmp:POP') <> 'C' and man:validateDateCode)
  
  p_web.SSV('job:POP',p_web.GSV('tmp:POP'))
  p_web.SSV('job:DOP',p_web.GSV('tmp:DOP'))
  
  if (p_web.GSV('job:DOP') > 0)
      if (sub(p_web.GSV('job:Current_Status'),1,3) = '130')
          if (p_web.GSV('job:Engineer') = '')
              p_web.SSV('GetStatus:StatusNumber',305)
              p_web.SSV('GetStatus:Type','JOB')
          else ! if (p_web.GSV('job:Engineer') = ''
              p_web.SSV('GetStatus:StatusNumber',sub(p_web.GSV('job:previousStatus'),1,3))
              p_web.SSV('GetStatus:Type','JOB')
          end ! if (p_web.GSV('job:Engineer') = ''
          GetStatus(p_web.GSV('GetStatus:StatusNumber'),0,p_web.GSV('GetStatus:Type'),p_web)
      end ! if (sub(p_web.GSV('job:Current_Status'),1,3) = '130')
  
      case p_web.GSV('job:POP')
      of 'F'
          p_web.SSV('job:Warranty_job','YES')
          p_web.SSV('job:Warranty_Charge_Type','WARRANTY (MFTR)')
      of 'S'
          p_web.SSV('job:Warranty_job','YES')
          p_web.SSV('job:Warranty_Charge_Type','WARRANTY (2ND YR)')
      of 'O'
          p_web.SSV('job:Warranty_job','YES')
          p_web.SSV('job:Warranty_Charge_Type','WARRANTY (OBF)')
      of 'C'
          p_web.SSV('job:Warranty_job','NO')
          p_web.SSV('job:Chargeable_job','YES')
          p_web.SSV('job:Warranty_Charge_Type','')
          p_web.SSV('job:Charge_Type','NON-WARRANTY')
      end ! case p_web.GSV('job:POP')
  end ! if (p_web.GSV('job:DOP') > 0)
  
  p_web.SSV('jobe:POPType',p_web.GSV('tmp:POPType')) ! #11912 Never saved this field. (Bryan: 14/02/2011)
  
  
  
  p_web.DeleteSessionValue('ProofOfPurchase_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
    do ValidateValue::tmp:DOP
    If loc:Invalid then exit.
    do ValidateValue::tmp:POP
    If loc:Invalid then exit.
    do ValidateValue::locPOPTypePassword
    If loc:Invalid then exit.
    do ValidateValue::tmp:POPType
    If loc:Invalid then exit.
    do ValidateValue::tmp:WarrantyRefNo
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('ProofOfPurchase:Primed',0)
  p_web.StoreValue('tmp:DOP')
  p_web.StoreValue('tmp:POP')
  p_web.StoreValue('locPOPTypePassword')
  p_web.StoreValue('tmp:POPType')
  p_web.StoreValue('tmp:WarrantyRefNo')

