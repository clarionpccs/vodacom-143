

   MEMBER('WebServer_Phase4.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSE092.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSE018.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE021.INC'),ONCE        !Req'd for module callout resolution
                     END


brwStockAllocation   PROCEDURE  (NetWebServerWorker p_web)
locEngineerName      STRING(60)                            !
locStockQuantity     LONG                                  !
locStockAvailable    BYTE                                  !
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256),dim(Net:MaxKeyFields)
idx           String(Net:HashSize)
sub           Long
            End
stl:JobNumber:IsInvalid  Long
stl:ShelfLocation:IsInvalid  Long
stl:Description:IsInvalid  Long
stl:PartNumber:IsInvalid  Long
locEngineerName:IsInvalid  Long
stl:Quantity:IsInvalid  Long
locStockQuantity:IsInvalid  Long
icnStockAvailable:IsInvalid  Long
icnStatus:IsInvalid  Long
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:DefaultSelection    String(Net:HashSize)
loc:ActualSelection     String(Net:HashSize)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
!loc:pagename            String(256)
loc:NavButtonPosition    Long
loc:UpdateButtonPosition Long
loc:SelectionMethod      Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorCase         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
loc:lookupdone          Long
loc:FormPopup           Long
ThisView            View(STOCKALL)
                      Project(stl:RecordNumber)
                      Project(stl:JobNumber)
                      Project(stl:ShelfLocation)
                      Project(stl:Description)
                      Project(stl:PartNumber)
                      Project(stl:Quantity)
                      Project(stl:PartRecordNumber)
                      Project(stl:PartType)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
Loc:popup             Long
loc:poppedup          Long,static,thread
FilesOpened     Long
STOCK::State  USHORT
USERS::State  USHORT
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return 
  End
  GlobalErrors.SetProcedureName('brwStockAllocation')
  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('brwStockAllocation:NoForm')
      loc:NoForm = p_web.GetValue('brwStockAllocation:NoForm')
      loc:FormName = p_web.GetValue('brwStockAllocation:FormName')
    else
      loc:FormName = 'brwStockAllocation_frm'
    End
    p_web.SSV('brwStockAllocation:NoForm',loc:NoForm)
    p_web.SSV('brwStockAllocation:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('brwStockAllocation:NoForm')
    loc:FormName = p_web.GSV('brwStockAllocation:FormName')
  end
  case p_web.site.formpopups
  of -1 ; loc:FormPopup = Net:Page
  of 0 ; loc:FormPopup = Net:Page
  of 1 ; loc:FormPopup = Net:Popup
  End
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('brwStockAllocation') & '_' & lower(loc:parent)
  else
    loc:divname = lower('brwStockAllocation')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if 1 = 2
  elsif p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  elsif p_web.IfExistsValue('_EIPRow_')
    do CallRow
  elsif p_web.GetValue('_CallPopups') > 0
    do CallPopups
  elsif p_web.IfExistsValue('_GetSecwinSettings')
      p_web.SetValue('Secwin_' & 'brwStockAllocation' & 'AccessGroupsArray','1') 
      p_web.SetValue('Secwin_AccessWindowName','brwStockAllocation')
      
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallPopups  Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
loc:CallPopups   Long
loc:name         String(255)
  code
  if loc:poppedup = 0
    loc:CallPopups = p_web.GetValue('_CallPopups')
    if loc:CallPopups = 1 !and p_web.GetPreCall('brwStockAllocation') = 0! for = 1 include popup divs and scripts for this browse
      !p_web.AddPreCall('brwStockAllocation')
      p_web.DivHeader('popup_brwStockAllocation','nt-hidden')
      p_web.DivHeader('brwStockAllocation',p_web.combine(p_web.site.style.browsediv,))
      p_web.DivFooter()
      p_web.DivFooter()
      loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(400)&', modal: true, position: [''center'',15]'
      packet =  p_web.jQuery('#' & lower('popup_brwStockAllocation_div'),'dialog',loc:options) & |
      p_web.jQuery('#' & lower('popup_brwStockAllocation_div'),'removeClass','''nt-hidden''')
    End
    ! for > 0 add in form, EIP-lookups and browse object
    do Popups
    loc:popup = 1
    do ClosingScripts
    do SendPacket
    loc:poppedup = 1
  end

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web.DivHeader('brwStockAllocation',p_web.combine(p_web.site.style.browsediv,))
  if loc:ParentSilent = 0
    do GenerateBrowse
    if p_web.RequestAjax = 1
      p_web.Script('BrowseProducts.pcv();')
    end
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web.DivFooter()
  do Children
  do Popups
  if loc:poppedup = 0
    do ClosingScripts
  end
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(STOCKALL,stl:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'STL:JOBNUMBER') then p_web.SetValue('brwStockAllocation_sort','3')
    ElsIf (loc:vorder = 'STL:SHELFLOCATION') then p_web.SetValue('brwStockAllocation_sort','2')
    ElsIf (loc:vorder = 'STL:DESCRIPTION') then p_web.SetValue('brwStockAllocation_sort','4')
    ElsIf (loc:vorder = 'STL:PARTNUMBER') then p_web.SetValue('brwStockAllocation_sort','5')
    ElsIf (loc:vorder = 'LOCENGINEERNAME') then p_web.SetValue('brwStockAllocation_sort','6')
    ElsIf (loc:vorder = '+STL:ENGINEER') then p_web.SetValue('brwStockAllocation_sort','6')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('brwStockAllocation:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('brwStockAllocation:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('brwStockAllocation:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('brwStockAllocation:LookupField')
    loc:selecting = 0
  End

Popups Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  If loc:popup = 0
  End
SetFormAction  Routine
  loc:formaction = 'brwStockAllocation'

GotFocusBack   Routine

GenerateBrowse Routine
  data
loc:viewoptions  Long
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  ! Set general Browse options
  loc:NavButtonPosition   = Net:Below
  loc:UpdateButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:PageLoad
  loc:Sorting          = Net:ServerSort
  if p_web.GetValue('_popup_') = 1
    loc:popup = 1
  end
  ! Set Locator Options
  loc:LocatorPosition  = Net:Above
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  p_web.ClearBrowse('brwStockAllocation')
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 15
  End
  loc:ActualSelection = ''
  !loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('brwStockAllocation_sort',net:DontEvaluate)
  If loc:vordernumber = 0
    loc:vordernumber = 3
  End
  p_web.SetSessionValue('brwStockAllocation_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'stl:JobNumber','-stl:JobNumber')
    Loc:LocateField = 'stl:JobNumber'
    Loc:LocatorCase = 0
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(stl:ShelfLocation)','-UPPER(stl:ShelfLocation)')
    Loc:LocateField = 'stl:ShelfLocation'
    Loc:LocatorCase = 0
  of 4
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(stl:Description)','-UPPER(stl:Description)')
    Loc:LocateField = 'stl:Description'
    Loc:LocatorCase = 0
  of 5
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(stl:PartNumber)','-UPPER(stl:PartNumber)')
    Loc:LocateField = 'stl:PartNumber'
    Loc:LocatorCase = 0
  of 6
    loc:vorder = Choose(Loc:SortDirection=1,'+stl:Engineer','-stl:Engineer')
    Loc:LocateField = 'stl:Engineer'
    Loc:LocatorCase = 0
  end
  if loc:vorder = ''
    loc:vorder = '+UPPER(stl:Location),+UPPER(stl:Status),+UPPER(stl:PartNumber)'
  end
  If False ! add range fields to sort order
  ElsIf (p_web.GSV('locStockAllocationType') = 'ALL')
  ElsIf (p_web.GSV('locStockAllocationType') <> 'ALL')
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('stl:JobNumber')
    loc:SortHeader = p_web.Translate('Job Number')
    p_web.SetSessionValue('brwStockAllocation_LocatorPic','@s8')
  Of upper('stl:ShelfLocation')
    loc:SortHeader = p_web.Translate('Shelf Location')
    p_web.SetSessionValue('brwStockAllocation_LocatorPic','@s30')
  Of upper('stl:Description')
    loc:SortHeader = p_web.Translate('Description')
    p_web.SetSessionValue('brwStockAllocation_LocatorPic','@s30')
  Of upper('stl:PartNumber')
    loc:SortHeader = p_web.Translate('Part Number')
    p_web.SetSessionValue('brwStockAllocation_LocatorPic','@s30')
  Of upper('locEngineerName')
  OrOf upper('stl:Engineer')
    loc:SortHeader = p_web.Translate('Engineer Name')
    p_web.SetSessionValue('brwStockAllocation_LocatorPic','@s60')
  Of upper('stl:Quantity')
    loc:SortHeader = p_web.Translate('Quantity')
    p_web.SetSessionValue('brwStockAllocation_LocatorPic','@s8')
  Of upper('locStockQuantity')
    loc:SortHeader = p_web.Translate('Stock Qty')
    p_web.SetSessionValue('brwStockAllocation_LocatorPic','@n_8')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
  End
  If loc:selecting = 1
    p_web.GetSettings(p_web.GetSessionValue('Push1'))
    loc:selectaction = p_web.FormSettings.ParentPage !p_web.GetSessionValue('brwStockAllocation:LookupFrom')
  End!Else
  do SendPacket
  do SetFormAction
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="brwStockAllocation:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="brwStockAllocation:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('brwStockAllocation:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="STOCKALL"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="stl:RecordNumberKey"></input><13,10>'
  end
  case p_web.GetValue('refresh') ; of 'next' orof 'previous' orof 'first' orof 'last'
    p_web.Script('try{{brwStockAllocation.restoreFocus();} catch(err){{};')
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'brwStockAllocation',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      case loc:locatortype
      of Net:Position
      orof Net:Date
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
      of Net:Contains
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
      of Net:Begins
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
      End
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
      Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
      Case Loc:LocatorType
      of Net:Date
        packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator2brwStockAllocation','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('brwStockAllocation_LocatorPic'),,,'onchange="brwStockAllocation.locate(''Locator2brwStockAllocation'',this.value);"',,0,,,) & '</div></td>'
      Else
        packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator2brwStockAllocation',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="brwStockAllocation.locate(''Locator2brwStockAllocation'',this.value);" ',,,,,,) & '</td>'
      End
      If loc:LocatorSearchButton or loc:LocatorClearButton
        packet = clip(packet) & '<td><div id="brwStockAllocation_locate_a" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
        If loc:LocatorSearchButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
        End
        If loc:LocatorClearButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'brwStockAllocation.cl(''brwStockAllocation'');')
        End
        packet = clip(packet) & '</div><13,10></td><13,10>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
      If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'brwStockAllocation_locate_a','buttonset',loc:options)
      End
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & p_web.DivHeader('brwStockAllocation_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'','sortable')&'" id="brwStockAllocation_tbl">'
  Else
    packet = clip(packet) & p_web.DivHeader('brwStockAllocation_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'')&'" id="brwStockAllocation_tbl">'
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'3','brwStockAllocation',p_web.Translate('Job Number'),'Click here to sort by Job Number',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'2','brwStockAllocation',p_web.Translate('Shelf Location'),'Click here to sort by Shelf Location',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'4','brwStockAllocation',p_web.Translate('Description'),'Click here to sort by Description',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'5','brwStockAllocation',p_web.Translate('Part Number'),'Click here to sort by Part Number',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'6','brwStockAllocation',p_web.Translate('Engineer Name'),,,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'7','brwStockAllocation',p_web.Translate('Quantity'),'Click here to sort by Quantity',,,,,,1,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'8','brwStockAllocation',p_web.Translate('Stock Qty'),,,,,,,1,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'9','brwStockAllocation',p_web.Translate('Available'),,,,,,,1,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
  If (p_web.GSV('RapidLocation') = 1) AND  true ! [A]
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'10','brwStockAllocation',p_web.Translate('Status'),,,,,,,1,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
  End ! Field condition [A]
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,15,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  If Instring('stl:recordnumber',lower(loc:vorder),1,1) = 0 !and STOCKALL{prop:SQLDriver} = 1
    loc:vorder = Choose(loc:vorder='','stl:RecordNumber',clip(loc:vorder) & ',' & 'stl:RecordNumber')
  End
  Loc:Selected = Choose(p_web.IfExistsValue('stl:RecordNumber'),p_web.GetValue('stl:RecordNumber'),p_web.GetSessionValue('stl:RecordNumber'))
  ThisView{prop:order} = p_web.CleanFilter(ThisView,clip(loc:vorder))
  If False  ! Generate Filter
  ElsIf (p_web.GSV('locStockAllocationType') = 'ALL')
      loc:FilterWas = 'UPPER(stl:Location) = UPPER(''' & p_web.GSV('Default:SiteLocation') & ''')'
  ElsIf (p_web.GSV('locStockAllocationType') <> 'ALL')
      loc:FilterWas = 'UPPER(stl:Location) = UPPER(''' & p_web.GSV('Default:SiteLocation') & ''') AND UPPER(stl:Status) = UPPER(''' & p_web.GSV('locStockAllocationType') & ''')'
  Else
        loc:FilterWas = 'UPPER(stl:Location) = UPPER(''' & p_web.GSV('Default:SiteLocation') & ''') AND UPPER(stl:Status) = UPPER(''' & p_web.GSV('locStockAllocationType') & ''')'
  End
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'brwStockAllocation',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('brwStockAllocation_Filter')
    p_web.SetSessionValue('brwStockAllocation_FirstValue','')
    p_web.SetSessionValue('brwStockAllocation_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,STOCKALL,stl:RecordNumberKey,loc:PageRows,'brwStockAllocation',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,loc:ViewOptions,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled,Loc:LocatorCase)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or Loc:LocatorType = Net:Date or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position or loc:LocatorType = Net:Date
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If STOCKALL{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(STOCKALL,loc:firstvalue)
              Reset(ThisView,STOCKALL)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If STOCKALL{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(STOCKALL,loc:lastvalue)
            Reset(ThisView,STOCKALL)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      Access:USERS.Clearkey(use:User_Code_Key)
      use:User_Code   = stl:Engineer
      If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
          !Found
          locEngineerName    = stl:Engineer & ' (' & Clip(use:Forename) & ' ' & Clip(use:Surname) & ')'
      Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
          !Error
          locEngineerName    = stl:Engineer
      End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
      
      !------------------------------------------------------------------
      !Show the pretty green box to show the stock is available
      locStockAvailable = 0
      locStockQuantity = 0
      
      Access:STOCK.Clearkey(sto:Ref_Number_Key)
      sto:Ref_Number  = stl:PartRefNumber
      If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
          !Found
          locStockQuantity   = sto:Quantity_Stock
          If locStockQuantity >= stl:Quantity
              locStockAvailable = 1
          End !If sto:Quantity_Stock <= stl:Quantity
      Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
          !Error
          Access:STOCK.ClearKey(sto:Location_Key)
          sto:Location    = use:Location
          sto:Part_Number = stl:PartNumber
          If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
              !Found
              locStockQuantity = sto:Quantity_Stock
              If locStockQuantity >= stl:Quantity
                  locStockAvailable = 1
              End !If sto:Quantity_Stock <= stl:Quantity
          Else!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
              locStockQuantity = ''
          End!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
      End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
      
      
      
      
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(stl:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td class="'&p_web.combine(p_web.site.style.BrowseEmpty,)&'">'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:NavButtonPosition=Net:Above or (loc:NavButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="brwStockAllocation_nav_a" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'brwStockAllocation.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'brwStockAllocation.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'brwStockAllocation.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'brwStockAllocation.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'brwStockAllocation_nav_a','buttonset',loc:options)
          End
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:UpdateButtonPosition=Net:Above or (loc:UpdateButtonPosition=Net:Both and loc:found > 0))
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'brwStockAllocation',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('brwStockAllocation_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('brwStockAllocation_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          case loc:locatortype
          of Net:Position
          orof Net:Date
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
          of Net:Contains
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
          of Net:Begins
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
          End
          packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
          Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
          Case Loc:LocatorType
          of Net:Date
            packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator1brwStockAllocation','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('brwStockAllocation_LocatorPic'),,,'onchange="brwStockAllocation.locate(''Locator1brwStockAllocation'',this.value);"',,0,,,) & '</div></td>'
          Else
            packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator1brwStockAllocation',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="brwStockAllocation.locate(''Locator1brwStockAllocation'',this.value);" ',,,,,,) & '</td>'
          End
          If loc:LocatorSearchButton or loc:LocatorClearButton
            packet = clip(packet) & '<td><div id="brwStockAllocation_locate_b" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
            If loc:LocatorSearchButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
            End
            If loc:LocatorClearButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'brwStockAllocation.cl(''brwStockAllocation'');')
            End
            packet = clip(packet) & '</div><13,10></td><13,10>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
          If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'brwStockAllocation_locate_b','buttonset',loc:options)
          End
      End
    End
  End
  p_web.SetSessionValue('brwStockAllocation_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('brwStockAllocation_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:NavButtonPosition=Net:Below or loc:NavButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="brwStockAllocation_nav_b" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'brwStockAllocation.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'brwStockAllocation.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'brwStockAllocation.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'brwStockAllocation.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'brwStockAllocation_nav_b','buttonset',loc:options)
          End
        end
        do SendPacket
  End
  If loc:UpdateButtonPosition=Net:Below or loc:UpdateButtonPosition=Net:Both
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
  data
loc:rowclick  string(1024)
  code
    loc:field = p_web.AddBrowseValue('brwStockAllocation','STOCKALL',stl:RecordNumberKey) !stl:RecordNumber
    p_web._thisrow = p_web._nocolon('stl:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if Loc:LocatorValue <> '' and loc:ActualSelection = ''
        loc:checked = 'checked'
        do SetSelection
      elsif loc:ActualSelection = '' and stl:RecordNumber = p_web.GetValue('stl:RecordNumber')
         loc:checked = 'checked'
         do SetSelection
      elsif loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('brwStockAllocation:LookupField')) = stl:RecordNumber and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      else
        loc:checked = Choose((stl:RecordNumber = loc:selected) and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      end
      If(loc:SelectionMethod  = Net:Radio)
        loc:RowStyle = 'class="' & p_web.combine(p_web.site.style.browserow,) &'"'
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'data-nt-id="'& p_web.AddBrowseValue('brwStockAllocation','STOCKALL',stl:RecordNumberKey) &'" onclick="'
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If STOCKALL{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(STOCKALL)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            loc:DefaultSelection = loc:field
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If STOCKALL{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(STOCKALL)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            loc:DefaultSelection = loc:field
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'>'&p_web.CreateInput('radio','_bidv_',clip(loc:field),,loc:checked,'')&'</td>'&CRLF
          If loc:DefaultSelection = ''
            loc:DefaultSelection = loc:field
          End
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          If loc:DefaultSelection = '' or loc:direction < 0
            loc:DefaultSelection = loc:field
          End
        End
    end ! loc:eip = 0
      IF (loc:Checked = 'checked')
          p_web.SSV('RecordSelected',1)
          p_web.SSV('stl:PartNumber',stl:PartNumber)
      END ! IF (loc:Checked = 'checked')
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::stl:JobNumber
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::stl:ShelfLocation
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::stl:Description
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::stl:PartNumber
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::locEngineerName
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('RightJustify')&'"><13,10>'
          end ! loc:eip = 0
          do value::stl:Quantity
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('RightJustify')&'"><13,10>'
          end ! loc:eip = 0
          do value::locStockQuantity
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
            if false
            elsif locStockAvailable = 1
              packet = clip(packet) & '<td class="'&p_web.combine('CenterJustify',)&'"><13,10>'
            else
              packet = clip(packet) & '<td class="'&p_web.combine('CenterJustify')&'"><13,10>'
            end  !False
          end ! loc:eip = 0
          do value::icnStockAvailable
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      If (p_web.GSV('RapidLocation') = 1) AND  true
          If Loc:Eip = 0
            if false
            elsif stl:Status = 'WEB'
              packet = clip(packet) & '<td class="'&p_web.combine('CenterJustify',)&'"><13,10>'
            elsif stl:Status = 'PIK'
              packet = clip(packet) & '<td class="'&p_web.combine('CenterJustify',)&'"><13,10>'
            elsif stl:Status = 'PRO'
              packet = clip(packet) & '<td class="'&p_web.combine('CenterJustify',)&'"><13,10>'
            elsif stl:status = 'RET'
              packet = clip(packet) & '<td class="'&p_web.combine('CenterJustify',)&'"><13,10>'
            else
              packet = clip(packet) & '<td class="'&p_web.combine('CenterJustify')&'"><13,10>'
            end  !False
          end ! loc:eip = 0
          do value::icnStatus
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      End ! Field Condition
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr ' & clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Idx = p_web.AddBrowseValue('brwStockAllocation','STOCKALL',stl:RecordNumberKey)
  TableQueue.Id[1] = stl:RecordNumber

ClosingScripts  Routine
  data
Rtn_SecwinProcedureName         string(252)
  code
  If p_web.RequestAjax = 0
    do SetFormAction
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var btibrwStockAllocation;if (btibrwStockAllocation != 1){{var brwStockAllocation=new browseTable(''brwStockAllocation'','''&clip(loc:formname)&|
      ''','''&p_web._jsok('stl:RecordNumber',Net:Parameter)&''',2,'''&clip(loc:divname)&''',1,1,1,'''&|
      clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&|
      p_web.Translate('Are you sure you want to delete this record?')&''','''&clip(loc:ActualSelection)&''','''&clip(loc:selectaction)&''','''&|
      clip(loc:formactiontarget)&''','''','&loc:popup&',0,'''','''',"' & clip(Rtn_SecwinProcedureName) & '");<13,10>'&|
      'brwStockAllocation.setGreenBar('''&p_web.ColorWeb(p_web.Site.Style.BrowseHighlightColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOneColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseTwoColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOverColor)&''');<13,10>' &|
      'brwStockAllocation.applyGreenBar();btibrwStockAllocation=1};<13,10>' &|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2brwStockAllocation')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1brwStockAllocation')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1brwStockAllocation')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2brwStockAllocation')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(STOCKALL)
  p_web._CloseFile(STOCK)
  p_web._CloseFile(USERS)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(STOCKALL)
  Bind(stl:Record)
  Clear(stl:Record)
  NetWebSetSessionPics(p_web,STOCKALL)
  p_web._OpenFile(STOCK)
  Bind(sto:Record)
  NetWebSetSessionPics(p_web,STOCK)
  p_web._OpenFile(USERS)
  Bind(use:Record)
  NetWebSetSessionPics(p_web,USERS)

Children Routine
  if loc:selecting = 0
    If p_web.RequestAjax = 0
      do StartChildren
    Else
      do AjaxChildren
    End
  end

AjaxChildren  Routine
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue('stl:RecordNumber',p_web.GetValue('stl:RecordNumber'))
   p_web.DeleteValue('_Clicked')
   loc:found = 1
   do Children
! ----------------------------------------------------------------------------------------
CallRow  Routine
  data
loc:result  long
  code
  do OpenFilesB
  stl:RecordNumber = p_web.GSV('stl:RecordNumber')
  loc:result = p_web._GetFile(STOCKALL,stl:RecordNumberKey)
  loc:eip = 1
  loc:viewstate = p_web.escape(p_web.Base64Encode(clip(stl:RecordNumber)))
  do BrowseRow
  do SendPacket
  do ClosefilesB

! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(STOCKALL)
  Case upper(p_web.GetValue('_EIPClm'))
  Else
    case p_web.GetValue('_action_')
    of Net:InsertRecord
    orof Net:ChangeRecord
    orof Net:DeleteRecord
    orof Net:CopyRecord
      loc:eip = 0
      do CallBrowse
    end
  End
  do GotFocusBack
  p_web._CloseFile(STOCKALL)
! ----------------------------------------------------------------------------------------
value::stl:JobNumber   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('brwStockAllocation_stl:JobNumber_'&stl:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(stl:JobNumber,'@s8')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::stl:ShelfLocation   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('brwStockAllocation_stl:ShelfLocation_'&stl:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(stl:ShelfLocation,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::stl:Description   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('brwStockAllocation_stl:Description_'&stl:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(stl:Description,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::stl:PartNumber   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('brwStockAllocation_stl:PartNumber_'&stl:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(stl:PartNumber,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::locEngineerName   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('brwStockAllocation_locEngineerName_'&stl:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(locEngineerName,'@s60')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::stl:Quantity   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('brwStockAllocation_stl:Quantity_'&stl:RecordNumber,'RightJustify',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(stl:Quantity,'@s8')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::locStockQuantity   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('brwStockAllocation_locStockQuantity_'&stl:RecordNumber,'RightJustify',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(locStockQuantity,'@n_8')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::icnStockAvailable   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    elsif locStockAvailable = 1 ! alternative condition for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('brwStockAllocation_icnStockAvailable_'&stl:RecordNumber,p_web.combine('CenterJustify',),net:crc,,loc:extra)
              packet = clip(packet) & p_web.CreateImage('/images/accept.png','','','',, ,loc:javascript,,0,,)
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('brwStockAllocation_icnStockAvailable_'&stl:RecordNumber,'CenterJustify',net:crc,,loc:extra)
      packet = clip(packet) & p_web.CreateImage('/images/reject.png','','','',,,loc:javascript,,0,'') &  p_web.CreateHyperLink('&#160;',,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::icnStatus   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
  If (p_web.GSV('RapidLocation') = 1)
    if false
    elsif stl:Status = 'WEB' ! alternative condition for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('brwStockAllocation_icnStatus_'&stl:RecordNumber,p_web.combine('CenterJustify',),net:crc,,loc:extra)
              packet = clip(packet) & p_web.CreateImage('/images/bullet_purple.png','','','',, ,loc:javascript,,0,,)
    elsif stl:Status = 'PIK' ! alternative condition for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('brwStockAllocation_icnStatus_'&stl:RecordNumber,p_web.combine('CenterJustify',),net:crc,,loc:extra)
              packet = clip(packet) & p_web.CreateImage('/images/bullet_blue.png','','','',, ,loc:javascript,,0,,)
    elsif stl:Status = 'PRO' ! alternative condition for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('brwStockAllocation_icnStatus_'&stl:RecordNumber,p_web.combine('CenterJustify',),net:crc,,loc:extra)
              packet = clip(packet) & p_web.CreateImage('/images/bullet_pink.png','','','',, ,loc:javascript,,0,,)
    elsif stl:status = 'RET' ! alternative condition for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('brwStockAllocation_icnStatus_'&stl:RecordNumber,p_web.combine('CenterJustify',),net:crc,,loc:extra)
              packet = clip(packet) & p_web.CreateImage('/images/bullet_red.png','','','',, ,loc:javascript,,0,,)
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('brwStockAllocation_icnStatus_'&stl:RecordNumber,'CenterJustify',net:crc,,loc:extra)
      packet = clip(packet) & p_web.CreateImage('/images/bullet_black.png','','','',,,loc:javascript,,0,'') &  p_web.CreateHyperLink('&#160;',,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
  End
OpenFiles  ROUTINE
  p_web._OpenFile(STOCK)
  p_web._OpenFile(USERS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(STOCK)
  p_Web._CloseFile(USERS)
     FilesOpened = False
  END
  return
!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
CheckForDuplicate  Routine
SetSelection  Routine
  loc:ActualSelection = loc:field
  p_web.SetSessionValue('stl:RecordNumber',stl:RecordNumber)

MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:ActualSelection = ''
    loc:ActualSelection = loc:DefaultSelection
    p_web.GetBrowseValue(loc:ActualSelection,Net:Web:SessionQueue) ! so children are primed with correct sessionValue
  End

  If loc:ActualSelection <> ''
    TableQueue.Kind = Net:RowTable
    get(TableQueue,TableQueue.Kind)
    if Errorcode() = 0
      TableQueue.Row = sub(TableQueue.Row,1,len(clip(TableQueue.Row))-1) &  ' data-nt-rows="'&loc:RowsHigh&'" data-nt-value="'&clip(loc:ActualSelection)&'"' & '>'
      Put(TableQueue)
    End
  End

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead class="'&p_web.combine(p_web.site.style.BrowseHeader,'')&'"><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot class="'&p_web.combine(p_web.site.style.BrowseFooter,)&'"><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody class="'&p_web.combine(p_web.site.style.BrowseBody,)&'"><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table><13,10>' & p_web.DivFooter(Net:NoSend) ! Table Div
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
frmChangeStockStatus PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locStockRefNo        LONG                                  !
FoundAnyPart         BYTE                                  !
FoundOrderedPart     BYTE                                  !
locJobNumber         LONG                                  !
RapidQueue           QUEUE,PRE(rapque)                     !
SessionID            LONG                                  !
RecordNumber         LONG                                  !
                     END                                   !
RapidJobQueue        QUEUE,PRE(jobque)                     !
SessionID            LONG                                  !
JobNumber            LONG                                  !
                     END                                   !
                    MAP
AddFaulty               Procedure(String  func:Type)
                    END
FilesOpened     Long
STOFAULT::State  USHORT
USERS::State  USHORT
STOCK::State  USHORT
JOBS::State  USHORT
WARPARTS::State  USHORT
PARTS::State  USHORT
ESTPARTS::State  USHORT
STOCKALL::State  USHORT
txtRequestFulfilled:IsInvalid  Long
txtErrorText:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('frmChangeStockStatus')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'frmChangeStockStatus_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('frmChangeStockStatus','')
    p_web.DivHeader('frmChangeStockStatus',p_web.combine(p_web.site.style.formdiv,))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('frmChangeStockStatus') = 0
        p_web.AddPreCall('frmChangeStockStatus')
        p_web.DivHeader('popup_frmChangeStockStatus','nt-hidden')
        p_web.DivHeader('frmChangeStockStatus',p_web.combine(p_web.site.style.formdiv,))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_frmChangeStockStatus_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_frmChangeStockStatus_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferfrmChangeStockStatus',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_frmChangeStockStatus',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmChangeStockStatus',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_frmChangeStockStatus',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmChangeStockStatus',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_frmChangeStockStatus',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_frmChangeStockStatus',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmChangeStockStatus',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_frmChangeStockStatus',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmChangeStockStatus',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_frmChangeStockStatus',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_frmChangeStockStatus',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('frmChangeStockStatus')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(STOFAULT)
  p_web._OpenFile(USERS)
  p_web._OpenFile(STOCK)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(WARPARTS)
  p_web._OpenFile(PARTS)
  p_web._OpenFile(ESTPARTS)
  p_web._OpenFile(STOCKALL)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(STOFAULT)
  p_Web._CloseFile(USERS)
  p_Web._CloseFile(STOCK)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(WARPARTS)
  p_Web._CloseFile(PARTS)
  p_Web._CloseFile(ESTPARTS)
  p_Web._CloseFile(STOCKALL)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('frmChangeStockStatus_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'frmChangeStockStatus'
    end
    p_web.formsettings.proc = 'frmChangeStockStatus'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('frmChangeStockStatus_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'frmStockAllocation'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('frmChangeStockStatus_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('frmChangeStockStatus_ChainTo')
    loc:formaction = p_web.GetSessionValue('frmChangeStockStatus_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction

GenerateForm   Routine
  do LoadRelatedRecords
  IF (p_web.IfExistsValue('STA'))
      p_web.StoreValue('STA')
  END
  
  Access:STOCKALL.Clearkey(stl:RecordNumberKey)
  stl:RecordNumber    = p_web.GSV('stl:RecordNumber')
  Access:STOCKALL.Tryfetch(stl:RecordNumberKey)
  IF (p_web.GSV('STA') = 'PIK' OR (p_web.GSV('STA') = 'PRO'))
  
          !Found
      p_web.SSV('txtErrorText','')
      p_web.SSV('txtRequestFulfilled','The selected part has been updated.')
      If stl:PartNumber = 'EXCH'
          p_web.SSV('txtErrorText','The selected part in an "Exchange Part".')
          p_web.SSV('txtRequestFulfilled','')
      Else !If stl:PartNumber = 'EXCH'
          Error# = 0
  
          If stl:Status = 'WEB'
              p_web.SSV('txtErrorText','The selected part is On Order.')
              p_web.SSV('txtRequestFulfilled','')
              Error# = 1
          End !If stl:Status = 'WEB'
  
          If Error# = 0
              Case stl:PartType
              Of 'CHA'
                  Access:PARTS.Clearkey(par:recordnumberkey)
                  par:Record_Number   = stl:PartRecordNumber
                  If Access:PARTS.Tryfetch(par:recordnumberkey) = Level:Benign
                              !Found
                      par:Status  = p_web.GSV('STA')
                      Access:PARTS.Update()
                  Else ! If Access:PARTS.Tryfetch(par:Record_Number_Key) = Level:Benign
                              !Error
                      Error# = 1
                  End !If AccessPARTS.Tryfetch(parRecord_Number_Key) = LevelBenign
              Of 'WAR'
                  Access:WARPARTS.Clearkey(wpr:RecordNumberKey)
                  wpr:Record_Number   = stl:PartRecordNumber
                  If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
                              !Found
                      wpr:Status  = p_web.GSV('STA')
                      Access:WARPARTS.Update()
                  Else ! If Access:WARPARTS.Tryfetch(wpr:Record_Number_Key) = Level:Benign
                              !Error
                      Error# = 1
                  End !If AccessWARPARTS.Tryfetch(wprRecord_Number_Key) = LevelBenign
              Of 'EST'
                  Access:ESTPARTS.Clearkey(epr:Record_Number_Key)
                  epr:Record_Number   = stl:PartRecordNumber
                  If Access:ESTPARTS.Tryfetch(epr:Record_Number_Key) = Level:Benign
                              !Found
                      epr:Status = p_web.GSV('STA')
                      Access:ESTPARTS.Update()
                  Else ! If Access:ESTPARTS.Tryfetch(epr:RecordNumberKey) = Level:Benign
                              !Error
                  End !If Access:ESTPARTS.Tryfetch(epr:RecordNumberKey) = Level:Benign
              End !Case rapsto:PartType
              If Error# = 0
                  stl:Status = p_web.GSV('STA')
                  Access:STOCKALL.Update()
  
              End !If Error# = 0
  
          End !If Error# = 0
      End !If stl:PartNumber = 'EXCH'
  END
  IF (p_web.GSV('STA') = 'ALL')
      If stl:PartNumber = 'EXCH'
          p_web.SSV('txtErrorText','The selected part in an "Exchange Part".')
          p_web.SSV('txtRequestFulfilled','')
      Else !If stl:PartNumber = 'EXCH'
          Error# = 0
          If stl:Status = 'WEB'
              p_web.SSV('txtErrorText','The selected part in On Order.')
              p_web.SSV('txtRequestFulfilled','')
              Error# = 1
          End !If stl:Status = 'WEB'
          If Error# = 0
              p_web.SSV('txtErrorText','')
              p_web.SSV('txtRequestFulfilled','The selected part has been allocated.')
              Case stl:PartType
              Of 'CHA'
                  Access:PARTS.Clearkey(par:recordnumberkey)
                  par:Record_Number   = stl:PartRecordNumber
                  If Access:PARTS.Tryfetch(par:recordnumberkey) = Level:Benign
                          !Found
                      Access:STOCK.Clearkey(sto:Ref_Number_Key)
                      sto:Ref_Number  = par:Part_Ref_Number
                      If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                              !Found
                          If sto:ReturnFaultySpare
                              p_web.SSV('txtErrorText','.')
                              p_web.SSV('txtRequestFulfilled','Part Updated. The faulty part must be returned before a new part can be issued.')
                              AddFaulty('C')
                          End !If sto:ReturnFaultySpare
                      Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                              !Error
                      End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                      If Error# = 0
                          par:PartAllocated = 1
                          par:WebOrder = 0
                          Access:PARTS.Update()
                      End !If Error# = 0
                  Else ! If Access:PARTS.Tryfetch(par:Record_Number_Key) = Level:Benign
                          !Error
                      Error# = 1
                  End !If AccessPARTS.Tryfetch(parRecord_Number_Key) = LevelBenign
              Of 'WAR'
                  Access:WARPARTS.Clearkey(wpr:RecordNumberKey)
                  wpr:Record_Number   = stl:PartRecordNumber
                  If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
                          !Found
                      Access:STOCK.Clearkey(sto:Ref_Number_Key)
                      sto:Ref_Number  = wpr:Part_Ref_Number
                      If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                              !Found
                          If sto:ReturnFaultySpare
                              p_web.SSV('txtErrorText','.')
                              p_web.SSV('txtRequestFulfilled','Part Updated. The faulty part must be returned before a new part can be issued.')
  
                              AddFaulty('W')
                          End !If sto:ReturnFaultySpare
  
                      Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                              !Error
                      End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                      If Error# = 0
                          wpr:PartAllocated = 1
                          wpr:WebOrder = 0
                          Access:WARPARTS.Update()
                      End !If Error# = 0
  
                  Else ! If Access:WARPARTS.Tryfetch(wpr:Record_Number_Key) = Level:Benign
                          !Error
                      Error# = 1
                  End !If AccessWARPARTS.Tryfetch(wprRecord_Number_Key) = LevelBenign
              Of 'EST'
                  Access:ESTPARTS.Clearkey(epr:Record_Number_Key)
                  epr:Record_Number   = stl:PartRecordNumber
                  If Access:ESTPARTS.Tryfetch(epr:Record_Number_Key) = Level:Benign
                          !Found
                      Access:STOCK.Clearkey(sto:Ref_Number_Key)
                      sto:Ref_Number  = epr:Part_Ref_Number
                      If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                              !Found
                          If sto:ReturnFaultySpare
                              p_web.SSV('txtErrorText','.')
                              p_web.SSV('txtRequestFulfilled','Part Updated. The faulty part must be returned before a new part can be issued.')
  
                              AddFaulty('E')
                          End !If sto:ReturnFaultySpare
  
                      Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                              !Error
                      End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                      If Error# = 0
                          epr:PartAllocated = 1
                          Access:ESTPARTS.Update()
                      End !If Error# = 0
  
                  Else ! If Access:ESTPARTS.Tryfetch(epr:RecordNumberKey) = Level:Benign
                          !Error
                      Error# = 1
                  End !If Access:ESTPARTS.Tryfetch(epr:RecordNumberKey) = Level:Benign
  
              End !Case stl:PartType
              If Error# = 0
                  Relate:STOCKALL.Delete(0)
              End !If Error# = 0
  
  
              !Added 05/12/02 - L393 / VP109 -
              !If all warranty/chargeable/estimate parts are now allocated - change job status to 315 - In Repair
              FoundAnyPart = false
  
              !first check the chargeable parts
              access:Parts.clearkey(par:Part_Number_Key)
              par:Ref_Number = stl:JobNumber
              set(par:Part_Number_Key,par:Part_Number_Key)
              Loop !to find an unallocated part
                  if access:Parts.next() then
                      !leave no more jobs
                      break
                  ELSE
                      if par:Ref_Number <> stl:JobNumber then
                          !Leave no more matching jobs
                          break
                      ELSE
                          !found a part for this job
                          if par:PartAllocated then
                              !ignore this it was allocated
                          ELSE
                              !found an unallocated part!
                              FoundAnyPart = true
                              break
                          END !if partAllocated
                      END !If ref_numbers disagree
                  END !if access:jobs.next()
              End !loop to find an unallocated part
  
              if FoundAnyPart = false
                  !Check warranty parts
                  access:warParts.clearkey(wpr:Part_Number_Key)
                  wpr:Ref_Number = stl:JobNumber
                  set(wpr:Part_Number_Key,wpr:Part_Number_Key)
                  Loop !to find an unallocated part
                      if access:WarParts.next() then
                          !leave no more jobs
                          break
                      ELSE
                          if wpr:Ref_Number <> stl:JobNumber then
                              !Leave no more matching jobs
                              break
                          ELSE
                              !found a part for this job
                              if wpr:PartAllocated then
                                  !ignore this it was allocated
                              ELSE
                                  !found an unallocated part!
                                  FoundAnyPart = true
                                  break
                              END !if partAllocated
                          END !If ref_numbers disagree
                      END !if access:jobs.next()
                  End !loop to find an unallocated part
  
              END !if I haven't FoundAnyPart
  
              if FoundAnyPart = false
                  !Check estimate parts
                  access:EstParts.clearkey(epr:Part_Number_Key)
                  epr:Ref_Number = stl:JobNumber
                  set(epr:Part_Number_Key,epr:Part_Number_Key)
                  Loop !to find an unallocated part
                      if access:EstParts.next() then
                          !leave no more jobs
                          break
                      ELSE
                          if epr:Ref_Number <> stl:JobNumber then
                              !Leave no more matching jobs
                              break
                          ELSE
                              !found a part for this job
                              if epr:PartAllocated then
                                  !ignore this it was allocated
                              ELSE
                                  !found an unallocated part!
                                  FoundAnyPart = true
                                  break
                              END !if partAllocated
                          END !If ref_numbers disagree
                      END !if access:jobs.next()
                  End !loop to find an unallocated part
              END !if I haven't FoundAnyPart
  
              if FoundAnyPart = false
                   !change job status to 315
                  Access:JOBS.Clearkey(job:Ref_Number_Key)
                  job:Ref_Number  = stl:JobNumber
                  If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                      Error# = 0
  
                      Pointer# = Pointer(JOBS)
                      Hold(JOBS,1)
                      Get(JOBS,Pointer#)
                      If Errorcode() = 43
  
                          Error# = 1
                !          !Post(Event:CloseWindow)
                      ELSE !If Errorcode() = 43
                            !Found
                          getstatus(315,0,'JOB')
                          Access:JOBS.TryUpdate()
                      End  !If Errorcode() = 43
                      Release(JOBS)
                  END !if tryfetch on jobs
              END !if I haven't FoundAnyPart
  
  
          !end of Added 05/12/02 - L393 / VP109 -
  
          End !If Error# = 0
      End !If stl:PartNumber = 'EXCH'
  
  END
      p_web.site.SaveButton.TextValue = 'OK'
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Fulfil Request') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Fulfil Request',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_frmChangeStockStatus',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_frmChangeStockStatus0_div')&'">'&p_web.Translate('Fulfil Request')&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="frmChangeStockStatus_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="frmChangeStockStatus_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'frmChangeStockStatus_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="frmChangeStockStatus_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'frmChangeStockStatus_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_frmChangeStockStatus')>0,p_web.GSV('showtab_frmChangeStockStatus'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_frmChangeStockStatus'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_frmChangeStockStatus') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_frmChangeStockStatus'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_frmChangeStockStatus')>0,p_web.GSV('showtab_frmChangeStockStatus'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_frmChangeStockStatus') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Fulfil Request') & ''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_frmChangeStockStatus_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_frmChangeStockStatus')>0,p_web.GSV('showtab_frmChangeStockStatus'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"frmChangeStockStatus",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_frmChangeStockStatus')>0,p_web.GSV('showtab_frmChangeStockStatus'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_frmChangeStockStatus_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('frmChangeStockStatus') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('frmChangeStockStatus')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Fulfil Request')&'</a></h3>' & CRLF & p_web.DivHeader('tab_frmChangeStockStatus0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_frmChangeStockStatus0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_frmChangeStockStatus0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_frmChangeStockStatus0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Fulfil Request')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_frmChangeStockStatus0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Fulfil Request')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_frmChangeStockStatus0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Fulfil Request')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_frmChangeStockStatus0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::txtRequestFulfilled
        do Comment::txtRequestFulfilled
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::txtErrorText
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::txtErrorText
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Validate::txtRequestFulfilled  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::txtRequestFulfilled  ! copies value to session value if valid.
  do Comment::txtRequestFulfilled ! allows comment style to be updated.

ValidateValue::txtRequestFulfilled  Routine
    If not (1=0)
    End

Value::txtRequestFulfilled  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('frmChangeStockStatus_' & p_web._nocolon('txtRequestFulfilled') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="txtRequestFulfilled" class="'&clip('green bold large')&'"'&clip(loc:extra)&'>' & p_web.Translate(p_web.GSV('txtRequestFulfilled'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::txtRequestFulfilled  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if txtRequestFulfilled:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('frmChangeStockStatus_' & p_web._nocolon('txtRequestFulfilled') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::txtErrorText  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::txtErrorText  ! copies value to session value if valid.
  do Comment::txtErrorText ! allows comment style to be updated.

ValidateValue::txtErrorText  Routine
    If not (1=0)
    End

Value::txtErrorText  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('frmChangeStockStatus_' & p_web._nocolon('txtErrorText') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="txtErrorText" class="'&clip('red bold large')&'"'&clip(loc:extra)&'>' & p_web.Translate(p_web.GSV('txtErrorText'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::txtErrorText  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if txtErrorText:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('frmChangeStockStatus_' & p_web._nocolon('txtErrorText') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('frmChangeStockStatus_nexttab_' & 0)
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_frmChangeStockStatus_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('frmChangeStockStatus_tab_' & 0)
    do GenerateTab0
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('frmChangeStockStatus_form:ready_',1)

  p_web.SetSessionValue('frmChangeStockStatus_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_frmChangeStockStatus',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('frmChangeStockStatus_form:ready_',1)
  p_web.SetSessionValue('frmChangeStockStatus_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_frmChangeStockStatus',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('frmChangeStockStatus_form:ready_',1)
  p_web.SetSessionValue('frmChangeStockStatus_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('frmChangeStockStatus:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('frmChangeStockStatus_form:ready_',1)
  p_web.SetSessionValue('frmChangeStockStatus_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('frmChangeStockStatus:Primed',0)
  p_web.setsessionvalue('showtab_frmChangeStockStatus',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('frmChangeStockStatus_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('frmChangeStockStatus_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
    do ValidateValue::txtRequestFulfilled
    If loc:Invalid then exit.
    do ValidateValue::txtErrorText
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('frmChangeStockStatus:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('')

AddFaulty     Procedure(String  func:Type)
Code
    If Access:STOFAULT.PrimeRecord() = Level:Benign
        Case func:Type
        Of 'W'
            stf:PartNumber  = wpr:Part_Number
            stf:Description = wpr:Description
            stf:Quantity    = wpr:Quantity
            stf:PurchaseCost    = wpr:Purchase_Cost
        Of 'C'
            stf:PartNumber  = par:Part_Number
            stf:Description = par:Description
            stf:Quantity    = par:Quantity
            stf:PurchaseCost    = par:Purchase_Cost
        Of 'E'
            stf:PartNumber  = epr:Part_Number
            stf:Description = epr:Description
            stf:Quantity    = epr:Quantity
            stf:PurchaseCost    = epr:Purchase_Cost
        End !Case func:Type

        stf:ModelNumber = job:Model_Number
        stf:IMEI        = job:ESN
        stf:Engineer    = job:Engineer
        stf:StoreUserCode   = p_web.GSV('BookingUserCode')
        stf:PartType        = 'FAU'
        If Access:STOFAULT.TryInsert() = Level:Benign
            !Insert Successful

        Else !If Access:STOFAULT.TryInsert() = Level:Benign
            !Insert Failed
            Access:STOFAULT.CancelAutoInc()
        End !If Access:STOFAULT.TryInsert() = Level:Benign
    End !If Access:STOFAULT.PrimeRecord() = Level:Benign
frmFulfilRequest     PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locStockRefNo        LONG                                  !
FoundAnyPart         BYTE                                  !
FoundOrderedPart     BYTE                                  !
locJobNumber         LONG                                  !
RapidQueue           QUEUE,PRE(rapque)                     !
SessionID            LONG                                  !
RecordNumber         LONG                                  !
                     END                                   !
RapidJobQueue        QUEUE,PRE(jobque)                     !
SessionID            LONG                                  !
JobNumber            LONG                                  !
                     END                                   !
                    MAP
RemovePartFromStock     Procedure(Long func:StockRefNumber,Long func:PartRecordNumber,Long func:Quantity,String func:PartType,String func:PartNumber),BYTE  !StockRefNumber, PartRecordNumber, Quantity, PartType, Quantity
AddFaulty               Procedure(String  func:Type)
FindOtherParts          Procedure(Long func:JobNumber,Long func:RecordNumber),BYTE    !Job Number, Record Number
                    END
FilesOpened     Long
STOFAULT::State  USHORT
USERS::State  USHORT
STOCK::State  USHORT
JOBS::State  USHORT
WARPARTS::State  USHORT
PARTS::State  USHORT
ESTPARTS::State  USHORT
STOCKALL::State  USHORT
txtRequestFulfilled:IsInvalid  Long
txtErrorText:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('frmFulfilRequest')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'frmFulfilRequest_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('frmFulfilRequest','')
    p_web.DivHeader('frmFulfilRequest',p_web.combine(p_web.site.style.formdiv,))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('frmFulfilRequest') = 0
        p_web.AddPreCall('frmFulfilRequest')
        p_web.DivHeader('popup_frmFulfilRequest','nt-hidden')
        p_web.DivHeader('frmFulfilRequest',p_web.combine(p_web.site.style.formdiv,))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_frmFulfilRequest_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_frmFulfilRequest_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferfrmFulfilRequest',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_frmFulfilRequest',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmFulfilRequest',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_frmFulfilRequest',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmFulfilRequest',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_frmFulfilRequest',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_frmFulfilRequest',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmFulfilRequest',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_frmFulfilRequest',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmFulfilRequest',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_frmFulfilRequest',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_frmFulfilRequest',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('frmFulfilRequest')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(STOFAULT)
  p_web._OpenFile(USERS)
  p_web._OpenFile(STOCK)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(WARPARTS)
  p_web._OpenFile(PARTS)
  p_web._OpenFile(ESTPARTS)
  p_web._OpenFile(STOCKALL)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(STOFAULT)
  p_Web._CloseFile(USERS)
  p_Web._CloseFile(STOCK)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(WARPARTS)
  p_Web._CloseFile(PARTS)
  p_Web._CloseFile(ESTPARTS)
  p_Web._CloseFile(STOCKALL)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('frmFulfilRequest_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'frmFulfilRequest'
    end
    p_web.formsettings.proc = 'frmFulfilRequest'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('frmFulfilRequest_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'frmStockAllocation'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('frmFulfilRequest_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('frmFulfilRequest_ChainTo')
    loc:formaction = p_web.GetSessionValue('frmFulfilRequest_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction

GenerateForm   Routine
  do LoadRelatedRecords
  IF (p_web.IfExistsValue('FulfilType'))
      p_web.StoreValue('FulFilType')
  END
  
  IF (p_web.GSV('FulFilType') = 'SINGLE')
      IF (p_web.IfExistsValue('stl:RecordNumber'))
          p_web.StoreValue('stl:RecordNumber')
          Access:STOCKALL.Clearkey(stl:RecordNumberKey)
          stl:RecordNumber    = p_web.GSV('stl:RecordNumber')
          If Access:STOCKALL.Tryfetch(stl:RecordNumberKey) = Level:Benign
              p_web.SSV('txtErrorText','')
              p_web.SSV('txtRequestFulfilled','Request Fulfilled')
  
              Case stl:PartType
              Of 'CHA'
                  Access:PARTS.Clearkey(par:RecordNumberKey)
                  par:Record_Number   = stl:PartRecordNumber
                  If Access:PARTS.Tryfetch(par:RecordNumberKey) = Level:Benign
                  !Found
                      locStockRefNo  = par:Part_Ref_Number
                  Else ! If Access:PARTS.Tryfetch(par:RecordNumberKey) = Level:Benign
                  !Error
                      p_web.SSV('txtErrorText','The selected part does not exist. The entry will be removed from the list.')
                      p_web.SSV('txtRequestFulfilled','')
                      Relate:STOCKALL.Delete(0)
                  End !If Access:PARTS.Tryfetch(par:RecordNumberKey) = Level:Benign
              Of 'WAR'
                  Access:WARPARTS.Clearkey(wpr:RecordNumberKey)
                  wpr:Record_Number   = stl:PartRecordNumber
                  If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
                  !Found
                      locStockRefNo  = wpr:Part_Ref_Number
  
                  Else ! If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
                  !Error
                      p_web.SSV('txtErrorText','The selected part does not exist. The entry will be removed from the list.')
                      p_web.SSV('txtRequestFulfilled','')
                      Relate:STOCKALL.Delete(0)
                  End !If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
              End !Case stl:PartType
  
              If RemovePartFromStock(locStockRefNo,stl:PartRecordNumber,stl:Quantity,stl:PartType,stl:PartNumber) = Level:Benign
  
          !Added 05/12/02 - L393 / VP109 -
          !If all warranty/chargeable/estimate parts are now in stock - change job status to 330 - Parts Ordered
          !And if not using loc:UseRapidStock - if all parts allocated change job status to 310 allocated to engineer
  
                  FoundAnyPart = false
                  FoundOrderedPart = false
  
          !first check the chargeable parts
                  access:Parts.clearkey(par:Part_Number_Key)
                  par:Ref_Number = stl:JobNumber
                  set(par:Part_Number_Key,par:Part_Number_Key)
                  Loop !to find an unallocated part
                      if access:Parts.next() then
                  !leave no more jobs
                          break
                      ELSE
                          if par:Ref_Number <> stl:JobNumber then
                      !Leave no more matching jobs
                              break
                          ELSE
                      !found a part for this job
                              if par:PartAllocated then
                          !ignore this it was allocated
                              ELSE
                          !found an unallocated part!
                                  FoundAnyPart = true
                                  If par:order_number <> ''
                                      If par:date_received = ''
                                          FoundOrderedPart = 1
  
                                      Else!If par:date_received = ''
                                  !ignore this it has been received
                                      End!If par:date_received = ''
                                  End
                              END !if partAllocated
                          END !If ref_numbers disagree
                      END !if access:jobs.next()
                      if FoundAnyPart and FoundORderedPart then break.
                  End !loop to find an unallocated part
  
                  if FoundAnyPart and FoundOrderedPart then
              !I dont need to check any further
                  ELSE
              !Check warranty parts
                      access:warParts.clearkey(wpr:Part_Number_Key)
                      wpr:Ref_Number = stl:JobNumber
                      set(wpr:Part_Number_Key,wpr:Part_Number_Key)
                      Loop !to find an unallocated part
                          if access:WarParts.next() then
                      !leave no more jobs
                              break
                          ELSE
                              if wpr:Ref_Number <> stl:JobNumber then
                          !Leave no more matching jobs
                                  break
                              ELSE
                          !found a part for this job
                                  if wpr:PartAllocated then
                              !ignore this it was allocated
                                  ELSE
                              !found an unallocated part!
                                      FoundAnyPart = true
                                      If wpr:order_number <> ''
                                          If wpr:date_received = ''
                                              FoundOrderedPart = 1
                                          Else!If par:date_received = ''
                                      !ignore this it has been received
                                          End!If par:date_received = ''
                                      End
  
                                  END !if partAllocated
                              END !If ref_numbers disagree
                          END !if access:jobs.next()
                          if foundanypart and foundorderedPart then break.
                      End !loop to find an unallocated part
  
                  END !if I have what I need already
  
                  if FoundAnyPart and foundorderedPart then
              !I dont need to check this
                  ELSE
              !Check estimate parts
                      access:EstParts.clearkey(epr:Part_Number_Key)
                      epr:Ref_Number = stl:JobNumber
                      set(epr:Part_Number_Key,epr:Part_Number_Key)
                      Loop !to find an unallocated part
                          if access:EstParts.next() then
                      !leave no more jobs
                              break
                          ELSE
                              if epr:Ref_Number <> stl:JobNumber then
                          !Leave no more matching jobs
                                  break
                              ELSE
                          !found a part for this job
                                  if epr:PartAllocated then
                              !ignore this it was allocated
                                  ELSE
                              !found an unallocated part!
                                      FoundAnyPart = true
                                      If epr:order_number <> ''
                                          If epr:date_received = ''
                                              FoundOrderedPart = 1
                                          Else!If par:date_received = ''
                                      !ignore this it has been received
                                          End!If par:date_received = ''
                                      End
  
                                  END !if partAllocated
                              END !If ref_numbers disagree
                          END !if access:jobs.next()
                          if foundanypart and foundorderedpart then break.
                      End !loop to find an unallocated part
                  END !if I haven't FoundAnyPart
  
  
                  if FoundOrderedPart = false or FoundAnyPArt = false
               !change job status to 330 then to 315 as needed
                      Access:JOBS.Clearkey(job:Ref_Number_Key)
                      job:Ref_Number  = stl:JobNumber
                      If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                          Error# = 0
  
                          Pointer# = Pointer(JOBS)
                          Hold(JOBS,1)
                          Get(JOBS,Pointer#)
                          If Errorcode() = 43
  !                        Case Missive('This job (' & Clip(brw2.q.stl:JobNumber) & ') is currently in use by another station. Unable to update status. '&|
  !                            '<13,10>You will have to do this manually.','ServiceBase 3g',|
  !                            'mstop.jpg','/OK')
  !                        Of 1 ! OK Button
  !                        End ! Case Missive
  !                        Error# = 1
            !          !Post(Event:CloseWindow)
                          ELSE !If Errorcode() = 43
                        !Found
                              if FoundOrderedPart = false
                                  getstatus(330,0,'JOB')  !Spares Requested
                              END !If no outstanding orders
                              if FoundAnyPart = false
                                  getstatus(345,0,'JOB')  !In repair
                              end !found no unallocated parts
                              Access:JOBS.TryUpdate()
                          End  !If Errorcode() = 43
                          Release(JOBS)
                      END !if tryfetch on jobs
                  END !if I haven't FoundAnyPart
          !end of Added 05/12/02 - L393 / VP109 -
  
          !Remove Entry from browse - 3451 (DBH: 28-10-2003)
                  Relate:STOCKALL.Delete(0)
              End !Local.RemovePartFromStock(tmp:StockRefNo,stl:PartRecordNumber,stl:Quantity,stl:PartType,stl:PartNumber)
  
          Else ! If Access:STOCKALL.Tryfetch(stl:RecordNumberKey) = Level:Benign
      !Error
              p_web.SSV('txtErrorText','An error occurred. Try again.')
              p_web.SSV('txtRequestFulfilled','')
  
          End !If Access:STOCKALL.Tryfetch(stl:RecordNumberKey) = Level:Benign
      ELSE
          p_web.SSV('txtErrorText','An error occurred. Try again.')
          p_web.SSV('txtRequestFulfilled','')
      END
  END ! IF (p_web.GSV('FulFilType') = 'SINGLE')
  IF (p_web.GSV('FulFilType') = 'MULTIPLE')
  p_web.SSV('txtErrorText','')
  p_web.SSV('txtRequestFulfilled','Request(s) Fulfilled')
      LOOP x# = 1 TO RECORDS(RapidQueue)
          GET(RapidQueue,x#)
          IF (RapidQueue.SessionID <> p_web.SessionID)
              CYCLE
          END
          Delete(RapidQueue)
      END
  
      LOOP x# = 1 TO RECORDS(RapidJobQueue)
          GET(RapidJobQueue,x#)
          IF (RapidJobQueue.SessionID <> p_web.SessionID)
              CYCLE
          END
          Delete(RapidJobQueue)
      END
  
      Access:STOCKALL.ClearKey(stl:StatusJobNumberKey)
      stl:Location  = p_web.GSV('Default:SiteLocation')
      stl:Status    = 'WEB'
      Set(stl:StatusJobNumberKey,stl:StatusJobNumberKey)
      Loop
          If Access:STOCKALL.NEXT()
              Break
          End !If
          If stl:Location  <> p_web.GSV('Default:SiteLocation')      |
              Or stl:Status    <> 'WEB'      |
              Then Break.  ! End If
          rapque:RecordNumber = stl:RecordNumber
          rapque:SessionID = p_web.SessionID
          Add(RapidQueue)
      End !Loop
  
      Sort(RapidQueue,rapque:RecordNumber)
  
      Loop x# = 1 To Records(RapidQueue)
          Get(RapidQueue,x#)
          IF (RapidQueue.SessionID <> p_web.SessionID)
              CYCLE
          END
  
          Access:STOCKALL.Clearkey(stl:RecordNumberKEy)
          stl:RecordNumber  = rapque:RecordNumber
          If Access:STOCKALL.Tryfetch(stl:RecordNumberKEy) = Level:Benign
              !Found
              !This is from stock
              !Has this job already been tried and failed?
              Sort(RapidJobQueue,jobque:JobNumber)
              jobque:JobNumber    = stl:JobNumber
              Get(RapidJobQueue,jobque:JobNumber)
              If ~Error()
                  Cycle
              End !If ~Error()
  
              locStockRefNo = stl:PartRefNumber
  
              Case stl:PartType
              OF 'CHA'
                  Access:Parts.ClearKey(par:recordnumberkey)
                  par:Record_Number = stl:PartRecordNumber
                  IF Access:Parts.Fetch(par:recordnumberkey)
                          !Error!
                      Cycle
                  ELSE
  
                  END
              OF 'WAR'
                  Access:WarParts.ClearKey(wpr:recordnumberkey)
                  wpr:Record_Number = stl:PartRecordNumber
                  IF Access:WarParts.Fetch(wpr:recordnumberkey)
                          !Error!
                      Cycle
                  ELSE
  
                  END
              End
  
              Access:STOCK.Clearkey(sto:Ref_Number_Key)
              sto:Ref_Number  = locStockRefNo
              If Access:STOCK.Tryfetch(sto:Ref_Number_Key)
                 !Cannot find stock entry
                  Cycle
              END
              If stl:Quantity > sto:Quantity_Stock
                  !Cannot fullfill request
                  Cycle
              Else !stl:Quantity > sto:Quantity_Stock
                  !
              End !stl:Quantity > sto:Quantity_Stock
  
              locJobNumber   = stl:JobNumber
  
              If (FindOtherParts(stl:JobNumber,stl:RecordNumber) = Level:Benign)
                  !All parts are ok to be taken
                  Access:STOCKALL.ClearKey(stl:StatusJobNumberKey)
                  stl:Location  = p_web.GSV('Default:SiteLocation')
                  stl:Status    = 'WEB'
                  stl:JobNumber = locJobNumber
                  Set(stl:StatusJobNumberKey,stl:StatusJobNumberKey)
                  Loop
                      If Access:STOCKALL.NEXT()
                          Break
                      End !If
                      If stl:Location  <> p_web.GSV('Default:SiteLocation')      |
                          Or stl:Status    <> 'WEB'      |
                          Or stl:JobNumber <> locJobNumber |
                          Then Break.  ! End If
                      If RemovePartFromStock(stl:PartRefNumber,stl:PartRecordNumber,stl:Quantity,stl:PartType,stl:PartNumber) = Level:Benign
                          Relate:STOCKALL.Delete(0)
                      End !.RemoveFromStock(par:Part_Ref_Number,par:Record_Number,par:Quantity,'C',par:Part_Number)
                  End !Loop
  
              Else !If Local.FindOtherParts(stl:JobNumber,stl:RecordNumber)
                  !Not all parts can be fullfilled
                  !Add job number to queue, so no other parts for this job will be picked
                  Sort(RapidJobQueue,jobque:JobNumber)
                  jobque:JobNumber = locJobNumber
                  jobque:SessionID = p_web.SessionID
                  Add(RapidJobQueue)
              End !If Local.FindOtherParts(stl:JobNumber,stl:RecordNumber)
  
  
          Else!If Access:RAPIDSTOCK.TryFetch(stl:RecordNumberKey) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End!If Access:RAPIDSTOCK.TryFetch(stl:RecordNumberKey) = Level:Benign
  
          !Added 05/12/02 - L393 / VP109 -
          !If all warranty/chargeable/estimate parts are now in stock - change job status to 330 - Parts Ordered
          !And if not using loc:UseRapidStock - if all parts allocated change job status to 310 allocated to engineer
  
          Access:STOCKALL.Clearkey(stl:RecordNumberKey)
          stl:recordNumber    = rapque:RecordNumber
          If Access:STOCKALL.Tryfetch(stl:RecordNumberKey) = Level:Benign
              !Found
              !Reget the record to check the status bits below
          Else ! If Access:STOCKALL.Tryfetch(stl:RecordNumberKey) = Level:Benign
              !Error
          End !If Access:STOCKALL.Tryfetch(stl:RecordNumberKey) = Level:Benign
  
  
          FoundAnyPart = false
          FoundOrderedPart = false
  
          !first check the chargeable parts
          access:Parts.clearkey(par:Part_Number_Key)
          par:Ref_Number = stl:JobNumber
          set(par:Part_Number_Key,par:Part_Number_Key)
          Loop !to find an unallocated part
              if access:Parts.next() then
                  !leave no more jobs
                  break
              ELSE
                  if par:Ref_Number <> stl:JobNumber then
                      !Leave no more matching jobs
                      break
                  ELSE
                      !found a part for this job
                      if par:PartAllocated then
                          !ignore this it was allocated
                      ELSE
                          !found an unallocated part!
                          FoundAnyPart = true
                          If par:order_number <> ''
                              If par:date_received = ''
                                  FoundOrderedPart = 1
  
                              Else!If par:date_received = ''
                                  !ignore this it has been received
                              End!If par:date_received = ''
                          End
                      END !if partAllocated
                  END !If ref_numbers disagree
              END !if access:jobs.next()
              if FoundAnyPart and FoundORderedPart then break.
          End !loop to find an unallocated part
  
          if FoundAnyPart and FoundOrderedPart then
              !I dont need to check any further
          ELSE
              !Check warranty parts
              access:warParts.clearkey(wpr:Part_Number_Key)
              wpr:Ref_Number = stl:JobNumber
              set(wpr:Part_Number_Key,wpr:Part_Number_Key)
              Loop !to find an unallocated part
                  if access:WarParts.next() then
                      !leave no more jobs
                      break
                  ELSE
                      if wpr:Ref_Number <> stl:JobNumber then
                          !Leave no more matching jobs
                          break
                      ELSE
                          !found a part for this job
                          if wpr:PartAllocated then
                              !ignore this it was allocated
                          ELSE
                              !found an unallocated part!
                              FoundAnyPart = true
                              If wpr:order_number <> ''
                                  If wpr:date_received = ''
                                      FoundOrderedPart = 1
                                  Else!If par:date_received = ''
                                      !ignore this it has been received
                                  End!If par:date_received = ''
                              End
  
                          END !if partAllocated
                      END !If ref_numbers disagree
                  END !if access:jobs.next()
                  if foundanypart and foundorderedPart then break.
              End !loop to find an unallocated part
  
          END !if I have what I need already
  
          if FoundAnyPart and foundorderedPart then
              !I dont need to check this
          ELSE
              !Check estimate parts
              access:EstParts.clearkey(epr:Part_Number_Key)
              epr:Ref_Number = stl:JobNumber
              set(epr:Part_Number_Key,epr:Part_Number_Key)
              Loop !to find an unallocated part
                  if access:EstParts.next() then
                      !leave no more jobs
                      break
                  ELSE
                      if epr:Ref_Number <> stl:JobNumber then
                          !Leave no more matching jobs
                          break
                      ELSE
                          !found a part for this job
                          if epr:PartAllocated then
                              !ignore this it was allocated
                          ELSE
                              !found an unallocated part!
                              FoundAnyPart = true
                              If epr:order_number <> ''
                                  If epr:date_received = ''
                                      FoundOrderedPart = 1
                                  Else!If par:date_received = ''
                                      !ignore this it has been received
                                  End!If par:date_received = ''
                              End
  
                          END !if partAllocated
                      END !If ref_numbers disagree
                  END !if access:jobs.next()
                  if foundanypart and foundorderedpart then break.
              End !loop to find an unallocated part
          END !if I haven't FoundAnyPart
  
  
          if FoundOrderedPart = false or FoundAnyPArt = false
               !change job status to 330 then to 315 as needed
              Access:JOBS.Clearkey(job:Ref_Number_Key)
              job:Ref_Number  = stl:JobNumber
              If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                  Error# = 0
  
                  Pointer# = Pointer(JOBS)
                  Hold(JOBS,1)
                  Get(JOBS,Pointer#)
                  If Errorcode() = 43
                      CYCLE
  !                    Case Missive('This job is currently in use by another station. Unable to update status. You will have to do this manually.','ServiceBase 3g',|
  !                        'mstop.jpg','/OK')
  !                    Of 1 ! OK Button
  !                    End ! Case Missive
                      Error# = 1
            !          !Post(Event:CloseWindow)
                  ELSE !If Errorcode() = 43
                        !Found
                      if FoundOrderedPart = false
                          getstatus(330,0,'JOB')  !Spares Requested
                      END !If no outstanding orders
                      if FoundAnyPart = false
                          getstatus(345,0,'JOB')  !In repair
                      end !found no unallocated parts
                      Access:JOBS.TryUpdate()
                  End  !If Errorcode() = 43
                  Release(JOBS)
              END !if tryfetch on jobs
          END !if I haven't FoundAnyPart
  
          !end of Added 05/12/02 - L393 / VP109 -
  
      End !x# = 1 To Records(RapidQueue)
  
  END ! IF (p_web.GSV('FulFilType') = 'MULTIPLE')
      p_web.site.SaveButton.TextValue = 'OK'
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Fulfil Request') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Fulfil Request',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_frmFulfilRequest',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_frmFulfilRequest0_div')&'">'&p_web.Translate('Fulfil Request')&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="frmFulfilRequest_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="frmFulfilRequest_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'frmFulfilRequest_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="frmFulfilRequest_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'frmFulfilRequest_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_frmFulfilRequest')>0,p_web.GSV('showtab_frmFulfilRequest'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_frmFulfilRequest'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_frmFulfilRequest') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_frmFulfilRequest'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_frmFulfilRequest')>0,p_web.GSV('showtab_frmFulfilRequest'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_frmFulfilRequest') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Fulfil Request') & ''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_frmFulfilRequest_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_frmFulfilRequest')>0,p_web.GSV('showtab_frmFulfilRequest'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"frmFulfilRequest",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_frmFulfilRequest')>0,p_web.GSV('showtab_frmFulfilRequest'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_frmFulfilRequest_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('frmFulfilRequest') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('frmFulfilRequest')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Fulfil Request')&'</a></h3>' & CRLF & p_web.DivHeader('tab_frmFulfilRequest0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_frmFulfilRequest0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_frmFulfilRequest0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_frmFulfilRequest0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Fulfil Request')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_frmFulfilRequest0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Fulfil Request')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_frmFulfilRequest0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Fulfil Request')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_frmFulfilRequest0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::txtRequestFulfilled
        do Comment::txtRequestFulfilled
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::txtErrorText
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::txtErrorText
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Validate::txtRequestFulfilled  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::txtRequestFulfilled  ! copies value to session value if valid.
  do Comment::txtRequestFulfilled ! allows comment style to be updated.

ValidateValue::txtRequestFulfilled  Routine
    If not (1=0)
    End

Value::txtRequestFulfilled  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('frmFulfilRequest_' & p_web._nocolon('txtRequestFulfilled') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="txtRequestFulfilled" class="'&clip('green bold large')&'"'&clip(loc:extra)&'>' & p_web.Translate(p_web.GSV('txtRequestFulfilled'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::txtRequestFulfilled  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if txtRequestFulfilled:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('frmFulfilRequest_' & p_web._nocolon('txtRequestFulfilled') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::txtErrorText  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::txtErrorText  ! copies value to session value if valid.
  do Comment::txtErrorText ! allows comment style to be updated.

ValidateValue::txtErrorText  Routine
    If not (1=0)
    End

Value::txtErrorText  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('frmFulfilRequest_' & p_web._nocolon('txtErrorText') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="txtErrorText" class="'&clip('red bold large')&'"'&clip(loc:extra)&'>' & p_web.Translate(p_web.GSV('txtErrorText'),) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::txtErrorText  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if txtErrorText:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('frmFulfilRequest_' & p_web._nocolon('txtErrorText') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('frmFulfilRequest_nexttab_' & 0)
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_frmFulfilRequest_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('frmFulfilRequest_tab_' & 0)
    do GenerateTab0
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('frmFulfilRequest_form:ready_',1)

  p_web.SetSessionValue('frmFulfilRequest_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_frmFulfilRequest',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('frmFulfilRequest_form:ready_',1)
  p_web.SetSessionValue('frmFulfilRequest_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_frmFulfilRequest',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('frmFulfilRequest_form:ready_',1)
  p_web.SetSessionValue('frmFulfilRequest_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('frmFulfilRequest:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('frmFulfilRequest_form:ready_',1)
  p_web.SetSessionValue('frmFulfilRequest_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('frmFulfilRequest:Primed',0)
  p_web.setsessionvalue('showtab_frmFulfilRequest',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('frmFulfilRequest_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('frmFulfilRequest_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
    do ValidateValue::txtRequestFulfilled
    If loc:Invalid then exit.
    do ValidateValue::txtErrorText
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('frmFulfilRequest:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('')

RemovePartFromStock   Procedure(Long func:StockRefNumber,Long func:PartRecordNumber,Long func:Quantity,String func:PartType,String func:PartNumber)  !StockRefNumber, PartRecordNumber, Quantity, PartType, Quantity
Code
    Access:STOCK.Clearkey(sto:Ref_Number_Key)
    sto:Ref_Number  = func:StockRefNumber
    If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
        !Found
        ! #11309 Make sure STock Item is not in use (DBH: 30/06/2010)
        IF (BHFileInUse(STOCK))
            p_web.SSV('txtErrorText','Error: The selected stock item is in use.')
            p_web.SSV('txtRequestFulfilled','')
            Return Level:Fatal
        End

        If sto:Quantity_Stock < func:Quantity
            p_web.SSV('txtErrorText','Error: There are insufficient items in stock.')
            p_web.SSV('txtRequestFulfilled','')
            !There isn't enough in stock anymore
            Return Level:Fatal
        End !If sto:Quantity < func:Quantity

        If func:PartNumber = 'EXCH'
!            Case Missive('The selected part is an "Exchange Part".'&|
!                '<13,10>Click "Allocate Exchange" to allocate an exchange unit.','ServiceBase 3g',|
!                'mstop.jpg','/OK')
!            Of 1 ! OK Button
!            End ! Case Missive
            p_web.SSV('txtErrorText','Error: This in an "Exchange Part"')
            p_web.SSV('txtRequestFulfilled','')
        Else !If stl:PartNumber = 'EXCH'
            Error# = 0
            Case func:PartType
            Of 'CHA'
                Access:PARTS.Clearkey(par:recordnumberkey)
                par:Record_Number   = func:PartRecordNumber
                If Access:PARTS.Tryfetch(par:recordnumberkey) = Level:Benign
                        !Found
                    Access:JOBS.Clearkey(job:Ref_Number_Key)
                    job:Ref_Number  = par:Ref_Number
                    If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                            !Found

                    Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                            !Error
                        p_web.SSV('txtErrorText','Error: Cannot find the selected job.')
                        p_web.SSV('txtRequestFulfilled','')
                        Return Level:Fatal
                    End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                    Access:STOCK.Clearkey(sto:Ref_Number_Key)
                    sto:Ref_Number  = par:Part_Ref_Number
                    If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                            !Found
                            ! #11309 Make sure STock Item is not in use (DBH: 30/06/2010)
                        IF (BHFileInUse(STOCk))
                            p_web.SSV('txtErrorText','Error: The selected stock part is in use')
                            p_web.SSV('txtRequestFulfilled','')
                            Return Level:Fatal
                        End

                        If sto:ReturnFaultySpare

                            Access:USERS.Clearkey(use:User_Code_Key)
                            use:User_Code = job:Engineer
                            If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                                  !Found

                            Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                                  !Error
                            End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                            p_web.SSV('txtErrorText','This faulty part must be returned before a new part can be issued')
!                            Case Missive('This faulty part must be returned before a new part can be issued:'&|
!                                '<13,10>Part: ' & Clip(sto:Part_Number) & ' - ' & Clip(sto:Description) & '.'&|
!                                '<13,10>Job: ' & Clip(par:Ref_Number) & ' - ' & Clip(use:Forename) & ' ' & Clip(use:Surname) & '.','ServiceBase 3g',|
!                                'mquest.jpg','Decline|Confirm')
!                            Of 2 ! Confirm Button
                                AddFaulty('C')
!                            Of 1 ! Decline Button
!                                Error# = 1
!                            End ! Case Missive
                        End !If sto:ReturnFaultySpare
                    Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                            !Error
                    End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                Else ! If Access:PARTS.Tryfetch(par:Record_Number_Key) = Level:Benign
                        !Error
                    Error# = 1
                End !If AccessPARTS.Tryfetch(parRecord_Number_Key) = LevelBenign
            Of 'WAR'
                Access:WARPARTS.Clearkey(wpr:RecordNumberKey)
                wpr:Record_Number   = func:PartRecordNumber
                If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
                        !Found
                    Access:JOBS.Clearkey(job:Ref_Number_Key)
                    job:Ref_Number  = wpr:Ref_Number
                    If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                            !Found

                    Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                            !Error
                        p_web.SSV('txtErrorText','Error: Cannot find the selected job.')
                        p_web.SSV('txtRequestFulfilled','')
                        Return Level:Fatal

                    End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                    Access:STOCK.Clearkey(sto:Ref_Number_Key)
                    sto:Ref_Number  = wpr:Part_Ref_Number
                    If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                            !Found
                            ! #11309 Make sure STock Item is not in use (DBH: 30/06/2010)
                        IF (BHFileInUse(STOCK))
                            p_web.SSV('txtErrorText','Error: The selected stock item is in use')
                            p_web.SSV('txtRequestFulfilled','')
                            Return Level:Fatal
                        End

                        If sto:ReturnFaultySpare
                            Access:USERS.Clearkey(use:User_Code_Key)
                            use:User_Code = job:Engineer
                            If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                                  !Found

                            Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                                  !Error
                            End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                            p_web.SSV('txtErrorText','This faulty part must be returned before a new part can be issued')
!                            Case Missive('This faulty part must be returned before a new part can be issued:'&|
!                                '<13,10>Part: ' & Clip(sto:Part_Number) & ' - ' & Clip(sto:Description) & '.'&|
!                                '<13,10>Job: ' & Clip(job:Ref_Number) & ' - ' & Clip(use:Forename) & ' ' & Clip(use:Surname) & '.','ServiceBase 3g',|
!                                'mquest.jpg','Decline|Confirm')
!                            Of 2 ! Confirm Button
                                AddFaulty('W')
!                            Of 1 ! Decline Button
!                                Error# = 1
!                            End ! Case Missive

                        End !If sto:ReturnFaultySpare

                    Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                            !Error
                    End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign

                Else ! If Access:WARPARTS.Tryfetch(wpr:Record_Number_Key) = Level:Benign
                        !Error
                    Error# = 1
                End !If AccessWARPARTS.Tryfetch(wprRecord_Number_Key) = LevelBenign
            Of 'EST'
                Access:ESTPARTS.Clearkey(epr:Record_Number_Key)
                epr:Record_Number   = func:PartRecordNumber
                If Access:ESTPARTS.Tryfetch(epr:Record_Number_Key) = Level:Benign
                        !Found
                    Access:JOBS.Clearkey(job:Ref_Number_Key)
                    job:Ref_Number  = epr:Ref_Number
                    If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                            !Found

                    Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                            !Error
                        p_web.SSV('txtErrorText','Error: Cannot find the selected job.')
                        p_web.SSV('txtRequestFulfilled','')
                        Return Level:Fatal

                    End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                    Access:STOCK.Clearkey(sto:Ref_Number_Key)
                    sto:Ref_Number  = epr:Part_Ref_Number
                    If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                            !Found
                            ! #11309 Make sure STock Item is not in use (DBH: 30/06/2010)
                        IF (BHFileINUse(STOCK))
                            p_web.SSV('txtErrorText','Error: The selected stock item is in use')
                            p_web.SSV('txtRequestFulfilled','')
                            Return Level:Fatal
                        End

                        If sto:ReturnFaultySpare
                            Access:USERS.Clearkey(use:User_Code_Key)
                            use:User_Code = job:Engineer
                            If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                                  !Found

                            Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                                  !Error
                            End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                            p_web.SSV('txtErrorText','This faulty part must be returned before a new part can be issued')
!                            Case Missive('This faulty part must be returned before a new part can be issued:'&|
!                                '<13,10>Part: ' & Clip(sto:Part_Number) & ' - ' & Clip(sto:Description) & '.'&|
!                                '<13,10>Job: ' & Clip(job:Ref_Number) & ' - ' & Clip(use:Forename) & ' ' & Clip(use:Surname) & '.','ServiceBase 3g',|
!                                'mquest.jpg','Decline|Confirm')
!                            Of 2 ! Confirm Button
                                AddFaulty('E')
!                            Of 1 ! Decline Button
!                                Error# = 1
!                            End ! Case Missive

                        End !If sto:ReturnFaultySpare

                    Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                            !Error
                    End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign

                Else ! If Access:ESTPARTS.Tryfetch(epr:RecordNumberKey) = Level:Benign
                        !Error
                    Error# = 1
                End !If Access:ESTPARTS.Tryfetch(epr:RecordNumberKey) = Level:Benign
            End !Case stl:PartType
        End !If func:PartNumber = 'EXCH'

        If Error# = 0
            sto:Quantity_Stock  -= func:Quantity
            If sto:Quantity_Stock < 0
                sto:Quantity_Stock = 0
            End !If sto:Quantity_Stock < 0
            IF Access:STOCK.Update()
                !Error
            END

            Case func:PartType
            Of 'CHA'
                Access:PARTS.Clearkey(par:RecordNumberKey)
                par:Record_Number   = func:PartRecordNumber
                If Access:PARTS.Tryfetch(par:RecordNumberKey) = Level:Benign
                    ! Found
                    If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                        'DEC', | ! Transaction_Type
                        par:Despatch_Note_Number, | ! Depatch_Note_Number
                        par:Ref_Number, | ! Job_Number
                        0, | ! Sales_Number
                        func:Quantity, | ! Quantity
                        par:Purchase_Cost, | ! Purchase_Cost
                        par:Sale_Cost, | ! Sale_Cost
                        par:Retail_Cost, | ! Retail_Cost
                        'STOCK DECREMENTED', | ! Notes
                        '', |
                        p_web.GSV('BookingUsercode'), |
                        sto:Quantity_Stock) ! Information
                        ! Added OK

                    Else ! AddToStockHistory
                        ! Error
                    End ! AddToStockHistory
                    par:WebOrder    = False
                    par:Status      = ''
                    par:PartAllocated   = 1
                    Access:PARTS.Update()
                Else ! If Access:PARTS.Tryfetch(par:RecordNumberKey) = Level:Benign
                    ! Error
                End ! If Access:PARTS.Tryfetch(par:RecordNumberKey) = Level:Benign
            Of 'WAR'
                Access:WARPARTS.Clearkey(wpr:RecordNumberKey)
                wpr:Record_Number   = func:PartRecordNumber
                If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
                    ! Found
                    If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                        'DEC', | ! Transaction_Type
                        wpr:Despatch_Note_Number, | ! Depatch_Note_Number
                        wpr:Ref_Number, | ! Job_Number
                        0, | ! Sales_Number
                        func:Quantity, | ! Quantity
                        wpr:Purchase_Cost, | ! Purchase_Cost
                        wpr:Sale_Cost, | ! Sale_Cost
                        wpr:Retail_Cost, | ! Retail_Cost
                        'STOCK DECREMENTED', | ! Notes
                        '', |
                        p_web.GSV('BookingUsercode'), |
                        sto:Quantity_Stock) ! Information
                        ! Added OK

                    Else ! AddToStockHistory
                        ! Error
                    End ! AddToStockHistory
                    wpr:WebOrder    = False
                    wpr:Status      = ''
                    wpr:PartAllocated   = 1
                    Access:WARPARTS.Update()
                Else ! If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
                    ! Error
                End ! If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
            End ! Case func:PartType
            ! #10396 Suspend part if none in stock and suspended at main store. (DBH: 16/07/2010)
            If (sto:Location <> VodacomClass.MainStoreLocation())
                If (VodacomClass.MainStoreSuspended(sto:Part_Number))
                    if (sto:Quantity_Stock = 0)
                    ! Suspend Item
                        sto:Suspend = 1
                        If (Access:STOCK.TryUpdate() = Level:Benign)
                            if (AddToStockHistory(sto:Ref_Number, |
                                'ADD',|
                                '',|
                                0, |
                                0, |
                                0, |
                                sto:Purchase_Cost,|
                                sto:Sale_Cost, |
                                sto:Retail_Cost, |
                                'PART SUSPENDED',|
                                '', |
                                p_web.GSV('BookingUserCode'), |
                                sto:Quantity_Stock))
                            end
                        ENd ! If (Access:STOCK.TryUpdate() = Level:Benign)
                    else
                    ! Show warning
!                        Beep(Beep:SystemExclamation)  ;  Yield()
!                        Case Missive('This part has been suspended at Main Store. It will not be available for ordering.'&|
!                            '|'&|
!                            '|Current Stock Level: ' & Clip(sto:Quantity_stock) & '.','ServiceBase',|
!                            'mexclam.jpg','/&OK')
!                        Of 1 ! &OK Button
!                        End!Case Message
                    end
                end ! If (MainStoreSuspened(sto:Part_Number))
            End
        Else !If Error# = 0
            Return Level:Fatal
        End !If Error# = 0
    Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
        p_web.SSV('txtErrorText','Error: The selected stock item cannot be found. The line will be removed.')
        p_web.SSV('txtRequestFulfilled','')
        RemoveFromStockAllocation(func:PartRecordNumber,func:PartType)

!        !Error
!        !Assert(0,'<13,10>Fetch Error<13,10>')
!        Access:JOBS.Clearkey(job:Ref_Number_Key)
!        job:Ref_Number  = stl:JobNumber
!        If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
!            !Found
!
!        Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
!            !Error
!        End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
!
!        Access:USERS.Clearkey(use:User_Code_Key)
!        use:User_COde   = job:Engineer
!        If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
!            !Found
!
!        Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
!            !Error
!        End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
!
!        !Blank entries are still being left after a refresh.
!        !This will ensure that they can be deleted. - 3451 (DBH: 28-10-2003)
!        Case Missive('Unable to fund the selected part in Stock Control.'&|
!            '<13,10>Part:' & Clip(stl:PartNumber) & ' - ' & Clip(stl:Description) & '.'&|
!            '<13,10>Job: ' & Clip(job:Ref_NUmber) & ' - ' & Clip(use:Forename) & ' ' & Clip(use:Surname) &|
!            '<13,10>Do you wish to REMOVE this part line from Stock Allocation?','ServiceBase 3g',|
!            'mquest.jpg','\Cancel|Remove')
!        Of 2 ! Remove Button
!            RemoveFromStockAllocation(func:PartRecordNumber,func:PartType)
!        Of 1 ! Cancel Button
!        End ! Case Missive
!        Return Level:Fatal
    End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
    Return Level:Benign
AddFaulty     Procedure(String  func:Type)
Code
    If Access:STOFAULT.PrimeRecord() = Level:Benign
        Case func:Type
        Of 'W'
            stf:PartNumber  = wpr:Part_Number
            stf:Description = wpr:Description
            stf:Quantity    = wpr:Quantity
            stf:PurchaseCost    = wpr:Purchase_Cost
        Of 'C'
            stf:PartNumber  = par:Part_Number
            stf:Description = par:Description
            stf:Quantity    = par:Quantity
            stf:PurchaseCost    = par:Purchase_Cost
        Of 'E'
            stf:PartNumber  = epr:Part_Number
            stf:Description = epr:Description
            stf:Quantity    = epr:Quantity
            stf:PurchaseCost    = epr:Purchase_Cost
        End !Case func:Type

        stf:ModelNumber = job:Model_Number
        stf:IMEI        = job:ESN
        stf:Engineer    = job:Engineer
        stf:StoreUserCode   = p_web.GSV('BookingUserCode')
        stf:PartType        = 'FAU'
        If Access:STOFAULT.TryInsert() = Level:Benign
            !Insert Successful

        Else !If Access:STOFAULT.TryInsert() = Level:Benign
            !Insert Failed
            Access:STOFAULT.CancelAutoInc()
        End !If Access:STOFAULT.TryInsert() = Level:Benign
    End !If Access:STOFAULT.PrimeRecord() = Level:Benign
FindOtherParts        Procedure(Long func:JobNumber,Long func:RecordNumber)    !Job Number, Record Number
Code
    Found# = 0
        !Lets find some other parts in the allocation list

    Access:STOCKALL.ClearKey(stl:StatusJobNumberKey)
    stl:Location  = p_web.GSV('Default:SiteLocation')
    stl:Status    = 'WEB'
    Set(stl:StatusJobNumberKey,stl:StatusJobNumberKey)
    Loop
        If Access:STOCKALL.NEXT()
            Break
        End !If
        If stl:Location  <> p_web.GSV('Default:SiteLocation')      |
            Or stl:Status    <> 'WEB'      |
            Then Break.  ! End If

            !Only look for parts from the same job
        If stl:JobNumber <> func:JobNumber
            Cycle
        End !If stl:JobNumber <> tmp:JobNumber
            !Make sure you haven't found the original record
        If stl:RecordNumber = func:RecordNumber
            Cycle
        End !If stl:RecordNumber = rapque:RecordNumber

            !Are there any in stock
        Case stl:PartType
        Of 'CHA'
            Access:PARTS.Clearkey(par:RecordNumberKey)
            par:Record_Number   = stl:PartRecordNumber
            If Access:PARTS.Tryfetch(par:RecordNumberKey) = Level:Benign
                        !Found
                Access:STOCK.Clearkey(sto:Ref_Number_Key)
                sto:Ref_Number  = par:Part_Ref_Number
                If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                            !Found
                    If par:Quantity > sto:Quantity_Stock
                        Found# = 1
                        Break
                    End !If par:Quantity < sto:Quantity_Stock
                Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                            !Error
                    Found# = 1
                    Break
                End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
            Else ! If Access:PARTS.Tryfetch(par:RecordNumberKey) = Level:Benign
                        !Error
            End !If Access:PARTS.Tryfetch(par:RecordNumberKey) = Level:Benign
        Of 'WAR'
            Access:WARPARTS.Clearkey(wpr:RecordNumberKey)
            wpr:Record_Number   = stl:PartRecordNumber
            If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
                        !Found
                Access:STOCK.Clearkey(sto:Ref_Number_Key)
                sto:Ref_Number  = wpr:Part_Ref_Number
                If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                            !Found
                    If wpr:Quantity > sto:Quantity_Stock
                        Found# = 1
                        Break
                    End !If wpr:Quantity > sto:Quantity_Stock
                Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                            !Error
                    Found# = 1
                    Break
                End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
            Else ! If Access:WARPARTS.Tryfetch(war:RecordNumberKey) = Level:Benign
                        !Error
            End !If Access:WARPARTS.Tryfetch(war:RecordNumberKey) = Level:Benign
        End !Case stl:PartType
    End !Loop

    Return Found#
