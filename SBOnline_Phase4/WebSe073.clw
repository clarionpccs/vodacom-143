

   MEMBER('WebServer_Phase4.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSE073.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSE021.INC'),ONCE        !Req'd for module callout resolution
                     END


SendAnEmail          PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
FilesOpened     Long
messageText:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  GlobalErrors.SetProcedureName('SendAnEmail')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'SendAnEmail_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('SendAnEmail','')
    p_web.DivHeader('SendAnEmail',p_web.combine(p_web.site.style.formdiv,'fdiv'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('SendAnEmail') = 0
        p_web.AddPreCall('SendAnEmail')
        p_web.DivHeader('popup_SendAnEmail','nt-hidden')
        p_web.DivHeader('SendAnEmail',p_web.combine(p_web.site.style.formdiv,'fdiv'))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_SendAnEmail_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_SendAnEmail_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferSendAnEmail',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_SendAnEmail',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferSendAnEmail',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_SendAnEmail',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferSendAnEmail',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_SendAnEmail',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_SendAnEmail',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferSendAnEmail',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_SendAnEmail',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferSendAnEmail',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_SendAnEmail',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_SendAnEmail',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('SendAnEmail')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
      p_web.StoreValue('returnURL')
  p_web.SetValue('SendAnEmail_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'SendAnEmail'
    end
    p_web.formsettings.proc = 'SendAnEmail'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('SendAnEmail_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.GSV('returnURL')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('SendAnEmail_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('SendAnEmail_ChainTo')
    loc:formaction = p_web.GetSessionValue('SendAnEmail_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = p_web.GSV('returnURL')

GenerateForm   Routine
  do LoadRelatedRecords
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Send Email') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Send Email',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_SendAnEmail',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_SendAnEmail0_div')&'">'&p_web.Translate()&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="SendAnEmail_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.cancel();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CancelButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="SendAnEmail_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'SendAnEmail_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="SendAnEmail_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'SendAnEmail_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_SendAnEmail')>0,p_web.GSV('showtab_SendAnEmail'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_SendAnEmail'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_SendAnEmail') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_SendAnEmail'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_SendAnEmail')>0,p_web.GSV('showtab_SendAnEmail'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_SendAnEmail') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_SendAnEmail_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_SendAnEmail')>0,p_web.GSV('showtab_SendAnEmail'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"SendAnEmail",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_SendAnEmail')>0,p_web.GSV('showtab_SendAnEmail'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_SendAnEmail_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('SendAnEmail') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('SendAnEmail')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate()&'</a></h3>' & CRLF & p_web.DivHeader('tab_SendAnEmail0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_SendAnEmail0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_SendAnEmail0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_SendAnEmail0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,)
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_SendAnEmail0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend)
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_SendAnEmail0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain">' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_SendAnEmail0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::messageText
        do Comment::messageText
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Validate::messageText  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::messageText  ! copies value to session value if valid.
  do Comment::messageText ! allows comment style to be updated.

ValidateValue::messageText  Routine
    If not (1=0)
    End

Value::messageText  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('SendAnEmail_' & p_web._nocolon('messageText') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="messageText" class="'&clip('green bold')&'"'&clip(loc:extra)&'>' & p_web.Translate('Are you sure you want to resend an Email?',) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::messageText  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if messageText:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('SendAnEmail_' & p_web._nocolon('messageText') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('SendAnEmail_nexttab_' & 0)
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_SendAnEmail_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('SendAnEmail_tab_' & 0)
    do GenerateTab0
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('SendAnEmail_form:ready_',1)

  p_web.SetSessionValue('SendAnEmail_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_SendAnEmail',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('SendAnEmail_form:ready_',1)
  p_web.SetSessionValue('SendAnEmail_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_SendAnEmail',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('SendAnEmail_form:ready_',1)
  p_web.SetSessionValue('SendAnEmail_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('SendAnEmail:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('SendAnEmail_form:ready_',1)
  p_web.SetSessionValue('SendAnEmail_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('SendAnEmail:Primed',0)
  p_web.setsessionvalue('showtab_SendAnEmail',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('SendAnEmail_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
      AddEmailSMS(p_web.GSV('job:Ref_Number')|
      ,p_web.GSV('wob:HeadAccountNumber'),'COMP','EMAIL','', |
          p_web.GSV('jobe2:EmailAlertAddress'),0,'')
  p_web.DeleteSessionValue('SendAnEmail_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
    loc:InvalidTab += 1
    do ValidateValue::messageText
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('SendAnEmail:Primed',0)
  p_web.StoreValue('')

PickContactHistoryNotes PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locPickList          STRING(10000)                         !
locFinalList         STRING(10000)                         !
locFinalListField    STRING(10000)                         !
locFoundField        STRING(10000)                         !
FilesOpened     Long
CONTHIST::State  USHORT
NOTESCON::State  USHORT
title:IsInvalid  Long
locPickList:IsInvalid  Long
locFinalList:IsInvalid  Long
button:AddSelected:IsInvalid  Long
button:RemoveSelected:IsInvalid  Long
gap:IsInvalid  Long
button:RemoveAll:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  GlobalErrors.SetProcedureName('PickContactHistoryNotes')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'PickContactHistoryNotes_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('PickContactHistoryNotes','')
    p_web.DivHeader('PickContactHistoryNotes',p_web.combine(p_web.site.style.formdiv,'fdiv'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('PickContactHistoryNotes') = 0
        p_web.AddPreCall('PickContactHistoryNotes')
        p_web.DivHeader('popup_PickContactHistoryNotes','nt-hidden')
        p_web.DivHeader('PickContactHistoryNotes',p_web.combine(p_web.site.style.formdiv,'fdiv'))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_PickContactHistoryNotes_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_PickContactHistoryNotes_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferPickContactHistoryNotes',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_PickContactHistoryNotes',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPickContactHistoryNotes',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_PickContactHistoryNotes',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPickContactHistoryNotes',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_PickContactHistoryNotes',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_PickContactHistoryNotes',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPickContactHistoryNotes',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_PickContactHistoryNotes',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPickContactHistoryNotes',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_PickContactHistoryNotes',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_PickContactHistoryNotes',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('PickContactHistoryNotes')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
clearVariables      ROUTINE
    p_web.DeleteSessionValue('locPickList')
    p_web.DeleteSessionValue('locFinalList')
    p_web.DeleteSessionValue('locFinalListField')
    p_web.DeleteSessionValue('locFoundField')
OpenFiles  ROUTINE
  p_web._OpenFile(CONTHIST)
  p_web._OpenFile(NOTESCON)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(CONTHIST)
  p_Web._CloseFile(NOTESCON)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('PickContactHistoryNotes_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'PickContactHistoryNotes'
    end
    p_web.formsettings.proc = 'PickContactHistoryNotes'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine
  do clearVariables

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  if p_web.IfExistsValue('locPickList') = 0
    p_web.SetSessionValue('locPickList',locPickList)
  Else
    locPickList = p_web.GetSessionValue('locPickList')
  End
  if p_web.IfExistsValue('locFinalList') = 0
    p_web.SetSessionValue('locFinalList',locFinalList)
  Else
    locFinalList = p_web.GetSessionValue('locFinalList')
  End

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locPickList')
    locPickList = p_web.GetValue('locPickList')
    p_web.SetSessionValue('locPickList',locPickList)
  Else
    locPickList = p_web.GetSessionValue('locPickList')
  End
  if p_web.IfExistsValue('locFinalList')
    locFinalList = p_web.GetValue('locFinalList')
    p_web.SetSessionValue('locFinalList',locFinalList)
  Else
    locFinalList = p_web.GetSessionValue('locFinalList')
  End

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('PickContactHistoryNotes_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'FormContactHistory'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('PickContactHistoryNotes_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('PickContactHistoryNotes_ChainTo')
    loc:formaction = p_web.GetSessionValue('PickContactHistoryNotes_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'FormContactHistory'

GenerateForm   Routine
  do LoadRelatedRecords
 locPickList = p_web.RestoreValue('locPickList')
 locFinalList = p_web.RestoreValue('locFinalList')
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Contact History Notes') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Contact History Notes',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_PickContactHistoryNotes',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_PickContactHistoryNotes0_div')&'">'&p_web.Translate()&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="PickContactHistoryNotes_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.cancel();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CancelButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="PickContactHistoryNotes_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'PickContactHistoryNotes_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="PickContactHistoryNotes_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'PickContactHistoryNotes_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locPickList')
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_PickContactHistoryNotes')>0,p_web.GSV('showtab_PickContactHistoryNotes'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_PickContactHistoryNotes'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_PickContactHistoryNotes') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_PickContactHistoryNotes'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_PickContactHistoryNotes')>0,p_web.GSV('showtab_PickContactHistoryNotes'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_PickContactHistoryNotes') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_PickContactHistoryNotes_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_PickContactHistoryNotes')>0,p_web.GSV('showtab_PickContactHistoryNotes'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"PickContactHistoryNotes",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_PickContactHistoryNotes')>0,p_web.GSV('showtab_PickContactHistoryNotes'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_PickContactHistoryNotes_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('PickContactHistoryNotes') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('PickContactHistoryNotes')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate()&'</a></h3>' & CRLF & p_web.DivHeader('tab_PickContactHistoryNotes0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_PickContactHistoryNotes0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_PickContactHistoryNotes0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_PickContactHistoryNotes0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,)
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_PickContactHistoryNotes0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend)
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_PickContactHistoryNotes0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain">' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_PickContactHistoryNotes0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::title
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locPickList
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locPickList
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locFinalList
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locFinalList
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::button:AddSelected
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::button:AddSelected
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::button:RemoveSelected
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::button:RemoveSelected
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::gap
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::button:RemoveAll
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Validate::title  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::title  ! copies value to session value if valid.

ValidateValue::title  Routine
    If not (1=0)
    End

Value::title  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickContactHistoryNotes_' & p_web._nocolon('title') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="title" class="'&clip('green bold')&'"'&clip(loc:extra)&'>' & p_web.Translate('Hold "CTRL" or "SHIFT" to select more than one entry.',) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::locPickList  Routine
  packet = clip(packet) & p_web.DivHeader('PickContactHistoryNotes_' & p_web._nocolon('locPickList') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locPickList  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locPickList = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locPickList = p_web.GetValue('Value')
  End
  do ValidateValue::locPickList  ! copies value to session value if valid.
  do Value::locFinalList  !1

ValidateValue::locPickList  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locPickList',locPickList).
    End

Value::locPickList  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickContactHistoryNotes_' & p_web._nocolon('locPickList') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,p_web.site.style.formselect,'FormEntry',)
  If loc:retrying
    locPickList = p_web.RestoreValue('locPickList')
    do ValidateValue::locPickList
    If locPickList:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- DROPLIST ---
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locPickList'',''pickcontacthistorynotes_locpicklist_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locPickList')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('locPickList',loc:fieldclass,loc:readonly,30,420,1,loc:javascript,)
  loc:fieldclass = ''
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('locPickList') = 0
    p_web.SetSessionValue('locPickList','')
  end
    packet = clip(packet) & p_web.CreateOption('---- Select Note -----','',choose('' = p_web.getsessionvalue('locPickList')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
      access:notescon.clearkey(noc:Notes_Key)
      noc:Reference = ''
      SET(noc:Notes_Key,noc:Notes_Key)
      LOOP
          IF (access:notescon.next())
              BREAK
          END
          If Instring(';' & Clip(noc:notes),p_web.GSV('locFinalListField'),1,1)
              Cycle
          End ! If Instring(';' & Clip(acr:Accessory) & ';',p_web.GSV('locFinalListField'),1,1)
  
          loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
          packet = clip(packet) & p_web.CreateOption(CLIP(noc:Reference) & ' - ' & Clip(noc:Notes),noc:Notes,choose(noc:Notes = p_web.GSV('locPickList')),clip(loc:rowstyle),,)&CRLF
          loc:even = Choose(loc:even=1,2,1)
      END
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web.DivFooter()

Prompt::locFinalList  Routine
  packet = clip(packet) & p_web.DivHeader('PickContactHistoryNotes_' & p_web._nocolon('locFinalList') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locFinalList  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locFinalList = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locFinalList = p_web.GetValue('Value')
  End
  do ValidateValue::locFinalList  ! copies value to session value if valid.
  do Value::locFinalList
  do SendAlert
  do Value::locPickList  !1

ValidateValue::locFinalList  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locFinalList',locFinalList).
    End

Value::locFinalList  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickContactHistoryNotes_' & p_web._nocolon('locFinalList') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,p_web.site.style.formselect,'FormEntry',)
  If loc:retrying
    locFinalList = p_web.RestoreValue('locFinalList')
    do ValidateValue::locFinalList
    If locFinalList:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- DROPLIST ---
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locFinalList'',''pickcontacthistorynotes_locfinallist_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locFinalList')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('locFinalList',loc:fieldclass,loc:readonly,30,350,,loc:javascript,)
  loc:fieldclass = ''
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('locFinalList') = 0
    p_web.SetSessionValue('locFinalList','')
  end
    packet = clip(packet) & p_web.CreateOption('---- Selected Notes -----','',choose('' = p_web.getsessionvalue('locFinalList')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
      If p_web.GSV('locFinalListField') <> ''
          Loop x# = 1 To 10000
              If Sub(p_web.GSV('locFinalListField'),x#,2) = '|;'
                  Start# = x# + 2
                  Cycle
              End ! If Sub(p_web.GSV('locFinalListField'),x#,2) = '|;'
  
              If Start# > 0
                  If Sub(p_web.GSV('locFinalListField'),x#,2) = ';|'
                      locFoundField = Sub(p_web.GSV('locFinalListField'),Start#,x# - Start#)
  
                      If locFoundField <> ''
                          loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
                          packet = clip(packet) & p_web.CreateOption(CLIP(locFoundField),locFoundField,choose(locFoundField = p_web.GSV('locFinalList')),clip(loc:rowstyle),,)&CRLF
                          loc:even = Choose(loc:even=1,2,1)
                      End ! If tmp:FoundAccessory <> ''
                      Start# = 0
                  End ! If Sub(p_web.GSV('locFinalListField'),x#,2) = ';|'
              End ! If Start# > 0
          End ! Loop x# = 1 To 1000
  
          If Start# > 0
              locFoundField = Clip(Sub(p_web.GSV('locFinalListField'),Start#,30))
              If locFoundField <> ''
                  loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
                  packet = clip(packet) & p_web.CreateOption(CLIP(locFoundField),locFoundField,choose(locFoundField = p_web.GSV('locFinalList')),clip(loc:rowstyle),,)&CRLF
                  loc:even = Choose(loc:even=1,2,1)
              End ! If tmp:FoundAccessory <> ''
          End ! If Start# > 0
      End ! If p_web.GSV('locFinalListField') <> ''
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web.DivFooter()

Prompt::button:AddSelected  Routine
  packet = clip(packet) & p_web.DivHeader('PickContactHistoryNotes_' & p_web._nocolon('button:AddSelected') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::button:AddSelected  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::button:AddSelected  ! copies value to session value if valid.
      p_web.SSV('locFinalListField',p_web.GSV('locFinalListField') & p_web.GSV('locPickList'))
      p_web.SSV('locPickList','')
  do Value::button:AddSelected
  do Value::locFinalList  !1
  do Value::locPickList  !1

ValidateValue::button:AddSelected  Routine
    If not (1=0)
    End

Value::button:AddSelected  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickContactHistoryNotes_' & p_web._nocolon('button:AddSelected') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''button:AddSelected'',''pickcontacthistorynotes_button:addselected_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','AddSelected','Add Selected',p_web.combine(Choose('Add Selected' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'SmallButtonFixed'),loc:formname,,,,loc:javascript,loc:disabled,,,,,,,,,,'nt-left')!h
  do SendPacket
  End
  p_web.DivFooter()

Prompt::button:RemoveSelected  Routine
  packet = clip(packet) & p_web.DivHeader('PickContactHistoryNotes_' & p_web._nocolon('button:RemoveSelected') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::button:RemoveSelected  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::button:RemoveSelected  ! copies value to session value if valid.
      locFinalListField = p_web.GSV('locFinalListField') & ';'
      locFinalList = '|;' & Clip(p_web.GSV('locFinalList')) & ';'
      Loop x# = 1 To 10000
          pos# = Instring(Clip(locFinalList),locFinalListField,1,1)
          If pos# > 0
              locFinalListField = Sub(locFinalListField,1,pos# - 1) & Sub(locFinalListField,pos# + Len(Clip(locFinalList)),10000)
              Break
          End ! If pos# > 0#
      End ! Loop x# = 1 To 1000
      p_web.SSV('locFinalListField',Sub(locFinalListField,1,Len(Clip(locFinalListField)) - 1))
      p_web.SSV('locFinalList','')
  do Value::button:RemoveSelected
  do Value::locFinalList  !1
  do Value::locPickList  !1

ValidateValue::button:RemoveSelected  Routine
    If not (1=0)
    End

Value::button:RemoveSelected  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickContactHistoryNotes_' & p_web._nocolon('button:RemoveSelected') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''button:RemoveSelected'',''pickcontacthistorynotes_button:removeselected_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','DeleteSelected','Delete Selected',p_web.combine(Choose('Delete Selected' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'SmallButtonFixed'),loc:formname,,,,loc:javascript,loc:disabled,,,,,,,,,,'nt-left')!h
  do SendPacket
  End
  p_web.DivFooter()

Validate::gap  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::gap  ! copies value to session value if valid.

ValidateValue::gap  Routine
    If not (1)
    End

Value::gap  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickContactHistoryNotes_' & p_web._nocolon('gap') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Validate::button:RemoveAll  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::button:RemoveAll  ! copies value to session value if valid.
      p_web.SetSessionValue('locFinalListField','')
      p_web.SetSessionValue('locFinalList','')
      p_web.SetSessionValue('locPickList','')
  do Value::button:RemoveAll
  do Value::locFinalList  !1
  do Value::locPickList  !1

ValidateValue::button:RemoveAll  Routine
    If not (1=0)
    End

Value::button:RemoveAll  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('PickContactHistoryNotes_' & p_web._nocolon('button:RemoveAll') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''button:RemoveAll'',''pickcontacthistorynotes_button:removeall_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','RemoveAll','Remove All',p_web.combine(Choose('Remove All' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'SmallButtonFixed'),loc:formname,,,,loc:javascript,loc:disabled,,,,,,,,,,'nt-left')!h
  do SendPacket
  End
  p_web.DivFooter()

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('PickContactHistoryNotes_nexttab_' & 0)
    locPickList = p_web.GSV('locPickList')
    do ValidateValue::locPickList
    If loc:Invalid
      loc:retrying = 1
      do Value::locPickList
      !do SendAlert
      !exit
    End
    locFinalList = p_web.GSV('locFinalList')
    do ValidateValue::locFinalList
    If loc:Invalid
      loc:retrying = 1
      do Value::locFinalList
      !do SendAlert
      !exit
    End
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_PickContactHistoryNotes_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('PickContactHistoryNotes_tab_' & 0)
    do GenerateTab0
  of lower('PickContactHistoryNotes_locPickList_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locPickList
      of event:timer
        do Value::locPickList
      else
        do Value::locPickList
      end
  of lower('PickContactHistoryNotes_locFinalList_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locFinalList
      of event:timer
        do Value::locFinalList
      else
        do Value::locFinalList
      end
  of lower('PickContactHistoryNotes_button:AddSelected_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::button:AddSelected
      of event:timer
        do Value::button:AddSelected
      else
        do Value::button:AddSelected
      end
  of lower('PickContactHistoryNotes_button:RemoveSelected_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::button:RemoveSelected
      of event:timer
        do Value::button:RemoveSelected
      else
        do Value::button:RemoveSelected
      end
  of lower('PickContactHistoryNotes_button:RemoveAll_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::button:RemoveAll
      of event:timer
        do Value::button:RemoveAll
      else
        do Value::button:RemoveAll
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('PickContactHistoryNotes_form:ready_',1)

  p_web.SetSessionValue('PickContactHistoryNotes_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_PickContactHistoryNotes',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('PickContactHistoryNotes_form:ready_',1)
  p_web.SetSessionValue('PickContactHistoryNotes_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_PickContactHistoryNotes',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('PickContactHistoryNotes_form:ready_',1)
  p_web.SetSessionValue('PickContactHistoryNotes_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('PickContactHistoryNotes:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('PickContactHistoryNotes_form:ready_',1)
  p_web.SetSessionValue('PickContactHistoryNotes_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('PickContactHistoryNotes:Primed',0)
  p_web.setsessionvalue('showtab_PickContactHistoryNotes',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
      If not (1=0)
          If p_web.IfExistsValue('locPickList')
            locPickList = p_web.GetValue('locPickList')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('locFinalList')
            locFinalList = p_web.GetValue('locFinalList')
          End
      End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('PickContactHistoryNotes_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
      If Clip(p_web.GSV('locFinalListField')) <> ''
          Loop x# = 1 To 10000
              If Sub(p_web.GSV('locFinalListField'),x#,2) = '|;'
                  Start# = x# + 2
                  Cycle
              End ! If Sub(p_web.GSV('locFinalListField'),x#,2) = '|;'
  
              If Start# > 0
                  If Sub(p_web.GSV('locFinalListField'),x#,2) = ';|'
                      locFoundField = Sub(p_web.GSV('locFinalListField'),Start#,x# - Start#)
  
                      If locFoundField <> ''
                          p_web.SSV('cht:Notes',p_web.GSV('cht:Notes') & '<13,10>' & |
                              CLIP(locFoundField))
                      End ! If locFoundField <> ''
                      Start# = 0
                  End ! If Sub(p_web.GSV('locFinalListField'),x#,2) = ';|'
              End ! If Start# > 0
          End ! Loop x# = 1 To 1000
  
          If Start# > 0
              locFoundField = Clip(Sub(p_web.GSV('locFinalListField'),Start#,30))
              If locFoundField <> ''
                  p_web.SSV('cht:Notes',p_web.GSV('cht:Notes') & '<13,10>' & |
                      CLIP(locFoundField))
              End ! If locFoundField <> ''
          End ! If Start# > 0
      End ! If Clip(p_web.GSV('locFinalListField') <> ''
  p_web.DeleteSessionValue('PickContactHistoryNotes_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
    do ValidateValue::title
    If loc:Invalid then exit.
    do ValidateValue::locPickList
    If loc:Invalid then exit.
    do ValidateValue::locFinalList
    If loc:Invalid then exit.
    do ValidateValue::button:AddSelected
    If loc:Invalid then exit.
    do ValidateValue::button:RemoveSelected
    If loc:Invalid then exit.
    do ValidateValue::gap
    If loc:Invalid then exit.
    do ValidateValue::button:RemoveAll
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
  do clearVariables
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('PickContactHistoryNotes:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('locPickList')
  p_web.StoreValue('locFinalList')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')

FormContactHistory   PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
FilesOpened     Long
ACTION::State  USHORT
CONTHIST::State  USHORT
cht:Action:IsInvalid  Long
cht:Notes:IsInvalid  Long
button:ContactHistoryNotes:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
cht:Action_OptionView   View(ACTION)
                          Project(act:Action)
                        End
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormContactHistory')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'FormContactHistory_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('FormContactHistory','')
    p_web.DivHeader('FormContactHistory',p_web.combine(p_web.site.style.formdiv,'fdiv'))
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('FormContactHistory') = 0
        p_web.AddPreCall('FormContactHistory')
        p_web.DivHeader('popup_FormContactHistory','nt-hidden')
        p_web.DivHeader('FormContactHistory',p_web.combine(p_web.site.style.formdiv,'fdiv'))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_FormContactHistory_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_FormContactHistory_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormContactHistory',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:StagePost
    do RestoreMem
    do PostInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_FormContactHistory',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormContactHistory',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:StagePost
    do RestoreMem
    do PostCopy
  of Net:CopyRecord + NET:WEB:Populate
    If p_web.IfExistsValue('cht:Record_Number') = 0 then p_web.SetValue('cht:Record_Number',p_web.GSV('cht:Record_Number')).
    do PreCopy
    p_web.setsessionvalue('showtab_FormContactHistory',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormContactHistory',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormContactHistory',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do PostInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do PostUpdate
    End
  of Net:ChangeRecord + NET:WEB:Populate
    If p_web.IfExistsValue('cht:Record_Number') = 0 then p_web.SetValue('cht:Record_Number',p_web.GSV('cht:Record_Number')).
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_FormContactHistory',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormContactHistory',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:DeleteRecord + NET:WEB:StagePost
    do RestoreMem
    do PostDelete
  of Net:ViewRecord + NET:WEB:Populate
    If p_web.IfExistsValue('cht:Record_Number') = 0 then p_web.SetValue('cht:Record_Number',p_web.GSV('cht:Record_Number')).
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_FormContactHistory',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormContactHistory',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormContactHistory',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormContactHistory',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('FormContactHistory')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(ACTION)
  p_web._OpenFile(CONTHIST)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(ACTION)
  p_Web._CloseFile(CONTHIST)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FormContactHistory_form:inited_',1)
  p_web.formsettings.file = 'CONTHIST'
  p_web.formsettings.key = 'cht:Record_Number_Key'
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = 'CONTHIST'
    p_web.formsettings.key = 'cht:Record_Number_Key'
      clear(p_web.formsettings.FieldName)
    p_web.formsettings.recordid[1] = cht:Record_Number
    p_web.formsettings.FieldName[1] = 'cht:Record_Number'
    do SetAction
    if p_web.GetSessionValue('FormContactHistory:Primed') = 1
      p_web.formsettings.action = Net:ChangeRecord
    Else
      p_web.formsettings.action = Loc:Act
    End
    p_web.formsettings.OriginalAction = Loc:Act
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'FormContactHistory'
    end
    p_web.formsettings.proc = 'FormContactHistory'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine
  IF p_web.GetSessionValue('FormContactHistory:Primed') = 1
    p_web._deleteFile(CONTHIST)
    p_web.SetSessionValue('FormContactHistory:Primed',0)
  End

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine
  p_web.SetValue('UpdateFile','CONTHIST')
  p_web.SetValue('UpdateKey','cht:Record_Number_Key')

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'cht:Action'
    p_web.setsessionvalue('showtab_FormContactHistory',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(ACTION)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.cht:Notes')
  End
  p_web.DeleteValue('LookupField')

StoreMem       Routine

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=File

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('FormContactHistory_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'FormBrowseContactHistory'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormContactHistory_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormContactHistory_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormContactHistory_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'FormBrowseContactHistory'

GenerateForm   Routine
  do LoadRelatedRecords
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
    loc:viewonly = Choose(cht:SystemHistory = 1,1,0)
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Contact History') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Contact History',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_FormContactHistory',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_FormContactHistory0_div')&'">'&p_web.Translate('General')&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="FormContactHistory_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.cancel();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CancelButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="FormContactHistory_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'FormContactHistory_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="FormContactHistory_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'FormContactHistory_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.cht:Action')
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_FormContactHistory')>0,p_web.GSV('showtab_FormContactHistory'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_FormContactHistory'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_FormContactHistory') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_FormContactHistory'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_FormContactHistory')>0,p_web.GSV('showtab_FormContactHistory'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_FormContactHistory') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('General') & ''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_FormContactHistory_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_FormContactHistory')>0,p_web.GSV('showtab_FormContactHistory'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"FormContactHistory",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_FormContactHistory')>0,p_web.GSV('showtab_FormContactHistory'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_FormContactHistory_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('FormContactHistory') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('FormContactHistory')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('General')&'</a></h3>' & CRLF & p_web.DivHeader('tab_FormContactHistory0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_FormContactHistory0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_FormContactHistory0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_FormContactHistory0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'General')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_FormContactHistory0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('General')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_FormContactHistory0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('General')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_FormContactHistory0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::cht:Action
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::cht:Action
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::cht:Action
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::cht:Notes
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::cht:Notes
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::cht:Notes
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::button:ContactHistoryNotes
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::button:ContactHistoryNotes
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Prompt::cht:Action  Routine
  packet = clip(packet) & p_web.DivHeader('FormContactHistory_' & p_web._nocolon('cht:Action') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Action'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::cht:Action  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    cht:Action = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s80
    cht:Action = p_web.Dformat(p_web.GetValue('Value'),'@s80')
  End
  do ValidateValue::cht:Action  ! copies value to session value if valid.
  do Value::cht:Action
  do SendAlert
  do Comment::cht:Action ! allows comment style to be updated.

ValidateValue::cht:Action  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('cht:Action',cht:Action).
    End

Value::cht:Action  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormContactHistory_' & p_web._nocolon('cht:Action') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,p_web.site.style.formselect,'FormEntry',)
  If loc:retrying
    cht:Action = p_web.RestoreValue('cht:Action')
    do ValidateValue::cht:Action
    If cht:Action:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- DROPLIST ---
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''cht:Action'',''formcontacthistory_cht:action_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('cht:Action',loc:fieldclass,loc:readonly,,178,,loc:javascript,)
  loc:fieldclass = ''
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('cht:Action') = 0
    p_web.SetSessionValue('cht:Action','')
  end
    packet = clip(packet) & p_web.CreateOption('-------------------------------------','',choose('' = p_web.getsessionvalue('cht:Action')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(ACTION)
  bind(act:Record)
  p_web._OpenFile(CONTHIST)
  bind(cht:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(cht:Action_OptionView)
  cht:Action_OptionView{prop:order} = p_web.CleanFilter(cht:Action_OptionView,'UPPER(act:Action)')
  Set(cht:Action_OptionView)
  Loop
    Next(cht:Action_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('cht:Action') = 0
      p_web.SetSessionValue('cht:Action',act:Action)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,act:Action,choose(act:Action = p_web.getsessionvalue('cht:Action')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(cht:Action_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(ACTION)
  p_Web._CloseFile(CONTHIST)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web.DivFooter()
Comment::cht:Action  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if cht:Action:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormContactHistory_' & p_web._nocolon('cht:Action') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::cht:Notes  Routine
  packet = clip(packet) & p_web.DivHeader('FormContactHistory_' & p_web._nocolon('cht:Notes') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Notes'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::cht:Notes  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    cht:Notes = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s255
    cht:Notes = p_web.Dformat(p_web.GetValue('Value'),'@s255')
  End
  do ValidateValue::cht:Notes  ! copies value to session value if valid.
  do Value::cht:Notes
  do SendAlert
  do Comment::cht:Notes ! allows comment style to be updated.

ValidateValue::cht:Notes  Routine
    If not (1=0)
    cht:Notes = Upper(cht:Notes)
      if loc:invalid = '' then p_web.SetSessionValue('cht:Notes',cht:Notes).
    End

Value::cht:Notes  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormContactHistory_' & p_web._nocolon('cht:Notes') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    cht:Notes = p_web.RestoreValue('cht:Notes')
    do ValidateValue::cht:Notes
    If cht:Notes:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- TEXT --- cht:Notes
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''cht:Notes'',''formcontacthistory_cht:notes_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'readonly','')
  do SendPacket
  p_web.CreateTextArea('cht:Notes',p_web.GetSessionValue('cht:Notes'),10,40,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,255,,,,Net:Send)
  do SendPacket
  End
  p_web.DivFooter()
Comment::cht:Notes  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if cht:Notes:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormContactHistory_' & p_web._nocolon('cht:Notes') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::button:ContactHistoryNotes  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::button:ContactHistoryNotes  ! copies value to session value if valid.
  do Comment::button:ContactHistoryNotes ! allows comment style to be updated.

ValidateValue::button:ContactHistoryNotes  Routine
    If not (1=0)
    End

Value::button:ContactHistoryNotes  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormContactHistory_' & p_web._nocolon('button:ContactHistoryNotes') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','ContactHistoryNotes','Contact History Notes',p_web.combine(Choose('Contact History Notes' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'SmallButtonFixed'),loc:formname,,,p_web.WindowOpen(clip('PickContactHistoryNotes')&''&'','_self'),,loc:disabled,,,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::button:ContactHistoryNotes  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if button:ContactHistoryNotes:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormContactHistory_' & p_web._nocolon('button:ContactHistoryNotes') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('FormContactHistory_nexttab_' & 0)
    cht:Action = p_web.GSV('cht:Action')
    do ValidateValue::cht:Action
    If loc:Invalid
      loc:retrying = 1
      do Value::cht:Action
      !do SendAlert
      do Comment::cht:Action ! allows comment style to be updated.
      !exit
    End
    cht:Notes = p_web.GSV('cht:Notes')
    do ValidateValue::cht:Notes
    If loc:Invalid
      loc:retrying = 1
      do Value::cht:Notes
      !do SendAlert
      do Comment::cht:Notes ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_FormContactHistory_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('FormContactHistory_tab_' & 0)
    do GenerateTab0
  of lower('FormContactHistory_cht:Action_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::cht:Action
      of event:timer
        do Value::cht:Action
        do Comment::cht:Action
      else
        do Value::cht:Action
      end
  of lower('FormContactHistory_cht:Notes_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::cht:Notes
      of event:timer
        do Value::cht:Notes
        do Comment::cht:Notes
      else
        do Value::cht:Notes
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('FormContactHistory_form:ready_',1)

  p_web.SetSessionValue('FormContactHistory_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_FormContactHistory',0)
  cht:SystemHistory = 0
  p_web.SetSessionValue('cht:SystemHistory',cht:SystemHistory)
  cht:Date = Today()
  p_web.SetSessionValue('cht:Date',cht:Date)
  cht:Time = clock()
  p_web.SetSessionValue('cht:Time',cht:Time)
  cht:User = p_web.GSV('BookingUserCode')
  p_web.SetSessionValue('cht:User',cht:User)
  cht:Ref_Number = p_web.GSV('job:Ref_Number')
  p_web.SetSessionValue('cht:Ref_Number',cht:Ref_Number)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('FormContactHistory_form:ready_',1)
  p_web.SetSessionValue('FormContactHistory_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormContactHistory',0)
  p_web._PreCopyRecord(CONTHIST,cht:Record_Number_Key)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('FormContactHistory_form:ready_',1)
  p_web.SetSessionValue('FormContactHistory_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('FormContactHistory:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('FormContactHistory_form:ready_',1)
  p_web.SetSessionValue('FormContactHistory_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('FormContactHistory:Primed',0)
  p_web.setsessionvalue('showtab_FormContactHistory',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
      If not (1=0)
          If p_web.IfExistsValue('cht:Action')
            cht:Action = p_web.GetValue('cht:Action')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('cht:Notes')
            cht:Notes = p_web.GetValue('cht:Notes')
          End
      End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormContactHistory_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormContactHistory_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
    loc:InvalidTab += 1
    do ValidateValue::cht:Action
    If loc:Invalid then exit.
    do ValidateValue::cht:Notes
    If loc:Invalid then exit.
    do ValidateValue::button:ContactHistoryNotes
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostInsert      Routine
PostCopy        Routine
  p_web.SetSessionValue('FormContactHistory:Primed',0)
PostUpdate      Routine
  p_web.SetSessionValue('FormContactHistory:Primed',0)
  p_web.StoreValue('')


PostDelete      Routine
