

   MEMBER('WebServer_Phase4.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSE042.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSE013.INC'),ONCE        !Req'd for module callout resolution
                     END


BrowseJobFaultCodeLookup PROCEDURE  (NetWebServerWorker p_web)
local:FaultCodeValue STRING(255)                           !
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256),dim(Net:MaxKeyFields)
idx           String(Net:HashSize)
sub           Long
            End
Select:IsInvalid  Long
mfo:Field:IsInvalid  Long
mfo:Description:IsInvalid  Long
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:DefaultSelection    String(Net:HashSize)
loc:ActualSelection     String(Net:HashSize)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
!loc:pagename            String(256)
loc:NavButtonPosition    Long
loc:UpdateButtonPosition Long
loc:SelectionMethod      Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorCase         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
loc:lookupdone          Long
loc:FormPopup           Long
ThisView            View(MANFAULO)
                      Project(mfo:RecordNumber)
                      Project(mfo:Field)
                      Project(mfo:Description)
                      Project(mfo:Manufacturer)
                      Project(mfo:Field_Number)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
Loc:popup             Long
loc:poppedup          Long,static,thread
FilesOpened     Long
MANFAUPA::State  USHORT
MODELCCT::State  USHORT
MANFAULO_ALIAS::State  USHORT
MANFPARL::State  USHORT
MANFAUPA_ALIAS::State  USHORT
MANFPALO_ALIAS::State  USHORT
MANFAURL::State  USHORT
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return 
  End
  GlobalErrors.SetProcedureName('BrowseJobFaultCodeLookup')
  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseJobFaultCodeLookup:NoForm')
      loc:NoForm = p_web.GetValue('BrowseJobFaultCodeLookup:NoForm')
      loc:FormName = p_web.GetValue('BrowseJobFaultCodeLookup:FormName')
    else
      loc:FormName = 'BrowseJobFaultCodeLookup_frm'
    End
    p_web.SSV('BrowseJobFaultCodeLookup:NoForm',loc:NoForm)
    p_web.SSV('BrowseJobFaultCodeLookup:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseJobFaultCodeLookup:NoForm')
    loc:FormName = p_web.GSV('BrowseJobFaultCodeLookup:FormName')
  end
  case p_web.site.formpopups
  of -1 ; loc:FormPopup = Net:Page
  of 0 ; loc:FormPopup = Net:Page
  of 1 ; loc:FormPopup = Net:Popup
  End
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseJobFaultCodeLookup') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseJobFaultCodeLookup')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if 1 = 2
  elsif p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  elsif p_web.IfExistsValue('_EIPRow_')
    do CallRow
  elsif p_web.GetValue('_CallPopups') > 0
    do CallPopups
  elsif p_web.IfExistsValue('_GetSecwinSettings')
      p_web.SetValue('Secwin_' & 'BrowseJobFaultCodeLookup' & 'AccessGroupsArray','1') 
      p_web.SetValue('Secwin_AccessWindowName','BrowseJobFaultCodeLookup')
      
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallPopups  Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
loc:CallPopups   Long
loc:name         String(255)
  code
  if loc:poppedup = 0
    loc:CallPopups = p_web.GetValue('_CallPopups')
    if loc:CallPopups = 1 !and p_web.GetPreCall('BrowseJobFaultCodeLookup') = 0! for = 1 include popup divs and scripts for this browse
      !p_web.AddPreCall('BrowseJobFaultCodeLookup')
      p_web.DivHeader('popup_BrowseJobFaultCodeLookup','nt-hidden')
      p_web.DivHeader('BrowseJobFaultCodeLookup',p_web.combine(p_web.site.style.browsediv,))
      p_web.DivFooter()
      p_web.DivFooter()
      loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(400)&', modal: true, position: [''center'',15]'
      packet =  p_web.jQuery('#' & lower('popup_BrowseJobFaultCodeLookup_div'),'dialog',loc:options) & |
      p_web.jQuery('#' & lower('popup_BrowseJobFaultCodeLookup_div'),'removeClass','''nt-hidden''')
    End
    ! for > 0 add in form, EIP-lookups and browse object
    do Popups
    loc:popup = 1
    do ClosingScripts
    do SendPacket
    loc:poppedup = 1
  end

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web.DivHeader('BrowseJobFaultCodeLookup',p_web.combine(p_web.site.style.browsediv,))
  if loc:ParentSilent = 0
    do GenerateBrowse
    if p_web.RequestAjax = 1
      p_web.Script('BrowseProducts.pcv();')
    end
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web.DivFooter()
  do Children
  do Popups
  if loc:poppedup = 0
    do ClosingScripts
  end
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(MANFAULO,mfo:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'MFO:FIELD') then p_web.SetValue('BrowseJobFaultCodeLookup_sort','1')
    ElsIf (loc:vorder = 'MFO:DESCRIPTION') then p_web.SetValue('BrowseJobFaultCodeLookup_sort','2')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseJobFaultCodeLookup:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseJobFaultCodeLookup:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseJobFaultCodeLookup:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseJobFaultCodeLookup:LookupField')
    loc:selecting = 0
  End

Popups Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  If loc:popup = 0
  End
SetFormAction  Routine
  loc:formaction = 'BrowseJobFaultCodeLookup'

GotFocusBack   Routine

GenerateBrowse Routine
  data
loc:viewoptions  Long
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  ! Set general Browse options
  loc:NavButtonPosition   = Net:Below
  loc:UpdateButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:FileLoad
  loc:FillBack         = 0
  loc:Sorting          = Net:ServerSort
  if p_web.GetValue('_popup_') = 1
    loc:popup = 1
  end
  ! Set Locator Options
  loc:LocatorPosition  = Net:Above
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  p_web.ClearBrowse('BrowseJobFaultCodeLookup')
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  if (p_web.ifExistsValue('fieldNumber'))
      p_web.storeValue('fieldNumber')
  end !if (p_web.ifExistsValue('fieldNumber'))
  if (p_web.ifExistsValue('partType'))
      p_web.storeValue('partType')
  end ! if (p_web.ifExistsValue('partType'))
  ! Has this been called from Parts Main Fault?
  if (p_web.ifExistsValue('partMainFault'))
      p_web.storeValue('partMainFault')
  end ! if (p_web.ifExistsValue('partMainFault'))
  if (p_web.ifExistsValue('relatedPartCode'))
      p_web.storeValue('relatedPartCode')
  end ! if (p-web.ifExistsValue('relatedPartCode'))
  
  !Clear queue for this user
  clear(tmpfau:Record)
  tmpfau:sessionID = p_web.SessionID
  set(tmpfau:keySessionID,tmpfau:keySessionID)
  loop
      next(tempFaultCodes)
      if (error())
          break
      end! if (error())
      if (tmpfau:sessionID <> p_web.sessionID)
          break
      end ! if (tmpfau:sessionID <> p_web.sessionID)
      delete(tempFaultCodes)
  end ! loop
  
  if (p_web.GSV('BrowseJobFaultCodeLookup:LookupFrom') = 'JobFaultCodes')
      loop x# = 1 to 20
          if (p_web.GSV('Hide:JobFaultCode' & x#) = 1 )
              cycle
          end ! if (p_web.GSV('Hide:JobFaultCode' & x#) = 1 )
          if (p_web.GSV('tmp:FaultCode' & x#) = '')
              cycle
          end ! if (p_web.GSV('tmp:FaultCode' & x#) = '')
  
          Access:MANFAULT.Clearkey(maf:ScreenOrderKey)
          maf:Manufacturer    = p_web.GSV('job:Manufacturer')
          maf:ScreenOrder    = x#
          if (Access:MANFAULT.TryFetch(maf:ScreenOrderKey) = Level:Benign)
              ! Found
              if (maf:Lookup = 'YES')
                  Access:MANFAULO_ALIAS.Clearkey(mfo_ali:Field_Key)
                  mfo_ali:Manufacturer    = p_web.GSV('job:Manufacturer')
                  mfo_ali:Field_Number    = maf:Field_Number
                  mfo_ali:Field = p_web.GSV('tmp:FaultCode' & x#)
                  if (Access:MANFAULO_ALIAS.TryFetch(mfo_ali:Field_Key) = Level:Benign)
                      ! Found
                      found# = 0
                      Access:MANFAURL.Clearkey(mnr:FieldKey)
                      mnr:MANFAULORecordNumber    = mfo_ali:RecordNumber
  !                    mnr:PartFaultCode    = 1
                      mnr:FieldNumber    = p_web.GSV('fieldNumber')
                      set(mnr:FieldKey,mnr:FieldKey)
                      loop
                          if (Access:MANFAURL.Next())
                              Break
                          end ! if (Access:MANFAURL.Next())
                          if (mnr:MANFAULORecordNumber    <> mfo_ali:RecordNumber)
                              Break
                          end ! if (mnr:MANFALORecordNumber    <> mfo:RecordNumber)
  !                        if (mnr:PartFaultCode    <> 1)
  !                            Break
  !                        end ! if (mnr:PartFaultCode    <> 1)
                          if (mnr:FieldNumber    <> p_web.GSV('fieldNumber'))
                              Break
                          end ! if (mnr:FieldNumber    <> fieldNumber)
                          found# = 1
                          break
                      end ! loop
                      if (found# = 1)
                          tmpfau:sessionID = p_web.sessionID
                          tmpfau:recordNumber = mfo_ali:RecordNumber
                          tmpfau:faultType = 'J'
                          add(tempFaultCodes)
                      end ! if (found# = 1)
                  else ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                      ! Error
                  end ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
              end ! if (maf:Lookup = 'YES')
          else ! if (Access:MANFAULT.TryFetch(maf:Field_Number_Key) = Level:Benign)
              ! Error
          end ! if (Access:MANFAULT.TryFetch(maf:Field_Number_Key) = Level:Benign)
  
  
          local:FaultCodeValue = ''
          Access:WARPARTS.Clearkey(wpr:Part_Number_Key)
          wpr:Ref_Number = p_web.GSV('job:Ref_Number')
          Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
          Loop
              If Access:WARPARTS.Next()
                  Break
              End ! If Access:WARPARTS.Next()
              If wpr:Ref_Number <> p_web.GSV('job:Ref_Number')
                  Break
              End ! If wpr:Ref_Number <> job:Ref_Number
              Loop f# = 1 To 12
                  Case f#
                  Of 1
                      If wpr:Fault_Code1 = ''
                          Cycle
                      End ! If wpr:Fault_Code1 = ''
                      local:FaultCodeValue = wpr:Fault_Code1
                  Of 2
                      If wpr:Fault_Code2 = ''
                          Cycle
                      End ! If wpr:Fault_Code1 = ''
                      local:FaultCodeValue = wpr:Fault_Code2
                  Of 3
                      If wpr:Fault_Code3 = ''
                          Cycle
                      End ! If wpr:Fault_Code1 = ''
                      local:FaultCodeValue = wpr:Fault_Code3
                  Of 4
                      If wpr:Fault_Code4 = ''
                          Cycle
                      End ! If wpr:Fault_Code1 = ''
                      local:FaultCodeValue = wpr:Fault_Code4
                  Of 5
                      If wpr:Fault_Code5 = ''
                          Cycle
                      End ! If wpr:Fault_Code1 = ''
                      local:FaultCodeValue = wpr:Fault_Code5
                  Of 6
                      If wpr:Fault_Code6 = ''
                          Cycle
                      End ! If wpr:Fault_Code1 = ''
                      local:FaultCodeValue = wpr:Fault_Code6
                  Of 7
                      If wpr:Fault_Code7 = ''
                          Cycle
                      End ! If wpr:Fault_Code1 = ''
                      local:FaultCodeValue = wpr:Fault_Code7
                  Of 8
                      If wpr:Fault_Code8 = ''
                          Cycle
                      End ! If wpr:Fault_Code1 = ''
                      local:FaultCodeValue = wpr:Fault_Code8
                  Of 9
                      If wpr:Fault_Code9 = ''
                          Cycle
                      End ! If wpr:Fault_Code1 = ''
                      local:FaultCodeValue = wpr:Fault_Code9
                  Of 10
                      If wpr:Fault_Code10 = ''
                          Cycle
                      End ! If wpr:Fault_Code1 = ''
                      local:FaultCodeValue = wpr:Fault_Code10
                  Of 11
                      If wpr:Fault_Code11 = ''
                          Cycle
                      End ! If wpr:Fault_Code1 = ''
                      local:FaultCodeValue = wpr:Fault_Code11
                  Of 12
                      If wpr:Fault_Code12 = ''
                          Cycle
                      End ! If wpr:Fault_Code1 = ''
                      local:FaultCodeValue = wpr:Fault_Code12
                  End ! Case
  
                  Access:MANFAUPA.Clearkey(map:Field_Number_Key)
                  map:Manufacturer = p_web.GSV('job:Manufacturer')
                  map:Field_Number = f#
                  If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
                      ! Found
                      If map:UseRelatedJobCode
                          Access:MANFAULT_ALIAS.Clearkey(maf_ali:MainFaultKey)
                          maf_ali:Manufacturer = p_web.GSV('job:Manufacturer')
                          maf_ali:MainFault = 1
                          If Access:MANFAULT_ALIAS.TryFetch(maf_ali:MainFaultKey) = Level:Benign
                              ! Found
                              Access:MANFAULO_ALIAS.Clearkey(mfo_ali:RelatedFieldKey)
                              mfo_ali:Manufacturer = p_web.GSV('job:Manufacturer')
                              mfo_ali:RelatedPartCode = map:Field_Number
                              mfo_ali:Field_Number = maf_ali:Field_Number
                              mfo_ali:Field = local:FaultCodeValue
                              If Access:MANFAULO_ALIAS.TryFetch(mfo_ali:RelatedFieldKey) = Level:Benign
                                  ! Found
                                  Found# = 0
                                  Access:MANFAURL.Clearkey(mnr:FieldKey)
                                  mnr:MANFAULORecordNumber = mfo_ali:RecordNumber
                                  mnr:PartFaultCode = 0
                                  mnr:FieldNumber = p_web.GSV('fieldNumber')
                                  Set(mnr:FieldKey,mnr:FieldKey)
                                  Loop
                                      If Access:MANFAURL.Next()
                                          Break
                                      End ! If Access:MANFAURL.Next()
                                      If mnr:MANFAULORecordNumber <> mfo_ali:RecordNumber
                                          Break
                                      End ! If mnr:MANFAULORecordNumber <> mfo_ali:RecordNumber
                                      If mnr:PartFaultCode <> 0
                                          Break
                                      End ! If mnr:PartFaultCOde <> 1
                                      If mnr:FieldNUmber <> p_web.GSV('fieldNumber')
                                          Break
                                      End ! If mnr:FIeldNUmber <> maf:Field_Number
                                      If mnr:RelatedPartFaultCode > 0
                                          If mnr:RelatedPartFaultCode <> map:Field_Number
                                              Cycle
                                          End ! If mnr:RelatedPartFaultCode <> map_ali:Field_Number
                                      End ! If mnr:RelaterdPartFaultCode > 0
                                      Found# = 1
                                      Break
                                  End ! Loop (MANFAURL)
                                  If Found# = 1
                                      tmpfau:sessionID = p_web.sessionID
                                      tmpfau:recordNumber = mfo_ali:RecordNumber
                                      tmpfau:faultType = 'J'
                                      add(tempFaultCodes)
                                  End ! If Found# = 1
  
                              Else ! If Access:MANFAULO_ALIAS.TryFetch(mfo_ali:RelatedFieldKey) = Level:Benign
                              End ! If Access:MANFAULO_ALIAS.TryFetch(mfo_ali:RelatedFieldKey) = Level:Benign
                          Else ! If Access:MANFAULT_ALIAS.TryFetch(maf_ali:MainFaultKey) = Level:Benign
                          End ! If Access:MANFAULT_ALIAS.TryFetch(maf_ali:MainFaultKey) = Level:Benign
                      Else ! If map:UseRelatedJobCode
                          Access:MANFPALO.Clearkey(mfp:Field_Key)
                          mfp:Manufacturer = p_web.GSV('job:Manufacturer')
                          mfp:Field_Number = map:Field_Number
                          mfp:Field = local:FaultCodeValue
                          If Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign
                              ! Found
                              Access:MANFPARL.Clearkey(mpr:FieldKey)
                              mpr:MANFPALORecordNumber = mfp:RecordNumber
                              mpr:JobFaultCode = 1
                              mpr:FieldNumber = p_web.GSV('fieldNumber')
                              Set(mpr:FieldKey,mpr:FieldKey)
                              Loop
                                  If Access:MANFPARL.Next()
                                      Break
                                  End ! If Access:MANFPARL.Next()
                                  If mpr:MANFPALORecordNumber <> mfp:RecordNumber
                                      Break
                                  End ! If mpr:MANFPALORecordNumber <> mfp:RecordNumber
                                  If mpr:JobFaultCode <> 1
                                      Break
                                  End ! If mpr:JobFaultCode <> 1
                                  If mpr:FieldNumber <> p_web.GSV('fieldNumber')
                                      Break
                                  End ! If mpr:FieldNumber <> maf:Field_Number
  
                                  Found# = 1
                                  Break
                              End ! Loop (MANFPARL)
                              If Found# = 1
                                  tmpfau:sessionID = p_web.sessionID
                                  tmpfau:recordNumber = mfp:recordNumber
                                  tmpfau:faultType = 'P'
                                  add(tempFaultCodes)
                              End ! If Found# = 1
                          Else ! If Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign
                          End ! If Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign
                      End ! If map:UseRelatedJobCode
                  Else ! If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
                  End ! If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
              End ! Loop f# = 1 To 20
  
          End ! Loop (WARPARTS)
  
      end
  else ! if (p_web.GSV('BrowseJobFaultCodeLookup:LookupFrom') = 'JobFaultCodes')
      if (p_web.GSV('partMainFault') = 0)
          ! Build Associated Fault Codes Queue
  
          loop x# = 1 to 12
              if (x# = p_web.GSV('fieldNumber'))
                  cycle
              end ! if (x# = fieldNumber)
  
              if (p_web.GSV('Hide:PartFaultCode' & x#))
                  cycle
              end ! if (p_web.GSV('Hide:PartFaultCode' & x#))
  
              if (p_web.GSV('tmp:FaultCodes' & x#) = '')
                  cycle
              end ! if (p_web.GSV('tmp:FaultCodes' & x#) = '')
  
              Access:MANFAUPA_ALIAS.Clearkey(map_ali:ScreenOrderKey)
              map_ali:Manufacturer    = p_web.GSV('job:Manufacturer')
              map_ali:ScreenOrder    = x#
              if (Access:MANFAUPA_ALIAS.TryFetch(map_ali:ScreenOrderKey) = Level:Benign)
                  ! Found
                  Access:MANFPALO_ALIAS.Clearkey(mfp_ali:Field_Key)
                  mfp_ali:Manufacturer    = p_web.GSV('job:Manufacturer')
                  mfp_ali:Field_Number    = map_ali:Field_Number
                  mfp_ali:Field    = p_web.GSV('tmp:FaultCodes' & x#)
                  if (Access:MANFPALO_ALIAS.TryFetch(mfp_ali:Field_Key) = Level:Benign)
                      ! Found
                      found# = 0
  
                      Access:MANFPARL.Clearkey(mpr:FieldKey)
                      mpr:MANFPALORecordNumber    = mfp_ali:RecordNumber
                      mpr:FieldNumber    = p_web.GSV('fieldNumber')
                      set(mpr:FieldKey,mpr:FieldKey)
                      loop
                          if (Access:MANFPARL.Next())
                              Break
                          end ! if (Access:MANFPARL.Next())
                          if (mpr:MANFPALORecordNumber    <> mfp_ali:RecordNumber)
                              Break
                          end ! if (mfp:MANFPALORecordNumber    <> mfp_ali:RecordNumber)
                          if (mpr:FieldNumber    <> p_web.GSV('fieldNumber'))
                              Break
                          end ! if (mfp:FieldNumber    <> p_web.GSV('fieldNumber'))
                          found# = 1
                          break
                      end ! loop
  
                      if (found# = 1)
                          tmpfau:sessionID = p_web.sessionID
                          tmpfau:recordNumber = mfp_ali:recordNumber
                          tmpfau:faultType = 'P'
                          add(tempFaultCodes)
                      end ! if (found# = 1)
  
                  else ! if (Access:MANFPALO_ALIAS.TryFetch(mfp_ali:Field_Key) = Level:Benign)
                      ! Error
                  end ! if (Access:MANFPALO_ALIAS.TryFetch(mfp_ali:Field_Key) = Level:Benign)
              else ! if (Access:MANFAUPA_ALIAS.TryFetch(map_ali:Field_Number_Key) = Level:Benign)
                  ! Error
              end ! if (Access:MANFAUPA_ALIAS.TryFetch(map_ali:Field_Number_Key) = Level:Benign)
  
          end
          loop x# = 1 to 20
              Access:MANFAULT.Clearkey(maf:Field_Number_Key)
              maf:Manufacturer    = p_web.GSV('job:Manufacturer')
              maf:Field_Number    = x#
              if (Access:MANFAULT.TryFetch(maf:Field_Number_Key) = Level:Benign)
                  ! Found
                  if (maf:Lookup = 'YES')
  
                      Access:MANFAULO.Clearkey(mfo:Field_Key)
                      mfo:Manufacturer    = p_web.GSV('job:Manufacturer')
                      mfo:Field_Number    = maf:Field_Number
                      if (x# < 13)
                          mfo:Field = p_web.GSV('job:Fault_Code' & x#)
                      else ! if (x# < 13)
                          mfo:Field = p_web.GSV('wob:FaultCode' & x#)
                      end !if (x# < 13)
                      if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                          ! Found
                          found# = 0
                          Access:MANFAURL.Clearkey(mnr:FieldKey)
                          mnr:MANFAULORecordNumber    = mfo:RecordNumber
                          mnr:PartFaultCode    = 1
                          mnr:FieldNumber    = p_web.GSV('fieldNumber')
                          set(mnr:FieldKey,mnr:FieldKey)
                          loop
                              if (Access:MANFAURL.Next())
                                  Break
                              end ! if (Access:MANFAURL.Next())
                              if (mnr:MANFAULORecordNumber    <> mfo:RecordNumber)
                                  Break
                              end ! if (mnr:MANFALORecordNumber    <> mfo:RecordNumber)
                              if (mnr:PartFaultCode    <> 1)
                                  Break
                              end ! if (mnr:PartFaultCode    <> 1)
                              if (mnr:FieldNumber    <> p_web.GSV('fieldNumber'))
                                  Break
                              end ! if (mnr:FieldNumber    <> fieldNumber)
                              found# = 1
                              break
                          end ! loop
                          if (found# = 1)
                              tmpfau:sessionID = p_web.sessionID
                              tmpfau:recordNumber = mfo:recordNumber
                              tmpfau:faultType = 'J'
                              add(tempFaultCodes)
                          end ! if (found# = 1)
                      else ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                          ! Error
                      end ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                  end ! if (maf:Lookup = 'YES')
              else ! if (Access:MANFAULT.TryFetch(maf:Field_Number_Key) = Level:Benign)
                  ! Error
              end ! if (Access:MANFAULT.TryFetch(maf:Field_Number_Key) = Level:Benign)
  
          end ! loop x# = 1 to 12
  
      end ! if (p_web.GSV('partMainFault') = 0)
  
  end !if (p_web.GSV('BrowseJobFaultCodeLookup:LookupFrom') = 'JobFaultCodes')
  
  
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 15
  End
  loc:ActualSelection = ''
  !loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseJobFaultCodeLookup_sort',net:DontEvaluate)
  p_web.SetSessionValue('BrowseJobFaultCodeLookup_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 3
    Loc:LocateField = ''
    Loc:LocatorCase = 0
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(mfo:Field)','-UPPER(mfo:Field)')
    Loc:LocateField = 'mfo:Field'
    Loc:LocatorCase = 0
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(mfo:Description)','-UPPER(mfo:Description)')
    Loc:LocateField = 'mfo:Description'
    Loc:LocatorCase = 0
  end
  if loc:vorder = ''
    loc:vorder = '+UPPER(mfo:Manufacturer),+mfo:Field_Number,+UPPER(mfo:Field)'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('mfo:Field')
    loc:SortHeader = p_web.Translate('Field')
    p_web.SetSessionValue('BrowseJobFaultCodeLookup_LocatorPic','@s30')
  Of upper('mfo:Description')
    loc:SortHeader = p_web.Translate('Description')
    p_web.SetSessionValue('BrowseJobFaultCodeLookup_LocatorPic','@s60')
  End
  If loc:selecting = 1
    p_web.GetSettings(p_web.GetSessionValue('Push1'))
    loc:selectaction = p_web.FormSettings.ParentPage !p_web.GetSessionValue('BrowseJobFaultCodeLookup:LookupFrom')
  End!Else
  do SendPacket
  do SetFormAction
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseJobFaultCodeLookup:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseJobFaultCodeLookup:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseJobFaultCodeLookup:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="MANFAULO"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="mfo:RecordNumberKey"></input><13,10>'
  end
  If p_web.Translate('Select Fault Codes') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.browseSubHeading,)&'">'&p_web.Translate('Select Fault Codes',0)&'</div>'&CRLF
  End
  case p_web.GetValue('refresh') ; of 'next' orof 'previous' orof 'first' orof 'last'
    p_web.Script('try{{BrowseJobFaultCodeLookup.restoreFocus();} catch(err){{};')
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseJobFaultCodeLookup',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      case loc:locatortype
      of Net:Position
      orof Net:Date
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
      of Net:Contains
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
      of Net:Begins
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
      End
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
      Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
      Case Loc:LocatorType
      of Net:Date
        packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator2BrowseJobFaultCodeLookup','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('BrowseJobFaultCodeLookup_LocatorPic'),,,'onchange="BrowseJobFaultCodeLookup.locate(''Locator2BrowseJobFaultCodeLookup'',this.value);"',,0,,,) & '</div></td>'
      Else
        packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator2BrowseJobFaultCodeLookup',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="BrowseJobFaultCodeLookup.locate(''Locator2BrowseJobFaultCodeLookup'',this.value);" ',,,,,,) & '</td>'
      End
      If loc:LocatorSearchButton or loc:LocatorClearButton
        packet = clip(packet) & '<td><div id="BrowseJobFaultCodeLookup_locate_a" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
        If loc:LocatorSearchButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
        End
        If loc:LocatorClearButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseJobFaultCodeLookup.cl(''BrowseJobFaultCodeLookup'');')
        End
        packet = clip(packet) & '</div><13,10></td><13,10>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
      If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'BrowseJobFaultCodeLookup_locate_a','buttonset',loc:options)
      End
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & p_web.DivHeader('BrowseJobFaultCodeLookup_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'','sortable')&'" id="BrowseJobFaultCodeLookup_tbl">'
  Else
    packet = clip(packet) & p_web.DivHeader('BrowseJobFaultCodeLookup_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'')&'" id="BrowseJobFaultCodeLookup_tbl">'
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
      If loc:Selecting = 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'3','BrowseJobFaultCodeLookup',p_web.Translate('Pick'),,,,,,,0,loc:Sorting,'Button')
          do AddPacket
          loc:columns += 1
      End ! Selecting
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'1','BrowseJobFaultCodeLookup',p_web.Translate('Field'),'Click here to sort by Field',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'2','BrowseJobFaultCodeLookup',p_web.Translate('Description'),'Click here to sort by Description',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,15,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  If Instring('mfo:recordnumber',lower(loc:vorder),1,1) = 0 !and MANFAULO{prop:SQLDriver} = 1
    loc:vorder = Choose(loc:vorder='','mfo:RecordNumber',clip(loc:vorder) & ',' & 'mfo:RecordNumber')
  End
  Loc:Selected = Choose(p_web.IfExistsValue('mfo:RecordNumber'),p_web.GetValue('mfo:RecordNumber'),p_web.GetSessionValue('mfo:RecordNumber'))
  ThisView{prop:order} = p_web.CleanFilter(ThisView,clip(loc:vorder))
      loc:FilterWas = 'Upper(mfo:Manufacturer) = Upper(''' & p_web.GSV('job:Manufacturer') & ''') AND Upper(mfo:Field_Number) = ' & p_web.GSV('fieldNumber')
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseJobFaultCodeLookup',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseJobFaultCodeLookup_Filter')
    p_web.SetSessionValue('BrowseJobFaultCodeLookup_FirstValue','')
    p_web.SetSessionValue('BrowseJobFaultCodeLookup_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,MANFAULO,mfo:RecordNumberKey,loc:PageRows,'BrowseJobFaultCodeLookup',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,loc:ViewOptions,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled,Loc:LocatorCase)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or Loc:LocatorType = Net:Date or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position or loc:LocatorType = Net:Date
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If MANFAULO{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(MANFAULO,loc:firstvalue)
              Reset(ThisView,MANFAULO)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If MANFAULO{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(MANFAULO,loc:lastvalue)
            Reset(ThisView,MANFAULO)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      if (mfo:NotAvailable)
          ! #11655 Don't show if not available (Bryan: 24/08/2010)
          CYCLE
      end
      
      if (p_web.GSV('partType') = 'C')
          if (mfo:RestrictLookup = 1)
              if (p_web.GSV('par:Adjustment') = 'YES')
                  if (mfo:RestrictLookupType <> 3)
                      Cycle
                  end ! if (mfo:RestrictLookupType <> 3)
              else ! if (p_web.GSV('par:Adjustment') = 'YES')
                  if (p_web.GSV('par:Correction') = 1)
                      if (mfo:RestrictLookupType <> 2)
                          Cycle
                      end ! if (mfo:RestrictLookupType <> 2)
                  else ! if (p_web.GSV('par:Correction') = 1)
                      if (mfo:RestrictLookupType <> 1)
                          Cycle
                      end ! if (mfo:RestrictLookupType <> 1)
                  end ! if (p_web.GSV('par:Correction') = 1)
              end ! if (p_web.GSV('par:Adjustment') = 'YES')
          end ! if (mfo:RestrictLookup = 1)
      end ! if (p_web.GSV('partType') = 'C')
      
      if (p_web.GSV('partType') = 'W')
          if (mfo:RestrictLookup = 1)
              if (p_web.GSV('wpr:Adjustment') = 'YES')
                  if (mfo:RestrictLookupType <> 3)
                      Cycle
                  end ! if (mfo:RestrictLookupType <> 3)
              else ! if (p_web.GSV('par:Adjustment') = 'YES')
                  if (p_web.GSV('wpr:Correction') = 1)
                      if (mfo:RestrictLookupType <> 2)
                          Cycle
                      end ! if (mfo:RestrictLookupType <> 2)
                  else ! if (p_web.GSV('par:Correction') = 1)
                      if (mfo:RestrictLookupType <> 1)
                          Cycle
                      end ! if (mfo:RestrictLookupType <> 1)
                  end ! if (p_web.GSV('par:Correction') = 1)
              end ! if (p_web.GSV('par:Adjustment') = 'YES')
          end ! if (mfo:RestrictLookup = 1)
      end ! if (p_web.GSV('partType') = 'C')
      
      if (mfo:JobTypeAvailability = 1 and p_web.GSV('partType') <> '')
          if (p_web.GSV('partType') <> 'C')
              Cycle
          end ! if (p_web.GSV('job:Warranty_Job') = 'YES')
      end ! if (mfo:JobTypeAvailability = 1)
      
      if (mfo:JobTypeAvailability = 2 and p_web.GSV('partType') <> '')
          if (p_web.GSV('parType') <> 'W')
              Cycle
          end ! if (p_web.GSV('job:Chargeable_Job') = 'YES')
      end ! if (mfo:JobTypeAvailability = 2)
      
      
      !if (p_web.GSV('partMainFault') = 0)
          found# = 1
          clear(tmpfau:record)
          tmpfau:sessionID = p_web.sessionID
          set(tmpfau:keySessionID,tmpfau:keySessionID)
          loop
              next(tempFaultCodes)
              if (error())
                  break
              end ! if (error())
              if (tmpfau:sessionID <> p_web.sessionID)
                  break
              end ! if (tmpfau:sessionID <> p_web.sessionID)
      
              found# = 0
              case tmpfau:FaultType
              of 'P' ! Part Fault Code
                  Access:MANFPARL.Clearkey(mpr:LinkedRecordNumberKey)
                  mpr:MANFPALORecordNumber    = tmpfau:RecordNumber
                  mpr:LinkedRecordNumber    = mfo:RecordNumber
                  mpr:JobFaultCode    = 0
                  if (Access:MANFPARL.TryFetch(mpr:LinkedRecordNumberKey) = Level:Benign)
                      ! Found
                      found# = 1
                      break
                  else ! if (Access:MANFPARL.TryFetch(mpr:LinkedRecordNumberKey) = Level:Benign)
                      ! Error
                  end ! if (Access:MANFPARL.TryFetch(mpr:LinkedRecordNumberKey) = Level:Benign)
      
              of 'J' ! Job Fault Code
      
                  Access:MANFAURL.Clearkey(mnr:LinkedRecordNumberKey)
                  mnr:MANFAULORecordNumber    = tmpfau:RecordNumber
                  mnr:LinkedRecordNumber    = mfo:RecordNumber
                  mnr:PartFaultCode    = 1
                  if (Access:MANFAURL.TryFetch(mnr:LinkedRecordNumberKey) = Level:Benign)
                      ! Found
                      found# = 1
                      break
                  else ! if (Access:MANFAURL.TryFetch(mnr:LinkedRecordNumberKey) = Level:Benign)
                      ! Error
                  end ! if (Access:MANFAURL.TryFetch(mnr:LinkedRecordNumberKey) = Level:Benign)
              end ! case faultQueue.FaultType
          end ! loop
      
          if (found# = 0)
              cycle
          end ! if (found# = 0)
      !end !if (p_web.GSV('partMainFault') = 0)
      
      if (p_web.GSV('partMainFault') = 1 and p_web.GSV('relatedPartCode') <> 0)
          if (mfo:RelatedPartCode <> p_web.GSV('relatedPartCode'))
              Cycle
          end ! if (mfo:RelatedPartCode <> p_web.GSV('relatedPartCode'))
      end ! if (p_web.GSV('relatedPartCode') <> 0)
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(mfo:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td class="'&p_web.combine(p_web.site.style.BrowseEmpty,)&'">'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:NavButtonPosition=Net:Above or (loc:NavButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseJobFaultCodeLookup_nav_a" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseJobFaultCodeLookup.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseJobFaultCodeLookup.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseJobFaultCodeLookup.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseJobFaultCodeLookup.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseJobFaultCodeLookup_nav_a','buttonset',loc:options)
          End
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:UpdateButtonPosition=Net:Above or (loc:UpdateButtonPosition=Net:Both and loc:found > 0))
    If loc:selecting = 1
      If loc:found
        packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SelectButton,'BrowseJobFaultCodeLookup',,,loc:popup)
      End
      TableQueue.Kind = Net:BeforeTable
      do AddPacket
    End
    If loc:selecting = 1 !and loc:parent = ''
      if loc:popup
        packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:BrowseCancelButton,'BrowseJobFaultCodeLookup',,,loc:popup)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
      end
      TableQueue.Kind = Net:BeforeTable
      do AddPacket
    End
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseJobFaultCodeLookup',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseJobFaultCodeLookup_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseJobFaultCodeLookup_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          case loc:locatortype
          of Net:Position
          orof Net:Date
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
          of Net:Contains
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
          of Net:Begins
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
          End
          packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
          Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
          Case Loc:LocatorType
          of Net:Date
            packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator1BrowseJobFaultCodeLookup','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('BrowseJobFaultCodeLookup_LocatorPic'),,,'onchange="BrowseJobFaultCodeLookup.locate(''Locator1BrowseJobFaultCodeLookup'',this.value);"',,0,,,) & '</div></td>'
          Else
            packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator1BrowseJobFaultCodeLookup',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="BrowseJobFaultCodeLookup.locate(''Locator1BrowseJobFaultCodeLookup'',this.value);" ',,,,,,) & '</td>'
          End
          If loc:LocatorSearchButton or loc:LocatorClearButton
            packet = clip(packet) & '<td><div id="BrowseJobFaultCodeLookup_locate_b" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
            If loc:LocatorSearchButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
            End
            If loc:LocatorClearButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseJobFaultCodeLookup.cl(''BrowseJobFaultCodeLookup'');')
            End
            packet = clip(packet) & '</div><13,10></td><13,10>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
          If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseJobFaultCodeLookup_locate_b','buttonset',loc:options)
          End
      End
    End
  End
  p_web.SetSessionValue('BrowseJobFaultCodeLookup_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseJobFaultCodeLookup_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:NavButtonPosition=Net:Below or loc:NavButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseJobFaultCodeLookup_nav_b" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseJobFaultCodeLookup.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseJobFaultCodeLookup.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseJobFaultCodeLookup.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseJobFaultCodeLookup.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseJobFaultCodeLookup_nav_b','buttonset',loc:options)
          End
        end
        do SendPacket
  End
  If loc:UpdateButtonPosition=Net:Below or loc:UpdateButtonPosition=Net:Both
  If loc:selecting = 1
    If loc:found
      packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SelectButton,'BrowseJobFaultCodeLookup',,,loc:popup)
    End
    do SendPacket
  End
  If loc:selecting = 1 !and loc:parent = ''
    if loc:popup
      packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:BrowseCancelButton,'BrowseJobFaultCodeLookup',,,loc:popup)
    else
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
    end
    do SendPacket
  End
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
  data
loc:rowclick  string(1024)
  code
    loc:field = p_web.AddBrowseValue('BrowseJobFaultCodeLookup','MANFAULO',mfo:RecordNumberKey) !mfo:RecordNumber
    p_web._thisrow = p_web._nocolon('mfo:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if Loc:LocatorValue <> '' and loc:ActualSelection = ''
        loc:checked = 'checked'
        do SetSelection
      elsif loc:ActualSelection = '' and mfo:RecordNumber = p_web.GetValue('mfo:RecordNumber')
         loc:checked = 'checked'
         do SetSelection
      elsif loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseJobFaultCodeLookup:LookupField')) = mfo:RecordNumber and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      else
        loc:checked = Choose((mfo:RecordNumber = loc:selected) and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      end
      If(loc:SelectionMethod  = Net:Radio)
        loc:RowStyle = 'class="' & p_web.combine(p_web.site.style.browserow,) &'"'
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'data-nt-id="'& p_web.AddBrowseValue('BrowseJobFaultCodeLookup','MANFAULO',mfo:RecordNumberKey) &'" onclick="'
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If MANFAULO{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(MANFAULO)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            loc:DefaultSelection = loc:field
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If MANFAULO{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(MANFAULO)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            loc:DefaultSelection = loc:field
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'>'&p_web.CreateInput('radio','_bidv_',clip(loc:field),,loc:checked,'')&'</td>'&CRLF
          If loc:DefaultSelection = ''
            loc:DefaultSelection = loc:field
          End
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          If loc:DefaultSelection = '' or loc:direction < 0
            loc:DefaultSelection = loc:field
          End
        End
    end ! loc:eip = 0
        If Loc:Selecting = 1
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::Select
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        End     !Selecting
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::mfo:Field
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::mfo:Description
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr ' & clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Idx = p_web.AddBrowseValue('BrowseJobFaultCodeLookup','MANFAULO',mfo:RecordNumberKey)
  TableQueue.Id[1] = mfo:RecordNumber

ClosingScripts  Routine
  data
Rtn_SecwinProcedureName         string(252)
  code
  If p_web.RequestAjax = 0
    do SetFormAction
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var btiBrowseJobFaultCodeLookup;if (btiBrowseJobFaultCodeLookup != 1){{var BrowseJobFaultCodeLookup=new browseTable(''BrowseJobFaultCodeLookup'','''&clip(loc:formname)&|
      ''','''&p_web._jsok('mfo:RecordNumber',Net:Parameter)&''',2,'''&clip(loc:divname)&''',1,1,1,'''&|
      clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&|
      p_web.Translate('Are you sure you want to delete this record?')&''','''&clip(loc:ActualSelection)&''','''&clip(loc:selectaction)&''','''&|
      clip(loc:formactiontarget)&''','''','&loc:popup&',0,'''','''',"' & clip(Rtn_SecwinProcedureName) & '");<13,10>'&|
      'BrowseJobFaultCodeLookup.setGreenBar('''&p_web.ColorWeb(p_web.Site.Style.BrowseHighlightColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOneColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseTwoColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOverColor)&''');<13,10>' &|
      'BrowseJobFaultCodeLookup.applyGreenBar();btiBrowseJobFaultCodeLookup=1};<13,10>' &|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseJobFaultCodeLookup')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseJobFaultCodeLookup')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseJobFaultCodeLookup')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseJobFaultCodeLookup')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(MANFAULO)
  p_web._CloseFile(MANFAUPA)
  p_web._CloseFile(MODELCCT)
  p_web._CloseFile(MANFAULO_ALIAS)
  p_web._CloseFile(MANFPARL)
  p_web._CloseFile(MANFAUPA_ALIAS)
  p_web._CloseFile(MANFPALO_ALIAS)
  p_web._CloseFile(MANFAURL)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(MANFAULO)
  Bind(mfo:Record)
  Clear(mfo:Record)
  NetWebSetSessionPics(p_web,MANFAULO)
  p_web._OpenFile(MANFAUPA)
  Bind(map:Record)
  NetWebSetSessionPics(p_web,MANFAUPA)
  p_web._OpenFile(MODELCCT)
  Bind(mcc:Record)
  NetWebSetSessionPics(p_web,MODELCCT)
  p_web._OpenFile(MANFAULO_ALIAS)
  Bind(mfo_ali:Record)
  NetWebSetSessionPics(p_web,MANFAULO_ALIAS)
  p_web._OpenFile(MANFPARL)
  Bind(mpr:Record)
  NetWebSetSessionPics(p_web,MANFPARL)
  p_web._OpenFile(MANFAUPA_ALIAS)
  Bind(map_ali:Record)
  NetWebSetSessionPics(p_web,MANFAUPA_ALIAS)
  p_web._OpenFile(MANFPALO_ALIAS)
  Bind(mfp_ali:Record)
  NetWebSetSessionPics(p_web,MANFPALO_ALIAS)
  p_web._OpenFile(MANFAURL)
  Bind(mnr:Record)
  NetWebSetSessionPics(p_web,MANFAURL)

Children Routine
  if loc:selecting = 0
    If p_web.RequestAjax = 0
      do StartChildren
    Else
      do AjaxChildren
    End
  end

AjaxChildren  Routine
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue('mfo:RecordNumber',p_web.GetValue('mfo:RecordNumber'))
   p_web.DeleteValue('_Clicked')
   loc:found = 1
   do Children
! ----------------------------------------------------------------------------------------
CallRow  Routine
  data
loc:result  long
  code
  do OpenFilesB
  mfo:RecordNumber = p_web.GSV('mfo:RecordNumber')
  loc:result = p_web._GetFile(MANFAULO,mfo:RecordNumberKey)
  loc:eip = 1
  loc:viewstate = p_web.escape(p_web.Base64Encode(clip(mfo:RecordNumber)))
  do BrowseRow
  do SendPacket
  do ClosefilesB

! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(MANFAULO)
  Case upper(p_web.GetValue('_EIPClm'))
  Else
    case p_web.GetValue('_action_')
    of Net:InsertRecord
    orof Net:ChangeRecord
    orof Net:DeleteRecord
    orof Net:CopyRecord
      loc:eip = 0
      do CallBrowse
    end
  End
  do GotFocusBack
  p_web._CloseFile(MANFAULO)
! ----------------------------------------------------------------------------------------
value::Select   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    p_web.site.SmallSelectButton.TextValue = p_web.Translate('>>')
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseJobFaultCodeLookup_Select_'&mfo:RecordNumber,,net:crc,,loc:extra)
        packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'BrowseJobFaultCodeLookup',p_web.AddBrowseValue('BrowseJobFaultCodeLookup','MANFAULO',mfo:RecordNumberKey),,loc:popup)
    End
    p_web.site.SmallSelectButton = p_web.RequestData.WebServer._SitesQueue.Defaults.SmallSelectButton  !bruce
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::mfo:Field   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseJobFaultCodeLookup_mfo:Field_'&mfo:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(mfo:Field,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::mfo:Description   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseJobFaultCodeLookup_mfo:Description_'&mfo:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(mfo:Description,'@s60')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  p_web._OpenFile(MANFAUPA)
  p_web._OpenFile(MODELCCT)
  p_web._OpenFile(MANFAULO_ALIAS)
  p_web._OpenFile(MANFPARL)
  p_web._OpenFile(MANFAUPA_ALIAS)
  p_web._OpenFile(MANFPALO_ALIAS)
  p_web._OpenFile(MANFAURL)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(MANFAUPA)
  p_Web._CloseFile(MODELCCT)
  p_Web._CloseFile(MANFAULO_ALIAS)
  p_Web._CloseFile(MANFPARL)
  p_Web._CloseFile(MANFAUPA_ALIAS)
  p_Web._CloseFile(MANFPALO_ALIAS)
  p_Web._CloseFile(MANFAURL)
     FilesOpened = False
  END
  p_web.deleteSessionValue('fieldNumber')
  p_web.deleteSessionValue('partType')
  p_web.deleteSessionValue('partMainFault')
  p_web.deleteSessionValue('relatedPartCode')
  return
!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
CheckForDuplicate  Routine
SetSelection  Routine
  loc:ActualSelection = loc:field
  p_web.SetSessionValue('mfo:RecordNumber',mfo:RecordNumber)

MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:ActualSelection = ''
    loc:ActualSelection = loc:DefaultSelection
    p_web.GetBrowseValue(loc:ActualSelection,Net:Web:SessionQueue) ! so children are primed with correct sessionValue
  End

  If loc:ActualSelection <> ''
    TableQueue.Kind = Net:RowTable
    get(TableQueue,TableQueue.Kind)
    if Errorcode() = 0
      TableQueue.Row = sub(TableQueue.Row,1,len(clip(TableQueue.Row))-1) &  ' data-nt-rows="'&loc:RowsHigh&'" data-nt-value="'&clip(loc:ActualSelection)&'"' & '>'
      Put(TableQueue)
    End
  End

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead class="'&p_web.combine(p_web.site.style.BrowseHeader,'')&'"><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot class="'&p_web.combine(p_web.site.style.BrowseFooter,)&'"><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody class="'&p_web.combine(p_web.site.style.BrowseBody,)&'"><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table><13,10>' & p_web.DivFooter(Net:NoSend) ! Table Div
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
JobEstimate          PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locEstAccBy          STRING(30)                            !
locEstAccCommunicationMethod STRING(30)                    !
locEstRejBy          STRING(30)                            !
locEstRejCommunicationMethod STRING(30)                    !
locEstRejReason      STRING(30)                            !
FilesOpened     Long
AUDSTATS::State  USHORT
JOBS::State  USHORT
ESREJRES::State  USHORT
PARTS::State  USHORT
job:Estimate:IsInvalid  Long
job:Estimate_If_Over:IsInvalid  Long
job:Estimate_Ready:IsInvalid  Long
job:Estimate_Accepted:IsInvalid  Long
job:Estimate_Rejected:IsInvalid  Long
textEstimateAccepted:IsInvalid  Long
locEstAccBy:IsInvalid  Long
locEstAccCommunicationMethod:IsInvalid  Long
textEstimateRejected:IsInvalid  Long
locEstRejBy:IsInvalid  Long
locEstRejCommunicationMethod:IsInvalid  Long
locEstRejReason:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
locEstRejReason_OptionView   View(ESREJRES)
                          Project(esr:RejectionReason)
                        End
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  GlobalErrors.SetProcedureName('JobEstimate')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'JobEstimate_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('JobEstimate','')
    p_web.DivHeader('JobEstimate',p_web.combine(p_web.site.style.formdiv,'fdiv'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('JobEstimate') = 0
        p_web.AddPreCall('JobEstimate')
        p_web.DivHeader('popup_JobEstimate','nt-hidden')
        p_web.DivHeader('JobEstimate',p_web.combine(p_web.site.style.formdiv,'fdiv'))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_JobEstimate_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_JobEstimate_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferJobEstimate',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_JobEstimate',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferJobEstimate',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_JobEstimate',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferJobEstimate',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_JobEstimate',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_JobEstimate',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferJobEstimate',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_JobEstimate',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferJobEstimate',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_JobEstimate',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_JobEstimate',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('JobEstimate')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(AUDSTATS)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(ESREJRES)
  p_web._OpenFile(PARTS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(AUDSTATS)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(ESREJRES)
  p_Web._CloseFile(PARTS)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  ! Initi
      p_web.SSV('Hide:EstimateAccepted',1)
      p_web.SSV('Hide:EstimateRejected',1)
      p_web.SSV('Comment:EstimateReady','')
      p_web.SSV('Comment:EstimateAccepted','')
      p_web.SSV('Comment:EstimateRejected','')
  p_web.SetValue('JobEstimate_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'JobEstimate'
    end
    p_web.formsettings.proc = 'JobEstimate'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('job:Estimate_If_Over')
    p_web.SetPicture('job:Estimate_If_Over','@n14.2')
  End
  p_web.SetSessionPicture('job:Estimate_If_Over','@n14.2')

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  if p_web.IfExistsValue('job:Estimate') = 0
    p_web.SetSessionValue('job:Estimate',job:Estimate)
  Else
    job:Estimate = p_web.GetSessionValue('job:Estimate')
  End
  if p_web.IfExistsValue('job:Estimate_If_Over') = 0
    p_web.SetSessionValue('job:Estimate_If_Over',job:Estimate_If_Over)
  Else
    job:Estimate_If_Over = p_web.GetSessionValue('job:Estimate_If_Over')
  End
  if p_web.IfExistsValue('job:Estimate_Ready') = 0
    p_web.SetSessionValue('job:Estimate_Ready',job:Estimate_Ready)
  Else
    job:Estimate_Ready = p_web.GetSessionValue('job:Estimate_Ready')
  End
  if p_web.IfExistsValue('job:Estimate_Accepted') = 0
    p_web.SetSessionValue('job:Estimate_Accepted',job:Estimate_Accepted)
  Else
    job:Estimate_Accepted = p_web.GetSessionValue('job:Estimate_Accepted')
  End
  if p_web.IfExistsValue('job:Estimate_Rejected') = 0
    p_web.SetSessionValue('job:Estimate_Rejected',job:Estimate_Rejected)
  Else
    job:Estimate_Rejected = p_web.GetSessionValue('job:Estimate_Rejected')
  End
  if p_web.IfExistsValue('locEstAccBy') = 0
    p_web.SetSessionValue('locEstAccBy',locEstAccBy)
  Else
    locEstAccBy = p_web.GetSessionValue('locEstAccBy')
  End
  if p_web.IfExistsValue('locEstAccCommunicationMethod') = 0
    p_web.SetSessionValue('locEstAccCommunicationMethod',locEstAccCommunicationMethod)
  Else
    locEstAccCommunicationMethod = p_web.GetSessionValue('locEstAccCommunicationMethod')
  End
  if p_web.IfExistsValue('locEstRejBy') = 0
    p_web.SetSessionValue('locEstRejBy',locEstRejBy)
  Else
    locEstRejBy = p_web.GetSessionValue('locEstRejBy')
  End
  if p_web.IfExistsValue('locEstRejCommunicationMethod') = 0
    p_web.SetSessionValue('locEstRejCommunicationMethod',locEstRejCommunicationMethod)
  Else
    locEstRejCommunicationMethod = p_web.GetSessionValue('locEstRejCommunicationMethod')
  End
  if p_web.IfExistsValue('locEstRejReason') = 0
    p_web.SetSessionValue('locEstRejReason',locEstRejReason)
  Else
    locEstRejReason = p_web.GetSessionValue('locEstRejReason')
  End

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('job:Estimate')
    job:Estimate = p_web.GetValue('job:Estimate')
    p_web.SetSessionValue('job:Estimate',job:Estimate)
  Else
    job:Estimate = p_web.GetSessionValue('job:Estimate')
  End
  if p_web.IfExistsValue('job:Estimate_If_Over')
    job:Estimate_If_Over = p_web.dformat(clip(p_web.GetValue('job:Estimate_If_Over')),'@n14.2')
    p_web.SetSessionValue('job:Estimate_If_Over',job:Estimate_If_Over)
  Else
    job:Estimate_If_Over = p_web.GetSessionValue('job:Estimate_If_Over')
  End
  if p_web.IfExistsValue('job:Estimate_Ready')
    job:Estimate_Ready = p_web.GetValue('job:Estimate_Ready')
    p_web.SetSessionValue('job:Estimate_Ready',job:Estimate_Ready)
  Else
    job:Estimate_Ready = p_web.GetSessionValue('job:Estimate_Ready')
  End
  if p_web.IfExistsValue('job:Estimate_Accepted')
    job:Estimate_Accepted = p_web.GetValue('job:Estimate_Accepted')
    p_web.SetSessionValue('job:Estimate_Accepted',job:Estimate_Accepted)
  Else
    job:Estimate_Accepted = p_web.GetSessionValue('job:Estimate_Accepted')
  End
  if p_web.IfExistsValue('job:Estimate_Rejected')
    job:Estimate_Rejected = p_web.GetValue('job:Estimate_Rejected')
    p_web.SetSessionValue('job:Estimate_Rejected',job:Estimate_Rejected)
  Else
    job:Estimate_Rejected = p_web.GetSessionValue('job:Estimate_Rejected')
  End
  if p_web.IfExistsValue('locEstAccBy')
    locEstAccBy = p_web.GetValue('locEstAccBy')
    p_web.SetSessionValue('locEstAccBy',locEstAccBy)
  Else
    locEstAccBy = p_web.GetSessionValue('locEstAccBy')
  End
  if p_web.IfExistsValue('locEstAccCommunicationMethod')
    locEstAccCommunicationMethod = p_web.GetValue('locEstAccCommunicationMethod')
    p_web.SetSessionValue('locEstAccCommunicationMethod',locEstAccCommunicationMethod)
  Else
    locEstAccCommunicationMethod = p_web.GetSessionValue('locEstAccCommunicationMethod')
  End
  if p_web.IfExistsValue('locEstRejBy')
    locEstRejBy = p_web.GetValue('locEstRejBy')
    p_web.SetSessionValue('locEstRejBy',locEstRejBy)
  Else
    locEstRejBy = p_web.GetSessionValue('locEstRejBy')
  End
  if p_web.IfExistsValue('locEstRejCommunicationMethod')
    locEstRejCommunicationMethod = p_web.GetValue('locEstRejCommunicationMethod')
    p_web.SetSessionValue('locEstRejCommunicationMethod',locEstRejCommunicationMethod)
  Else
    locEstRejCommunicationMethod = p_web.GetSessionValue('locEstRejCommunicationMethod')
  End
  if p_web.IfExistsValue('locEstRejReason')
    locEstRejReason = p_web.GetValue('locEstRejReason')
    p_web.SetSessionValue('locEstRejReason',locEstRejReason)
  Else
    locEstRejReason = p_web.GetSessionValue('locEstRejReason')
  End

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('JobEstimate_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'ViewJob'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('JobEstimate_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('JobEstimate_ChainTo')
    loc:formaction = p_web.GetSessionValue('JobEstimate_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'ViewJob'

GenerateForm   Routine
  do LoadRelatedRecords
 locEstAccBy = p_web.RestoreValue('locEstAccBy')
 locEstAccCommunicationMethod = p_web.RestoreValue('locEstAccCommunicationMethod')
 locEstRejBy = p_web.RestoreValue('locEstRejBy')
 locEstRejCommunicationMethod = p_web.RestoreValue('locEstRejCommunicationMethod')
 locEstRejReason = p_web.RestoreValue('locEstRejReason')
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Estimate Details') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Estimate Details',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_JobEstimate',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_JobEstimate0_div')&'">'&p_web.Translate()&'</a></li>'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_JobEstimate1_div')&'">'&p_web.Translate()&'</a></li>'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_JobEstimate2_div')&'">'&p_web.Translate()&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="JobEstimate_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.cancel();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CancelButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="JobEstimate_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'JobEstimate_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="JobEstimate_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'JobEstimate_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.job:Estimate')
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_JobEstimate')>0,p_web.GSV('showtab_JobEstimate'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_JobEstimate'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_JobEstimate') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_JobEstimate'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_JobEstimate')>0,p_web.GSV('showtab_JobEstimate'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_JobEstimate') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_JobEstimate_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_JobEstimate')>0,p_web.GSV('showtab_JobEstimate'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"JobEstimate",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_JobEstimate')>0,p_web.GSV('showtab_JobEstimate'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_JobEstimate_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('JobEstimate') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('JobEstimate')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate()&'</a></h3>' & CRLF & p_web.DivHeader('tab_JobEstimate0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_JobEstimate0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_JobEstimate0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_JobEstimate0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,)
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_JobEstimate0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend)
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_JobEstimate0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain">' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_JobEstimate0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&150&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Estimate
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&200&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Estimate
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::job:Estimate
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&150&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Estimate_If_Over
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&200&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Estimate_If_Over
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::job:Estimate_If_Over
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&150&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Estimate_Ready
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&200&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Estimate_Ready
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::job:Estimate_Ready
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&150&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Estimate_Accepted
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&200&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Estimate_Accepted
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::job:Estimate_Accepted
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&150&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::job:Estimate_Rejected
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&200&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::job:Estimate_Rejected
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::job:Estimate_Rejected
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab1  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate()&'</a></h3>' & CRLF & p_web.DivHeader('tab_JobEstimate1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_JobEstimate1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_JobEstimate1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_JobEstimate1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,)
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_JobEstimate1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend)
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_JobEstimate1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain">' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_JobEstimate1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&150&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&200&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::textEstimateAccepted
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::textEstimateAccepted
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&150&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locEstAccBy
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&200&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locEstAccBy
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locEstAccBy
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&150&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locEstAccCommunicationMethod
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&200&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locEstAccCommunicationMethod
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locEstAccCommunicationMethod
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab2  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate()&'</a></h3>' & CRLF & p_web.DivHeader('tab_JobEstimate2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_JobEstimate2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_JobEstimate2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_JobEstimate2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,)
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_JobEstimate2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend)
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_JobEstimate2',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain">' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_JobEstimate2',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&150&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&200&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::textEstimateRejected
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::textEstimateRejected
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&150&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locEstRejBy
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&200&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locEstRejBy
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locEstRejBy
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&150&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locEstRejCommunicationMethod
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&200&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locEstRejCommunicationMethod
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locEstRejCommunicationMethod
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ' width="'&150&'"'
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locEstRejReason
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ' width="'&200&'"'
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locEstRejReason
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::locEstRejReason
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Prompt::job:Estimate  Routine
  packet = clip(packet) & p_web.DivHeader('JobEstimate_' & p_web._nocolon('job:Estimate') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Estimate Required'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Estimate  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Estimate = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value','NO')
    end
    job:Estimate = p_web.GetValue('Value')
  End
  do ValidateValue::job:Estimate  ! copies value to session value if valid.
  if (p_web.GSV('job:Estimate') = 'YES')
      ! #11656 Set status when estimate = yes / no (Bryan: 23/08/2010)
  
      getStatus(505,0,'JOB',p_web)
  ELSE
      ! Get the previous Status
      if (p_web.GSV('job:Current_Status') = '505 ESTIMATE REQUIRED')
          Access:AUDSTATS.CLearkey(aus:StatusTypeRecordKey)
          aus:RefNumber = p_web.GSV('job:Ref_Number')
          aus:Type = 'JOB'
          Set(aus:StatusTypeRecordKey,aus:StatusTypeRecordKey)
          Loop Until Access:AUDSTATS.Next()
              if (aus:RefNumber <> p_web.GSV('job:Ref_Number'))
                  break
              end
              if (aus:Type <> 'JOB')
                  cycle
              end
              if (aus:NewStatus = '505 ESTIMATE REQUIRED')
                  p_web.SSV('GetStatus:Type','JOB')
                  p_web.SSV('GetStatus:StatusNumber',sub(aus:OldStatus,1,3))
                  GetStatus(p_web.GSV('GetStatus:StatusNumber'),0,p_web.GSV('GetStatus:Type'),p_web)
                  break
              end
          end
      end
  END
  do Value::job:Estimate
  do SendAlert
  do Comment::job:Estimate ! allows comment style to be updated.
  do Prompt::job:Estimate_Accepted
  do Value::job:Estimate_Accepted  !1
  do Prompt::job:Estimate_If_Over
  do Value::job:Estimate_If_Over  !1
  do Prompt::job:Estimate_Ready
  do Value::job:Estimate_Ready  !1
  do Prompt::job:Estimate_Rejected
  do Value::job:Estimate_Rejected  !1

ValidateValue::job:Estimate  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('job:Estimate',job:Estimate).
    End

Value::job:Estimate  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobEstimate_' & p_web._nocolon('job:Estimate') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If loc:retrying
    job:Estimate = p_web.RestoreValue('job:Estimate')
    do ValidateValue::job:Estimate
    If job:Estimate:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- CHECKBOX --- job:Estimate
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''job:Estimate'',''jobestimate_job:estimate_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Estimate')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('job:Estimate') = 'YES'
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','job:Estimate',clip('YES'),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::job:Estimate  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if job:Estimate:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('JobEstimate_' & p_web._nocolon('job:Estimate') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::job:Estimate_If_Over  Routine
  packet = clip(packet) & p_web.DivHeader('JobEstimate_' & p_web._nocolon('job:Estimate_If_Over') & '_prompt',Choose(p_web.GSV('job:Estimate') <> 'YES','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('job:Estimate') <> 'YES','',p_web.Translate('Estimate If Over'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Estimate_If_Over  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Estimate_If_Over = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture = '@n14.2'  !FieldPicture = @n14.2
    job:Estimate_If_Over = p_web.Dformat(p_web.GetValue('Value'),'@n14.2')
  End
  do ValidateValue::job:Estimate_If_Over  ! copies value to session value if valid.
  do Value::job:Estimate_If_Over
  do SendAlert
  do Comment::job:Estimate_If_Over ! allows comment style to be updated.

ValidateValue::job:Estimate_If_Over  Routine
    If not (p_web.GSV('job:Estimate') <> 'YES')
      if loc:invalid = '' then p_web.SetSessionValue('job:Estimate_If_Over',job:Estimate_If_Over).
    End

Value::job:Estimate_If_Over  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('job:Estimate') <> 'YES','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobEstimate_' & p_web._nocolon('job:Estimate_If_Over') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    job:Estimate_If_Over = p_web.RestoreValue('job:Estimate_If_Over')
    do ValidateValue::job:Estimate_If_Over
    If job:Estimate_If_Over:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('job:Estimate') <> 'YES')
  ! --- STRING --- job:Estimate_If_Over
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Estimate_If_Over'',''jobestimate_job:estimate_if_over_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Estimate_If_Over',p_web.GetSessionValue('job:Estimate_If_Over'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::job:Estimate_If_Over  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if job:Estimate_If_Over:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('job:Estimate') <> 'YES','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('JobEstimate_' & p_web._nocolon('job:Estimate_If_Over') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('job:Estimate') <> 'YES'
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::job:Estimate_Ready  Routine
  packet = clip(packet) & p_web.DivHeader('JobEstimate_' & p_web._nocolon('job:Estimate_Ready') & '_prompt',Choose(p_web.GSV('job:Estimate') <> 'YES' OR p_web.GSV('job:Estimate_Ready') <> 'YES' or (p_web.GSV('job:Estimate_Ready') = 'YES' and (p_web.GSV('job:Estimate_Accepted') = 'YES' or p_web.GSV('job:Estimate_Rejected') = 'YES')),'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('job:Estimate') <> 'YES' OR p_web.GSV('job:Estimate_Ready') <> 'YES' or (p_web.GSV('job:Estimate_Ready') = 'YES' and (p_web.GSV('job:Estimate_Accepted') = 'YES' or p_web.GSV('job:Estimate_Rejected') = 'YES')),'',p_web.Translate('Estimate Ready'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Estimate_Ready  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Estimate_Ready = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value','NO')
    end
    job:Estimate_Ready = p_web.GetValue('Value')
  End
  do ValidateValue::job:Estimate_Ready  ! copies value to session value if valid.
  do Value::job:Estimate_Ready
  do SendAlert
  do Comment::job:Estimate_Ready ! allows comment style to be updated.
  do Prompt::job:Estimate_Ready

ValidateValue::job:Estimate_Ready  Routine
    If not (p_web.GSV('job:Estimate') <> 'YES' OR p_web.GSV('job:Estimate_Ready') <> 'YES' or (p_web.GSV('job:Estimate_Ready') = 'YES' and (p_web.GSV('job:Estimate_Accepted') = 'YES' or p_web.GSV('job:Estimate_Rejected') = 'YES')))
      if loc:invalid = '' then p_web.SetSessionValue('job:Estimate_Ready',job:Estimate_Ready).
    End

Value::job:Estimate_Ready  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('job:Estimate') <> 'YES' OR p_web.GSV('job:Estimate_Ready') <> 'YES' or (p_web.GSV('job:Estimate_Ready') = 'YES' and (p_web.GSV('job:Estimate_Accepted') = 'YES' or p_web.GSV('job:Estimate_Rejected') = 'YES')),'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobEstimate_' & p_web._nocolon('job:Estimate_Ready') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If loc:retrying
    job:Estimate_Ready = p_web.RestoreValue('job:Estimate_Ready')
    do ValidateValue::job:Estimate_Ready
    If job:Estimate_Ready:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('job:Estimate') <> 'YES' OR p_web.GSV('job:Estimate_Ready') <> 'YES' or (p_web.GSV('job:Estimate_Ready') = 'YES' and (p_web.GSV('job:Estimate_Accepted') = 'YES' or p_web.GSV('job:Estimate_Rejected') = 'YES')))
  ! --- CHECKBOX --- job:Estimate_Ready
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''job:Estimate_Ready'',''jobestimate_job:estimate_ready_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Estimate_Ready')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('job:Estimate_Ready') = 'YES'
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','job:Estimate_Ready',clip('YES'),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::job:Estimate_Ready  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if job:Estimate_Ready:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:EstimateReady'))
  loc:class = Choose(p_web.GSV('job:Estimate') <> 'YES' OR p_web.GSV('job:Estimate_Ready') <> 'YES' or (p_web.GSV('job:Estimate_Ready') = 'YES' and (p_web.GSV('job:Estimate_Accepted') = 'YES' or p_web.GSV('job:Estimate_Rejected') = 'YES')),'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('JobEstimate_' & p_web._nocolon('job:Estimate_Ready') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('job:Estimate') <> 'YES' OR p_web.GSV('job:Estimate_Ready') <> 'YES' or (p_web.GSV('job:Estimate_Ready') = 'YES' and (p_web.GSV('job:Estimate_Accepted') = 'YES' or p_web.GSV('job:Estimate_Rejected') = 'YES'))
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::job:Estimate_Accepted  Routine
  packet = clip(packet) & p_web.DivHeader('JobEstimate_' & p_web._nocolon('job:Estimate_Accepted') & '_prompt',Choose(p_web.GSV('job:Estimate') <> 'YES','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('job:Estimate') <> 'YES','',p_web.Translate('Estimate Accepted'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Estimate_Accepted  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Estimate_Accepted = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value','NO')
    end
    job:Estimate_Accepted = p_web.GetValue('Value')
  End
  do ValidateValue::job:Estimate_Accepted  ! copies value to session value if valid.
  if (p_web.GSV('job:Estimate_Accepted') = 'YES')
      if (p_web.GSV('job:Estimate_Ready') <> 'YES')
          p_web.SSV('Comment:EstimateAccepted','Estimate has not been marked as ready')
          p_web.SSV('job:Estimate_Accepted','NO')
      else ! if (p_web.GSV('job:Estimate_Ready') <> 'YES')
          p_web.SSV('Comment:EstimateAccepted','')
          p_web.SSV('Hide:EstimateAccepted',0)
          p_web.SSV('job:Estimate_Rejected','NO')
      end !if (p_web.GSV('job:Estimate_Ready') <> 'YES')
  else ! if (p_web.GSV('job:Estimate_Accepted') = 'YES')
      ! Check for char parts
      found# = 0
      Access:PARTS.Clearkey(par:part_Number_Key)
      par:ref_Number    = p_web.GSV('job:Ref_Number')
      set(par:part_Number_Key,par:part_Number_Key)
      loop
          if (Access:PARTS.Next())
              Break
          end ! if (Access:PARTS.Next())
          if (par:ref_Number    <> p_web.GSV('job:Ref_Number'))
              Break
          end ! if (par:ref_Number    <> p_web.GSV('job:Ref_Number'))
          found# = 1
          break
      end ! loop
  
      if (found# = 1)
          p_web.SSV('Comment:EstimateAccepted','Error! There are Chargeable Parts on this job.')
          p_web.SSV('job:Estimate_Accepted','YES')
      else ! if (found# = 1)
          p_web.SSV('Comment:EstimateAccepted','')
          p_web.SSV('Hide:EstimateAccepted',1)
      end ! if (found# = 1)
  
  end ! if (p_web.GSV('job:Estimate_Accepted') = 'YES')
  do Value::job:Estimate_Accepted
  do SendAlert
  do Comment::job:Estimate_Accepted ! allows comment style to be updated.
  do Prompt::locEstAccBy
  do Value::locEstAccBy  !1
  do Prompt::locEstAccCommunicationMethod
  do Value::locEstAccCommunicationMethod  !1
  do Value::textEstimateAccepted  !1
  do Comment::job:Estimate_Accepted
  do Value::job:Estimate_Rejected  !1
  do Comment::job:Estimate_Rejected
  do Prompt::job:Estimate_Ready
  do Value::job:Estimate_Ready  !1

ValidateValue::job:Estimate_Accepted  Routine
    If not (p_web.GSV('job:Estimate') <> 'YES')
      if loc:invalid = '' then p_web.SetSessionValue('job:Estimate_Accepted',job:Estimate_Accepted).
    End

Value::job:Estimate_Accepted  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('job:Estimate') <> 'YES','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobEstimate_' & p_web._nocolon('job:Estimate_Accepted') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If loc:retrying
    job:Estimate_Accepted = p_web.RestoreValue('job:Estimate_Accepted')
    do ValidateValue::job:Estimate_Accepted
    If job:Estimate_Accepted:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('job:Estimate') <> 'YES')
  ! --- CHECKBOX --- job:Estimate_Accepted
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''job:Estimate_Accepted'',''jobestimate_job:estimate_accepted_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Estimate_Accepted')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('job:Estimate_Accepted') = 'YES'
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','job:Estimate_Accepted',clip('YES'),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::job:Estimate_Accepted  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if job:Estimate_Accepted:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:EstimateAccepted'))
  loc:class = Choose(p_web.GSV('job:Estimate') <> 'YES','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('JobEstimate_' & p_web._nocolon('job:Estimate_Accepted') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('job:Estimate') <> 'YES'
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::job:Estimate_Rejected  Routine
  packet = clip(packet) & p_web.DivHeader('JobEstimate_' & p_web._nocolon('job:Estimate_Rejected') & '_prompt',Choose(p_web.GSV('job:Estimate') <> 'YES','nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('job:Estimate') <> 'YES','',p_web.Translate('Estimate Rejected'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::job:Estimate_Rejected  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    job:Estimate_Rejected = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value','NO')
    end
    job:Estimate_Rejected = p_web.GetValue('Value')
  End
  do ValidateValue::job:Estimate_Rejected  ! copies value to session value if valid.
  if (p_web.GSV('job:Estimate_Rejected') = 'YES')
      if (p_web.GSV('job:Estimate_Accepted') = 'YES')
          p_web.SSV('Comment:EstimateRejected','You must untick Estimate Accepted')
      else ! if (p_web.GSV('job:Estimate_Accepted') = 'YES')
          p_web.SSV('Hide:EstimateRejected',0)
          p_web.SSV('Comment:EstimateRejected','')
      end ! if (p_web.GSV('job:Estimate_Accepted') = 'YES')
  else ! if (p_web.GSV('job:Estimate_Rejected') = 'YES')
      p_web.SSV('Hide:EstimateRejected',1)
      p_web.SSV('Comment:EstimateRejected','')
  end ! if (p_web.GSV('job:Estimate_Rejected') = 'YES')
  do Value::job:Estimate_Rejected
  do SendAlert
  do Comment::job:Estimate_Rejected ! allows comment style to be updated.
  do Prompt::locEstRejBy
  do Value::locEstRejBy  !1
  do Prompt::locEstRejCommunicationMethod
  do Value::locEstRejCommunicationMethod  !1
  do Prompt::locEstRejReason
  do Value::locEstRejReason  !1
  do Comment::job:Estimate_Rejected

ValidateValue::job:Estimate_Rejected  Routine
    If not (p_web.GSV('job:Estimate') <> 'YES')
      if loc:invalid = '' then p_web.SetSessionValue('job:Estimate_Rejected',job:Estimate_Rejected).
    End

Value::job:Estimate_Rejected  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('job:Estimate') <> 'YES','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobEstimate_' & p_web._nocolon('job:Estimate_Rejected') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  If loc:retrying
    job:Estimate_Rejected = p_web.RestoreValue('job:Estimate_Rejected')
    do ValidateValue::job:Estimate_Rejected
    If job:Estimate_Rejected:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('job:Estimate') <> 'YES')
  ! --- CHECKBOX --- job:Estimate_Rejected
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''job:Estimate_Rejected'',''jobestimate_job:estimate_rejected_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Estimate_Rejected')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('job:Estimate_Rejected') = 'YES'
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','job:Estimate_Rejected',clip('YES'),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::job:Estimate_Rejected  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if job:Estimate_Rejected:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:EstimateRejected'))
  loc:class = Choose(p_web.GSV('job:Estimate') <> 'YES','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('JobEstimate_' & p_web._nocolon('job:Estimate_Rejected') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('job:Estimate') <> 'YES'
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::textEstimateAccepted  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::textEstimateAccepted  ! copies value to session value if valid.
  do Comment::textEstimateAccepted ! allows comment style to be updated.

ValidateValue::textEstimateAccepted  Routine
    If not (p_web.GSV('Hide:EstimateAccepted') = 1)
    End

Value::textEstimateAccepted  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:EstimateAccepted') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobEstimate_' & p_web._nocolon('textEstimateAccepted') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (p_web.GSV('Hide:EstimateAccepted') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="textEstimateAccepted" class="'&clip('green bold')&'"'&clip(loc:extra)&'>' & p_web.Translate('Estimate Accepted',) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::textEstimateAccepted  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if textEstimateAccepted:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:EstimateAccepted') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('JobEstimate_' & p_web._nocolon('textEstimateAccepted') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:EstimateAccepted') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locEstAccBy  Routine
  packet = clip(packet) & p_web.DivHeader('JobEstimate_' & p_web._nocolon('locEstAccBy') & '_prompt',Choose(p_web.GSV('Hide:EstimateAccepted') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:EstimateAccepted') = 1,'',p_web.Translate('Estimate Accepted By'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locEstAccBy  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locEstAccBy = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locEstAccBy = p_web.GetValue('Value')
  End
  do ValidateValue::locEstAccBy  ! copies value to session value if valid.
  do Value::locEstAccBy
  do SendAlert
  do Comment::locEstAccBy ! allows comment style to be updated.

ValidateValue::locEstAccBy  Routine
    If not (p_web.GSV('Hide:EstimateAccepted') = 1)
    locEstAccBy = Upper(locEstAccBy)
      if loc:invalid = '' then p_web.SetSessionValue('locEstAccBy',locEstAccBy).
    End

Value::locEstAccBy  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:EstimateAccepted') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobEstimate_' & p_web._nocolon('locEstAccBy') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    locEstAccBy = p_web.RestoreValue('locEstAccBy')
    do ValidateValue::locEstAccBy
    If locEstAccBy:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:EstimateAccepted') = 1)
  ! --- STRING --- locEstAccBy
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locEstAccBy'',''jobestimate_locestaccby_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locEstAccBy',p_web.GetSessionValueFormat('locEstAccBy'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locEstAccBy  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locEstAccBy:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:EstimateAccepted') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('JobEstimate_' & p_web._nocolon('locEstAccBy') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:EstimateAccepted') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locEstAccCommunicationMethod  Routine
  packet = clip(packet) & p_web.DivHeader('JobEstimate_' & p_web._nocolon('locEstAccCommunicationMethod') & '_prompt',Choose(p_web.GSV('Hide:EstimateAccepted') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:EstimateAccepted') = 1,'',p_web.Translate('Communication Method'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locEstAccCommunicationMethod  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locEstAccCommunicationMethod = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locEstAccCommunicationMethod = p_web.GetValue('Value')
  End
  do ValidateValue::locEstAccCommunicationMethod  ! copies value to session value if valid.
  do Value::locEstAccCommunicationMethod
  do SendAlert
  do Comment::locEstAccCommunicationMethod ! allows comment style to be updated.

ValidateValue::locEstAccCommunicationMethod  Routine
    If not (p_web.GSV('Hide:EstimateAccepted') = 1)
    locEstAccCommunicationMethod = Upper(locEstAccCommunicationMethod)
      if loc:invalid = '' then p_web.SetSessionValue('locEstAccCommunicationMethod',locEstAccCommunicationMethod).
    End

Value::locEstAccCommunicationMethod  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:EstimateAccepted') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobEstimate_' & p_web._nocolon('locEstAccCommunicationMethod') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    locEstAccCommunicationMethod = p_web.RestoreValue('locEstAccCommunicationMethod')
    do ValidateValue::locEstAccCommunicationMethod
    If locEstAccCommunicationMethod:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:EstimateAccepted') = 1)
  ! --- STRING --- locEstAccCommunicationMethod
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locEstAccCommunicationMethod'',''jobestimate_locestacccommunicationmethod_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locEstAccCommunicationMethod',p_web.GetSessionValueFormat('locEstAccCommunicationMethod'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locEstAccCommunicationMethod  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locEstAccCommunicationMethod:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:EstimateAccepted') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('JobEstimate_' & p_web._nocolon('locEstAccCommunicationMethod') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:EstimateAccepted') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::textEstimateRejected  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::textEstimateRejected  ! copies value to session value if valid.
  do Comment::textEstimateRejected ! allows comment style to be updated.

ValidateValue::textEstimateRejected  Routine
    If not (p_web.GSV('Hide:EstimateRejected') = 1)
    End

Value::textEstimateRejected  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:EstimateRejected') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobEstimate_' & p_web._nocolon('textEstimateRejected') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:extra = ''
  If Not (p_web.GSV('Hide:EstimateRejected') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="textEstimateRejected" class="'&clip('red bold')&'"'&clip(loc:extra)&'>' & p_web.Translate('Estimate Rejected',) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::textEstimateRejected  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if textEstimateRejected:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:EstimateRejected') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('JobEstimate_' & p_web._nocolon('textEstimateRejected') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:EstimateRejected') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locEstRejBy  Routine
  packet = clip(packet) & p_web.DivHeader('JobEstimate_' & p_web._nocolon('locEstRejBy') & '_prompt',Choose(p_web.GSV('Hide:EstimateRejected') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:EstimateRejected') = 1,'',p_web.Translate('Estimate Rejected By'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locEstRejBy  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locEstRejBy = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locEstRejBy = p_web.GetValue('Value')
  End
  do ValidateValue::locEstRejBy  ! copies value to session value if valid.
  do Value::locEstRejBy
  do SendAlert
  do Comment::locEstRejBy ! allows comment style to be updated.

ValidateValue::locEstRejBy  Routine
    If not (p_web.GSV('Hide:EstimateRejected') = 1)
    locEstRejBy = Upper(locEstRejBy)
      if loc:invalid = '' then p_web.SetSessionValue('locEstRejBy',locEstRejBy).
    End

Value::locEstRejBy  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:EstimateRejected') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobEstimate_' & p_web._nocolon('locEstRejBy') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    locEstRejBy = p_web.RestoreValue('locEstRejBy')
    do ValidateValue::locEstRejBy
    If locEstRejBy:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:EstimateRejected') = 1)
  ! --- STRING --- locEstRejBy
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locEstRejBy'',''jobestimate_locestrejby_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locEstRejBy',p_web.GetSessionValueFormat('locEstRejBy'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locEstRejBy  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locEstRejBy:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:EstimateRejected') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('JobEstimate_' & p_web._nocolon('locEstRejBy') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:EstimateRejected') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locEstRejCommunicationMethod  Routine
  packet = clip(packet) & p_web.DivHeader('JobEstimate_' & p_web._nocolon('locEstRejCommunicationMethod') & '_prompt',Choose(p_web.GSV('Hide:EstimateRejected') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:EstimateRejected') = 1,'',p_web.Translate('Communication Method'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locEstRejCommunicationMethod  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locEstRejCommunicationMethod = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locEstRejCommunicationMethod = p_web.GetValue('Value')
  End
  do ValidateValue::locEstRejCommunicationMethod  ! copies value to session value if valid.
  do Value::locEstRejCommunicationMethod
  do SendAlert
  do Comment::locEstRejCommunicationMethod ! allows comment style to be updated.

ValidateValue::locEstRejCommunicationMethod  Routine
    If not (p_web.GSV('Hide:EstimateRejected') = 1)
    locEstRejCommunicationMethod = Upper(locEstRejCommunicationMethod)
      if loc:invalid = '' then p_web.SetSessionValue('locEstRejCommunicationMethod',locEstRejCommunicationMethod).
    End

Value::locEstRejCommunicationMethod  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:EstimateRejected') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobEstimate_' & p_web._nocolon('locEstRejCommunicationMethod') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  If loc:retrying
    locEstRejCommunicationMethod = p_web.RestoreValue('locEstRejCommunicationMethod')
    do ValidateValue::locEstRejCommunicationMethod
    If locEstRejCommunicationMethod:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:EstimateRejected') = 1)
  ! --- STRING --- locEstRejCommunicationMethod
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locEstRejCommunicationMethod'',''jobestimate_locestrejcommunicationmethod_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locEstRejCommunicationMethod',p_web.GetSessionValueFormat('locEstRejCommunicationMethod'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locEstRejCommunicationMethod  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locEstRejCommunicationMethod:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
    loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:EstimateRejected') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('JobEstimate_' & p_web._nocolon('locEstRejCommunicationMethod') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:EstimateRejected') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Prompt::locEstRejReason  Routine
  packet = clip(packet) & p_web.DivHeader('JobEstimate_' & p_web._nocolon('locEstRejReason') & '_prompt',Choose(p_web.GSV('Hide:EstimateRejected') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Hide:EstimateRejected') = 1,'',p_web.Translate('Rejection Reason'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locEstRejReason  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locEstRejReason = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locEstRejReason = p_web.GetValue('Value')
  End
  do ValidateValue::locEstRejReason  ! copies value to session value if valid.
  do Value::locEstRejReason
  do SendAlert
  do Comment::locEstRejReason ! allows comment style to be updated.

ValidateValue::locEstRejReason  Routine
    If not (p_web.GSV('Hide:EstimateRejected') = 1)
      if loc:invalid = '' then p_web.SetSessionValue('locEstRejReason',locEstRejReason).
    End

Value::locEstRejReason  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(p_web.GSV('Hide:EstimateRejected') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobEstimate_' & p_web._nocolon('locEstRejReason') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,p_web.site.style.formselect,'FormEntry',)
  If loc:retrying
    locEstRejReason = p_web.RestoreValue('locEstRejReason')
    do ValidateValue::locEstRejReason
    If locEstRejReason:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (p_web.GSV('Hide:EstimateRejected') = 1)
  ! --- DROPLIST ---
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locEstRejReason'',''jobestimate_locestrejreason_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('locEstRejReason',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
  loc:fieldclass = ''
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('locEstRejReason') = 0
    p_web.SetSessionValue('locEstRejReason','')
  end
    packet = clip(packet) & p_web.CreateOption('-------------------------------------','',choose('' = p_web.getsessionvalue('locEstRejReason')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(AUDSTATS)
  bind(aus:Record)
  p_web._OpenFile(JOBS)
  bind(job:Record)
  p_web._OpenFile(ESREJRES)
  bind(esr:Record)
  p_web._OpenFile(PARTS)
  bind(par:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(locEstRejReason_OptionView)
  locEstRejReason_OptionView{prop:order} = p_web.CleanFilter(locEstRejReason_OptionView,'UPPER(esr:RejectionReason)')
  Set(locEstRejReason_OptionView)
  Loop
    Next(locEstRejReason_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('locEstRejReason') = 0
      p_web.SetSessionValue('locEstRejReason',esr:RejectionReason)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,esr:RejectionReason,choose(esr:RejectionReason = p_web.getsessionvalue('locEstRejReason')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(locEstRejReason_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(AUDSTATS)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(ESREJRES)
  p_Web._CloseFile(PARTS)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web.DivFooter()
Comment::locEstRejReason  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locEstRejReason:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('Hide:EstimateRejected') = 1,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('JobEstimate_' & p_web._nocolon('locEstRejReason') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('Hide:EstimateRejected') = 1
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('JobEstimate_nexttab_' & 0)
    job:Estimate = p_web.GSV('job:Estimate')
    do ValidateValue::job:Estimate
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Estimate
      !do SendAlert
      do Comment::job:Estimate ! allows comment style to be updated.
      !exit
    End
    job:Estimate_If_Over = p_web.GSV('job:Estimate_If_Over')
    do ValidateValue::job:Estimate_If_Over
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Estimate_If_Over
      !do SendAlert
      do Comment::job:Estimate_If_Over ! allows comment style to be updated.
      !exit
    End
    job:Estimate_Ready = p_web.GSV('job:Estimate_Ready')
    do ValidateValue::job:Estimate_Ready
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Estimate_Ready
      !do SendAlert
      do Comment::job:Estimate_Ready ! allows comment style to be updated.
      !exit
    End
    job:Estimate_Accepted = p_web.GSV('job:Estimate_Accepted')
    do ValidateValue::job:Estimate_Accepted
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Estimate_Accepted
      !do SendAlert
      do Comment::job:Estimate_Accepted ! allows comment style to be updated.
      !exit
    End
    job:Estimate_Rejected = p_web.GSV('job:Estimate_Rejected')
    do ValidateValue::job:Estimate_Rejected
    If loc:Invalid
      loc:retrying = 1
      do Value::job:Estimate_Rejected
      !do SendAlert
      do Comment::job:Estimate_Rejected ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('JobEstimate_nexttab_' & 1)
    locEstAccBy = p_web.GSV('locEstAccBy')
    do ValidateValue::locEstAccBy
    If loc:Invalid
      loc:retrying = 1
      do Value::locEstAccBy
      !do SendAlert
      do Comment::locEstAccBy ! allows comment style to be updated.
      !exit
    End
    locEstAccCommunicationMethod = p_web.GSV('locEstAccCommunicationMethod')
    do ValidateValue::locEstAccCommunicationMethod
    If loc:Invalid
      loc:retrying = 1
      do Value::locEstAccCommunicationMethod
      !do SendAlert
      do Comment::locEstAccCommunicationMethod ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('JobEstimate_nexttab_' & 2)
    locEstRejBy = p_web.GSV('locEstRejBy')
    do ValidateValue::locEstRejBy
    If loc:Invalid
      loc:retrying = 1
      do Value::locEstRejBy
      !do SendAlert
      do Comment::locEstRejBy ! allows comment style to be updated.
      !exit
    End
    locEstRejCommunicationMethod = p_web.GSV('locEstRejCommunicationMethod')
    do ValidateValue::locEstRejCommunicationMethod
    If loc:Invalid
      loc:retrying = 1
      do Value::locEstRejCommunicationMethod
      !do SendAlert
      do Comment::locEstRejCommunicationMethod ! allows comment style to be updated.
      !exit
    End
    locEstRejReason = p_web.GSV('locEstRejReason')
    do ValidateValue::locEstRejReason
    If loc:Invalid
      loc:retrying = 1
      do Value::locEstRejReason
      !do SendAlert
      do Comment::locEstRejReason ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_JobEstimate_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('JobEstimate_tab_' & 0)
    do GenerateTab0
  of lower('JobEstimate_job:Estimate_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Estimate
      of event:timer
        do Value::job:Estimate
        do Comment::job:Estimate
      else
        do Value::job:Estimate
      end
  of lower('JobEstimate_job:Estimate_If_Over_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Estimate_If_Over
      of event:timer
        do Value::job:Estimate_If_Over
        do Comment::job:Estimate_If_Over
      else
        do Value::job:Estimate_If_Over
      end
  of lower('JobEstimate_job:Estimate_Ready_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Estimate_Ready
      of event:timer
        do Value::job:Estimate_Ready
        do Comment::job:Estimate_Ready
      else
        do Value::job:Estimate_Ready
      end
  of lower('JobEstimate_job:Estimate_Accepted_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Estimate_Accepted
      of event:timer
        do Value::job:Estimate_Accepted
        do Comment::job:Estimate_Accepted
      else
        do Value::job:Estimate_Accepted
      end
  of lower('JobEstimate_job:Estimate_Rejected_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Estimate_Rejected
      of event:timer
        do Value::job:Estimate_Rejected
        do Comment::job:Estimate_Rejected
      else
        do Value::job:Estimate_Rejected
      end
  of lower('JobEstimate_tab_' & 1)
    do GenerateTab1
  of lower('JobEstimate_locEstAccBy_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locEstAccBy
      of event:timer
        do Value::locEstAccBy
        do Comment::locEstAccBy
      else
        do Value::locEstAccBy
      end
  of lower('JobEstimate_locEstAccCommunicationMethod_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locEstAccCommunicationMethod
      of event:timer
        do Value::locEstAccCommunicationMethod
        do Comment::locEstAccCommunicationMethod
      else
        do Value::locEstAccCommunicationMethod
      end
  of lower('JobEstimate_tab_' & 2)
    do GenerateTab2
  of lower('JobEstimate_locEstRejBy_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locEstRejBy
      of event:timer
        do Value::locEstRejBy
        do Comment::locEstRejBy
      else
        do Value::locEstRejBy
      end
  of lower('JobEstimate_locEstRejCommunicationMethod_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locEstRejCommunicationMethod
      of event:timer
        do Value::locEstRejCommunicationMethod
        do Comment::locEstRejCommunicationMethod
      else
        do Value::locEstRejCommunicationMethod
      end
  of lower('JobEstimate_locEstRejReason_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locEstRejReason
      of event:timer
        do Value::locEstRejReason
        do Comment::locEstRejReason
      else
        do Value::locEstRejReason
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('JobEstimate_form:ready_',1)

  p_web.SetSessionValue('JobEstimate_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_JobEstimate',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('JobEstimate_form:ready_',1)
  p_web.SetSessionValue('JobEstimate_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_JobEstimate',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('JobEstimate_form:ready_',1)
  p_web.SetSessionValue('JobEstimate_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('JobEstimate:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('JobEstimate_form:ready_',1)
  p_web.SetSessionValue('JobEstimate_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('JobEstimate:Primed',0)
  p_web.setsessionvalue('showtab_JobEstimate',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
      If not (1=0)
          If p_web.IfExistsValue('job:Estimate') = 0
            p_web.SetValue('job:Estimate','NO')
            job:Estimate = 'NO'
          Else
            job:Estimate = p_web.GetValue('job:Estimate')
          End
      End
      If not (p_web.GSV('job:Estimate') <> 'YES')
          If p_web.IfExistsValue('job:Estimate_If_Over')
            job:Estimate_If_Over = p_web.GetValue('job:Estimate_If_Over')
          End
      End
      If not (p_web.GSV('job:Estimate') <> 'YES' OR p_web.GSV('job:Estimate_Ready') <> 'YES' or (p_web.GSV('job:Estimate_Ready') = 'YES' and (p_web.GSV('job:Estimate_Accepted') = 'YES' or p_web.GSV('job:Estimate_Rejected') = 'YES')))
          If p_web.IfExistsValue('job:Estimate_Ready') = 0
            p_web.SetValue('job:Estimate_Ready','NO')
            job:Estimate_Ready = 'NO'
          Else
            job:Estimate_Ready = p_web.GetValue('job:Estimate_Ready')
          End
      End
      If not (p_web.GSV('job:Estimate') <> 'YES')
          If p_web.IfExistsValue('job:Estimate_Accepted') = 0
            p_web.SetValue('job:Estimate_Accepted','NO')
            job:Estimate_Accepted = 'NO'
          Else
            job:Estimate_Accepted = p_web.GetValue('job:Estimate_Accepted')
          End
      End
      If not (p_web.GSV('job:Estimate') <> 'YES')
          If p_web.IfExistsValue('job:Estimate_Rejected') = 0
            p_web.SetValue('job:Estimate_Rejected','NO')
            job:Estimate_Rejected = 'NO'
          Else
            job:Estimate_Rejected = p_web.GetValue('job:Estimate_Rejected')
          End
      End
      If not (p_web.GSV('Hide:EstimateAccepted') = 1)
          If p_web.IfExistsValue('locEstAccBy')
            locEstAccBy = p_web.GetValue('locEstAccBy')
          End
      End
      If not (p_web.GSV('Hide:EstimateAccepted') = 1)
          If p_web.IfExistsValue('locEstAccCommunicationMethod')
            locEstAccCommunicationMethod = p_web.GetValue('locEstAccCommunicationMethod')
          End
      End
      If not (p_web.GSV('Hide:EstimateRejected') = 1)
          If p_web.IfExistsValue('locEstRejBy')
            locEstRejBy = p_web.GetValue('locEstRejBy')
          End
      End
      If not (p_web.GSV('Hide:EstimateRejected') = 1)
          If p_web.IfExistsValue('locEstRejCommunicationMethod')
            locEstRejCommunicationMethod = p_web.GetValue('locEstRejCommunicationMethod')
          End
      End
      If not (p_web.GSV('Hide:EstimateRejected') = 1)
          If p_web.IfExistsValue('locEstRejReason')
            locEstRejReason = p_web.GetValue('locEstRejReason')
          End
      End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('JobEstimate_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('JobEstimate_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
    do ValidateValue::job:Estimate
    If loc:Invalid then exit.
    do ValidateValue::job:Estimate_If_Over
    If loc:Invalid then exit.
    do ValidateValue::job:Estimate_Ready
    If loc:Invalid then exit.
    do ValidateValue::job:Estimate_Accepted
    If loc:Invalid then exit.
    do ValidateValue::job:Estimate_Rejected
    If loc:Invalid then exit.
  ! tab = 2
    loc:InvalidTab += 1
    do ValidateValue::textEstimateAccepted
    If loc:Invalid then exit.
    do ValidateValue::locEstAccBy
    If loc:Invalid then exit.
    do ValidateValue::locEstAccCommunicationMethod
    If loc:Invalid then exit.
  ! tab = 3
    loc:InvalidTab += 1
    do ValidateValue::textEstimateRejected
    If loc:Invalid then exit.
    do ValidateValue::locEstRejBy
    If loc:Invalid then exit.
    do ValidateValue::locEstRejCommunicationMethod
    If loc:Invalid then exit.
    do ValidateValue::locEstRejReason
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
      if (p_web.GSV('job:estimate_rejected') = 'YES')
          if (p_web.GSV('Hide:EstimateRejected') = 0)
  
              getStatus(540,0,'JOB',p_web)
  
              p_web.SSV('AddToAudit:Type','JOB')
              p_web.SSV('AddToAudit:Action','ESTIMATE REJECTED FROM: ' & p_web.GSV('locEstRejBy'))
              p_web.SSV('AddToAudit:Notes','COMMUNICATION METHOD: ' & p_web.GSV('locEstRejCommunicationMethod') & |
                           '<13,10>REASON: ' & p_web.GSV('locEstRejReason'))
              addToAudit(p_web)
          end ! if (p_web.GSV('Hide:EstimateRejected') = 0)
      end ! if (p_web.GSV('job:estimate_rejected') = 'YES')
  
      if (p_web.GSV('job:estimate_Accepted') = 'YES')
          if (p_web.GSV('Hide:EstimateAccepted') = 0)
  
              getStatus(535,0,'JOB',p_web)
  
              p_web.SSV('AddToAudit:Type','JOB')
              p_web.SSV('AddToAudit:Action','ESTIMATE ACCEPTED FROM: ' & p_web.GSV('locEstAccBy'))
              p_web.SSV('AddToAudit:Notes','COMMUNICATION METHOD: ' & p_web.GSV('locEstAccCommunicationMethod'))
              addToAudit(p_web)
  
              p_web.SSV('job:Courier_Cost',p_web.GSV('job:Courier_Cost_Estimate'))
  
              p_web.SSV('job:Labour_Cost',p_web.GSV('job:Labour_Cost_Estimate'))
              p_web.SSV('job:Parts_Cost',p_web.GSV('job:Parts_Cost_Estimate'))
              p_web.SSV('job:Ignore_Chargeable_Charges',p_web.GSV('job:Ignore_Estimate_Charges'))
              p_web.SSV('jobe:RRCCLabourCost',p_web.GSV('jobe:RRCELabourCost'))
              p_web.SSV('jobe:RRCCPartsCost',p_web.GSV('jobe:RRCEPartsCost'))
              p_web.SSV('jobe:IgnoreRRCChaCosts',p_web.GSV('jobe:IgnoreRRCEstCosts'))
              convertEstimateParts(p_web)
          end ! if (p_web.GSV('Hide:EstimateAccepted') = 0)
      end ! if (p_web.GSV('job:estimate_Accepted') = 'YES')
  
  
  
  
  
  
  
  ! Delete Variables
      p_web.deleteSessionValue('locEstAccBy')
      p_web.deleteSessionValue('locEstAccCommunicationMethod')
      p_web.deleteSessionValue('locEstRejBy')
      p_web.deleteSessionValue('locEstRejCommunicationMethod')
      p_web.deleteSessionValue('locEstRejReason')
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('JobEstimate:Primed',0)
  p_web.StoreValue('job:Estimate')
  p_web.StoreValue('job:Estimate_If_Over')
  p_web.StoreValue('job:Estimate_Ready')
  p_web.StoreValue('job:Estimate_Accepted')
  p_web.StoreValue('job:Estimate_Rejected')
  p_web.StoreValue('')
  p_web.StoreValue('locEstAccBy')
  p_web.StoreValue('locEstAccCommunicationMethod')
  p_web.StoreValue('')
  p_web.StoreValue('locEstRejBy')
  p_web.StoreValue('locEstRejCommunicationMethod')
  p_web.StoreValue('locEstRejReason')

FormAccessoryNumbers PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
FilesOpened     Long
JOBACCNO::State  USHORT
joa:AccessoryNumber:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormAccessoryNumbers')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'FormAccessoryNumbers_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('FormAccessoryNumbers','')
    p_web.DivHeader('FormAccessoryNumbers',p_web.combine(p_web.site.style.formdiv,'fdiv'))
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('FormAccessoryNumbers') = 0
        p_web.AddPreCall('FormAccessoryNumbers')
        p_web.DivHeader('popup_FormAccessoryNumbers','nt-hidden')
        p_web.DivHeader('FormAccessoryNumbers',p_web.combine(p_web.site.style.formdiv,'fdiv'))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_FormAccessoryNumbers_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_FormAccessoryNumbers_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormAccessoryNumbers',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:StagePost
    do RestoreMem
    do PostInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_FormAccessoryNumbers',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormAccessoryNumbers',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:StagePost
    do RestoreMem
    do PostCopy
  of Net:CopyRecord + NET:WEB:Populate
    If p_web.IfExistsValue('joa:RecordNumber') = 0 then p_web.SetValue('joa:RecordNumber',p_web.GSV('joa:RecordNumber')).
    do PreCopy
    p_web.setsessionvalue('showtab_FormAccessoryNumbers',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormAccessoryNumbers',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormAccessoryNumbers',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do PostInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do PostUpdate
    End
  of Net:ChangeRecord + NET:WEB:Populate
    If p_web.IfExistsValue('joa:RecordNumber') = 0 then p_web.SetValue('joa:RecordNumber',p_web.GSV('joa:RecordNumber')).
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_FormAccessoryNumbers',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormAccessoryNumbers',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:DeleteRecord + NET:WEB:StagePost
    do RestoreMem
    do PostDelete
  of Net:ViewRecord + NET:WEB:Populate
    If p_web.IfExistsValue('joa:RecordNumber') = 0 then p_web.SetValue('joa:RecordNumber',p_web.GSV('joa:RecordNumber')).
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_FormAccessoryNumbers',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormAccessoryNumbers',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormAccessoryNumbers',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormAccessoryNumbers',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('FormAccessoryNumbers')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(JOBACCNO)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(JOBACCNO)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FormAccessoryNumbers_form:inited_',1)
  p_web.formsettings.file = 'JOBACCNO'
  p_web.formsettings.key = 'joa:RecordNumberKey'
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = 'JOBACCNO'
    p_web.formsettings.key = 'joa:RecordNumberKey'
      clear(p_web.formsettings.FieldName)
    p_web.formsettings.recordid[1] = joa:RecordNumber
    p_web.formsettings.FieldName[1] = 'joa:RecordNumber'
    do SetAction
    if p_web.GetSessionValue('FormAccessoryNumbers:Primed') = 1
      p_web.formsettings.action = Net:ChangeRecord
    Else
      p_web.formsettings.action = Loc:Act
    End
    p_web.formsettings.OriginalAction = Loc:Act
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'FormAccessoryNumbers'
    end
    p_web.formsettings.proc = 'FormAccessoryNumbers'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine
  IF p_web.GetSessionValue('FormAccessoryNumbers:Primed') = 1
    p_web._deleteFile(JOBACCNO)
    p_web.SetSessionValue('FormAccessoryNumbers:Primed',0)
  End

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine
  p_web.SetValue('UpdateFile','JOBACCNO')
  p_web.SetValue('UpdateKey','joa:RecordNumberKey')
  If p_web.IfExistsValue('joa:AccessoryNumber')
    p_web.SetPicture('joa:AccessoryNumber','@s30')
  End
  p_web.SetSessionPicture('joa:AccessoryNumber','@s30')

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=File

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('FormAccessoryNumbers_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferFormAccessoryNumbers')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormAccessoryNumbers_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormAccessoryNumbers_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormAccessoryNumbers_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction

GenerateForm   Routine
  do LoadRelatedRecords
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Accessory Numbers') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Accessory Numbers',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_FormAccessoryNumbers',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_FormAccessoryNumbers0_div')&'">'&p_web.Translate('General')&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="FormAccessoryNumbers_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.cancel();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CancelButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="FormAccessoryNumbers_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'FormAccessoryNumbers_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="FormAccessoryNumbers_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'FormAccessoryNumbers_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.joa:AccessoryNumber')
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_FormAccessoryNumbers')>0,p_web.GSV('showtab_FormAccessoryNumbers'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_FormAccessoryNumbers'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_FormAccessoryNumbers') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_FormAccessoryNumbers'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_FormAccessoryNumbers')>0,p_web.GSV('showtab_FormAccessoryNumbers'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_FormAccessoryNumbers') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('General') & ''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_FormAccessoryNumbers_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_FormAccessoryNumbers')>0,p_web.GSV('showtab_FormAccessoryNumbers'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"FormAccessoryNumbers",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_FormAccessoryNumbers')>0,p_web.GSV('showtab_FormAccessoryNumbers'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_FormAccessoryNumbers_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('FormAccessoryNumbers') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('FormAccessoryNumbers')
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('General')&'</a></h3>' & CRLF & p_web.DivHeader('tab_FormAccessoryNumbers0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_FormAccessoryNumbers0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_FormAccessoryNumbers0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_FormAccessoryNumbers0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'General')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_FormAccessoryNumbers0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('General')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_FormAccessoryNumbers0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('General')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_FormAccessoryNumbers0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::joa:AccessoryNumber
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::joa:AccessoryNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::joa:AccessoryNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Prompt::joa:AccessoryNumber  Routine
  packet = clip(packet) & p_web.DivHeader('FormAccessoryNumbers_' & p_web._nocolon('joa:AccessoryNumber') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Accessory Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::joa:AccessoryNumber  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    joa:AccessoryNumber = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s30
    joa:AccessoryNumber = p_web.Dformat(p_web.GetValue('Value'),'@s30')
  End
  do ValidateValue::joa:AccessoryNumber  ! copies value to session value if valid.
  do Value::joa:AccessoryNumber
  do SendAlert
  do Comment::joa:AccessoryNumber ! allows comment style to be updated.

ValidateValue::joa:AccessoryNumber  Routine
    If not (1=0)
  If joa:AccessoryNumber = ''
    loc:Invalid = 'joa:AccessoryNumber'
    joa:AccessoryNumber:IsInvalid = true
    loc:alert = p_web.translate('Accessory Number') & ' ' & p_web.site.RequiredText
  End
    joa:AccessoryNumber = Upper(joa:AccessoryNumber)
      if loc:invalid = '' then p_web.SetSessionValue('joa:AccessoryNumber',joa:AccessoryNumber).
    End

Value::joa:AccessoryNumber  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('FormAccessoryNumbers_' & p_web._nocolon('joa:AccessoryNumber') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,'FormEntry',)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,'formreadonly')
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryRequired,'formrqd')
  If loc:retrying
    joa:AccessoryNumber = p_web.RestoreValue('joa:AccessoryNumber')
    do ValidateValue::joa:AccessoryNumber
    If joa:AccessoryNumber:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,'formerror').
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- joa:AccessoryNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''joa:AccessoryNumber'',''formaccessorynumbers_joa:accessorynumber_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','joa:AccessoryNumber',p_web.GetSessionValueFormat('joa:AccessoryNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'Accessory Number',,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::joa:AccessoryNumber  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if joa:AccessoryNumber:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web._jsok(p_web.site.RequiredText)
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('FormAccessoryNumbers_' & p_web._nocolon('joa:AccessoryNumber') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('FormAccessoryNumbers_nexttab_' & 0)
    joa:AccessoryNumber = p_web.GSV('joa:AccessoryNumber')
    do ValidateValue::joa:AccessoryNumber
    If loc:Invalid
      loc:retrying = 1
      do Value::joa:AccessoryNumber
      !do SendAlert
      do Comment::joa:AccessoryNumber ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_FormAccessoryNumbers_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('FormAccessoryNumbers_tab_' & 0)
    do GenerateTab0
  of lower('FormAccessoryNumbers_joa:AccessoryNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::joa:AccessoryNumber
      of event:timer
        do Value::joa:AccessoryNumber
        do Comment::joa:AccessoryNumber
      else
        do Value::joa:AccessoryNumber
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('FormAccessoryNumbers_form:ready_',1)

  p_web.SetSessionValue('FormAccessoryNumbers_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_FormAccessoryNumbers',0)
  joa:RefNumber = p_web.GSV('job:Ref_Number')
  p_web.SetSessionValue('joa:RefNumber',joa:RefNumber)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('FormAccessoryNumbers_form:ready_',1)
  p_web.SetSessionValue('FormAccessoryNumbers_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormAccessoryNumbers',0)
  p_web._PreCopyRecord(JOBACCNO,joa:RecordNumberKey)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('FormAccessoryNumbers_form:ready_',1)
  p_web.SetSessionValue('FormAccessoryNumbers_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('FormAccessoryNumbers:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('FormAccessoryNumbers_form:ready_',1)
  p_web.SetSessionValue('FormAccessoryNumbers_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('FormAccessoryNumbers:Primed',0)
  p_web.setsessionvalue('showtab_FormAccessoryNumbers',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
      If not (1=0)
          If p_web.IfExistsValue('joa:AccessoryNumber')
            joa:AccessoryNumber = p_web.GetValue('joa:AccessoryNumber')
          End
      End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormAccessoryNumbers_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormAccessoryNumbers_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
    do ValidateValue::joa:AccessoryNumber
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostInsert      Routine
PostCopy        Routine
  p_web.SetSessionValue('FormAccessoryNumbers:Primed',0)
PostUpdate      Routine
  p_web.SetSessionValue('FormAccessoryNumbers:Primed',0)


PostDelete      Routine
BrowseAccessoryNumber PROCEDURE  (NetWebServerWorker p_web)
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256),dim(Net:MaxKeyFields)
idx           String(Net:HashSize)
sub           Long
            End
joa:AccessoryNumber:IsInvalid  Long
Delete:IsInvalid  Long
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:DefaultSelection    String(Net:HashSize)
loc:ActualSelection     String(Net:HashSize)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
!loc:pagename            String(256)
loc:NavButtonPosition    Long
loc:UpdateButtonPosition Long
loc:SelectionMethod      Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorCase         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
loc:lookupdone          Long
loc:FormPopup           Long
ThisView            View(JOBACCNO)
                      Project(joa:RecordNumber)
                      Project(joa:AccessoryNumber)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
Loc:popup             Long
loc:poppedup          Long,static,thread
FilesOpened     Long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  GlobalErrors.SetProcedureName('BrowseAccessoryNumber')
  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseAccessoryNumber:NoForm')
      loc:NoForm = p_web.GetValue('BrowseAccessoryNumber:NoForm')
      loc:FormName = p_web.GetValue('BrowseAccessoryNumber:FormName')
    else
      loc:FormName = 'BrowseAccessoryNumber_frm'
    End
    p_web.SSV('BrowseAccessoryNumber:NoForm',loc:NoForm)
    p_web.SSV('BrowseAccessoryNumber:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseAccessoryNumber:NoForm')
    loc:FormName = p_web.GSV('BrowseAccessoryNumber:FormName')
  end
  case p_web.site.formpopups
  of -1 ; loc:FormPopup = Net:Page
  of 0 ; loc:FormPopup = Net:Popup
  of 1 ; loc:FormPopup = Net:Popup
  End
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseAccessoryNumber') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseAccessoryNumber')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if 1 = 2
  elsif p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  elsif p_web.IfExistsValue('_EIPRow_')
    do CallRow
  elsif p_web.GetValue('_CallPopups') > 0
    do CallPopups
  elsif p_web.IfExistsValue('_GetSecwinSettings')
      p_web.SetValue('Secwin_' & 'BrowseAccessoryNumber' & 'AccessGroupsArray','1') 
      p_web.SetValue('Secwin_AccessWindowName','BrowseAccessoryNumber')
      
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallPopups  Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
loc:CallPopups   Long
loc:name         String(255)
  code
  if loc:poppedup = 0
    loc:CallPopups = p_web.GetValue('_CallPopups')
    if loc:CallPopups = 1 !and p_web.GetPreCall('BrowseAccessoryNumber') = 0! for = 1 include popup divs and scripts for this browse
      !p_web.AddPreCall('BrowseAccessoryNumber')
      p_web.DivHeader('popup_BrowseAccessoryNumber','nt-hidden')
      p_web.DivHeader('BrowseAccessoryNumber',p_web.combine(p_web.site.style.browsediv,))
      p_web.DivFooter()
      p_web.DivFooter()
      loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(400)&', modal: true, position: [''center'',15]'
      packet =  p_web.jQuery('#' & lower('popup_BrowseAccessoryNumber_div'),'dialog',loc:options) & |
      p_web.jQuery('#' & lower('popup_BrowseAccessoryNumber_div'),'removeClass','''nt-hidden''')
    End
    ! for > 0 add in form, EIP-lookups and browse object
    do Popups
    loc:popup = 1
    do ClosingScripts
    do SendPacket
    loc:poppedup = 1
  end

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web.DivHeader('BrowseAccessoryNumber',p_web.combine(p_web.site.style.browsediv,))
  if loc:ParentSilent = 0
    do GenerateBrowse
    if p_web.RequestAjax = 1
      p_web.Script('BrowseProducts.pcv();')
    end
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web.DivFooter()
  do Children
  do Popups
  if loc:poppedup = 0
    do ClosingScripts
  end
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(JOBACCNO,joa:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'JOA:ACCESSORYNUMBER') then p_web.SetValue('BrowseAccessoryNumber_sort','1')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseAccessoryNumber:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseAccessoryNumber:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseAccessoryNumber:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseAccessoryNumber:LookupField')
    loc:selecting = 0
  End

Popups Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  If loc:popup = 0
    If p_web.GetPreCall('FormAccessoryNumbers') = 0 then FormAccessoryNumbers(p_web,Net:Web:Popup).
  End
SetFormAction  Routine
  loc:formaction = 'FormAccessoryNumbers'
  loc:formactiontarget = '_self'

GotFocusBack   Routine

GenerateBrowse Routine
  data
loc:viewoptions  Long
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  ! Set general Browse options
  loc:NavButtonPosition   = Net:Below
  loc:UpdateButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:FileLoad
  loc:FillBack         = 0
  loc:Sorting          = Net:ServerSort
  if p_web.GetValue('_popup_') = 1
    loc:popup = 1
  end
  ! Set Locator Options
  loc:LocatorPosition  = Net:Below
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  p_web.ClearBrowse('BrowseAccessoryNumber')
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 10
  End
  loc:ActualSelection = ''
  !loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseAccessoryNumber_sort',net:DontEvaluate)
  p_web.SetSessionValue('BrowseAccessoryNumber_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(joa:AccessoryNumber)','-UPPER(joa:AccessoryNumber)')
    Loc:LocateField = 'joa:AccessoryNumber'
    Loc:LocatorCase = 0
  of 2
    Loc:LocateField = ''
    Loc:LocatorCase = 0
  end
  if loc:vorder = ''
    loc:vorder = '+joa:RecordNumber'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('joa:AccessoryNumber')
    loc:SortHeader = p_web.Translate('Accessory Number')
    p_web.SetSessionValue('BrowseAccessoryNumber_LocatorPic','@s30')
  End
  If loc:selecting = 1
    p_web.GetSettings(p_web.GetSessionValue('Push1'))
    loc:selectaction = p_web.FormSettings.ParentPage !p_web.GetSessionValue('BrowseAccessoryNumber:LookupFrom')
  End!Else
  do SendPacket
  do SetFormAction
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseAccessoryNumber:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseAccessoryNumber:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseAccessoryNumber:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="JOBACCNO"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="joa:RecordNumberKey"></input><13,10>'
  end
  case p_web.GetValue('refresh') ; of 'next' orof 'previous' orof 'first' orof 'last'
    p_web.Script('try{{BrowseAccessoryNumber.restoreFocus();} catch(err){{};')
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseAccessoryNumber',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      case loc:locatortype
      of Net:Position
      orof Net:Date
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
      of Net:Contains
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
      of Net:Begins
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
      End
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
      Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
      Case Loc:LocatorType
      of Net:Date
        packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator2BrowseAccessoryNumber','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('BrowseAccessoryNumber_LocatorPic'),,,'onchange="BrowseAccessoryNumber.locate(''Locator2BrowseAccessoryNumber'',this.value);"',,0,,,) & '</div></td>'
      Else
        packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator2BrowseAccessoryNumber',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="BrowseAccessoryNumber.locate(''Locator2BrowseAccessoryNumber'',this.value);" ',,,,,,) & '</td>'
      End
      If loc:LocatorSearchButton or loc:LocatorClearButton
        packet = clip(packet) & '<td><div id="BrowseAccessoryNumber_locate_a" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
        If loc:LocatorSearchButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
        End
        If loc:LocatorClearButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseAccessoryNumber.cl(''BrowseAccessoryNumber'');')
        End
        packet = clip(packet) & '</div><13,10></td><13,10>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
      If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'BrowseAccessoryNumber_locate_a','buttonset',loc:options)
      End
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & p_web.DivHeader('BrowseAccessoryNumber_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'','sortable')&'" id="BrowseAccessoryNumber_tbl">'
  Else
    packet = clip(packet) & p_web.DivHeader('BrowseAccessoryNumber_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'')&'" id="BrowseAccessoryNumber_tbl">'
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'1','BrowseAccessoryNumber',p_web.Translate('Accessory Number'),'Click here to sort by Accessory Number',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
  If (p_web.GSV('Job:ViewOnly') <> 1) AND  true ! [A]
      If loc:Selecting = 0
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'2','BrowseAccessoryNumber',p_web.Translate(),,,,,,,0,loc:Sorting,'Button')
          do AddPacket
          loc:columns += 1
      End ! Selecting
  End ! Field condition [A]
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,10,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  If Instring('joa:recordnumber',lower(loc:vorder),1,1) = 0 !and JOBACCNO{prop:SQLDriver} = 1
    loc:vorder = Choose(loc:vorder='','joa:RecordNumber',clip(loc:vorder) & ',' & 'joa:RecordNumber')
  End
  Loc:Selected = Choose(p_web.IfExistsValue('joa:RecordNumber'),p_web.GetValue('joa:RecordNumber'),p_web.GetSessionValue('joa:RecordNumber'))
  ThisView{prop:order} = p_web.CleanFilter(ThisView,clip(loc:vorder))
      loc:FilterWas = 'joa:RefNumber = ' & p_web.GSV('job:Ref_Number')
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseAccessoryNumber',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseAccessoryNumber_Filter')
    p_web.SetSessionValue('BrowseAccessoryNumber_FirstValue','')
    p_web.SetSessionValue('BrowseAccessoryNumber_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,JOBACCNO,joa:RecordNumberKey,loc:PageRows,'BrowseAccessoryNumber',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,loc:ViewOptions,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled,Loc:LocatorCase)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or Loc:LocatorType = Net:Date or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position or loc:LocatorType = Net:Date
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If JOBACCNO{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(JOBACCNO,loc:firstvalue)
              Reset(ThisView,JOBACCNO)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If JOBACCNO{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(JOBACCNO,loc:lastvalue)
            Reset(ThisView,JOBACCNO)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(joa:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td class="'&p_web.combine(p_web.site.style.BrowseEmpty,)&'">'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:NavButtonPosition=Net:Above or (loc:NavButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseAccessoryNumber_nav_a" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseAccessoryNumber.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseAccessoryNumber.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseAccessoryNumber.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseAccessoryNumber.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseAccessoryNumber_nav_a','buttonset',loc:options)
          End
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:UpdateButtonPosition=Net:Above or (loc:UpdateButtonPosition=Net:Both and loc:found > 0))
    packet = clip(packet) & '<div id="BrowseAccessoryNumber_update_a" class="'&p_web.combine(p_web.site.style.BrowseUpdateButtonSet,)&'">'
    If loc:selecting = 0 or loc:popup
      if p_web.GSV('Job:ViewOnly') <> 1 and loc:viewOnly = 0
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:InsertButton,'BrowseAccessoryNumber',,,loc:FormPopup,'FormAccessoryNumbers')
          TableQueue.Kind = Net:BeforeTable
          do AddPacket
      End
    End
    If loc:found
          TableQueue.Kind = Net:BeforeTable
          do AddPacket
    End
    packet = clip(packet) & '</div><13,10>'
    If p_web.site.UseUpdateButtonSet
      loc:options = ''
      packet = clip(packet) & p_web.jQuery('#' & 'BrowseAccessoryNumber_update_a','buttonset',loc:options)
    End ! If p_web.site.UseUpdateButtonSet
      TableQueue.Kind = Net:BeforeTable
      do AddPacket
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseAccessoryNumber',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseAccessoryNumber_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseAccessoryNumber_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          case loc:locatortype
          of Net:Position
          orof Net:Date
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
          of Net:Contains
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
          of Net:Begins
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
          End
          packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
          Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
          Case Loc:LocatorType
          of Net:Date
            packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator1BrowseAccessoryNumber','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('BrowseAccessoryNumber_LocatorPic'),,,'onchange="BrowseAccessoryNumber.locate(''Locator1BrowseAccessoryNumber'',this.value);"',,0,,,) & '</div></td>'
          Else
            packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator1BrowseAccessoryNumber',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="BrowseAccessoryNumber.locate(''Locator1BrowseAccessoryNumber'',this.value);" ',,,,,,) & '</td>'
          End
          If loc:LocatorSearchButton or loc:LocatorClearButton
            packet = clip(packet) & '<td><div id="BrowseAccessoryNumber_locate_b" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
            If loc:LocatorSearchButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
            End
            If loc:LocatorClearButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseAccessoryNumber.cl(''BrowseAccessoryNumber'');')
            End
            packet = clip(packet) & '</div><13,10></td><13,10>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
          If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseAccessoryNumber_locate_b','buttonset',loc:options)
          End
      End
    End
  End
  p_web.SetSessionValue('BrowseAccessoryNumber_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseAccessoryNumber_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:NavButtonPosition=Net:Below or loc:NavButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseAccessoryNumber_nav_b" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseAccessoryNumber.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseAccessoryNumber.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseAccessoryNumber.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseAccessoryNumber.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseAccessoryNumber_nav_b','buttonset',loc:options)
          End
        end
        do SendPacket
  End
  If loc:UpdateButtonPosition=Net:Below or loc:UpdateButtonPosition=Net:Both
  packet = clip(packet) & '<div id="BrowseAccessoryNumber_update_b" class="'&p_web.combine(p_web.site.style.BrowseUpdateButtonSet,)&'">'
  If loc:selecting = 0 or loc:popup
    if p_web.GSV('Job:ViewOnly') <> 1 and loc:viewOnly = 0
          packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:InsertButton,'BrowseAccessoryNumber',,,loc:FormPopup,'FormAccessoryNumbers')
        do SendPacket
    End
  End
  If loc:found
        do SendPacket
  End
  packet = clip(packet) & '</div><13,10>'
  If p_web.site.UseUpdateButtonSet
    loc:options = ''
    packet = clip(packet) & p_web.jQuery('#' & 'BrowseAccessoryNumber_update_b','buttonset',loc:options)
  End ! If p_web.site.UseUpdateButtonSet
    do SendPacket
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
  data
loc:rowclick  string(1024)
  code
    loc:field = p_web.AddBrowseValue('BrowseAccessoryNumber','JOBACCNO',joa:RecordNumberKey) !joa:RecordNumber
    p_web._thisrow = p_web._nocolon('joa:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if Loc:LocatorValue <> '' and loc:ActualSelection = ''
        loc:checked = 'checked'
        do SetSelection
      elsif loc:ActualSelection = '' and joa:RecordNumber = p_web.GetValue('joa:RecordNumber')
         loc:checked = 'checked'
         do SetSelection
      elsif loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseAccessoryNumber:LookupField')) = joa:RecordNumber and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      else
        loc:checked = Choose((joa:RecordNumber = loc:selected) and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      end
      If(loc:SelectionMethod  = Net:Radio)
        loc:RowStyle = 'class="' & p_web.combine(p_web.site.style.browserow,) &'"'
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'data-nt-id="'& p_web.AddBrowseValue('BrowseAccessoryNumber','JOBACCNO',joa:RecordNumberKey) &'" onclick="'
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If JOBACCNO{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(JOBACCNO)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            loc:DefaultSelection = loc:field
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If JOBACCNO{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(JOBACCNO)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            loc:DefaultSelection = loc:field
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'>'&p_web.CreateInput('radio','_bidv_',clip(loc:field),,loc:checked,'')&'</td>'&CRLF
          If loc:DefaultSelection = ''
            loc:DefaultSelection = loc:field
          End
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          If loc:DefaultSelection = '' or loc:direction < 0
            loc:DefaultSelection = loc:field
          End
        End
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::joa:AccessoryNumber
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      If (p_web.GSV('Job:ViewOnly') <> 1) AND  true
        If Loc:Selecting = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::Delete
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        End     !Selecting
      End ! Field Condition
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr ' & clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Idx = p_web.AddBrowseValue('BrowseAccessoryNumber','JOBACCNO',joa:RecordNumberKey)
  TableQueue.Id[1] = joa:RecordNumber

ClosingScripts  Routine
  data
Rtn_SecwinProcedureName         string(252)
  code
  If p_web.RequestAjax = 0
    do SetFormAction
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var btiBrowseAccessoryNumber;if (btiBrowseAccessoryNumber != 1){{var BrowseAccessoryNumber=new browseTable(''BrowseAccessoryNumber'','''&clip(loc:formname)&|
      ''','''&p_web._jsok('joa:RecordNumber',Net:Parameter)&''',2,'''&clip(loc:divname)&''',1,1,1,'''&|
      clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&|
      p_web.Translate('Are you sure you want to delete this record?')&''','''&clip(loc:ActualSelection)&''','''&clip(loc:selectaction)&''','''&|
      clip(loc:formactiontarget)&''',''FormAccessoryNumbers'','&loc:popup&',0,'''','''',"' & clip(Rtn_SecwinProcedureName) & '");<13,10>'&|
      'BrowseAccessoryNumber.setGreenBar('''&p_web.ColorWeb(p_web.Site.Style.BrowseHighlightColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOneColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseTwoColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOverColor)&''');<13,10>' &|
      'BrowseAccessoryNumber.applyGreenBar();btiBrowseAccessoryNumber=1};<13,10>' &|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseAccessoryNumber')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseAccessoryNumber')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseAccessoryNumber')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseAccessoryNumber')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(JOBACCNO)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(JOBACCNO)
  Bind(joa:Record)
  Clear(joa:Record)
  NetWebSetSessionPics(p_web,JOBACCNO)

Children Routine
  if loc:selecting = 0
    If p_web.RequestAjax = 0
      do StartChildren
    Else
      do AjaxChildren
    End
  end

AjaxChildren  Routine
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue('joa:RecordNumber',p_web.GetValue('joa:RecordNumber'))
   p_web.DeleteValue('_Clicked')
   loc:found = 1
   do Children
! ----------------------------------------------------------------------------------------
CallRow  Routine
  data
loc:result  long
  code
  do OpenFilesB
  joa:RecordNumber = p_web.GSV('joa:RecordNumber')
  loc:result = p_web._GetFile(JOBACCNO,joa:RecordNumberKey)
  loc:eip = 1
  loc:viewstate = p_web.escape(p_web.Base64Encode(clip(joa:RecordNumber)))
  do BrowseRow
  do SendPacket
  do ClosefilesB

! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(JOBACCNO)
  Case upper(p_web.GetValue('_EIPClm'))
  Else
    case p_web.GetValue('_action_')
    of Net:InsertRecord
    orof Net:ChangeRecord
    orof Net:DeleteRecord
    orof Net:CopyRecord
      loc:eip = 0
      do CallBrowse
    end
  End
  do GotFocusBack
  p_web._CloseFile(JOBACCNO)
! ----------------------------------------------------------------------------------------
value::joa:AccessoryNumber   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseAccessoryNumber_joa:AccessoryNumber_'&joa:RecordNumber,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(joa:AccessoryNumber,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::Delete   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
  If (p_web.GSV('Job:ViewOnly') <> 1)
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseAccessoryNumber_Delete_'&joa:RecordNumber,,net:crc,,loc:extra)
          If p_web.GSV('Job:ViewOnly') <> 1 and loc:viewonly = 0
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallDeleteButton,'BrowseAccessoryNumber',p_web.AddBrowseValue('BrowseAccessoryNumber','JOBACCNO',joa:RecordNumberKey),,loc:FormPopup,'FormAccessoryNumbers') & '<13,10>'
          End
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
  End
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END
  return
!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
CheckForDuplicate  Routine
SetSelection  Routine
  loc:ActualSelection = loc:field
  p_web.SetSessionValue('joa:RecordNumber',joa:RecordNumber)

MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:ActualSelection = ''
    loc:ActualSelection = loc:DefaultSelection
    p_web.GetBrowseValue(loc:ActualSelection,Net:Web:SessionQueue) ! so children are primed with correct sessionValue
  End

  If loc:ActualSelection <> ''
    TableQueue.Kind = Net:RowTable
    get(TableQueue,TableQueue.Kind)
    if Errorcode() = 0
      TableQueue.Row = sub(TableQueue.Row,1,len(clip(TableQueue.Row))-1) &  ' data-nt-rows="'&loc:RowsHigh&'" data-nt-value="'&clip(loc:ActualSelection)&'"' & '>'
      Put(TableQueue)
    End
  End

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead class="'&p_web.combine(p_web.site.style.BrowseHeader,'')&'"><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot class="'&p_web.combine(p_web.site.style.BrowseFooter,)&'"><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody class="'&p_web.combine(p_web.site.style.BrowseBody,)&'"><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table><13,10>' & p_web.DivFooter(Net:NoSend) ! Table Div
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
JobAccessories       PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
tmp:FoundAccessory   STRING(1000)                          !
tmp:TheAccessory     STRING(1000)                          !
tmp:ShowAccessory    STRING(1000)                          !
tmp:TheJobAccessory  STRING(1000)                          !
FilesOpened     Long
ACCESSOR::State  USHORT
JOBSE::State  USHORT
textAccessoryMessage:IsInvalid  Long
tmp:ShowAccessory:IsInvalid  Long
tmp:TheAccessory:IsInvalid  Long
buttonAddAccessories:IsInvalid  Long
buttonRemoveAccessory:IsInvalid  Long
jobe:AccessoryNotes:IsInvalid  Long
BrowseAccessoryNumber:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('JobAccessories')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'JobAccessories_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('JobAccessories','')
    p_web.DivHeader('JobAccessories',p_web.combine(p_web.site.style.formdiv,))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('JobAccessories') = 0
        p_web.AddPreCall('JobAccessories')
        p_web.DivHeader('popup_JobAccessories','nt-hidden')
        p_web.DivHeader('JobAccessories',p_web.combine(p_web.site.style.formdiv,))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(800)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_JobAccessories_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_JobAccessories_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferJobAccessories',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_JobAccessories',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferJobAccessories',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_JobAccessories',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferJobAccessories',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_JobAccessories',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_JobAccessories',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferJobAccessories',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_JobAccessories',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferJobAccessories',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_JobAccessories',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_JobAccessories',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('JobAccessories')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(ACCESSOR)
  p_web._OpenFile(JOBSE)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(ACCESSOR)
  p_Web._CloseFile(JOBSE)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE
  loc:EipClm = upper(p_web.GetValue('_EIPClm'))
  p_web.DeleteValue('_EIPClm')
  case loc:EipClm
  of ''
    case upper(p_web.GetValue('_calledfrom_'))
    of upper('BrowseAccessoryNumber')
      do Value::BrowseAccessoryNumber
    end
  end

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('JobAccessories_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'JobAccessories'
    end
    p_web.formsettings.proc = 'JobAccessories'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  if p_web.IfExistsValue('tmp:ShowAccessory') = 0
    p_web.SetSessionValue('tmp:ShowAccessory',tmp:ShowAccessory)
  Else
    tmp:ShowAccessory = p_web.GetSessionValue('tmp:ShowAccessory')
  End
  if p_web.IfExistsValue('tmp:TheAccessory') = 0
    p_web.SetSessionValue('tmp:TheAccessory',tmp:TheAccessory)
  Else
    tmp:TheAccessory = p_web.GetSessionValue('tmp:TheAccessory')
  End
  if p_web.IfExistsValue('jobe:AccessoryNotes') = 0
    p_web.SetSessionValue('jobe:AccessoryNotes',jobe:AccessoryNotes)
  Else
    jobe:AccessoryNotes = p_web.GetSessionValue('jobe:AccessoryNotes')
  End

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('tmp:ShowAccessory')
    tmp:ShowAccessory = p_web.GetValue('tmp:ShowAccessory')
    p_web.SetSessionValue('tmp:ShowAccessory',tmp:ShowAccessory)
  Else
    tmp:ShowAccessory = p_web.GetSessionValue('tmp:ShowAccessory')
  End
  if p_web.IfExistsValue('tmp:TheAccessory')
    tmp:TheAccessory = p_web.GetValue('tmp:TheAccessory')
    p_web.SetSessionValue('tmp:TheAccessory',tmp:TheAccessory)
  Else
    tmp:TheAccessory = p_web.GetSessionValue('tmp:TheAccessory')
  End
  if p_web.IfExistsValue('jobe:AccessoryNotes')
    jobe:AccessoryNotes = p_web.GetValue('jobe:AccessoryNotes')
    p_web.SetSessionValue('jobe:AccessoryNotes',jobe:AccessoryNotes)
  Else
    jobe:AccessoryNotes = p_web.GetSessionValue('jobe:AccessoryNotes')
  End

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('JobAccessories_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'ViewJob'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('JobAccessories_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('JobAccessories_ChainTo')
    loc:formaction = p_web.GetSessionValue('JobAccessories_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'ViewJob'

GenerateForm   Routine
  do LoadRelatedRecords
 tmp:ShowAccessory = p_web.RestoreValue('tmp:ShowAccessory')
 tmp:TheAccessory = p_web.RestoreValue('tmp:TheAccessory')
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Job Accessories') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Job Accessories',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_JobAccessories',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_JobAccessories0_div')&'">'&p_web.Translate()&'</a></li>'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_JobAccessories1_div')&'">'&p_web.Translate('Accessory Numbers')&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  do GenerateTab1
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="JobAccessories_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('JobAccessories_BrowseAccessoryNumber_embedded_div')&''');'
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="JobAccessories_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'JobAccessories_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="JobAccessories_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('JobAccessories_BrowseAccessoryNumber_embedded_div')&''');'
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'JobAccessories_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:ShowAccessory')
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_JobAccessories')>0,p_web.GSV('showtab_JobAccessories'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_JobAccessories'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_JobAccessories') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_JobAccessories'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_JobAccessories')>0,p_web.GSV('showtab_JobAccessories'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_JobAccessories') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Accessory Numbers') & ''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_JobAccessories_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_JobAccessories')>0,p_web.GSV('showtab_JobAccessories'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"JobAccessories",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_JobAccessories')>0,p_web.GSV('showtab_JobAccessories'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_JobAccessories_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('JobAccessories') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('JobAccessories')
    p_web.SetValue('_CallPopups',2)
    If p_web.GetPreCall('BrowseAccessoryNumber') = 0
      p_web.SetValue('BrowseAccessoryNumber:NoForm',1)
      p_web.SetValue('BrowseAccessoryNumber:FormName',loc:formname)
      p_web.SetValue('BrowseAccessoryNumber:parentIs','Form')
      p_web.SetValue('_parentProc','JobAccessories')
      BrowseAccessoryNumber(p_web)
      p_web.SetValue('_CallPopups',0)
      p_web.DeleteValue('BrowseAccessoryNumber:NoForm')
      p_web.DeleteValue('BrowseAccessoryNumber:FormName')
      p_web.DeleteValue('BrowseAccessoryNumber:parentIs')
      p_web.DeleteValue('_parentProc')
    End
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate()&'</a></h3>' & CRLF & p_web.DivHeader('tab_JobAccessories0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_JobAccessories0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_JobAccessories0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_JobAccessories0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,)
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_JobAccessories0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend)
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_JobAccessories0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain">' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_JobAccessories0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::textAccessoryMessage
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:ShowAccessory
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:ShowAccessory
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::tmp:TheAccessory
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::tmp:TheAccessory
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::buttonAddAccessories
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::buttonAddAccessories
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::buttonRemoveAccessory
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::buttonRemoveAccessory
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td valign="top"'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::jobe:AccessoryNotes
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::jobe:AccessoryNotes
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab1  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Accessory Numbers')&'</a></h3>' & CRLF & p_web.DivHeader('tab_JobAccessories1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_JobAccessories1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_JobAccessories1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_JobAccessories1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Accessory Numbers')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_JobAccessories1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Accessory Numbers')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_JobAccessories1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Accessory Numbers')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_JobAccessories1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 2.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::BrowseAccessoryNumber
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Validate::textAccessoryMessage  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::textAccessoryMessage  ! copies value to session value if valid.

ValidateValue::textAccessoryMessage  Routine
    If not (1=0)
    End

Value::textAccessoryMessage  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobAccessories_' & p_web._nocolon('textAccessoryMessage') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
    '<div id="textAccessoryMessage" class="'&clip('green bold')&'"'&clip(loc:extra)&'>' & p_web.Translate('Hold "CTRL" or "SHIFT" to select more than one accessory',) & '</div>' & |
    '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()

Prompt::tmp:ShowAccessory  Routine
  packet = clip(packet) & p_web.DivHeader('JobAccessories_' & p_web._nocolon('tmp:ShowAccessory') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:ShowAccessory  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:ShowAccessory = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:ShowAccessory = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:ShowAccessory  ! copies value to session value if valid.
  do Value::tmp:TheAccessory  !1

ValidateValue::tmp:ShowAccessory  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:ShowAccessory',tmp:ShowAccessory).
    End

Value::tmp:ShowAccessory  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobAccessories_' & p_web._nocolon('tmp:ShowAccessory') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,p_web.site.style.formselect,,)
  If loc:retrying
    tmp:ShowAccessory = p_web.RestoreValue('tmp:ShowAccessory')
    do ValidateValue::tmp:ShowAccessory
    If tmp:ShowAccessory:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- DROPLIST ---
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ShowAccessory'',''jobaccessories_tmp:showaccessory_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:ShowAccessory')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:ShowAccessory',loc:fieldclass,loc:readonly,20,180,1,loc:javascript,)
  loc:fieldclass = ''
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  loc:rowstyle = choose(sub('GreyRegular',1,1)=' ',clip(loc:rowstyle) & 'GreyRegular','GreyRegular')
  if p_web.IfExistsSessionValue('tmp:ShowAccessory') = 0
    p_web.SetSessionValue('tmp:ShowAccessory','')
  end
    packet = clip(packet) & p_web.CreateOption('--- Available Accessories ---','',choose('' = p_web.getsessionvalue('tmp:ShowAccessory')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  Access:ACCESSOR.Clearkey(acr:Accesory_Key)
  acr:Model_NUmber = p_web.GSV('job:Model_Number')
  Set(acr:Accesory_Key,acr:Accesory_Key)
  Loop
      If Access:ACCESSOR.Next()
          Break
      End ! If Access:ACCESSOR.Next()
      If acr:Model_Number <> p_web.GSV('job:Model_Number')
          Break
      End ! If acr:Model_Number <> p_web.GSV('tmp:ModelNumber')
      If Instring(';' & Clip(acr:Accessory),p_web.GSV('tmp:TheJobAccessory'),1,1)
          Cycle
      End ! If Instring(';' & Clip(acr:Accessory) & ';',p_web.GSV('tmp:TheJobAccessory'),1,1)
  
      loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
      packet = clip(packet) & p_web.CreateOption(Clip(acr:Accessory),acr:Accessory,choose(acr:Accessory = p_web.GSV('tmp:ShowAccessory')),clip(loc:rowstyle),,)&CRLF
      loc:even = Choose(loc:even=1,2,1)
  End ! Loop
  
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web.DivFooter()

Prompt::tmp:TheAccessory  Routine
  packet = clip(packet) & p_web.DivHeader('JobAccessories_' & p_web._nocolon('tmp:TheAccessory') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::tmp:TheAccessory  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    tmp:TheAccessory = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    tmp:TheAccessory = p_web.GetValue('Value')
  End
  do ValidateValue::tmp:TheAccessory  ! copies value to session value if valid.

ValidateValue::tmp:TheAccessory  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('tmp:TheAccessory',tmp:TheAccessory).
    End

Value::tmp:TheAccessory  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobAccessories_' & p_web._nocolon('tmp:TheAccessory') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,p_web.site.style.formselect,,)
  If loc:retrying
    tmp:TheAccessory = p_web.RestoreValue('tmp:TheAccessory')
    do ValidateValue::tmp:TheAccessory
    If tmp:TheAccessory:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- DROPLIST ---
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:TheAccessory'',''jobaccessories_tmp:theaccessory_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:TheAccessory',loc:fieldclass,loc:readonly,20,180,0,loc:javascript,)
  loc:fieldclass = ''
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  loc:rowstyle = choose(sub('GreyRegular',1,1)=' ',clip(loc:rowstyle) & 'GreyRegular','GreyRegular')
  if p_web.IfExistsSessionValue('tmp:TheAccessory') = 0
    p_web.SetSessionValue('tmp:TheAccessory','')
  end
    packet = clip(packet) & p_web.CreateOption('--- Accessories On Job ---','',choose('' = p_web.getsessionvalue('tmp:TheAccessory')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
      If p_web.GSV('tmp:TheJobAccessory') <> ''
          Loop x# = 1 To 1000
              If Sub(p_web.GSV('tmp:TheJobAccessory'),x#,2) = '|;'
                  Start# = x# + 2
                  Cycle
              End ! If Sub(p_web.GSV('tmp:TheJobAccessory'),x#,2) = '|;'
  
              If Start# > 0
                 If Sub(p_web.GSV('tmp:TheJobAccessory'),x#,2) = ';|'
                     tmp:FoundAccessory = Sub(p_web.GSV('tmp:TheJobAccessory'),Start#,x# - Start#)
  
                     If tmp:FoundAccessory <> ''
                         loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
                         packet = clip(packet) & p_web.CreateOption(tmp:FoundAccessory,tmp:FoundAccessory,choose(tmp:FoundAccessory = p_web.GSV('tmp:TheAccessory')),clip(loc:rowstyle),,)&CRLF
                         loc:even = Choose(loc:even=1,2,1)
                     End ! If tmp:FoundAccessory <> ''
                     Start# = 0
                 End ! If Sub(p_web.GSV('tmp:TheJobAccessory'),x#,2) = ';|'
              End ! If Start# > 0
          End ! Loop x# = 1 To 1000
  
          If Start# > 0
             tmp:FoundAccessory = Clip(Sub(p_web.GSV('tmp:TheJobAccessory'),Start#,30))
             If tmp:FoundAccessory <> ''
                 loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
                 packet = clip(packet) & p_web.CreateOption(tmp:FoundAccessory,tmp:FoundAccessory,choose(tmp:FoundAccessory = p_web.GSV('tmp:TheAccessory')),clip(loc:rowstyle),,)&CRLF
                 loc:even = Choose(loc:even=1,2,1)
             End ! If tmp:FoundAccessory <> ''
          End ! If Start# > 0
      End ! If p_web.GSV('tmp:TheJobAccessory') <> ''
  
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web.DivFooter()

Prompt::buttonAddAccessories  Routine
  packet = clip(packet) & p_web.DivHeader('JobAccessories_' & p_web._nocolon('buttonAddAccessories') & '_prompt',Choose(p_web.GSV('Job:ViewOnly') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Job:ViewOnly') = 1,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::buttonAddAccessories  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonAddAccessories  ! copies value to session value if valid.
  p_web.SSV('tmp:TheJobAccessory',p_web.GSV('tmp:TheJobAccessory') & p_web.GSV('tmp:ShowAccessory'))
  p_web.SSV('tmp:ShowAccessory','')
  do Value::buttonAddAccessories
  do Value::tmp:ShowAccessory  !1
  do Value::tmp:TheAccessory  !1

ValidateValue::buttonAddAccessories  Routine
    If not (p_web.GSV('Job:ViewOnly') = 1)
    End

Value::buttonAddAccessories  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('Job:ViewOnly') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobAccessories_' & p_web._nocolon('buttonAddAccessories') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('Job:ViewOnly') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonAddAccessories'',''jobaccessories_buttonaddaccessories_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','AddAccessories','Add Accessories',p_web.combine(Choose('Add Accessories' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'button-entryfield'),loc:formname,,,,loc:javascript,loc:disabled,,,,,,,,,,'nt-left')!h
  do SendPacket
  End
  p_web.DivFooter()

Prompt::buttonRemoveAccessory  Routine
  packet = clip(packet) & p_web.DivHeader('JobAccessories_' & p_web._nocolon('buttonRemoveAccessory') & '_prompt',Choose(p_web.GSV('Job:ViewOnly') = 1,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(p_web.GSV('Job:ViewOnly') = 1,'',p_web.Translate(''))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::buttonRemoveAccessory  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::buttonRemoveAccessory  ! copies value to session value if valid.
  tmp:TheJobAccessory = p_web.GSV('tmp:TheJobAccessory') & ';'
  tmp:TheAccessory = '|;' & Clip(p_web.GSV('tmp:TheAccessory')) & ';'
  Loop x# = 1 To 1000
      pos# = Instring(Clip(tmp:TheAccessory),tmp:TheJobAccessory,1,1)
      If pos# > 0
          tmp:TheJobAccessory = Sub(tmp:TheJobAccessory,1,pos# - 1) & Sub(tmp:TheJobAccessory,pos# + Len(Clip(tmp:TheAccessory)),1000)
          Break
      End ! If pos# > 0#
  End ! Loop x# = 1 To 1000
  p_web.SetSessionValue('tmp:TheJobAccessory',Sub(tmp:TheJobAccessory,1,Len(Clip(tmp:TheJobAccessory)) - 1))
  p_web.SetSessionValue('tmp:TheAccessory','')
  
  do Value::buttonRemoveAccessory
  do Value::tmp:ShowAccessory  !1
  do Value::tmp:TheAccessory  !1

ValidateValue::buttonRemoveAccessory  Routine
    If not (p_web.GSV('Job:ViewOnly') = 1)
    End

Value::buttonRemoveAccessory  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('Job:ViewOnly') = 1,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobAccessories_' & p_web._nocolon('buttonRemoveAccessory') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('Job:ViewOnly') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonRemoveAccessory'',''jobaccessories_buttonremoveaccessory_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','RemoveAccessory','Remove Accessory',p_web.combine(Choose('Remove Accessory' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'button-entryfield'),loc:formname,,,,loc:javascript,loc:disabled,,,,,,,,,,'nt-left')!h
  do SendPacket
  End
  p_web.DivFooter()

Prompt::jobe:AccessoryNotes  Routine
  packet = clip(packet) & p_web.DivHeader('JobAccessories_' & p_web._nocolon('jobe:AccessoryNotes') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Accessory Notes'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::jobe:AccessoryNotes  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    jobe:AccessoryNotes = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = @s255
    jobe:AccessoryNotes = p_web.Dformat(p_web.GetValue('Value'),'@s255')
  End
  do ValidateValue::jobe:AccessoryNotes  ! copies value to session value if valid.
  do Value::jobe:AccessoryNotes
  do SendAlert

ValidateValue::jobe:AccessoryNotes  Routine
    If not (1=0)
    jobe:AccessoryNotes = Upper(jobe:AccessoryNotes)
      if loc:invalid = '' then p_web.SetSessionValue('jobe:AccessoryNotes',jobe:AccessoryNotes).
    End

Value::jobe:AccessoryNotes  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('JobAccessories_' & p_web._nocolon('jobe:AccessoryNotes') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:fieldclass = clip(loc:fieldclass) & ' nt-upper'
  If p_web.GSV('Job:ViewOnly') = 1 or loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    jobe:AccessoryNotes = p_web.RestoreValue('jobe:AccessoryNotes')
    do ValidateValue::jobe:AccessoryNotes
    If jobe:AccessoryNotes:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- TEXT --- jobe:AccessoryNotes
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe:AccessoryNotes'',''jobaccessories_jobe:accessorynotes_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('Job:ViewOnly') = 1,'readonly','')
  do SendPacket
  p_web.CreateTextArea('jobe:AccessoryNotes',p_web.GetSessionValue('jobe:AccessoryNotes'),5,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(jobe:AccessoryNotes),'Accessory Notes',,,Net:Send)
  do SendPacket
  End
  p_web.DivFooter()

Validate::BrowseAccessoryNumber  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  Elsif true
    p_web.StoreValue('joa:RecordNumber')
  End
  do ValidateValue::BrowseAccessoryNumber  ! copies value to session value if valid.

ValidateValue::BrowseAccessoryNumber  Routine
    If not (1=0)
    End

Value::BrowseAccessoryNumber  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(1=0,1,0))
  ! --- BROWSE ---  BrowseAccessoryNumber --
  p_web.SetValue('BrowseAccessoryNumber:NoForm',1)
  p_web.SetValue('BrowseAccessoryNumber:FormName',loc:formname)
  p_web.SetValue('BrowseAccessoryNumber:parentIs','Form')
  p_web.SetValue('_parentProc','JobAccessories')
  if p_web.RequestAjax = 0
    p_web.SSV('JobAccessories:_popup_',p_web.GetValue('_popup_')) ! stores the current browse popup state
    packet = clip(packet) & '<div id="'&lower('JobAccessories_BrowseAccessoryNumber_embedded_div')&'"><!-- Net:BrowseAccessoryNumber --></div><13,10>'
    do SendPacket
    p_web.DivHeader('JobAccessories_' & lower('BrowseAccessoryNumber') & '_value')
    p_web.DivFooter()
  else
    if p_web.GetValue('_popup_') = 1
      p_web.SSV('JobAccessories:_popup_',1)
    elsif p_web.GSV('JobAccessories:_popup_') = 1
      p_web.SetValue('_popup_',1)
    end
    packet = clip(packet) & '<!-- Net:BrowseAccessoryNumber --><13,10>'
  end
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('JobAccessories_nexttab_' & 0)
    tmp:ShowAccessory = p_web.GSV('tmp:ShowAccessory')
    do ValidateValue::tmp:ShowAccessory
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:ShowAccessory
      !do SendAlert
      !exit
    End
    tmp:TheAccessory = p_web.GSV('tmp:TheAccessory')
    do ValidateValue::tmp:TheAccessory
    If loc:Invalid
      loc:retrying = 1
      do Value::tmp:TheAccessory
      !do SendAlert
      !exit
    End
    jobe:AccessoryNotes = p_web.GSV('jobe:AccessoryNotes')
    do ValidateValue::jobe:AccessoryNotes
    If loc:Invalid
      loc:retrying = 1
      do Value::jobe:AccessoryNotes
      !do SendAlert
      !exit
    End
    If loc:Invalid then exit.
  of lower('JobAccessories_nexttab_' & 1)
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_JobAccessories_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('JobAccessories_tab_' & 0)
    do GenerateTab0
  of lower('JobAccessories_tmp:ShowAccessory_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ShowAccessory
      of event:timer
        do Value::tmp:ShowAccessory
      else
        do Value::tmp:ShowAccessory
      end
  of lower('JobAccessories_tmp:TheAccessory_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:TheAccessory
      of event:timer
        do Value::tmp:TheAccessory
      else
        do Value::tmp:TheAccessory
      end
  of lower('JobAccessories_buttonAddAccessories_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonAddAccessories
      of event:timer
        do Value::buttonAddAccessories
      else
        do Value::buttonAddAccessories
      end
  of lower('JobAccessories_buttonRemoveAccessory_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonRemoveAccessory
      of event:timer
        do Value::buttonRemoveAccessory
      else
        do Value::buttonRemoveAccessory
      end
  of lower('JobAccessories_jobe:AccessoryNotes_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe:AccessoryNotes
      of event:timer
        do Value::jobe:AccessoryNotes
      else
        do Value::jobe:AccessoryNotes
      end
  of lower('JobAccessories_tab_' & 1)
    do GenerateTab1
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('JobAccessories_form:ready_',1)

  p_web.SetSessionValue('JobAccessories_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_JobAccessories',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('JobAccessories_form:ready_',1)
  p_web.SetSessionValue('JobAccessories_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_JobAccessories',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('JobAccessories_form:ready_',1)
  p_web.SetSessionValue('JobAccessories_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('JobAccessories:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('JobAccessories_form:ready_',1)
  p_web.SetSessionValue('JobAccessories_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('JobAccessories:Primed',0)
  p_web.setsessionvalue('showtab_JobAccessories',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
      If not (1=0)
          If p_web.IfExistsValue('tmp:ShowAccessory')
            tmp:ShowAccessory = p_web.GetValue('tmp:ShowAccessory')
          End
      End
      If not (1=0)
          If p_web.IfExistsValue('tmp:TheAccessory')
            tmp:TheAccessory = p_web.GetValue('tmp:TheAccessory')
          End
      End
      If not (1=0)
        If not (p_web.GSV('Job:ViewOnly') = 1)
          If p_web.IfExistsValue('jobe:AccessoryNotes')
            jobe:AccessoryNotes = p_web.GetValue('jobe:AccessoryNotes')
          End
        End
      End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('JobAccessories_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('JobAccessories_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
    do ValidateValue::textAccessoryMessage
    If loc:Invalid then exit.
    do ValidateValue::tmp:ShowAccessory
    If loc:Invalid then exit.
    do ValidateValue::tmp:TheAccessory
    If loc:Invalid then exit.
    do ValidateValue::buttonAddAccessories
    If loc:Invalid then exit.
    do ValidateValue::buttonRemoveAccessory
    If loc:Invalid then exit.
    do ValidateValue::jobe:AccessoryNotes
    If loc:Invalid then exit.
  ! tab = 3
    loc:InvalidTab += 1
    do ValidateValue::BrowseAccessoryNumber
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('JobAccessories:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('tmp:ShowAccessory')
  p_web.StoreValue('tmp:TheAccessory')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')

