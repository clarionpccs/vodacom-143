

   MEMBER('WebServer_Phase4.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSE049.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSE053.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE054.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSE055.INC'),ONCE        !Req'd for module callout resolution
                     END


BrowseAdjustedWebOrderReceipts PROCEDURE  (NetWebServerWorker p_web)
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256),dim(Net:MaxKeyFields)
idx           String(Net:HashSize)
sub           Long
            End
res:Ref_Number:IsInvalid  Long
res:Part_Number:IsInvalid  Long
res:Description:IsInvalid  Long
res:Item_Cost:IsInvalid  Long
res:Quantity:IsInvalid  Long
res:QuantityReceived:IsInvalid  Long
res:Amend_Reason:IsInvalid  Long
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:DefaultSelection    String(Net:HashSize)
loc:ActualSelection     String(Net:HashSize)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
!loc:pagename            String(256)
loc:NavButtonPosition    Long
loc:UpdateButtonPosition Long
loc:SelectionMethod      Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorCase         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
loc:lookupdone          Long
loc:FormPopup           Long
ThisView            View(RETSTOCK)
                      Project(res:Record_Number)
                      Project(res:Ref_Number)
                      Project(res:Part_Number)
                      Project(res:Description)
                      Project(res:Item_Cost)
                      Project(res:Quantity)
                      Project(res:QuantityReceived)
                      Project(res:Amend_Reason)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
Loc:popup             Long
loc:poppedup          Long,static,thread
FilesOpened     Long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return 
  End
  GlobalErrors.SetProcedureName('BrowseAdjustedWebOrderReceipts')
  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseAdjustedWebOrderReceipts:NoForm')
      loc:NoForm = p_web.GetValue('BrowseAdjustedWebOrderReceipts:NoForm')
      loc:FormName = p_web.GetValue('BrowseAdjustedWebOrderReceipts:FormName')
    else
      loc:FormName = 'BrowseAdjustedWebOrderReceipts_frm'
    End
    p_web.SSV('BrowseAdjustedWebOrderReceipts:NoForm',loc:NoForm)
    p_web.SSV('BrowseAdjustedWebOrderReceipts:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseAdjustedWebOrderReceipts:NoForm')
    loc:FormName = p_web.GSV('BrowseAdjustedWebOrderReceipts:FormName')
  end
  case p_web.site.formpopups
  of -1 ; loc:FormPopup = Net:Page
  of 0 ; loc:FormPopup = Net:Page
  of 1 ; loc:FormPopup = Net:Popup
  End
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseAdjustedWebOrderReceipts') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseAdjustedWebOrderReceipts')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if 1 = 2
  elsif p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  elsif p_web.IfExistsValue('_EIPRow_')
    do CallRow
  elsif p_web.GetValue('_CallPopups') > 0
    do CallPopups
  elsif p_web.IfExistsValue('_GetSecwinSettings')
      p_web.SetValue('Secwin_' & 'BrowseAdjustedWebOrderReceipts' & 'AccessGroupsArray','1') 
      p_web.SetValue('Secwin_AccessWindowName','BrowseAdjustedWebOrderReceipts')
      
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallPopups  Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
loc:CallPopups   Long
loc:name         String(255)
  code
  if loc:poppedup = 0
    loc:CallPopups = p_web.GetValue('_CallPopups')
    if loc:CallPopups = 1 !and p_web.GetPreCall('BrowseAdjustedWebOrderReceipts') = 0! for = 1 include popup divs and scripts for this browse
      !p_web.AddPreCall('BrowseAdjustedWebOrderReceipts')
      p_web.DivHeader('popup_BrowseAdjustedWebOrderReceipts','nt-hidden')
      p_web.DivHeader('BrowseAdjustedWebOrderReceipts',p_web.combine(p_web.site.style.browsediv,))
      p_web.DivFooter()
      p_web.DivFooter()
      loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(400)&', modal: true, position: [''center'',15]'
      packet =  p_web.jQuery('#' & lower('popup_BrowseAdjustedWebOrderReceipts_div'),'dialog',loc:options) & |
      p_web.jQuery('#' & lower('popup_BrowseAdjustedWebOrderReceipts_div'),'removeClass','''nt-hidden''')
    End
    ! for > 0 add in form, EIP-lookups and browse object
    do Popups
    loc:popup = 1
    do ClosingScripts
    do SendPacket
    loc:poppedup = 1
  end

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web.DivHeader('BrowseAdjustedWebOrderReceipts',p_web.combine(p_web.site.style.browsediv,))
  if loc:ParentSilent = 0
    do GenerateBrowse
    if p_web.RequestAjax = 1
      p_web.Script('BrowseProducts.pcv();')
    end
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web.DivFooter()
  do Children
  do Popups
  if loc:poppedup = 0
    do ClosingScripts
  end
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(RETSTOCK,res:Record_Number_Key,loc:vorder)
    If False
    ElsIf (loc:vorder = 'RES:REF_NUMBER') then p_web.SetValue('BrowseAdjustedWebOrderReceipts_sort','1')
    ElsIf (loc:vorder = 'RES:PART_NUMBER') then p_web.SetValue('BrowseAdjustedWebOrderReceipts_sort','2')
    ElsIf (loc:vorder = 'RES:DESCRIPTION') then p_web.SetValue('BrowseAdjustedWebOrderReceipts_sort','3')
    ElsIf (loc:vorder = 'RES:ITEM_COST') then p_web.SetValue('BrowseAdjustedWebOrderReceipts_sort','4')
    ElsIf (loc:vorder = 'RES:QUANTITY') then p_web.SetValue('BrowseAdjustedWebOrderReceipts_sort','5')
    ElsIf (loc:vorder = 'RES:QUANTITYRECEIVED') then p_web.SetValue('BrowseAdjustedWebOrderReceipts_sort','6')
    ElsIf (loc:vorder = 'RES:AMEND_REASON') then p_web.SetValue('BrowseAdjustedWebOrderReceipts_sort','7')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseAdjustedWebOrderReceipts:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseAdjustedWebOrderReceipts:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseAdjustedWebOrderReceipts:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseAdjustedWebOrderReceipts:LookupField')
    loc:selecting = 0
  End

Popups Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  If loc:popup = 0
  End
SetFormAction  Routine
  loc:formaction = 'BrowseAdjustedWebOrderReceipts'

GotFocusBack   Routine

GenerateBrowse Routine
  data
loc:viewoptions  Long
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  ! Set general Browse options
  loc:NavButtonPosition   = Net:Below
  loc:UpdateButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:PageLoad
  loc:Sorting          = Net:ServerSort
  if p_web.GetValue('_popup_') = 1
    loc:popup = 1
  end
  ! Set Locator Options
  loc:LocatorPosition  = Net:Above
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  p_web.ClearBrowse('BrowseAdjustedWebOrderReceipts')
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
      p_web.site.CloseButton.Image = '/images/psave.png'
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 15
  End
  loc:ActualSelection = ''
  !loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseAdjustedWebOrderReceipts_sort',net:DontEvaluate)
  If loc:vordernumber = 0
    loc:vordernumber = 1
  End
  p_web.SetSessionValue('BrowseAdjustedWebOrderReceipts_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'res:Ref_Number','-res:Ref_Number')
    Loc:LocateField = 'res:Ref_Number'
    Loc:LocatorCase = 0
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(res:Part_Number)','-UPPER(res:Part_Number)')
    Loc:LocateField = 'res:Part_Number'
    Loc:LocatorCase = 0
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(res:Description)','-UPPER(res:Description)')
    Loc:LocateField = 'res:Description'
    Loc:LocatorCase = 0
  of 4
    loc:vorder = Choose(Loc:SortDirection=1,'res:Item_Cost','-res:Item_Cost')
    Loc:LocateField = 'res:Item_Cost'
    Loc:LocatorCase = 0
  of 5
    loc:vorder = Choose(Loc:SortDirection=1,'res:Quantity','-res:Quantity')
    Loc:LocateField = 'res:Quantity'
    Loc:LocatorCase = 0
  of 6
    loc:vorder = Choose(Loc:SortDirection=1,'res:QuantityReceived','-res:QuantityReceived')
    Loc:LocateField = 'res:QuantityReceived'
    Loc:LocatorCase = 0
  of 7
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(res:Amend_Reason)','-UPPER(res:Amend_Reason)')
    Loc:LocateField = 'res:Amend_Reason'
    Loc:LocatorCase = 0
  end
  if loc:vorder = ''
    loc:vorder = '+UPPER(res:Amend_Site_Loc),+res:Amended,+res:Ref_Number'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('res:Ref_Number')
    loc:SortHeader = p_web.Translate('Sales Number')
    p_web.SetSessionValue('BrowseAdjustedWebOrderReceipts_LocatorPic','@N_8')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
  Of upper('res:Part_Number')
    loc:SortHeader = p_web.Translate('Part Number')
    p_web.SetSessionValue('BrowseAdjustedWebOrderReceipts_LocatorPic','@s30')
  Of upper('res:Description')
    loc:SortHeader = p_web.Translate('Description')
    p_web.SetSessionValue('BrowseAdjustedWebOrderReceipts_LocatorPic','@s30')
  Of upper('res:Item_Cost')
    loc:SortHeader = p_web.Translate('Retail Cost')
    p_web.SetSessionValue('BrowseAdjustedWebOrderReceipts_LocatorPic','@n14.2')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
  Of upper('res:Quantity')
    loc:SortHeader = p_web.Translate('Qty Req')
    p_web.SetSessionValue('BrowseAdjustedWebOrderReceipts_LocatorPic','@n_8')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
  Of upper('res:QuantityReceived')
    loc:SortHeader = p_web.Translate('Qty Rcvd')
    p_web.SetSessionValue('BrowseAdjustedWebOrderReceipts_LocatorPic','@n_8')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
  Of upper('res:Amend_Reason')
    loc:SortHeader = p_web.Translate('Amend Reason')
    p_web.SetSessionValue('BrowseAdjustedWebOrderReceipts_LocatorPic','@s254')
  End
  If loc:selecting = 1
    p_web.GetSettings(p_web.GetSessionValue('Push1'))
    loc:selectaction = p_web.FormSettings.ParentPage !p_web.GetSessionValue('BrowseAdjustedWebOrderReceipts:LookupFrom')
  End!Else
  loc:CloseAction = 'FormBrowseStock'
  do SendPacket
  do SetFormAction
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseAdjustedWebOrderReceipts:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseAdjustedWebOrderReceipts:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseAdjustedWebOrderReceipts:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="RETSTOCK"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="res:Record_Number_Key"></input><13,10>'
  end
  if p_web.Translate('') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.browseHeading,)&'">'&p_web.Translate('',0)&'</div>'&CRLF
  end
  If p_web.Translate('Browse Adjusted Web Order Receipts') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.browseSubHeading,)&'">'&p_web.Translate('Browse Adjusted Web Order Receipts',0)&'</div>'&CRLF
  End
  case p_web.GetValue('refresh') ; of 'next' orof 'previous' orof 'first' orof 'last'
    p_web.Script('try{{BrowseAdjustedWebOrderReceipts.restoreFocus();} catch(err){{};')
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseAdjustedWebOrderReceipts',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      case loc:locatortype
      of Net:Position
      orof Net:Date
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
      of Net:Contains
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
      of Net:Begins
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
      End
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
      Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
      Case Loc:LocatorType
      of Net:Date
        packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator2BrowseAdjustedWebOrderReceipts','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('BrowseAdjustedWebOrderReceipts_LocatorPic'),,,'onchange="BrowseAdjustedWebOrderReceipts.locate(''Locator2BrowseAdjustedWebOrderReceipts'',this.value);"',,0,,,) & '</div></td>'
      Else
        packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator2BrowseAdjustedWebOrderReceipts',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="BrowseAdjustedWebOrderReceipts.locate(''Locator2BrowseAdjustedWebOrderReceipts'',this.value);" ',,,,,,) & '</td>'
      End
      If loc:LocatorSearchButton or loc:LocatorClearButton
        packet = clip(packet) & '<td><div id="BrowseAdjustedWebOrderReceipts_locate_a" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
        If loc:LocatorSearchButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
        End
        If loc:LocatorClearButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseAdjustedWebOrderReceipts.cl(''BrowseAdjustedWebOrderReceipts'');')
        End
        packet = clip(packet) & '</div><13,10></td><13,10>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
      If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'BrowseAdjustedWebOrderReceipts_locate_a','buttonset',loc:options)
      End
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & p_web.DivHeader('BrowseAdjustedWebOrderReceipts_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'','sortable')&'" id="BrowseAdjustedWebOrderReceipts_tbl">'
  Else
    packet = clip(packet) & p_web.DivHeader('BrowseAdjustedWebOrderReceipts_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'')&'" id="BrowseAdjustedWebOrderReceipts_tbl">'
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'1','BrowseAdjustedWebOrderReceipts',p_web.Translate('Sales Number'),'Click here to sort by Sales Number',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'2','BrowseAdjustedWebOrderReceipts',p_web.Translate('Part Number'),'Click here to sort by Part Number',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'3','BrowseAdjustedWebOrderReceipts',p_web.Translate('Description'),'Click here to sort by Description',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'4','BrowseAdjustedWebOrderReceipts',p_web.Translate('Retail Cost'),'Click here to sort by Retail Cost',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'5','BrowseAdjustedWebOrderReceipts',p_web.Translate('Qty Req'),'Click here to sort by Quantity',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'6','BrowseAdjustedWebOrderReceipts',p_web.Translate('Qty Rcvd'),'Click here to sort by Quantity Received',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'7','BrowseAdjustedWebOrderReceipts',p_web.Translate('Amend Reason'),'Click here to sort by Amend Reason',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,15,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  If Instring('res:record_number',lower(loc:vorder),1,1) = 0 !and RETSTOCK{prop:SQLDriver} = 1
    loc:vorder = Choose(loc:vorder='','res:Record_Number',clip(loc:vorder) & ',' & 'res:Record_Number')
  End
  Loc:Selected = Choose(p_web.IfExistsValue('res:Record_Number'),p_web.GetValue('res:Record_Number'),p_web.GetSessionValue('res:Record_Number'))
  ThisView{prop:order} = p_web.CleanFilter(ThisView,clip(loc:vorder))
      loc:FilterWas = 'Upper(res:Amend_Site_Loc) = Upper(''' & p_web.GSV('Default:SiteLocation') & ''') AND res:Amended = 1'
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseAdjustedWebOrderReceipts',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseAdjustedWebOrderReceipts_Filter')
    p_web.SetSessionValue('BrowseAdjustedWebOrderReceipts_FirstValue','')
    p_web.SetSessionValue('BrowseAdjustedWebOrderReceipts_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,RETSTOCK,res:Record_Number_Key,loc:PageRows,'BrowseAdjustedWebOrderReceipts',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,loc:ViewOptions,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled,Loc:LocatorCase)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or Loc:LocatorType = Net:Date or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position or loc:LocatorType = Net:Date
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If RETSTOCK{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(RETSTOCK,loc:firstvalue)
              Reset(ThisView,RETSTOCK)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If RETSTOCK{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(RETSTOCK,loc:lastvalue)
            Reset(ThisView,RETSTOCK)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(res:Record_Number)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td class="'&p_web.combine(p_web.site.style.BrowseEmpty,)&'">'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:NavButtonPosition=Net:Above or (loc:NavButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseAdjustedWebOrderReceipts_nav_a" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,' nt-left nt-margin-right',)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseAdjustedWebOrderReceipts.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseAdjustedWebOrderReceipts.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseAdjustedWebOrderReceipts.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseAdjustedWebOrderReceipts.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseAdjustedWebOrderReceipts_nav_a','buttonset',loc:options)
          End
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:UpdateButtonPosition=Net:Above or (loc:UpdateButtonPosition=Net:Both and loc:found > 0))
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseAdjustedWebOrderReceipts',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseAdjustedWebOrderReceipts_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseAdjustedWebOrderReceipts_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          case loc:locatortype
          of Net:Position
          orof Net:Date
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
          of Net:Contains
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
          of Net:Begins
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
          End
          packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
          Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
          Case Loc:LocatorType
          of Net:Date
            packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator1BrowseAdjustedWebOrderReceipts','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('BrowseAdjustedWebOrderReceipts_LocatorPic'),,,'onchange="BrowseAdjustedWebOrderReceipts.locate(''Locator1BrowseAdjustedWebOrderReceipts'',this.value);"',,0,,,) & '</div></td>'
          Else
            packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator1BrowseAdjustedWebOrderReceipts',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="BrowseAdjustedWebOrderReceipts.locate(''Locator1BrowseAdjustedWebOrderReceipts'',this.value);" ',,,,,,) & '</td>'
          End
          If loc:LocatorSearchButton or loc:LocatorClearButton
            packet = clip(packet) & '<td><div id="BrowseAdjustedWebOrderReceipts_locate_b" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
            If loc:LocatorSearchButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
            End
            If loc:LocatorClearButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseAdjustedWebOrderReceipts.cl(''BrowseAdjustedWebOrderReceipts'');')
            End
            packet = clip(packet) & '</div><13,10></td><13,10>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
          If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseAdjustedWebOrderReceipts_locate_b','buttonset',loc:options)
          End
      End
    End
  End
  p_web.SetSessionValue('BrowseAdjustedWebOrderReceipts_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseAdjustedWebOrderReceipts_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:NavButtonPosition=Net:Below or loc:NavButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseAdjustedWebOrderReceipts_nav_b" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,' nt-left nt-margin-right',)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseAdjustedWebOrderReceipts.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseAdjustedWebOrderReceipts.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseAdjustedWebOrderReceipts.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseAdjustedWebOrderReceipts.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseAdjustedWebOrderReceipts_nav_b','buttonset',loc:options)
          End
        end
        do SendPacket
  End
  If loc:UpdateButtonPosition=Net:Below or loc:UpdateButtonPosition=Net:Both
  End
  If loc:selecting = 0 and loc:parent = ''
      if loc:popup
        packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:BrowseCloseButton,'BrowseAdjustedWebOrderReceipts',,,loc:popup)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCloseButton,loc:Formname,loc:CloseAction)
      end
  End
    do SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
  data
loc:rowclick  string(1024)
  code
    loc:field = p_web.AddBrowseValue('BrowseAdjustedWebOrderReceipts','RETSTOCK',res:Record_Number_Key) !res:Record_Number
    p_web._thisrow = p_web._nocolon('res:Record_Number')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if Loc:LocatorValue <> '' and loc:ActualSelection = ''
        loc:checked = 'checked'
        do SetSelection
      elsif loc:ActualSelection = '' and res:Record_Number = p_web.GetValue('res:Record_Number')
         loc:checked = 'checked'
         do SetSelection
      elsif loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseAdjustedWebOrderReceipts:LookupField')) = res:Record_Number and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      else
        loc:checked = Choose((res:Record_Number = loc:selected) and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      end
      If(loc:SelectionMethod  = Net:Radio)
        loc:RowStyle = 'class="' & p_web.combine(p_web.site.style.browserow,) &'"'
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'data-nt-id="'& p_web.AddBrowseValue('BrowseAdjustedWebOrderReceipts','RETSTOCK',res:Record_Number_Key) &'" onclick="'
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If RETSTOCK{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(RETSTOCK)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            loc:DefaultSelection = loc:field
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If RETSTOCK{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(RETSTOCK)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            loc:DefaultSelection = loc:field
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'>'&p_web.CreateInput('radio','_bidv_',clip(loc:field),,loc:checked,'')&'</td>'&CRLF
          If loc:DefaultSelection = ''
            loc:DefaultSelection = loc:field
          End
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          If loc:DefaultSelection = '' or loc:direction < 0
            loc:DefaultSelection = loc:field
          End
        End
    end ! loc:eip = 0
      IF (loc:Checked = 'checked')
      END ! IF (loc:Checked = 'checked')
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('RightJustify')&'"><13,10>'
          end ! loc:eip = 0
          do value::res:Ref_Number
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::res:Part_Number
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::res:Description
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('RightJustify')&'"><13,10>'
          end ! loc:eip = 0
          do value::res:Item_Cost
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('RightJustify')&'"><13,10>'
          end ! loc:eip = 0
          do value::res:Quantity
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::res:QuantityReceived
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::res:Amend_Reason
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr ' & clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Idx = p_web.AddBrowseValue('BrowseAdjustedWebOrderReceipts','RETSTOCK',res:Record_Number_Key)
  TableQueue.Id[1] = res:Record_Number

ClosingScripts  Routine
  data
Rtn_SecwinProcedureName         string(252)
  code
  If p_web.RequestAjax = 0
    do SetFormAction
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var btiBrowseAdjustedWebOrderReceipts;if (btiBrowseAdjustedWebOrderReceipts != 1){{var BrowseAdjustedWebOrderReceipts=new browseTable(''BrowseAdjustedWebOrderReceipts'','''&clip(loc:formname)&|
      ''','''&p_web._jsok('res:Record_Number',Net:Parameter)&''',2,'''&clip(loc:divname)&''',1,1,1,'''&|
      clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&|
      p_web.Translate('Are you sure you want to delete this record?')&''','''&clip(loc:ActualSelection)&''','''&clip(loc:selectaction)&''','''&|
      clip(loc:formactiontarget)&''','''','&loc:popup&',0,'''','''',"' & clip(Rtn_SecwinProcedureName) & '");<13,10>'&|
      'BrowseAdjustedWebOrderReceipts.setGreenBar('''&p_web.ColorWeb(p_web.Site.Style.BrowseHighlightColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOneColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseTwoColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOverColor)&''');<13,10>' &|
      'BrowseAdjustedWebOrderReceipts.applyGreenBar();btiBrowseAdjustedWebOrderReceipts=1};<13,10>' &|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseAdjustedWebOrderReceipts')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseAdjustedWebOrderReceipts')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseAdjustedWebOrderReceipts')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseAdjustedWebOrderReceipts')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(RETSTOCK)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(RETSTOCK)
  Bind(res:Record)
  Clear(res:Record)
  NetWebSetSessionPics(p_web,RETSTOCK)

Children Routine
  if loc:selecting = 0
    If p_web.RequestAjax = 0
      do StartChildren
    Else
      do AjaxChildren
    End
  end

AjaxChildren  Routine
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue('res:Record_Number',p_web.GetValue('res:Record_Number'))
   p_web.DeleteValue('_Clicked')
   loc:found = 1
   do Children
! ----------------------------------------------------------------------------------------
CallRow  Routine
  data
loc:result  long
  code
  do OpenFilesB
  res:Record_Number = p_web.GSV('res:Record_Number')
  loc:result = p_web._GetFile(RETSTOCK,res:Record_Number_Key)
  loc:eip = 1
  loc:viewstate = p_web.escape(p_web.Base64Encode(clip(res:Record_Number)))
  do BrowseRow
  do SendPacket
  do ClosefilesB

! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(RETSTOCK)
  Case upper(p_web.GetValue('_EIPClm'))
  Else
    case p_web.GetValue('_action_')
    of Net:InsertRecord
    orof Net:ChangeRecord
    orof Net:DeleteRecord
    orof Net:CopyRecord
      loc:eip = 0
      do CallBrowse
    end
  End
  do GotFocusBack
  p_web._CloseFile(RETSTOCK)
! ----------------------------------------------------------------------------------------
value::res:Ref_Number   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseAdjustedWebOrderReceipts_res:Ref_Number_'&res:Record_Number,'RightJustify',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(res:Ref_Number,'@N_8')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::res:Part_Number   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseAdjustedWebOrderReceipts_res:Part_Number_'&res:Record_Number,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(res:Part_Number,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::res:Description   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseAdjustedWebOrderReceipts_res:Description_'&res:Record_Number,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(res:Description,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::res:Item_Cost   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseAdjustedWebOrderReceipts_res:Item_Cost_'&res:Record_Number,'RightJustify',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(res:Item_Cost,'@n14.2')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::res:Quantity   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseAdjustedWebOrderReceipts_res:Quantity_'&res:Record_Number,'RightJustify',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(res:Quantity,'@n_8')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::res:QuantityReceived   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseAdjustedWebOrderReceipts_res:QuantityReceived_'&res:Record_Number,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(res:QuantityReceived,'@n_8')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::res:Amend_Reason   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseAdjustedWebOrderReceipts_res:Amend_Reason_'&res:Record_Number,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(res:Amend_Reason,'@s254')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END
  return
!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
CheckForDuplicate  Routine
SetSelection  Routine
  loc:ActualSelection = loc:field
  p_web.SetSessionValue('res:Record_Number',res:Record_Number)

MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:ActualSelection = ''
    loc:ActualSelection = loc:DefaultSelection
    p_web.GetBrowseValue(loc:ActualSelection,Net:Web:SessionQueue) ! so children are primed with correct sessionValue
  End

  If loc:ActualSelection <> ''
    TableQueue.Kind = Net:RowTable
    get(TableQueue,TableQueue.Kind)
    if Errorcode() = 0
      TableQueue.Row = sub(TableQueue.Row,1,len(clip(TableQueue.Row))-1) &  ' data-nt-rows="'&loc:RowsHigh&'" data-nt-value="'&clip(loc:ActualSelection)&'"' & '>'
      Put(TableQueue)
    End
  End

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead class="'&p_web.combine(p_web.site.style.BrowseHeader,'')&'"><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot class="'&p_web.combine(p_web.site.style.BrowseFooter,)&'"><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody class="'&p_web.combine(p_web.site.style.BrowseBody,)&'"><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table><13,10>' & p_web.DivFooter(Net:NoSend) ! Table Div
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
SaveSessionVars  ROUTINE
RestoreSessionVars  ROUTINE
BrowseRetailSales    PROCEDURE  (NetWebServerWorker p_web)
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256),dim(Net:MaxKeyFields)
idx           String(Net:HashSize)
sub           Long
            End
ret:Ref_Number:IsInvalid  Long
ret:Purchase_Order_Number:IsInvalid  Long
ret:date_booked:IsInvalid  Long
ret:Invoice_Date:IsInvalid  Long
ret:who_booked:IsInvalid  Long
ret:Courier:IsInvalid  Long
ret:Invoice_Number:IsInvalid  Long
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:DefaultSelection    String(Net:HashSize)
loc:ActualSelection     String(Net:HashSize)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
!loc:pagename            String(256)
loc:NavButtonPosition    Long
loc:UpdateButtonPosition Long
loc:SelectionMethod      Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorCase         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
loc:lookupdone          Long
loc:FormPopup           Long
ThisView            View(RETSALES)
                      Project(ret:Ref_Number)
                      Project(ret:Ref_Number)
                      Project(ret:Purchase_Order_Number)
                      Project(ret:date_booked)
                      Project(ret:Invoice_Date)
                      Project(ret:who_booked)
                      Project(ret:Courier)
                      Project(ret:Invoice_Number)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
Loc:popup             Long
loc:poppedup          Long,static,thread
FilesOpened     Long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return 
  End
  GlobalErrors.SetProcedureName('BrowseRetailSales')
  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseRetailSales:NoForm')
      loc:NoForm = p_web.GetValue('BrowseRetailSales:NoForm')
      loc:FormName = p_web.GetValue('BrowseRetailSales:FormName')
    else
      loc:FormName = 'BrowseRetailSales_frm'
    End
    p_web.SSV('BrowseRetailSales:NoForm',loc:NoForm)
    p_web.SSV('BrowseRetailSales:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseRetailSales:NoForm')
    loc:FormName = p_web.GSV('BrowseRetailSales:FormName')
  end
  case p_web.site.formpopups
  of -1 ; loc:FormPopup = Net:Page
  of 0 ; loc:FormPopup = Net:Page
  of 1 ; loc:FormPopup = Net:Popup
  End
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseRetailSales') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseRetailSales')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if 1 = 2
  elsif p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  elsif p_web.IfExistsValue('_EIPRow_')
    do CallRow
  elsif p_web.GetValue('_CallPopups') > 0
    do CallPopups
  elsif p_web.IfExistsValue('_GetSecwinSettings')
      p_web.SetValue('Secwin_' & 'BrowseRetailSales' & 'AccessGroupsArray','1') 
      p_web.SetValue('Secwin_AccessWindowName','BrowseRetailSales')
      
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallPopups  Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
loc:CallPopups   Long
loc:name         String(255)
  code
  if loc:poppedup = 0
    loc:CallPopups = p_web.GetValue('_CallPopups')
    if loc:CallPopups = 1 !and p_web.GetPreCall('BrowseRetailSales') = 0! for = 1 include popup divs and scripts for this browse
      !p_web.AddPreCall('BrowseRetailSales')
      p_web.DivHeader('popup_BrowseRetailSales','nt-hidden')
      p_web.DivHeader('BrowseRetailSales',p_web.combine(p_web.site.style.browsediv,))
      p_web.DivFooter()
      p_web.DivFooter()
      loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(400)&', modal: true, position: [''center'',15]'
      packet =  p_web.jQuery('#' & lower('popup_BrowseRetailSales_div'),'dialog',loc:options) & |
      p_web.jQuery('#' & lower('popup_BrowseRetailSales_div'),'removeClass','''nt-hidden''')
    End
    ! for > 0 add in form, EIP-lookups and browse object
    do Popups
    loc:popup = 1
    do ClosingScripts
    do SendPacket
    loc:poppedup = 1
  end

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
      do SendPacket
      Do ShowWait
      do SendPacket
      Do CloseWait
      do SendPacket
  p_web.DivHeader('BrowseRetailSales',p_web.combine(p_web.site.style.browsediv,))
  if loc:ParentSilent = 0
    do GenerateBrowse
    if p_web.RequestAjax = 1
      p_web.Script('BrowseProducts.pcv();')
    end
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web.DivFooter()
  do Children
  do Popups
  if loc:poppedup = 0
    do ClosingScripts
  end
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(RETSALES,ret:Ref_Number_Key,loc:vorder)
    If False
    ElsIf (loc:vorder = 'RET:REF_NUMBER') then p_web.SetValue('BrowseRetailSales_sort','1')
    ElsIf (loc:vorder = 'RET:PURCHASE_ORDER_NUMBER') then p_web.SetValue('BrowseRetailSales_sort','2')
    ElsIf (loc:vorder = 'RET:DATE_BOOKED') then p_web.SetValue('BrowseRetailSales_sort','4')
    ElsIf (loc:vorder = 'RET:INVOICE_DATE') then p_web.SetValue('BrowseRetailSales_sort','5')
    ElsIf (loc:vorder = 'RET:WHO_BOOKED') then p_web.SetValue('BrowseRetailSales_sort','6')
    ElsIf (loc:vorder = 'RET:COURIER') then p_web.SetValue('BrowseRetailSales_sort','7')
    ElsIf (loc:vorder = 'RET:INVOICE_NUMBER') then p_web.SetValue('BrowseRetailSales_sort','8')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseRetailSales:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseRetailSales:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseRetailSales:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseRetailSales:LookupField')
    loc:selecting = 0
  End

Popups Routine
  data
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  If loc:popup = 0
  End
SetFormAction  Routine
  loc:formaction = 'FormRetailSales'
  loc:formactiontarget = '_self'

GotFocusBack   Routine

GenerateBrowse Routine
  data
loc:viewoptions  Long
loc:options      String(OptionsStringLen)  ! options for jQuery calls
  code
  ! Set general Browse options
  loc:NavButtonPosition   = Net:Below
  loc:UpdateButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:PageLoad
  loc:Sorting          = Net:ServerSort
  if p_web.GetValue('_popup_') = 1
    loc:popup = 1
  end
  ! Set Locator Options
  loc:LocatorPosition  = Net:Above
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  p_web.ClearBrowse('BrowseRetailSales')
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
      p_web.site.CloseButton.Image = '/images/psave.png'
      p_web.site.ChangeButton.TextValue = 'View Sale'
      p_web.site.ChangeButton.Class = 'DoubleButton'
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 15
  End
  loc:ActualSelection = ''
  !loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseRetailSales_sort',net:DontEvaluate)
  If loc:vordernumber = 0
    loc:vordernumber = 1
  End
  p_web.SetSessionValue('BrowseRetailSales_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'ret:Ref_Number','-ret:Ref_Number')
    Loc:LocateField = 'ret:Ref_Number'
    Loc:LocatorCase = 0
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(ret:Purchase_Order_Number)','-UPPER(ret:Purchase_Order_Number)')
    Loc:LocateField = 'ret:Purchase_Order_Number'
    Loc:LocatorCase = 0
  of 4
    loc:vorder = Choose(Loc:SortDirection=1,'ret:date_booked','-ret:date_booked')
    Loc:LocateField = 'ret:date_booked'
    Loc:LocatorCase = 0
  of 5
    loc:vorder = Choose(Loc:SortDirection=1,'ret:Invoice_Date','-ret:Invoice_Date')
    Loc:LocateField = 'ret:Invoice_Date'
    Loc:LocatorCase = 0
  of 6
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(ret:who_booked)','-UPPER(ret:who_booked)')
    Loc:LocateField = 'ret:who_booked'
    Loc:LocatorCase = 0
  of 7
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(ret:Courier)','-UPPER(ret:Courier)')
    Loc:LocateField = 'ret:Courier'
    Loc:LocatorCase = 0
  of 8
    loc:vorder = Choose(Loc:SortDirection=1,'ret:Invoice_Number','-ret:Invoice_Number')
    Loc:LocateField = 'ret:Invoice_Number'
    Loc:LocatorCase = 0
  end
  if loc:vorder = ''
    loc:vorder = '+UPPER(ret:Account_Number),+ret:Ref_Number'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('ret:Ref_Number')
    loc:SortHeader = p_web.Translate('Sales Number')
    p_web.SetSessionValue('BrowseRetailSales_LocatorPic','@n_8')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
  Of upper('ret:Purchase_Order_Number')
    loc:SortHeader = p_web.Translate('Purchase Order Number')
    p_web.SetSessionValue('BrowseRetailSales_LocatorPic','@s30')
  Of upper('ret:date_booked')
    loc:SortHeader = p_web.Translate('Date Raised')
    p_web.SetSessionValue('BrowseRetailSales_LocatorPic','@d6b')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
    loc:LocatorType = Net:Date
  Of upper('ret:Invoice_Date')
    loc:SortHeader = p_web.Translate('Invoice Date')
    p_web.SetSessionValue('BrowseRetailSales_LocatorPic','@d6b')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
    loc:LocatorType = Net:Date
  Of upper('ret:who_booked')
    loc:SortHeader = p_web.Translate('User')
    p_web.SetSessionValue('BrowseRetailSales_LocatorPic','@s3')
  Of upper('ret:Courier')
    loc:SortHeader = p_web.Translate('Courier')
    p_web.SetSessionValue('BrowseRetailSales_LocatorPic','@s30')
  Of upper('ret:Invoice_Number')
    loc:SortHeader = p_web.Translate('Inv No')
    p_web.SetSessionValue('BrowseRetailSales_LocatorPic','@n_8')
    if loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains then loc:LocatorType = Net:Position.
  End
  If loc:selecting = 1
    p_web.GetSettings(p_web.GetSessionValue('Push1'))
    loc:selectaction = p_web.FormSettings.ParentPage !p_web.GetSessionValue('BrowseRetailSales:LookupFrom')
  End!Else
  loc:CloseAction = 'FormBrowseStock'
  do SendPacket
  do SetFormAction
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseRetailSales:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseRetailSales:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseRetailSales:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="RETSALES"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="ret:Ref_Number_Key"></input><13,10>'
  end
  if p_web.Translate('') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.browseHeading,)&'">'&p_web.Translate('',0)&'</div>'&CRLF
  end
  If p_web.Translate('Browse Retail Sales') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.browseSubHeading,)&'">'&p_web.Translate('Browse Retail Sales',0)&'</div>'&CRLF
  End
  case p_web.GetValue('refresh') ; of 'next' orof 'previous' orof 'first' orof 'last'
    p_web.Script('try{{BrowseRetailSales.restoreFocus();} catch(err){{};')
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseRetailSales',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      case loc:locatortype
      of Net:Position
      orof Net:Date
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
      of Net:Contains
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
      of Net:Begins
        p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
      End
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
      Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
      Case Loc:LocatorType
      of Net:Date
        packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator2BrowseRetailSales','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('BrowseRetailSales_LocatorPic'),,,'onchange="BrowseRetailSales.locate(''Locator2BrowseRetailSales'',this.value);"',,0,,,) & '</div></td>'
      Else
        packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator2BrowseRetailSales',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="BrowseRetailSales.locate(''Locator2BrowseRetailSales'',this.value);" ',,,,,,) & '</td>'
      End
      If loc:LocatorSearchButton or loc:LocatorClearButton
        packet = clip(packet) & '<td><div id="BrowseRetailSales_locate_a" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
        If loc:LocatorSearchButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
        End
        If loc:LocatorClearButton
          packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseRetailSales.cl(''BrowseRetailSales'');')
        End
        packet = clip(packet) & '</div><13,10></td><13,10>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
      If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'BrowseRetailSales_locate_a','buttonset',loc:options)
      End
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & p_web.DivHeader('BrowseRetailSales_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'','sortable')&'" id="BrowseRetailSales_tbl">'
  Else
    packet = clip(packet) & p_web.DivHeader('BrowseRetailSales_table',p_web.Combine(p_web.site.style.BrowseTableDiv,),Net:NoSend) &| ! Table Div
    '<table class="'&p_web.Combine(p_web.site.style.BrowseTable,'')&'" id="BrowseRetailSales_tbl">'
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'1','BrowseRetailSales',p_web.Translate('Sales Number'),'Click here to sort by Sales Number',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'2','BrowseRetailSales',p_web.Translate('Purchase Order Number'),'Click here to sort by Purchase Order Number',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'4','BrowseRetailSales',p_web.Translate('Date Raised'),'Click here to sort by date booked',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'5','BrowseRetailSales',p_web.Translate('Invoice Date'),'Click here to sort by Invoice Date',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'6','BrowseRetailSales',p_web.Translate('User'),'Click here to sort by who booked',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'7','BrowseRetailSales',p_web.Translate('Courier'),'Click here to sort by Courier',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
            packet = clip(packet) & p_web.CreateSortHeader(loc:vordernumber,'8','BrowseRetailSales',p_web.Translate('Inv No'),'Click here to sort by Invoice Number',,,,,,0,loc:Sorting,'String')
          do AddPacket
          loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:LocateField = 'ret:date_booked' then Loc:NoBuffer = 1.
  If Loc:LocateField = 'ret:Invoice_Date' then Loc:NoBuffer = 1.
  If Loc:NoBuffer = 0
    Buffer(ThisView,15,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  If Instring('ret:ref_number',lower(loc:vorder),1,1) = 0 !and RETSALES{prop:SQLDriver} = 1
    loc:vorder = Choose(loc:vorder='','ret:Ref_Number',clip(loc:vorder) & ',' & 'ret:Ref_Number')
  End
  Loc:Selected = Choose(p_web.IfExistsValue('ret:Ref_Number'),p_web.GetValue('ret:Ref_Number'),p_web.GetSessionValue('ret:Ref_Number'))
  ThisView{prop:order} = p_web.CleanFilter(ThisView,clip(loc:vorder))
      loc:FilterWas = 'Upper(ret:Account_Number) = Upper(''' & p_web.GSV('BookingStoresAccount') & ''')'
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseRetailSales',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseRetailSales_Filter')
    p_web.SetSessionValue('BrowseRetailSales_FirstValue','')
    p_web.SetSessionValue('BrowseRetailSales_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,RETSALES,ret:Ref_Number_Key,loc:PageRows,'BrowseRetailSales',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,loc:ViewOptions,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled,Loc:LocatorCase)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or Loc:LocatorType = Net:Date or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position or loc:LocatorType = Net:Date
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If RETSALES{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(RETSALES,loc:firstvalue)
              Reset(ThisView,RETSALES)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If RETSALES{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(RETSALES,loc:lastvalue)
            Reset(ThisView,RETSALES)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(ret:Ref_Number)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td class="'&p_web.combine(p_web.site.style.BrowseEmpty,)&'">'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:NavButtonPosition=Net:Above or (loc:NavButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseRetailSales_nav_a" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseRetailSales.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseRetailSales.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseRetailSales.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseRetailSales.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseRetailSales_nav_a','buttonset',loc:options)
          End
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:UpdateButtonPosition=Net:Above or (loc:UpdateButtonPosition=Net:Both and loc:found > 0))
    packet = clip(packet) & '<div id="BrowseRetailSales_update_a" class="'&p_web.combine(p_web.site.style.BrowseUpdateButtonSet,)&'">'
    If loc:found
          Loc:IsChange = 0
          If loc:selecting = 0 or loc:popup
            If loc:viewonly = 0
              packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:ChangeButton,'BrowseRetailSales',,,loc:FormPopup,'FormRetailSales')
              loc:isChange = 1
            End
          End
          TableQueue.Kind = Net:BeforeTable
          do AddPacket
    End
    packet = clip(packet) & '</div><13,10>'
    If p_web.site.UseUpdateButtonSet
      loc:options = ''
      packet = clip(packet) & p_web.jQuery('#' & 'BrowseRetailSales_update_a','buttonset',loc:options)
    End ! If p_web.site.UseUpdateButtonSet
      TableQueue.Kind = Net:BeforeTable
      do AddPacket
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseRetailSales',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseRetailSales_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseRetailSales_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          case loc:locatortype
          of Net:Position
          orof Net:Date
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextPosition
          of Net:Contains
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextContains
          of Net:Begins
            p_web.site.LocatePromptText = p_web.site.LocatePromptTextBegins
          End
          packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.BrowseLocator,)&'"><tr>' &|
          Choose(0=0,'<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>','')
          Case Loc:LocatorType
          of Net:Date
            packet = clip(packet) & '<td><div>' & p_web.CreateDateInput('Locator1BrowseRetailSales','',p_web.combine(p_web.site.style.BrowseLocator,),,, p_web.GSV('BrowseRetailSales_LocatorPic'),,,'onchange="BrowseRetailSales.locate(''Locator1BrowseRetailSales'',this.value);"',,0,,,) & '</div></td>'
          Else
            packet = clip(packet) & '<td>' & p_web.CreateInput('input','Locator1BrowseRetailSales',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),p_web.combine(p_web.site.style.BrowseLocator,),,'size="30" onchange="BrowseRetailSales.locate(''Locator1BrowseRetailSales'',this.value);" ',,,,,,) & '</td>'
          End
          If loc:LocatorSearchButton or loc:LocatorClearButton
            packet = clip(packet) & '<td><div id="BrowseRetailSales_locate_b" class="'&p_web.combine(p_web.site.style.BrowseLocateButtonSet,)&'">'
            If loc:LocatorSearchButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:LocateButton)
            End
            If loc:LocatorClearButton
              packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseRetailSales.cl(''BrowseRetailSales'');')
            End
            packet = clip(packet) & '</div><13,10></td><13,10>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
          If (loc:LocatorSearchButton or loc:LocatorClearButton) and p_web.site.UseLocatorButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseRetailSales_locate_b','buttonset',loc:options)
          End
      End
    End
  End
  p_web.SetSessionValue('BrowseRetailSales_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseRetailSales_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:NavButtonPosition=Net:Below or loc:NavButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) & '<div id="BrowseRetailSales_nav_b" class="'&p_web.combine(p_web.site.style.BrowseNavButtonSet,)&'">'
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseRetailSales.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseRetailSales.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseRetailSales.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseRetailSales.last();',,loc:nextdisabled)
          packet = clip(packet) & '</div><13,10>'
          If p_web.site.UseNavigationButtonSet
            loc:options = ''
            packet = clip(packet) & p_web.jQuery('#' & 'BrowseRetailSales_nav_b','buttonset',loc:options)
          End
        end
        do SendPacket
  End
  If loc:UpdateButtonPosition=Net:Below or loc:UpdateButtonPosition=Net:Both
  packet = clip(packet) & '<div id="BrowseRetailSales_update_b" class="'&p_web.combine(p_web.site.style.BrowseUpdateButtonSet,)&'">'
  If loc:found
        Loc:IsChange = 0
        If loc:selecting = 0 or loc:popup
          If loc:viewonly = 0
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:ChangeButton,'BrowseRetailSales',,,loc:FormPopup,'FormRetailSales')
            loc:isChange = 1
          End
        End
        do SendPacket
  End
  packet = clip(packet) & '</div><13,10>'
  If p_web.site.UseUpdateButtonSet
    loc:options = ''
    packet = clip(packet) & p_web.jQuery('#' & 'BrowseRetailSales_update_b','buttonset',loc:options)
  End ! If p_web.site.UseUpdateButtonSet
    do SendPacket
  End
  If loc:selecting = 0 and loc:parent = ''
      if loc:popup
        packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:BrowseCloseButton,'BrowseRetailSales',,,loc:popup)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCloseButton,loc:Formname,loc:CloseAction)
      end
  End
    do SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
  data
loc:rowclick  string(1024)
  code
    loc:field = p_web.AddBrowseValue('BrowseRetailSales','RETSALES',ret:Ref_Number_Key) !ret:Ref_Number
    p_web._thisrow = p_web._nocolon('ret:Ref_Number')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if Loc:LocatorValue <> '' and loc:ActualSelection = ''
        loc:checked = 'checked'
        do SetSelection
      elsif loc:ActualSelection = '' and ret:Ref_Number = p_web.GetValue('ret:Ref_Number')
         loc:checked = 'checked'
         do SetSelection
      elsif loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseRetailSales:LookupField')) = ret:Ref_Number and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      else
        loc:checked = Choose((ret:Ref_Number = loc:selected) and loc:ActualSelection = '','checked','')
        if loc:checked <> '' then do SetSelection.
      end
      If(loc:SelectionMethod  = Net:Radio)
        loc:RowStyle = 'class="' & p_web.combine(p_web.site.style.browserow,) &'"'
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'data-nt-id="'& p_web.AddBrowseValue('BrowseRetailSales','RETSALES',ret:Ref_Number_Key) &'" onclick="'
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If RETSALES{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(RETSALES)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            loc:DefaultSelection = loc:field
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If RETSALES{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(RETSALES)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            loc:DefaultSelection = loc:field
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'>'&p_web.CreateInput('radio','_bidv_',clip(loc:field),,loc:checked,'')&'</td>'&CRLF
          If loc:DefaultSelection = ''
            loc:DefaultSelection = loc:field
          End
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          If loc:DefaultSelection = '' or loc:direction < 0
            loc:DefaultSelection = loc:field
          End
        End
    end ! loc:eip = 0
      IF (loc:Checked = 'checked')
      END ! IF (loc:Checked = 'checked')
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('RightJustify')&'"><13,10>'
          end ! loc:eip = 0
          do value::ret:Ref_Number
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::ret:Purchase_Order_Number
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('RightJustify')&'"><13,10>'
          end ! loc:eip = 0
          do value::ret:date_booked
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('RightJustify')&'"><13,10>'
          end ! loc:eip = 0
          do value::ret:Invoice_Date
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::ret:who_booked
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td><13,10>'
          end ! loc:eip = 0
          do value::ret:Courier
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&p_web.combine('RightJustify')&'"><13,10>'
          end ! loc:eip = 0
          do value::ret:Invoice_Number
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr ' & clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Idx = p_web.AddBrowseValue('BrowseRetailSales','RETSALES',ret:Ref_Number_Key)
  TableQueue.Id[1] = ret:Ref_Number

ClosingScripts  Routine
  data
Rtn_SecwinProcedureName         string(252)
  code
  If p_web.RequestAjax = 0
    do SetFormAction
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var btiBrowseRetailSales;if (btiBrowseRetailSales != 1){{var BrowseRetailSales=new browseTable(''BrowseRetailSales'','''&clip(loc:formname)&|
      ''','''&p_web._jsok('ret:Ref_Number',Net:Parameter)&''',2,'''&clip(loc:divname)&''',1,1,1,'''&|
      clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&|
      p_web.Translate('Are you sure you want to delete this record?')&''','''&clip(loc:ActualSelection)&''','''&clip(loc:selectaction)&''','''&|
      clip(loc:formactiontarget)&''',''FormRetailSales'','&loc:popup&',0,'''','''',"' & clip(Rtn_SecwinProcedureName) & '");<13,10>'&|
      'BrowseRetailSales.setGreenBar('''&p_web.ColorWeb(p_web.Site.Style.BrowseHighlightColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOneColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseTwoColor)&''','''&p_web.ColorWeb(p_web.Site.Style.BrowseOverColor)&''');<13,10>' &|
      'BrowseRetailSales.applyGreenBar();btiBrowseRetailSales=1};<13,10>' &|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseRetailSales')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseRetailSales')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseRetailSales')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseRetailSales')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(RETSALES)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(RETSALES)
  Bind(ret:Record)
  Clear(ret:Record)
  NetWebSetSessionPics(p_web,RETSALES)

Children Routine
  if loc:selecting = 0
    If p_web.RequestAjax = 0
      do StartChildren
    Else
      do AjaxChildren
    End
  end

AjaxChildren  Routine
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue('ret:Ref_Number',p_web.GetValue('ret:Ref_Number'))
   p_web.DeleteValue('_Clicked')
   loc:found = 1
   do Children
! ----------------------------------------------------------------------------------------
CallRow  Routine
  data
loc:result  long
  code
  do OpenFilesB
  ret:Ref_Number = p_web.GSV('ret:Ref_Number')
  loc:result = p_web._GetFile(RETSALES,ret:Ref_Number_Key)
  loc:eip = 1
  loc:viewstate = p_web.escape(p_web.Base64Encode(clip(ret:Ref_Number)))
  do BrowseRow
  do SendPacket
  do ClosefilesB

! ----------------------------------------------------------------------------------------
SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(RETSALES)
  Case upper(p_web.GetValue('_EIPClm'))
  Else
    case p_web.GetValue('_action_')
    of Net:InsertRecord
    orof Net:ChangeRecord
    orof Net:DeleteRecord
    orof Net:CopyRecord
      loc:eip = 0
      do CallBrowse
    end
  End
  do GotFocusBack
  p_web._CloseFile(RETSALES)
! ----------------------------------------------------------------------------------------
value::ret:Ref_Number   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseRetailSales_ret:Ref_Number_'&ret:Ref_Number,'RightJustify',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(ret:Ref_Number,'@n_8')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::ret:Purchase_Order_Number   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseRetailSales_ret:Purchase_Order_Number_'&ret:Ref_Number,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(ret:Purchase_Order_Number,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::ret:date_booked   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseRetailSales_ret:date_booked_'&ret:Ref_Number,'RightJustify',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(ret:date_booked,'@d6b')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::ret:Invoice_Date   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseRetailSales_ret:Invoice_Date_'&ret:Ref_Number,'RightJustify',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(ret:Invoice_Date,'@d6b')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::ret:who_booked   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseRetailSales_ret:who_booked_'&ret:Ref_Number,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(ret:who_booked,'@s3')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::ret:Courier   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseRetailSales_ret:Courier_'&ret:Ref_Number,,net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(ret:Courier,'@s30')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::ret:Invoice_Number   Routine
  data
loc:extra      String(256)
loc:disabled   String(20)
loc:FormOk     Long(1)
loc:options    String(OptionsStringLen)  ! options for jQuery calls
loc:fieldClass String(255)
loc:javascript String(JavascriptStringLen)
loc:ok         Long
  code
    if false
    else ! default settings for column
      loc:extra = ''
      packet = clip(packet) & p_web.DivHeader('BrowseRetailSales_ret:Invoice_Number_'&ret:Ref_Number,'RightJustify',net:crc,,loc:extra)
      packet = clip(packet) &  p_web.CreateHyperLink(p_web._jsok(Left(Format(ret:Invoice_Number,'@n_8')),0),,,,loc:javascript,,,0)
    End
    packet = clip(packet) & p_web.DivFooter(Net:NoSend)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END
  return
!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
CheckForDuplicate  Routine
SetSelection  Routine
  loc:ActualSelection = loc:field
  p_web.SetSessionValue('ret:Ref_Number',ret:Ref_Number)

MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:ActualSelection = ''
    loc:ActualSelection = loc:DefaultSelection
    p_web.GetBrowseValue(loc:ActualSelection,Net:Web:SessionQueue) ! so children are primed with correct sessionValue
  End

  If loc:ActualSelection <> ''
    TableQueue.Kind = Net:RowTable
    get(TableQueue,TableQueue.Kind)
    if Errorcode() = 0
      TableQueue.Row = sub(TableQueue.Row,1,len(clip(TableQueue.Row))-1) &  ' data-nt-rows="'&loc:RowsHigh&'" data-nt-value="'&clip(loc:ActualSelection)&'"' & '>'
      Put(TableQueue)
    End
  End

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Begins or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead class="'&p_web.combine(p_web.site.style.BrowseHeader,'')&'"><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot class="'&p_web.combine(p_web.site.style.BrowseFooter,)&'"><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody class="'&p_web.combine(p_web.site.style.BrowseBody,)&'"><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table><13,10>' & p_web.DivFooter(Net:NoSend) ! Table Div
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
ShowWait  Routine
  packet = clip(packet) & p_web.AsciiToUTF(|
    '<<!-- Net:ShowWait --><13,10>'&|
    '',net:OnlyIfUTF)
  DO SendPacket
CloseWait  Routine
  packet = clip(packet) & p_web.AsciiToUTF(|
    '<<!-- Net:CloseWait --><13,10>'&|
    '',net:OnlyIfUTF)
SaveSessionVars  ROUTINE
RestoreSessionVars  ROUTINE
frmPendingWebOrder   PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
FilesOpened     Long
TagFile::State  USHORT
brwPendingWebOrder:IsInvalid  Long
btnMakeTaggedOrders:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('frmPendingWebOrder')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'frmPendingWebOrder_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('frmPendingWebOrder','')
    p_web.DivHeader('frmPendingWebOrder',p_web.combine(p_web.site.style.formdiv,))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('frmPendingWebOrder') = 0
        p_web.AddPreCall('frmPendingWebOrder')
        p_web.DivHeader('popup_frmPendingWebOrder','nt-hidden')
        p_web.DivHeader('frmPendingWebOrder',p_web.combine(p_web.site.style.formdiv,))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_frmPendingWebOrder_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_frmPendingWebOrder_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferfrmPendingWebOrder',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_frmPendingWebOrder',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmPendingWebOrder',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_frmPendingWebOrder',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmPendingWebOrder',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_frmPendingWebOrder',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_frmPendingWebOrder',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmPendingWebOrder',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_frmPendingWebOrder',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmPendingWebOrder',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_frmPendingWebOrder',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_frmPendingWebOrder',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('frmPendingWebOrder')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(TagFile)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(TagFile)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE
  loc:EipClm = upper(p_web.GetValue('_EIPClm'))
  p_web.DeleteValue('_EIPClm')
  case loc:EipClm
  of ''
    case upper(p_web.GetValue('_calledfrom_'))
    of upper('brwPendingWebOrder')
      do Value::brwPendingWebOrder
    end
  end

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('frmPendingWebOrder_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'frmPendingWebOrder'
    end
    p_web.formsettings.proc = 'frmPendingWebOrder'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('frmPendingWebOrder_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'FormBrowseStock'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('frmPendingWebOrder_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('frmPendingWebOrder_ChainTo')
    loc:formaction = p_web.GetSessionValue('frmPendingWebOrder_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction

GenerateForm   Routine
  do LoadRelatedRecords
      p_web.site.SaveButton.TextValue = 'Close'
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Browse Pending Web Orders') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Browse Pending Web Orders',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_frmPendingWebOrder',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_frmPendingWebOrder0_div')&'">'&p_web.Translate()&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="frmPendingWebOrder_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('frmPendingWebOrder_brwPendingWebOrder_embedded_div')&''');'
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="frmPendingWebOrder_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'frmPendingWebOrder_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="frmPendingWebOrder_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('frmPendingWebOrder_brwPendingWebOrder_embedded_div')&''');'
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'frmPendingWebOrder_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_frmPendingWebOrder')>0,p_web.GSV('showtab_frmPendingWebOrder'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_frmPendingWebOrder'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_frmPendingWebOrder') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_frmPendingWebOrder'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_frmPendingWebOrder')>0,p_web.GSV('showtab_frmPendingWebOrder'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_frmPendingWebOrder') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_frmPendingWebOrder_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_frmPendingWebOrder')>0,p_web.GSV('showtab_frmPendingWebOrder'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"frmPendingWebOrder",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_frmPendingWebOrder')>0,p_web.GSV('showtab_frmPendingWebOrder'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_frmPendingWebOrder_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('frmPendingWebOrder') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('frmPendingWebOrder')
    p_web.SetValue('_CallPopups',2)
    If p_web.GetPreCall('brwPendingWebOrder') = 0
      p_web.SetValue('brwPendingWebOrder:NoForm',1)
      p_web.SetValue('brwPendingWebOrder:FormName',loc:formname)
      p_web.SetValue('brwPendingWebOrder:parentIs','Form')
      p_web.SetValue('_parentProc','frmPendingWebOrder')
      brwPendingWebOrder(p_web)
      p_web.SetValue('_CallPopups',0)
      p_web.DeleteValue('brwPendingWebOrder:NoForm')
      p_web.DeleteValue('brwPendingWebOrder:FormName')
      p_web.DeleteValue('brwPendingWebOrder:parentIs')
      p_web.DeleteValue('_parentProc')
    End
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate()&'</a></h3>' & CRLF & p_web.DivHeader('tab_frmPendingWebOrder0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_frmPendingWebOrder0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_frmPendingWebOrder0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_frmPendingWebOrder0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,)
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_frmPendingWebOrder0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend)
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_frmPendingWebOrder0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain">' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_frmPendingWebOrder0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          if loc:maxcolumns = 0 then loc:maxcolumns = 3.
          packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::brwPendingWebOrder
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::btnMakeTaggedOrders
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::btnMakeTaggedOrders
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Validate::brwPendingWebOrder  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  Elsif true
    p_web.StoreValue('orw:RecordNumber')
  End
  do ValidateValue::brwPendingWebOrder  ! copies value to session value if valid.
  do Comment::brwPendingWebOrder ! allows comment style to be updated.
  do Value::btnMakeTaggedOrders  !1

ValidateValue::brwPendingWebOrder  Routine
    If not (1=0)
    End

Value::brwPendingWebOrder  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(1=0,1,0))
  ! --- BROWSE ---  brwPendingWebOrder --
  p_web.SetValue('brwPendingWebOrder:NoForm',1)
  p_web.SetValue('brwPendingWebOrder:FormName',loc:formname)
  p_web.SetValue('brwPendingWebOrder:parentIs','Form')
  p_web.SetValue('_parentProc','frmPendingWebOrder')
  if p_web.RequestAjax = 0
    p_web.SSV('frmPendingWebOrder:_popup_',p_web.GetValue('_popup_')) ! stores the current browse popup state
    packet = clip(packet) & '<div id="'&lower('frmPendingWebOrder_brwPendingWebOrder_embedded_div')&'"><!-- Net:brwPendingWebOrder --></div><13,10>'
    do SendPacket
    p_web.DivHeader('frmPendingWebOrder_' & lower('brwPendingWebOrder') & '_value')
    p_web.DivFooter()
  else
    if p_web.GetValue('_popup_') = 1
      p_web.SSV('frmPendingWebOrder:_popup_',1)
    elsif p_web.GSV('frmPendingWebOrder:_popup_') = 1
      p_web.SetValue('_popup_',1)
    end
    packet = clip(packet) & '<!-- Net:brwPendingWebOrder --><13,10>'
  end
  do SendPacket
Comment::brwPendingWebOrder  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if brwPendingWebOrder:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('frmPendingWebOrder_' & p_web._nocolon('brwPendingWebOrder') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::btnMakeTaggedOrders  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::btnMakeTaggedOrders  ! copies value to session value if valid.
  do Comment::btnMakeTaggedOrders ! allows comment style to be updated.

ValidateValue::btnMakeTaggedOrders  Routine
    If not (1=0)
    End

Value::btnMakeTaggedOrders  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('frmPendingWebOrder_' & p_web._nocolon('btnMakeTaggedOrders') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','btnMakeTaggedOrders','Make Tagged Orders',p_web.combine(Choose('Make Tagged Orders' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.WindowOpen('frmMakeTaggedOrders'&'','_self'),,loc:disabled,,,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::btnMakeTaggedOrders  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if btnMakeTaggedOrders:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('frmPendingWebOrder_' & p_web._nocolon('btnMakeTaggedOrders') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('frmPendingWebOrder_nexttab_' & 0)
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_frmPendingWebOrder_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('frmPendingWebOrder_tab_' & 0)
    do GenerateTab0
  of lower('frmPendingWebOrder_brwPendingWebOrder_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::brwPendingWebOrder
      of event:timer
        do Value::brwPendingWebOrder
        do Comment::brwPendingWebOrder
      else
        do Value::brwPendingWebOrder
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('frmPendingWebOrder_form:ready_',1)

  p_web.SetSessionValue('frmPendingWebOrder_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_frmPendingWebOrder',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('frmPendingWebOrder_form:ready_',1)
  p_web.SetSessionValue('frmPendingWebOrder_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_frmPendingWebOrder',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('frmPendingWebOrder_form:ready_',1)
  p_web.SetSessionValue('frmPendingWebOrder_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('frmPendingWebOrder:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('frmPendingWebOrder_form:ready_',1)
  p_web.SetSessionValue('frmPendingWebOrder_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('frmPendingWebOrder:Primed',0)
  p_web.setsessionvalue('showtab_frmPendingWebOrder',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('frmPendingWebOrder_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('frmPendingWebOrder_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
    do ValidateValue::brwPendingWebOrder
    If loc:Invalid then exit.
    do ValidateValue::btnMakeTaggedOrders
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('frmPendingWebOrder:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('')

SaveSessionVars  ROUTINE
     p_web.SSV('Ans',Ans) ! LONG
RestoreSessionVars  ROUTINE
     Ans = p_web.GSV('Ans') ! LONG
frmStockReceive      PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' routines are called when the form _opens_
! the 'post' routines are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use the SessionQueue here
! if you want to carry information from the pre, to the post, stage.

! there are many stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locInvoiceNumber     STRING(30)                            !
FilesOpened     Long
STOCKRECEIVETMP::State  USHORT
SUBTRACC::State  USHORT
TRADEACC::State  USHORT
RETSALES::State  USHORT
locInvoiceNumber:IsInvalid  Long
btnRefresh:IsInvalid  Long
brwStockReceive:IsInvalid  Long
btnProcessTaggedItems:IsInvalid  Long
btnProcessExchangeItems:IsInvalid  Long
btnProcessLoanItems:IsInvalid  Long
btnPrintGRN:IsInvalid  Long
btnReprintGRN:IsInvalid  Long
loc:WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(JavascriptStringLen)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(1024)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:buttonset               String(64)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
loc:options                string(OptionsStringLen) ! options for jQuery calls
loc:popup                  long
loc:poppedup               long,static,thread
loc:ok                     long
loc:formheading            string(1024)
packet                     string(16384)
packetlen                  long
Loc:User            long
ThisSecwinAccess    string(252)
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('frmStockReceive')
  if p_stage = 0 and p_web.GetValue('_CallPopups') = 1 then p_stage = Net:Web:Popup. ! required for forms in DLL's, where PreCall doesn't know it's a form.
  loc:formname = 'frmStockReceive_frm'
  loc:popup = p_web.GetValue('_popup_')
  if p_stage = 0 and p_web.GetValue('_focus_') = 1 and p_Web.RequestAjax = 1
    p_stage = Net:Web:FocusBack
  end

  loc:WebStyle = p_web.site.WebFormStyle
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    p_web.FormReady('frmStockReceive','')
    p_web.DivHeader('frmStockReceive',p_web.combine(p_web.site.style.formdiv,))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web.DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
  orof Net:Web:Init + Net:InsertRecord
  orof Net:Web:Init + Net:ChangeRecord
  orof Net:Web:Init + Net:CopyRecord
  orof Net:Web:Init + Net:ViewRecord
  orof Net:Web:Init + Net:DeleteRecord
    do InitForm

  of Net:Web:FocusBack
    do GotFocusBack

  of net:web:popup
    if loc:poppedup = 0 and p_Web.RequestAjax = 0
      If p_web.GetPreCall('frmStockReceive') = 0
        p_web.AddPreCall('frmStockReceive')
        p_web.DivHeader('popup_frmStockReceive','nt-hidden')
        p_web.DivHeader('frmStockReceive',p_web.combine(p_web.site.style.formdiv,))
        p_web.DivFooter()
        p_web.DivFooter()
        loc:options = 'close: function(event, ui) {{ ntd.pop(); },autoOpen: false, width: '&clip(600)&', modal: true, position: [''center'',15]'
        loc:options = clip(loc:options) & ',addsec:""'
        packet = p_web.jQuery('#' & lower('popup_frmStockReceive_div'),'dialog',loc:options) & p_web.jQuery('#' & lower('popup_frmStockReceive_div'),'removeClass','''nt-hidden''')
        do SendPacket
      End
      do popups ! includes all the popups dependant on this procedure
      loc:poppedup = 1
    end

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of Net:InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferfrmStockReceive',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of Net:InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:InsertRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreInsert
    p_web.setsessionvalue('showtab_frmStockReceive',0)
    do CloseFiles
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmStockReceive',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:Populate
    do PreCopy
    p_web.setsessionvalue('showtab_frmStockReceive',0)
  of Net:ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmStockReceive',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_frmStockReceive',0)
  of Net:ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If false
    ElsIf loc:act = Net:InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of Net:ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate
  of Net:ChangeRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    do CloseFiles
    p_web.setsessionvalue('showtab_frmStockReceive',0)
  of Net:DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmStockReceive',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of Net:DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:ViewRecord + NET:WEB:Populate
    do OpenFiles
    do InitForm
    do PreUpdate
    p_web.setsessionvalue('showtab_frmStockReceive',0)
    do CloseFiles

  of Net:ViewRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmStockReceive',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_frmStockReceive',0)
  of Net:Web:NextTab
    do NextTab
  of Net:Web:Div
    do CallDiv
  Else
    ans = 0
  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
      p_web.requestfilename = p_web.formsettings.parentpage
      if p_web.GetValue('_parentPage') = ''
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_frmStockReceive',Loc:InvalidTab)
    If (p_stage = Net:Web:NextTab or p_stage = NET:WEB:StageValidate + Net:InsertRecord  or p_stage = NET:WEB:StageValidate + Net:ChangeRecord) and p_web.RequestAjax = 1 and loc:alert <> '' and p_web._popUpDone = 0
      p_web.Popup(loc:alert,net:Send)
    End
  ElsIf band(p_stage,NET:WEB:StageValidate) > 0 and band(p_stage,Net:DeleteRecord) <> Net:DeleteRecord and band(p_stage,Net:WriteMask) > 0 and p_web.RequestAjax = 1 and loc:popup
    p_web.script('jQuery(''#popup_'&lower('frmStockReceive')&'_div'').dialog(''close'');')
  End
  if loc:alert <> ''
    p_web.SetValue('alert',loc:Alert)
  end
  GlobalErrors.SetProcedureName()
  return Ans
ClearList           ROUTINE
    ClearStockReceiveList(p_web)
BuildList           ROUTINE
    BuildStockReceiveList(p_web,p_web.GSV('ret:Ref_Number'),p_web.GSV('ret:ExchangeOrder'),p_web.GSV('ret:LoanOrder'))
OpenFiles  ROUTINE
  p_web._OpenFile(STOCKRECEIVETMP)
  p_web._OpenFile(SUBTRACC)
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(RETSALES)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(STOCKRECEIVETMP)
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(RETSALES)
     FilesOpened = False
  END

GotFocusBack  routine
  DATA
loc:EipClm  string(255)
  CODE
  loc:EipClm = upper(p_web.GetValue('_EIPClm'))
  p_web.DeleteValue('_EIPClm')
  case loc:EipClm
  of ''
    case upper(p_web.GetValue('_calledfrom_'))
    of upper('brwStockReceive')
      do Value::brwStockReceive
    end
  end

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('frmStockReceive_form:inited_',1)
  p_web.formsettings.file = ''
  p_web.formsettings.key = ''
  do RestoreMem

SetFormSettings  routine
  If p_web.Formstate = ''
    p_web.formsettings.file = ''
    p_web.formsettings.key = ''
    p_web.formsettings.action = Net:ChangeRecord
    clear(p_web.formsettings.recordid)
    clear(p_web.formsettings.FieldName)
    If p_web.GetValue('_parentPage') <> ''
      p_web.formsettings.parentpage = p_web.GetValue('_parentPage')
    else
      p_web.formsettings.parentpage = 'frmStockReceive'
    end
    p_web.formsettings.proc = 'frmStockReceive'
    clear(p_web.formsettings.target)
    p_web.FormState = p_web.AddSettings()
  end

CancelForm  Routine

SendAlert Routine
  p_web.Message('Alert',loc:alert,p_web.site.MessageClass,Net:Send)

SetPics        Routine

AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  if p_web.IfExistsValue('locInvoiceNumber') = 0
    p_web.SetSessionValue('locInvoiceNumber',locInvoiceNumber)
  Else
    locInvoiceNumber = p_web.GetSessionValue('locInvoiceNumber')
  End

! RestoreMem primes all the non-file fields with their session value. Useful in Validate and PostAction routines
RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locInvoiceNumber')
    locInvoiceNumber = p_web.GetValue('locInvoiceNumber')
    p_web.SetSessionValue('locInvoiceNumber',locInvoiceNumber)
  Else
    locInvoiceNumber = p_web.GetSessionValue('locInvoiceNumber')
  End

SetAction  routine
  data
  code
  If Band(p_Stage,Net:ViewRecord) <>  Net:ViewRecord
    Case p_web.GetSessionValue('frmStockReceive_CurrentAction')
    of Net:InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = Net:InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of Net:ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = Net:ChangeRecord
    of Net:DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = Net:DeleteRecord
    Else
      loc:action = ''
      loc:act = 0
    End
  Else
    Loc:ViewOnly = 1
    loc:action = p_web.site.ViewPromptText
    loc:act = Net:ViewRecord
  End

SetFormAction  routine
  data
  code
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'FormBrowseStock'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('frmStockReceive_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('frmStockReceive_ChainTo')
    loc:formaction = p_web.GetSessionValue('frmStockReceive_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction

GenerateForm   Routine
  do LoadRelatedRecords
      p_web.site.SaveButton.TextValue = 'Close'
 locInvoiceNumber = p_web.RestoreValue('locInvoiceNumber')
  do SetFormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & p_web.CreateInput('hidden','LookupField',p_web.GetValue('LookupField')) & '<13,10>'
  end
  packet = clip(packet) & p_web.CreateInput('hidden','FormState',p_web.FormState) & '<13,10>'
  If p_web.Translate('Stock Receive Procedure') <> ''
    packet = clip(packet) & '<div class="'&p_web.combine(p_web.site.style.formSubHeading,)&'">'&p_web.Translate('Stock Receive Procedure',0)&'</div>'&CRLF
  End
  do SendPacket
    Packet = clip(Packet) & p_web.DivHeader('Tab_frmStockReceive',p_web.combine(p_web.site.style.FormTabOuter,),Net:NoSend)
    Case loc:WebStyle
    of Net:Web:Tab
      Packet = clip(Packet) & '<ul class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'">'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_frmStockReceive0_div')&'">'&p_web.Translate('Enter Invoice Number')&'</a></li>'& CRLF
      Packet = clip(Packet) & '<li><a href="#'&lower('tab_frmStockReceive1_div')&'">'&p_web.Translate('Stock On Invoice')&'</a></li>'& CRLF
      Packet = clip(Packet) & '</ul>'& CRLF
    end
    do SendPacket
  do GenerateTab0
  do GenerateTab1
  Packet = clip(Packet) & p_web.DivFooter(Net:NoSend) !'</div>'&CRLF
  do SendPacket
    if loc:ViewOnly = 0
      packet = clip(packet) & '<div id="frmStockReceive_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      If loc:WebStyle = Net:Web:Wizard
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizPreviousButton,loc:formname,,,loc:javascript)
        packet = clip(packet) & p_web.CreateStdButton('button',NET:WEB:WizNextButton,loc:formname,,,loc:javascript)
      End
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('frmStockReceive_brwStockReceive_embedded_div')&''');'
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.save(event);'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:SaveButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
      end
      packet = clip(packet) & '</div><13,10>' ! end id="frmStockReceive_saveset"
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'frmStockReceive_saveset','buttonset',loc:options)
      End
    Else
      packet = clip(packet) & '<div id="frmStockReceive_saveset" class="'&p_web.combine(p_web.site.style.FormSaveButtonSet,)&'">'
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('frmStockReceive_brwStockReceive_embedded_div')&''');'
      if loc:popup
        loc:javascript = clip(loc:javascript) & 'ntd.close();'
        packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:CloseButton,loc:formname,,,loc:javascript)
      else
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
      end
      packet = clip(packet) & '</div><13,10>'
      If p_web.site.UseSaveButtonSet
        loc:options = ''
        packet = clip(packet) & p_web.jQuery('#' & 'frmStockReceive_saveset','buttonset',loc:options)
      End
    End
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locInvoiceNumber')
    End
  End
    Case loc:WebStyle
    of Net:Web:Accordion
      loc:options = 'active:' & choose(p_web.GSV('showtab_frmStockReceive')>0,p_web.GSV('showtab_frmStockReceive'),'0') & ',' &|
                    'change: function(event, ui) {{ SetSessionValue(''showtab_frmStockReceive'',jQuery(this).find("h3").index(ui.newHeader[0])); }'
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_frmStockReceive') & '_div','accordion',loc:options)
    of Net:Web:Tab
      loc:options = 'select: function(event,ui){{SetSessionValue(''showtab_frmStockReceive'',ui.index);},<13,10> selected:' & choose(p_web.GSV('showtab_frmStockReceive')>0,p_web.GSV('showtab_frmStockReceive'),'0')
      Packet = clip(Packet) & p_web.jQuery('#' & lower('Tab_frmStockReceive') & '_div','tabs',loc:options)
    of Net:Web:TabXP
      loc:tabs = ''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Enter Invoice Number') & ''''
        if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
        loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Stock On Invoice') & ''''
      Packet = clip(Packet) &|
      '<script type="text/javascript">initTabs('''&lower('tab_frmStockReceive_div')&''',Array('&clip(loc:tabs)&'),'&choose(p_web.GSV('showtab_frmStockReceive')>0,p_web.GSV('showtab_frmStockReceive'),'0')&',"100%","");</script>' ! IE can't deal with a width of ""....
    of Net:Web:Wizard
       loc:options =  'procedure:"frmStockReceive",' &|
                      'activeTab:' & choose(p_web.GSV('showtab_frmStockReceive')>0,p_web.GSV('showtab_frmStockReceive'),0)
       Packet = clip(Packet) & p_web.jQuery('#' & lower('tab_frmStockReceive_div'),'ntwiz',loc:options)
    end
    do SendPacket
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  loc:options = 'procedure: "'& lower('frmStockReceive') &'", tabType:' & loc:WebStyle
  packet= clip(packet) & p_web.Jquery('#' & clip(loc:formname) ,'ntform',loc:options)
  do SendPacket
  do Popups
  if p_web.RequestAjax then do AutoLookups.

Popups Routine
  If p_web.RequestAjax = 0
    do AutoLookups
    p_web.AddPreCall('frmStockReceive')
    p_web.SetValue('_CallPopups',2)
    If p_web.GetPreCall('brwStockReceive') = 0
      p_web.SetValue('brwStockReceive:NoForm',1)
      p_web.SetValue('brwStockReceive:FormName',loc:formname)
      p_web.SetValue('brwStockReceive:parentIs','Form')
      p_web.SetValue('_parentProc','frmStockReceive')
      brwStockReceive(p_web)
      p_web.SetValue('_CallPopups',0)
      p_web.DeleteValue('brwStockReceive:NoForm')
      p_web.DeleteValue('brwStockReceive:FormName')
      p_web.DeleteValue('brwStockReceive:parentIs')
      p_web.DeleteValue('_parentProc')
    End
    p_web.SetValue('_popup_',0)
  End
AutoLookups  Routine
GenerateTab0  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Enter Invoice Number')&'</a></h3>' & CRLF & p_web.DivHeader('tab_frmStockReceive0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_frmStockReceive0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_frmStockReceive0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_frmStockReceive0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Enter Invoice Number')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_frmStockReceive0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Enter Invoice Number')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_frmStockReceive0',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Enter Invoice Number')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_frmStockReceive0',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Prompt::locInvoiceNumber
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::locInvoiceNumber
        do Comment::locInvoiceNumber
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td'&clip(loc:width)&'>'
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        If loc:FirstInCell = 1
          packet = clip(packet) & '</td>'&CRLF ! Field Heading
          loc:cellstarted = 0
          do SendPacket
        End
        if loc:cellstarted = 0
          loc:width = ''
          packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
          loc:columncounter += 1
          do SendPacket
          loc:cellstarted = 1
        end
        do Value::btnRefresh
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        loc:width = ''
        packet = clip(packet) & '<td'&clip(loc:width)&'>'
        loc:columncounter += 1
        do SendPacket
        do Comment::btnRefresh
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket
GenerateTab1  Routine
      Case loc:WebStyle
      of Net:Web:Accordion
        Packet = clip(Packet) & '<h3 class="'&p_web.combine(p_web.site.style.FormTabTitle,)&'"><a href="#">'&p_web.Translate('Stock On Invoice')&'</a></h3>' & CRLF & p_web.DivHeader('tab_frmStockReceive1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:Tab
        Packet = clip(Packet) & p_web.DivHeader('tab_frmStockReceive1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      of Net:Web:TabXP
        Packet = clip(Packet) & p_web.DivHeader('tab_frmStockReceive1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' dhtmlgoodies_aTab',,),Net:NoSend)
      of Net:Web:Wizard
        Packet = clip(Packet) & p_web.DivHeader('tab_frmStockReceive1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-wizard',,),Net:NoSend,'Stock On Invoice')
      of Net:Web:Rounded
        Packet = clip(Packet) & p_web.DivHeader('tab_frmStockReceive1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-rounded',,),Net:NoSend) & '<div class="ui-state-default nt-rounded-header ui-corner-all">' & p_web.Translate('Stock On Invoice')&'</div>'& CRLF
      of Net:Web:Plain
        Packet = clip(Packet) & p_web.DivHeader('tab_frmStockReceive1',p_web.combine(clip(p_web.site.style.FormTabInner) & ' nt-plain',,),Net:NoSend) & '<fieldset class="ui-tabs ui-widget ui-widget-content ui-corner-all plain"><legend>' & p_web.Translate('Stock On Invoice')&'</legend>' & CRLF
      of Net:Web:None
        Packet = clip(Packet) & p_web.DivHeader('tab_frmStockReceive1',p_web.combine(p_web.site.style.FormTabInner,,),Net:NoSend)
      end
      do SendPacket
      packet = clip(packet) & '<table class="'&p_web.combine(p_web.site.style.formtable,)&'">'&CRLF
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::brwStockReceive
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::btnProcessTaggedItems
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::btnProcessExchangeItems
        do Comment::btnProcessExchangeItems
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::btnProcessLoanItems
        do Comment::btnProcessLoanItems
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::btnPrintGRN
        do Comment::btnPrintGRN
      do SendPacket
        if loc:rowstarted = 0
          packet = clip(packet) & '<tr>'&CRLF
          if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
          loc:columncounter = 0
          loc:rowstarted = 1
        end
        do SendPacket
        loc:width = ''
        If loc:cellstarted = 0
          packet = clip(packet) & '<td>'&CRLF
          do SendPacket
          loc:cellstarted = 1
          loc:FirstInCell = 1
        Else
          loc:FirstInCell = 0
        End
        do Value::btnReprintGRN
        do Comment::btnReprintGRN
        packet = clip(packet) & '</td>'&CRLF
        loc:cellstarted = 0
        do SendPacket
        if loc:cellstarted
          packet = clip(packet) & '</td></tr>'&CRLF
          loc:cellstarted = 0
        Else
          packet = clip(packet) & '</tr>'&CRLF
        End
        loc:rowstarted = 0
        loc:cellstarted = 0
      do SendPacket
      if loc:rowstarted and loc:cellstarted
        packet = clip(packet) & '</td></tr></table>'&CRLF
        loc:cellstarted = 0
        loc:rowstarted = 0
      elsif loc:rowstarted
        packet = clip(packet) & '</tr></table>'&CRLF
        loc:rowstarted = 0
      else
        packet = clip(packet) & '</table>'&CRLF
      end
      do SendPacket
      Case loc:WebStyle
      of Net:Web:Plain
        Packet = clip(Packet) & '</fieldset>' & p_web.DivFooter(Net:NoSend)
      else
        Packet = clip(Packet) & p_web.DivFooter(Net:NoSend)
      end
      do SendPacket


Prompt::locInvoiceNumber  Routine
  packet = clip(packet) & p_web.DivHeader('frmStockReceive_' & p_web._nocolon('locInvoiceNumber') & '_prompt',Choose(1=0,'nt-hidden',p_web.combine(p_web.site.style.formprompt,,)),Net:NoSend)
  loc:prompt = Choose(1=0,'',p_web.Translate('Invoice Number'))
  packet = clip(packet) & loc:prompt
  packet = clip(packet) & p_web.DivFooter(Net:NoSend)
  do SendPacket

Validate::locInvoiceNumber  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
    locInvoiceNumber = p_web.GetValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
    locInvoiceNumber = p_web.GetValue('Value')
  End
  do ValidateValue::locInvoiceNumber  ! copies value to session value if valid.
  DO ClearList
  p_web.SSV('Comment:InvoiceNumber','')
  Access:RETSALES.ClearKey(ret:Invoice_Number_Key)
  ret:Invoice_Number = p_web.GSV('locInvoiceNumber')
  If Access:RETSALES.TryFetch(ret:Invoice_Number_Key) = Level:Benign
      !Found
      !Does the account number of the sale match either the account number
      !of the current user, or it's Stores Account?
  
      IF (ret:Account_Number <> p_web.GSV('BookingStoresAccount'))
          p_web.SSV('Comment:InvoiceNumber','Invoice Is Not For Your Site')
      ELSE
          p_web.FileToSessionQueue(RETSALES)
          DO BuildList
      END
  Else!If Access:RETSALES.TryFetch(ret:Invoice_Number_Key) = Level:Benign
      !Error
      p_web.SSV('Comment:InvoiceNumber','Invalid Invoice Number')
  End!If Access:RETSALES.TryFetch(ret:Invoice_Number_Key) = Level:Benign
  do Value::locInvoiceNumber
  do SendAlert
  do Comment::locInvoiceNumber ! allows comment style to be updated.
  do Value::brwStockReceive  !1
  do Value::btnPrintGRN  !1
  do Value::btnProcessExchangeItems  !1
  do Value::btnProcessTaggedItems  !1
  do Value::btnProcessLoanItems  !1

ValidateValue::locInvoiceNumber  Routine
    If not (1=0)
      if loc:invalid = '' then p_web.SetSessionValue('locInvoiceNumber',locInvoiceNumber).
    End

Value::locInvoiceNumber  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.formentrydiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('frmStockReceive_' & p_web._nocolon('locInvoiceNumber') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  If loc:viewonly
    loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryReadOnly,)
  End
  If loc:retrying
    locInvoiceNumber = p_web.RestoreValue('locInvoiceNumber')
    do ValidateValue::locInvoiceNumber
    If locInvoiceNumber:IsInvalid then loc:fieldclass = clip(loc:fieldclass) & ' ' & p_web.combine(p_web.site.style.formEntryError,).
  End
  loc:extra = ''
  If Not (1=0)
  ! --- STRING --- locInvoiceNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locInvoiceNumber'',''frmstockreceive_locinvoicenumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locInvoiceNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locInvoiceNumber',p_web.GetSessionValueFormat('locInvoiceNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,,,) & '<13,10>'
  do SendPacket
  End
  p_web.DivFooter()
Comment::locInvoiceNumber  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if locInvoiceNumber:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = p_web.Translate(p_web.GSV('Comment:InvoiceNumber'))
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('frmStockReceive_' & p_web._nocolon('locInvoiceNumber') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::btnRefresh  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::btnRefresh  ! copies value to session value if valid.
  DO ClearList
  p_web.SSV('Comment:InvoiceNumber','')
  Access:RETSALES.ClearKey(ret:Invoice_Number_Key)
  ret:Invoice_Number = p_web.GSV('locInvoiceNumber')
  If Access:RETSALES.TryFetch(ret:Invoice_Number_Key) = Level:Benign
      !Found
      !Does the account number of the sale match either the account number
      !of the current user, or it's Stores Account?
  
      IF (ret:Account_Number <> p_web.GSV('BookingStoresAccount'))
          p_web.SSV('Comment:InvoiceNumber','Invoice Is Not For Your Site')
      ELSE
          p_web.FileToSessionQueue(RETSALES)
          DO BuildList
      END
  Else!If Access:RETSALES.TryFetch(ret:Invoice_Number_Key) = Level:Benign
      !Error
      p_web.SSV('Comment:InvoiceNumber','Invalid Invoice Number')
  End!If Access:RETSALES.TryFetch(ret:Invoice_Number_Key) = Level:Benign
  do Value::btnRefresh
  do Comment::btnRefresh ! allows comment style to be updated.
  do Value::brwStockReceive  !1
  do Value::btnPrintGRN  !1
  do Value::btnProcessExchangeItems  !1
  do Value::btnProcessTaggedItems  !1
  do Value::btnProcessLoanItems  !1

ValidateValue::btnRefresh  Routine
    If not (1=0)
    End

Value::btnRefresh  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('frmStockReceive_' & p_web._nocolon('btnRefresh') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnRefresh'',''frmstockreceive_btnrefresh_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','btnRefresh','Refresh',p_web.combine(Choose('Refresh' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'SmallButton'),loc:formname,,,,loc:javascript,loc:disabled,,,,,,0,,,,'nt-left')!h
  do SendPacket
  End
  p_web.DivFooter()
Comment::btnRefresh  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if btnRefresh:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('frmStockReceive_' & p_web._nocolon('btnRefresh') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::brwStockReceive  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  Elsif true
    p_web.StoreValue('stotmp:RecordNumber')
  End
  do ValidateValue::brwStockReceive  ! copies value to session value if valid.
  do Comment::brwStockReceive ! allows comment style to be updated.

ValidateValue::brwStockReceive  Routine
    If not (1=0)
    End

Value::brwStockReceive  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(1=0,1,0))
  ! --- BROWSE ---  brwStockReceive --
  p_web.SetValue('brwStockReceive:NoForm',1)
  p_web.SetValue('brwStockReceive:FormName',loc:formname)
  p_web.SetValue('brwStockReceive:parentIs','Form')
  p_web.SetValue('_parentProc','frmStockReceive')
  if p_web.RequestAjax = 0
    p_web.SSV('frmStockReceive:_popup_',p_web.GetValue('_popup_')) ! stores the current browse popup state
    packet = clip(packet) & '<div id="'&lower('frmStockReceive_brwStockReceive_embedded_div')&'"><!-- Net:brwStockReceive --></div><13,10>'
    do SendPacket
    p_web.DivHeader('frmStockReceive_' & lower('brwStockReceive') & '_value')
    p_web.DivFooter()
  else
    if p_web.GetValue('_popup_') = 1
      p_web.SSV('frmStockReceive:_popup_',1)
    elsif p_web.GSV('frmStockReceive:_popup_') = 1
      p_web.SetValue('_popup_',1)
    end
    packet = clip(packet) & '<!-- Net:brwStockReceive --><13,10>'
  end
  do SendPacket
Comment::brwStockReceive  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if brwStockReceive:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('frmStockReceive_' & p_web._nocolon('brwStockReceive') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::btnProcessTaggedItems  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::btnProcessTaggedItems  ! copies value to session value if valid.

ValidateValue::btnProcessTaggedItems  Routine
    If not (p_web.GSV('locINvoiceNumber') = '')
    End

Value::btnProcessTaggedItems  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('locINvoiceNumber') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('frmStockReceive_' & p_web._nocolon('btnProcessTaggedItems') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('locINvoiceNumber') = '')
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','btnProcessTaggedItems','Process Tagged Stock Items',p_web.combine(Choose('Process Tagged Stock Items' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.WindowOpen('frmStockReceiveProcess'&'','_self'),,loc:disabled,,,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::btnProcessTaggedItems  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if btnProcessTaggedItems:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('locINvoiceNumber') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('frmStockReceive_' & p_web._nocolon('btnProcessTaggedItems') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('locINvoiceNumber') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::btnProcessExchangeItems  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::btnProcessExchangeItems  ! copies value to session value if valid.
  do Comment::btnProcessExchangeItems ! allows comment style to be updated.

ValidateValue::btnProcessExchangeItems  Routine
    If not (p_web.GSV('locINvoiceNumber') = '')
    End

Value::btnProcessExchangeItems  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('locINvoiceNumber') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('frmStockReceive_' & p_web._nocolon('btnProcessExchangeItems') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('locINvoiceNumber') = '')
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','btnProcessExchangeItems','Process Tagged Exchange Items',p_web.combine(Choose('Process Tagged Exchange Items' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.WindowOpen('frmExchangeReceiveProcess&' & p_web._jsok('fLoan=0') &''&'','_self'),,loc:disabled,,,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::btnProcessExchangeItems  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if btnProcessExchangeItems:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('locINvoiceNumber') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('frmStockReceive_' & p_web._nocolon('btnProcessExchangeItems') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('locINvoiceNumber') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::btnProcessLoanItems  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::btnProcessLoanItems  ! copies value to session value if valid.
  do Comment::btnProcessLoanItems ! allows comment style to be updated.

ValidateValue::btnProcessLoanItems  Routine
    If not (p_web.GSV('locINvoiceNumber') = '')
    End

Value::btnProcessLoanItems  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('locINvoiceNumber') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('frmStockReceive_' & p_web._nocolon('btnProcessLoanItems') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('locINvoiceNumber') = '')
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','btnProcessLoanItems','Process Loan Items',p_web.combine(Choose('Process Loan Items' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.WindowOpen('frmExchangeReceiveProcess&' & p_web._jsok('fLoan=1') &''&'','_self'),,loc:disabled,,,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::btnProcessLoanItems  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if btnProcessLoanItems:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('locINvoiceNumber') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('frmStockReceive_' & p_web._nocolon('btnProcessLoanItems') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('locINvoiceNumber') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::btnPrintGRN  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::btnPrintGRN  ! copies value to session value if valid.
  do Comment::btnPrintGRN ! allows comment style to be updated.

ValidateValue::btnPrintGRN  Routine
    If not (p_web.GSV('locINvoiceNumber') = '')
    End

Value::btnPrintGRN  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(p_web.GSV('locINvoiceNumber') = '','nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('frmStockReceive_' & p_web._nocolon('btnPrintGRN') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (p_web.GSV('locINvoiceNumber') = '')
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','btnPrintGRN','Print GRN',p_web.combine(Choose('Print GRN' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.WindowOpen('frmPrintGRN'&'','_self'),,loc:disabled,,,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::btnPrintGRN  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if btnPrintGRN:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(p_web.GSV('locINvoiceNumber') = '','nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('frmStockReceive_' & p_web._nocolon('btnPrintGRN') & '_comment',loc:class,Net:NoSend)
  If p_web.GSV('locINvoiceNumber') = ''
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

Validate::btnReprintGRN  Routine
  If p_web.RequestAjax = 1 and p_web.ifExistsValue('NewValue')
  ElsIf p_web.IfExistsValue('Value') !FormFieldPicture =   !FieldPicture = 
  End
  do ValidateValue::btnReprintGRN  ! copies value to session value if valid.
  do Comment::btnReprintGRN ! allows comment style to be updated.

ValidateValue::btnReprintGRN  Routine
    If not (1=0)
    End

Value::btnReprintGRN  Routine
  data
loc:fieldclass string(1024)
loc:disabled   long
  code
  loc:fieldclass = p_web.Combine(p_web.site.style.FormButtonDiv,,)
  loc:fieldclass = choose(1=0,'nt-hidden ' & loc:fieldclass,loc:fieldclass)
  p_web.DivHeader('frmStockReceive_' & p_web._nocolon('btnReprintGRN') & '_value',loc:fieldclass)
  loc:fieldclass = ''
  loc:fieldclass = p_web.combine(p_web.site.style.formentry,,)
  loc:extra = ''
  If Not (1=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateButton('button','btnReprintGRN','Reprint GRN',p_web.combine(Choose('Reprint GRN' <> '',p_web.site.style.FormOtherButtonWithText,p_web.site.style.FormOtherButtonWithOutText),'DoubleButton'),loc:formname,,,p_web.WindowOpen('frmPrintAllGRN'&'','_self'),,loc:disabled,,,,,,,,,,'nt-left') !e
  do SendPacket
  End
  p_web.DivFooter()
Comment::btnReprintGRN  Routine
  data
loc:class  string(255)
  code
  loc:class = p_web.Combine(p_web.site.style.formcomment,,)
  if btnReprintGRN:IsInvalid
    loc:class = clip(loc:class) & ' ' & p_web.site.style.FormCommentError
  end
  loc:comment = ''
  loc:class = Choose(1=0,'nt-hidden ' & loc:class,loc:class)
  packet = clip(packet) & p_web.DivHeader('frmStockReceive_' & p_web._nocolon('btnReprintGRN') & '_comment',loc:class,Net:NoSend)
  If 1=0
    loc:comment = ''
  End
  packet = clip(packet) & clip(loc:comment) & p_web.DivFooter(net:nosend)
  do SendPacket

NextTab    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('frmStockReceive_nexttab_' & 0)
    locInvoiceNumber = p_web.GSV('locInvoiceNumber')
    do ValidateValue::locInvoiceNumber
    If loc:Invalid
      loc:retrying = 1
      do Value::locInvoiceNumber
      !do SendAlert
      do Comment::locInvoiceNumber ! allows comment style to be updated.
      !exit
    End
    If loc:Invalid then exit.
  of lower('frmStockReceive_nexttab_' & 1)
    If loc:Invalid then exit.
  End
  p_web.Script(p_web.jQuery('#' & lower('tab_frmStockReceive_div'),'ntwiz','"next"'))

CallDiv    routine
  data
  code
  p_web.RequestAjax = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)

  case lower(p_web.PageName)
  of lower('frmStockReceive_tab_' & 0)
    do GenerateTab0
  of lower('frmStockReceive_locInvoiceNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locInvoiceNumber
      of event:timer
        do Value::locInvoiceNumber
        do Comment::locInvoiceNumber
      else
        do Value::locInvoiceNumber
      end
  of lower('frmStockReceive_btnRefresh_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnRefresh
      of event:timer
        do Value::btnRefresh
        do Comment::btnRefresh
      else
        do Value::btnRefresh
      end
  of lower('frmStockReceive_tab_' & 1)
    do GenerateTab1
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end
! NET:WEB:StagePRE

PreInsert       Routine
  p_web.SetValue('frmStockReceive_form:ready_',1)

  p_web.SetSessionValue('frmStockReceive_CurrentAction',Net:InsertRecord)
  p_web.setsessionvalue('showtab_frmStockReceive',0)
  do SetFormSettings

PreCopy  Routine
  p_web.SetValue('frmStockReceive_form:ready_',1)
  p_web.SetSessionValue('frmStockReceive_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_frmStockReceive',0)
  ! here we need to copy the non-unique fields across
  do SetFormSettings

PreUpdate       Routine
  data
loc:offset      Long
  code
  p_web.SetValue('frmStockReceive_form:ready_',1)
  p_web.SetSessionValue('frmStockReceive_CurrentAction',Net:ChangeRecord)
  p_web.SetSessionValue('frmStockReceive:Primed',0)
  do SetFormSettings

PreDelete       Routine
  p_web.SetValue('frmStockReceive_form:ready_',1)
  p_web.SetSessionValue('frmStockReceive_CurrentAction',Net:DeleteRecord)
  p_web.SetSessionValue('frmStockReceive:Primed',0)
  p_web.setsessionvalue('showtab_frmStockReceive',0)
  do SetFormSettings

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

! copies fields from the Value queue to the File Field. Used to be done in PrimeRecord, but now that only
! primes the fields from the session queue.
CompleteForm       Routine
  data
loc:pic   string(40)
  code
  do SetPics
      If not (1=0)
          If p_web.IfExistsValue('locInvoiceNumber')
            locInvoiceNumber = p_web.GetValue('locInvoiceNumber')
          End
      End


! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteForm
  do ValidateRecord

ValidateCopy  Routine
  do CompleteForm
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteForm
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('frmStockReceive_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('frmStockReceive_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
    do ValidateValue::locInvoiceNumber
    If loc:Invalid then exit.
    do ValidateValue::btnRefresh
    If loc:Invalid then exit.
  ! tab = 2
    loc:InvalidTab += 1
    do ValidateValue::brwStockReceive
    If loc:Invalid then exit.
    do ValidateValue::btnProcessTaggedItems
    If loc:Invalid then exit.
    do ValidateValue::btnProcessExchangeItems
    If loc:Invalid then exit.
    do ValidateValue::btnProcessLoanItems
    If loc:Invalid then exit.
    do ValidateValue::btnPrintGRN
    If loc:Invalid then exit.
    do ValidateValue::btnReprintGRN
    If loc:Invalid then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostUpdate      Routine
  p_web.SetSessionValue('frmStockReceive:Primed',0)
  p_web.StoreValue('locInvoiceNumber')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')

SaveSessionVars  ROUTINE
     p_web.SSV('Ans',Ans) ! LONG
     p_web.SSV('locInvoiceNumber',locInvoiceNumber) ! STRING(30)
RestoreSessionVars  ROUTINE
     Ans = p_web.GSV('Ans') ! LONG
     locInvoiceNumber = p_web.GSV('locInvoiceNumber') ! STRING(30)
PageProcess          PROCEDURE  (NetWebServerWorker p_web)
loc:x          Long
packet         String(NET:MaxBinData)
packetlen      Long
CRLF           String('<13,10>')
NBSP           String('&#160;')
loc:options    String(OptionsStringLen)  ! options for jQuery calls
Loc:User            long
ThisSecwinAccess    string(252)

  CODE
  GlobalErrors.SetProcedureName('PageProcess')
  p_web.SetValue('_parentPage','PageProcess')
  p_web.publicpage = 1
  if p_web.sessionId = 0 then p_web.NewSession().
        p_web.StoreValue('ProcessType')
        p_web.StoreValue('ReturnURL')
        p_web.StoreValue('RedirectURL')
  do Header
  packet = clip(packet) & p_web.BodyOnLoad(p_web.Combine(p_web.site.bodyclass,),,p_web.Combine(p_web.site.bodydivclass,))
    do SendPacket
    Do heading
    do SendPacket
    Do ShowWait
    do SendPacket
    Do CloseWait
    do SendPacket
  do Footer
  packet = clip(packet) & p_web.Popup()
  do SendPacket
  GlobalErrors.SetProcedureName()
  Return

BuildOutstandingOrders      ROUTINE
DATA
locAccountNumber    STRING(30)
bt  LONG()
locRecordcount      LONG()
locTotalRecords LONG()
kReturnURL  EQUATE('FormBrowseStock')
CODE
    p_web.SSV('locOrderType','S')
    p_web.SSV('RedirectURL','FormBrowseOutstandingParts')

    STREAM(SBO_OutParts)
    STREAM(ORDHEAD)
    STREAM(ORDITEMS)
    STREAM(RETSTOCK)
    STREAM(RETSALES)
    STREAM(EXCHORDR)
    STREAM(LOAORDR)

    bt=clock()

    UpdateProgress(p_web,0,kReturnURL)

    locTotalRecords = RECORDS(SBO_OutParts)

    ! Clear Existing Record
    Access:SBO_OutParts.ClearKey(sout:PartNumberKey)
    sout:SessionID = p_web.SessionID
    SET(sout:PartNumberKey,sout:PartNumberKey)
    LOOP UNTIL Access:SBO_OutParts.Next()
        IF (sout:SessionID <> p_web.SessionID)
            BREAK
        END

        locRecordcount += 1


        if (clock() - bt) > 200
            bt=clock()

            UpdateProgress(p_web,INT((locRecordcount / locTotalRecords) * 100),kReturnURL,'Preparing...')

        end

        Delete(SBO_OutParts)
    END

    locRecordcount = 0

    IF (p_web.GSV('BookingStoresAccount') = '')
        locAccountNumber = p_web.GSV('BookingAccount')
    ELSE
        locAccountNumber = p_web.GSV('BookingStoresAccount')
    END


    ! Loop Through Web Orders For This Account
    Access:ORDHEAD.Clearkey(orh:AccountDateKey)
    orh:Account_No = locAccountNumber
    Set(orh:AccountDateKey,orh:AccountDateKey)
    Loop
        If Access:ORDHEAD.Next()
            Break
        End ! If Access:ORDHEAD.Next()
        If orh:Account_No <> locAccountNumber
            Break
        End ! If orh:Account_No <> locAccountNumber

        locRecordcount += 1


        if (clock() - bt) > 200
            bt=clock()

            UpdateProgress(p_web,INT((locRecordcount / RECORDS(ORDHEAD)) * 100),kReturnURL,'Checking Part Orders')

        end


    ! Count unprocessed orders first (DBH: 06/12/2007)
        If orh:Procesed = 0
        ! Add up items on the unprocessed order (DBH: 06/12/2007)
            Access:ORDITEMS.Clearkey(ori:KeyOrdHNo)
            ori:OrdHNo = orh:Order_No
            Set(ori:KeyOrdHNo,ori:KeyOrdHNo)
            Loop
                If Access:ORDITEMS.Next()
                    Break
                End ! If Access:ORDITEMS.Next()
                If ori:OrdHNo <> orh:Order_No
                    Break
                End ! If ori:OrdHNo <> orh:Order_No
                If ori:Qty = 0
                    Cycle
                End ! If ori:Qty = 0

                Access:SBO_OutParts.ClearKey(sout:PartDescDateRaisedKey)
                sout:SessionID = p_web.SessionID
                sout:LineType = 'S'
                sout:PartNumber = ori:PartNo
                sout:Description = ori:partdiscription
                sout:DateRaised = orh:TheDate
                IF (Access:SBO_OutParts.TryFetch(sout:PartDescDateRaisedKey))
                    IF (Access:SBO_OutParts.PrimeRecord() = Level:Benign)
                        sout:SessionID = p_web.SessionID
                        sout:PartNumber = ori:PartNo
                        sout:Description = ori:partdiscription
                        sout:DateRaised = orh:TheDate
                        sout:Quantity = ori:Qty
                        sout:DateProcessed = 0
                        sout:LineType = 'S'
                        IF (Access:SBO_OutParts.TryInsert())
                        END

                    END

                ELSE
                    sout:Quantity += ori:Qty
                    Access:SBO_OutParts.TryUpdate()
                END
            End ! Loop

        Else ! If orh:Procesed = 0
        ! Find the retail sale associated with this web order (DBH: 06/12/2007)
            Access:RETSALES.Clearkey(ret:Ref_Number_Key)
            ret:Ref_Number = orh:SalesNumber
            If Access:RETSALES.TryFetch(ret:Ref_Number_Key) = Level:Benign
            ! Double check the the sale has the corresponding web order number (DBH: 06/12/2007)
                If ret:WebOrderNumber = 0
                    Cycle
                End ! If ret:WebOrderNumber = 0

            ! Count the items on the sale (DBH: 06/12/2007)
                Access:RETSTOCK.Clearkey(res:Part_Number_Key)
                res:Ref_Number = ret:Ref_Number
                Set(res:Part_Number_Key,res:Part_Number_Key)
                Loop
                    If Access:RETSTOCK.Next()
                        Break
                    End ! If Access:RETSTOCK.Next()
                    If res:Ref_Number <> ret:Ref_Number
                        Break
                    End ! If res:Ref_Number <> ret:Ref_Number
                ! Item should be on another sale, so don't count here. (DBH: 06/12/2007)
                    If res:Despatched = 'OLD'
                        Cycle
                    End ! If res:Despatched = 'OLD'

                    If res:Despatched = 'CAN'
                        CYCLE
                    END

                ! Count the part if it hasn't arrived yet (DBH: 06/12/2007)
                    Access:SBO_OutParts.ClearKey(sout:PartDescDateRaisedProcessedKey)
                    sout:SessionID = p_web.SessionID
                    sout:LineType = 'S'
                    sout:PartNumber = res:Part_Number
                    sout:Description = res:Description
                    sout:DateRaised = orh:TheDate
                    sout:DateProcessed = orh:Pro_Date
                    IF (Access:SBO_OutParts.TryFetch(sout:PartDescDateRaisedProcessedKey))
                        IF (res:Received = 0)
                            IF (Access:SBO_OutParts.PrimeRecord() = Level:Benign)
                                sout:SessionID = p_web.SessionID
                                sout:PartNumber = res:Part_Number
                                sout:Description = res:Description
                                sout:DateRaised = orh:TheDate
                                sout:DateProcessed = orh:Pro_date

                                sout:Quantity = res:Quantity


                                sout:LineType = 'S'
                                IF (Access:SBO_OutParts.TryInsert())
                                END

                            END
                        END

                    ELSE
                        IF (res:Received = 0)
                            sout:Quantity += res:Quantity
                            Access:SBO_OutParts.TryUpdate()
                        END

                    END

                End ! Loop
            End ! If Access:RETSALES.TryFetch(ret:Ref_Number_Key) = Level:Benign
        End ! If orh:Procesed = 0
    End ! Loop

    locRecordcount = 0

! Load exchange queue
    Access:EXCHORDR.ClearKey(exo:Ref_Number_Key)
    exo:Ref_Number = 1
    set(exo:Ref_Number_Key,exo:Ref_Number_Key)
    loop until Access:EXCHORDR.Next()

        locRecordcount += 1

        if (clock() - bt) > 200
            bt=clock()

            UpdateProgress(p_web,INT((locRecordcount / RECORDS(EXCHORDR)) * 100),kReturnURL,'Checking Exchange Orders')

        end

        if exo:Qty_Received < exo:Qty_Required              ! This check is just for safety, when exchange orders are completed the EXCHORDR record is deleted
            IF (exo:Location <> p_web.GSV('BookingSiteLocation'))
                CYCLE
            END



            IF (Access:SBO_OutParts.PrimeRecord() = Level:Benign)
                sout:SessionID = p_web.SessionID
                sout:PartNumber = exo:Model_Number
                sout:Description = exo:Manufacturer
                sout:Quantity = exo:Qty_Required - exo:qty_received
                sout:DateRaised = exo:DateCreated
                sout:LineType = 'E'
                IF (Access:SBO_OutParts.TryInsert() = Level:Benign)
                END

            END
        end
    end

    locRecordcount = 0

! Load loan queue
    Access:LOAORDR.ClearKey(lor:Ref_Number_Key)
    lor:Ref_Number = 1
    set(lor:Ref_Number_Key,lor:Ref_Number_Key)
    loop until Access:LOAORDR.Next()

        locRecordcount += 1

        if (clock() - bt) > 200
            bt=clock()

            UpdateProgress(p_web,INT((locRecordcount / RECORDS(LOAORDR)) * 100),kReturnURL,'Checking Loan Orders')

        end

        if lor:Qty_Received < lor:Qty_Required
            IF (exo:Location <> p_web.GSV('BookingSiteLocation'))
                CYCLE
            END

            IF (Access:SBO_OutParts.PrimeRecord() = Level:Benign)
                sout:SessionID = p_web.SessionID
                sout:PartNumber = lor:Model_Number
                sout:Description = lor:Manufacturer
                sout:Quantity = lor:Qty_Required - lor:Qty_Received
                sout:DateRaised = lor:DateCreated
                sout:LineType = 'L'
                IF (Access:SBO_OutParts.TryInsert() = Level:Benign)
                END

            END

        end
    end

    FLUSH(SBO_OutParts)
    FLUSH(ORDHEAD)
    FLUSH(ORDITEMS)
    FLUSH(RETSTOCK)
    FLUSH(RETSALES)
    FLUSH(EXCHORDR)
    FLUSH(LOAORDR)


BuildPartsList      ROUTINE
DATA
locRecordCount      LONG()
kReturnURL  EQUATE('FormBrowseStock')
locTotalRecords     LONG()
bt  LONG()
CODE

    p_web.SSV('RedirectURL','FormBrowseOldPartNumber')
    IF (p_web.GSV('locOldPartNumber') = '')
        EXIT
    END

    bt = CLOCK()
    UpdateProgress(p_web,0,kReturnURL)
    locTotalRecords = RECORDS(SBO_OutParts)

    ! Clear Existing Record
    Access:SBO_OutParts.ClearKey(sout:PartNumberKey)
    sout:SessionID = p_web.SessionID
    SET(sout:PartNumberKey,sout:PartNumberKey)
    LOOP UNTIL Access:SBO_OutParts.Next()
        IF (sout:SessionID <> p_web.SessionID)
            BREAK
        END

        locRecordcount += 1


        if (clock() - bt) > 200
            bt=clock()

            UpdateProgress(p_web,INT((locRecordcount /locTotalRecords ) * 100),kReturnURL,'Clearing...')

        end

        Delete(SBO_OutParts)
    END

    locRecordcount = 0


    Access:STOPARTS.Clearkey(spt:LocationOldPartKey)
    spt:Location = p_web.GSV('Default:SiteLocation')
    spt:OldPartNumber = p_web.GSV('locOldPartNumber')
    Set(spt:LocationOldPartKey,spt:LocationOldPartKey)
    Loop ! Begin Loop
        If Access:STOPARTS.Next()
            Break
        End ! If Access:STOPARTS.Next()

        locRecordcount += 1


        if (clock() - bt) > 200
            bt=clock()
            UpdateProgress(p_web,INT(locRecordCount / RECORDS(STOPARTS) * 100),kReturnURL,'Searching For Parts')
        END


        If spt:Location <> p_web.GSV('Default:SiteLocation')
            Break
        End ! If spt:Location <> f:Location
        If spt:OldPartNumber <> p_web.GSV('locOldPartNumber')
            Break
        End ! If spt:OldPartNumber <> f:PartNumber
        Access:STOCK.ClearKey(sto:Ref_Number_Key)
        sto:Ref_Number = spt:STOCKRefNumber
        If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
        !Found
        Else ! If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
        !Error
            Cycle
        End ! If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign

        IF (Access:SBO_OutParts.PrimeRecord() = Level:Benign)
            sout:SessionID = p_web.SessionID
            sout:LineType = 'S'
            sout:PartNumber = sto:Part_Number
            sout:Description = sto:Description
            sout:Quantity = sto:Ref_Number ! Use to get part number later
            IF (Access:SBO_OutParts.TryInsert())
                Access:SBO_OutParts.CancelAutoInc()
            END
        END

    End ! Loop
    Access:STOCK.ClearKey(sto:Location_Key)
    sto:Part_Number = p_web.GSV('locOldPartNumber')
    sto:Location = p_web.GSV('Default:SiteLocation')
    If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
    !Found
        IF (Access:SBO_OutParts.PrimeRecord() = Level:Benign)
            sout:SessionID = p_web.SessionID
            sout:LineType = 'S'
            sout:PartNumber = sto:Part_Number
            sout:Description = sto:Description
            sout:Quantity = sto:Ref_Number ! Use to get part number later
            IF (Access:SBO_OutParts.TryInsert())
                Access:SBO_OutParts.CancelAutoInc()
            END
        END
    Else ! If Access:STOCK.TryFetch(sto:Part_Number_Key) = Level:Benign
    !Error
    End ! If Access:STOCK.TryFetch(sto:Part_Number_Key) = Level:Benign

BuildExchangeAllocationQueue           Routine
Data
local:WrongSite     Byte(0)
local:FoundHistory  Byte(0)
locRecordCount      LONG()
locTotalRecords     LONG()
bt LONG()

Code

    bt=clock()

    UpdateProgress(p_web,0,p_web.GSV('RedirectURL'))

    ClearSBOGenericFile(p_web)

    Access:STATUS.ClearKey(sts:Ref_Number_Only_Key)
    sts:Ref_Number = 350
    If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key) = Level:Benign
        !Found
    Else!If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End!If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key) = Level:Benign

    locTotalRecords = RECORDS(JOBS)

    Access:JOBS.ClearKey(job:ExcStatusKey)
    job:Exchange_Status = sts:Status
    Set(job:ExcStatusKey,job:ExcStatusKey)
    Loop
        If Access:JOBS.NEXT()
            Break
        End !If
        If job:Exchange_Status <> sts:Status      |
            Then Break.  ! End If

        locRecordcount += 1
        if (clock() - bt) > 200
            bt=clock()

            UpdateProgress(p_web,INT((locRecordcount /locTotalRecords ) * 100),p_web.GSV('RedirectURL'),'Working..')

        end

        !Which engineer changed the status. Is that engineer at this site? - 4000 (DBH: 04-03-2004)
        local:WrongSite = False
        local:FoundHistory = False

        Access:AUDSTATS.ClearKey(aus:DateChangedKey)
        aus:RefNumber   = job:Ref_Number
        aus:Type        = 'EXC'
        aus:DateChanged = Today()
        Set(aus:DateChangedKey,aus:DateChangedKey)
        Loop
            If Access:AUDSTATS.PREVIOUS()
                Break
            End !If
            If aus:RefNumber   <> job:Ref_Number      |
                Or aus:Type        <> 'EXC'      |
                Then Break.  ! End If
            If aus:NewStatus = sts:Status
                Access:USERS.Clearkey(use:User_Code_Key)
                use:User_Code   = aus:UserCode
                If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                    !Found
                    If use:Location <> p_web.GSV('Default:SiteLocation')
                        local:WrongSite = True
                    End !If use:Location <> p_web.GSV('Default:SiteLocation')
                Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                    !Error
                End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                ! Inserting (DBH 13/04/2007) # 8912 - Found correct status history
                local:FoundHistory = True
                ! End (DBH 13/04/2007) #8912
                Break
            End !If aus:NewStatus = tmp:Status
        End !Loop


        If local:WrongSite = True
            Cycle
        End !If WrongSite# = True

        ! Inserting (DBH 13/04/2007) # 8912 - Add double check in case there is no status history, or even audit trail to determine location
        If local:FoundHistory = False
            Access:AUDIT.Clearkey(aud:TypeActionKey)
            aud:Ref_Number = job:Ref_Number
            aud:Type = 'EXC'
            aud:Action = '48 HOUR EXCHANGE ORDER CREATED'
            aud:Date = Today()
            Set(aud:TypeActionKey,aud:TypeActionKey)
            Loop ! Begin Loop
                If Access:AUDIT.Next()
                    Break
                End ! If Access:AUDIT.Next()
                If aud:Ref_Number <> job:Ref_Number
                    Break
                End ! If aud:Ref_Number <> job:Ref_Number
                If aud:Type <> 'EXC'
                    Break
                End ! If aud:Type <> 'EXC'
                If aud:Action <> '48 HOUR EXCHANGE ORDER CREATED'
                    Break
                End ! If aud:Action <> '48 HOUR EXCHANGE ORDER CREATED'
                If aud:Date > Today()
                    Break
                End ! If aud:Date > Today()
                Access:USERS.ClearKey(use:User_Code_Key)
                use:User_Code = aud:User
                If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
                    !Found
                    If use:Location <> p_web.GSV('Default:SiteLocation')
                        local:WrongSite = True
                    End ! If use:Location <> p_web.GSV('Default:SiteLocation')
                Else ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
                    !Error
                End ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
                local:FoundHistory = True
                Break
            End ! Loop

            If local:WrongSite = True
                Cycle
            End ! If local:WrongSite = True
        End ! If local:FoundStatusHistory = False

        If local:FoundHistory = False
            ! Can't find status or audit history, then just compare the booking location and assume the RRC ordered the exchange (DBH: 13/04/2007)
            Access:TRADEACC.ClearKey(tra:Account_Number_Key)
            tra:Account_Number = p_web.GSV('BookingAccount')
            If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                !Found
                If tra:SiteLocation <> p_web.GSV('Default:SiteLocation')
                    local:WrongSite = True
                End ! If tra:SiteLocation <> p_web.GSV('Default:SiteLocation')
            Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                !Error
            End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
        End ! If local:FoundHistory = False
        If local:WrongSite = True
            Cycle
        End ! If local:WrongSite = True
        ! End (DBH 13/04/2007) #8912




        IF (Access:SBO_GenericFile.PrimeRecord() = Level:Benign)
            sbogen:SessionID = p_web.SessionID
            sbogen:Long1 = job:Ref_Number
            sbogen:String1 = job:Manufacturer
            sbogen:String2 = job:Model_Number
            ! Use this to indicate if it's 48 Hour or not - TrkBs: 6841 (DBH: 09-12-2005)
            sbogen:Long2 = 0
            Access:EXCHORDR.ClearKey(exo:By_Location_Key)
            exo:Location     = p_web.GSV('Default:SiteLocation')
            exo:Manufacturer = job:Manufacturer
            exo:Model_Number = job:Model_Number
            If Access:EXCHORDR.TryFetch(exo:By_Location_Key) = Level:Benign
                !Found
                ! Use this field to indicate the "On Order" - TrkBs: 6841 (DBH: 09-12-2005)
                sbogen:Byte2 = 1
            Else!If Access:EXCHORDR.TryFetch(exo:By_Location_Key) = Level:Benign
                !Error
                sbogen:Byte2 = 0
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End!If Access:EXCHORDR.TryFetch(exo:By_Location_Key) = Level:Benign
            ! Use this field to indicate the "48 Hour" - TrkBs: 6841 (DBH: 09-12-2005)
            sbogen:Byte1 = 0

            If Access:SBO_GenericFile.TryInsert() = Level:Benign
                ! Insert Successful
            Else ! If Access:RAPENGLS.TryInsert() = Level:Benign
                ! Insert Failed
                Access:SBO_GenericFile.CancelAutoInc()
            End ! If Access:RAPENGLS.TryInsert() = Level:Benign
        End !If Access:RAPENGLS.PrimeRecord() = Level:Benign
    End !Loop

    Access:STATUS.ClearKey(sts:Ref_Number_Only_Key)
    sts:Ref_Number = 360
    If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key) = Level:Benign
        !Found

    Else!If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End!If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key) = Level:Benign


    Access:JOBS.ClearKey(job:ExcStatusKey)
    job:Exchange_Status = sts:Status
    Set(job:ExcStatusKey,job:ExcStatusKey)
    Loop
        If Access:JOBS.NEXT()
            Break
        End !If
        If job:Exchange_Status <> sts:Status      |
            Then Break.  ! End If

        locRecordcount += 1
        if (clock() - bt) > 200
            bt=clock()

            UpdateProgress(p_web,INT((locRecordcount /locTotalRecords ) * 100),p_web.GSV('RedirectURL'),'Working..')

        end


        !Which engineer changed the status. Is that engineer at this site? - 4000 (DBH: 04-03-2004)
        local:FoundHistory = False
        local:WrongSite = False

        Access:AUDSTATS.ClearKey(aus:DateChangedKey)
        aus:RefNumber   = job:Ref_Number
        aus:Type        = 'EXC'
        aus:DateChanged = Today()
        Set(aus:DateChangedKey,aus:DateChangedKey)
        Loop
            If Access:AUDSTATS.PREVIOUS()
                Break
            End !If
            If aus:RefNumber   <> job:Ref_Number      |
                Or aus:Type        <> 'EXC'      |
                Then Break.  ! End If
            If aus:NewStatus = sts:Status
                Access:USERS.Clearkey(use:User_Code_Key)
                use:User_Code   = aus:UserCode
                If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                    !Found
                    If use:Location <> p_web.GSV('Default:SiteLocation')
                        local:WrongSite = True
                    End !If use:Location <> p_web.GSV('Default:SiteLocation')
                Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                    !Error
                End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                local:FoundHistory = True
                Break
            End !If aus:NewStatus = tmp:Status
        End !Loop

        If local:WrongSite = True
            Cycle
        End !If WrongSite# = True

        ! Inserting (DBH 13/04/2007) # 8912 - Add double check in case there is no status history, or even audit trail to determine location
        If local:FoundHistory = False

            Access:AUDIT.Clearkey(aud:TypeActionKey)
            aud:Ref_Number = job:Ref_Number
            aud:Type = 'EXC'
            aud:Action = '48 HOUR EXCHANGE ORDER CREATED'
            aud:Date = Today()
            Set(aud:TypeActionKey,aud:TypeActionKey)
            Loop ! Begin Loop
                If Access:AUDIT.Next()
                    Break
                End ! If Access:AUDIT.Next()
                If aud:Ref_Number <> job:Ref_Number
                    Break
                End ! If aud:Ref_Number <> job:Ref_Number
                If aud:Type <> 'EXC'
                    Break
                End ! If aud:Type <> 'EXC'
                If aud:Action <> '48 HOUR EXCHANGE ORDER CREATED'
                    Break
                End ! If aud:Action <> '48 HOUR EXCHANGE ORDER CREATED'
                If aud:Date > Today()
                    Break
                End ! If aud:Date > Today()
                Access:USERS.ClearKey(use:User_Code_Key)
                use:User_Code = aud:User
                If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
                    !Found
                    If use:Location <> p_web.GSV('Default:SiteLocation')
                        local:WrongSite = True
                    End ! If use:Location <> p_web.GSV('Default:SiteLocation')
                Else ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
                    !Error
                End ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
                local:FoundHistory = True
                Break
            End ! Loop

            If local:WrongSite = True
                Cycle
            End ! If local:WrongSite = True
        End ! If local:FoundStatusHistory = False

        If local:FoundHistory = False
            ! Can't find status or audit history, then just compare the booking location and assume the RRC ordered the exchange (DBH: 13/04/2007)
            Access:TRADEACC.ClearKey(tra:Account_Number_Key)
            tra:Account_Number = p_web.GSV('BookingAccount')
            If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                !Found
                If tra:SiteLocation <> p_web.GSV('Default:SiteLocation')
                    local:WrongSite = True
                End ! If tra:SiteLocation <> p_web.GSV('Default:SiteLocation')
            Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                !Error
            End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
        End ! If local:FoundHistory = False
        If local:WrongSite = True
            Cycle
        End ! If local:WrongSite = True
        ! End (DBH 13/04/2007) #8912

        If Access:SBO_GenericFile.PrimeRecord() = Level:Benign
            sbogen:SessionID = p_web.SessionID
            sbogen:Long1 = job:Ref_Number
            sbogen:String1 = job:Manufacturer
            sbogen:String2 = job:Model_Number
            sbogen:Byte2 = 0
            ! Use this to indicate if it's 48 Hour or not - TrkBs: 6841 (DBH: 09-12-2005)
            sbogen:Long2 = 1
            !Has the exchange order been processed? -  (DBH: 30-10-2003)
            Access:EXCHOR48.ClearKey(ex4:LocationJobKey)
            ex4:Received  = 1
            ex4:Returning = 0
            ex4:Location  = p_web.GSV('Default:SiteLocation')
            ex4:JobNumber = sbogen:Long1
            If Access:EXCHOR48.TryFetch(ex4:LocationJobKey) = Level:Benign
                sbogen:Byte1 = 2
            Else !If Access:EXCHOR48.TryFetch(ex4:LocationJobKey) = Level:Benign
                sbogen:Byte1 = 1
            End !If Access:EXCHOR48.TryFetch(ex4:LocationJobKey) = Level:Benign
            If Access:SBO_GenericFile.TryInsert() = Level:Benign
                ! Insert Successful
            Else ! If Access:RAPENGLS.TryInsert() = Level:Benign
                ! Insert Failed
                Access:SBO_GenericFile.CancelAutoInc()
            End ! If Access:RAPENGLS.TryInsert() = Level:Benign
        End !If Access:RAPENGLS.PrimeRecord() = Level:Benign

    End !Loop

SendPacket  Routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet,1,packetlen,Net:NoHeader)
    packet = ''
  end
Header Routine
  packet = p_web.w3Header(p_web.Combine(p_web.site.HtmlClass,))
  packet = clip(packet) & '<head>'&|
      '<title>'&p_web.Translate(p_web.site.PageTitle)&'</title>'&|
      '<meta http-equiv="Content-Type" content="text/html; charset='&clip(p_web.site.HtmlCharset)&'" /><13,10>' &|
      clip(p_web.MetaHeaders)
    Do declareProgressBar
    Do redirect
  packet =  clip(packet) & p_web.IncludeStyles()
  packet =  clip(packet) & p_web.IncludeScripts()
  packet = clip(packet) & '</head><13,10>'
  p_web.ParseHTML(packet,1,0,Net:SendHeader+Net:DontCache)
  packet = ''
Footer Routine
  packet = clip(packet) & '<!-- Net:SelectField --><13,10>' &|
                         '</div></body><13,10></html><13,10>'
declareProgressBar  Routine
  packet = clip(packet) & p_web.AsciiToUTF(|
    '<<!-- Net:DeclareProgressBar --><13,10>'&|
    '',net:OnlyIfUTF)
heading  Routine
  packet = clip(packet) & p_web.AsciiToUTF(|
    '<<!-- Net:BannerBlank --><13,10>'&|
    '',net:OnlyIfUTF)
redirect  Routine
    packet = clip(packet) & p_web.AsciiToUTF(|
        '<<META HTTP-EQUIV="refresh" CONTENT="1;URL=' & p_web.GSV('RedirectURL') & '"><13,10>'&|
        '',net:OnlyIfUTF)
  packet = clip(packet) & p_web.AsciiToUTF(|
    '<13,10>'&|
    '',net:OnlyIfUTF)
ShowWait  Routine
  packet = clip(packet) & p_web.AsciiToUTF(|
    '<13,10>'&|
    '',net:OnlyIfUTF)
  do sendpacket
  
  CASE p_web.GSV('ProcessType')
  OF 'ExchangeAllocation'
      DO BuildExchangeAllocationQueue
  OF 'OutstandingOrders'
  DO BuildOutstandingOrders
  OF 'OldPartNumber'
      DO BuildPartsList
  END ! Case
CloseWait  Routine
    packet = clip(packet) & p_web.AsciiToUTF(|
        '<<!-- Net:CloseWait --><13,10>'&|
        '<<div id="waitframe" class="nt-process"><13,10>'&|
        '<<p class="nt-process-text">Process Completed<<BR /><<br/><13,10>'&|
        'If you are not re-directed then click <<a href="' & p_web.GSV('ReturnURL') & '">here<</a> to view the results.<13,10>'&|
        '<</p><13,10>'&|
        '<<BR/><<BR/><13,10>'&|
        '<<BR/><<BR/><13,10>'&|
        '<<BR/><<BR/><13,10>'&|
        '<</div><13,10>'&|
        '',net:OnlyIfUTF)
  packet = clip(packet) & p_web.AsciiToUTF(|
    '<13,10>'&|
    '',net:OnlyIfUTF)
